﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Data.OperationParameters;
using SolarWinds.Orion.IpSla.Web.UI.OperationParameters;
using SolarWinds.Orion.IpSla.Web;
using System.Web.Script.Serialization;
using SolarWinds.Orion.IpSla.Data;
using System.ComponentModel;

public partial class Orion_Voip_Controls_EditOperation : System.Web.UI.UserControl
{
    /// <summary>
    /// If true edit-checkboxes for parameters are visible if all operations equals,
    /// if false edit-checkboxes are visible if there is more than one operation to edit
    /// </summary>
    public bool SmartMode { get; set; }

    /// <summary>
    /// If true, exclude parameters/settings that have individual nature.
    /// </summary>
    public bool ExcludeIndividualSettings { get; set; }

    public void Initialize(OperationDefinitionCollection operations, bool doNotLoadData)
    {
        bool allParamValuesEquals = this.InitializeChildrenData(this, operations, doNotLoadData);
        if (this.ExcludeIndividualSettings)
        {
            this.HideIndividualForChildern(this);
        }
        if (this.SmartMode && allParamValuesEquals)
        {
            this.HideEnableEditForChildern(this);
        }
    }

    public void Modify(OperationDefinitionCollection operations)
    {
        this.ModifyDataFromChildren(this, operations);
    }

    private bool InitializeChildrenData(Control parent, OperationDefinitionCollection operations, bool doNotLoadData)
    {
        bool allParamValuesEquals = true;
        foreach (Control ctl in parent.Controls)
        {
            var paramCtl = ctl as OperationParameterControl;
            if (paramCtl != null)
            {
                allParamValuesEquals = paramCtl.Initialize(operations, doNotLoadData) && allParamValuesEquals;
            }
            else
            {
                this.InitializeChildrenData(ctl, operations, doNotLoadData);
            }
        }
        return allParamValuesEquals;
    }

    private void ModifyDataFromChildren(Control parent, OperationDefinitionCollection operations)
    {
        foreach (Control ctl in parent.Controls)
        {
            var paramCtl = ctl as OperationParameterControl;
            if (paramCtl != null)
            {
                paramCtl.Modify(operations);
            }
            else
            {
                this.ModifyDataFromChildren(ctl, operations);
            }
        }
    }

    private void HideEnableEditForChildern(Control parent)
    {
        foreach (Control ctl in parent.Controls)
        {
            var paramCtl = ctl as OperationParameterControl;
            if (paramCtl != null)
            {
                if (paramCtl.Visible)
                {
                    paramCtl.HideEnableEdit = true;
                }
            }
            else
            {
                this.HideEnableEditForChildern(ctl);
            }
        }
    }

    private void HideIndividualForChildern(Control parent)
    {
        foreach (Control ctl in parent.Controls)
        {
            var paramCtl = ctl as OperationParameterControl;
            if (paramCtl != null)
            {
                if (paramCtl.Visible && paramCtl.IsIndividual)
                {
                    paramCtl.Visible = false;
                }
            }
            else
            {
                this.HideIndividualForChildern(ctl);
            }
        }
    }

    private static bool IsValid(Control parent)
    {
        foreach (Control ctl in parent.Controls)
        {
            var validator = ctl as IValidator;
            if (validator != null && !validator.IsValid)
                return false;
            if (!IsValid(ctl))
                return false;
        }
        return true;
    }

    protected override void OnPreRender(EventArgs e)
    {
        var scriptManager = ScriptManager.GetCurrent(Page);
        if (scriptManager.IsInAsyncPostBack)
        {
            scriptManager.RegisterDataItem(this,
                IsValid(this) ? "{IsValid:true}" : "{IsValid:false}",
                true);
        }
        base.OnPreRender(e);
        this.AdvancedPanel.Visible = this.opCodecTypeSelect.Visible || this.opTypeOfService.Visible ||
            this.opVrfName.Visible || this.opSourceIpAddress.Visible || this.opTargetIpAddress.Visible;
        this.ThresholdHeader.Visible = this.RTTThreshold.Visible || this.PacketLossThreshold.Visible ||
            this.MOSThreshold.Visible || this.JitterThreshold.Visible || this.LatencyThreshold.Visible ||
            this.NumberOfHopsThreshold.Visible;
    }
}
