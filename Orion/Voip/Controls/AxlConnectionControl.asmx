<%@ WebService Language="C#" Class="Orion.Voip.Controls.Axl.AxlConnectionControlService" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Contracts;
using SolarWinds.Orion.IpSla.Web;

namespace Orion.Voip.Controls.Axl
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class AxlConnectionControlService : WebService
    {
        private static readonly Log log = new Log();

        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public object GetCredentials()
        {
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();

            try
            {
                IEnumerable<AxlCredentialsPublic> credentialsList;
                using (var blProxy = GetBusinessLayer())
                {
                    credentialsList = blProxy.GetAxlCredentials();
                }

                var list = (
                    from credentials in credentialsList
                    select new
                        {
                            id = credentials.Id,
                            name = credentials.Name,
                            user = credentials.Username
                        }
                    ).ToList();

                log.DebugFormat("AXL credentials retrieved. [Count={0}]", list.Count);
                return list;
            }
            catch (Exception ex)
            {
                log.Error("Error while getting AXL credentials.", ex);
                throw;
            }
        }

        [WebMethod]
        public object ValidateAxlConnectionCredentialId(IEnumerable<int> nodeIds, string ip, int credentialId)
        {
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();
            try
            {
                IPAddress ipAddress;
                if (!IPAddress.TryParse(ip, out ipAddress))
                    throw new ApplicationException("Node IP address has invalid format.");
                using (var blProxy = GetBusinessLayer())
                {
                    return blProxy.ValidateAxlCredentials(nodeIds, ipAddress, credentialId);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error while validating AXL credentials.", ex);
                throw;
            }
        }

        [WebMethod]
        public object ValidateAxlConnectionCredentials(IEnumerable<int> nodeIds, string ip, string username, string password)
        {
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();
            try
            {
                if (username == null)
                    throw new ArgumentNullException(nameof(username));
                if (password == null)
                    throw new ArgumentNullException(nameof(password));

                IPAddress ipAddress;
                if (!IPAddress.TryParse(ip, out ipAddress))
                    throw new ApplicationException("Node IP address has invalid format.");
                using (var blProxy = GetBusinessLayer())
                {
                    return blProxy.ValidateAxlCredentials(nodeIds, ipAddress, username, password);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error while validating AXL credentials.", ex);
                throw;
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public object GetCommonAxlCredentials(IEnumerable<int> nodeIds)
        {
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();
            using (var blProxy = GetBusinessLayer())
            {
                int lastCredentialsId = 0;
                foreach (int nodeId in nodeIds)
                {
                    int credentialsId = blProxy.GetNodeAxlCredentialsId(nodeId);
                    if (lastCredentialsId == 0)
                    {
                        lastCredentialsId = credentialsId;
                    }
                    else if (lastCredentialsId != credentialsId)
                    {
                        lastCredentialsId = 0;
                        break;
                    }
                }

                return new
                           {
                               CredentialId = lastCredentialsId
                           };
            }
        }

        private IIpSlaBusinessLayer GetBusinessLayer()
        {
            return
                BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>(
                    this.Context.Request.AppRelativeCurrentExecutionFilePath);
        }
    }
}
