<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PeriodSelector.ascx.cs" Inherits="Orion_Voip_Resources_PeriodSelector" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="voip" TagName="RelativePeriod" Src="~/Orion/Voip/Controls/RelativePeriod.ascx" %>
<%@ Register TagPrefix="voip" TagName="TimeDate" Src="~/Orion/Voip/Controls/TimeDate.ascx" %>

<table>
<tr>
    <td>
        <input type="radio" runat="server" id="radNamed" name="PeriodType"  /><span style="padding-left:3px;"><%= VNQMWebContent.VNQMWEBDATA_TM0_89 %></span>
    </td>
    <td>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:ListBox runat="server"  CssClass="PeriodSelector"  ID="lbxNamedPeriods" Rows="1" Width="135px" />
    </td>
</tr>
<tr>
    <td>
        <input type="radio" runat="server" id="radRelative" name="PeriodType" /><span style="padding-left:3px;"><%= VNQMWebContent.VNQMWEBDATA_TM0_90 %></span>
    </td>
    <td>
        <voip:RelativePeriod runat="server" ID="relativePeriod" ValidationEnabled="false" />
    </td>
</tr>
<tr>
    <td>
        <input type="radio" runat="server" id="radAbsolute"  name="PeriodType" /><span style="padding-left:3px;"><%= VNQMWebContent.VNQMWEBDATA_TM0_91 %></span>
    </td>
</tr>

 <tr>
    <td>
    <br />
         <%= VNQMWebContent.VNQMWEBDATA_TM0_92 %> <voip:TimeDate runat="server" ID="StartTime" />
    </td>
    <td>
    <br />
         <%= VNQMWebContent.VNQMWEBDATA_TM0_93 %> <voip:TimeDate runat="server" ID="EndTime"  />
    </td>
 </tr>
</table>