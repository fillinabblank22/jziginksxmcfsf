﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FtpSettingsControl.ascx.cs"
    Inherits="Orion_Voip_Admin_Controls_Ftp_FtpSettingsControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="ipsla" TagName="TestResultControl" Src="~/Orion/Voip/Controls/TestResultControl.ascx" %>
<orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include ID="Include2" runat="server" Module="Voip" File="Voip.css" />
<asp:ScriptManagerProxy ID="scriptManager" runat="server">
    <scripts>
        <%-- This is required to fix issues with nested script controls under Chrome and Safari.
        See: http://blog.lavablast.com/post/2008/10/20/Gotcha-WebKit-(Safari-3-and-Google-Chrome)-Bug-with-ASPNET-AJAX.aspx --%>
        <asp:ScriptReference Path="~/Orion/Voip/js/WebKit_fix.js" />
    </scripts>
    <services>
        <asp:ServiceReference Path="~/Orion/Voip/Controls/FtpSettingsControlService.asmx" />
    </services>
</asp:ScriptManagerProxy>
<script type="text/javascript">
    function VoipFtpValidateName_<%= this.ClientID %>(sender, args) {
        if ($('#<%= this.tbReturnCredentialsId.ClientID %>').val() == 0
            && $('#<%= this.tbUsername.ClientID %>').val() == '') {
            args.IsValid = false;
            return;
        }

        args.IsValid = true;
    }
</script>
<table>
    <tr>
        <td>
            <table class="voipAxlCredentialsTable">
                <tr>
                    <td class="leftLabelColumn">
                        <%= FTPServerLabel %>:
                    </td>
                    <td class="rightInputColumn">
                        <asp:TextBox runat="server" ID="tbFTPServer" Width="250"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tbFTPServer"
                            Display="Dynamic" ValidationGroup="VoipFtpSettings">
                <%= RequiredFieldError %>
                        </asp:RequiredFieldValidator>
                        <span class="helpfulText">
                            <%= FTPServerHelpText %></span>
                    </td>
                </tr>
                <tr>
                    <td class="leftLabelColumn">
                        <%= PortLabel %>:
                    </td>
                    <td class="rightInputColumn">
                        <asp:TextBox runat="server" ID="tbPort" Width="25"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                            TargetControlID="tbPort" FilterType="Numbers" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="tbPort"
                            Display="Dynamic" ValidationGroup="VoipFtpSettings">
                <%= RequiredFieldError %>
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="leftLabelColumn">
                    </td>
                    <td class="rightInputColumn">
                        <asp:CheckBox ID="cbPassiveMode" runat="server" Checked="True" />
                        <asp:Label ID="Label1" runat="server" AssociatedControlID="cbPassiveMode"><%= PassiveModeLabel %></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="leftLabelColumn">
                    </td>
                    <td class="rightInputColumn">
                        <asp:CheckBox ID="cbSecureConnection" runat="server" />
                        <asp:Label ID="Label2" runat="server" AssociatedControlID="cbSecureConnection"><%= SecureConnectionLabel %></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="leftLabelColumn">
                        <%= PathLabel %>:
                    </td>
                    <td class="rightInputColumn">
                        <asp:TextBox runat="server" ID="tbPath" Width="250"></asp:TextBox>
                        <span class="helpfulText">
                            <%= PathHelpText %></span>
                    </td>
                </tr>
                <tr>
                    <td class="leftLabelColumn">
                        <%= SelectCredentialsLabel %>:
                    </td>
                    <td class="rightInputColumn">
                        <asp:HiddenField ID="tbReturnCredentialsId" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="tbReturnCredentialsName" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="tbSavedCredentialsId" runat="server"></asp:HiddenField>
                        <asp:TextBox ID="tbSavedCredentialsName" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="leftLabelColumn">
                        <%= UsernameLabel %>:
                    </td>
                    <td class="rightInputColumn">
                        <asp:TextBox ID="tbUsername" runat="server"></asp:TextBox>
                        <asp:CustomValidator runat="server" ID="userNameValidator" OnServerValidate="userNameValidator_OnServerValidate"
                            ControlToValidate="tbUsername" Display="Dynamic" ValidationGroup="VoipFtpSettings"
                            ValidateEmptyText="True">
                <%= RequiredFieldError %>
                        </asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td class="leftLabelColumn">
                        <%= PasswordLabel %>:
                    </td>
                    <td class="rightInputColumn">
                        <asp:TextBox ID="tbPassword" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="leftLabelColumn">
                        &nbsp;
                    </td>
                    <td class="rightInputColumn">
                        <div style="padding-top: 5px; padding-bottom: 5px;">
                            <orion:LocalizableButton ID="btnValidate" runat="server" DisplayType="Small" LocalizedText="CustomText"
                                CausesValidation="True" ValidationGroup="VoipFtpSettings" Text="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_127 %>" />
                            <ipsla:TestResultControl ID="testResult" runat="server" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <div id="validationProgress" runat="server" style="display: none;">
                            <img src="/Orion/images/animated_loading_sm3_whbg.gif" style="vertical-align: middle;" />
                            <span><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_81%></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="leftLabelColumn">
                        <%= PollingFrequencyLabel %>:
                    </td>
                    <td class="rightInputColumn">
                        <asp:TextBox runat="server" ID="tbFrequency" Width="25"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbe" runat="server" TargetControlID="tbFrequency"
                            FilterType="Numbers" />
                        <%= PollingFrequencyUnitLabel%>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="tbFrequency"
                            Display="Dynamic" ValidationGroup="VoipFtpSettings">
                <%= RequiredFieldError %>
                        </asp:RequiredFieldValidator>
                        <span class="helpfulText">
                            <%= PollingFrequencyHelpText%></span>
                    </td>
                </tr>
                <tr>
                    <td class="leftLabelColumn">
                    </td>
                    <td class="rightInputColumn">
                        <asp:CheckBox ID="cbDeleteFilesAfterDownload" runat="server" />
                        <asp:Label ID="Label3" runat="server" AssociatedControlID="cbDeleteFilesAfterDownload"><%= DeleteCDRLabel %></asp:Label>
                        <span class="paddingHelpfulText"><a href="<%= DeleteCDRHelpLink %>" class="sw-link"
                            target="_blank">
                            <%= DeleteCDRHelpText %></a></span>
                    </td>
                </tr>
            </table>
        </td>
        <td class="voipFtpBannerAligment">
            <table class="voipFtpBanner" style="width: 420px;">
                <tr>
                    <td class="voipFtpBannerAligment">
                        <img id="imgBanner" runat="server" src="~/Orion/Voip/images/lightbulb_important_text.gif"
                            alt="" />
                    </td>
                    <td class="voipFtpBannerAligment voipFTPBannerTextPadding">
                        <%= BannerText %>
                        <br />
                        <br />
                        <a href="<%= BannerLink %>" class="VNQM_HelpLink" target="_blank">
                            <%= BannerLinkText %></a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<%-- Render the DIV control only in demo mode --%>
<%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
  {%>
<div id="isDemoModeFtp" style="display: none;">
</div>
<%}%>