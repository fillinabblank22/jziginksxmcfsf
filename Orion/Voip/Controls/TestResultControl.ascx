<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TestResultControl.ascx.cs"
    Inherits="Orion_Voip_Controls_Cli_TestResultControl" %>

<span id="elSuccess" runat="server" style="vertical-align: middle;"><span class="validationTest success">
    <img src="/Orion/images/nodemgmt_art/icons/icon_OK.gif" alt="Success" />
    <span><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_72%></span>
</span></span>
<span id="elFailed" runat="server" style="vertical-align: middle;"><span class="validationTest failure">
    <img src="/Orion/Voip/images/stop_16x16.gif" alt="Failed" />
    <span><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_74%></span>
</span></span>
