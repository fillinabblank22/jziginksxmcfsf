﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OrionNodesInfo.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_Operations_OrionNodesInfo" %>

<table border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td style="padding: 0px 3px 0px 0px; vertical-align: top;">
      <img src="/Orion/Voip/images/Icon.Info_16.gif" alt="Information" />
    </td>
    <td>
      <span id="message" runat="server" />
      You may <a href="/Orion/Nodes/Add/Default.aspx" title="Add nodes manually">manually add nodes</a> or add nodes with <a href="/Orion/Discovery/Default.aspx?origUrl=Nodes" title="Network Sonar Discovery">Network Sonar Discovery</a>.
    </td>
  </tr>
</table>
