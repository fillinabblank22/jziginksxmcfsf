<%@ Control Language="C#" ClassName="Metric" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web.DisplayTypes" %>

<script runat="server">
    private VoipMetric _value;

	public VoipMetric Value
    {
        get { return _value; }
        set { _value = value; }
    }

    protected string ClassName
    {
        get
        {
			return string.Format("{0:class}", Value);
        }
    }
</script>

<span class="<%= this.ClassName %>"><%= this.Value %></span>
