﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.IpSla.Common.Settings;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;

public partial class Orion_Voip_Controls_SearchVoipCalls : ScriptUserControlBase
{
    private const string CallManagerListItemValue_All = "0|ALL";
    private const string CallManagerListItemValue_AllCisco = "-1|ALLCISCO";
    private const string CallManagerListItemValue_AllAvaya = "-2|ALLAVAYA";

    public Control ResultsControl { get; set; }

    /// <summary>
    // Submit button id is used for enabling/disabling from SearchVoipCallsResults control
    /// </summary>
    public string SubmitButtonId => submitButton.ClientID;

    /// <summary>
    /// SearchCriteria Label Id is used to get the searchcriteria value for including it in Excel file when search results are exported
    /// </summary>
    public string SearchCriteriaLabelId => lblSearchCriteria.ClientID;

    protected void Page_Load(object sender, EventArgs e)
    {
       
        FillCallProtocolDropdowns();

        if (!IsPostBack){

            dtStartDateTime.Value = DateTime.Now.Date;
            dtEndDateTime.Value = DateTime.Now;
            int ccmCount = 0;
            int acmCount = 0;
            
            using ( var ccmTable = CallManagersDAL.Instance.GetCallManagerCallSearchData(CallManagerType.CCM, CallManagerType.ACM) )
            {

            
            var ccmQuery = from ccm in ccmTable.AsEnumerable()
                           select new
                               {
                                   Caption = ccm["Caption"],
                                   IdAndType = $"{ccm["ID"]}|{ccm["TypeCode"]}"
                               };

            ddlCallManagers.DataSource = ccmQuery;

            ddlCallManagers.DataTextField = "Caption";
            ddlCallManagers.DataValueField = "IdAndType";
            ddlCallManagers.DataBind();
            
            ddlCallManagers.Items.Insert(0, new ListItem(Resources.VNQMWebContent.VNQMWEBDATA_AK1_97, CallManagerListItemValue_All));

            ccmCount = ccmTable.AsEnumerable()
                               .Count( x => (Convert.ToInt32(x["CCMMonitoringTypeID"]) == (int)CallManagerType.CCM) ||                  
                                            (Convert.ToInt32(x["CCMMonitoringTypeID"]) == (int)CallManagerType.CCME));

            acmCount = ccmTable.AsEnumerable()
                               .Count(y => (Convert.ToInt32(y["CCMMonitoringTypeID"])) == (int)CallManagerType.ACM);
            }

            if (ccmCount != 0)
            {
                ddlCallManagers.Items.Insert(1, new ListItem(Resources.VNQMWebContent.VNQMWEBDATA_VM_2, CallManagerListItemValue_AllCisco));
            }
            if (acmCount != 0)
            {
                ddlCallManagers.Items.Insert(1, new ListItem(Resources.VNQMWebContent.VNQMWEBDATA_VM_1, CallManagerListItemValue_AllAvaya));
            }

            var gatewayTable = GatewaysDAL.Instance.GetAllGatewayRecords();

            ddlOriginGateway.DataSource = gatewayTable;
            ddlOriginGateway.DataTextField = "DisplayName";
            ddlOriginGateway.DataValueField = "ID";
            ddlOriginGateway.DataBind();

            ddlOriginGateway.Items.Insert(0, new ListItem(Resources.VNQMWebContent.VNQMWEBDATA_AK1_129, "-1"));

            ddlDestinationGateway.DataSource = gatewayTable;
            ddlDestinationGateway.DataTextField = "DisplayName";
            ddlDestinationGateway.DataValueField = "ID";
            ddlDestinationGateway.DataBind();

            ddlDestinationGateway.Items.Insert(0, new ListItem(Resources.VNQMWebContent.VNQMWEBDATA_AK1_129, "-1"));

            using (var regionTable = RegionsDAL.Instance.GetAllRegionRecords())
            {
                ddlOriginRegion.DataSource = regionTable;
                ddlOriginRegion.DataTextField = "RegionName";
                ddlOriginRegion.DataValueField = "RegionID";
                ddlOriginRegion.DataBind();

                ddlOriginRegion.Items.Insert(0, new ListItem(Resources.VNQMWebContent.VNQMWEBDATA_AK1_129, "-1"));

                ddlDestinationRegion.DataSource = regionTable;
                ddlDestinationRegion.DataTextField = "RegionName";
                ddlDestinationRegion.DataValueField = "RegionID";
                ddlDestinationRegion.DataBind();

                ddlDestinationRegion.Items.Insert(0, new ListItem(Resources.VNQMWebContent.VNQMWEBDATA_AK1_129, "-1"));
            }

            ddlCallTerminationCause.Items.Add(new ListItem(Resources.VNQMWebContent.VNQMWEBDATA_AK1_97, "-1"));
            var causeCodeList = IpSlaSettings.VoipTerminationCauseCodes.Value;
            if (!string.IsNullOrEmpty(causeCodeList))
            {
                AddCauseCodestoDropDown(causeCodeList);
            }
            else
            {                
                ddlCallTerminationCause.Items.AddRange((from VoipCallTerminationCause n in Enum.GetValues(typeof(VoipCallTerminationCause))
                                                        select new ListItem() { Value = ((int)n).ToString(), Text = string.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_7, ((int)n).ToString(), GetLocalizedProperty("VoipCallTerminationCause", n.ToString())) }).ToArray());
            }

            ddlRedirectReason.Items.Add(new ListItem(Resources.VNQMWebContent.VNQMWEBDATA_AK1_97, "-1"));
            var redirectionCodeList = IpSlaSettings.VoipRedirectionCodes.Value;
            if (!string.IsNullOrEmpty(redirectionCodeList))
            {
                AddRedirectionCodestoDropDown(redirectionCodeList);
            }
            else
            {
                ddlRedirectReason.Items.AddRange((from VoipCallRedirectReason n in Enum.GetValues(typeof(VoipCallRedirectReason))
                                                  select new ListItem() { Value = ((int)n).ToString(), Text = string.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_7, ((int)n).ToString(), GetLocalizedProperty("VoipCallRedirectReason", n.ToString())) }).ToArray());
            }


            ddlAvayaCondCode.Items.Add(new ListItem(Resources.VNQMWebContent.VNQMWEBDATA_AK1_97, "-1"));
            var avayaCondCodeList = IpSlaSettings.AvayaConditionCodes.Value;
            if (!string.IsNullOrEmpty(avayaCondCodeList))
            {
                AddAvayaConditionCodesToDropDown(avayaCondCodeList);
            }
            else
            {
                ddlAvayaCondCode.Items.AddRange((from AvayaConditionCodes n in Enum.GetValues(typeof(AvayaConditionCodes))
                                                 select new ListItem() { Value = ((char)n).ToString(), Text = string.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_7, (char)n, GetLocalizedProperty("AvayaConditionCodes", n.ToString())) }).ToArray());
            }


            ddlCallOriginationType.Attributes["onchange"] = "$find('" + this.ClientID + "').origintypechanged();";
            ddlCallDestinationType.Attributes["onchange"] = "$find('" + this.ClientID + "').destinationtypechanged();";
            
            ddlCallTime.Attributes["onchange"] = "$find('" + this.ClientID + "').calltimechanged();";

            ddlMOS.Attributes["onchange"] = "$find('" + this.ClientID + "').callqualitymoschanged();";
            ddlLatency.Attributes["onchange"] = "$find('" + this.ClientID + "').callqualitylatencychanged();";
            ddlJitter.Attributes["onchange"] = "$find('" + this.ClientID + "').callqualityjitterchanged();";
            ddlPacketLoss.Attributes["onchange"] = "$find('" + this.ClientID + "').callqualitypacketlosschanged();";
            ddlCallManagers.Attributes["onchange"] = "$find('" + this.ClientID + "').callmanagerchanged()";
            submitButton.Attributes["onclick"] = "return $find('" + this.ClientID + "').submitclick();";

            ((TextBox)dtStartDateTime.FindControl("txtTimePicker")).Attributes["onblur"] = "$find('" + this.ClientID + "').validatestarttime();";
            ((TextBox)dtEndDateTime.FindControl("txtTimePicker")).Attributes["onblur"] = "$find('" + this.ClientID + "').validateendtime();";
        }
    }

    private void AddCauseCodestoDropDown(string causeCodeList)
    {
        var causeCodes = causeCodeList.Split(',');

        foreach (var causeCode in causeCodes)
        {
            var code = 0;
            if (int.TryParse(causeCode, out code))
            {
                if (Enum.IsDefined(typeof(VoipCallTerminationCause), code))
                {
                    var cause = (VoipCallTerminationCause)code;
                    ddlCallTerminationCause.Items.Add(new ListItem(string.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_7, causeCode, GetLocalizedProperty("VoipCallTerminationCause", cause.ToString())), causeCode));
                }
            }
        }
    }

    private void AddRedirectionCodestoDropDown(string redirectionCodeList)
    {
        var redirectionCodes = redirectionCodeList.Split(',');

        foreach (var redirectionCode in redirectionCodes)
        {
            var code = 0;
            if (int.TryParse(redirectionCode, out code) && Enum.IsDefined(typeof (VoipCallRedirectReason), code))
            {
                var reason = (VoipCallRedirectReason) code;
                ddlRedirectReason.Items.Add(
                    new ListItem(
                        string.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_7, redirectionCode,
                            GetLocalizedProperty("VoipCallRedirectReason", reason.ToString())), redirectionCode));
            }
        }
    }

    private void AddAvayaConditionCodesToDropDown(string avayaCondCodesList)
    {
        var avayaCondCodes = avayaCondCodesList.Split(',');

        foreach (var avayaCondCode in avayaCondCodes)
        {
            char code;
            if (char.TryParse(avayaCondCode, out code))
            {
                if (Enum.IsDefined(typeof(AvayaConditionCodes), (int)code))
                {
                    var reason = (AvayaConditionCodes)code;
                    ddlAvayaCondCode.Items.Add(new ListItem(string.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_7, avayaCondCode, GetLocalizedProperty("AvayaConditionCodes", reason.ToString())), code.ToString()));
                }
            }
        }
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        var manager = ResourceManagerRegistrar.Instance;
        var key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }

    private void FillCallProtocolDropdowns()
    {
        foreach (var protocol in Enum.GetValues(typeof(VoipCallProtocol)))
        {
            var listItem = new ListItem(Enum.GetName(typeof(VoipCallProtocol), protocol), protocol.ToString());
            if (listItem.Value == VoipCallProtocol.All.ToString()) continue;
            ddlIncomingCallProtocol.Items.Add(listItem);
            ddlOutgoingCallProtocol.Items.Add(listItem);
        }
    }
    #region IScriptControl Members

    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        var descriptor = new ScriptControlDescriptor("Orion.Voip.Controls.SearchVoipCalls", this.ClientID);

        descriptor.AddElementProperty("rbSearchCriteriaOR", rbSearchCriteriaOR.ClientID);
        descriptor.AddElementProperty("rbSearchCriteriaAND", rbSearchCriteriaAND.ClientID);

        descriptor.AddElementProperty("ddlCallOriginationType", ddlCallOriginationType.ClientID);
        descriptor.AddElementProperty("divCallOriginationIPAddressorPhone", divCallOriginationIPAddressorPhone.ClientID);
        descriptor.AddElementProperty("ddlOriginGateway", ddlOriginGateway.ClientID);
        descriptor.AddElementProperty("ddlOriginRegion", ddlOriginRegion.ClientID);
        descriptor.AddElementProperty("txtOriginIPAddressorPhone", txtOriginIPAddressorPhone.ClientID);

        descriptor.AddElementProperty("ddlCallDestinationType", ddlCallDestinationType.ClientID);
        descriptor.AddElementProperty("divCallDestinationIPAddressorPhone", divCallDestinationIPAddressorPhone.ClientID);
        descriptor.AddElementProperty("ddlDestinationGateway", ddlDestinationGateway.ClientID);
        descriptor.AddElementProperty("ddlDestinationRegion", ddlDestinationRegion.ClientID);
        descriptor.AddElementProperty("txtDestinationIPAddressorPhone", txtDestinationIPAddressorPhone.ClientID);

        descriptor.AddElementProperty("ddlCallManagers", ddlCallManagers.ClientID);

        descriptor.AddElementProperty("ddlCallTime", ddlCallTime.ClientID);
        descriptor.AddElementProperty("divCallTime", divCallTime.ClientID);
        descriptor.AddElementProperty("txtStartDate", dtStartDateTime.FindControl("txtDatePicker").ClientID);
        descriptor.AddElementProperty("txtStartTime", dtStartDateTime.FindControl("txtTimePicker").ClientID);
        descriptor.AddElementProperty("txtEndDate", dtEndDateTime.FindControl("txtDatePicker").ClientID);
        descriptor.AddElementProperty("txtEndTime", dtEndDateTime.FindControl("txtTimePicker").ClientID);

        descriptor.AddElementProperty("cbFailed", cbFailed.ClientID);
        descriptor.AddElementProperty("cbNoZeroLength", cbNoZeroLength.ClientID);
        descriptor.AddElementProperty("cbSuccessful", cbSuccessful.ClientID);
        descriptor.AddElementProperty("cbCallWithIssue", cbCallWithIssue.ClientID);
        descriptor.AddElementProperty("cbConferenceCall", cbConferenceCall.ClientID);
        descriptor.AddElementProperty("cbRedirectReason", cbRedirectReason.ClientID);
        descriptor.AddElementProperty("ddlRedirectReason", ddlRedirectReason.ClientID);
        descriptor.AddElementProperty("cbCallTerminationCause", cbCallTerminationCause.ClientID);
        descriptor.AddElementProperty("ddlCallTerminationCause", ddlCallTerminationCause.ClientID);
        descriptor.AddElementProperty("cbAvayaCondCode", cbAvayaCondCode.ClientID);
        descriptor.AddElementProperty("ddlAvayaCondCode", ddlAvayaCondCode.ClientID);

        descriptor.AddElementProperty("cbMOS", cbMOS.ClientID);
        descriptor.AddElementProperty("ddlMOS", ddlMOS.ClientID);
        descriptor.AddElementProperty("divMOS", divMOS.ClientID);
        descriptor.AddElementProperty("txtMOSLimit1", txtMOSLimit1.ClientID);
        descriptor.AddElementProperty("txtMOSLimit2", txtMOSLimit2.ClientID);
        descriptor.AddElementProperty("cbLatency", cbLatency.ClientID);
        descriptor.AddElementProperty("ddlLatency", ddlLatency.ClientID);
        descriptor.AddElementProperty("divLatency", divLatency.ClientID);
        descriptor.AddElementProperty("txtLatencyLimit1", txtLatencyLimit1.ClientID);
        descriptor.AddElementProperty("txtLatencyLimit2", txtLatencyLimit2.ClientID);
        descriptor.AddElementProperty("cbJitter", cbJitter.ClientID);
        descriptor.AddElementProperty("ddlJitter", ddlJitter.ClientID);
        descriptor.AddElementProperty("divJitter", divJitter.ClientID);
        descriptor.AddElementProperty("txtJitterLimit1", txtJitterLimit1.ClientID);
        descriptor.AddElementProperty("txtJitterLimit2", txtJitterLimit2.ClientID);
        descriptor.AddElementProperty("cbPacketLoss", cbPacketLoss.ClientID);
        descriptor.AddElementProperty("ddlPacketLoss", ddlPacketLoss.ClientID);
        descriptor.AddElementProperty("divPacketLoss", divPacketLoss.ClientID);
        descriptor.AddElementProperty("txtPacketLossLimit1", txtPacketLossLimit1.ClientID);
        descriptor.AddElementProperty("txtPacketLossLimit2", txtPacketLossLimit2.ClientID);

        descriptor.AddElementProperty("phMos", phMos.ClientID);
        descriptor.AddElementProperty("phLatency", phLatency.ClientID);
        descriptor.AddElementProperty("phCiscoCallTerminatationCause", phCiscoCallTerminatationCause.ClientID);
        descriptor.AddElementProperty("phCiscoRedirectReason", phCiscoRedirectReason.ClientID);
        descriptor.AddElementProperty("phAvayaCondCode", phAvayaCondCode.ClientID);

        descriptor.AddElementProperty("btnSubmit", submitButton.ClientID);
        descriptor.AddElementProperty("lblSearchCriteria", lblSearchCriteria.ClientID);

        descriptor.AddComponentProperty("resultsControl", ResultsControl.ClientID);
        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        yield return new ScriptReference(this.ResolveUrl("SearchVoipCalls.js"));
    }

    #endregion
}