<%@ Control Language="C#" ClassName="VoIPHelpButton" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<script runat="server">
	private string _helpUrlFragment;

	public string HelpUrlFragment
	{
		get { return _helpUrlFragment; }
		set { _helpUrlFragment = value; }
	}

    protected string HelpURL
    {
        get
        {
            return HelpHelper.GetHelpUrl(this.HelpUrlFragment); 
        }
    }
    
</script>



    <span><a href='<%= HelpURL %>'
        target='_blank' style='background-image: url(/Orion/Voip/images/Icon.Help.gif);
        background-position: center left; background-repeat: no-repeat; padding: 2px 0px 2px 20px;'>
        Help </a></span>


