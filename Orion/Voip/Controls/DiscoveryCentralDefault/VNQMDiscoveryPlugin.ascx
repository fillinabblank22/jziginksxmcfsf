﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VNQMDiscoveryPlugin.ascx.cs" Inherits="Orion_Voip_Controls_DiscoveryCentralDefault_VNQMDiscoveryPlugin" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<div class="coreDiscoveryIcon">
<asp:Image ID="IPSLAImage" runat="server" ImageUrl="~/Orion/Discovery/Controls/DiscoveryCentralDefault/images/IPSLA_discovery.gif"/>
</div>

<div class="coreDiscoveryPluginBody">
    <h2>
        <%= DiscoveryCentralCaption %></h2>
    <div>
        <%= DiscoveryCentralContent %>
    <span class="LinkArrow">&#0187;</span>
    <orion:HelpLink ID="VNQMDiscoveryHelpLink" runat="server" HelpUrlFragment="OrionIPSLAManagerAGAddingNodes"
        CssClass="helpLink" />
    <br />&nbsp;<br />
    </div>
    
    <orion:LocalizableButton ID="discoverVNQMButton" LocalizedText="CustomText" DisplayType="Secondary" runat="server" 
    OnClick="AddDeviceButton_Click" />
    <orion:LocalizableButton ID="discoverVNQMButton2" LocalizedText="CustomText" DisplayType="Secondary" runat="server" 
    OnClick="MonitorOperationsButton_Click" />
    <orion:LocalizableButton ID="discoverVNQMButton3" LocalizedText="CustomText" DisplayType="Secondary" runat="server" 
    OnClick="AddCallManager_Click" />
    <orion:LocalizableButton ID="discoverVNQMButton4" LocalizedText="CustomText" DisplayType="Secondary" runat="server" 
    OnClick="AddVoipGateway_Click" />
</div>
