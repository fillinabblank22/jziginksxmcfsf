﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Voip_Controls_DiscoveryCentralDefault_VNQMDiscoveryPlugin : System.Web.UI.UserControl
{
    private static Log _log;
    private static Log _logger { get { return _log ?? (_log = new Log()); } }

    protected readonly string DiscoveryCentralCaption = VNQMWebContent.VNQMWEBCODE_TM0_12;

    protected readonly string DiscoveryCentralContent = VNQMWebContent.VNQMWEBCODE_TM0_10;

    protected void Page_Load(object sender, EventArgs e)
    {
        VNQMDiscoveryHelpLink.HelpDescription = VNQMWebContent.VNQMWEBCODE_TM0_14;
        discoverVNQMButton.Text = VNQMWebContent.VNQMWEBCODE_TM0_11;
        discoverVNQMButton2.Text = VNQMWebContent.VNQMWEBCODE_TM0_12;
        discoverVNQMButton3.Text = VNQMWebContent.VNQMWEBCODE_TM0_13;
        discoverVNQMButton4.Text = VNQMWebContent.VNQMWEBCODE_PG_1;
        DataBind();
    }

    protected void AddDeviceButton_Click(object sender, EventArgs e)
    {
        DiscoveryCentralHelper.Leave();
        Response.Redirect("~/Orion/Voip/Admin/ManageNodes/Default.aspx");
    }

    protected void MonitorOperationsButton_Click(object sender, EventArgs e)
    {
        DiscoveryCentralHelper.Leave();
        Response.Redirect("~/Orion/Voip/Admin/OperationsWizard.aspx");
    }

    protected void AddCallManager_Click(object sender, EventArgs e)
    {
        DiscoveryCentralHelper.Leave();
        Response.Redirect("~/Orion/Voip/Admin/ManageCallManagers/AddCallManagerWizard.aspx");
    }

    protected void AddVoipGateway_Click(object sender, EventArgs e)
    {
        DiscoveryCentralHelper.Leave();
        Response.Redirect("~/Orion/Voip/Admin/ManageMediaGateway/AddMediaGatewayWizard.aspx");
    }
}