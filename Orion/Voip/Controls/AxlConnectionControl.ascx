<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AxlConnectionControl.ascx.cs"
    Inherits="Orion_Voip_Controls_Axl_AxlConnectionControl" %>
<%@ Register TagPrefix="ipsla" TagName="AxlTestResultControl" Src="~/Orion/Voip/Controls/AxlTestResultControl.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include ID="Include2" runat="server" Module="Voip" File="Voip.css" />

<asp:ScriptManagerProxy ID="scriptManager" runat="server">
    <Scripts>
        <%-- This is required to fix issues with nested script controls under Chrome and Safari.
        See: http://blog.lavablast.com/post/2008/10/20/Gotcha-WebKit-(Safari-3-and-Google-Chrome)-Bug-with-ASPNET-AJAX.aspx --%>
        <asp:ScriptReference Path="~/Orion/Voip/js/WebKit_fix.js" />
    </Scripts>
    <services>
        <asp:ServiceReference Path="~/Orion/Voip/Controls/AxlConnectionControl.asmx" />
    </services>
</asp:ScriptManagerProxy>

<script type="text/javascript">
    function VoipAxlValidateName_<%= this.ClientID %>(sender, args) {
        if ($('#<%= this.tbReturnCredentialsId.ClientID %>').val() == 0
            && $('#<%= this.tbUsername.ClientID %>').val() == '') {
            args.IsValid = false;
            return;
        }

        args.IsValid = true;
    }
</script>

<table class="voipAxlCredentialsTable">
    <tbody>
        <tr>
            <td class="leftLabelColumn">
                <%= SelectCredentialsLabel %>:
            </td>
            <td class="rightInputColumn">
                <asp:HiddenField ID="tbReturnCredentialsId" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="tbReturnCredentialsName" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="tbSavedCredentialsId" runat="server"></asp:HiddenField>
                <asp:TextBox ID="tbSavedCredentialsName" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="leftLabelColumn">
                <%= UsernameLabel %>:
            </td>
            <td class="rightInputColumn">
                <asp:TextBox ID="tbUsername" runat="server"></asp:TextBox>
                <asp:CustomValidator runat="server" ID="userNameValidator" OnServerValidate="userNameValidator_OnServerValidate" 
                ControlToValidate="tbUsername" Display="Dynamic" ValidationGroup="VoipAxlCredentials" ValidateEmptyText="True">
                    <%= RequiredFieldError %>
                </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="leftLabelColumn">
                <%= PasswordLabel %>:
            </td>
            <td class="rightInputColumn">
                <asp:TextBox ID="tbPassword" runat="server" TextMode="Password"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="leftLabelColumn">
                &nbsp;
            </td>
            <td class="rightInputColumn">
                <div>
                    <orion:LocalizableButton ID="btnValidate" runat="server" DisplayType="Small" LocalizedText="Test" CausesValidation="true" ValidationGroup="VoipAxlCredentials"/>
                    <ipsla:AxlTestResultControl ID="testResult" runat="server" />
                </div>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <div ID="validationProgress" runat="server" style="display: none;">
                    <img src="/Orion/images/animated_loading_sm3_whbg.gif" style="vertical-align: middle;" />
                    <span><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_81%></span>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<%-- Render the DIV control only in demo mode --%>
<%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
		<div id="isDemoModeAxl" style="display:none;"></div>
<%}%>