﻿/// <reference name="MicrosoftAjax.js"/>
/// <reference path="~/Orion/js/jquery.js" />
/// <reference path="~/Orion/js/extjs/debug/Ext.js" />

Type.registerNamespace("Orion.Voip.Controls");

Orion.Voip.Controls.CliSettingsControl = function(element) {
    Orion.Voip.Controls.CliSettingsControl.initializeBase(this, [element]);
    this.ddlSavedCredentials = null;
    this.tbUsername = null;
    this.tbPassword = null;
    this.tbEnablePassword = null;
    this.ddlEnableLevel = null;
    this.ddlProtocol = null;
    this.tbPort = null;
    this.tbTimeout = null;
    this.testResult = null;
    this.btnValidate = null;
    this.validationProgress = null;
    this.validationIp = null;
    this.preselectedCredentialsId = null;
    this.nodeIds = null;
    
    this.credentialsStore = null;
    this.extCredentialsCombo = null;
    this.extUsername = null;
    this.extPassword = null;
    this.extEnablePassword = null;
    this.enableLevelData = null;
    this.enableLevelStore = null;
    this.extEnableLevelCombo = null;
    this.protocolData = null;
    this.protocolStore = null;
    this.extProtocol = null;
    this.extPort = null;
    this.extTimeout = null;

    this.extEnableLevelCombo_valid = null;
    this.extPort_valid = null;
    this.extTimeout_valid = null;

    this.hfReturnCredentialsId = null;
    this.hfReturnEnableLevelValue = null;
    this.hfReturnProtocolValue = null;
    this.isFipsEnabledOnAnyEngine = false;
};

Orion.Voip.Controls.CliSettingsControl.prototype = {
    //properties
    get_IsFipsEnabledOnAnyEngine: function () { return this.isFipsEnabledOnAnyEngine; },
    set_IsFipsEnabledOnAnyEngine: function (value) {
        this.isFipsEnabledOnAnyEngine = value;
    },

    get_ValidationIp: function() { return this.validationIp; },
    set_ValidationIp: function(value) {
        this.validationIp = value;
        if (this.btnValidate) {
            this.btnValidate.style.display = (value != null ? 'inline-block' : 'none');
        }
        if (this.testResult) {
            this.testResult.set_Result(null);
        }
    },

    get_ValidationEngineId: function() { return this.validationEngineId; },
    set_ValidationEngineId: function(value) { this.validationEngineId = value; },

    get_PreselectedCredentialsId: function() { return this.preselectedCredentialsId; },
    set_PreselectedCredentialsId: function(value) { this.preselectedCredentialsId = value; },

    get_NodeIds: function() { return this.nodeIds; },
    set_NodeIds: function(nodeIds) { this.nodeIds = nodeIds; },

    get_Protocol: function() {
        return this.extProtocol.getValue();
    },
    set_Protocol: function(value) {
        this.extProtocol.setValue(value);
        $('#' + this.hfReturnProtocolValue.id).val(value);
    },

    get_Port: function() {
        var num = this.extPort.getValue();
        return (num == null || num == '' || isNaN(num)
            ? null
            : eval(num));
    },
    set_Port: function(value) {
        this.extPort.setValue(value);
    },

    get_Timeout: function() {
        var num = this.extTimeout.getValue();
        return (num == null || num == '' || isNaN(num)
            ? null
            : eval(num));
    },
    set_Timeout: function(value) {
        this.extTimeout.setValue(value);
    },

    get_CredentialId: function() {
        return this.selectedCredentialsId;
    },
    set_CredentialId: function(value) {
        this.extCredentialsCombo.setValue();

        var loadCallbackScope = { credentialId: value, sender: this };
        var loadOptions = {
            callback: function(r, options, success) {
                var credentialId = this.credentialId;
                var storeIndex = this.sender.credentialsStore.findBy(function(record, id) { return (record.data.id == credentialId); });
                this.sender.RefreshCredentialsContents(storeIndex);
            },
            scope: loadCallbackScope
        };

        this.credentialsStore.load(loadOptions);
    },

    get_CredentialName: function() { return this.extCredentialsCombo.getValue(); },
    get_Username: function() { return this.extUsername.getValue(); },
    get_Password: function() { return this.extPassword.getValue(); },
    get_EnablePassword: function() { return this.extEnablePassword.getValue(); },

    get_EnableLevel: function() {
        var name = this.extEnableLevelCombo.getValue();
        var index = this.enableLevelStore.find('name', name);
        if (index == -1) {
            return null;
        }
        var record = this.enableLevelStore.getAt(index);
        if (record == null) {
            return null;
        }
        return record.data.value;
    },
    set_EnableLevel: function(value) {
        var index = this.enableLevelStore.find('value', value);
        if (index == -1) {
            return;
        }
        var record = this.enableLevelStore.getAt(index);
        if (record == null) {
            return;
        }
        this.extEnableLevelCombo.setValue(record.data.name);
    },

    get_ddlSavedCredentials: function() { return this.ddlSavedCredentials; },
    set_ddlSavedCredentials: function(value) { this.ddlSavedCredentials = value; },

    get_tbUsername: function() { return this.tbUsername; },
    set_tbUsername: function(value) { this.tbUsername = value; },

    get_tbPassword: function() { return this.tbPassword; },
    set_tbPassword: function(value) { this.tbPassword = value; },

    get_tbEnablePassword: function() { return this.tbEnablePassword; },
    set_tbEnablePassword: function(value) { this.tbEnablePassword = value; },

    get_ddlEnableLevel: function() { return this.ddlEnableLevel; },
    set_ddlEnableLevel: function(value) { this.ddlEnableLevel = value; },

    get_ddlProtocol: function() { return this.ddlProtocol; },
    set_ddlProtocol: function(value) { this.ddlProtocol = value; },

    get_rbProtocolSsh: function() { return this.rbProtocolSsh; },
    set_rbProtocolSsh: function(value) { this.rbProtocolSsh = value; },

    get_tbPort: function() { return this.tbPort; },
    set_tbPort: function(value) { this.tbPort = value; },

    get_tbTimeout: function() { return this.tbTimeout; },
    set_tbTimeout: function(value) { this.tbTimeout = value; },

    get_testResult: function() { return this.testResult; },
    set_testResult: function(value) {
        this.testResult = value;
    },

    get_btnValidate: function() { return this.btnValidate; },
    set_btnValidate: function(value) { this.btnValidate = value; },

    get_validationProgress: function() { return this.validationProgress; },
    set_validationProgress: function(value) { this.validationProgress = value; },

    get_hfReturnCredentialsId: function() { return this.hfReturnCredentialsId; },
    set_hfReturnCredentialsId: function(value) { this.hfReturnCredentialsId = value; },

    get_hfReturnEnableLevelValue: function() { return this.hfReturnEnableLevelValue; },
    set_hfReturnEnableLevelValue: function(value) { this.hfReturnEnableLevelValue = value; },

    get_hfReturnProtocolValue: function() { return this.hfReturnProtocolValue; },
    set_hfReturnProtocolValue: function(value) { this.hfReturnProtocolValue = value; },
    
    //methods
    initialize: function() {
        Orion.Voip.Controls.CliSettingsControl.callBaseMethod(this, 'initialize');
        this.internalControlInit();

        if (this.preselectedCredentialsId) {
            this.set_CredentialId(this.preselectedCredentialsId);
        }

        if (this.nodeIds) {
            this.PreFillNodeCredentials(this.nodeIds);
        }

        this.RefreshCredentialsContents(null);

        this.set_ValidationIp(this.validationIp);

    },

    dispose: function() {
        Orion.Voip.Controls.CliSettingsControl.callBaseMethod(this, 'dispose');
    },

    PreFillNodeCredentials: function(nodeIds) {
        this.testResult.set_Result(null);
        Orion.Voip.Controls.CliConnectionControlService.GetCommonCliCredentials(
            nodeIds,
            Function.createDelegate(this, function(result, context) {
                this.set_CredentialId(result.CredentialId);
                if (this.isFipsEnabledOnAnyEngine) {
                    this.set_Protocol(result.Protocol ? result.Protocol : 'Telnet');
                } else {
                    this.set_Protocol(result.Protocol ? result.Protocol : 'SshAuto');
                }
                this.set_Port(result.Port ? result.Port : 22);
                this.set_Timeout(result.Timeout ? result.Timeout : 5000);
            }),
            function(error, context) {
                // ignore
            },
            null
        );
    },

    ShowValidationMask: function (btnValidateElement) {
        var isAddCallManagerWizardExist = $('#AddCallManagerWizard').length > 0;

        if (isAddCallManagerWizardExist) {
            //disable test,next buttons.
            var btnNextElement, btnBackElement, btnCancelElement;
            btnNextElement = $('#'+GetNextButtonClientID());
            btnBackElement = $('#'+GetBackButtonClientID());
            btnCancelElement = $('#'+GetCancelButtonClientID());

            var disableLinks = new Array(btnValidateElement, btnNextElement, btnBackElement, btnCancelElement);

            for (var i = 0; i < disableLinks.length; i++) {
                var link = $(disableLinks[i]);
                // get the 'href' and 'onclick' attribute values
                var linkHRef = link.attr('href');
                var linkOnClick = link.attr('onclick');

                // set the dummy attributes values
                link.attr('href', '#').attr('onclick', 'return false;');

                // add a temporary attribute to keep the original 'href' value
                if (linkHRef != undefined)
                    link.attr('tmphref', linkHRef);

                // add a temporary attribute to keep the original 'onclick' value
                if (linkOnClick != undefined)
                    link.attr('tmponclick', linkOnClick);

                // add the class disabled links
                link.addClass('sw-btn-disabled');
            }
        }

        this.validationProgress.style.display = 'block';

        if (Ext.isIE6) {
            jQuery('select', this.get_element()).css('visibility', 'hidden');
        }
    },

    HideValidationMask: function (btnValidateElement) {
        this.validationProgress.style.filter = '';
        this.validationProgress.style.display = 'none';

        if (Ext.isIE6) {
            jQuery('select', this.get_element()).css('visibility', 'visible');
        }

        var isAddCallManagerWizardExist = $('#AddCallManagerWizard').length > 0;

        if (isAddCallManagerWizardExist) {
            //enable test,next buttons
            var btnNextElement, btnBackElement, btnCancelElement;

            btnNextElement = $('#'+GetNextButtonClientID());
            btnBackElement = $('#'+GetBackButtonClientID());
            btnCancelElement = $('#'+GetCancelButtonClientID());

            var enableLinks = new Array(btnValidateElement, btnNextElement, btnBackElement, btnCancelElement);

            for (var i = 0; i < enableLinks.length; i++) {
                var link = $(enableLinks[i]);
                // get the original values of 'href' and 'onclick' attributes from temporary attributes
                var originalLinkHRef = link.attr('tmphref');
                var originalLinkOnClick = link.attr('tmponclick');

                // restore the original 'href' value
                if (originalLinkHRef != undefined)
                    link.attr('href', originalLinkHRef);

                // restore the original 'onclick' value
                if (originalLinkOnClick != undefined)
                    link.attr('onclick', originalLinkOnClick);

                // remove temporary attributes
                link.removeAttr('tmphref').removeAttr('tmponclick');

                // remove the class disabled links
                link.removeClass('sw-btn-disabled');
            }
        }
    },

    RefreshCredentialsContents: function(storeIndex) {
        var disableConnEdit = true;
        var disableCredEdit = true;
        var name = this.extCredentialsCombo.getValue();
        var id = 0;
        var record = null;
        if (storeIndex == null) {
            storeIndex = this.credentialsStore.findBy(function(record, id) { return (record.data.name == name); });
            storeIndex = storeIndex >= 0 ? storeIndex : null;
        }
        if (storeIndex != null) {
            record = this.credentialsStore.getAt(storeIndex);
            name = record != null ? record.data.name : '';
            id = record != null ? record.data.id : '';
            this.extCredentialsCombo.setValue(name);
        }
        if (record == null) {
            disableCredEdit = name == '';
            disableConnEdit = disableCredEdit;
        } else {
            disableConnEdit = false;
            disableCredEdit = true;
        }

        $('#' + this.hfReturnCredentialsId.id).val(id);

        this.selectedCredentialsId = record ? record.data.id : null;
        this.extUsername.setDisabled(disableCredEdit);
        this.extUsername.setValue(record ? record.data.user : '');
        this.extPassword.setDisabled(disableCredEdit);
        this.extPassword.setValue(record ? '********' : '');
        this.set_EnableLevel(record ? record.data.level : -2);
        this.extEnablePassword.setDisabled(disableCredEdit || (this.get_EnableLevel() == -2));
        this.extEnablePassword.setValue(record && record.data.level != -2 ? '********' : '');
        this.extEnableLevelCombo.setDisabled(disableCredEdit);
        this.extProtocol.setDisabled(disableConnEdit);
        this.extPort.setDisabled(disableConnEdit);
        this.extTimeout.setDisabled(disableConnEdit);
        this.btnValidate.disabled = disableConnEdit;
        //this.btnValidate.setDisabled(disableConnEdit);
        this.testResult.set_Result(null);
    },

    internalControlInit: function() {
        Ext.QuickTips.init();
        this.enableLevelData = [
            [-2, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_19;E=js}'],
            [-1, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_20;E=js}'],
            [0, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_21;E=js}'],
            [1, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_22;E=js}'],
            [2, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_23;E=js}'],
            [3, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_24;E=js}'],
            [4, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_25;E=js}'],
            [5, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_26;E=js}'],
            [6, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_27;E=js}'],
            [7, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_28;E=js}'],
            [8, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_29;E=js}'],
            [9, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_30;E=js}'],
            [10, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_31;E=js}'],
            [11, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_32;E=js}'],
            [12, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_33;E=js}'],
            [13, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_34;E=js}'],
            [14, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_35;E=js}'],
            [15, '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_36;E=js}']];
        this.enableLevelStore = new Ext.data.SimpleStore({
            fields: ['value', 'name'],
            data: this.enableLevelData
        });

        if (this.isFipsEnabledOnAnyEngine) {
            this.protocolData = [
            ['Telnet', '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_37;E=js}'],
            ['Ssh2', '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_40;E=js}']];
        } else {
            this.protocolData = [
            ['Telnet', '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_37;E=js}'],
            ['SshAuto', '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_38;E=js}'],
            ['Ssh1', '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_39;E=js}'],
            ['Ssh2', '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_40;E=js}']];
        };

        this.protocolStore = new Ext.data.SimpleStore({
            fields: ['value', 'name'],
            data: this.protocolData
        });

        this.credentialsStore = new Ext.data.Store({
            autoLoad: true,
            proxy: new Ext.data.HttpProxy({
                url: '/Orion/Voip/Controls/Cli/CliConnectionControl.asmx/GetCredentials',
                method: 'POST',
                headers: { 'Content-type': 'application/json' }
            }),
            reader: new Ext.data.JsonReader({
                root: 'd',
                id: 'id'
            }, [
                    { name: 'id', type: 'int' },
                    { name: 'name', type: 'string' },
                    { name: 'user', type: 'string' },
                    { name: 'level' }
            ]),
            sortInfo: { field: 'name', direction: "ASC" }
        });
        this.extCredentialsCombo = new Ext.form.ComboBox({
            applyTo: this.ddlSavedCredentials,
            store: this.credentialsStore,
            displayField: 'name',
            mode: 'local',
            triggerAction: 'all',
            emptyText: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_41;E=js}',
            // width: 200,
            autoWidth: true,
            lazyRender: true,
            autoSelect: false,
            typeAhead: true,
            enableKeyEvents: true,
            listClass: 'x-menu x-menu-neutralize',
            listeners: {
                blur: Function.createDelegate(this, function(field, newValue, oldValue) {
                    this.RefreshCredentialsContents(null);
                }),
                keypress: Function.createDelegate(this, function(field, eventObject) {
                    var delegate = Function.createDelegate(this, function() { this.RefreshCredentialsContents(null); });
                    setTimeout(function() { delegate(); }, 0);
                }),
                select: Function.createDelegate(this, function(combo, record, index) {
                    this.RefreshCredentialsContents(index);
                })
            }
        });
        this.extUsername = new Ext.form.TextField({
            applyTo: this.tbUsername,
            disabledCls: 'x-item-disabled extDisabled',
            width: 200
        });
        this.extPassword = new Ext.form.TextField({
            applyTo: this.tbPassword,
            disabledCls: 'x-item-disabled extDisabled',
            inputType: 'password',
            width: 200
        });
        this.extEnablePassword = new Ext.form.TextField({
            applyTo: this.tbEnablePassword,
            disabledCls: 'x-item-disabled extDisabled',
            inputType: 'password',
            width: 200
        });

        this.extEnableLevelCombo = new Ext.form.ComboBox({
            applyTo: this.ddlEnableLevel,
            store: this.enableLevelStore,
            displayField: 'name',
            mode: 'local',
            triggerAction: 'all',
            forceSelection: true,
            // width: 200,
            autoWidth: true,
            lazyRender: false,
            autoSelect: false,
            editable: false,
            typeAhead: true,
            listClass: 'x-menu x-menu-neutralize',
            listeners: {
                change: Function.createDelegate(this, function(field, newValue, oldValue) {
                    this.extEnablePassword.setDisabled(this.get_EnableLevel() == -2);
                }),
                select: Function.createDelegate(this, function(combo, record, index) {
                    var level = this.get_EnableLevel();
                    this.extEnablePassword.setDisabled(level == -2);
                    $('#' + this.hfReturnEnableLevelValue.id).val(level);
                })
            }
        });
        this.extProtocol = new Ext.form.ComboBox({
            applyTo: this.ddlProtocol,
            store: this.protocolStore,
            displayField: 'name',
            valueField: 'value',
            mode: 'local',
            triggerAction: 'all',
            forceSelection: true,
            width: 200,
            autoWidth: true,
            lazyRender: false,
            autoSelect: false,
            editable: false,
            typeAhead: true,
            listClass: 'x-menu x-menu-neutralize',
            listeners: {
                change: Function.createDelegate(this, function(field, newValue, oldValue) {
                    this.tbPort.value = newValue == 'Telnet' ? '23' : '22';
                }),
                select: Function.createDelegate(this, function(combo, record, index) {

                    var protocol = this.get_Protocol();
                    this.tbPort.value = protocol == 'Telnet' ? '23' : '22';
                    $('#' + this.hfReturnProtocolValue.id).val(protocol);
                })
            }
        });


        this.extPort = new Ext.form.NumberField({
            applyTo: this.tbPort,
            allowDecimals: false,
            allowNegative: false,
            allowBlank: false,
            blankText: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_42;E=js}',
            minValue: 1,
            minText: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_43;E=js}',
            maxValue: 65535,
            maxText: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_44;E=js}',
            width: 50,
            listeners: {
                invalid: Function.createDelegate(this, function(sender, msg) {
                    this.extPort_valid = msg;
                }),
                valid: Function.createDelegate(this, function(sender) {
                    this.extPort_valid = null;
                })
            }
        });
        this.extTimeout = new Ext.form.NumberField({
            applyTo: this.tbTimeout,
            allowDecimals: false,
            allowNegative: false,
            allowBlank: false,
            blankText: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_45;E=js}',
            minValue: 0,
            minText: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_46;E=js}',
            width: 50,
            listeners: {
                invalid: Function.createDelegate(this, function(sender, msg) {
                    this.extTimeout_valid = msg;
                }),
                valid: Function.createDelegate(this, function(sender) {
                    this.extTimeout_valid = null;
                })
            }
        });


    },
    btnAdvanced_Expanded: function () {
        if (this.extProtocol != null)
            this.extProtocol.setWidth(1000);

    },

    btnValidate_Click: function (btnValidateElement) {
        if (CheckDemoMode()) return false;

        var credentialId = this.get_CredentialId();
        var credentialName = this.get_CredentialName();
        this.ShowValidationMask(btnValidateElement);
        if (credentialId != null || (credentialName != null && credentialName.length > 0)) {
            var userName = this.get_Username().trim();
            var password = this.get_Password().trim();
            if (userName.length > 0 && password.length > 0) {
                var successHandler = Function.createDelegate(this, function (result, context) {
                    this.HideValidationMask(btnValidateElement);
                    this.testResult.set_Result(result);
                });
                var errorHandler = Function.createDelegate(this, function (error, context) {
                    this.HideValidationMask(btnValidateElement);
                    Ext.Msg.alert(String.format("@{R=VNQM.Strings;K=VNQMWEBJS_VB1_15;E=js}", +error.get_message()));
                });
               
                if (credentialId != null) {
                    Orion.Voip.Controls.CliConnectionControlService.ValidateCliConnectionCredentialId(
                        this.get_ValidationEngineId(), this.get_ValidationIp(),
                        credentialId,
                        this.get_Protocol(), this.get_Port(), this.get_Timeout(), true,
                        successHandler,
                        errorHandler,
                        null
                    );
                } else {
                    Orion.Voip.Controls.CliConnectionControlService.ValidateCliConnectionCredentials(
                        this.get_ValidationEngineId(), this.get_ValidationIp(),
                        this.get_Username(), this.get_Password(), this.get_EnablePassword(), this.get_EnableLevel(),
                        this.get_Protocol(), this.get_Port(), this.get_Timeout(), true,
                        successHandler,
                        errorHandler,
                        null
                    );
                }
            }
            else {
                if (userName.length == 0 && password.length == 0) {
                    Ext.Msg.alert('@{R=VNQM.Strings;K=VNQMWEBJS_PG_15;E=js}');
                } else if (userName.length == 0) {
                    Ext.Msg.alert('@{R=VNQM.Strings;K=VNQMWEBJS_PG_13;E=js}');
                }
                else {
                    Ext.Msg.alert('@{R=VNQM.Strings;K=VNQMWEBJS_PG_14;E=js}');
                }
            }
        }
    }
};

Orion.Voip.Controls.CliSettingsControl.registerClass('Orion.Voip.Controls.CliSettingsControl', Sys.UI.Control);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();