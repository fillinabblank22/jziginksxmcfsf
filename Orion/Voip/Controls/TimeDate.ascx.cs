using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Orion_Voip_Resources_TimeDate : System.Web.UI.UserControl
{
   public string DateTimeValue
        {
            get
            {
                 //return string.Format("{0} {1}",InputDate.Text, InputTime.Text);
                return InputDate.Text;
            }
            set
            {
                InputDate.Text = value;
            }
        }
    [PersistenceMode(PersistenceMode.Attribute)]
    public string InputDateId
    {
        get { return InputDate.ClientID; }
    }

    /*[PersistenceMode(PersistenceMode.Attribute)]
    public string InputTimeId
    {
        get { return InputTime.ClientID; }
    }*/

 
}
