﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SummaryWebWizardControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_Operations_SummaryWebWizardControl" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web.Wizards.Operations" %>
<%@ Register src="~/Orion/Voip/Admin/ProgressControl.ascx" tagname="ProgressControl" tagprefix="uc2" %>

<asp:ScriptManagerProxy id="smp" runat="server">
    <Services>
        <asp:ServiceReference Path="~/Orion/Voip/Controls/Wizards/Operations/SummaryService.asmx" />
    </Services>
    <Scripts>
        <asp:ScriptReference Path="~/Orion/Voip/Controls/Wizards/Operations/SummaryExtGrid.js" /> 
    </Scripts> 
</asp:ScriptManagerProxy>

<script type="text/javascript">
    var CancelProgressBox = function () {
        $find('<%= this.ClientID %>').AskCancel(); return false;
    }
</script>

<style type="text/css">
    .GroupBox .ipsla_ProgressBox{width: 450px !important}
</style>

<div class="ipsla_WP_Title">
<%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_77 %>
</div>

<div id="progressArea" runat="server">
  <div class="ipsla_WP_Description">
    <img src="/orion/images/animated_loading_sm3_whbg.gif" alt="Creating operations" class="waitingImg" />
    <span id="totalAreaHead" runat="server"/>
    <span id="totalAreaHeadExist" runat="server" style="position: absolute; left: 20px;"/>
  </div>
  <div class="GroupBox ipsla_ProgressBox">
      <uc2:ProgressControl ID="ProgressControl" runat="server" CssClass="" ShowAnimatedGif="false"/>
      <div class="ipsla_ProgressButtons">
          <orion:LocalizableButton runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClientClick="CancelProgressBox(); return false"/>
      </div>
  </div>
</div>

<div id="resultsArea" runat="server" style="display:none;">

  <div class="ipsla_WP_Description">
      <span id="createdAreaResults" runat="server"/>
      <span id="createdAreaResultsExist" runat="server" style="position: absolute; left: 20px;"/>
  </div>

  <div id="gridAreaResults" runat="server" class="ExtJsGrid"/>
</div>