﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExternalNodesOption.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_Operations_ExternalNodesOption" %>

<style type="text/css">
    .radiobutton label{
        padding-left:5px;
    }
</style>

<%@ Register src="ExternalNodesList.ascx" tagname="ExternalNodesList" tagprefix="uc1" %>
<asp:UpdatePanel ID="up" runat="server" UpdateMode="Always">
    <ContentTemplate>
        <div class="externalNodesOption">
            <div class="ipsla_WP_SubTitle alternateRow">
                &nbsp;<%= SingleNode ? Resources.VNQMWebContent.VNQMWEBDATA_VB1_51 : Resources.VNQMWebContent.VNQMWEBDATA_VB1_52%>
            </div>
            <div>
                <asp:RadioButton ID="rbtnYes" runat="server" 
                    AutoPostBack="true" 
                    GroupName="ExternalNodesOption" 
                    oncheckedchanged="rbtn_CheckedChanged" CssClass="radiobutton"/>
                <uc1:ExternalNodesList ID="externalNodeList" runat="server" />
            </div>
            <div>
                <asp:RadioButton ID="rbtnNo" runat="server" 
                    AutoPostBack="true" 
                    GroupName="ExternalNodesOption" 
                    oncheckedchanged="rbtn_CheckedChanged" CssClass="radiobutton"/>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>