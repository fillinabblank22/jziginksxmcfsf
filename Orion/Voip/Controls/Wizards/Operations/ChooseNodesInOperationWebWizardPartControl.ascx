﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChooseNodesInOperationWebWizardPartControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_Operations_ChooseNodesInOperationWebWizardPartControl" %>
<%@ Register src="ChooseNodesTreeControl.ascx" tagname="ChooseNodesTreeControl" tagprefix="ipsla" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web.Wizards.Operations" %>
<%@ Register src="ExternalNodesOption.ascx" tagname="ExternalNodesOption" tagprefix="ipsla" %>
<%@ Register src="../../../Controls/NodeGroupingSelector.ascx" tagname="NodeGroupingSelector" tagprefix="ipsla" %>
<%@ Register src="~/Orion/Voip/Controls/Wizards/Operations/CredentialsCheckControl.ascx" tagname="CredentialsCheckControl" tagprefix="ipsla" %>

<div class="ipsla_WP_Title">
  <%= Title %>
</div>
<div class="ipsla_WP_Description">
  <%= Description %>
</div>
<div class="ipsla_WP_Description">
    <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_28, "<a href=\"ManageNodes/AddNodes.aspx\">", "</a>")%>
</div>

<div class="TreeGroupBy">
<div style="padding-bottom: 5px"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_29%></div>
<ipsla:NodeGroupingSelector ID="groupBySelector" runat="server" OnSelectedValueChanged="groupBySelector_SelectedValueChanged" IncludeSiteProperties="true"/>
</div>

<div class="TreeContent">
    <ipsla:ChooseNodesTreeControl ID="ChooseNodesTreeControl" runat="server">
        <WebService Path="~/Orion/Voip/Controls/Wizards/Operations/ChooseNodesAjaxTreeService.asmx" />
    </ipsla:ChooseNodesTreeControl>
</div>

<div id="operationNexusArea" runat="server" class="ipsla_WP_Description">
    <table border="0" cellpadding="0" cellspacing="5px" style="background-color: #FFF7CD;">
        <tr>
            <td valign="middle" style="text-align: center; width: 32px;">
                <img src="/orion/voip/images/lightbulb_important_text.gif" alt="<%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_69 %>" />
            </td>
            <td valign="middle">
                <div>
                    <%= String.Format( Resources.VNQMWebContent.VNQMWEBDATA_VB1_221, String.Format("<a href={0} >{1}</a>", Resources.VNQMWebContent.VNQMWEBDATA_VB1_222, Resources.VNQMWebContent.VNQMWEBCODE_VB1_136))%>
                </div>
            </td>
        </tr>
    </table>
</div>


<ipsla:ExternalNodesOption ID="externalNodesOption" runat="server" />

<ipsla:CredentialsCheckControl ID="CredentialsCheckControl" runat="server" />
