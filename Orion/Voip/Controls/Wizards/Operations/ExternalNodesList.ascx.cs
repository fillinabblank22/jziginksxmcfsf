﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_Controls_Wizards_Operations_ExternalNodesList : System.Web.UI.UserControl
{
    protected const string REGEX_HOST_NAME_OR_IP_ADDRESS = @"^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)*([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?)$";

    public bool SingleNode { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.btnAddAnother.Visible = !SingleNode;

        if (!this.IsPostBack)
        {
            this.BindData();
        }
    }

    protected void gwExternalNodes_OnDataBound(object sender, EventArgs e)
    {
        this.gwExternalNodes.Columns[1].Visible = (gwExternalNodes.Rows.Count > 1);
    }

    private void BindData()
    {
        var list = new List<ExternalIpSlaNode>(OperationsWizardSession.Current.Data.ExternalTargetNodeList);
        if (list.Count == 0)
        {
            list.Add(new ExternalIpSlaNode() { IpAddressOrHostName = "" });
        }
        else if (SingleNode)
        {
            list.RemoveRange(1, list.Count - 1);
        }

        this.gwExternalNodes.DataSource = list;
        this.gwExternalNodes.DataBind();
    }

    public void SaveValues()
    {
        var list = OperationsWizardSession.Current.Data.ExternalTargetNodeList;

        var n = gwExternalNodes.Rows.Count;
        if (SingleNode && n > 1)
            n = 1;

        for (var i = 0; i < n; i++)
        {
            var item = this.gwExternalNodes.Rows[i];
            var txbExternalNode = (TextBox)item.FindControl("txbExternalNode");
            if (i < list.Count)
            {
                list[i].IpAddressOrHostName = txbExternalNode.Text;
            }
            else
            {
                list.Add(new ExternalIpSlaNode { IpAddressOrHostName = txbExternalNode.Text });
            }
        }

        while (list.Count > n)
        {
            list.RemoveAt(n);
        }
    }

    private bool ValidateDuplicity(int rowIndex)
    {
        var externalNodeList = OperationsWizardSession.Current.Data.ExternalTargetNodeList;
        var validatedExternalNode = externalNodeList[rowIndex];
        foreach (var externalNode in externalNodeList)
        {
            if (externalNode == validatedExternalNode)
            {
                continue;
            }
            if (externalNode.IpAddressOrHostName.Trim().ToLower() == validatedExternalNode.IpAddressOrHostName.Trim().ToLower())
            {
                return false;
            }
        }
        return true;
    }

    protected void gwExternalNodes_RowDelete(object sender, CommandEventArgs e)
    {
        this.SaveValues();
        int index = Convert.ToInt32(e.CommandArgument);
        if (index < gwExternalNodes.Rows.Count)
        {
            OperationsWizardSession.Current.Data.ExternalTargetNodeList.RemoveAt(index);
        }
        this.BindData();
    }

    /// <summary> </summary>
    protected void btnAddAnother_Clicked(object sender, EventArgs e)
    {
        this.SaveValues();
        OperationsWizardSession.Current.Data.ExternalTargetNodeList.Add(new ExternalIpSlaNode() { IpAddressOrHostName = "" });
        this.BindData();
    }

    /// <summary> </summary>
    protected void vldCustDuplicity_ServerValidate(object source, ServerValidateEventArgs args)
    {
        CustomValidator cv = (CustomValidator)source;
        int rowIndex = ((GridViewRow)cv.NamingContainer).RowIndex;
        args.IsValid = ValidateDuplicity(rowIndex);
        cv.Visible = true;
    }

    protected void vldCustEmpty_ServerValidate(object source, ServerValidateEventArgs args)
    {
        CustomValidator cv = (CustomValidator)source;
        args.IsValid = OperationsWizardSession.Current.Data.ExternalTargetNodeList.Count > 0;
    }
}
