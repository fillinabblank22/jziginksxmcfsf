﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeDiscoveryWebWizardPartControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_Operations_NodeDiscoveryWebWizardPartControl" %>
<%@ Register src="~/Orion/Voip/Admin/ManageNodes/AddNodesProcessControl.ascx" tagname="AddNodesProcessControl" tagprefix="ipsla" %>

<div class="ipsla_WP_Title">
  <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_115 %>
</div>
<div class="ipsla_WP_Description">
  <img src="/Orion/Voip/images/ipsla_discovery_32x32.gif" alt="Discovery" style="float: left; padding-right: 10px;" />
  <div>
    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_116 %>
  </div>
  <div>
      <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_117, String.Format("<a href=\"/Orion/Nodes/Add/Default.aspx\" title=\"{0}\">", Resources.VNQMWebContent.VNQMWEBDATA_VB1_122), "</a>", String.Format("<a href=\"/Orion/Discovery/Default.aspx?origUrl=Nodes\" title=\"{0}\">", Resources.VNQMWebContent.VNQMWEBDATA_VB1_123))%>
  </div>
</div>
<div class="ipsla_WP_Description">
  <div style="background-color: #E4f1f8; font-weight: bold; padding: 10px 10px 10px 10px;">
    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_118 %>
  </div>
</div>

<ipsla:AddNodesProcessControl ID="AddNodesProcessControl" runat="server"
 ShowWriteCredentialsErrors="false" ShowResults="false" UsageContext="StartupWizard" />
