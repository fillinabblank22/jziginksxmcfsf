﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Voip.Controls.Wizards.Operations");

Orion.Voip.Controls.Wizards.Operations.SelectTypeWebWizardPartControl = function(element) {
    Orion.Voip.Controls.Wizards.Operations.SelectTypeWebWizardPartControl.initializeBase(this, [element]);
    this.val = null;
}

Orion.Voip.Controls.Wizards.Operations.SelectTypeWebWizardPartControl.prototype = {

    get_Value: function() {
        return this.val;
    },
    set_Value: function(value) {
        this.val = value;
    },

    cvSelection_Validate: function(sender, args) {
        if (this.val == null) {
            Ext.MessageBox.show({
                title: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}",
                icon: Ext.MessageBox.ERROR,
                msg: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_3;E=js}",
                closable: false,
                buttons: Ext.MessageBox.OK
            });
            args.IsValid = false;
        }
    },

    initialize: function() {
        Orion.Voip.Controls.Wizards.Operations.SelectTypeWebWizardPartControl.callBaseMethod(this, 'initialize');
        // Add custom initialization here
    },
    dispose: function() {
        // Add custom dispose actions here
        Orion.Voip.Controls.Wizards.Operations.SelectTypeWebWizardPartControl.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Controls.Wizards.Operations.SelectTypeWebWizardPartControl.registerClass('Orion.Voip.Controls.Wizards.Operations.SelectTypeWebWizardPartControl', Orion.Voip.Controls.Wizards.WizardFlowPartControl);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
