﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Web.Wizards;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;
using System.Diagnostics;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_Controls_Wizards_Operations_AssignOperationsWebWizardPartControl : WizardFlowPartControl<OperationsWizardData>
{
    private AssignOperationsWebWizardPart m_part;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ChooseNodesTreeControl.SelectedNodesListIdentifier = AssignOperationsWebWizardPart.CHOOSED_NODES_LIST_KEY;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        m_part = (AssignOperationsWebWizardPart)OperationsWizardSession.Current.CurrentPart;
        m_part.TooManyNodesSelected += this.part_TooManyNodesSelected;
        m_part.TooFewNodesSelected += this.part_TooFewNodesSelected;
        m_part.LicenseCountExceeded += this.part_LicenseCountExceeded;

    }

    public override bool Initialize(OperationsWizardData data)
    {
        RefreshTree();
        return true;
    }

    public override bool ModifyData(OperationsWizardData data)
    {
        return true;
    }

    void part_LicenseCountExceeded(object sender, MessageEventArgs args)
    {
        string message = args.Message;
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "LicenseCountExceeded",
            "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').OnLicenseCountExceeded('" + message + "');},0);});",
            true);
        RefreshTree();
    }

    void part_TooManyNodesSelected(object sender, MessageEventArgs args)
    {
        string message = args.Message;
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "TooManyNodesSelected",
            "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').OnTooManyNodesSelected('" + message + "');},0);});",
            true);
        RefreshTree();
    }

    void part_TooFewNodesSelected(object sender, EventArgs e)
    {
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "TooFewNodesSelected",
            "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').OnTooFewNodesSelected();},0);});",
            true);
        RefreshTree();
    }


    public void RefreshTree()
    {
        ChooseNodesTreeControl.RefreshTree("", new AssignOperationsAjaxTreeService());
    }

    protected override void OnUnload(EventArgs e)
    {
        if (m_part != null)
        {
            m_part.TooManyNodesSelected -= this.part_TooManyNodesSelected;
            m_part.TooFewNodesSelected -= this.part_TooFewNodesSelected;
        }
        else
        {
            Debug.WriteLine("m_part is null !!!");
        }
        base.OnUnload(e);
    }

    protected override ScriptControlDescriptor GetScriptDescriptor()
    {
        var descriptor = base.GetScriptDescriptor();
        descriptor.Type = "Orion.Voip.Controls.Wizards.Operations.AssignOperationsWebWizardPartControl";
        return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        var result = base.GetScriptReferences();
        result = result.Concat(new[] { new ScriptReference(this.ResolveUrl("AssignOperationsWebWizardPartControl.js")) });
        return result;
    }


}
