﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MonitorSelectNodesWebWizardPartControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_Operations_MonitorSelectNodesWebWizardPartControl" %>
<%@ Register src="ChooseNodesTreeControl.ascx" tagname="ChooseNodesTreeControl" tagprefix="ipsla" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web.Wizards.Operations" %>
<%@ Register src="../../../Controls/NodeGroupingSelector.ascx" tagname="NodeGroupingSelector" tagprefix="ipsla" %>
<%@ Register src="~/Orion/Voip/Controls/Wizards/Operations/CredentialsCheckControl.ascx" tagname="CredentialsCheckControl" tagprefix="ipsla" %>

<div class="ipsla_WP_Title">
  <% if (this.WizardData.MonitorExplicitOperationNumbers != false)
     { %>
    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_85 %>
  <% } else { %>
    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_86 %>
  <% } %>
</div>
<div class="ipsla_WP_Description">
  <% if (this.WizardData.MonitorExplicitOperationNumbers != false)
     { %>
  <% } else { %>
    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_87 %>
  <% } %>
</div>
<div class="ipsla_WP_Description">
</div>

<div class="TreeGroupBy">
<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_68 %><br />
<ipsla:NodeGroupingSelector ID="groupBySelector" runat="server" OnSelectedValueChanged="groupBySelector_SelectedValueChanged" IncludeSiteProperties="True" />
</div>

<div class="TreeContent">
    <ipsla:ChooseNodesTreeControl ID="ChooseNodesTreeControl" runat="server">
        <WebService Path="~/Orion/Voip/Controls/Wizards/Operations/ChooseNodesAjaxTreeService.asmx" />
    </ipsla:ChooseNodesTreeControl>
</div>

<ipsla:CredentialsCheckControl ID="CredentialsCheckControl" runat="server"
    WindowTitle="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_88 %>"
    WindowMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_89 %>"
    IssueIconUrl="/Orion/Voip/images/lightbulb_tip_16x16.gif"
    />
