﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Voip.Controls.Wizards.Operations");

Orion.Voip.Controls.Wizards.Operations.OperationsWizardControlContainer = function(element) {
    Orion.Voip.Controls.Wizards.Operations.OperationsWizardControlContainer.initializeBase(this, [element]);

    this.eventOperationAdding = [];

    this.btnNext = null;
}

Orion.Voip.Controls.Wizards.Operations.OperationsWizardControlContainer.prototype = {

    get_btnNext: function() { return this.btnNext; },
    set_btnNext: function(value) { this.btnNext = value; },

    add_OperationsAdding: function(handler) { this.get_events().addHandler('OperationsAdding', handler); },
    remove_OperationsAdding: function(handler) { this.get_events().removeHandler('OperationsAdding', handler); },

    ShowButton: function(buttonId, show) {
        var btn = null;
        switch (buttonId) {
            case 'Next':
                btn = this.btnNext;
                break;
        }
        if (btn != null) {
            btn = jQuery(btn);
            if (show) btn.show();
            else btn.hide();
        }
    },

    OnAnotherSetupRunning: function() {
        Ext.MessageBox.show({
            title: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}",
            icon: Ext.MessageBox.ERROR,
            msg: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_47;E=js}",
            closable: false,
            buttons: Ext.MessageBox.OK
        });
    },

    OnSetupCanceled: function() {
        Ext.MessageBox.show({
            title: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}",
            icon: Ext.MessageBox.ERROR,
            msg: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_48;E=js}",
            closable: false,
            buttons: Ext.MessageBox.OK
        });
    },

    OnSetupError: function(message) {
        Ext.MessageBox.show({
            title: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}",
            icon: Ext.MessageBox.ERROR,
            msg: String.format("@{R=VNQM.Strings;K=VNQMWEBJS_VB1_49;E=js}", message),
            closable: false,
            buttons: Ext.MessageBox.OK
        });
    },

    CancelWizard: function(cancelFunc) {
        Ext.MessageBox.show({
            title: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}",
            icon: Ext.MessageBox.QUESTION,
            msg: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_50;E=js}',
            closable: false,
            buttons: Ext.MessageBox.YESNO,
            fn: function(btn, value) {
                if (btn == 'yes') {
                    cancelFunc();
                }
            }
        });
    },

    OnOperationsAdding: function() {
        if (CheckDemoMode()) return false;

        var handlers = this.get_events().getHandler('OperationsAdding');
        if (handlers) {
            var e = new Sys.CancelEventArgs();
            handlers(this, e);
            return !e.get_cancel();
        }

        return true;
    },

    initialize: function() {
        Orion.Voip.Controls.Wizards.Operations.OperationsWizardControlContainer.callBaseMethod(this, 'initialize');
        // Add custom initialization here
    },
    dispose: function() {
        // Add custom dispose actions here
        Orion.Voip.Controls.Wizards.Operations.OperationsWizardControlContainer.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Controls.Wizards.Operations.OperationsWizardControlContainer.registerClass('Orion.Voip.Controls.Wizards.Operations.OperationsWizardControlContainer', Orion.Voip.Controls.Wizards.WizardControl);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
