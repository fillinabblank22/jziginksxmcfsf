﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DefinePropertiesWebWizardPartControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_Operations_DefinePropertiesWebWizardPartControl" %>
<%@ Register Src="~/Orion/Voip/Controls/EditOperation.ascx" TagPrefix="ipsla" TagName="EditOperation" %>	

<div class="ipsla_WP_Title">
    <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_59, SolarWinds.Orion.IpSla.Web.Wizards.Operations.OperationsWizardSession.Current.Data.OperationTypeName)%>
</div>
<div class="ipsla_WP_Description">
    <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_60, SolarWinds.Orion.IpSla.Web.Wizards.Operations.OperationsWizardSession.Current.Data.OperationTypeName)%>
</div>

<asp:UpdatePanel ID="EditorUpdatePanel" runat="server">
<ContentTemplate>
<div id="editDialog">
<ipsla:EditOperation runat="server" ID="editOp" SmartMode="true" ExcludeIndividualSettings="true" />
</div>

<div style="margin: 15px 0px 15px 0px">
    <orion:LocalizableButton runat="server" ID="RestoreDefaultsImageButton" OnClick="RestoreDefaultsImageButton_Click" Text="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_61 %>"  ToolTip="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_61 %>" CausesValidation ="false" DisplayType="Secondary"/>
</div>
</ContentTemplate>
</asp:UpdatePanel>