﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Voip.Controls.Wizards.Operations");

Orion.Voip.Controls.Wizards.Operations.ReviewWebWizardControl = function(element) {
    Orion.Voip.Controls.Wizards.Operations.ReviewWebWizardControl.initializeBase(this, [element]);

    var opTypeHtmlFormat = "<img src='/Orion/Voip/images/OperationTypeIcons/{0}_16.gif' style='vertical-align:middle;'> {1}";
    this.opTypeToHtml = {
        Dhcp: String.format(opTypeHtmlFormat, "Dhcp", "@{R=VNQM.Strings;K=OperationType_dhcp;E=js}"),
        Dns: String.format(opTypeHtmlFormat, "Dns", "@{R=VNQM.Strings;K=OperationType_dns;E=js}"),
        Ftp: String.format(opTypeHtmlFormat, "Ftp", "@{R=VNQM.Strings;K=OperationType_ftp;E=js}"),
        Http: String.format(opTypeHtmlFormat, "Http", "@{R=VNQM.Strings;K=OperationType_http;E=js}"),
        IcmpEcho: String.format(opTypeHtmlFormat, "IcmpEcho", "@{R=VNQM.Strings;K=OperationType_icmpecho;E=js}"),
        RtpBasedVoip: String.format(opTypeHtmlFormat, "Rtp", "@{R=VNQM.Strings;K=OperationType_rtpbasedvoip;E=js}"),
        TcpConnect: String.format(opTypeHtmlFormat, "TcpConnect", "@{R=VNQM.Strings;K=OperationType_tcpconnect;E=js}"),
        UdpEcho: String.format(opTypeHtmlFormat, "UdpEcho", "@{R=VNQM.Strings;K=OperationType_udpecho;E=js}"),
        UdpJitter: String.format(opTypeHtmlFormat, "UdpJitter", "@{R=VNQM.Strings;K=OperationType_udpjitter;E=js}"),
        UdpJitterCli: String.format(opTypeHtmlFormat, "UdpJitter", "@{R=VNQM.Strings;K=OperationType_udpjittercli;E=js}"),
        UdpVoipJitter: String.format(opTypeHtmlFormat, "VoipJitter", "@{R=VNQM.Strings;K=OperationType_udpvoipjitter;E=js}"),
        UdpVoipJitterCli: String.format(opTypeHtmlFormat, "VoipJitter", "@{R=VNQM.Strings;K=OperationType_udpvoipjittercli;E=js}"),
        VoipCallSetup: String.format(opTypeHtmlFormat, "Voip", "@{R=VNQM.Strings;K=OperationType_voipcallsetup;E=js}"),
        IcmpPathEcho: String.format(opTypeHtmlFormat, "IcmpPathEcho", "@{R=VNQM.Strings;K=OperationType_icmppathecho;E=js}"),
        IcmpPathJitter: String.format(opTypeHtmlFormat, "IcmpPathJitter", "@{R=VNQM.Strings;K=OperationType_icmppathjitter;E=js}")
    };

    var operationsDataStore;
    var operationsSelectionModel;
    var operationsColumnModel;
    var operationsGrid;
    var editorWindow;

    var gridArea;
    var btnOpenEditor;
    var btnSubmitEditor;
    var btnCancelEditor;
    var nodesStateField;

    var editOpID;

    var waitBeforeSave = false;

    this.operationsAddingHandler = null;

    this.gridAreaCaption = null;

    this.isMonitorMode = false;
    this.nodesDone = 0;
    this.nodesTotal = 0;
    this.progressCurrent = 0;
    this.progressTotal = 1;
    this.operationScanArea = null;
    this.progressControl = null;
    this.useTagAsNameCheckBox = null;
    this.operationListArea = null;
    this.operationDiscoveryExclusionsArea = null;
    this.operationsNotFoundArea = null;
    this.operationsNotFoundList = null;
    this.operationsMonitoredArea = null;
    this.operationsMonitoredList = null;
    this.operationsAutomaticArea = null;
    this.operationsAutomaticList = null;
}

Orion.Voip.Controls.Wizards.Operations.ReviewWebWizardControl.prototype = {
    get_gridArea: function () { return this.gridArea; },
    set_gridArea: function (value) { this.gridArea = value; },

    get_gridAreaCaption: function () { return this.gridAreaCaption; },
    set_gridAreaCaption: function (value) { this.gridAreaCaption = value; },

    get_operationScanArea: function (value) { return this.operationScanArea; },
    set_operationScanArea: function (value) { this.operationScanArea = value; },

    get_progressControl: function (value) { return this.progressControl; },
    set_progressControl: function (value) { this.progressControl = value; },

    get_useTagAsNameCheckBox: function () { return this.useTagAsNameCheckBox; },
    set_useTagAsNameCheckBox: function (value) { this.useTagAsNameCheckBox = value; },

    get_operationListArea: function (value) { return this.operationListArea; },
    set_operationListArea: function (value) { this.operationListArea = value; },

    get_operationDiscoveryExclusionsArea: function (value) { return this.operationDiscoveryExclusionsArea; },
    set_operationDiscoveryExclusionsArea: function (value) { this.operationDiscoveryExclusionsArea = value; },
    get_operationsNotFoundArea: function (value) { return this.operationsNotFoundArea; },
    set_operationsNotFoundArea: function (value) { this.operationsNotFoundArea = value; },
    get_operationsNotFoundList: function (value) { return this.operationsNotFoundList; },
    set_operationsNotFoundList: function (value) { this.operationsNotFoundList = value; },
    get_operationsMonitoredArea: function (value) { return this.operationsMonitoredArea; },
    set_operationsMonitoredArea: function (value) { this.operationsMonitoredArea = value; },
    get_operationsMonitoredList: function (value) { return this.operationsMonitoredList; },
    set_operationsMonitoredList: function (value) { this.operationsMonitoredList = value; },
    get_operationsAutomaticArea: function (value) { return this.operationsAutomaticArea; },
    set_operationsAutomaticArea: function (value) { this.operationsAutomaticArea = value; },
    get_operationsAutomaticList: function (value) { return this.operationsAutomaticList; },
    set_operationsAutomaticList: function (value) { this.operationsAutomaticList = value; },

    get_btnOpenEditor: function (value) { return this.btnOpenEditor; },
    set_btnOpenEditor: function (value) { this.btnOpenEditor = value; },

    get_btnSubmitEditor: function (value) { return this.btnSubmitEditor; },
    set_btnSubmitEditor: function (value) { this.btnSubmitEditor = value; },

    get_btnCancelEditor: function (value) { return this.btnCancelEditor; },
    set_btnCancelEditor: function (value) { this.btnCancelEditor = value; },

    get_editOpID: function () { return this.editOpID; },
    set_editOpID: function (value) { this.editOpID = value; },

    get_nodesStateField: function (value) { return this.nodesStateField; },
    set_nodesStateField: function (value) { this.nodesStateField = value; },

    OnError: function (error, context) {
        if (error.get_statusCode() > 0) {
            Ext.Msg.show({
                title: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}',
                msg: String.format('@{R=VNQM.Strings;K=VNQMWEBJS_VB1_52;E=js}', error.get_message()),
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
    },

    StartScan: function () {
        this.wizardControl.ShowButton('Next', false);
        this.RefreshScanStatus();
        this.StartTimer();
    },

    StartReview: function () {
        this.RefreshOperationsExclusions();
    },

    StartTimer: function () {
        window.setTimeout(Function.createDelegate(this, this.OnTick), 1000);
    },

    OnTick: function (error, context) {
        //ask for state
        Orion.Voip.Controls.Wizards.Operations.OperationDiscoveryService.GetState(
            Function.createDelegate(this, function (result, context) {
                if (result == null || result.Finished) {
                    this.RefreshOperationsExclusions();
                    this.operationsDataStore.load();
                    jQuery(this.operationScanArea).hide();
                    jQuery(this.operationListArea).show();
                    this.wizardControl.ShowButton('Next', true);
                    this.UpdateGridSize();
                    return;
                }
                this.nodesDone = result.FinishedNodes;
                this.nodesTotal = result.TotalNodes;
                this.progressCurrent = result.ProgressCurrent;
                this.progressTotal = result.ProgressTotal;
                this.RefreshScanStatus();
                this.StartTimer();
            }),
            Function.createDelegate(this, this.OnError)
        );
    },

    RefreshScanStatus: function () {
        var processedInnerHtml = '<div class=\"ipsla_ProgressLegendValue\">' + this.nodesDone + '</div>&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_VB1_53;E=js}';
        var totalInnerHtml = '<div class=\"ipsla_ProgressLegendValue\">' + this.nodesTotal + '</div>&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_VB1_54;E=js}';
        var value = (this.progressTotal != 0 ? this.progressCurrent / this.progressTotal : 0);
        this.progressControl.UpdateProgress(value, processedInnerHtml, totalInnerHtml);
    },

    RefreshOperationsExclusions: function () {
        if (this.isMonitorMode) {
            Orion.Voip.Controls.Wizards.Operations.OperationDiscoveryService.GetOperationsExcluded(
                Function.createDelegate(this, function (result, context) {
                    var notFoundHtml = '', monitoredHtml = '', automaticHtml = '';
                    var resultLen = result.length;
                    if (resultLen > 0) {
                        var formatOperationListOrCount = function (sourceName, explicitNumbers, listOrCount) {
                            if (explicitNumbers ? listOrCount.length == 0 : listOrCount == 0)
                                return '';
                            var resultHtml = '<tr><td class="sourceName" style="white-space: nowrap; width: auto;">' + String.format("@{R=VNQM.Strings;K=VNQMWEBJS_VB1_55;E=js}", sourceName) + '</td><td>';
                            if (explicitNumbers) {
                                resultHtml += listOrCount.join('@{R=VNQM.Strings;K=VNQMWEBJS_VB1_56;E=js}');
                            }
                            else if (listOrCount == 1) {
                                resultHtml += String.format('@{R=VNQM.Strings;K=VNQMWEBJS_VB1_57;E=js}', listOrCount);
                            }
                            else {
                                resultHtml += String.format('@{R=VNQM.Strings;K=VNQMWEBJS_VB1_58;E=js}', listOrCount);
                            }
                            return resultHtml + '</td></tr>';
                        }
                        for (var i = 0; i < resultLen; i++) {
                            var item = result[i];
                            notFoundHtml += formatOperationListOrCount(item.SourceName, item.ExplicitNumbers, item.NotFound);
                            monitoredHtml += formatOperationListOrCount(item.SourceName, item.ExplicitNumbers, item.AlreadyMonitored);
                            automaticHtml += formatOperationListOrCount(item.SourceName, item.ExplicitNumbers, item.Automatic);
                        }
                        if (this.operationsGrid) //FB  11544 - the yellow box is covered by the grid with results in IE 7 and 8
                        {
                            this.operationsGrid.hide();
                            this.operationsGrid.show();
                        }
                    }
                    if (notFoundHtml.length > 0) {
                        jQuery(this.operationDiscoveryExclusionsArea).show();
                        jQuery(this.operationsNotFoundArea).show();
                        jQuery(this.operationsNotFoundList).html(notFoundHtml).show();
                    }
                    if (monitoredHtml.length > 0) {
                        jQuery(this.operationDiscoveryExclusionsArea).show();
                        jQuery(this.operationsMonitoredArea).show();
                        jQuery(this.operationsMonitoredList).html(monitoredHtml).show();
                    }
                    if (automaticHtml.length > 0) {
                        jQuery(this.operationDiscoveryExclusionsArea).show();
                        jQuery(this.operationsAutomaticArea).show();
                        jQuery(this.operationsAutomaticList).html(automaticHtml).show();
                    }
                }),
                Function.createDelegate(this, this.OnError)
            );
        }
    },

    initialize: function () {
        Orion.Voip.Controls.Wizards.Operations.ReviewWebWizardControl.callBaseMethod(this, 'initialize');

        if (this.isMonitorMode) {
            this.operationsAddingHandler = Function.createDelegate(this, function (sender, e) {
                this.persistCurrentSelection();
            });
            this.wizardControl.add_OperationsAdding(this.operationsAddingHandler);
        }

        Ext.onReady(Function.createDelegate(this, function () {
            Ext.QuickTips.init();
            this.initializeGrid();

            this.operationsGrid.render(this.gridArea);

            this.operationsSelectionModel.on("selectionchange", Function.createDelegate(this, this.updateToolbarButtons));
            //this.operationsDataStore.load({ params: { start: 0, limit: 2} });
            this.operationsDataStore.load();
            this.updateToolbarButtons();

            var updateGridSizeFunc = Function.createDelegate(this, this.UpdateGridSize);
            jQuery(window).resize(updateGridSizeFunc);
            setTimeout(updateGridSizeFunc, 0);

            var endRequestDelegate = Function.createDelegate(this, this.endRequest);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequestDelegate);
        }));
    },
    dispose: function () {
        if (this.operationsAddingHandler) {
            this.wizardControl.remove_OperationsAdding(this.operationsAddingHandler);
        }

        jQuery(window).unbind();

        Orion.Voip.Controls.Wizards.Operations.ReviewWebWizardControl.callBaseMethod(this, 'dispose');
    },

    UpdateGridSize: function () {
        this.operationsGrid.setSize(1600, 200);
    },

    initializeGrid: function () {
        this.operationsDataStore = new Ext.data.Store({
            id: 'operationsDataStore',
            proxy: new Ext.data.HttpProxy({
                url: '/Orion/Voip/Controls/Wizards/Operations/ReviewExtGridDataProvider.ashx',
                method: 'POST'
            }),
            baseParams: { task: "select" }, // this parameter is passed for any HTTP request
            reader: new Ext.data.JsonReader({
                root: 'results',
                totalProperty: 'total',
                id: 'id'
            }, [
                            { name: 'OperationID', type: 'int' },
                            { name: 'OperationNumber', type: 'int' },
                            { name: 'OperationName', type: 'string' },
                            { name: 'OperationType', type: 'string' },
                            { name: 'Source', type: 'string' },
                            { name: 'SourceAddress', type: 'string' },
                            { name: 'Target', type: 'string' },
                            { name: 'TargetAddress', type: 'string' },
                            { name: 'Frequency', type: 'int' },
                            { name: 'AdminTag', type: 'string' },
                            { name: 'Owner', type: 'string' },
                        ]),
            sortInfo: { field: 'Source', direction: "ASC" }
        });

        this.operationsSelectionModel = new Ext.grid.CheckboxSelectionModel({ singleSelect: false });

        var columns = [this.operationsSelectionModel,
            {
                header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_7;E=js}',
                dataIndex: 'OperationName',
                width: 200
            }, {
                header: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_59;E=js}',
                dataIndex: 'SourceAddress',
                width: 125
            }, {
                header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_13;E=js}',
                dataIndex: 'Target',
                width: 150
            }, {
                header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_14;E=js}',
                dataIndex: 'Frequency',
                renderer: function (value, meta, record) {
                    return String.format('@{R=VNQM.Strings;K=VNQMWEBJS_VB1_60;E=js}', value);
                },
                width: 100
            }
        ];

        if (this.isMonitorMode) {
            columns.splice(1, 0,
                {
                    header: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_61;E=js}',
                    dataIndex: 'Source',
                    width: 100
                }, {
                    header: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_62;E=js}',
                    dataIndex: 'OperationNumber',
                    width: 100
                }, {
                    header: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_63;E=js}',
                    dataIndex: 'OperationType',
                    renderer: Function.createDelegate(this, function (value, meta, record, sender) {
                        var html = this.opTypeToHtml[value];
                        return html != null ? html : "<i>@{R=VNQM.Strings;K=OperationStatus_unknown;E=js}</i>";
                    }),
                    width: 125
                }
            );
            columns.splice(8, 0,
                {
                    header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_15;E=js}',
                    dataIndex: 'AdminTag',
                    width: 150,
                    renderer: function (value, meta, record) {
                        if (!value || value == "")
                            return "<i>@{R=VNQM.Strings;K=VNQMWEBJS_TM0_12;E=js}</i>";
                        else return value;
                    }
                },
                {
                    header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_18;E=js}',
                    dataIndex: 'Owner',
                    width: 300,
                    hidden: true,
                    renderer: function (value, meta, record) {
                        if (!value || value == "")
                            return "<i>@{R=VNQM.Strings;K=VNQMWEBJS_TM0_12;E=js}</i>";
                        else return value;
                    }
                }
            );
        }
        else {
            columns.splice(4, 0,
                {
                    header: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_64;E=js}',
                    dataIndex: 'TargetAddress',
                    width: 125
                }
            );
            columns.splice(2, 0,
                {
                    header: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_61;E=js}',
                    dataIndex: 'Source',
                    width: 100
                }
            );
        }

        this.operationsColumnModel = new Ext.grid.ColumnModel(columns);
        this.operationsColumnModel.defaultSortable = true;

        var toolbar = this.isMonitorMode ? null :
            [{
                id: "btnEdit",
                text: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_22;E=js}',
                tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_65;E=js}',
                handler: Function.createDelegate(this, this.editOperations),
                iconCls: 'ipsla_EditOperation'
            },
            '-',
            {
                id: "btnDelete",
                text: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_24;E=js}',
                tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_66;E=js}',
                handler: Function.createDelegate(this, this.confirmDeleteOperations),   // Confirm before deleting
                iconCls: 'ipsla_RemoveOperation'
            }];

        this.operationsGrid = new Ext.grid.EditorGridPanel({
            id: 'operationsGrid',
            store: this.operationsDataStore,
            cm: this.operationsColumnModel,
            enableColLock: false,
            selModel: this.operationsSelectionModel,
            height: 200,
            width: 1200,
            //            bbar: new Ext.PagingToolbar({
            //                pageSize: 2,
            //                store: this.operationsDataStore,
            //                displayInfo: true
            //            }),
            tbar: toolbar
        });
    },

    updateToolbarButtons: function () {
        if (!this.isMonitorMode) {
            var count = this.operationsSelectionModel.getCount();
            var map = this.operationsGrid.getTopToolbar().items.map;

            map.btnEdit.setDisabled(count === 0);
            map.btnDelete.setDisabled(count === 0);
        }
    },

    updateNamesByTag: function () {
        Ext.Ajax.request({
            waitMsg: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_67;E=js}',
            url: '/Orion/Voip/Controls/Wizards/Operations/ReviewExtGridDataProvider.ashx',
            params: {
                task: "updateNames"
            },
            success: Function.createDelegate(this, function (response) {
                var result = eval(response.responseText);
                this.operationsDataStore.load();
                this.useTagAsNameCheckBox.checked = result;

            }),
            failure: function (response) {
                var result = response.responseText;
                Ext.MessageBox.alert('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_19;E=js}', '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_68;E=js}');
            }
        });
    },

    editOperations: function () {
        var count = this.operationsGrid.selModel.getCount();
        if (count == 0) // only one operation is selected here
        {
            Ext.MessageBox.alert('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_1;E=js}', '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_69;E=js}');
        } else {
            this.persistCurrentSelection();

            var title = '';
            if (count == 1)
                title += String.format('@{R=VNQM.Strings;K=VNQMWEBJS_TM0_3;E=js}', this.operationsGrid.getSelectionModel().getSelected().data['OperationName']);
            else
                title += '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_4;E=js}';

            $('.x-window-header').html(title);

            this.showEditor();
        }
    },

    persistCurrentSelection: function () {
        var ids = this.serializeSelections();
        this.nodesStateField.value = ids;
    },

    serializeSelections: function () {
        var selections = this.operationsGrid.selModel.getSelections();
        var selectedNodes = [];
        for (i = 0; i < this.operationsGrid.selModel.getCount(); i++) {
            selectedNodes.push(selections[i].json.OperationID);
        }
        return Sys.Serialization.JavaScriptSerializer.serialize(selectedNodes);

    },

    showEditor: function () {
        if (!this.editorWindow) {
            this.editorWindow = new Ext.Window({
                applyTo: 'editor',
                layout: 'fit',
                width: 600,
                height: 500,
                closeAction: 'hide',
                closable: false,
                autoScroll: true,
                plain: true,
                resizable: false,
                modal: true,
                
                buttons: [{
                    text: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_70;E=js}',
                    handler: Function.createDelegate(this, this.submitEditor)
                }, {
                    text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_26;E=js}',
                    handler: Function.createDelegate(this, this.cancelEditor)
                }]
            });
        }
        this.openEditor();
    },

    submitEditor: function () {
        this.waitBeforeSave = true;
        var clickDelegate = Function.createDelegate(this, this.btnSubmitEditorClick);
        setTimeout(clickDelegate, 0);
    },

    btnSubmitEditorClick: function () {
        this.btnSubmitEditor.click();
    },

    cancelEditor: function () {
        var clickDelegate = Function.createDelegate(this, this.btnCancelEditorClick);
        setTimeout(clickDelegate, 0);
        this.editorWindow.hide();
    },

    btnCancelEditorClick: function () {
        this.btnCancelEditor.click();
    },

    openEditor: function () {
        var clickDelegate = Function.createDelegate(this, this.btnOpenEditorClick);
        setTimeout(clickDelegate, 0);
        this.editorWindow.show();
    },

    btnOpenEditorClick: function () {
        this.btnOpenEditor.click();
    },

    endRequest: function (sender, args) {
        //only "our" requests
        if (sender._postBackSettings.sourceElement == this.btnSubmitEditor) {
            if (this.waitBeforeSave) {
                // waiting for edit to complete update - close edit dialog and refresh
                this.waitBeforeSave = false;
                var dataItems = args.get_dataItems()[this.editOpID];
                var isValid = (!dataItems || dataItems['IsValid'] != false);
                if (Page_IsValid && isValid) {
                    this.editorWindow.hide();
                    //RefreshObjects();
                }
            }
        }
        //if (!this.editorWindow.hidden) {
        //    this.editorWindow.syncSize();
        //}
    },

    confirmDeleteOperations: function () {
        if (this.operationsGrid.selModel.getCount() == 1) { // only one operation is selected here
            Ext.MessageBox.confirm('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_3;E=js}', '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_71;E=js}', Function.createDelegate(this, this.deleteOperations));
        } else if (this.operationsGrid.selModel.getCount() > 1) {
            Ext.MessageBox.confirm('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_3;E=js}', '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_72;E=js}', Function.createDelegate(this, this.deleteOperations));
        } else {
            Ext.MessageBox.alert('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_1;E=js}', '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_73;E=js}');
        }
    },

    deleteOperations: function (btn) {
        if (btn == 'yes') {
            var selections = this.operationsGrid.selModel.getSelections();
            var selectedOperations = [];
            for (i = 0; i < this.operationsGrid.selModel.getCount(); i++) {
                selectedOperations.push(selections[i].json.OperationID);
            }
            var encoded_array = Ext.encode(selectedOperations);
            var reloadDelegate = Function.createDelegate(this.operationsDataStore, this.operationsDataStore.reload);
            Ext.Ajax.request({
                waitMsg: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_67;E=js}',
                url: '/Orion/Voip/Controls/Wizards/Operations/ReviewExtGridDataProvider.ashx',
                params: {
                    task: "delete",
                    ids: encoded_array
                },
                success: function (response) {
                    var result = eval(response.responseText);
                    reloadDelegate();
                    switch (result) {
                        case 1:  // Success
                            break;
                        default:
                            Ext.MessageBox.alert('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_17;E=js}', '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_18;E=js}');
                            break;
                    }
                },
                failure: function (response) {
                    var result = response.responseText;
                    Ext.MessageBox.alert('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_19;E=js}', '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_74;E=js}');
                }
            });
        }
    }
}
Orion.Voip.Controls.Wizards.Operations.ReviewWebWizardControl.registerClass('Orion.Voip.Controls.Wizards.Operations.ReviewWebWizardControl', Orion.Voip.Controls.Wizards.WizardFlowPartControl);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();