﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Contracts;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.Wizards;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;
using SolarWinds.Orion.Web.UI;
using ButtonType = SolarWinds.Orion.Web.UI.ButtonType;

public partial class Orion_Voip_Controls_Wizards_Operations_SummaryWebWizardControl : WizardFlowPartControl<OperationsWizardData>
{
    private OperationSetupStatus setupStatus;
    private readonly PlaceHolder phAdditionalWizardButtons;
    private readonly LocalizableButtonLink btnAddMoreOperations;

    public Orion_Voip_Controls_Wizards_Operations_SummaryWebWizardControl()
    {
        this.phAdditionalWizardButtons = new PlaceHolder { ID = "phSummaryButtons" };

        btnAddMoreOperations = new LocalizableButtonLink();
        btnAddMoreOperations.ID = "ibtnAddMoreOperations";
        btnAddMoreOperations.Text = Resources.VNQMWebContent.VNQMWEBDATA_VB1_78;
        btnAddMoreOperations.DisplayType = ButtonType.Secondary;
        btnAddMoreOperations.ToolTip = Resources.VNQMWebContent.VNQMWEBDATA_VB1_78;
        btnAddMoreOperations.NavigateUrl = "/Orion/Voip/Admin/OperationsWizard.aspx";
        this.btnAddMoreOperations.Style[HtmlTextWriterStyle.Display] = "none";
        this.btnAddMoreOperations.Style[HtmlTextWriterStyle.MarginRight] = "10px";


        this.phAdditionalWizardButtons.Controls.Add(this.btnAddMoreOperations);

        LocalizableButtonLink btn = new LocalizableButtonLink();
        btn.ID = "ibtnGoHome";
        btn.Text = Resources.VNQMWebContent.VNQMWEBDATA_VB1_79;
        btn.DisplayType = ButtonType.Primary;
        btn.ToolTip = Resources.VNQMWebContent.VNQMWEBDATA_VB1_79;
        btn.NavigateUrl = "/Orion/Voip/Summary.aspx";

        this.phAdditionalWizardButtons.Controls.Add(btn);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.setupStatus = OperationsWizardSession.Current.Data.BusinessLayerSetupStatus;

        string createdCountStr;
        string totalCountStr;
        if (this.setupStatus == null)
        {
            if (!OperationsWizardSession.Current.Finished)
            {
                var setupPackage = new List<OperationSetupItem>();

                foreach (var operation in OperationsWizardSession.Current.Data.Operations)
                {
                    var item = new OperationSetupItem
                    {
                        ID = operation.TemporaryID,
                        OperationDefinition = operation.OperationDefinition,
                    };
                    setupPackage.Add(item);
                }

                using (var blProxy = BusinessLayer.GetBusinessLayer())
                {
                    var setupId = blProxy.CreateOperationSetupPackage();
                    try
                    {
                        BusinessLayer.SendList(blProxy.AddOperationSetupItems, setupId, setupPackage);
                        this.setupStatus = blProxy.StartOperationSetupPackage(setupId);
                    }
                    catch
                    {
                        blProxy.CloseOperationSetupPackage(setupId);
                        throw;
                    }
                }
            }
            createdCountStr = "0";
            totalCountStr = OperationsWizardSession.Current.Data.Operations.Count.ToString();
        }
        else
        {
            using (var blProxy = BusinessLayer.GetBusinessLayer())
            {
                this.setupStatus = blProxy.GetOperationSetupPackageStatus(this.setupStatus.SetupId);
            }

            if (this.setupStatus.IsSuspended)
            {
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowPauseDialog",
                    "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').ShowPauseDialog();},0);});",
                    true);
            }

            createdCountStr = (this.setupStatus.CreatedItemCount + this.setupStatus.FailedItemCount).ToString();
            totalCountStr = this.setupStatus.TotalItemCount.ToString();
        }

        //this.totalAreaHead.InnerText = totalCountStr;
        //this.createdArea.InnerText = createdCountStr;
        //this.totalArea.InnerText = totalCountStr;

        OperationsWizardSession.Current.Data.BusinessLayerSetupStatus = this.setupStatus;

        if (this.WizardData.WizardMode != WizardMode.MonitorExistingOperations)
        {
            createdAreaResultsExist.Attributes.Add("style", "visibility: hidden");
            totalAreaHeadExist.Attributes.Add("style", "visibility: hidden");
        }
        else
        {
            createdAreaResults.Attributes.Add("style", "visibility: hidden");
            totalAreaHead.Attributes.Add("style", "visibility: hidden");
        } 
    }

    public override bool Initialize(OperationsWizardData data)
    {
        return true;
    }

    public override bool ModifyData(OperationsWizardData data)
    {
        return true;
    }

    public Control AdditionalWizardButtons
    {
        get { return this.phAdditionalWizardButtons; }
    }

    private HtmlAnchor CreateButton(string id, string imageUrl, string alternateText, string toolTip, string href)
    {
        var link = new HtmlAnchor
                       {
                           ID = id,
                           HRef = href,
                           Title = toolTip,
                       };
        link.Controls.Add(new HtmlImage
                              {
                                  ID = id + "img",
                                  Alt = alternateText,
                                  Src = imageUrl,
                              });
        return link;
    }

    protected override ScriptControlDescriptor GetScriptDescriptor()
    {
        var descriptor = base.GetScriptDescriptor();
        descriptor.Type = "Orion.Voip.Controls.Wizards.Operations.SummaryWebWizardControl";
        descriptor.AddProperty("progressAreaID", progressArea.ClientID);
        //descriptor.AddProperty("pbarBoxID", pbarBox.ClientID);
        //descriptor.AddProperty("pbarID", pbar.ClientID);
        //descriptor.AddProperty("createdAreaID", createdArea.ClientID);
        //descriptor.AddProperty("totalAreaID", totalArea.ClientID);
        descriptor.AddProperty("totalAreaHeadID", totalAreaHead.ClientID);
        descriptor.AddProperty("resultsAreaID", resultsArea.ClientID);
        descriptor.AddProperty("createdAreaResultsID", createdAreaResults.ClientID);
        descriptor.AddProperty("gridAreaResultsID", gridAreaResults.ClientID);
        descriptor.AddProperty("totalAreaHeadExistID", totalAreaHeadExist.ClientID);
        descriptor.AddProperty("createdAreaResultsExistID", createdAreaResultsExist.ClientID);

        if (this.setupStatus != null)
        {
            descriptor.AddProperty("setupId", this.setupStatus.SetupId);
            descriptor.AddProperty("setupCreated", this.setupStatus.CreatedItemCount + this.setupStatus.FailedItemCount);
            descriptor.AddProperty("setupTotal", this.setupStatus.TotalItemCount);
        }
        descriptor.AddProperty("AddMoreOperationsID", this.btnAddMoreOperations.ClientID);
        descriptor.AddComponentProperty("progressControl", this.ProgressControl.ClientID);
        return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        var result = base.GetScriptReferences();
        result = result.Concat(new[] { new ScriptReference(this.ResolveUrl("SummaryWebWizardControl.js")) });
        return result;
    }
}
