﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Voip.Controls.Wizards.Operations");

Orion.Voip.Controls.Wizards.Operations.MonitorSelectNodesWebWizardPartControl = function(element) {
    Orion.Voip.Controls.Wizards.Operations.MonitorSelectNodesWebWizardPartControl.initializeBase(this, [element]);
}

Orion.Voip.Controls.Wizards.Operations.MonitorSelectNodesWebWizardPartControl.prototype = {
    OnNotAllowedNodeSelected: function(message) {
        Ext.MessageBox.show({
            title: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}",
            icon: Ext.MessageBox.ERROR,
            msg: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_86;E=js}",
            closable: false,
            buttons: Ext.MessageBox.OK
        });
    },

    OnNoNodeSelected: function(message) {
        Ext.MessageBox.show({
            title: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}",
            icon: Ext.MessageBox.ERROR,
            msg: message,
            closable: false,
            buttons: Ext.MessageBox.OK
        });
    },

    initialize: function() {
        Orion.Voip.Controls.Wizards.Operations.MonitorSelectNodesWebWizardPartControl.callBaseMethod(this, 'initialize');
        
        // Add custom initialization here
    },
    dispose: function() {
        //Add custom dispose actions here
        Orion.Voip.Controls.Wizards.Operations.MonitorSelectNodesWebWizardPartControl.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Controls.Wizards.Operations.MonitorSelectNodesWebWizardPartControl.registerClass('Orion.Voip.Controls.Wizards.Operations.MonitorSelectNodesWebWizardPartControl', Orion.Voip.Controls.Wizards.WizardFlowPartControl);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
