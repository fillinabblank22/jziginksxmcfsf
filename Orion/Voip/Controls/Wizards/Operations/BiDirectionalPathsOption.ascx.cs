﻿using System;
using System.Web.UI;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;

public partial class Orion_Voip_Controls_Wizards_Operations_BiDirectionalPathsOption : System.Web.UI.UserControl
{
    public bool SingleNode { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.rbtnYes.Text = SingleNode ? Resources.VNQMWebContent.VNQMWEBCODE_VB1_39 : Resources.VNQMWebContent.VNQMWEBCODE_VB1_40;
        this.rbtnNo.Text = SingleNode ? Resources.VNQMWebContent.VNQMWEBCODE_VB1_41 : Resources.VNQMWebContent.VNQMWEBCODE_VB1_42;
        ResetRadioButtons();
    }

    protected void rbtn_CheckedChanged(object sender, EventArgs e)
    {
        var enable = this.rbtnYes.Checked;

        if (enable && OperationsWizardSession.Current.Data.UseExternalNodes)
        {
            if (OperationsWizardSession.Current.Data.PathDefinitionMethod == PathDefinitionMethod.Simple)
            {
                ScriptManager.RegisterStartupScript(Page, GetType(), "SingleDirectionalPathsNotAllowed",
                    SingleNode
                        ? String.Format("Ext.MessageBox.alert('{0}', '{1}');", Resources.VNQMWebContent.VNQMWEBCODE_AK1_1, Resources.VNQMWebContent.VNQMWEBCODE_VB1_43)
                        : String.Format("Ext.MessageBox.alert('{0}', '{1}');", Resources.VNQMWebContent.VNQMWEBCODE_AK1_1, Resources.VNQMWebContent.VNQMWEBCODE_VB1_44)
                    , true);
                enable = false;
            }
        }

        OperationsWizardSession.Current.Data.BiDirectionalPaths = enable;
        ResetRadioButtons();
    }

    private void ResetRadioButtons()
    {
        this.rbtnNo.Checked = !(this.rbtnYes.Checked = OperationsWizardSession.Current.Data.BiDirectionalPaths);
    }
}
