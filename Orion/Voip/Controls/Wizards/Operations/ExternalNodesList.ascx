﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExternalNodesList.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_Operations_ExternalNodesList" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<div class="externalNodes alternateRow">
    <asp:GridView ID="gwExternalNodes" runat="server" 
        AutoGenerateColumns="False" 
        GridLines="None" 
        ShowHeader="false"
        ShowFooter="false"
        OnDataBound="gwExternalNodes_OnDataBound"  >
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="lblExternalNode" runat="server" AssociatedControlID="txbExternalNode" Text="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_53 %>"></asp:Label>
                    <asp:TextBox ID="txbExternalNode" runat="server" 
                        autocomplete="off"  
                        Text='<%# DataBinder.Eval(Container.DataItem, "IpAddressOrHostName") %>'
                        CssClass="externalNodeInput"></asp:TextBox><br/>
                    <asp:CustomValidator
                        ID="vldCustDuplicity" runat="server" 
                        ControlToValidate="txbExternalNode"
                        OnServerValidate="vldCustDuplicity_ServerValidate"
                        ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_54 %>" 
                        Display="Dynamic"
                        ValidationGroup="Group">
                    </asp:CustomValidator>
                    <asp:RequiredFieldValidator 
                        ID="vldReqExternalNode" runat="server"
                        ControlToValidate="txbExternalNode" 
                        Display="Dynamic"
                        ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_55 %>"
                        EnableClientScript="false"
                        ValidationGroup="Group"
                        CssClass="error">
                    </asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator
                        id="vldRegExExternalNode" runat="server"
                        ControlToValidate="txbExternalNode" 
                        EnableClientScript="false"
                        Display="Dynamic"
                        ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_56 %>"
                        ValidationExpression="<%# REGEX_HOST_NAME_OR_IP_ADDRESS %>"
                        ValidationGroup="Group">
                    </asp:RegularExpressionValidator>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="deleteButton" runat="server"
                        CommandName="DeleteRow" 
                        CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' 
                        OnCommand="gwExternalNodes_RowDelete"
                        ImageUrl="~/Orion/Discovery/images/button_X_04.gif"
                        CausesValidation="false"
                        CssClass="deleteButton" />
                </ItemTemplate>
             </asp:TemplateField>   
        </Columns>
    </asp:GridView>
    <div class="buttonLine" style="text-align: left">
        <orion:LocalizableButton runat="server" ID="btnAddAnother" OnClick="btnAddAnother_Clicked" CausesValidation="false" Text="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_58 %>" DisplayType="Small"/>
    </div>    
    <div>
        <asp:CustomValidator
            ID="vldCustEmpty" 
            runat="server" 
            ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_57 %>" 
            EnableClientScript="false"
            OnServerValidate="vldCustEmpty_ServerValidate"
            Display="Dynamic"
            ValidationGroup="Group">
        </asp:CustomValidator>
    </div>
</div>