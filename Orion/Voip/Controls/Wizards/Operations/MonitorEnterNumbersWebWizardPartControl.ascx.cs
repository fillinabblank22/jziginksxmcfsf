﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.IpSla.Web.WebServices.AjaxTree;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;
using SolarWinds.Orion.IpSla.Web.Wizards;

public partial class Orion_Voip_Controls_Wizards_Operations_MonitorEnterNumbersWebWizardPartControl : WizardFlowPartControl<OperationsWizardData>
{
    private static readonly Log Log = new Log();

    private readonly List<OperationNumbersSaver> operationNumbersSavers = new List<OperationNumbersSaver>();

    public override bool Initialize(OperationsWizardData data)
    {
        Boxes.DataSource =
            from site in SitesDAL.Instance.GetSitesBySiteIDs(from x in data.SourceNodeList select x.NodeId)
            select new { Site = site, Numbers = IsPostBack ? null : GetOperationNumbers(data, site.ID) };
        Boxes.DataBind();
        return true;
    }

    public override bool ModifyData(OperationsWizardData data)
    {
        this.operationNumbersSavers.ForEach(x => x.Save());
        return true;
    }

    protected override ScriptControlDescriptor GetScriptDescriptor()
    {
        var descriptor = base.GetScriptDescriptor();
        descriptor.Type = "Orion.Voip.Controls.Wizards.Operations.MonitorEnterNumbersWebWizardPartControl";
        return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        var result = base.GetScriptReferences();
        result = result.Concat(new[] { new ScriptReference(ResolveUrl("MonitorEnterNumbersWebWizardPartControl.js")) });
        return result;
    }

    private static string GetOperationNumbers(OperationsWizardData data, int siteId)
    {
        KeyValuePair<string, int[]> pair;
        return data.ExplicitOperationNumbersToDiscover.TryGetValue(siteId, out pair)
            ? pair.Key
            : null;
    }

    protected void Boxes_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        switch (e.Item.ItemType)
        {
            case ListItemType.Item:
            case ListItemType.AlternatingItem:
                var repeaterItem = e.Item;
                var textBox = (TextBox)repeaterItem.FindControl("OperationNumbers");
                var site = (SolarWinds.Orion.IpSla.Data.Site)DataBinder.Eval(repeaterItem.DataItem, "Site");
                this.operationNumbersSavers.Add(new OperationNumbersSaver(site.ID, textBox, WizardData));
                if (!IsPostBack)
                {
                    textBox.Text = DataBinder.Eval(repeaterItem.DataItem, "Numbers") as string ?? string.Empty;
                }
                break;
        }
    }

    private class OperationNumbersSaver
    {
        private static readonly char[] NumberSeparators = new[] {',', '\n', '\r'};

        private readonly int siteId;
        private readonly TextBox textBox;
        private readonly OperationsWizardData data;

        public OperationNumbersSaver(int siteId, TextBox textBox, OperationsWizardData data)
        {
            if (textBox == null)
                throw new ArgumentNullException("textBox");
            if (data == null)
                throw new ArgumentNullException("data");

            this.siteId = siteId;
            this.textBox = textBox;
            this.data = data;
        }

        public void Save()
        {
            var numberStringList = this.textBox.Text;
            try
            {
                var numbers = SkipDuplicatesInOrderedEnumerable(
                    from s in numberStringList
                        .Replace("\r\n", "\n")
                        .Split(NumberSeparators, StringSplitOptions.RemoveEmptyEntries)
                    let sn = s.Trim()
                    where sn.Length > 0
                    let n = int.Parse(sn, NumberStyles.AllowLeadingWhite | NumberStyles.AllowTrailingWhite)
                    select n
                    ).ToArray();

                var pair = new KeyValuePair<string, int[]>(numberStringList, numbers);
                data.ExplicitOperationNumbersToDiscover[this.siteId] = pair;
            }
            catch (Exception ex)
            {
                Log.Error("Cannot parse operation numbers:\n" + this.textBox.Text, ex);
            }
        }

        private static IEnumerable<int> SkipDuplicatesInOrderedEnumerable(IEnumerable<int> input)
        {
            var enumerator = input.GetEnumerator();
            if (!enumerator.MoveNext())
                yield break;

            int value;
            yield return value = enumerator.Current;

            while (enumerator.MoveNext())
            {
                var value2 = enumerator.Current;
                if (value != value2)
                {
                    yield return value = value2;
                }
            }
        }
    }
}
