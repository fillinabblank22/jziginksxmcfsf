﻿using System;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Web.Wizards;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Data.OperationParameters;
using System.Net;

public partial class Orion_Voip_Controls_Wizards_Operations_SelectTargetExplicitWebWizardPartControl : WizardFlowPartControl<OperationsWizardData>
{
    //private const string REGEX_URL = @"(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?";
    private const string REGEX_IP_ADDRESS = @"^(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])$";

    //private const string REGEX_FQDN = @"^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}$";

    private const string REGEX_HOST_NAME_OR_IP_ADDRESS = @"^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)*([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?)$";

    private SelectTargetExplicitWebWizardPart m_part;

    protected string Description
    {
        get
        {
            return GetTextForOperationType(
                Resources.VNQMWebContent.VNQMWEBCODE_VB1_57,
                Resources.VNQMWebContent.VNQMWEBCODE_VB1_58,
                Resources.VNQMWebContent.VNQMWEBCODE_VB1_59,
                Resources.VNQMWebContent.VNQMWEBCODE_VB1_60);
        }
    }

    protected string IpAddressLabel
    {
        get
        {
            return GetTextForOperationType(
                Resources.VNQMWebContent.VNQMWEBCODE_VB1_61,
                Resources.VNQMWebContent.VNQMWEBCODE_VB1_62,
                "",
                "");
        }
    }

    protected string ExampleUrl
    {
        get
        {
            switch (this.WizardData.OperationType)
            {
                case OperationType.Ftp:
                    return "ftp://username:password@ftp.yourhost.com/file.txt";
                case OperationType.Http:
                    return "http://www.yourhost.com/";
                default:
                    return string.Empty;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.ipAddressRegularExpressionValidator.ValidationExpression = REGEX_IP_ADDRESS;
        this.dnsResolveValueRegularExpressionValidator.ValidationExpression = REGEX_HOST_NAME_OR_IP_ADDRESS;
    }

    private string GetTextForOperationType(string dhcpText, string dnsText, string ftpText, string httpText)
    {
        OperationType operationType = OperationsWizardSession.Current.Data.OperationType.Value;
        switch (operationType)
        {
            case OperationType.Dhcp:
                return dhcpText;
            case OperationType.Dns:
                return dnsText;
            case OperationType.Ftp:
                return ftpText;
            case OperationType.Http:
                return httpText;
            default:
                throw new NotSupportedException(string.Format("Operation type not supported {0}", operationType));
        }
    }

    public override bool Initialize(OperationsWizardData data)
    {
        if (!IsPostBack)
        {
            SetVisibility(OperationsWizardSession.Current.Data.OperationType.Value);
            FillInfo(OperationsWizardSession.Current.Data.OperationType.Value, OperationsWizardSession.Current.Data.DefaultOperationParameters);
        }
        return true;
    }

    private void SetVisibility(OperationType operationType)
    {
        this.urlRow.Visible = (operationType == OperationType.Http || operationType == OperationType.Ftp);
        this.ipAddressRow.Visible = (operationType == OperationType.Dhcp || operationType == OperationType.Dns);
        this.dnsResolveValueRow.Visible = (operationType == OperationType.Dns);
    }

    public override bool ModifyData(OperationsWizardData data)
    {
        if (!this.Page.IsValid)
        {
            return false;
        }
        OperationType operationType = OperationsWizardSession.Current.Data.OperationType.Value;
        OperationParameters parameters = OperationsWizardSession.Current.Data.DefaultOperationParameters;

        switch (operationType)
        {
            case OperationType.Dhcp:
                TargetIpSlaOperationParameters targetParameters = parameters as TargetIpSlaOperationParameters;
                if (targetParameters != null)
                {
                    targetParameters.TargetAddress = this.ipAdressTextBox.Text;
                }
                break;
            case OperationType.Dns:
                DnsIpSlaOperationParameters dnsParameters = parameters as DnsIpSlaOperationParameters;
                if (dnsParameters != null)
                {
                    dnsParameters.DnsServer = IPAddress.Parse(this.ipAdressTextBox.Text);
                    dnsParameters.NameToResolve = this.dnsResolveValueTextBox.Text;
                }
                break;
            case OperationType.Ftp:
                FtpIpSlaOperationParameters ftpParameters = parameters as FtpIpSlaOperationParameters;
                if (ftpParameters != null)
                {
                    Uri uri;
                    ftpParameters.Url = (Uri.TryCreate(this.urlTextBox.Text, UriKind.Absolute, out uri) ?
                                            uri.AbsoluteUri :
                                            this.urlTextBox.Text);
                }
                break;
            case OperationType.Http:
                HttpIpSlaOperationParameters httpParameters = parameters as HttpIpSlaOperationParameters;
                if (httpParameters != null)
                {
                    Uri uri;
                    httpParameters.Url = (Uri.TryCreate(this.urlTextBox.Text, UriKind.Absolute, out uri) ?
                                            uri.AbsoluteUri :
                                            this.urlTextBox.Text);
                }
                break;
        }


        return true;
    }

    void FillInfo(OperationType operationType, OperationParameters parameters)
    {
        switch (operationType)
        {
            case OperationType.Dhcp:
                TargetIpSlaOperationParameters targetParameters = parameters as TargetIpSlaOperationParameters;
                if (targetParameters != null && targetParameters.TargetAddress != null)
                {
                    this.ipAdressTextBox.Text = targetParameters.TargetAddress;
                }
                break;
            case OperationType.Dns:
                DnsIpSlaOperationParameters dnsParameters = parameters as DnsIpSlaOperationParameters;
                if (dnsParameters != null && dnsParameters.DnsServer != null)
                {
                    this.ipAdressTextBox.Text = dnsParameters.DnsServer.ToString();
                    this.dnsResolveValueTextBox.Text = dnsParameters.NameToResolve;
                }
                break;
            case OperationType.Ftp:
                FtpIpSlaOperationParameters ftpParameters = parameters as FtpIpSlaOperationParameters;
                if (ftpParameters != null)
                {
                    this.urlTextBox.Text = ftpParameters.Url;
                }
                break;
            case OperationType.Http:
                HttpIpSlaOperationParameters httpParameters = parameters as HttpIpSlaOperationParameters;
                if (httpParameters != null)
                {
                    this.urlTextBox.Text = httpParameters.Url;
                }
                break;
        }
    }

    protected void UrlTextBox_ServerValidate(object source, ServerValidateEventArgs args)
    {
        Uri uri;
        args.IsValid = Uri.TryCreate(args.Value, UriKind.Absolute, out uri);
    }
}