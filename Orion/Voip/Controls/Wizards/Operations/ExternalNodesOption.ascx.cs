﻿using System;
using System.Web.UI;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;

public partial class Orion_Voip_Controls_Wizards_Operations_ExternalNodesOption : System.Web.UI.UserControl
{
    public bool SingleNode { get; set; }

    public bool ExternalNodesSelected
    {
        get { return OperationsWizardSession.Current.Data.UseExternalNodes; }
    }

    public event EventHandler ExternalNodesSelectedChanged;

    protected void Page_Load(object sender, EventArgs e)
    {
        ResetRadioButtons();
        this.rbtnYes.Text = SingleNode ? Resources.VNQMWebContent.VNQMWEBCODE_VB1_45 : Resources.VNQMWebContent.VNQMWEBCODE_VB1_46;
        this.rbtnNo.Text = SingleNode ? Resources.VNQMWebContent.VNQMWEBCODE_VB1_47 : Resources.VNQMWebContent.VNQMWEBCODE_VB1_48;
        this.externalNodeList.SingleNode = SingleNode;
        this.externalNodeList.Visible = this.rbtnYes.Checked;
    }

    protected void rbtn_CheckedChanged(object sender, EventArgs e)
    {
        var enable = this.rbtnYes.Checked;

        if (enable && OperationsWizardSession.Current.Data.BiDirectionalPaths)
        {
            if (OperationsWizardSession.Current.Data.PathDefinitionMethod == PathDefinitionMethod.Simple)
            {
                ScriptManager.RegisterStartupScript(Page, GetType(), "ExternalNodesNotAllowed",
                    SingleNode
                        ? String.Format("Ext.MessageBox.alert('{0}', '{1}');", Resources.VNQMWebContent.VNQMWEBCODE_AK1_1, Resources.VNQMWebContent.VNQMWEBCODE_VB1_49)
                        : String.Format("Ext.MessageBox.alert('{0}', '{1}');", Resources.VNQMWebContent.VNQMWEBCODE_AK1_1, Resources.VNQMWebContent.VNQMWEBCODE_VB1_50)
                    , true);
                enable = false;
            }
        }

        if (enable != OperationsWizardSession.Current.Data.UseExternalNodes)
        {
            this.externalNodeList.Visible = enable;
            OperationsWizardSession.Current.Data.UseExternalNodes = enable;
            RaiseExternalNodesSelectedChanged();
        }
        ResetRadioButtons();
    }

    public void SaveExternalNodes()
    {
        if (OperationsWizardSession.Current.Data.UseExternalNodes)
        {
            this.externalNodeList.SaveValues();
        }
    }

    private void ResetRadioButtons()
    {
        this.rbtnNo.Checked = !(this.rbtnYes.Checked = OperationsWizardSession.Current.Data.UseExternalNodes);
    }

    private void RaiseExternalNodesSelectedChanged()
    {
        var handlers = ExternalNodesSelectedChanged;
        if (handlers != null)
            handlers(this, EventArgs.Empty);
    }
}
