﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web.Wizards;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;
using SolarWinds.Orion.IpSla.Web.Admin.Settings;
using SolarWinds.Orion.IpSla.Contracts;
using SolarWinds.Orion.Web.UI;
using ButtonType = SolarWinds.Orion.Web.UI.ButtonType;

public partial class Orion_Voip_Controls_Wizards_Operations_NodeDiscoveryResultsWebWizardPartControl : WizardFlowPartControl<OperationsWizardData>, IPostBackEventHandler
{
    private static readonly Log log = new Log();

    private readonly PlaceHolder phAdditionalWizardButtons;

    protected static readonly string ResultErrorTitle = Resources.VNQMWebContent.VNQMWEBCODE_VB1_63;

    protected static readonly string ResultErrorDescription = String.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_64, "<a href='ManageNodes/AddNodes.aspx'>", "</a>");

    public Orion_Voip_Controls_Wizards_Operations_NodeDiscoveryResultsWebWizardPartControl()
    {
        this.phAdditionalWizardButtons = new PlaceHolder { ID = "phSummaryButtons" };

        var btn = new LocalizableButton
        {
            ID = "btnDiscoveryNext",
            LocalizedText = CommonButtonType.Next,
            ToolTip = Resources.VNQMWebContent.VNQMWEBDATA_VB1_4,
            DisplayType = ButtonType.Primary
        };
        btn.Click += delegate
                         {
                             if (!this.WizardData.OperationSetupAfterDiscovery.HasValue || this.WizardData.OperationSetupAfterDiscovery.Value)
                             {
                                 this.WizardControl.NextStep();
                             }
                             else
                             {
                                 this.WizardControl.WizardSession.Close();
                                 Response.Redirect(this.Page.ResolveUrl(this.WizardControl.ReturnUrl));
                             }
                         };
        this.phAdditionalWizardButtons.Controls.Add(btn);
    }

    protected bool? Value
    {
        get { return this.ViewState["Value"] as bool?; }
        set { this.ViewState["Value"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager sm = ScriptManager.GetCurrent(Page);
        sm.RegisterAsyncPostBackControl(this);

        DoDataBind();

        var addNodesManager = AddNodesProcessManager.GetCurrent(AddNodesProcessUsageContext.StartupWizard);
        int failedCount = addNodesManager.GetResultCountForCapabilities(new NodeIpSlaCapability[] {NodeIpSlaCapability.NoIpSla, NodeIpSlaCapability.NoAccess});
        int readOnlyCount = addNodesManager.GetResultCountForCapabilities(new NodeIpSlaCapability[] {NodeIpSlaCapability.ReadOnly});
        int readWriteCount = addNodesManager.GetResultCountForCapabilities(new NodeIpSlaCapability[] {NodeIpSlaCapability.ReadWrite});

        int errorCount = failedCount;
        //int okCount = readOnlyCount + readWriteCount;
        if(readWriteCount != 0)
        {
            this.litResultMessageCorrect.Text = string.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_65, readWriteCount);
        }
        else
        {
            ErrorMessage.Visible = true;
        }

        this.litResultMessageReadOnly.Text = string.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_66, readOnlyCount);
        this.litResultMessageError.Text = string.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_67, errorCount);

        this.correctAjaxTree.LoadTree(new NodeDiscoveryResultsAjaxTreeService(), new object[] { NodeDiscoveryResultCategory.NoIssues });
        this.readOnlyAjaxTree.LoadTree(new NodeDiscoveryResultsAjaxTreeService(), new object[] { NodeDiscoveryResultCategory.Warning });
        this.errorAjaxTree.LoadTree(new NodeDiscoveryResultsAjaxTreeService(), new object[] { NodeDiscoveryResultCategory.Error });

        this.discoveryCorrectTreeSection.Visible = readWriteCount > 0;
        
        this.discoveryReadOnlyTreeSection.Visible = readOnlyCount > 0;
        this.discoveryResultMessageReadOnly.Visible = readOnlyCount > 0;
        
        this.discoveryErrorTreeSection.Visible = errorCount > 0;
        this.discoveryResultMessageError.Visible = errorCount > 0;

        this.cvSelection.ClientValidationFunction = "$find('" + this.ClientID + "').cvSelection_Validate";
    }

    private void DoDataBind()
    {
        rptSelect.DataSource = PrepareDataTable();
        rptSelect.DataBind();
    }

    public override bool Initialize(OperationsWizardData data)
    {
        if (!IsPostBack)
        {
            this.Value = data.OperationSetupAfterDiscovery;
        }

        return true;
    }

    public override bool ModifyData(OperationsWizardData data)
    {
        if (!this.Value.HasValue)
        {
            return false;
        }

        data.OperationSetupAfterDiscovery = this.Value;

        return true;
    }

    public Control AdditionalWizardButtons
    {
        get { return this.phAdditionalWizardButtons; }
    }

    private DataTable PrepareDataTable()
    {
        var ret = new DataTable();
        ret.Columns.Add(new DataColumn("Value", typeof(bool)));
        ret.Columns.Add(new DataColumn("Selected", typeof(bool)));
        ret.Columns.Add(new DataColumn("ImageUrl"));
        ret.Columns.Add(new DataColumn("Name"));
        ret.Columns.Add(new DataColumn("Description"));
        ret.Columns.Add(new DataColumn("Url"));
        ret.Columns.Add(new DataColumn("UrlText"));

        AddRowToDataTable(ret, true, Resources.VNQMWebContent.VNQMWEBCODE_VB1_68, Resources.VNQMWebContent.VNQMWEBCODE_VB1_69);
        AddRowToDataTable(ret, false, Resources.VNQMWebContent.VNQMWEBCODE_VB1_70, Resources.VNQMWebContent.VNQMWEBCODE_VB1_71);

        return ret;
    }

    private void AddRowToDataTable(DataTable table, bool addOperations, string title, string description)
    {
        //todo image url, url

        table.Rows.Add(new object[]
            {
                addOperations, // 'Value'
                this.Value.HasValue && this.Value == addOperations, // 'Selected'
                String.Empty, // 'ImageUrl'
                title, // 'Name'
                description, // 'Description'
                "#", // 'Url'   TODO url to help
                String.Empty, // 'UrlText'
            });
    }

    protected string GetScript(bool addOperations)
    {
        string script = this.Page.ClientScript.GetPostBackEventReference(this, addOperations.ToString());
        script = script.Replace("\"", "\'");
        script = script.Replace("\'", "\\\'");
        script += ";$find(\\\'" + this.ClientID + "\\\').val=\\\'" + addOperations.ToString() + "\\\';";
        script = "javascript:setTimeout('" + script + "', 0)";
        return script;
    }

    protected void rptSelect_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item)
        {
            // we'd like to have odd items with "alternate" color, instead of even one
            // indicated with 'ListItemType.AlternatingItem'
            ((HtmlTableRow)e.Item.FindControl("row")).Attributes["class"] = "alternate";
        }
    }

    protected override ScriptControlDescriptor GetScriptDescriptor()
    {
        var descriptor = base.GetScriptDescriptor();
        descriptor.Type = "Orion.Voip.Controls.Wizards.Operations.NodeDiscoveryResultsWebWizardPartControl";
        descriptor.AddProperty("Value", this.Value);
        return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        var result = base.GetScriptReferences();
        result = result.Concat(new[] { new ScriptReference(this.ResolveUrl("NodeDiscoveryResultsWebWizardPartControl.js")) });
        return result;
    }

    #region IPostBackEventHandler Members

    void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
    {
        if (string.IsNullOrEmpty(eventArgument))
            return;

        bool continueInOperationSetup;
        if (bool.TryParse(eventArgument, out continueInOperationSetup))
        {
            this.Value = continueInOperationSetup;
            RaiseDataChanged(EventArgs.Empty);
            DoDataBind();
        }
    }

    #endregion
}
