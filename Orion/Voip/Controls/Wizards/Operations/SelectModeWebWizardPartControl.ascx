﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectModeWebWizardPartControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_Operations_SelectModeWebWizardPartControl" %>

<%@ Import Namespace="SolarWinds.Orion.IpSla.Data" %>

<asp:CustomValidator ID="cvSelection" runat="server" />

<div class="ipsla_WP_Title">
<%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_8 %>
</div>
<div class="ipsla_WP_Description">
</div>

<asp:Repeater ID="rptSelectType" runat="server" OnItemDataBound="rptSelectType_OnItemDataBound">
    <HeaderTemplate>
        <table width="100%" cellspacing="0" cellpadding="3" class="Events">
    </HeaderTemplate>
    <ItemTemplate>
        <tr id="row" runat="server">
            <td class="rbtnCell">
                <%-- Table cells for one  table row of SelectType wizard page.
                 Uses html input radio control instead of ASP.Net RadioButton control, becauce of problem with name attribute 
                 (used for radiobuttons grouping) in Repeater - every Repeater item is InamingContainer - so name attribute is 
                 generated unique for each radio
                --%>
                <input id='rbtn<%# ((int)Eval("Value")).ToString() %>'
                    type="radio"
                    name="selectTypeGroup" 
                    value='<%# ((SolarWinds.Orion.IpSla.Web.Wizards.Operations.WizardMode)Eval("Value")).ToString() %>'
                    <%# (bool)Eval("Selected") ? "checked" : string.Empty %>
                    onclick="<%# GetScript((SolarWinds.Orion.IpSla.Web.Wizards.Operations.WizardMode)Eval("Value")) %>" />
            </td>
            <td class="iconCell">
            </td>
            <td class="textCell">
                <div class="ipsla_WP_SubTitle"><%#Eval("Name") %></div>
                <%# Eval("Description") %>
                <asp:HyperLink ID="hlUrl" runat="server"
                    NavigateUrl='<%# Eval("Url") %>'
                    Target="_blank"
                    Visible='<%# !string.IsNullOrEmpty((string)Eval("Url")) %>'><br />
                  <%# Eval("UrlText") %>
                </asp:HyperLink>
            </td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>
