﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Web.Wizards;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;
using System.Web.Script.Serialization;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Data.OperationParameters;
using SolarWinds.Orion.IpSla.Data;

public partial class Orion_Voip_Controls_Wizards_Operations_ReviewWebWizardControl : WizardFlowPartControl<OperationsWizardData>
{
    private ReviewWebWizardPart wizardPart;
    private int[] siteIDs;

    protected string NodesStateKey
    {
        get { return "__NODES_" + this.ClientID; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.wizardPart = (ReviewWebWizardPart)OperationsWizardSession.Current.CurrentPart;
        this.wizardPart.AnotherSetupRunning += this.wizardPart_AnotherSetupRunning;
        this.wizardPart.SetupCanceled += this.wizardPart_SetupCanceled;
        this.wizardPart.SetupError += wizardPart_SetupError;

        this.useTagAsNameCheckBox.Attributes["onclick"] = "$find('" + ClientID + "').updateNamesByTag();";
        this.useTagAsNameCheckBox.Checked = OperationsWizardSession.Current.Data.UseTagsForOperationNames;
    }

    protected override void OnUnload(EventArgs e)
    {
        if (this.wizardPart != null)
        {
            this.wizardPart.AnotherSetupRunning -= this.wizardPart_AnotherSetupRunning;
            this.wizardPart.SetupCanceled -= this.wizardPart_SetupCanceled;
            this.wizardPart.SetupError -= wizardPart_SetupError;
        }

        base.OnUnload(e);
    }

    public override bool Initialize(OperationsWizardData data)
    {
        if (data.WizardMode == WizardMode.MonitorExistingOperations && data.OperationDiscovery != null)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "StartOperationDiscovery",
                "Ext.onReady(function(){setTimeout(function(){$find('" + ClientID + "').StartScan();},0);});", true);
            this.operationListArea.Style[HtmlTextWriterStyle.Display] = "none";
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "ReviewOperationDiscoveryResults",
                "Ext.onReady(function(){setTimeout(function(){$find('" + ClientID + "').StartReview();},0);});", true);
            this.operationScanArea.Style[HtmlTextWriterStyle.Display] = "none";
        }
        return true;
    }

    public override bool ModifyData(OperationsWizardData data)
    {
        if (data.WizardMode == WizardMode.MonitorExistingOperations)
        {
            var temporaryIds = LoadNodeIdsClientState(Request[NodesStateKey]);
            data.Operations.RemoveAll(x => !temporaryIds.Contains(x.TemporaryID));

            if (data.UseTagsForOperationNames)
            {
                foreach (var taggedOperation in (from op in data.Operations where !(string.IsNullOrEmpty(op.OperationDefinition.Parameters.Tag)) select op))
                {
                    taggedOperation.OperationDefinition.OperationName = taggedOperation.OperationDefinition.Parameters.Tag;
                }
            }
        }
        return true;
    }

    private void wizardPart_AnotherSetupRunning(object sender, EventArgs e)
    {
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "AnotherSetupRunning",
            "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').wizardControl.OnAnotherSetupRunning();},0);});",
            true);
    }

    private void wizardPart_SetupCanceled(object sender, EventArgs e)
    {
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "SetupCanceled",
            "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').wizardControl.OnSetupCanceled();},0);});",
            true);
    }

    void wizardPart_SetupError(object sender, MessageEventArgs e)
    {
        string message = e.Message;
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "SetupError",
            "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').wizardControl.OnSetupError('" + message + "');},0);});",
            true);

    }

    private OperationDefinitionCollection GetSelectedOperationDefinitions()
    {
        siteIDs = LoadNodeIdsClientState(Request[NodesStateKey]);

        IEnumerable<OperationDefinition> operations =
            from selectedTempId in siteIDs
            join item in OperationsWizardSession.Current.Data.Operations on selectedTempId equals item.TemporaryID
            select item.OperationDefinition;

        return new OperationDefinitionCollection(operations);
    }

    protected void btnOpenEditor_Click(object sender, EventArgs e)
    {
        System.Diagnostics.Debug.WriteLine("btnOpenEditor_Click");

        this.DisplayEditor(true);
        
        this.editOp.Initialize(GetSelectedOperationDefinitions(), false);
    }

    protected void btnSubmitEditor_Click(object sender, EventArgs e)
    {
        System.Diagnostics.Debug.WriteLine("btnSubmitEditor_Click");
        this.Page.Validate();
        if (this.Page.IsValid)
        {
            this.DisplayEditor(false);

            var operations = GetSelectedOperationDefinitions();
            this.editOp.Initialize(operations, true);
            this.editOp.Modify(operations);

            Response.Redirect(Request.RawUrl);
        }
    }

    protected void btnCancelEditor_Click(object sender, EventArgs e)
    {
        this.DisplayEditor(false);
    }

    private void DisplayEditor(bool show)
    {
        this.editDialogPanel.Visible = show;
        this.editDialogLoading.Visible = !show;
    }

    /// <summary>
    /// Deserialize list of NodeIDs which should be removed,
    /// received from client.
    /// </summary>
    /// <param name="field"></param>
    protected int[] LoadNodeIdsClientState(string field)
    {
        var jss = new JavaScriptSerializer();
        return jss.Deserialize<int[]>(field);
    }

    protected string SaveNodesToRemoveState()
    {
        var jss = new JavaScriptSerializer();
        return jss.Serialize(siteIDs);
    }

    protected override void OnPreRender(EventArgs e)
    {
        if (WizardData.WizardMode == WizardMode.MonitorExistingOperations)
        {
            this.operationEditHintsArea.Style[HtmlTextWriterStyle.Display] = "none";
        }
        base.OnPreRender(e);
        Page.ClientScript.RegisterHiddenField(NodesStateKey, SaveNodesToRemoveState());
    }

    #region ScriptControl Behaviour

    protected override ScriptControlDescriptor GetScriptDescriptor()
    {
        ScriptControlDescriptor descriptor = base.GetScriptDescriptor();
        descriptor.Type = "Orion.Voip.Controls.Wizards.Operations.ReviewWebWizardControl";
        descriptor.AddElementProperty("gridArea", this.gridArea.ClientID);
        descriptor.AddElementProperty("gridAreaCaption", this.gridAreaCaption.ClientID);
        descriptor.AddElementProperty("btnOpenEditor", this.btnOpenEditor.ClientID);
        descriptor.AddElementProperty("btnSubmitEditor", this.btnSubmitEditor.ClientID);
        descriptor.AddElementProperty("btnCancelEditor", this.btnCancelEditor.ClientID);
        descriptor.AddProperty("editOpID", this.editOp.ClientID);
        descriptor.AddElementProperty("nodesStateField", this.NodesStateKey);
        descriptor.AddProperty("isMonitorMode", WizardData.WizardMode == WizardMode.MonitorExistingOperations);
        var operationDiscovery = WizardData.OperationDiscovery;
        descriptor.AddProperty("nodesDone", operationDiscovery != null ? operationDiscovery.TotalNodes - operationDiscovery.PendingNodes : 0);
        descriptor.AddProperty("nodesTotal", operationDiscovery != null ? operationDiscovery.TotalNodes : 0);
        descriptor.AddProperty("progressCurrent", operationDiscovery != null ? operationDiscovery.ProgressCurrent : 0);
        descriptor.AddProperty("progressTotal", operationDiscovery != null ? operationDiscovery.ProgressTotal : 1);
        descriptor.AddElementProperty("operationScanArea", this.operationScanArea.ClientID);
        descriptor.AddComponentProperty("progressControl", this.ProgressControl.ClientID);
        descriptor.AddElementProperty("useTagAsNameCheckBox", this.useTagAsNameCheckBox.ClientID);
        descriptor.AddElementProperty("operationListArea", this.operationListArea.ClientID);
        descriptor.AddElementProperty("operationDiscoveryExclusionsArea", this.operationDiscoveryExclusionsArea.ClientID);
        descriptor.AddElementProperty("operationsNotFoundArea", this.operationsNotFoundArea.ClientID);
        descriptor.AddElementProperty("operationsNotFoundList", this.operationsNotFoundList.ClientID);
        descriptor.AddElementProperty("operationsMonitoredArea", this.operationsMonitoredArea.ClientID);
        descriptor.AddElementProperty("operationsMonitoredList", this.operationsMonitoredList.ClientID);
        descriptor.AddElementProperty("operationsAutomaticArea", this.operationsAutomaticArea.ClientID);
        descriptor.AddElementProperty("operationsAutomaticList", this.operationsAutomaticList.ClientID);
        return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        var result = base.GetScriptReferences();
        result = result.Concat(new[] { new ScriptReference(this.ResolveUrl("ReviewWebWizardControl.js")) });
        return result;
    }

    #endregion
}
