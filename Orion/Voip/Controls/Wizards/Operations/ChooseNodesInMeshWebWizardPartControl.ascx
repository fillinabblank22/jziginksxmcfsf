﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChooseNodesInMeshWebWizardPartControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_Operations_ChooseNodesInMeshWebWizardPartControl" %>
<%@ Register src="ChooseNodesTreeControl.ascx" tagname="ChooseNodesTreeControl" tagprefix="ipsla" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web.Wizards.Operations" %>
<%@ Register src="../../../Controls/NodeGroupingSelector.ascx" tagname="NodeGroupingSelector" tagprefix="ipsla" %>
<%@ Register src="~/Orion/Voip/Controls/Wizards/Operations/CredentialsCheckControl.ascx" tagname="CredentialsCheckControl" tagprefix="ipsla" %>

<div class="ipsla_WP_Title">
  <% if (this.WizardData.WizardMode != WizardMode.MonitorExistingOperations)
     { %>
     <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_106, SolarWinds.Orion.IpSla.Web.Wizards.Operations.OperationsWizardSession.Current.Data.OperationTypeName)%>
  <% } else { %>
  <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_107, SolarWinds.Orion.IpSla.Web.Wizards.Operations.OperationsWizardSession.Current.Data.OperationTypeName)%>
  <% } %>
</div>
<div class="ipsla_WP_Description">
<%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_108, SolarWinds.Orion.IpSla.Web.Wizards.Operations.OperationsWizardSession.Current.Data.OperationTypeName)%>
</div>
<div class="ipsla_WP_Description">
<%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_109, "<a href=\"ManageNodes/AddNodes.aspx\">", "</a>")%>
</div>
<div class="ipsla_WP_Description">
<%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_110 %>
</div>

<div class="TreeGroupBy">
<div style="padding-bottom: 5px"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_29 %></div>
<ipsla:NodeGroupingSelector ID="groupBySelector" runat="server" OnSelectedValueChanged="groupBySelector_SelectedValueChanged" IncludeSiteProperties="true"/>
</div>

<div class="TreeContent">
    <ipsla:ChooseNodesTreeControl ID="MeshChooseNodesTreeControl" runat="server">
        <WebService Path="~/Orion/Voip/Controls/Wizards/Operations/ChooseNodesAjaxTreeService.asmx" />
    </ipsla:ChooseNodesTreeControl>
</div>

<div id="operationNexusArea" runat="server" class="ipsla_WP_Description">
    <table border="0" cellpadding="0" cellspacing="5px" style="background-color: #FFF7CD;">
        <tr>
            <td valign="middle" style="text-align: center; width: 32px;">
                <img src="/orion/voip/images/lightbulb_important_text.gif" alt="<%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_69 %>" />
            </td>
            <td valign="middle">
                <div>
                    <%= String.Format( Resources.VNQMWebContent.VNQMWEBDATA_VB1_221, String.Format("<a href={0} >{1}</a>", Resources.VNQMWebContent.VNQMWEBDATA_VB1_222, Resources.VNQMWebContent.VNQMWEBCODE_VB1_136))%>
                </div>
            </td>
        </tr>
    </table>
</div>

<ipsla:CredentialsCheckControl ID="CredentialsCheckControl" runat="server" />
