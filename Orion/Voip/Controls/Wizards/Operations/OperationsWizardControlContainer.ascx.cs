﻿using System;
using System.Web.UI;
using SolarWinds.Orion.IpSla.Web.UI;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;
using SolarWinds.Orion.IpSla.Web.Wizards;

public partial class Orion_Voip_Controls_Wizards_Operations_OperationsWizardControlContainer : System.Web.UI.UserControl
{
    public string ReturnUrl
    {
        get { return this.wizardControl.ReturnUrl; }
        set { this.wizardControl.ReturnUrl = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.wizardControl.WizardSession = OperationsWizardSession.Current;
        this.wizardControl.WizardPartControlFactory += this.GetEditorControl;
        this.wizardControl.Initialize();

        var session = OperationsWizardSession.Current;
        var currentPart = session.CurrentPart;
        System.Diagnostics.Debug.Assert(currentPart != null);

        var operationsWizardData = session.Data;

        if (operationsWizardData != null && operationsWizardData.WizardMode == WizardMode.MonitorExistingOperations)
        {
            this.ibtnCreateOperations.Text = Resources.VNQMWebContent.VNQMWEBCODE_VB1_1;
            this.ibtnCreateOperations.ToolTip = Resources.VNQMWebContent.VNQMWEBCODE_VB1_1;
            this.ibtnCreateOperations.OnClientClick = "return $find('" + this.wizardControl.ClientID + "').OnOperationsAdding();";

            if (operationsWizardData.MonitorExplicitOperationNumbers == true
                ? currentPart == MonitorEnterNumbersWebWizardPart.Instance
                : currentPart == MonitorSelectNodesWebWizardPart.Instance)
            {
                this.ibtnNext.OnClientClick = "if (CheckDemoMode()) return false;";
            }
        }
        else
        {
            this.ibtnCreateOperations.Text = Resources.VNQMWebContent.VNQMWEBCODE_VB1_2;
            this.ibtnCreateOperations.ToolTip = Resources.VNQMWebContent.VNQMWEBCODE_VB1_2;
            this.ibtnCreateOperations.OnClientClick = "if (CheckDemoMode()) return false;";
        }

        if (currentPart.WarnBeforeCancel)
        {
            this.ibtnCancel.OnClientClick = "$find('" + this.wizardControl.ClientID + "').CancelWizard(function(){" + this.Page.ClientScript.GetPostBackEventReference(this.ibtnCancel, "Cancel") + ";});return false;";
        }

        var isLast = !currentPart.GetNext(session.Data).GetEnumerator().MoveNext();

        this.ibtnNext.Visible = (!isLast && currentPart.AllowMoveNext && (currentPart != NodeDiscoveryWebWizardPart.Instance));
        this.ibtnPrev.Visible = (currentPart != session.InitialPart && currentPart.AllowMoveBack);
        this.ibtnCreateOperations.Visible = (currentPart == ReviewWebWizardPart.Instance);
        this.ibtnCancel.Visible = currentPart.AllowCancel;
        this.ibtnStartIpSlaDiscovery.Visible = (currentPart == NodeDiscoveryWebWizardPart.Instance);
    }

    protected string WizardTitle
    {
        get
        {
            var data = (OperationsWizardData)this.wizardControl.WizardSession.Data;
            switch (data.Flow)
            {
                case OperationsWizardFlow.OperationSetup:
                    return Resources.VNQMWebContent.VNQMWEBCODE_VB1_3;
                case OperationsWizardFlow.GettingStarted:
                case OperationsWizardFlow.NodeDiscovery:
                    return Resources.VNQMWebContent.VNQMWEBCODE_VB1_4;
                default:
                    return Resources.VNQMWebContent.VNQMWEBCODE_VB1_5;
            }
        }
    }

    protected void wizardControl_ScriptDescriptorInitializing(object sender, ScriptControlDescriptorEventArgs e)
    {
        e.Descriptor.Type = "Orion.Voip.Controls.Wizards.Operations.OperationsWizardControlContainer";
        e.Descriptor.AddElementProperty("btnNext", this.btnNext.ClientID);
    }

    protected void wizardControl_ScriptReferencesRequired(object sender, ScriptReferencesEventArgs e)
    {
        e.AddReference(new ScriptReference(this.ResolveUrl("OperationsWizardControlContainer.js")));
    }

    protected void ibtnPrev_Click(object sender, EventArgs e)
    {
        this.wizardControl.PrevStep();
    }

    protected void ibtnNext_Click(object sender, EventArgs e)
    {
        this.wizardControl.NextStep();
    }

    protected void ibtnCancel_Click(object sender, EventArgs e)
    {
        this.wizardControl.AbortWizard();
        Response.Redirect(this.Page.ResolveUrl(this.ReturnUrl));
    }

    protected void ibtnCreateOperations_Click(object sender, EventArgs e)
    {
        this.wizardControl.NextStep();
    }

    protected void ibtnStartIpSlaDiscovery_Click(object sender, EventArgs e)
    {
        OperationsWizardSession.Current.CurrentPart.DoCustomCommand("StartIpSlaDiscovery");
    }

    protected WizardFlowPartControl GetEditorControl(string wizardPartName)
    {
        //todo - move to different class - this is common class for all wizards

        string controlFileName;
        switch (wizardPartName)
        {
            //node discovery
            case NodeDiscoveryWebWizardPart.NAME:
                controlFileName = "NodeDiscoveryWebWizardPartControl.ascx";
                break;
            case NodeDiscoveryResultsWebWizardPart.NAME:
                controlFileName = "NodeDiscoveryResultsWebWizardPartControl.ascx";
                break;

            case SelectModeWebWizardPart.NAME:
                controlFileName = "SelectModeWebWizardPartControl.ascx";
                break;

            case SelectTypeWebWizardPart.NAME:
                //test !!!
                //controlFileName = "SummaryWebWizardControl.ascx";
                controlFileName = "SelectTypeWebWizardPartControl.ascx";
                break;
            case DefinePathsWebWizardPart.NAME:
                controlFileName = "DefinePathsWebWizardPartControl.ascx";
                break;

            //mesh
            case ChooseNodesInMeshWebWizardPart.NAME:
                controlFileName = "ChooseNodesInMeshWebWizardPartControl.ascx";
                break;

            //custom flow, single target flow, hub and spoke flow
            case ChooseNodesInOperationWebWizardPart.NAME:
                controlFileName = "ChooseNodesInOperationWebWizardPartControl.ascx";
                break;
            
            //custom flow
            case AssignOperationsWebWizardPart.NAME:
                controlFileName = "AssignOperationsWebWizardPartControl.ascx";
                break;
            
            //single target flow
            case SelectTargetExplicitWebWizardPart.NAME:
                controlFileName = "SelectTargetExplicitWebWizardPartControl.ascx";
                break;
            
            //hub and spoke flow
            case SelectSourceNodeWebWizardPart.NAME:
                controlFileName = "SelectSourceNodeWebWizardPartControl.ascx";
                break;
            case SelectTargetNodeWebWizardPart.NAME:
                controlFileName = "SelectTargetNodeWebWizardPartControl.ascx";
                break;
            case DefinePropertiesWebWizardPart.NAME:
                controlFileName = "DefinePropertiesWebWizardPartControl.ascx";
                break;
            case ReviewWebWizardPart.NAME:
                controlFileName = "ReviewWebWizardControl.ascx";
                break;
            case SummaryWebWizardPart.NAME:
                controlFileName = "SummaryWebWizardControl.ascx";
                break;

            // monitor existing operations
            case MonitorScanOrEnterWebWizardPart.NAME:
                controlFileName = "MonitorScanOrEnterWebWizardPartControl.ascx";
                break;
            case MonitorSelectNodesWebWizardPart.NAME:
                controlFileName = "MonitorSelectNodesWebWizardPartControl.ascx";
                break;
            case MonitorEnterNumbersWebWizardPart.NAME:
                controlFileName = "MonitorEnterNumbersWebWizardPartControl.ascx";
                break;
            default:
                throw new NotSupportedException(string.Format("Wizard part '{0}' not found", wizardPartName));
        }
        const string path = "~/Orion/Voip/Controls/Wizards/Operations/";
        var wp = (WizardFlowPartControl)Page.LoadControl(path + controlFileName);

        var buttonsProperty = wp.GetType().GetProperty("AdditionalWizardButtons", typeof(Control));
        if (buttonsProperty != null)
        {
            var buttonsGetter = buttonsProperty.GetGetMethod();
            if (buttonsGetter != null)
            {
                var buttons = (Control)buttonsGetter.Invoke(wp, null);
                if (buttons != null)
                {
                    this.phCustomButtons.Controls.Add(buttons);
                }
            }
        }

        return wp;
    }
}
