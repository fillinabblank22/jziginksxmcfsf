﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Web.Wizards;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;
using System.Diagnostics;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;

public partial class Orion_Voip_Controls_Wizards_Operations_ChooseNodesInMeshWebWizardPartControl : WizardFlowPartControl<OperationsWizardData>
{
    private ChooseNodesInMeshWebWizardPart m_cPart;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        MeshChooseNodesTreeControl.SelectedNodesListIdentifier = ChooseNodesInMeshWebWizardPart.CHOOSED_NODES_LIST_KEY;
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        m_cPart = (ChooseNodesInMeshWebWizardPart)OperationsWizardSession.Current.CurrentPart;
        m_cPart.TooManyNodesSelected += this.cPart_TooManyNodesSelected;
        m_cPart.TooFewNodesSelected += this.m_cPart_TooFewNodesSelected;
        m_cPart.LicenseCountExceeded += this.m_cPart_LicenseCountExceeded;
    }

    public override bool Initialize(OperationsWizardData data)
    {
        if (IpSlaOperationHelper.IsCliBased(data.OperationType.Value))
        {
            this.CredentialsCheckControl.WindowTitle = Resources.VNQMWebContent.VNQMWEBCODE_VB1_33;
            this.CredentialsCheckControl.WindowMessage = Resources.VNQMWebContent.VNQMWEBCODE_VB1_34;
        }
        else
        {
            this.CredentialsCheckControl.WindowTitle = Resources.VNQMWebContent.VNQMWEBCODE_VB1_35;
            this.CredentialsCheckControl.WindowMessage = Resources.VNQMWebContent.VNQMWEBCODE_VB1_36;
        }

        RefreshTree(this.groupBySelector.SelectedValue);
        var nodeList = SitesDAL.Instance.GetAllGroupingTypes("MachineType");
        this.operationNexusArea.Visible = nodeList.Keys.Any(NodeDeviceHelper.IsCiscoNexusMachineType);

        return true;
    }

    public override bool ModifyData(OperationsWizardData data)
    {
        return true;
    }

    void cPart_TooManyNodesSelected(object sender, MessageEventArgs args)
    {
        string message = args.Message;
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "TooManyNodesSelected",
            "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').OnTooManyNodesSelected('" + message + "');},0);});",
            true);
        RefreshTree(this.groupBySelector.SelectedValue);
    }

    void m_cPart_TooFewNodesSelected(object sender, EventArgs e)
    {
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "TooFewNodesSelected",
            "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').OnTooFewNodesSelected();},0);});",
            true);
        RefreshTree(this.groupBySelector.SelectedValue);
    }

    void m_cPart_LicenseCountExceeded(object sender, MessageEventArgs args)
    {
        string message = args.Message;
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "LicenseCountExceeded",
            "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').OnLicenseCountExceeded('" + message + "');},0);});",
            true);
        RefreshTree(this.groupBySelector.SelectedValue);
    }

    protected void groupBySelector_SelectedValueChanged(object sender, EventArgs e)
    {
        RefreshTree(this.groupBySelector.SelectedValue);
    }

    public void RefreshTree(string groupBy)
    {
        MeshChooseNodesTreeControl.RefreshTree(groupBy, new FilterChooseNodesAjaxTreeService());
    }

    protected override void OnUnload(EventArgs e)
    {
        if (m_cPart != null)
        {
            m_cPart.TooManyNodesSelected -= this.cPart_TooManyNodesSelected;
            m_cPart.TooFewNodesSelected -= this.m_cPart_TooFewNodesSelected;
        }
        else
        {
            Debug.WriteLine("m_cPart is null !!!");
        }
        base.OnUnload(e);
    }

    protected override ScriptControlDescriptor GetScriptDescriptor()
    {
        var descriptor = base.GetScriptDescriptor();
        descriptor.Type = "Orion.Voip.Controls.Wizards.Operations.ChooseNodesInMeshWebWizardPartControl";
        return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        var result = base.GetScriptReferences();
        result = result.Concat(new[] { new ScriptReference(this.ResolveUrl("ChooseNodesInMeshWebWizardPartControl.js")) });
        return result;
    }

}
