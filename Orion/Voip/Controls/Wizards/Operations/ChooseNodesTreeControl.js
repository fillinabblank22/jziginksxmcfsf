﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Voip.Controls.Wizards.Operations");

Orion.Voip.Controls.Wizards.Operations.ChooseNodesTreeControl = function(element) {
    Orion.Voip.Controls.Wizards.Operations.ChooseNodesTreeControl.initializeBase(this, [element]);
}

Orion.Voip.Controls.Wizards.Operations.ChooseNodesTreeControl.prototype = {

    initialize: function() {
        Orion.Voip.Controls.Wizards.Operations.ChooseNodesTreeControl.callBaseMethod(this, 'initialize');

        // Add custom initialization here
    },
    dispose: function() {
        //Add custom dispose actions here
        Orion.Voip.Controls.Wizards.Operations.ChooseNodesTreeControl.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Controls.Wizards.Operations.ChooseNodesTreeControl.registerClass('Orion.Voip.Controls.Wizards.Operations.ChooseNodesTreeControl', SolarWinds.Orion.IpSla.Web.AjaxTree.SelectableNodesAjaxTreeControl);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
