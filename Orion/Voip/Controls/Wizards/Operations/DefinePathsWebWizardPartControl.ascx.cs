﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;
using SolarWinds.Orion.IpSla.Web.Wizards;

public partial class Orion_Voip_Controls_Wizards_Operations_DefinePathsWebWizardPartControl : WizardFlowPartControl<OperationsWizardData>, IPostBackEventHandler
{
    protected PathDefinitionMethod? Value
    {
        get { return this.ViewState["Value"] as PathDefinitionMethod?; }
        set { this.ViewState["Value"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager sm = ScriptManager.GetCurrent(Page);
        sm.RegisterAsyncPostBackControl(this);

        this.advancedBtn.ElementID = AdvancedSectionElementID;

        this.cvSelection.ClientValidationFunction = "$find('" + this.ClientID + "').cvSelection_Validate";
    }

    public override bool Initialize(OperationsWizardData data)
    {
        if (!IsPostBack)
        {
            var pathMethod = data.PathDefinitionMethod;

            Value = pathMethod;

            this.advancedBtn.Collapsed = !pathMethod.HasValue || pathMethod.Value == PathDefinitionMethod.Simple;
        }

        return true;
    }

    public override bool ModifyData(OperationsWizardData data)
    {
        if (!this.Value.HasValue)
        {
            return false;
        }
        data.PathDefinitionMethod = this.Value;
        return true;
    }

    protected string AdvancedSectionElementID
    {
        get { return ClientID + "_advancedSection"; }
    }

    protected string GetScript(PathDefinitionMethod pm)
    {
        string script = this.Page.ClientScript.GetPostBackEventReference(this, pm.ToString());
        script = script.Replace("\"", "\'");
        script = script.Replace("\'", "\\\'");
        script += ";$find(\\\'" + this.ClientID + "\\\').val=\\\'" + pm.ToString() + "\\\';";
        return "javascript:setTimeout('" + script + "', 0)";
    }

    protected override ScriptControlDescriptor GetScriptDescriptor()
    {
        var descriptor = base.GetScriptDescriptor();
        descriptor.Type = "Orion.Voip.Controls.Wizards.Operations.DefinePathsWebWizardPartControl";
        descriptor.AddProperty("Value", this.Value);
        return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        var result = base.GetScriptReferences();
        result = result.Concat(new[] { new ScriptReference(this.ResolveUrl("DefinePathsWebWizardPartControl.js")) });
        return result;
    }

    #region IPostBackEventHandler Members

    void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
    {
        if (string.IsNullOrEmpty(eventArgument))
            return;

        if (Enum.IsDefined(typeof(PathDefinitionMethod), eventArgument))
        {
            this.Value = (PathDefinitionMethod)Enum.Parse(typeof(PathDefinitionMethod), eventArgument);
            RaiseDataChanged(EventArgs.Empty);
        }
    }

    #endregion
}
