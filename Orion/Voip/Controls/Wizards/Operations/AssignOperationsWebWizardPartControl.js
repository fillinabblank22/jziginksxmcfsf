﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Voip.Controls.Wizards.Operations");

Orion.Voip.Controls.Wizards.Operations.AssignOperationsWebWizardPartControl = function(element) {
    Orion.Voip.Controls.Wizards.Operations.AssignOperationsWebWizardPartControl.initializeBase(this, [element]);
}

Orion.Voip.Controls.Wizards.Operations.AssignOperationsWebWizardPartControl.prototype = {
    OnTooManyNodesSelected: function(message) {
        Ext.MessageBox.show({
            title: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}",
            icon: Ext.MessageBox.ERROR,
            msg: message,
            closable: false,
            buttons: Ext.MessageBox.OK
        });
    },

    OnLicenseCountExceeded: function(message) {
        Ext.MessageBox.show({
            title: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}",
            icon: Ext.MessageBox.ERROR,
            msg: String.format("@{R=VNQM.Strings;K=VNQMWEBJS_VB1_88;E=js}", message, "<a href='/Orion/Voip/Admin/ManageOperations.aspx'>", "</a>"),
            closable: false,
            buttons: Ext.MessageBox.OK
        });
    },

    OnTooFewNodesSelected: function() {
        Ext.MessageBox.show({
            title: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}",
            icon: Ext.MessageBox.ERROR,
            msg: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_89;E=js}",
            closable: false,
            buttons: Ext.MessageBox.OK
        });
    },
    
    initialize: function() {
        Orion.Voip.Controls.Wizards.Operations.AssignOperationsWebWizardPartControl.callBaseMethod(this, 'initialize');
        
        // Add custom initialization here
    },
    dispose: function() {        
        //Add custom dispose actions here
        Orion.Voip.Controls.Wizards.Operations.AssignOperationsWebWizardPartControl.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Controls.Wizards.Operations.AssignOperationsWebWizardPartControl.registerClass('Orion.Voip.Controls.Wizards.Operations.AssignOperationsWebWizardPartControl', Orion.Voip.Controls.Wizards.WizardFlowPartControl);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
