﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Web.Wizards;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Data.OperationParameters;

public partial class Orion_Voip_Controls_Wizards_Operations_DefinePropertiesWebWizardPartControl : WizardFlowPartControl<OperationsWizardData>
{

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    OperationDefinitionCollection GetOperationDefinitionCollection(OperationsWizardData data)
    {
        return new OperationDefinitionCollection(data.Operations.Select(o => o.OperationDefinition)); 
    }

    public override bool Initialize(OperationsWizardData data)
    {
        if (!IsPostBack)
        {
            editOp.Initialize(this.GetOperationDefinitionCollection(data), false);
        }
        return true;
    }

    public override bool ModifyData(OperationsWizardData data)
    {
        //return OperationProperties.ModifyData(data.OperationParameters);
        editOp.Modify(this.GetOperationDefinitionCollection(data));
        return true;
    }

    protected void RestoreDefaultsImageButton_Click(object sender, EventArgs e)
    {
        DefinePropertiesWebWizardPart.Instance.RestoreDefaults(OperationsWizardSession.Current.Data);
        editOp.Initialize(this.GetOperationDefinitionCollection(OperationsWizardSession.Current.Data), false);
        //if (OperationsWizardSession.Current != null && OperationsWizardSession.Current.Data != null
        //    && OperationsWizardSession.Current.Data.OperationType.HasValue)
        //{
            
        //    OperationsWizardData data = OperationsWizardSession.Current.Data;
        //    //OperationType operationType = data.OperationType.Value;

        //    data.ResetDefaultPropertiesAndThresholds();

        //    //OperationProperties.InitializeChildren(OperationProperties, data.OperationParameters);
        //}
    }

    

}
