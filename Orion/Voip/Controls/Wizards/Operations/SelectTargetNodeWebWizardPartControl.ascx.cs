﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using SolarWinds.Orion.IpSla.Web.AjaxTree;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;
using System.Diagnostics;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web.Wizards;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;

public partial class Orion_Voip_Controls_Wizards_Operations_SelectTargetNodeWebWizardPartControl : WizardFlowPartControl<OperationsWizardData>
{
    private SelectTargetNodeWebWizardPart m_part;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        var data = OperationsWizardSession.Current.Data;
        switch (data.PathDefinitionMethod)
        {
            case PathDefinitionMethod.Simple:
                this.biDirectionalPathsOption.SingleNode = true;
                this.externalNodesOption.SingleNode = true;
                ChooseNodesTree.Disabled = data.UseExternalNodes;
                break;
            default:
                this.biDirectionalPathsOption.Visible = false;
                this.externalNodesOption.Visible = false;
                break;
        }
        ChooseNodesTree.SelectedNodesListIdentifier = SelectTargetNodeWebWizardPart.CHOOSED_NODES_LIST_KEY;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        m_part = (SelectTargetNodeWebWizardPart)OperationsWizardSession.Current.CurrentPart;
        m_part.NoNodeSelected += new EventHandler<MessageEventArgs>(part_NoNodeSelected);
        m_part.TooManyNodesSelected += new EventHandler<MessageEventArgs>(part_TooManyNodesSelected);
        m_part.LicenseCountExceeded += new EventHandler<MessageEventArgs>(part_LicenseCountExceeded);

        RefreshTree(this.groupBySelector.SelectedValue);

        var nodeList = SitesDAL.Instance.GetAllGroupingTypes("MachineType");
        this.operationNexusArea.Visible = nodeList.Keys.Any(NodeDeviceHelper.IsCiscoNexusMachineType);
    }

    public override bool Initialize(OperationsWizardData data)
    {
        if (IpSlaOperationHelper.IsCliBased(data.OperationType.Value))
        {
            this.CredentialsCheckControl.WindowTitle = Resources.VNQMWebContent.VNQMWEBCODE_VB1_33;
            this.CredentialsCheckControl.WindowMessage = Resources.VNQMWebContent.VNQMWEBCODE_VB1_34;
        }
        else
        {
            this.CredentialsCheckControl.WindowTitle = Resources.VNQMWebContent.VNQMWEBCODE_VB1_35;
            this.CredentialsCheckControl.WindowMessage = Resources.VNQMWebContent.VNQMWEBCODE_VB1_36;
        }

        return true;
    }

    public override bool ModifyData(OperationsWizardData data)
    {
        return true;
    }

    public override void SaveControlData(WizardData data)
    {
        this.externalNodesOption.SaveExternalNodes();
    }

    void part_NoNodeSelected(object sender, MessageEventArgs e)
    {
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "NoNodeSelected", "Ext.onReady(NoNodeSelected.showDialog, NoNodeSelected, true);", true);
        //RefreshTree(this.groupBySelector.SelectedValue);
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "NoNodeSelected",
            "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').OnNoNodeSelected('" + e.Message + "');},0);});",
            true);
        RefreshTree(this.groupBySelector.SelectedValue);
    }

    void part_TooManyNodesSelected(object sender, MessageEventArgs e)
    {
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "TooManyNodesSelected",
            "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').OnTooManyNodesSelected('" + e.Message + "');},0);});",
            true);
        RefreshTree(this.groupBySelector.SelectedValue);
    }

    void part_LicenseCountExceeded(object sender, MessageEventArgs e)
    {
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "LicenseCountExceeded",
            "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').OnLicenseCountExceeded('" + e.Message + "');},0);});",
            true);
        RefreshTree(this.groupBySelector.SelectedValue);
    }

    protected void groupBySelector_SelectedValueChanged(object sender, EventArgs e)
    {
        RefreshTree(this.groupBySelector.SelectedValue);
    }

    protected void ExternalNodesOption_ExternalNodesSelectedChanged(object sender, EventArgs e)
    {
        if (OperationsWizardSession.Current.Data.PathDefinitionMethod == PathDefinitionMethod.Simple)
        {
            var externalTarget = OperationsWizardSession.Current.Data.UseExternalNodes;
            var externalTargetStr = externalTarget ? "true" : "false";

            if (externalTarget)
            {
                ScriptManager.RegisterStartupScript(Page, GetType(), "NodeTreeUnselect:",
                    string.Format(CultureInfo.InvariantCulture, "$find('{0}').ChangeCheckedAllRoot(false);",
                        ChooseNodesTree.ClientID),
                    true);
            }
            ScriptManager.RegisterStartupScript(Page, GetType(), "NodeTreeDisable:" + externalTargetStr,
                string.Format(CultureInfo.InvariantCulture, "$find('{0}').set_Disabled({1});",
                    ChooseNodesTree.ClientID, externalTargetStr),
                true);

        }
    }

    public void RefreshTree(string groupBy)
    {
        ChooseNodesTree.RefreshTree(groupBy, new FilterChooseNodesAjaxTreeService());
    }

    protected override void OnUnload(EventArgs e)
    {
        if (m_part != null)
        {
            m_part.NoNodeSelected -= part_NoNodeSelected;
            m_part.TooManyNodesSelected -= part_TooManyNodesSelected;
        }
        else
        {
            Debug.WriteLine("m_cPart is null !!!");
        }
        base.OnUnload(e);
    }

    protected override ScriptControlDescriptor GetScriptDescriptor()
    {
        var descriptor = base.GetScriptDescriptor();
        descriptor.Type = "Orion.Voip.Controls.Wizards.Operations.SelectTargetNodeWebWizardPartControl";
        return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        var result = base.GetScriptReferences();
        result = result.Concat(new[] { new ScriptReference(this.ResolveUrl("SelectTargetNodeWebWizardPartControl.js")) });
        return result;
    }

}
