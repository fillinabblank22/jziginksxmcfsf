﻿// Global vars
var OperationsDataStore;
var OperationsSelectionModel;
var OperationsColumnModel;
var OperationsGrid;

Ext.onReady(function() {

    Ext.QuickTips.init();

    OperationsDataStore = new Ext.data.Store({
        id: 'OperationsDataStore',
        proxy: new Ext.data.HttpProxy({
            url: '/Orion/Voip/Controls/Wizards/Operations/SummaryExtGridDataProvider.ashx',
            method: 'POST'
        }),
        baseParams: { task: "select" }, // this parameter is passed for any HTTP request
        reader: new Ext.data.JsonReader({
            root: 'results',
            totalProperty: 'total',
            id: 'id'
        }, [
                        { name: 'OperationID', type: 'int' },
                        { name: 'OperationName', type: 'string' },
                        { name: 'Source', type: 'string' },
                        { name: 'Target', type: 'string' },
                    ]),
        sortInfo: { field: 'OperationName', direction: "ASC" }
    });

    OperationsColumnModel = new Ext.grid.ColumnModel(
                [{
                    header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_7;E=js}',
                    dataIndex: 'OperationName',
                    width: 300,
                    renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                        return Ext.util.Format.htmlDecode(value).replace(/&#39;/g, '\'');
                    }
                }, {
                    header: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_61;E=js}',
                    dataIndex: 'Source',
                    width: 200
                }, {
                    header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_13;E=js}',
                    dataIndex: 'Target',
                    width: 200
}]
            );
    OperationsColumnModel.defaultSortable = true;

    OperationsGrid = new Ext.grid.EditorGridPanel({
        id: 'OperationsGrid',
        height: 200,
        width: 725,
        store: OperationsDataStore,
        cm: OperationsColumnModel,
        enableColLock: false
    });
});