﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeDiscoveryResultsWebWizardPartControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_Operations_NodeDiscoveryResultsWebWizardPartControl" %>
<%@ Register TagPrefix="ipslam" Assembly="SolarWinds.Orion.IpSla.Web" Namespace="SolarWinds.Orion.IpSla.Web.AjaxTree" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Data" %>

<asp:CustomValidator ID="cvSelection" runat="server" />

<div id="discoveryResultMessageCorrect" class="ipsla_WP_Title">
<asp:Literal ID="litResultMessageCorrect" runat="server"></asp:Literal>
</div>
<div id="discoveryCorrectTreeSection" runat="server">
<ipslam:AjaxTreeControl ID="correctAjaxTree" runat="server">
    <WebService Path="~/Orion/Voip/Controls/Wizards/Operations/NodeDiscoveryResultsAjaxTreeService.asmx" />
</ipslam:AjaxTreeControl>
</div>
<div runat="server" ID="ErrorMessage" class="sw-suggestion sw-suggestion-fail" Visible="False" >
    <span class="sw-suggestion-icon"></span>
    <div class="sw-suggestion-title"><%=ResultErrorTitle%></div>
    <%=ResultErrorDescription%>
</div>
<div id="discoveryResultMessageReadOnly" class="ipsla_WP_Title" runat="server">
<asp:Literal ID="litResultMessageReadOnly" runat="server"></asp:Literal>
</div>
<div id="discoveryReadOnlyTreeSection" runat="server">
<div class="ipsla_ExclamationBox">
  <div class="ExclamationBoxBody">
    <div>
      <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_119 %>
    </div>
    <div>
        <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_120, "<a target=\"_blank\" href=\"/Orion/Nodes/Default.aspx\">", "</a>")%>
    </div>
  </div>
</div>
<ipslam:AjaxTreeControl ID="readOnlyAjaxTree" runat="server">
    <WebService Path="~/Orion/Voip/Controls/Wizards/Operations/NodeDiscoveryResultsAjaxTreeService.asmx" />
</ipslam:AjaxTreeControl>
</div>

<div id="discoveryResultMessageError" class="ipsla_WP_Title" runat="server">
<asp:Literal ID="litResultMessageError" runat="server" Text=""></asp:Literal>
</div>
<div id="discoveryErrorTreeSection" runat="server">
<ipslam:AjaxTreeControl ID="errorAjaxTree" runat="server">
    <WebService Path="~/Orion/Voip/Controls/Wizards/Operations/NodeDiscoveryResultsAjaxTreeService.asmx" />
</ipslam:AjaxTreeControl>
</div>

<div class="ipsla_WP_Title">
<%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_121 %>
</div>
<div class="ipsla_WP_Description">
</div>

<asp:Repeater ID="rptSelect" runat="server" OnItemDataBound="rptSelect_OnItemDataBound">
    <HeaderTemplate>
        <table width="100%" cellspacing="0" cellpadding="3" class="Events">
    </HeaderTemplate>
    <ItemTemplate>
        <tr id="row" runat="server">
            <td class="rbtnCell">
                <%-- Table cells for one  table row of SelectType wizard page.
                 Uses html input radio control instead of ASP.Net RadioButton control, becauce of problem with name attribute 
                 (used for radiobuttons grouping) in Repeater - every Repeater item is InamingContainer - so name attribute is 
                 generated unique for each radio
                --%>
                <input id='rbtn<%# ((bool)Eval("Value")).ToString() %>'
                    type="radio"
                    name="selectTypeGroup" 
                    value='<%# ((bool)Eval("Value")).ToString() %>'
                    <%# (bool)Eval("Selected") ? "checked" : string.Empty %>
                    onclick="<%# GetScript((bool)Eval("Value")) %>" />
            </td>
            <td class="textCell">
                <div class="ipsla_WP_SubTitle"><%#Eval("Name") %></div>
                <%# Eval("Description") %>
                <asp:HyperLink ID="hlUrl" runat="server"
                    NavigateUrl='<%# Eval("Url") %>'
                    Visible='<%# !string.IsNullOrEmpty((string)Eval("Url")) %>'>
                  <%# Eval("UrlText") %>
                </asp:HyperLink>
            </td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>
