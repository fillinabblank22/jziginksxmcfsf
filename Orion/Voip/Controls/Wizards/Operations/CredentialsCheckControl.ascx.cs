﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.UI;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;

public partial class Orion_Voip_Controls_Wizards_Operations_CredentialsCheckControl : ScriptUserControlBase, IPostBackEventHandler
{
    public string ListKey { get; set; }

    public event EventHandler Continue;

    public string WindowTitle { get; set; }
    public string WindowMessage { get; set; }
    public string IssueIconUrl { get; set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.ProgressControl.Title = Resources.VNQMWebContent.VNQMWEBCODE_VB1_38;
    }

    protected override void OnPreRender(EventArgs e)
    {
        var credentialsVerifier = CredentialsVerifier;
        if (credentialsVerifier != null)
        {
            this.snmpInfoElement.Visible = credentialsVerifier.Snmp;
            this.cliInfoElement.Visible = credentialsVerifier.Cli;
            ShowProgress();
        }
        base.OnPreRender(e);
    }

    private static OperationsWizardData WizardData
    {
        get
        {
            var session = OperationsWizardSession.Current;
            return session != null ? session.Data : null;
        }
    }

    private static CredentialsVerifier CredentialsVerifier
    {
        get
        {
            var data = WizardData;
            return data != null ? data.CredentialsVerifier : null;
        }
    }

    #region IScriptControl Members
    private ScriptControlDescriptor descriptor;
    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        descriptor = new ScriptControlDescriptor("Orion.Voip.Controls.Wizards.Operations.CredentialsCheckControl", this.ClientID);

        var credentialsVerifier = CredentialsVerifier;
        if (credentialsVerifier != null)
        {
            if (credentialsVerifier.Snmp)
            {
                descriptor.AddComponentProperty("snmpInfo", this.snmpInfo.ClientID);
                descriptor.AddElementProperty("snmpInfoElement", this.snmpInfoElement.ClientID);
            }
            if (credentialsVerifier.Cli)
            {
                descriptor.AddComponentProperty("cliInfo", this.cliInfo.ClientID);
                descriptor.AddElementProperty("cliInfoElement", this.cliInfoElement.ClientID);
            }
        }
        descriptor.AddComponentProperty("progressControl", this.ProgressControl.ClientID);
        descriptor.AddProperty("windowTitle", WindowTitle ?? string.Empty);
        descriptor.AddProperty("windowMessage", WindowMessage ?? string.Empty);
        if (IssueIconUrl != null) descriptor.AddProperty("issueIconUrl", ResolveUrl(IssueIconUrl));

        descriptor.AddScriptProperty("invokeContinuePostback",
            "function() { " + this.Page.ClientScript.GetPostBackEventReference(this, "Continue") + "; }"
            );
        descriptor.AddScriptProperty("invokeCancelPostback",
            "function() { " + this.Page.ClientScript.GetPostBackEventReference(this, "Cancel") + "; }"
            );
        
        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        yield return new ScriptReference { Path = ResolveUrl("CredentialsCheckControl.js") };
    }

    #endregion

    private void ShowProgress()
    {
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ShowProgress",
            "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').ShowProgress();});});",
            true);
    }

    private void ShowCredentialsDialog()
    {
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ShowCredentialsDialog",
            "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').ShowCredentialsDialog();});});",
            true);
    }

    #region IPostBackEventHandler Members

    public void RaisePostBackEvent(string eventArgument)
    {
        switch (eventArgument)
        {
            case "Continue":
                OnContinue();
                break;
            case "Cancel":
                OnCancel();
                break;
        }
    }
    #endregion

    private void OnContinue()
    {
        var parentControl = Parent;
        while (parentControl != null)
        {
            var wizardControl = parentControl as WizardControl;
            if (wizardControl != null)
            {
                wizardControl.NextStep();
                break;
            }
            parentControl = parentControl.Parent;
        }
    }

    private static void OnCancel()
    {
        var credentialsVerifier = OperationsWizardSession.Current.Data.CredentialsVerifier;
        if (credentialsVerifier == null)
            return;

        OperationsWizardSession.Current.Data.CredentialsVerifier = null;
        credentialsVerifier.Close();
    }
}
