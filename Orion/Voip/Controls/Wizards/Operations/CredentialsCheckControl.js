﻿/// <reference name="MicrosoftAjax.js"/>
/// <reference path="~/Orion/js/jquery.js" />
/// <reference path="~/Orion/js/extjs/debug/Ext.js" />
/// <reference path="~/Orion/js/extjs/debug/jquery-bridge.js" />
/// <reference path="~/Orion/js/extjs/debug/ext-all-debug.js" />
/// <reference path="~/Orion/Voip/js/ScriptServiceInvoker.js" />
/// <reference path="~/Orion/Voip/js/ScriptServiceProxy.js" />
/// <reference path="~/Orion/Voip/js/FormMenu.js" />
/// <reference path="~/Orion/Voip/Controls/Wizards/Operations/CheckCredentialsDialog.js" />

Type.registerNamespace("Orion.Voip.Controls.Wizards.Operations");
Orion.Voip.Controls.Wizards.Operations.CredentialsCheckControl = function(element) {
    Orion.Voip.Controls.Wizards.Operations.CredentialsCheckControl.initializeBase(this, [element]);

    this.invokeContinuePostback = null;
    this.invokeCancelPostback = null;

    this.progressWindow = null;
    this.credentialsErrorDialog = null;
    this.statusServiceInvoker = null;

    this.testingCredentialsIssues = null;
    this.cancelationConfirming = false;

    this.usageContext = null;
    this.snmpInfo = null;
    this.snmpInfoElement = null;
    this.cliInfo = null;
    this.cliInfoElement = null;
    this.progressControl = null;
    this.windowTitle = null;
    this.windowMessage = null;
    this.issueIconUrl = null;
}

Orion.Voip.Controls.Wizards.Operations.CredentialsCheckControl.prototype = {

    get_usageContext: function() {
        return this.usageContext;
    },
    set_usageContext: function(value) {
        this.usageContext = value;
    },

    get_snmpInfo: function() {
        return this.snmpInfo;
    },
    set_snmpInfo: function(value) {
        this.snmpInfo = value;
    },

    get_snmpInfoElement: function() {
        return this.snmpInfoElement;
    },
    set_snmpInfoElement: function(value) {
        this.snmpInfoElement = value;
    },

    get_cliInfo: function() {
        return this.cliInfo;
    },
    set_cliInfo: function(value) {
        this.cliInfo = value;
    },

    get_cliInfoElement: function() {
        return this.cliInfoElement;
    },
    set_cliInfoElement: function(value) {
        this.cliInfoElement = value;
    },

    get_progressControl: function() {
        return this.progressControl;
    },
    set_progressControl: function(value) {
        this.progressControl = value;
    },

    ShowProgress: function() {
        if (this.progressWindow == null) {
            this.progressWindow = new Ext.Window({
                title: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_8;E=js}',
                width: 475,
                height: 172,
                closeAction: 'hide',
                resizable: false,
                modal: true,
                closable: false,
                bodyCssClass: 'GroupBox ipsla_ProgressBarDiscovery',
                contentEl: 'pbarBody',
                buttons: [{
                    text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_26;E=js}',
                    handler: Function.createDelegate(this, function() {
                        if (!this.cancelationConfirming) {
                            this.cancelationConfirming = true;
                            try {
                                Ext.MessageBox.show({
                                    title: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_9;E=js}",
                                    icon: Ext.MessageBox.QUESTION,
                                    msg: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_10;E=js}',
                                    closable: false,
                                    buttons: Ext.MessageBox.YESNO,
                                    fn: Function.createDelegate(this, function(btn, value) {
                                        this.cancelationConfirming = false;
                                        if (btn == 'yes') {
                                            this.progressWindow.close();
                                            this.OnDone(false);
                                        }
                                        else {
                                            this.OnProgressFinished();
                                        }
                                    })
                                });
                            }
                            catch (ex) {
                                this.cancelationConfirming = false;
                                throw ex;
                            }
                        }
                    })
                }]
            });
        }
        this.statusServiceInvoker = $create(Orion.Voip.js.ScriptServiceInvoker);
        this.statusServiceInvoker.callService(
            Orion.Voip.Controls.Wizards.Operations.CredentialsCheckControlService.GetProgress,
            [], {}, 1000, this,
            Function.createDelegate(this, function(resultParams, context) {
                this.HandleStatus(resultParams);
            }),
            Function.createDelegate(this, function(resultParams, context) {
                this.HandleStatus(resultParams);
                this.testingCredentialsIssues = resultParams.result.ResponseData.CredentialsIssueNodesCount > 0;
                this.OnProgressFinished();
            }),
            Function.createDelegate(this, function(error, context) {
                if (!this.progressWindow.hidden) {
                    Ext.MessageBox.alert('@{R=VNQM.Strings;K=VNQMWEBJS_VB1_11;E=js}', String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_29;E=js}', error.get_message()));
                }
            })
        );
        this.progressWindow.show(this);
        this.progressWindow.center();
    },

    OnProgressFinished: function() {
        if (!this.cancelationConfirming && this.testingCredentialsIssues != null) {
            if (this.testingCredentialsIssues == true) {
                this.progressWindow.close();
                this.ShowCredentialsDialog();
            }
            else {
                this.OnDone(false);
            }
        }
    },

    HandleStatus: function(resultContainer) {
        if (this.progressWindow != null && !this.progressWindow.hidden) {
            var responseData = resultContainer.result.ResponseData;
            var nodesFinished = responseData.NodesCount - responseData.PendingNodesCount;
            var validNodes = nodesFinished - responseData.CredentialsIssueNodesCount;
            var processedInnerHtml = '<div class=\"ipsla_ProgressLegendValue\">' + nodesFinished + '</div><div style="padding-left:15px; white-space: nowrap">&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_VB1_12;E=js}</div>';
            var totalInnerHtml = '<div class=\"ipsla_ProgressLegendValue\">' + responseData.NodesCount + '</div><div style="padding-left:15px; white-space: nowrap">&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_VB1_13;E=js}</div>';
            var value = (responseData.NodesCount !== 0 ? (responseData.NodesCount - responseData.PendingNodesCount) / responseData.NodesCount : 0);
            this.progressControl.UpdateProgress(value, processedInnerHtml, totalInnerHtml);
        }
        else {
            //todo - cleanup
            resultContainer.callServiceAgain = false;
        }
    },

    ShowCredentialsDialog: function() {
        this.credentialsErrorDialog = $create(Orion.Voip.Admin.ManageNodes.WriteCredentialsErrorDialog, {
            snmpInfo: this.snmpInfo,
            snmpInfoElement: this.snmpInfoElement,
            cliInfo: this.cliInfo,
            cliInfoElement: this.cliInfoElement,
            WindowTitle: this.windowTitle,
            Message: this.windowMessage,
            IssueIconUrl: this.issueIconUrl,
            Buttons: [{
                id: 'btnContinue',
                text: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_14;E=js}',
                handler: Function.createDelegate(this, this.OnContinue)
            }, {
                id: 'btnCancel',
                text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_26;E=js}',
                handler: Function.createDelegate(this, this.OnCancel)
            }],
            StoreScriptServiceMethod: Orion.Voip.Controls.Wizards.Operations.CredentialsCheckControlService.GetResults,
            StoreScriptServiceMethodParams: {},
            OnGridUpdateRequest: Function.createDelegate(this, function(nodeIds) {
                var succeedHandler = Function.createDelegate(this, function(resultContainer, callerData) {
                    resultContainer.methodParams[1] = false;
                    this.credentialsErrorDialog.RefreshGridItems(resultContainer.result.ResponseData);
                });

                var serviceInvoker = $create(Orion.Voip.js.ScriptServiceInvoker);
                serviceInvoker = new Orion.Voip.js.ScriptServiceInvoker();
                serviceInvoker.callService(Orion.Voip.Controls.Wizards.Operations.CredentialsCheckControlService.Restart,
                [nodeIds, true], { invoker: serviceInvoker }, 1000, this,
                succeedHandler,
                succeedHandler,
                function(error, callerData) { callerData.cancel(); }
            );
            })
        });
        this.credentialsErrorDialog.ShowDialog();
    },

    OnContinue: function() {
        this.OnCredentialsDialogFinished(false);
    },

    OnCancel: function() {
        this.OnCredentialsDialogFinished(true);
    },

    OnCredentialsDialogFinished: function(canceled) {
        this.credentialsErrorDialog.CloseDialog();
        this.OnDone(canceled);
    },

    OnDone: function(canceled) {
        var postbackFunc = canceled ? this.invokeCancelPostback : this.invokeContinuePostback;
        if (postbackFunc != null) postbackFunc();
    },

    initialize: function() {
        Orion.Voip.Controls.Wizards.Operations.CredentialsCheckControl.callBaseMethod(this, 'initialize');
    },

    dispose: function() {
        //Add custom dispose actions here
        Orion.Voip.Controls.Wizards.Operations.CredentialsCheckControl.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Controls.Wizards.Operations.CredentialsCheckControl.registerClass('Orion.Voip.Controls.Wizards.Operations.CredentialsCheckControl', Sys.UI.Control);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
