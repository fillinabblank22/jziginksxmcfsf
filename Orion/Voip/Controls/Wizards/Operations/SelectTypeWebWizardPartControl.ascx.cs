﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.IpSla.Web.Wizards;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;
using SolarWinds.Orion.IpSla.Data;
using System.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Logging;
using System.Web.UI.HtmlControls;

public partial class Orion_Voip_Controls_Wizards_Operations_SelectTypeWebWizardPartControl : WizardFlowPartControl<OperationsWizardData>, IPostBackEventHandler
{
    private static readonly Log log = new Log();

    private List<DataTable> _operationsByCategory = new List<DataTable>();

    protected OperationType? Value
    {
        get { return this.ViewState["Value"] as OperationType?; }
        set { this.ViewState["Value"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager sm = ScriptManager.GetCurrent(Page);
        sm.RegisterAsyncPostBackControl(this);

        PrepareOperationsByCategory();
        DoDataBind();

        this.cvSelection.ClientValidationFunction = "$find('" + this.ClientID + "').cvSelection_Validate";
    }

    private void PrepareOperationsByCategory()
    {
        DataTable categoryOperations = GetCategoryOperationsDataTable();
        AddRowToDataTable(categoryOperations, OperationType.TcpConnect, Resources.VNQMWebContent.VNQMWEBCODE_VB1_23, HelpFragments.AddingTCPConnect);
        AddRowToDataTable(categoryOperations, OperationType.IcmpEcho, Resources.VNQMWebContent.VNQMWEBCODE_VB1_24, HelpFragments.AddingICMPEcho);
        AddRowToDataTable(categoryOperations, OperationType.IcmpPathEcho, Resources.VNQMWebContent.VNQMWEBCODE_VB1_25, HelpFragments.AddingICMPPathEcho);
        AddRowToDataTable(categoryOperations, OperationType.UdpEcho, Resources.VNQMWebContent.VNQMWEBCODE_VB1_26, HelpFragments.AddingUDPEcho);
        _operationsByCategory.Add(categoryOperations);

        categoryOperations = GetCategoryOperationsDataTable();
        AddRowToDataTable(categoryOperations, OperationType.UdpJitter, Resources.VNQMWebContent.VNQMWEBCODE_VB1_23, HelpFragments.AddingUDPJitter);
        AddRowToDataTable(categoryOperations, OperationType.IcmpPathJitter, Resources.VNQMWebContent.VNQMWEBCODE_VB1_27, HelpFragments.AddingICMPPathJitter);
        AddRowToDataTable(categoryOperations, OperationType.UdpVoipJitter, Resources.VNQMWebContent.VNQMWEBCODE_VB1_28, HelpFragments.AddingVoIPJitter);
        _operationsByCategory.Add(categoryOperations);


        categoryOperations = GetCategoryOperationsDataTable();
        AddRowToDataTable(categoryOperations, OperationType.Dns, Resources.VNQMWebContent.VNQMWEBCODE_VB1_29, HelpFragments.AddingDNS);
        AddRowToDataTable(categoryOperations, OperationType.Dhcp, Resources.VNQMWebContent.VNQMWEBCODE_VB1_30, HelpFragments.AddingDHCP);
        _operationsByCategory.Add(categoryOperations);


        categoryOperations = GetCategoryOperationsDataTable();
        AddRowToDataTable(categoryOperations, OperationType.Ftp, Resources.VNQMWebContent.VNQMWEBCODE_VB1_31, HelpFragments.AddingFTP);
        AddRowToDataTable(categoryOperations, OperationType.Http, Resources.VNQMWebContent.VNQMWEBCODE_VB1_32, HelpFragments.AddingHTTP);
        _operationsByCategory.Add(categoryOperations);


#if false // not to be implemented in Zilker, maybe in next releases
        AddRowToDataTable(ret, OperationType.RtpBasedVoip, "Measures performance using DSPs for determining voice quality.");
        AddRowToDataTable(ret, OperationType.VoipCallSetup, "Measures call quality between two sites.");
#endif
    
    }

    private DataTable GetCategoryOperationsDataTable()
    {
        DataTable categoryOperations = new DataTable();
        categoryOperations.Columns.Add(new DataColumn("Value", typeof(int)));
        categoryOperations.Columns.Add(new DataColumn("Selected", typeof(bool)));
        categoryOperations.Columns.Add(new DataColumn("ImageUrl"));
        categoryOperations.Columns.Add(new DataColumn("Name"));
        categoryOperations.Columns.Add(new DataColumn("Description"));
        categoryOperations.Columns.Add(new DataColumn("Url"));
        categoryOperations.Columns.Add(new DataColumn("UrlText"));
        return categoryOperations;
    }

    private void DoDataBind()
    {
        rptOperationCategory.DataSource = PrepareDataTable();
        rptOperationCategory.DataBind();
    }

    public override bool Initialize(OperationsWizardData data)
    {
        if (!IsPostBack)
        {
            this.Value = data.OperationType;
        }

        return true;
    }

    public override bool ModifyData(OperationsWizardData data)
    {
        if (!this.Value.HasValue)
        {
            return false;
        }

        data.OperationType = this.Value.Value;

        if (OperationTypeHelper.IsTargetLessOperation(data.OperationType.Value))
        {
            data.PathDefinitionMethod = PathDefinitionMethod.TargetExplicit;
        }
        else
        {
            data.PathDefinitionMethod = null;
        }

        return true;
    }

    private DataTable PrepareDataTable()
    {
        DataTable ret = new DataTable();
        ret.Columns.Add(new DataColumn("Title", typeof(string)));
        ret.Columns.Add(new DataColumn("Subtitle", typeof(string)));

        ret.Rows.Add(new object[] { Resources.VNQMWebContent.VNQMWEBCODE_VB1_14, Resources.VNQMWebContent.VNQMWEBCODE_VB1_15 });
        ret.Rows.Add(new object[] { Resources.VNQMWebContent.VNQMWEBCODE_VB1_16, Resources.VNQMWebContent.VNQMWEBCODE_VB1_17 });
        ret.Rows.Add(new object[] { Resources.VNQMWebContent.VNQMWEBCODE_VB1_18, Resources.VNQMWebContent.VNQMWEBCODE_VB1_19 });
        ret.Rows.Add(new object[] { Resources.VNQMWebContent.VNQMWEBCODE_VB1_20, Resources.VNQMWebContent.VNQMWEBCODE_VB1_21 });

        return ret;
    }

    private void AddRowToDataTable(DataTable table, OperationType operationType, string description, string helpFragment)
    {
        string name = GetLocalizedProperty("OperationType", operationType.ToString());

        table.Rows.Add(new object[]
            {
                (int)operationType, // 'Value'
                this.Value == operationType, // 'Selected'
                IpSlaOperationHelper.GetTypeImageUrl(operationType, true), // 'ImageUrl'
                name, // 'Name'
                description, // 'Description'
                HelpLocator.GetHelpUrl(helpFragment),
                string.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_22, name), // 'UrlText'
            });
    }

    protected string GetScript(OperationType ot)
    {
        string script = this.Page.ClientScript.GetPostBackEventReference(this, ot.ToString());
        script = script.Replace("\"", "\'");
        script = script.Replace("\'", "\\\'");
        script += ";$find(\\\'" + this.ClientID + "\\\').val=\\\'" + ot.ToString() + "\\\';";
        script = "javascript:setTimeout('" + script + "', 0)";
        return script;
    }

    protected  void rptOperationCategory_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        DataRowView dataRowView = e.Item.DataItem as DataRowView;

        if (dataRowView != null)
        {
            DataTable nestedData = _operationsByCategory[e.Item.ItemIndex];

            Repeater nestedRepeater = e.Item.FindControl("rptOperations") as Repeater; 
            if (nestedRepeater != null)
            {
                nestedRepeater.DataSource = nestedData; 
                nestedRepeater.DataBind();
            }                
        }
    }

    protected void rptOperations_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item)
        {
            // we'd like to have odd items with "alternate" color, instead of even one
            // indicated with 'ListItemType.AlternatingItem'
            ((HtmlTableRow)e.Item.FindControl("row")).Attributes["class"] = "alternate";
        }
    }

    protected override ScriptControlDescriptor GetScriptDescriptor()
    {
        var descriptor = base.GetScriptDescriptor();
        descriptor.Type = "Orion.Voip.Controls.Wizards.Operations.SelectTypeWebWizardPartControl";
        descriptor.AddProperty("Value", this.Value);
        return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        var result = base.GetScriptReferences();
        result = result.Concat(new[] { new ScriptReference(this.ResolveUrl("SelectTypeWebWizardPartControl.js")) });
        return result;
    }

    #region IPostBackEventHandler Members

    void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
    {
        if (string.IsNullOrEmpty(eventArgument))
            return;

        if (Enum.IsDefined(typeof(OperationType), eventArgument))
        {
            this.Value = (OperationType)Enum.Parse(typeof(OperationType), eventArgument);
            RaiseDataChanged(EventArgs.Empty);
            DoDataBind();
        }
    }

    #endregion

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}
