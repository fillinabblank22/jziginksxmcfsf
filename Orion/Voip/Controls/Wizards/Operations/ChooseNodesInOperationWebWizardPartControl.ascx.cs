﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.Wizards;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;
using System.Diagnostics;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web.DAL;

public partial class Orion_Voip_Controls_Wizards_Operations_ChooseNodesInOperationWebWizardPartControl : WizardFlowPartControl<OperationsWizardData>
{
    private ChooseNodesInOperationWebWizardPart m_part;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ChooseNodesTreeControl.SelectedNodesListIdentifier = ChooseNodesInOperationWebWizardPart.CHOOSED_NODES_LIST_KEY;
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        m_part = (ChooseNodesInOperationWebWizardPart)OperationsWizardSession.Current.CurrentPart;
        m_part.NoNodeSelected += new EventHandler(part_NoNodeSelected);
        m_part.TooFewNodesSelected += new EventHandler(part_TooFewNodesSelected);
        m_part.TooManyNodesSelected += new EventHandler<SolarWinds.Orion.IpSla.Web.MessageEventArgs>(part_TooManyNodesSelected);
        m_part.LicenseCountExceeded += part_LicenseCountExceeded;
        RefreshTree(this.groupBySelector.SelectedValue);
    }

    protected string Title
    {
        get
        {
            switch (this.WizardData.OperationType)
            {
                case OperationType.Dns:
                case OperationType.Ftp:
                case OperationType.Http:
                case OperationType.Dhcp:
                    if (this.WizardData.WizardMode != WizardMode.MonitorExistingOperations)
                        return string.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_51, this.WizardData.OperationTypeName);
                    else
                        return string.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_52, this.WizardData.OperationTypeName);
                default:
                    if (this.WizardData.WizardMode != WizardMode.MonitorExistingOperations)
                        return string.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_53, this.WizardData.OperationTypeName);
                    else
                        return string.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_54, this.WizardData.OperationTypeName);
            }
        }
    }

    protected string Description
    {
        get
        {
            switch (this.WizardData.OperationType)
            {
                case OperationType.Dns:
                case OperationType.Ftp:
                case OperationType.Http:
                case OperationType.Dhcp:
                    return string.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_55, this.WizardData.OperationTypeName);
                default:
                    return string.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_56, this.WizardData.OperationTypeName);
            }
        }
    }

    public override bool Initialize(OperationsWizardData data)
    {
        if (IpSlaOperationHelper.IsCliBased(data.OperationType.Value))
        {
            this.CredentialsCheckControl.WindowTitle = Resources.VNQMWebContent.VNQMWEBCODE_VB1_33;
            this.CredentialsCheckControl.WindowMessage = Resources.VNQMWebContent.VNQMWEBCODE_VB1_34;
        }
        else
        {
            this.CredentialsCheckControl.WindowTitle = Resources.VNQMWebContent.VNQMWEBCODE_VB1_35;
            this.CredentialsCheckControl.WindowMessage = Resources.VNQMWebContent.VNQMWEBCODE_VB1_36;
        }

        if (!IsPostBack)
        {
            if (!data.OperationType.HasValue)
            {
                throw new ArgumentNullException("data.OperationType");
            }
            this.externalNodesOption.Visible = OperationTypeHelper.IsExternalNodePossible(data.OperationType.Value);
        }

        var nodeList = SitesDAL.Instance.GetAllGroupingTypes("MachineType");
        this.operationNexusArea.Visible = nodeList.Keys.Any(NodeDeviceHelper.IsCiscoNexusMachineType);

        return true;
    }

    public override void SaveControlData(WizardData data)
    {
        this.externalNodesOption.SaveExternalNodes();
    }

    public override bool ModifyData(OperationsWizardData data)
    {
        //this.externalNodesOption.SaveExternalNodes();
        return true;
    }

    void part_NoNodeSelected(object sender, EventArgs e)
    {
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "NoNodeSelected",
            "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').OnNoNodeSelected();},0);});",
            true);
        RefreshTree(this.groupBySelector.SelectedValue);
    }

    void part_TooFewNodesSelected(object sender, EventArgs e)
    {
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "TooFewNodesSelected",
            "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').OnTooFewNodesSelected();},0);});",
            true);
        RefreshTree(this.groupBySelector.SelectedValue);
    }

    void part_TooManyNodesSelected(object sender, SolarWinds.Orion.IpSla.Web.MessageEventArgs e)
    {
        string message = e.Message;
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "TooManyNodesSelected",
            "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').OnTooManyNodesSelected('" + message + "');},0);});",
            true);
        RefreshTree(this.groupBySelector.SelectedValue);
    }

    void part_LicenseCountExceeded(object sender, SolarWinds.Orion.IpSla.Web.MessageEventArgs e)
    {
        string message = e.Message;
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "LicenseCountExceeded",
            "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').OnLicenseCountExceeded('" + message + "');},0);});",
            true);
        RefreshTree(this.groupBySelector.SelectedValue);
    }

    protected void groupBySelector_SelectedValueChanged(object sender, EventArgs e)
    {
        RefreshTree(this.groupBySelector.SelectedValue);
    }

    public void RefreshTree(string groupBy)
    {
        ChooseNodesTreeControl.RefreshTree(groupBy, new FilterChooseNodesAjaxTreeService());
    }

    protected override void OnUnload(EventArgs e)
    {
        if (m_part != null)
        {
            m_part.TooFewNodesSelected -= part_TooFewNodesSelected;
            m_part.TooFewNodesSelected -= part_TooFewNodesSelected;
            m_part.TooManyNodesSelected -= part_TooManyNodesSelected;
        }
        else
        {
            Debug.WriteLine("m_part is null !!!");
        }
        base.OnUnload(e);
    }

    protected override ScriptControlDescriptor GetScriptDescriptor()
    {
        var descriptor = base.GetScriptDescriptor();
        descriptor.Type = "Orion.Voip.Controls.Wizards.Operations.ChooseNodesInOperationWebWizardPartControl";
        return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        var result = base.GetScriptReferences();
        result = result.Concat(new[] { new ScriptReference(this.ResolveUrl("ChooseNodesInOperationWebWizardPartControl.js")) });
        return result;
    }

}
