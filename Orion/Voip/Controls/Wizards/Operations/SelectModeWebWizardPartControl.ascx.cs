﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.Wizards;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;
using SolarWinds.Orion.IpSla.Data;
using System.Data;
using SolarWinds.Logging;
using System.Web.UI.HtmlControls;

public partial class Orion_Voip_Controls_Wizards_Operations_SelectModeWebWizardPartControl : WizardFlowPartControl<OperationsWizardData>, IPostBackEventHandler
{
    private static readonly Log log = new Log();

    protected WizardMode? Value
    {
        get { return this.ViewState["Value"] as WizardMode?; }
        set { this.ViewState["Value"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager sm = ScriptManager.GetCurrent(Page);
        sm.RegisterAsyncPostBackControl(this);

        DoDataBind();

        this.cvSelection.ClientValidationFunction = "$find('" + this.ClientID + "').cvSelection_Validate";
    }

    private void DoDataBind()
    {
        rptSelectType.DataSource = PrepareDataTable();
        rptSelectType.DataBind();
    }

    public override bool Initialize(OperationsWizardData data)
    {
        if (!IsPostBack)
        {
            this.Value = data.WizardMode;
        }

        return true;
    }

    public override bool ModifyData(OperationsWizardData data)
    {
        if (!this.Value.HasValue)
        {
            return false;
        }

        data.WizardMode = this.Value.Value;

        return true;
    }

    private DataTable PrepareDataTable()
    {
        DataTable ret = new DataTable();
        ret.Columns.Add(new DataColumn("Value", typeof(int)));
        ret.Columns.Add(new DataColumn("Selected", typeof(bool)));
        ret.Columns.Add(new DataColumn("ImageUrl"));
        ret.Columns.Add(new DataColumn("Name"));
        ret.Columns.Add(new DataColumn("Description"));
        ret.Columns.Add(new DataColumn("Url"));
        ret.Columns.Add(new DataColumn("UrlText"));

        this.AddRowToDataTable(
            ret, 
            WizardMode.AutoCreateOperations,
            Resources.VNQMWebContent.VNQMWEBCODE_VB1_6, 
            "#", 
            null);
        this.AddRowToDataTable(ret, 
            WizardMode.MonitorExistingOperations,
            Resources.VNQMWebContent.VNQMWEBCODE_VB1_7,
            HelpLocator.GetHelpUrl(HelpFragments.AddingManual),
            Resources.VNQMWebContent.VNQMWEBCODE_VB1_8);

        return ret;
    }

    private void AddRowToDataTable(DataTable table, WizardMode wizardMode, string description, string url, string urlText)
    {
        string name = GetLocalizedProperty("WizardMode", wizardMode.ToString());

        table.Rows.Add(new object[]
            {
                (int)wizardMode, // 'Value'
                this.Value == wizardMode, // 'Selected'
                String.Empty, // 'ImageUrl'
                name, // 'Name'
                description, // 'Description'
                url, // 'Url'
                urlText, // 'UrlText'
            });
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }

    protected string GetScript(WizardMode ot)
    {
        string script = this.Page.ClientScript.GetPostBackEventReference(this, ot.ToString());
        script = script.Replace("\"", "\'");
        script = script.Replace("\'", "\\\'");
        script += ";$find(\\\'" + this.ClientID + "\\\').val=\\\'" + ot.ToString() + "\\\';";
        script = "javascript:setTimeout('" + script + "', 0)";
        return script;
    }

    protected void rptSelectType_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item)
        {
            // we'd like to have odd items with "alternate" color, instead of even one
            // indicated with 'ListItemType.AlternatingItem'
            ((HtmlTableRow)e.Item.FindControl("row")).Attributes["class"] = "alternate";
        }
    }

    protected override ScriptControlDescriptor GetScriptDescriptor()
    {
        var descriptor = base.GetScriptDescriptor();
        descriptor.Type = "Orion.Voip.Controls.Wizards.Operations.SelectModeWebWizardPartControl";
        descriptor.AddProperty("Value", this.Value);
        return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        var result = base.GetScriptReferences();
        result = result.Concat(new[] { new ScriptReference(this.ResolveUrl("SelectModeWebWizardPartControl.js")) });
        return result;
    }

    #region IPostBackEventHandler Members

    void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
    {
        if (string.IsNullOrEmpty(eventArgument))
            return;

        if (Enum.IsDefined(typeof(WizardMode), eventArgument))
        {
            this.Value = (WizardMode)Enum.Parse(typeof(WizardMode), eventArgument);
            RaiseDataChanged(EventArgs.Empty);
            DoDataBind();
        }
    }

    #endregion
}
