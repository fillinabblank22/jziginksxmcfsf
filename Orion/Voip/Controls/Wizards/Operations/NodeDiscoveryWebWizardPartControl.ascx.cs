﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.Wizards;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;

public partial class Orion_Voip_Controls_Wizards_Operations_NodeDiscoveryWebWizardPartControl : WizardFlowPartControl<OperationsWizardData>
{
    private static readonly Log log = new Log();

    private NodeDiscoveryWebWizardPart wizardPart;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.wizardPart = (NodeDiscoveryWebWizardPart)OperationsWizardSession.Current.CurrentPart;
        this.wizardPart.CustomCommand += this.wizardPart_CustomCommand;
        this.AddNodesProcessControl.Close += this.AddNodesProcessControl_Close;
    }

    private void wizardPart_CustomCommand(object sender, WizardPartCustomCommandEventArgs e)
    {
        if (e.CommandName == "StartIpSlaDiscovery")
        {
            if (ProfileHelper.DemoRestriction)
            {
                // if Demo Mode active, skip discovery and move forward (the user should be warned on client side)
                this.WizardControl.NextStep();
            }
            else
            {
                this.StartIpSlaDiscovery();
            }
        }
    }

    private void StartIpSlaDiscovery()
    {
        this.AddNodesProcessControl.AddSelectedNodes(null, true);
    }

    private void AddNodesProcessControl_Close(object sender, EventArgs e)
    {
        this.WizardControl.NextStep();
    }


    public override bool Initialize(OperationsWizardData data)
    {
        return true;
    }

    public override bool ModifyData(OperationsWizardData data)
    {
        return true;
    }

    protected override ScriptControlDescriptor GetScriptDescriptor()
    {
        var descriptor = base.GetScriptDescriptor();
        descriptor.Type = "Orion.Voip.Controls.Wizards.Operations.NodeDiscoveryWebWizardPartControl";
        return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        var result = base.GetScriptReferences();
        result = result.Concat(new[] { new ScriptReference(this.ResolveUrl("NodeDiscoveryWebWizardPartControl.js")) });
        return result;
    }

    protected override void OnUnload(EventArgs e)
    {
        if (this.wizardPart != null)
        {
            this.wizardPart.CustomCommand -= wizardPart_CustomCommand;
        }
        else
        {
            log.Debug("wizardPart is null !!!");
        }
        base.OnUnload(e);
    }
}
