﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;
using SolarWinds.Orion.IpSla.Web.Wizards;

public partial class Orion_Voip_Controls_Wizards_Operations_MonitorScanOrEnterWebWizardPartControl : WizardFlowPartControl<OperationsWizardData>, IPostBackEventHandler
{
    protected bool? Value
    {
        get { return ViewState["Value"] as bool?; }
        set { ViewState["Value"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var sm = ScriptManager.GetCurrent(Page);
        sm.RegisterAsyncPostBackControl(this);

        this.advancedBtn.ElementID = AdvancedSectionElementID;

        this.cvSelection.ClientValidationFunction = "$find('" + ClientID + "').cvSelection_Validate";
    }

    public override bool Initialize(OperationsWizardData data)
    {
        var monitorExplicit = data.MonitorExplicitOperationNumbers;

        Value = monitorExplicit;

        this.advancedBtn.Collapsed = !monitorExplicit.HasValue || !monitorExplicit.Value;

        return true;
    }

    public override bool ModifyData(OperationsWizardData data)
    {
        if (!Value.HasValue)
        {
            return false;
        }
        data.MonitorExplicitOperationNumbers = Value;
        return true;
    }

    protected string AdvancedSectionElementID
    {
        get { return ClientID + "_advancedSection"; }
    }

    protected string GetScript(bool monitorExplicit)
    {
        var script = Page.ClientScript.GetPostBackEventReference(this, monitorExplicit.ToString());
        script = script.Replace("\"", "\'");
        script = script.Replace("\'", "\\\'");
        script += ";$find(\\\'" + this.ClientID + "\\\').val=\\\'" + monitorExplicit.ToString() + "\\\';";
        return "javascript:setTimeout('" + script + "', 0)";
    }

    protected override ScriptControlDescriptor GetScriptDescriptor()
    {
        var descriptor = base.GetScriptDescriptor();
        descriptor.Type = "Orion.Voip.Controls.Wizards.Operations.MonitorScanOrEnterWebWizardPartControl";
        descriptor.AddProperty("Value", Value);
        return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        var result = base.GetScriptReferences();
        result = result.Concat(new[] { new ScriptReference(ResolveUrl("MonitorScanOrEnterWebWizardPartControl.js")) });
        return result;
    }

    #region IPostBackEventHandler Members

    void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
    {
        if (string.IsNullOrEmpty(eventArgument))
            return;

        bool monitorExplicit;
        if (bool.TryParse(eventArgument, out monitorExplicit))
        {
            Value = monitorExplicit;
            RaiseDataChanged(EventArgs.Empty);
        }
    }

    #endregion
}
