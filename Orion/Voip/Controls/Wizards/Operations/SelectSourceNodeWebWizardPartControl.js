﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Voip.Controls.Wizards.Operations");

Orion.Voip.Controls.Wizards.Operations.SelectSourceNodeWebWizardPartControl = function(element) {
    Orion.Voip.Controls.Wizards.Operations.SelectSourceNodeWebWizardPartControl.initializeBase(this, [element]);
}

Orion.Voip.Controls.Wizards.Operations.SelectSourceNodeWebWizardPartControl.prototype = {
    OnNotAllowedNodeSelected: function(message) {
        Ext.MessageBox.show({
            title: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}",
            icon: Ext.MessageBox.ERROR,
            msg: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_7;E=js}",
            closable: false,
            buttons: Ext.MessageBox.OK
        });
    },

    OnNoNodeSelected: function(message) {
        Ext.MessageBox.show({
            title: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}",
            icon: Ext.MessageBox.ERROR,
            msg: message,
            closable: false,
            buttons: Ext.MessageBox.OK
        });
    },

    initialize: function() {
        Orion.Voip.Controls.Wizards.Operations.SelectSourceNodeWebWizardPartControl.callBaseMethod(this, 'initialize');
        
        // Add custom initialization here
    },
    dispose: function() {
        //Add custom dispose actions here
        Orion.Voip.Controls.Wizards.Operations.SelectSourceNodeWebWizardPartControl.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Controls.Wizards.Operations.SelectSourceNodeWebWizardPartControl.registerClass('Orion.Voip.Controls.Wizards.Operations.SelectSourceNodeWebWizardPartControl', Orion.Voip.Controls.Wizards.WizardFlowPartControl);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
