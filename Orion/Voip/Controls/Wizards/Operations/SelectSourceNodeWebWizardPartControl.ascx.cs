﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;
using System.Diagnostics;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.IpSla.Web.Wizards;

public partial class Orion_Voip_Controls_Wizards_Operations_SelectSourceNodeWebWizardPartControl : WizardFlowPartControl<OperationsWizardData>
{
    private SelectSourceNodeWebWizardPart m_part;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ChooseNodesTreeControl.SelectedNodesListIdentifier = SelectSourceNodeWebWizardPart.CHOOSED_NODE_KEY;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        m_part = (SelectSourceNodeWebWizardPart)OperationsWizardSession.Current.CurrentPart;
        m_part.NoNodeSelected += new EventHandler<MessageEventArgs>(part_NoNodeSelected);
        m_part.NotAllowedNodeSelected += new EventHandler(part_NotAllowedNodeSelected);
    }

    public override bool Initialize(OperationsWizardData data)
    {
        if (IpSlaOperationHelper.IsCliBased(data.OperationType.Value))
        {
            this.CredentialsCheckControl.WindowTitle = Resources.VNQMWebContent.VNQMWEBCODE_VB1_33;
            this.CredentialsCheckControl.WindowMessage = Resources.VNQMWebContent.VNQMWEBCODE_VB1_34;
        }
        else
        {
            this.CredentialsCheckControl.WindowTitle = Resources.VNQMWebContent.VNQMWEBCODE_VB1_35;
            this.CredentialsCheckControl.WindowMessage = Resources.VNQMWebContent.VNQMWEBCODE_VB1_36;
        }

        RefreshTree(this.groupBySelector.SelectedValue);

        var nodeList = SitesDAL.Instance.GetAllGroupingTypes("MachineType");
        this.operationNexusArea.Visible = nodeList.Keys.Any(NodeDeviceHelper.IsCiscoNexusMachineType);

        return true;
    }

    public override bool ModifyData(OperationsWizardData data)
    {
        return true;
    }

    void part_NoNodeSelected(object sender, MessageEventArgs e)
    {
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "NoNodeSelected",
            "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').OnNoNodeSelected('" + e.Message + "');},0);});",
            true);
        RefreshTree(this.groupBySelector.SelectedValue);
    }

    void part_NotAllowedNodeSelected(object sender, EventArgs e)
    {
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "NotAllowedNodeSelected",
            "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').OnNotAllowedNodeSelected();},0);});",
            true);
        RefreshTree(this.groupBySelector.SelectedValue);
    }

    protected void groupBySelector_SelectedValueChanged(object sender, EventArgs e)
    {
        RefreshTree(this.groupBySelector.SelectedValue);
    }

    public void RefreshTree(string groupBy)
    {
        ChooseNodesTreeControl.RefreshTree(groupBy, new FilterChooseNodesAjaxTreeService());
    }

    protected override void OnUnload(EventArgs e)
    {
        if (m_part != null)
        {
            m_part.NoNodeSelected -= part_NoNodeSelected;
            m_part.NotAllowedNodeSelected -= part_NotAllowedNodeSelected;
        }
        else
        {
            Debug.WriteLine("m_cPart is null !!!");
        }
        base.OnUnload(e);
    }

    protected override ScriptControlDescriptor GetScriptDescriptor()
    {
        var descriptor = base.GetScriptDescriptor();
        descriptor.Type = "Orion.Voip.Controls.Wizards.Operations.SelectSourceNodeWebWizardPartControl";
        return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        var result = base.GetScriptReferences();
        result = result.Concat(new[] { new ScriptReference(this.ResolveUrl("SelectSourceNodeWebWizardPartControl.js")) });
        return result;
    }

}
