﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectTargetExplicitWebWizardPartControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_Operations_SelectTargetExplicitWebWizardPartControl" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web.Wizards.Operations" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Data" %>

<div class="ipsla_WP_Title">
  <% if (this.WizardData.WizardMode != WizardMode.MonitorExistingOperations)
     { %>
     <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_95, SolarWinds.Orion.IpSla.Web.Wizards.Operations.OperationsWizardSession.Current.Data.OperationTypeName)%>
  <% } else { %>
  <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_96, SolarWinds.Orion.IpSla.Web.Wizards.Operations.OperationsWizardSession.Current.Data.OperationTypeName)%>
  <% } %>
</div>
<div class="ipsla_WP_Description">
	<%= Description %>
</div>
<table>
	<tr id="urlRow" runat="server">
		<td valign="top">
			<%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_97 %>
		</td>
		<td>
			<asp:TextBox ID="urlTextBox" runat="server" Width="300"></asp:TextBox>
            <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_98, ExampleUrl)%>
			<asp:RequiredFieldValidator ID="urlRequiredFieldValidator" runat="server" 
			    Display="Dynamic"
			    ControlToValidate="urlTextBox"
			    ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_99 %>" >
			</asp:RequiredFieldValidator>
			<asp:CustomValidator runat="server" ControlToValidate="urlTextBox"
			    Display="Dynamic"
			    ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_100 %>"
			    OnServerValidate="UrlTextBox_ServerValidate" />
    		</td>
	</tr>
	<tr id="ipAddressRow" runat="server">
		<td>
			<%= IpAddressLabel %>
		</td>
		<td>
			<asp:TextBox ID="ipAdressTextBox" runat="server"></asp:TextBox>
			<asp:RequiredFieldValidator ID="ipAdressRequiredFieldValidator" runat="server" 
			    Display="Dynamic"
			    ControlToValidate="ipAdressTextBox"
			    ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_101 %>" >
			</asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="ipAddressRegularExpressionValidator" runat="server"
			    Display="Dynamic"
			    ControlToValidate="ipAdressTextBox"
			    ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_102 %>" ></asp:RegularExpressionValidator>
		</td>
	</tr>
	<tr id="dnsResolveValueRow" runat="server">
		<td>
			<%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_103%>
		</td>
		<td>
			<asp:TextBox ID="dnsResolveValueTextBox" runat="server"></asp:TextBox>
			<asp:RequiredFieldValidator ID="dnsResolveValueRequiredFieldValidator" runat="server" 
			    Display="Dynamic"
			    ControlToValidate="dnsResolveValueTextBox"
			    ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_104 %>" >
			</asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="dnsResolveValueRegularExpressionValidator" runat="server"
			    Display="Dynamic"
			    ControlToValidate="dnsResolveValueTextBox"
			    ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_105 %>" ></asp:RegularExpressionValidator>
		</td>
	</tr>
</table>