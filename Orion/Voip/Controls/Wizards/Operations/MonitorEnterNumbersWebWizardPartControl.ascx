﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MonitorEnterNumbersWebWizardPartControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_Operations_MonitorEnterNumbersWebWizardPartControl" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web.WebServices.AjaxTree"%>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web.Wizards.Operations" %>
<%@ Register Assembly="SolarWinds.Orion.IpSla.Web" Namespace="SolarWinds.Orion.IpSla.Web.UI" TagPrefix="ipsla" %>

<asp:CustomValidator ID="cvSelection" runat="server" />

<div class="ipsla_WP_Title">
  <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_90 %>
</div>

<asp:Repeater ID="Boxes" runat="server" OnItemDataBound="Boxes_ItemDataBound">
    <HeaderTemplate>
        <table width="100%" cellspacing="0" cellpadding="3" class="Events">
    </HeaderTemplate>
    <ItemTemplate>
        <tr>
            <td colspan="2">
                <b><%# String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_91, Eval("Site.Name"))%></b>
            </td>
        </tr>
        <tr>
            <td style="text-align: right; vertical-align: top; padding-left: 5em; white-space: nowrap">
                <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_92 %>
            </td>
	        <td style="width: 100%;">
	            <div>
	                <asp:TextBox ID="OperationNumbers" runat="server"
	                    Style="width: 100%;"
	                    TextMode="MultiLine" Wrap="true" Rows="4" />
	            </div>
	            <span style="color: gray;">
	                <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_93 %>
	            </span><br />
	            <asp:RegularExpressionValidator ID="OperationNumberFormatValidator" runat="server"
	                ControlToValidate="OperationNumbers" Display="Static" SetFocusOnError="true"
	                ValidationExpression="^\s*\d+([ \f\t\v\u00A0\u2028\u2029]*(,|(\r\n|\r|\n)+)[ \f\t\v\u00A0\u2028\u2029]*\d+)*\s*$"
	                ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_94 %>" />
	        </td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>
