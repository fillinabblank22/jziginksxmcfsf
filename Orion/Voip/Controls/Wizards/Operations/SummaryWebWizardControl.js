﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Voip.Controls.Wizards.Operations");

Orion.Voip.Controls.Wizards.Operations.SummaryWebWizardControl = function (element) {
    Orion.Voip.Controls.Wizards.Operations.SummaryWebWizardControl.initializeBase(this, [element]);
    this.progressControl = null;
    this.progressAreaID = "";
    //this.pbarBoxID = "";
    //this.pbarID = "";
    //this.createdAreaID = "";
    //this.totalAreaID = "";
    this.totalAreaHeadID = "";
    this.resultsAreaID = "";
    this.createdAreaResultsID = "";
    this.gridAreaResultsID = "";
    this.totalAreaHeadExistID = "";
    this.createdAreaResultsExistID = "";

    this.setupId = "";
    this.setupCreated = 0;
    this.setupTotal = 0;

    this.addMoreOperations = null;
}

Orion.Voip.Controls.Wizards.Operations.SummaryWebWizardControl.prototype = {

    get_AddMoreOperationsID: function () {
        return this.addMoreOperations ? this.addMoreOperations.id : null;
    },
    set_AddMoreOperationsID: function (value) {
        this.addMoreOperations = $get(value);
    },

    get_progressAreaID: function () {
        return this.progressAreaID;
    },

    set_progressAreaID: function (value) {
        this.progressAreaID = value;
    },

    get_progressControl: function () {
        return this.progressControl;
    },

    set_progressControl: function (value) {
        this.progressControl = value;
    },


    //    get_pbarBoxID: function() {
    //        return this.pbarBoxID;
    //    },

    //    set_pbarBoxID: function(value) {
    //        this.pbarBoxID = value;
    //    },

    //    get_pbarID: function() {
    //        return this.pbarID;
    //    },

    //    set_pbarID: function(value) {
    //        this.pbarID = value;
    //    },

    //    get_createdAreaID: function() {
    //        return this.createdAreaID;
    //    },

    //    set_createdAreaID: function(value) {
    //        this.createdAreaID = value;
    //    },

    //    get_totalAreaID: function() {
    //        return this.totalAreaID;
    //    },

    //    set_totalAreaID: function(value) {
    //        this.totalAreaID = value;
    //    },

    get_totalAreaHeadID: function () {
        return this.totalAreaHeadID;
    },

    set_totalAreaHeadID: function (value) {
        this.totalAreaHeadID = value;
    },

    get_resultsAreaID: function () {
        return this.resultsAreaID;
    },

    set_resultsAreaID: function (value) {
        this.resultsAreaID = value;
    },

    get_createdAreaResultsID: function () {
        return this.createdAreaResultsID;
    },

    set_createdAreaResultsID: function (value) {
        this.createdAreaResultsID = value;
    },

    get_gridAreaResultsID: function () {
        return this.gridAreaResultsID;
    },

    set_gridAreaResultsID: function (value) {
        this.gridAreaResultsID = value;
    },
    get_createdAreaResultsExistID: function () {
        return this.createdAreaResultsExistID;
    },

    set_createdAreaResultsExistID: function (value) {
        this.createdAreaResultsExistID = value;
    },
    ShowAddMoreOperations: function () {
        if (this.addMoreOperations) {
            this.addMoreOperations.style.display = 'inline-block';
        }
    },

    get_totalAreaHeadExistID: function () {
        return this.totalAreaHeadExistID;
    },

    set_totalAreaHeadExistID: function (value) {
        this.totalAreaHeadExistID = value;
    },

    Start: function () {
        //        this.ProgressBar = new Ext.ProgressBar({
        //            id: 'ProgressBar',
        //            cls: 'custom'
        //        });
        this.RefreshStatus(this.setupCreated, this.setupTotal);
        //this.ProgressBar.render(this.pbarID);
        this.StartTimer();
    },

    Resume: function () {
        SolarWinds.Orion.IpSla.Web.Wizards.Operations.SummaryService.Resume(
            Function.createDelegate(this, this.OnSuccess), Function.createDelegate(this, this.OnError));
        this.StartTimer();
    },

    AskCancel: function () {
        SolarWinds.Orion.IpSla.Web.Wizards.Operations.SummaryService.Pause(
            Function.createDelegate(this, this.OnSuccessPause), Function.createDelegate(this, this.OnError));
    },

    Cancel: function (leaveCreatedOperations) {
        SolarWinds.Orion.IpSla.Web.Wizards.Operations.SummaryService.Cancel(leaveCreatedOperations,
            Function.createDelegate(this, this.OnSuccessCancel), Function.createDelegate(this, this.OnError));
    },

    StartTimer: function () {
        window.setTimeout(Function.createDelegate(this, this.OnTick), 1000);
    },

    OnTick: function (error, context) {
        //ask for state
        SolarWinds.Orion.IpSla.Web.Wizards.Operations.SummaryService.GetState(
            Function.createDelegate(this, this.OnSuccessGetState), Function.createDelegate(this, this.OnError));
    },

    OnSuccess: function (result, context) {
    },

    OnSuccessPause: function (result, context) {
        if (this.HandleStatus(result)) {
            this.ShowPauseDialog();
        }
    },

    OnSuccessCancel: function (result, context) {
        window.location.reload(true);
    },

    ShowPauseDialog: function () {
        Ext.Msg.show({
            title: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}',
            msg: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_75;E=js}',
            buttons: { yes: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_76;E=js}', no: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_77;E=js}', cancel: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_26;E=js}' },
            fn: Function.createDelegate(this, this.ProcessCancelDialogResult),
            icon: Ext.MessageBox.QUESTION
        });
    },

    ProcessCancelDialogResult: function (btn) {
        if (btn == "yes") { //Cancel, leave created operations
            this.Cancel(true);
        }
        else if (btn == "no") { //Cancel, remove created operations
            this.Cancel(false);
        }
        else if (btn == "cancel") { //Continue creating operations
            this.Resume();
        }
    },

    OnSuccessGetState: function (result, context) {
        if (this.HandleStatus(result)) {
            if (result.IsWorking) {
                this.StartTimer();
            }
        }
    },

    OnError: function (error, context) {
        if (error.get_statusCode() > 0) {
            Ext.Msg.show({
                title: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}',
                msg: String.format('@{R=VNQM.Strings;K=VNQMWEBJS_VB1_78;E=js}', error.get_message()),
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
    },

    HandleStatus: function (status) {
        if (!status) {
            this.wizardControl.OnSetupCanceled();
            return false;
        }
        this.statusCreated = status.CountCreatedOperations;
        this.statusTotal = status.CountAllOperations;
        this.RefreshStatus(this.statusCreated, this.statusTotal);
        if (status.IsFinished) {
            this.HideProgressAndShowResults(status);
            return false;
        }
        return true;
    },

    RefreshStatus: function (createdOperations, totalOperations) {

        //jQuery('#' + this.createdAreaID).get(0).innerHTML = createdOperations;
        //jQuery('#' + this.totalAreaID).get(0).innerHTML = totalOperations;
        jQuery('#' + this.totalAreaHeadID).get(0).innerHTML = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_VB1_79;E=js}', totalOperations);
        jQuery('#' + this.totalAreaHeadExistID).get(0).innerHTML = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_VB1_80;E=js}', totalOperations);

        var processedInnerHtml = '<div class=\"ipsla_ProgressLegendValue\">' + createdOperations + '</div>&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_VB1_81;E=js}';
        var totalInnerHtml = '<div class=\"ipsla_ProgressLegendValue\">' + totalOperations + '</div>&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_VB1_82;E=js}';
        var value = (totalOperations != 0 ? createdOperations / totalOperations : 0);
        this.progressControl.UpdateProgress(value, processedInnerHtml, totalInnerHtml);


    },

    HideProgressAndShowResults: function (result) {
        jQuery(jQuery('#' + this.progressAreaID).get(0)).hide();
        jQuery(jQuery('#' + this.resultsAreaID).get(0)).show();

        jQuery('#' + this.createdAreaResultsID).get(0).innerHTML = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_VB1_83;E=js}', result.CountCreatedOperations);
        jQuery('#' + this.createdAreaResultsExistID).get(0).innerHTML = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_VB1_84;E=js}', result.CountCreatedOperations);

        this.LoadOperationsGrid();

        this.ShowAddMoreOperations();
    },

    LoadOperationsGrid: function () {
        OperationsDataStore.load({
            params: { id: this.setupId },
            callback: Function.createDelegate(this, this.OperationsGridDataLoaded)
        });
    },

    OperationsGridDataLoaded: function (e, options, success) {
        if (success) {
            OperationsGrid.render(this.gridAreaResultsID);
        }
        SolarWinds.Orion.IpSla.Web.Wizards.Operations.SummaryService.FinishWizard(
                Function.createDelegate(this, this.OnSuccess), Function.createDelegate(this, this.OnError));
    },



    initialize: function () {
        Orion.Voip.Controls.Wizards.Operations.SummaryWebWizardControl.callBaseMethod(this, 'initialize');

        // Add custom initialization here
        this.Start();
    },
    dispose: function () {
        //Add custom dispose actions here
        Orion.Voip.Controls.Wizards.Operations.SummaryWebWizardControl.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Controls.Wizards.Operations.SummaryWebWizardControl.registerClass('Orion.Voip.Controls.Wizards.Operations.SummaryWebWizardControl', Orion.Voip.Controls.Wizards.WizardFlowPartControl);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
