﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BiDirectionalPathsOption.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_Operations_BiDirectionalPathsOption" %>
    <style type="text/css">
        .radiobutton label{
            padding-left:5px;
        }
    </style>

<asp:UpdatePanel ID="up" runat="server" UpdateMode="Always">
    <ContentTemplate>
        <div class="externalNodesOption">
            <div class="ipsla_WP_SubTitle alternateRow">
                &nbsp;<%= SingleNode ? Resources.VNQMWebContent.VNQMWEBDATA_VB1_49 : Resources.VNQMWebContent.VNQMWEBDATA_VB1_50%>
            </div>
            <div>
                <asp:RadioButton ID="rbtnYes" runat="server" 
                    AutoPostBack="true" 
                    GroupName="BiDirectionalPathsOption" 
                    oncheckedchanged="rbtn_CheckedChanged" CssClass="radiobutton"/>
            </div>
            <div>
                <asp:RadioButton ID="rbtnNo" runat="server" 
                    AutoPostBack="true" 
                    GroupName="BiDirectionalPathsOption" 
                    oncheckedchanged="rbtn_CheckedChanged" CssClass="radiobutton"/>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>