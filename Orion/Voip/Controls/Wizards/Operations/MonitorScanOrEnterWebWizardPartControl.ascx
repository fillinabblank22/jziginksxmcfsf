﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MonitorScanOrEnterWebWizardPartControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_Operations_MonitorScanOrEnterWebWizardPartControl" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web.Wizards.Operations" %>
<%@ Register Assembly="SolarWinds.Orion.IpSla.Web" Namespace="SolarWinds.Orion.IpSla.Web.UI" TagPrefix="ipsla" %>

<asp:CustomValidator ID="cvSelection" runat="server" />

<div class="ipsla_WP_Title">
  <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_80%>
</div>

<table width="100%" cellspacing="0" cellpadding="3" class="Events">
    <tbody>
        <%-- Scan --%>
        <tr class="alternate">
            <td class="rbtnCell">
                <input type="radio" name="selectTypeGroup"
                    <%= Value == false ? "checked" : string.Empty %>
                    onclick="<%= GetScript(false) %>" />
            </td>
		    <td class="iconCell">
		    </td>
            <td class="textCell" valign="middle" style="vertical-align:middle;">
			    <div class="ipsla_WP_SubTitle">
			        <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_81 %>
			    </div>
			    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_82 %>
            </td>
        </tr>
        <%-- Advanced toggle control --%>
        <tr>
            <td colspan="3" style="padding-top: 2em; padding-bottom: 2em;">
                <ipsla:CollapseElement ID="advancedBtn" runat="server" LinkHandle="true">
                    <Handle><%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_49 %></Handle>
                </ipsla:CollapseElement>
            </td>
        </tr>
    </tbody>
    <tbody id="<%= AdvancedSectionElementID %>" style="display: none;">
        <%-- Manually Enter --%>
        <tr class="alternate">
            <td class="rbtnCell">
                <input type="radio" name="selectTypeGroup"
                    <%= Value == true ? "checked" : string.Empty %>
                    onclick="<%= GetScript(true) %>" />
            </td>
		    <td class="iconCell">
		    </td>
            <td class="textCell" valign="middle" style="vertical-align:middle;">
			    <div class="ipsla_WP_SubTitle">
			        <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_83 %>
			    </div>
			    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_84%>
            </td>
        </tr>
    </tbody>
</table>
