﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OperationsWizardControlContainer.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_Operations_OperationsWizardControlContainer" %>
<%@ Register Assembly="SolarWinds.Orion.IpSla.Web" TagPrefix="ipsla" Namespace="SolarWinds.Orion.IpSla.Web.UI" %>

<div class="WizardControl">
<h1><%= WizardTitle %></h1>

<ipsla:WizardControl ID="wizardControl" runat="server"
    OnScriptDescriptorInitializing="wizardControl_ScriptDescriptorInitializing"
    OnScriptReferencesRequired="wizardControl_ScriptReferencesRequired">
    <CustomButtonsTemplate>
        <orion:LocalizableButton runat="server" ID="ibtnPrev" CausesValidation="false" OnClick="ibtnPrev_Click"
            LocalizedText="Back" DisplayType="Secondary" ToolTip="<%$ Resources :VNQMWebContent, VNQMWEBDATA_VB1_1 %>" />

        <asp:PlaceHolder ID="phCustomButtons" runat="server" />

        <%--If DemoMode, just warn for Node Discovery. The discovery will be skipped in the WizardPart web control.--%>
        <orion:LocalizableButton runat="server" ID="ibtnStartIpSlaDiscovery" ToolTip="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_3 %>"
            OnClientClick="CheckDemoMode();" OnClick="ibtnStartIpSlaDiscovery_Click" Text="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_2 %>"
            DisplayType="Primary" />

        <span id="btnNext" runat="server">
            <orion:LocalizableButton ID="ibtnCreateOperations" runat="server" OnClick="ibtnCreateOperations_Click" DisplayType="Primary"/>

            <orion:LocalizableButton ID="ibtnNext" runat="server" OnClick="ibtnNext_Click" ToolTip="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_4 %>"
                LocalizedText="Next" DisplayType="Primary" />
        </span>

        <orion:LocalizableButton runat="server" ID="ibtnCancel" CausesValidation="false"
            ToolTip="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_5 %>" OnClick="ibtnCancel_Click" LocalizedText="Cancel" DisplayType="Secondary" />
    </CustomButtonsTemplate>
</ipsla:WizardControl>

</div>