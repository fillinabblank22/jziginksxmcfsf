﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Voip.Controls.Wizards.Operations");

Orion.Voip.Controls.Wizards.Operations.ChooseNodesInOperationWebWizardPartControl = function(element) {
    Orion.Voip.Controls.Wizards.Operations.ChooseNodesInOperationWebWizardPartControl.initializeBase(this, [element]);
}

Orion.Voip.Controls.Wizards.Operations.ChooseNodesInOperationWebWizardPartControl.prototype = {
    OnTooFewNodesSelected: function() {
        Ext.MessageBox.show({
            title: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}",
            icon: Ext.MessageBox.ERROR,
            msg: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_87;E=js}",
            closable: false,
            buttons: Ext.MessageBox.OK
        });
    },

    OnTooManyNodesSelected: function(message) {
        Ext.MessageBox.show({
            title: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}",
            icon: Ext.MessageBox.ERROR,
            msg: message,
            closable: false,
            buttons: Ext.MessageBox.OK
        });
    },

    OnLicenseCountExceeded: function(message) {
        Ext.MessageBox.show({
            title: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}",
            icon: Ext.MessageBox.ERROR,
            msg: String.format("@{R=VNQM.Strings;K=VNQMWEBJS_VB1_88;E=js}", message, "<a href='/Orion/Voip/Admin/ManageOperations.aspx'>", "</a>"),
            closable: false,
            buttons: Ext.MessageBox.OK
        });
    },

    OnNoNodeSelected: function() {
        Ext.MessageBox.show({
            title: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}",
            icon: Ext.MessageBox.ERROR,
            msg: "@{R=VNQM.Strings;K=VNQMLIBCODE_VB1_30;E=js}",
            closable: false,
            buttons: Ext.MessageBox.OK
        });
    },
    
    initialize: function() {
        Orion.Voip.Controls.Wizards.Operations.ChooseNodesInOperationWebWizardPartControl.callBaseMethod(this, 'initialize');
        
        // Add custom initialization here
    },
    dispose: function() {        
        //Add custom dispose actions here
        Orion.Voip.Controls.Wizards.Operations.ChooseNodesInOperationWebWizardPartControl.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Controls.Wizards.Operations.ChooseNodesInOperationWebWizardPartControl.registerClass('Orion.Voip.Controls.Wizards.Operations.ChooseNodesInOperationWebWizardPartControl', Orion.Voip.Controls.Wizards.WizardFlowPartControl);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
