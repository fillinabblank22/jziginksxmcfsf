﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Voip.Controls.Wizards.Operations");

Orion.Voip.Controls.Wizards.Operations.NodeDiscoveryWebWizardPartControl = function(element) {
    Orion.Voip.Controls.Wizards.Operations.NodeDiscoveryWebWizardPartControl.initializeBase(this, [element]);
}

Orion.Voip.Controls.Wizards.Operations.NodeDiscoveryWebWizardPartControl.prototype = {

    AbortWizard: Function.createDelegate(this, function() {
        this.wizardControl.AbortWizard();
    }),

    initialize: function() {
        Orion.Voip.Controls.Wizards.Operations.NodeDiscoveryWebWizardPartControl.callBaseMethod(this, 'initialize');
        // Add custom initialization here
    },
    dispose: function() {
        // Add custom dispose actions here
        Orion.Voip.Controls.Wizards.Operations.NodeDiscoveryWebWizardPartControl.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Controls.Wizards.Operations.NodeDiscoveryWebWizardPartControl.registerClass('Orion.Voip.Controls.Wizards.Operations.NodeDiscoveryWebWizardPartControl', Orion.Voip.Controls.Wizards.WizardFlowPartControl);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
