<%@ WebService Language="C#" Class="Orion.Voip.Controls.Wizards.Operations.OperationDiscoveryService" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;

namespace Orion.Voip.Controls.Wizards.Operations
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class OperationDiscoveryService : WebService
    {
        private static readonly Log Log = new Log();

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public object GetState()
        {
            using (Log.Block())
            {
                try
                {
                    var session = OperationsWizardSession.Current;
                    if (session == null)
                        return null;

                    var data = session.Data;

                    var operationDiscovery = data.OperationDiscovery;
                    if (operationDiscovery == null)
                        return null;

                    operationDiscovery.UpdateStatus();

                    if (operationDiscovery.Finished)
                    {
                        operationDiscovery.SetDiscoveredOperations(data);
                        data.OperationDiscovery = null;
                    }

                    return new
                        {
                            Finished = operationDiscovery.Finished,
                            FinishedNodes = operationDiscovery.TotalNodes - operationDiscovery.PendingNodes,
                            TotalNodes = operationDiscovery.TotalNodes,
                            ProgressTotal = operationDiscovery.ProgressTotal,
                            ProgressCurrent = operationDiscovery.ProgressCurrent,
                        };
                }
                catch (Exception ex)
                {
                    Log.Error("Error getting operation discovery state.", ex);
                    throw;
                }
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public object GetOperationsExcluded()
        {
            using (Log.Block())
            {
                try
                {
                    var session = OperationsWizardSession.Current;
                    if (session != null)
                    {
                        var data = session.Data;

                        var sitesByNodeId = SitesDAL.Instance.GetSitesDictionaryByIDs(data.ManualOperationNumbersExcluded.Keys, false, false);

                        return (
                            from pair in data.ManualOperationNumbersExcluded
                            let exclusions = pair.Value
                            where exclusions.NotFound.Count > 0 || exclusions.AlreadyMonitored.Count > 0 || exclusions.Automatic.Count > 0
                            select new
                                {
                                    SourceName = sitesByNodeId[pair.Key].Name,
                                    ExplicitNumbers = data.MonitorExplicitOperationNumbers == true,
                                    NotFound = data.MonitorExplicitOperationNumbers == true
                                        ? (object)pair.Value.NotFound.ToArray()
                                        : pair.Value.NotFound.Count,
                                    AlreadyMonitored = data.MonitorExplicitOperationNumbers == true
                                        ? (object)pair.Value.AlreadyMonitored.ToArray()
                                        : pair.Value.AlreadyMonitored.Count,
                                    Automatic = data.MonitorExplicitOperationNumbers == true
                                        ? (object)pair.Value.Automatic.ToArray()
                                        : pair.Value.Automatic.Count,
                                }
                            ).ToArray();
                    }

                    return new object[0];
                }
                catch (Exception ex)
                {
                    Log.Error("Error getting operation discovery state.", ex);
                    throw;
                }
            }
        }
    }
}
