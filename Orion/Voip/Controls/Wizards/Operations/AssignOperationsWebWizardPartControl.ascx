﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AssignOperationsWebWizardPartControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_Operations_AssignOperationsWebWizardPartControl" %>
<%@ Register src="ChooseNodesTreeControl.ascx" tagname="ChooseNodesTreeControl" tagprefix="ipsla" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web.Wizards.Operations" %>

<div class="ipsla_WP_Title">
  <% if (this.WizardData.WizardMode != WizardMode.MonitorExistingOperations)
     { %>
        <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_111, SolarWinds.Orion.IpSla.Web.Wizards.Operations.OperationsWizardSession.Current.Data.OperationTypeName)%>
  <% } else { %>
        <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_112, SolarWinds.Orion.IpSla.Web.Wizards.Operations.OperationsWizardSession.Current.Data.OperationTypeName)%>
  <% } %>
</div>
<div class="ipsla_WP_Description">
  <% if (this.WizardData.WizardMode != WizardMode.MonitorExistingOperations)
     { %>
    <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_113, SolarWinds.Orion.IpSla.Web.Wizards.Operations.OperationsWizardSession.Current.Data.OperationTypeName)%>
  <% } else { %>
    <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_114, SolarWinds.Orion.IpSla.Web.Wizards.Operations.OperationsWizardSession.Current.Data.OperationTypeName)%>
  <% } %>
</div>

<div class="TreeContent">
    <ipsla:ChooseNodesTreeControl ID="ChooseNodesTreeControl" runat="server">
        <WebService Path="~/Orion/Voip/Controls/Wizards/Operations/AssignOperationsAjaxTreeService.asmx" />
     </ipsla:ChooseNodesTreeControl>
</div>


