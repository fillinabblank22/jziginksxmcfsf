﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DefinePathsWebWizardPartControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_Operations_DefinePathsWebWizardPartControl" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web.Wizards.Operations" %>
<%@ Register Assembly="SolarWinds.Orion.IpSla.Web" Namespace="SolarWinds.Orion.IpSla.Web.UI" TagPrefix="ipsla" %>

<asp:CustomValidator ID="cvSelection" runat="server" />

<div class="ipsla_WP_Title">
  <% if (WizardData.WizardMode != WizardMode.MonitorExistingOperations)
     { %>
        <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_11, OperationsWizardSession.Current.Data.OperationTypeName)%>
  <% } else { %>
        <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_12, OperationsWizardSession.Current.Data.OperationTypeName)%>
  <% } %>
</div>

<table width="100%" cellspacing="0" cellpadding="3" class="Events">
    <tbody>
        <%-- Simple --%>
        <tr class="alternate">
            <td class="rbtnCell">
                <input type="radio" name="selectTypeGroup"
                    <%= Value == PathDefinitionMethod.Simple ? "checked" : string.Empty %>
                    onclick="<%= GetScript(PathDefinitionMethod.Simple) %>" />
            </td>
		    <td style="width:70px;">
			    <img src="/Orion/Voip/images/PathTypeIcons/path_type_simple_bidir.gif"/>
		    </td>
            <td class="textCell" valign="middle" style="vertical-align:middle;">
			    <div class="ipsla_WP_SubTitle">
			        <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_13 %>
			    </div>
			    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_14 %>
            </td>
        </tr>
        <%-- Advanced toggle control --%>
        <tr>
            <td colspan="3" style="padding-top: 2em; padding-bottom: 2em;">
                <ipsla:CollapseElement ID="advancedBtn" runat="server" LinkHandle="true">
                    <Handle><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_15 %></Handle>
                </ipsla:CollapseElement>
            </td>
        </tr>
    </tbody>
    <tbody id="<%= AdvancedSectionElementID %>" style="display: none;">
        <%-- Fully meshed --%>
        <tr class="alternate">
            <td class="rbtnCell">
                <input type="radio" name="selectTypeGroup"
                    <%= Value == PathDefinitionMethod.FullyMeshed ? "checked" : string.Empty %>
                    onclick="<%= GetScript(PathDefinitionMethod.FullyMeshed) %>" />
            </td>
		    <td style="width:70px;">
			    <img src="/Orion/Voip/images/PathTypeIcons/path_type_fullmesh.gif"/>
		    </td>
            <td class="textCell" valign="middle" style="vertical-align:middle;">
			    <div class="ipsla_WP_SubTitle">
			        <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_16 %>
			    </div>
			    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_17 %>
            </td>
        </tr>
        <%-- Hub and Spoke --%>
        <tr>
            <td class="rbtnCell">
                <input type="radio" name="selectTypeGroup"
                    <%= Value == PathDefinitionMethod.HubAndSpoke ? "checked" : string.Empty %>
                    onclick="<%= GetScript(PathDefinitionMethod.HubAndSpoke) %>" />
            </td>
		    <td style="width:70px;">
			    <img src="/Orion/Voip/images/PathTypeIcons/path_type_hubspoke.gif"/>
		    </td>
            <td class="textCell" valign="middle" style="vertical-align:middle;">
			    <div class="ipsla_WP_SubTitle">
			        <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_18%>
			    </div>
			    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_19 %>
            </td>
        </tr>
        <%-- Custom --%>
        <tr class="alternate">
            <td class="rbtnCell">
                <input type="radio" name="selectTypeGroup"
                    <%= Value == PathDefinitionMethod.Custom ? "checked" : string.Empty %>
                    onclick="<%= GetScript(PathDefinitionMethod.Custom) %>" />
            </td>
		    <td style="width:70px;">
			    <img src="/Orion/Voip/images/PathTypeIcons/path_type_custom.gif"/>
		    </td>
            <td class="textCell" valign="middle" style="vertical-align:middle;">
			    <div class="ipsla_WP_SubTitle">
			        <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_20 %>
			    </div>
			    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_21 %>
            </td>
        </tr>
    </tbody>
</table>
