﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectTypeWebWizardPartControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_Operations_SelectTypeWebWizardPartControl" %>

<%@ Import Namespace="SolarWinds.Orion.IpSla.Data" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web.Wizards.Operations" %>

<asp:CustomValidator ID="cvSelection" runat="server" />

<div class="ipsla_WP_Title">
  <% if (this.WizardData.WizardMode != WizardMode.MonitorExistingOperations)
     { %>
    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_9 %>
  <% } else { %>
    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_10 %>
  <% } %>
</div>
<div class="ipsla_WP_Description">
</div>

<table>
    <tr>
        <td width="50%">
            <asp:Repeater ID="rptOperationCategory" runat="server" OnItemDataBound="rptOperationCategory_OnItemDataBound">    
                <ItemTemplate>

                    <%# (Container.ItemIndex == 2) ? @"</td><td style='vertical-align: top;'>" : string.Empty%> 
                
                    <div style="margin-bottom:10px; margin-right:5px; padding: 5px; border:1px solid #dddddd">
                        <div class="ipsla_WP_SubTitle">
                            <%# Eval("Title")%>            
                        </div>
                        <div class="ipsla_WP_Description">
                            <%# Eval("Subtitle")%>            
                        </div>

                        <asp:Repeater ID="rptOperations" runat="server" OnItemDataBound="rptOperations_OnItemDataBound">
                            <HeaderTemplate>
                                <table width="100%" cellspacing="0" cellpadding="3" class="Events">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr id="row" runat="server">
                                    <td class="rbtnCell">
                                        <%-- Table cells for one  table row of SelectType wizard page.
                                         Uses html input radio control instead of ASP.Net RadioButton control, becauce of problem with name attribute 
                                         (used for radiobuttons grouping) in Repeater - every Repeater item is InamingContainer - so name attribute is 
                                         generated unique for each radio
                                        --%>
                                        <input id='rbtn<%# ((int)Eval("Value")).ToString() %>'
                                            type="radio"
                                            name="selectTypeGroup" 
                                            value='<%# ((OperationType)Eval("Value")).ToString() %>'
                                            <%# (bool)Eval("Selected") ? "checked" : string.Empty %>
                                            onclick="<%# GetScript((OperationType)Eval("Value")) %>" />
                                    </td>
                                    <td class="iconCell">
                                        <img src="<%# Eval("ImageUrl") %>" alt="Operation Type <%# Eval("Name") %>">
                                    </td>
                                    <td class="textCell">
                                        <div class="ipsla_WP_SubTitle"><%#Eval("Name") %></div>
                                        <%# Eval("Description") %>
                                        <asp:HyperLink ID="hlUrl" runat="server"
                                            NavigateUrl='<%# Eval("Url") %>'
                                            Visible='<%# !string.IsNullOrEmpty((string)Eval("Url")) %>'>
                                          <%# Eval("UrlText") %>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>

                    </div>
                </ItemTemplate>       
            </asp:Repeater>
        </td>
    </tr>
</table>