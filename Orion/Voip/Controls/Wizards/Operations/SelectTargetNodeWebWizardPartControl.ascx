﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectTargetNodeWebWizardPartControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_Operations_SelectTargetNodeWebWizardPartControl" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web.Wizards.Operations" %>
<%@ Register src="ExternalNodesOption.ascx" tagname="ExternalNodesOption" tagprefix="ipsla" %>
<%@ Register src="BiDirectionalPathsOption.ascx" tagname="BiDirectionalPathsOption" tagprefix="ipsla" %>
<%@ Register Src="ChooseNodesTreeControl.ascx" tagname="ChooseNodesTreeControl" tagprefix="ipsla" %>
<%@ Register src="../../../Controls/NodeGroupingSelector.ascx" tagname="NodeGroupingSelector" tagprefix="ipsla" %>
<%@ Register src="~/Orion/Voip/Controls/Wizards/Operations/CredentialsCheckControl.ascx" tagname="CredentialsCheckControl" tagprefix="ipsla" %>

<div class="ipsla_WP_Title">
  <% if (this.WizardData.PathDefinitionMethod == PathDefinitionMethod.Simple && this.WizardData.WizardMode != WizardMode.MonitorExistingOperations)
       { %>
    <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_43, OperationsWizardSession.Current.Data.OperationTypeName)%>
    <% }
       else if (this.WizardData.PathDefinitionMethod != PathDefinitionMethod.Simple && this.WizardData.WizardMode != WizardMode.MonitorExistingOperations)
       { %>
    <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_44, OperationsWizardSession.Current.Data.OperationTypeName)%>
    <% }
       else if (this.WizardData.PathDefinitionMethod == PathDefinitionMethod.Simple && this.WizardData.WizardMode == WizardMode.MonitorExistingOperations)
       { %>
    <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_45, OperationsWizardSession.Current.Data.OperationTypeName)%>
    <% }
       else
       { %>
    <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_46, OperationsWizardSession.Current.Data.OperationTypeName)%>
    <% } %>
</div>
<div class="ipsla_WP_Description">
  <% if (this.WizardData.PathDefinitionMethod == PathDefinitionMethod.Simple)
     { %>
    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_47 %>
  <% } else { %>
    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_48 %>
  <% } %>
</div>
<div class="ipsla_WP_Description">
    <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_28, "<a href=\"ManageNodes/AddNodes.aspx\">", "</a>")%>
</div>

<div class="TreeGroupBy">
<%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_29%><br />
<ipsla:NodeGroupingSelector ID="groupBySelector" runat="server" OnSelectedValueChanged="groupBySelector_SelectedValueChanged" IncludeSiteProperties="true"/>
</div>

<div class="TreeContent">
    <ipsla:ChooseNodesTreeControl ID="ChooseNodesTree" runat="server">
        <WebService Path="~/Orion/Voip/Controls/Wizards/Operations/ChooseNodesAjaxTreeService.asmx" />
    </ipsla:ChooseNodesTreeControl>
</div>


<div id="operationNexusArea" runat="server" class="ipsla_WP_Description">
    <table border="0" cellpadding="0" cellspacing="5px" style="background-color: #FFF7CD;">
        <tr>
            <td valign="middle" style="text-align: center; width: 32px;">
                <img src="/orion/voip/images/lightbulb_important_text.gif" alt="<%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_69 %>" />
            </td>
            <td valign="middle">
                <div>
                    <%= String.Format( Resources.VNQMWebContent.VNQMWEBDATA_VB1_221, String.Format("<a href={0} >{1}</a>", Resources.VNQMWebContent.VNQMWEBDATA_VB1_222, Resources.VNQMWebContent.VNQMWEBCODE_VB1_136))%>
                </div>
            </td>
        </tr>
    </table>
</div>

<ipsla:BiDirectionalPathsOption ID="biDirectionalPathsOption" runat="server" />
<ipsla:ExternalNodesOption ID="externalNodesOption" runat="server" OnExternalNodesSelectedChanged="ExternalNodesOption_ExternalNodesSelectedChanged" />

<ipsla:CredentialsCheckControl ID="CredentialsCheckControl" runat="server" />
