﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Voip.Controls.Wizards.Operations");

Orion.Voip.Controls.Wizards.Operations.SelectTargetNodeWebWizardPartControl = function(element) {
    Orion.Voip.Controls.Wizards.Operations.SelectTargetNodeWebWizardPartControl.initializeBase(this, [element]);
}

Orion.Voip.Controls.Wizards.Operations.SelectTargetNodeWebWizardPartControl.prototype = {
    OnTooManyNodesSelected: function(message) {
        Ext.MessageBox.show({
            title: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}",
            icon: Ext.MessageBox.ERROR,
            msg: message,
            closable: false,
            buttons: Ext.MessageBox.OK
        });
    },

    OnLicenseCountExceeded: function(message) {
        Ext.MessageBox.show({
            title: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}",
            icon: Ext.MessageBox.ERROR,
            msg: String.format("@{R=VNQM.Strings;K=VNQMWEBJS_VB1_51;E=js}", message),
            closable: false,
            buttons: Ext.MessageBox.OK
        });
    },

    OnNoNodeSelected: function(message) {
        Ext.MessageBox.show({
            title: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}",
            icon: Ext.MessageBox.ERROR,
            msg: message,
            closable: false,
            buttons: Ext.MessageBox.OK
        });
    },

    initialize: function() {
        Orion.Voip.Controls.Wizards.Operations.SelectTargetNodeWebWizardPartControl.callBaseMethod(this, 'initialize');
        
        // Add custom initialization here
    },
    dispose: function() {        
        //Add custom dispose actions here
        Orion.Voip.Controls.Wizards.Operations.SelectTargetNodeWebWizardPartControl.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Controls.Wizards.Operations.SelectTargetNodeWebWizardPartControl.registerClass('Orion.Voip.Controls.Wizards.Operations.SelectTargetNodeWebWizardPartControl', Orion.Voip.Controls.Wizards.WizardFlowPartControl);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
