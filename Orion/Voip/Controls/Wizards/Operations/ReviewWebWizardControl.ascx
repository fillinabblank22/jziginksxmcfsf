﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReviewWebWizardControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_Operations_ReviewWebWizardControl" %>
<%@ Register Src="~/Orion/Voip/Controls/EditOperation.ascx" TagPrefix="ipsla" TagName="EditOperation" %>	
<%@ Register Src="~/Orion/Voip/Admin/ProgressControl.ascx" TagPrefix="ipsla" TagName="ProgressControl" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web.Wizards.Operations" %>

<style type="text/css">
    #editDialog .main td{white-space: normal}
    .GroupBox .ipsla_ProgressBox{width: 400px !important}
</style>

<asp:ScriptManagerProxy id="smp" runat="server">
    <Services>
        <asp:ServiceReference Path="~/Orion/Voip/Controls/Wizards/Operations/OperationDiscoveryService.asmx" />
    </Services>
</asp:ScriptManagerProxy>

<div class="ipsla_WP_Title">
  <% if (this.WizardData.WizardMode != WizardMode.MonitorExistingOperations)
     { %>
    <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_62, OperationsWizardSession.Current.Data.OperationTypeName)%>
  <% } else { %>
    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_63 %>
  <% } %>
</div>

<div id="operationScanArea" runat="server">
  <div class="ipsla_WP_Description">
    <img src="/orion/images/animated_loading_sm3_whbg.gif" alt="<%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_64 %>" class="waitingImg" />
    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_65 %>
  </div>
  <div class="GroupBox ipsla_ProgressBox">
    <ipsla:ProgressControl ID="ProgressControl" runat="server" CssClass="" ShowAnimatedGif="false" />
  </div>
</div>

<div id="operationListArea" runat="server">
<div id="operationDiscoveryExclusionsArea" runat="server" class="ipsla_WP_Description" style="display: none;">
  <table border="0" cellpadding="0" cellspacing="5px" style="background-color: #FFF7CD;">
    <tr>
      <td style="vertical-align: top;">
        <img src="/orion/voip/images/warning_yellow_message_32.gif" alt="<%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_69 %>" />
      </td>
      <td valign="top">
        <table class="operationDiscoveryReport" cellspacing="0" cellspacing="0" border="0">
          <tbody id="operationsNotFoundArea" runat="server" style="padding-top: 10em; display: none;">
            <tr>
              <td colspan="2" style="padding: 0.25em 0 0.2em 0;"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_66 %></td>
            </tr>
          </tbody>
          <tbody id="operationsNotFoundList" runat="server" style="display: none;"></tbody>
          <tbody id="operationsMonitoredArea" runat="server" style="display: none;">
            <tr>
              <td colspan="2" style="padding: 0.25em 0 0.2em 0;"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_67 %></td>
            </tr>
          </tbody>
          <tbody id="operationsMonitoredList" runat="server" style="display: none;"></tbody>
          <tbody id="operationsAutomaticArea" runat="server" style="display: none;">
            <tr>
              <td colspan="2" style="padding: 0.25em 0 0.2em 0;"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_68 %></td>
            </tr>
          </tbody>
          <tbody id="operationsAutomaticList" runat="server" style="display: none;"></tbody>
        </table>
      </td>
    </tr>
  </table>
</div>
<div id="operationEditHintsArea" runat="server" class="ipsla_WP_Description">
  <table border="0" cellpadding="0" cellspacing="5px" style="background-color: #FFF7CD;">
    <tr>
      <td valign="middle" style="text-align: center; width: 32px;">
        <img src="/orion/voip/images/lightbulb_important_text.gif" alt="<%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_69 %>" />
      </td>
      <td valign="middle">
        <div>
          <% if (this.WizardData.WizardMode != WizardMode.MonitorExistingOperations)
             { %>
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_70%>
          <% } else { %>
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_71%>
          <% } %>
        </div>
        <div>
            <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_72, "<b>", "</b>")%>
        </div>
      </td>
    </tr>
  </table>
</div>

<div id="gridAreaCaption" runat="server" class="ipsla_WP_Description">
  <% if (this.WizardData.WizardMode != WizardMode.MonitorExistingOperations)
     { %>
     <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_73, "<b>", "</b>")%>
  <% } else { %>
    <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_74, "<b>", "</b>")%> 
  <% } %>
</div>

<% if (this.WizardData.WizardMode == WizardMode.MonitorExistingOperations)
     { %>
<div runat="server" id="useTagAsNameAreaDiv" class="ipsla_WP_Description">
    <asp:CheckBox ID="useTagAsNameCheckBox" runat="server" />
    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_75 %> &#0187; <a  target="_blank" href="<%=  SolarWinds.Orion.IpSla.Web.HelpLocator.GetHelpUrl(SolarWinds.Orion.IpSla.Web.HelpFragments.WhatIsTag) %>"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_76 %></a>
</div>
<% } %>

<div id="gridArea" runat="server" class="ExtJsGrid"></div>

<div id="editor" class="x-hidden">
    <div class="x-window-header"><%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_43 %></div>
    <div class="x-window-body">
    <asp:UpdatePanel ID="upEditor" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hdIDs" runat="server" />
        <asp:PlaceHolder runat="server" ID="editDialogPanel" Visible="false" >
            <div id="editDialog">
                <ipsla:EditOperation runat="server" ID="editOp" />
            </div>
            <br />
        </asp:PlaceHolder>            
        <asp:PlaceHolder runat="server" ID="editDialogLoading" Visible="true" >
            <table style="width:100%; height:300px;"><tr><td style="text-align:center; vertical-align:middle;"> 
                <img src="/Orion/Voip/images/loading_gen_16x16.gif" alt="<%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_44 %>" style="padding-right:10px;" />
                    <%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_44 %>
            </td></tr></table>
        </asp:PlaceHolder>            
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnOpenEditor" />
        <asp:AsyncPostBackTrigger ControlID="btnSubmitEditor" />
        <asp:AsyncPostBackTrigger ControlID="btnCancelEditor" />
    </Triggers>
    </asp:UpdatePanel>
    <orion:LocalizableButton ID="btnOpenEditor" runat="server" LocalizedText="Edit" CausesValidation="false" OnClick="btnOpenEditor_Click"  CssClass="x-hidden"/>
    <orion:LocalizableButton ID="btnSubmitEditor" runat="server" LocalizedText="Submit" OnClick="btnSubmitEditor_Click" CssClass="x-hidden"/>
    <orion:LocalizableButton ID="btnCancelEditor" runat="server" LocalizedText="Cancel" CausesValidation="false" OnClick="btnCancelEditor_Click" CssClass="x-hidden"/>
    </div>
</div>

</div>