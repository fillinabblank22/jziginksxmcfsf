﻿using System;
using System.Linq;
using Resources;
using SolarWinds.Orion.IpSla.Web.Wizards.MediaGateway;

public partial class Orion_Voip_Controls_Wizards_MediaGateway_SummaryWizardPartControl : MediaGatewayAddWizardFlowPartControl
{
    protected static readonly string SummaryTitle = VNQMWebContent.VNQMWEBCODE_GK_5;
    protected static readonly string GatewayAddedText = VNQMWebContent.VNQMWEBCODE_GK_4;
    protected static readonly string GatewayNotAbletoAddText = VNQMWebContent.VNQMWEBCODE_GK_6;

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public bool? Success { get; private set; }
    public string Title { get; private set; }

    //VoIP Gateway ftp settings
    protected string FtpServer { get; private set; }
    protected string FtpPort { get; private set; }
    protected string FtpPassive { get; private set; }
    protected string FtpSecured { get; private set; }
    protected string FtpDirectory { get; private set; }
    protected string FtpCredentialsName { get; private set; }
    protected string FtpCredentialsUserName { get; private set; }
    protected string FtpFrequency { get; private set; }
    protected string FtpDelete { get; private set; }

    public override bool Initialize(MediaGatewayWizardData data)
    {
       
        if (data.NodesDetail.Any(success => success.Success == true))
        {
            GatewayAddedLstView.DataSource = data.NodesDetail.FindAll(success => success.Success == true);
            GatewayAddedLstView.DataBind();
        }
        else
        {
            GatewayAddedDiv.Visible = false;
        }
        if (data.NodesDetail.Any(success => success.Success == false))
        {
            NotaGatewayLstView.DataSource = data.NodesDetail.FindAll(success => success.Success == false);
            NotaGatewayLstView.DataBind();
        }
        else
        {
            NotaGatewaysDiv.Visible = false;

        }
        return true;
    }
    public override bool ModifyData(MediaGatewayWizardData data)
    {
        return true;
    }
}
