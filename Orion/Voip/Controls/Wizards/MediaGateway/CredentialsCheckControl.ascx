﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CredentialsCheckControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_MediaGateway_CredentialsCheckControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register src="~/Orion/Voip/Controls/Snmp/SnmpInfo.ascx" tagname="SnmpInfo" tagprefix="uc1" %>
<%@ Register src="~/Orion/Voip/Controls/Cli/CliConnectionControl.ascx" tagname="CliConnectionControl" tagprefix="uc1" %>
<%@ Register src="~/Orion/Voip/Admin/ProgressControl.ascx" tagname="ProgressControl" tagprefix="uc2" %>

<script type="text/javascript" language="javascript">
    Ext.onReady(function () {
        Ext.QuickTips.init();
    });
</script>

<style type="text/css">
    .x-ipsla-form-menu-item {width: 580px !important;}
    .x-ipsla-form-menu .x-panel-footer {width: auto !important; text-align: right; }
    .x-menu-plain {width:459px !important;}
</style>

<asp:ScriptManagerProxy ID="smp" runat="server">
    <Services>
        <asp:ServiceReference Path="~/Orion/Voip/Controls/Wizards/MediaGateway/CredentialsCheckControlService.asmx" />
    </Services>
    <Scripts>
        <asp:ScriptReference Path="~/Orion/Voip/js/FormMenu.js" />
        <asp:ScriptReference Path="~/Orion/Voip/js/ScriptServiceInvoker.js" />
        <asp:ScriptReference Path="~/Orion/Voip/js/ScriptServiceProxy.js" />
        <asp:ScriptReference Path="~/Orion/Voip/Admin/ManageNodes/WriteCredentialsErrorDialog.js" />
    </Scripts>
</asp:ScriptManagerProxy>

<div id="cliInfoElement" runat="server" class="x-hidden" >
    <div class="ipsla_MaskArea">
    <table class="ipsla_EditNodeBox" border="0" cellpadding="0" cellspacing="0" >
        <uc1:CliConnectionControl ID="cliInfo" runat="server" />
    </table>
    </div>
</div>

<div id="snmpInfoElement" runat="server" class="x-hidden">
    <uc1:SnmpInfo ID="snmpInfo" runat="server" />
</div>

<div id="pbarBody" class="x-hidden">
    <div class="x-window-body">
        <uc2:ProgressControl ID="ProgressControl" runat="server" CssClass="ipsla_ProgressBox" TitleWidth="20%" />
    </div>
</div>
