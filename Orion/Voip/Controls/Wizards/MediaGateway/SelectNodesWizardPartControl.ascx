﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectNodesWizardPartControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_MediaGateway_SelectNodesWizardPartControl" %>
<%@ Register TagPrefix="ipsla" TagName="NodesBrowser" Src="~/Orion/Voip/Controls/NodesBrowser/NodesBrowser.ascx" %>
<%@ Register src="~/Orion/Voip/Controls/Wizards/MediaGateway/CredentialsCheckControl.ascx" tagname="CredentialsCheckControl" tagprefix="ipsla" %>

<div class="ipsla_WP_Title"><%= Title %></div>
<div class="ipsla_WP_Description"><%= Description %></div>
<div class="ipsla_WP_Description"><%= AddNodesText %></div>

<script type="text/javascript">
    function VoipValidateNodes_<%= this.ClientID %>(sender, args) {
        if (SW.VoIP.NodesBrowser.GetSelectedNodeIds().length == 0) {
            VoipAddMediaGatewayShowError('<%= NoNodeSelectedError %>');
            args.IsValid = false;
            return;
        }

        args.IsValid = true;
    }
</script>

<ipsla:NodesBrowser runat="server" ID="nodesBrowser" IsMultiSelect="true" />

<asp:CustomValidator runat="server" ID="nodesValidator" ValidateEmptyText="True"></asp:CustomValidator>

<ipsla:CredentialsCheckControl ID="CredentialsCheckControl" runat="server" />
