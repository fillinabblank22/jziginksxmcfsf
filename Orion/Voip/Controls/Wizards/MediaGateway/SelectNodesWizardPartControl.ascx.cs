﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using SolarWinds.Orion.IpSla.Common.Settings;
using SolarWinds.Orion.IpSla.Contracts;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.IpSla.Web.Wizards.MediaGateway;

public partial class Orion_Voip_Controls_Wizards_MediaGateway_SelectNodesWizardPartControl : MediaGatewayAddWizardFlowPartControl
{
    protected static readonly string Title = Resources.VNQMWebContent.VNQMWEBCODE_GK_1;
    protected static readonly string Description = Resources.VNQMWebContent.VNQMWEBCODE_GK_2;
    protected static readonly string NoNodeSelectedError = Resources.VNQMWebContent.VNQMWEBCODE_GK_3;
    protected static readonly string NodeDoesntExistError = Resources.VNQMWebContent.VNQMWEBCODE_VB1_75;
    protected static readonly string CantValidateError = Resources.VNQMWebContent.VNQMWEBCODE_VB1_76;
    protected static readonly string ValidationFailedErrorFormat = Resources.VNQMWebContent.VNQMWEBCODE_VB1_77;
    protected static readonly string AddNodesText = String.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_78, "<a href=\"/Orion/Nodes/Default.aspx\">", "</a>");
    
    protected void Page_Load(object sender, EventArgs e)
    {
        nodesValidator.ClientValidationFunction = "VoipValidateNodes_" + this.ClientID;
        this.CredentialsCheckControl.WindowTitle = Resources.VNQMWebContent.VNQMWEBCODE_VB1_33;
        this.CredentialsCheckControl.WindowMessage = Resources.VNQMWebContent.VNQMWEBCODE_GK_9;
        
    }

    public override bool Initialize(MediaGatewayWizardData data)
    {
       if (data.NodesDetail.Count >0)
       {
        nodesBrowser.SelectedNodes =
               data.NodesDetail.Select(
                   a => new Orion_Voip_Controls_NodesBrowser_NodesBrowser.Node {ID = a.SelectedNode, Name = a.Message}).
                   ToList();
       }

        // hide nodes that are already Mediagateway
        DataTable dt = DALHelper.ExecuteQuery("SELECT NodeID FROM Orion.IpSla.VoipGateways");
        if (dt != null)
        {
            List<int> excludedNodes = new List<int>();
            foreach (DataRow row in dt.Rows)
            {
                excludedNodes.Add((int)row["NodeID"]);
            }
            nodesBrowser.ExcludedNodesIds = excludedNodes;
        }

        return true;
    }

    public override bool ModifyData(MediaGatewayWizardData data)
    {
        if (nodesBrowser.SelectedNodes == null || !nodesBrowser.SelectedNodes.Any())
        {
            ShowError(NoNodeSelectedError);
            return false;
        }

        IEnumerable<NpmNode> nodes = NpmNodesDAL.GetById(nodesBrowser.SelectedNodes.Select(ids=> ids.ID));
        if (nodes == null)
        {
            ShowError(NodeDoesntExistError);
            return false;
        }
/*        
        if (IpSlaSettings.IsDemoServer)
        {
            return true;
        }
*/ 
        MediaGatewayWizardData sessionData = MediaGatewayWizardSession.Current.Data;
        var nodeCLIDetails = sessionData.NodesDetail.ToDictionary(x => x.SelectedNode, x => x.IsCliIssue);
        using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>())
        {
           IEnumerable<BusinessLayerResult> results = proxy.VerifyMediaGatewaysNodes(nodes.Select(ids=>ids.ID));
            if (!results.Any())
            {
                ShowError(CantValidateError);
                return false;
            }
            data.NodesDetail.Clear();
            foreach(var result in results )
            {
                string message =result.Message;
                bool? isGateway=result.IsMediaGateway;
                if (nodeCLIDetails.Count > 0)
                {
                    if (nodeCLIDetails.ContainsKey(result.NodeId))
                    {
                        bool isCLICredentialIssue = nodeCLIDetails[result.NodeId];
                        if (isCLICredentialIssue)
                        {
                            if (!Convert.ToBoolean(isGateway))
                            {
                                message = string.Format("{0}, {1}." ,message, Resources.VNQMWebContent.VNQMWEBCODE_GK_8); 
                            }
                            else
                            {
                                message = string.Format("{0} - {1}.", message, Resources.VNQMWebContent.VNQMWEBCODE_GK_8);
                            }
                            isGateway = !isCLICredentialIssue;
                        }
                    }
                }
                data.NodesDetail.Add(new MediaGatewayWizardData.SelectedNodeDetails(result.NodeId, isGateway, message));
            }
        }

      return true;
    }
}