﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Voip.Controls.Wizards.MediaGateway");

Orion.Voip.Controls.Wizards.MediaGateway.MediaGatewayWizardControlContainer = function (element) {
    Orion.Voip.Controls.Wizards.MediaGateway.MediaGatewayWizardControlContainer.initializeBase(this, [element]);
};

Orion.Voip.Controls.Wizards.MediaGateway.MediaGatewayWizardControlContainer.prototype = {

    CancelWizard: function(cancelFunc) {
        Ext.MessageBox.show({
            title: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}",
            icon: Ext.MessageBox.QUESTION,
            msg: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_50;E=js}',
            closable: false,
            buttons: Ext.MessageBox.YESNO,
            fn: function(btn, value) {
                if (btn == 'yes') {
                    cancelFunc();
                }
            }
        });
    },

    initialize: function() {
        Orion.Voip.Controls.Wizards.MediaGateway.MediaGatewayWizardControlContainer.callBaseMethod(this, 'initialize');
        // Add custom initialization here
    },
    dispose: function() {
        // Add custom dispose actions here
        Orion.Voip.Controls.Wizards.MediaGateway.MediaGatewayWizardControlContainer.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Controls.Wizards.MediaGateway.MediaGatewayWizardControlContainer.registerClass('Orion.Voip.Controls.Wizards.MediaGateway.MediaGatewayWizardControlContainer', Orion.Voip.Controls.Wizards.WizardControl);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
