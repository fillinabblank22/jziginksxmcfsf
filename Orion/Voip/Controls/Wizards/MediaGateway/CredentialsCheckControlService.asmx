﻿<%@ WebService Language="C#" Class="Orion.Voip.Controls.Wizards.MediaGateway.CredentialsCheckControlService" %>

using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Contracts;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.IpSla.Web.Json;
using SolarWinds.Orion.IpSla.Web.WebServices;
using SolarWinds.Orion.IpSla.Web.Wizards.MediaGateway;

namespace Orion.Voip.Controls.Wizards.MediaGateway
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class CredentialsCheckControlService : System.Web.Services.WebService
    {
        private static readonly Log Log = new Log();

        public class ProgressInfo
        {
            public bool Finished { get; set; }
            public int NodesCount { get; set; }
            public int PendingNodesCount { get; set; }
            public int CredentialsIssueNodesCount { get; set; }
        } 
        
        [WebMethod(EnableSession = true)]
        public PartialResponse<ProgressInfo> GetProgress()
        {
            using (Log.Block())
            {
                var session = MediaGatewayWizardSession.Current;
                var data = session != null ? session.Data : null;
                var credentialsVerifier = data != null ? data.CredentialsVerifier : null;

                var results = credentialsVerifier != null ? credentialsVerifier.GetStatus(null) : new CredentialsCheckItem[0];

                var progressInfo = new ProgressInfo
                    {
                        NodesCount = results.Length,
                        PendingNodesCount = results.Count(x => x.InProgress),
                        CredentialsIssueNodesCount = results.Count(x => !x.InProgress && x.Error),
                    };
                progressInfo.Finished = progressInfo.PendingNodesCount == 0;

                Log.DebugFormat("Get progress request handled. [Nodes={0},PendingNodes={1},NodesWithIssues={2},Finished={3}]",
                    progressInfo.NodesCount, progressInfo.PendingNodesCount, progressInfo.CredentialsIssueNodesCount, progressInfo.Finished);

                return new PartialResponse<ProgressInfo>
                    {
                        ResponseData = progressInfo,
                        IsLastResult = progressInfo.Finished,
                    };
            }
        }

        [WebMethod(EnableSession = true)]
        public PartialResponse<PageableDataTable> GetResults(Dictionary<string, object> parameters)
        {
            using (Log.Block())
            {
                return GetItemStatus(null, true);
            }
        }

        [WebMethod(EnableSession = true)]
        public PartialResponse<PageableDataTable> Restart(IEnumerable<int> nodeIds, bool restart)
        {
            using (Log.Block())
            {
                if (restart && nodeIds != null)
                {
                    var session = MediaGatewayWizardSession.Current;
                    var data = session != null ? session.Data : null;
                    var credentialsVerifier = data != null ? data.CredentialsVerifier : null;

                    if (credentialsVerifier != null)
                    {
                        credentialsVerifier.Restart(nodeIds);
                    }
                }

                return GetItemStatus(nodeIds, false);
            }
        }

        private static PartialResponse<PageableDataTable> GetItemStatus(IEnumerable<int> nodeIds, bool filterValid)
        {
            var session = MediaGatewayWizardSession.Current;
            var data = session != null ? session.Data : null;
            var credentialsVerifier = data != null ? data.CredentialsVerifier : null;
            var nodeDetails = data != null ? data.NodesDetail  : null;
            var results = credentialsVerifier != null ? credentialsVerifier.GetStatus(nodeIds) : new CredentialsCheckItem[0];

            var ret = new PartialResponse<PageableDataTable> { IsLastResult = true };

            var nodes = NpmNodesDAL.GetById(results.Select(x => x.NodeId));

            var table = new DataTable();
            table.Columns.Add("NodeId", typeof(int));
            table.Columns.Add("NodeName", typeof(string));
            table.Columns.Add("IpAddress", typeof(string));
            table.Columns.Add("EngineId", typeof(int));
            table.Columns.Add("Message", typeof(string));
            table.Columns.Add("Error", typeof(bool));
            table.Columns.Add("InProgress", typeof(bool));

            foreach (var result in results)
            {
                if (filterValid && !result.Error)
                    continue;

                var nodeId = result.NodeId;
                var node = nodes.First(x => x.ID == nodeId);
                if (nodeDetails != null)
                {
                    var sesseionNodeDetail = nodeDetails.First(x => x.SelectedNode == nodeId);
                    sesseionNodeDetail.IsCliIssue = result.Error;
                }
                
                table.Rows.Add(
                    result.NodeId,
                    node.Name,
                    node.IpAddress.ToString(),
                    node.EngineId,
                    result.InProgress ? string.Empty : result.Message,
                    result.Error, 
                    result.InProgress);
                if (result.InProgress)
                    ret.IsLastResult = false;
            }

            Log.DebugFormat("Get status request handled. [Results={0},Finished={1}]",
                results.Length, ret.IsLastResult);

            ret.ResponseData = new PageableDataTable(table, results.Length);
            return ret;
        }
    }
}
