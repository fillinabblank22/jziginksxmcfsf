﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MediaGatewayWizardControlContainer.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_MediaGateway_MediaGatewayWizardControlContainer" %>
<%@ Register Assembly="SolarWinds.Orion.IpSla.Web" TagPrefix="ipsla" Namespace="SolarWinds.Orion.IpSla.Web.UI" %>

<%@ Register assembly="SolarWinds.Orion.IpSla.Web" namespace="SolarWinds.Orion.IpSla.Web.Wizards" tagprefix="cc1" %>

<div class="WizardControl" id="AddMediaGatewayWizard">
<h1><%= WizardTitle %></h1>

<script type="text/javascript">
    function VoipAddMediaGatewayShowError(message) {
        Ext.MessageBox.show({
            title: '<%= ErrorDialogTitle %>',
            icon: Ext.MessageBox.ERROR,
            msg: message,
            closable: false,
            buttons: Ext.MessageBox.OK
        });
    }

    function VoipAddMediaGatewayShowLoadMask(message) {
        var mask = new Ext.LoadMask(Ext.getBody(),
            { msg: message });
        mask.show();
    }
</script>

<ipsla:WizardControl ID="wizardControl" runat="server"
    OnScriptDescriptorInitializing="wizardControl_ScriptDescriptorInitializing"
    OnScriptReferencesRequired="wizardControl_ScriptReferencesRequired">
    <CustomButtonsTemplate>
        <orion:LocalizableButton ID="btnBack" runat="server" DisplayType="Secondary" LocalizedText="Back" CausesValidation="False" OnClick="btnBack_Click"/>
        <orion:LocalizableButton ID="btnNext" runat="server" DisplayType="Primary" LocalizedText="Next" CausesValidation="True" OnClick="btnNext_Click" />
        <orion:LocalizableButton ID="btnAddMediaGateway" runat="server" DisplayType="Primary" LocalizedText="Next" CausesValidation="True" OnClick="btnAddMediaGateway_Click" />
        <orion:LocalizableButton ID="btnCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" CausesValidation="False" OnClick="btnCancel_Click" />
    </CustomButtonsTemplate>
</ipsla:WizardControl>
<%-- This validator is here only to generate Page_ClientValidate() for each wizard step. 
btnNext calls Page_ClientValidate and if there is no validator, this function is not generated and error occurs. --%>
<asp:CustomValidator runat="server" ID="dummyValidator"></asp:CustomValidator>
</div>