﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SummaryWizardPartControl.ascx.cs"
    Inherits="Orion_Voip_Controls_Wizards_MediaGateway_SummaryWizardPartControl" %>
<div class="ipsla_WP_Title">
    <%= Title %></div>
<div class="ipsla_WP_Title">
    <%= SummaryTitle %>
</div>
<div class="ipsla_WP_Title" id="GatewayAddedDiv" runat="server">
    <%= GatewayAddedText%></div>
<asp:ListView ID="GatewayAddedLstView" runat="server" class="sw-voip-add-MG-summary">
    <LayoutTemplate>
        <ul>
            <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
        </ul>
    </LayoutTemplate>
    <ItemTemplate>
        <li class="sw-voip-add-MG-summary">
            <img src="/Orion/Voip/images/icon_add.gif" style="vertical-align: middle;" />
            <%#Eval("Message")%>
        </li>
    </ItemTemplate>
</asp:ListView>
<div class="ipsla_WP_Title" id="NotaGatewaysDiv" runat="server">
    <%= GatewayNotAbletoAddText%></div>
<asp:ListView ID="NotaGatewayLstView" runat="server">
    <LayoutTemplate>
        <ul>
            <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
        </ul>
    </LayoutTemplate>
    <ItemTemplate>
        <li class="sw-voip-add-MG-summary">
            <img src="/Orion/Voip/images/icon_delete.gif" style="vertical-align: middle;" />
            <%#Eval("Message")%>
        </li>
    </ItemTemplate>
 </asp:ListView>
