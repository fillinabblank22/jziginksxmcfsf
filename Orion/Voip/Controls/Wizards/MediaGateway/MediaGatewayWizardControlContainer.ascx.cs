﻿using System;
using System.Web.UI;
using SolarWinds.Orion.IpSla.Common.Settings;
using SolarWinds.Orion.IpSla.Web.UI;
using SolarWinds.Orion.IpSla.Web.Wizards.MediaGateway;
using SolarWinds.Orion.IpSla.Web.Wizards;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Voip_Controls_Wizards_MediaGateway_MediaGatewayWizardControlContainer : System.Web.UI.UserControl
{
    private static readonly string AddMediaGatewayButtonText = Resources.VNQMWebContent.VNQMWEBDATA_GK_6;
    protected static readonly string ErrorDialogTitle = Resources.VNQMWebContent.VNQMWEBCODE_AK1_3;
    public static readonly string AddingMediaGatewayText = Resources.VNQMWebContent.VNQMWEBDATA_GK_7;

    private IWizardSession session;
    private MediaGatewayAddWizardFlowPart currentPart;

    public string ReturnUrl
    {
        get { return this.wizardControl.ReturnUrl; }
        set { this.wizardControl.ReturnUrl = value; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.wizardControl.WizardSession = MediaGatewayWizardSession.Current;
        this.wizardControl.WizardPartControlFactory += this.GetEditorControl;
        this.wizardControl.Initialize();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        session = MediaGatewayWizardSession.Current;
        currentPart = session.CurrentPart as MediaGatewayAddWizardFlowPart;
        if (currentPart == null)
        {
            throw new ArgumentException("Wizard part is not of MediaGatewayAddWizardFlowPart type.");
        }

        if (!string.IsNullOrEmpty(currentPart.NextStepLoadingMessage))
        {
            btnNext.OnClientClick = string.Format("if (Page_ClientValidate()) {{ VoipAddMediaGatewayShowLoadMask('{0}'); }} else {{ return false; }}",
                    currentPart.NextStepLoadingMessage);
        }
        if (IpSlaSettings.IsDemoServer)
        {
            //btnAddMediaGateway.Enabled = false;
            btnAddMediaGateway.OnClientClick = "if (CheckDemoMode()) return false;";
        }
        else
        {

            btnAddMediaGateway.OnClientClick = string.Format("if (Page_ClientValidate()) {{ VoipAddMediaGatewayShowLoadMask('{0}'); }} else {{ return false; }}",
                    AddingMediaGatewayText);
        }
        if (currentPart.WarnBeforeCancel)
        {
            this.btnCancel.OnClientClick = "$find('" + this.wizardControl.ClientID + "').CancelWizard(function(){" + this.Page.ClientScript.GetPostBackEventReference(this.btnCancel, "Cancel") + ";});return false;";
        }

        btnAddMediaGateway.LocalizedText = CommonButtonType.CustomText;
        btnAddMediaGateway.Text = AddMediaGatewayButtonText;
        btnAddMediaGateway.Visible = (currentPart == SummaryWizardPart.Instance);

        btnBack.Visible = currentPart.AllowMoveBack;
        btnNext.Visible = currentPart.AllowMoveNext;
        btnCancel.Visible = (currentPart != SummaryWizardPart.Instance);
    }

    protected string WizardTitle
    {
        get { return Resources.VNQMWebContent.VNQMWEBDATA_GK_4; }
    }

    protected void wizardControl_ScriptDescriptorInitializing(object sender, ScriptControlDescriptorEventArgs e)
    {
        e.Descriptor.Type = "Orion.Voip.Controls.Wizards.MediaGateway.MediaGatewayWizardControlContainer";
    }

    protected void wizardControl_ScriptReferencesRequired(object sender, ScriptReferencesEventArgs e)
    {
        e.AddReference(new ScriptReference(this.ResolveUrl("MediaGatewayWizardControlContainer.js")));
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        this.wizardControl.PrevStep();
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        this.wizardControl.NextStep();
    }

    protected void btnAddMediaGateway_Click(object sender, EventArgs e)
    {
        this.wizardControl.NextStep();
        bool? success = ((MediaGatewayWizardData)session.Data).Success;
        if (success == true)
        {
            this.wizardControl.AbortWizard();
            Response.Redirect(this.Page.ResolveUrl(this.ReturnUrl));
        }
        else if (success == false)
        {
            this.wizardControl.RedirectToCurrentWizardPage();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        this.wizardControl.AbortWizard();
        Response.Redirect(this.Page.ResolveUrl(this.ReturnUrl));
    }

    protected WizardFlowPartControl GetEditorControl(string wizardPartName)
    {
        string controlFileName;
        switch (wizardPartName)
        {
            case SelectNodesWizardPart.NAME:
                controlFileName = "SelectNodesWizardPartControl.ascx";
                break;
           case SummaryWizardPart.NAME:
                controlFileName = "SummaryWizardPartControl.ascx";
                break;
            default:
                throw new NotSupportedException(string.Format("Wizard part '{0}' not found", wizardPartName));
        }

        const string path = "~/Orion/Voip/Controls/Wizards/MediaGateway/";
        var wp = (WizardFlowPartControl)Page.LoadControl(path + controlFileName);

        return wp;
    }
}