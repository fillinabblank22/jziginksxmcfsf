﻿using System;
using System.Web.UI;
using SolarWinds.Orion.IpSla.Common.Settings;
using SolarWinds.Orion.IpSla.Web.UI;
using SolarWinds.Orion.IpSla.Web.Wizards.CallManager;
using SolarWinds.Orion.IpSla.Web.Wizards;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Voip_Controls_Wizards_CallManager_CallManagerWizardControlContainer : System.Web.UI.UserControl
{
    private static readonly string AddCallManagerButtonText = Resources.VNQMWebContent.VNQMWEBDATA_VB1_124;
    protected static readonly string ErrorDialogTitle = Resources.VNQMWebContent.VNQMWEBCODE_AK1_3;
    public static readonly string AddingCallManagerText = Resources.VNQMWebContent.VNQMWEBDATA_VB1_125;

    private IWizardSession session;
    private CallManagerAddWizardFlowPart currentPart;

    public string ReturnUrl
    {
        get { return this.wizardControl.ReturnUrl; }
        set { this.wizardControl.ReturnUrl = value; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.wizardControl.WizardSession = CallManagerWizardSession.Current;
        this.wizardControl.WizardPartControlFactory += this.GetEditorControl;
        this.wizardControl.Initialize();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        session = CallManagerWizardSession.Current;
        currentPart = session.CurrentPart as CallManagerAddWizardFlowPart;
        if (currentPart == null)
        {
            throw new ArgumentException("Wizard part is not of CallManagerAddWizardFlowPart type.");
        }

        if (!string.IsNullOrEmpty(currentPart.NextStepLoadingMessage))
        {
            btnNext.OnClientClick = string.Format("if (Page_ClientValidate()) {{ VoipAddCallManagerShowLoadMask('{0}'); }} else {{ return false; }}",
                    currentPart.NextStepLoadingMessage);
        }
        if (IpSlaSettings.IsDemoServer)
        {
            btnAddCallManager.Enabled = false;
        }
        else
        { 

            btnAddCallManager.OnClientClick = string.Format("if (Page_ClientValidate()) {{ VoipAddCallManagerShowLoadMask('{0}'); }} else {{ return false; }}",
                    AddingCallManagerText);
        }
        if (currentPart.WarnBeforeCancel)
        {
            this.btnCancel.OnClientClick = "$find('" + this.wizardControl.ClientID + "').CancelWizard(function(){" + this.Page.ClientScript.GetPostBackEventReference(this.btnCancel, "Cancel") + ";});return false;";
        }

        btnAddCallManager.LocalizedText = CommonButtonType.CustomText;
        btnAddCallManager.Text = AddCallManagerButtonText;
        btnAddCallManager.Visible = (currentPart == SummaryWizardPart.Instance);

        btnBack.Visible = currentPart.AllowMoveBack;
        btnNext.Visible = currentPart.AllowMoveNext;
        btnCancel.Visible = currentPart.AllowCancel;
    }

    protected string WizardTitle
    {
        get { return Resources.VNQMWebContent.VNQMWEBDATA_VB1_124; }
    }

    protected void wizardControl_ScriptDescriptorInitializing(object sender, ScriptControlDescriptorEventArgs e)
    {
        e.Descriptor.Type = "Orion.Voip.Controls.Wizards.CallManager.CallManagerWizardControlContainer";
    }

    protected void wizardControl_ScriptReferencesRequired(object sender, ScriptReferencesEventArgs e)
    {
        e.AddReference(new ScriptReference(this.ResolveUrl("CallManagerWizardControlContainer.js")));
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        this.wizardControl.PrevStep();
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        this.wizardControl.NextStep();
    }

    protected void btnAddCallManager_Click(object sender, EventArgs e)
    {
        this.wizardControl.NextStep();
        bool? success = ((CallManagerWizardData) session.Data).Success;
        if (success == true)
        {
            this.wizardControl.AbortWizard();
            Response.Redirect(this.Page.ResolveUrl(this.ReturnUrl));
        }
        else if (success == false)
        {
            this.wizardControl.RedirectToCurrentWizardPage();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        this.wizardControl.AbortWizard();
        Response.Redirect(this.Page.ResolveUrl(this.ReturnUrl));
    }

    protected WizardFlowPartControl GetEditorControl(string wizardPartName)
    {
        string controlFileName;
        switch (wizardPartName)
        {
            case SelectNodesWizardPart.NAME:
                controlFileName = "SelectNodesWizardPartControl.ascx";
                break;
            case DefineMonitoringWizardPart.NAME:
                controlFileName = "DefineMonitoringWizardPartControl.ascx";
                break;
            case DefineAvayaMonitoringWizardPart.NAME:
                controlFileName = "AvayaDefineMonitoringWizardPartControl.ascx";
                break;
            case DefineAxlWizardPart.NAME:
                controlFileName = "DefineAxlWizardPartControl.ascx";
                break;
            case DefineFtpWizardPart.NAME:
                controlFileName = "DefineFtpWizardPartControl.ascx";
                break;
            case SummaryWizardPart.NAME:
                controlFileName = "SummaryWizardPartControl.ascx";
                break;
            case DefineCliWizardPart.NAME:
                controlFileName = "DefineCliCredentialsWizardPartControl.ascx";
                break;
            default:
                throw new NotSupportedException(string.Format("Wizard part '{0}' not found", wizardPartName));
        }

        const string path = "~/Orion/Voip/Controls/Wizards/CallManager/";
        var wp = (WizardFlowPartControl)Page.LoadControl(path + controlFileName);
        
        return wp;
    }
}
