﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CallManagerWizardControlContainer.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_CallManager_CallManagerWizardControlContainer" %>
<%@ Register Assembly="SolarWinds.Orion.IpSla.Web" TagPrefix="ipsla" Namespace="SolarWinds.Orion.IpSla.Web.UI" %>

<div class="WizardControl" id="AddCallManagerWizard">
<h1><%= WizardTitle %></h1>

<script type="text/javascript">
    function VoipAddCallManagerShowError(message) {
        Ext.MessageBox.show({
            title: '<%= ErrorDialogTitle %>',
            icon: Ext.MessageBox.ERROR,
            msg: message,
            closable: false,
            buttons: Ext.MessageBox.OK
        });
    }

    function VoipAddCallManagerShowLoadMask(message) {
        var mask = new Ext.LoadMask(Ext.getBody(), 
        { msg: message });
        mask.show();
    }
    function GetNextButtonClientID() {
        var nextButtonClientId = '<%=btnNext.ClientID %>';
        return nextButtonClientId;
    }
    function GetBackButtonClientID() {
        var backButtonClientId = '<%=btnBack.ClientID %>';
        return backButtonClientId;
    }
    function GetCancelButtonClientID() {
        var backButtonClientId = '<%=btnCancel.ClientID %>';
        return backButtonClientId;
    }
</script>

<ipsla:WizardControl ID="wizardControl" runat="server"
    OnScriptDescriptorInitializing="wizardControl_ScriptDescriptorInitializing"
    OnScriptReferencesRequired="wizardControl_ScriptReferencesRequired">
    <CustomButtonsTemplate>
        <orion:LocalizableButton ID="btnBack" runat="server" DisplayType="Secondary" LocalizedText="Back" CausesValidation="False" OnClick="btnBack_Click"/>
        <orion:LocalizableButton ID="btnNext" runat="server" DisplayType="Primary" LocalizedText="Next" CausesValidation="True" OnClick="btnNext_Click" />
        <orion:LocalizableButton ID="btnAddCallManager" runat="server" DisplayType="Primary" LocalizedText="Next" CausesValidation="True" OnClick="btnAddCallManager_Click" />
        <orion:LocalizableButton ID="btnCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" CausesValidation="False" OnClick="btnCancel_Click" />
    </CustomButtonsTemplate>
</ipsla:WizardControl>
<%-- This validator is here only to generate Page_ClientValidate() for each wizard step. 
btnNext calls Page_ClientValidate and if there is no validator, this function is not generated and error occurs. --%>
<asp:CustomValidator runat="server" ID="dummyValidator"></asp:CustomValidator>
</div>