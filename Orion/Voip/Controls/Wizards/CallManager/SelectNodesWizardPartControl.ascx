﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectNodesWizardPartControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_CallManager_SelectNodesWizardPartControl" %>
<%@ Register TagPrefix="ipsla" TagName="NodesBrowser" Src="~/Orion/Voip/Controls/NodesBrowser/NodesBrowser.ascx" %>

<div class="ipsla_WP_Title"><%= Title %></div>
<div class="ipsla_WP_Description"><%= Description %></div>
<div class="ipsla_WP_Description"><%= CiscoDescription %> <a href="<%= CiscoBannerLink %>" class="VNQM_SNMPHelpLink" target="_blank"><%= CiscoBannerLinkText %></a></div>
<div class="ipsla_WP_Description"><%= AvayaDescription %> <a href="<%= AvayaBannerLink %>" class="VNQM_SNMPHelpLink" target="_blank"><%= AvayaBannerLinkText %></a></div>
<div class="ipsla_WP_Description"><%= AddNodesText %></div>

<script type="text/javascript">
    function VoipValidateNodes_<%= this.ClientID %>(sender, args) {
        if (SW.VoIP.NodesBrowser.GetSelectedNodeIds().length == 0) {
            VoipAddCallManagerShowError('<%= NoNodeSelectedError %>');
            args.IsValid = false;
            return;
        }

        args.IsValid = true;
    }
</script>

<ipsla:NodesBrowser runat="server" ID="nodesBrowser" />

<asp:CustomValidator runat="server" ID="nodesValidator" ValidateEmptyText="True"></asp:CustomValidator>