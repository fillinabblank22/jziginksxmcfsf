﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Web.UI;
using SolarWinds.Orion.IpSla.Common.Settings;
using SolarWinds.Orion.IpSla.Contracts;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.Wizards.CallManager;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;

public partial class Orion_Voip_Controls_Wizards_CallManager_DefineCliCredentialsWizardPartControl : CallManagerAddWizardFlowPartControl
{
    protected static readonly string TitleFormat = "Add CLI Credentials for '{0}'";
    protected static readonly string Description = "Adding CLI credentials allows read-only access to region and location information for call managers and phones.";
    protected static readonly string Description2 = "Region and location information for call managers and VoIP phones will not be available if you do not provide CLI credentials.";
    protected static readonly string CliCredentialsHelpLink = "What are CLI Credentials";
    protected static readonly string CliCredentialsInvalidErrorFormat = "Defined CLI credentials are not valid for Call Manager \"{0}\"";

    protected string CliCredentialsHelp = string.Empty;
    private const string helpFragment = "OrionIPSLAManagerAGAddingorDeletingCiscoCallManagerDevices";

    protected void Page_Load(object sender, EventArgs e)
    {
        CliCredentialsHelp = HelpLocator.GetHelpUrl(helpFragment);
    }

    public string Title { get; private set; }

    private static CallManagerWizardData WizardData
    {
        get
        {
            var session = CallManagerWizardSession.Current;
            return session != null ? session.Data : null;
        }
    }

    private static CredentialsVerifier CredentialsVerifier
    {
        get
        {
            var data = WizardData;
            //TODO
           //return data != null ? data.CredentialsVerifier : null;
            return null;
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        var credentialsVerifier = CredentialsVerifier;
        if (credentialsVerifier != null)
        {
           // ShowProgress();
        }
        base.OnPreRender(e);
    }

    public override bool Initialize(CallManagerWizardData data)
    {
        Title = string.Format(TitleFormat,
            (data.SelectedNode != null)
            ? data.SelectedNode.Name
            : string.Empty);

        if (data.SelectedNode != null)
        {
            cliSettings.ValidationIp = data.SelectedNode.IpAddress.ToString();
            cliSettings.PreselectedCredentialsId = data.CliConnectionInfo.Credentials.ID ?? 0;
            cliSettings.NodeIds = new int[] { data.SelectedNode.ID };
        }

        return true;
    }

    public override bool ModifyData(CallManagerWizardData data)
    {
        data.CliConnectionInfo.Credentials.Name = cliSettings.CredentialsName;
        data.CliConnectionInfo.Credentials.Username = cliSettings.Username;
        data.CliConnectionInfo.Protocol = cliSettings.Protocol;
        data.CliConnectionInfo.Timeout = cliSettings.Timeout;
        data.CliConnectionInfo.Port = cliSettings.Port;

        if (IpSlaSettings.IsDemoServer)
        {
            return true;
        }

        // reset success flag before final step
        if (cliSettings.CredentialsId == 0)
        {
            data.CliConnectionInfo.Credentials.ID = CreateCliCredentials(cliSettings.CredentialsName,
                                                                         cliSettings.Username,
                                                                         cliSettings.Password,
                                                                         cliSettings.EnablePassword,
                                                                         cliSettings.EnableLevel,
                                                                         data.SelectedNode.ID,
                                                                         cliSettings.Port,
                                                                         cliSettings.Timeout,
                                                                         cliSettings.Protocol);


        }
        else
        {
            data.CliConnectionInfo.Credentials.ID = cliSettings.CredentialsId;
            UpdateCliCredentials(cliSettings.CredentialsId, data.SelectedNode.ID, cliSettings.Port,
                                 cliSettings.Timeout,
                                 cliSettings.Protocol);
        }

        bool isValidCred = ValidateCliCredentials(data.CliConnectionInfo.Credentials.ID.Value,
                                                  data.SelectedNode.EngineId ?? 1,
                                                  data.SelectedNode.IpAddress,
                                                  data.CliConnectionInfo.Port,
                                                  data.CliConnectionInfo.Timeout,
                                                  data.CliConnectionInfo.Protocol);
        if (!isValidCred)
        {
            ShowError(string.Format(CliCredentialsInvalidErrorFormat, data.SelectedNode.Name));
            return false;
        }
        return true;
    }

    private int CreateCliCredentials(string credName, string userName, string password, string enabledPassword, int?enableLevel, int nodeId, int port, int timeout,CliConnectionProtocol protocol)
    {
        using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>())
        {
            var credId = proxy.CreateCliCiscoCredentials(credName, userName, password, enabledPassword, enableLevel);
            proxy.UpdateCliConnectionInfo(nodeId,credId,protocol,port,timeout);
            return credId;
        }
        
    }

    private void UpdateCliCredentials(int credId, int nodeId, int port, int timeout, CliConnectionProtocol protocol)
    {
        using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>())
        {
            proxy.UpdateCliConnectionInfo(nodeId, credId, protocol, port, timeout);
        }
    }

    private bool ValidateCliCredentials(int credId, int engineId,  IPAddress ip, int port, int timeout, CliConnectionProtocol protocol)
    {
        using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>())
        {
            return proxy.ValidateCliConnectionInfo(engineId, ip, credId, protocol, port, timeout, true);
        }
    }
}