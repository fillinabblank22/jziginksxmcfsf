﻿using System;
using SolarWinds.Orion.IpSla.Common.Settings;
using SolarWinds.Orion.IpSla.Contracts;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.Wizards.CallManager;
using System.Linq;

public partial class Orion_Voip_Controls_Wizards_CallManager_SummaryWizardPartControl : CallManagerAddWizardFlowPartControl
{
    protected static readonly string SummaryTitleFormat = Resources.VNQMWebContent.VNQMWEBCODE_VB1_116;
    protected static readonly string ErrorTitleFormat = Resources.VNQMWebContent.VNQMWEBCODE_VB1_117;
    protected static readonly string SummaryTextFormat = Resources.VNQMWebContent.VNQMWEBCODE_VB1_118;
    protected static readonly string SummaryTextNoCDRFormat = Resources.VNQMWebContent.VNQMWEBCODE_VB1_119;
    protected static readonly string SummaryTextCcmeFormat = Resources.VNQMWebContent.VNQMWEBCODE_VB1_120;
    protected static readonly string AvayaSummaryTextFormat = Resources.VNQMWebContent.VNQMWEBCODE_VB1_271;
    protected static readonly string AvayaSummaryTextNoCDRFormat = Resources.VNQMWebContent.VNQMWEBCODE_VB1_272;
    protected static readonly string ErrorTextFormat = Resources.VNQMWebContent.VNQMWEBCODE_VB1_121;
    protected static readonly string ErrorTextFormat2 = Resources.VNQMWebContent.VNQMWEBCODE_VB1_122;
    protected static readonly string AxlCredentialsTitle = Resources.VNQMWebContent.VNQMWEBCODE_VB1_123;
    protected static readonly string AxlCredentialsNameLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_124;
    protected static readonly string AxlCredentialsUserNameNameLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_93;
    protected static readonly string AxlCredentialsPasswordLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_94;
    protected static readonly string FtpSettingsTitle = Resources.VNQMWebContent.VNQMWEBCODE_VB1_125;
    protected static readonly string FtpServerLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_102;
    protected static readonly string FtpPortLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_104;
    protected static readonly string FtpPassiveLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_126;
    protected static readonly string FtpSecuredLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_127;
    protected static readonly string FtpDirectoryLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_128;
    protected static readonly string FtpCredentialsNameLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_124;
    protected static readonly string FtpCredentialsUserNameNameLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_100;
    protected static readonly string FtpCredentialsPasswordLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_101;
    protected static readonly string FtpFrequencyLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_108;
    protected static readonly string FtpFrequencyUnit = Resources.VNQMWebContent.VNQMWEBCODE_VB1_132;
    protected static readonly string FtpDeleteLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_110;
    protected static readonly string CredentialsError = Resources.VNQMWebContent.VNQMWEBCODE_VB1_129;
    protected static readonly string Yes = Resources.VNQMWebContent.VNQMWEBCODE_VB1_130;
    protected static readonly string No = Resources.VNQMWebContent.VNQMWEBCODE_VB1_131;


    protected static readonly string CliCredentialsTitle = Resources.VNQMWebContent.VNQMWEBCODE_SS_9;
    protected static readonly string CliCredentialsNameLabel = Resources.VNQMWebContent.VNQMWEBCODE_SS_10;
    protected static readonly string CliCredentialsUsernameLabel = Resources.VNQMWebContent.VNQMWEBCODE_SS_11;
    protected static readonly string CliCredentialsPasswordLabel = Resources.VNQMWebContent.VNQMWEBCODE_SS_12;
    protected static readonly string CliProtocolLabel = Resources.VNQMWebContent.VNQMWEBCODE_SS_15;
    protected static readonly string CliPortLabel = Resources.VNQMWebContent.VNQMWEBCODE_SS_16;
    protected static readonly string CliTimeoutLabel = Resources.VNQMWebContent.VNQMWEBCODE_SS_17;

    protected static readonly string UtcTimeZoneTitle = Resources.VNQMWebContent.VNQMWEBCODE_SS_6;
    protected static readonly string UtcTimeZoneLabel = Resources.VNQMWebContent.VNQMWEBCODE_SS_7;

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public bool? Success { get; private set; }
    public bool CDRMonitoringEnabled { get; private set; }
    public string Title { get; private set; }
    public string Description { get; private set; }
    public string ErrorText { get; private set; }

    //Cisco CM axl settings
    protected string AxlCredentialsName { get; private set; }
    protected string AxlCredentialsUserName { get; private set; }
    
    //Cisco CM ftp settings
    protected string FtpServer { get; private set; }
    protected string FtpPort { get; private set; }
    protected string FtpPassive { get; private set; }
    protected string FtpSecured { get; private set; }
    protected string FtpDirectory { get; private set; }
    protected string FtpCredentialsName { get; private set; }
    protected string FtpCredentialsUserName { get; private set; }
    protected string FtpFrequency { get; private set; }
    protected string FtpDelete { get; private set; }

    //Avay CM cli settings
    protected string CliCredentialsName { get; private set; }
    protected string CliCredentialsUsername { get; private set; }
    protected string CliProtocol { get; private set; }
    protected string CliTimeout { get; set; }
    protected string CliPort { get; set; }

	//Avay CDRCQR settings
	protected string CdrPort { get; private set; }
	protected string CqrPort { get; private set; }

    protected string UtcTimeZoneValue { get; set; }
    protected string SipTrunkPollingFrequency { get; private set; }
    protected bool SipTrunkMonitoringEnabled { get; private set; }

    public override bool Initialize(CallManagerWizardData data)
    {
        Success = data.Success;
        CDRMonitoringEnabled = !data.AddWithoutCDRMonitoring;
        CallManagerType cmType = data.CallManagerType;

        cliPanel.Visible = false;
        axlPanel.Visible = false;
        ftpPanel.Visible = false;
		avayaPortsPanel.Visible = false;

        if (!Success.HasValue)
        {
            // initial display of summary page
            Title = string.Format(SummaryTitleFormat,
                (data.SelectedNode != null)
                ? data.SelectedNode.Name
                : string.Empty);

            if(cmType == CallManagerType.ACM)
                DisplayAvayaCallManagerSummaryInfo(data);
            else
                DisplayCiscoCallManagerSummaryInfo(data);
        }
        else if (!Success.Value)
        {
            // error during adding Call manager
            Title = string.Format(ErrorTitleFormat,
                (data.SelectedNode != null)
                ? data.SelectedNode.Name
                : string.Empty);
            Description = string.Format(ErrorTextFormat,
                (data.SelectedNode != null)
                ? data.SelectedNode.Name
                : string.Empty);
            ErrorText = string.Format(ErrorTextFormat2, data.Error);

            pageText.CssClass += pageText.CssClass + " Warning";
            errorText.Visible = true;
        }

        return true;
    }
    
    public override bool ModifyData(CallManagerWizardData data)
    {
        return true;
    }

    private void InitializeCiscoCdrSummaryData(CallManagerWizardData data)
    {
        cliPanel.Visible = false;
        axlPanel.Visible = true;
        ftpPanel.Visible = true;

        Description = string.Format(SummaryTextFormat,
                   (data.SelectedNode != null)
                   ? data.SelectedNode.Name
                   : string.Empty);
        if (IpSlaSettings.IsDemoServer)
        {
            AxlCredentialsName = data.AxlCredentialsName;
            AxlCredentialsUserName = data.AxlUserName;
            FtpServer = data.FtpConnectionInfo.FtpServer;
            FtpPort = data.FtpConnectionInfo.Port.ToString();
            FtpPassive = data.FtpConnectionInfo.PassiveMode ? Yes : No;
            FtpSecured = data.FtpConnectionInfo.SecureConnection ? Yes : No;
            FtpDirectory = data.FtpConnectionInfo.Directory;
            FtpCredentialsName = data.FtpConnectionInfo.CredentialName;
            FtpCredentialsUserName = data.FtpConnectionInfo.UserName;
            FtpFrequency = data.FtpConnectionInfo.Frequency.ToString();
            FtpDelete = data.FtpConnectionInfo.DeleteDownloadedFiles ? Yes : No;
            SipTrunkPollingFrequency = data.SipTrunkPollingFrequency.ToString();
            SipTrunkMonitoringEnabled = data.SipTrunkMonitoringEnabled;
        }
        else
        {
            using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>())
            {
                AxlCredentialsPublic axlCredentials = proxy.GetAxlCredentials(data.AxlCredentialsId);
                FtpCredentialsPublic ftpCredentials = proxy.GetFtpCredentials(data.FtpConnectionInfo.CredentialID);

                AxlCredentialsName = (axlCredentials != null) ? axlCredentials.Name : CredentialsError;
                AxlCredentialsUserName = (axlCredentials != null) ? axlCredentials.Username : CredentialsError;
                FtpServer = data.FtpConnectionInfo.FtpServer;
                FtpPort = data.FtpConnectionInfo.Port.ToString();
                FtpPassive = data.FtpConnectionInfo.PassiveMode ? Yes : No;
                FtpSecured = data.FtpConnectionInfo.SecureConnection ? Yes : No;
                FtpDirectory = data.FtpConnectionInfo.Directory;
                FtpCredentialsName = (ftpCredentials != null) ? ftpCredentials.Name : CredentialsError;
                FtpCredentialsUserName = (ftpCredentials != null) ? ftpCredentials.Username : CredentialsError;
                FtpFrequency = data.FtpConnectionInfo.Frequency.ToString();
                FtpDelete = data.FtpConnectionInfo.DeleteDownloadedFiles ? Yes : No;
                SipTrunkPollingFrequency = data.SipTrunkPollingFrequency.ToString();
                SipTrunkMonitoringEnabled = data.SipTrunkMonitoringEnabled;
            }
        }
    }

    private void InitializeAvayaSummaryData(CallManagerWizardData data)
    {
        cliPanel.Visible = true;
        axlPanel.Visible = false;
        ftpPanel.Visible = false;
		avayaPortsPanel.Visible = false;

        if (CDRMonitoringEnabled)
        {
            avayaPortsPanel.Visible = true;
            InitializeAvayaCdrCqrPort();
        }

        InitializeAvayaCliInfo(data);
    }

    private void InitializeAvayaCliInfo(CallManagerWizardData data)
    {
        Description = string.Format(AvayaSummaryTextFormat,
                                        (data.SelectedNode != null)
                                            ? data.SelectedNode.Name
                                            : string.Empty);

        if (IpSlaSettings.IsDemoServer)
        {
            CliCredentialsName = data.CliConnectionInfo.Credentials.Name;
            CliCredentialsUsername = data.CliConnectionInfo.Credentials.Username;
            CliProtocol = data.CliConnectionInfo.Protocol.ToString();//todo: convert to string
            CliTimeout = data.CliConnectionInfo.Timeout.ToString();
            CliPort = data.CliConnectionInfo.Port.ToString();
        }
        else
        {
            CliCiscoCredentialsPublic cliCredentials = GetCliConnectionInfoById(data.CliConnectionInfo.Credentials.ID ?? 0);

            CliCredentialsName = (cliCredentials != null)?cliCredentials.Name:CredentialsError;
            CliCredentialsUsername = (cliCredentials != null) ? cliCredentials.Username : CredentialsError; ;

            CliProtocol = CliProtocolToDisplayString(data.CliConnectionInfo.Protocol);
            CliTimeout = data.CliConnectionInfo.Timeout.ToString();
            CliPort = data.CliConnectionInfo.Port.ToString();
        }
    }

	private void InitializeAvayaCdrCqrPort()
	{
	  	CdrPort = IpSlaSettings.AvayaCdrTcpListenerPort.Value.ToString();
	  	CqrPort = IpSlaSettings.AvayaRtcpListenerPort.Value.ToString();
	}

    private void DisplayAvayaCallManagerSummaryInfo(CallManagerWizardData data)
    {
        InitializeAvayaSummaryData(data);

        if (!CDRMonitoringEnabled)
        {
            Description = string.Format(AvayaSummaryTextNoCDRFormat,
                                        (data.SelectedNode != null)
                                            ? data.SelectedNode.Name
                                            : string.Empty);
        }
    }

    private void DisplayCiscoCallManagerSummaryInfo(CallManagerWizardData data)
    {
        sipTrunkPanel.Visible = data.SipTrunkPollingFrequency.HasValue;

        if (CDRMonitoringEnabled)
        {
            InitializeCiscoCdrSummaryData(data);
        }
        else
        {
            if (data.CallManagerType == CallManagerType.CCM)
            {
                Description = string.Format(SummaryTextNoCDRFormat,
                    (data.SelectedNode != null)
                    ? data.SelectedNode.Name
                    : string.Empty);
            }
            else if (data.CallManagerType == CallManagerType.CCME)
            {
                Description = string.Format(SummaryTextCcmeFormat,
                    (data.SelectedNode != null)
                    ? data.SelectedNode.Name
                    : string.Empty);
            }
        }
    }

    private CliCiscoCredentialsPublic GetCliConnectionInfoById(int credId)
    {
        using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>())
        {
            var creds= proxy.GetCliCiscoCredentials();
            if (creds == null)
                return null;

            return creds.FirstOrDefault(o => o.Id == credId);
        }
    }

    private string CliProtocolToDisplayString(CliConnectionProtocol protocol)
    {
        switch (protocol)
        {
            case CliConnectionProtocol.Ssh1:
                return "SSH1";
            case CliConnectionProtocol.Ssh2:
                return "SSH2";
            case CliConnectionProtocol.SshAuto:
                return "SSH auto";
            case CliConnectionProtocol.Telnet:
                return "Telnet";
            default:
                return "Unknown";
        }
    }
}