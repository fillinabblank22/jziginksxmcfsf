﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AvayaDefineMonitoringWizardPartControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_CallManager_AvayaDefineMonitoringWizardPartControl" %>

<div class="ipsla_WP_Title"><%= Title %></div>
<div class="ipsla_WP_Description"><%= Description %></div>

<table class="Events ipsla_WP_Table">
        <%-- Define CDR polling --%>
        <tr class="alternate">
            <td class="rbtnCell">
                <input runat="server" type="radio" name="enableCDR" id="enableCDR" value="1" checked="True" />
            </td>
		    <td class="iconCell"></td>
            <td >
			    <div class="ipsla_WP_SubTitle" for="<%= enableCDR.ClientID %>">
			        <label><%= EnableCDRText %></label>
			    </div>
                <%= EnableCDRDescription %>
                <a class="VNQM_HelpLink" href="<%= ConfigCmHelp %>" target="_blank">&raquo; <%= ConfigCmHelpLink %></a>
                
            </td>
        </tr>
        <%-- No CDR monitoring --%>
        <tr>
            <td class="rbtnCell">
                <input type="radio" runat="server" name="enableCDR" id="disableCDR" value="0" />
            </td>
		    <td class="iconCell">
		    </td>
            <td>
			    <div class="ipsla_WP_SubTitle">
			        <label for="<%= disableCDR.ClientID %>"><%= DisableCDRText %></label>
			    </div>
                <%= DisableCDRDescription %>
            </td>
        </tr>
</table>