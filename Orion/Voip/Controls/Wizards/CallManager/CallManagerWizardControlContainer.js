﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Voip.Controls.Wizards.CallManager");

Orion.Voip.Controls.Wizards.CallManager.CallManagerWizardControlContainer = function(element) {
    Orion.Voip.Controls.Wizards.CallManager.CallManagerWizardControlContainer.initializeBase(this, [element]);
};

Orion.Voip.Controls.Wizards.CallManager.CallManagerWizardControlContainer.prototype = {

    CancelWizard: function(cancelFunc) {
        Ext.MessageBox.show({
            title: "@{R=VNQM.Strings;K=VNQMWEBJS_VB1_1;E=js}",
            icon: Ext.MessageBox.QUESTION,
            msg: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_50;E=js}',
            closable: false,
            buttons: Ext.MessageBox.YESNO,
            fn: function(btn, value) {
                if (btn == 'yes') {
                    cancelFunc();
                }
            }
        });
    },

    initialize: function() {
        Orion.Voip.Controls.Wizards.CallManager.CallManagerWizardControlContainer.callBaseMethod(this, 'initialize');
        // Add custom initialization here
    },
    dispose: function() {
        // Add custom dispose actions here
        Orion.Voip.Controls.Wizards.CallManager.CallManagerWizardControlContainer.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Controls.Wizards.CallManager.CallManagerWizardControlContainer.registerClass('Orion.Voip.Controls.Wizards.CallManager.CallManagerWizardControlContainer', Orion.Voip.Controls.Wizards.WizardControl);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
