﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DefineCliCredentialsWizardPartControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_CallManager_DefineCliCredentialsWizardPartControl" %>
<%@ Register src="~/Orion/Voip/Controls/CliSettingsControl.ascx" tagname="CliSettingsControl" tagprefix="uc1" %>

<div class="ipsla_WP_Title"><%= Title %></div>
<div class="ipsla_WP_Description"><%= Description %></div>
<div class="ipsla_WP_Description"><%= Description2 %>
<a class="VNQM_HelpLink" href="<%= CliCredentialsHelp %>" target="_blank">&raquo; <%= CliCredentialsHelpLink %></a></div>

<div class="clirCredentialsControl">
     <uc1:CliSettingsControl ID="cliSettings" runat="server" HideEnableLevel="True" />
</div>
