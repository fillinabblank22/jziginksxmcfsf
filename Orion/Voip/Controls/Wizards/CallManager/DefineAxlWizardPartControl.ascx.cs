﻿using System;
using SolarWinds.Orion.IpSla.Common.Settings;
using SolarWinds.Orion.IpSla.Contracts;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.Wizards.CallManager;

public partial class Orion_Voip_Controls_Wizards_CallManager_DefineAxlWizardPartControl : CallManagerAddWizardFlowPartControl
{
    protected static readonly string TitleFormat = Resources.VNQMWebContent.VNQMWEBCODE_VB1_87;
    protected static readonly string Description = Resources.VNQMWebContent.VNQMWEBCODE_VB1_88;
    protected static readonly string Description2 = Resources.VNQMWebContent.VNQMWEBCODE_VB1_89;
    protected static readonly string AxlCredentialsHelpLink = Resources.VNQMWebContent.VNQMWEBCODE_VB1_90;
    protected static readonly string AxlCredentialsInvalidErrorFormat = Resources.VNQMWebContent.VNQMWEBCODE_VB1_91;

    protected string AxlCredentialsHelp = string.Empty;

    private const string helpFragment = "OrionIPSLAManagerAGAddingorDeletingCiscoCallManagerDevices";

    protected void Page_Load(object sender, EventArgs e)
    {
        AxlCredentialsHelp = HelpLocator.GetHelpUrl(helpFragment);
    }

    public string Title { get; private set; }

    public override bool Initialize(CallManagerWizardData data)
    {
        Title = string.Format(TitleFormat, 
            (data.SelectedNode != null)
            ? data.SelectedNode.Name
            : string.Empty);
        axlConnectionControl.PreselectedCredentialsId = data.AxlCredentialsId;
        if (data.SelectedNode != null)
        {
            axlConnectionControl.TargetEngine = data.SelectedNode.EngineId.HasValue ? data.SelectedNode.EngineId.Value : 1;
            axlConnectionControl.TargetIP = data.SelectedNode.IpAddress.ToString();
            axlConnectionControl.NodeIds = new int[] { data.SelectedNode.ID };
        }

        return true;
    }

    public override bool ModifyData(CallManagerWizardData data)
    {
        if (IpSlaSettings.IsDemoServer)
        {
            data.AxlCredentialsId = axlConnectionControl.CredentialsId;
            data.AxlCredentialsName = axlConnectionControl.CredentialsName;
            data.AxlUserName = axlConnectionControl.Username;
            return true;
        }
        if (axlConnectionControl.CredentialsId == 0)
        {
            // Create new credentials in credentials store
            data.AxlCredentialsId = CreateCredentials(axlConnectionControl.CredentialsName, axlConnectionControl.Username, axlConnectionControl.Password);
            axlConnectionControl.PreselectedCredentialsId = data.AxlCredentialsId;
        }
        else
        {
            data.AxlCredentialsId = axlConnectionControl.CredentialsId;
        }
        
        using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>())
        {
            var isValid = proxy.ValidateAxlCredentials(new []{ data.SelectedNode.ID }, data.SelectedNode.IpAddress,data.AxlCredentialsId);

            if (isValid)
                return true;

            ShowError(string.Format(AxlCredentialsInvalidErrorFormat, data.SelectedNode.Name));
            return false;
        }
    }

    private static int CreateCredentials(string name, string username, string password)
    {
        using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>())
        {
            return proxy.CreateAxlCredentials(name, username, password);
        }
    }
}
