﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DefineFtpWizardPartControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_CallManager_DefineFtpWizardPartControl" %>
<%@ Register TagPrefix="ipsla" TagName="FtpSettingsControl" Src="~/Orion/Voip/Controls/FtpSettingsControl.ascx" %>

<div class="ipsla_WP_Title"><%= Title %></div>
<div class="ipsla_WP_Description"><%= Description %></div>

<div class="credentialsControl">
    <ipsla:FtpSettingsControl runat="server" ID="ftpSettingsControl" />
</div>