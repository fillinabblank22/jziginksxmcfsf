﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DefineMonitoringWizardPartControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_CallManager_DefineMonitoringWizardPartControl" %>
<%@ Register TagPrefix="ipsla" TagName="SipPollingSettingsControl" Src="~/Orion/Voip/Controls/SipPollingSettingsControl.ascx" %>

<script type="text/javascript">
    function enableCdrChanged(enable) {
        if (enable) {
            $('#divSipPollingSettingsControl').show();
        } else {
            $('#divSipPollingSettingsControl').hide();
        }
    }
</script>

<div class="ipsla_WP_Title"><%= Title %></div>
<div class="ipsla_WP_Description"><%= Description %></div>

<table width="100%" cellspacing="0" cellpadding="3" class="Events">
        <%-- Define CDR polling --%>
        <tr class="alternate">
            <td class="rbtnCell">
                <input runat="server" type="radio" name="enableCDR" id="enableCDR" value="1" checked="True" onclick="enableCdrChanged(true)" />
            </td>
		    <td class="iconCell">
		    </td>
            <td>
			    <div class="ipsla_WP_SubTitle">
			        <label for="<%= enableCDR.ClientID %>"><%= EnableCDRText %></label>
			    </div>
                <%= EnableCDRDescription %>
                <a class="VNQM_HelpLink" href="<%= ConfigCmHelp %>" target="_blank">&raquo; <%= ConfigCmHelpLink %></a>
            </td>
        </tr>
        <%-- SIP Polling settings --%>
        <tr>
            <td class="leftLabelColumn">
                &nbsp;
            </td>
		    <td class="iconCell">
		    </td>
            <td>
               <ipsla:SipPollingSettingsControl runat="server" ID="SipPollingSettingsControl" />
            </td>
        </tr>

        <%-- No CDR monitoring --%>
        <tr>
            <td class="rbtnCell">
                <input type="radio" runat="server" name="enableCDR" id="disableCDR" value="0" onclick="enableCdrChanged(false)"/>
            </td>
		    <td class="iconCell">
		    </td>
            <td>
			    <div class="ipsla_WP_SubTitle">
			        <label for="<%= disableCDR.ClientID %>"><%= DisableCDRText %></label>
			    </div>
                <%= DisableCDRDescription %>
            </td>
        </tr>
</table>
