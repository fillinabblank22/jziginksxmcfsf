﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using SolarWinds.Orion.IpSla.Web.Wizards.CallManager;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_Controls_Wizards_CallManager_AvayaDefineMonitoringWizardPartControl : CallManagerAddWizardFlowPartControl
{
    protected static readonly string TitleFormat = Resources.VNQMWebContent.VNQMWEBCODE_VB1_81;
    protected static readonly string DescriptionFormat = Resources.VNQMWebContent.VNQMWEBCODE_VB1_82;

    protected static readonly string EnableCDRText = Resources.VNQMWebContent.VNQMWEBCODE_SS_1;
    protected static readonly string EnableCDRDescription = Resources.VNQMWebContent.VNQMWEBCODE_VB1_84;

    protected static readonly string DisableCDRText = Resources.VNQMWebContent.VNQMWEBCODE_SS_2;
    protected static readonly string DisableCDRDescription = Resources.VNQMWebContent.VNQMWEBCODE_VB1_86;

    protected static readonly string ConfigCmHelpLink = Resources.VNQMWebContent.VNQMWEBCODE_VK1_2;
    protected static readonly string ConfigCmTimeZoneHelpLink = Resources.VNQMWebContent.VNQMWEBCODE_PG_16;

    protected static readonly string UtcTimeZoneTitle = Resources.VNQMWebContent.VNQMWEBCODE_SS_3;
    protected static readonly string TimeZoneNotSelectedError = Resources.VNQMWebContent.VNQMWEBCODE_SS_4;
    protected static readonly string DefaultTimeZone = Resources.VNQMWebContent.VNQMWEBCODE_SS_5;

    protected string ConfigCmHelp = string.Empty;
    protected string ConfigCmTimeZoneHelp = string.Empty;
    private const string configCmHelpFragment = "OrionIPSLAManagerAGAddingorDeletingAvayaCallManagerDevices";
    private const string configCmTimeZoneHelpFragment = "OrionIPSLAManagerAGAddingorDeletingAvayaCallManagerDevicesTimeZone";

    protected void Page_Load(object sender, EventArgs e)
    {
        ConfigCmHelp = HelpLocator.GetHelpUrl(configCmHelpFragment);
        ConfigCmTimeZoneHelp = HelpLocator.GetHelpUrl(configCmTimeZoneHelpFragment);
    }

    public string Title { get; private set; }
    public string Description { get; private set; }

    public ReadOnlyCollection<TimeZoneInfo> TimeZones = TimeZoneInfo.GetSystemTimeZones();

    public override bool Initialize(CallManagerWizardData data)
    {
        Title = string.Format(TitleFormat,
            (data.SelectedNode != null)
            ? data.SelectedNode.Name
            : string.Empty);

        Description = string.Format(DescriptionFormat,
            (data.SelectedNode != null)
            ? data.SelectedNode.Name
            : string.Empty);

        disableCDR.Checked = data.AddWithoutCDRMonitoring;



        return true;
    }

    public override bool ModifyData(CallManagerWizardData data)
    {
        // reset success flag before possible final step
        data.Success = null;

        data.AddWithoutCDRMonitoring = disableCDR.Checked;

        return true;
    }



    
}