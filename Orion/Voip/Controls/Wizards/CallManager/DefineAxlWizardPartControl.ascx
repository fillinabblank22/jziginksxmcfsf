﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DefineAxlWizardPartControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_CallManager_DefineAxlWizardPartControl" %>
<%@ Register TagPrefix="ipsla" TagName="AxlConnectionControl" Src="~/Orion/Voip/Controls/AxlConnectionControl.ascx" %>

<div class="ipsla_WP_Title"><%= Title %></div>
<div class="ipsla_WP_Description"><%= Description %></div>
<div class="ipsla_WP_Description"><%= Description2 %>
<a class="VNQM_HelpLink" href="<%= AxlCredentialsHelp %>" target="_blank">&raquo; <%= AxlCredentialsHelpLink %></a></div>

<div class="credentialsControl">
    <ipsla:AxlConnectionControl runat="server" ID="axlConnectionControl" />
</div>
