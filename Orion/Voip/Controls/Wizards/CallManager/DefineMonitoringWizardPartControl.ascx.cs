﻿using System;
using SolarWinds.Orion.IpSla.Web.Wizards.CallManager;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_Controls_Wizards_CallManager_DefineMonitoringWizardPartControl : CallManagerAddWizardFlowPartControl
{
    protected static readonly string TitleFormat = Resources.VNQMWebContent.VNQMWEBCODE_VB1_81;
    protected static readonly string DescriptionFormat = Resources.VNQMWebContent.VNQMWEBCODE_VB1_82;

    protected static readonly string EnableCDRText = Resources.VNQMWebContent.VNQMWEBCODE_VB1_83;
    protected static readonly string EnableCDRDescription = Resources.VNQMWebContent.VNQMWEBCODE_VB1_84;

    protected static readonly string DisableCDRText = Resources.VNQMWebContent.VNQMWEBCODE_VB1_85;
    protected static readonly string DisableCDRDescription = Resources.VNQMWebContent.VNQMWEBCODE_VB1_86;

    protected static readonly string ConfigCmHelpLink = Resources.VNQMWebContent.VNQMWEBCODE_VK1_1;
    protected string ConfigCmHelp = string.Empty;
    private const string helpFragment = "OrionIPSLAManagerAGAddingorDeletingCiscoCallManagerDevices";

    protected void Page_Load(object sender, EventArgs e)
    {
        ConfigCmHelp = HelpLocator.GetHelpUrl(helpFragment);
    }

    public string Title { get; private set; }
    public string Description { get; private set; }

    public override bool Initialize(CallManagerWizardData data)
    {
        Title = string.Format(TitleFormat, 
            (data.SelectedNode != null)
            ? data.SelectedNode.Name
            : string.Empty);

        Description = string.Format(DescriptionFormat,
            (data.SelectedNode != null)
            ? data.SelectedNode.Name
            : string.Empty);

        disableCDR.Checked = data.AddWithoutCDRMonitoring;

        if (!data.AddWithoutCDRMonitoring && data.SelectedNode != null)
        {
            if (data.CallManagerType == SolarWinds.Orion.IpSla.Data.CallManagerType.CCM)
            {
                SipPollingSettingsControl.Visible = true;
                SipPollingSettingsControl.SipTrunkMonitoringEnabled = true;
                data.SipTrunkMonitoringEnabled = true;

                int sipPollingFrequency;
                data.SipTrunkPollingFrequency =
                    int.TryParse(SipPollingSettingsControl.SipTrunkPollingFrequency, out sipPollingFrequency)
                        ? sipPollingFrequency
                        : (int?)null;
            }
        }

        return true;
    }

    public override bool ModifyData(CallManagerWizardData data)
    {
        // reset success flag before possible final step
        data.Success = null;

        data.AddWithoutCDRMonitoring = disableCDR.Checked;
        data.SipTrunkMonitoringEnabled = SipPollingSettingsControl.SipTrunkMonitoringEnabled;

        if (enableCDR.Checked && SipPollingSettingsControl.SipTrunkMonitoringEnabled)
        {
            int sipPollingFrequency;

            data.SipTrunkPollingFrequency =
                int.TryParse(SipPollingSettingsControl.SipTrunkPollingFrequency, out sipPollingFrequency)
                    ? sipPollingFrequency
                    : (int?) null;

            if (data.SipTrunkPollingFrequency == null)
            {
                ShowError(Resources.VNQMWebContent.VNQMWEB_BO_SipTrunk_Interval_Error);
                return false;
            }
        }
        else
        {
            data.SipTrunkMonitoringEnabled = false;
            data.SipTrunkPollingFrequency = 0;
        }

        return true;
    }
}
