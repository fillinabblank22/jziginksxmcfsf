﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using SolarWinds.Orion.IpSla.Common.Settings;
using SolarWinds.Orion.IpSla.Contracts;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.IpSla.Web.Wizards.CallManager;

public partial class Orion_Voip_Controls_Wizards_CallManager_SelectNodesWizardPartControl : CallManagerAddWizardFlowPartControl
{
    protected static readonly string Title = Resources.VNQMWebContent.VNQMWEBCODE_VB1_72;
    protected static readonly string Description = Resources.VNQMWebContent.VNQMWEBCODE_VB1_73;
	protected static readonly string CiscoDescription = Resources.VNQMWebContent.VNQMWEBCODE_VK2_10;
	protected static readonly string AvayaDescription = Resources.VNQMWebContent.VNQMWEBCODE_VK2_11;
    protected static readonly string NoNodeSelectedError = Resources.VNQMWebContent.VNQMWEBCODE_VB1_74;
    protected static readonly string NodeDoesntExistError = Resources.VNQMWebContent.VNQMWEBCODE_VB1_75;
    protected static readonly string CantValidateError = Resources.VNQMWebContent.VNQMWEBCODE_VB1_76;
    protected static readonly string ValidationFailedErrorFormat = Resources.VNQMWebContent.VNQMWEBCODE_VB1_77;
    protected static readonly string AddNodesText = String.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_78, "<a href=\"/Orion/Nodes/Default.aspx\">", "</a>");
    protected static readonly string CiscoBannerLinkText = Resources.VNQMWebContent.VNQMWEBCODE_PG_13;
	protected static readonly string AvayaBannerLinkText = Resources.VNQMWebContent.VNQMWEBCODE_VK2_12;
    protected const string CiscoBannerLink = "http://www.solarwinds.com/documentation/kbloader.aspx?kb=4596&lang=en";
	protected static readonly string AvayaBannerLink = SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("OrionIPSLAManagerAdministratorGuide-AddingAvayaCallManagersToVNQM");

    protected void Page_Load(object sender, EventArgs e)
    {
        nodesValidator.ClientValidationFunction = "VoipValidateNodes_" + this.ClientID;
    }

    public override bool Initialize(CallManagerWizardData data)
    {
        if (data.SelectedNode != null)
        {
            nodesBrowser.SelectedNodes =
                new[]
                    {
                        new Orion_Voip_Controls_NodesBrowser_NodesBrowser.Node(data.SelectedNode.ID,
                                                                               data.SelectedNode.Name)
                    };
        }

        // hide nodes that are already Call Managers
        DataTable dt = DALHelper.ExecuteQuery("SELECT NodeID FROM Orion.IpSla.CCMMonitoring WHERE Deleted = 'False'");
        if (dt != null)
        {
            List<int> excludedNodes = new List<int>();
            foreach (DataRow row in dt.Rows)
            {
                excludedNodes.Add((int)row["NodeID"]);
            }
            nodesBrowser.ExcludedNodesIds = excludedNodes;
        }

        return true;
    }

    public override bool ModifyData(CallManagerWizardData data)
    {
        if (nodesBrowser.SelectedNodes == null || !nodesBrowser.SelectedNodes.Any())
        {
            ShowError(NoNodeSelectedError);
            return false;
        }

        NpmNode node = NpmNodesDAL.GetById(nodesBrowser.SelectedNodes.First().ID);
        if (node == null)
        {
            ShowError(NodeDoesntExistError);
            return false;
        }

        if (IpSlaSettings.IsDemoServer)
        {
            data.CallManagerType = node.Name.StartsWith("Avaya") ? CallManagerType.ACM : CallManagerType.CCM;
            data.AddWithoutCDRMonitoring = true;
            data.SelectedNode = node;
            return true;
        }
        using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>())
        {
            IEnumerable<BusinessLayerResult> results = proxy.VerifyCallManagerNodes(new[] {node.ID});
            if (!results.Any())
            {
                ShowError(CantValidateError);
                return false;
            }

            if (results.First().Error)
            {
                ShowError(string.Format(ValidationFailedErrorFormat, results.First().Message));
                return false;
            }
            data.CallManagerType = results.First().CallManagerType;
            if (data.CallManagerType != CallManagerType.CCM && data.CallManagerType != CallManagerType.ACM)
            {
                data.AddWithoutCDRMonitoring = true;
            }
            data.SelectedNode = node;
        }

        return true;
    }
}
