﻿using System;
using SolarWinds.Orion.IpSla.Common.Settings;
using SolarWinds.Orion.IpSla.Contracts;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.Wizards.CallManager;

public partial class Orion_Voip_Controls_Wizards_CallManager_DefineFtpWizardPartControl : CallManagerAddWizardFlowPartControl
{
    protected static readonly string TitleFormat = Resources.VNQMWebContent.VNQMWEBCODE_VB1_96;
    protected static readonly string Description = Resources.VNQMWebContent.VNQMWEBCODE_VB1_97;
    protected static readonly string FtpSettingsInvalidErrorFormat = Resources.VNQMWebContent.VNQMWEBCODE_VB1_98;

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public string Title { get; private set; }

    public override bool Initialize(CallManagerWizardData data)
    {
        Title = string.Format(TitleFormat,
            (data.SelectedNode != null)
            ? data.SelectedNode.Name
            : string.Empty);

        ftpSettingsControl.PreselectedCredentialsId = data.FtpConnectionInfo.CredentialID;
        ftpSettingsControl.FTPServer = data.FtpConnectionInfo.FtpServer;
        ftpSettingsControl.Frequency = data.FtpConnectionInfo.Frequency;
        ftpSettingsControl.DeleteAfterDownload = data.FtpConnectionInfo.DeleteDownloadedFiles;
        ftpSettingsControl.Port = data.FtpConnectionInfo.Port;
        ftpSettingsControl.SecuredConnection = data.FtpConnectionInfo.SecureConnection;
        ftpSettingsControl.Path = data.FtpConnectionInfo.Directory;
        ftpSettingsControl.PassiveMode = data.FtpConnectionInfo.PassiveMode;

        if (data.SelectedNode != null)
        {
            ftpSettingsControl.TargetEngine = data.SelectedNode.EngineId.HasValue ? data.SelectedNode.EngineId.Value : 1;
            ftpSettingsControl.TargetIP = data.SelectedNode.IpAddress.ToString();
            ftpSettingsControl.NodeIds = new int[] { data.SelectedNode.ID };
        }

        return true;
    }

    public override bool ModifyData(CallManagerWizardData data)
    {
        // reset success flag before final step
        data.Success = null;

        data.FtpConnectionInfo.FtpServer = ftpSettingsControl.FTPServer;
        data.FtpConnectionInfo.Frequency = ftpSettingsControl.Frequency;
        data.FtpConnectionInfo.DeleteDownloadedFiles = ftpSettingsControl.DeleteAfterDownload;
        data.FtpConnectionInfo.Port = ftpSettingsControl.Port;
        data.FtpConnectionInfo.SecureConnection = ftpSettingsControl.SecuredConnection;
        data.FtpConnectionInfo.Directory = ftpSettingsControl.Path;
        data.FtpConnectionInfo.PassiveMode = ftpSettingsControl.PassiveMode;

        if (IpSlaSettings.IsDemoServer)
        {
            data.FtpConnectionInfo.CredentialName = ftpSettingsControl.CredentialsName;
            data.FtpConnectionInfo.CredentialID = ftpSettingsControl.CredentialsId;
            data.FtpConnectionInfo.UserName = ftpSettingsControl.Username;
            return true;
        }

        if (ftpSettingsControl.CredentialsId == 0)
        {
            // Create new credentials in credentials store
            data.FtpConnectionInfo.CredentialID = CreateCredentials(ftpSettingsControl.CredentialsName, ftpSettingsControl.Username, ftpSettingsControl.Password);
            ftpSettingsControl.PreselectedCredentialsId = data.FtpConnectionInfo.CredentialID;
        }
        else
        {
            data.FtpConnectionInfo.CredentialID = ftpSettingsControl.CredentialsId;
        }

        using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>())
        {
            bool isValid = proxy.ValidateVoipCCMFtpConnectionInfo(
                data.SelectedNode.EngineId.HasValue ? data.SelectedNode.EngineId.Value : 1,
                data.SelectedNode.IpAddress,
                data.FtpConnectionInfo.FtpServer,
                data.FtpConnectionInfo.CredentialID,
                data.FtpConnectionInfo.Directory,
                data.FtpConnectionInfo.SecureConnection,
                data.FtpConnectionInfo.Port,
                data.FtpConnectionInfo.PassiveMode);

            if (!isValid)
            {
                ShowError(string.Format(FtpSettingsInvalidErrorFormat, data.SelectedNode.Name));
                return false;
            }
        }

        return true;
    }

    private static int CreateCredentials(string name, string username, string password)
    {
        using (var proxy = BusinessLayer.GetBusinessLayer<IIpSlaBusinessLayer>())
        {
            return proxy.CreateFtpCredentials(name, username, password);
        }
    }
}
