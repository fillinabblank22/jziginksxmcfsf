﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SummaryWizardPartControl.ascx.cs" Inherits="Orion_Voip_Controls_Wizards_CallManager_SummaryWizardPartControl" %>

<style type="text/css">
    .ZebraStripe th{ white-space: nowrap;width: 260px;}
</style>

<div class="ipsla_WP_Title"><%= Title %></div>
<asp:Panel CssClassc="ipsla_WP_Description" runat="server" ID="pageText"><%= Description %></asp:Panel>
<asp:Panel CssClassc="ipsla_WP_Description" runat="server" ID="errorText" Visible="False" CssClass="Warning"><%= ErrorText %></asp:Panel>


<asp:Panel ID="axlPanel" runat="server">
    <div class="ipsla_WP_Title"><%= AxlCredentialsTitle %></div>
    <table class="sw-voip-add-ccm-summary">
        <tr class="ZebraStripe"><th><%= AxlCredentialsNameLabel %>:</th><td> <%= AxlCredentialsName %></td></tr>
        <tr><th><%= AxlCredentialsUserNameNameLabel %>:</th><td> <%= AxlCredentialsUserName %></td></tr>
        <tr class="ZebraStripe"><th><%= AxlCredentialsPasswordLabel %>:</th><td> &bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;</td></tr>
    </table>
</asp:Panel>
<% if (SipTrunkMonitoringEnabled) { %>
<asp:Panel ID="sipTrunkPanel" runat="server">
    <div class="ipsla_WP_Title"><%=Resources.VNQMWebContent.VNQMWEB_BO_SipTrunk_Settings%></div>
    <table class="sw-voip-add-ccm-summary">
        <tr class="ZebraStripe"><th><%=Resources.VNQMWebContent.VNQMWEB_BO_SipTrunk_PollingFrequency%>:</th><td><%= string.Format(FtpFrequencyUnit, SipTrunkPollingFrequency) %></td></tr>
    </table>
</asp:Panel>
<% } %>
<asp:Panel ID="ftpPanel" runat="server">
    <div class="ipsla_WP_Title"><%= FtpSettingsTitle %></div>
    <table class="sw-voip-add-ccm-summary">
        <tr class="ZebraStripe"><th><%= FtpServerLabel %>:</th><td> <%= FtpServer %></td></tr>
        <tr><th><%= FtpPortLabel %>:</th><td> <%= FtpPort %></td></tr>
        <tr class="ZebraStripe"><th><%= FtpPassiveLabel %>:</th><td> <%= FtpPassive %></td></tr>
        <tr><th><%= FtpSecuredLabel %>:</th><td> <%= FtpSecured %></td></tr>
        <tr class="ZebraStripe"><th><%= FtpDirectoryLabel %>:</th><td> <%= FtpDirectory %></td></tr>
        <tr><th><%= FtpCredentialsNameLabel %>:</th><td> <%= FtpCredentialsName %></td></tr>
        <tr class="ZebraStripe"><th><%= FtpCredentialsUserNameNameLabel %>:</th><td> <%= FtpCredentialsUserName %></td></tr>
        <tr><th><%= FtpCredentialsPasswordLabel %>:</th><td> &bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;</td></tr>
        <tr class="ZebraStripe"><th><%= FtpFrequencyLabel %>:</th><td> <%= String.Format(FtpFrequencyUnit, FtpFrequency) %></td></tr>
        <tr><th><%= FtpDeleteLabel %>:</th><td> <%= FtpDelete %></td></tr>
    </table>
</asp:Panel>

<asp:Panel ID="cliPanel" runat="server">
    <div class="ipsla_WP_Title"><%= CliCredentialsTitle %></div>
    <table class="sw-voip-add-ccm-summary">
        <tr class="ZebraStripe"><th><%= CliCredentialsNameLabel %>:</th><td> <%= CliCredentialsName %></td></tr>
        <tr><th><%= CliCredentialsUsernameLabel %>:</th><td> <%= CliCredentialsUsername %></td></tr>
        <tr class="ZebraStripe"><th><%= CliCredentialsPasswordLabel %>:</th><td>&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;</td></tr>
        <tr><th><%= CliProtocolLabel %>:</th><td><%=CliProtocol %></td></tr>
        <tr class="ZebraStripe"><th><%= CliPortLabel %>:</th><td> <%= CliPort %></td></tr>
        <tr><th><%= CliTimeoutLabel %>:</th><td><%=CliTimeout %></td></tr>
    </table>
</asp:Panel>

<asp:Panel ID="avayaPortsPanel" runat="server">
    <div class="ipsla_WP_Title"><%= Resources.VNQMWebContent.VNQMWEBDATA_VK2_07 %></div>
    <table class="sw-voip-add-ccm-summary">
        <tr class="ZebraStripe"><th><%= Resources.VNQMWebContent.VNQMWEBDATA_VK2_08 %>:</th><td><%= CdrPort %> </td></tr>
		<tr><th><%= Resources.VNQMWebContent.VNQMWEBDATA_VK2_09 %>:</th><td><%= CqrPort %> </td></tr>
    </table>
</asp:Panel>


