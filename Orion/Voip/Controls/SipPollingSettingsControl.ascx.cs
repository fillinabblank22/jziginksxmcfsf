﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.Web.Model.ExtJSGrid.Request;

public partial class Orion_Voip_Controls_SipPollingSettingsControl : UserControl
{
    protected static readonly string SipPollingSettingsHeader = VNQMWebContent.VNQMWEB_SipPollingSettingsHeader;

    public bool SipTrunkMonitoringEnabled
    {
        get { return cbSipTrunkMonitoringEnabled.Checked; }
        set { cbSipTrunkMonitoringEnabled.Checked = value; }
    }

    public string SipTrunkPollingFrequency
    {
        get { return txtSipTrunkFrequency.Text; }
        set { txtSipTrunkFrequency.Text = value; }
    }

    public bool SipTrunkPollinFrequencyVisible
    {
        get { return trSipTrunkFrequency.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] == "visible"; }
        set { trSipTrunkFrequency.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] = value ? "visible" : "hidden"; }
    }

    public bool EnableFrequencyValidator
    {
        set { reqSipTrunkFrequency.Enabled = value; }
    }
}