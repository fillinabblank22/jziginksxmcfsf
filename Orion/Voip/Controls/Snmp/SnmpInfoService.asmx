<%@ WebService Language="C#" Class="Orion.Nodes.Controls.SnmpInfoService" %>

using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.Serialization;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Models.Credentials;
using SolarWinds.Orion.IpSla.Common.ServiceModel;
using SolarWinds.Orion.IpSla.Contracts;
using SolarWinds.Orion.IpSla.Data.Snmp;
using SolarWinds.Orion.IpSla.Web;
using SnmpCredentials = SolarWinds.Orion.Core.Common.Models.SnmpCredentials;
using SNMPv3AuthType = SolarWinds.Orion.Core.Common.Models.SNMPv3AuthType;
using SNMPv3PrivacyType = SolarWinds.Orion.Core.Common.Models.SNMPv3PrivacyType;
using SNMPVersion = SolarWinds.Orion.Core.Common.Models.SNMPVersion;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;

namespace Orion.Nodes.Controls
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class SnmpInfoService : System.Web.Services.WebService
    {
        private static readonly Log log = new Log();

        [WebMethod]
        public SnmpValidationResults ValidateSnmpNode(
            int? engineId,
            string ip, uint port,
            SNMPVersion snmpVersion,
            bool testRead,
            SnmpCredentials roCredentials,
            bool testWrite,
            SnmpCredentials rwCredentials)
        {
#if false // NPM validation function, WRITE TEST SUPPORT MISSING
            var result = new SnmpValidationResults();

            // test read credentials
            if (testRead)
            {
                using (var proxy = SolarWinds.NPM.Common.NPMBusinessLayerProxy.CreateNPMBusinessLayerProxy(BusinessLayerExceptionHandler, engineId ?? 0, false))
                {
                    result.ReadAccess = proxy.ValidateSNMP(
                        snmpVersion, ip, snmpPort,
                        roCredentials.CommunityString,
                        roCredentials.SNMPv3AuthPassword,
                        roCredentials.SNMPv3AuthType,
                        roCredentials.SNMPv3PrivacyType,
                        roCredentials.SNMPv3PrivacyPassword,
                        roCredentials.SnmpV3Context,
                        roCredentials.SNMPv3UserName);
                }
            }
#endif
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();
            IPAddress ipAddress;
            if (!IPAddress.TryParse(ip, out ipAddress))
            {
                return new SnmpValidationResults();
            }

            var roV3Credentials = new SnmpV3Credentials
            {
                AuthType = (SnmpAuthType)roCredentials.SNMPv3AuthType,
                AuthKeyIsPwd = roCredentials.SNMPV3AuthKeyIsPwd,
                AuthKey = roCredentials.SNMPv3AuthPassword,
                PrivacyType = (SnmpPrivacyType)roCredentials.SNMPv3PrivacyType,
                PrivacyKeyIsPwd = roCredentials.SNMPV3PrivKeyIsPwd,
                PrivacyKey = roCredentials.SNMPv3PrivacyPassword,
                Context = roCredentials.SnmpV3Context,
                UserName = roCredentials.SNMPv3UserName,
            };
            var rwV3Credentials = new SnmpV3Credentials
            {
                AuthType = (SnmpAuthType)rwCredentials.SNMPv3AuthType,
                AuthKeyIsPwd = rwCredentials.SNMPV3AuthKeyIsPwd,
                AuthKey = rwCredentials.SNMPv3AuthPassword,
                PrivacyType = (SnmpPrivacyType)rwCredentials.SNMPv3PrivacyType,
                PrivacyKeyIsPwd = rwCredentials.SNMPV3PrivKeyIsPwd,
                PrivacyKey = rwCredentials.SNMPv3PrivacyPassword,
                Context = rwCredentials.SnmpV3Context,
                UserName = rwCredentials.SNMPv3UserName,
            };

            using (var proxy = BusinessLayer.GetBusinessLayer())
            {
                return proxy.ValidateSnmpCredentials(
                    engineId,
                    ipAddress, (int)port,
                    (SnmpVersion)snmpVersion,
                    testRead, roCredentials.CommunityString, roV3Credentials,
                    testWrite, rwCredentials.CommunityString, rwV3Credentials);
            }
        }

        [WebMethod]
        public void SaveSnmpCredentials(
            IEnumerable<int> nodeIds,
            SNMPVersion snmpVersion,
            uint snmpPort,
            SnmpCredentials roCredentials,
            SnmpCredentials rwCredentials)
        {
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();
            using (var proxy = CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(BusinessLayerExceptionHandler))
            {
                log.DebugFormat("saveSnmpCredentials");
                foreach (var nodeId in nodeIds)
                {
                    var node = proxy.GetNode(nodeId);
                    node.SNMPPort = snmpPort;
                    node.SNMPVersion = snmpVersion;

                    node.NodeSubType = (snmpVersion == SNMPVersion.None ? NodeSubType.ICMP : NodeSubType.SNMP);

                    node.ObjectSubType = node.NodeSubType.ToString();

                    node.ReadOnlyCredentials.CommunityString = roCredentials.CommunityString;
                    node.ReadWriteCredentials.CommunityString = rwCredentials.CommunityString;

                    if (node.SNMPVersion == SNMPVersion.SNMP3)
                    {
                        var roCredentialName = string.IsNullOrEmpty(roCredentials.CredentialName) ? $"{roCredentials.SNMPv3UserName} ({node.Caption})"
                                : roCredentials.CredentialName;
                        try
                        {
                            var roSnmpCredentials = new SnmpCredentialsV3
                            {
                                UserName = roCredentials.SNMPv3UserName,
                                AuthenticationKeyIsPassword = roCredentials.SNMPV3AuthKeyIsPwd,
                                AuthenticationPassword = roCredentials.SNMPv3AuthPassword,
                                PrivacyKeyIsPassword = roCredentials.SNMPV3PrivKeyIsPwd,
                                PrivacyPassword = roCredentials.SNMPv3PrivacyPassword,
                                Context = roCredentials.SnmpV3Context,
                                Name = roCredentialName,
                                PrivacyType = (SolarWinds.Orion.Core.Models.Credentials.SNMPv3PrivacyType)roCredentials.SNMPv3PrivacyType,
                                AuthenticationType = (SolarWinds.Orion.Core.Models.Credentials.SNMPv3AuthType)roCredentials.SNMPv3AuthType,

                                Description = string.Empty
                            };

                            if (!node.NodeSettings.ContainsKey(NodeSettingsNames.RoSnmpCredentialSettingName))
                            {
                                int? credID = proxy.InsertSnmpV3Credentials(roSnmpCredentials);
                                if (credID.HasValue)
                                {
                                    node.NodeSettings[NodeSettingsNames.RoSnmpCredentialSettingName] = credID.Value.ToString();
                                }
                            }
                            else if (node.NodeSettings.ContainsKey(NodeSettingsNames.RoSnmpCredentialSettingName))
                            {
                                roSnmpCredentials.ID =
                                    int.Parse(node.NodeSettings[NodeSettingsNames.RoSnmpCredentialSettingName]);
                                proxy.UpdateSnmpV3Credentials(roSnmpCredentials);
                            }
                        }
                        catch (Exception ex)
                        {
                            log.DebugFormat("Problem in updating ReadOnly SNMPV3 credentials [NodeId:{0}], ErrorMessage:{1}, StackTrace:{2}", node.ID, ex.Message, ex.StackTrace);
                        }

                        if (!string.IsNullOrEmpty(rwCredentials.SNMPv3UserName))
                        {
                            try
                            {
                                var rwCredentialName = string.IsNullOrEmpty(rwCredentials.CredentialName) ? $"{rwCredentials.SNMPv3UserName} ({node.Caption})"
                                : rwCredentials.CredentialName;
                                var rwSnmpCredentials = new SnmpCredentialsV3
                                {
                                    UserName = rwCredentials.SNMPv3UserName,
                                    AuthenticationKeyIsPassword = rwCredentials.SNMPV3AuthKeyIsPwd,
                                    AuthenticationPassword = rwCredentials.SNMPv3AuthPassword,
                                    PrivacyKeyIsPassword = rwCredentials.SNMPV3PrivKeyIsPwd,
                                    PrivacyPassword = rwCredentials.SNMPv3PrivacyPassword,
                                    Context = rwCredentials.SnmpV3Context,
                                    Name = rwCredentialName,
                                    PrivacyType = (SolarWinds.Orion.Core.Models.Credentials.SNMPv3PrivacyType)rwCredentials.SNMPv3PrivacyType,
                                    AuthenticationType = (SolarWinds.Orion.Core.Models.Credentials.SNMPv3AuthType)rwCredentials.SNMPv3AuthType,

                                };

                                if (!node.NodeSettings.ContainsKey(NodeSettingsNames.RwSnmpCredentialSettingName))
                                {
                                    int? rwCredID = proxy.InsertSnmpV3Credentials(rwSnmpCredentials);
                                    if (rwCredID.HasValue)
                                    {
                                        node.NodeSettings[NodeSettingsNames.RwSnmpCredentialSettingName] = rwCredID.Value.ToString();
                                    }
                                }
                                else if (node.NodeSettings.ContainsKey(NodeSettingsNames.RwSnmpCredentialSettingName))
                                {
                                    rwSnmpCredentials.ID = int.Parse(node.NodeSettings[NodeSettingsNames.RwSnmpCredentialSettingName]);
                                    proxy.UpdateSnmpV3Credentials(rwSnmpCredentials);
                                }
                            }
                            catch (Exception ex)
                            {
                                log.DebugFormat("Problem in updating Read/Write SNMPV3 credentials [NodeId:{0}], ErrorMessage:{1}, StackTrace:{2}", node.ID, ex.Message, ex.StackTrace);
                            }
                        }
                    }

                    proxy.UpdateNode(node);
                }
            }
        }

        [WebMethod]
        public SnmpCredentialsInfo GetCommonSnmpCredentials(IEnumerable<int> nodeIds)
        {
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();
            SnmpCredentialsInfo result = null;
            using (var proxy = CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(BusinessLayerExceptionHandler))
            {
                foreach (var nodeId in nodeIds)
                {
                    var node = proxy.GetNode(nodeId);
                    if (result == null)
                    {
                        result = new SnmpCredentialsInfo
                        {
                            SnmpPort = node.SNMPPort,
                            SnmpVersion = node.SNMPVersion,
                            ReadOnly = node.ReadOnlyCredentials,
                            ReadWrite = node.ReadWriteCredentials,
                        };
                    }
                    else
                    {
                        if (result.SnmpPort.HasValue && result.SnmpPort.Value != node.SNMPPort) result.SnmpPort = null;
                        if (result.SnmpVersion != node.SNMPVersion) result.SnmpVersion = SNMPVersion.SNMP2c;
                        if (result.ReadOnly.CommunityString != node.ReadOnlyCredentials.CommunityString) result.ReadOnly.CommunityString = string.Empty;
                        if (result.ReadOnly.SNMPv3UserName != node.ReadOnlyCredentials.SNMPv3UserName) result.ReadOnly.SNMPv3UserName = string.Empty;
                        if (result.ReadOnly.SnmpV3Context != node.ReadOnlyCredentials.SnmpV3Context) result.ReadOnly.SnmpV3Context = string.Empty;
                        if (result.ReadOnly.SNMPv3AuthType != node.ReadOnlyCredentials.SNMPv3AuthType) result.ReadOnly.SNMPv3AuthType = SNMPv3AuthType.None;
                        if (result.ReadOnly.SNMPv3AuthPassword != node.ReadOnlyCredentials.SNMPv3AuthPassword) result.ReadOnly.SNMPv3AuthPassword = string.Empty;
                        if (result.ReadOnly.SNMPv3PrivacyType != node.ReadOnlyCredentials.SNMPv3PrivacyType) result.ReadOnly.SNMPv3PrivacyType = SNMPv3PrivacyType.None;
                        if (result.ReadOnly.SNMPv3PrivacyPassword != node.ReadOnlyCredentials.SNMPv3PrivacyPassword) result.ReadOnly.SNMPv3PrivacyPassword = string.Empty;
                        if (result.ReadWrite.CommunityString != node.ReadWriteCredentials.CommunityString) result.ReadWrite.CommunityString = string.Empty;
                        if (result.ReadWrite.SNMPv3UserName != node.ReadWriteCredentials.SNMPv3UserName) result.ReadWrite.SNMPv3UserName = string.Empty;
                        if (result.ReadWrite.SnmpV3Context != node.ReadWriteCredentials.SnmpV3Context) result.ReadWrite.SnmpV3Context = string.Empty;
                        if (result.ReadWrite.SNMPv3AuthType != node.ReadWriteCredentials.SNMPv3AuthType) result.ReadWrite.SNMPv3AuthType = SNMPv3AuthType.None;
                        if (result.ReadWrite.SNMPv3AuthPassword != node.ReadWriteCredentials.SNMPv3AuthPassword) result.ReadWrite.SNMPv3AuthPassword = string.Empty;
                        if (result.ReadWrite.SNMPv3PrivacyType != node.ReadWriteCredentials.SNMPv3PrivacyType) result.ReadWrite.SNMPv3PrivacyType = SNMPv3PrivacyType.None;
                        if (result.ReadWrite.SNMPv3PrivacyPassword != node.ReadWriteCredentials.SNMPv3PrivacyPassword) result.ReadWrite.SNMPv3PrivacyPassword = string.Empty;
                    }
                }
            }
            if (result == null) result = new SnmpCredentialsInfo();

            //TT#1973 - Write Credential Error pop-up: In R/W Community string editbox for node with not presented R/W credentials user can see some symbol
            if (result.ReadOnly.CommunityString == " ") result.ReadOnly.CommunityString = String.Empty;
            if (result.ReadWrite.CommunityString == " ") result.ReadWrite.CommunityString = String.Empty;
            //TT#1973 - end

            return result;
        }

        private static void BusinessLayerExceptionHandler(Exception ex)
        {
            log.Error(ex);
        }
    }

    [Serializable]
    [DataContract(Name = "SnmpCredentialsInfo", Namespace = Namespaces.IpSla)]
    public class SnmpCredentialsInfo
    {
        [DataMember]
        public uint? SnmpPort { get; set; }
        [DataMember]
        public SNMPVersion SnmpVersion { get; set; }
        [DataMember]
        public SnmpCredentials ReadOnly { get; set; }
        [DataMember]
        public SnmpCredentials ReadWrite { get; set; }
    }
}

