<%@ WebService Language="C#" Class="Orion.Nodes.Controls.NodeSNMPv3CredentialsService" %>

using System;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.IpSla.Data;
 

namespace Orion.Nodes.Controls
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class NodeSNMPv3CredentialsService : System.Web.Services.WebService
    {
        private static readonly Log log = new Log();

        [WebMethod]
        public void DeleteCredentials(string credentialName)
        {
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();

            using (var proxy = CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(BusinessLayerExceptionHandler))
            {
                proxy.DeleteCredentials(credentialName);
            }
        }

        [WebMethod]
        public string[] GetCredentialsSet()
        {
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();

            using (var proxy = CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(BusinessLayerExceptionHandler))
            {
                var result = proxy.GetCredentialsSet().ToArray();
                return result;
            }
        }

        [WebMethod]
        public SnmpCredentials GetCredentials(string credentialsName)
        {
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();

            using (var proxy = CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(BusinessLayerExceptionHandler))
            {
                var result = proxy.GetCredentials(credentialsName);
                return result;
            }
        }

        [WebMethod]
        public void SaveCredentials(string credentialsName,
            string username,
			string context,
            SNMPv3AuthType authMethod,
            SNMPv3PrivacyType securityMethod,
			string securityPasswordKey,
            string passwordKey)
        {
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();
            using (var proxy = CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(BusinessLayerExceptionHandler))
            {
                var credentials = new SnmpCredentials
                                      {
                                          CredentialName = credentialsName,
                                          SNMPv3UserName = username,
                                          SnmpV3Context = context,
                                          SNMPv3AuthType = authMethod,
                                          SNMPv3AuthPassword = passwordKey,
                                          SNMPV3AuthKeyIsPwd = authMethod != SNMPv3AuthType.None,
                                          SNMPv3PrivacyType = securityMethod,
                                          SNMPv3PrivacyPassword = securityPasswordKey,
                                          SNMPV3PrivKeyIsPwd = securityMethod != SNMPv3PrivacyType.None,
                                      };

                if (proxy.GetCredentialsSet().Contains(credentialsName))
                    proxy.UpdateCredentials(credentials);
                else
                    proxy.InsertCredentials(credentials);
            }
        }

        private static void BusinessLayerExceptionHandler(Exception ex)
        {
            log.Error(ex);
        }
    }
}

