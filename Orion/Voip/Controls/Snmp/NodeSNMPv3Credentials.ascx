<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeSNMPv3Credentials.ascx.cs" Inherits="Orion_Nodes_Controls_NodeSNMPv3Credentials" %>
<%@ Register Src="~/Orion/Voip/Controls/Snmp/SnmpTestResultControl.ascx" TagPrefix="orion" TagName="SnmpTestResultControl" %>

<asp:ScriptManagerProxy ID="scriptManager" runat="server">
  <Services>
    <asp:ServiceReference Path="~/Orion/Voip/Controls/Snmp/NodeSNMPv3CredentialsService.asmx" />
  </Services>
</asp:ScriptManagerProxy>

<style type="text/css">
    .snmpInfo .colInput { width: 110px; }
</style>

<table class="credBox" align="center">
    <tr>
        <td class="colLabel caption">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_82 %>
        </td>
        <td class="colInput">
            <asp:DropDownList ID="ddlCredentialsSet" runat="server" CssClass="inputFrame"></asp:DropDownList>
        </td>
        <td class="colExts">
            <orion:LocalizableButton runat="server" ID="btnDelete" LocalizedText="Delete" DisplayType="Small" />
        </td>
    </tr>
    <tr>
        <td class="colLabel caption">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_84 %>
        </td>
        <td class="colInput">
            <asp:TextBox ID="txtCredentialsName" runat="server" CssClass="input inputFrame" ValidationGroup="credentials"></asp:TextBox>            
        </td>
        <td class="colExts">
            <orion:LocalizableButton runat="server" ID="btnSave" LocalizedText="Save" DisplayType="Small" />
        </td>
    </tr>
    <tr>
        <td class="colLabel caption">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_86 %>
        </td>
        <td class="colInput">
            <asp:TextBox ID="tbSNMPv3Username" runat="server" CssClass="input inputFrame"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="colLabel caption">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_87 %></td>
        <td class="colInput">
            <asp:TextBox ID="tbSNMPv3Context" runat="server" CssClass="input inputFrame"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="colLabel caption" style="white-space: normal; line-height: 120%;">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_88 %>
        </td>
        <td class="colInput">
            <asp:DropDownList ID="ddlAuthMethod" runat="server" CssClass="inputFrame">
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_89 %>" Value="None"/>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_148 %>" Value="MD5" />
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_149 %>" Value="SHA1" />
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="colLabel caption">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_90 %>
        </td>
        <td class="colInput">
            <asp:TextBox ID="tbPasswordKey" runat="server" CssClass="input inputFrame" TextMode="Password" Enabled="False"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="colLabel caption" style="white-space: normal; line-height: 120%;">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_91 %>
        </td>
        <td class="colInput">
            <asp:DropDownList ID="ddlSecurityMethod" runat="server" CssClass="inputFrame" Enabled="False">
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_89 %>" Value="None" />
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_150 %>" Value="DES56" />
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_151 %>" Value="AES128" />
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1192_151 %>" Value="AES192" />
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1256_151 %>" Value="AES256" />
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="colLabel caption">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_90 %>
        </td>
        <td class="colInput">
            <asp:TextBox ID="tbSecurityPasswordKey" runat="server" CssClass="input inputFrame" TextMode="Password" Enabled="False"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="colLabel caption">
            &nbsp;
        </td>
        <td colspan="2">
			<orion:SnmpTestResultControl ID="result" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="colLabel caption">
            &nbsp;
        </td>
        <td colspan="2" style="display: inline-block;">
            <orion:LocalizableButton ID="btnValidate" runat="server" LocalizedText="Test" DisplayType="Small" />
        </td>
    </tr>
</table>
