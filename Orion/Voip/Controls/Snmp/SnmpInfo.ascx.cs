using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web;

public partial class Orion_Nodes_Controls_NodeSNMP : ScriptUserControlBase
{
	private static readonly Log log = new Log();

    public int EngineID
    {
        get { return (int?)ViewState["EngineID"] ?? 0; }
        set { ViewState["EngineID"] = (int?)value; log.DebugFormat("SNMPInfo.EngineID set to {0}", value); }
    }

	protected static void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}

	public string ValidationIp
	{
		get { return (string)ViewState["ip"]; }
		set { ViewState["ip"] = value; }
	}

	public SNMPVersion SNMPVersion
	{
		get
		{
            switch (ddlSnmpVersion.SelectedValue)
			{
				case "SNMPv1":
					return SNMPVersion.SNMP1;
				case "SNMPv2c":
					return SNMPVersion.SNMP2c;
				case "SNMPv3":
					return SNMPVersion.SNMP3;
				default:
					return SNMPVersion.None;
			}
		}
		set
		{
			switch (value)
			{
				case SNMPVersion.SNMP1:
                    ddlSnmpVersion.SelectedIndex = 0; break;
				case SNMPVersion.SNMP2c:
                    ddlSnmpVersion.SelectedIndex = 1; break;
				case SNMPVersion.SNMP3:
                    ddlSnmpVersion.SelectedIndex = 2; break;
                //default: ddlSnmpVersion.SelectedIndex = 0; break;
			}
		}
	}

	public string CommunityString
	{
		get { return tbCommunityString.Text; }
		set { tbCommunityString.Text = value; }
	}

	public string RWCommunityString
	{
		get { return string.IsNullOrEmpty(tbRWCommunityString.Text) ? " " : tbRWCommunityString.Text; }
		set { tbRWCommunityString.Text = value; }
	}

	public uint SNMPPort
	{
		get
		{
			uint port = 0;
			uint.TryParse(tbSNMPPort.Text, out port);
			return port;
		}
		set { tbSNMPPort.Text = value.ToString(); }
	}

	public string SNMPv3Username
	{
        get { return roSnmpV3Cred.Username; }
        set { roSnmpV3Cred.Username = value; }
	}

	public string SNMPv3Context
	{
        get { return roSnmpV3Cred.SNMPv3Context; }
        set { roSnmpV3Cred.SNMPv3Context = value; }
	}

    public SNMPv3AuthType SNMPv3AuthType
    {
        get { return roSnmpV3Cred.SNMPv3AuthType; }
        set { roSnmpV3Cred.SNMPv3AuthType = value; }
    }

    public SNMPv3PrivacyType SNMPv3PrivacyType
    {
        get { return roSnmpV3Cred.SNMPv3PrivacyType; }
        set { roSnmpV3Cred.SNMPv3PrivacyType = value; }
    }

	public string AuthPasswordKey
	{
        get { return roSnmpV3Cred.AuthPasswordKey; }
        set { roSnmpV3Cred.AuthPasswordKey = value; }
	}

	public string PrivacyPasswordKey
	{
        get { return roSnmpV3Cred.PrivacyPasswordKey; }
        set { roSnmpV3Cred.PrivacyPasswordKey = value; }
	}

	public string RWSNMPv3Username
	{
        get { return rwSnmpV3Cred.Username; }
        set { rwSnmpV3Cred.Username = value; }
	}

	public string RWSNMPv3Context
	{
        get { return rwSnmpV3Cred.SNMPv3Context; }
        set { rwSnmpV3Cred.SNMPv3Context = value; }
	}

    public SNMPv3AuthType RWSNMPv3AuthType
    {
        get { return rwSnmpV3Cred.SNMPv3AuthType; }
        set { rwSnmpV3Cred.SNMPv3AuthType = value; }
    }

    public SNMPv3PrivacyType RWSNMPv3PrivacyType
    {
        get { return rwSnmpV3Cred.SNMPv3PrivacyType; }
        set { rwSnmpV3Cred.SNMPv3PrivacyType = value; }
    }

	public string RWAuthPasswordKey
	{
        get { return rwSnmpV3Cred.AuthPasswordKey; }
        set { rwSnmpV3Cred.AuthPasswordKey = value; }
	}

	public string RWPrivacyPasswordKey
	{
        get { return rwSnmpV3Cred.PrivacyPasswordKey; }
        set { rwSnmpV3Cred.PrivacyPasswordKey = value; }
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		SetupSNMPv3CredentialsFromSession();
	}

	protected void Page_Load(object sender, EventArgs e)
	{
        this.btnValidate.OnClientClick = "$find('" + this.ClientID + "').btnValidate_Click(); return false;";

        this.ddlSnmpVersion.Attributes["onchange"] = "$find('" + this.ClientID + "').ddlSnmpVersion_Changed();";

        this.tbRWCommunityString.Attributes.Add("value", (this.RWCommunityString == " ") ? string.Empty : this.RWCommunityString);
    }

	protected void imbtnValidateSNMP_Click(object sender, ImageClickEventArgs e)
	{
        //if (string.IsNullOrEmpty(this.Ip))
        //{
        //    SNMPValidationFalse.Visible = true;
        //    SNMPValidationTrue.Visible = false;
        //    return;
        //}
        //if (!System.Text.RegularExpressions.Regex.Match(this.Ip, @"([0-9]{1,3}\.){3,3}[0-9]{1,3}").Success)
        //{
        //    try
        //    {
        //        IPAddress ip = Dns.GetHostAddresses(this.Ip)[0];
        //        this.Ip = ip.ToString();
        //    }
        //    catch
        //    {
        //        SNMPValidationFalse.Visible = true;
        //        SNMPValidationTrue.Visible = false;
        //        return;
        //    }
        //}
        //bool validationSucceeded;
        //string msg = string.Empty;

        //try
        //{
        //    using (var proxy = NPMBusinessLayerProxy.CreateNPMBusinessLayerProxy(BusinessLayerExceptionHandler, EngineID, false))
        //    {
        //        validationSucceeded = NodeWorkflowHelper.ValidateSNMPNode(proxy, this.SNMPVersion, this.Ip, this.SNMPPort, this.CommunityString, this.RWCommunityString,
        //            this.AuthPasswordKey, this.RWAuthPasswordKey, this.SNMPv3AuthType, this.RWSNMPv3AuthType, this.SNMPv3PrivacyType, this.RWSNMPv3PrivacyType,
        //            this.PrivacyPasswordKey, this.RWPrivacyPasswordKey, this.SNMPv3Context, this.RWSNMPv3Context, this.SNMPv3Username, this.RWSNMPv3Username);
        //    }
        //}
        //catch (Exception ex)
        //{
        //    validationSucceeded = false;
        //    msg = ex.Message;
        //}

        //SNMPValidationTrue.Visible = validationSucceeded;
        //SNMPValidationFalse.Visible = !validationSucceeded;
        //SNMPValidationFailedMessage.Text = msg;
	}

	public Node FillNodeWithValues(Node node)
	{
		node.SNMPPort = this.SNMPPort;
		node.SNMPVersion = this.SNMPVersion;

		node.NodeSubType = (this.SNMPVersion == SNMPVersion.None ? NodeSubType.ICMP : NodeSubType.SNMP);

		node.ObjectSubType = node.NodeSubType.ToString();


		node.ReadOnlyCredentials.CommunityString = this.CommunityString;
		node.ReadWriteCredentials.CommunityString = this.RWCommunityString;

		if (node.SNMPVersion == SNMPVersion.SNMP3)
		{
			node.ReadOnlyCredentials.SNMPv3AuthType = this.SNMPv3AuthType;
			node.ReadOnlyCredentials.SNMPv3PrivacyType = this.SNMPv3PrivacyType;
			node.ReadOnlyCredentials.SnmpV3Context = this.SNMPv3Context;
			node.ReadOnlyCredentials.SNMPv3UserName = this.SNMPv3Username;
			node.ReadOnlyCredentials.SNMPv3AuthPassword = this.AuthPasswordKey;
			node.ReadOnlyCredentials.SNMPv3PrivacyPassword = this.PrivacyPasswordKey;
			if (!node.CustomProperties.ContainsKey("SNMPV3PrivKeyIsPwd"))
				node.CustomProperties.Add("SNMPV3PrivKeyIsPwd", true);
			else
				node.CustomProperties["SNMPV3PrivKeyIsPwd"] = true;
			if (!node.CustomProperties.ContainsKey("SNMPV3AuthKeyIsPwd"))
				node.CustomProperties.Add("SNMPV3AuthKeyIsPwd", true);
			else
				node.CustomProperties["SNMPV3AuthKeyIsPwd"] = true;

			node.ReadWriteCredentials.SNMPv3AuthType = this.RWSNMPv3AuthType;
			node.ReadWriteCredentials.SNMPv3PrivacyType = this.RWSNMPv3PrivacyType;
			node.ReadWriteCredentials.SnmpV3Context = this.RWSNMPv3Context;
			node.ReadWriteCredentials.SNMPv3UserName = this.RWSNMPv3Username;
			node.ReadWriteCredentials.SNMPv3AuthPassword = this.RWAuthPasswordKey;
			node.ReadWriteCredentials.SNMPv3PrivacyPassword = this.RWPrivacyPasswordKey;
			if (!node.CustomProperties.ContainsKey("RWSNMPV3PrivKeyIsPwd"))
				node.CustomProperties.Add("RWSNMPV3PrivKeyIsPwd", true);
			else
				node.CustomProperties["RWSNMPV3PrivKeyIsPwd"] = true;
			if (!node.CustomProperties.ContainsKey("RWSNMPV3AuthKeyIsPwd"))
				node.CustomProperties.Add("RWSNMPV3AuthKeyIsPwd", true);
			else
				node.CustomProperties["RWSNMPV3AuthKeyIsPwd"] = true;
		}

		return node;
	}

	public void LoadSNMPPropertiesFromSession()
	{
		Node node = NodeWorkflowHelper.Node;

		if (node == null || node.SNMPPort == 0)
			return;

		this.ValidationIp = node.IpAddress;

		if (node.NodeSubType != NodeSubType.SNMP)
			this.Visible = false;

		this.SNMPPort = node.SNMPPort;
		this.SNMPVersion = node.SNMPVersion;

		this.CommunityString = node.ReadOnlyCredentials.CommunityString;
		this.RWCommunityString = node.ReadWriteCredentials.CommunityString;



		if (node.SNMPVersion == SNMPVersion.SNMP3)
		{
			this.SNMPv3AuthType = node.ReadOnlyCredentials.SNMPv3AuthType;
			this.SNMPv3PrivacyType = node.ReadOnlyCredentials.SNMPv3PrivacyType;
			this.SNMPv3Context = node.ReadOnlyCredentials.SnmpV3Context;
			this.SNMPv3Username = node.ReadOnlyCredentials.SNMPv3UserName;
			this.AuthPasswordKey = node.ReadOnlyCredentials.SNMPv3AuthPassword;
			this.PrivacyPasswordKey = node.ReadOnlyCredentials.SNMPv3PrivacyPassword;

			this.RWSNMPv3AuthType = node.ReadWriteCredentials.SNMPv3AuthType;
			this.RWSNMPv3PrivacyType = node.ReadWriteCredentials.SNMPv3PrivacyType;
			this.RWSNMPv3Context = node.ReadWriteCredentials.SnmpV3Context;
			this.RWSNMPv3Username = node.ReadWriteCredentials.SNMPv3UserName;
			this.RWAuthPasswordKey = node.ReadWriteCredentials.SNMPv3AuthPassword;
			this.RWPrivacyPasswordKey = node.ReadWriteCredentials.SNMPv3PrivacyPassword;
		}
	}

	public void SetupSNMPv3CredentialsFromSession()
	{
		if (Session["SNMPv3SNMPv3CredAvailable"] != null)
		{
            roSnmpV3Cred.Username = Session["SNMPv3Username"].ToString();
            roSnmpV3Cred.SNMPv3Context = Session["SNMPv3Context"].ToString();
            roSnmpV3Cred.SNMPv3AuthType = (Session["SNMPv3AuthType"] != null) ?
				(SNMPv3AuthType)Session["SNMPv3AuthType"] : SNMPv3AuthType.None;
            roSnmpV3Cred.AuthPasswordKey = Session["AuthPasswordKey"].ToString();
            roSnmpV3Cred.SNMPv3PrivacyType = (Session["SNMPv3PrivacyType"] != null) ?
				(SNMPv3PrivacyType)Session["SNMPv3PrivacyType"] : SNMPv3PrivacyType.None;
            roSnmpV3Cred.PrivacyPasswordKey = Session["PrivacyPasswordKey"].ToString();

            rwSnmpV3Cred.Username = Session["RWSNMPv3Username"].ToString();
            rwSnmpV3Cred.SNMPv3Context = Session["RWSNMPv3Context"].ToString();
            rwSnmpV3Cred.SNMPv3AuthType = (Session["RWSNMPv3AuthType"] != null) ?
				(SNMPv3AuthType)Session["RWSNMPv3AuthType"] : SNMPv3AuthType.None;
            rwSnmpV3Cred.AuthPasswordKey = Session["RWAuthPasswordKey"].ToString();
            rwSnmpV3Cred.SNMPv3PrivacyType = (Session["RWSNMPv3PrivacyType"] != null) ?
				(SNMPv3PrivacyType)Session["RWSNMPv3PrivacyType"] : SNMPv3PrivacyType.None;
            rwSnmpV3Cred.PrivacyPasswordKey = Session["RWPrivacyPasswordKey"].ToString();
		}
	}

	public void SaveSNMPv3CredentialsToSession()
	{
		Session.Add("SNMPv3SNMPv3CredAvailable", true);
		Session.Add("SNMPv3Username", roSnmpV3Cred.Username);
        Session.Add("SNMPv3Context", roSnmpV3Cred.SNMPv3Context);
        Session.Add("SNMPv3AuthType", roSnmpV3Cred.SNMPv3AuthType);
        Session.Add("AuthPasswordKey", roSnmpV3Cred.AuthPasswordKey);
        Session.Add("SNMPv3PrivacyType", roSnmpV3Cred.SNMPv3PrivacyType);
        Session.Add("PrivacyPasswordKey", roSnmpV3Cred.PrivacyPasswordKey);

        Session.Add("RWSNMPv3Username", rwSnmpV3Cred.Username);
        Session.Add("RWSNMPv3Context", rwSnmpV3Cred.SNMPv3Context);
        Session.Add("RWSNMPv3AuthType", rwSnmpV3Cred.SNMPv3AuthType);
        Session.Add("RWAuthPasswordKey", rwSnmpV3Cred.AuthPasswordKey);
        Session.Add("RWSNMPv3PrivacyType", rwSnmpV3Cred.SNMPv3PrivacyType);
        Session.Add("RWPrivacyPasswordKey", rwSnmpV3Cred.PrivacyPasswordKey);
	}

    #region IScriptControl Members

    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        var descriptor = new ScriptControlDescriptor("Orion.Nodes.Controls.SnmpInfo", this.ClientID);
        descriptor.AddProperty("cardsID", this.cards.ClientID);
        descriptor.AddElementProperty("ddlSnmpVersion", this.ddlSnmpVersion.ClientID);
        descriptor.AddElementProperty("tbSNMPPort", this.tbSNMPPort.ClientID);
        descriptor.AddElementProperty("pnlSNMPV1_2", this.pnlSNMPV1_2.ClientID);
        descriptor.AddElementProperty("tbCommunityString", this.tbCommunityString.ClientID);
        descriptor.AddElementProperty("tbRWCommunityString", this.tbRWCommunityString.ClientID);
        descriptor.AddElementProperty("pnlSNMPV3", this.pnlSNMPV3.ClientID);
        descriptor.AddComponentProperty("roSnmpV3Cred", this.roSnmpV3Cred.ClientID);
        descriptor.AddComponentProperty("rwSnmpV3Cred", this.rwSnmpV3Cred.ClientID);
        descriptor.AddComponentProperty("resultV2Read", this.resultV2Read.ClientID);
        descriptor.AddComponentProperty("resultV2ReadWrite", this.resultV2ReadWrite.ClientID);
        descriptor.AddElementProperty("btnValidate", this.btnValidate.ClientID);
        descriptor.AddElementProperty("validationProgress", this.validationProgress.ClientID);
        descriptor.AddProperty("validationIp", this.ValidationIp);
        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        yield return new ScriptReference(this.ResolveUrl("SnmpInfo.js"));
    }

    #endregion
}
