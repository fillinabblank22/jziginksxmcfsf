<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SnmpTestResultControl.ascx.cs"
    Inherits="Orion_Nodes_Controls_SnmpTestResultControl" %>

<span id="elSuccess" runat="server" style="vertical-align: middle;"><span class="snmpTest success">
	<img src="/Orion/images/nodemgmt_art/icons/icon_OK.gif" alt="<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_71 %>" />
	<span><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_72 %></span>
</span></span>
<span id="elFailed" runat="server" style="vertical-align: middle;"><span class="snmpTest failure">
	<img src="/Orion/Voip/images/stop_16x16.gif" alt="<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_73 %>" />
	<span><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_74 %></span>
</span></span>
