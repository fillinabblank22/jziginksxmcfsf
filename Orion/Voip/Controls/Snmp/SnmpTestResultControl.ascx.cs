using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using SolarWinds.Logging;
using SolarWinds.NPM.Common;
using SolarWinds.NPM.Common.Models;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Nodes_Controls_SnmpTestResultControl : ScriptUserControlBase
{
	public bool? Result { get; set; }

	protected void Page_Load(object sender, EventArgs e)
	{
		if (this.Result.HasValue)
		{
			if (this.Result.Value)
			{
				this.elFailed.Style[HtmlTextWriterStyle.Display] = "none";
			}
			else
			{
				this.elSuccess.Style[HtmlTextWriterStyle.Display] = "none";
			}
		}
		else
		{
			this.elFailed.Style[HtmlTextWriterStyle.Display] = "none";
			this.elSuccess.Style[HtmlTextWriterStyle.Visibility] = "hidden";
		}
	}

    protected override void RenderWrapperElementBegin(HtmlTextWriter writer)
    {
        writer.AddAttribute("id", this.ClientID);
        writer.RenderBeginTag(HtmlTextWriterTag.Span);
    }

    #region IScriptControl Members

    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        var descriptor = new ScriptControlDescriptor("Orion.Nodes.Controls.SnmpTestResultControl", this.ClientID);
        descriptor.AddElementProperty("elSuccess", this.elSuccess.ClientID);
        descriptor.AddElementProperty("elFailed", this.elFailed.ClientID);
        descriptor.AddProperty("result", this.Result);
        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        yield return new ScriptReference(this.ResolveUrl("SnmpTestResultControl.js"));
    }

    #endregion
}