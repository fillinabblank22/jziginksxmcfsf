using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Nodes_Controls_NodeSNMPv3Credentials : ScriptUserControlBase
{
	private static readonly Log log = new Log();	

	public string Title
	{
		get { return (string)ViewState[this.ID + "_Title"]; }
		set { ViewState[this.ID + "_Title"] = value; }
	}

	public bool Enabled
	{
		get {
			if (ViewState[this.ID + "_Enabled"] == null)
				ViewState[this.ID + "_Enabled"] = true;
			return (bool)ViewState[this.ID + "_Enabled"];
		}
		set {		
			ViewState[this.ID + "_Enabled"] = value;
			if (!value)
			{
				tbPasswordKey.Enabled = value;
				tbSecurityPasswordKey.Enabled = value;
				ddlSecurityMethod.Enabled = value;
				tbPasswordKey.CssClass = "disabled";
				tbSecurityPasswordKey.CssClass = "disabled";
			}
			else
			{
				tbPasswordKey.CssClass = "";
				tbSecurityPasswordKey.CssClass = "";
			}

			tbSNMPv3Context.Enabled = value;
			tbSNMPv3Username.Enabled = value;

			tbSNMPv3Context.CssClass = tbSNMPv3Context.Enabled ? string.Empty : "disabled";
			tbSNMPv3Username.CssClass = tbSNMPv3Username.Enabled ? string.Empty : "disabled";

			//Credentials set
			txtCredentialsName.Enabled = value;
			ddlCredentialsSet.Enabled = value;
			btnDelete.Enabled = value;
			btnSave.Enabled = value;

			ddlAuthMethod.Enabled = value;
		}
	}

	public SNMPv3AuthType SNMPv3AuthType
	{
		get
		{
			switch (ddlAuthMethod.SelectedValue)
			{
				case "None":
					return SNMPv3AuthType.None;
				case "MD5":
					return SNMPv3AuthType.MD5;
				case "SHA1":
					return SNMPv3AuthType.SHA1;
				default:
					return SNMPv3AuthType.None;
			}
		}
		set 	{
			switch (value)
			{
				case SNMPv3AuthType.MD5:
					ddlAuthMethod.SelectedIndex = 1;
					break;
				case SNMPv3AuthType.SHA1:
					ddlAuthMethod.SelectedIndex = 2;
					break;
				default:
					ddlAuthMethod.SelectedIndex = 0;
					break;
			}
		}
	}

	public SNMPv3PrivacyType SNMPv3PrivacyType
	{
		get
		{
			switch (ddlSecurityMethod.SelectedValue)
			{
				case "None":
					return SNMPv3PrivacyType.None;
				case "DES56":
					return SNMPv3PrivacyType.DES56;
				case "AES128":
					return SNMPv3PrivacyType.AES128;
                case "AES192":
                    return SNMPv3PrivacyType.AES192;
                case "AES256":
                    return SNMPv3PrivacyType.AES256;
                default:
					return SNMPv3PrivacyType.None;
			}
		}
		set
		{
			switch (value)
			{
				case SNMPv3PrivacyType.DES56:
					ddlSecurityMethod.SelectedIndex = 1; break;
				case SNMPv3PrivacyType.AES128:
					ddlSecurityMethod.SelectedIndex = 2; break;
                case SNMPv3PrivacyType.AES192:
                    ddlSecurityMethod.SelectedIndex = 3; break;
                case SNMPv3PrivacyType.AES256:
                    ddlSecurityMethod.SelectedIndex = 4; break;
                default:
					ddlSecurityMethod.SelectedIndex = 0; break;
			}
		}
	}

	public string AuthPasswordKey
	{
		get { return tbPasswordKey.Text; }
		set { tbPasswordKey.Text = value; }
	}

	public string PrivacyPasswordKey
	{
		get { return tbSecurityPasswordKey.Text; }
		set { tbSecurityPasswordKey.Text = value; }
	}

	public string Username
	{
		get { return tbSNMPv3Username.Text; }
		set { tbSNMPv3Username.Text = value; }
	}

	public string SNMPv3Context
	{
		get { return tbSNMPv3Context.Text; }
		set { tbSNMPv3Context.Text = value; }
	}

	protected void Page_Load(object sender, EventArgs e)
	{
        this.btnSave.OnClientClick = "$find('" + this.ClientID + "').btnSave_Click(); return false;";
        this.btnDelete.OnClientClick = "$find('" + this.ClientID + "').btnDelete_Click(); return false;";
        this.btnValidate.OnClientClick = "$find('" + this.ClientID + "').btnValidate_Click(); return false;";

        this.ddlAuthMethod.Attributes["onchange"] = "$find('" + this.ClientID + "').OnAuthMethodChanged();";
        this.ddlSecurityMethod.Attributes["onchange"] = "$find('" + this.ClientID + "').OnSecurityMethodChanged();";
        this.ddlCredentialsSet.Attributes["onchange"] = "$find('" + this.ClientID + "').OnCredentialsSetChanged();";

		this.tbPasswordKey.Attributes.Add("value", this.AuthPasswordKey);
		this.tbSecurityPasswordKey.Attributes.Add("value", this.PrivacyPasswordKey);

		if (!IsPostBack)
		{
			LoadCredentialsSet();
		}
	}

	private void LoadCredentialsSet()
	{
        using (var proxy = CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(BusinessLayerExceptionHandler))
		{
			var credentialsSet = proxy.GetCredentialsSet();
			ddlCredentialsSet.DataSource = credentialsSet;
			ddlCredentialsSet.DataBind();
			ddlCredentialsSet.Items.Insert(0, new ListItem(string.Empty, "-1"));
		}
	}

    private static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    #region IScriptControl Members

    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        var descriptor = new ScriptControlDescriptor("Orion.Nodes.Controls.NodeSNMPv3Credentials", this.ClientID);
        descriptor.AddElementProperty("tbSNMPv3Username", this.tbSNMPv3Username.ClientID);
        descriptor.AddElementProperty("tbSNMPv3Context", this.tbSNMPv3Context.ClientID);
        descriptor.AddElementProperty("ddlAuthMethod", this.ddlAuthMethod.ClientID);
        descriptor.AddElementProperty("tbPasswordKey", this.tbPasswordKey.ClientID);
        descriptor.AddElementProperty("ddlSecurityMethod", this.ddlSecurityMethod.ClientID);
        descriptor.AddElementProperty("tbSecurityPasswordKey", this.tbSecurityPasswordKey.ClientID);
        descriptor.AddElementProperty("txtCredentialsName", this.txtCredentialsName.ClientID);
        descriptor.AddElementProperty("ddlCredentialsSet", this.ddlCredentialsSet.ClientID);
        descriptor.AddElementProperty("btnValidate", this.btnValidate.ClientID);
        descriptor.AddComponentProperty("result", this.result.ClientID);
        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        yield return new ScriptReference(this.ResolveUrl("NodeSNMPv3Credentials.js"));
    }

    #endregion
}