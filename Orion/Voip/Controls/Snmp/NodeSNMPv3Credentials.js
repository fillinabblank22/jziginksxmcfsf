﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Nodes.Controls");

Orion.Nodes.Controls.NodeSNMPv3Credentials = function(element) {
    Orion.Nodes.Controls.NodeSNMPv3Credentials.initializeBase(this, [element]);
    this.tbSNMPv3Username = null;
    this.tbSNMPv3Context = null;
    this.ddlAuthMethod = null;
    this.tbPasswordKey = null;
    this.ddlSecurityMethod = null;
    this.tbSecurityPasswordKey = null;
    this.txtCredentialsName = null;
    this.ddlCredentialsSet = null;
	this.btnValidate = null;
    this.result = null;
}

Orion.Nodes.Controls.NodeSNMPv3Credentials.prototype = {

    add_Validate: function(handler) {
        this.get_events().addHandler('Validate', handler);
    },
    remove_Validate: function(handler) {
        this.get_events().removeHandler('Validate', handler);
    },
    get_ValidateButtonVisible: function() {
        return (this.btnValidate.style.display != 'none');
    },
    set_ValidateButtonVisible: function(value) {
        this.btnValidate.style.display = (value ? 'inline-block' : 'none');
    },
    get_ValidationResult: function() {
        return this.result.get_Result();
    },
    set_ValidationResult: function(value) {
        this.result.set_Result(value);
    },

    get_Username: function() {
        return this.tbSNMPv3Username.value;
    },
    set_Username: function(value) {
        this.tbSNMPv3Username.value = value;
    },

    get_Context: function() {
        return this.tbSNMPv3Context.value;
    },
    set_Context: function(value) {
        this.tbSNMPv3Context.value = value;
    },


    get_AuthMethod: function() {
        return this.ddlAuthMethod.options[this.ddlAuthMethod.selectedIndex].value;
    },
    set_AuthMethod: function(value) {
        for (var i = 0; i < this.ddlAuthMethod.options.length; i++) {
            if (value == this.ddlAuthMethod.options[i].value) {
                value = i;
                break;
            }
        }
        this.ddlAuthMethod.selectedIndex = value;
        this.OnAuthMethodChanged();
    },

    get_AuthKey: function() {
        return this.tbPasswordKey.value;
    },
    set_AuthKey: function(value) {
        this.tbPasswordKey.value = value;
    },

    get_SecurityMethod: function() {
        return this.ddlSecurityMethod.options[this.ddlSecurityMethod.selectedIndex].value;
    },
    set_SecurityMethod: function(value) {
        for (var i = 0; i < this.ddlSecurityMethod.options.length; i++) {
            if (value == this.ddlSecurityMethod.options[i].value) {
                value = i;
                break;
            }
        }
        this.ddlSecurityMethod.selectedIndex = value;
        this.OnSecurityMethodChanged();
    },

    get_SecurityKey: function() {
        return this.tbSecurityPasswordKey.value;
    },
    set_SecurityKey: function(value) {
        this.tbSecurityPasswordKey.value = value;
    },

    get_tbSNMPv3Username: function() {
        return this.tbSNMPv3Username;
    },
    set_tbSNMPv3Username: function(value) {
        this.tbSNMPv3Username = value;
    },
    get_tbSNMPv3Context: function() {
        return this.tbSNMPv3Context;
    },
    set_tbSNMPv3Context: function(value) {
        this.tbSNMPv3Context = value;
    },
    get_ddlAuthMethod: function() {
        return this.ddlAuthMethod;
    },
    set_ddlAuthMethod: function(value) {
        this.ddlAuthMethod = value;
    },
    get_tbPasswordKey: function() {
        return this.tbPasswordKey;
    },
    set_tbPasswordKey: function(value) {
        this.tbPasswordKey = value;
    },
    get_ddlSecurityMethod: function() {
        return this.ddlSecurityMethod;
    },
    set_ddlSecurityMethod: function(value) {
        this.ddlSecurityMethod = value;
    },
    get_tbSecurityPasswordKey: function() {
        return this.tbSecurityPasswordKey;
    },
    set_tbSecurityPasswordKey: function(value) {
        this.tbSecurityPasswordKey = value;
    },
    get_txtCredentialsName: function() {
        return this.txtCredentialsName;
    },
    set_txtCredentialsName: function(value) {
        this.txtCredentialsName = value;
    },
    get_ddlCredentialsSet: function() {
        return this.ddlCredentialsSet;
    },
    set_ddlCredentialsSet: function(value) {
        this.ddlCredentialsSet = value;
    },
    get_result: function() {
        return this.result;
    },
    set_result: function(value) {
        this.result = value;
    },
    get_btnValidate: function() {
        return this.btnValidate;
    },
    set_btnValidate: function(value) {
        this.btnValidate = value;
    },

    btnSave_Click: function() {
        var credentialsName = this.txtCredentialsName.value;
        if (!credentialsName || credentialsName.length == 0) {
            alert('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_133;E=js}');
            return;
        }

        if (this.CredentialsExist(credentialsName)) {
            if (!confirm(String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_134;E=js}', credentialsName))) {
                return;
            }
        }

        Orion.Nodes.Controls.NodeSNMPv3CredentialsService.SaveCredentials(credentialsName,
            this.tbSNMPv3Username.value,
			this.tbSNMPv3Context.value,
			this.ddlAuthMethod.options[this.ddlAuthMethod.selectedIndex].value, // drop-down
			this.ddlSecurityMethod.options[this.ddlSecurityMethod.selectedIndex].value, // drop-down
			this.tbSecurityPasswordKey.value,
			this.tbPasswordKey.value,
			Function.createDelegate(this, function(result, context) {
			    window.alert(String.format("@{R=VNQM.Strings;K=VNQMWEBJS_AK1_135;E=js}", context));
			    this.LoadCredentialsSet(context);
			}),
			Function.createDelegate(this, function(error, context) {
			    window.alert(String.format("@{R=VNQM.Strings;K=VNQMWEBJS_AK1_136;E=js}", context, '\n\n' + error.get_message()));
			}),
			credentialsName
        );
    },

    btnDelete_Click: function() {
        if (this.ddlCredentialsSet.selectedIndex <= 0) {
            window.alert('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_137;E=js}');
            return;
        }

        var credentialsName = this.ddlCredentialsSet.options[this.ddlCredentialsSet.selectedIndex].value;
        if (!confirm(String.format("@{R=VNQM.Strings;K=VNQMWEBJS_AK1_138;E=js}", credentialsName))) {
            return;
        }

        Orion.Nodes.Controls.NodeSNMPv3CredentialsService.DeleteCredentials(credentialsName,
            Function.createDelegate(this, function(result, context) {
                this.LoadCredentialsSet();
            }),
			Function.createDelegate(this, function(error, context) {
			    window.alert(String.format("@{R=VNQM.Strings;K=VNQMWEBJS_AK1_136;E=js}", context, '\n\n' + error.get_message()));
			}),
			credentialsName
        );
    },

    LoadCredentialsSet: function(preselect) {
        Orion.Nodes.Controls.NodeSNMPv3CredentialsService.GetCredentialsSet(
            Function.createDelegate(this, function(result, context) {
                this.ddlCredentialsSet.options.length = 0;
                var newOption = document.createElement("option");
                newOption.value = '';
                newOption.text = '';
                this.ddlCredentialsSet.options.add(newOption);
                for (var i = 0; i < result.length; i++) {
                    newOption = document.createElement("option");
                    newOption.value = result[i];
                    newOption.text = result[i];
                    this.ddlCredentialsSet.options.add(newOption);
                    if (result[i] == context) {
                        newOption.selected = true;
                    }
                }
            }),
			Function.createDelegate(this, function(error, context) {
			    window.alert(String.format("@{R=VNQM.Strings;K=VNQMWEBJS_AK1_139;E=js}", '\n\n' + error.get_message()));
			}),
			preselect
        );
    },

    CredentialsExist: function(credentialsName) {
        for (var i = 0; i < this.ddlCredentialsSet.options.length; i = i + 1) {
            if (this.ddlCredentialsSet.options[i].value == credentialsName) { //TODO make it case-insensitive
                return true;
            }
        }
        return false;
    },

    OnAuthMethodChanged: function() {
        var noAuthMethod = (this.ddlAuthMethod.options[this.ddlAuthMethod.selectedIndex].value == "None");
        this.tbPasswordKey.disabled = noAuthMethod;
        this.ddlSecurityMethod.disabled = noAuthMethod;
        if (noAuthMethod) {
            this.ddlSecurityMethod.selectedIndex = 0;
            this.tbPasswordKey.value = '';
        }
        this.OnSecurityMethodChanged();
    },

    OnSecurityMethodChanged: function() {
        var noSecurityMethod = (this.ddlSecurityMethod.options[this.ddlSecurityMethod.selectedIndex].value == "None");
        this.tbSecurityPasswordKey.disabled = noSecurityMethod;
        if (noSecurityMethod) {
            this.tbSecurityPasswordKey.value = '';
        }
    },

    OnCredentialsSetChanged: function () {
        var credentialsName = '';
        if (this.ddlCredentialsSet.options[this.ddlCredentialsSet.selectedIndex].value !== '-1') {
            credentialsName = this.ddlCredentialsSet.options[this.ddlCredentialsSet.selectedIndex].value;
        }
        if (credentialsName) {
            Orion.Nodes.Controls.NodeSNMPv3CredentialsService.GetCredentials(credentialsName,
                Function.createDelegate(this, function (result, context) {                    

                    this.tbSNMPv3Username.value = result.SNMPv3UserName;
                    this.tbSNMPv3Context.value = result.SnmpV3Context;

                    if (!result.SNMPV3AuthKeyIsPwd) {
                        this.ddlAuthMethod.selectedIndex = 0; // "None"
                        this.OnAuthMethodChanged();
                        this.tbPasswordKey.value = '';
                    }
                    else {
                        this.ddlAuthMethod.selectedIndex = result.SNMPv3AuthType;
                        this.OnAuthMethodChanged();
                        this.tbPasswordKey.value = result.SNMPv3AuthPassword;
                    }

                    if (!result.SNMPV3PrivKeyIsPwd) {
                        this.ddlSecurityMethod.selectedIndex = 0; // "None"
                        this.OnSecurityMethodChanged();
                        this.tbSecurityPasswordKey.value = '';
                    }
                    else {
                        this.ddlSecurityMethod.selectedIndex = result.SNMPv3PrivacyType;
                        this.OnSecurityMethodChanged();
                        this.tbSecurityPasswordKey.value = result.SNMPv3PrivacyPassword;
                    }
                }),
                Function.createDelegate(this, function (error, context) {
                    window.alert(String.format("@{R=VNQM.Strings;K=VNQMWEBJS_AK1_140;E=js}", context, '\n\n' + error.get_message()));
                }),
                credentialsName
            );
        }
        this.txtCredentialsName.value = credentialsName;        
    },

    SelectDropDownValue: function(ddlCtl, value) {
        for (var i = 0; i < ddlCtl.options.length; i++) {
            if (ddlCtl.options[i].value == value) {
                ddlCtl.selectedIndex = i;
                return;
            }
        }
        ddlCtl.selectedIndex = -1;
    },

    btnValidate_Click: function() {
        var handlers = this.get_events().getHandler('Validate');
        if (handlers) {
            handlers(this, Sys.EventArgs.Empty);
        }
    },

    initialize: function() {
        Orion.Nodes.Controls.NodeSNMPv3Credentials.callBaseMethod(this, 'initialize');

        // Add custom initialization here
        this.OnAuthMethodChanged();
        this.OnSecurityMethodChanged();
    },
    dispose: function() {
        //Add custom dispose actions here
        Orion.Nodes.Controls.NodeSNMPv3Credentials.callBaseMethod(this, 'dispose');
    }
}
Orion.Nodes.Controls.NodeSNMPv3Credentials.registerClass('Orion.Nodes.Controls.NodeSNMPv3Credentials', Sys.UI.Control);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
