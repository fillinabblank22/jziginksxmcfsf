﻿/// <reference name="MicrosoftAjax.js"/>
/// <reference path="~/Orion/js/jquery.js" />
/// <reference path="~/Orion/js/extjs/debug/Ext.js" />

Type.registerNamespace("Orion.Nodes.Controls");

Orion.Nodes.Controls.SnmpInfo = function(element) {
    Orion.Nodes.Controls.SnmpInfo.initializeBase(this, [element]);
    this.cardsID = null;
    this.snmpV3Tabs = null;
    this.switchPanel = null;
    this.ddlSnmpVersion = null;
    this.tbSNMPPort = null;
    this.pnlSNMPV1_2 = null;
    this.tbCommunityString = null;
    this.tbRWCommunityString = null;
    this.pnlSNMPV3 = null;
    this.roSnmpV3Cred = null;
    this.rwSnmpV3Cred = null;
    this.resultV2Read = null;
    this.resultV2ReadWrite = null;
    this.btnValidate = null;
    this.validationProgress = null;
    this.validationIp = null;
    this.validationEngineId = null;
}

Orion.Nodes.Controls.SnmpInfo.prototype = {

    get_ValidationIp: function () { return this.validationIp; },
    set_ValidationIp: function (value) {
        this.validationIp = value;
        this.btnValidate.style.display = (value != null ? 'inline-block' : 'none');
        this.roSnmpV3Cred.set_ValidateButtonVisible(value != null);
        this.rwSnmpV3Cred.set_ValidateButtonVisible(value != null);
    },

    get_ValidationEngineId: function () { return this.validationEngineId; },
    set_ValidationEngineId: function (value) { this.validationEngineId = value; },

    PreFillNodeCredentials: function (nodeIds) {
        this.resultV2Read.set_Result(null);
        this.resultV2ReadWrite.set_Result(null);
        this.roSnmpV3Cred.set_ValidationResult(null);
        this.rwSnmpV3Cred.set_ValidationResult(null);
        Orion.Nodes.Controls.SnmpInfoService.GetCommonSnmpCredentials(
            nodeIds,
            Function.createDelegate(this, function (result, context) {
                this.tbSNMPPort.value = (result.SnmpPort != null ? result.SnmpPort : ''),
                this.ddlSnmpVersion.selectedIndex = result.SnmpVersion - 1;
                this.SetupSnmpCredentials(this.tbCommunityString, this.roSnmpV3Cred, result.ReadOnly);
                this.SetupSnmpCredentials(this.tbRWCommunityString, this.rwSnmpV3Cred, result.ReadWrite);
                this.ddlSnmpVersion_Changed();
            }),
            function (error, context) {
                // ignore
            },
            null
        );
    },

    SaveNodeCredentials: function (nodeIds, resultHandler, errorHandler) {
        if (CheckDemoMode()) return false;

        var snmpPort = this.GetSnmpPort();
        if (snmpPort == null) return false;

        Orion.Nodes.Controls.SnmpInfoService.SaveSnmpCredentials(
            nodeIds,
            this.ddlSnmpVersion.options[this.ddlSnmpVersion.selectedIndex].value,
            snmpPort,
        // Read only
            this.BuildSnmpCredentials(this.tbCommunityString.value, this.roSnmpV3Cred),
        // Read/Write
            this.BuildSnmpCredentials(this.tbRWCommunityString.value, this.rwSnmpV3Cred),
            function (result, context) { resultHandler(); },
            function (error, context) { errorHandler(error); },
            null
        );

        return true;
    },

    get_ddlSnmpVersion: function () { return this.ddlSnmpVersion; },
    set_ddlSnmpVersion: function (value) { this.ddlSnmpVersion = value; },

    get_tbSNMPPort: function () { return this.tbSNMPPort; },
    set_tbSNMPPort: function (value) { this.tbSNMPPort = value; },

    get_pnlSNMPV1_2: function () { return this.pnlSNMPV1_2; },
    set_pnlSNMPV1_2: function (value) { this.pnlSNMPV1_2 = value; },

    get_pnlSNMPV3: function () { return this.pnlSNMPV3; },
    set_pnlSNMPV3: function (value) { this.pnlSNMPV3 = value; },

    get_tbCommunityString: function () { return this.tbCommunityString; },
    set_tbCommunityString: function (value) { this.tbCommunityString = value; },

    get_tbRWCommunityString: function () { return this.tbRWCommunityString; },
    set_tbRWCommunityString: function (value) { this.tbRWCommunityString = value; },

    get_roSnmpV3Cred: function () { return this.roSnmpV3Cred; },
    set_roSnmpV3Cred: function (value) { this.roSnmpV3Cred = value; },

    get_rwSnmpV3Cred: function () { return this.rwSnmpV3Cred; },
    set_rwSnmpV3Cred: function (value) { this.rwSnmpV3Cred = value; },

    get_resultV2Read: function () { return this.resultV2Read; },
    set_resultV2Read: function (value) { this.resultV2Read = value; },

    get_resultV2ReadWrite: function () { return this.resultV2ReadWrite; },
    set_resultV2ReadWrite: function (value) { this.resultV2ReadWrite = value; },

    get_btnValidate: function () { return this.btnValidate; },
    set_btnValidate: function (value) { this.btnValidate = value; },

    get_validationProgress: function () { return this.validationProgress; },
    set_validationProgress: function (value) { this.validationProgress = value; },

    ShowValidationMask: function () {
        var progress = $(this.validationProgress);
        var container = $(this.get_element());
        progress.width(container.width());
        progress.height(container.height());
        progress.find('.background').css('opacity', 0.7);
        this.validationProgress.style.display = 'block';

        if (Ext.isIE6) {
            jQuery('select', this.get_element()).css('visibility', 'hidden');
        }
    },

    HideValidationMask: function () {
        this.validationProgress.style.filter = '';
        this.validationProgress.style.display = 'none';

        if (Ext.isIE6) {
            jQuery('select', this.get_element()).css('visibility', 'visible');
        }

    },

    ddlSnmpVersion_Changed: function () {
        this.switchPanel.getLayout().setActiveItem(this.ddlSnmpVersion.selectedIndex != 2 ? 0 : 1);
        this.switchPanel.syncSize();
    },

    BuildSnmpCredentials: function (community, snmpV3Cred) {
        return {
            CredentialName: snmpV3Cred.txtCredentialsName.value,
            CommunityString: community,
            SNMPv3UserName: snmpV3Cred.get_Username(),
            SnmpV3Context: snmpV3Cred.get_Context(),
            SNMPv3AuthType: snmpV3Cred.get_AuthMethod(),
            SNMPV3AuthKeyIsPwd: snmpV3Cred.get_AuthMethod() != 'None',
            SNMPv3AuthPassword: snmpV3Cred.get_AuthKey(),
            SNMPv3PrivacyType: snmpV3Cred.get_SecurityMethod(),
            SNMPV3PrivKeyIsPwd: snmpV3Cred.get_SecurityMethod() != 'None',
            SNMPv3PrivacyPassword: snmpV3Cred.get_SecurityKey()
        }
    },

    SetupSnmpCredentials: function (tbCommunity, snmpV3Cred, snmpCred) {
        tbCommunity.value = (snmpCred.CommunityString != null ? snmpCred.CommunityString : '');
        snmpV3Cred.set_Username(snmpCred.SNMPv3UserName != null ? snmpCred.SNMPv3UserName : '');
        snmpV3Cred.set_Context(snmpCred.SnmpV3Context != null ? snmpCred.SnmpV3Context : '');
        snmpV3Cred.set_AuthMethod(snmpCred.SNMPv3AuthType != null ? snmpCred.SNMPv3AuthType : '');
        snmpV3Cred.set_AuthKey(snmpCred.SNMPv3AuthPassword != null ? snmpCred.SNMPv3AuthPassword : '');
        snmpV3Cred.set_SecurityMethod(snmpCred.SNMPv3PrivacyType != null ? snmpCred.SNMPv3PrivacyType : '');
        snmpV3Cred.set_SecurityKey(snmpCred.SNMPv3PrivacyPassword != null ? snmpCred.SNMPv3PrivacyPassword : '');
    },

    GetSnmpPort: function () {
        var port = this.tbSNMPPort.value;
        if (port == null || port == '' || isNaN(port)) {
            this.tbSNMPPort.focus();
            alert('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_68;E=js}');
            return null;
        }
        return eval(port);
    },

    btnValidate_Click: function () {
        if (CheckDemoMode()) return false;

        var snmpPort = this.GetSnmpPort();
        if (snmpPort == null) return;

        this.ShowValidationMask();
        Orion.Nodes.Controls.SnmpInfoService.ValidateSnmpNode(
            this.validationEngineId,
            this.validationIp, snmpPort,
            this.ddlSnmpVersion.selectedIndex >= 0 ? this.ddlSnmpVersion.options[this.ddlSnmpVersion.selectedIndex].value : 'None',
        // Read only
            (this.tbCommunityString.value.length > 0),
            this.BuildSnmpCredentials(this.tbCommunityString.value, this.roSnmpV3Cred),
        // Read/Write
            (this.tbRWCommunityString.value.length > 0),
            this.BuildSnmpCredentials(this.tbRWCommunityString.value, this.rwSnmpV3Cred),
            Function.createDelegate(this, function (result, context) {
                this.HideValidationMask();
                this.resultV2Read.set_Result(result.ReadAccess);
                this.resultV2ReadWrite.set_Result(result.ReadWriteAccess);
            }),
            Function.createDelegate(this, function (error, context) {
                this.HideValidationMask();
                window.alert(String.format("@{R=VNQM.Strings;K=VNQMWEBJS_AK1_69;E=js}", error.get_message()));
            }),
            null
        );
    },

    snmpV3Cred_Validate: function (sender, e) {
        if (CheckDemoMode()) return false;

        var snmpPort = this.GetSnmpPort();
        if (snmpPort == null) return;

        if (sender == this.roSnmpV3Cred) {
            this.ShowValidationMask();
            Orion.Nodes.Controls.SnmpInfoService.ValidateSnmpNode(
                this.validationEngineId,
                this.validationIp, snmpPort,
                this.ddlSnmpVersion.options[this.ddlSnmpVersion.selectedIndex].value,
            // Read only
                true,
                this.BuildSnmpCredentials(this.tbCommunityString.value, this.roSnmpV3Cred),
            // Read/Write
                false,
                this.BuildSnmpCredentials(this.tbRWCommunityString.value, this.rwSnmpV3Cred),
                Function.createDelegate(this, function (result, context) {
                    this.HideValidationMask();
                    this.roSnmpV3Cred.set_ValidationResult(result.ReadAccess);
                }),
                Function.createDelegate(this, function (error, context) {
                    this.HideValidationMask();
                    window.alert(String.format("@{R=VNQM.Strings;K=VNQMWEBJS_AK1_69;E=js}", error.get_message()));
                }),
                null
            );
        }
        else if (sender == this.rwSnmpV3Cred) {
            this.ShowValidationMask();
            Orion.Nodes.Controls.SnmpInfoService.ValidateSnmpNode(
                this.validationEngineId,
                this.validationIp, snmpPort,
                this.ddlSnmpVersion.options[this.ddlSnmpVersion.selectedIndex].value,
            // Read only
                false,
                this.BuildSnmpCredentials(this.tbCommunityString.value, this.roSnmpV3Cred),
            // Read/Write
                true,
                this.BuildSnmpCredentials(this.tbRWCommunityString.value, this.rwSnmpV3Cred),
                Function.createDelegate(this, function (result, context) {
                    this.HideValidationMask();
                    this.rwSnmpV3Cred.set_ValidationResult(result.ReadWriteAccess);
                }),
                Function.createDelegate(this, function (error, context) {
                    this.HideValidationMask();
                    window.alert(String.format("@{R=VNQM.Strings;K=VNQMWEBJS_AK1_69;E=js}", error.get_message()));
                }),
                null
            );
        }
    },

    initialize: function () {
        Orion.Nodes.Controls.SnmpInfo.callBaseMethod(this, 'initialize');

        // Add custom initialization here
        Ext.QuickTips.init();

        this.snmpV3Tabs = new Ext.TabPanel({
            renderTo: this.pnlSNMPV3,
            activeTab: 0,
            deferredRender: false,
            layoutOnTabChange: true,
            autoTabs: true,
            autoWidth: true,
            items: [{
                title: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_70;E=js}',
                contentEl: this.rwSnmpV3Cred.get_element()
            }, {
                title: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_71;E=js}',
                contentEl: this.roSnmpV3Cred.get_element()
            }]
        });
        this.switchPanel = new Ext.Panel({
            applyTo: this.cardsID,
            width: 500,
            activeItem: 0,
            layout: 'card',
            layoutConfig: {
                layoutOnCardChange: true
            },
            autoHeight: true,
            border: false,
            defaults: { border: false },
            items: [{ contentEl: this.pnlSNMPV1_2 }, this.snmpV3Tabs]
        });

        this.roSnmpV3Cred.add_Validate(Function.createDelegate(this, this.snmpV3Cred_Validate));
        this.rwSnmpV3Cred.add_Validate(Function.createDelegate(this, this.snmpV3Cred_Validate));

        this.ddlSnmpVersion_Changed();

        this.set_ValidationIp(this.validationIp);
    },
    dispose: function () {
        //Add custom dispose actions here
        Orion.Nodes.Controls.SnmpInfo.callBaseMethod(this, 'dispose');
    }
}
Orion.Nodes.Controls.SnmpInfo.registerClass('Orion.Nodes.Controls.SnmpInfo', Sys.UI.Control);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
