﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Nodes.Controls");

Orion.Nodes.Controls.SnmpTestResultControl = function(element) {
    Orion.Nodes.Controls.SnmpTestResultControl.initializeBase(this, [element]);
    this.elSuccess = null;
	this.elFailed = null;
	this.result = null;
}

Orion.Nodes.Controls.SnmpTestResultControl.prototype = {

    get_elSuccess: function() {
        return this.elSuccess;
    },
    set_elSuccess: function(value) {
        this.elSuccess = value;
    },
    get_elFailed: function() {
        return this.elFailed;
    },
    set_elFailed: function(value) {
        this.elFailed = value;
    },

    get_Result: function() {
        return this.result;
    },
    set_Result: function(value) {
        this.result = value;
		this.elFailed.style.display = (value != null && !value ? 'inline' : 'none');
		this.elSuccess.style.visibility = (value != null ? 'visible' : 'hidden');
		this.elSuccess.style.display = (value == null || value ? 'inline' : 'none');
    },

    initialize: function() {
        Orion.Nodes.Controls.SnmpTestResultControl.callBaseMethod(this, 'initialize');

        // Add custom initialization here
    },
    dispose: function() {
        //Add custom dispose actions here
        Orion.Nodes.Controls.SnmpTestResultControl.callBaseMethod(this, 'dispose');
    }
}
Orion.Nodes.Controls.SnmpTestResultControl.registerClass('Orion.Nodes.Controls.SnmpTestResultControl', Sys.UI.Control);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
