<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SnmpInfo.ascx.cs" Inherits="Orion_Nodes_Controls_NodeSNMP" %>
<%@ Register Src="~/Orion/Voip/Controls/Snmp/NodeSNMPv3Credentials.ascx" TagPrefix="orion" TagName="SnmpV3Cred" %>
<%@ Register Src="~/Orion/Voip/Controls/Snmp/SnmpTestResultControl.ascx" TagPrefix="orion" TagName="SnmpTestResultControl" %>

<asp:ScriptManagerProxy ID="scriptManager" runat="server">
  <Services>
    <asp:ServiceReference Path="~/Orion/Voip/Controls/Snmp/SnmpInfoService.asmx" />
  </Services>
</asp:ScriptManagerProxy>

<style type="text/css">
    .x-menu-list-item { width: 530px !important; white-space: normal; }    
    .x-panel-noborder { width: auto !important; }
    .x-ipsla-form-menu .x-panel-footer {width: auto !important; text-align: right; }    
    .snmpInfo .colInput { width: 90px; }
    .snmpInfo .inputFrame { visibility: visible !important;}
    .snmpInfo .colLabel{ width: 185px !important;}
    .x-btn-wrap .x-btn{ width: 100px !important;}
</style>

<div id="container" runat="server" class="snmpInfo" style="position: relative; width: auto;">

	<table class="snmpHeader">
		<tr>
			<td class="colLabel caption">
				<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_75 %></td>
			<td>
				<asp:DropDownList ID="ddlSnmpVersion" runat="server" CssClass="inputFrame">
					<asp:ListItem Text="SNMPv1" Value="SNMP1" />
					<asp:ListItem Text="SNMPv2c" Value="SNMP2c" Selected="True" />
					<asp:ListItem Text="SNMPv3" Value="SNMP3" />
				</asp:DropDownList>
			</td>
			<td class="caption">
				<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_76 %>
			</td>
			<td>
				<asp:TextBox ID="tbSNMPPort" runat="server" Text="161" CssClass="inputFrame port"></asp:TextBox>
			</td>
		</tr>
	</table>

	<div id="cards" runat="server"></div>
	<div runat="server" id="pnlSNMPV1_2" class="x-hidden">
		<table class="credBox" >
			<tr>
				<td class="colLabel caption">
					<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_77 %>
				</td>
				<td class="colInput" style="width: 90px !important;">
					<asp:TextBox ID="tbCommunityString" runat="server" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_78 %>" CssClass="CommunityStringTextBox input inputFrame"></asp:TextBox>
				</td>
                <td class="colInput" style="width: auto !important;">
                    <orion:SnmpTestResultControl ID="resultV2Read" runat="server" />
                </td>                
			</tr>
			<tr class="hint">
				<td class="colLabel caption">&nbsp;</td>
				<td class="colInput" style="width: auto !important;" colspan="2">
					<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_79 %>
				</td>
                <td>&nbsp;</td>
			</tr>
			<tr>
				<td class="colLabel caption" style="white-space: normal; padding-bottom: 2px">
					<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_80 %>
				</td>
				<td class="colInput" style="width: 90px !important;">
					<asp:TextBox ID="tbRWCommunityString" runat="server" CssClass="input inputFrame" TextMode="Password"></asp:TextBox>
                </td>
                <td class="colInput" style="width: auto !important;">
					<orion:SnmpTestResultControl ID="resultV2ReadWrite" runat="server" />
				</td>
			</tr>
			<tr>
				<td class="colLabel caption">&nbsp;</td>
				<td class="colInput"><br/>
				    <orion:LocalizableButton ID="btnValidate" runat="server" LocalizedText="Test" DisplayType="Small" />
				</td>
				<td>&nbsp;</td>
			</tr>
		</table>
	</div>

	<div runat="server" id="pnlSNMPV3" class="x-hidden">
	</div>
	<orion:SnmpV3Cred runat="server" ID="rwSnmpV3Cred" CssClass="x-hidden" />
	<orion:SnmpV3Cred runat="server" ID="roSnmpV3Cred" CssClass="x-hidden" />

    <div id="validationProgress" class="validation" runat="server" style="position: absolute; left: 0; top: 0; width: 0; height: 0; z-index: 1000; overflow: hidden; display: none;">
        <div class="background"></div>
        <table><tr><td>
            <div class="message">
                <img src="/Orion/images/animated_loading_sm3_whbg.gif" />
                <span><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_81 %></span>
            </div>
        </td></tr></table>
    </div>

</div>
