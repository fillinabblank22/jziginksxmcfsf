using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Resources;

public partial class Orion_Voip_Controls_Grouping : System.Web.UI.UserControl
{
    private List<string> fields;
    private string selectedValue;
    public List<string> GroupContent
    {
        get { return fields; }
        set { fields = value; }
    }

    public string GroupedValue
    {
        get
        {
            if (this.ddlGrouping.SelectedIndex != 0)
                return ddlGrouping.SelectedValue;
            else
                return string.Empty;
        }
        set
        {
            selectedValue = value;
            SelectGroupingField();
        }
    }

    public Orion_Voip_Controls_Grouping()
    {
        fields = new List<string>(new string[] {VNQMWebContent.VNQMWEBCODE_AK1_46, VNQMWebContent.VNQMWEBCODE_AK1_47, VNQMWebContent.VNQMWEBCODE_AK1_48});
        if (SolarWinds.Orion.NPM.Web.Node.GetCustomPropertyNames(true).Count != 0)
            fields.AddRange(SolarWinds.Orion.NPM.Web.Node.GetCustomPropertyNames(true));

    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        
        ddlGrouping.DataSource = fields;
        ddlGrouping.DataBind();

    }

    private void SelectGroupingField()
    {
        if (selectedValue == null)
            this.ddlGrouping.SelectedIndex = 0;
        else
        {
            this.ddlGrouping.SelectedIndex = fields.IndexOf(selectedValue);
        }
    }
}
