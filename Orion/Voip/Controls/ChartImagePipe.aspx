<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register TagPrefix="voip" Namespace="SolarWinds.Orion.IpSla.Web.Charting" Assembly="SolarWinds.Orion.IpSla.Web" %>


<html xmlns="http://www.w3.org/1999/xhtml" >

<body>
    <form id="mainForm" runat="server">
        <voip:SecureImagePipe id="SecureImagePipe" runat="server"/>
    </form>
</body>
</html>
