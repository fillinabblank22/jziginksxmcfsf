<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RelativePeriod.ascx.cs" Inherits="Orion_Voip_Resources_RelativePeriod" %>

<asp:Label ID="Label1" runat="server" CssClass="PeriodSelector"  Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_101 %>" ></asp:Label>&nbsp;
<asp:TextBox runat="server" Columns="5" ID="txtQuantity" />
     <asp:ListBox runat="server" ID="lbxTimeType" Rows="1" SelectionMode="single">
        <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_97 %>" Value="Minutes" />
        <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_98 %>" Value="Hours" />
        <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_99 %>" Value="Days" />
     </asp:ListBox>
<asp:RequiredFieldValidator runat="server" ID="valQuantityRequired" ControlToValidate="txtQuantity" EnableClientScript="false" Text="*" Display="Static" />
<asp:CompareValidator runat="server" ID="valQuantityNumeric" ControlToValidate="txtQuantity" Type="Integer" Operator="DataTypeCheck" EnableClientScript="false" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_100 %>" Display="Static" />