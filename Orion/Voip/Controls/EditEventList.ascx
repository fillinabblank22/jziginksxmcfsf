﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditEventList.ascx.cs"
    Inherits="Orion_Voip_Controls_EditEventList" %>

<p>
    <b><%= maxEventsLabel %></b><br />
    <asp:TextBox runat="server" ID="MaxEventsText" Width="50" />

    <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_212 %>"
                Display="Dynamic" ControlToValidate="MaxEventsText" MinimumValue="1" MaximumValue="5000"
                Type="Integer" />
</p>
