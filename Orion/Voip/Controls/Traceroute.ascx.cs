﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.Security.AntiXss;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.UI.Grid;

public partial class Orion_Voip_Controls_Traceroute : UserControl
{
    public MetricType[] MetricTypes { get; set; }
    public int? OperationInstanceId { get; set; }
    public int? PathId { get; set; }
    public ICollection DataSource { get; set; }

    public bool AlternateRowColors { get; set; }

    /// <summary>
    /// Gets or sets the value specifying whether hops should have hyperlinks navigating to the Hop Details page.
    /// </summary>
    public bool DisableHopHyperlinks { get; set; }

    protected string GetHopUrl(object dataItem)
    {
        if (!OperationInstanceId.HasValue)
            return string.Empty;

        var record = (DataRow)dataItem;
        var hopIndex = (int)record["HopIndex"];
        return IpSlaOperationHelper.GetOperationDetailPageLink(OperationInstanceId.Value, PathId, hopIndex + 1);
    }

    protected string GetHopIndex(object dataItem)
    {
        var record = (DataRow)dataItem;
        var hopIndex = (int)record["HopIndex"];
        return (hopIndex + 1).ToString();
    }

    protected string GetHopName(object dataItem)
    {
        var record = (DataRow)dataItem;
        return AntiXssEncoder.HtmlEncode(record["HopNodeCaption"] as string, true) ??
               AntiXssEncoder.HtmlEncode(record["HopIpAddress"] as string, true);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        EnsureChildControls();
        if (DataSource != null)
        {
            if (AlternateRowColors)
            {
                foreach (var column in ExpandableGrid.Columns)
                {
                    column.ItemCssStyle += "border:0px;";
                }
            }

            var hopList = DataSource.Cast<DataRow>().Where(row => row["HopIndex"] is int).ToArray();

            ExpandableGrid.DataSource = hopList;
            ExpandableGrid.DataBind();

            var numberOfHopsThreshold = ThresholdManager.GetThreshold(OperationInstanceId.Value, ThresholdType.NumberOfHops);
            var hopCount = hopList.Length;
            HopCount.InnerText = string.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_239, hopCount);
            HopCount.Style[HtmlTextWriterStyle.Padding] = "2px 6px 2px 6px";
            HopCount.Style[HtmlTextWriterStyle.Color] = "white";
            var hopCountColor = "#66CC00";
            string hopCountTitle = null;
            if (numberOfHopsThreshold != null)
            {
                if (hopCount >= numberOfHopsThreshold.ErrorLevel)
                {
                    hopCountColor = "red";
                    hopCountTitle = Resources.VNQMWebContent.VNQMWEBCODE_VB1_240;
                }
                else if (hopCount >= numberOfHopsThreshold.WarningLevel)
                {
                    hopCountColor = "gold";
                    hopCountTitle = Resources.VNQMWebContent.VNQMWEBCODE_VB1_240;
                }
            }
            HopCount.Style[HtmlTextWriterStyle.BackgroundColor] = hopCountColor;
            if (!string.IsNullOrEmpty(hopCountTitle))
            {
                HopCount.Attributes["title"] = hopCountTitle;
            }
        }
    }

    protected override void CreateChildControls()
    {
        base.CreateChildControls();

        var metricTypes = MetricTypes;
        if (metricTypes == null || metricTypes.Length == 0)
            return;

        var valueCssWidth = string.Format(CultureInfo.InvariantCulture, "width: {0:D}%;", 2 + 12/metricTypes.Length);
        ExpandableGrid.Columns.RemoveRange(1, ExpandableGrid.Columns.Count - 1);
        ExpandableGrid.HeaderCssClass = "ReportHeader";
        ExpandableGrid.ItemCssStyle = valueCssWidth;
        ExpandableGrid.ItemCssClass = "Property";
        ExpandableGrid.ItemCssStyle = valueCssWidth;
        foreach (var metricType in metricTypes)
        {
            Column column;
            switch (metricType)
            {
                case MetricType.RoundTripTime:
                    column = new Column
                    {
                        Caption = Resources.VNQMWebContent.VNQMWEBCODE_VB1_241,
                        Template = DisableHopHyperlinks ? ValueTemplate.RoundTripTimeDisabled : ValueTemplate.RoundTripTime,
                    };
                    break;
                case MetricType.Jitter:
                    column = new Column
                    {
                        Caption = Resources.VNQMWebContent.VNQMWEBDATA_AK1_163,
                        Template = DisableHopHyperlinks ? ValueTemplate.JitterDisabled : ValueTemplate.Jitter,
                    };
                    break;
                case MetricType.Latency:
                    column = new Column
                    {
                        Caption = Resources.VNQMWebContent.VNQMWEBDATA_AK1_164,
                        Template = DisableHopHyperlinks ? ValueTemplate.LatencyDisabled : ValueTemplate.Latency,
                    };
                    break;
                case MetricType.PacketLoss:
                    column = new Column
                    {
                        Caption = Resources.VNQMWebContent.VNQMWEBDATA_AK1_165,
                        Template = DisableHopHyperlinks ? ValueTemplate.PacketLossDisabled : ValueTemplate.PacketLoss,
                    };
                    break;
                default:
                    continue;
            }
            ExpandableGrid.Columns.Add(column);
        }
    }

    private class ValueTemplate : ITemplate
    {
        public static readonly ValueTemplate RoundTripTime = new ValueTemplate("AvgRoundTripTime", Resources.VNQMWebContent.VNQMWEBDATA_VB1_42, false);
        public static readonly ValueTemplate Latency = new ValueTemplate("AvgLatency", Resources.VNQMWebContent.VNQMWEBDATA_VB1_42, false);
        public static readonly ValueTemplate Jitter = new ValueTemplate("AvgJitter", Resources.VNQMWebContent.VNQMWEBDATA_VB1_42, false);
        public static readonly ValueTemplate PacketLoss = new ValueTemplate("AvgPacketLoss", Resources.VNQMWebContent.VNQMWEBCODE_VB1_204, false);

        //no hyperlinks
        public static readonly ValueTemplate RoundTripTimeDisabled = new ValueTemplate("AvgRoundTripTime", Resources.VNQMWebContent.VNQMWEBDATA_VB1_42, true);
        public static readonly ValueTemplate LatencyDisabled = new ValueTemplate("AvgLatency", Resources.VNQMWebContent.VNQMWEBDATA_VB1_42, true);
        public static readonly ValueTemplate JitterDisabled = new ValueTemplate("AvgJitter", Resources.VNQMWebContent.VNQMWEBDATA_VB1_42, true);
        public static readonly ValueTemplate PacketLossDisabled = new ValueTemplate("AvgPacketLoss", Resources.VNQMWebContent.VNQMWEBCODE_VB1_204, true);

        private readonly string valueName;
        private readonly string unitName;
        private bool disableHyperlink;

        private ValueTemplate(string valueName, string unitName, bool disableHyperlink)
        {
            this.valueName = valueName;
            this.unitName = unitName;
            this.disableHyperlink = disableHyperlink;
        }

        public void InstantiateIn(Control container)
        {
            var columnItem = (GridItemControl)container;
            var dataItem = DataBinder.GetDataItem(container);
            var valueLiteral = new Literal {ID = "value", Visible = false};
            var unitText = new HtmlGenericControl {InnerHtml = "&nbsp;" + this.unitName, Visible = false};
            var naText = new HtmlGenericControl { InnerHtml = String.Format("<i>{0}</i>", Resources.VNQMWebContent.VNQMWEBCODE_VB1_134), Visible = false };
            valueLiteral.DataBinding += (sender, e) =>
                {
                    var value = DataBinder.Eval(dataItem, "['" + this.valueName + "']") as double?;
                    if (value.HasValue)
                    {
                        valueLiteral.Text = value.Value.ToString("0.0");
                        valueLiteral.Visible = unitText.Visible = true;
                    }
                    else
                    {
                        naText.Visible = true;
                    }
                };
            var div = new HtmlGenericControl("div");
            if (disableHyperlink)
            {
                var content = new HtmlGenericControl("span")
                {
                    Controls = { valueLiteral, unitText, naText }
                };
                div.Controls.Add(content);
            }
            else
            {
                var anchor = new HtmlAnchor
                    {
                        HRef = ((Orion_Voip_Controls_Traceroute)columnItem.Grid.Parent).GetHopUrl(dataItem),
                        Controls = { valueLiteral, unitText, naText },
                    }
                ;
                div.Controls.Add(anchor);
            }
            container.Controls.Add(div);
        }
    }
}
