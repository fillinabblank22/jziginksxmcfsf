﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchVoipCalls.ascx.cs" Inherits="Orion_Voip_Controls_SearchVoipCalls" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="orion" TagName="DateTimePicker" Src="~/Orion/Controls/DateTimePicker.ascx" %>

<asp:ScriptManagerProxy ID="scriptManager" runat="server">
    <services>
    <asp:ServiceReference Path="~/Orion/Voip/Services/VoipCallSearchService.asmx" />
  </services>
</asp:ScriptManagerProxy>

<script type="text/javascript">
    $(document).ready(function () {

        $(document).keyup(function (event) {
            if (event.which === 27) {
                $('.time-picker').hide();
            }
        });

    });
</script>

<style type="text/css">
    .ui-datepicker {
        z-index: 9999 !important;
    }
</style>

<asp:UpdatePanel runat="server"> <contenttemplate>

<table width="1080px">
    <tr>
        <td class="voipSearchTableLeftPadding">&nbsp;</td>
        <td style="padding-left: 10px;" colspan="11">
            <asp:RadioButton runat="server" ID="rbSearchCriteriaOR" Checked="False" CssClass="voipSearchText voipSearchFont" GroupName="rbSearchCriteria" /> <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_94 %>
            <br />
            <asp:RadioButton runat="server" ID="rbSearchCriteriaAND" Checked="True" CssClass="voipSearchText voipSearchFont" GroupName="rbSearchCriteria" /> <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_95 %>
        </td>
    </tr>
    <tr>
        <td colspan="12">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="voipSearchTableLeftPadding">&nbsp;</td>
        <td style="vertical-align: top; width: 210px;">
            <table width="210px">
            <tr>
            <td class="voipSearchColumnHeadingRow voipSearchFont">
            <span><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_96 %></span>
            </td>
            </tr>
            <tr>
            <td style="padding-top: 5px; padding-left: 10px;">
            <asp:DropDownList ID="ddlCallOriginationType" runat="server" CssClass="voipSearchDropDownListOriginDestination voipSearchFont" Width="160">
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_97 %>" Value="-All-" Selected="True"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_98 %>" Value="PhoneNumber"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_99 %>" Value="IPAddress"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_100 %>" Value="Location"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_101 %>" Value="Gateway"></asp:ListItem>
            </asp:DropDownList>
            </td>
            </tr>
            <tr>
            <td style="height: 45px; padding-left: 10px;">
            <div id="divCallOriginationIPAddressorPhone" runat="server" style="display:none">
                <asp:TextBox ID="txtOriginIPAddressorPhone" runat="server" MaxLength="15" Width="126px"></asp:TextBox>
               <ajaxToolkit:TextBoxWatermarkExtender ID="txtOriginIPAddressorPhone_WatermarkExtender" runat="server"
                Enabled="True" TargetControlID="txtOriginIPAddressorPhone" WatermarkText="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_102 %>" WatermarkCssClass="voipSearchWaterMarkStyle voipSearchFont">
               </ajaxToolkit:TextBoxWatermarkExtender>
               </div>
                <asp:DropDownList runat="server" ID="ddlOriginRegion" style="display:none" CssClass="voipSearchDropDownListOriginDestination voipSearchFont"></asp:DropDownList>
                <asp:DropDownList runat="server" ID="ddlOriginGateway" style="display:none" CssClass="voipSearchDropDownListOriginDestination voipSearchFont"></asp:DropDownList>
            </td>
            </tr>
            <tr>
            <td class="voipSearchColumnHeadingRow voipSearchFont">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_103 %>
            </td>
            </tr>
            <tr>
            <td style="padding-top: 5px; padding-left: 10px;">
                <asp:DropDownList ID="ddlCallDestinationType" runat="server" CssClass="voipSearchDropDownListOriginDestination voipSearchFont" Width="160">
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_97 %>" Value="-All-" Selected="True"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_98 %>" Value="PhoneNumber"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_99 %>" Value="IPAddress"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_100 %>" Value="Location"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_101 %>" Value="Gateway"></asp:ListItem>
            </asp:DropDownList>
            </td>
            </tr>
            <tr>
            <td style="height: 40px; padding-left: 10px;">
            <div id="divCallDestinationIPAddressorPhone" runat="server" style="display:none">
                <asp:TextBox ID="txtDestinationIPAddressorPhone" runat="server" MaxLength="15" Width="126px"></asp:TextBox>
               <ajaxToolkit:TextBoxWatermarkExtender ID="txtDestinationIPAddressorPhone_WatermarkExtender" runat="server"
                Enabled="True" TargetControlID="txtDestinationIPAddressorPhone" WatermarkText="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_102 %>" WatermarkCssClass="voipSearchWaterMarkStyle voipSearchFont">
               </ajaxToolkit:TextBoxWatermarkExtender>
               </div>
                <asp:DropDownList runat="server" ID="ddlDestinationRegion" style="display:none" CssClass="voipSearchDropDownListOriginDestination voipSearchFont"></asp:DropDownList>
                <asp:DropDownList runat="server" ID="ddlDestinationGateway" style="display:none" CssClass="voipSearchDropDownListOriginDestination voipSearchFont"></asp:DropDownList>
            </td>
            </tr>
            </table>
        </td>
        <td class="voipSearchTableBorderRight"><br /></td>
        <td style="width: 9px;"><br/></td>
        <td style="vertical-align: top; width: 210px;">
            <table width="210px">
            <tr>
            <td class="voipSearchColumnHeadingRow voipSearchFont">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_104 %>
            </td>
            </tr>
            <tr>
            <td style="padding-top: 5px; padding-left: 10px;">
            <asp:DropDownList ID="ddlCallManagers" runat="server" CssClass="voipSearchDropDownListOriginDestination voipSearchFont" Width="160"></asp:DropDownList>
            </td>
            </tr>
            <tr>
                <td style="height:45px">&nbsp;</td>
            </tr>
            <tr>
            <td class="voipSearchColumnHeadingRow voipSearchFont">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_105 %>
            </td>
            </tr>
            <tr>
            <td style="padding-top: 5px; padding-left: 10px;">
            <asp:DropDownList runat="server" ID="ddlCallTime" CssClass="voipSearchDropDownListOriginDestination voipSearchFont" Width="160">
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_106 %>" Value="Last15Minutes"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_107 %>" Value="Last30Minutes"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_108 %>" Value="LastHour"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_109 %>" Value="Last2Hours"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_110 %>" Value="Last12Hours"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_111 %>" Value="LastDay"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_112 %>" Value="Last7Days"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_113 %>" Value="LastMonth"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_114 %>" Value="Between"></asp:ListItem>
            </asp:DropDownList>
            </td>
            </tr>
            <tr>
            <td style="height: 58px; padding-left: 10px;">
            <div id="divCallTime" runat="server" style="display:none;">
                <orion:DateTimePicker ID="dtStartDateTime" runat="server"/>
                <br/>
                <span class="voipSearchFont" style="font-style:italic;color:#333333;"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_122 %></span>
                <br />
                <orion:DateTimePicker ID="dtEndDateTime" runat="server"/>
            </div>
            </td>
            </tr>
            </table>
        </td>
        <td class="voipSearchTableBorderRight"><br /></td>
        <td style="width: 9px;"><br/></td>
        <td style="vertical-align: top; width: 350px">
            <table width="350px">
            <tr>
            <td class="voipSearchColumnHeadingRow voipSearchFont" colspan="2">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_115 %>
            </td>
            </tr>
            <tr style="height: 35px">
                <td style="width: auto; padding-left: 10px">
                    <asp:CheckBox runat="server" ID="cbFailed"/><span class="voipSearchText voipSearchFont voipSearchCBText"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_73 %></span>
                </td>
                <td>
                   <asp:CheckBox runat="server" ID="cbSuccessful"/><span class="voipSearchText voipSearchFont voipSearchCBText"><%= Resources.VNQMWebContent.VNQMWEBDATA_VM_3 %></span>
                </td>
            </td>
            <tr style="height: 35px">
                <td style="width: auto; padding-left: 10px">
                    <asp:CheckBox runat="server" ID="cbNoZeroLength"/><span class="voipSearchText voipSearchFont voipSearchCBText"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_116 %></span>
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="cbCallWithIssue"/><span class="voipSearchText voipSearchFont voipSearchCBText"><%= Resources.VNQMWebContent.VNQMWEBDATA_VM_6 %></span>
                </td>
            </tr>
            <tr style="height: 35px">
                <td colspan="2" style="width: auto; padding-left: 10px">
                    <asp:CheckBox runat="server" ID="cbConferenceCall"/><span class="voipSearchText voipSearchFont voipSearchCBText"><%= Resources.VNQMWebContent.VNQMWEBDATA_VM_4 %></span>
                </td>
            </tr>
            <tr style="height: 35px;" runat="server" id="phCiscoRedirectReason">
            <td style="width: auto; padding-left: 10px">
            <asp:CheckBox runat="server" ID="cbRedirectReason"/>
            <span class="voipSearchText voipSearchFont"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_117 %></span>
            </td>
            <td>
            <asp:DropDownList runat="server" ID="ddlRedirectReason" CssClass="voipSearchDropDownListCallStatus voipSearchFont">
            </asp:DropDownList>
            </td>
            </tr>
            <tr style="height: 35px;" runat="server" id="phCiscoCallTerminatationCause">
            <td style="width: auto; padding-left: 10px">
            <asp:CheckBox runat="server" ID="cbCallTerminationCause"/>
            <span class="voipSearchText voipSearchFont"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_118 %></span>
            </td>
            <td>
            <asp:DropDownList runat="server" ID="ddlCallTerminationCause" CssClass="voipSearchDropDownListCallStatus voipSearchFont">
            </asp:DropDownList>
            </td>
            </tr>
            <tr style="height: 35px;" runat="server" id="phAvayaCondCode">
                <td style="width: auto; padding-left: 10px">
                    <asp:CheckBox runat="server" ID="cbAvayaCondCode"/>
                    <span class="voipSearchText voipSearchFont"><%= Resources.VNQMWebContent.VNQMWEBDATA_VM_5 %></span>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlAvayaCondCode" CssClass="voipSearchDropDownListCallStatus voipSearchFont"/>
                </td>
            </tr>
</table>

    
        <td class="voipSearchTableBorderRight"><br /></td>
        <td style="width: 9px;"><br/></td>
        <td style="vertical-align: top; width: 555px">
            <table width="300px">
            <tr>
            <td class="voipSearchColumnHeadingRow voipSearchFont" colspan="3">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_119 %>
            </td>
            </tr>
            <tr style="height: 35px" id="phMos" runat="server">
            <td class="td-call-quality-protocol">
            <asp:CheckBox runat="server" ID="cbMOS"/>
            <span class="voipSearchText voipSearchFont"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_120 %></span>
            </td>
            <td>
            <asp:DropDownList runat="server" ID="ddlMOS" CssClass="voipSearchDropDownListCallQuality voipSearchFont" Width="110">
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_97 %>" Value="-All-"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_37 %>" Value="2"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_35 %>" Value="3"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_41 %>" Value="0"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_121 %>" Value="Between"></asp:ListItem>
            </asp:DropDownList>
            </td>
            <td style="width: 250px;">
            <div id="divMOS" runat="server" style="display:none;">
                    <asp:TextBox runat="server" ID="txtMOSLimit1" Width="30px"></asp:TextBox>
                    <span class="voipSearchText voipSearchFont" style="display: inline-block; width: 22px"></span><span class="voipSearchText voipSearchFont"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_122 %></span>
                    <asp:TextBox runat="server" ID="txtMOSLimit2" Width="30px"></asp:TextBox>
            </div>
            </td>
            </tr>
            <tr style="height: 35px" id="phLatency" runat="server">
            <td class="td-call-quality-protocol">
            <asp:CheckBox runat="server" ID="cbLatency"/>
            <span class="voipSearchText voipSearchFont"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_123 %></span>
            </td>
            <td>
            <asp:DropDownList runat="server" ID="ddlLatency" CssClass="voipSearchDropDownListCallQuality voipSearchFont" Width="110">
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_97 %>" Value="-All-"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_37 %>" Value="2"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_35 %>" Value="3"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_41 %>" Value="0"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_121 %>" Value="Between"></asp:ListItem>
            </asp:DropDownList>
            </td>
            <td style="width: 250px;">
            <div id="divLatency" runat="server" style="display:none;">
                <asp:TextBox runat="server" ID="txtLatencyLimit1" Width="30px"></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="ftbeLatency1" runat="server"
                    TargetControlID="txtLatencyLimit1"         
                    FilterType="Numbers" />
                <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_AK1_147, "<span class=\"voipSearchText voipSearchFont\" style=\"display: inline-block; width: 18px\">", "</span>", "<span class=\"voipSearchText voipSearchFont\">", "</span>" ) %>
                <asp:TextBox runat="server" ID="txtLatencyLimit2" Width="30px"></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="ftbeLatency2" runat="server"
                    TargetControlID="txtLatencyLimit2"         
                    FilterType="Numbers" />
                <span class="voipSearchText voipSearchFont"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_124 %></span>
            </div>
            </td>
            </tr>
            <tr style="height: 35px">
            <td class="td-call-quality-protocol">
            <asp:CheckBox runat="server" ID="cbJitter"/>
            <span class="voipSearchText voipSearchFont"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_125 %></span>
            </td>
            <td>
            <asp:DropDownList runat="server" ID="ddlJitter" CssClass="voipSearchDropDownListCallQuality voipSearchFont" Width="110">
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_97 %>" Value="-All-"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_37 %>" Value="2"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_35 %>" Value="3"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_41 %>" Value="0"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_121 %>" Value="Between"></asp:ListItem>
            </asp:DropDownList>
            </td>
            <td style="width: 250px;">
            <div id="divJitter" runat="server" style="display:none;">
            <asp:TextBox runat="server" ID="txtJitterLimit1" Width="30px"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="ftbeJitter1" runat="server"
                TargetControlID="txtJitterLimit1"         
                FilterType="Numbers" />
            <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_AK1_147, "<span class=\"voipSearchText voipSearchFont\" style=\"display: inline-block; width: 18px\">", "</span>", "<span class=\"voipSearchText voipSearchFont\">", "</span>" ) %>
            <asp:TextBox runat="server" ID="txtJitterLimit2" Width="30px"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="ftbeJitter2" runat="server"
                TargetControlID="txtJitterLimit2"         
                FilterType="Numbers" />
            <span class="voipSearchText voipSearchFont"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_124 %></span>
            </div>
            </td>
            </tr>
            <tr style="height: 35px">
            <td class="td-call-quality-protocol">
            <asp:CheckBox runat="server" ID="cbPacketLoss"/>
            <span class="voipSearchText voipSearchFont"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_126 %></span>
            </td>
            <td>
            <asp:DropDownList runat="server" ID="ddlPacketLoss" CssClass="voipSearchDropDownListCallQuality voipSearchFont" Width="110">
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_97 %>" Value="-All-"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_37 %>" Value="2"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_35 %>" Value="3"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_41 %>" Value="0"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_121 %>" Value="Between"></asp:ListItem>
            </asp:DropDownList>
            </td>
            <td style="width: 250px;">
            <div id="divPacketLoss" runat="server" style="display:none;">
            <asp:TextBox runat="server" ID="txtPacketLossLimit1" Width="30px"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="ftbePacketLoss1" runat="server"
                TargetControlID="txtPacketLossLimit1"         
                FilterType="Numbers" />
            <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_AK1_194, "<span class=\"voipSearchText voipSearchFont\" style=\"display: inline-block; width: 18px\">", "</span>", "<span class=\"voipSearchText voipSearchFont\">", "</span>") %>
            <asp:TextBox runat="server" ID="txtPacketLossLimit2" Width="30px"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="ftbePacketLoss2" runat="server"
                TargetControlID="txtPacketLossLimit2"
                FilterType="Numbers" />
            <span class="voipSearchText voipSearchFont"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_195 %></span>
            </div>
            </td>
            </tr>
            </table>
        </td>
        <td class="voipSearchTableBorderRight"><br /></td>
        <td style="width: 9px;"><br/></td>
        <td style="vertical-align: top; width: 555px">
            <table width="300px">
            <tr>
            <td class="voipSearchColumnHeadingRow voipSearchFont" colspan="3">
              <%= Resources.VNQMWebContent.VNQMWEBDATA_BO_CallProtocol %>
            </td>
            </tr>
            <tr style="height: 35px" id="Tr1" runat="server">
            <td class="td-call-quality-protocol">
                <input type="checkbox" id="cbIncomingCallProtocol"/>
                <span class="voipSearchText voipSearchFont"><%= Resources.VNQMWebContent.VNQMWEBDATA_BO_IncomingCallProtocol %></span>
            </td>
            <td>
            <asp:DropDownList runat="server" ID="ddlIncomingCallProtocol" CssClass="voipSearchDropDownListCallQuality voipSearchFont" Width="110" ClientIDMode="Static">
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_97 %>" Value="-All-"></asp:ListItem>
            </asp:DropDownList>
            </td>
            </tr>
            <tr style="height: 35px" id="Tr2" runat="server">
                <td class="td-call-quality-protocol">
                    <input type="checkbox" id="cbOutgoingCallProtocol"/>
                    <span class="voipSearchText voipSearchFont"><%=Resources.VNQMWebContent.VNQMWEBDATA_BO_OutgoingCallProtocol %></span>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlOutgoingCallProtocol" CssClass="voipSearchDropDownListCallQuality voipSearchFont" Width="110" ClientIDMode="Static">
                        <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_97 %>" Value="-All-"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 250px;">
                </td>
                </tr>
            </table>
        </td>
   
    <%--</tr>--%>
    <tr>
        <td class="voipSearchTableLeftPadding">&nbsp;</td>
        <td style="padding-left: 10px" colspan="11">
            <orion:LocalizableButton runat="server" ID="submitButton" LocalizedText="CustomText" DisplayType="Primary" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_127 %>" />
        </td>
    </tr>
    <tr>
        <td class="voipSearchTableLeftPadding">&nbsp;</td>
        <td style="width: 1px; border-bottom: 1px solid #E6E6E6" colspan="11"><br /></td>
    </tr>
    <tr>
    <td class="voipSearchTableLeftPadding">&nbsp;</td>
        <td colspan="11">
            <table>
                <tr>
                    <td style="width: 90px; vertical-align: top">
                        <span class="voipSearchFont"><b><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_128 %></b></span>
                    </td>
                    <td style="vertical-align: top; height: 25px">
                        <asp:Label runat="server" ID="lblSearchCriteria" CssClass="voipSearchFont">&nbsp;</asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</contenttemplate>
</asp:UpdatePanel>