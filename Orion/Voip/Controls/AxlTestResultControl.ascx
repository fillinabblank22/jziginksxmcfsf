<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AxlTestResultControl.ascx.cs"
    Inherits="Orion_Voip_Controls_AxlTestResultControl" %>
<orion:Include ID="Include2" runat="server" Module="Voip" File="admin.css" />
<span id="elSuccess" runat="server" style="vertical-align: middle;"><span class="validationTest success">
    <img src="/Orion/images/nodemgmt_art/icons/icon_OK.gif" alt="Success" />
    <span><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_72 %></span>
</span></span>
<span id="elFailed" runat="server" style="vertical-align: middle;"><span class="validationTest failure ipsla_AxlCredentialTestFailedBorder">
    <img src="/Orion/Voip/images/stop_16x16.gif" alt="Failed" />
    <span><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_74 %></span> <br/>
    <span class="ipsla_AxlCredentialTestFailed"><%= Resources.VNQMWebContent.VNQMWEBCODE_PG_15 %> 
    <a class="VNQM_HelpLink" href="<%= AxlCredentialsFailedHelpLink %>" target="_blank">&raquo; <%= Resources.VNQMWebContent.VNQMWEBCODE_PG_14 %></a></span>
</span>
</span>
