using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;

public partial class Orion_Voip_CallPath : OrionView, ICallPathProvider, INodeProvider
{
	public override string ViewType
	{
		get { return "Voip Call Path"; }
	}

	public CallPath CallPath
	{
		get { return (CallPath)this.NetObject; }
	}

	public Node Node
	{
		get { return CallPath.SourceSite.SimulationNode; }
	}

	public override string NetObjectIDForASP
	{
		get { return CallPath.SourceSite.SimulationNode.NetObjectID; }
	}

	public string ArrowStyle
	{
		get
		{
			if (CallPath.IsGood)
				return "ArrowNormal";
			else if (CallPath.IsWarning)
				return "ArrowWarning";
			else
				return "ArrowError";
		}
	}

	protected override void OnInit(EventArgs e)
	{
		this.resContainer.DataSource = this.ViewInfo;
		this.resContainer.DataBind();
		base.OnInit(e);
		// Call Paths normally have an arrow glyph in their name. This works fine in 
		// the page body, but the browser chrome has trouble with it. Make a 
		// friendlier page title out of ascii.
		this.Title = string.Format("{0}->{1} - {2}", CallPath.SourceSite.Name, CallPath.DestSite.Name, ViewInfo.ViewTitle);
		this.TimeWindow.ViewID = this.ViewInfo.ViewID;
	}
}
