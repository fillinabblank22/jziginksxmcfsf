<%@ Page Title="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_104 %>" Language="C#" MasterPageFile="~/Orion/Voip/VoipView.master" AutoEventWireup="true" CodeFile="OperationOverviewDetails.aspx.cs" Inherits="Orion_Voip_OperationOverviewDetails" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Data"%>
<%@ Register TagPrefix="voip" TagName="StatusIcon" Src="~/Orion/Voip/Controls/OperationStatusImage.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="VoipMainContentPlaceHolder" Runat="Server">
<style type="text/css">
        body {
            background: #FFFFFF;            
        }
		table{
		   border: 1px solid #ecedee;
		}
</style>
<orion:Include ID="Include1" File="js/AsyncView.js" runat="server"/>
<div style="padding-left: 10px;">
<% if (SourceNode != null) {%>
 <h4 style="margin-top: 5px;"> <%= String.Format(VNQMWebContent.VNQMWEBDATA_TM0_108, String.Format("<a href=\"{0}\" />{1}</a>", BaseResourceControl.GetViewLink(SourceNode), SourceNode.Name))%></h4>
<%} %>
 <h4 style="margin-top: 5px;"><asp:Label ID="MessageLabel" runat="server" /></h4>
 <asp:Repeater ID="Details" runat="server">
    <HeaderTemplate>
        <table cellspacing="0" cellpadding="2" border="0" style=" width: 800px;" class="NeedsZebraStripes">
        <thead>
        <td class="ReportHeader"><%= VNQMWebContent.VNQMWEBDATA_TM0_39 %></td>
        <td class="ReportHeader"><%= VNQMWebContent.VNQMWEBDATA_TM0_105 %></td>
        <td class="ReportHeader"><%= VNQMWebContent.VNQMWEBDATA_TM0_106 %></td>
        <td class="ReportHeader"><%= VNQMWebContent.VNQMWEBDATA_TM0_40 %></td>
        <td class="ReportHeader" style="text-align: right"><%= VNQMWebContent.VNQMWEBDATA_TM0_107 %></td>
        </thead>
    </HeaderTemplate>
    <ItemTemplate>
        <tr>
            <td class="Property" >
                <a href="IpSlaOperation.aspx?NetObject=ISOP:<%# DataBinder.Eval(Container.DataItem, "OperationInstanceID") %>">
                    <voip:StatusIcon ID="icon" OperationStatus='<%# this.GetOperationStatus(Container.DataItem) %>' StatusMessage='<%# DataBinder.Eval(Container.DataItem, "StatusMessage") %>' runat="server" />
                    <asp:Label ID="OperationStatusLabel" Text="<%# this.GetOperationStatusLabel(Container.DataItem) %>" runat="server" />
                </a>
            </td>
            <td class="Property" >
                <a href="IpSlaOperation.aspx?NetObject=ISOP:<%# DataBinder.Eval(Container.DataItem, "OperationInstanceID") %>">
                    <%# DataBinder.Eval(Container.DataItem, "OperationName")%>
                </a>
            </td>
            <td class="Property" >
                <a href="IpSlaOperation.aspx?NetObject=ISOP:<%# DataBinder.Eval(Container.DataItem, "OperationInstanceID") %>">
                    <asp:Label ID="PathLabel" Text="<%# this.GetOperationPath(Container.DataItem) %>" runat="server" />
                </a>
            </td>
            <td class="Property" >
                <a href="IpSlaOperation.aspx?NetObject=ISOP:<%# DataBinder.Eval(Container.DataItem, "OperationInstanceID") %>">
                    <img src="<%# GetOperationImage(Container.DataItem)%>" class="operImage" />
                    <asp:Label ID="OperationTypeLabel" Text="<%# this.GetOperationType(Container.DataItem) %>" runat="server" />
                </a>
            </td>
            <td class="Property" style="text-align: right" >
                <a href="IpSlaOperation.aspx?NetObject=ISOP:<%# DataBinder.Eval(Container.DataItem, "OperationInstanceID") %>">
                    <asp:Label ID="RttLabel" Text="<%# this.GetOperationRtt(Container.DataItem) %>" runat="server" />
                </a>
            </td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
    </asp:Repeater>
</div>
</asp:Content>

