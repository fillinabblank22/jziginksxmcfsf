using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_DetachResource : System.Web.UI.Page
{
    private NetObject _netObject;
    private ResourceInfo _resource;

    public NetObject NetObject
    {
        get { return _netObject; }
    }

    public ResourceInfo Resource
    {
        get { return _resource; }
    }

    protected override void OnPreInit(EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["NetObject"].Trim()))
        {
            try
            {
                _netObject = NetObjectFactory.Create(Request.QueryString["NetObject"].Trim());
            }
            catch (AccountLimitationException)
            {
                this.Page.Server.Transfer(
                    string.Format("/Orion/AccountLimitationError.aspx?NetObject={0}", Request.QueryString["NetObject"])
                    );
            }
        }

        if (string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
            throw new InvalidOperationException("ResourceID missing from Query String.");

        _resource = ResourceManager.GetResourceByID(Convert.ToInt32(Request.QueryString["ResourceID"]));
        this.Context.Items[typeof(ViewInfo).Name] = this.Resource.View;

        base.OnPreInit(e);
    }

    protected override void OnInit(EventArgs e)
    {
        if (null == this.NetObject)
        {
            this.Title = this.Resource.Title;
        }
        else
        {
            this.Title = string.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_10, this.NetObject.Name, this.Resource.Title);
        }

        this.divContainer.Style[HtmlTextWriterStyle.Width] = string.Format("{0}px", Resource.Width);
        this.divContainer.Style[HtmlTextWriterStyle.MarginLeft] = "12px";

        if (!Resource.IsAspNetControl)
        {
            ResourceTemplate template = ResourceTemplate.CreateLegacy(TranslateLegacyPath(Resource.File), new Type[0]);
            if (string.IsNullOrEmpty(template.ControlPath))
            {
                string myPath = Resource.GetAsyncResourceAspPath(null == this.NetObject ? string.Empty : this.NetObject.NetObjectID);
                string resourceContents = ClassicSiteProxy.FetchPage(Context, myPath);
                this.divContainer.Controls.Add(new LiteralControl(resourceContents));
            }
            else
            {
                BaseResourceControl ctrl = (BaseResourceControl)LoadControl(template.ControlPath);
                ctrl.Resource = this.Resource;
                ResourceHostControl host = ResourceHostManager.GetSupportingControl(ctrl);
                this.divContainer.Controls.Add(host);
                host.LoadFromRequest();
                host.Controls.Add(ctrl);
            }
        }
        else
        {
            BaseResourceControl ctrl = (BaseResourceControl)LoadControl(Resource.File);
            ctrl.Resource = Resource;
            ResourceHostControl host = ResourceHostManager.GetSupportingControl(ctrl);

            this.divContainer.Controls.Add(host);
            host.LoadFromRequest();

            host.Controls.Add(ctrl);
        }

        HtmlMeta metaTag = new HtmlMeta();
        metaTag.HttpEquiv = "REFRESH";
        metaTag.Content = string.Format("{0}; URL='{1}'", WebSettingsDAL.AutoRefreshSeconds, this.Request.Url.ToString());
        this.Page.Header.Controls.Add(metaTag);

        HtmlLink linkTag = new HtmlLink();
        linkTag.Href = string.Format("/WebEngine/Resources/{0}", WebSettingsDAL.StyleSheet);
        linkTag.Attributes["type"] = "text/css";
        linkTag.Attributes["rel"] = "stylesheet";
        this.Page.Header.Controls.Add(linkTag);
        
        base.OnInit(e);
    }

    private string TranslateLegacyPath(string path)
    {
        // Apparently classic NPM resources' file paths may be missing the prefix below;
        //  Thankfully, Server.MapPath is forgiving WRT backslashes & forward slashes
        if (!path.StartsWith("\\"))
        {
            return string.Format("\\NetPerfMon\\Resources\\{0}", path);
        }

        return path;
    }
}
