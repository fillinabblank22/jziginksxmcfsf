﻿using System;
using SolarWinds.Orion.IpSla.Web;


public partial class Orion_Voip_VoipGateway : VoipView, IVoipGatewayProvider 
{
    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();
        base.OnInit(e);
    }
    public override string ViewType
    {
        get { return "Voip VoIPGateway"; }
    }

    public override string HelpFragment
    {
        get { return "OrionVoIPMonitorPHViewGateway"; } 
    }

    public VoipGateway VoipGateway
    {
        get { return (VoipGateway)this.NetObject; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
       // TO DO
    }
}