using System;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web;
using System.Web.UI;
using System.Globalization;
using System.Web.UI.WebControls;

public partial class Orion_Voip_Summary : VoipView
{
    protected override void OnInit(EventArgs e)
	{
        this.resContainer.DataSource = this.ViewInfo;
		this.resContainer.DataBind();

        AlterTabControlWidth();

		base.OnInit(e);
	}

    void AlterTabControlWidth()
    {
        var ctrl = FindControlRecursive(this, "MainResourceContent") as Panel;
        if (ctrl == null)
            return;

        var viewInfo = this.ViewInfo;
        if (viewInfo == null)
            return;

        int width = 12, columns = viewInfo.ColumnCount;
        for (var i = 1; i <= columns; i++)
        {
            width += (23 + 11) + viewInfo.GetColumnWidth(i);
        }

        ctrl.Style.Add(HtmlTextWriterStyle.Width, width.ToString(CultureInfo.InvariantCulture) + "px");
    }

    private Control FindControlRecursive(Control root, string id)
    {
        if (root.ID == id)
        {
            return root;
        }

        foreach (Control c in root.Controls)
        {
            Control t = FindControlRecursive(c, id);
            if (t != null)
            {
                return t;
            }
        }

        return null;
    }

    public override string HelpFragment
    {
        get
        {
            var tabName = Master.SelectedTab;

            if ("IpSla".Equals(tabName, StringComparison.OrdinalIgnoreCase))
            {
                return "OrionIPSLAManagerPHViewIPSLASummary";
            }
            else if ("Top10".Equals(tabName, StringComparison.OrdinalIgnoreCase))
            {
                return "OrionIPSLAManagerPHViewIPSLATop10";
            }
            else if ("Web".Equals(tabName, StringComparison.OrdinalIgnoreCase))
            {
                return "OrionIPSLAManagerPHViewIPSLAWebSummary";
            }
            else if ("VoIP".Equals(tabName, StringComparison.OrdinalIgnoreCase))
            {
                return "OrionIPSLAManagerPHViewVoIPSummary";
            }
            else
            {
                return String.Empty;
            }
        }
    }

	public override string ViewType
	{
        get
        {
            var tabName = Master.SelectedTab;

            if ("Top10".Equals(tabName, StringComparison.OrdinalIgnoreCase))
            {
                return "IpSlaTop10";
            }
            else if ("Web".Equals(tabName, StringComparison.OrdinalIgnoreCase))
            {
                return "IpSlaWebSummary";
            }
            else if ("VoIP".Equals(tabName, StringComparison.OrdinalIgnoreCase))
            {
                return "Voip Summary";
            }
            else
            {
                return "IpSlaSummary";
            }
        }
	}

}