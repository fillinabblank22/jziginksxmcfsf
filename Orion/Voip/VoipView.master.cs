﻿using System;
using SolarWinds.Orion.IpSla.Web;

/// <summary>
/// Summary description for VoipView
/// </summary>
public partial class Orion_Voip_VoipView : System.Web.UI.MasterPage, IRefreshablePage, IHelpPageLink
{
    private bool disableRefresher;
    public string Breadcrumb {get; set; }
    protected string HelpFragment = string.Empty;

    protected void Page_PreRender(object sender, EventArgs e)
    {
        lnkHelp.HelpUrlFragment = (this.Page is VoipView) ? ((VoipView)this.Page).HelpFragment : HelpFragment;

        if (disableRefresher)
        {
            Refresher1.Visible = false;
        }

        if (String.IsNullOrEmpty(Breadcrumb) && breadcrumbsDiv != null)
            breadcrumbsDiv.Visible = false;
    }

    public void DisableRefresher()
    {
        disableRefresher = true;
    }

    public void HelpLink(string helpFragment)
    {
        HelpFragment = helpFragment;
    }
}