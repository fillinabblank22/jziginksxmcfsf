<%@ Page Async="true" Language="C#" AutoEventWireup="true" CodeFile="DetachResource.aspx.cs" Inherits="Orion_DetachResource" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title><%= Page.Title %></title>
    
    <link rel="stylesheet" type="text/css" href="/Orion/styles/OrionCSS.ashx?path=/Orion/styles/OrionMinReqs.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/OrionCSS.ashx?path=/Orion/styles/Resources.css" />
	
	<!-- Stylesheets left here to support legacy resources -->
	<link rel="stylesheet" type="text/css" href="/SolarWinds.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/Events.css" />
</head>
<body>
	<style type="text/css">
        body {
            background: #ecedee;            
        }
		.sw-app-region {
            padding-top: 0;
       }
	</style>
    <form id="form1" runat="server">
        <h1><%= Page.Title %></h1>
        <asp:ScriptManager runat="server" ID="scriptManager" />
        <div runat="server" id="divContainer">
        
        </div>
    </form>
</body>
</html>
