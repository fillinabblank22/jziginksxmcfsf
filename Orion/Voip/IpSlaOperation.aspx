﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/Voip/VoipView.master" AutoEventWireup="true" CodeFile="IpSlaOperation.aspx.cs" Inherits="Orion_Voip_IpSlaOperation" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="orion" TagName="TimeWindow" Src="~/Orion/Voip/Resources/TimeWindows.ascx" %>
<%@ Register TagPrefix="ipslam" TagName="OperationTitle" Src="~/Orion/Voip/Controls/OperationTitle.ascx" %>
<%@ MasterType VirtualPath="VoipView.master" %> 

<asp:Content ID="Content2" ContentPlaceHolderID="PageTitle" Runat="Server">
    <div class="titleTable">
        <h1><%= ViewName %> &ndash;
	            <% if (IsHop)
                { %>
                    <img alt="Hop" src="images/OperationTypeIcons/PathHop_16.gif" />&nbsp;<%= HopName %>
	                <% }
                else
                { %>
	                <ipslam:OperationTitle ID="OperationTitle" runat="Server" />
	                <% } %>
        </h1>
	</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="VoipMainContentPlaceHolder" Runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <script type="text/javascript" src="Resources/scripts/VoipMenus.js">
    </script>
    <script type="text/javascript" src="../js/ig_webGauge.js">
    </script>
    <script type="text/javascript" src="../js/ig_shared.js">
    </script>
    <script type="text/javascript" src="js/CollapsePanel.js">
    </script>
	<div style="padding: 0 0 10px 10px">
		<orion:TimeWindow runat="server" ID="TimeWindow" />
	</div>
	
	<orion:ResourceHostControl ID="ResourceHostControl" runat="server">
		<orion:ResourceContainer runat="server" ID="resContainer" />
	</orion:ResourceHostControl>

</asp:Content>

