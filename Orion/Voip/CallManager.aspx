<%@ Page Language="C#" MasterPageFile="VoipView.master" AutoEventWireup="true" CodeFile="CallManager.aspx.cs" Inherits="Orion_Voip_CallManager" %>
<%@ Register TagPrefix="voip" Assembly="SolarWinds.Orion.IpSla.Web" Namespace="SolarWinds.Orion.IpSla.Web.UI" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>

<asp:Content runat="server" ID="CallManagerContent" ContentPlaceHolderID="VoipMainContentPlaceHolder">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<script type="text/javascript" src="Resources/scripts/VoipMenus.js">
	</script>

	<voip:CallManagerResourceHost runat="Server" ID="resHost">
		<orion:ResourceContainer runat="server" ID="resContainer" />
	</voip:CallManagerResourceHost>
</asp:Content>
