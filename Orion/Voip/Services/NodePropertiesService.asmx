<%@ WebService Language="C#" Class="Orion.Voip.Services.NodePropertiesService" %>

using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Common;
using SolarWinds.Orion.IpSla.Contracts;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.Admin.Settings;
using SolarWinds.Orion.IpSla.Web.DAL;

namespace Orion.Voip.Services
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class NodePropertiesService : WebService
    {
        private static readonly Log log = new Log();

        [WebMethod]
        public object GetProperties(int siteId)
        {
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();
            try
            {
                var site = SitesDAL.Instance.GetSite(siteId);
                if (site == null)
                    return null;

                var node = NpmNodesDAL.GetById(site.NodeID);
                if (node == null)
                    return null;

                int ?credentialId, port, timeout;
                CliConnectionProtocol? protocol;
                using (var blProxy = BusinessLayer.GetBusinessLayer())
                {
                    if (!blProxy.GetCliConnectionInfo(node.ID, out credentialId, out protocol, out port, out timeout))
                        throw new ApplicationException("Error reading node properties.");
                }

                log.InfoFormat("IP SLA node properties retrieved. [SiteID={0}]", siteId);
                return new
                    {
                        node.EngineId,
                        NodeIpAddress = node.IpAddress.ToString(),
                        SiteName = site.Name,
                        SourceIpAddress = site.IPAddress,
                        CredentialId = credentialId,
                        Protocol = protocol.ToString(),
                        Port = port,
                        Timeout = timeout,
                    };
            }
            catch (FaultException<IpSlaFaultContract> ex)
            {
                log.Error(string.Format("Error while getting IP SLA node properties. [SiteID={0}]", siteId), ex);
                throw new ApplicationException(ex.Detail.Message);
            }
            catch (Exception ex)
            {
                log.Error(string.Format("Error while getting IP SLA node properties. [SiteID={0}]", siteId), ex);
                throw;
            }
        }

        [WebMethod]
        public void UpdateProperties(int siteId, string siteName, string sourceIpAddress, WebConnectionInfo connectionInfo)
        {
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();
            try
            {
                IPAddress srcIpAddr;
                if (!IPAddress.TryParse(sourceIpAddress, out srcIpAddr))
                    throw new LocalizedApplicationException(() => Resources.VNQMWebContent.VNQMWEBCODE_VB1_247);

                var site = SitesDAL.Instance.GetSite(siteId);
                if (site == null)
                    throw new LocalizedApplicationException(() => Resources.VNQMWebContent.VNQMWEBCODE_VB1_248);

                using (var blProxy = BusinessLayer.GetBusinessLayer())
                {
                    var errorMessage = blProxy.UpdateSite(siteId, siteName, srcIpAddr, null);
                    if (errorMessage != null)
                        throw new ApplicationException(errorMessage);

                    if (connectionInfo == null)
                    {
                        blProxy.DeleteCliConnectionInfo(site.NodeID);
                        return;
                    }

                    int? enableLevel;
                    string enablePassword;
                    NodePropertiesHelper.TransformEnableLevelValuesToServerSideRepresentation(
                        connectionInfo.EnableLevel,
                        connectionInfo.EnablePassword,
                        out enableLevel,
                        out enablePassword);

                    if (!connectionInfo.CredentialId.HasValue)
                        connectionInfo.CredentialId = blProxy.CreateCliCiscoCredentials(connectionInfo.CredentialName, connectionInfo.Username, connectionInfo.Password, enablePassword, enableLevel);

                    blProxy.UpdateCliConnectionInfo(site.NodeID, connectionInfo.CredentialId.Value, connectionInfo.Protocol, connectionInfo.Port, connectionInfo.Timeout);
                }
            }
            catch (FaultException<IpSlaFaultContract> ex)
            {
                log.Error(string.Format("Error while updating IP SLA node properties. [SiteID={0}]", siteId), ex);
                throw new ApplicationException(ex.Detail.Message);
            }
            catch (Exception ex)
            {
                log.Error(string.Format("Error while updating IP SLA node properties. [SiteID={0}]", siteId), ex);
                throw new Exception(SolarWinds.Internationalization.Exceptions.ExceptionHelper.GetLocalizedOrDefaultMessage(ex));
            }
        }

        [WebMethod]
        public object GetCliConnectionInformation(int nodeId)
        {
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();
            try
            {
                var node = NpmNodesDAL.GetById(nodeId);
                if (node == null)
                    return null;

                int? credentialId, port, timeout;
                CliConnectionProtocol? protocol;
                using (var blProxy = BusinessLayer.GetBusinessLayer())
                {
                    if (!blProxy.GetCliConnectionInfo(node.ID, out credentialId, out protocol, out port, out timeout))
                        throw new ApplicationException("Error reading CLI connection information.");
                }

                log.InfoFormat("CLI connection information retrieved. [NodeID={0}]", nodeId);
                return new
                {
                    node.EngineId,
                    NodeIpAddress = node.IpAddress.ToString(),
                    CredentialId = credentialId,
                    Protocol = protocol.ToString(),
                    Port = port,
                    Timeout = timeout,
                };
            }
            catch (FaultException<IpSlaFaultContract> ex)
            {
                log.Error(string.Format("Error while getting CLI connection information. [NodeID={0}]", nodeId), ex);
                throw new ApplicationException(ex.Detail.Message);
            }
            catch (Exception ex)
            {
                log.Error(string.Format("Error while getting CLI connection information. [NodeID={0}]", nodeId), ex);
                throw;
            }
        }

        [WebMethod]
        public void UpdateCliConnectionInformation(IEnumerable<int> nodeIds, WebConnectionInfo connectionInfo)
        {
            if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();

            int? enableLevel = null;
            string enablePassword = null;
            if (connectionInfo != null)
            {
                NodePropertiesHelper.TransformEnableLevelValuesToServerSideRepresentation(
                    connectionInfo.EnableLevel,
                    connectionInfo.EnablePassword,
                    out enableLevel,
                    out enablePassword);
            }

            foreach (var nodeId in nodeIds)
            {
                try
                {
                    using (var blProxy = BusinessLayer.GetBusinessLayer())
                    {
                        if (connectionInfo == null)
                        {
                            blProxy.DeleteCliConnectionInfo(nodeId);
                            continue;
                        }

                        if (!connectionInfo.CredentialId.HasValue)
                            connectionInfo.CredentialId = blProxy.CreateCliCiscoCredentials(connectionInfo.CredentialName, connectionInfo.Username, connectionInfo.Password, enablePassword, enableLevel);

                        blProxy.UpdateCliConnectionInfo(nodeId, connectionInfo.CredentialId.Value, connectionInfo.Protocol, connectionInfo.Port, connectionInfo.Timeout);
                    }
                }
                catch (FaultException<IpSlaFaultContract> ex)
                {
                    log.Error(string.Format("Error while updating CLI connection information. [NodeID={0}]", nodeId), ex);
                    throw new ApplicationException(ex.Detail.Message);
                }
                catch (Exception ex)
                {
                    log.Error(string.Format("Error while updating CLI connection information. [NodeID={0}]", nodeId), ex);
                    throw;
                }
            }
        }
    }
    
    public class WebConnectionInfo
    {
        public int? CredentialId { get; set; }
        public string CredentialName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string EnablePassword { get; set; }
        public int EnableLevel { get; set; }
        public CliConnectionProtocol Protocol { get; set; }
        public int Port { get; set; }
        public int Timeout { get; set; }
    }
}

