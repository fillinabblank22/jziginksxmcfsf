﻿<%@ WebHandler Language="C#" Class="AutoCompleteHandler" %>

using System;
using System.Web;
using System.Web.Services;
using System.Collections.Generic;

using SolarWinds.Orion.IpSla.Data;

using Node = SolarWinds.Orion.NPM.Web.Node;

[WebService(Namespace = "http://www.yoursite.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class AutoCompleteHandler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";

        string[] results = null;

        if (context.Request["dialogType"] != null)
        {
            string val = context.Request["dialogType"];
            string contextKey = context.Request["fieldName"];
            string prefixText = context.Request["q"];
            
            string limitCount = context.Request["limit"];
            int limit = 10;
            int.TryParse(limitCount,out limit);
            
            try
            {
                FilterType filterType = (FilterType)Enum.Parse(typeof(FilterType), val, true);
                if (filterType == FilterType.Node)
                {
                    results = GetExistingValuesForNode(prefixText, limit, contextKey);
                }
                else if (filterType == FilterType.CallPath)
                {
                    results = GetExistingValuesForCallPath(prefixText, limit, contextKey);
                }
            }
            catch
            {
                context.Response.End();
                return;
            }
        }

        if (results != null)
        {
            foreach (string str in results)
            {
                context.Response.Write(str + Environment.NewLine);
            }
        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

    public string[] GetExistingValuesForNode(string prefixText, int count, string contextKey)
    {
        if (String.IsNullOrEmpty(contextKey))
            return new string[] { };

        List<string> supportedColumns = new List<string>();
        supportedColumns.Add("Location");
        supportedColumns.Add("Vendor");

        IList<string> customProps = Node.GetCustomPropertyNames(true);
        if (customProps.Count != 0)
        {
            foreach (string property in customProps)
                supportedColumns.Add(property);
        }

        if (IsColumnSupported(supportedColumns, contextKey))
        {
            return LoadAndFilterData(prefixText, count, contextKey, "Nodes");
        }

        return new string[] { };
    }


    public string[] GetExistingValuesForCallPath(string prefixText, int count, string contextKey)
    {
        if (String.IsNullOrEmpty(contextKey))
            return new string[] { };

        List<string> supportedColumns = new List<string>();
        supportedColumns.Add("SrcName");
        supportedColumns.Add("DstName");

        if (IsColumnSupported(supportedColumns, contextKey))
        {
            return LoadAndFilterData(prefixText, count, "Name", "VoipSites");
        }

        return new string[] { };
    }

    static bool IsColumnSupported(List<string> supportedColumns, string columnName)
    {
        foreach (string col in supportedColumns)
        {
            if (String.Compare(col, columnName, true) == 0)
            {
                return true;
            }
        }

        return false;
    }


    string[] LoadAndFilterData(string prefixText, int count, string columnName, string tableName)
    {
        List<string> allValues = VoipFilter.GetFilterValues(columnName, tableName, prefixText, count);

        allValues.Sort();

        string[] result = new string[allValues.Count];
        allValues.CopyTo(result);
        return result;
    }
    
}