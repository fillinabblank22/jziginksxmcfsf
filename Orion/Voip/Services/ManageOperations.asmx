<%@ WebService Language="C#" Class="ManageOperations" %>

using System;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.MapEngine.Serialization;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.IpSla.Data;
using PageableDataTable = SolarWinds.Orion.IpSla.Web.PageableDataTable;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ManageOperations : WebService
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    [WebMethod]
    public DataTable GetValuesAndCountForProperty(string property)
    {
        if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();
        string icon;
        var propertyName = property;
        switch(property)
        {
            case "OperationStatus": icon = "max(oi.OperationStatusID)"; propertyName = "OperationStatuses." + property; break;
            case "OperationType": icon = "max(oi.OperationTypeID)"; break;
            case "DisplaySource": icon = "max(SourceNode.Status)"; break;
            case "DisplayTarget": icon = "max(TargetNode.Status)"; break;
            default: icon = "-1"; break;
        }

        DataTable table;

        try
        {
            table = DALHelper.ExecuteQuery("SELECT COUNT(oi.OperationInstanceID) as theCount FROM Orion.IpSla.Operations oi");
            var allCount = Convert.ToInt32(table.Rows[0][0]);

            var swqlQuery = string.Format(@"
SELECT {0} AS theValue, COUNT(oi.OperationInstanceID) AS theCount, {1} AS iconID
FROM Orion.IpSla.Operations oi
INNER JOIN Orion.IpSla.OperationStatuses OperationStatuses ON OperationStatuses.OperationStatusID = oi.OperationStatusID
INNER JOIN Orion.IpSla.Sites SourceSites ON SourceSites.NodeID = oi.SourceNodeID
INNER JOIN Orion.Nodes SourceNode ON SourceNode.NodeID = oi.SourceNodeID
LEFT JOIN Orion.Nodes TargetNode ON TargetNode.NodeID = oi.TargetNodeID
LEFT JOIN Orion.IpSla.Sites TargetSites ON TargetSites.NodeID = oi.TargetNodeID
WHERE oi.OperationStateID <> 6 AND oi.Deleted = 0
GROUP BY {0}", propertyName, icon);

            table = DALHelper.ExecuteQuery(swqlQuery);
            // add the first row with all items count 
            var row = table.NewRow();
            row["theValue"] = "All";
            row["theCount"] = allCount;
            row["iconID"] = -1;
            table.Rows.InsertAt(row, 0);
        }
        catch(Exception ex)
        {
            log.Error("Error while querying IP SLA operations", ex);
            throw;
        }

        icon = string.Empty;
        var iconIndex = table.Columns.Add("theIcon", typeof(string)).Ordinal;

        foreach (DataRow row in table.Rows)
        {
            //transform icon if possible
            if(row.IsNull("iconID")) continue;

            var iconID = Convert.ToInt32(row["iconID"]);
            if (iconID == -1) continue;

            switch (property)
            {
                case "OperationStatus": icon = IpSlaOperationHelper.GetStatusImageUrl((OperationStatus)iconID); break;
                case "OperationType": icon = IpSlaOperationHelper.GetTypeImageUrl((OperationType)iconID, false); break;
                case "DisplaySource":
                case "DisplayTarget": icon = string.Format("/Orion/images/StatusIcons/Small-{0}.gif", ((OBJECT_STATUS)iconID)); break;
                default: icon = string.Empty; break;
            }
            row[iconIndex] = icon;
        }
        table.Columns.Remove("iconID");

        return table;
    }

    [WebMethod]
    public PageableDataTable GetOperations(string property, string value)
    {
        if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();
        int pageSize;
        int startRowNumber;

        int.TryParse(this.Context.Request.QueryString["start"], out startRowNumber);
        int.TryParse(this.Context.Request.QueryString["limit"], out pageSize);

        var sortColumn = this.Context.Request.QueryString["sort"];
        var sortDirection = this.Context.Request.QueryString["dir"];

        //TODO: AdminTagValue on join does not work.
        if (sortColumn == "AdminTagValue")
        {
            sortColumn = "opTag.Value";
        }
        else if (sortColumn == "OwnerValue")
        {
            sortColumn = "opOwner.Value";
        }

        if (pageSize < 25) pageSize = 25;
        if (property == "OperationStatus")
            property = "OperationStatuses." + property;

        var condition = string.Empty;
        //TODO handle case where the value really equals 'All'
        if (!string.IsNullOrEmpty(property) && value != "All")
        {
            //TODO should be RTRIM(LTRIM(property)) once SWIS will support that 
            condition = string.IsNullOrEmpty(value) ?
                string.Format("AND ({0} IS NULL OR {0}='')", property) :
                string.Format("AND {0}='{1}'", property, value.Replace("'", "''"));
        }

        var swqlQuery = string.Format(@"
SELECT oi.OperationInstanceID AS ID, oi.OperationName AS OperationName, OperationStatuses.OperationStatus AS OperationStatus,
    oi.OperationType AS OperationType, oi.DisplaySource as SourceSiteName, oi.DisplayTarget as TargetSiteName,
    oi.Frequency as Frequency, oi.OperationStatusID AS OperationStatusID, oi.OperationTypeID AS OperationTypeID, oi.IsAutoConfigured as IsAutoConfigured, opTag.Value AS AdminTag, opOwner.Value AS Owner
    ,srcRegion.RegionName AS SrcRegion, destRegion.RegionName AS DestRegion
FROM Orion.IpSla.Operations oi
INNER JOIN Orion.IpSla.OperationStatuses OperationStatuses ON OperationStatuses.OperationStatusID = oi.OperationStatusID
LEFT JOIN Orion.IpSla.OperationParameters opTag ON opTag.OperationInstanceID = oi.OperationInstanceID AND opTag.OperationParameterTypeID = 31
LEFT JOIN Orion.IpSla.OperationParameters opOwner ON opOwner.OperationInstanceID = oi.OperationInstanceID AND opOwner.OperationParameterTypeID = 32
LEFT JOIN Orion.Ipsla.Sites srcSite ON srcSite.NodeID = oi.SourceNodeID
LEFT JOIN Orion.Ipsla.Sites destSite ON destSite.NodeID = oi.TargetNodeID
LEFT JOIN Orion.Ipsla.CCMRegions srcRegion ON srcSite.RegionID = srcRegion.RegionID
LEFT JOIN Orion.Ipsla.CCMRegions destRegion ON destSite.RegionID = destRegion.RegionID

WHERE oi.OperationStateID <> 6 AND oi.Deleted = 0 {0}
ORDER BY {1} {2} WITH ROWS {3} TO {4}", condition, sortColumn, sortDirection, startRowNumber + 1, startRowNumber + pageSize);

        var table = DALHelper.ExecuteQuery(swqlQuery);

        swqlQuery = string.Format(@"
SELECT COUNT(oi.OperationInstanceID) AS RowCount
FROM Orion.IpSla.Operations oi
INNER JOIN Orion.IpSla.OperationStatuses OperationStatuses ON OperationStatuses.OperationStatusID = oi.OperationStatusID
WHERE oi.OperationStateID <> 6 AND oi.Deleted = 0 {0}", condition);
        var count = (int)DALHelper.ExecuteQuery(swqlQuery).Rows[0][0];

        var statusIcon = table.Columns.Add("StatusIcon", typeof(string)).Ordinal;
        var typeIcon = table.Columns.Add("TypeIcon", typeof(string)).Ordinal;
        var statusId = table.Columns.IndexOf("OperationStatusID");
        var typeId = table.Columns.IndexOf("OperationTypeID");
        var name = table.Columns.IndexOf("OperationName");

        foreach (DataRow row in table.Rows)
        {
            row[statusIcon] = IpSlaOperationHelper.GetStatusImageUrl((OperationStatus)((short)row[statusId]));
            row[typeIcon] = IpSlaOperationHelper.GetTypeImageUrl((OperationType)((short)row[typeId]), false);
            row[name] = IpSlaOperationHelper.FixName(row[name].ToString());
        }
        return new PageableDataTable(table, count);
    }

    [WebMethod]
    public void DeleteOperations(int[] operationIDs)
    {
        if (!ProfileHelper.AllowAdmin)  throw new UnauthorizedAccessException();
        using (var blProxy = BusinessLayer.GetBusinessLayer())
        {
            blProxy.RemoveOperationInstances(operationIDs);
        }

    }

    private void CheckNodeManagement()
    {
        if (!((ProfileCommon)HttpContext.Current.Profile).AllowNodeManagement)
            throw new UnauthorizedAccessException("You must have the Allow Node Management right to perform this operation.");
    }

}


