using System;

using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;

public partial class Orion_Voip_Site : VoipView, ISiteProvider, INodeProvider
{
	protected override void OnInit(EventArgs e)
	{
		this.resContainer.DataSource = this.ViewInfo;
		this.resContainer.DataBind();
		base.OnInit(e);

        OrionInclude.CoreFile("/js/OrionCore.js");
	}

	public override string ViewType
	{
		get { return "Voip Site"; }
	}

	public override string NetObjectIDForASP
	{
		get { return Site.SimulationNode.NetObjectID; }
	}

    public override string HelpFragment
    {
        get { return "OrionVoIPMonitorPHViewSite"; }
    }

	#region ISiteProvider Members

	public new Site Site
	{
		get { return (Site)this.NetObject; }
	}

	#endregion
	
	#region INodeProvider Members
	public Node Node
	{
		get { return Site.SimulationNode; }
	}
	#endregion
}
