﻿using System;
using System.Text;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_Summary : System.Web.UI.MasterPage
{
    #region const
    /*
       Orion summary - Top 5 Sites by all Metrics
       Orion Top 10 - Top 5 Call paths by all metrics
       Orion Node Details - CallManger resource
     */

    #endregion

    public string SelectedTab
    {
        get { return Request["Tab"] ?? "IpSla"; }
    }

    protected override void OnPreRender(EventArgs e)
    {
        FixQueryString();

        base.OnPreRender(e);
    }

    private void FixQueryString()
    {
        //FB 1651 - Previewing a customized/copied VoIP Summary View presents the user with the incorrect view
        // Customization page redirects to this and appends "?viewid=xxx" regardless the URL already contains query string arguments.
        var query = Request.Url.Query;
        var questionMark = query.IndexOf('?');
        if (questionMark < 0)
            return;

        questionMark = query.IndexOf('?', questionMark + 1);
        if (questionMark < 0)
            return;

        // now we know that there is another question mark, let's replace all other question marks with '&'
        var sb = new StringBuilder(query);
        sb.Replace('?', '&', questionMark, sb.Length - questionMark);
        sb.Insert(0, Request.Path);
        Response.Redirect(sb.ToString());
    }

    protected override void OnInit(EventArgs e)
    {
        //Dispaly Sumary dialog
        if (!this.ScriptManager1.IsInAsyncPostBack)
        {
            HandleViewUpgrades();
        }

        base.OnInit(e);
    }

    private void HandleViewUpgrades()
    {
        var viewVersionIpSla = Config.ViewsVersionIpSla;
        var originalViewVersionIpSla = viewVersionIpSla;

        if (viewVersionIpSla < 1)
        {
            const string topOperationsResourcePath = @"/Orion/Voip/Resources/TopXX/TopXXIpSlaOperations.ascx";

            ViewResourceManager.UpdateResourceInView("IpSlaTop10", null,
                resourceInfo =>
                    {
                        if (resourceInfo.File.CompareTo(topOperationsResourcePath) != 0)
                            return false;

                        var selectedOperationTypes = resourceInfo.LoadSelectedValues<OperationType>(ResourcePropertiesHelper.OperationTypePrefix, false);
                        if (selectedOperationTypes == null)
                            return false;

                        if (selectedOperationTypes.Contains(OperationType.UdpJitter))
                        {
                            if (selectedOperationTypes.Contains(OperationType.IcmpPathJitter))
                                return false;
                            selectedOperationTypes.Add(OperationType.IcmpPathJitter);
                        }
                        else if (selectedOperationTypes.Contains(OperationType.UdpEcho))
                        {
                            if (selectedOperationTypes.Contains(OperationType.IcmpPathEcho))
                                return false;
                            selectedOperationTypes.Add(OperationType.IcmpPathEcho);
                        }
                        else
                        {
                            return false;
                        }

                        resourceInfo.SaveEnumSelection(selectedOperationTypes, ResourcePropertiesHelper.OperationTypePrefix);
                        return true;
                    }
                );

            viewVersionIpSla = 1;
        }

        if (viewVersionIpSla != originalViewVersionIpSla)
        {
            Config.ViewsVersionIpSla = viewVersionIpSla;
        }
    }
}
