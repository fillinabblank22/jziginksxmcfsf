<%@ Page Language="C#" MasterPageFile="VoipView.master" AutoEventWireup="true" CodeFile="CallPath.aspx.cs" Inherits="Orion_Voip_CallPath" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="orion" TagName="TimeWindow" Src="~/Orion/Voip/Resources/TimeWindows.ascx" %>

<asp:Content ID="SummaryTitle" ContentPlaceHolderID="PageTitle" runat="server">
	 <h1><%=ViewInfo.ViewTitle %></h1>
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td class="SiteName"><a href='<%=BaseResourceControl.GetViewLink(CallPath.SourceSite.NetObjectID)%>'><%=CallPath.SourceSite.Name%></a></td>
		<td class="<%=ArrowStyle%>"><div>&nbsp;</div></td>
		<td class="SiteName"><a href='<%=BaseResourceControl.GetViewLink(CallPath.DestSite.NetObjectID)%>'><%=CallPath.DestSite.Name%></a></td>
	</tr>
	</table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="VoipMainContentPlaceHolder" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <script type="text/javascript" src="Resources/scripts/VoipMenus.js">
    </script>
    <script type="text/javascript" src="js/ig_webGauge.js">
    </script>
    <script type="text/javascript" src="js/ig_shared.js">
    </script>
	<div style="padding: 0 0 10px 10px">
		<orion:TimeWindow runat="server" ID="TimeWindow" />
	</div>
	
	<orion:ResourceHostControl ID="ResourceHostControl1" runat="server">
		<orion:ResourceContainer runat="server" ID="resContainer" />
	</orion:ResourceHostControl>
</asp:Content>

