using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Orion_Voip_Resources_Summary_CustomPeriodAction : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string netObject = string.Empty;
	    string viewType = string.Empty;
	    string period = string.Empty;

		viewType = getViewFromRequest();

	    netObject = Page.Request.QueryString["NetObject"];
        
	    if (!string.IsNullOrEmpty(Request["quantity"]))
        {
		    period = string.Format("&Period=Last {0} {1}", Request["quantity"], Request["timeUnit"] );
        }else
        if (!string.IsNullOrEmpty(Request["startTime"]))
        {
		    period = string.Format("&Period={0}~{1}", Request["startTime"], Request["endTime"]);
	    }   
	    period = Server.UrlPathEncode(period);

        string URL = string.Format("/Orion/View.aspx?NetObject={0}{1}{2}", netObject, period, viewType);

	    Response.Redirect(URL);
    }

    private string getViewFromRequest()
    {
        string viewType = Request.Params["ViewID"];
        if (!string.IsNullOrEmpty(viewType))
            return string.Format("&ViewID={0}", viewType);

        viewType = Request.Params["ViewName"];
        if (!string.IsNullOrEmpty(viewType))
            return string.Format("&ViewName={0}", viewType);

        viewType = Request.Params["View"];
        if (!string.IsNullOrEmpty(viewType))
            return string.Format("&View={0}", viewType);
        return string.Empty;
    }
}
