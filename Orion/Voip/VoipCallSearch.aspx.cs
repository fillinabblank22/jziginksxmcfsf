﻿using System;
using System.Web.UI;
using Resources;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_VoipCallSearch : Page
{
    protected const string HelpFragment = "OrionIPSLAMonitorPHVoIPSearch";
    protected void Page_Load(object sender, EventArgs e)
    {
        ((IRefreshablePage)Master).DisableRefresher();
        ((IHelpPageLink)Master).HelpLink(HelpFragment);

        searchControl.ResultsControl = searchVoipCallsResults;
        // Submit button id is used for enabling/disabling from search controls
        searchVoipCallsResults.SubmitButtonId = searchControl.SubmitButtonId;
		
		  // SearchCriteria Label Id is used to get the searchcriteria value for including it in Excel file when search results are exported
        searchVoipCallsResults.SearchCriteriaLabelId = searchControl.SearchCriteriaLabelId;

        Master.Breadcrumb =
            $"<span id=\"breadcrumb\"> <a href=\"/Orion/Voip/Summary.aspx\">{VNQMWebContent.VNQMWEBDATA_AK1_93}</a> &gt; </span>";
    }}