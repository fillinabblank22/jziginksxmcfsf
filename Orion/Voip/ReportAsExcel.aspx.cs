﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security.AntiXss;
using System.Web.UI;
using Infragistics.Documents.Excel;
using Newtonsoft.Json;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Reporting.Models.Data;

public partial class Orion_Voip_ReportAsExcel : Page
{
    public const string QueryParamFileName = "filename";
    public const string QueryParamSearchCondition = "searchCondition";
    public const string SessionExportStorageKey = "SolarWinds.Temp.Json.Export.Array";
    public const string ExcelFileName = "OrionExport.xls";
    public const string ExcelContentType = "application/vnd.ms-excel";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.HttpMethod != "POST")
        {
            var str = Request.QueryString["download"];

            if (string.IsNullOrWhiteSpace(str))
                Response.Redirect("/Orion/Voip/VoipCallSearch.aspx");

            var blocks = Session[SessionExportStorageKey] as InterimExportFormat[];

            if (blocks == null) // handle this gracefully.
                Response.Redirect("/Orion/Voip/VoipCallSearch.aspx");
            else
                SendExcelFile(Context, blocks);

            return;
        }

        string nextUrl = StoreExportContentInSession(Context);
        SendNextUrlJson(nextUrl);
    }

    private void SendNextUrlJson(string url)
    {
        string ret = string.Format("{{ \"url\": \"{0}\" }}", HttpUtility.JavaScriptStringEncode(url));
        var bytes = Encoding.UTF8.GetBytes(ret);
        Response.ContentType = "application/json; charset=UTF-8";
        Response.OutputStream.Write(bytes, 0, bytes.Length);
        Response.End();
    }

    private static void SendExcelFile(HttpContext context, InterimExportFormat[] blocks)
    {
        bool success = false;

        try
        {
            Workbook wb = new Workbook(WorkbookFormat.Excel97To2003);

            foreach (var block in blocks)
            {
                AddToXls(block, wb);
            }

            string filename = ExportFileHelper.MakeValidFileName(
                AntiXssEncoder.HtmlEncode(context.Request.QueryString[QueryParamFileName], true) ?? string.Empty);

            if (string.IsNullOrWhiteSpace(filename))
                filename = ExcelFileName;

            if (!filename.EndsWith(".xls") && !filename.EndsWith(".xslx"))
                filename += ".xls";

            context.Response.ContentType = ExcelContentType;
            context.Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
            wb.Save(context.Response.OutputStream);
            success = true;
            context.Response.End();
        }
        catch (Exception)
        {
            if (!success)
                throw;
        }
        finally
        {
            if (success)
                context.Session[SessionExportStorageKey] = null;
        }
    }

    private static string StoreExportContentInSession(HttpContext context)
    {
        try
        {
            string searchCondition = context.Request.QueryString[QueryParamSearchCondition] ?? string.Empty;

            var items = Deserialize(searchCondition);

            context.Session[SessionExportStorageKey] = items;

            string filename = context.Request.QueryString[QueryParamFileName] ?? string.Empty;

            return "/Orion/Voip/ReportAsExcel.aspx?download=yes&filename=" + ExportFileHelper.MakeValidFileName(filename);
        }
        catch (Exception ex)
        {
            var ErrorDetails = (ErrorInspector.Inspect(context, ex, ErrorInspector.CoreUnhandledWebsiteInspector()) ??
                            ErrorInspector.Inspect(context, ex.InnerException, ErrorInspector.CoreUnhandledWebsiteInspector())) ??
                            new ErrorInspectorDetails { Error = ex };

            OrionErrorPageBase.CacheError(context, ErrorDetails);

            return ErrorDetails.ErrorPageResolved;
        }
    }

    private static InterimExportFormat[] Deserialize(string searchCondition)
    {
        DataTable searchResult = CallDetailsDAL.Instance.GetExportDataTable();//ExportDataTable;
        
        string v = ConvertDataTableToJson(searchResult,searchCondition);

        if (Regex.IsMatch(v, "^\\s*\\["))
        {
            var ret = JsonConvert.DeserializeObject<InterimExportFormat[]>(v,
                 new JsonSerializerSettings
                 {
                     DateTimeZoneHandling = DateTimeZoneHandling.Utc
                 });
            return ret;
        }
        else
        {
            var ret = JsonConvert.DeserializeObject<InterimExportFormat>(v,
                new JsonSerializerSettings
                {
                    DateTimeZoneHandling = DateTimeZoneHandling.Utc
                });
            return new[] { ret };
        }
    }
    private static string ConvertDataTableToJson(DataTable dt, string searchCondition)
    {
        ExportDataFormat formatData = new ExportDataFormat();

        formatData.Properties = new Dictionary<string, string>();
        formatData.Properties.Add(ExportDataFormat.PropertyNames.Title,"Voip Search Report");
        formatData.Properties.Add(ExportDataFormat.PropertyNames.Subtitle, searchCondition);

        formatData.Headers = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
        DataRow[] drs = dt.Rows.Cast<DataRow>().ToArray();
        formatData.Rows = drs.Select(x => x.ItemArray).ToList();

        string contentData = JsonConvert.SerializeObject(formatData);

        string finalContent = "[" + contentData + "]";
      
        return finalContent;
    }

    private static string GetProperty(InterimExportFormat fmt, string name)
    {
        if (fmt.Properties == null)
            return null;

        // [pc] yes, this is inefficient case insensitive string retrieval.
        // the dictionary is created without the benefit of knowing it should be case insensitive.

        return fmt.Properties
                        .Where(x => StringComparer.OrdinalIgnoreCase.Equals(x.Key, name))
                        .Select(x => x.Value)
                        .FirstOrDefault();
    }

    private static void AddToXls(InterimExportFormat fmt, Workbook wb)
    {
        string name = string.Format("Sheet{0}", wb.Worksheets.Count + 1);

        var sheet = wb.Worksheets.Add(name);

        sheet.DefaultColumnWidth = 6000;
        
        // add title, if found.

        int rowN = 0;

        string title = GetProperty(fmt, InterimExportFormat.PropertyNames.Title);
        string subTitle = GetProperty(fmt, InterimExportFormat.PropertyNames.Subtitle);

        if (string.IsNullOrWhiteSpace(title) && string.IsNullOrWhiteSpace(subTitle) == false)
        {
            title = subTitle;
            subTitle = null;
        }

        if (string.IsNullOrWhiteSpace(title) == false)
        {
            var rowOut = sheet.Rows[rowN++];
            var cell = rowOut.Cells[6];
            cell.Value = title;
            cell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True;
            cell.CellFormat.Font.Height = 300;
            
        }

        if (string.IsNullOrWhiteSpace(subTitle) == false)
        {
            rowN = rowN + 1;
            var rowOut = sheet.Rows[rowN];
            var cell = rowOut.Cells[0];
            cell.Value = "Search Criteria : " + subTitle;
            cell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True;
        }
       
        
        if (rowN != 0)
            rowN = rowN + 2;

        // add headers

        int cellN;

        if (fmt.Headers != null && fmt.Headers.Any())
        {
            cellN = 0;
            foreach (var header in fmt.Headers)
            {
                if (header != null)
                {
                    var cell = sheet.Rows[rowN].Cells[cellN];
                    cell.Value = header;
                    cell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True;
                    cell.CellFormat.WrapText = ExcelDefaultableBoolean.True;
                }
                cellN++;
            }

            rowN++;
        }

        // add rows

        foreach (var rowIn in fmt.Rows ?? Enumerable.Empty<Object[]>())
        {
            var rowOut = sheet.Rows[rowN++];

            cellN = 0;
            foreach (var val in rowIn)
            {
                if (val == null)
                {
                    cellN++;
                    continue;
                }

                // why handle arrays? custom SQL use may lead to serializing arrays.

                var a = val as IEnumerable<object>;

                if (a != null)
                {
                    StringBuilder sb = new StringBuilder();
                    bool first = true;

                    foreach (var v in a)
                    {
                        if (first) first = false;
                        else sb.Append(",");
                        sb.Append(v);
                    }

                    rowOut.Cells[cellN].Value = sb.ToString();
                   
                }
                else
                {
                    string cellContent = val.ToString();
                   
                    if (cellContent.Contains(";"))
                    {
                        string[] content = cellContent.Split(';');
                        rowOut.Cells[cellN].Value = content[0];

                        if (content[1] != "0")
                        {
                            rowOut.Cells[cellN].CellFormat.Font.ColorInfo = new WorkbookColorInfo(Color.Red);
                        }
                    }
                    else if (cellContent.Contains("Failed"))
                    {
                        rowOut.Cells[cellN].Value = val;
                        rowOut.Cells[cellN].CellFormat.Font.ColorInfo = new WorkbookColorInfo(Color.Red);
                    }
                    else
                    {
                        rowOut.Cells[cellN].Value = val;
                    }
                    
                }
                rowOut.Cells[cellN].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                cellN++;
            }
        }
    }
}
