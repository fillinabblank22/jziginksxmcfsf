﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("SolarWinds.Orion.IpSla.Web.AjaxTree");

SolarWinds.Orion.IpSla.Web.AjaxTree.AjaxTreeControl = function(element) {
    SolarWinds.Orion.IpSla.Web.AjaxTree.AjaxTreeControl.initializeBase(this, [element]);
    this.WebServiceProxy = null;
    this.nodes = [];
    this.rootTreeNodeId = "";
    this.dataProviderId = "";
    this.hiddenDisabled = null;
    this.rootParameters = [];
}

SolarWinds.Orion.IpSla.Web.AjaxTree.AjaxTreeControl.prototype = {
    get_WebServiceProxy: function() {
        return this.WebServiceProxy;
    },
    set_WebServiceProxy: function(value) {
        this.WebServiceProxy = value;
    },

    get_Disabled: function() {
        return (this.hiddenDisabled.value == "True");
    },
    set_Disabled: function(value) {
        this.hiddenDisabled.value = value ? "True" : "False";
        this.DisableInputElements(jQuery(this.get_element()));
    },

    get_hiddenDisabled: function() {
        return this.hiddenDisabled;
    },
    set_hiddenDisabled: function(value) {
        this.hiddenDisabled = value;
    },

    get_rootParameters: function() {
        return this.rootParameters;
    },
    set_rootParameters: function(value) {
        this.rootParameters = value;
    },

    GetTreeSectionSucceeded: function(result, treeNodeId) {
        var contentDiv = null;
        if (result.FirtsNodeIndex == 0) {
            contentDiv = jQuery('#' + treeNodeId + '-content');
            jQuery(contentDiv).get(0).innerHTML = result.RenderedHtml;
            contentDiv.slideDown('fast');
        }
        else {
            contentDiv = jQuery('#' + treeNodeId + '-nodeList');
            jQuery(contentDiv).get(0).innerHTML += result.RenderedHtml;
        }
        this.DisableInputElements(contentDiv);
        if (!result.IsLast) {
            var getTreeSectionSucceedDelegate = Function.createDelegate(this, this.GetTreeSectionSucceeded);
            var getTreeSectionFailedDelegate = Function.createDelegate(this, this.GetTreeSectionFailed);

            this.WebServiceProxy.GetTreeSection(this.get_id(), result.TreeNodeId, result.Parameters, result.TreeLevel, result.LastNodeIndex,
                getTreeSectionSucceedDelegate,
                getTreeSectionFailedDelegate,
                treeNodeId);
        }

    },

    GetTreeSectionFailed: function(error, treeNodeId) {
        var contentDiv = jQuery('#' + treeNodeId + '-content');
        contentDiv.innerHTML = String.format("@{R=VNQM.Strings;K=VNQMWEBJS_VB1_5;E=js}", error.get_message());
    },

    DescriptionSucceeded: function(result, treeNodeId) {
        this.UpdateNodeDescription(treeNodeId, result);
    },

    DescriptionFailed: function(error, treeNodeId) {
        this.UpdateNodeDescription(treeNodeId, error.get_message());
    },

    UpdateNodeDescription: function(treeNodeId, description) {
        var descriptionSpan = jQuery('#' + treeNodeId + '-description');
        jQuery(descriptionSpan).get(0).innerHTML = description;
    },

    Click: function(controlId, treeNodeId, parameters, treeLevel) {
        var getTreeSectionSucceedDelegate = Function.createDelegate(this, this.GetTreeSectionSucceeded);
        var getTreeSectionFailedDelegate = Function.createDelegate(this, this.GetTreeSectionFailed);
        var descriptionSucceededDelegate = Function.createDelegate(this, this.DescriptionSucceeded);
        var descriptionFailedDelegate = Function.createDelegate(this, this.DescriptionFailed);

        var getTreeSectionDelegate = Function.createDelegate(this, this.WebServiceProxy.GetTreeSection);
        var collapseDelegate = Function.createDelegate(this, this.WebServiceProxy.CollapseTreeSection);
        var updateDescriptionDelegate = Function.createDelegate(this, this.WebServiceProxy.UpdateTreeNodeDescription);

        var contentDiv = jQuery(treeNodeId);

        this.HandleClick(
            treeNodeId,
            function(treeNodeIdParam) {
                getTreeSectionDelegate(controlId, treeNodeId, parameters, treeLevel, 0,
                    getTreeSectionSucceedDelegate,
                    getTreeSectionFailedDelegate,
                    treeNodeIdParam);
            },
            function() {
                collapseDelegate(controlId, treeNodeId, parameters, treeLevel);
            },
            function(treeNodeIdParam, isExpanded) {
                updateDescriptionDelegate(controlId, treeNodeId, parameters, treeLevel, isExpanded,
                    descriptionSucceededDelegate,
                    descriptionFailedDelegate,
                    treeNodeIdParam);
            }
        );
        return false;
    },

    HandleClick: function(treeNodeId, getTreeSectionCallback, collapseCallback, updateDescriptionCallback) {
        var toggleImg = jQuery('#' + treeNodeId + '-toggle');
        var contentDiv = jQuery('#' + treeNodeId + '-content');

        if (!contentDiv.is(':hidden')) {
            contentDiv.slideUp('fast');
            toggleImg.attr("src", "/Orion/images/Button.Expand.gif");
            collapseCallback();
            updateDescriptionCallback(treeNodeId, false);
        } else {
            contentDiv.html("@{R=VNQM.Strings;K=VNQMWEBJS_VB1_6;E=js}");
            contentDiv.slideDown('fast');
            toggleImg.attr('src', "/Orion/images/Button.Collapse.gif");
            getTreeSectionCallback(treeNodeId);
            updateDescriptionCallback(treeNodeId, true);
        }
    },

    GetIsNodeVisible: function(treeNodeId) {
        var nodeDiv = jQuery('#' + treeNodeId + '-node');
        return (nodeDiv != null);
    },

    GetIsNodeExpanded: function(treeNodeId) {
        if (this.GetIsNodeExpanded(treeNodeId)) {
            return false;
        }
        var nodeContentDiv = jQuery('#' + treeNodeId + '-content');
        return (!contentDiv.is(':hidden'));
    },

    DisableInputElements: function(elem) {
        if (this.get_Disabled()) {
            elem.find('input[type!=hidden]').attr('disabled', true);
        }
        else {
            elem.find('input[type!=hidden]').removeAttr('disabled');
        }
    },

    initialize: function() {
        SolarWinds.Orion.IpSla.Web.AjaxTree.AjaxTreeControl.callBaseMethod(this, 'initialize');
        // Add custom initialization here
        this.set_Disabled(this.get_Disabled());
    },
    dispose: function() {
        //Add custom dispose actions here
        SolarWinds.Orion.IpSla.Web.AjaxTree.AjaxTreeControl.callBaseMethod(this, 'dispose');
    }
}
SolarWinds.Orion.IpSla.Web.AjaxTree.AjaxTreeControl.registerClass('SolarWinds.Orion.IpSla.Web.AjaxTree.AjaxTreeControl', Sys.UI.Control);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
