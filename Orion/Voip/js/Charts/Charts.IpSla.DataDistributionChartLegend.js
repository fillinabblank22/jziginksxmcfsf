﻿SW = SW || {};
SW.IPSLA = SW.IPSLA || {};
SW.IPSLA.Charts = SW.IPSLA.Charts || {};
SW.IPSLA.Charts.DataDistributionChartLegend = SW.IPSLA.Charts.DataDistributionChartLegend || {};
(function (legend) {
    legend.createStandardLegend = function (chart, legendContainerId) {
        var decDigits = 2;
        var table = document.getElementById(legendContainerId);
        
        while(table.hasChildNodes())
        {
           table.removeChild(table.firstChild);
        }
        
        var series = chart.series[0];
        var row;

        if (series.data.length == 1) {
            row = $('<tr />');
            $('<td class="NoBorder voipDataDistributionAlignCenter">' + series.data[0].name + '</td>').appendTo(row);
            row.appendTo(table);
        } else if (series.data.length == 3){
            row = $('<tr class="voipDataDistributionTableRow" />');

            $('<td class="voipDataDistributionTableHeaderAligned" >@{R=VNQM.Strings;K=VNQMWEBJS_PG_5;E=js}</td>' +
                '<td class="voipDataDistributionTableHeaderAligned voipDataDistributionAlign">@{R=VNQM.Strings;K=VNQMWEBJS_PG_6;E=js}</td>' +
                '<td class="voipDataDistributionTableHeaderAligned voipDataDistributionAlign">@{R=VNQM.Strings;K=VNQMWEBJS_PG_7;E=js}</td>' +
                '<td class="voipDataDistributionTableHeaderAligned voipDataDistributionAlign">@{R=VNQM.Strings;K=VNQMWEBJS_PG_8;E=js}</td>').appendTo(row);
            row.appendTo(table);
            
            row = $('<tr class="voipDataDistributionTableRow" />');
            $('<td><span class="voipDataDistributionLegendColorIcon" style="background-color: ' + series.data[0].color + '" /><span class="voipDataDistributionLabelPosition">' + series.data[0].name + '</span></td>' +
                '<td class="voipDataDistributionAlign">' + series.data[0].outgoing.toFixed(decDigits) + '%</td>' +
                '<td class="voipDataDistributionAlign">' + series.data[0].incoming.toFixed(decDigits) + '%</td>' +
                '<td class="voipDataDistributionAlign">' + series.data[0].count.toFixed(decDigits) + '%</td>').appendTo(row);
            row.appendTo(table);

            row = $('<tr class="voipDataDistributionTableRow" />');
            $('<td><span class="voipDataDistributionLegendColorIcon" style="background-color: ' + series.data[1].color + '" /><span class="voipDataDistributionLabelPosition">' + series.data[1].name + '</span></td>' +
                '<td class="voipDataDistributionAlign">' + series.data[1].outgoing.toFixed(decDigits) + '%</td>' +
                '<td class="voipDataDistributionAlign">' + series.data[1].incoming.toFixed(decDigits) + '%</td>' +
                '<td class="voipDataDistributionAlign">' + series.data[1].count.toFixed(decDigits) + '%</td>').appendTo(row);
            row.appendTo(table);
            
            row = $('<tr class="voipDataDistributionTableRow" />');
            $('<td colspan="3" class="NoBorder"><span class="voipDataDistributionLegendColorIcon" style="background-color: ' + series.data[2].color +'" /><span class="voipDataDistributionLabelPosition">' 
                    + series.data[2].name + '</span></td>' +
                '<td class="NoBorder voipDataDistributionAlign">' + series.data[2].count.toFixed(decDigits) + '%</td>').appendTo(row);
            row.appendTo(table);
        }
    };
} (SW.IPSLA.Charts.DataDistributionChartLegend));