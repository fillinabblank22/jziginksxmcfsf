﻿SW = SW || {};
SW.IPSLA = SW.IPSLA || {};
SW.IPSLA.Charts = SW.IPSLA.Charts || {};
SW.IPSLA.Charts.TrunkUtilizationChartLegend = SW.IPSLA.Charts.TrunkUtilizationChartLegend || {};
(function (legend) {
    legend.toggleSeries = function(cb, series) {
        cb.checked ? series.show() : series.hide();
    };

    legend.addCheckbox = function(series, container) {
        
        var check = $('<input type="checkbox" class="voipTrunkutilizationCheckboxPosition"/>')
                    .click(function() { legend.toggleSeries(this, series); });
        
        if (series.visible)
            check.attr("checked", 'checked');

        check.appendTo(container);
    };
    
    legend.createStandardLegend = function (chart, legendContainerId) {
        var decDigits = 2;
        var table = document.getElementById(legendContainerId);

        while (table.hasChildNodes()) {
            table.removeChild(table.firstChild);
        }

        var series = chart.series;
        var row;

        if (series.length >= 3) {
            row = $('<tr class="voipDataDistributionTableRow" />');
            $('<td class="voipTrunkUtilizationCheckboxHeader"></td>' +
                '<td class="voipTrunkUtilizationTableHeader">@{R=VNQM.Strings;K=VNQMWEBJS_PG_5;E=js}</td>' +
                '<td class="voipTrunkUtilizationTableHeader">@{R=VNQM.Strings;K=VNQMWEBJS_PG_6;E=js}</td>' +
                '<td class="voipTrunkUtilizationTableHeader">@{R=VNQM.Strings;K=VNQMWEBJS_PG_7;E=js}</td>' +
                '<td class="voipTrunkUtilizationTableHeader">@{R=VNQM.Strings;K=VNQMWEBJS_PG_8;E=js}</td>').appendTo(row);
            row.appendTo(table);

            for (i = 0; i < series.length; i++) {
                if (series[i].name == "@{R=VNQM.Strings;K=VNQMWEBJS_PG_2;E=js}") {//voip calls

                    row = $('<tr class="voipDataDistributionTableRow" />');
                    var firstColumn = $('<td><span class="voipTrunkUtilizationLegendColorIcon" style="background-color: ' +
                        series[i].color + '" /></td>').appendTo(row);
                    legend.addCheckbox(series[i], firstColumn);

                    var voice = parseFloat(chart.options.VOutgoing) + parseFloat(chart.options.VIncoming);


                    $('<td >' + series[i].name + '</td>' +
                        '<td>' + parseFloat(chart.options.VOutgoing).toFixed(decDigits) + '%</td>' +
                        '<td>' + parseFloat(chart.options.VIncoming).toFixed(decDigits) + '%</td>' +
                        '<td>' + parseFloat(voice).toFixed(decDigits) + '%</td>').appendTo(row);
                    row.appendTo(table);
                }
                if (series[i].name == "@{R=VNQM.Strings;K=VNQMWEBJS_PG_3;E=js}") {//data

                    row = $('<tr class="voipDataDistributionTableRow" />');
                    firstColumn = $('<td><span class="voipTrunkUtilizationLegendColorIcon" style="background-color: ' +
                         series[i].color + '" /></td>').appendTo(row);
                    legend.addCheckbox(series[i], firstColumn);

                    var data = parseFloat(chart.options.DOutgoing) + parseFloat(chart.options.DIncoming);

                    $('<td >' + series[i].name + '</td>' +
                        '<td>' + parseFloat(chart.options.DOutgoing).toFixed(decDigits) + '%</td>' +
                        '<td>' + parseFloat(chart.options.DIncoming).toFixed(decDigits) + '%</td>' +
                        '<td>' + parseFloat(data).toFixed(decDigits) + '%</td>').appendTo(row);
                    row.appendTo(table);
                }
            }
        }
    };
} (SW.IPSLA.Charts.TrunkUtilizationChartLegend));