﻿/// <reference name="MicrosoftAjax.js"/>
/// <reference path="~/Orion/js/jquery.js" />
/// <reference path="~/Orion/js/extjs/debug/Ext.js" />
/// <reference path="~/Orion/js/extjs/debug/jquery-bridge.js" />
/// <reference path="~/Orion/js/extjs/debug/ext-all-debug.js" />

Ext.menu.FormMenu = function(config) {
    Ext.menu.FormMenu.superclass.constructor.call(this, config);
    this.plain = true;
};

Ext.extend(Ext.menu.FormMenu, Ext.menu.Menu, {
    cls: 'x-ipsla-form-menu'});


Ext.menu.FormMenuItem = function(component, config) {

    Ext.menu.FormMenuItem.superclass.constructor.call(this, component, config);

    component.on("render", function(panel) {
        panel.getEl().swallowEvent("click");
        panel.container.addClass("x-ipsla-form-menu-item");
    });
};

Ext.extend(Ext.menu.FormMenuItem, Ext.menu.BaseItem, {
});
