﻿function CollapsePanel_Click(buttonID, panelID, hiddenID) {
    var imgTag;
    var collapseArea;
    imgTag = document.getElementById(buttonID);
    collapseArea = document.getElementById(panelID);
    var hidden = document.getElementById(hiddenID);
    if (collapseArea.style["display"] == "") {
        collapseArea.style["display"] = "none";
        // swap img src since we've collapsed
        imgTag.src = "/Orion/images/Button.Expand.gif";
        hidden.value = "1";
    }
    else {
        collapseArea.style["display"] = "";
        // Swap img src since we've expanded
        imgTag.src = "/Orion/images/Button.Collapse.gif";
        hidden.value = "0";
    }

    return false;
}
