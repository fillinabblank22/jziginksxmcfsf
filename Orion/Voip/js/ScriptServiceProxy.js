﻿Ext.data.ScriptServiceProxy = function(configuration) {
    Ext.data.ScriptServiceProxy.superclass.constructor.call(this);
    this.serviceInvoker = $create(Orion.Voip.js.ScriptServiceInvoker);
    this.configuration = configuration;
    this.result == null;
};

Ext.extend(Ext.data.ScriptServiceProxy, Ext.data.DataProxy, {

    load: function (params, reader, callback, scope, arg) {
        this.result = null;
        if (this.fireEvent("beforeload", this, params) !== false) {
            //call data, will be transferred to callInvoker because 
            //will be used later in callback (loadResponse) method of 'virtual' request
            var o = {
                params: params || {},
                request: {
                    callback: callback,
                    scope: scope,
                    arg: arg
                },
                reader: reader,
                callback: this.loadResponse,
                scope: this
            };

            //array from params
            var allParams = [];
            if (this.configuration.methodArgs) {
                for (var i in this.configuration.methodArgs) {
                    if (params[this.configuration.methodArgs[i]] != undefined) {
                        allParams.push(params[this.configuration.methodArgs[i]]);
                    }
                }
            }
            else {
                allParams = [params];
            }

            this.serviceInvoker.callService(this.configuration.scriptServiceMethod, allParams, o, 1000, this,
                Function.createDelegate(this, this.loadResponse),
                Function.createDelegate(this, this.loadResponse),
                Function.createDelegate(this, this.onError));
        } else {
            callback.call(scope, null, arg, false);
        }
    },


    loadResponse: function (resultContainer, o) {
        try {
            var responseData = resultContainer.result.ResponseData ? resultContainer.result.ResponseData : resultContainer.result;
            var partialResult = o.reader.readRecords(responseData);
            if (this.result == null) {
                this.result = partialResult;
            }
            else {
                //todo - shouldn't user provide method for concatenating of partial results?
                this.result.records = this.result.records.concat(partialResult.records);
                this.result.totalRecords = partialResult.totalRecords;
                this.result.success = partialResult.success;
            }
            if (!resultContainer.result.IsLastResult) {
                resultContainer.methodParams[0].recordsCount = this.result.records.length;

                if (partialResult.records.length == 0) {
                    //we don't have records, we will wait some time
                    resultContainer.interval = 1000;
                }
                else {
                    //we have records, server side is 'ready' for next request
                    resultContainer.interval = 0;
                }
            }

        } catch (e) {
            this.fireEvent("exception", this, 'response', 'read', o, resultContainer.response, e);
            this.fireEvent("loadexception", this, null, resultContainer.response, e);
            o.request.callback.call(o.request.scope, null, o.request.arg, false);
            return;
        }
        if (resultContainer.result.IsLastResult) {
            this.fireEvent("load", this, null, null);
            o.request.callback.call(o.request.scope, this.result, o.request.arg, true);
        }
    },

    onError: function (error, o) {
        this.fireEvent("exception", this, 'response', 'read', o, null, error);
        this.fireEvent("loadexception", this, null, null, error);
        o.request.callback.call(o.request.scope, null, o.request.arg, false);
    },

    update: function (dataSet) {

    },


    updateResponse: function (dataSet) {

    }
});