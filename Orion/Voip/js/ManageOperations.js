/// <reference path="~/Orion/js/jquery.js" />
/// <reference path="~/Orion/js/extjs/debug/Ext.js" />
/// <reference path="~/Orion/js/extjs/debug/jquery-bridge.js" />
/// <reference path="~/Orion/js/extjs/debug/ext-all-debug.js" />

Ext.namespace('SW');
Ext.namespace('SW.IPSLA');

SW.IPSLA.ManageOperations = function () {

    GetGroupByValues = function (groupBy, onSuccess) {
        ORION.callWebService("/Orion/Voip/Services/ManageOperations.asmx", "GetValuesAndCountForProperty", { property: groupBy }, onSuccess);
    };

    LoadGroupByValues = function () {
        var prop = $("#groupByProperty option:selected").val();
        ORION.Prefs.save('GroupBy', prop);
        if (prop) {
            var groupItems = $(".GroupItems").text("@{R=VNQM.Strings;K=VNQMWEBJS_AK1_36;E=js}");

            GetGroupByValues(prop, function (result) {
                result = ORION.objectFromDataTable(result);
                groupItems.empty();
                $(result.Rows).each(function () {
                    var value = this.theValue;
                    var icon = '';
                    if (this.theIcon)
                        icon = this.theIcon;
                    var disp;

                    if (value === null && prop == "DisplayTarget") {
                        disp = '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_2;E=js}';
                        value = '';
                    }
                    else {
                        disp = ($.trim(String(value)) || "@{R=VNQM.Strings;K=OperationStatus_unknown;E=js}");
                    }
                    if (disp == "All")
                        disp = "@{R=VNQM.Strings;K=VNQMWEBJS_TM0_34;E=js}";
                    disp = String.format("@{R=VNQM.Strings;K=VNQMWEBJS_TM0_35;E=js}", disp, this.theCount);
                    var displayValue = (disp.length > 23 ? disp.substr(0, 20) + '...' : disp);
                    $("<a href='#'></a>")
                        .attr('title', disp)
                        .attr('value', value)
                        .css('background-image', 'url(' + icon + ')')
                        .text(displayValue).click(SelectGroup)
                        .appendTo(".GroupItems")
                        .wrap("<li class='GroupItem'/>");
                });

                var toSelect = $(".GroupItem a[value='" + ORION.Prefs.load("GroupByValue") + "']");

                if (toSelect.length === 0) {
                    toSelect = $(".GroupItem a:first");
                }

                toSelect.click();
            });
        } else {
            $(".GroupItems").empty();
            RefreshObjects();
        }
    };

    SelectGroup = function () {
        $(this).parent().addClass("SelectedGroupItem").siblings().removeClass("SelectedGroupItem");
        grid.store.removeAll();
        RefreshObjects();
        return false;
    };

    RefreshObjects = function () {
        var prop = $("#groupByProperty option:selected").val();
        var value = $(".SelectedGroupItem a").attr('value');
        if (value) {
            ORION.Prefs.save('GroupByValue', value);
        }

        grid.store.proxy.conn.jsonData = { property: prop, value: value || "" };
        grid.store.load();
    };

    var editLocationDialog;
    var addCallManagerDialog;

    function EditLocation() {
        var id;
        grid.getSelectionModel().each(function (rec) {
            id = rec.data["ID"];
        });

        $('#' + SelectedOperationID).val(id);

        var title = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_TM0_1;E=js}', grid.getSelectionModel().getSelected().data['OperationName']);

        $('.x-window-header').html(title);

        var callManagerCount = $('#' + CallManagersCount).attr('value');

        if (callManagerCount > 0) {
            beforeEditLocationDialog = true;

            $('#' + EditorLocationLoadButtonID).click();

            if (!editLocationDialog) {
                editLocationDialog = new Ext.Window({
                    applyTo: 'editLocationDialog',
                    layout: 'fit',
                    width: 530,
                    height: 230,
                    closeAction: 'hide',
                    onEsc: CancelEditLocation,
                    plain: true,
                    resizable: false,
                    modal: true,
                    autoScroll: false,
                    buttons: [{
                        text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_41;E=js}',
                        handler: function () {
                            if (CheckDemoMode()) return;
                            waitBeforeSave = true;
                            isEditLocation = true;
                            setTimeout(function () { $('#' + EditorLocationSubmitButtonID).click(); }, 0);
                        }
                    },
                {
                    text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_26;E=js}',
                    handler: CancelEditLocation
                }]
                });
            }

            editLocationDialog.alignTo(document.body, "c-c");
            editLocationDialog.show();
        }
        else {
            if (!addCallManagerDialog) {
                addCallManagerDialog = new Ext.Window({
                    applyTo: 'addCallManagerDialog',
                    layout: 'fit',
                    width: 500,
                    height: 215,
                    closeAction: 'hide',
                    onEsc: CancelAddCallManager,
                    plain: true,
                    resizable: false,
                    modal: true,
                    autoScroll: true,
                    buttons: [{
                        text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_38;E=js}',
                        handler: CancelAddCallManager
                    }]
                });
            }

            addCallManagerDialog.alignTo(document.body, "c-c");
            addCallManagerDialog.show();
        }

        return false;
    }

    function CancelEditLocation() {
        setTimeout(function () { $('#' + EditorLocationCancelButtonID).click(); }, 0);
        editLocationDialog.hide();
    }

    function CancelAddCallManager() {
        addCallManagerDialog.hide();
    }

    var editDialog;

    function EditOperations() {
        var ids = [];
        grid.getSelectionModel().each(function (rec) {
            ids.push(rec.data["ID"]);
        });
        $('#' + SelectedOperationIDs).val(Sys.Serialization.JavaScriptSerializer.serialize(ids));

        var title;
        if (ids.length == 1)
            title = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_TM0_3;E=js}', grid.getSelectionModel().getSelected().data['OperationName']);
        else
            title = '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_4;E=js}';

        $('.x-window-header').html(title);

        beforeEditDialog = true;
        $('#' + EditorLoadButtonID).click();

        if (!editDialog) {
            editDialog = new Ext.Window({
                applyTo: 'editDialog',
                layout: 'fit',
                width: 765,
                height: 500,
                closeAction: 'hide',
                onEsc: CancelEdit,
                plain: true,
                resizable: false,
                modal: true,
                autoScroll: true,
                buttons: [{
                    text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_41;E=js}',
                    handler: function () {
                        if (CheckDemoMode()) return;
                        waitBeforeSave = true;
                        setTimeout(function () { editDialog.disable(); $('#' + EditorSubmitButtonID).click(); }, 0);
                    }
                },
                {
                    text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_26;E=js}',
                    handler: CancelEdit
                }]
            });
        }

        editDialog.alignTo(document.body, "c-c");
        editDialog.enable();
        editDialog.show();


        return false;
    }

    function CancelEdit() {
        setTimeout(function () { $('#' + EditorCancelButtonID).click(); }, 0);
        editDialog.hide();
    }

    var delDialog;
    var delGrid;
    var delStore;

    function DeleteOperations() {
        var names = [];
        var areThereManualOperations = false;
        grid.getSelectionModel().each(function (rec) {
            names.push([
            rec.data["ID"],
            rec.data["OperationName"],
            rec.data["IsAutoConfigured"],
            rec.data["StatusIcon"]]);
            if (!rec.data["IsAutoConfigured"]) {
                areThereManualOperations = true;
            }
        });

        var layout = $("#delDialogDescription");

        var operations = $("#operations");
        operations.empty();

        delStore = new Ext.data.Store({
            proxy: new Ext.data.MemoryProxy(names),
            reader: new Ext.data.ArrayReader({},
            [
                { name: 'ID' },
                { name: 'OperationName' },
                { name: 'IsAutoConfigured' },
                { name: 'StatusIcon' }
            ])
        });
        var additionalMessage = areThereManualOperations
            ? '<div class="ipsla_manualOperationNotice">@{R=VNQM.Strings;K=VNQMWEBJS_TM0_5;E=js}</div>'
            : '';

        //if (!delGrid) {
        delGrid = new Ext.grid.GridPanel({
            id: 'delGrid',
            title: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_6;E=js}' + additionalMessage,
            height: 300,
            autoWidth: true,
            cm: new Ext.grid.ColumnModel([{
                header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_7;E=js}',
                dataIndex: 'OperationName',
                renderer: renderName,
                width: 380 + (areThereManualOperations ? 0 : 150)
            }, {
                header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_8;E=js}',
                dataIndex: 'IsAutoConfigured',
                renderer: renderIsAutoConfigured,
                width: 175,
                hidden: !areThereManualOperations
            }]),
            store: delStore
        });


        delDialog = new Ext.Window({
            title: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_9;E=js}',
            layout: 'fit',
            width: 600,
            autoHeight: true,
            closeAction: 'hide',
            plain: true,
            resizable: false,
            modal: true,
            items: [
                delGrid
            ],
            buttons: [{
                text: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_24;E=js}',
                handler: function () {
                    if (CheckDemoMode()) return;

                    var ids = [];
                    grid.getSelectionModel().each(function (rec) {
                        ids.push(rec.data["ID"]);
                    });

                    ORION.callWebService("/Orion/Voip/Services/ManageOperations.asmx", "DeleteOperations", { operationIDs: ids }, function (result) { grid.store.load(); });
                    delDialog.hide();
                }
            }, {
                text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_26;E=js}',
                handler: function () { delDialog.hide(); }
            }]
        });
        delStore.load();

        delDialog.show();

        return false;
    };

    UpdateToolbarButtons = function () {
        var selItems = grid.getSelectionModel(), count = selItems.getCount(), map = grid.getTopToolbar().items.map;

        if (count == 1) {
            map.EditLocationButton.enable();
        }
        else {
            map.EditLocationButton.disable();
        }

        map.EditButton.setDisabled(count === 0);
        map.DeleteButton.setDisabled(count === 0);
    };

    // Column rendering functions
    function renderName(value, meta, record) {
        return String.format('<a href="/Orion/View.aspx?NetObject=ISOP:{2}"><img src="{0}" /> {1}</a>', record.data['StatusIcon'], value, record.data['ID']);
    }

    function renderType(value, meta, record) {
        return String.format('<img src="{0}" /> {1}', record.data['TypeIcon'], value);
    }

    function renderFrequency(value, meta, record) {
        return String.format('@{R=VNQM.Strings;K=VNQMWEBJS_TM0_11;E=js}', value);
    }

    function renderNullable(value, meta, record) {
        if (!value || value == "")
            return "<i>@{R=VNQM.Strings;K=VNQMWEBJS_TM0_12;E=js}</i>";
        else return value;
    }

    function renderIsAutoConfigured(value, meta, record) {
        return value ? '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_23;E=js}' : '<span class="ipsla_manualOperationColumn">@{R=VNQM.Strings;K=VNQMWEBJS_AK1_22;E=js}</span>';
    }

    ORION.prefix = "IPSLA_ManageOperations_";

    var selectorModel;
    var dataStore;
    var grid;
    var waitBeforeSave = false;
    var isEditLocation = false;

    return {
        init: function () {

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", UpdateToolbarButtons);

            dataStore = new ORION.WebServiceStore(
		        "/Orion/Voip/Services/ManageOperations.asmx/GetOperations",
		        [
			        { name: 'ID', mapping: 0 },
			        { name: 'OperationName', mapping: 1 },
			        { name: 'OperationStatus', mapping: 2 },
			        { name: 'OperationType', mapping: 3 },
			        { name: 'DisplaySource', mapping: 4 },
			        { name: 'DisplayTarget', mapping: 5 },
			        { name: 'Frequency', mapping: 6 },
			        { name: 'IsAutoConfigured', mapping: 9 },
			        { name: 'AdminTagValue', mapping: 10 },
			        { name: 'OwnerValue', mapping: 11 },
			        { name: 'SrcRegion', mapping: 12 },
			        { name: 'DestRegion', mapping: 13 },
			        { name: 'StatusIcon', mapping: 14 },
			        { name: 'TypeIcon', mapping: 15 },

		        ],
		        "OperationName");
            
            
            grid = new Ext.grid.GridPanel({
                store: dataStore,
                columns: [
                selectorModel,
                { header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_19;E=js}', width: 10, hidden: true, hideable: false, sortable: true, dataIndex: 'ID' },
                { header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_7;E=js}', width: 230, sortable: true, dataIndex: 'OperationName', renderer: renderName },
                { header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_10;E=js}', width: 120, sortable: true, dataIndex: 'OperationType', renderer: renderType },
                { header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_13;E=js}', width: 130, sortable: true, dataIndex: 'DisplayTarget' },
                { header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_14;E=js}', width: 120, sortable: true, dataIndex: 'Frequency', renderer: renderFrequency },
                { header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_15;E=js}', width: 100, sortable: true, dataIndex: 'AdminTagValue', renderer: renderNullable },
                { header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_16;E=js}', width: 140, sortable: true, dataIndex: 'SrcRegion', renderer: renderNullable },
                { header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_17;E=js}', width: 140, sortable: true, dataIndex: 'DestRegion', renderer: renderNullable },
                { header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_18;E=js}', width: 100, hidden: true, sortable: true, dataIndex: 'OwnerValue', renderer: renderNullable }
                ],
                sm: selectorModel,
                viewConfig: {
                    forceFit: false
                },
                width: 1030,
                height: 500,
                stripeRows: true,
                tbar: [{
                    id: 'AddButton',
                    text: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_20;E=js}',
                    tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_20;E=js}',
                    iconCls: 'add',
                    handler: function () {
                        window.location = 'OperationsWizard.aspx';
                    }
                },
                '-',
                {
                    id: 'EditLocationButton',
                    text: '  @{R=VNQM.Strings;K=VNQMWEBJS_TM0_21;E=js}',
                    tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_21;E=js}',
                    iconCls: 'editlocation',
                    handler: function () {
                        EditLocation();
                    }
                },
                '-',
                {
                    id: 'EditButton',
                    text: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_22;E=js}',
                    tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_22;E=js}',
                    iconCls: 'edit',
                    handler: function () {
                        EditOperations();
                    }
                },
                '-',
                {
                    id: 'DeleteButton',
                    text: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_24;E=js}',
                    tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_24;E=js}',
                    iconCls: 'del',
                    handler: function () {
                        if (CheckDemoMode()) return;
                        DeleteOperations();
                    }
                }],
                bbar: new Ext.PagingToolbar({
                    store: dataStore,
                    pageSize: 25,
                    displayInfo: true,
                    displayMsg: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_25;E=js}',
                    emptyMsg: "@{R=VNQM.Strings;K=VNQMWEBJS_TM0_26;E=js}",
                    beforePageText: "@{R=VNQM.Strings;K=VNQMWEBJS_TM0_27; E=js}",
                    afterPageText: "@{R=VNQM.Strings;K=VNQMWEBJS_TM0_28; E=js}",
                    firstText: "@{R=VNQM.Strings;K=VNQMWEBJS_TM0_29; E=js}",
                    prevText: "@{R=VNQM.Strings;K=VNQMWEBJS_TM0_30; E=js}",
                    nextText: "@{R=VNQM.Strings;K=VNQMWEBJS_TM0_31; E=js}",
                    lastText: "@{R=VNQM.Strings;K=VNQMWEBJS_TM0_32; E=js}",
                    refreshText: "@{R=VNQM.Strings;K=VNQMWEBJS_TM0_33; E=js}"
                })
            });

            grid.render('Grid');
            UpdateToolbarButtons();

            var fudgeFactor = 12;
            var groupItemsHeight = $("#Grid").height() - $(".GroupSection").height() - fudgeFactor;
            $(".GroupItems").height(groupItemsHeight);

            // Set the width of the grid
            grid.setWidth($('#gridCell').width());

            $("#groupByProperty").val(ORION.Prefs.load("GroupBy", "OperationType")).change(LoadGroupByValues);
            $("#groupByProperty").change();

            $("form").submit(function () { return false; });

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(
            function (sender, args) {
                if (waitBeforeSave) {
                    // waiting for edit to complete update - close edit dialog and refresh
                    waitBeforeSave = false;
                    var dataItems = args.get_dataItems()[EditOperationID];
                    var isValid = (!dataItems || dataItems['IsValid'] !== false);
                    if (isEditLocation) {
                        isEditLocation = false;
                        if (editLocationDialog) {
                            editLocationDialog.hide();
                        }
                        RefreshObjects();
                    } else if (Page_IsValid && isValid) {
                        if (editLocationDialog) {
                            editLocationDialog.hide();
                        }
                        if (editDialog) {
                            editDialog.hide();
                        }
                        RefreshObjects();
                    } else if (!isValid) {
                        if (editDialog) {
                            editDialog.enable();
                        }
                        RefreshObjects();
                    }
                }
            });
        }
    };
} ();

Ext.onReady(SW.IPSLA.ManageOperations.init, SW.IPSLA.ManageOperations);
