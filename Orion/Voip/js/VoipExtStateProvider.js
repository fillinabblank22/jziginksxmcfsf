﻿//Ext("Ext.state");
/**
* @class Ext.state.VoipExtStateProvider
* @extends Ext.state.Provider
*/
Ext.state.VoipExtStateProvider = function (configuration) {
    Ext.state.VoipExtStateProvider.superclass.constructor.call(this);
    this.configuration = configuration;
};


Ext.state.VoipExtStateProvider = Ext.extend(Ext.state.Provider, {

    constructor: function (config) {
        Ext.state.VoipExtStateProvider.superclass.constructor.call(this);
        Ext.apply(this, config);
    },

    // override
    get: function (name, defaultValue) {
        var value = SolarWinds.Orion.IpSla.Web.UI.WebUserSettingsManager.GetProvider().GetValue(name);
        
        return typeof value == "undefined" ? defaultValue : this.decodeValue(value);
    },

    // override
    set: function (name, value) {
        SolarWinds.Orion.IpSla.Web.UI.WebUserSettingsManager.GetProvider().SetValue(name, this.encodeValue(value));
    }
});
