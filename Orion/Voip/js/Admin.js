﻿function DemoModeMessage() {
    alert("@{R=VNQM.Strings;K=VNQMWEBJS_VB1_101;E=js}");
    return false;
}

function IsDemoMode() {
    return $("#isDemoMode").length > 0;
}

function CheckDemoMode() {
    if (IsDemoMode()) {
        DemoModeMessage();
        return true;
    }
    return false;
}
