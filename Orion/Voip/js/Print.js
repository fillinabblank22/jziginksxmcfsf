﻿function printArea(divId)
    {
        var html = '<HTML>\n<HEAD>\n';
        
        html += '<link rel="stylesheet" type="text/css" href="/SolarWinds.css" />';
        html += '<link rel="stylesheet" type="text/css" href="/Orion/Voip/Voip.css" />\n';
        html += '<link rel="stylesheet" type="text/css" href="/Orion/styles/Admin.css" />\n';
        html += '<link rel="stylesheet" type="text/css" href="/Orion/styles/OrionCSS.ashx?path=/Orion/styles/Admin.css" />\n';        
        
        html += '\n</HEAD>\n<BODY>\n';
        var printAreaElem = document.getElementById(divId);
        html += '<div id="content" style="font-size:10pt">\n';
        html += printAreaElem.innerHTML;
        html += '\n</div>';
        html += '\n<div id="footer"></div>';
        html += '\n</BODY>\n</HTML>';
        
        var printWin = window.open("","PrintPreviewWindow");
        printWin.document.open();
        printWin.document.write(html);
        printWin.document.close();
        printWin.print();
    }

