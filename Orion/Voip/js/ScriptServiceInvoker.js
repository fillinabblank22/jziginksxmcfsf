﻿/// <reference name="MicrosoftAjax.js"/>
Type.registerNamespace("Orion.Voip.js");

Orion.Voip.js.ScriptServiceInvoker = function() {
    Orion.Voip.js.ScriptServiceInvoker.initializeBase(this);

    //this.Interval = interval;
    //this._enabled = false;
    //this._timer = null;
    this.isCanceled = false;
};

Orion.Voip.js.ScriptServiceInvoker.prototype = {

    dispose: function() {
        Orion.Voip.js.ScriptServiceInvoker.callBaseMethod(this, 'dispose');
    },

    callService: function(proxyMethod, methodParams, callerData, interval, scope,
        notLastResponseCallback, lastResponseCallback, errorCallback) {
        var callContext = {};
        callContext.proxyMethod = proxyMethod;
        callContext.methodParams = methodParams;
        callContext.callerData = callerData;
        callContext.interval = interval;
        callContext.scope = scope;
        callContext.notLastResponseCallback = notLastResponseCallback;
        callContext.lastResponseCallback = lastResponseCallback;
        callContext.errorCallback = errorCallback;

        this.callServiceMethod(callContext);
    },

    callServiceMethod: function(callContext) {
        if (!this.isCanceled) {
            var invokeParams = [Function.createDelegate(this, this.onSuccess),
            Function.createDelegate(this, this.onError),
            callContext];

            var allParams = (callContext.methodParams || []).concat(invokeParams);
            callContext.proxyMethod.apply(this, allParams);
        }
    },

    cancel: function() {
        this.isCanceled = true;
    },

    onSuccess: function(result, context) {
        var resultContainer = {};
        if (typeof result.IsLastResult == 'undefined') {
            result.IsLastResult = true;
        }
        resultContainer.result = result;

        //let's allow consumer to modify methodParams and delay interval for next partial request
        resultContainer.methodParams = context.methodParams;
        resultContainer.interval = context.interval;

        if (!result.IsLastResult) {

            //call custom callback first
            resultContainer.callServiceAgain = true;

            if (context.notLastResponseCallback !== null) {
                context.notLastResponseCallback.call(context.scope, resultContainer, context.callerData);
            }

            //if user don't want to escape iteration
            if (resultContainer.callServiceAgain) {
                //change method params and interval (could be modified by consumer)
                context.methodParams = resultContainer.methodParams;
                context.interval = resultContainer.interval;

                var callServiceMethodDelegate = Function.createDelegate(this, this.callServiceMethod);
                setTimeout(function() { callServiceMethodDelegate(context); }, context.interval);
            }
        }
        else {
            //to satisfy an interface
            resultContainer.callServiceAgain = false;

            if (context.lastResponseCallback !== null) {
                context.lastResponseCallback.call(context.scope, resultContainer, context.callerData);
            }
        }
    },

    onError: function(error, context) {
        if (context.errorCallback !== null) {
            context.errorCallback.call(context.scope, error, context.callerData);
        }
    }
};

// JSON object that describes all properties, events, and methods of this component that should
// be addressable through the Sys.TypeDescriptor methods, and addressable via xml-script.
Orion.Voip.js.ScriptServiceInvoker.descriptor = {
    properties: [],
    events: []
};

Orion.Voip.js.ScriptServiceInvoker.registerClass('Orion.Voip.js.ScriptServiceInvoker', Sys.Component);

// Since this script is not loaded by System.Web.Handlers.ScriptResourceHandler
// invoke Sys.Application.notifyScriptLoaded to notify ScriptManager 
// that this is the end of the script.
if (typeof (Sys) !== 'undefined') { Sys.Application.notifyScriptLoaded(); }
