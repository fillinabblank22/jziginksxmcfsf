﻿using System;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_CCMPhone : VoipView, ICCMPhoneProvider
{

    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();
        base.OnInit(e);
    }
    public override string ViewType
    {
        get { return "Voip CCMPhone"; }
    }

    public override string HelpFragment
    {
        get { return "OrionIPSLAMonitorPHViewVoIPCCMPhone"; }
    }

    public CCMPhone CCMPhone
    {
        get { return (CCMPhone)this.NetObject; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = String.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_10, this.ViewInfo.ViewTitle, this.CCMPhone.VoipCCMPhone.Extension);
    }
}