﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VoipCallSearch.aspx.cs" Inherits="Orion_Voip_VoipCallSearch" MasterPageFile="~/Orion/Voip/VoipView.master" Title="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_92 %>" %>
<%@ Register TagPrefix="ipsla" TagName="SearchControl" Src="~/Orion/Voip/Controls/SearchVoipCalls.ascx" %>
<%@ Register TagPrefix="ipsla" TagName="SearchVoipCallsResults" Src="~/Orion/Voip/Controls/SearchVoipCallsResults.ascx" %>
<%@ Register TagPrefix="ipsla" Assembly="SolarWinds.Orion.IpSla.Web" Namespace="SolarWinds.Orion.IpSla.Web.UI" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagName="IncludeExtJs" TagPrefix="orion" %>
<%@ MasterType VirtualPath="~/Orion/Voip/VoipView.master"  %>

<asp:Content ID="contentHead" ContentPlaceHolderID="VoipHead" runat="server">
    <orion:IncludeExtJs ID="includeExtJs" runat="server" Version="3.4"/>
    <orion:Include ID="IncludeOrionCore" runat="server" File="OrionCore.js" />
	<orion:InlineCss runat="server"> body { background: transparent !important; } </orion:InlineCss>
</asp:Content>

<asp:Content ID="contentMain" ContentPlaceHolderID="VoipMainContentPlaceHolder" runat="server">
    <asp:ScriptManager ID="scriptManager" runat="server"></asp:ScriptManager>
    <ipsla:WebUserSettingsControl ID="webUserSettingsControl" SettingNamePrefix="VoipCallSearch#" runat="server" />
    <ipsla:SearchControl ID="searchControl" runat="server" />
    <ipsla:SearchVoipCallsResults ID="searchVoipCallsResults" runat="server" />
</asp:Content>