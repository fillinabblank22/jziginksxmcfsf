﻿using System;
using System.Collections.Generic;
using System.Text;
using Resources;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.IpSla.Common;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;

using System.Data;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web.DisplayTypes;
using SolarWinds.Orion.Web;

public partial class Orion_Voip_OperationOverviewDetails : System.Web.UI.Page
{
    private const string SourceNameColumn = "SrcName";
    private const string DestinationColumnName = "DstName";
    private const string RoundTripTimeColumn = "RoundTripTime";
    private const string OperationTypeIdColumn = "OperationTypeID";
    private const string OperationStatusIdColumn = "OperationStatusID";

    private const string NotSupportedNetObjectError = "The specified NetObject is not supported by this view.";

    private bool StatusIsDefined { get; set; }

    public OperationStatus SelectedStatus { get; set; }

    private bool OperationTypeIsDefined { get; set; }

    public List<int> SelectedOperationType { get; set; }

    public NetObject SourceNode { get ; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.IsPostBack)
            return;

        CheckForSourceID();

        CheckForOperationStatus();

        CheckForOperationType();

        LoadData();

        var ctrl = ControlHelper.FindControlRecursive(this.Page.Master, "TopRightPageLinks");
        if (ctrl != null)
            ctrl.Controls.Clear();
    }

    private void CheckForSourceID()
    {
        string id = this.Request["SourceID"];
        
        if(String.IsNullOrEmpty(id))
            return;

        int sourceID;
        if (int.TryParse(id, out sourceID))
        {
            SourceNode = NetObjectFactory.Create("N:" + sourceID);
        }
    }

    private void CheckForOperationStatus()
    {
        string status = this.Request["OperationStatus"];
        StatusIsDefined = (status != null) && Enum.IsDefined(typeof (OperationStatus), status);
        if (StatusIsDefined)
        {
            this.SelectedStatus = (OperationStatus)Enum.Parse(typeof(OperationStatus), status, true);
        }
    }

    private void CheckForOperationType()
    {
        string value = this.Request["OperationType"];

        List<int> types = new List<int>();
        if (value != null)
        {
            string[] values = value.Split(',');
            foreach (var type in values)
            {
                bool defined = (type != null) && Enum.IsDefined(typeof (OperationType), type);
                if (defined)
                {
                    OperationType o = (OperationType) Enum.Parse(typeof (OperationType), type, true);
                    types.Add((int) o);
                }
            }
        }

        OperationTypeIsDefined = types.Count > 0;
        this.SelectedOperationType = types;
    }

    private NetObjectHelper GetNetObjectConstraint()
    {
        string netObject = this.Request["NetObject"];
        if(String.IsNullOrEmpty(netObject))
            return null;

        NetObjectHelper helper;

        try
        {
            helper = new NetObjectHelper(netObject);
        }
        catch
        {
            throw new ArgumentException(NotSupportedNetObjectError);            
        }

        if (helper == null || !helper.IsNPMNode())
            throw new ArgumentException(NotSupportedNetObjectError);
    
        return helper;
    }

    void LoadData()
    {
        StringBuilder builder = new StringBuilder(@"
SELECT O.OperationInstanceID
    ,O.OperationName
    ,O.OperationStatusID
    ,O.StatusMessage
    ,O.OperationStateID
    ,O.OperationTypeID
    ,O.SourceNodeID
    ,O.TargetNodeID
    ,src.Name AS SrcName
    ,dst.Name AS DstName
    ,O.CurrentStats.RoundTripTime
FROM Orion.IpSla.Operations O
INNER JOIN Orion.IpSla.Sites src ON O.SourceNodeID = src.NodeID
LEFT JOIN Orion.IpSla.Sites dst ON O.TargetNodeID = dst.NodeID
WHERE O.Deleted = 0 AND O.OperationStateID <> 6 
");
        if (StatusIsDefined)
            builder.AppendFormat(" AND O.OperationStatusID = {0} ", (int)this.SelectedStatus);

        if (OperationTypeIsDefined)
            builder.AppendFormat(" AND O.OperationTypeID IN ({0}) ", this.SelectedOperationType.Concatenate(","));

        try
        {
            NetObjectHelper netObject = GetNetObjectConstraint();
            if (netObject != null)
                builder.AppendFormat(" AND O.SourceNodeID = {0} ", netObject.Id);
        }
        catch (ArgumentException ex)
        {
            MessageLabel.Text = ex.Message;
            return;
        }

        builder.Append(" ORDER BY O.OperationName ASC");

        var LimitIDs = Limitation.GetCurrentListOfLimitationIDs();
        if (LimitIDs.Count > 0)
        {
            foreach (var limitID in LimitIDs)
            {
                builder.Append($" {Environment.NewLine} WITH LIMITATION {limitID}");
            }
        }
		
        DataTable table = DALHelper.ExecuteQuery(builder.ToString());

        MessageLabel.Text = String.Format(VNQMWebContent.VNQMWEBCODE_TM0_8, table.Rows.Count);

        Details.DataSource = table;
        Details.DataBind();
    }

    protected OperationStatus GetOperationStatus(object item)
    {
        DataRowView row = item as DataRowView;
        if (row != null)
        {
            OperationStatus operationStatus = (OperationStatus)(short)row[OperationStatusIdColumn];
            return operationStatus;
        }
        return OperationStatus.Unknown;
    }

    protected string GetOperationStatusLabel(object item)
    {
        return GetLocalizedProperty("OperationStatus", GetOperationStatus(item).ToString());
    }

    protected string GetOperationImage(object item)
    {
        DataRowView row = item as DataRowView;
        if (row != null)
        {
            OperationType operationType = (OperationType) (short) row[OperationTypeIdColumn];
            return IpSlaOperationHelper.GetTypeImageUrl(operationType, false);
        }

        return String.Empty;
    }

    protected string GetOperationType(object item)
    {
        DataRowView row = item as DataRowView;
        if (row != null)
        {
            OperationType operationType = (OperationType)(short)row[OperationTypeIdColumn];
            return GetLocalizedProperty("OperationType", operationType.ToString());
        }
        return String.Empty;
    }

    protected string GetOperationPath(object item)
    {
        DataRowView row = item as DataRowView;
        if (row != null && row[SourceNameColumn] != DBNull.Value)
        {
            string sourceNodeName = (string)row[SourceNameColumn];
            if (row[DestinationColumnName] != DBNull.Value)
            {
                string destNodeName = (string)row[DestinationColumnName];
                return IpSlaOperationHelper.GetOperationName(sourceNodeName, destNodeName);
            }
            else
                return sourceNodeName;
        }
        return String.Empty;
    }

    protected string GetOperationRtt(object item)
    {
        DataRowView row = item as DataRowView;
        if (row != null && row[RoundTripTimeColumn] != DBNull.Value)
        {
            int rtt = (int)row[RoundTripTimeColumn];
            return new RoundTripTime(rtt).ToString();
        }
        return String.Empty;
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}
