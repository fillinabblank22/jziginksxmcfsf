using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Infragistics.UltraChart.Shared.Styles;
using Resources;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.Web;
using Periods = SolarWinds.Orion.IpSla.Web.Periods;

public partial class Orion_Voip_Resources_Graphs_PhoneStatusGraph : System.Web.UI.Page
{
	protected enum CustomFontSize
	{
        Small = 7,
		Medium = 10,
		Large = 12
	}

	protected void Chart_Init(object sender, EventArgs e)
	{
		GetDataFromRequest();
		GetDataByNodeIDAndMacAddress();
		InitDropDownLists();
		InitCustomFields();
	    AddTopRightLinks();
				
		Chart.Title = title;
		Chart.SubTitle1 = subtitle1;
		Chart.SubTitle2 = subtitle2;
		Chart.Width = width;
		Chart.SeriesLabelsOrientation = xAxisLabelsOrientation;
		Chart.FontSize = (int)fontSize;
		Chart.AspectRatio = (float)height /(float)width;
		Chart.YAxisLabel = Resources.VNQMWebContent.VNQMWEBCODE_AK1_8;
		Chart.BarColors = new Color[] { Color.Green, Color.Yellow, Color.Red, Color.AntiqueWhite };

		//XAxis labels should include dates only if difference between start time and end time is more than one day.
		Chart.XAxisFormatString = (endTime - startTime > TimeSpan.FromDays(1)) ? " <SERIES_LABEL:g>  " : " <SERIES_LABEL:t>  ";
		Chart.XAxisExtent = (endTime - startTime > TimeSpan.FromDays(1)) ? 100 : 50 ; 

		DataSet dataset =
			 CCMPhonesDAL.Instance.GetPhoneStatusGraphData(phoneID, startTime, endTime, intervalMinutes);
		Chart.DataBind(dataset);
		VoipCustomChartTable.DataSource = dataset;
		VoipCustomChartTable.DataBind();
		Chart.IsYAxisTickmarksSmart = GetMaxValueFromDataset(dataset) > 10; 
		CustomChartDiv.Visible = showTable;
	}

	protected void Refresh_Click(object sender, EventArgs e)
	{
        string url = "PhoneStatusGraph.aspx?phoneID={0}&startTime={1}&endTime={2}&period={3}&samplesize={4}&title={5}&subtitle1={6}&subtitle2={7}&width={8}&height={9}&fontsize={10}&showtable={11}&labelsOrientation={12}";
		url = string.Format(url, phoneID, BeginningTimeInput.Text, EndingTimeInput.Text,
		                    TimePeriodSelector.SelectedValue, SampleIntervalInput.Text,
		                    TitleInput.Text, SubTitle1Input.Text, SubTitle2Input.Text, WidthInput.Text,
		                    HeightInput.Text, FontSizeSelector.SelectedValue, DataTableExistanceSelector.SelectedValue, LabelOrientationSelector.SelectedValue);

		Response.Redirect(url);
	}

	private int GetMaxValueFromDataset(DataSet dataset)
	{
		DataTable table = dataset.Tables[0];
		int maxValue = 1;
		foreach (DataRow row in table.Rows)
		{
			if ((int)row["Unknown"]> maxValue)
				maxValue = (int) row["Unknown"];
			if ((int)row["Rejected"] > maxValue)
				maxValue = (int)row["Rejected"];
			if ((int)row["Registered"] > maxValue)
				maxValue = (int)row["Registered"];
			if ((int)row["Unregistered"] > maxValue)
				maxValue = (int)row["Unregistered"];
		}
		return maxValue;
	}

	private void InitCustomFields()
	{
		if (!string.IsNullOrEmpty(title))
			TitleInput.Text = title;
		else
			title = phoneName;

		if (!string.IsNullOrEmpty(subtitle1))
			SubTitle1Input.Text = subtitle1;
		else
			subtitle1 = string.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_9, PhoneStatus);

		if (!string.IsNullOrEmpty(subtitle2))
			SubTitle2Input.Text = subtitle2;
		else
		{
			bool isAbsoluteTime = !(string.IsNullOrEmpty(Request.Params["StartTime"]) || string.IsNullOrEmpty(Request.Params["EndTime"]));
			bool isCustomPeriod = !(string.IsNullOrEmpty(period));
			subtitle2 = string.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_10, isAbsoluteTime ?
						string.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_10,startTime,endTime) : isCustomPeriod ?
															period : Resources.VNQMWebContent.VNQMWEBCODE_AK1_11,
															Description);
		}

		if (null != startTime && !string.IsNullOrEmpty(Request.Params["startTime"]))
			BeginningTimeInput.Text = startTime.ToString();

		if (null != endTime && !string.IsNullOrEmpty(Request.Params["endTime"]))
			EndingTimeInput.Text = endTime.ToString();

		if (0 != width)
			WidthInput.Text = width.ToString();
		if (0 != height)
			HeightInput.Text = height.ToString();

		if (0 != intervalMinutes)
			SampleIntervalInput.Text = intervalMinutes.ToString();
	}

	private void InitDropDownLists()
	{
        TimePeriodSelector.DataSource = Periods.GetTimePeriodList();
        TimePeriodSelector.DataTextField = "DisplayName";
        TimePeriodSelector.DataValueField = "Name";
        TimePeriodSelector.DataBind();
        ListItem periodItem = TimePeriodSelector.Items.FindByValue(period);
        if (periodItem != null)
            TimePeriodSelector.SelectedValue = period;
        else
            TimePeriodSelector.SelectedValue = "TODAY";

		DataTableExistanceSelector.DataSource = new string[] {Resources.VNQMWebContent.VNQMWEBCODE_AK1_12, Resources.VNQMWebContent.VNQMWEBCODE_AK1_13};
		DataTableExistanceSelector.DataBind();
		DataTableExistanceSelector.SelectedValue = showTable ? Resources.VNQMWebContent.VNQMWEBCODE_AK1_12 : Resources.VNQMWebContent.VNQMWEBCODE_AK1_13;

        FontSizeSelector.DataSource = new List<KeyValuePair<string, string>> { new KeyValuePair<string, string>("Small", VNQMWebContent.VNQMWEBCODE_AK1_14), 
                                                                               new KeyValuePair<string, string>("Medium", VNQMWebContent.VNQMWEBCODE_AK1_15),
                                                                               new KeyValuePair<string, string>("Large", VNQMWebContent.VNQMWEBCODE_AK1_16)};
	    FontSizeSelector.DataTextField = "Value";
	    FontSizeSelector.DataValueField = "Key";
		FontSizeSelector.DataBind();
		string customFontSize = Enum.GetName(typeof(CustomFontSize),fontSize);
		ListItem fontItem = FontSizeSelector.Items.FindByValue(customFontSize);
		if (fontItem != null)
			FontSizeSelector.SelectedValue = customFontSize;

        LabelOrientationSelector.DataSource = new List<KeyValuePair<string, string>> { new KeyValuePair<string, string>("Horizontal", VNQMWebContent.VNQMWEBCODE_AK1_17),
                                                                                       new KeyValuePair<string, string>("VerticalLeftFacing", VNQMWebContent.VNQMWEBCODE_AK1_18),
                                                                                       new KeyValuePair<string, string>("VerticalRightFacing", VNQMWebContent.VNQMWEBCODE_AK1_19) };
	    LabelOrientationSelector.DataTextField = "Value";
	    LabelOrientationSelector.DataValueField = "Key";
		LabelOrientationSelector.DataBind();
        string customLabelOrientation = Enum.GetName(typeof(TextOrientation), xAxisLabelsOrientation);
		ListItem customOrientationItem = LabelOrientationSelector.Items.FindByValue(customLabelOrientation);
		if (customOrientationItem != null)
			LabelOrientationSelector.SelectedValue = customLabelOrientation;
	}

    private void AddTopRightLinks()
    {
        if (this.Page.Master == null) return;

        try
        {
            var ctrl = ControlHelper.FindControlRecursive(this.Page.Master, "TopRightPageLinks");
            var div= ControlHelper.FindControlRecursive(this.Page.Master, "TopRightPageLinksDiv");
            if (ctrl == null || div==null) return;
            div.Visible = false;

            ctrl.Controls.Clear();

            var printableLink = new HyperLink
                                    {
                                        ID = "Img2",
                                        CssClass = "printablePageLink",
                                        Text = VNQMWebContent.VNQMWEBCODE_TM0_2,
                                        EnableViewState = false,
                                        NavigateUrl = "#"
                                    };
            printableLink.Attributes["onclick"] = "javascript:printArea('Printable')";
            ctrl.Controls.Add(printableLink);

            ctrl.Controls.Add(new ASP.IconHelpButton
            {
                ID = "HelpButton",
                HelpUrlFragment = "OrionVoIPMonitorPHChartPhoneCustom",
                EnableViewState = false
            });
        }
        catch (System.Web.HttpException)
        {
            // -- shouldn't occur --
            // the page may be using this control and incompatible - having control blocks in a content section.
            // if so, this exception will occur. we'd rather default to not having the links show up than see an error page.
            // -- shouldn't occur --
        }
    }

	private void GetDataFromRequest()
	{
        if (!string.IsNullOrEmpty(Request.Params["phoneID"]))
            int.TryParse(Request.Params["phoneID"], out phoneID);

		if (!string.IsNullOrEmpty(Request.Params["startTime"]))
			DateTime.TryParse(Request.Params["startTime"],out startTime);
		if (!string.IsNullOrEmpty(Request.Params["endTime"]))
			DateTime.TryParse(Request.Params["endTime"],out endTime);

		if (string.IsNullOrEmpty(Request.Params["startTime"]) || string.IsNullOrEmpty(Request.Params["endTime"]))
		{
			if (!string.IsNullOrEmpty(Request.Params["period"]))
				period = Request.Params["period"];
			else
				period = "TODAY";
			Periods.Parse(ref period, ref startTime, ref endTime);
		}

		if (!string.IsNullOrEmpty(Request.Params["samplesize"]))
			if (!int.TryParse(Request.Params["samplesize"], out intervalMinutes))
				intervalMinutes = 30;

		if (!string.IsNullOrEmpty(Request.Params["title"]))
			title = Request.Params["title"];
		if (!string.IsNullOrEmpty(Request.Params["subtitle1"]))
			subtitle1 = Request.Params["subtitle1"];
		if (!string.IsNullOrEmpty(Request.Params["subtitle2"]))
			subtitle2 = Request.Params["subtitle2"];

		if (!string.IsNullOrEmpty(Request.Params["width"]))
			if (!int.TryParse(Request.Params["width"], out width))
				width = 800;

		if (!string.IsNullOrEmpty(Request.Params["height"]))
			if (!int.TryParse(Request.Params["height"], out height))
				height = 480;
		if (!string.IsNullOrEmpty(Request.Params["fontsize"]))
			fontSize = (CustomFontSize)Enum.Parse(typeof(CustomFontSize),Request.Params["fontsize"]);
		if (!string.IsNullOrEmpty(Request.Params["labelsOrientation"]))
			xAxisLabelsOrientation = (TextOrientation)Enum.Parse(typeof(TextOrientation), Request.Params["labelsOrientation"]);
		if (!string.IsNullOrEmpty(Request.Params["showtable"]))
			showTable = (Request.Params["showtable"] == "Yes");

	}

	private void GetDataByNodeIDAndMacAddress()
	{
	    CCMPhone phone = CCMPhonesDAL.Instance.GetCCMPhoneByID(PhoneID);
		phoneName = phone.PhoneName;
		if (string.IsNullOrEmpty(phoneStatus))
			phoneStatus = phone.PhoneStatus.ToString();
		if (string.IsNullOrEmpty(description))
			description = phone.Description;
	}

	#region Properties
	private int phoneID = 0;
	protected int PhoneID
	{
        get { return phoneID; }
        set { phoneID = value; }
	}

	private string phoneName;
	protected string PhoneName
	{
		get { return phoneName; }
		set { phoneName = value; }
	}

	private string phoneStatus;
	protected string PhoneStatus
	{
		get { return phoneStatus; }
		set { phoneStatus = value; }
	}

	private string description;
	protected string Description
	{
		get { return description; }
		set { description = value; }
	}

	private string title;
	protected string GraphTitle
	{
		get { return title; }
		set { title = value; }
	}

	private string subtitle1;
	protected string Subtitle1
	{
		get { return subtitle1; }
		set { subtitle1 = value; }
	}

	private string subtitle2;
	protected string Subtitle2
	{
		get { return subtitle2; }
		set { subtitle2 = value; }
	}

	private int width = 800;
	protected int Width
	{
		get { return width; }
		set { width = value; }
	}

	private int height = 480;
	protected int Height
	{
		get { return height; }
		set { height = value; }
	}

	private CustomFontSize fontSize = CustomFontSize.Small;
	protected CustomFontSize FontSize
	{
		get { return fontSize; }
		set { fontSize = value; }
	}

	private TextOrientation xAxisLabelsOrientation = TextOrientation.VerticalLeftFacing;
	protected TextOrientation TimeLabelsOrientation
	{
		get { return xAxisLabelsOrientation; }
		set { xAxisLabelsOrientation = value; }
	}

	private string period;
	protected string ChartPeriod
	{
		get { return period; }
		set { period = value; }
	}

	private DateTime startTime = DateTime.Today;
	protected DateTime StartTime
	{
		get { return startTime; }
		set { startTime = value; }
	}

	private DateTime endTime = DateTime.Now;
	protected DateTime EndTime
	{
		get { return endTime; }
		set { endTime = value; }
	}

	private int intervalMinutes = 30;
	protected int IntervalMinutes
	{
		get { return intervalMinutes; }
		set { intervalMinutes = value; }
	}

	private bool showTable = false;
	protected bool ShowTable
	{
		get { return showTable; }
		set { showTable = value; }
	}
	#endregion
}
