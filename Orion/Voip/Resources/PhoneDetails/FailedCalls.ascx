<%@ Control Language="C#" ClassName="FailedCallPhoneGraph" Inherits="SolarWinds.Orion.IpSla.Web.CCMPhoneGraphResource" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web" %>

<script runat="server">

    protected static readonly string searchPageLinkText = Resources.VNQMWebContent.VNQMWEBCODE_VB1_188;
    protected override void OnInit(EventArgs e)
    {
        OrionInclude.ModuleFile("Voip", "Voip.css");
        base.OnInit(e);
    }
    protected string ChartImageUrl
    {
        get
        {
            return GetChartImageUrl(CcmPhone, "VoipFailedCallPhone");
        }
    }

    protected string CustomUrl
    {
        get { return GenerateCustomChartLink(CcmPhone, "VoipFailedCallPhone"); }
    }
    
	protected override string DefaultTitle
	{
        get { return Resources.VNQMWebContent.VNQMWEBCODE_VB1p_189; }
	}

	public override string HelpLinkFragment
	{
        get { return "OrionIPSLAMonitorPHFailedCalls"; }
	}

       
</script>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <a href="<%= this.CustomUrl %>">
            <img src="<%= this.ChartImageUrl %>" />
       </a>
       <div class ="voipSearchPageHyperLink">
       <asp:HyperLink ID="HyperLink1" CssClass="sw-link" runat="server" NavigateUrl="/Orion/Voip/VoipCallSearch.aspx?CallTime=Last15Minutes&CallFailed=true"><%= searchPageLinkText%></asp:HyperLink>
       </div>
    </Content>
</orion:resourceWrapper>