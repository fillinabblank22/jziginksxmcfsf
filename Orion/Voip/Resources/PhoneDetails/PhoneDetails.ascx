﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PhoneDetails.ascx.cs"
    Inherits="Orion_Voip_Resources_CallManager_PhoneDetails" %>

    <style type="text/css">
        a.overrideHoverEffect:hover {
            color: black;
        }
    </style>
<orion:resourceWrapper runat="server" ID="Wrapper" ShowEditButton="False">
    <Content>
        <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%" runat="server" id="voipPhoneDetailsTable">
            <tr>
                <td class="voipCallDetailsHeaderText">
                    <%= PhoneStatus %>
                </td>
                <td class="voipCallDetailsRowText">
                    <img src="" alt="" id="imgPhoneStatus" runat="server" />
                    <asp:Label runat="server" ID="lblPhoneStatus"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="voipCallDetailsHeaderText">
                    <%= PhoneNumber %>
                </td>
                <td class="voipCallDetailsRowText">
                    <asp:Label runat="server" ID="lblPhoneNumber"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="voipCallDetailsHeaderText">
                    <%= IpAddress %>
                </td>
                <td class="voipCallDetailsRowText">
                    <asp:HyperLink runat="server" ID="hlIpAddress" Target="_blank"></asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td class="voipCallDetailsHeaderText">
                    <%= DeviceName %>
                </td>
                <td class="voipCallDetailsRowText">
                    <asp:Label runat="server" ID="lblDeviceName"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="voipCallDetailsHeaderText">
                    <%= Location %>
                </td>
                <td class="voipCallDetailsRowText">
                    <asp:HyperLink runat="server" ID="hlLocation"></asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td class="voipCallDetailsHeaderText VoipThickBottomBorder">
                    <%= CallManager %>
                </td>
                <td class="voipCallDetailsRowText VoipThickBottomBorder">
                    <img src="" alt="" id="imgCallManger" runat="server" />
                    <asp:HyperLink runat="server" ID="hlCallManager"></asp:HyperLink>
                </td>
            </tr>
        </table>
    </Content>
</orion:resourceWrapper>
