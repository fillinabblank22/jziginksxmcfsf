﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.IpSla.Common.Model;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using CCMPhone = SolarWinds.Orion.IpSla.Web.CCMPhone;

public partial class Orion_Voip_Resources_CallManager_PhoneDetails : VoipBaseResource
{
    protected static readonly string PhoneStatus = Resources.VNQMWebContent.VNQMWEBCODE_VB1_185;
    protected static readonly string PhoneNumber = Resources.VNQMWebContent.VNQMWEBCODE_VB1_143;
    protected static readonly string IpAddress = Resources.VNQMWebContent.VNQMWEBDATA_AK1_99;
    protected static readonly string DeviceName = Resources.VNQMWebContent.VNQMWEBCODE_VB1_186;
    protected static readonly string Location = Resources.VNQMWebContent.VNQMWEBCODE_VB1_144;
    protected static readonly string CallManager = Resources.VNQMWebContent.VNQMWEBCODE_VB1_157;
    protected static readonly string NA = Resources.VNQMWebContent.VNQMWEBCODE_VB1_134;

    private string callManagerPath = "/Orion/Voip/CallManager.aspx?NetObject=VCCM:";
    private string ccmRegionPath = "/Orion/Voip/Region.aspx?NetObject=VR:";

    protected void Page_Load(object sender, EventArgs e)
    {
        CCMPhone phone = GetInterfaceInstance<ICCMPhoneProvider>().CCMPhone.VoipCCMPhone;
        imgPhoneStatus.Src = GetPhoneStatusImageURL(phone.PhoneStatus);
        lblPhoneStatus.Text = GetLocalizedProperty("CCMDeviceStatus", phone.PhoneStatus.ToString());
        lblPhoneNumber.Text = phone.Extension;
        if (!string.IsNullOrEmpty(phone.PhoneIPAddress))
        {
            hlIpAddress.Text = string.Format("http://{0}", phone.PhoneIPAddress);
            hlIpAddress.NavigateUrl = string.Format("http://{0}", phone.PhoneIPAddress);
        }
        else
        {
            hlIpAddress.Text = NA;
            hlIpAddress.CssClass = "overrideHoverEffect";
        }
        lblDeviceName.Text = (!string.IsNullOrEmpty(phone.PhoneName)) ? phone.PhoneName : NA;
        if (!string.IsNullOrEmpty(phone.Location))
        {
            hlLocation.Text = phone.Location;
            hlLocation.NavigateUrl = this.Page.ResolveUrl(ccmRegionPath + phone.RegionId.ToString());
        }
        else
        {
            hlLocation.Text = NA;
            hlLocation.CssClass = "overrideHoverEffect";
        }
        imgCallManger.Src = "/Orion/Voip/images/CallIcons/cM_green_s.png";
        hlCallManager.Text = phone.CallManagerName;
        hlCallManager.NavigateUrl = this.Page.ResolveUrl(callManagerPath + phone.PhoneNodeID.ToString());
    }

    private string GetPhoneStatusImageURL(CCMDeviceStatus CCMDeviceStatus)
    {
        string imageUrl = string.Empty;
        switch (CCMDeviceStatus)
        {
            case CCMDeviceStatus.Registered:
                imageUrl = "/Orion/Voip/images/CallIcons/p_l_green.png";
                break;
            case CCMDeviceStatus.Unregistered:
                imageUrl = "/Orion/Voip/images/CallIcons/p_l_red.png";
                break;
            case CCMDeviceStatus.Rejected:
                imageUrl = "/Orion/Voip/images/CallIcons/p_l_yellow.png";
                break;
            case CCMDeviceStatus.Unknown:
                imageUrl = "/Orion/Voip/images/CallIcons/p_l_gray.png";
                break;
        }
        return imageUrl;
    }
    protected override string DefaultTitle
    {
        get { return Resources.VNQMWebContent.VNQMWEBCODE_VB1_187; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionIPSLAMonitorPHResourcesVoIPCCMPhone"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(ICCMPhoneProvider) }; }
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}
