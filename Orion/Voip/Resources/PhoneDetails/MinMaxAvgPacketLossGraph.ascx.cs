
using SolarWinds.Orion.IpSla.Data.Database;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web.UI;

public partial class CCMPhone_Graphs_MinMaxAvgPacketLoss : CCMPhoneGraphResource
{
    protected string ChartImageUrl
    {
        get
        {
            return GetChartImageUrl(CcmPhone, OperationResultsKey.PacketLoss.ToString());
        }
    }

    protected string CustomUrl
    {
        get { return GenerateCustomChartLink(CcmPhone, OperationResultsKey.PacketLoss.ToString()); }
    }

	protected override string DefaultTitle
	{
        get { return Resources.VNQMWebContent.VNQMWEBCODE_VB1_193; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionVoIPMonitorPHChartMMAPacketLoss"; }
	}
}
