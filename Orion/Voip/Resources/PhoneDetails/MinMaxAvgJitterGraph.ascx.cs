
using SolarWinds.Orion.IpSla.Data.Database;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web.UI;

public partial class CCMPhone_Graphs_MinMaxAvgJitter : CCMPhoneGraphResource
{
    protected string ChartImageUrl
    {
        get
        {
            return GetChartImageUrl(CcmPhone, OperationResultsKey.Jitter.ToString());
        }
    }

    protected string CustomUrl
    {
        get { return GenerateCustomChartLink(CcmPhone, OperationResultsKey.Jitter.ToString()); }
    }

	protected override string DefaultTitle
	{
        get { return Resources.VNQMWebContent.VNQMWEBCODE_VB1_190; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionVoIPMonitorPHChartMMAJitter"; }
	}

}
