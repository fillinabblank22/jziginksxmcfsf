using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Data.Database;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;
using System;

public partial class CCMPhone_Graphs_MinMaxAvgMOSGraph : CCMPhoneGraphResource
{
    protected string ChartImageUrl
    {
        get
        {
            return GetChartImageUrl(CcmPhone, OperationResultsKey.MOS.ToString());
        }
    }

    protected string CustomUrl
    {
        get { return GenerateCustomChartLink(CcmPhone, OperationResultsKey.MOS.ToString()); }
    }

	protected override string DefaultTitle
	{
        get { return Resources.VNQMWebContent.VNQMWEBCODE_VB1_192; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionVoIPMonitorPHChartMMAMOS"; }
	}
}
