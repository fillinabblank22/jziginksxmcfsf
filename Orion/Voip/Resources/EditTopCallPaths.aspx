<%@ Page Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true" CodeFile="EditTopCallPaths.aspx.cs" Inherits="Orion_Voip_Resources_EditTopCallPaths" Title="Untitled Page" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1 style="padding-left: 0px"><%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_AK1_167, Resource.Title) %></h1>
    
	<asp:ValidationSummary ID="ValidationSummary1" runat="server" />
    
   <b><orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="false" /></b>
   
   <b><asp:Label runat="server" ID="ObjectType" /></b>
   <br /> 
   <asp:TextBox runat="server" ID="HowMany" />
   <asp:CompareValidator ID="CompareValidator3" runat="server" ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_213 %>" Operator="GreaterThanEqual" Type="Integer" ValueToCompare="1" ControlToValidate="HowMany">*</asp:CompareValidator>
   <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="HowMany" ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_214 %>">*</asp:RequiredFieldValidator>   
   <br />
   <br />
   <div class="sw-btn-bar">
       <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" ToolTip="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_215 %>" DisplayType="Primary" OnClick="SubmitClick" />
   </div>

</asp:Content>

