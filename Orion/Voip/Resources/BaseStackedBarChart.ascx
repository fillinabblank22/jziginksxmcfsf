<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BaseStackedBarChart.ascx.cs" Inherits="Orion_Voip_Resources_BaseStackedBarChart" %>

<div id="ChartContent" runat="server">
    <table>
        <tr>
            <td><center><h1><asp:Literal ID="ChartTitle" runat="server"></asp:Literal></h1></center></td>
        </tr>
        <tr>
            <td><center><asp:Literal ID="ChartSubTitle1" runat="server"></asp:Literal></center></td>
        </tr>
        <tr>
            <td><center><asp:Literal ID="ChartSubTitle2" runat="server"></asp:Literal></center></td>
        </tr>
        <tr>
            <td><voip.igchart:UltraChart id="chart" runat="server">
            </voip.igchart:UltraChart></td>
        </tr>
    </table>
</div>
<div id="NoDataContent" style="padding-left:20px" visible="false" runat="server">
    <b><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_144 %></b>
</div>