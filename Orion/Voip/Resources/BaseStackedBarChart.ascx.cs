using System;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;

using Infragistics.UltraChart.Resources.Appearance;
using Infragistics.UltraChart.Shared.Styles;
using Infragistics.WebUI.UltraWebChart;
using Infragistics.UltraChart.Core.Layers;

public partial class Orion_Voip_Resources_BaseStackedBarChart : System.Web.UI.UserControl
{
    private Color[] barColors;

    private int width;
    private double aspectRatio;
	private int fontsize = 7;
	private string xAxisFormatString;
	private TextOrientation xAxisLabelsOrientation;
	private bool isYAxisTickmarksSmart;
	private int xAxisExtent;

	private const ChartType CHART_TYPE = ChartType.StackColumnChart;

	public Orion_Voip_Resources_BaseStackedBarChart()
    {
        width = 600;
        aspectRatio = 0.5;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        SetupChart();
    	chart.Axis.X = CreateStandardXAxis();
    	chart.Axis.Y = CreateCommonYAxis();
    	chart.Legend = CreateLegend();
    }

    private void SetupChart()
    {
        chart.Height = Height;
        chart.Width = Width;

        chart.ChartType = CHART_TYPE;
        chart.Border.Thickness = 0;
        chart.BackColor = Color.White;
        chart.EmptyChartText = String.Empty;

    	chart.ColorModel.CustomPalette = barColors;
        chart.ColorModel.ModelStyle = ColorModels.CustomLinear;

    	chart.StackChart.StackStyle = StackStyle.Normal;

        chart.Tooltips.Display = TooltipDisplay.Never;

    	chart.TitleLeft.Visible = true;
        chart.TitleLeft.HorizontalAlign = StringAlignment.Center;
        chart.TitleLeft.VerticalAlign = StringAlignment.Center;
        chart.TitleLeft.Font = new Font("Verdana", fontsize);

        GradientEffect chartEffect = new GradientEffect();
        chart.Effects.Effects.Add(chartEffect);

    	chart.ColumnChart.NullHandling = NullHandling.Zero;

        chart.DeploymentScenario.Scenario = ImageDeploymentScenario.Session;
        chart.DeploymentScenario.ImageType = ImageFormat.Png;
        chart.ImagePipePageName = "/Orion/Voip/Controls/ChartImagePipe.aspx";

   }

    private AxisItem CreateCommonYAxis()
    {
        AxisItem yAxis = new AxisItem();

		yAxis.axisNumber = AxisNumber.Y_Axis;
		yAxis.DataType = AxisDataType.Numeric;
		
		if (isYAxisTickmarksSmart)
		{
			yAxis.TickmarkStyle = AxisTickStyle.Smart;
		}
		else
		{
			yAxis.TickmarkStyle = AxisTickStyle.DataInterval;
			yAxis.TickmarkInterval = 1;
		}
		 
		yAxis.LineThickness = 1;

		yAxis.Labels.Font = new Font("Verdana", fontsize);
		yAxis.Labels.FontColor = Color.Black;
		yAxis.Labels.HorizontalAlign = StringAlignment.Far;
		yAxis.Labels.VerticalAlign = StringAlignment.Center;
		yAxis.Labels.Layout.Behavior = AxisLabelLayoutBehaviors.Auto;
		yAxis.Labels.Layout.Padding = 5;

		yAxis.Labels.ItemFormat = AxisItemLabelFormat.Custom;
		yAxis.Labels.ItemFormatString = "<DATA_VALUE:#.#>";
		yAxis.Extent = 60;      		
		yAxis.RangeType = AxisRangeType.Automatic;
        return yAxis;
    }

	private LegendAppearance CreateLegend()
	{
		LegendAppearance legend = new LegendAppearance();
		legend.DataAssociation = ChartTypeData.ColumnData;
		legend.Location = LegendLocation.Top;
		legend.Margins.Top = 5;
		legend.Margins.Left = 40;
		legend.Margins.Right = 5;
		legend.Margins.Bottom = 5;
		legend.Visible = true;
		legend.SpanPercentage = 10;
		legend.BorderThickness = 0;
		legend.BackgroundColor = Color.White;
		legend.Font = new Font("Verdana", fontsize);
		return legend;
	}

    private AxisItem CreateStandardXAxis()
    {
        AxisItem xAxis = new AxisItem();

        xAxis.TickmarkStyle = AxisTickStyle.Smart;

        xAxis.LineThickness = 1;
		// Depending on the text orientation xAxis extent is calcualated
		xAxis.Extent = (xAxisLabelsOrientation == TextOrientation.Horizontal ? 30 : xAxisExtent + (fontsize - 7) * 10);

		xAxis.Labels.SeriesLabels.Font = new Font("Verdana", fontsize);
    	xAxis.Labels.SeriesLabels.Format = AxisSeriesLabelFormat.Custom;
		xAxis.Labels.SeriesLabels.FormatString = xAxisFormatString;
		xAxis.Labels.SeriesLabels.FontSizeBestFit = true;
		xAxis.Labels.SeriesLabels.Orientation = xAxisLabelsOrientation;
		
        return xAxis;
    }

	/// <summary>
	/// Binds chart to first table in dataset
	/// </summary>
	/// <param name="dataset"></param>
    public void DataBind(DataSet dataset)
    {
		DataTable table = dataset.Tables[0];
		chart.DataSource = table;
		chart.DataBind();
		if (chart.DataSource == null || table.Rows.Count == 0)
		{
			NoDataContent.Visible = true;
			ChartContent.Visible = false;
		}
    }

    public void DataBind(DataSet dataset, string tableName)
    {
        DataTable table = dataset.Tables[tableName];
    	chart.DataSource = table;
		chart.DataBind();
    }

    #region Properties

	public string Title
	{
		set { ChartTitle.Text = value; }
	}

	public string SubTitle1
	{
		set { ChartSubTitle1.Text = value; }
	}

	public string SubTitle2
	{
		set { ChartSubTitle2.Text = value; }
	}

    public Color[] BarColors
    {
        get { return barColors; }
        set { barColors = value; }
    }

    public UltraChart Chart
    {
        get
        {
            return chart;
        }
    }

    public string YAxisLabel
    {
        set { chart.TitleLeft.Text = value; }
    }

    public int Width
    {
        get { return width; }
        set { width = value; }
    }

    public double AspectRatio
    {
        get { return aspectRatio; }
        set { aspectRatio = value; }
    }

    public int Height
    {
        get { return (int) (Width*AspectRatio); }
    }

	public int FontSize
	{
		get { return fontsize; }
		set { fontsize = value; }
	}

	public TextOrientation SeriesLabelsOrientation
	{
		get { return xAxisLabelsOrientation; }
		set { xAxisLabelsOrientation = value; }
	}

	public string XAxisFormatString
	{
		get { return xAxisFormatString; }
		set { xAxisFormatString = value; }
	}

	public bool IsYAxisTickmarksSmart
	{
		get { return isYAxisTickmarksSmart; }
		set { isYAxisTickmarksSmart = value; }
	}

	public int XAxisExtent
	{
		get { return xAxisExtent; }
		set { xAxisExtent = value; }
	}
    #endregion
}
