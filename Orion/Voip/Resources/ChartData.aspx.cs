using System;
using System.Data;
using System.IO;
using System.Web.UI;
using SolarWinds.Orion.Common;

public partial class Orion_Voip_Resources_ChartData : Page
{
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Response.AddHeader("Content-Disposition", "attachment; filename=ChartData.csv");
            Response.ContentType = "application/octet-stream";

            DataTable data = CustomChart.ChartData.Tables[0];
            
            using (StreamWriter writer = new StreamWriter(Response.OutputStream))
                DatabaseFunctions.DataTableToCSV(data, writer);

            Response.End();
        }
    }


    
}
