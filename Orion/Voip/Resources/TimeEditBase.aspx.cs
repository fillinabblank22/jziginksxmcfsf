﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_Voip_Resources_TimeEditBase : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
        {
            int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
            this._resource = ResourceManager.GetResourceByID(resourceID);
            Page.Title = string.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_216, this.Resource.Name);
        }

        if (!string.IsNullOrEmpty(Request.QueryString["NetObject"]))
            _netObjectID = Request.QueryString["NetObject"];

        if (!Page.IsPostBack)
        {
            string period = Resource.Properties.ContainsKey("Period") ?
                Resource.Properties["Period"] :
                "Last Hour";

            foreach (var _period in Periods.GetTimePeriodList())
            {
                TimePeriodsList.Items.Add(new ListItem(_period.DisplayName, _period.Name));
            }
            TimePeriodsList.SelectedValue = period;
            tbxSubTitle.Text = Resource.SubTitle;
            tbxTitle.Text = Resource.Title;
        }
    }

    private ResourceInfo _resource;
    protected ResourceInfo Resource
    {
        get { return _resource; }
    }

    private string _netObjectID;

    protected void SubmitButton_Click(object sender, EventArgs e)
    {
        Resource.Properties["Period"] = TimePeriodsList.SelectedValue;
        Resource.SubTitle = tbxSubTitle.Text;
        Resource.Title = tbxTitle.Text;

        ResourcesDAL.Update(Resource);

        string url = string.Format("/Orion/View.aspx?ViewID={0}", Resource.View.ViewID);
        if (!string.IsNullOrEmpty(_netObjectID))
            url = string.Format("{0}&NetObject={1}", url, _netObjectID);
        Response.Redirect(url);
    }
}
