﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SipTrunkDetails.ascx.cs"
    Inherits="Orion.Voip.Resources.SipTrunkDetails.OrionVoipResourcesSipTrunkDetails" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagName="IncludeExtJs" TagPrefix="orion" %>
<orion:IncludeExtJs ID="IncludeExtJs" runat="server" Version="3.4"/>
<orion:Include File="OrionCore.js" runat="server" />

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <script type="text/javascript">
                Ext.namespace('SW');
                Ext.namespace('SW.CCMSIPTrunk');

                SW.CCMSIPTrunk.PerfStack = {
                    redirectToPerfStack: function() {
                        var sipTrunkId = '<%= CcmSipTrunk.VoipCcmSipTrunk.SipTrunkId %>';
                        var perfStackRedirectLink = "/ui/perfstack/?presetTime=last12Hours&charts=0_Orion.IpSla.CCMSipTrunk_{SipTrunkId}-Orion.IpSla.CCMSipTrunkCallActivity.CallsActive," +
                            "0_Orion.IpSla.CCMSipTrunk_{SipTrunkId}-Orion.IpSla.CCMSipTrunkCallActivity.CallsAttempted,0_Orion.IpSla.CCMSipTrunk_{SipTrunkId}-Orion.IpSla.CCMSipTrunkCallActivity.CallsCompleted," +
                            "0_Orion.IpSla.CCMSipTrunk_{SipTrunkId}-Orion.IpSla.CCMSipTrunkCallActivity.VCallsActive,0_Orion.IpSla.CCMSipTrunk_{SipTrunkId}-Orion.IpSla.CCMSipTrunkCallActivity.VCallsCompleted;";
                        window.location = perfStackRedirectLink.replace(/{SipTrunkId}/g, sipTrunkId);
                    }
                };
         </script>   
               
        <table class="NeedsZebraStripes" cellpadding="2" cellspacing="0" width="100%" id="voipCallDetailsTable">
            <tbody id="voipCallDetailsTableBody">
            <tr class="voipCallDetailsTableRow">
		        <td class="voipCallDetailsHeaderText NoBorder"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_192 %></td>
		        <td colspan="2" class="Property NodeManagementIcons">
			        <div>
		                <a href="#" onclick="SW.CCMSIPTrunk.PerfStack.redirectToPerfStack(); return false;">
		                    <img src="/Orion/Voip/images/PerfStack/show-in-perfstack-color.svg" height="16" />  
                            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_274 %>
		                </a>
		            </div>
                </td>
	        </tr>
            <tr id="sipTrunkStatus">
                <td class="voipCallDetailsHeaderText NoBorder">
                   <%=Resources.VNQMWebContent.VNQMWEB_BO_Sip_Trunk_Status_Title%>
                </td>
                <td colspan="3" class="NoBorder">
                    <img id="imgIcon" runat="server" src="/Orion/Voip/images/SipTrunk/trunk16.png" alt="" />
                    &nbsp;&nbsp;
                    <asp:Label runat="server" ID="lblMessage" CssClass="voipSipTrunkDetailStatus"></asp:Label>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                   <%=Resources.VNQMWebContent.VNQMWEB_BO_Name%>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <asp:Label runat="server" ID="lblName"></asp:Label>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                  <%=Resources.VNQMWebContent.VNQMWEB_BO_Description%>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <asp:Label runat="server" ID="lblDescription"></asp:Label>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                   <%=Resources.VNQMWebContent.VNQMWEB_BO_DevicePool%>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                   <asp:Label runat="server" ID="lblDevicePool"></asp:Label>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                   <%=Resources.VNQMWebContent.VNQMWEB_BO_Location%>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                   <asp:Label runat="server" ID="lblLocation"></asp:Label>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                   <%=Resources.VNQMWebContent.VNQMWEB_BO_SipProfile%>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                   <asp:Label runat="server" ID="lblSipProfile"></asp:Label>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                   <%=Resources.VNQMWebContent.VNQMWEB_BO_SecurityProfile%>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                   <asp:Label runat="server" ID="lblSecurityProfile"></asp:Label>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                   <%=Resources.VNQMWebContent.VNQMWEB_BO_DefaultDtmfCapability%> 
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                   <asp:Label runat="server" ID="lblDefaultDtmfCapability"></asp:Label>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                   <%=Resources.VNQMWebContent.VNQMWEB_BO_Codec%> 
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                   <asp:Label runat="server" ID="lblCodec"></asp:Label>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                   <%=Resources.VNQMWebContent.VNQMWEB_BO_LastPollinLocalTime%> 
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                   <asp:Label runat="server" ID="lblRecordTimeLocal"></asp:Label>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                   <%=Resources.VNQMWebContent.VNQMWEB_BO_Sip_Trunk_Call_Manager_Title%> 
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <img src="" alt="" id="imgCallManger" runat="server" />
                    <asp:HyperLink runat="server" ID="hlCallManager"></asp:HyperLink>
                </td>
            </tr>
        </table>
    </Content>
</orion:resourceWrapper>
