using System;
using System.Collections.Generic;
using System.Linq;
using Resources;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

namespace Orion.Voip.Resources.SipTrunkDetails
{
    [ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.VOIP)]
    public partial class OrionVoipResourcesSipTrunkDestinations : VoipBaseResource
    {
        private const string helpFragment = "OrionIPSLAManagerAGVNQMLicensing";
        private string callManagerPath = "/Orion/Voip/CallManager.aspx?NetObject=VCCM:";
        protected int ScriptFriendlyResourceID => Math.Abs(Resource.ID);

        private CcmSipTrunk _sipTrunk;

        protected CcmSipTrunk CcmSipTrunk
            => _sipTrunk ?? (_sipTrunk = GetInterfaceInstance<ICcmSipTrunkProvider>().CcmSipTrunk);

        public override IEnumerable<Type> RequiredInterfaces => new[] { typeof(ICcmSipTrunkProvider) };

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!CcmSipTrunk.VoipCcmSipTrunkDestinations.Any())
            {
                Wrapper.Visible = false;
            }
            else
            {
                SipTrunkDestinationsTable.UniqueClientID = ScriptFriendlyResourceID;
                SipTrunkDestinationsTable.SWQL = SWQL;
            }
        }
    
        public string SWQL => $@"SELECT IpAddress, Port 
                        FROM Orion.IpSla.CCMSipTrunkDestinations
                        WHERE SipTrunkId = '{CcmSipTrunk.VoipCcmSipTrunk.SipTrunkId}'";
    
        public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Ajax;

        public override string DetachURL =>
            $"/Orion/DetachResource.aspx?ResourceID={Resource.ID}&NetObject=VCCMSIP:{CcmSipTrunk.VoipCcmSipTrunk.SipTrunkId}";

        public override string EditURL =>
            $"/Orion/Voip/Resources/EditSipTrunkDestinations.aspx?ResourceID={Resource.ID}&NetObject=VCCMSIP:{CcmSipTrunk.VoipCcmSipTrunk.SipTrunkId}";

        protected override string DefaultTitle => VNQMWebContent.VNQMWEBJS_SipTrunkDestinationsDefaultTitle;

        public override string HelpLinkFragment => "OrionIPSLAManagerAdministratorGuide_AddingCallManagersToVNQM";
    }
}
