<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SipTrunkDestinations.ascx.cs"
    Inherits="Orion.Voip.Resources.SipTrunkDetails.OrionVoipResourcesSipTrunkDestinations" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:CustomQueryTable runat="server" ID="SipTrunkDestinationsTable"/>
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= ScriptFriendlyResourceID %>,
                        initialPage: 0,
                        rowsPerPage: <%= Resource.Properties["SipTrunkDestinationsPageSize"] ?? "5" %>,
                        allowSort: false,
                        allowSearch: false,
                        allowPaging: true
                    });
		    
                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= SipTrunkDestinationsTable.ClientID %>');
                refresh();
            });    
        </script>
    </Content>
</orion:resourceWrapper>
