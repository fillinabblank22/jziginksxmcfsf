﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using Resources;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI.StatusIcons;
using StatusIconProvider = SolarWinds.Orion.IpSla.Web.StatusIconProvider;

namespace Orion.Voip.Resources.SipTrunkDetails
{
    [ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.VOIP)]
    public partial class OrionVoipResourcesSipTrunkDetails : VoipBaseResource
    {
        private const string CallManagerPath = "/Orion/Voip/CallManager.aspx?NetObject=VCCM:";

        private CcmSipTrunk _sipTrunk;

        protected CcmSipTrunk CcmSipTrunk
            => _sipTrunk ?? (_sipTrunk = GetInterfaceInstance<ICcmSipTrunkProvider>().CcmSipTrunk);

        public override IEnumerable<Type> RequiredInterfaces => new[] { typeof(ICcmSipTrunkProvider) };
        
        protected void Page_Init(object sender, EventArgs e)
        {
            if (CcmSipTrunk.VoipCcmSipTrunk == null) return;

            imgIcon.Src = StatusIconProvider.GetSipTrunkImagePath((int)CcmSipTrunk.VoipCcmSipTrunk.Status, StatusIconSize.Small);

            var isSipRegistered = CcmSipTrunk.VoipCcmSipTrunk.Status == SipTrunkStatus.Registered ||
                                  CcmSipTrunk.VoipCcmSipTrunk.Status == SipTrunkStatus.PartiallyRegistered;

            lblMessage.Text = ((SipTrunkStatus)CcmSipTrunk.VoipCcmSipTrunk.Status).ToString();
 
            lblName.Text = CcmSipTrunk.VoipCcmSipTrunk.Name;
            lblDescription.Text = CcmSipTrunk.VoipCcmSipTrunk.Description;
            lblDevicePool.Text = CcmSipTrunk.VoipCcmSipTrunk.DevicePool;
            lblLocation.Text = CcmSipTrunk.VoipCcmSipTrunk.Location;
            lblSipProfile.Text = CcmSipTrunk.VoipCcmSipTrunk.SipProfile;
            lblSecurityProfile.Text = CcmSipTrunk.VoipCcmSipTrunk.SecurityProfile;
            lblDefaultDtmfCapability.Text = CcmSipTrunk.VoipCcmSipTrunk.DefaultDtmfCapability.ToString();
            lblCodec.Text = CcmSipTrunk.VoipCcmSipTrunk.MTPOrigCodec;
            lblRecordTimeLocal.Text = CcmSipTrunk.VoipCcmSipTrunk.RecordTimeUtc.ToLocalTime().ToString(CultureInfo.InvariantCulture);

            imgCallManger.Src = "/Orion/Voip/images/CallIcons/cM_green_s.png";
            hlCallManager.Text = CcmSipTrunk.VoipCcmSipTrunk.CCMName;
            hlCallManager.NavigateUrl = this.Page.ResolveUrl(CallManagerPath + CcmSipTrunk.VoipCcmSipTrunk.NodeId);
        }

        public override string DetachURL =>
            $"/Orion/DetachResource.aspx?ResourceID={Resource.ID}&NetObject=VCCMSIP:{CcmSipTrunk.VoipCcmSipTrunk.SipTrunkId}";

        public override string EditURL =>
            $"/Orion/Voip/Resources/EditVoipResource.aspx?ResourceID={Resource.ID}&NetObject=VCCMSIP:{CcmSipTrunk.VoipCcmSipTrunk.SipTrunkId}";

        protected override string DefaultTitle => VNQMWebContent.VNQMWEBJS_SipTrunkDetailsDefaultTitle;

        public override string HelpLinkFragment => "OrionIPSLAManagerAdministratorGuide_AddingCallManagersToVNQM";
        protected string GetLocalizedProperty(string prefix, string property)
        {
            var manager = ResourceManagerRegistrar.Instance;
            var key = manager.CleanResxKey(prefix, property);
            return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
        }

    }
}
