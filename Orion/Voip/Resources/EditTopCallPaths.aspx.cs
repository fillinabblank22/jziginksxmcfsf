using System;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.IpSla.Web.UI;

public partial class Orion_Voip_Resources_EditTopCallPaths : ResourceEditPage
{
    
    protected override void Initialize()
	{
		this.resourceTitleEditor.ResourceTitle = this.Resource.Title;
        this.Title = String.Format(Resources.VNQMWebContent.VNQMWEBDATA_AK1_167, Resource.Title);
		this.ObjectType.Text = String.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_77, WebSecurityHelper.SanitizeHtml(Request.QueryString["ObjectType"]));

		if (!this.IsPostBack)
		{
			string howMany = this.Resource.Properties["HowMany"];
			if (string.IsNullOrEmpty(howMany))
				howMany = "5";
			this.HowMany.Text = howMany;
		}
	}

	protected void SubmitClick(object sender, EventArgs e)
	{
		if (!resourceTitleEditor.ResourceTitle.Equals(Resource.Title))
		{
			Resource.Title = resourceTitleEditor.ResourceTitle;
			SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
		}
		
		this.Resource.Properties["HowMany"] = this.HowMany.Text;

	    GoBack();
	}
}
