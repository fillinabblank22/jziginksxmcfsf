﻿<%@ Page Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true" CodeFile="EditNodeTree.aspx.cs" Inherits="Orion_Voip_Resources_EditNodeTree" Title="Untitled Page" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register TagPrefix="orion" TagName="FilterNodesSql" Src="~/Orion/Controls/FilterNodesSql.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1 style="padding-left: 0px"><%= Page.Title %></h1>
    
    <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="true" ShowSubTitleHintMessage="true" SubTitleHintMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_196 %>" />
    
    <p>
        <b><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_197 %></b><br />
        <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_198 %>
    </p>
    
    <p>
        <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_199 %><br />
        <asp:ListBox runat="server" ID="lbxGroup1" SelectionMode="single" Rows="1">
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_89  %>" Value="" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_39  %>" Value="Status" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_VB1_31  %>" Value="Vendor" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_69  %>" Value="MachineType" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_206 %>" Value="Contact" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBCODE_AK1_47  %>" Value="Location" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_207 %>" Value="IOSImage" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_208 %>" Value="IOSVersion" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_209 %>" Value="ObjectSubType" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_210 %>" Value="SysObjectID" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_211 %>" Value="NPM_NV_EW_NODES_V.EnergyWise" />
        </asp:ListBox>
    </p>
    
    <p>
        <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_200 %><br />
        <asp:ListBox runat="server" ID="lbxGroup2" SelectionMode="single" Rows="1">
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_89  %>" Value="" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_39  %>" Value="Status" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_VB1_31  %>" Value="Vendor" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_69  %>" Value="MachineType" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_206 %>" Value="Contact" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBCODE_AK1_47  %>" Value="Location" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_207 %>" Value="IOSImage" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_208 %>" Value="IOSVersion" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_209 %>" Value="ObjectSubType" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_210 %>" Value="SysObjectID" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_211 %>" Value="NPM_NV_EW_NODES_V.EnergyWise" />
        </asp:ListBox>
    </p>
    
    <p>
        <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_201 %><br />
        <asp:ListBox runat="server" ID="lbxGroup3" SelectionMode="single" Rows="1">
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_89  %>" Value="" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_39  %>" Value="Status" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_VB1_31  %>" Value="Vendor" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_69  %>" Value="MachineType" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_206 %>" Value="Contact" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBCODE_AK1_47  %>" Value="Location" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_207 %>" Value="IOSImage" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_208 %>" Value="IOSVersion" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_209 %>" Value="ObjectSubType" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_210 %>" Value="SysObjectID" />
            <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_211 %>" Value="NPM_NV_EW_NODES_V.EnergyWise" />
        </asp:ListBox>
    </p>
    
    <b><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_202 %></b>
    <asp:RadioButtonList runat="server" ID="GroupNulls">
        <asp:ListItem Value="true" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_203 %>" />
        <asp:ListItem Value="false" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_204 %>" />
    </asp:RadioButtonList>
    
    <p>
        <asp:CheckBox runat="server" ID="RememberCollapseState" /> <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_205 %>
    </p>

   <orion:FilterNodesSql runat="server" ID="SQLFilter" />
   <br/>
    <div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" DisplayType="Primary" OnClick="SubmitClick" />
    </div>
</asp:Content>

