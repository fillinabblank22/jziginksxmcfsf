// The ID of the timer set in HidePeriodMenu
var PeriodMenuID;
var CustomPeriodFormID; 

// A helper function
function FindPosition(obj)  
{  
   var curleft = curtop = 0;  
   if (obj.offsetParent) 
   {  
       curleft = obj.offsetLeft; 
       curtop = obj.offsetTop; 
 
       while (obj = obj.offsetParent) 
       {  
           curleft += obj.offsetLeft; 
           curtop += obj.offsetTop; 
       }  
   }  
   return [curleft,curtop];  
} 

// PeriodMenu's OnMouseOut
function HidePeriodMenu()  
{  
   document.getElementById("PeriodMenu").style.display = 'none';
}  


// PeriodDropDown's onClick
function ShowPeriodMenu()  
{  
    var MyMenu = document.getElementById("PeriodMenu");
    var MyDropDown = document.getElementById("PeriodDropDown");
    var position = FindPosition(MyDropDown);

    //Adding 10px to not overlap header text
    MyMenu.style.top = position[1] + MyDropDown.clientHeight + 10; 
    MyMenu.style.left = position[0]+MyDropDown.clientWidth; 

    MyMenu.style.display = "";
    return false ;  
}  
 

