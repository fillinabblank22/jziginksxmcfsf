﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IpSlaGraph.ascx.cs" Inherits="Orion_Voip_Resources_Graphs_IpSlaGraph" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div style="position:relative;left:-8px;">
           <a href="<%= this.CustomUrl %>">
                <img src="<%= this.ChartImageUrl %>" />
           </a>
        </div>
    </Content>
</orion:resourceWrapper>