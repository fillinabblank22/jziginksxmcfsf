﻿using System;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Data.Database;

public partial class 
    Orion_Voip_Resources_Graphs_IpSlaGraph : IpSlaGraphResource
{
    private OperationResultsKey _key;

    protected void Page_Load(object sender, EventArgs e)
    {
        string graphType = Resource.Properties["GraphType"];

        _key = OperationResultsKey.RoundTripTime;

        if (!String.IsNullOrEmpty(graphType) && Enum.IsDefined(typeof (OperationResultsKey), graphType))
            _key = (OperationResultsKey) Enum.Parse(typeof (OperationResultsKey), graphType);
    }

    protected string ChartImageUrl
    {
        get
        {
            return GetChartImageUrl(IpSlaOperation, _key.ToString());
        }
    }

    protected string CustomUrl
    {
        get { return GenerateCustomChartLink(IpSlaOperation, _key.ToString()); }
    }

    protected override string DefaultTitle
    {
        get { return Resources.VNQMWebContent.VNQMWEBCODE_VB1_218; }
    }

    public override string HelpLinkFragment
    {
        get 
        {
            var value = Resource.Properties["HelpLinkFragment"];
            if (String.IsNullOrEmpty(value))
                value = "OrionVoIPMonitorPHChartMinMaxAverage";

            return value;
        }
    }
}
