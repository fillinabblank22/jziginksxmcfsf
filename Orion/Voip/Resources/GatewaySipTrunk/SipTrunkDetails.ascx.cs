﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Resources;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using VoipGatewaySipTrunk = SolarWinds.Orion.IpSla.Web.VoipGatewaySipTrunk;

namespace Orion.Voip.Resources.GatewaySipTrunk
{
    [ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.VOIP)]
    public partial class OrionVoipResourcesGatewaySipTrunkSipTrunkDetails : VoipBaseResource
    {
        private const string GatewayPath = "/Orion/Voip/VoIPGateway.aspx?NetObject=VVG:";
        private VoipGatewaySipTrunk _sipTrunk;

        protected VoipGatewaySipTrunk SipTrunk
            => _sipTrunk ?? (_sipTrunk = GetInterfaceInstance<IVoipGatewaySipTrunkProvider>().VoipGatewaySipTrunk);

        public override IEnumerable<Type> RequiredInterfaces => new[] { typeof(IVoipGatewaySipTrunkProvider)};
        
        protected void Page_Init(object sender, EventArgs e)
        {
            if (SipTrunk == null) return;

            imgIcon.Src = StatusIconProvider.GetGatewaySipTrunkImagePath(SipTrunk.Status);
            lblMessage.Text = ((VoipGatewaySipTrunkStatuses)SipTrunk.Status).ToString();
            lblName.Text = SipTrunk.Name;
            lblRecordTimeLocal.Text = SipTrunk.RecordTimeUtc.ToLocalTime().ToString(CultureInfo.InvariantCulture);
            imgVoipGateway.Src = "/Orion/Voip/images/CallIcons/g_green_s.png";
            hlVoipGateway.Text = SipTrunk.VoipGatewayName;
            hlVoipGateway.NavigateUrl = Page.ResolveUrl(GatewayPath + SipTrunk.VoipGatewayNodeID);
        }

        public override string DetachURL =>
            $"/Orion/DetachResource.aspx?ResourceID={Resource.ID}&NetObject=VGWSIP:{SipTrunk.SipTrunkID}";

        public override string EditURL =>
            $"/Orion/Voip/Resources/EditVoipResource.aspx?ResourceID={Resource.ID}&NetObject=VGWSIP:{SipTrunk.SipTrunkID}";

        protected override string DefaultTitle => VNQMWebContent.VNQMWEBJS_BO_GatewaySipTrunkDetails;

        public override string HelpLinkFragment => "";
        protected string GetLocalizedProperty(string prefix, string property)
        {
            var manager = ResourceManagerRegistrar.Instance;
            var key = manager.CleanResxKey(prefix, property);
            return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
        }

    }
}
