﻿using System;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;


namespace Orion.Voip.Resources.GatewaySipTrunk
{
    [ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.VOIP)]
    public partial class OrionVoipResourcesGatewaySipTrunkChartsXuiSipTrunkUtilization : BaseResourceControl,
        IXuiResource, IResourceIsInternal
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ResourceWrapper.Visible = OrionMinReqsMaster.IsApolloEnabled;
            Selector = GetStringValue("selector", null);
        }

        protected string Selector { get; set; }

        protected override string DefaultTitle => "VoIP Gateway SIP Trunk Utilization";

        public bool IsInternal => true;

        public override string HelpLinkFragment => "";
    }
}
