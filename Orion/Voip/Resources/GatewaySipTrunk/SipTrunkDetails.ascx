﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SipTrunkDetails.ascx.cs"
    Inherits="Orion.Voip.Resources.GatewaySipTrunk.OrionVoipResourcesGatewaySipTrunkSipTrunkDetails" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagName="IncludeExtJs" TagPrefix="orion" %>
<orion:IncludeExtJs ID="IncludeExtJs" runat="server" Version="3.4"/>
<orion:Include File="OrionCore.js" runat="server" />

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <script type="text/javascript">
                Ext.namespace('SW');
                Ext.namespace('SW.VoIPGatewaySIPTrunk');

                SW.VoIPGatewaySIPTrunk.PerfStack = {
                    redirectToPerfStack: function() {
                        var sipTrunkId = '<%= SipTrunk.SipTrunkID %>';
                        var perfStackRedirectLink = "/ui/perfstack/?presetTime=last12Hours&charts=0_Orion.IpSla.VoipGatewaySipTrunks_{SipTrunkId}-Orion.IpSla.VoipGatewaySipTrunkStatusStats.Status," +
                            "0_Orion.IpSla.VoipGatewaySipTrunks_{SipTrunkId}-Orion.IpSla.VoipGatewaySipTrunkCallActivity.IncomingCalls,0_Orion.IpSla.VoipGatewaySipTrunks_{SipTrunkId}-Orion.IpSla.VoipGatewaySipTrunkCallActivity.OutgoingCalls;";
                        window.location = perfStackRedirectLink.replace(/{SipTrunkId}/g, sipTrunkId);
                    }
                };
         </script>   
               
        <table class="NeedsZebraStripes" cellpadding="2" cellspacing="0" width="100%" id="voipCallDetailsTable">
            <tbody id="voipCallDetailsTableBody">
            <tr class="voipCallDetailsTableRow">
		        <td class="voipCallDetailsHeaderText NoBorder"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_192 %></td>
		        <td colspan="2" class="Property NodeManagementIcons">
			        <div>
		                <a href="#" onclick="SW.VoIPGatewaySIPTrunk.PerfStack.redirectToPerfStack(); return false;">
		                    <img src="/Orion/Voip/images/PerfStack/show-in-perfstack-color.svg" height="16" />  
                            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_274 %>
		                </a>
		            </div>
                </td>
	        </tr>
            <tr id="sipTrunkStatus">
                <td class="voipCallDetailsHeaderText NoBorder">
                   <%=Resources.VNQMWebContent.VNQMWEB_BO_Sip_Trunk_Status_Title%>
                </td>
                <td colspan="3" class="NoBorder">
                    <img id="imgIcon" runat="server" src="/Orion/Voip/images/SipTrunk/trunk16.png" alt="" />
                    <asp:Label runat="server" ID="lblMessage" CssClass="voipSipTrunkDetailStatus"></asp:Label>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                   <%=Resources.VNQMWebContent.VNQMWEB_BO_Name%>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <asp:Label runat="server" ID="lblName"></asp:Label>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                   <%=Resources.VNQMWebContent.VNQMWEB_BO_LastPollinLocalTime%> 
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                   <asp:Label runat="server" ID="lblRecordTimeLocal"></asp:Label>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                   <%=Resources.VNQMWebContent.VNQMWEB_BO_VoipGateway%> 
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <img src="" alt="" id="imgVoipGateway" runat="server" />
                    <asp:HyperLink runat="server" ID="hlVoipGateway"></asp:HyperLink>
                </td>
            </tr>
        </table>
    </Content>
</orion:resourceWrapper>
