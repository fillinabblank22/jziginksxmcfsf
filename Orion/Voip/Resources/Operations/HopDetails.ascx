﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HopDetails.ascx.cs" Inherits="Orion_Voip_Resources_Operations_HopDetails" %>
<%@ Register TagPrefix="voip" TagName="StatusIcon" Src="~/Orion/Voip/Controls/OperationStatusImage.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
<Content>
	
	<div style="height: 5px;">&nbsp;</div>
	
	<table class="NeedsZebraStripes" border="0" cellPadding="2" cellSpacing="0" width="100%">
	<tr>
      <td class="Property" width="10">&nbsp;</td>
      <td class="PropertyHeader" width="10" vAlign="center"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_182%></td>
      <td class="Property" width="10">&nbsp;</td>
	  <td align="center" class="Property" width="10"><img src="/Orion/Voip/images/OperationTypeIcons/PathHop_16.gif"/></td>
      <td class="Property"><%# String.Format(Resources.VNQMWebContent.VNQMWEBDATA_VB1_183, GetCurrentHopNumber(), GetMaxHopNumber())%></td>
	</tr>
	<tr>
      <td class="Property" width="10">&nbsp;</td>
      <td class="PropertyHeader" width="10" vAlign="center"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_184%></td>
      <td class="Property" width="10">&nbsp;</td>
      <td align="center" class="Property" width="10"><voip:StatusIcon ID="icon" OperationStatus='<%# GetOperationStatus() %>' runat="server" /></td>
      <td class="Property"><a href="<%# GetOperationUrl() %>"><%# GetOperationFullName() %></a></td>
	</tr>
	<tr>
      <td class="Property" width="10">&nbsp;</td>
      <td class="PropertyHeader" width="10" vAlign="center"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_185 %></td>
      <td class="Property" width="10">&nbsp;</td>
	  <td class="Property" width="10">&nbsp;</td>
      <td class="Property"><%# GetTimeOfScan() %>&nbsp;</td>
	</tr>
	</table>

</Content>
</orion:resourceWrapper>

