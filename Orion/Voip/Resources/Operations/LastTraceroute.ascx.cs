﻿using System;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;

public partial class Orion_Voip_Resources_Operations_LastTraceroute : IpSlaResource
{
    private static readonly MetricType[] EchoMetrics = new[] {MetricType.RoundTripTime};
    private static readonly MetricType[] JitterMetrics = new[] { MetricType.RoundTripTime, MetricType.Jitter, MetricType.PacketLoss, MetricType.Latency };

    protected override string DefaultTitle
    {
        get { return Resources.VNQMWebContent.VNQMWEBDATA_VB1_187; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            var value = Resource.Properties["HelpLinkFragment"];
            if (String.IsNullOrEmpty(value))
            {
                var operation = Operation;
                if (operation != null)
                {
                    switch (operation.OperationType)
                    {
                        case OperationType.IcmpPathEcho:
                        case OperationType.IcmpPathJitter:
                            goto default;
                        default:
                            return "OrionIPSLAManagerPHResourceTraceroute";
                    }
                }
            }

            return value;
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        var operation = Operation;
        if (operation != null)
        {
            switch (operation.OperationType)
            {
                case OperationType.IcmpPathEcho:
                    Traceroute.MetricTypes = EchoMetrics;
                    break;
                case OperationType.IcmpPathJitter:
                    Traceroute.MetricTypes = JitterMetrics;
                    break;
            }
            Traceroute.OperationInstanceId = operation.OperationInstanceID;
            Traceroute.PathId = operation.PathId;
            Traceroute.DataSource = PathOperationResultsDAL.GetHopResults(operation.OperationInstanceID, null, operation.PathId, PathOperationResultsDAL.Columns.HopIpAddress | PathOperationResultsDAL.Columns.HopNodeCaption, Traceroute.MetricTypes).Rows;
        }
    }
}
