﻿using System;
using System.Data;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using System.Collections.Generic;
using SolarWinds.Orion.IpSla.Web.DAL;

public partial class Orion_Voip_Resources_Operations_HopDetails : IpSlaResource
{
    private static readonly string NotAvailableHtml = String.Format("<i>{0}</i>", Resources.VNQMWebContent.VNQMWEBCODE_VB1_134);

    private DataRow detailRow;

    protected override string DefaultTitle
    {
        get { return Resources.VNQMWebContent.VNQMWEBDATA_VB1_186; }
    }

    public override string EditURL
    {
        get { return Resource.Properties["EditURL"] ?? string.Empty; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            var value = Resource.Properties["HelpLinkFragment"];
            if (String.IsNullOrEmpty(value))
                value = "OrionIPSLAManagerPHResourceHopDetails";

            return value;
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IIpSlaOperationProvider) }; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var operation = Operation;
        if (operation != null)
        {
            var hopDetails = PathOperationResultsDAL.GetHopResults(operation.OperationInstanceID, operation.PathHopIndex ?? -1, operation.PathId, PathOperationResultsDAL.Columns.OperationStatus | PathOperationResultsDAL.Columns.RecordTime | PathOperationResultsDAL.Columns.MaxHopIndex);
            if (hopDetails.Rows.Count > 0)
            {
                this.detailRow = hopDetails.Rows[0];
                this.DataBind();
            }
        }
    }

    protected OperationStatus GetOperationStatus()
    {
        if (this.detailRow != null)
        {
            var status = this.detailRow["OperationStatus"] as short?;
            if (status.HasValue)
            {
                return (OperationStatus)status.Value;
            }
        }

        return OperationStatus.Unknown;
    }

    protected string GetOperationUrl()
    {
        var operation = Operation;
        if (operation != null)
        {
            return IpSlaOperationHelper.GetOperationDetailPageLink(operation.OperationInstanceID, null, null);
        }
        return string.Empty;
    }

    protected string GetOperationFullName()
    {
        var operation = Operation;
        if (operation != null)
        {
            //var hopName = this.detailRow != null
            //    ? this.detailRow["HopNodeCaption"] as string ?? this.detailRow["HopIpAddress"] as string ?? NotAvailableHtml
            //    : NotAvailableHtml;
            //return operation.OperationTypeDescription + " &ndash; " + IpSlaOperationHelper.GetOperationName(operation.SourceSiteName, hopName);
            return operation.OperationTypeDescription + " &ndash; " + operation.Name;
        }
        return string.Empty;
    }

    protected string GetTimeOfScan()
    {
        if (this.detailRow != null)
        {
            var recordTimeUtc = this.detailRow["RecordTime"] as DateTime?;
            if (recordTimeUtc.HasValue)
            {
                return recordTimeUtc.Value.ToLocalTime().ToString("F");
            }
        }

        return NotAvailableHtml;
    }

    protected string GetCurrentHopNumber()
    {
        if (this.detailRow != null)
        {
            var hopIndex = this.detailRow["HopIndex"] as int?;
            if (hopIndex.HasValue)
            {
                return (hopIndex + 1).Value.ToString();
            }
        }

        return NotAvailableHtml;
    }

    protected string GetMaxHopNumber()
    {
        if (this.detailRow != null)
        {
            var maxHopIndex = this.detailRow["MaxHopIndex"] as int?;
            if (maxHopIndex.HasValue)
            {
                return (maxHopIndex + 1).Value.ToString();
            }
        }

        return NotAvailableHtml;
    }
}
