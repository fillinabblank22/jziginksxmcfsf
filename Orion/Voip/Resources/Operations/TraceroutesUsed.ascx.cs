﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.IpSla.Web.UI.Grid;

public partial class Orion_Voip_Resources_Operations_TraceroutesUsed : IpSlaResource
{
    private static readonly MetricType[] EchoMetrics = new[] {MetricType.RoundTripTime};
    private static readonly MetricType[] JitterMetrics = new[] {MetricType.RoundTripTime, MetricType.Jitter, MetricType.PacketLoss, MetricType.Latency};

    private MetricType[] metricTypes;
    private int totalPathCount;
    private string period;

    protected override string DefaultTitle
    {
        get { return Resources.VNQMWebContent.VNQMWEBCODE_VB1_246; }
    }

    public override string EditURL
    {
        get
        {
            //If user wants to set specific parameters for graph - period must be custom for each graph
            string returnURL = GraphHelper.RemoveQueryStringParameter(Page.Request.Url.PathAndQuery, GraphResource.Keys.Period);
            returnURL = Server.UrlEncode(returnURL);
            return string.Format("/Orion/Voip/Resources/TraceroutesUsedEdit.aspx?ResourceID={0}&ReturnURL={1}", Resource.ID, returnURL);
        }
    }

    public override string SubTitle
    {
        get { return GetLocalizedProperty("Period", Period); }
    }

    public override string HelpLinkFragment
    {
        get
        {
            var value = Resource.Properties["HelpLinkFragment"];
            if (String.IsNullOrEmpty(value))
            {
                var operation = Operation;
                if (operation != null)
                {
                    switch (operation.OperationType)
                    {
                        case OperationType.IcmpPathEcho:
                        case OperationType.IcmpPathJitter:
                            goto default;
                        default:
                            return "OrionIPSLAManagerPHResourceTraceroutesUsed";
                    }
                }
            }

            return value;
        }
    }

    public string Period
    {
        get { return !string.IsNullOrEmpty(this.period) ? this.period : DefaultPeriod; }
        set { this.period = value; }
    }

    protected virtual string DefaultPeriod
    {
        get { return "TODAY"; }
    }

    protected void GetPeriodData()
    {
        this.period = !string.IsNullOrEmpty(Request.Params[GraphResource.Keys.Period])
            ? Request.Params[GraphResource.Keys.Period]
            : this.Resource.Properties[GraphResource.Keys.Period];
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        GetPeriodData();

        EnsureChildControls();

        var operation = Operation;
        if (operation != null)
        {
            var dateEnd = DateTime.Now;
            var dateStart = DateTime.Now;

            var timePeriod = Period;

            Periods.Parse(ref timePeriod, ref dateStart, ref dateEnd);
            DALHelper.ConvertDatesToUtc(ref dateStart, ref dateEnd);

            var dataRows = PathOperationResultsDAL.GetRecentPaths(Operation.OperationInstanceID, dateStart, dateEnd, PathOperationResultsDAL.Columns.RecordTime | PathOperationResultsDAL.Columns.HopIpAddress | PathOperationResultsDAL.Columns.HopNodeCaption | PathOperationResultsDAL.Columns.PathID | PathOperationResultsDAL.Columns.PathCount, this.metricTypes).Rows;
            var groups = dataRows.Cast<DataRow>().GroupBy(row => (int)row["PathID"]).Select(grouping =>
                {
                    DataRow lastHopRow = null;
                    int? lastHopIndex = null;
                    var rows = grouping.Where(row =>
                        {
                            var hopIndex = row["HopIndex"] as int?;
                            if (!hopIndex.HasValue)
                            {
                                lastHopRow = row;
                                return false;
                            }

                            if (!lastHopIndex.HasValue || hopIndex.Value > lastHopIndex.Value)
                            {
                                lastHopRow = row;
                                lastHopIndex = hopIndex;
                            }

                            return true;
                        }).ToArray();
                    return new KeyValuePair<DataRow, DataRow[]>(lastHopRow, rows);
                }).OrderByDescending(pair => pair.Key["RecordTime"]).ToArray();
            this.totalPathCount = groups.Sum(pair => (int)pair.Key["PathCount"]);
            ExpandableGrid.DataSource = groups;
            ExpandableGrid.DataBind();
        }
    }

    protected override void CreateChildControls()
    {
        base.CreateChildControls();

        ExpandableGrid.Columns.RemoveRange(1, ExpandableGrid.Columns.Count - 3);

        var operation = Operation;
        if (operation == null)
            return;

        switch (operation.OperationType)
        {
            case OperationType.IcmpPathEcho:
                this.metricTypes = EchoMetrics;
                break;
            case OperationType.IcmpPathJitter:
                this.metricTypes = JitterMetrics;
                break;
            default:
                this.metricTypes = null;
                break;
        }

        if (this.metricTypes == null || this.metricTypes.Length == 0)
            return;

        var colStyle = string.Empty;// string.Format(CultureInfo.InvariantCulture, "width: {0:D}%;", 15 + 20 / metricTypes.Length);
        var colIndex = 2;
        foreach (var metricType in this.metricTypes)
        {
            Column column;
            switch (metricType)
            {
                case MetricType.RoundTripTime:
                    column = new Column
                    {
                        Caption = Resources.VNQMWebContent.VNQMWEBCODE_VB1_241,
                        Template = ValueTemplate.RoundTripTime,
                    };
                    break;
                case MetricType.Jitter:
                    column = new Column
                    {
                        Caption = Resources.VNQMWebContent.VNQMWEBDATA_AK1_163,
                        Template = ValueTemplate.Jitter,
                    };
                    break;
                case MetricType.Latency:
                    column = new Column
                    {
                        Caption = Resources.VNQMWebContent.VNQMWEBDATA_AK1_164,
                        Template = ValueTemplate.Latency,
                    };
                    break;
                case MetricType.PacketLoss:
                    column = new Column
                    {
                        Caption = Resources.VNQMWebContent.VNQMWEBDATA_AK1_165,
                        Template = ValueTemplate.PacketLoss,
                    };
                    break;
                default:
                    continue;
            }
            ExpandableGrid.Columns.Insert(colIndex, column);
            colIndex++;
        }
    }

    protected string GetRecordTime(object dataItem)
    {
        var pair = (KeyValuePair<DataRow, DataRow[]>)dataItem;
        return ((DateTime)pair.Key["RecordTime"]).ToLocalTime().ToString("F");
    }

    protected string GetNumberOfHops(object dataItem)
    {
        var pair = (KeyValuePair<DataRow, DataRow[]>)dataItem;
        return pair.Value.Length.ToString("D");
    }

    protected string GetUsagePercentage(object dataItem)
    {
        var pair = (KeyValuePair<DataRow, DataRow[]>)dataItem;
        var occurences = (int)pair.Key["PathCount"];
        return this.totalPathCount != 0
            ? string.Format("{0:0.#}{1}", ((float)occurences * 100) / this.totalPathCount, Resources.VNQMWebContent.VNQMWEBCODE_VB1_204)
            : String.Format("<i>{0}</i>", Resources.VNQMWebContent.VNQMWEBCODE_VB1_134);
    }

    protected void Grid_ExpandableDataBound(object sender, GridItemEventArgs e)
    {
        var operation = Operation;
        if (operation == null)
            return;

        var pair = (KeyValuePair<DataRow, DataRow[]>)e.GridItem.DataItem;

        var traceroute = (Orion_Voip_Controls_Traceroute)e.GridItem.FindControl("Traceroute");
        traceroute.MetricTypes = this.metricTypes;
        traceroute.OperationInstanceId = operation.OperationInstanceID;
        traceroute.PathId = (int)pair.Key["PathID"];
        traceroute.DataSource = pair.Value;
    }

    private class ValueTemplate : ITemplate
    {
        public static readonly ValueTemplate RoundTripTime = new ValueTemplate("AvgRoundTripTime", Resources.VNQMWebContent.VNQMWEBDATA_AK1_124);
        public static readonly ValueTemplate Latency = new ValueTemplate("AvgLatency", Resources.VNQMWebContent.VNQMWEBDATA_AK1_124);
        public static readonly ValueTemplate Jitter = new ValueTemplate("AvgJitter", Resources.VNQMWebContent.VNQMWEBDATA_AK1_124);
        public static readonly ValueTemplate PacketLoss = new ValueTemplate("AvgPacketLoss", Resources.VNQMWebContent.VNQMWEBDATA_AK1_195);

        private readonly string valueName;
        private readonly string unitName;

        private ValueTemplate(string valueName, string unitName)
        {
            this.valueName = valueName;
            this.unitName = unitName;
        }

        public void InstantiateIn(Control container)
        {
            var dataItem = DataBinder.GetDataItem(container);
            var valueLiteral = new Literal { ID = "value", Visible = false };
            var unitText = new HtmlGenericControl { InnerHtml = "&nbsp;" + this.unitName, Visible = false };
            var naText = new HtmlGenericControl { InnerHtml = String.Format("<i>{0}</i>", Resources.VNQMWebContent.VNQMWEBCODE_VB1_134), Visible = false };
            valueLiteral.DataBinding += (sender, e) =>
            {
                var value = DataBinder.Eval(dataItem, "Key['" + this.valueName + "']") as double?;
                if (value.HasValue)
                {
                    valueLiteral.Text = value.Value.ToString("0.0");
                    valueLiteral.Visible = unitText.Visible = true;
                }
                else
                {
                    naText.Visible = true;
                }
            };
            var div = new HtmlGenericControl("div")
                {
                    Controls = {valueLiteral, unitText, naText},
                };
            container.Controls.Add(div);
        }
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}
