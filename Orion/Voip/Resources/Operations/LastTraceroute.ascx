﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LastTraceroute.ascx.cs" Inherits="Orion_Voip_Resources_Operations_LastTraceroute" %>
<%@ Register TagPrefix="ipsla" TagName="Traceroute" Src="~/Orion/Voip/Controls/Traceroute.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
<Content>

<div style="height: 5px;">&nbsp;</div>

<ipsla:Traceroute ID="Traceroute" runat="server" AlternateRowColors="true" />

</Content>
</orion:resourceWrapper>
