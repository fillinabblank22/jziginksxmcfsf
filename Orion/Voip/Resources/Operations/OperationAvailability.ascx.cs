﻿using System;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web;
using GraphResource=SolarWinds.Orion.IpSla.Web.GraphResource;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

public partial class Orion_Voip_Resources_Operations_OperationAvailability : GraphResource, IResourceIsInternal
{
    protected override string DefaultTitle
    {
        get { return Resources.VNQMWebContent.VNQMWEBCODE_VB1_242; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionIPSLAManagerPHResourceOperationAvailability";
        }
    }

    public string ChartImageUrl
    {
        get
        {
            NetObject netObject = GetNetObject();
            if(netObject != null)
                return GetChartImageUrl(netObject, "VoipOperationAvailability");

            return String.Empty;
        }
    }

    protected NetObject GetNetObject()
    {
        IIpSlaOperationProvider operationProvider = GetInterfaceInstance<IIpSlaOperationProvider>();
        if (operationProvider != null)
        {
            return operationProvider.IpSlaOperation;
        }

        INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
        if (nodeProvider != null)
        {
            return nodeProvider.Node;
        }

        string netObject = this.Request["NetObject"];

        if (String.IsNullOrEmpty(netObject))
            return null;

        NetObjectHelper helper;
        if (NetObjectHelper.TryParse(netObject, out helper) && (helper.IsNPMNode() || helper.IsOperation()))
            return NetObjectFactory.Create(netObject);
        
        return null;
    }

    public bool IsInternal 
    {
        get { return true; }
    }
}
