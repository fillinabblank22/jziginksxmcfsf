﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OperationDetails.ascx.cs" Inherits="Orion_Voip_Resources_Operations_OperationDetails" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagName="IncludeExtJs" TagPrefix="orion" %>
<%@ Register TagPrefix="voip" TagName="StatusIcon" Src="~/Orion/Voip/Controls/OperationStatusImage.ascx" %>
<%@ Register TagPrefix="orion" TagName="SmallStatusIcon" Src="~/Orion/Controls/SmallStatusIcon.ascx" %>
<%@ Register Src="~/Orion/Voip/Controls/EditOperation.ascx" TagPrefix="voip" TagName="EditOperation" %>	

<orion:resourceWrapper runat="server" ID="Wrapper">
<Content>
	
	<asp:FormView runat="server" ID="OperationDetailsFormView" >
	<ItemTemplate>
	<div style="height: 5px;">&nbsp;</div>
	
	<table class="NeedsZebraStripes" border="0" cellPadding="2" cellSpacing="0" width="100%">
	<%if (Profile.AllowNodeManagement) {%>
	<tr>
		<td class="Property" width="10">&nbsp;</td>
		<td class="PropertyHeader" valign="center"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_192 %></td>
		<td colspan="2" class="Property NodeManagementIcons">
			<div>
			    <a href="#" onclick="SW.IPSLA.EditOperation.edit(); return false;">
			        <img src="/Orion/Nodes/images/icons/icon_edit.gif" /> <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_193 %>
			    </a>
			</div>
		    <div>
		        <a href="#" onclick="SW.IPSLA.PerfStackRedirect.perfstack_redirect(); return false;">
		            <img src="/Orion/Voip/images/PerfStack/show-in-perfstack-color.svg" height="16" />  <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_274 %>
		        </a>
		    </div>
        </td>
	</tr>
	<%} %>
	<tr>
      <td class="Property" width="10">&nbsp;</td>
      <td class="PropertyHeader" vAlign="center"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_194 %></td>
      <td align="center" class="Property"><voip:StatusIcon ID="icon" OperationStatus='<%# Eval("OperationStatus") %>' runat="server" /></td>
      <td class="Property"><%# GetLocalizedProperty("OperationStatus", Eval("OperationStatus").ToString()) %>&nbsp;</td>
	</tr>
	<tr>
      <td class="Property" width="10">&nbsp;</td>
      <td class="PropertyHeader" vAlign="center"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_195 %></td>
	  <td class="Property">&nbsp;</td>
      <td class="Property"><%# Eval("OperationStatusMessage") %>&nbsp;</td>
	</tr>
	<tr>
	  <td class="Property" width="10">&nbsp;</td>
	  <td class="PropertyHeader"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_145%></td>
	  <td align="center" class="Property"><img src="<%# GetOperationImage() %>"/></td>
      <td class="Property"><%# GetLocalizedProperty("OperationType", Eval("OperationType").ToString()) %>&nbsp;</td>
	</tr>
	<tr>
	  <td class="Property" width="10">&nbsp;</td>
	  <td class="PropertyHeader"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_129 %></td>
	  <td class="Property">&nbsp;</td>
	  <td class="Property"><%# Eval("Description") %>&nbsp;</td>
	</tr>
	<tr>
	  <td class="Property" width="10">&nbsp;</td>
	  <td class="PropertyHeader"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_196 %></td>
	  <td align="center" class="Property"><orion:SmallStatusIcon ID="SourceSiteStatus" runat="server" StatusValue='<%# GetSiteStatus(Eval("SourceSite")) %>' />&nbsp;</td>
	  <td class="Property"><a href="<%# GetSiteLink(Eval("SourceSite")) %>" > <%# Eval("SourceSiteName")%> </a>
	  &nbsp;</td>
	</tr>
	<tr>
	  <td class="Property" width="10">&nbsp;</td>
	  <td class="PropertyHeader"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_197 %></td>
	  <td align="center" class="Property"><orion:SmallStatusIcon ID="TargetSiteStatus" runat="server" StatusValue='<%# GetSiteStatus(Eval("TargetSite")) %>' Visible='<%# Eval("TargetSite") != null %>' />&nbsp;</td>
	  <td class="Property">
	      <asp:PlaceHolder ID="TargetSite" runat="server" Visible='<%# Eval("TargetSite") != null %>' >
	          <a href="<%# GetSiteLink(Eval("TargetSite")) %>" >
	            <%# Eval("TargetSiteName") %> 
	          </a>
	      </asp:PlaceHolder>
	      <asp:PlaceHolder ID="TargetParm" runat="server" Visible='<%# Eval("TargetSite") == null && Eval("Target") != null %>' >
              <%# Eval("Target") %> 
	      </asp:PlaceHolder>
	      <asp:PlaceHolder ID="NoTargetSite" runat="server" Visible='<%# Eval("TargetSite") == null && Eval("Target") == null %>' >
	          <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_198 %>
	      </asp:PlaceHolder>
	  &nbsp;</td>
	</tr>
	<tr>
	  <td class="Property" width="10">&nbsp;</td>
	  <td class="PropertyHeader" nowrap><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_199 %></td>
	  <td class="Property">&nbsp;</td>
	  <td class="Property"><%# Eval("OperationNumber") %>&nbsp;</td>
	</tr>
	<tr>
	  <td class="Property" width="10">&nbsp;</td>
	  <td class="PropertyHeader" nowrap><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_200 %></td>
	  <td class="Property">&nbsp;</td>
	  <td class="Property"><%# EvaluateRecordTime(Eval("RecordTimeUtc"))%>&nbsp;</td>
	</tr>
	<tr>
	  <td class="Property" width="10">&nbsp;</td>
	  <td class="PropertyHeader"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_201 %></td>
	  <td class="Property">&nbsp;</td>
	  <td class="Property"><%# Eval("Frequency") %> <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_202 %>&nbsp;</td>
	</tr>
	<tr>
	  <td class="Property" width="10">&nbsp;</td>
	  <td class="PropertyHeader"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_203 %></td>
	  <td class="Property">&nbsp;</td>
	  <td class="Property"><%# EncodeLines(Eval("Parameters")) %>&nbsp;</td>
	</tr>
	</table>
</ItemTemplate>
</asp:FormView>

    <orion:IncludeExtJs ID="IncludeExtJs" runat="server" Version="3.4"/>
	<orion:Include File="OrionCore.js" runat="server" />
	<style type="text/css">
        #editDialog td { padding-right: 0; padding-bottom: 0; border-style: none; }
        #editDialog input, #editDialog textarea, #editDialog select { margin: 0 5px 0 0; }
        #editDialog .x-btn-center
        {
            padding: 0 5px;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;     
        }
        #editDialog .PropertyHeader { white-space: nowrap; padding: 0 5px; }
        #editDialog table { width: auto; }
        #editDialog tr { border: 0; margin-top: 0; margin-bottom: 0; }
        #editDialog .main td { white-space: nowrap; padding: 0 5px; padding-top: 3px; }
        #editDialog .hint td { padding: 0 5px; color: Gray; }
        #editDialog .validators td { padding: 0 15px; }
        
        #editDialog .x-window-body { padding: 5px; background-color: #FFF !important; }
        .x-panel-btns-ct table { width: auto; }
    </style>

    <script type="text/javascript">
Ext.namespace('SW');
Ext.namespace('SW.IPSLA');

SW.IPSLA.PerfStackRedirect =  {
    perfstack_redirect: function() {
        var operationInstanceID = '<%= this.OperationInstanceID %>';
        var perfStackRedirectLink = "/ui/perfstack/?context=0_Orion.IpSla.Operations_{IPSLA.ID}&withRelationships=true&presetTime=last12Hours&charts=" +
            "0_Orion.IpSla.Operations_{IPSLA.ID}-Orion.IpSla.IcmpPathJitterOperationStats.AvgRoundTripTime;" +
            "0_Orion.IpSla.Operations_{IPSLA.ID}-Orion.IpSla.IcmpPathJitterOperationStats.AvgJitter;" +
            "0_Orion.IpSla.Operations_{IPSLA.ID}-Orion.IpSla.IcmpPathJitterOperationStats.AvgLatency;" +
            "0_Orion.IpSla.Operations_{IPSLA.ID}-Orion.IpSla.IcmpPathJitterOperationStats.AvgPacketLoss;" +
            "0_Orion.IpSla.Operations_{IPSLA.ID}-Orion.IpSla.NonPathOperationStats.AvgRoundTripTime;" +
            "0_Orion.IpSla.Operations_{IPSLA.ID}-Orion.IpSla.UdpJitterOperationStats.AvgRoundTripTime;" +
            "0_Orion.IpSla.Operations_{IPSLA.ID}-Orion.IpSla.UdpJitterOperationStats.AvgMOS;" +
            "0_Orion.IpSla.Operations_{IPSLA.ID}-Orion.IpSla.UdpJitterOperationStats.AvgJitter;" +
            "0_Orion.IpSla.Operations_{IPSLA.ID}-Orion.IpSla.UdpJitterOperationStats.AvgLatency;" +
            "0_Orion.IpSla.Operations_{IPSLA.ID}-Orion.IpSla.UdpJitterOperationStats.AvgPacketLoss;" +
            "0_Orion.IpSla.Operations_{IPSLA.ID}-Orion.IpSla.NonMOSUdpJitterOperationStats.AvgRoundTripTime;" +
            "0_Orion.IpSla.Operations_{IPSLA.ID}-Orion.IpSla.NonMOSUdpJitterOperationStats.AvgJitter;" +
            "0_Orion.IpSla.Operations_{IPSLA.ID}-Orion.IpSla.NonMOSUdpJitterOperationStats.AvgLatency;" +
            "0_Orion.IpSla.Operations_{IPSLA.ID}-Orion.IpSla.NonMOSUdpJitterOperationStats.AvgPacketLoss;" +
            "0_Orion.IpSla.Operations_{IPSLA.ID}-Orion.PerfStack.Events;" +
            "0_Orion.IpSla.Operations_{IPSLA.ID}-Orion.PerfStack.Alerts;";
        window.location = perfStackRedirectLink.replace(/{IPSLA.ID}/g, operationInstanceID);
    }
};

SW.IPSLA.EditOperation = function() {
    var editDialog;
    var waitBeforeSave = false;
    var EditorLoadButtonID = '<%= this.EditorLoadButton.ClientID %>';
    var EditorCancelButtonID = '<%= this.EditorCancelButton.ClientID %>';
    var EditOperationID = '<%= this.editOp.ClientID %>';
    var EditorSubmitButtonID = '<%= this.EditorSubmitButton.ClientID %>';

    function CancelEdit() {
        setTimeout(function() { $('#' + EditorCancelButtonID).click(); }, 0);
        editDialog.hide();
    }

    return {
        init: function() {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function(sender, args) {
                if (waitBeforeSave) {
                    // waiting for edit to complete update - close edit dialog and refresh
                    waitBeforeSave = false;
                    var dataItems = args.get_dataItems()[EditOperationID];
                    var isValid = (!dataItems || dataItems['IsValid'] != false);
                    if (Page_IsValid && isValid) {
                        editDialog.hide();
                        window.location.reload(true);
                    }
                }
            });
        },
        edit: function() {
            $('#' + EditorLoadButtonID).click();

            if (!editDialog) {
                editDialog = new Ext.Window({
                    applyTo: 'editDialog',
                    layout: 'fit',
                    width: 730,
                    height: 460,
                    closeAction: 'hide',
                    closable: false,
                    onEsc: CancelEdit,
                    plain: true,
                    resizable: false,
                    modal: true,
                    autoScroll: true,
                    buttons: [{
                        text: '<%= ControlHelper.EncodeJsString(Resources.VNQMWebContent.VNQM_WEB_JS_DATA_VB1_1)%>',
                        handler: function() {
                            if (CheckDemoMode()) return;
                            waitBeforeSave = true;
                            setTimeout(function() { $('#' + EditorSubmitButtonID).click(); }, 0);
                        }
                    },
                    {
                        text: '<%= ControlHelper.EncodeJsString(Resources.VNQMWebContent.VNQM_WEB_JS_DATA_VB1_2)%>',
                        handler: CancelEdit
                    }
    ]
                });
            }

            editDialog.alignTo(document.body, "c-c");
            editDialog.show();
        }
    };
} ();
Ext.onReady(SW.IPSLA.EditOperation.init, SW.IPSLA.EditOperation);

    </script>

    <div id="editDialog" class="x-hidden">
        <div class="x-window-header"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_204 %></div>
        
        <div class="x-window-body">
        <asp:UpdatePanel ID="upEditor" runat="server">
            <ContentTemplate>
                <asp:PlaceHolder runat="server" ID="editDialogPanel" Visible="false" >
                    <voip:EditOperation runat="server" ID="editOp" />
                    <br />
                </asp:PlaceHolder>            
                <asp:PlaceHolder runat="server" ID="editDialogLoading" Visible="true" >
                    <table style="width:100%; height:300px;"><tr><td style="text-align:center; vertical-align:middle;"> 
                        <img src="/Orion/Voip/images/loading_gen_16x16.gif" alt="<%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_44 %>" style="padding-right:10px;" />
                        <%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_44 %>
                    </td></tr></table>
                </asp:PlaceHolder>        
                <asp:Button ID="EditorLoadButton" runat="server" Text="Load" OnClick="EditorLoadButton_Click" CssClass="x-hidden" CausesValidation="false" />
                <asp:Button ID="EditorCancelButton" runat="server" Text="Cancel" OnClick="EditorCancelButton_Click" CssClass="x-hidden" CausesValidation="false" />
                <asp:Button ID="EditorSubmitButton" runat="server" Text="Submit" OnClick="EditorSubmitButton_Click" CssClass="x-hidden" />
            </ContentTemplate>
        </asp:UpdatePanel>
        </div>
    </div>

</Content>
</orion:resourceWrapper>

