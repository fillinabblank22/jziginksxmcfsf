﻿using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.IpSla.Web;
using System.Collections.Generic;

public partial class Orion_Voip_Resources_Operations_OperationsEvents : VoipBaseResource
{
    private int _maxEventCount = 25;
    private const string MaxEvents = "MaxEvents";

    private IpSlaOperation Operation
    {
        get
        {
            var provider = GetInterfaceInstance<IIpSlaOperationProvider>();
            return provider?.IpSlaOperation;
        }
    }

    public override string DisplayTitle => Title.Replace("XX", _maxEventCount.ToString());

    protected override string DefaultTitle => Resources.VNQMWebContent.VNQMWEBCODE_VB1_245;

    public override string HelpLinkFragment => "OrionIPSLAManagerPHResourceLast25Events";

    public override string EditURL
    {
        get
        {
            string url = $"/Orion/Voip/Resources/Operations/EditOperationEvents.aspx?ResourceID={this.Resource.ID}";
            if (this.Operation != null)
            {
                url = $"{url}&NetObject={Operation.NetObjectID}";
            }
            if (Page is OrionView)
                url = $"{url}&ViewID={((OrionView) Page).ViewInfo.ViewID}";

            return url;
        }
    }

    public override IEnumerable<Type> RequiredInterfaces => new Type[] { typeof(IIpSlaOperationProvider) };

    protected void EventsControl_Init(object sender, EventArgs e)
    {
        _maxEventCount = GetIntProperty(MaxEvents, _maxEventCount);

	    EventsControl.LoadData(VoipEvent.GetOperationEvents(Operation.OperationInstanceID, _maxEventCount));
    }
}
