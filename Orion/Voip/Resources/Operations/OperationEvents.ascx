<%@ Control Language="C#" CodeFile="OperationEvents.ascx.cs"  AutoEventWireup="true" Inherits="Orion_Voip_Resources_Operations_OperationsEvents"  %>
<%@ Register Src="~/Orion/Controls/EventsReportControl.ascx" TagName="Events" TagPrefix="orion" %>


<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
		<orion:Events runat="server" ID="EventsControl" OnInit="EventsControl_Init" />
	</Content>
</orion:resourceWrapper>
