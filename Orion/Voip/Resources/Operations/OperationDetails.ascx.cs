﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using Resources;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Data.OperationParameters;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.UI;
using Site = SolarWinds.Orion.IpSla.Web.Site;

public partial class Orion_Voip_Resources_Operations_OperationDetails : VoipBaseResource
{
    private IpSlaOperation _operation;
    protected override bool DefaultLoadingMode => true;
    protected int OperationInstanceID => _operation.OperationInstanceID;

    protected override string DefaultTitle => VNQMWebContent.VNQMWEBCODE_VB1_243;

    public override string HelpLinkFragment => "OrionIPSLAManagerPHResourceOperationDetails";

    protected string ReturnUrl => ReferrerRedirectorBase.GetReturnUrl();

    public override IEnumerable<Type> RequiredInterfaces => new[] { typeof(IIpSlaOperationProvider) };

    IpSlaOperation CreateOperation()
    {
        string netObjectID = Request["NetObject"];
        if (!string.IsNullOrEmpty(netObjectID))
        {
            IpSlaOperation operation = new IpSlaOperation(netObjectID);
            return operation;
        }
        return GetInterfaceInstance<IIpSlaOperationProvider>().IpSlaOperation;
    }

    private string IsEditorVisibleHiddenFieldName => this.ClientID + "::IsEditorVisible";

    protected bool IsEditorVisible { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        bool isEditorVisiblePostData;
        IsEditorVisible = bool.TryParse(Page.Request.Form[IsEditorVisibleHiddenFieldName], out isEditorVisiblePostData) && isEditorVisiblePostData;

        ShowEditor(IsEditorVisible);

        _operation = CreateOperation();

        object[] datasource = new object[] { _operation };
        this.OperationDetailsFormView.DataSource = datasource;
        this.OperationDetailsFormView.DataBind();
    }

    private bool renderDemoDiv;

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        ScriptManager.RegisterHiddenField(this.editOp, IsEditorVisibleHiddenFieldName, IsEditorVisible.ToString());

        ScriptManager.RegisterClientScriptInclude(this, typeof(Orion_Voip_Resources_Operations_OperationDetails), "DemoLimitationFunctionality", "/Orion/Voip/js/Admin.js");
        if (!Page.ClientScript.IsClientScriptBlockRegistered("IPSLA-DemoMarkerBlock"))
        {
            this.renderDemoDiv = OrionConfiguration.IsDemoServer;
            Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "IPSLA-DemoMarkerBlock", string.Empty, false);
        }
    }

    public override void RenderControl(HtmlTextWriter writer)
    {
        base.RenderControl(writer);
        if (this.renderDemoDiv)
        {
            writer.AddAttribute("id", "isDemoMode");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.RenderEndTag();
        }
    }

    protected string EvaluateRecordTime(object runTimeUtc)
    {
        if (runTimeUtc is DateTime?)
        {
            DateTime? time = runTimeUtc as DateTime?;
            if (time.HasValue)
            {
                return time.Value.ToLocalTime().ToString("F");
            }
        }

        return VNQMWebContent.VNQMWEBCODE_VB1_244;
    }

    protected string GetOperationImage()
    {
        if(_operation != null)
            return IpSlaOperationHelper.GetTypeImageUrl(_operation.OperationType, false);

        return string.Empty;
    }

    protected string GetSiteLink(object site)
    {
        Site ipslaSite = site as Site;

        if (ipslaSite != null)
        {
            return GetViewLink("N:" + ipslaSite.VoipSite.NodeID);
        }

        return string.Empty;
    }

    protected Status GetSiteStatus(object site)
    {
        Site ipslaSite = site as Site;

        if (site != null)
        {
            return ipslaSite.VoipSite.NodeStatus;
        }

        return new Status(OBJECT_STATUS.Unknown);
    }

    protected string EncodeLines(object obj)
    {
        string str = obj.ToString();
        StringBuilder builder = new StringBuilder();

        string[] lines = str.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
        for (int i = 0; i < lines.Length; i++)
        {
            //skip parameters with empty values
            string append = lines[i];
            if (append.Remove(0, append.IndexOf(':') + 1).Trim().Length == 0)
                continue;

            builder.Append(HttpUtility.HtmlEncode(append));
            if (i < lines.Length - 1)
                builder.Append("<br>");
        }

        return builder.ToString();
    }

    public override string DetachURL => string.Format("DetachResource.aspx?ResourceID={0}&NetObject={1}", this.Resource.ID, Request["NetObject"]);

    protected void EditorLoadButton_Click(object sender, EventArgs e)
    {
        this.DisplayEditor(true);

        LoadEditor(false);
    }

    protected void EditorCancelButton_Click(object sender, EventArgs e)
    {
        this.DisplayEditor(false);
    }

    protected void EditorSubmitButton_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
        {
            LoadEditor(true);
            return;
        }

        this.DisplayEditor(false);

        int operationInstanceId = this.GetOperationID();
        using (var blProxy = BusinessLayer.GetBusinessLayer())
        {
            List<OperationInstance> operationInstances = blProxy.GetOperationInstances(new int[] { operationInstanceId });
            var operations = new OperationDefinitionCollection(operationInstances.Select(oi => oi.Definition));
            this.editOp.Initialize(operations, true);
            this.editOp.Modify(operations);
            blProxy.UpdateOperationInstances(operationInstances);
            ThresholdManager.ClearThresholdCacheForOperation(operationInstanceId);
        }
    }

    private void LoadEditor(bool doNotLoadData)
    {
        int operationInstanceId = this.GetOperationID();

        List<OperationInstance> operationInstances;
        using (var blProxy = BusinessLayer.GetBusinessLayer())
        {
            operationInstances = blProxy.GetOperationInstances(new int[] { operationInstanceId });
        }
        OperationDefinitionCollection operations = new OperationDefinitionCollection(operationInstances.Select(oi => oi.Definition));
        this.editOp.Initialize(operations, doNotLoadData);
    }

    private void DisplayEditor(bool show)
    {
        ShowEditor(show);
        this.IsEditorVisible = show;
    }

    private void ShowEditor(bool show)
    {
        this.editDialogPanel.Visible = show;
        this.editDialogLoading.Visible = !show;
    }

    private int GetOperationID()
    {
        string netObjectId = this.Request["NetObject"];
        string[] netObjectParts = netObjectId.Split(':');
        int operationInstanceId;
        if (netObjectParts[0] != IpSlaOperation.Prefix || netObjectParts.Length != 2 || !int.TryParse(netObjectParts[1], out operationInstanceId))
            throw new ArgumentException("netObjectId was not in the proper format for IpSlaOperation", "netObjectId");

        return operationInstanceId;
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}
