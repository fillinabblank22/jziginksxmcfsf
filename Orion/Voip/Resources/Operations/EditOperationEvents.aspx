<%@ Page Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true" CodeFile="EditOperationEvents.aspx.cs" Inherits="EditOperationEvents" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1 style="padding-left: 0px;">
        <%= string.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_216, this.Resource.Title)%>
    </h1>
    <b><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_206 %></b><br/>
    <asp:TextBox runat="server" ID="tbMaxEvents" Text="25"/>
       <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="tbMaxEvents" SetFocusOnError="true">*</asp:RequiredFieldValidator>
       <asp:RangeValidator ID="integerValueValidate" runat="server" ControlToValidate="tbMaxEvents"
                                Display="Dynamic" Type="Integer" MaximumValue="10000" MinimumValue="1"
                                ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_207 %>"
                                SetFocusOnError="true"></asp:RangeValidator>
    <br />
    <br />
    <orion:LocalizableButton runat="server" ID="btnSubmit" CausesValidation="true" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick" />
</asp:Content>
