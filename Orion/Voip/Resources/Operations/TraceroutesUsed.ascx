﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TraceroutesUsed.ascx.cs" Inherits="Orion_Voip_Resources_Operations_TraceroutesUsed" %>
<%@ Register TagPrefix="ipsla" Namespace="SolarWinds.Orion.IpSla.Web.UI.Grid" Assembly="SolarWinds.Orion.IpSla.Web" %>
<%@ Register TagPrefix="ipsla" TagName="Traceroute" Src="~/Orion/Voip/Controls/Traceroute.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
<Content>

	<div style="height: 5px;">&nbsp;</div>

		<ipsla:ExpandableGrid ID="ExpandableGrid" runat="server" 
            HeaderCssClass="ReportHeader"
            HeaderCssStyle="padding:5px 2px 5px 0px;"
            ItemCssClass="Property"
            ItemCssStyle="padding:2px 2px 8px 0px;border:0px;vertical-align:middle;"
            ExpandableCssStyle="padding:8px 8px 8px 0px;border:0px;"
            OnExpandableDataBound="Grid_ExpandableDataBound">
        <Columns>
            <ipsla:Column Caption="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_208 %>"
                    HeaderCssStyle="padding-left:0px;"
                    ItemCssStyle="padding-left:0px;">
                <Template>
                    <div>
                        <%# GetRecordTime(Container.DataItem) %>
                    </div>
                </Template>
            </ipsla:Column>
            <ipsla:Column Caption="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_209 %>">
                <Template>
                    <div>
                        <%# GetNumberOfHops(Container.DataItem) %>
                    </div>
                </Template>
            </ipsla:Column>
            <ipsla:Column Caption="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_210 %>">
                <Template>
                    <div>
                        <%# GetUsagePercentage(Container.DataItem) %>
                    </div>
                </Template>
            </ipsla:Column>
        </Columns>
        <Expandable>
            <ipsla:Traceroute ID="Traceroute" runat="server" DisableHopHyperlinks="true" />
        </Expandable>
    </ipsla:ExpandableGrid>

</Content>
</orion:resourceWrapper>

