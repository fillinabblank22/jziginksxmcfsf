
using System;
using SolarWinds.Orion.IpSla.Web.UI;

public partial class EditOperationEvents : ResourceEditPage
{
    protected override void OnLoad(EventArgs e)
    {
        if (!this.IsPostBack)
        {
            string howMany = this.Resource.Properties["MaxEvents"];
            if (string.IsNullOrEmpty(howMany))
                howMany = "25";

            this.tbMaxEvents.Text = howMany;
        }

        base.OnLoad(e);
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        this.Resource.Properties["MaxEvents"] = this.tbMaxEvents.Text;

        GoBack();
    }
}
