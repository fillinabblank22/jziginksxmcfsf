<%@ Control Language="C#" ClassName="SitesOverviewBody" %>
<%@ Register TagPrefix="voip" TagName="StatusIcon" Src="~/Orion/Voip/Controls/OperationStatusImage.ascx" %>
<%@ Register TagPrefix="voip" TagName="Metric" Src="~/Orion/Voip/Controls/Metric.ascx" %>
<%@ Register TagPrefix="voip" TagName="MOSBar" Src="~/Orion/Voip/Controls/MOSBar.ascx" %>


<asp:Repeater runat="server" ID="CallPathsRepeater">
	<ItemTemplate>
		<tr class="SitesOverview_CallPath">
			<td> </td>
			<td colspan="2">
				&nbsp;&nbsp;&nbsp;&nbsp;
				<a href='<%#BaseResourceControl.GetViewLink((string)Eval("NetObjectID"))%>'>
				    <voip:StatusIcon ID="SmallStatusIcon1" runat="server" OperationStatus='<%#Eval("Status") %>' StatusMessage='<%#Eval("StatusMessage") %>' /><%#Eval("Name")%>
				</a>
			</td>
			
			<asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible='<%#(bool)Eval("HasData")&&(!(bool)Eval("IsCallPathDown"))%>'>			
				<td><voip:Metric runat="server" Value='<%#Eval("MOS")%>' /></td>
				<voip:MOSBar runat="server" Value='<%#Eval("MOS")%>' />
				<td>&nbsp;</td>
				<td><voip:Metric runat="server" Value='<%#Eval("Jitter")%>' /></td>
				<td><voip:Metric runat="server" Value='<%#Eval("Latency")%>' /></td>
				<td><voip:Metric runat="server" Value='<%#Eval("PacketLoss")%>' /></td>
			</asp:PlaceHolder>

			<asp:PlaceHolder ID="PlaceHolder2" runat="server" Visible='<%#(!(bool)Eval("HasData"))||(bool)Eval("IsCallPathDown")%>'>
				<td colspan="6"><%#Eval("StatusMessage") %></td>
			</asp:PlaceHolder>
		</tr>
	</ItemTemplate>
</asp:Repeater>
