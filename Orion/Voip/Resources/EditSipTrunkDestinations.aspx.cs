using System;
using SolarWinds.Orion.IpSla.Web.UI;

public partial class Orion_Voip_Resources_EditSipTrunkDestinations : ResourceEditPage
{

    protected override void Initialize()
    {
        this.ResourceTitleEditor.ResourceTitle = this.Resource.Title;
        
        if (!this.IsPostBack)
        {
            string howMany = this.Resource.Properties["SipTrunkDestinationsPageSize"];
            if (string.IsNullOrEmpty(howMany))
                howMany = "5";

            this.SipTrunkDestinationsPageSize.Text = howMany;
        }
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (!ResourceTitleEditor.ResourceTitle.Equals(Resource.Title))
        {
            Resource.Title = ResourceTitleEditor.ResourceTitle;
            SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
        }

        this.Resource.Properties["SipTrunkDestinationsPageSize"] = this.SipTrunkDestinationsPageSize.Text;

        GoBack();
    }
}
