﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SipTrunkAvailability.ascx.cs" Inherits="Orion.Voip.Resources.SipTrunkChartsXui.OrionVoipResourcesSipTrunkChartsXuiSipTrunkAvailability" %>
<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true" CssClass="XuiResourceWrapper">
    <Content>
        <div class="xui">
            <<%=Selector%>></<%=Selector%>>
        </div>
    </Content>
</orion:ResourceWrapper>