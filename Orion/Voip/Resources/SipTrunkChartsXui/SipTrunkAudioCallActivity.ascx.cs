﻿using System;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

namespace Orion.Voip.Resources.SipTrunkChartsXui
{
    [ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.VOIP)]
    public partial class OrionVoipResourcesSipTrunkChartsXuiSipTrunkAudioCallActivity : BaseResourceControl, IXuiResource, IResourceIsInternal
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ResourceWrapper.Visible = OrionMinReqsMaster.IsApolloEnabled;
            Selector = GetStringValue("selector", null);
        }
        protected string Selector { get; set; }

        protected override string DefaultTitle => "VoIP Call Manager SIP Trunk Audio Call Activity";

        public bool IsInternal => true;

        public override string HelpLinkFragment => "OrionIPSLAManagerAdministratorGuide_AddingCallManagersToVNQM";
    }
}