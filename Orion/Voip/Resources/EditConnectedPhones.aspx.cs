using System;
using SolarWinds.Orion.IpSla.Web.UI;

public partial class Orion_Voip_Resources_EditConnectedPhones : ResourceEditPage
{

    protected override void Initialize()
    {
        this.resourceTitleEditor.ResourceTitle = this.Resource.Title;
        
        if (!this.IsPostBack)
        {
            string howMany = this.Resource.Properties["ConnectedPhonesPageSize"];
            if (string.IsNullOrEmpty(howMany))
                howMany = "20";

            this.ConnectedPhonesPageSize.Text = howMany;
        }
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (!resourceTitleEditor.ResourceTitle.Equals(Resource.Title))
        {
            Resource.Title = resourceTitleEditor.ResourceTitle;
            SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
        }

        this.Resource.Properties["ConnectedPhonesPageSize"] = this.ConnectedPhonesPageSize.Text;

        GoBack();
    }
}
