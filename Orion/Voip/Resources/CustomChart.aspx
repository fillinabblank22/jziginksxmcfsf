<%@ Page Language="C#" MasterPageFile="~/Orion/Voip/VoipView.master" AutoEventWireup="true" 
    CodeFile="CustomChart.aspx.cs" Inherits="Orion_Voip_Resources_CustomChart" Title="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_57%>" %>
<%@ Register TagPrefix="voip" TagName="CustomChartControl" Src="~/Orion/Voip/Controls/InternalCustomChart.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="VoipPageTitle" Runat="Server">
    <h1 runat="server" id="PageHeader"></h1>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="VoipMainContentPlaceHolder" Runat="Server">
    <voip:CustomChartControl runat="server" ID="CustomChart" EnableEditing="true"/>
</asp:Content>

