using System;
using System.Web.UI;
using Resources;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Voip_Resources_CustomChart : Page
{
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        PageHeader.InnerHtml = Page.Title;
        if (!Page.IsPostBack)
        {
            PageHeader.InnerHtml = GeneratePageTitle();
        }
    }

    private string GeneratePageTitle()
    {
        string title;

        NetObject netObject = CustomChart.ChartInfo.NetObject;

        if (netObject is CallPath)
        {
            CallPath cp = (CallPath)netObject;

            title = String.Format(VNQMWebContent.VNQMWEBDATA_TM0_58,
                            NetObjectLink(cp.SourceSite),
                            NetObjectLink(cp.DestSite));
        }
        else if (netObject is CallManager)
        {
            title = String.Format(VNQMWebContent.VNQMWEBDATA_TM0_59, NetObjectLink(netObject));
        }
        else
        {
            title = VNQMWebContent.VNQMWEBDATA_TM0_57;
        }

        return title;
    }

    private static string NetObjectLink(NetObject netObject)
    {
        return String.Format("<a href='{0}'>{1}</a>", BaseResourceControl.GetViewLink(netObject.NetObjectID), netObject.Name);
    }
}

