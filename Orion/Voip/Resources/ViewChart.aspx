<%@ Page Language="C#" %>
<%@ Register TagPrefix="voip" TagName="CustomChartControl" Src="~/Orion/Voip/Controls/InternalCustomChart.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title><%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_56 %></title>
	<link rel="stylesheet" type="text/css" href="/Orion/Voip/Voip.css" />
</head>

<body>
    <form id="form1" runat="server">
    <div>
        <voip:CustomChartControl runat="server" ID="CustomChart" EnableEditing="false"/>    
    </div>
    </form>
</body>
</html>
