using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_Resources_TimeWindows : System.Web.UI.UserControl
{
	private int _viewID;

	public int ViewID
	{
		get { return _viewID; }
		set { _viewID = value; }
	}


    protected ScriptManager GetScriptManager()
    {
        ScriptManager ret = ScriptManager.GetCurrent(Page);

        if (ret == null)
            throw new HttpException("A ScriptManager control must exist on the current page.");

        return ret;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        GetScriptManager().RegisterAsyncPostBackControl(this);
        periodSelector.Period = Request.Params["Period"] ?? Request.Params["LocalizedPeriod"] ?? "";
    }

    protected void Submit_Click(object sender, EventArgs e)
    {
        if (PeriodValidator.IsValid)
            Response.Redirect(GetURL());
    }

    protected void PeriodValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {
            DateTime periodBegin = new DateTime(), periodEnd = new DateTime();
            string period = this.periodSelector.Period;
            Periods.Parse(ref period, ref periodBegin, ref periodEnd);
            args.IsValid = true;
        }
        catch (FormatException)
        {
            args.IsValid = false;
        }
    }

    private string GetURL()
    {
        return string.Format("{0}?NetObject={1}&Period={2}&LocalizedPeriod={3}",
                             Request.Url.AbsolutePath,
                             Request.Params["NetObject"],
                             Server.UrlPathEncode(this.periodSelector.Period),
                             Server.UrlPathEncode(this.periodSelector.LocalizedPeriod));
            
    }
}
