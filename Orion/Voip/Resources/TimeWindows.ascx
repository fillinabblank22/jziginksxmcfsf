<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TimeWindows.ascx.cs" Inherits="Orion_Voip_Resources_TimeWindows" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="System.Web.Security.AntiXss" %>
<%@ Register TagPrefix="voip" TagName="PeriodSelector" Src="~/Orion/Voip/Controls/PeriodSelector.ascx" %>

<script type="text/javascript">
	var ViewID = "<%=ViewID %>";
</script>

<h2>
    <a href="#" onclick="ShowPeriodMenu(); return false;">
        <img id="PeriodDropDown" alt="<%= VNQMWebContent.VNQMWEBDATA_TM0_96 %>" src="/Orion/images/Button.DropDown.gif" />
    </a>
    <span id="PeriodTitle"><%= AntiXssEncoder.HtmlEncode( Request.Params["LocalizedPeriod"],true) ?? AntiXssEncoder.HtmlEncode(Request.Params["Period"],true) ?? VNQMWebContent.VNQMWEBDATA_TM0_94 %></span>
</h2>

<div id="PeriodMenu"  style="display:none">


    <voip:PeriodSelector runat="server" ID="periodSelector" />
    <asp:UpdatePanel ID="PerioValidatorUpdatePanel" runat="server">
        <ContentTemplate>
 	        <asp:CustomValidator ID="PeriodValidator" runat="server" ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_95 %>"
                    OnServerValidate="PeriodValidator_ServerValidate" CssClass="PeriodValidator"></asp:CustomValidator>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <div class="sw-btn-bar"> 
        <orion:LocalizableButton runat="server" ID="btnSubmit"  DisplayType="Primary" 
                                 LocalizedText="Submit"
                                 OnClick="Submit_Click" />
        <orion:LocalizableButton runat="server" ID="btnCancel"  DisplayType="Secondary" 
                                 LocalizedText="Cancel"
                                 OnClientClick="HidePeriodMenu(); return false;" />
    </div>
</div>

