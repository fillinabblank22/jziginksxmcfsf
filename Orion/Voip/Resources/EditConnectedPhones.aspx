<%@ Page Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true" CodeFile="EditConnectedPhones.aspx.cs" Inherits="Orion_Voip_Resources_EditConnectedPhones" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
   <h1 style="padding-left: 0px"><%= Page.Title %></h1>
   
   <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
   
   <b><orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="false" /></b>
   
   <b><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_135 %></b>
   <br /> 
   <asp:TextBox runat="server" ID="ConnectedPhonesPageSize"  />
   
   <asp:RangeValidator ID="RangeValidator2" runat="server"
    ControlToValidate="ConnectedPhonesPageSize"
    ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_136 %>"
    MaximumValue="500"
	MinimumValue="10"
	Type="Integer">*</asp:RangeValidator>

   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
    ControlToValidate="ConnectedPhonesPageSize"
    ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_137 %>">*</asp:RequiredFieldValidator>
   <br />
   <br />
    <orion:LocalizableButton runat="server" ID="btnSubmit" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary" ToolTip="<%$ Resources : VNQMWebContent, VNQMWEBCODE_VB1_171 %>"/>    
</asp:Content>
