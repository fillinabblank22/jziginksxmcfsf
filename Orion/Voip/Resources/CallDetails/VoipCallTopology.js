﻿$(document).ready(function () {

    $(".voipTopologyConfCallClick").click(function () {
        var element = this;
        if (typeof (element.ShowConfCallDetailsDialog) === 'undefined') {
            var CallOriginDivID = $(element).attr("CallOriginDivID");

            element.ShowConfCallDetailsDialog = new Ext.Window({
                title: '@{R=VNQM.Strings;K=VNQMWEBJS_VB1_103;E=js}',
                layout: 'fit',
                closeAction: 'hide',
                closable: true,
                width: 300,
                modal: true,
                applyTo: CallOriginDivID,
                buttons: [{
                    text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_63;E=js}',
                    handler: function () {
                        element.ShowConfCallDetailsDialog.hide();
                    }
                }]
            });
        }
        element.ShowConfCallDetailsDialog.show(this);
    });
});