﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VoipPathDetails.ascx.cs"
    Inherits="Orion_Voip_Resources_CallDetails_VoipPathDetails" %>
    <style type="text/css">
        .voipPathDetailsTableRow {text-align: left}
    </style>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <table border="0" cellpadding="2" cellspacing="0" width="100%" runat="server" id="voipPathDetailsTable">
            <tr class="voipCallDetailsTableRow voipPathDetailsTableRow">
                <td class="voipCallDetailsHeaderText voipCallDetailsTableHeader" style="width: 25px">
                    <img src="" alt="" id="imgCallOrigin" runat="server" />
                </td>
                <td class="voipCallDetailsHeaderText voipCallDetailsTableHeader" style="width: 115px">
                    <%= CallOriginLabel %>
                </td>
                <td class="voipCallDetailsHeaderText voipCallDetailsTableHeader" style="width: 125px">
                    <asp:Label runat="server" ID="lblOrigPhoneNumber"></asp:Label>
                    <asp:HyperLink runat="server" ID="hlOrigPhoneNumber"></asp:HyperLink>
                </td>
                <td class="voipCallDetailsHeaderText voipCallDetailsTableHeader" style="width: 220px">
                    <asp:HyperLink runat="server" ID="hlOrigRegion" Visible="False"></asp:HyperLink>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow voipPathDetailsTableRow">
                <td class="voipCallDetailsHeaderText voipCallDetailsTableHeader" style="width: 25px">
                    <img src="/Orion/Voip/images/CallIcons/CCM16x16.png" alt="" id="imgCallManager" runat="server" />
                </td>
                <td class="voipCallDetailsHeaderText voipCallDetailsTableHeader" style="width: 115px">
                    <%= CallManagerLabel %>
                </td>
                <td class="voipCallDetailsHeaderText voipCallDetailsTableHeader" colspan="2">
                    <asp:Label runat="server" ID="lblCallManager"></asp:Label>
                    <asp:HyperLink runat="server" ID="hlCallManager"></asp:HyperLink>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow voipPathDetailsTableRow">
                <td class="voipCallDetailsHeaderText voipPathDetailsSlaOperationRow" style="width: 25px">
                    <img src="" alt="" id="imgSlaOperation" runat="server" />
                </td>
                <td class="voipCallDetailsHeaderText voipPathDetailsSlaOperationRow" style="width: 115px">
                    <%= SlaOperationPath %>
                </td>
                <td class="voipCallDetailsHeaderText voipPathDetailsSlaOperationRow" colspan="2">
                    <asp:Label runat="server" ID="lblSlaOperation"></asp:Label>
                    <asp:HyperLink runat="server" ID="hlSlaOperation"></asp:HyperLink>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow voipPathDetailsTableRow">
                <td colspan="4" class="voipCallDetailsRowText voipPathDetailsSlaOperationRow">
                    <table cellspacing="0">
                            <tr>
                                <td style="width: 25px" class="NoBorder">
                                    <%= HopLabel %>
                                </td>
                                <td style="width: 25px" class="NoBorder">
                                    &nbsp;
                                </td>
                                <td style="width: 210px" class="NoBorder">
                                    <%= IpAddressLabel %>
                                </td>
                                <td style="width: 75px" class="NoBorder">
                                    <%= RttLabel %>
                                </td>
                                <td style="width: 75px" class="NoBorder">
                                    <%= JitterLabel%>
                                </td>
                                <td style="text-align: center" class="NoBorder">
                                    <%= LatencyLabel %>
                                </td>
                            </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="background-color: white">
                    <asp:Repeater runat="server" ID="PathDetailsRepearter" OnItemDataBound="PathDetailsRepeater_DataBound">
                        <HeaderTemplate>
                            <table cellspacing="0">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr id="trPathDetailRow" runat="server">
                                <td class="NoBorder" style="width: 25px; padding-left: 5px;">
                                    <%#DataBinder.Eval(Container.DataItem,"Hop")%>
                                </td>
                                <td class="NoBorder" style="width: 25px">
                                    <asp:Image ID="imgStatus" runat="server" ImageUrl='<%#DataBinder.Eval(Container.DataItem,"ImageLocation")%>' />
                                </td>
                                <td class="NoBorder" style="width: 210px">
                                    <asp:HyperLink runat="server" ID="hlHop" Text='<%#DataBinder.Eval(Container.DataItem,"IpAddress")%>'></asp:HyperLink>
                                </td>
                                <td class="NoBorder" style="width: 75px;">
                                    <%#DataBinder.Eval(Container.DataItem,"RTT")%>
                                </td>
                                <td class="NoBorder" style="width: 75px;">
                                    <%#DataBinder.Eval(Container.DataItem,"Jitter")%>
                                </td>
                                <td class="NoBorder" style="text-align: right; padding-right: 15px;">
                                    <%#DataBinder.Eval(Container.DataItem,"Latency")%>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <div id="divRegion" runat="server" visible="False" class="voipSearchFont" style="padding-left: 10px;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            padding-top: 10px;">
                        <span>
                            <%= NoRegionLabel %></span><br />
                        <div class="voipSearchFont" style="padding-bottom: 10px; padding-left: 20px; padding-top: 10px;">
                            <asp:HyperLink runat="server" ID="hlManageOperations" CssClass="voipPathDetailsManageOperationsLink"><%= SetupOperationsLabel %></asp:HyperLink>
                        </div>
                    </div>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow voipPathDetailsTableRow">
                <td class="voipCallDetailsHeaderText voipCallDetailsTableHeader" style="width: 25px">
                    <img src="" alt="" id="imgDestination" runat="server" />
                </td>
                <td class="voipCallDetailsHeaderText voipCallDetailsTableHeader" style="width: 115px">
                    <%= CallDestinationLabel %>
                </td>
                <td class="voipCallDetailsHeaderText voipCallDetailsTableHeader" style="width: 125px">
                    <asp:Label runat="server" ID="lblDestPhoneNumber"></asp:Label>
                    <asp:HyperLink runat="server" ID="hlDestPhoneNumber"></asp:HyperLink>
                </td>
                <td class="voipCallDetailsHeaderText voipCallDetailsTableHeader">
                    <asp:HyperLink runat="server" ID="hlDestRegion"></asp:HyperLink>
                </td>
            </tr>
        </table>
    </Content>
</orion:resourceWrapper>
