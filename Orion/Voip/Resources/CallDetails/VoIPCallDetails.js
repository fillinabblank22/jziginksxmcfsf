﻿function ShowUnlicensed()
{

    var htmlToDisplay = ' <div ><table><tr> <td><img src="/Orion/Voip/images/lightbulb_tip_16x16.gif"></td> <td>@{R=VNQM.Strings;K=VNQMWEBJS_AK1_98;E=js}</td></tr>';
    htmlToDisplay = htmlToDisplay + '<tr> </tr> <tr> <td> &nbsp;</td> </tr><tr><td>&nbsp;</td><td class ="voipSearchPageLink">&raquo;<a class="sw-link" href="../Admin/Details/ModulesDetailsHost.aspx"> @{R=VNQM.Strings;K=VNQMWEBJS_AK1_99;E=js} </a></td></tr><tr><td> &nbsp;</td> </tr>';
    htmlToDisplay = htmlToDisplay + '<tr><td>&nbsp;</td><td class ="voipSearchPageLink">&raquo;<a class="sw-link" href ="http://www.solarwinds.com/embedded_in_products/productLink.aspx?id=upgrade_my_license"> @{R=VNQM.Strings;K=VNQMWEBJS_AK1_100;E=js} <a></td></tr></table> </div>';
        this.showUnlicenseDialog = new Ext.Window({
            title: 'Unlicensed Phone',
            layout: 'fit',
            width: 590,
            height: 220,
            closeAction: 'hide',
            modal: true,
            closable: true,
            draggable: !($.browser.msie && $.browser.version <= 8),
            layout: 'anchor',
            items: [{
                title: '',
                html: htmlToDisplay,
                anchor: 'top 90%',
                autoScroll: false
            }],
            buttons: [{
                text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_63;E=js}',
                handler: Function.createDelegate(this, function () {
                    this.showUnlicenseDialog.hide();
                })
            }]
        });

        this.showUnlicenseDialog.show();
        return false;
}