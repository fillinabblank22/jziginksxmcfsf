﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Services.Description;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Common.Settings;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;

public partial class Orion_Voip_Resources_CallDetails_VoipPathDetails : CallDetailsBaseResource
{
    protected string CallOriginLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_141;
    protected string CallDestinationLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_142;
    protected string CallManagerLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_157;
    protected string SlaOperationPath = Resources.VNQMWebContent.VNQMWEBCODE_VB1_172;
    protected string HopLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_173;
    protected string IpAddressLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_174;
    protected string RttLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_175;
    protected string JitterLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_176;
    protected string LatencyLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_177;
    protected string NoRegionLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_178;
    protected string SetupOperationsLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_179;

    private const string operationPath = "/Orion/Voip/IpSlaOperation.aspx?NetObject=ISOP:{0}";
    private const string operationHopPath = "/Orion/Voip/IpSlaOperation.aspx?NetObject=ISOP:{0}&Path={1}&Hop={2}";

    protected static readonly string UnLicensed = Resources.VNQMWebContent.VNQMWEBCODE_VB1_137;

    private IpSlaOperation ipSlaOperation;
    private DataTable dataTableSlaOperations;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (CallDetails.VoipCallDetails != null)
        {
            if (CallDetails.VoipCallDetails.OriginLicensed)
            {
                int? origDeviceId = null;

                var origDeviceType = CallDetails.VoipCallDetails.VoipCCMType == CallManagerType.CCM
                    ? DeviceHelper.GetCiscoDeviceType(
                        CallDetails.VoipCallDetails.VoipCCMMonitoringID, CallDetails.VoipCallDetails.OrigDeviceName,
                        CallDetails.VoipCallDetails.OrigIpAddr, out origDeviceId)
                    : DeviceHelper.GetAvayaDeviceType(
                        CallDetails.VoipCallDetails.VoipCCMMonitoringID, CallDetails.VoipCallDetails.CallingPartyNumber,
                        CallDetails.VoipCallDetails.OrigIpAddr, out origDeviceId);

                imgCallOrigin.Src = CallDetailHelper.GetDeviceImage(origDeviceType, (CallDetails.VoipCallDetails.VoipCCMType == CallManagerType.ACM) ?
            IpSlaSettings.AvayaCmCallSucessCodes.Value.Contains(
            (char)CallDetails.VoipCallDetails.AvayaConditionCode) : IpSlaSettings.CcmCallSucessCodes.Value.Contains(CallDetails.VoipCallDetails.OrigCauseValue));

                if (origDeviceId.HasValue)
                {
                    lblOrigPhoneNumber.Visible = false;
                    hlOrigPhoneNumber.Text = string.IsNullOrEmpty(CallDetails.VoipCallDetails.CallingPartyNumber) ? CallDetails.VoipCallDetails.OrigDeviceName : CallDetails.VoipCallDetails.CallingPartyNumber;
                    hlOrigPhoneNumber.NavigateUrl = CallDetailHelper.GetDeviceURL(origDeviceType, origDeviceId.Value);
                }
                else
                {
                    hlOrigPhoneNumber.Visible = false;
                    lblOrigPhoneNumber.Text = string.IsNullOrEmpty(CallDetails.VoipCallDetails.CallingPartyNumber) ? CallDetails.VoipCallDetails.OrigDeviceName : CallDetails.VoipCallDetails.CallingPartyNumber;
                }

                if (CallDetails.VoipCallDetails.OrigRegionID.HasValue)
                {
                    hlOrigRegion.Visible = true;
                    hlOrigRegion.Text = CallDetails.VoipCallDetails.OrigRegionName;
                    hlOrigRegion.NavigateUrl = CallDetailHelper.GetDeviceURL(VoipDeviceType.Region, CallDetails.VoipCallDetails.OrigRegionID.Value);
                }
            }
            else
            {
                hlOrigPhoneNumber.Visible = false;
                lblOrigPhoneNumber.Visible = true;
                lblOrigPhoneNumber.Text = UnLicensed;
                hlOrigRegion.Visible = false;
                imgCallOrigin.Visible = false;
            }

            if (CallDetails.VoipCallDetails.DestLicensed)
            {
                int? destDeviceId = null;

                var destDeviceType = CallDetails.VoipCallDetails.VoipCCMType == CallManagerType.CCM
                    ? DeviceHelper.GetCiscoDeviceType(
                        CallDetails.VoipCallDetails.VoipCCMMonitoringID, CallDetails.VoipCallDetails.DestDeviceName,
                        CallDetails.VoipCallDetails.DestIpAddr, out destDeviceId)
                    : DeviceHelper.GetAvayaDeviceType(CallDetails.VoipCallDetails.VoipCCMMonitoringID,
                        CallDetails.VoipCallDetails.FinalCalledPartyNumber, 
                        CallDetails.VoipCallDetails.DestIpAddr, 
                        out destDeviceId);

                imgDestination.Src = CallDetailHelper.GetDeviceImage(destDeviceType, (CallDetails.VoipCallDetails.VoipCCMType == CallManagerType.ACM) ?
            IpSlaSettings.AvayaCmCallSucessCodes.Value.Contains(
            (char)CallDetails.VoipCallDetails.AvayaConditionCode) : IpSlaSettings.CcmCallSucessCodes.Value.Contains(CallDetails.VoipCallDetails.DestCauseValue));

                if (CallDetails.IsConference())
                {
                    imgDestination.Src = CallDetailHelper.GetDeviceImage(VoipDeviceType.Group, (CallDetails.VoipCallDetails.VoipCCMType == CallManagerType.ACM) ?
            IpSlaSettings.AvayaCmCallSucessCodes.Value.Contains(
            (char)CallDetails.VoipCallDetails.AvayaConditionCode) : IpSlaSettings.CcmCallSucessCodes.Value.Contains(CallDetails.VoipCallDetails.DestCauseValue));

                    hlDestPhoneNumber.Visible = false;
                    lblDestPhoneNumber.Text = CallDetails.VoipCallDetails.FinalCalledPartyNumber;
                }
                else if (destDeviceId.HasValue)
                {
                    lblDestPhoneNumber.Visible = false;
                    hlDestPhoneNumber.Text = CallDetails.VoipCallDetails.FinalCalledPartyNumber;
                    hlDestPhoneNumber.NavigateUrl = CallDetailHelper.GetDeviceURL(destDeviceType, destDeviceId.Value);
                }
                else
                {
                    hlDestPhoneNumber.Visible = false;
                    lblDestPhoneNumber.Text = CallDetails.VoipCallDetails.FinalCalledPartyNumber;
                }

                if (CallDetails.VoipCallDetails.DestRegionID.HasValue)
                {
                    hlDestRegion.Visible = true;
                    hlDestRegion.Text = CallDetails.VoipCallDetails.DestRegionName;
                    hlDestRegion.NavigateUrl = CallDetailHelper.GetDeviceURL(VoipDeviceType.Region, CallDetails.VoipCallDetails.DestRegionID.Value);
                }
            }
            else
            {
                hlDestPhoneNumber.Visible = false;
                lblDestPhoneNumber.Visible = true;
                lblDestPhoneNumber.Text = UnLicensed;
                hlDestRegion.Visible = false;
                imgDestination.Visible = false;
            }

            hlCallManager.Text = CallDetails.VoipCallDetails.CallManagerIpAddress;
            hlCallManager.NavigateUrl = CallDetailHelper.GetDeviceURL(VoipDeviceType.CallManager, CallDetails.VoipCallDetails.CallManagerNodeID);

            CreateSlaOperationsTable();
            GetPathJitterOperationResults();
        }
    }

    protected void PathDetailsRepeater_DataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var htmlTableRow = (HtmlTableRow)e.Item.FindControl("trPathDetailRow");

            var row = (DataRowView)e.Item.DataItem;
            if (row["Status"].Equals("Fail"))
            {
                htmlTableRow.Attributes.Add("class", "voipPathDetailsFailedRowColor");
            }

            var hlHop = (HyperLink)htmlTableRow.FindControl("hlHop");
            hlHop.NavigateUrl =
                this.Page.ResolveUrl(string.Format(operationHopPath,
                ipSlaOperation.OperationInstanceID, row["Path"], row["Hop"]));
        }
    }

    private void CreateSlaOperationsTable()
    {
        dataTableSlaOperations = new DataTable();
        dataTableSlaOperations.Columns.Add("Path");
        dataTableSlaOperations.Columns.Add("Hop");
        dataTableSlaOperations.Columns.Add("ImageLocation");
        dataTableSlaOperations.Columns.Add("Status");
        dataTableSlaOperations.Columns.Add("IpAddress");
        dataTableSlaOperations.Columns.Add("RTT");
        dataTableSlaOperations.Columns.Add("Jitter");
        dataTableSlaOperations.Columns.Add("Latency");
    }

    private void GetPathJitterOperationResults()
    {
        if (CallDetails.VoipCallDetails.OriginLicensed && CallDetails.VoipCallDetails.DestLicensed)
        {
            if (CallDetails.VoipCallDetails.OrigRegionID.HasValue && CallDetails.VoipCallDetails.DestRegionID.HasValue)
            {
                divRegion.Visible = false;

                DataTable dt = CallDetails.GetLatestOperationDetails(CallDetails.VoipCallDetails.OrigRegionID.Value,
                                                                     CallDetails.VoipCallDetails.DestRegionID.Value,
                                                                     CallDetails.VoipCallDetails.DateTimeDisconnectUTC,
                                                                     true);

                if (dt.Rows.Count > 0)
                {
                    LoadPathDetails(dt);
                }
                else
                {
                    dt = CallDetails.GetLatestOperationDetails(CallDetails.VoipCallDetails.OrigRegionID.Value,
                                                               CallDetails.VoipCallDetails.DestRegionID.Value,
                                                               CallDetails.VoipCallDetails.DateTimeDisconnectUTC,
                                                               false);

                    if (dt.Rows.Count <= 0)
                    {
                        OperationOrResultsNotAvailable(true);
                    }
                    else
                    {
                        LoadPathDetails(dt);
                    }
                }
            }
            else
            {
                OperationOrResultsNotAvailable(true);
            }
        }
        else
        {
            OperationOrResultsNotAvailable(false);
        }
        PathDetailsRepearter.DataSource = dataTableSlaOperations;
        PathDetailsRepearter.DataBind();
    }

    private void LoadPathDetails(DataTable dt)
    {
        Resource.SubTitle = String.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_182, Resource.SubTitle, Convert.ToDateTime(dt.Rows[0]["recordtime"].ToString()).ToLocalTime().ToString("F").ToUpper());

        OperationStatus operationStatus = (OperationStatus)int.Parse(dt.Rows[0]["operationstatusid"].ToString());
        imgSlaOperation.Src = GetOperationStatusImage(operationStatus);

        hlSlaOperation.Visible = true;
        lblSlaOperation.Visible = false;
        hlSlaOperation.Text = dt.Rows[0]["sourcename"].ToString() + " &rarr; " + dt.Rows[0]["destname"].ToString();
        hlSlaOperation.NavigateUrl = this.Page.ResolveUrl(string.Format(operationPath, dt.Rows[0]["operationinstanceid"].ToString()));

        ipSlaOperation = CreateOperation("ISOP:" + dt.Rows[0]["operationinstanceid"].ToString());

        DataTable dtPathDetails = CallDetails.GetLastHopDetails(ipSlaOperation.OperationInstanceID, Convert.ToDateTime(dt.Rows[0]["recordtime"].ToString()));

        foreach (DataRow drPathInfo in dtPathDetails.Rows)
        {

            DataRow drSlaOperation = dataTableSlaOperations.NewRow();
            drSlaOperation["Path"] = drPathInfo["PathID"].ToString();
            drSlaOperation["Hop"] = drPathInfo["Hop"].ToString();
            drSlaOperation["IpAddress"] = drPathInfo["HopIpAddress"].ToString();

            bool rttFlag = true;
            drSlaOperation["RTT"] =
                CallDetailHelper.GetResultValue(double.Parse(drPathInfo["AvgRoundTripTime"].ToString()),
                               IpSlaSettings.CcmCallRoundTripTimeWarningLevel.Value,
                               IpSlaSettings.CcmCallRoundTripTimeCriticalLevel.Value,
                               Resources.VNQMWebContent.VNQMWEBDATA_AK1_124, out rttFlag);
            bool jitterFlag = true;
            drSlaOperation["Jitter"] = CallDetailHelper.GetResultValue(double.Parse(drPathInfo["AvgJitter"].ToString()),
                                                      IpSlaSettings.CcmCallJitterWarningLevel.Value,
                                                      IpSlaSettings.CcmCallJitterCriticalLevel.Value,
                                                      Resources.VNQMWebContent.VNQMWEBDATA_AK1_124, out jitterFlag);
            bool latencyFlag = true;
            drSlaOperation["Latency"] = CallDetailHelper.GetResultValue(
                double.Parse(drPathInfo["AvgLatency"].ToString()),
                IpSlaSettings.CcmCallLatencyWarningLevel.Value,
                IpSlaSettings.CcmCallLatencyCriticalLevel.Value, Resources.VNQMWebContent.VNQMWEBDATA_AK1_124, out latencyFlag);

            drSlaOperation["ImageLocation"] = "~/Orion/Voip/Images/OperationTypeIcons/PathHop_16.gif";

            if (!(rttFlag && jitterFlag && latencyFlag))
            {
                drSlaOperation["Status"] = Resources.VNQMWebContent.VNQMWEBCODE_VB1_180;
            }
            else
            {
                drSlaOperation["Status"] = Resources.VNQMWebContent.VNQMWEBCODE_VB1_181;
            }

            dataTableSlaOperations.Rows.Add(drSlaOperation);
        }

        divRegion.Visible = false;
    }

    private void OperationOrResultsNotAvailable(bool setupOperations)
    {
        Resource.SubTitle = String.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_182, Resource.SubTitle, Resources.VNQMWebContent.VNQMWEBCODE_PG_12);

        imgSlaOperation.Src = GetOperationStatusImage(OperationStatus.Down);
        hlSlaOperation.Visible = false;
        lblSlaOperation.Visible = true;
        lblSlaOperation.Text = Resources.VNQMWebContent.VNQMWEBCODE_VB1_183;

        hlManageOperations.NavigateUrl = this.Page.ResolveUrl("/Orion/Voip/Admin/ManageOperations.aspx");

        divRegion.Visible = setupOperations;
    }

    IpSlaOperation CreateOperation(string operationInstanceId)
    {
        if (!string.IsNullOrEmpty(operationInstanceId))
        {
            var operation = new IpSlaOperation(operationInstanceId);

            int i;
            operation.PathId = (int.TryParse(Request["Path"], out i) ? i : (int?)null);
            operation.PathHopIndex = (int.TryParse(Request["Hop"], out i) ? i - 1 : (int?)null);

            return operation;
        }

        return null;
    }

    private string GetOperationStatusImage(OperationStatus operationStatus)
    {
        string statusImage = string.Empty;

        switch (operationStatus)
        {
            case OperationStatus.Critical:
                statusImage = "~/Orion/Voip/images/OperationStatusIcons/Small-UpCritical.gif";
                break;
            case OperationStatus.Down:
                statusImage = "~/Orion/Voip/images/OperationStatusIcons/Small-Down.gif";
                break;
            case OperationStatus.Up:
                statusImage = "~/Orion/Voip/images/OperationStatusIcons/Small-Up.gif";
                break;
            case OperationStatus.Unreachable:
                statusImage = "~/Orion/Voip/images/OperationStatusIcons/Small-Unreachable.gif";
                break;
            case OperationStatus.Warning:
                statusImage = "~/Orion/Voip/images/OperationStatusIcons/Small-UpWarning.gif";
                break;
            default:
                statusImage = "~/Orion/Voip/images/OperationStatusIcons/Small-Unknown.gif";
                break;
        }

        return statusImage;
    }

    public override string DetachURL
    {
        get
        {
            return string.Format("/Orion/DetachResource.aspx?ResourceID={0}&CallDetailsID={1}&NetObject=", this.Resource.ID, Request["CallDetailsID"]);
        }
    }

    public override string EditURL
    {
        get { return string.Format("/Orion/Voip/Resources/EditVoipResource.aspx?ResourceID={0}&NetObject=", this.Resource.ID); }
    }

    protected override string DefaultTitle
    {
        get { return Resources.VNQMWebContent.VNQMWEBCODE_VB1_184; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionIPSLAMonitorPHResourcePathDetails"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(ICallDetailsProvider) }; }
    }
}
