﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VoipCallTopology.ascx.cs"
    Inherits="Orion_Voip_Resources_CallDetails_VoipCallTopology" %>
    <orion:Include ID="Include1" runat="server" File="Voip/Resources/CallDetails/VoipCallTopology.js" />
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>

                    <table border="0" runat="server" id="unlicensedPhoneWarningTable">
                        <tr> 
                            <td class="NoBorder" width="24" valign="top"><img src="/Orion/images/warning_16x16.gif"/></td>
                            <td class="NoBorder" style="padding-bottom: 10px;">
                                <asp:Label ID="UnlicensedPhonesWarningLabel" runat="server"><%= UnlicensedPhonesWarningText %></asp:Label>
                                <a href="<%= LearnMoreUnlicensedPhonesUrl %>" class="VNQM_HelpLink" target="_blank">
                                    <span class="LinkArrow">&#0187;</span> <%= LearnMoreLabel %>
                                </a>
                            </td>
                        </tr>
                    </table>
        <orion:Include ID="Include2" runat="server" Framework="Ext" FrameworkVersion="3.4" />
        <style>
            /* Override ExtJs H1 styles back to MainLayout.css */
            h1 { 
                font-size: large;
	            font-family: Arial, Helvetica, Sans-Serif;
	            font-weight: normal;
	            padding-left: 10px;
	            padding-top: 15px;
	            padding-bottom: 10px;
	            margin: 0;
            }
        </style>
        <asp:Panel runat="server" ID="panelControl" ScrollBars="Horizontal">
        </asp:Panel>
    </Content>
</orion:resourceWrapper>
