﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using SolarWinds.Orion.IpSla.Common;
using SolarWinds.Orion.IpSla.Common.Settings;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_Resources_CallDetails_VoipCallTopology : CallDetailsBaseResource
{
    protected static readonly string UnlicensedPhonesWarningText = Resources.VNQMWebContent.VNQMWEBCODE_VB1_135;
    protected static readonly string LearnMoreLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_136;
    protected static readonly string UnLicensed = Resources.VNQMWebContent.VNQMWEBCODE_VB1_137;

    private IList<CallTopologyStep> callTopology { get; set; }
    private StringBuilder headerRow = new StringBuilder();
    private StringBuilder contentRow = new StringBuilder();
    private string tableControl = "<table cellspacing='0'>{0}{1}</table>";

    private const string helpFragment = "OrionIPSLAManagerAGVNQMLicensing";

    protected string LearnMoreUnlicensedPhonesUrl = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        panelControl.Width = Resource.Width;

        // Show/Hide warning when there are unlicensed nodes
        unlicensedPhoneWarningTable.Visible = LicenseHelper.HasUnlicensedPhones();
        LearnMoreUnlicensedPhonesUrl = HelpLocator.GetHelpUrl(helpFragment);

        callTopology = CallDetails.GetCallTopologyPath();

        if (callTopology != null)
        {
            headerRow.Append("<tr class='voipCallDetailsTableRow VoipCallDetailsTableAlternateRow'>");
            contentRow.Append("<tr class='voipCallTopologyContentRow'>");

            int stepCount = callTopology.Count;

            foreach (var callTopologyStep in callTopology)
            {
                AddStep(callTopologyStep);

                if (callTopologyStep.StepNumber + 1 < stepCount)
                {
                    AddHeader("&nbsp;");
                    AddArrow();
                }
            }

            headerRow.Append("</tr>");
            contentRow.Append("</tr>");

            panelControl.Controls.Add(new LiteralControl(string.Format(tableControl, headerRow, contentRow)));
        }
    }

    private void AddStep(CallTopologyStep callTopologyStep)
    {
        //originator
        if (callTopologyStep.SourceAttendees.Count == 1 && callTopologyStep.DestinationAttendees.Count == 0)
        {
            Attendee attendee = callTopologyStep.SourceAttendees[0];

            if (attendee.DeviceType == VoipDeviceType.CallManager)
            {
                AddHeader(Resources.VNQMWebContent.VNQMWEBCODE_VB1_164);
                AddCallManager(attendee.DeviceId.Value, attendee.DeviceName, attendee.IpAddress);
            }
            else
            {
                AddHeader(Resources.VNQMWebContent.VNQMWEBCODE_VB1_165);
                AddTerminal(attendee);
            }
        }
        else if (callTopologyStep.SourceAttendees.Count > 1)
        {
            AddHeader(Resources.VNQMWebContent.VNQMWEBCODE_VB1_166);
            AddGroup(callTopologyStep.SourceAttendees, callTopologyStep.StepNumber, callTopologyStep.StepDescription);
        }
        else if (callTopologyStep.DestinationAttendees.Count == 1)
        {
            Attendee attendee = callTopologyStep.DestinationAttendees[0];
            AddHeader(Resources.VNQMWebContent.VNQMWEBCODE_VB1_167);
            AddTerminal(attendee);
        }
    }

    private void AddTerminal(Attendee attendee)
    {
        contentRow.AppendFormat("<td align='center' class='voipCallDetailsRowText NoBorder voipTopologyContentRow'>");

        if (attendee.Licensed)
        {
            bool isCallSuccessful = (CallDetails.VoipCallDetails.VoipCCMType == CallManagerType.ACM)?IpSlaSettings.AvayaCmCallSucessCodes.Value.Contains(attendee.ConditionCode) 
                : IpSlaSettings.CcmCallSucessCodes.Value.Contains(attendee.CauseValue);

            int ipAddress;
            string deviceImageURL = string.Empty;
            string ipAddressString = string.Empty;

            if (int.TryParse(attendee.IpAddress, out ipAddress))
            {
                deviceImageURL = CallDetailHelper.GetDeviceImage(attendee.DeviceType, isCallSuccessful);
                ipAddressString = IPAddressHelper.IntToIPv4string(ipAddress);
            }
            else
            {
                if ((CallDetails.VoipCallDetails.VoipCCMType == CallManagerType.CCM) || (CallDetails.VoipCallDetails.VoipCCMType == CallManagerType.ACM))
                    deviceImageURL = CallDetailHelper.GetDeviceImage(VoipDeviceType.CCMPhone);
                else
                    deviceImageURL = CallDetailHelper.GetDeviceImage(VoipDeviceType.Gateway, isCallSuccessful);

                ipAddressString = IPAddressHelper.IntToIPv4string(0);
            }

            contentRow.AppendFormat("<img src='{0}' alt='{1}' /><br/>", deviceImageURL, ipAddressString);

            if (attendee.DeviceId.HasValue)
            {
                string ccmDestPhonePath = CallDetailHelper.GetDeviceURL(attendee.DeviceType, attendee.DeviceId);

                contentRow.AppendFormat("<a href='{0}'>{1}<br/>{2}</a>", ccmDestPhonePath, attendee.AttendeeNumber,
                                        ipAddressString);
            }
            else
            {
                contentRow.AppendFormat("{0}<br/>{1}", attendee.AttendeeNumber, ipAddressString);
            }

            if (attendee.RegionId.HasValue)
            {
                contentRow.AppendFormat("<br/><a href='{0}'><b>{1}</b></a>",
                                        CallDetailHelper.GetDeviceURL(VoipDeviceType.Region, attendee.RegionId), attendee.RegionName);
            }
        }
        else
        {
            contentRow.AppendFormat(UnLicensed + "<br/>" + UnLicensed);
        }

        contentRow.Append("</td>");
    }

    private void AddGroup(IList<Attendee> attendees, int stepNumber, string conferenceBridge)
    {
        contentRow.AppendFormat("<td align='center'class='NoBorder voipTopologyContentRow'><div class='voipCallDetailsRowText voipTopologyConfCallClick' CallOriginDivID='{2}'><img src='{0}' alt='{3}' /><br/><b>{1}</b></div>",
                                 CallDetailHelper.GetDeviceImage(VoipDeviceType.Group), conferenceBridge, "divConfCall" + stepNumber, Resources.VNQMWebContent.VNQMWEBCODE_VB1_168);

        StringBuilder strBuilder = new StringBuilder();
        strBuilder.AppendFormat("<div style='display:none' id='{0}'>" +
                                "<div class='voipDivConfCall x-window-body'><table cellspacing='0' cellpadding='0' style='border-spacing:0'>" +
                                "<thead><tr class='voipCallDetailsHeaderText'><td>{1}</td><td>{2}</td></tr></thead><tbody>", "divConfCall" + stepNumber, Resources.VNQMWebContent.VNQMWEBCODE_VB1_141, Resources.VNQMWebContent.VNQMWEBCODE_VB1_169);
        int index = 0;
        foreach (Attendee attendee in attendees)
        {
            if (attendee.Licensed)
            {
                strBuilder.AppendFormat("<tr class='{0}'><td><a href='{1}'>{2}</a></td>",
                                        (index++ % 2 != 0) ? "ZebraStripe" : "", attendee.CallDetailParam,
                                        attendee.AttendeeNumber);
                strBuilder.Append("<td>");
                if (attendee.RegionId.HasValue)
                {
                    strBuilder.AppendFormat("<a href='{0}'>{1}</a>",
                                            CallDetailHelper.GetDeviceURL(VoipDeviceType.Region, attendee.RegionId), attendee.RegionName);
                }
                else
                {
                    strBuilder.AppendFormat("{0}", attendee.RegionName);
                }
                strBuilder.Append("</td></tr>");
            }
            else
            {
                strBuilder.AppendFormat("<tr><td>" + UnLicensed + "</td><td>" + UnLicensed + "</td></tr>");
            }
        }

        strBuilder.Append("</tbody></table></div></div>");

        contentRow.Append(strBuilder.ToString());
        contentRow.Append("</td>");
    }

    private void AddCallManager(int callManagerNodeID, string callManagerName, string callManagerIpAddress)
    {
        contentRow.AppendFormat("<td align='center' class='voipCallDetailsRowText NoBorder voipTopologyContentRow'><img src='{0}' alt='' /><br/>", CallDetailHelper.GetDeviceImage(VoipDeviceType.CallManager));

        contentRow.AppendFormat("<a href='{0}'>{1}<br/>{2}</a></td>", CallDetailHelper.GetDeviceURL(VoipDeviceType.CallManager, callManagerNodeID), callManagerName, callManagerIpAddress);
    }

    private void AddHeader(string headerText)
    {
        headerRow.AppendFormat("<td align='center' class='voipSearchFont NoBorder'>{0}</td>", headerText);
    }

    private void AddArrow()
    {
        contentRow.AppendFormat("<td align='center' class='voipSearchFont NoBorder voipTopologyContentRow'><img src='{0}' alt='' /><br/></td>", Page.ResolveUrl("/Orion/Voip/images/CallIcons/arrow_call_topology.png"));
    }

    public override string DetachURL
    {
        get
        {
            return string.Format("/Orion/DetachResource.aspx?ResourceID={0}&CallDetailsID={1}&NetObject=", this.Resource.ID, Request["CallDetailsID"]);
        }
    }

    public override string EditURL
    {
        get { return string.Format("/Orion/Voip/Resources/EditVoipResource.aspx?ResourceID={0}&NetObject=", this.Resource.ID); }
    }

    protected override string DefaultTitle
    {
        get { return Resources.VNQMWebContent.VNQMWEBCODE_VB1_170; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionIPSLAMonitorPHResourceVOIPCallTopology"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(ICallDetailsProvider) }; }
    }
}