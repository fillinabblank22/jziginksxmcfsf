﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VoipCallDetails.ascx.cs"
    Inherits="Orion_Voip_Resources_CallDetails_VoipCallDetails" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web" %>
<orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include ID="Include2" runat="server" File="Voip/Resources/CallDetails/VoIPCallDetails.js" />
 <script type="text/javascript">
     $(document).ready(function () {
         $('#voipCallDetailsTable > #voipCallDetailsTableBody > tr:nth-child(even)').not('#rowSeperator').not('#voipCallDetailsHeader').not('#unlicensedPhonesWarningRow').not('#callDetailStatus').addClass('VoipCallDetailsTableAlternateRow');
     });
    </script>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <table class="NeedsZebraStripes" "border="0" cellpadding="2" cellspacing="0" width="100%" id="voipCallDetailsTable">
            <tbody id="voipCallDetailsTableBody">
            <% if (LicenseHelper.HasUnlicensedPhones())
               { %>
            <tr id="unlicensedPhonesWarningRow">
                <td colspan="3" class="NoBorder">
                    <table border="0" runat="server" id="unlicensedPhoneWarningTable">
                        <tr>
                            <td class="NoBorder" width="24" valign="top">
                                <img src="/Orion/images/warning_16x16.gif" />
                            </td>
                            <td class="NoBorder" style="padding-bottom: 10px;">
                                <asp:Label ID="UnlicensedPhonesWarningLabel" runat="server"><%= UnlicensedPhonesWarningText %></asp:Label>
                                <a href="<%= LearnMoreUnlicensedPhonesUrl %>" class="VNQM_HelpLink" target="_blank">
                                    <span class="LinkArrow">&#0187;</span>
                                    <%= LearnMoreLabel %>
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <% } %>
            <tr id="callDetailStatus">
                <td colspan="3" class="NoBorder">
                    <img id="imgSuccess" runat="server" src="/Orion/Voip/images/CallIcons/Call32x32.png"
                        visible="False" alt="" />
                    <img id="imgFailure" runat="server" src="/Orion/Voip/images/CallIcons/Call32x32_rX.png"
                        visible="False" alt="" />
                    &nbsp;&nbsp;
                    <asp:Label runat="server" ID="lblSuccessMessage" ForeColor="Green" Visible="False"
                        CssClass="voipCallDetailStatus"></asp:Label>
                    <asp:Label runat="server" ID="lblFailureMessage" ForeColor="Red" Visible="False"
                        CssClass="voipCallDetailStatus"></asp:Label>
                </td>
            </tr>
            <tr id="rowSeperator">
                <td colspan="3" class="NoBorder">
                    &nbsp;
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow" id="voipCallDetailsHeader">
                <td class="voipCallDetailsHeaderText voipCallDetailsTableHeader NoBorder">
                    <table style="border-right: solid 1px white; height: 100%">
                        <tr>
                            <td style="border: #CCE1F1;">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="voipCallDetailsHeaderText voipCallDetailsTableHeader NoBorder">
                    <table style="border-right: solid 1px white; height: 100%">
                        <tr>
                            <td style="border: #CCE1F1;">
                                <img src="" alt="" id="imgCallOrigin" runat="server" />
                                <span class="voipCallOrigDestLabelPosition">
                                    <%= CallOriginLabel %></span>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="voipCallDetailsHeaderText voipCallDetailsTableHeader">
                    <table style="height: 100%">
                        <tr>
                            <td style="border: #CCE1F1;">
                                <img src="" alt="" id="imgCallDest" runat="server" />
                                <span class="voipCallOrigDestLabelPosition">
                                    <%= CallDestinationLabel%></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                    <%= PhoneNumberLabel %>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <asp:HyperLink runat="server" ID="hlOriginPhoneNumber"></asp:HyperLink>
                    <asp:Label runat="server" ID="lblOriginPhoneNumber"></asp:Label>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <asp:HyperLink runat="server" ID="hlDestPhoneNumber"></asp:HyperLink>
                    <asp:Label runat="server" ID="lblDestPhoneNumber"></asp:Label>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                    <%= IPAddressLabel %>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <asp:HyperLink runat="server" ID="hlOriginIPAddress"></asp:HyperLink>
                    <asp:Label runat="server" ID="lblOriginIPAddress"></asp:Label>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <asp:HyperLink runat="server" ID="hlDestIPAddress"></asp:HyperLink>
                    <asp:Label runat="server" ID="lblDestIPAddress"></asp:Label>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                    <%= LocationLabel %>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <asp:HyperLink runat="server" ID="hlOrigRegion"></asp:HyperLink>
                    <asp:Label runat="server" ID="lblOrigRegion"></asp:Label>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <asp:HyperLink runat="server" ID="hlDestRegion"></asp:HyperLink>
                    <asp:Label runat="server" ID="lblDestRegion"></asp:Label>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                    <%= MOSLabel %>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <%= OrigMOSValue %>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <%= DestMOSValue %>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                    <%= LatencyLabel %>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <%= OrigLatencyValue %>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <%= DestLatencyValue %>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                    <%= JitterLabel %>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <%= OrigJitterValue %>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <%= DestJitterValue %>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                    <%= PacketLossLabel %>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <%= OrigPacketLossValue %>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <%= DestPacketLossValue %>
                </td>
            </tr>
             <% if (!IsAvayaCm)
               { %>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                    <%= CallPrecedenceLevelLabel %>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <asp:Label runat="server" ID="lblOriginCallPrecedenceLevel"></asp:Label>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <asp:Label runat="server" ID="lblDestCallPrecedenceLevel"></asp:Label>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                    <%= ProtocolLabel %>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <asp:Label runat="server" ID="lblOriginProtocol"></asp:Label>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <asp:Label runat="server" ID="lblDestinationProtocol"></asp:Label>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                    <%= CodecLabel %>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <asp:Label runat="server" ID="lblOriginCodec"></asp:Label>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <asp:Label runat="server" ID="lblDestinationCodec"></asp:Label>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText NoBorder">
                    <%= CallTerminationCauseLabel %>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <asp:Label runat="server" ID="lblOriginCallTerminationCause"></asp:Label>
                </td>
                <td class="voipCallDetailsRowText NoBorder">
                    <asp:Label runat="server" ID="lblDestCallTerminationCause"></asp:Label>
                </td>
            </tr>
            <% } %>
            </tbody>
            <tr style="padding-top: 3px;">
                <td colspan="3" style="width: 1px; border-bottom: 1px solid #A9A99D; font-size: 2pt;">
                    &nbsp;
                </td>
            </tr>
            <% if (IsAvayaCm)
               { %>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText">
                    <%= ConditionCodeLabel %>
                </td>
                <td class="voipCallDetailsRowText" colspan="2">
                        <asp:Label runat="server" ID="lblConditionCode" />
                </td>
            </tr>
            <% } %>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText">
                    <%= StartTimeLabel %>
                </td>
                <td class="voipCallDetailsRowText" colspan="2">
                    <asp:Label runat="server" ID="lblStartTime"></asp:Label>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText">
                    <%= EndTimeLabel %>
                </td>
                <td class="voipCallDetailsRowText" colspan="2">
                    <asp:Label runat="server" ID="lblEndTime"></asp:Label>
                </td>
            </tr>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText">
                    <%= DurationLabel %>
                </td>
                <td class="voipCallDetailsRowText" colspan="2">
                    <asp:Label runat="server" ID="lblDuration"></asp:Label>
                </td>
            </tr>
            <% if (!IsAvayaCm)
               { %>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText">
                    <%= RedirectReasonLabel %>
                </td>
                <td class="voipCallDetailsRowText" colspan="2">
                    <asp:Label runat="server" ID="lblRedirectReason"></asp:Label>
                </td>
            </tr>
            <% } %>
            <tr class="voipCallDetailsTableRow">
                <td class="voipCallDetailsHeaderText" style="border: white">
                    <%= CallManagerLabel %>
                </td>
                <td class="voipCallDetailsRowText" style="border: white;" colspan="2">
                    <img id="imgCCM" runat="server" src="/Orion/Voip/images/CallIcons/CCM16x16.png"
                        style="position: relative; top: 3px;" alt="" />
                    &nbsp;
                    <asp:HyperLink runat="server" ID="hlCallManager"></asp:HyperLink>
                </td>
            </tr>
        </table>
    </Content>
</orion:resourceWrapper>
