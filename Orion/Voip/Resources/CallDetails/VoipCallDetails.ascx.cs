﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.IpSla.Common;
using SolarWinds.Orion.IpSla.Common.Settings;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_Resources_CallDetails_VoipCallDetails : CallDetailsBaseResource
{
    protected static readonly string NA = Resources.VNQMWebContent.VNQMWEBCODE_VB1_134;
    protected static readonly string UnlicensedPhonesWarningText = Resources.VNQMWebContent.VNQMWEBCODE_VB1_135;
    protected static readonly string LearnMoreLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_136;
    protected static readonly string UnLicensed = Resources.VNQMWebContent.VNQMWEBCODE_VB1_137;
    protected static readonly string UnSupported = Resources.VNQMWebContent.VNQMWEBCODE_VB1_138;
    protected static readonly string CallSuccessMessage = Resources.VNQMWebContent.VNQMWEBCODE_VB1_139;
    protected static readonly string CallFailureMessage = Resources.VNQMWebContent.VNQMWEBCODE_VB1_140;

    protected string LearnMoreUnlicensedPhonesUrl = string.Empty;
    protected string CallOriginLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_141;
    protected string CallDestinationLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_142;
    protected string PhoneNumberLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_143;
    protected string IPAddressLabel = Resources.VNQMWebContent.VNQMWEBDATA_AK1_99;
    protected string LocationLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_144;
    protected string MOSLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_145;
    protected string JitterLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_146;
    protected string LatencyLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_147;
    protected string PacketLossLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_148;
    protected string CallTerminationCauseLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_149;
    protected string ConditionCodeLabel = Resources.VNQMWebContent.VNQMWEBCODE_SS_8;
    protected string CallPrecedenceLevelLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_150;
    protected string ProtocolLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_151;
    protected string CodecLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_152;
    protected string StartTimeLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_153;
    protected string EndTimeLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_154;
    protected string DurationLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_155;
    protected string RedirectReasonLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_156;
    protected string CallManagerLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_157;
    protected string OrigMOSValue = string.Empty;
    protected string DestMOSValue = string.Empty;
    protected string OrigJitterValue = string.Empty;
    protected string DestJitterValue = string.Empty;
    protected string OrigLatencyValue = string.Empty;
    protected string DestLatencyValue = string.Empty;
    protected string OrigPacketLossValue = string.Empty;
    protected string DestPacketLossValue = string.Empty;

    private const string helpFragment = "OrionIPSLAManagerAGVNQMLicensing";

    protected void Page_Init(object sender, EventArgs e)
    {
        // Show/Hide warning when there are unlicensed nodes
        LearnMoreUnlicensedPhonesUrl = HelpLocator.GetHelpUrl(helpFragment);

        lblSuccessMessage.Text = CallSuccessMessage;
        lblFailureMessage.Text = CallFailureMessage;

        if (CallDetails.VoipCallDetails != null)
        {
            bool isCallSuccess = CallDetails.VoipCallDetails.CallSuccess;

            if (isCallSuccess)
            {
                lblSuccessMessage.Visible = true;
                imgSuccess.Visible = true;
            }
            else
            {
                lblFailureMessage.Visible = true;
                imgFailure.Visible = true;
            }

            if (CallDetails.VoipCallDetails.OriginLicensed)
            {
                int? origDeviceId = null;

                var origDeviceType = CallDetails.VoipCallDetails.VoipCCMType == CallManagerType.CCM
                    ? DeviceHelper.GetCiscoDeviceType(
                        CallDetails.VoipCallDetails.VoipCCMMonitoringID, CallDetails.VoipCallDetails.OrigDeviceName,
                        CallDetails.VoipCallDetails.OrigIpAddr, out origDeviceId)
                    : DeviceHelper.GetAvayaDeviceType(
                        CallDetails.VoipCallDetails.VoipCCMMonitoringID, CallDetails.VoipCallDetails.CallingPartyNumber,
                        CallDetails.VoipCallDetails.OrigIpAddr, out origDeviceId);

                LicensedOrigin(origDeviceId, origDeviceType, isCallSuccess);
            }
            else
            {
                UnLicensedOrigin();
            }

            if (CallDetails.VoipCallDetails.DestLicensed)
            {
                int? destDeviceId = null;

                var destDeviceType = CallDetails.VoipCallDetails.VoipCCMType == CallManagerType.CCM
                    ? DeviceHelper.GetCiscoDeviceType(
                        CallDetails.VoipCallDetails.VoipCCMMonitoringID, CallDetails.VoipCallDetails.DestDeviceName,
                        CallDetails.VoipCallDetails.DestIpAddr, out destDeviceId)
                    : DeviceHelper.GetAvayaDeviceType(CallDetails.VoipCallDetails.VoipCCMMonitoringID,
                        CallDetails.VoipCallDetails.FinalCalledPartyNumber,
                        CallDetails.VoipCallDetails.DestIpAddr, 
                        out destDeviceId);

                LicensedDestination(destDeviceId, destDeviceType, isCallSuccess);
            }
            else
            {
                UnLicensedDestination();
            }

            lblStartTime.Text = CallDetails.VoipCallDetails.DateTimeOriginationUTC.ToLocalTime().ToString(DateTimeFormatInfo.CurrentInfo);
            lblEndTime.Text = CallDetails.VoipCallDetails.DateTimeDisconnectUTC.ToLocalTime().ToString(DateTimeFormatInfo.CurrentInfo);

            lblDuration.Text = GetDuration(CallDetails.VoipCallDetails.Duration);

            if (Enum.IsDefined(typeof(VoipCallRedirectReason), CallDetails.VoipCallDetails.RedirectReason))
            {
                var redirectReason = (VoipCallRedirectReason)CallDetails.VoipCallDetails.RedirectReason;

                if (!isCallSuccess)
                {
                    lblRedirectReason.CssClass = "voipWarningValue";

                    lblRedirectReason.Text = GetLocalizedProperty((typeof(VoipCallRedirectReason)).Name, redirectReason.ToString());

                }
                else
                {
                    lblRedirectReason.Text = CallDetails.VoipCallDetails.RedirectReason == (int)VoipCallRedirectReason.Unknown ? NA : GetLocalizedProperty((typeof(VoipCallRedirectReason)).Name, redirectReason.ToString());
                }
            }

            if (Enum.IsDefined(typeof(AvayaConditionCodes), CallDetails.VoipCallDetails.AvayaConditionCode))
            {
                AvayaConditionCodes conditionCode = CallDetails.VoipCallDetails.AvayaConditionCode;

                if (!isCallSuccess)
                {
                    lblConditionCode.CssClass = "voipWarningValue";
                }

                if (!CallDetails.VoipCallDetails.OriginLicensed || !CallDetails.VoipCallDetails.DestLicensed)
                {
                    lblConditionCode.Text = UnLicensed;
                }
                else
                {
                    lblConditionCode.Text = GetLocalizedProperty((typeof (AvayaConditionCodes)).Name,
                        conditionCode.ToString());
                }
            }


            hlCallManager.Text = CallDetails.VoipCallDetails.CallManagerIpAddress;
            hlCallManager.NavigateUrl = CallDetailHelper.GetDeviceURL(VoipDeviceType.CallManager, CallDetails.VoipCallDetails.CallManagerNodeID);
        }
    }

    private void LicensedOrigin(int? origDeviceId, VoipDeviceType origDeviceType, bool isCallSuccess)
    {
        imgCallOrigin.Src = CallDetailHelper.GetDeviceImage(origDeviceType, (CallDetails.VoipCallDetails.VoipCCMType == CallManagerType.ACM) ?
            IpSlaSettings.AvayaCmCallSucessCodes.Value.Contains(
            (char)CallDetails.VoipCallDetails.AvayaConditionCode) :
                                               IpSlaSettings.CcmCallSucessCodes.Value.Contains(
                                                   CallDetails.VoipCallDetails.OrigCauseValue));

        if (origDeviceId.HasValue)
        {
            string ccmOrigPhonePath = CallDetailHelper.GetDeviceURL(origDeviceType, origDeviceId.Value);
            lblOriginPhoneNumber.Visible = false;
            lblOriginIPAddress.Visible = false;
            if (CallDetails.VoipCallDetails.CallingPartyNumber != "")
                hlOriginPhoneNumber.Text = CallDetails.VoipCallDetails.CallingPartyNumber;
            else
                hlOriginPhoneNumber.Text = CallDetails.VoipCallDetails.OrigDeviceName;
            hlOriginPhoneNumber.NavigateUrl = ccmOrigPhonePath;
            hlOriginIPAddress.Text = IPAddressHelper.IntToIPv4string(CallDetails.VoipCallDetails.OrigIpAddr);
            hlOriginIPAddress.NavigateUrl = ccmOrigPhonePath;
        }
        else
        {
            hlOriginPhoneNumber.Visible = false;
            hlOriginIPAddress.Visible = false;
            if (CallDetails.VoipCallDetails.CallingPartyNumber != "")
                lblOriginPhoneNumber.Text = CallDetails.VoipCallDetails.CallingPartyNumber;
            else
                lblOriginPhoneNumber.Text = CallDetails.VoipCallDetails.OrigDeviceName;
            lblOriginIPAddress.Text = IPAddressHelper.IntToIPv4string(CallDetails.VoipCallDetails.OrigIpAddr);
        }

        if (CallDetails.VoipCallDetails.OrigRegionID.HasValue)
        {
            lblOrigRegion.Visible = false;
            hlOrigRegion.Text = CallDetails.VoipCallDetails.OrigRegionName;
            hlOrigRegion.NavigateUrl = CallDetailHelper.GetDeviceURL(VoipDeviceType.Region, CallDetails.VoipCallDetails.OrigRegionID.Value);
        }
        else
        {
            hlOrigRegion.Visible = false;
            lblOrigRegion.Text = NA;
        }
       
        string nullValueContent = CallDetails.VoipCallDetails.OrigCmrSupported ? NA : UnSupported;
        OrigMOSValue = CallDetails.VoipCallDetails.OrigMOS.HasValue ? CallDetailHelper.GetMos(CallDetails.VoipCallDetails.OrigMOS.Value) : nullValueContent;
        OrigJitterValue = CallDetails.VoipCallDetails.OrigJitter.HasValue ? CallDetailHelper.GetResultValue(CallDetails.VoipCallDetails.OrigJitter.Value,
                                            IpSlaSettings.CcmCallJitterWarningLevel.Value,
                                            IpSlaSettings.CcmCallJitterCriticalLevel.Value, Resources.VNQMWebContent.VNQMWEBDATA_AK1_124) : nullValueContent;
        OrigLatencyValue = CallDetails.VoipCallDetails.OrigLatency.HasValue ? CallDetailHelper.GetResultValue(CallDetails.VoipCallDetails.OrigLatency.Value,
                                            IpSlaSettings.CcmCallLatencyWarningLevel.Value,
                                            IpSlaSettings.CcmCallLatencyCriticalLevel.Value, Resources.VNQMWebContent.VNQMWEBDATA_AK1_124) : nullValueContent;
        OrigPacketLossValue = CallDetails.VoipCallDetails.OrigPacketLoss.HasValue ? CallDetailHelper.GetResultValue(CallDetails.VoipCallDetails.OrigPacketLoss.Value,
                                                IpSlaSettings.CcmCallPacketLossWarningLevel.Value,
                                                IpSlaSettings.CcmCallPacketLossCriticalLevel.Value, Resources.VNQMWebContent.VNQMWEBCODE_VB1_204) : nullValueContent;
       
        if (Enum.IsDefined(typeof(VoipCallTerminationCause), CallDetails.VoipCallDetails.OrigCauseValue))
        {
            if (!isCallSuccess)
            {
                lblOriginCallTerminationCause.CssClass = "voipWarningValue";
            }

            VoipCallTerminationCause origCause = (VoipCallTerminationCause)CallDetails.VoipCallDetails.OrigCauseValue;
            lblOriginCallTerminationCause.Text = GetLocalizedProperty("VoipCallTerminationCause", origCause.ToString());
        }
        else
        {
            lblOriginCallTerminationCause.Text = CallDetails.VoipCallDetails.OrigCauseValue.ToString();
        }

        if (Enum.IsDefined(typeof(VoipCallPrecedenceLevel), CallDetails.VoipCallDetails.OrigPrecedenceLevel))
        {
            VoipCallPrecedenceLevel origPrecedence = (VoipCallPrecedenceLevel)CallDetails.VoipCallDetails.OrigPrecedenceLevel;
            lblOriginCallPrecedenceLevel.Text = GetLocalizedProperty("VoipCallPrecedenceLevel", origPrecedence.ToString());
        }

        if (CallDetails.VoipCallDetails.IncomingProtocolId.HasValue)
        {
            if (Enum.IsDefined(typeof(VoipCallProtocol), CallDetails.VoipCallDetails.IncomingProtocolId))
            {
                VoipCallProtocol origProtocol = (VoipCallProtocol)CallDetails.VoipCallDetails.IncomingProtocolId;
                lblOriginProtocol.Text = GetLocalizedProperty("VoipCallProtocol", origProtocol.ToString());
            }
        }
        else
        {
            lblOriginProtocol.Text = NA;
        }
        VoipCallAudioCodec origAudioCodec = (VoipCallAudioCodec)CallDetails.VoipCallDetails.OrigMediaCapPayloadCapability;
        VoipCallVideoCodec origVideoCodec = (VoipCallVideoCodec)CallDetails.VoipCallDetails.OrigVideoCapCodec;
        lblOriginCodec.Text = String.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_158, GetLocalizedProperty("VoipCallAudioCodec", origAudioCodec.ToString()), GetLocalizedProperty("VoipCallVideoCodec", origVideoCodec.ToString()));
    }

    private void UnLicensedOrigin()
    {
        hlOriginPhoneNumber.Visible = true;
        hlOriginIPAddress.Visible = true;
        lblOriginPhoneNumber.Visible = false;
        lblOriginIPAddress.Visible = false;
        hlOriginPhoneNumber.Text = UnLicensed;
        hlOriginIPAddress.Text = UnLicensed;
        hlOrigRegion.Text = UnLicensed;
        hlOriginPhoneNumber.Attributes.Add("onclick", "ShowUnlicensed()");
        hlOriginIPAddress.Attributes.Add("onclick", "ShowUnlicensed()");

        OrigMOSValue = UnLicensed;
        OrigJitterValue = UnLicensed;
        OrigLatencyValue = UnLicensed;
        OrigPacketLossValue = UnLicensed;

        lblOriginCallTerminationCause.Text = UnLicensed;
        lblOriginCallPrecedenceLevel.Text = UnLicensed;
        lblOriginProtocol.Text = UnLicensed;
        lblOriginCodec.Text = UnLicensed;
        imgCallOrigin.Visible = false;
    }

    private void LicensedDestination(int? destDeviceId, VoipDeviceType destDeviceType, bool isCallSuccess)
    {
        
        imgCallDest.Src = CallDetailHelper.GetDeviceImage(destDeviceType,(CallDetails.VoipCallDetails.VoipCCMType == CallManagerType.ACM) ?
            IpSlaSettings.AvayaCmCallSucessCodes.Value.Contains(
            (char)CallDetails.VoipCallDetails.AvayaConditionCode) :
                                                 IpSlaSettings.CcmCallSucessCodes.Value.Contains(
                                                     CallDetails.VoipCallDetails.DestCauseValue));

        if (CallDetails.IsConference())
        {
            imgCallDest.Src = CallDetailHelper.GetDeviceImage(VoipDeviceType.Group, IpSlaSettings.CcmCallSucessCodes.Value.Contains(CallDetails.VoipCallDetails.DestCauseValue));

            hlDestPhoneNumber.Visible = false;
            hlDestIPAddress.Visible = false;
            lblDestPhoneNumber.Text = String.IsNullOrEmpty(CallDetails.VoipCallDetails.FinalCalledPartyNumber) ? NA : CallDetails.VoipCallDetails.FinalCalledPartyNumber;
            lblDestIPAddress.Text = NA;
        }
        else if (destDeviceId.HasValue)
        {
            string ccmDestPhonePath = CallDetailHelper.GetDeviceURL(destDeviceType, destDeviceId.Value);
            lblDestPhoneNumber.Visible = false;
            lblDestIPAddress.Visible = false;
            hlDestPhoneNumber.Text = CallDetails.VoipCallDetails.FinalCalledPartyNumber;
            hlDestPhoneNumber.NavigateUrl = ccmDestPhonePath;
            hlDestIPAddress.Text = IPAddressHelper.IntToIPv4string(CallDetails.VoipCallDetails.DestIpAddr);
            hlDestIPAddress.NavigateUrl = ccmDestPhonePath;
        }
        else
        {
            hlDestPhoneNumber.Visible = false;
            hlDestIPAddress.Visible = false;
            lblDestPhoneNumber.Text = String.IsNullOrEmpty(CallDetails.VoipCallDetails.FinalCalledPartyNumber) ? NA : CallDetails.VoipCallDetails.FinalCalledPartyNumber;
            lblDestIPAddress.Text = IPAddressHelper.IntToIPv4string(CallDetails.VoipCallDetails.DestIpAddr);
        }


        if (CallDetails.VoipCallDetails.DestRegionID.HasValue)
        {
            lblDestRegion.Visible = false;
            hlDestRegion.Text = CallDetails.VoipCallDetails.DestRegionName;
            hlDestRegion.NavigateUrl = CallDetailHelper.GetDeviceURL(VoipDeviceType.Region, CallDetails.VoipCallDetails.DestRegionID.Value);
        }
        else
        {
            hlDestRegion.Visible = false;
            lblDestRegion.Text = NA;
        }

        string nullValueContent = CallDetails.VoipCallDetails.DestCmrSupported ? NA : UnSupported;
        DestMOSValue = CallDetails.VoipCallDetails.DestMOS.HasValue ? CallDetailHelper.GetMos(CallDetails.VoipCallDetails.DestMOS.Value) : nullValueContent;
        DestJitterValue = CallDetails.VoipCallDetails.DestJitter.HasValue ? CallDetailHelper.GetResultValue(CallDetails.VoipCallDetails.DestJitter.Value,
                                             IpSlaSettings.CcmCallJitterWarningLevel.Value,
                                             IpSlaSettings.CcmCallJitterCriticalLevel.Value, Resources.VNQMWebContent.VNQMWEBDATA_AK1_124) : nullValueContent;
        DestLatencyValue = CallDetails.VoipCallDetails.DestLatency.HasValue ? CallDetailHelper.GetResultValue(CallDetails.VoipCallDetails.DestLatency.Value,
                                            IpSlaSettings.CcmCallLatencyWarningLevel.Value,
                                            IpSlaSettings.CcmCallLatencyCriticalLevel.Value, Resources.VNQMWebContent.VNQMWEBDATA_AK1_124) : nullValueContent;
        DestPacketLossValue = CallDetails.VoipCallDetails.DestPacketLoss.HasValue ? CallDetailHelper.GetResultValue(CallDetails.VoipCallDetails.DestPacketLoss.Value,
                                                IpSlaSettings.CcmCallPacketLossWarningLevel.Value,
                                                IpSlaSettings.CcmCallPacketLossCriticalLevel.Value, Resources.VNQMWebContent.VNQMWEBCODE_VB1_204) : nullValueContent;
        

        if (Enum.IsDefined(typeof(VoipCallTerminationCause), CallDetails.VoipCallDetails.DestCauseValue))
        {
            if (!isCallSuccess)
            {
                lblDestCallTerminationCause.CssClass = "voipWarningValue";
            }

            VoipCallTerminationCause destCause = (VoipCallTerminationCause)CallDetails.VoipCallDetails.DestCauseValue;
            lblDestCallTerminationCause.Text = GetLocalizedProperty("VoipCallTerminationCause", destCause.ToString());
        }
        else
        {
            lblDestCallTerminationCause.Text = CallDetails.VoipCallDetails.DestCauseValue.ToString();
        }

        if (Enum.IsDefined(typeof(VoipCallPrecedenceLevel), CallDetails.VoipCallDetails.DestPrecedenceLevel))
        {
            VoipCallPrecedenceLevel destPrecedence = (VoipCallPrecedenceLevel)CallDetails.VoipCallDetails.DestPrecedenceLevel;
            lblDestCallPrecedenceLevel.Text = GetLocalizedProperty("VoipCallPrecedenceLevel", destPrecedence.ToString());
        }


        if (CallDetails.VoipCallDetails.OutgoingProtocolId.HasValue)
        {
            if (Enum.IsDefined(typeof(VoipCallProtocol), CallDetails.VoipCallDetails.OutgoingProtocolId))
            {
                VoipCallProtocol destProtocol = (VoipCallProtocol)CallDetails.VoipCallDetails.OutgoingProtocolId;
                lblDestinationProtocol.Text = GetLocalizedProperty("VoipCallProtocol", destProtocol.ToString());
            }
        }
        else
        {
            lblDestinationProtocol.Text = NA;
        }

        VoipCallAudioCodec destAudioCodec = (VoipCallAudioCodec)CallDetails.VoipCallDetails.DestMediaCapPayloadCapability;
        VoipCallVideoCodec destVideoCodec = (VoipCallVideoCodec)CallDetails.VoipCallDetails.DestVideoCapCodec;
        lblDestinationCodec.Text = String.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_158, GetLocalizedProperty("VoipCallAudioCodec", destAudioCodec.ToString()), GetLocalizedProperty("VoipCallVideoCodec", destVideoCodec.ToString()));
    }

    private void UnLicensedDestination()
    {
        hlDestPhoneNumber.Visible = true;
        hlDestIPAddress.Visible = true;
        lblDestPhoneNumber.Visible = false;
        lblDestIPAddress.Visible = false;
        hlDestPhoneNumber.Text = UnLicensed;
        hlDestIPAddress.Text = UnLicensed;
        hlDestRegion.Text = UnLicensed;
        hlDestPhoneNumber.Attributes.Add("onclick", "ShowUnlicensed()");
        hlDestIPAddress.Attributes.Add("onclick", "ShowUnlicensed()");

        DestMOSValue = UnLicensed;
        DestJitterValue = UnLicensed;
        DestLatencyValue = UnLicensed;
        DestPacketLossValue = UnLicensed;

        lblDestCallTerminationCause.Text = UnLicensed;
        lblDestCallPrecedenceLevel.Text = UnLicensed;
        lblDestinationProtocol.Text = UnLicensed;
        lblDestinationCodec.Text = UnLicensed;
        imgCallDest.Visible = false;
    }

    private string GetDuration(int durationSec)
    {
        TimeSpan span = new TimeSpan(0, 0, durationSec);

        if(span.Hours > 0)
            return String.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_160, span.Hours, span.Minutes, span.Seconds);

        if(span.Minutes > 0)
            return String.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_162, span.Minutes, span.Seconds);

        return String.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_163, span.Seconds);
    }

    public override string DetachURL
    {
        get
        {
            return string.Format("/Orion/DetachResource.aspx?ResourceID={0}&CallDetailsID={1}&NetObject=", this.Resource.ID, Request["CallDetailsID"]);
        }
    }

    public override string EditURL
    {
        get { return string.Format("/Orion/Voip/Resources/EditVoipResource.aspx?ResourceID={0}&NetObject=", this.Resource.ID); }
    }

    protected override string DefaultTitle
    {
        get { return Resources.VNQMWebContent.VNQMWEBCODE_VB1_159; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionIPSLAMonitorPHResourceVoIPCallDetails"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(ICallDetailsProvider) }; }
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
   
    protected bool IsAvayaCm
    {
        get { return CallDetails.VoipCallDetails.VoipCCMType == CallManagerType.ACM; }
    }
}
