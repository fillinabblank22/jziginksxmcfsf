<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChartData.aspx.cs" Inherits="Orion_Voip_Resources_ChartData" %>
<%@ Register TagPrefix="voip" TagName="CustomChartControl" Src="~/Orion/Voip/Controls/InternalCustomChart.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Chart Data</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <voip:CustomChartControl runat="server" ID="CustomChart" Visible="false"/>        
    </div>
    </form>
</body>
</html>
