using System;

using SolarWinds.Orion.IpSla.Web.UI;

public partial class Orion_Voip_Resources_EditCallManagers : ResourceEditPage
{

    protected override void Initialize()
    {
         this.resourceTitleEditor.ResourceTitle = this.Resource.Title;
         this.Title = String.Format(Resources.VNQMWebContent.VNQMWEBDATA_AK1_167, Resource.Title);
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            CCMGroup.GroupedValue = Resource.Properties["GroupingBy"];
        }
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (!resourceTitleEditor.ResourceTitle.Equals(Resource.Title))
        {
            Resource.Title = resourceTitleEditor.ResourceTitle;
            SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
        }

        this.Resource.Properties["GroupingBy"] = CCMGroup.GroupedValue;

        GoBack();
    }
}
