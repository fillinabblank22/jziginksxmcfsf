using System;
using System.ComponentModel;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.IpSla.Web.Charting;

public partial class Resources_IpSlaGraphEdit : System.Web.UI.Page
{
    private int resourceId;
    private bool isCustom;

    private DateTime startPeriod;
    private DateTime endPeriod;
    private ResourceInfo _resource;

    public enum Metrics
    {
        [Description("Round Trip Time")]
        RoundTripTime,
        [Description("Jitter")]
        Jitter,
        [Description("Jitter Source to Destination")]
        JitterSD,
        [Description("Jitter Destination to Source")]
        JitterDS,
        [Description("Latency")]
        Latency,
        [Description("Packet Loss")]
        PacketLoss,
        [Description("One Way Packet Loss Source to Destination")]
        PacketLossSD,
        [Description("One Way Packet Loss Destination to Source")]
        PacketLossDS,
        [Description("HTTP Round Trip Time")]
        HttpRtt,
        [Description("DNS Round Trip Time")]
        DnsRtt,
        [Description("TCP Connection Round Trip Time")]
        TcpConnectRtt,
        [Description("Transaction Round Trip Time")]
        TransactionRtt,
        [Description("MOS")]
        MOS,
        [Description("One Way Delay Source to Destination")]
        OneWayDelaySD,
        [Description("One Way Delay Destination to Source")]
        OneWayDelayDS,
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        resourceId = int.Parse(Request.Params["ResourceID"]);
        _resource = ResourceManager.GetResourceByID(resourceId);

        Page.Title = string.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_216, _resource.Title);

        // If page is called from other then save previous page Url and fill a list
        if (!Page.IsPostBack)
        {
            this.resourceTitleEditor.ResourceTitle = _resource.Title;

            DropDownListHelper.GenerateListBoxItems(MetricDropDownList, typeof(Metrics));

            string value = _resource.Properties["GraphType"];
            if (String.IsNullOrEmpty(value)) value = Metrics.RoundTripTime.ToString();

            MetricDropDownList.SelectedValue = value;

            // Filling dropdown list with periods
            foreach (var period in Periods.GetTimePeriodList())
            {
                TimePeriodsList.Items.Add(new ListItem(period.DisplayName, period.Name));
            }
            isCustom = false;


            foreach (var sampleSize in SampleSizes.GenerateSelectionList())
            {
                SampleSizeList.Items.Add(new ListItem(GetLocalizedProperty("SampleSize", sampleSize.Name), sampleSize.SizeName));
            }

            getCurrentGraphData();

            if (BeginDateTimePeriod.Value == DateTime.MinValue)
            {
                DateTime now = DateTime.Now;
                setTimeValues(now, now);
            }
        }
    }

    /// <summary>
    /// Returns true if entered Interval fit in range
    /// </summary>
    /// <returns></returns>
    private bool IsIntervalCorrect()
    {
        DateTime _startTime = DateTime.Now;
        DateTime _endTime = DateTime.Now;
        if (CheckBoxCustom.Checked)
        {
            string _startTimeString = BeginDateTimePeriod.Value.ToString();
            string _endTimeString = EndDateTimePeriod.Value.ToString();
            _startTime = DateTime.Parse(_startTimeString);
            _endTime = DateTime.Parse(_endTimeString);
        }
        else
        {
            string _periodName = TimePeriodsList.SelectedValue;
            Periods.Parse(ref _periodName, ref _startTime, ref _endTime);
        }

        TimeSpan difference = _endTime - _startTime;

        TimeSpan sampleInterval = TimeSpan.FromMinutes((SampleSizes.FindSampleSizeBySizeName(SampleSizeList.SelectedValue) ?? new SampleSize()).Minutes);
        // Minimum value of interval is difference of start time and end time
        return (difference.TotalMinutes >= sampleInterval.TotalMinutes);
    }
    /// <summary>
    /// Custom validation of SampleSizeList
    /// </summary>
    /// <param name="source"></param>
    /// <param name="args"></param>
    protected void IntervalValidation(object source, ServerValidateEventArgs args)
    {
        args.IsValid = IsIntervalCorrect();
    }

    protected void SubmitButton_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            _resource.Title = resourceTitleEditor.ResourceTitle;
            _resource.Properties["GraphType"] = MetricDropDownList.SelectedValue;

            // If custom period is entered
            isCustom = CheckBoxCustom.Checked;
            string _startTime = BeginDateTimePeriod.Value.ToString();
            string _endTime = EndDateTimePeriod.Value.ToString();

            SampleSize sampleInterval = SampleSizes.FindSampleSizeBySizeName(SampleSizeList.SelectedValue) ?? new SampleSize();

            SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(_resource);

            if (isCustom)
            {
                // If the start period, end period and interval are correctly entered properties are saved to DB
                if (DateTime.TryParse(_startTime, out startPeriod) && DateTime.TryParse(_endTime, out endPeriod))
                {
                    GraphHelper.SetGraphIntervalProperties(resourceId, isCustom, startPeriod, endPeriod, string.Empty, sampleInterval.SizeName);
                    //Redirect to graph holder page
                    Response.Redirect(Request.Params["ReturnUrl"].ToString());
                }
            }
            // If period alias is entered (LAST 5 MINUTES for example)
            else
            {
                GraphHelper.SetGraphIntervalProperties(resourceId, isCustom, DateTime.Now, DateTime.Now, TimePeriodsList.SelectedValue, sampleInterval.SizeName);
                //Redirect to graph holder page
                Response.Redirect(Request.Params["ReturnUrl"].ToString());
            }

        }
    }

    private void getCurrentGraphData()
    {
        bool isCustom = false;
        string _period = string.Empty;
        DateTime _StartPeriod = DateTime.Now;
        DateTime _EndPeriod = DateTime.Now;
        string _SampleSize = string.Empty;

        //Gets current properties from database
        GraphHelper.GetCurrentGraphProperties(resourceId, ref _period, ref _SampleSize);

        // If such period is custom period then parse it to StartPeriod and EndPeriod using Parse method 
        if (string.IsNullOrEmpty(_period))
        {
            isCustom = false;
            _period = GaugeConstants.DEFAULT_PERIOD_VALUE;
            _SampleSize = GaugeConstants.DEFAULT_SAMPLE_SIZE;
        }
        else
        {
            // If such period is custom period then parse it to StartPeriod and EndPeriod using Parse method 
            if (Periods.GetPeriod(_period) == null)
            {
                isCustom = true;
                Periods.Parse(ref _period, ref _StartPeriod, ref _EndPeriod);
            }
        }

        SampleSizeList.SelectedValue = _SampleSize;

        CheckBoxCustom.Checked = isCustom;
        if (isCustom)
        {
            setTimeValues(_StartPeriod, _EndPeriod);                        
        }
        else
        {
            TimePeriodsList.SelectedValue = _period;
        }
    }

    /// <summary>
    /// Fills current date values using American Calendar system currently
    /// </summary>
    /// <param name="startPeriod"></param>
    /// <param name="endPeriod"></param>
    private void setTimeValues(DateTime startPeriod, DateTime endPeriod)
    {
        BeginDateTimePeriod.Value = startPeriod.ToLocalTime();
        EndDateTimePeriod.Value = endPeriod.ToLocalTime();
    }

    protected void checkStartEndDate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = IsStartEndTimeIntervalCorrect();
    }

    private bool IsStartEndTimeIntervalCorrect()
    {
        DateTime startPeriod = BeginDateTimePeriod.Value;
        DateTime endPeriod = EndDateTimePeriod.Value;

        return endPeriod >= startPeriod;
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}
