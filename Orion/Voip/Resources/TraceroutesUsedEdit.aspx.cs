using System;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web;

public partial class Resources_TraceroutesUsedEdit : System.Web.UI.Page
{
    private int resourceId;
    private bool isCustom;

    private DateTime startPeriod;
    private DateTime endPeriod;
    private ResourceInfo _resource;

    protected void Page_Load(object sender, EventArgs e)
    {
        resourceId = int.Parse(Request.Params["ResourceID"]);
        _resource = ResourceManager.GetResourceByID(resourceId);

        Page.Title = string.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_216, _resource.Title);

        // If page is called from other then save previous page Url and fill a list
        if (!Page.IsPostBack)
        {
            this.resourceTitleEditor.ResourceTitle = _resource.Title;

            // Filling dropdown list with periods
            foreach (var period in Periods.GetTimePeriodList())
            {
                TimePeriodsList.Items.Add(new ListItem(period.DisplayName, period.Name));
            }
            isCustom = false;

            getCurrentGraphData();

            if (BeginDateTimePeriod.Value == DateTime.MinValue)
            {
                DateTime now = DateTime.Now;
                setTimeValues(now, now);
            }
        }
    }

    protected void SubmitButton_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            _resource.Title = resourceTitleEditor.ResourceTitle;

            // If custom period is entered
            isCustom = CheckBoxCustom.Checked;
            string _startTime = BeginDateTimePeriod.Value.ToString();
            string _endTime = EndDateTimePeriod.Value.ToString();

            SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(_resource);

            if (isCustom)
            {
                // If the start period, end period and interval are correctly entered properties are saved to DB
                if (DateTime.TryParse(_startTime, out startPeriod) && DateTime.TryParse(_endTime, out endPeriod))
                {
                    GraphHelper.SetPeriodProperties(resourceId, isCustom, startPeriod, endPeriod, string.Empty);
                    //Redirect to graph holder page
                    Response.Redirect(Request.Params["ReturnUrl"].ToString());
                }
            }
            // If period alias is entered (LAST 5 MINUTES for example)
            else
            {
                GraphHelper.SetPeriodProperties(resourceId, isCustom, DateTime.Now, DateTime.Now, TimePeriodsList.SelectedValue);
                //Redirect to graph holder page
                Response.Redirect(Request.Params["ReturnUrl"].ToString());
            }

        }
    }


    private void getCurrentGraphData()
    {
        string _period = string.Empty;
        DateTime _StartPeriod = DateTime.Now;
        DateTime _EndPeriod = DateTime.Now;
        string _SampleSize = string.Empty;

        //Gets current properties from database
        GraphHelper.GetCurrentGraphProperties(resourceId, ref _period, ref _SampleSize);

        // If such period is custom period then parse it to StartPeriod and EndPeriod using Parse method 
        if (string.IsNullOrEmpty(_period))
        {
            _period = GaugeConstants.DEFAULT_PERIOD_VALUE;
        }
        else
        {
            // If such period is custom period then parse it to StartPeriod and EndPeriod using Parse method 
            if (Periods.GetPeriod(_period) == null)
            {
                isCustom = true;
                Periods.Parse(ref _period, ref _StartPeriod, ref _EndPeriod);
            }
        }

        CheckBoxCustom.Checked = isCustom;
        if (isCustom)
        {
            setTimeValues(_StartPeriod, _EndPeriod);                        
        }
        else
        {
            TimePeriodsList.SelectedValue = _period;
        }
    }

    /// <summary>
    /// Fills current date values using American Calendar system currently
    /// </summary>
    /// <param name="startPeriod"></param>
    /// <param name="endPeriod"></param>
    private void setTimeValues(DateTime startPeriod, DateTime endPeriod)
    {
        BeginDateTimePeriod.Value = startPeriod.ToLocalTime();
        EndDateTimePeriod.Value = endPeriod.ToLocalTime();
    }

    protected void checkStartEndDate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = IsStartEndTimeIntervalCorrect();
    }

    private bool IsStartEndTimeIntervalCorrect()
    {
        DateTime startPeriod = BeginDateTimePeriod.Value;
        DateTime endPeriod = EndDateTimePeriod.Value;

        return endPeriod >= startPeriod;
    }
}
