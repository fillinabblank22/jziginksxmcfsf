﻿using System;
using Resources;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.IpSla.Web.Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
public partial class Orion_Voip_Resources_TopXX_TopXXIpSlaOperations : VoipBaseResource
{
    public override string EditURL
    {
        get
        {
            var netobject = Request["NetObject"];
            var url = $"/Orion/Voip/Resources/TopXX/EditTopXXIpSlaOperations.aspx?ResourceID={this.Resource.ID}";
            if (!string.IsNullOrEmpty(netobject))
                url = $"{url}&NetObject={netobject}";
            return url;
        }
    }

    protected override string DefaultTitle => VNQMWebContent.VNQMWEBCODE_VB1_223;

    public override string DisplayTitle => Title.Replace("XX", MaxCount.ToString());

    public override string HelpLinkFragment => "OrionIPSLAMonitorPHResourceTopIPSLAOperations";

    protected int MaxCount => GetIntProperty("MaxCount", 10);

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
    }

    private void LoadData()
    {
        var selectedOperationTypes = ResourcePropertiesHelper.LoadSelectedValues<OperationType>(this.Resource,
            ResourcePropertiesHelper.OperationTypePrefix, true);
        var selectedMetrics = ResourcePropertiesHelper.LoadSelectedValues<TopXXMetric>(this.Resource,
            ResourcePropertiesHelper.TopXXMetricPrefix, false);
        var selectedStatuses = ResourcePropertiesHelper.LoadSelectedValues<OperationStatus>(this.Resource,
            ResourcePropertiesHelper.OperationStatusPrefix, true);

        var topXX = MaxCount;

        if (selectedMetrics.Count == 0)
            selectedMetrics.Add(TopXXMetric.RoundTripTime);

        //Merge UDL Jitter and Voidp UDP Jitter Operations
        if (selectedOperationTypes.Contains(OperationType.UdpJitter)) 
            selectedOperationTypes.Add(OperationType.UdpJitterCli);
        else if (selectedOperationTypes.Contains(OperationType.UdpJitterCli)) 
            selectedOperationTypes.Add(OperationType.UdpJitter);

        if (selectedOperationTypes.Contains(OperationType.UdpVoipJitter))
            selectedOperationTypes.Add(OperationType.UdpVoipJitterCli);
        else if (selectedOperationTypes.Contains(OperationType.UdpVoipJitterCli))
            selectedOperationTypes.Add(OperationType.UdpVoipJitter);

        //Validate
        if (selectedOperationTypes.Count == 0)
        {
            //No resource properties are set, use default
            selectedOperationTypes.AddRange(IpSlaOperationHelper.GetSupportedOperationTypes());
        }

        var metric = ResourcePropertiesHelper.LoadEnumValue(this.Resource,
            ResourcePropertiesHelper.MetricOrderBy,
            TopXXMetric.RoundTripTime);

        var nodeID = GetSourceNodeID();

        //Load data
        var dataTable = OperationsDAL.GetTopXXData(selectedOperationTypes, selectedStatuses, selectedMetrics, topXX,
            metric, nodeID);

        var ctr = Page.LoadControl("~/Orion/Voip/Controls/TopXXGrid.ascx");

        var topCtrl = ctr as TopXXControl;

        topCtrl.DataSource = dataTable;
        topCtrl.Metrics = selectedMetrics;
        topCtrl.ShowOperationTypeColumn = selectedOperationTypes.Count > 1;

        TopResources.Controls.Add(topCtrl);
    }

    private int GetSourceNodeID()
    {
        var netObject = this.Request["NetObject"];

        if (string.IsNullOrEmpty(netObject))
            return -1;

        NetObjectHelper helper;
        if (NetObjectHelper.TryParse(netObject, out helper) && helper.IsNPMNode())
            return helper.Id;

        return -1;
    }
}
