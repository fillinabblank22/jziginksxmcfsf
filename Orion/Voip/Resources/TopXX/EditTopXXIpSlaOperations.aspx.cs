﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.IpSla.Web.UI;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Data;

public partial class Orion_Voip_Resources_TopXX_EditTopXXIpSlaOperations : ResourceEditPage
{
    protected override void Initialize()
    {
       this.resourceTitleEditor.ResourceTitle = this.Resource.Title;

       this.OperationTypeSelection.Resource = this.Resource;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.MaxCount.Text = LoadMaxCount();

            // Status filter
            DropDownListHelper.GenerateCheckBoxListItems(StatusFilter, typeof(OperationStatus));
            this.Resource.LoadEnumSelection<OperationStatus>(StatusFilter, ResourcePropertiesHelper.OperationStatusPrefix);
            if (!DropDownListHelper.IsSomethingSelected(StatusFilter))
            {
                DropDownListHelper.SelectAllItems(StatusFilter);
            }

            OperationTypeSelection.Initialize();
            InitializeMetrics();

            // Sort by metric
            TopXXMetric orderBymetric = LoadOrderByMetric();
            SortByDropdown.SelectedValue = orderBymetric.ToString();

            // Type of metric filter
            this.Resource.LoadEnumSelection<TopXXMetric>(MetricFilter, ResourcePropertiesHelper.TopXXMetricPrefix, false);

            if (!DropDownListHelper.IsSomethingSelected(MetricFilter))
            {
                foreach (ListItem listItem in MetricFilter.Items)
                {
                    if (listItem.Value == TopXXMetric.RoundTripTime.ToString())
                        listItem.Selected = true;
                    break;
                }
            }
        }
        else
            UpdateMetricAvailability();
    }

    private string LoadMaxCount()
    {
        string maxCount = this.Resource.Properties[ResourcePropertiesHelper.MaxCountProperty];
           
        if (string.IsNullOrEmpty(maxCount))
            maxCount = "10";

        return maxCount;
    }

    private TopXXMetric LoadOrderByMetric()
    {
        string value = Resource.Properties[ResourcePropertiesHelper.MetricOrderBy];

        if(String.IsNullOrEmpty(value) || !Enum.IsDefined(typeof(TopXXMetric), value))
            return TopXXMetric.RoundTripTime;

        return (TopXXMetric) Enum.Parse(typeof (TopXXMetric), value);
    }

    void UpdateMetricAvailability()
    {
        List<OperationType> selectedOperationTypes = OperationTypeSelection.SelectedTypes;

        string previousSortByValue = SortByDropdown.SelectedValue;
        ListItem selectedSortByValueItem = null;

        SortByDropdown.Items.Clear();

        foreach (ListItem listItem in MetricFilter.Items)
        {
            if (Enum.IsDefined(typeof(TopXXMetric), listItem.Value))
            {
                TopXXMetric metric = (TopXXMetric)Enum.Parse(typeof(TopXXMetric), listItem.Value);

                MetricCompatibilityAttribute attr = AttributeHelper.GetEnumAttribute<MetricCompatibilityAttribute>(metric);

                bool allOk = true;
                foreach (OperationType opType in selectedOperationTypes)
                {
                    if (!attr.CompatibleOperationTypes.Contains(opType))
                    {
                        allOk = false;
                        break;
                    }
                }

                listItem.Enabled = selectedOperationTypes.Count != 0 && allOk;
                listItem.Selected &= listItem.Enabled;

                if(listItem.Enabled)
                {
                    ListItem sort = new ListItem(listItem.Text, listItem.Value);
                    SortByDropdown.Items.Add(sort);

                    if (listItem.Value == previousSortByValue)
                        selectedSortByValueItem = listItem;
                }
            }
        }

        if (selectedSortByValueItem != null)
            SortByDropdown.SelectedValue = selectedSortByValueItem.Value;
    }

    private void InitializeMetrics()
    {
        foreach (TopXXMetric metric in Enum.GetValues(typeof(TopXXMetric)))
        {
            ListItem item = new ListItem(GetLocalizedProperty("TopXXMetric", metric.ToString()), metric.ToString());
            item.Selected = true;
            MetricFilter.Items.Add(item);
        }

        UpdateMetricAvailability();

        foreach (ListItem item in MetricFilter.Items)
        {
            if (!item.Enabled)
            {
                item.Selected = false;
            }
        }
    }

    protected void OperationTypeValidator_ServerValidate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = OperationTypeSelection.SelectedTypes.Count != 0;
    }

    protected void OperationStatusValidator_ServerValidate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = DropDownListHelper.IsSomethingSelected(StatusFilter);
    }

    protected void MetricValidator_ServerValidate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = DropDownListHelper.IsSomethingSelected(MetricFilter);
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (this.Page.IsValid)
        {
            if (!resourceTitleEditor.ResourceTitle.Equals(Resource.Title))
            {
                Resource.Title = resourceTitleEditor.ResourceTitle;
                SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
            }

            this.Resource.Properties[ResourcePropertiesHelper.MaxCountProperty] = this.MaxCount.Text;
            this.Resource.Properties[ResourcePropertiesHelper.MetricOrderBy] = this.SortByDropdown.SelectedValue;

            OperationTypeSelection.SaveChanges();
            ResourcePropertiesHelper.SaveEnumSelection<TopXXMetric>(this.Resource, MetricFilter, ResourcePropertiesHelper.TopXXMetricPrefix);
            ResourcePropertiesHelper.SaveEnumSelection<OperationStatus>(this.Resource, StatusFilter, ResourcePropertiesHelper.OperationStatusPrefix);

            GoBack(null, this.NetObjectID);
        }
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}
