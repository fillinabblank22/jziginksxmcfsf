﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true"
    CodeFile="EditTopXXIpSlaOperations.aspx.cs" Inherits="Orion_Voip_Resources_TopXX_EditTopXXIpSlaOperations" %>

<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register TagPrefix="ipslam" TagName="OperationTypeSelection" Src="~/Orion/Voip/Controls/OperationTypeSelection.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <style type="text/css">
        .settingsSection
        {
            padding-bottom: 20px;
        }
    </style>

    <h1><%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_AK1_167, this.Resource.Title) %></h1>
    
    <div style="padding-left: 16px;">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
        
        <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="false" />
        
        <div class="settingsSection">
            <b><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_146 %></b><br />
            <asp:TextBox runat="server" ID="MaxCount" />
            <asp:CompareValidator ID="CompareValidator3" runat="server" ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_AK1_213 %>"
                Operator="GreaterThanEqual" Type="Integer" ValueToCompare="1" ControlToValidate="MaxCount">*</asp:CompareValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="MaxCount"
                ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_AK1_214 %>">*</asp:RequiredFieldValidator>
        </div>
        
        <div class="settingsSection">
            <b><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_147 %></b><br />
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_148%><br />
            <asp:CustomValidator ID="OperationTypeValidator" runat="server"
                ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_149 %>" OnServerValidate="OperationTypeValidator_ServerValidate"></asp:CustomValidator>
            <ipslam:OperationTypeSelection runat="server" ID="OperationTypeSelection" AutoPostBack="true" CheckedByDefault="True" />
        </div>
                
        <div class="settingsSection">
            <b><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_150 %></b><br />
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_151 %><br />
            
            <asp:CustomValidator ID="MetricValidator" runat="server"
                ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_152 %>" OnServerValidate="MetricValidator_ServerValidate"></asp:CustomValidator>
            <asp:CheckBoxList ID="MetricFilter" runat="server" />
        </div>
        
        <div class="settingsSection">
            <b><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_153 %></b><br />
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_154 %><br />
            <asp:DropDownList ID="SortByDropdown" runat="server"  />
        </div>
        
        <div class="settingsSection">
            <b><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_155 %></b><br/>
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_156 %><br />
        
            <asp:CustomValidator ID="StatusFilterCustomValidator" runat="server"
                    ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_157 %>" OnServerValidate="OperationStatusValidator_ServerValidate"></asp:CustomValidator>
            <asp:CheckBoxList ID="StatusFilter" runat="server" />
        </div>
        
        <orion:LocalizableButton runat="server" D="btnSubmit" ToolTip="<%$ Resources : VNQMWebContent, VNQMWEBCODE_VB1_171 %>" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"/>
    </div>
</asp:Content>
