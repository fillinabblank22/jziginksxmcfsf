﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TimeEditBase.aspx.cs"  Inherits="Orion_Voip_Resources_TimeEditBase"
 MasterPageFile="~/Orion/ResourceEdit.master" Title="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_138%>" ValidateRequest="false" %>
<asp:Content ContentPlaceHolderID="HeadPlaceHolder" ID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/Voip.css" />
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainContent">
    <div>
    <asp:PlaceHolder ID="mainPlaceHolder" runat="server">
        <table>
            <tr style="height:50px">
                <td colspan="2">
                    <h1 style="padding-left: 0px"><%= Page.Title %></h1>
                </td>
            </tr>
            <tr>
                <td class="EditPageHeader">
                    <%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_63 %>
                </td>
                <td>
                    <asp:TextBox ID="tbxTitle" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="EditPageHeader">
                    <%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_64 %>
                </td>
                <td>
                    <asp:TextBox ID="tbxSubTitle" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="EditPageHeader">
                    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_139 %>
                </td>
                <td >
                    <asp:DropDownList ID="TimePeriodsList" runat="server"/>
                </td>
            </tr>
            <tr>
                <td>
                    <orion:LocalizableButton runat="server" ToolTip="<%$ Resources : VNQMWebContent, VNQMWEBCODE_VB1_171 %>" ID="SubmitButton" OnClick="SubmitButton_Click" LocalizedText="Submit" DisplayType="Primary"/>
                </td>
            </tr>
        </table>
    </asp:PlaceHolder>
    </div>
</asp:Content>
