<%@ Control Language="C#" ClassName="CallManagerBody" %>

<script runat="server">
    protected string GetStatusMessage(string pollingStatus)
    {
        return string.IsNullOrEmpty(pollingStatus) ? Resources.VNQMWebContent.VNQMWEBDATA_AK1_177 : pollingStatus;
    }
</script>

<asp:Repeater ID="CallManagerRepeater" runat="server">
    <ItemTemplate>
    <tr>
	    <td class="Property"><a href='<%#BaseResourceControl.GetViewLink((string)Eval("NetObjectID"))%>'><%#Eval("NodeName")%></a></td>
		
		<asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible='<%#Convert.ToBoolean(Eval("HasData"))%>'>
			<td><a href='<%#BaseResourceControl.GetViewLink((string)Eval("NetObjectID"))%>'><%#Eval("InactivePhonesPercentage", Resources.VNQMWebContent.VNQMWEBDATA_AK1_176)%></a></td>
			<td><a href='<%#BaseResourceControl.GetViewLink((string)Eval("NetObjectID"))%>'><%#Eval("ActivePhonesPercentage", Resources.VNQMWebContent.VNQMWEBDATA_AK1_176)%></a></td>
			<td><a href='<%#BaseResourceControl.GetViewLink((string)Eval("NetObjectID"))%>'><%#Eval("InactiveGatewaysPercentage", Resources.VNQMWebContent.VNQMWEBDATA_AK1_176)%></a></td>
			<td><a href='<%#BaseResourceControl.GetViewLink((string)Eval("NetObjectID"))%>'><%#Eval("ActiveGatewaysPercentage", Resources.VNQMWebContent.VNQMWEBDATA_AK1_176)%></a></td>
			<td style="white-space:nowrap"><%#Eval("LastPoll")%></td>
		</asp:PlaceHolder>
	
        <asp:PlaceHolder ID="PlaceHolder2" runat="server" Visible='<%#!Convert.ToBoolean(Eval("HasData")) %>'>  
		    <td colspan="5"><%# GetStatusMessage(Eval("PollingStatus") as string)%></td>
		</asp:PlaceHolder>
    </tr>		
    </ItemTemplate>
</asp:Repeater>
