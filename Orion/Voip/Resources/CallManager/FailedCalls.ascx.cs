﻿using System;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

namespace Orion.Voip.Resources.CallManager
{
    [ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.VOIP)]
    public partial class FailedCalls : BaseResourceControl, IXuiResource, IResourceIsInternal
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (GetInterfaceInstance<ICallManagerProvider>() == null)
            {
                ResourceWrapper.Visible = false;
            }
            else
            {
                ResourceWrapper.Visible = OrionMinReqsMaster.IsApolloEnabled;
                Selector = GetStringValue("selector", null);
            }
        }

        protected string Selector { get; set; }

        protected string SearchPageLinkText => "Search for all failed calls >>";

        protected override string DefaultTitle => "Failed Calls - Call Manager";

        public bool IsInternal => true;

        public override string HelpLinkFragment => "OrionIPSLAMonitorPHCallManagerFailedCalls";

    }
}