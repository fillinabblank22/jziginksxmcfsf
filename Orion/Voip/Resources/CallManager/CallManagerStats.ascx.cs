using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using CallManager = SolarWinds.Orion.IpSla.Web.CallManager;

public partial class Orion_Voip_Resources_CallManager_CallManagerStats : VoipBaseResource
{
	private int TotalPhones
	{
		get { return CallManager.ActivePhones + CallManager.InactivePhones + CallManager.RejectedPhones; }
	}

	private int TotalGateways
	{
		get { return CallManager.ActiveGateways + CallManager.InactiveGateways + CallManager.RejectedGateways; }
	}

	protected void CMDetails_Init(object sender, EventArgs e)
	{
		if (GetInterfaceInstance<ICallManagerProvider>() == null)
		{
			Wrapper.Visible = false;
			return;
		}

		CMDetails.RowDataBound += CMDetails_RowDataBound;
        //Correction percentage for phones
        float registeredPhonesPercentage = CallManager.ActivePhonesPercentage;
        float unregisteredPhonesPercentage = CallManager.InactivePhonesPercentage;
        float rejectedPhonesPercentage = CallManager.RejectedPhonesPercentage;
        PercentageCorrection(ref registeredPhonesPercentage, ref unregisteredPhonesPercentage, ref rejectedPhonesPercentage);

        //Correction percentage for gateways
        float registeredGatewaysPercentage = CallManager.ActiveGatewaysPercentage;
        float unregisteredGatewaysPercentage = CallManager.InactiveGatewaysPercentage;
        float rejectedGatewaysPercentage = CallManager.RejectedGatewaysPercentage;
        PercentageCorrection(ref registeredGatewaysPercentage, ref unregisteredGatewaysPercentage, ref rejectedGatewaysPercentage);

        DataTable table = new DataTable();
        table.Columns.Add(VNQMWebContent.VNQMWEBDATA_VB1_128);
        table.Columns.Add(VNQMWebContent.VNQMWEBDATA_VB1_219);
        table.Columns.Add(VNQMWebContent.VNQMWEBDATA_VB1_220);
        table.Rows.Add(new object[3] { VNQMWebContent.VNQMWEBCODE_VB1_196, CallManager.ActivePhones, this.Format(registeredPhonesPercentage) });
        table.Rows.Add(new object[3] { VNQMWebContent.VNQMWEBCODE_VB1_197, CallManager.InactivePhones, this.Format(unregisteredPhonesPercentage) });
		if (CallManager.CCMType == CallManagerType.CCM)
            table.Rows.Add(new object[3] { VNQMWebContent.VNQMWEBCODE_VB1_198, CallManager.RejectedPhones, this.Format(rejectedPhonesPercentage) });

		table.Rows.Add(new object[3] { VNQMWebContent.VNQMWEBCODE_VK2_05, this.TotalPhones, string.Empty });

		table.Rows.Add(new object[3]);//Add new line as separator between grid grops.

        table.Rows.Add(new object[3] { VNQMWebContent.VNQMWEBCODE_VB1_199, CallManager.ActiveGateways, this.Format(registeredGatewaysPercentage) });
        table.Rows.Add(new object[3] { VNQMWebContent.VNQMWEBCODE_VB1_200, CallManager.InactiveGateways, this.Format(unregisteredGatewaysPercentage) });

		if (CallManager.CCMType == CallManagerType.CCM)
            table.Rows.Add(new object[3] { VNQMWebContent.VNQMWEBCODE_VB1_201, CallManager.RejectedGateways, this.Format(rejectedGatewaysPercentage) });

		table.Rows.Add(new object[3] { VNQMWebContent.VNQMWEBCODE_VK2_06, this.TotalGateways, string.Empty });

		table.Rows.Add(new object[3]);//Add new line as separator between grid grops.
		table.Rows.Add(new object[3] { VNQMWebContent.VNQMWEBCODE_VB1_202, CallManager.LastPoll, string.Empty });

		CMDetails.DataSource = table;
		CMDetails.DataBind();


	}

	protected CallManager CallManager
	{
		get { return GetInterfaceInstance<ICallManagerProvider>().CallManager; }
	}

	public override IEnumerable<Type> RequiredInterfaces
	{
		get { return new Type[] { typeof(ICallManagerProvider) }; }
	}

	protected override string DefaultTitle
	{
		get { return VNQMWebContent.VNQMWEBCODE_VB1_203; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionVoIPMonitorPHResourceCallManagerStats"; }
	}

	private void CMDetails_RowDataBound(object sender, GridViewRowEventArgs e)
	{
		if (e.Row != null &&
			e.Row.RowType == DataControlRowType.DataRow &&
			(e.Row.Cells[0].Text == VNQMWebContent.VNQMWEBCODE_VK2_05 || e.Row.Cells[0].Text == VNQMWebContent.VNQMWEBCODE_VK2_06))
		{
			e.Row.CssClass = "voipCallManagerStatsDataGridSum";
		}

	}

    private void PercentageCorrection(ref float a, ref float b, ref float c)
    {
        float ar = (a > 0 && a <= 1) ? 1 : (float)Math.Round(a);
        float br = (b > 0 && b <= 1) ? 1 : (float)Math.Round(b);
        float cr = (c > 0 && c <= 1) ? 1 : (float)Math.Round(c);

        a = ar > br && ar > cr ? 100 - br - cr : ar;
        b = br >= ar && br > cr ? 100 - ar - cr : br;
        c = cr >= ar && cr >= br && cr != 0 ? 100 - ar - br : cr;
    }

    private string Format(float value)
	{
        return string.Format("{0:0}{1}", value, VNQMWebContent.VNQMWEBCODE_VB1_204);
	}
}
