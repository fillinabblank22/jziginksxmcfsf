﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DisconnectedCauses.ascx.cs"
    Inherits="Orion_Voip_Resources_CallManager_DisconnectedCauses" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Data" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
    <span><i><%= controlDescription %></i></span>
        <asp:Repeater runat="server" ID="DisconnectedCausesRepeater">
            <HeaderTemplate>
                <table style="margin-top: 10px;border-spacing:0" class="NeedsZebraStripes">
                    <thead>
                        <tr class="voipCallDetailsTableRow">
                            <td style="padding-left: 10px">
                                <%= DisconnectCause %>
                            </td>
                            <td style="width: 150px;">
                                <%= NumberOfCalls %>
                            </td>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="voipCallDetailsTableRow" runat="server">
                    <td style="padding-left: 10px">
                         <%#DataBinder.Eval(Container.DataItem, "CauseDescription")%>
                    </td>
                    <td style="width: 150px;">
                        <%#DataBinder.Eval(Container.DataItem, "TotalCount")%>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>
