﻿using System;
using System.Collections.Generic;
using Castle.Core.Internal;
using Resources;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

namespace Orion.Voip.Resources.CallManager
{
    [ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.VOIP)]
    public partial class OrionVoipResourcesSummaryCallManagerSipTrunks : VoipBaseResource
    {
        protected ResourceSearchControl SearchControl { get; private set; }
        protected int ScriptFriendlyResourceID => Math.Abs(Resource.ID);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (GetInterfaceInstance<ICallManagerProvider>() != null)
            {
                if (SipTrunks.GetSipTrunks().IsNullOrEmpty())
                {
                    Wrapper.Visible = false;
                }
                else
                {
                    SearchControl =
                        (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
                    Wrapper.HeaderButtons.Controls.Add(SearchControl);

                    CallManagerSipTrunksTable.UniqueClientID = ScriptFriendlyResourceID;
                    CallManagerSipTrunksTable.SWQL = SWQL;
                    CallManagerSipTrunksTable.SearchSWQL = SearchSWQL;
                }
            }
            else
            {
                Wrapper.Visible = false;
            }
        }

        private readonly string link = "\'/Orion/Voip/SipTrunkDetails.aspx?NetObject=VCCMSIP:\'+TOSTRING(SipTrunkId)";

        public string SWQL => $@"SELECT Name, {link} as [_LinkFor_Name], Description, DevicePool, Status
                            FROM Orion.IpSla.CCMSipTrunk 
                            WHERE VoipCCMMonitoringId = {CallManager.VoipCCMMonitoringID}
                            ORDER BY Name";

        public string SearchSWQL => $@"SELECT Name, {link} as [_LinkFor_Name], Description, DevicePool, Status
                            FROM Orion.IpSla.CCMSipTrunk  
                            WHERE VoipCCMMonitoringId = {CallManager.VoipCCMMonitoringID}
                            AND (Name LIKE '%${{SEARCH_STRING}}%' OR Description LIKE '%${{SEARCH_STRING}}%' OR DevicePool LIKE '%${{SEARCH_STRING}}%')
                            ORDER BY Name";

        protected CcmSipTrunks SipTrunks => new CcmSipTrunks(Request["NetObject"]);

        protected override string DefaultTitle => VNQMWebContent.WEBDATA_BO_SipTrunks;

        public override string HelpLinkFragment => "OrionIPSLAManagerAdministratorGuide_AddingCallManagersToVNQM";

        public override string EditURL => GetEditUrl(Resource);
        public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Ajax;
        protected SolarWinds.Orion.IpSla.Web.CallManager CallManager => GetInterfaceInstance<ICallManagerProvider>().CallManager;

        public override IEnumerable<Type> RequiredInterfaces => new Type[] { typeof(ICallManagerProvider) };

        private string GetEditUrl(ResourceInfo resource)
        {
            var netobject = Request["NetObject"];
            string url = $"/Orion/Voip/Resources/EditSipTrunks.aspx?ResourceID={resource.ID}";
            if (!string.IsNullOrEmpty(netobject))
                url = $"{url}&NetObject={netobject}";
            return url;
        }
    }
}