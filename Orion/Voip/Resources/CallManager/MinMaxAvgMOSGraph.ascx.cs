using System;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

namespace Orion.Voip.Resources.CallManager
{
    [ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.VOIP)]
    public partial class MinMaxAvgMOSGraph : BaseResourceControl, IXuiResource, IResourceIsInternal
    {
        protected void Page_Load(object o, EventArgs e)
        {
            if (GetInterfaceInstance<ICallManagerProvider>() == null)
            {
                ResourceWrapper.Visible = false;
            }
            else
            {
                ResourceWrapper.Visible = OrionMinReqsMaster.IsApolloEnabled;
                Selector = GetStringValue("selector", null);
            }
        }

        protected string Selector { get; set; }

        protected override string DefaultTitle => "VoIP Call Manager Min/Max/Avg MOS";

        public bool IsInternal => true;

        public override string HelpLinkFragment => "OrionVoIPMonitorPHChartMMAMOS";
    }
}
