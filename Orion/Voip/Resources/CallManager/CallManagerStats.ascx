<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CallManagerStats.ascx.cs" Inherits="Orion_Voip_Resources_CallManager_CallManagerStats" %>
<link rel="stylesheet" type="text/css" href="/Orion/Voip/Voip.css" />
<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
		<asp:GridView runat="server" ID="CMDetails" OnInit="CMDetails_Init" CssClass="voipCallManagerStatsDataGrid" ShowHeader="True"  >    
		</asp:GridView>
	</Content>
</orion:resourceWrapper>
