﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CallManagerSipTrunks.ascx.cs" Inherits="Orion.Voip.Resources.CallManager.OrionVoipResourcesSummaryCallManagerSipTrunks" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Common.Model" %>

<link rel="stylesheet" type="text/css" href="/Orion/Voip/Voip.css" />
<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
        <orion:CustomQueryTable runat="server" ID="CallManagerSipTrunksTable"/>
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= this.ScriptFriendlyResourceID %>,
                        initialPage: 0,
                        rowsPerPage: <%= this.Resource.Properties["CallManagerSipTrunksPageSize"] ?? "20" %>,
                        searchTextBoxId: '<%= this.SearchControl.SearchBoxClientID %>',
                        searchButtonId: '<%= this.SearchControl.SearchButtonClientID %>',
                        allowSort: true,
                        allowSearch: true,
				        allowPaging: true,
                        customFormatters: {
                            "Status": function(value) {
                                switch(value) {
                                    case 0:
                                        return "<%= Resources.VNQMWebContent.VNQMWEBJS_SipTrunkStatus_any %>";
                                    case 1:
                                        return "<%= Resources.VNQMWebContent.VNQMWEBJS_SipTrunkStatus_registered %>";
                                    case 2:
                                        return "<%= Resources.VNQMWebContent.VNQMWEBJS_SipTrunkStatus_unregistered %>";
                                    case 3:
                                        return "<%= Resources.VNQMWebContent.VNQMWEBJS_SipTrunkStatus_rejected %>";
                                    case 4:
                                        return "<%= Resources.VNQMWebContent.VNQMWEBJS_SipTrunkStatus_partiallyregistered %>";
                                    case 5:
                                        return "<%= Resources.VNQMWebContent.VNQMWEBJS_SipTrunkStatus_unknown %>";
                                }
                                return "Status is not polled yet";
                            }
                        }
                    });
		    
                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= this.ScriptFriendlyResourceID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= this.CallManagerSipTrunksTable.ClientID %>');
                refresh();
            });    
        </script>
	</Content>
</orion:resourceWrapper>