﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FailedCalls.ascx.cs" Inherits="Orion.Voip.Resources.CallManager.FailedCalls" %>
<orion:resourcewrapper id="ResourceWrapper" runat="server" showeditbutton="true" cssclass="XuiResourceWrapper">
    <Content>
        <orion:Include ID="Include1" runat="server" Module="Voip" File="Voip.css" />
        <div class="xui">
            <<%= Selector %>></<%= Selector %>>
        </div>
        <div class="voipSearchPageHyperLink">
            <asp:HyperLink ID="HyperLink1" CssClass="sw-link" runat="server" NavigateUrl="~/Orion/Voip/VoipCallSearch.aspx?CallTime=Last15Minutes&CallFailed=true"><%= SearchPageLinkText %></asp:HyperLink>
        </div>
    </Content>
</orion:resourcewrapper>
