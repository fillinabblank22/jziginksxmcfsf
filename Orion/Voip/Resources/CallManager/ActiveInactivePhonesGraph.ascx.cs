﻿using System;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

namespace Orion.Voip.Resources.CallManager
{
    [ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.VOIP)]
    public partial class ActiveInactivePhonesGraph : BaseResourceControl, IXuiResource, IResourceIsInternal
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var callmanagerProvider = GetInterfaceInstance<ICallManagerProvider>();
            if (callmanagerProvider != null && callmanagerProvider.CallManager.CCMType == CallManagerType.ACM)
            {
                ResourceWrapper.Visible = OrionMinReqsMaster.IsApolloEnabled;
                Selector = GetStringValue("selector", null);
            }
            else
            {
                ResourceWrapper.Visible = false;
            }
        }

        protected string Selector { get; set; }

        // Rejected Phones metric is not applicable for Avaya CM
        protected override string DefaultTitle => "Registered/Unregisterd Phones Chart";

        public bool IsInternal => true;

        public override string HelpLinkFragment => "OrionVoIPMonitorPHChartRegisteredUnregisteredPhones";
    }
}