using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class Orion_Voip_Resources_CallManager_CCMMsgQueue : System.Web.UI.UserControl
{
    public event EventHandler Close;
    
    public object DataSource
    {
        
        get { return Messages.DataSource; }
        set
        {
            Messages.DataSource = value;
            Messages.DataBind();
        }

    }

    protected void OnCloseClick(object sender, EventArgs e)
    {
        if (null != this.Close)
            this.Close(this, e);
    }
}
