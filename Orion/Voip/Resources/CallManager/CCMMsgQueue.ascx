<%@ Import namespace="SolarWinds.Orion.IpSla.Data"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CCMMsgQueue.ascx.cs" Inherits="Orion_Voip_Resources_CallManager_CCMMsgQueue" %>

<div  style="background-color:LightGrey; border-style:double">
<asp:Panel ID="MessageListPanel" runat="server" ScrollBars="Auto" Height="300px">
    <div id="pageHeader">
        <img src="<%= SolarWinds.Orion.Web.DAL.WebSettingsDAL.SiteLogo %>" id="Header" alt="Site Logo" />       
    </div>
    <br />
    <asp:Repeater ID="Messages" runat="server" >
	    <HeaderTemplate>
		    <table  width="100%"  cellspacing="0" cellpadding="0" >
	    </HeaderTemplate>
	
	    <ItemTemplate>
		    <tr>
		        <td style="border-bottom:1px groove black" width="10%">&nbsp;<img src="/Orion/Voip/images/MSG-<%#((CCMMessage)Container.DataItem).IsError ? "Error":"Info" %>.gif" border="0"></td>
			    <td style="border-bottom:1px groove black" width="90%"><%#((CCMMessage)Container.DataItem).Message %></td>
		    </tr>
	    </ItemTemplate>
	
	    <FooterTemplate>
		    </table>
	    </FooterTemplate>
    </asp:Repeater>
    
</asp:Panel>
<center>
    <asp:ImageButton runat="server" ID="btnClose" ImageUrl="~/Orion/voip/images/Button.Close.gif" OnClick="OnCloseClick" />
</center>
    <br />
</div>