<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConnectedPhones.ascx.cs" Inherits="Orion_Voip_Resources_CallManager_ConnectedPhones" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Common.Model" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<!-- This is needed if you want a search feature -->
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
 
 
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <!-- Include CustomQueryTable control in your resource -->
        <orion:CustomQueryTable runat="server" ID="CustomTable"/>
 
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= ScriptFriendlyResourceID %>,
                        initialPage: 0,
                        rowsPerPage: <%= Resource.Properties["ConnectedPhonesPageSize"] ?? "20" %>,
                        searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                        searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                        allowSort: true,
                        allowSearch: true,

                        customFormatters: {
                            "Registered": function(value) {
                                <% if (CallManager.CCMType == SolarWinds.Orion.IpSla.Data.CallManagerType.ACM)
                                   { %>
                                return "<%= Resources.VNQMWebContent.VNQMWEBCODE_VB1_244 %>";
                                <% }
                                   else
                                   { %>
                                return value;
                                <% } %>
                            },
                            "Status": function(value) {
                                if (value == 1) {
                                    return "<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_41 %>";
                                } else if (value == 2) {
                                    return "<%= Resources.VNQMWebContent.VNQMWEBDATA_PG_23 %>";
                                } else if (value == 3) {
                                    return "<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_158 %>";
                                } else if (value == 4) {
                                    return "<%= Resources.VNQMWebContent.VNQMWEBDATA_PG_24 %>";
                                } else if (value == 5) {
                                    return "<%= Resources.VNQMWebContent.VNQMWEBDATA_PG_25 %>";
                                }
                            },
                            "Licensed": function(value) {
                                if (value) {
                                    return "<%= Resources.VNQMWebContent.VNQMWEBCODE_AK1_12 %>";
                                } else {
                                    return "<%= Resources.VNQMWebContent.VNQMWEBCODE_AK1_13 %>";
                                }
                            }
                        }
                    });
 
                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });    
        </script>
    </Content>
</orion:resourceWrapper>