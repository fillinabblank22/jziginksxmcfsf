﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Data;
using System.Data;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Voip_Resources_CallManager_ConditionCode : VoipBaseResource
{
    protected static string ConditionCodeName = Resources.VNQMWebContent.VNQMWEBCODE_VB1_273;
    protected static readonly string NumberOfCalls = Resources.VNQMWebContent.VNQMWEBCODE_VB1_212;
    protected const string DefaultPeriodValue = "TODAY";
    protected static readonly string controlDescription = String.Empty;
    protected string Period = string.Empty;
    private string title = string.Empty;

    override public ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.Ajax; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        var callManagerProvider = GetInterfaceInstance<ICallManagerProvider>();
        if ((callManagerProvider != null) && (callManagerProvider.CallManager.CCMType == CallManagerType.ACM))
        {
            var callManager = callManagerProvider.CallManager;

            title = callManager.NodeName;

            var dateStart = DateTime.Now;
            var dateEnd = DateTime.Now;
            Period = GetPeriodData();
            Periods.Parse(ref Period, ref dateStart, ref dateEnd);

            var dt = callManager.GetDisconnectedCauseSummary(callManager.VoipCCMMonitoringID, callManager.CCMType, dateStart, dateEnd);
            dt.Columns.Add("CauseDescription");
            
            Type causeType = typeof(AvayaConditionCodes);
            string causeTypeName = causeType.Name;

            foreach (DataRow row in dt.Rows)
            {
                var causeValue = Convert.ToInt32(row["CauseValue"]);
                string property = Enum.GetName(causeType, causeValue);
                row["CauseDescription"] = GetLocalizedProperty(causeTypeName, property);
            }

            ConditionCodeRepeater.DataSource = dt;
            ConditionCodeRepeater.DataBind();
        }
        else
        {
            Wrapper.Visible = false;
        }
    }
    public override string DisplayTitle
    {
        get
        {
            return String.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_214, this.Resource.Title, title);
        }
    }
    public override string SubTitle
    {
        get
        {
            return String.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_215, this.Resource.SubTitle,
                                 GetLocalizedProperty("Period", Period.ToString()));
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionIPSLAMonitorPHConditionCode"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(ICallManagerProvider) }; }
    }
    public override string EditURL
    {
        get { return string.Format("/Orion/Voip/Resources/TimeEditBase.aspx?ResourceID={0}&NetObject={1}", this.Resource.ID, !string.IsNullOrEmpty(Request.Params["NetObject"]) ? Request.Params["NetObject"] : string.Empty); }
    }
    private string GetPeriodData()
    {
        string period = string.Empty;
        if (!string.IsNullOrEmpty(Request.Params["Period"]))
        {
            period = Request.Params["Period"];
        }
        else if (!string.IsNullOrEmpty(this.Resource.Properties["Period"]))
        {
            period = this.Resource.Properties["Period"];
        }
        else
        {
            period = DefaultPeriodValue;
        }
        return period;
    }

    private string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }

    protected override string DefaultTitle
    {
        get
        {
            return Resources.VNQMWebContent.VNQMWEBCODE_VB1_273;
        }
    }
    
}