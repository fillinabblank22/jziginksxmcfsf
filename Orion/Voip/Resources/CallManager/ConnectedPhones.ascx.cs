using System;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using CallManager = SolarWinds.Orion.IpSla.Web.CallManager;

public partial class Orion_Voip_Resources_CallManager_ConnectedPhones : VoipBaseResource
{
    protected ResourceSearchControl SearchControl { get; private set; }

    protected int ScriptFriendlyResourceID => Math.Abs(Resource.ID);

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetInterfaceInstance<ICallManagerProvider>() != null)
        {
            if (CCMPhonesDAL.Instance.GetCCMPhonesByNodeId(CallManager.NodeID).Count > 0)
            {
                // Load search control and add it to resource header area next to Edit button.
                // This is currently easiest way how to do that.
                SearchControl =
                    (ResourceSearchControl) LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
                Wrapper.HeaderButtons.Controls.Add(SearchControl);

                // Set unique ID CustomQueryTable instance that you put to .ascx file
                CustomTable.UniqueClientID = ScriptFriendlyResourceID;
                // Set SWQL query that loads data to display
                CustomTable.SWQL = SWQL;
                // Set search SWQL query that is used for searching
                CustomTable.SearchSWQL = SearchSWQL;
            }
            else
            {
                Wrapper.Visible = false;
            }
        }
        else
        {
            Wrapper.Visible = false;
        }
    }

    public string SWQL
    {
        get
        {
            if (CallManager.CCMType == CallManagerType.CCM)
            {
                return
                    $@"SELECT p.Name AS Name,
                '/Orion/Voip/CCMPhone.aspx?NetObject=VCCMP:' + TOSTRING(p.ID) AS [_LinkFor_Name],
                p.IPAddress AS IPAddress,
                p.Description AS Description,
                p.MACAddress AS MACAddress,
                cr.RegionName AS Location,
                ToLocal(cd.LastRegisteredUTC) AS Registered,
                p.Status AS Status,
                '/Orion/Voip/PhoneStatusGraph.aspx?phoneID=' + TOSTRING(p.ID) AS [_LinkFor_Status],
                p.Licensed AS Licensed
            FROM Orion.IpSla.CCMPhones as p
            INNER JOIN Orion.IpSla.CCMMonitoring ON p.CCMMonitoringID=CCMMonitoring.ID
            INNER JOIN Orion.Nodes ON CCMMonitoring.NodeID=Nodes.NodeID
            LEFT JOIN Orion.IpSla.CCMPhonesCiscoData as cd ON cd.CCMPhonesID=p.ID
            LEFT JOIN Orion.IpSla.CCMPhonesAvayaData as ad ON ad.CCMPhonesID=p.ID
            LEFT JOIN Orion.IpSla.CCMRegions as cr ON (cr.RegionID = cd.RegionID  OR cr.RegionID = ad.RegionID) AND cr.CCMMonitoringID = p.CCMMonitoringID
		    WHERE CCMMonitoring.Deleted<>1
            AND CCMMonitoring.NodeID = {CallManager.NodeID}
            ORDER BY Name, MACAddress";
            }
            return
                $@"SELECT p.Name AS Name,
                '/Orion/Voip/CCMPhone.aspx?NetObject=VCCMP:' + TOSTRING(p.ID) AS [_LinkFor_Name],
                p.IPAddress AS IPAddress,
                p.MACAddress AS MACAddress,
                cr.RegionName AS Location,
                p.Status AS Status,
                '/Orion/Voip/PhoneStatusGraph.aspx?phoneID=' + TOSTRING(p.ID) AS [_LinkFor_Status],
                p.Licensed AS Licensed
            FROM Orion.IpSla.CCMPhones as p
            INNER JOIN Orion.IpSla.CCMMonitoring ON p.CCMMonitoringID=CCMMonitoring.ID
            INNER JOIN Orion.Nodes ON CCMMonitoring.NodeID=Nodes.NodeID
            LEFT JOIN Orion.IpSla.CCMPhonesCiscoData as cd ON cd.CCMPhonesID=p.ID
            LEFT JOIN Orion.IpSla.CCMPhonesAvayaData as ad ON ad.CCMPhonesID=p.ID
            LEFT JOIN Orion.IpSla.CCMRegions as cr ON (cr.RegionID = cd.RegionID  OR cr.RegionID = ad.RegionID) AND cr.CCMMonitoringID = p.CCMMonitoringID
		    WHERE CCMMonitoring.Deleted<>1
            AND CCMMonitoring.NodeID = {CallManager.NodeID}
            ORDER BY Name, MACAddress";
        }
    }

    public string SearchSWQL
    {
        get
        {
            if (CallManager.CCMType == CallManagerType.CCM)
            {
                return
                    $@"SELECT p.Name AS Name,
                '/Orion/Voip/CCMPhone.aspx?NetObject=VCCMP:' + TOSTRING(p.ID) AS [_LinkFor_Name],
                p.IPAddress AS IPAddress,
                p.Description AS Description,
                p.MACAddress AS MACAddress,
                cr.RegionName AS Location,
                ToLocal(cd.LastRegisteredUTC) AS Registered,
                p.Status AS Status,
                '/Orion/Voip/PhoneStatusGraph.aspx?phoneID=' + TOSTRING(p.ID) AS [_LinkFor_Status],
                p.Licensed AS Licensed
            FROM Orion.IpSla.CCMPhones as p
            INNER JOIN Orion.IpSla.CCMMonitoring ON p.CCMMonitoringID=CCMMonitoring.ID
            INNER JOIN Orion.Nodes ON CCMMonitoring.NodeID=Nodes.NodeID
            LEFT JOIN Orion.IpSla.CCMPhonesCiscoData as cd ON cd.CCMPhonesID=p.ID
            LEFT JOIN Orion.IpSla.CCMPhonesAvayaData as ad ON ad.CCMPhonesID=p.ID
            LEFT JOIN Orion.IpSla.CCMRegions as cr ON (cr.RegionID = cd.RegionID  OR cr.RegionID = ad.RegionID) AND cr.CCMMonitoringID = p.CCMMonitoringID
		    WHERE CCMMonitoring.Deleted<>1
            AND CCMMonitoring.NodeID = {CallManager.NodeID}
            AND (Name LIKE '%${{SEARCH_STRING}}%' OR IPAddress LIKE '%${{SEARCH_STRING}}%' OR Description LIKE '%${{SEARCH_STRING}}%')    
            ORDER BY Name, MACAddress";
            }
            else
            {
                return
                    $@"SELECT p.Name AS Name,
                '/Orion/Voip/CCMPhone.aspx?NetObject=VCCMP:' + TOSTRING(p.ID) AS [_LinkFor_Name],
                p.IPAddress AS IPAddress,
                p.MACAddress AS MACAddress,
                cr.RegionName AS Location,
                p.Status AS Status,
                '/Orion/Voip/PhoneStatusGraph.aspx?phoneID=' + TOSTRING(p.ID) AS [_LinkFor_Status],
                p.Licensed AS Licensed
            FROM Orion.IpSla.CCMPhones as p
            INNER JOIN Orion.IpSla.CCMMonitoring ON p.CCMMonitoringID=CCMMonitoring.ID
            INNER JOIN Orion.Nodes ON CCMMonitoring.NodeID=Nodes.NodeID
            LEFT JOIN Orion.IpSla.CCMPhonesCiscoData as cd ON cd.CCMPhonesID=p.ID
            LEFT JOIN Orion.IpSla.CCMPhonesAvayaData as ad ON ad.CCMPhonesID=p.ID
            LEFT JOIN Orion.IpSla.CCMRegions as cr ON (cr.RegionID = cd.RegionID  OR cr.RegionID = ad.RegionID) AND cr.CCMMonitoringID = p.CCMMonitoringID
		    WHERE CCMMonitoring.Deleted<>1
            AND CCMMonitoring.NodeID = {CallManager.NodeID}
            AND (Name LIKE '%${{SEARCH_STRING}}%' OR IPAddress LIKE '%${{SEARCH_STRING}}%')    
            ORDER BY Name, MACAddress";
            }
        }
    }

    protected CallManager CallManager => GetInterfaceInstance<ICallManagerProvider>().CallManager;

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces => new Type[] {typeof (ICallManagerProvider)};

    protected override string DefaultTitle => Resources.VNQMWebContent.VNQMWEBCODE_VB1_207;

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Ajax;

    public override string HelpLinkFragment => "OrionVoIPMonitorPHResourceConnectedPhones";

    public override string EditURL => GetEditUrl(this.Resource);

    public string GetEditUrl(ResourceInfo resource)
    {
        string netobject = Request["NetObject"];
        string url = $"/Orion/Voip/Resources/EditConnectedPhones.aspx?ResourceID={resource.ID}";
        if (!string.IsNullOrEmpty(netobject))
            url = $"{url}&NetObject={netobject}";
        return url;
    }
}
