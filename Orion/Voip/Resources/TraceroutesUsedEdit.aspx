<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TraceroutesUsedEdit.aspx.cs" EnableEventValidation="false" Inherits="Resources_TraceroutesUsedEdit"
 MasterPageFile="~/Orion/ResourceEdit.master" Title="Edit graph properties" ValidateRequest="false" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
 <%@Register TagPrefix="orion" TagName="DateTimePicker" src="~/Orion/Controls/DateTimePicker.ascx" %>
 
<asp:Content ContentPlaceHolderID="HeadPlaceHolder" ID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/Voip.css" />
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainContent">
    <style type="text/css">
        .EditPageHeader{width: 220px !important;}
        .checkbox input{margin-left:0px;}
    </style>
    <div>
    <asp:PlaceHolder ID="mainPlaceHolder" runat="server">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
        <table>
            <tr style="height:50px">
                <td colspan="2">
                    <h1 style="padding-left: 0px;"><%= Page.Title %></h1>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <orion:EditResourceTitle runat="server"  ID="resourceTitleEditor" ShowSubTitle="false" ShowSubTitleHintMessage="false" SubTitleHintMessage=""/>
                </td>
            </tr>
            <tr>
                <td class="EditPageHeader">
                    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_211 %>
                </td>
                <td >
                    <asp:DropDownList ID="TimePeriodsList" runat="server"/>
                </td>
            </tr>
            <tr>
                <td class="EditPageHeader">
                    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_214%>
                </td>
                <td>
                    <asp:CheckBox ID="CheckBoxCustom" runat="server" CssClass="checkbox"/>
                </td>
            </tr>
            <tr class="CustomDate">
                <td class="EditPageHeader">
                    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_215%>
                </td>
                <td>
                    <orion:DateTimePicker ID="BeginDateTimePeriod" runat="server" Width="173"/>
                    <asp:CustomValidator ID="PeriodBeginCustomValidator" runat="server" ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_216 %>"
                        OnServerValidate="checkStartEndDate"> *</asp:CustomValidator>
                </td>        
            </tr>
            <tr class="CustomDate">
                <td class="EditPageHeader">
                    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_218%>
                </td>
                <td>
                    <orion:DateTimePicker ID="EndDateTimePeriod" runat="server" Width="173"/>
                     <asp:CustomValidator ID="PeriodEndCustomValidator" runat="server" ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_216 %>"
                         OnServerValidate="checkStartEndDate"> *</asp:CustomValidator>
                </td>
            </tr>
            <tr style="height:50px">
                <td>
				    <orion:LocalizableButton runat="server" ToolTip="<%$ Resources : VNQMWebContent, VNQMWEBDATA_AK1_215 %>" ID="SubmitButton" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitButton_Click" />   
                </td>
            </tr>
        </table>
    </asp:PlaceHolder>
    </div>
    
     <script type="text/javascript">
         function SetCustomDateVisibility() {
             if (document.getElementById('<%=CheckBoxCustom.ClientID %>').checked == true) {
                 $(".CustomDate").show();
                 document.getElementById('<%=TimePeriodsList.ClientID %>').disabled = true;
             } else {
                 $(".CustomDate").hide();
                 document.getElementById('<%=TimePeriodsList.ClientID %>').disabled = false;
             }

         }
         $('#<%=CheckBoxCustom.ClientID %>').change(SetCustomDateVisibility);
         SetCustomDateVisibility();
   </script>
</asp:Content>
