<%@ Page Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true" CodeFile="EditSitesOverview.aspx.cs" Inherits="Orion_Voip_Resources_EditSitesOverview" %>

<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register TagPrefix="voip" TagName="Grouping" Src="~/Orion/Voip/Controls/Grouping.ascx"%>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1 style="padding-left: 0px"><%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_AK1_167, Resource.Title) %></h1>
    
	<asp:ValidationSummary ID="ValidationSummary1" runat="server" />
    
    <b><orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="false" /></b>
    
    <b><voip:Grouping ID="CCMGroup" runat="server" /></b>
    <div class="sw-btn-bar" style="padding-top: 20px;">
        <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" ToolTip="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_215 %>" DisplayType="Primary" OnClick="SubmitClick" />
    </div>
</asp:Content>
