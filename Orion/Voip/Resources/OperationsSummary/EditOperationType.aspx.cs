﻿using System;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Web.UI;

public partial class Orion_Voip_Resources_Summary_EditOperationsByStatus : ResourceEditPage
{
    protected override void Initialize()
    {
        if (this.Resource != null)
        {
            this.resourceTitleEditor.ResourceTitle = this.Resource.Title;
            this.OperationTypeSelection.Resource = this.Resource;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            OperationTypeSelection.Initialize();
        }
    }

    protected void OperationTypeValidator_ServerValidate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = OperationTypeSelection.SelectedTypes.Count != 0;
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (this.Page.IsValid)
        {
            if (!resourceTitleEditor.ResourceTitle.Equals(Resource.Title))
            {
                Resource.Title = resourceTitleEditor.ResourceTitle;
                SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
            }

            OperationTypeSelection.SaveChanges();

            GoBack();
        }
    }
}
