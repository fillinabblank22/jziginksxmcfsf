<%@ Page Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true" CodeFile="EditAllOperations.aspx.cs" Inherits="Orion_Vip_Resources_Summary_EditNodeTree" Title="Untitled Page" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register TagPrefix="orion" TagName="FilterNodesSql" Src="~/Orion/Controls/FilterNodesSql.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= Server.HtmlEncode(Page.Title) %></h1>
    
    <div style="padding-left: 10px;">
    <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="true" ShowSubTitleHintMessage="true" SubTitleHintMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_158 %>"/>
    
    <div style="padding-top: 10px;padding-bottom: 10px" >
        <p>
            <b><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_197 %></b><br />
            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_198 %>
        </p>

        <p>
            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_199%><br />
            <asp:ListBox runat="server" ID="lbxGroup1" SelectionMode="single" Rows="1">
            </asp:ListBox>
        </p>

        <p>
            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_200%><br />
            <asp:ListBox runat="server" ID="lbxGroup2" SelectionMode="single" Rows="1">
            </asp:ListBox>
        </p>

        <p>
            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_201%><br />
            <asp:ListBox runat="server" ID="lbxGroup3" SelectionMode="single" Rows="1">
            </asp:ListBox>
        </p>
    </div>

    <div style="padding-bottom: 15px" >
        <p>
            <b><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_155%></b><br />
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_156%>
        </p>
        
         <p>
            <asp:CheckBoxList ID="StatusFilter" runat="server"></asp:CheckBoxList>
        </p>
    </div>
    <div style="padding-bottom: 15px" >
        <p>
            <b><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_147%></b><br />
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_148%>
        </p>
        
         <p>
            <asp:CheckBoxList ID="OperationTypeFilter" runat="server"></asp:CheckBoxList>
        </p>
    </div>
    <div style="padding-bottom: 15px" >
        <p>
            <b><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_159%></b><br />
        </p>
        
         <p>
            <asp:CheckBox ID="CheckBoxShowOperationTraceroute" runat="server" Text="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_160 %>"></asp:CheckBox>
        </p>
    </div>
    <orion:LocalizableButton runat="server" ID="btnSubmit" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick" ToolTip="<%$ Resources : VNQMWebContent, VNQMWEBCODE_VB1_171 %>"/>

    </div>
</asp:Content>

