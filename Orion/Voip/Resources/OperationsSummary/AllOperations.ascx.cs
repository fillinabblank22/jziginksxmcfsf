﻿using System;
using System.Linq;
using System.Text;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.WebServices.AllQualityTestsTree;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.IPSLA)]
public partial class Orion_Voip_Resources_Summary_AllQualityTests : VoipBaseResource
{
    private object[] _groupings;
    protected override bool DefaultLoadingMode => true;
    protected override string DefaultTitle => Resources.VNQMWebContent.VNQMWEBCODE_VB1_228;

    public override string EditURL
        =>
            $"/Orion/Voip/Resources/OperationsSummary/EditAllOperations.aspx?ResourceID={this.Resource.ID}";

    public override string HelpLinkFragment => "OrionIPSLAMonitorPHResourceAllOperations";

    public override string SubTitle
    {
        get
        {
            if (string.IsNullOrEmpty(base.SubTitle) || base.SubTitle.Trim().Length == 0)
                return BuildSubTitle();

            return base.SubTitle;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Wrapper.ManageButtonImage = "/Orion/Voip/Images/Button.ManageOperations.gif";
        Wrapper.ManageButtonTarget = "/Orion/Voip/Admin/ManageOperations.aspx";
        Wrapper.ShowManageButton = Profile.AllowAdmin || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;

        _groupings = LoadResourceGrouping();

        var provider = GetInterfaceInstance<INodeProvider>();
        string netObjectID = string.Empty;

        if (provider != null)
        {
            netObjectID = provider.Node.NetObjectID;

            var voipNodes = VoipNode.GetVoipSimulationNodes();
            if (voipNodes.Any(voipNode => voipNode.NetObjectID == netObjectID))
            {
                Resource.Title = string.Format("{0} {1}", Resource.Title, provider.Node.Name);
            }
            else
            {
                Wrapper.Visible = false;
                return;
            }
        }

        AjaxTree.LoadTree(new AllQualityTestsTreeService(), new object[]
        {
            _groupings,
            new object[] {},
            this.Resource.ID,
            netObjectID
        });
    }

    private string BuildSubTitle()
    {
        if (_groupings.Length == 0 || string.IsNullOrEmpty((string) _groupings[0]))
            return Resources.VNQMWebContent.VNQMWEBCODE_AK1_62;

        StringBuilder subtitle = new StringBuilder(string.Empty);

        for (int i = 0; i < _groupings.Length; i++)
        {
            string grouping = (string) _groupings[i];

            if (i > 0 && QualityTestGrouping.None.ToString().Equals(grouping, StringComparison.OrdinalIgnoreCase))
                break;

            if (string.IsNullOrEmpty(grouping))
                break;

            if (i > 0)
                subtitle.Append(Resources.VNQMWebContent.VNQMWEBCODE_VB1_229);

            subtitle.Append(GetGroupingDescription(grouping));
        }

        return string.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_63, subtitle.ToString());
    }

    private static string GetGroupingDescription(string grouping)
    {
        if (string.IsNullOrEmpty(grouping))
            return QualityTestGrouping.None.ToString();

        try
        {
            QualityTestGrouping enumVal = (QualityTestGrouping) Enum.Parse(typeof (QualityTestGrouping), grouping, true);
            return GetLocalizedProperty("QualityTestGrouping", enumVal.ToString());
        }
        catch (Exception)
        {
        }

        return grouping;
    }

    private object[] LoadResourceGrouping()
    {
        QualityTestGrouping? grouping1 = GetGrouping(AllQualityTestsTreeService.GroupingPropertyName1);
        QualityTestGrouping? grouping2 = null;
        QualityTestGrouping? grouping3 = null;

        if (grouping1.HasValue)
        {
            grouping2 = GetGrouping(AllQualityTestsTreeService.GroupingPropertyName2);

            if (grouping2.HasValue)
            {
                grouping3 = GetGrouping(AllQualityTestsTreeService.GroupingPropertyName3);
            }
        }

        if (grouping1 == null || grouping1 == QualityTestGrouping.None)
        {
            //Use default grouping
            grouping1 = QualityTestGrouping.OperationType;
            grouping2 = QualityTestGrouping.SourceNode;
            grouping3 = QualityTestGrouping.None;
        }

        //QualityTestGrouping.Operation grouping has to be implicitly last
        object[] arr = new object[4];
        arr[0] = grouping1.Value.ToString();
        if (grouping2.HasValue && grouping2.Value != QualityTestGrouping.None)
        {
            arr[1] = grouping2.Value.ToString();
            if (grouping3.HasValue && grouping3.Value != QualityTestGrouping.None)
            {
                arr[2] = grouping3.Value.ToString();
                arr[3] = QualityTestGrouping.Operation.ToString();
            }
            else
            {
                arr[2] = QualityTestGrouping.Operation.ToString();
                arr[3] = QualityTestGrouping.None.ToString();
            }

        }
        else
        {
            arr[1] = QualityTestGrouping.Operation.ToString();
            arr[2] = QualityTestGrouping.None.ToString();
            arr[3] = QualityTestGrouping.None.ToString();
        }

        return arr;
    }

    private QualityTestGrouping? GetGrouping(string propertyName)
    {
        string value = this.Resource.Properties[propertyName];
        if (string.IsNullOrEmpty(value))
            return null;

        if (!Enum.IsDefined(typeof (QualityTestGrouping), value))
            return null;

        return (QualityTestGrouping) Enum.Parse(typeof (QualityTestGrouping), value);
    }

    protected static string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}
