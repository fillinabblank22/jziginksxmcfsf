﻿using System;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Web.UI;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Data;

public partial class Orion_Voip_Resources_Summary_EditOperationsByStatus : ResourceEditPage
{
    protected override void Initialize()
    {
        if (this.Resource != null)
        {
            this.resourceTitleEditor.ResourceTitle = this.Resource.Title;
            this.OperationTypeSelection.Resource = this.Resource;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            DropDownListHelper.GenerateCheckBoxListItems(StatusFilter, typeof(OperationStatus));
            ResourcePropertiesHelper.LoadEnumSelection <OperationStatus>(this.Resource, StatusFilter, ResourcePropertiesHelper.OperationStatusPrefix);
            if (!DropDownListHelper.IsSomethingSelected(StatusFilter))
            {
                DropDownListHelper.SelectAllItems(StatusFilter);
            }

            OperationTypeSelection.Initialize();
        }
    }

    protected void OperationTypeValidator_ServerValidate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = OperationTypeSelection.SelectedTypes.Count != 0;
    }

    protected void OperationStatusValidator_ServerValidate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = DropDownListHelper.IsSomethingSelected(StatusFilter);
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (this.Page.IsValid)
        {
            if (!resourceTitleEditor.ResourceTitle.Equals(Resource.Title))
            {
                Resource.Title = resourceTitleEditor.ResourceTitle;
                SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
            }

            OperationTypeSelection.SaveChanges();

            ResourcePropertiesHelper.SaveEnumSelection<OperationStatus>(this.Resource, StatusFilter, ResourcePropertiesHelper.OperationStatusPrefix);

            GoBack();
        }
    }
}
