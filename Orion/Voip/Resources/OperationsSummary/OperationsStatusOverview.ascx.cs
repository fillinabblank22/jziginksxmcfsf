﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using Infragistics.UltraChart.Resources.Appearance;
using Infragistics.UltraChart.Shared.Styles;
using Resources;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.IpSla.Common;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.Charting;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.IPSLA)]
public partial class Orion_Voip_Resources_Summary_WanQualityTestOverview : VoipBaseResource
{

    private int countUp = 0;
    private int countUpWarning = 0;
    private int countUpCritical = 0;
    private int countDown = 0;
    private int countUnknown = 0;
    private int countUnreachable = 0;

    protected override string DefaultTitle => VNQMWebContent.VNQMWEBCODE_VB1_234;

    public override string EditURL
    {
        get
        {
            string url = string.Format("/Orion/Voip/Resources/OperationsSummary/EditOperationType.aspx?ResourceID={0}", this.Resource.ID);
            return url;
        }
    }

    public override string HelpLinkFragment => "OrionIPSLAMonitorPHResourceOperationsHealthOverview";

    private static readonly Color[] ChartColors = new Color[]
    {
        StatusColors.ColorUp,
        StatusColors.ColorUpWarning,
        StatusColors.ColorUpCritical,
        StatusColors.ColorDown,
        StatusColors.ColorUnknown,
        StatusColors.ColorUnreachable,
    };

    private int _sourceNodeID;
    private List<OperationType> _operationTypes;

    public int CountUp => countUp;

    public int CountUpWarning => countUpWarning;

    public int CountUpCritical => countUpCritical;

    public int CountDown => countDown;

    public int CountUnknown => countUnknown;

    public int CountUnreachable => countUnreachable;

    protected void Page_Load(object sender, EventArgs e)
    {
        Wrapper.ShowManageButton = Profile.AllowAdmin;

        ConfigureChart();
        LoadData();
    }

    private void LoadData()
    {
        using (DataTable table = GetData())
        {
            foreach (DataRow row in table.Rows)
            {
                int statusID = (short) row["OperationStatusID"];
                if (!Enum.IsDefined(typeof (OperationStatus), statusID))
                    continue;

                OperationStatus status = (OperationStatus) statusID;

                int count = (int) row["StatusCount"];

                ProcessRecord(status, count);
            }

            if (table.Rows.Count != 0)
            {
                ////
                //// Add the values to the series.  Order matters.  We want the order to match
                //// the order of the colors array, ChartColors
                ////
                NumericSeries series = new NumericSeries();
                series.Points.Add(new NumericDataPoint(countUp, VNQMWebContent.VNQMWEBDATA_AK1_33, false));
                series.Points.Add(new NumericDataPoint(countUpWarning, VNQMWebContent.VNQMWEBDATA_AK1_35, false));
                series.Points.Add(new NumericDataPoint(countUpCritical, VNQMWebContent.VNQMWEBDATA_AK1_37, false));
                series.Points.Add(new NumericDataPoint(countDown, VNQMWebContent.VNQMWEBDATA_AK1_39, false));
                series.Points.Add(new NumericDataPoint(countUnknown, VNQMWebContent.VNQMWEBDATA_AK1_41, false));
                series.Points.Add(new NumericDataPoint(countUnreachable, VNQMWebContent.VNQMWEBCODE_VB1_235, false));

                pieChart.Series.Add(series);
            }
            else
            {
                NumericSeries series = new NumericSeries();
                series.Points.Add(new NumericDataPoint(1, VNQMWebContent.VNQMWEBDATA_AK1_33, false));
                pieChart.Series.Add(series);
                pieChart.Tooltips.Display = TooltipDisplay.Never;
            }
        }
    }

    private int GetSourceNodeID()
    {
        string netObject = this.Request["NetObject"];

        if (String.IsNullOrEmpty(netObject))
            return 0;

        NetObjectHelper helper;
        if (NetObjectHelper.TryParse(netObject, out helper) && helper.IsNPMNode())
            return helper.Id;

        return 0;
    }

    private DataTable GetData()
    {
        _operationTypes = this.Resource.LoadSelectedValues<OperationType>(ResourcePropertiesHelper.OperationTypePrefix, false);

        _sourceNodeID = GetSourceNodeID();

        if (_sourceNodeID > 0)
        {
            return OperationsDAL.GetOperationStatusOverviewForNode(_sourceNodeID, _operationTypes.ToArray());
        }
        else
        {
            return OperationsDAL.GetOperationStatusOverview(_operationTypes.ToArray());
        }
    }

    protected string GetStatus(OperationStatus status)
    {
        string description = GetLocalizedProperty("OperationStatus", status.ToString());

        if (status == OperationStatus.Unknown)
            return description;

        return String.Format(VNQMWebContent.VNQMWEBCODE_VB1_233, description);
    }

    protected string GetOverviewPageUrl(OperationStatus status)
    {
        UrlBuilder builder = new UrlBuilder("/Orion/Voip/OperationOverviewDetails.aspx");

        builder["OperationStatus"] = status.ToString();

        if (_sourceNodeID > 0)
            builder["NetObject"] = "N:" + _sourceNodeID;

        if(_operationTypes.Count > 0)
        {
            builder["OperationType"] = _operationTypes.Concatenate(",");
        }

        return builder.Url;
    }

    void ProcessRecord(OperationStatus operationStatus, int count)
    {
        switch (operationStatus)
        {
            case OperationStatus.Up:
                countUp += count;
                break;
            case OperationStatus.Warning:
                countUpWarning += count;
                break;
            case OperationStatus.Critical:
                countUpCritical += count;
                break;
            case OperationStatus.Unknown:
                countUnknown += count;
                break;
            case OperationStatus.Down:
                countDown += count;
                break;
            case OperationStatus.Unreachable:
                countUnreachable += count;
                break;
        }
    }

    private void ConfigureChart()
    {
        pieChart.Transform3D.XRotation = 7;
        pieChart.Transform3D.YRotation = 3;
        pieChart.Transform3D.ZRotation = -55;
        pieChart.Transform3D.Perspective = 50;
        pieChart.Transform3D.Scale = 100;

        pieChart.PieChart3D.RadiusFactor = 129;
        pieChart.PieChart3D.PieThickness = 9;

        pieChart.ColorModel.CustomPalette = ChartColors;
        pieChart.ColorModel.ModelStyle = ColorModels.CustomLinear;
    }

    public static string HtmlColor(Color c)
    {
        return ColorTranslator.ToHtml(c);
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}
