﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OperationsStatusOverview.ascx.cs" 
    Inherits="Orion_Voip_Resources_Summary_WanQualityTestOverview" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Data"%>

<%@ Register Assembly="Infragistics2.WebUI.UltraWebChart.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebChart" TagPrefix="igChart" %>

<%@ Import Namespace="SolarWinds.Orion.IpSla.Web" %>

<%@ Import Namespace="SolarWinds.Orion.IpSla.Web.Charting" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
 <Content>
  <div>
    <div id="errorMessage" visible="false" runat="server" >
        <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_174 %>
    </div>
    <asp:PlaceHolder id="content" visible="true" runat="server">
        <table  class="ipsla_removeBottomTableBorder" cellpadding="0" cellspacing="0" border="0" css="opsOverview" style="height: 165px">
            <tr style="border: 0 none">
                <td style="border: solid 0 White" id="overviewPie">
                    <igchart:UltraChart runat="server" ID="pieChart"  ImagePipePageName="/Orion/VoIP/Controls/ChartImagePipe.aspx"
                        BackColor="White" Height="150px" Width="200px" ChartType="PieChart3D" 
                        Transform3D-Scale="100" Transform3D-XRotation="7" Transform3D-YRotation="3" Transform3D-ZRotation="-55" >

                        <Tooltips Display="MouseMove" BackColor="Yellow" BorderColor="Black" Format="Custom" 
                        FormatString=" <ITEM_LABEL>: <DATA_VALUE:0> " BorderThickness="0" 
                        Font-Names="Arial,Verdana,Helvetica,sans-serif" Font-Size="8" Padding="10" />
                        
                        <Border DrawStyle="Solid" Raised="false" Color="White" Thickness="0" />
                        
                        <DeploymentScenario Scenario="Session" />
                        
                        <PieChart3D OthersCategoryPercent="0" PieThickness="9" RadiusFactor="129">
                            <Labels Visible="False" FormatString="" LeaderLinesVisible="False" BorderColor="White" />
                        </PieChart3D>
                        
                        <Axis>
                            <PE ElementType="None" />
                        </Axis>
                    </igchart:UltraChart>
                </td>
                <td style="border: 0 none; white-space: nowrap; vertical-align:middle" id="leftOverviewStatuses" runat="server" >
                    <table class="NeedsZebraStripes" border="0">
                      <tr>
                        <td style="text-align: right; border-width: 0; color: <%= HtmlColor(StatusColors.ColorUp) %>;">
                          <%= this.CountUp %>
                        </td>
                        <td style="border-width: 0;">
                          <a href="<%= GetOverviewPageUrl(OperationStatus.Up) %>">
                            <img src="<%= IpSlaOperationHelper.GetStatusImageUrl(OperationStatus.Up)%>" alt="Up"/>
                            <%= this.GetStatus(OperationStatus.Up)%>
				          </a>
                        </td>
                      </tr>
                      <tr>
                        <td style="text-align: right; border-width: 0; color: <%= HtmlColor(StatusColors.ColorUpWarning) %>;">
                          <%= this.CountUpWarning %>
                        </td>
                        <td style="border-width: 0;">
                          <a href="<%= GetOverviewPageUrl(OperationStatus.Warning) %>">
                            <img src="<%= IpSlaOperationHelper.GetStatusImageUrl(OperationStatus.Warning)%>" alt="Warning"/>
                            <%= this.GetStatus(OperationStatus.Warning)%>
				          </a>
                        </td>
                      </tr>
                      <tr>
                        <td style="text-align: right; border-width: 0; color: <%= HtmlColor(StatusColors.ColorUpCritical) %>;">
                          <%= this.CountUpCritical %>
                        </td>
                        <td style="border-width: 0;">
                          <a href="<%= GetOverviewPageUrl(OperationStatus.Critical) %>">
                            <img src="<%= IpSlaOperationHelper.GetStatusImageUrl(OperationStatus.Critical)%>" alt="Critical"/>
                            <%= this.GetStatus(OperationStatus.Critical)%>
				          </a>
                        </td>
                      </tr>
                      <tr>
                        <td style="text-align: right; border-width: 0; color: <%= HtmlColor(StatusColors.ColorDown) %>;">
                          <%= this.CountDown %>
                        </td>
                        <td style="border-width: 0;">
                          <a href="<%= GetOverviewPageUrl(OperationStatus.Down) %>">
                            <img src="<%= IpSlaOperationHelper.GetStatusImageUrl(OperationStatus.Down)%>" alt="Down"/>
                            <%= this.GetStatus(OperationStatus.Down)%>
				          </a>
                        </td>
                      </tr>
                      <tr>
                        <td style="text-align: right; border-width: 0; color: <%= HtmlColor(StatusColors.ColorUnknown) %>;">
                          <%= this.CountUnknown %>
                        </td>
                        <td style="border-width: 0;">
                          <a href="<%= GetOverviewPageUrl(OperationStatus.Unknown) %>">
                            <img src="<%= IpSlaOperationHelper.GetStatusImageUrl(OperationStatus.Unknown)%>" alt="Unknown"/>
                            <%= this.GetStatus(OperationStatus.Unknown)%>
				          </a>
                        </td>
                      </tr>
                      <tr>
                        <td style="text-align: right; border-width: 0; color: <%= HtmlColor(StatusColors.ColorUnreachable) %>;">
                          <%= this.CountUnreachable %>
                        </td>
                        <td style="border-width: 0;">
                          <a href="<%= GetOverviewPageUrl(OperationStatus.Unreachable) %>">
                            <img src="<%= IpSlaOperationHelper.GetStatusImageUrl(OperationStatus.Unreachable)%>" alt="Unreachable"/>
                            <%= this.GetStatus(OperationStatus.Unreachable)%>
				          </a>
                        </td>
                      </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:PlaceHolder>        
</div>
        
</Content>
</orion:resourceWrapper>
