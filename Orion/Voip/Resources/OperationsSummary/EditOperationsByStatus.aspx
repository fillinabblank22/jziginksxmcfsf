﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true"
    CodeFile="EditOperationsByStatus.aspx.cs" Inherits="Orion_Voip_Resources_Summary_EditOperationsByStatus" %>

<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register TagPrefix="ipslam" TagName="OperationTypeSelection" Src="~/Orion/Voip/Controls/OperationTypeSelection.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <style type="text/css">
        .settingsSection
        {
            padding-bottom: 20px;
        }
    </style>

    <h1><%= String.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_216, this.Resource.Title) %></h1>
    
    <div style="padding-left: 10px;">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
        
        <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="false" />
                
        <div class="settingsSection">
            <b><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_147 %></b><br />
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_148 %><br />
            <ipslam:OperationTypeSelection runat="server" ID="OperationTypeSelection" AutoPostBack="true" CheckedByDefault="True" />
            <asp:CustomValidator ID="OperationTypeValidator" runat="server"
                ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_149 %>" OnServerValidate="OperationTypeValidator_ServerValidate"></asp:CustomValidator>
        </div>
		        
        <div class="settingsSection">
            <b><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_155%></b><br/>
            <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_156%><br />
        
            <asp:CheckBoxList ID="StatusFilter" runat="server" />
            <asp:CustomValidator ID="StatusFilterCustomValidator" runat="server" 
                    ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_157 %>" OnServerValidate="OperationStatusValidator_ServerValidate"></asp:CustomValidator>
        </div>
        
        <orion:LocalizableButton runat="server" ID="btnSubmit" ToolTip="<%$ Resources : VNQMWebContent, VNQMWEBCODE_VB1_171 %>" DisplayType="Primary" LocalizedText="Submit"
            OnClick="SubmitClick" />
    </div>
</asp:Content>
