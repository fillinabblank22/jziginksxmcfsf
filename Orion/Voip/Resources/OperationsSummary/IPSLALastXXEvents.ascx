﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPSLALastXXEvents.ascx.cs" Inherits="Orion_Voip_Resources_Summary_IPSLALastXXEvents" %>
<%@ Register Src="~/Orion/Controls/EventsReportControl.ascx" TagName="Events" TagPrefix="orion" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
		<orion:Events runat="server" ID="EventsControl" />
	</Content>
</orion:resourceWrapper>

