using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.IpSla.Web.UI;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web.WebServices.AllQualityTestsTree;

public partial class Orion_Vip_Resources_Summary_EditNodeTree : ResourceEditPage
{

    protected override void Initialize()
    {
        this.resourceTitleEditor.ResourceTitle = this.Resource.Title;

        if (!this.IsPostBack)
        {
            DropDownListHelper.GenerateListBoxItems(lbxGroup1, typeof(QualityTestGrouping), new object[] { QualityTestGrouping.Operation });
            DropDownListHelper.GenerateListBoxItems(lbxGroup2, typeof(QualityTestGrouping), new object[] { QualityTestGrouping.Operation });
            DropDownListHelper.GenerateListBoxItems(lbxGroup3, typeof(QualityTestGrouping), new object[] { QualityTestGrouping.Operation });

            DropDownListHelper.GenerateCheckBoxListItems(StatusFilter, typeof(OperationStatus));
            GenerateOperationTypes(OperationTypeFilter);

            bool firstTimeEdit = this.Resource.Properties[AllQualityTestsTreeService.GroupingPropertyName1] == null;
            if (firstTimeEdit)
            {
                lbxGroup1.SelectedValue = QualityTestGrouping.SourceNode.ToString();
                lbxGroup2.SelectedValue = QualityTestGrouping.Path.ToString();
                lbxGroup3.SelectedValue = QualityTestGrouping.OperationType.ToString();
            }
            else
            {
                SelectListBoxItem(lbxGroup1, this.Resource.Properties[AllQualityTestsTreeService.GroupingPropertyName1]);
                SelectListBoxItem(lbxGroup2, this.Resource.Properties[AllQualityTestsTreeService.GroupingPropertyName2]);
                SelectListBoxItem(lbxGroup3, this.Resource.Properties[AllQualityTestsTreeService.GroupingPropertyName3]);


                this.Resource.LoadEnumSelection<OperationType>(OperationTypeFilter, ResourcePropertiesHelper.OperationTypePrefix);
                this.Resource.LoadEnumSelection<OperationStatus>(StatusFilter, ResourcePropertiesHelper.OperationStatusPrefix);

                FixupBlankGroupings();

            }
            CheckBoxShowOperationTraceroute.Checked = 
                this.Resource.Properties[AllQualityTestsTreeService.ShowOperationTraceroutePropertyName] != null
                    ? Convert.ToBoolean(
                        this.Resource.Properties[AllQualityTestsTreeService.ShowOperationTraceroutePropertyName])
                    : AllQualityTestsTreeService.ShowOperationTracerouteDefaultValue;

	        this.resourceTitleEditor.ResourceSubTitle = this.Resource.SubTitle;
        }
    }

    

    void GenerateOperationTypes(CheckBoxList checkBoxList)
    {
        foreach (object value in IpSlaOperationHelper.GetSupportedOperationTypes())
        {
            ListItem item = new ListItem(GetLocalizedProperty("OperationType", value.ToString()), value.ToString());
            item.Selected = true;
            checkBoxList.Items.Add(item);
        }
    }


    QualityTestGrouping SelectListBoxItem(ListBox listBox, string value)
    {
        QualityTestGrouping grouping = QualityTestGrouping.None;
        if (!String.IsNullOrEmpty(value))
        {
            if (Enum.IsDefined(typeof(QualityTestGrouping), value))
            {
                grouping = (QualityTestGrouping)Enum.Parse(typeof(QualityTestGrouping), value);
            }
        }

        listBox.SelectedValue = grouping.ToString();
        return grouping;
    }

    private int ValidGroups
    {
        get
        {
            if (IsValidGroup(this.lbxGroup1.SelectedValue))
                return 0;
            if (IsValidGroup(this.lbxGroup2.SelectedValue))
                return 1;
            if (IsValidGroup(this.lbxGroup3.SelectedValue))
                return 2;

            return 3;
        }
    }

    private static bool IsValidGroup(string value)
    {
        return !string.IsNullOrEmpty(value)
                && !"none".Equals(value, StringComparison.OrdinalIgnoreCase);
    }

	private void FixupBlankGroupings()
	{
		List<string> groups = new List<string>();
		groups.Add(lbxGroup1.SelectedValue);
		groups.Add(lbxGroup2.SelectedValue);
		groups.Add(lbxGroup3.SelectedValue);

        groups.RemoveAll(string.IsNullOrEmpty);
        while (groups.Contains(QualityTestGrouping.None.ToString()))
        {
            groups.Remove(QualityTestGrouping.None.ToString());
        }
		
		for (int i = groups.Count; i < 3; ++i)
            groups.Add(QualityTestGrouping.None.ToString());

		lbxGroup1.SelectedValue = groups[0];
		lbxGroup2.SelectedValue = groups[1];
		lbxGroup3.SelectedValue = groups[2];
	}

	protected void SubmitClick(object sender, EventArgs e)
    {
		FixupBlankGroupings();

		Resource.Title = resourceTitleEditor.ResourceTitle;
		Resource.SubTitle = resourceTitleEditor.ResourceSubTitle;
		SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);

        this.Resource.Properties[AllQualityTestsTreeService.GroupingPropertyName1] = this.lbxGroup1.SelectedValue;
        this.Resource.Properties[AllQualityTestsTreeService.GroupingPropertyName2] = this.lbxGroup2.SelectedValue;
        this.Resource.Properties[AllQualityTestsTreeService.GroupingPropertyName3] = this.lbxGroup3.SelectedValue;

        this.Resource.SaveEnumSelection<OperationStatus>(StatusFilter, ResourcePropertiesHelper.OperationStatusPrefix);
        this.Resource.SaveEnumSelection<OperationType>(OperationTypeFilter, ResourcePropertiesHelper.OperationTypePrefix);

	    this.Resource.Properties[AllQualityTestsTreeService.ShowOperationTraceroutePropertyName] =
	        this.CheckBoxShowOperationTraceroute.Checked.ToString();

	    GoBack();
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}
