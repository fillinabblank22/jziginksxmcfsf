using System;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Help)]
public partial class IpSlaGettingStarted : BaseResourceControl
{
	protected override string DefaultTitle
	{
        get { return VNQMWebContent.VNQMWEBDATA_VB1_173; }
	}

	public override string HelpLinkFragment
	{
		get { return ""; }
	}

    public override string EditURL
    {
        get { return string.Empty; }
    }

    public override string SubTitle
    {
        get { return string.Empty; }
    }

    protected void OnRemoveResourceButtonClicked(Object sender, EventArgs e)
    {
        ResourceManager.DeleteById(Resource.ID);
        Response.Redirect(Request.Url.ToString());
    }
}
