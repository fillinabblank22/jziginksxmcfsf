﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using System.Data;
using Region = SolarWinds.Orion.IpSla.Web.Region;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.IPSLA)]
public partial class Orion_Voip_Resources_Summary_OperationsByStatus : VoipBaseResource
{
    public override string EditURL
    {
        get
        {
            IIpSlaOperationProvider operationProvider = GetInterfaceInstance<IIpSlaOperationProvider>();

            string url =
                $"/Orion/Voip/Resources/OperationsSummary/EditOperationsByStatus.aspx?ResourceID={this.Resource.ID}";
            if (operationProvider != null)
                url = $"{url}&NetObject={operationProvider.IpSlaOperation.NetObjectID}";
            return url;
        }
    }

    protected override string DefaultTitle => Resources.VNQMWebContent.VNQMWEBCODE_VB1_232;

    public override string HelpLinkFragment => "OrionIPSLAMonitorPHResourceIPSLAOperationsByStatus";

    private Region Region
    {
        get
        {
            var provider = GetInterfaceInstance<IRegionProvider>();
            return (provider != null ? provider.Region : null);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
    }

    void LoadData()
    {
        List<OperationType> selectedOperationTypes = this.Resource.LoadSelectedValues<OperationType>(ResourcePropertiesHelper.OperationTypePrefix, false);
        List<OperationStatus> selectedStatuses = this.Resource.LoadSelectedValues<OperationStatus>(ResourcePropertiesHelper.OperationStatusPrefix, false);

        //Validate
        if (selectedOperationTypes.Count == 0)
        {
            //No resource properties are set, use default
            selectedOperationTypes.AddRange(IpSlaOperationHelper.GetSupportedOperationTypes());
        }

        List<TopXXMetric> selectedMetrics = new List<TopXXMetric>();
        selectedMetrics.Add(TopXXMetric.RoundTripTime);
        
        //Load data

        int? sourceNodeId = GetSourceNodeID();
        int? regionId = GetRegionID();
        int? sourceRegionId = GetSourceRegionID();
        int? targetRegionId = GetTargetRegionID();
        DataTable dataTable = OperationsDAL.GetOperationsDataByStatus(selectedOperationTypes, selectedStatuses, sourceNodeId, regionId, sourceRegionId, targetRegionId);

        GridControl.Metrics = selectedMetrics;
        GridControl.ShowOperationTypeColumn = selectedOperationTypes.Count > 1;
        GridControl.DataSource = dataTable;
    }

    private int? GetSourceNodeID()
    {
        string netObject = this.Request["NetObject"];

        if (String.IsNullOrEmpty(netObject))
            return null;

        NetObjectHelper helper;
        if (NetObjectHelper.TryParse(netObject, out helper) && helper.IsNPMNode())
            return helper.Id;

        return null;
    }

    private int? GetRegionID()
    {
        return Region != null ? (int?) Region.VoipRegion.RegionID : null;
    }

    private int? GetSourceRegionID()
    {
        var provider = GetInterfaceInstance<ICallDetailsProvider>();
        return (provider != null ? provider.CallDetails.VoipCallDetails.OrigRegionID : null);
    }

    private int? GetTargetRegionID()
    {
        var provider = GetInterfaceInstance<ICallDetailsProvider>();
        return (provider != null ? provider.CallDetails.VoipCallDetails.DestRegionID : null);
    }
}
