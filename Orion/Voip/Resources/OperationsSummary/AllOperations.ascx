﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllOperations.ascx.cs" Inherits="Orion_Voip_Resources_Summary_AllQualityTests" %>
<%@ Register TagPrefix="ipslam" Assembly="SolarWinds.Orion.IpSla.Web" Namespace="SolarWinds.Orion.IpSla.Web.AjaxTree" %>


<orion:resourceWrapper runat="server" ID="Wrapper">
<Content>

<ipslam:AjaxTreeControl ID="AjaxTree" runat="server">
    <WebService Path="~/Orion/Voip/Services/AllQualityTestsTreeService.asmx" />
</ipslam:AjaxTreeControl>

</Content>
</orion:resourceWrapper>