﻿using System;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Events)]
public partial class Orion_Voip_Resources_Summary_IPSLALastXXEvents : BaseResourceControl
{
    override public ResourceLoadingMode ResourceLoadingMode { get { return ResourceLoadingMode.RenderControl; } }

    private int maxCount;
    protected void Page_Load(object sender, EventArgs e)
    {
        maxCount = GetIntProperty(ResourcePropertiesHelper.MaxCountProperty, 25);
        EventsControl.LoadData(VoipEvent.GetOperationEvents(0, maxCount));
    }

    protected override string DefaultTitle { get { return Resources.VNQMWebContent.VNQMWEBCODE_VB1_230; } }

    public override string DisplayTitle
    {
        get
        {
            return Title.Replace("XX", maxCount.ToString());
        }
    }


    public override string HelpLinkFragment
    {
        get { return "OrionVoIPMonitorPHResourceLast25Events"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/Voip/Controls/EditEventList.ascx"; }
    }
}