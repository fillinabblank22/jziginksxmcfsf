﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OperationsByStatus.ascx.cs" Inherits="Orion_Voip_Resources_Summary_OperationsByStatus" %>
<%@ Register TagPrefix="ipsla" TagName="TopXXGrid" Src="~/Orion/Voip/Controls/TopXXGrid.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <ipsla:TopXXGrid ID="GridControl" runat="server" />
    </Content>
</orion:resourceWrapper>
