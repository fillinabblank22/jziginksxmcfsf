<%@ Control Language="C#" ClassName="IpSlaActiveAlerts" Inherits="SolarWinds.Orion.Web.UI.BaseResourceControl" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Data"%>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web.DAL"%>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web" %>

<script runat="server">
    
    protected override string DefaultTitle
    {
        get { return Resources.VNQMWebContent.VNQMWEBCODE_VB1_231; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        IEnumerable<VoipAlert> alerts;
        IpSlaOperation operation = GetOperation();
        
        if(operation != null)
            alerts = VoipAlert.GetIpSlaActiveAlertsForOperation(operation.OperationInstanceID);
        else
	    {
            // we allows users to filter on summary views
    	    List<OperationType> selectedTypes =
	            this.Resource.LoadSelectedValues<OperationType>(ResourcePropertiesHelper.OperationTypePrefix, false);

            alerts = VoipAlert.GetIpSlaActiveAlerts(selectedTypes.ToArray());
	    }

	    AlertTable.DataSource = alerts;
		AlertTable.DataBind();
	}

    private IpSlaOperation GetOperation()
    {
        IIpSlaOperationProvider provider = GetInterfaceInstance<IIpSlaOperationProvider>();

        return provider != null
                   ? provider.IpSlaOperation 
                   : null;
    }
    
    public string GetViewLink(string triggeringObject, string triggeringObjectDetailsUrl)
    {
        if (string.IsNullOrEmpty(triggeringObjectDetailsUrl))
        {
            return triggeringObject;
        }
        return string.Format("<a href=\"{0}\">{1}</a>", triggeringObjectDetailsUrl, triggeringObject);
    }

	public override string HelpLinkFragment
	{
        get { return "OrionIPSLAMonitorPHResourceActiveIPSLAAlerts"; }
	}

    public override string EditURL
    {
        get
        {
            IpSlaOperation operation = GetOperation();
            string url = String.Empty;
            
            // only allow edit for resource on summary pages
            if(operation == null)
                url = string.Format("/Orion/Voip/Resources/OperationsSummary/EditOperationType.aspx?ResourceID={0}", this.Resource.ID);            
            
            return url;
        }
    }
	
	public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
    
</script>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
		<asp:Repeater ID="AlertTable" runat="server" >
			<HeaderTemplate>
				<table class="NeedsZebraStripes" border="0" cellPadding="2" cellSpacing="0" width="100%">
					<thead>
						<tr>
							<td colspan="2"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_183%></td>
							<td><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_184%></td>
							<td><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_185%></td>
						</tr>
					</thead>
			</HeaderTemplate>

			<ItemTemplate>
				<tr>
					<td><img src="/NetPerfMon/images/Event-19.gif" /></td>
					<td><%#Eval("AlertTime") %></a></td>
					<td><%#GetViewLink((string)Eval("ObjectName"), (string)Eval("ObjectID"))%></td>
					<td><%#Eval("AlertName") %></td>
				</tr>
			</ItemTemplate>

			<FooterTemplate>
				</table>
			</FooterTemplate>
		</asp:Repeater>
	</Content>
</orion:resourceWrapper>
