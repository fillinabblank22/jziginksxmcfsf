<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IpSlaGettingStarted.ascx.cs" Inherits="IpSlaGettingStarted" %>

<orion:resourceWrapper ShowHeaderBar="false" ID="ResourceWrapper1" runat="server">
    <Content>
        <div style="width: 100%;">
            <div style="padding: 1em; text-align: left; border: solid 15px #204a63;">
                <h1 class="IPSLAGettingStartTitle"><%=DefaultTitle%></h1>
                <table class="sw-ipsla-started-area" cellpadding="0" cellspacing="3" border="0">
                    <tr>
                        <td class="sw-ipsla-started-cc" width="80%"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_161 %></td>
                        <td class="sw-ipsla-started-bc" width="20%" style="text-align: center">
                            <orion:LocalizableButton ID="ThwackMediaButton" runat="server" PostBackUrl="http://thwack.com/media/p/77451.aspx" DisplayType="Primary" Width="85%" Text="<%$ Resources : VNQMWebContent, VNQMWEBDATA_MB_1 %>"></orion:LocalizableButton></td>
                    </tr>
                    <tr class="sw-ipsla-started-cc">
                        <td class="sw-ipsla-started-cc" width="80%"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_162 %></td>
                        <td class="sw-ipsla-started-bc" width="20%" style="text-align: center">
                            <orion:LocalizableButton ID="CredentialButton" runat="server"  PostBackUrl="~/Orion/Nodes/Default.aspx" DisplayType="Primary" Width="85%" Text="<%$ Resources : VNQMWebContent, VNQMWEBDATA_MB_2 %>"></orion:LocalizableButton></td>
                    </tr>
                    <tr>
                        <td class="sw-ipsla-started-cc" width="80%"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_164 %></td>
                        <td class="sw-ipsla-started-bc" width="20%" style="text-align: center">
                            <orion:LocalizableButton ID="ManageNodesButton" runat="server" PostBackUrl="~/Orion/VoIP/Admin/ManageNodes/Default.aspx" DisplayType="Primary" Width="85%" Text="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_165 %>"></orion:LocalizableButton></td>
                    </tr>
                    <tr>
                        <td class="sw-ipsla-started-cc" width="80%"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_166 %></td>
                        <td class="sw-ipsla-started-bc" width="20%" style="text-align: center">
                            <orion:LocalizableButton ID="addoperationButton" runat="server" PostBackUrl="~/Orion/VoIP/Admin/OperationsWizard.aspx" DisplayType="Primary" Width="85%" Text="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_167 %>"></orion:LocalizableButton></td>
                    </tr>
                    <tr>
                        <td class="sw-ipsla-started-cc" width="80%"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_168 %></td>
                        <td class="sw-ipsla-started-bc" width="20%" style="text-align: center">
                            <orion:LocalizableButton ID="AddCallManagerButton" runat="server" PostBackUrl="~/Orion/Voip/Admin/ManageCallManagers/AddCallManagerWizard.aspx" Width="85%" DisplayType="Primary" Text="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_169 %>"></orion:LocalizableButton></td>
                    </tr>
                    <tr>
                        <td class="sw-ipsla-started-cc" width="80%"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_170 %></td>
                        <td class="sw-ipsla-started-bc" width="20%" style="text-align: center">
                            <orion:LocalizableButton ID="AddMediaGatewayWizardButton" runat="server" PostBackUrl="~/Orion/Voip/Admin/ManageMediaGateway/AddMediaGatewayWizard.aspx" DisplayType="Primary" Width="85%" Text="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_171 %>"></orion:LocalizableButton></td>
                    </tr>
                </table>
                <div style="text-align: right; padding: 6px 6px 3px 3px;">
                    <orion:LocalizableButton ID="RemoveResourceButton" runat="server" OnClick="OnRemoveResourceButtonClicked" DisplayType="Secondary"  Text="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_172 %>" />
                </div>
            </div>
        </div>
    </Content>
</orion:resourceWrapper>