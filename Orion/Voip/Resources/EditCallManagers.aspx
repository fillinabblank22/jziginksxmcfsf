<%@ Page Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true" CodeFile="EditCallManagers.aspx.cs" Inherits="Orion_Voip_Resources_EditCallManagers" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register TagPrefix="voip" TagName="Grouping" Src="~/Orion/Voip/Controls/Grouping.ascx"%>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <span><b><big><%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_AK1_167, Resource.Title) %></big></b></span>
    
	<asp:ValidationSummary ID="ValidationSummary1" runat="server" />
    
    <b><orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="false" /></b>
    
    <b><voip:Grouping ID="CCMGroup" runat="server" /></b>
    <br />
    <br /><br />
    <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" DisplayType="Primary" OnClick="SubmitClick" />
</asp:Content>