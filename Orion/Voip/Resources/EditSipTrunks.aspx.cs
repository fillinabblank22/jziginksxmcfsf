﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Web.UI;

public partial class Orion_Voip_Resources_EditSipTrunks : ResourceEditPage
{
    protected override void Initialize()
    {
        ResourceTitleEditor.ResourceTitle = this.Resource.Title;

        if (!IsPostBack)
        {
            var callManagerSipTrunksPageSize = this.Resource.Properties["CallManagerSipTrunksPageSize"];
            if (string.IsNullOrEmpty(callManagerSipTrunksPageSize)
                || string.IsNullOrWhiteSpace(callManagerSipTrunksPageSize))
            {
                callManagerSipTrunksPageSize = "20";
            }

            this.CallManagerSipTrunksPageSize.Text = callManagerSipTrunksPageSize;
        }
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (!ResourceTitleEditor.ResourceTitle.Equals(Resource.Title))
        {
            Resource.Title = ResourceTitleEditor.ResourceTitle;
            SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
        }

        this.Resource.Properties["CallManagerSipTrunksPageSize"] = this.CallManagerSipTrunksPageSize.Text;

        GoBack();
    }
}