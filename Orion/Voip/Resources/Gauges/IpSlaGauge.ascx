﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IpSlaGauge.ascx.cs" Inherits="Orion_Voip_Resources_Gauges_IpSlaGauge" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Data" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web" %>


<orion:resourceWrapper runat="server" ID="Wrapper">
<Content>
<div>
	<table id="DualGauge">
		<tr runat="server" id="gaugeRow">
		</tr>
		<tr runat="server" id="gaugeCaptionRow">
		</tr>
	</table>
</div>
</Content>
</orion:resourceWrapper>