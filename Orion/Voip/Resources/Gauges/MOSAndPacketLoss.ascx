<%@ Import Namespace="SolarWinds.Orion.IpSla.Data" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web" %>
<%@ Control Language="C#" ClassName="MOSAndPacketLoss" Inherits="SolarWinds.Orion.IpSla.Web.GaugeResource" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web.DAL"%>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Data.Database"%>
<%@ Register TagPrefix="voip" Namespace="SolarWinds.NPM.Web.Gauge.V1" Assembly="SolarWinds.NPM.Web.Gauge.V1" %>

<script runat="server">
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (MOSValue == -1)
            gaugeMOS.NoData = true;
        gaugeMOS.Value = MOSValue;
        gaugeMOS.MinValue = GaugeConstants.MOS_MIN_VALUE;
        gaugeMOS.MaxValue = GaugeConstants.MOS_MAX_VALUE;
        gaugeMOS.TickCount = 4;
        gaugeMOS.Inverted = true;
        gaugeMOS.GaugeStyle = Style;
        gaugeMOS.ValueFormatString = "<DATA_VALUE:0.00>";
        gaugeMOS.Scale = Scale;
        SolarWinds.Orion.IpSla.Data.ThresholdInfo mosThreshold = SolarWinds.Orion.IpSla.Data.ThresholdManager.GetThresholdForOperationType(
            SolarWinds.Orion.IpSla.Data.OperationType.UdpVoipJitter, SolarWinds.Orion.IpSla.Data.ThresholdType.MOS);
        gaugeMOS.WarningThreshold = (float)mosThreshold.WarningLevel;
        gaugeMOS.ErrorThreshold = (float)mosThreshold.ErrorLevel;
        gaugeMOS.ReverseThreshold = true;
        
        if (PacketLossValue == -1)
            gaugePacketLoss.NoData = true;
        gaugePacketLoss.Value = PacketLossValue;
        gaugePacketLoss.MinValue = GaugeConstants.PACKET_LOSS_MIN_VALUE;
        gaugePacketLoss.MaxValue = GaugeConstants.PACKET_LOSS_MAX_VALUE;
        gaugePacketLoss.GaugeStyle = Style;
        gaugePacketLoss.ValueFormatString = "<DATA_VALUE:0><DATA_VALUE:%>";
        gaugePacketLoss.Scale = Scale;

        SolarWinds.Orion.IpSla.Data.ThresholdInfo packetLossThreshold = SolarWinds.Orion.IpSla.Data.ThresholdManager.GetThresholdForOperationType(
SolarWinds.Orion.IpSla.Data.OperationType.UdpVoipJitter, SolarWinds.Orion.IpSla.Data.ThresholdType.PacketLoss);

        gaugePacketLoss.WarningThreshold = (float)packetLossThreshold.WarningLevel;
        gaugePacketLoss.ErrorThreshold = (float)packetLossThreshold.ErrorLevel;
    }
    
	protected override string DefaultTitle
	{
        get { return Resources.VNQMWebContent.VNQMWEBCODE_VB1_238; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionVoIPMonitorPHGaugeMOSPacketLoss"; }
	}
	
	public override string EditURL
    {
        get { return GetEditUrl(this.Resource); }
    }
	
	public double MOSValue
	{
        get
        {
            return this.CallPath.MOS.Value;
        }
	}

    public double PacketLossValue
    {
        get
        {
            return this.CallPath.PacketLoss.Value;
        }
    }
</script>

<orion:resourceWrapper runat="server" ID="Wrapper">
<Content>
	<table id="DualGauge" >
		<tr>
			<td>
				<a href='<%=GetChartUrl("VoipCPMOS")%>' target="_blank">
					<voip:gauge ID="gaugeMOS" runat="server"/>
				</a>
			</td>
			<td>
				<a href='<%=GetChartUrl("VoipCPPacketLoss")%>' target="_blank">
					<voip:gauge ID="gaugePacketLoss" runat="server"/>
				</a>
			</td>
		</tr>
		<tr>
			<td><%= Resources.VNQMWebContent.VNQMWEBCODE_VB1_145%></td>
			<td><%= Resources.VNQMWebContent.VNQMWEBCODE_VB1_148%></td>
		</tr>
	</table>
</Content>
</orion:resourceWrapper>
