﻿
using System;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web.DisplayTypes;
using SolarWinds.Orion.IpSla.Data.Database;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.NPM.Web.Gauge.V1;
using System.Web.UI.HtmlControls;

public partial class Orion_Voip_Resources_Gauges_IpSlaGauge : SolarWinds.Orion.IpSla.Web.IpSlaGaugeResource
{
    readonly static Log _log = new Log();

    private readonly string[] gaugeTypeProperty = new []
    {
        "Gauge1Type",
        "Gauge2Type",
        "Gauge3Type"
    };


    protected override string DefaultTitle
    {
        get { return Resources.VNQMWebContent.VNQMWEBCODE_VB1_236; }
    }

    public override string EditURL
    {
        get { return GetEditUrl(this.Resource); }
    }

    public override string HelpLinkFragment
    {
        get
        {
            string value = Resource.Properties["HelpLinkFragment"];
            if(String.IsNullOrEmpty(value))
                value = "OrionIPSLAMonitorPHGaugeRoundTripTime";
    
            return value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        InitializeGauges();
    }

    private void InitializeGauges()
    {
        bool createdAtLeastOne = false;

        for (int i = 0; i < gaugeTypeProperty.GetLength(0); i++)
        {
            string gaugeType = Resource.Properties[gaugeTypeProperty[i]];
            if (gaugeType != null)
            {
                CreateGauge(gaugeType);
                createdAtLeastOne = true;
            }
        }

        if (!createdAtLeastOne)
            CreateGauge("RoundTripTime");

    }

    private void CreateGauge(string gaugeType)
    {
        try
        {
            OperationResultsKey key = (OperationResultsKey) Enum.Parse(typeof (OperationResultsKey), gaugeType, true);
            var gaugeCell = new HtmlTableCell();
            gaugeRow.Cells.Add(gaugeCell);
            var gauge = new Gauge();
            
            InitializeGauge(gauge, key);
            
            var anchor = new HtmlAnchor();
            anchor.HRef = GetChartUrl(key);
            anchor.Controls.Add(gauge);
            gaugeCell.Controls.Add(anchor);
            gaugeCaptionRow.Cells.Add(new HtmlTableCell() {InnerHtml = IpSlaGaugeConstants.GetCaption(key)});

        }
        catch (Exception ex)
        {
            _log.Error("Error creating ip sla gauage.", ex);
        }
    }

    private void NormalizeGaugeParameters(Gauge gauge, OperationResultsKey key, IpSlaMetric metric)
    {
        switch (key)
        {
            case OperationResultsKey.MOS:
                if (metric.Value.HasValue && metric.Value.Value < gauge.MinValue)
                {
                    gauge.MinValue = 0;
                    gauge.TickCount = Config.MosTickCountZeroValue;
                }
                return;
            default:
                return;
        }
    }


    public void InitializeGauge(Gauge gauge, OperationResultsKey key)
    {
        IpSlaMetric metric = this.GetMetric(key);
        gauge.MinValue = IpSlaGaugeConstants.GetMinValue(key);
        int tickCount = (int)IpSlaGaugeConstants.GetTickCount(key);
        if (tickCount != 0)
            gauge.TickCount = tickCount;
        if (!metric.Value.HasValue)
        {
            gauge.NoData = true;
        }
        else
        {
            gauge.Value = metric.Value.Value;
            this.NormalizeGaugeParameters(gauge, key, metric);
        }

        
        double maxValue = metric.HasThreshold
            ? metric.MaxLevel
            : IpSlaGaugeConstants.GetMaxValue(key);

        if (metric.Value.HasValue && gauge.Value > maxValue)
        {
            var maxValueInt = (int)Math.Round((gauge.Value - gauge.MinValue) * 270 / 250 + gauge.MinValue);
            var factor = maxValueInt >= 1000
                ? 100
                : maxValueInt >= 100
                    ? 10
                    : maxValueInt >= 10
                        ? 5
                        : 1;
            maxValue = ((maxValueInt + factor - 1) / factor) * factor;
        }

        gauge.MaxValue = maxValue;
        gauge.GaugeStyle = Style;
        gauge.ValueFormatString = IpSlaGaugeConstants.GetFormatString(key);

        gauge.Scale = Scale;

        if (metric.HasThreshold)
        {
            gauge.WarningThreshold = (float)Math.Min(metric.WarningLevel, gauge.MaxValue);
            gauge.ErrorThreshold = (float)Math.Min(metric.ErrorLevel, gauge.MaxValue);
            gauge.Inverted = metric.HigherValuesAreBetter;
            gauge.ReverseThreshold = metric.HigherValuesAreBetter;
        }
    }
}
