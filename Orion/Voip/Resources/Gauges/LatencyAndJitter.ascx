<%@ Import Namespace="SolarWinds.Orion.IpSla.Data" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web" %>
<%@ Control Language="C#" ClassName="LatencyAndJitter" Inherits="SolarWinds.Orion.IpSla.Web.GaugeResource" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Data.Database"%>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web.DAL"%>
<%@ Register TagPrefix="voip" Namespace="SolarWinds.NPM.Web.Gauge.V1" Assembly="SolarWinds.NPM.Web.Gauge.V1" %>

<script runat="server">
    
    protected void Page_Load(object sender, EventArgs e)
    {
        var latencyThreshold = ThresholdManager.GetThresholdForOperationType(OperationType.UdpVoipJitter, ThresholdType.Latency);
        var jitterThreshold = ThresholdManager.GetThresholdForOperationType(OperationType.UdpVoipJitter, ThresholdType.Jitter);
        
        if (LatencyValue == -1)
            gaugeLatency.NoData = true;
        gaugeLatency.Value = LatencyValue;
        gaugeLatency.MinValue = GaugeConstants.LATENCY_MIN_VALUE;
        gaugeLatency.MaxValue = Math.Max(LatencyValue, latencyThreshold.MaxLevel);
        gaugeLatency.GaugeStyle = Style;
        gaugeLatency.ValueFormatString = "<DATA_VALUE:0 ms>";
        gaugeLatency.Scale = Scale;

        gaugeLatency.WarningThreshold = ((float)latencyThreshold.WarningLevel > gaugeLatency.MaxValue) ? (float)gaugeLatency.MaxValue : (float)latencyThreshold.WarningLevel;
        gaugeLatency.ErrorThreshold = ((float)latencyThreshold.ErrorLevel > gaugeLatency.MaxValue) ? (float)gaugeLatency.MaxValue : (float)latencyThreshold.ErrorLevel;

        if (JitterValue == -1)
            gaugeJitter.NoData = true;
        gaugeJitter.Value = JitterValue;
        gaugeJitter.MinValue = GaugeConstants.JITTER_MIN_VALUE;
        gaugeJitter.MaxValue = Math.Max(JitterValue, jitterThreshold.MaxLevel);
        gaugeJitter.GaugeStyle = Style;
        gaugeJitter.ValueFormatString = "<DATA_VALUE:0 ms>";
        gaugeJitter.Scale = Scale;

        gaugeJitter.WarningThreshold = ((float)jitterThreshold.WarningLevel > gaugeJitter.MaxValue) ? (float)gaugeJitter.MaxValue : (float)jitterThreshold.WarningLevel;
        gaugeJitter.ErrorThreshold = ((float)jitterThreshold.ErrorLevel > gaugeJitter.MaxValue) ? (float)gaugeJitter.MaxValue : (float)jitterThreshold.ErrorLevel;
        gaugeJitter.TickCount = 5;
    }

	protected override string DefaultTitle
	{
		get { return Resources.VNQMWebContent.VNQMWEBCODE_VB1_237; }
	}

	public override string HelpLinkFragment
	{
        get { return "OrionIPSLAMonitorPHGaugeLatencyAndJitter"; }
	}

    public override string EditURL
    {
        get { return GetEditUrl(this.Resource); }
    }
    
	public double LatencyValue
	{
        get 
        {
            return this.CallPath.Latency.Value;
        }
	}

    public double JitterValue
    {
        get
        {
            return this.CallPath.Jitter.Value;
        }
    }
</script>

<orion:resourceWrapper runat="server" ID="Wrapper">
<Content>
	<table id="DualGauge">
		<tr>
			<td>
				<a href='<%=GetChartUrl("VoipCPLatency")%>' target="_blank">
					<voip:Gauge ID="gaugeLatency" runat="server" />
				</a>
			</td>
			<td>
				<a href='<%=GetChartUrl("VoipCPJitter")%>' target="_blank">
					<voip:Gauge ID="gaugeJitter" runat="server" />
				</a>
			</td>
		</tr>
		<tr>
			<td><%= Resources.VNQMWebContent.VNQMWEBCODE_VB1_147 %></td>
			<td><%= Resources.VNQMWebContent.VNQMWEBCODE_VB1_146%></td>
		</tr>
	</table>
</Content>
</orion:resourceWrapper>
