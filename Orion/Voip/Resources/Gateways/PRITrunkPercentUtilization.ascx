﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PRITrunkPercentUtilization.ascx.cs" Inherits="Orion_Voip_Resources_Gateways_PRITrunkPercentUtilization" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
<Content>
    <orion:Include ID="Include1" runat="server" Module="Voip" File="Voip.css" />

    <div class="sw-suggestion sw-suggestion-fail" runat="server" ID="PriInfo" Visible="False" style="margin-bottom: 1em;">
        <span class="sw-suggestion-icon"></span> 
        <%= PriInfoText %>
    </div>

    <table  class="NeedsZebraStripes" cellpadding="3" cellspacing="0" width="100%">
        <thead>
            <tr>
                <td class="voipTrunkPercentUtilizationTableHeader voipTrunkPercentUtilizationTableRow"></td>
	            <td class="voipTrunkPercentUtilizationTableHeader"><%= TrunkLabel %></td>
	            <td class="voipTrunkPercentUtilizationTableHeader"><%= TrunkTypeLabel %></td>
                <td class="voipTrunkPercentUtilizationTableHeader"><%= TotalChannelsAvailableLabel %></td>
	            <td class="voipTrunkPercentUtilizationTableHeader"><%= TransmitLabel %></td>
	            <td class="voipTrunkPercentUtilizationTableHeader"><%= ReceiveLabel %></td>
            </tr>
        </thead>
    <asp:Repeater runat="server" ID="trunkUtilizationTable">
        <ItemTemplate>
		        <tr>
		            <td style="padding-left: 10px"><img src="/Orion/Voip/images/CallIcons/Trunk_phone16x16.png" alt="Endpoint" /></td>
			        <td><%# Eval("TrunkName")%></td>
			        <td><%# Eval("TrunkType") %></td>
                    <td><%# Eval("ChannelCount")%></td>
			        <td><%# Eval("Transmit")%></td>
			        <td><%# Eval("Receive")%></td>
		        </tr>
        </ItemTemplate>
    </asp:Repeater>
    </table>
</Content>
</orion:resourceWrapper>