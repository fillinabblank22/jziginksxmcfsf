﻿using System;
using System.Web.UI;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_Voip_Resources_Gateways_TrunkUtilizationLegend : UserControl, IChartLegendControl
{
    public string LegendInitializer { get { return "Voip_TrunkUtilizationLegendInitializer_" + legend.ClientID; } }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}