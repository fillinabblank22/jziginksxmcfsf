<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditGatewayFailedCall.aspx.cs" EnableEventValidation="false" Inherits="Resources_GraphEdit"
 MasterPageFile="~/Orion/ResourceEdit.master" Title="Edit graph properties" ValidateRequest="false" %>
 <%@Register TagPrefix="orion" TagName="DateTimePicker" src="~/Orion/Controls/DateTimePicker.ascx" %>
 <%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ContentPlaceHolderID="HeadPlaceHolder" ID="HeadContent" runat="server">
    <orion:Include ID="Include1" runat="server" Module="Voip" File="Voip.css" />
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainContent">
    <style type="text/css">
        .EditPageHeader{width: 220px !important;}
        .checkbox input{margin-left:0px;}
    </style>
    <div>
        
    <asp:PlaceHolder ID="mainPlaceHolder" runat="server">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
        <table>
            <tr style="height:50px">
                <td colspan="2">
                   <h1 style="padding-left: 0px"><%= Page.Title %></h1>
                </td>
            </tr>
            <tr>
                <td colspan ="2">
                      <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="true" />
                </td>
            </tr>
          <tr>
                <td class="EditPageHeader">
                    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_211 %>
                </td>
                <td >
                    <asp:DropDownList ID="TimePeriodsList" runat="server" Width="173"/>
                </td>
            </tr>
            <tr>
                <td class="EditPageHeader">
                    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_212 %>
                </td>
                <td>
                    <asp:DropDownList ID="SampleSizeList" runat="server" Width="173"/>
		            <asp:CustomValidator ID="LargeIntervalValidator" runat="server" ControlToValidate="SampleSizeList"
		            ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_213 %>" OnServerValidate="IntervalValidation" > *</asp:CustomValidator> 
                </td>
            </tr>
            <tr>
                <td class="EditPageHeader">
                    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_214%>
                </td>
                <td>
                    <asp:CheckBox ID="CheckBoxCustom" runat="server" CssClass="checkbox"/>
                </td>
            </tr>
            <tr class="CustomDate">
                <td class="EditPageHeader">
                    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_215 %>
                </td>
                <td>
                    <orion:DateTimePicker ID="BeginDateTimePeriod" runat="server" />
                    <asp:CustomValidator ID="PeriodBeginCustomValidator" runat="server" ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_216 %>"
                        OnServerValidate="checkStartEndDate"> *</asp:CustomValidator>
                </td>        
            </tr>
            <tr class="CustomDate">
                <td class="EditPageHeader">
                    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_218 %>
                </td>
                <td>
                    <orion:DateTimePicker ID="EndDateTimePeriod" runat="server"/>
                     <asp:CustomValidator ID="PeriodEndCustomValidator" runat="server" ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_216 %>"
                         OnServerValidate="checkStartEndDate"> *</asp:CustomValidator>
                </td>
            </tr>
            <tr style="height:50px">
                <td>
				    <orion:LocalizableButton runat="server" ToolTip="<%$ Resources : VNQMWebContent, VNQMWEBCODE_VB1_171 %>" ID="SubmitButton" OnClick="SubmitButton_Click" LocalizedText="Submit" DisplayType="Primary"/> 
                </td>
            </tr>
        </table>
    </asp:PlaceHolder>
    </div>
    
     <script type="text/javascript">
         function SetCustomDateVisibility() {
             if (document.getElementById('<%=CheckBoxCustom.ClientID %>').checked == true) {
                 $(".CustomDate").show();
                 document.getElementById('<%=TimePeriodsList.ClientID %>').disabled = true;
             } else {
                 $(".CustomDate").hide();
                 document.getElementById('<%=TimePeriodsList.ClientID %>').disabled = false;
             }

         }
         $('#<%=CheckBoxCustom.ClientID %>').change(SetCustomDateVisibility);
         SetCustomDateVisibility();
   </script>
</asp:Content>
