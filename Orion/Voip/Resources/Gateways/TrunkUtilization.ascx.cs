﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using VoipGateway = SolarWinds.Orion.IpSla.Web.VoipGateway;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")] //hidden purposly FB 241737
public partial class Orion_Voip_Resources_Gateways_TrunkUtilization : StandardChartResource
{
    private static readonly Log Log = new Log();
    private NetObject _netObject;
    private int _netObjectId;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!Resource.Properties.ContainsKey("ChartName"))
        {
            Resource.Properties.Add("ChartName", "IPSLAGatewayTrunkUtilization");
        }
        if (!Resource.Properties.ContainsKey("ChartDateSpan"))
        {
            Resource.Properties.Add("ChartDateSpan", "1");
        }
        if (!Resource.Properties.ContainsKey("SampleSize"))
        {
            Resource.Properties.Add("SampleSize", "60");
        }
        if (!Resource.Properties.ContainsKey("ShowTitle"))
        {
            Resource.Properties.Add("ShowTitle", "1");
        }
        if (!Resource.Properties.ContainsKey("ChartSubTitle"))
        {
            Resource.Properties.Add("ChartSubTitle", "${ZoomRange}");
        }

        HandleInit(WrapperContents);
        OrionInclude.ModuleFile("Voip", "Charts.IpSla.TrunkUtilizationChartLegend.js");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetInterfaceInstance<IVoipGatewayProvider>() == null)
        {
            Wrapper.Visible = false;
            return;
        }

        var netObjectProvider = GetInterfaceInstance<INetObjectProvider>();
        if (netObjectProvider != null)
        {
            _netObject = netObjectProvider.NetObject;
        }

        if (_netObject == null)
        {
            Log.ErrorFormat("NetObject not provided: {0}", "Orion_Voip_Resources_Gateways_TrunkUtilization");
            return;
        }

        string[] parts = _netObject.NetObjectID.Split(':');
        if (parts.Length < 2 || !Int32.TryParse(parts[1], out _netObjectId))
        {
            Log.ErrorFormat("NetObject ID {0} doesn't have expected format.", Request["NetObject"]);
            return;
        }

        ResourceId.Value = Resource.ID > 0 ? Resource.ID.ToString() : (-1*Resource.ID).ToString();

        UpdateSubTitle(ChartPeriods.Lookup(Resource.Properties["ChartDateSpan"].ToString(CultureInfo.InvariantCulture)));
    }

    protected override Dictionary<string, object> GenerateDisplayDetails()
    {
        Dictionary<string, object> details = base.GenerateDisplayDetails();

        details[ResourcePropertiesKeys.EndpointId] = 0;
        details[ResourcePropertiesKeys.GatewayNodeId] = _netObjectId;

        if (details.ContainsKey("title"))
        {
            details["title"] = Resources.VNQMWebContent.VNQMWEBDATA_RKG_5;

            if (Request.QueryString.AllKeys.Contains("rpVEP"))
            {
                if (Request.QueryString["rpVEP"] != "0")
                {
                    var endpointId = int.Parse(Request.QueryString["rpVEP"]);

                    var endpoint = VoipGatewayEndpointsDAL.Instance.GetVoipGatewayEndpoint(endpointId);

                    details[ResourcePropertiesKeys.EndpointId] = endpointId;
                    details["title"] = endpoint.IfName;
                }
            }
        }

        return details;
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        return new[] { _netObjectId.ToString(CultureInfo.InvariantCulture) };
    }

    protected override void SetChartInitializerScript()
    {
        var script = String.Format(@"
            var displayDetails_{2} = {0};
            var refresh_{2} = function() {{ SW.Core.Charts.initializeStandardChart(displayDetails_{2}, {1}); }};
            
            $(function () {{ 
                if((typeof(SW.Core.View) != 'undefined') && (typeof(SW.Core.View.AddOnRefresh) != 'undefined')) {{            
                    SW.Core.View.AddOnRefresh(refresh_{2}, '{2}');
                }}
                refresh_{2}();
            }});", DisplayDetails, ChartOptions, ResourceId.Value);

        ScriptInitializer.InnerHtml = script;
    }

    protected VoipGateway VoipGateway
    {
        get { return GetInterfaceInstance<IVoipGatewayProvider>().VoipGateway; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IVoipGatewayProvider) }; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.VNQMWebContent.WEBDATA_RKG_1; }
    }

    protected override string NetObjectPrefix
    {
        get
        {
            return "VVG";
        }
    }

    private void UpdateSubTitle(string appendString)
    {
        Resource.SubTitle = string.IsNullOrEmpty(Resource.SubTitle) ? appendString : String.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_61, Resource.SubTitle, appendString);
    }
}