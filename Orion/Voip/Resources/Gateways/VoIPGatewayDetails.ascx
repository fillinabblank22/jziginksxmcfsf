﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VoIPGatewayDetails.ascx.cs" Inherits="Orion_Voip_Resources_Gateways_VoIPGatewayDetails" %>
<orion:resourceWrapper runat="server" ID="Wrapper" ShowEditButton="False">
    <Content>
        <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%" runat="server" id="voipGatewayDetailsTable">
            <tr>
                <td class="voipGatewayNodesHeaderText">
                    <%= NodeStatus%>
                </td>
                <td style="width: 20px;">
                    <img src="" alt="" id="imgStatus" runat="server" />
                </td>
                <td class="voipGatewayNodeRowText">
                    <asp:Label runat="server" ID="lblStatus"></asp:Label>
                </td>
            </tr>
             <tr>
                <td class="voipGatewayNodesHeaderText">
                    <%= IpAddress %>
                </td>
                <td>
                </td>
                <td class="voipGatewayNodeRowText">
                    <asp:HyperLink runat="server" ID="hplIpAddress"></asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td class="voipGatewayNodesHeaderText">
                    <%= SystemName%>
                </td>
                <td>
                </td>
                <td class="voipGatewayNodeRowText">
                    <asp:Label runat="server" ID="lblName"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="voipGatewayNodesHeaderText">
                    <%= Vendor %>
                </td>
                <td>
                </td>
                <td class="voipGatewayNodeRowText">
                    <asp:Label runat="server" ID="lblVendor"></asp:Label>
                </td>
            </tr>       
            <tr>
                <td class="voipGatewayNodesHeaderText">
                    <%= MachineType%>
                </td>
                <td>
                    <img src="" alt="" id="imgMachineIcon" runat="server" onerror="this.src='/NetPerfMon/images/Vendors/Unknown.gif'; return true;" />
                </td>
                <td class="voipGatewayNodeRowText">
                     <asp:Label runat="server" ID="lblMachineType"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="voipGatewayNodesHeaderText">
                    <%= TrunkNumber %>
                </td>
                <td>
                </td>
                <td class="voipGatewayNodeRowText">
                    <asp:Label runat="server" ID="lblTrunkNumber"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="voipGatewayNodesHeaderText">
                    <%= SipTrunkNumber %>
                </td>
                <td>
                </td>
                <td class="voipGatewayNodeRowText">
                    <asp:Label runat="server" ID="lblSipTrunkNumber"></asp:Label>
                </td>
            </tr>
             <tr>
                <td class="voipGatewayNodesHeaderText">
                    <%= ActiveCalls %>
                </td>
                <td>
                </td>
                <td class="voipGatewayNodeRowText">
                    <asp:Label runat="server" ID="lblActiveCalls"></asp:Label>
                </td>
            </tr>
             <tr>
                <td class="voipGatewayNodesHeaderText">
                    <%= LastPoll %>
                </td>
                <td>
                </td>
                <td class="voipGatewayNodeRowText">
                    <asp:Label runat="server" ID="lblLastPoll"></asp:Label>
                </td>
            </tr>
        </table>
    </Content>
</orion:resourceWrapper>