﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GatewayDetails.ascx.cs"
    Inherits="Orion_Voip_Resources_Gateways_GatewayDetails" %>
<orion:resourceWrapper runat="server" ID="Wrapper" ShowEditButton="False">
    <Content>
        <table border="0" cellpadding="2" cellspacing="0" width="100%" runat="server" id="voipGatewayDetailsTable">
            <tr>
                <td class="voipCallDetailsHeaderText">
                    <%= Status %>
                </td>
                <td class="voipCallDetailsRowText">
                    <img src="" alt="" id="imgStatus" runat="server" />
                    <asp:Label runat="server" ID="lblStatus"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="voipCallDetailsHeaderText">
                    <%= Name %>
                </td>
                <td class="voipCallDetailsRowText">
                    <asp:Label runat="server" ID="lblName"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="voipCallDetailsHeaderText">
                    <%= Description %>
                </td>
                <td class="voipCallDetailsRowText">
                    <asp:Label runat="server" ID="lblDescription"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="voipCallDetailsHeaderText">
                    <%= IpAddress %>
                </td>
                <td class="voipCallDetailsRowText">
                    <asp:Label runat="server" ID="lblIpAddress"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="voipCallDetailsHeaderText">
                    <%= RegionName %>
                </td>
                <td class="voipCallDetailsRowText">
                    <asp:HyperLink runat="server" ID="hlLocation"></asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td class="voipCallDetailsHeaderText VoipThickBottomBorder">
                    <%= CallManager %>
                </td>
                <td class="voipCallDetailsRowText VoipThickBottomBorder">
                    <img src="" alt="" id="imgCallManger" runat="server" />
                    <asp:HyperLink runat="server" ID="hlCallManager"></asp:HyperLink>
                </td>
            </tr>
        </table>
    </Content>
</orion:resourceWrapper>
