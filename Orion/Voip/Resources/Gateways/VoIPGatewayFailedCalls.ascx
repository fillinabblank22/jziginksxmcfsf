﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VoIPGatewayFailedCalls.ascx.cs"
    Inherits="Orion_Voip_Resources_Gateways_VoIPGatewayFailedCalls" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
       <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
        <%--magic here because WrapperContents are being loaded in specific order in OrionWeb.--%>
        <div class="voipSearchPageLinkRight" style="text-align: right;  margin-top: -23px; margin-right: 20px">
            <asp:HyperLink ID="HyperLink1" runat="server" CssClass="sw-link" NavigateUrl="~/Orion/Voip/VoipCallSearch.aspx?CallTime=Last15Minutes&CallFailed=true"><%= SearchPageLinkText%></asp:HyperLink>
        </div>
    </Content>
</orion:resourceWrapper>
