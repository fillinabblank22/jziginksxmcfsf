﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CallsWithQualityIssues.ascx.cs" Inherits="Orion_Voip_Resources_Gateways_CallsWithQualityIssues" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include ID="Include1" runat="server" Module="Voip" File="Voip.css" />
        <table cellpadding="3" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <td class="voipTrunkPercentUtilizationTableHeader voipGatewayCallQuality"><%= StatusLabel %></td>
                    <td class="voipTrunkPercentUtilizationTableHeader voipDataDistributionAlign"><%= CallOriginLabel %></td>
                    <td class="voipTrunkPercentUtilizationTableHeader voipDataDistributionAlign"><%= CallDestinationLabel %></td>
                    <td class="voipTrunkPercentUtilizationTableHeader voipDataDistributionAlign"><%= MOSLabel %></td>
                    <td class="voipTrunkPercentUtilizationTableHeader voipDataDistributionAlign"><%= LatencyLabel %></td>
                    <td class="voipTrunkPercentUtilizationTableHeader voipDataDistributionAlign"><%= JitterLabel %></td>
                    <td class="voipTrunkPercentUtilizationTableHeader voipDataDistributionAlign"><%= PacketLossLabel %></td>
                </tr>
            </thead>
            <asp:Repeater runat="server" ID="trunkUtilizationTable">
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("Status") %></td>
                        <td class="voipDataDistributionAlign"><%# Eval("CallOrigin") %></td>
                        <td class="voipDataDistributionAlign"><%# Eval("CallDestination") %></td>
                        <td class="voipDataDistributionAlign"><%# Eval("MOS") %></td>
                        <td class="voipDataDistributionAlign"><%# Eval("Latency") %></td>
                        <td class="voipDataDistributionAlign"><%# Eval("Jitter") %></td>
                        <td class="voipDataDistributionAlign"><%# Eval("PacketLoss") %></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
        <%if (!HasCallIssues) {%>
        <table cellpadding="3" cellspacing="0" width="100%">
            <tr>
                <td colspan="7" align="center" >                    
                     <%=DataNotAvailableText%> <br />
                    <a class="coloredLink" href='<%=HelpUrlLink %>'><%=DataNotAvailableQuestionText%></a>                    
                </td>
            </tr>
        </table>
        <%}%>
        <div class="voipSearchPageHyperLink">
            <asp:HyperLink ID="HyperLink1" runat="server" CssClass="sw-link" NavigateUrl="/Orion/Voip/VoipCallSearch.aspx?CallTime=Last15Minutes&MOS=3&Jitter=3&Latency=3&PacketLoss=3&useOR=true"><%= SearchPageLinkText %></asp:HyperLink>
        </div>
    </Content>
</orion:resourceWrapper>
