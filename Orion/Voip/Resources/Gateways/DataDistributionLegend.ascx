﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DataDistributionLegend.ascx.cs"
    Inherits="Orion_Voip_Resources_Gateways_DataDistributionLegend" %>
<script type="text/javascript">
    SW.Core.Charts.Legend.Voip_DataDistributionLegendInitializer_<%= legend.ClientID %> = function (chart) {
        SW.IPSLA.Charts.DataDistributionChartLegend.createStandardLegend(chart, '<%= legend.ClientID %>');
    };
</script>
<div id="DataDistributionLegend" runat="server">
    <table runat="server" id="legend" cellspacing="0">
    </table>
    <div style="float: right;">
        <img class="chartLegendLogo" src="/orion/images/SolarWinds.Logo.Footer.png" alt="" />
    </div>
</div>
