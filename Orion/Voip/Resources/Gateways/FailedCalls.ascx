<%@ Control Language="C#" ClassName="FailedCallPhoneGraph" Inherits="SolarWinds.Orion.IpSla.Web.GatewayGraphResource" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web" %>

<script runat="server">

    protected static readonly string searchPageLinkText = Resources.VNQMWebContent.VNQMWEBCODE_VB1_188;
    protected override void OnInit(EventArgs e)
    {
        OrionInclude.ModuleFile("Voip", "Voip.css");
        base.OnInit(e);
    }
    protected string ChartImageUrl
    {
        get
        {
            return GetChartImageUrl(Gateway, "VoipFailedCallGateway");
        }
    }

    protected string CustomUrl
    {
        get { return GenerateCustomChartLink(Gateway, "VoipFailedCallGateway"); }
    }
    
	protected override string DefaultTitle
	{
        get { return Resources.VNQMWebContent.VNQMWEBCODE_VB1g_189; }
	}

	public override string HelpLinkFragment
	{
        get { return "OrionIPSLAMonitorPHFailedCalls"; }
	}

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }    
</script>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <a href="<%= this.CustomUrl %>">
            <img src="<%= this.ChartImageUrl %>" />
       </a>
       <div class ="voipSearchPageHyperLink">
       <asp:HyperLink ID="HyperLink1" runat="server" CssClass="sw-link" NavigateUrl="/Orion/Voip/VoipCallSearch.aspx?CallTime=Last15Minutes&CallFailed=true"><%= searchPageLinkText%></asp:HyperLink>
       </div>
    </Content>
</orion:resourceWrapper>