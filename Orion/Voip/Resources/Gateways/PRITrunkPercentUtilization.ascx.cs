﻿using System;
using System.Globalization;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.IpSla.Web.WebServices;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.VOIP)]
public partial class Orion_Voip_Resources_Gateways_PRITrunkPercentUtilization : ResourceControl
{
    private string netObjectId = string.Empty;
    protected string TrunkLabel = Resources.VNQMWebContent.VNQMWEBCODE_PG_4;
    protected string TrunkTypeLabel = Resources.VNQMWebContent.VNQMWEBCODE_PG_5;
    protected string TotalChannelsAvailableLabel = Resources.VNQMWebContent.VNQMWEBCODE_PG_6;
    protected string TransmitLabel = Resources.VNQMWebContent.VNQMWEBCODE_PG_7;
    protected string ReceiveLabel = Resources.VNQMWebContent.VNQMWEBCODE_PG_8;
    protected string PriInfoText = Resources.VNQMWebContent.VNQMWEBCODE_RKG_15;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var voipGatewayProvider = GetInterfaceInstance<IVoipGatewayProvider>();

            if (voipGatewayProvider == null)
            {
                Wrapper.Visible = false;
                return;
            }

            var voIPgateway = voipGatewayProvider.VoipGateway.VoIpGateway;

            netObjectId = voIPgateway.NodeID.ToString(CultureInfo.InvariantCulture);

            if (!Resource.Properties.ContainsKey("TimePeriod"))
            {
                Resource.Properties.Add("TimePeriod", "Last 5 Minutes");
            }

            DateTime fromDate = DateTime.UtcNow;
            DateTime toDate = DateTime.UtcNow;
            bool isCustom = false;
            string period = Resource.Properties["TimePeriod"].ToString(CultureInfo.InvariantCulture);

            GatewayChartData.Parse(ref period, ref fromDate, ref toDate, ref isCustom);

            var trunkUtilization = VoipGatewaysDAL.Instance.GetTrunkUtilization(voIPgateway.NodeID, fromDate,
                                                                                toDate);

            trunkUtilizationTable.DataSource = trunkUtilization;
            trunkUtilizationTable.DataBind();

            Resource.Title = string.Format("{0} {1}", Resource.Title, voIPgateway.SystemName);
            Resource.SubTitle = string.IsNullOrEmpty(Resource.SubTitle)
                                    ? Resource.Properties["TimePeriod"].ToString(CultureInfo.InvariantCulture)
                                    : String.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_61, Resource.SubTitle,
                                                    Resource.Properties["TimePeriod"].ToString(
                                                        CultureInfo.InvariantCulture));

            PriInfo.Visible = VoipGatewaysDAL.Instance.GetOrphanActiveCallsCount(voIPgateway.NodeID, fromDate, toDate) != 0;
        }
    }

    public override string DetachURL
    {
        get
        {
            return string.Format("/Orion/DetachResource.aspx?ResourceID={0}&NetObject=VVG:{1}", this.Resource.ID, netObjectId);
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.VNQMWebContent.VNQMWEBCODE_PG_3; }
    }

    public override string EditURL
    {
        get
        {
            return string.Format("/Orion/Voip/Resources/Gateways/EditPRITrunkPercentUtilization.aspx?ResourceID={0}", this.Resource.ID);
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionIPSLAMonitorPHPRITrunkUtilization";
        }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IVoipGatewayProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}