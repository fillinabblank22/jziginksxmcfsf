﻿using System;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.UI;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_Voip_Resources_Gateways_EditDataDistribution : ResourceEditPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Title = String.Format(Resources.VNQMWebContent.VNQMWEBDATA_AK1_167, Resource.Title);

            foreach (var period in Periods.GetTimePeriodList())
            {
                ddlTimePeriod.Items.Add(new ListItem(period.DisplayName, period.Name));

                if (period.Name.Equals(Resource.Properties["TimePeriod"]))
                {
                    ddlTimePeriod.Items[ddlTimePeriod.Items.Count - 1].Selected = true;
                }
            }
        }
    }

    protected override void Initialize()
    {
        resourceTitleEditor.ResourceTitle = Resource.Title;
        resourceTitleEditor.ResourceSubTitle = Resource.SubTitle;
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (this.Page.IsValid)
        {
            Resource.Properties["TimePeriod"] = ddlTimePeriod.SelectedValue;

            if (!resourceTitleEditor.ResourceTitle.Equals(Resource.Title))
            {
                Resource.Title = resourceTitleEditor.ResourceTitle;
            }

            if (!resourceTitleEditor.ResourceSubTitle.Equals(Resource.SubTitle))
            {
                Resource.SubTitle = resourceTitleEditor.ResourceSubTitle;
            }

            ResourcesDAL.Update(Resource);

            GoBack();
        }
    }
}