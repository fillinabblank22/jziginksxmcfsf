﻿using System;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using VoipGateway = SolarWinds.Orion.IpSla.Web.VoipGateway;
using GraphResource = SolarWinds.Orion.IpSla.Web.GraphResource;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")] //hidden purposly FB 241737
public partial class Orion_Voip_Resources_Gateways_VoIPGatewayFailedCalls : StandardChartResource
{
    private static readonly Log Log = new Log();
    private NetObject _netObject;
    private int _netObjectId;

    protected static readonly string SearchPageLinkText = Resources.VNQMWebContent.VNQMWEBCODE_VB1_188;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!Resource.Properties.ContainsKey("ChartName"))
        {
            Resource.Properties.Add("ChartName", "IPSLAGatewayFailedCalls");
        }
        if (!Resource.Properties.ContainsKey("ChartDateSpan"))
        {
            Resource.Properties.Add("ChartDateSpan", "1");
        }
        if (!Resource.Properties.ContainsKey("SampleSize"))
        {
            Resource.Properties.Add("SampleSize", "60");
        }
        if (!Resource.Properties.ContainsKey("ShowTitle"))
        {
            Resource.Properties.Add("ShowTitle", "1");
        }
        if (!Resource.Properties.ContainsKey("ChartSubTitle"))
        {
            Resource.Properties.Add("ChartSubTitle", "${ZoomRange}");
        }

        var netObjectProvider = GetInterfaceInstance<INetObjectProvider>();
        if (netObjectProvider != null)
        {
            _netObject = netObjectProvider.NetObject;
        }

        if (_netObject == null)
        {
            Log.ErrorFormat("NetObject not provided: {0}", "Orion_Voip_Resources_Gateways_VoIPGatewayFailedCalls");
            return;
        }

        string[] parts = _netObject.NetObjectID.Split(':');
        if (parts.Length < 2 || !Int32.TryParse(parts[1], out _netObjectId))
        {
            Log.ErrorFormat("NetObject ID {0} doesn't have expected format.", Request["NetObject"]);
            return;
        }

        Resource.Title = string.Format("{0} {1}", Resource.Title, _netObject.Name);
        HandleInit(WrapperContents);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetInterfaceInstance<IVoipGatewayProvider>() == null)
        {
            Wrapper.Visible = false;
            return;
        }

        UpdateSubTitle(ChartPeriods.Lookup(Resource.Properties["ChartDateSpan"].ToString(CultureInfo.InvariantCulture)));
    }

    protected override Dictionary<string, object> GenerateDisplayDetails()
    {
        Dictionary<string, object> details = base.GenerateDisplayDetails();

        details[ResourcePropertiesKeys.GatewayNodeId] = _netObjectId;

        return details;
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        return new[] { _netObjectId.ToString(CultureInfo.InvariantCulture) };
    }

    protected override string DefaultTitle
    {
        get { return Resources.VNQMWebContent.VNQMWEBDATA_RKG_11; }
    }

    protected override string NetObjectPrefix
    {
        get
        {
            return "VVG";
        }
    }

    protected VoipGateway VoipGateway
    {
        get { return GetInterfaceInstance<IVoipGatewayProvider>().VoipGateway; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IVoipGatewayProvider) }; }
    }

    private void UpdateSubTitle(string appendString)
    {
        Resource.SubTitle = string.IsNullOrEmpty(Resource.SubTitle) ? appendString : String.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_61, Resource.SubTitle, appendString);
    }

}