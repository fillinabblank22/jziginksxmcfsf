﻿using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_Voip_Resources_Gateways_EditPRITrunkPercentUtilization : ResourceEditPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var resourceId = int.Parse(Request.Params["ResourceID"]);
        ResourceInfo resource = ResourceManager.GetResourceByID(resourceId);
        Page.Title = string.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_216, resource.Title);
        // If page is called from other then save previous page Url and fill a list
        if (!Page.IsPostBack)
        {
            resourceTitleEditor.ResourceTitle = Resource.Title;
            resourceTitleEditor.ResourceSubTitle = Resource.SubTitle;

            var timePeriod = resource.Properties["TimePeriod"].ToString(CultureInfo.InvariantCulture);
            // Filling dropdown list with periods
            foreach (var period in Periods.GetTimePeriodList())
            {
                TimePeriodsList.Items.Add(new ListItem(period.DisplayName, period.Name));

                if (timePeriod.Equals(period.Name))
                {
                    TimePeriodsList.Items[TimePeriodsList.Items.Count - 1].Selected = true;
                }
            }
        }
    }

    protected void SubmitButton_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (!resourceTitleEditor.ResourceTitle.Equals(Resource.Title))
            {
                Resource.Title = resourceTitleEditor.ResourceTitle;
            }

            if (!resourceTitleEditor.ResourceSubTitle.Equals(Resource.SubTitle))
            {
                Resource.SubTitle = resourceTitleEditor.ResourceSubTitle;
            }

            Resource.Properties["TimePeriod"] = TimePeriodsList.SelectedItem.Value;

            ResourcesDAL.Update(Resource);

            GoBack();
        }
    }
}