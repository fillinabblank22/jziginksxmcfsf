﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditDataDistribution.aspx.cs" Inherits="Orion_Voip_Resources_Gateways_EditDataDistribution" MasterPageFile="~/Orion/ResourceEdit.master" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_AK1_167, Resource.Title) %></h1>
    <div style="padding-left: 16px;">
    <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="true" />
    <b><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_168 %></b>
    <br/>
        <asp:DropDownList runat="server" ID="ddlTimePeriod" CssClass="voipSearchDropDownListOriginDestination voipSearchFont">
        </asp:DropDownList>
    <br/>
    <br/>
    <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" DisplayType="Primary" OnClick="SubmitClick" />
    </div>
</asp:Content>
