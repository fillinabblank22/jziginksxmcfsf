﻿using System;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.IpSla.Common.Ioc;

[ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.VOIP)]
public partial class Orion_Voip_Resources_Gateways_VoIPGatewayDetails : VoipBaseResource
{
    protected static readonly string NodeStatus = Resources.VNQMWebContent.VNQMWEBDATA_GK_13;
    protected static readonly string SystemName = Resources.VNQMWebContent.VNQMWEBDATA_GK_14;
    protected static readonly string Vendor = Resources.VNQMWebContent.VNQMWEBDATA_VB1_31;
    protected static readonly string IpAddress = Resources.VNQMWebContent.VNQMWEBDATA_AK1_99;
    protected static readonly string MachineType = Resources.VNQMWebContent.VNQMWEBDATA_VB1_30;
    protected static readonly string TrunkNumber = Resources.VNQMWebContent.VNQMWEBDATA_GK_15;
    protected static readonly string SipTrunkNumber = Resources.VNQMWebContent.VNQMWEBDATA_NumOfSIPTrunks_BO;
    protected static readonly string ActiveCalls = Resources.VNQMWebContent.VNQMWEBDATA_GK_16;
    protected static readonly string LastPoll = Resources.VNQMWebContent.VNQMWEBCODE_VB1_202;
    protected static readonly string NA = Resources.VNQMWebContent.VNQMWEBCODE_VB1_244;

    private string netObjectId = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        var voipGatewayProvider = GetInterfaceInstance<IVoipGatewayProvider>();
        var voipGatewaySipTrunksDal = IocContainer.Singleton.Container.Resolve<IVoipGatewaySipTrunksDAL>();

        if (voipGatewayProvider == null)
        {
            Wrapper.Visible = false;
            return;
        }

        var voIPgateway = voipGatewayProvider.VoipGateway.VoIpGateway;

        netObjectId = voIPgateway.NodeID.ToString(CultureInfo.InvariantCulture);

        var node = (SolarWinds.Orion.NPM.Web.Node)NetObjectFactory.Create($"N:{netObjectId}");

        imgStatus.Src = GetStatusImageURL(voIPgateway.Status);
        if ((Status)(node.StatusOriginal) == OBJECT_STATUS.Up && voIPgateway.Status == VoipGatewayStatus.Down)//in case the polling status is down but node is up, the credentials failed
        {
            lblStatus.Text =
                $"{GetLocalizedProperty("CCMDeviceStatus", voIPgateway.Status.ToString())} ({Resources.VNQMWebContent.VNQMWEBDATA_RKG_16})";
        }
        else
        {
            lblStatus.Text = GetLocalizedProperty("CCMDeviceStatus", voIPgateway.Status.ToString());
        }
        lblName.Text = voIPgateway.GatewayName;
        hplIpAddress.Text = voIPgateway.IPAddress;
        hplIpAddress.NavigateUrl = $"/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{netObjectId}";
        imgMachineIcon.Src = $"/NetPerfMon/images/Vendors/{node.VendorIcon}";
        lblMachineType.Text = voIPgateway.MachineType;
        lblVendor.Text = string.IsNullOrEmpty(voIPgateway.Vendor)
                             ? Resources.VNQMWebContent.VNQMWEBDATA_AK1_41
                             : voIPgateway.Vendor;
        if (DateTime.MinValue != voIPgateway.LastResultRecordTime)
        {
            lblLastPoll.Text = voIPgateway.LastResultRecordTime.ToLocalTime().ToString("MMMM dd, yyyy hh:mm:ss tt");

            if (voIPgateway.Status == VoipGatewayStatus.Down || voIPgateway.Status == VoipGatewayStatus.Unknown || voIPgateway.Status == VoipGatewayStatus.Unreachable)
            {//in case the status is down or so, we dont know how many active calls existed in last poll
                lblActiveCalls.Text = NA;
            }
            else
            {
                lblActiveCalls.Text =
                    VoipGatewaysDAL.Instance.GerNumberOfActiveCalls(voIPgateway.NodeID)
                                   .ToString(CultureInfo.InvariantCulture);
            }


            lblTrunkNumber.Text =
                VoipGatewaysDAL.Instance.GetNumberOfTrunks(voIPgateway.NodeID)
                               .ToString(CultureInfo.InvariantCulture);
            lblSipTrunkNumber.Text = voipGatewaySipTrunksDal.GetNumberOfSipTrunks(voIPgateway.VoipGatewayID).ToString(CultureInfo.InvariantCulture);
        }
        else
        {
            lblLastPoll.Text = NA;
            lblActiveCalls.Text = NA;
            lblTrunkNumber.Text = NA;
            lblSipTrunkNumber.Text = NA;
        }
    }

    private string GetStatusImageURL(VoipGatewayStatus status)
    {
        var imageUrl = string.Empty;
        switch (status)
        {
            case VoipGatewayStatus.Up:
                imageUrl = "/Orion/Voip/images/CallIcons/g_green_s.PNG";
                break;
            case VoipGatewayStatus.Critical:
                imageUrl = "/Orion/Voip/images/CallIcons/gateway16x16_rX.png";
                break;
            case VoipGatewayStatus.Down:
                imageUrl = "/Orion/Voip/images/CallIcons/g_red_sm.png";
                break;
            case VoipGatewayStatus.Warning:
                imageUrl = "/Orion/Voip/images/CallIcons/g_yellow_s.png";
                break;
            case VoipGatewayStatus.Unknown:
                imageUrl = "/Orion/Voip/images/CallIcons/g_grey_s.png";
                break;
            case VoipGatewayStatus.Unreachable:
                imageUrl = "/Orion/Voip/images/CallIcons/g_grey_s.png";
                break;
        }
        return imageUrl;
    }

    public override string DetachURL =>
        $"/Orion/DetachResource.aspx?ResourceID={Resource.ID}&NetObject=VVG:{netObjectId}";

    public override string HelpLinkFragment => "OrionIPSLAMonitorPhVoipGatewayDetails";

    protected override string DefaultTitle => Resources.VNQMWebContent.VNQMWEBCODE_VB1_220;

    public override IEnumerable<Type> RequiredInterfaces => new[] { typeof(IVoipGatewayProvider) };

    protected string GetLocalizedProperty(string prefix, string property)
    {
        var manager = ResourceManagerRegistrar.Instance;
        var key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}