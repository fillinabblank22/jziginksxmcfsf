<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DataDistribution.ascx.cs"
    Inherits="Orion_Voip_Resources_Gateways_DataDistribution" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include ID="Include1" runat="server" Module="Voip" File="Voip.css" />
        <script type="text/javascript">
            $(document).ready(function () {
                var sNetObject = 0;
                var sPageUrl = window.location.search.substring(1);
                var sUrlVariables = sPageUrl.split('&');
                for (var i = 0; i < sUrlVariables.length; i++) {
                    if (sUrlVariables[i].toLowerCase().indexOf('netobject') == 0) {
                        if (sUrlVariables[i].toLowerCase().indexOf('%3a') != -1) {
                            sNetObject = sUrlVariables[i].toLowerCase().split('%3a')[1];
                        } else {
                            sNetObject = sUrlVariables[i].toLowerCase().split('=')[1].split(':')[1];
                        }
                    }
                }

                $.ajax({
                    type: "POST",
                    url: "/Orion/Voip/Services/GatewayChartData.asmx/GetEndpoints",
                    data: "{'gatewayNodeId': " + parseInt(sNetObject) + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        var resId = <%= Resource.ID %>;
                        $('.ResourceWrapper').each(function() {
                            if ($(this).attr("ResourceID") == resId) {
                                var options = result.d;
                                var select = '<select name=\"ddlEndpoints_<%= Resource.ID %>\" onchange=\"endpointChanged_<%= Resource.ID %>(this);\" class=\"voipSearchDropDownListOriginDestination voipSearchFont\">' 
                                                + options + '</select>';
                                
                                var elem = $(this).find("h1");
                                $(elem).html($(elem).html() + select);
                                $('[name=ddlEndpoints_<%= Resource.ID %>]').val(displayDetails_<%= Resource.ID %>.EndpointId);
                            }
                        });
                    },
                    error: function (error) {
                        alert(error);
                    }
                });
            });

            function endpointChanged_<%= Resource.ID %>(sel) {
                var endpointId = sel[sel.selectedIndex].value;
                //displayDetails is loaded dynamically by SetChartInitializerScript method
                displayDetails_<%= Resource.ID %>.EndpointId = endpointId;
                refresh_<%= Resource.ID %>();
            }
        </script>
        <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
    </Content>
</orion:resourceWrapper>
