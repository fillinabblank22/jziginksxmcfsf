<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TrunkUtilization.ascx.cs" Inherits="Orion_Voip_Resources_Gateways_TrunkUtilization" %>
<%@ Import Namespace="System.Web.Security.AntiXss" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:Include ID="Include1" runat="server" Module="Voip" File="Voip.css" />
        <script type="text/javascript">
            $(document).ready(function () {
                var sNetObject = 0;
                var sPageUrl = window.location.search.substring(1);
                var sUrlVariables = sPageUrl.split('&');
                for (var i = 0; i < sUrlVariables.length; i++) {
                    if (sUrlVariables[i].toLowerCase().indexOf('netobject') == 0) {
                        if (sUrlVariables[i].toLowerCase().indexOf('%3a') != -1) {
                            sNetObject = sUrlVariables[i].toLowerCase().split('%3a')[1];
                        } else {
                            sNetObject = sUrlVariables[i].toLowerCase().split('=')[1].split(':')[1];
                        }
                    }
                }

                if (sNetObject === 0) {
                    return;
                }

                $.ajax({
                    type: "POST",
                    url: "/Orion/Voip/Services/GatewayChartData.asmx/GetEndpoints",
                    data: "{'gatewayNodeId': " + parseInt(sNetObject) + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        var resId = <%=AntiXssEncoder.HtmlEncode(ResourceId.Value,true) %>;
                        $('.ResourceWrapper').each(function() {
                            if ($(this).attr("ResourceID") === resId) {
                                var options = result.d;
                                var select =
                                    '<select name=\"ddlEndpoints_"+resId\" onchange=\"endpointChanged_<%= AntiXssEncoder.HtmlEncode(ResourceId.Value,true) %>(this);\" class=\"voipSearchDropDownListOriginDestination voipSearchFont\">' +
                                        options +
                                        '</select>';

                                var elem = $(this).find("h1");
                                $(elem).html($(elem).html() + select);
                                $('[name=ddlEndpoints_' + resId + ']')
                                    .val(displayDetails_<%= AntiXssEncoder.HtmlEncode(ResourceId.Value,true) %>
                                        .EndpointId);
                            }
                        });
                    },
                    error: function (error) {
                        alert(error);
                    }
                });
            });

            function endpointChanged_<%= AntiXssEncoder.HtmlEncode(ResourceId.Value,true) %>(sel) {
                var resId = <%= AntiXssEncoder.HtmlEncode(ResourceId.Value,true) %>;

                $(sel).parents('.ResourceWrapper').first().find('.TrunkUtilizationLegend').find('table').children()
                    .remove();

                var endpointId = sel[sel.selectedIndex].value;
                var endpoint = sel[sel.selectedIndex].text;
                //displayDetails is loaded dynamically by SetChartInitializerScript method
                displayDetails_<%= AntiXssEncoder.HtmlEncode(ResourceId.Value,true) %>.EndpointId = endpointId;
                displayDetails_<%= AntiXssEncoder.HtmlEncode(ResourceId.Value,true) %>.title = endpoint;
                refresh_<%= AntiXssEncoder.HtmlEncode(ResourceId.Value,true) %>();

                $('.ResourceWrapper').each(function() {
                    if ($(this).attr("ResourceID") === resId) {
                        var elem = $(this).find('a');

                        for (var i = 0; i < elem.length; i++) {
                            var href = $(elem[i]).attr('href');
                            if (href.indexOf('CustomChart.aspx') !== -1) {
                                if (href.indexOf('VEP=') === -1) {
                                    $(elem[i]).attr('href', href + '&rpVEP=' + endpointId);
                                } else {
                                    var index = href.indexOf('rpVEP=');
                                    href = href.substr(0, index);
                                    $(elem[i]).attr('href', href + 'rpVEP=' + endpointId);
                                }
                            }
                        }
                    }
                });
            }
        </script>
        <asp:HiddenField runat="server" ID="ResourceId"/>
        <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
    </Content>
</orion:resourceWrapper>