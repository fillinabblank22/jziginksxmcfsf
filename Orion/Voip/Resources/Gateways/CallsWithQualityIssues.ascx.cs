﻿using System;
using System.Data;
using System.Globalization;
using SolarWinds.Orion.IpSla.Common;
using SolarWinds.Orion.IpSla.Common.Settings;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.IpSla.Web.WebServices;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
public partial class Orion_Voip_Resources_Gateways_CallsWithQualityIssues : ResourceControl
{
    private string netObjectId = string.Empty;
    protected static readonly string StatusLabel = Resources.VNQMWebContent.VNQMWEBDATA_TM0_39;
    protected static readonly string CallOriginLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_141;
    protected static readonly string CallDestinationLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_142;
    protected static readonly string MOSLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_145;
    protected static readonly string LatencyLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_147;
    protected static readonly string JitterLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_146;
    protected static readonly string PacketLossLabel = Resources.VNQMWebContent.VNQMWEBCODE_VB1_148;
    protected static readonly string SearchPageLinkText = Resources.VNQMWebContent.VNQMWEBCODE_PG_10;
    protected static readonly string NA = Resources.VNQMWebContent.VNQMWEBCODE_VB1_134;
    protected static readonly string DataNotAvailableText = Resources.VNQMWebContent.VNQMWEBCODE_MM_1;
    protected static readonly string DataNotAvailableQuestionText = Resources.VNQMWebContent.VNQMWEBCODE_MM_2;
    protected string HelpUrlLink = string.Format("http://www.solarwinds.com/documentation/kbLoader.aspx?kb=4671&lang={0}", Resources.CoreWebContent.CurrentHelpLanguage);
    protected bool HasCallIssues = false;

    private const string SuccessCall =
        "<img src='/Orion/Voip/images/CallIcons/cM_s_green.png' alt='' /><span class='voipGatewayCallQualityStatus'><a href='{0}'>{1}</a></span>";
    private const string FailedCall =
        "<img src='/Orion/Voip/images/CallIcons/cM_s_red.png' alt='' /><span class='voipGatewayCallQualityStatus'><a href='{0}'>{1}</a></span>";
    private const string DeviceLink = "<a href='{0}'>{1}</a>";
    private const string QualityString = "{0} / {1}";

    private int maxCount;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack) return;

        var voipGatewayProvider = GetInterfaceInstance<IVoipGatewayProvider>();

        if (voipGatewayProvider == null)
        {
            Wrapper.Visible = false;
            return;
        }

        var voIPgateway = voipGatewayProvider.VoipGateway.VoIpGateway;

        netObjectId = voIPgateway.NodeID.ToString(CultureInfo.InvariantCulture);

        if (!Resource.Properties.ContainsKey("TimePeriod"))
        {
            Resource.Properties.Add("TimePeriod", "LAST 15 MINUTES");
        }
        if (!Resource.Properties.ContainsKey("MaxCount"))
        {
            Resource.Properties.Add("MaxCount", "10");
        }

        if (!int.TryParse(Resource.Properties["MaxCount"], out maxCount))
        {
            maxCount = 10;
        }

        DateTime toDate = DateTime.UtcNow;
        DateTime fromDate = DateTime.UtcNow;
        string timePeriod = Resource.Properties["TimePeriod"].ToString(CultureInfo.InvariantCulture);
        bool isCustom = false;

        GatewayChartData.Parse(ref timePeriod, ref fromDate, ref toDate, ref isCustom);

        var callsWithIssue = VoipGatewaysDAL.Instance.GetTopCallsWithQualityIssues(voIPgateway.NodeID, fromDate,
                                                                                   toDate, maxCount);

        var dtCalls = new DataTable();
        dtCalls.Columns.Add("Status");
        dtCalls.Columns.Add("CallOrigin");
        dtCalls.Columns.Add("CallDestination");
        dtCalls.Columns.Add("MOS");
        dtCalls.Columns.Add("Latency");
        dtCalls.Columns.Add("Jitter");
        dtCalls.Columns.Add("PacketLoss");

        if (callsWithIssue != null)
        {
            foreach (DataRow dataRow in callsWithIssue.Rows)
            {
                DataRow dRow = dtCalls.NewRow();

                bool isCallSuccess = Convert.ToBoolean(dataRow["CallSuccess"]);

                var callDetailsLink =
                    this.Page.ResolveUrl("/orion/voip/calldetails.aspx?CallDetailsID=" + dataRow["ID"]);

                dRow["Status"] = isCallSuccess
                                     ? String.Format(SuccessCall, callDetailsLink,
                                                     Resources.VNQMWebContent.VNQMWEBDATA_AK1_71)
                                     : String.Format(FailedCall, callDetailsLink,
                                                     Resources.VNQMWebContent.VNQMWEBDATA_AK1_73);

                var ccmType = CallManagersDAL.Instance.GetCallManagerTypeById((int) dataRow["CcmID"]);

                int? origDeviceId = null;
                var origDeviceType = ccmType == CallManagerType.CCM
                    ? DeviceHelper.GetCiscoDeviceType((int)dataRow["CcmID"], dataRow["OrigDeviceName"].ToString(), (int)dataRow["OrigIpAddrInt"], out origDeviceId)
                    : DeviceHelper.GetAvayaDeviceType((int)dataRow["CcmID"], (string)dataRow["CallingPartyNumber"], (int)dataRow["OrigIpAddrInt"], out origDeviceId);

                dRow["CallOrigin"] = origDeviceId.HasValue
                                         ? string.Format(DeviceLink,
                                                         CallDetailHelper.GetDeviceURL(origDeviceType,
                                                                                       origDeviceId.Value),
                                                         IPAddressHelper.IntToIPv4string(
                                                             (int) dataRow["OrigIpAddrInt"]))
                                         : IPAddressHelper.IntToIPv4string((int) dataRow["OrigIpAddrInt"]);

                var origMOSValue = NA;
                if (dataRow["OrigMOS"] != DBNull.Value)
                {
                    float outMos;
                    if (float.TryParse(dataRow["OrigMOS"].ToString(), out outMos))
                    {
                        origMOSValue = CallDetailHelper.GetMos(outMos);
                    }
                }

                var origJitterValue = dataRow["OrigJitter"] != DBNull.Value
                                          ? CallDetailHelper.GetResultValue((int) dataRow["OrigJitter"],
                                                                            IpSlaSettings
                                                                                .CcmCallJitterWarningLevel.Value,
                                                                            IpSlaSettings
                                                                                .CcmCallJitterCriticalLevel
                                                                                .Value,
                                                                            Resources.VNQMWebContent
                                                                                     .VNQMWEBDATA_AK1_124)
                                          : NA;
                var origLatencyValue = dataRow["OrigLatency"] != DBNull.Value
                                           ? CallDetailHelper.GetResultValue((int) dataRow["OrigLatency"],
                                                                             IpSlaSettings
                                                                                 .CcmCallLatencyWarningLevel
                                                                                 .Value,
                                                                             IpSlaSettings
                                                                                 .CcmCallLatencyCriticalLevel
                                                                                 .Value,
                                                                             Resources.VNQMWebContent
                                                                                      .VNQMWEBDATA_AK1_124)
                                           : NA;
                var origPacketLossValue = dataRow["OrigPacketLoss"] != DBNull.Value
                                              ? CallDetailHelper.GetResultValue((int) dataRow["OrigPacketLoss"],
                                                                                IpSlaSettings
                                                                                    .CcmCallPacketLossWarningLevel
                                                                                    .Value,
                                                                                IpSlaSettings
                                                                                    .CcmCallPacketLossCriticalLevel
                                                                                    .Value,
                                                                                Resources.VNQMWebContent
                                                                                         .VNQMWEBCODE_VB1_204)
                                              : NA;

                int? destDeviceId = null;

                var destDeviceType = ccmType == CallManagerType.CCM
                    ? DeviceHelper.GetCiscoDeviceType((int)dataRow["CcmID"], dataRow["DestDeviceName"].ToString(), (int)dataRow["DestIpAddrInt"], out destDeviceId)
                    : DeviceHelper.GetAvayaDeviceType((int)dataRow["CcmID"],(string)dataRow["FinalCalledPartyNumber"], (int)dataRow["DestIpAddrInt"], out destDeviceId);

                dRow["CallDestination"] = destDeviceId.HasValue
                                              ? string.Format(DeviceLink,
                                                              CallDetailHelper.GetDeviceURL(destDeviceType,
                                                                                            destDeviceId.Value),
                                                              IPAddressHelper.IntToIPv4string(
                                                                  (int) dataRow["DestIpAddrInt"]))
                                              : IPAddressHelper.IntToIPv4string((int) dataRow["DestIpAddrInt"]);

                var destMOSValue = NA;
                if (dataRow["DestMOS"] != DBNull.Value)
                {
                    float outMos;
                    if (float.TryParse(dataRow["DestMOS"].ToString(), out outMos))
                    {
                        destMOSValue = CallDetailHelper.GetMos(outMos);
                    }
                }

                var destJitterValue = dataRow["DestJitter"] != DBNull.Value
                                          ? CallDetailHelper.GetResultValue((int) dataRow["DestJitter"],
                                                                            IpSlaSettings
                                                                                .CcmCallJitterWarningLevel.Value,
                                                                            IpSlaSettings
                                                                                .CcmCallJitterCriticalLevel
                                                                                .Value,
                                                                            Resources.VNQMWebContent
                                                                                     .VNQMWEBDATA_AK1_124)
                                          : NA;
                var destLatencyValue = dataRow["DestLatency"] != DBNull.Value
                                           ? CallDetailHelper.GetResultValue((int) dataRow["DestLatency"],
                                                                             IpSlaSettings
                                                                                 .CcmCallLatencyWarningLevel
                                                                                 .Value,
                                                                             IpSlaSettings
                                                                                 .CcmCallLatencyCriticalLevel
                                                                                 .Value,
                                                                             Resources.VNQMWebContent
                                                                                      .VNQMWEBDATA_AK1_124)
                                           : NA;
                var destPacketLossValue = dataRow["DestPacketLoss"] != DBNull.Value
                                              ? CallDetailHelper.GetResultValue((int) dataRow["DestPacketLoss"],
                                                                                IpSlaSettings
                                                                                    .CcmCallPacketLossWarningLevel
                                                                                    .Value,
                                                                                IpSlaSettings
                                                                                    .CcmCallPacketLossCriticalLevel
                                                                                    .Value,
                                                                                Resources.VNQMWebContent
                                                                                         .VNQMWEBCODE_VB1_204)
                                              : NA;

                dRow["MOS"] = string.Format(QualityString, origMOSValue, destMOSValue);
                dRow["Latency"] = string.Format(QualityString, origLatencyValue, destLatencyValue);
                dRow["Jitter"] = string.Format(QualityString, origJitterValue, destJitterValue);
                dRow["PacketLoss"] = string.Format(QualityString, origPacketLossValue, destPacketLossValue);

                dtCalls.Rows.Add(dRow);
            }
        }

        trunkUtilizationTable.DataSource = dtCalls;
        trunkUtilizationTable.DataBind();

        if (callsWithIssue != null && callsWithIssue.Rows.Count > 0)
            HasCallIssues = true;

        Resource.Title = string.Format("{0} {1}", Resource.Title, voIPgateway.SystemName);
        Resource.Title = Resource.Title.Replace("XX", maxCount.ToString(CultureInfo.InvariantCulture));
        Resource.SubTitle = string.IsNullOrEmpty(Resource.SubTitle)
                                ? Resource.Properties["TimePeriod"].ToString(CultureInfo.InvariantCulture)
                                : String.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_61, Resource.SubTitle,
                                                Resource.Properties["TimePeriod"].ToString(
                                                    CultureInfo.InvariantCulture));
    }

    public override string DetachURL
    {
        get
        {
            return string.Format("/Orion/DetachResource.aspx?ResourceID={0}&NetObject=VVG:{1}", this.Resource.ID, netObjectId);
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.VNQMWebContent.VNQMWEBCODE_PG_9; }
    }

    public override string EditURL
    {
        get
        {
            return string.Format("/Orion/Voip/Resources/Gateways/EditCallsWithQualityIssues.aspx?ResourceID={0}", this.Resource.ID);
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionVoIPMonitorPHResourceTopCallQualityIssuesVoIPGateway";
        }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IVoipGatewayProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}