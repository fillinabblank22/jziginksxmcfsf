﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditPRITrunkPercentUtilization.aspx.cs" Inherits="Orion_Voip_Resources_Gateways_EditPRITrunkPercentUtilization" MasterPageFile="~/Orion/ResourceEdit.master" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ContentPlaceHolderID="HeadPlaceHolder" ID="HeadContent" runat="server">
    <orion:Include ID="Include1" runat="server" Module="Voip" File="Voip.css" />
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainContent">
    <div>
    <asp:PlaceHolder ID="mainPlaceHolder" runat="server">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
        <table>
            <tr style="height:50px">
                <td colspan="2">
                   <h1 style="padding-left: 0px"><%= Page.Title %></h1>
                </td>
            </tr>
            <tr>
                <td colspan ="2">
                      <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="true" />
                </td>
            </tr>
          <tr>
                <td class="EditPageHeader">
                    <%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_211 %>
                </td>
                <td >
                    <asp:DropDownList ID="TimePeriodsList" runat="server" Width="173"/>
                </td>
            </tr>
            <tr style="height:50px">
                <td>
				    <orion:LocalizableButton runat="server" ToolTip="<%$ Resources : VNQMWebContent, VNQMWEBCODE_VB1_171 %>" ID="SubmitButton" OnClick="SubmitButton_Click" LocalizedText="Submit" DisplayType="Primary"/> 
                </td>
            </tr>
        </table>
    </asp:PlaceHolder>
    </div>
</asp:Content>
