﻿using System;
using System.Collections.Generic;
using System.Globalization;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.WebServices;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_Voip_Resources_Gateways_DataDistribution : StandardChartResource
{
    private static readonly Log Log = new Log();
    private NetObject _netObject;
    private int _netObjectId;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!Resource.Properties.ContainsKey("ChartName"))
        {
            Resource.Properties.Add("ChartName", "IPSLAGatewayDataDistribution");
        }
        if (!Resource.Properties.ContainsKey("TimePeriod"))
        {
            Resource.Properties.Add("TimePeriod", "LAST 15 MINUTES");
        }

        _netObject = NetObjectFactory.Create(Request["NetObject"]);
        if (_netObject == null)
        {
            Log.ErrorFormat("NetObject {0} doesn't exist.", Request["NetObject"]);
            return;
        }

        string[] parts = _netObject.NetObjectID.Split(':');
        if (parts.Length < 2 || !Int32.TryParse(parts[1], out _netObjectId))
        {
            Log.ErrorFormat("NetObject ID {0} doesn't have expected format.", Request["NetObject"]);
            return;
        }

        HandleInit(WrapperContents);
        UpdateSubTitle(Resource.Properties["TimePeriod"]);

        OrionInclude.ModuleFile("Voip", "Charts.IpSla.DataDistributionChartLegend.js");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GetInterfaceInstance<IVoipGatewayProvider>() == null)
        {
            Wrapper.Visible = false;
            return;
        }
    }

    protected override Dictionary<string, object> GenerateDisplayDetails()
    {
        Dictionary<string, object> details = base.GenerateDisplayDetails();

        details[ResourcePropertiesKeys.EndpointId] = 0;
        details[ResourcePropertiesKeys.GatewayNodeId] = _netObjectId;
        details[ResourcePropertiesKeys.TimePeriod] = Resource.Properties["TimePeriod"];

        int endpointId = 0;
        if (int.TryParse(Request.QueryString["rpVEP"], out endpointId))
        {
            details[ResourcePropertiesKeys.EndpointId] = endpointId;
        }        

        return details;
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        return new[] { _netObjectId.ToString(CultureInfo.InvariantCulture) };
    }

    protected override void SetChartInitializerScript()
    {
        var script = String.Format(@"            
            var displayDetails_{2} = {0};
            var refresh_{2} = function() {{ SW.Core.Charts.initializeStandardChart(displayDetails_{2}, {1}); }};
            
            $(function () {{ 
                if((typeof(SW.Core.View) != 'undefined') && (typeof(SW.Core.View.AddOnRefresh) != 'undefined')) {{            
                    SW.Core.View.AddOnRefresh(refresh_{2}, '{2}');
                }}
                refresh_{2}();
            }});", DisplayDetails, ChartOptions, Resource.ID);

        ScriptInitializer.InnerHtml = script;
    }

    protected VoipGateway VoipGateway
    {
        get { return GetInterfaceInstance<IVoipGatewayProvider>().VoipGateway; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IVoipGatewayProvider) }; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.VNQMWebContent.WEBDATA_RKG_2; }
    }

    protected override string NetObjectPrefix
    {
        get
        {
            return "VVG";
        }
    }

    public override string EditURL
    {
        get
        {
            return string.Format("/Orion/Voip/Resources/Gateways/EditDataDistribution.aspx?ResourceID={0}", this.Resource.ID);
        }
    }

    private void UpdateSubTitle(string appendString)
    {
        Resource.SubTitle = string.IsNullOrEmpty(Resource.SubTitle) ? appendString : String.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_61, Resource.SubTitle, appendString);
    }
}