﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TrunkUtilizationLegend.ascx.cs"
    Inherits="Orion_Voip_Resources_Gateways_TrunkUtilizationLegend" %>
<script type="text/javascript">
    SW.Core.Charts.Legend.Voip_TrunkUtilizationLegendInitializer_<%= legend.ClientID %> = function (chart) {
        SW.IPSLA.Charts.TrunkUtilizationChartLegend.createStandardLegend(chart, '<%= legend.ClientID %>');
    };
</script>
<div id="TrunkUtilizationLegend" class="TrunkUtilizationLegend" runat="server">
    <table runat="server" id="legend" cellspacing="0" width="100%">
    </table>
    <br/>
    <div style="float: right;">
        <img class="chartLegendLogo" src="/orion/images/SolarWinds.Logo.Footer.png" />
    </div>
</div>
