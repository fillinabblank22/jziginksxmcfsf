﻿using System;
using System.Web.UI;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.IpSla.Resources;

public partial class Orion_Voip_Resources_Gateways_DataDistributionLegend : UserControl, IChartLegendControl
{
    public string LegendInitializer { get { return "Voip_DataDistributionLegendInitializer_" + legend.ClientID; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
}