﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.IpSla.Common.Model;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.VOIP)]
public partial class Orion_Voip_Resources_Gateways_GatewayDetails : VoipBaseResource
{
    protected static readonly string Status = Resources.VNQMWebContent.VNQMWEBDATA_TM0_39;
    protected static readonly string Name = Resources.VNQMWebContent.VNQMWEBDATA_VB1_128;
    protected static readonly string Description = Resources.VNQMWebContent.VNQMWEBDATA_VB1_129;
    protected static readonly string IpAddress = Resources.VNQMWebContent.VNQMWEBDATA_AK1_99;
    protected static readonly string Location = Resources.VNQMWebContent.VNQMWEBCODE_VB1_144;
    protected static readonly string RegionName = Resources.VNQMWebContent.VNQMWEBDATA_VB1_131;
    protected static readonly string CallManager = Resources.VNQMWebContent.VNQMWEBCODE_VB1_219;
    protected static readonly string NA = Resources.VNQMWebContent.VNQMWEBCODE_VB1_134;

    private string callManagerPath = "/Orion/Voip/CallManager.aspx?NetObject=VCCM:";
    private string ccmRegionPath = "/Orion/Voip/Region.aspx?NetObject=VR:";
    private string netObjectId = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        var gateway = GetInterfaceInstance<IGatewayProvider>().Gateway.VoipGateway;
        imgStatus.Src = GetStatusImageURL(gateway.Status);
        lblStatus.Text = GetLocalizedProperty("CCMDeviceStatus", gateway.Status.ToString());
        lblName.Text = gateway.Name;
        lblDescription.Text = gateway.Description;
        lblIpAddress.Text = gateway.IPAddress;
        if (!string.IsNullOrEmpty(gateway.RegionName))
        {
            hlLocation.Text = gateway.RegionName;
            hlLocation.NavigateUrl = this.Page.ResolveUrl(ccmRegionPath + gateway.RegionId.ToString());
        }
        else
        {
            hlLocation.Text = NA;
        }
        imgCallManger.Src = "/Orion/Voip/images/CallIcons/cM_green_s.png";
        hlCallManager.Text = gateway.CallManagerName;
        hlCallManager.NavigateUrl = this.Page.ResolveUrl(callManagerPath + gateway.CallManagerNodeId.ToString());
        netObjectId = gateway.GatewayID.ToString();
    }

    private string GetStatusImageURL(CCMDeviceStatus ccmStatus)
    {
        string imageUrl = string.Empty;
        switch (ccmStatus)
        {
            case CCMDeviceStatus.Registered:
                imageUrl = "/Orion/Voip/images/CallIcons/g_green_s.PNG";
                break;
            case CCMDeviceStatus.Unregistered:
                imageUrl = "/Orion/Voip/images/CallIcons/g_red_l.png";
                break;
            case CCMDeviceStatus.Rejected:
                imageUrl = "/Orion/Voip/images/CallIcons/g_yellow_l.png";
                break;
            case CCMDeviceStatus.Unknown:
                imageUrl = "/Orion/Voip/images/CallIcons/g_grey_l.png";
                break;
        }
        return imageUrl;
    }

    public override string DetachURL
    {
        get
        {
            return string.Format("/Orion/DetachResource.aspx?ResourceID={0}&NetObject=VG:{1}", this.Resource.ID, netObjectId);
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.VNQMWebContent.VNQMWEBCODE_VB1_220; }
    }

    public override string HelpLinkFragment
    {
        get { return ""; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IGatewayProvider) }; }
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}