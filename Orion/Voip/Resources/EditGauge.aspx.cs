﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.NPM.Web.Gauge.V1;
using SolarWinds.Orion.Core.Common.i18n.Registrar;

public partial class Orion_Voip_Resources_EditGauge : SolarWinds.Orion.NPM.Web.UI.GaugeEditBasePage
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        resourceTitleEditor.ResourceTitle = Resource.Title;
        resourceTitleEditor.ResourceSubTitle = Resource.SubTitle;

        if (!IsPostBack)
        {
            //Set scale input
            string scale = Resource.Properties["Scale"];
            if (string.IsNullOrEmpty(scale))
                scale = "100";
            scaleInput.Text = scale;
            //Set styles combobox
            List<string> styles = SolarWinds.NPM.Web.Gauge.V1.GaugeHelper.GetAllGaugeStyles(GaugeType.Radial);
            foreach (var _style in styles)
            {
                stylesList.Items.Add(new ListItem(GetLocalizedProperty("GaugeStyle", _style), _style));
            }
            string style = Resource.Properties["Style"];
            if (string.IsNullOrEmpty(style))
                style = "Elegant Black";
            stylesList.SelectedValue = style;
            //Set sample gauges panel

            foreach (string gaugeStyle in styles)
            {
                SolarWinds.NPM.Web.Gauge.V1.Gauge gauge = new SolarWinds.NPM.Web.Gauge.V1.Gauge();
                gauge.GaugeStyle = gaugeStyle;
                gauge.IsSample = true;
                GaugeStylesPanel.Controls.Add(gauge);
            }
        }
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if ((!resourceTitleEditor.ResourceTitle.Equals(Resource.Title)) || (!resourceTitleEditor.ResourceSubTitle.Equals(Resource.SubTitle)))
        {
            Resource.Title = resourceTitleEditor.ResourceTitle;
            Resource.SubTitle = resourceTitleEditor.ResourceSubTitle;
            SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
        }

        Resource.Properties["Scale"] = scaleInput.Text;
        Resource.Properties["Style"] = stylesList.SelectedValue;

        NavigateToPreviousView();
    }

    protected void BackClick(object sender, EventArgs e)
    {
        NavigateToPreviousView();
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}
