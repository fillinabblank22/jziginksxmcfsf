﻿<%@ Page Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true" CodeFile="EditVoipResource.aspx.cs" Inherits="Orion_Voip_Resources_EditVoipResource" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1 style="padding-left: 0"><b><%= Page.Title %></b></h1>
    
    <b><orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="true" /></b>
    
    <br />
     
    <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" DisplayType="Primary" OnClick="SubmitClick" ToolTip="<%$ Resources : VNQMWebContent, VNQMWEBCODE_VB1_171 %>"/>   
</asp:Content>
