﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;

public partial class Orion_Voip_Resources_EditNodeTree : System.Web.UI.Page
{
    private ResourceInfo _resource;
    protected ResourceInfo Resource
    {
        get { return _resource; }
    }

    private string _netObjectID;

    protected override void OnInit(EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
        {
            int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
            this._resource = ResourceManager.GetResourceByID(resourceID);
            this.Title = string.Format(Resources.VNQMWebContent.VNQMWEBDATA_AK1_167, this.Resource.Title);
        }

        if (!string.IsNullOrEmpty(Request.QueryString["NetObject"]))
            _netObjectID = Request.QueryString["NetObject"];

        this.resourceTitleEditor.ResourceTitle = this.Resource.Title;
        this.SQLFilter.FilterTextBox.Text = this.Resource.Properties["Filter"];

        bool allowUnsortableProperties;
        bool.TryParse(Request.QueryString["AllowUnsortableProperties"] ?? "false", out allowUnsortableProperties);

        if (!this.IsPostBack)
        {
            foreach (string propName in Node.GetCustomPropertyNames(allowUnsortableProperties))
            {
                this.lbxGroup1.Items.Add(propName);
                this.lbxGroup2.Items.Add(propName);
                this.lbxGroup3.Items.Add(propName);
            }

            this.lbxGroup1.SelectedValue = this.Resource.Properties["Grouping1"];
            this.lbxGroup2.SelectedValue = this.Resource.Properties["Grouping2"];
            this.lbxGroup3.SelectedValue = this.Resource.Properties["Grouping3"];

            this.GroupNulls.SelectedValue = this.Resource.Properties["GroupNodesWithNullPropertiesAsUnknown"] ?? "true";

            string rememberCollapseState = this.Resource.Properties["RememberCollapseState"] ?? "true";
            this.RememberCollapseState.Checked = Boolean.Parse(rememberCollapseState);

            if (this.Resource.SubTitle != this.BuildSubTitle())
                this.resourceTitleEditor.ResourceSubTitle = this.Resource.SubTitle;
        }
    }

    private int ValidGroups
    {
        get
        {
            if (string.IsNullOrEmpty(this.lbxGroup1.SelectedValue))
                return 0;
            if (string.IsNullOrEmpty(this.lbxGroup2.SelectedValue))
                return 1;
            if (string.IsNullOrEmpty(this.lbxGroup3.SelectedValue))
                return 2;

            return 3;
        }
    }

    private string BuildSubTitle()
    {
        switch (this.ValidGroups)
        {
            case 0:
                return Resources.VNQMWebContent.VNQMWEBCODE_AK1_62;
            case 1:
                return string.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_63, this.lbxGroup1.SelectedItem.Text);
            case 2:
                return string.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_64, this.lbxGroup1.SelectedItem.Text, this.lbxGroup2.SelectedItem.Text);
            case 3:
                return string.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_65, this.lbxGroup1.SelectedItem.Text, this.lbxGroup2.SelectedItem.Text, this.lbxGroup3.SelectedItem.Text);
        }

        return string.Empty;
    }

    private void FixupBlankGroupings()
    {
        List<string> groups = new List<string>();
        groups.Add(lbxGroup1.SelectedValue);
        groups.Add(lbxGroup2.SelectedValue);
        groups.Add(lbxGroup3.SelectedValue);

        groups.RemoveAll(string.IsNullOrEmpty);

        for (int i = 0; i < 3; ++i) groups.Add(string.Empty);

        lbxGroup1.SelectedValue = groups[0];
        lbxGroup2.SelectedValue = groups[1];
        lbxGroup3.SelectedValue = groups[2];
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        FixupBlankGroupings();

        Resource.Title = resourceTitleEditor.ResourceTitle;
        Resource.SubTitle = string.IsNullOrEmpty(resourceTitleEditor.ResourceSubTitle) ? BuildSubTitle() : resourceTitleEditor.ResourceSubTitle;
        SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);

        this.Resource.Properties["Grouping1"] = this.lbxGroup1.SelectedValue;
        this.Resource.Properties["Grouping2"] = this.lbxGroup2.SelectedValue;
        this.Resource.Properties["Grouping3"] = this.lbxGroup3.SelectedValue;

        this.Resource.Properties["Filter"] = SqlFilterChecker.CleanFilter(this.SQLFilter.FilterTextBox.Text);

        this.Resource.Properties["GroupNodesWithNullPropertiesAsUnknown"] = this.GroupNulls.SelectedValue;
        this.Resource.Properties["RememberCollapseState"] = this.RememberCollapseState.Checked.ToString();

        // We changed stuff so clear out the expanded tree node information
        TreeStateManager manager = new TreeStateManager(Context.Session, Resource.ID);
        manager.Clear();

        string url = string.Format("/Orion/View.aspx?ViewID={0}", Resource.View.ViewID);
        if (!string.IsNullOrEmpty(_netObjectID))
            url = string.Format("{0}&NetObject={1}", url, _netObjectID);
        Response.Redirect(url);
    }
}
