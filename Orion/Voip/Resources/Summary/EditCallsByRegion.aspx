﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditCallsByRegion.aspx.cs" Inherits="Orion_Voip_Resources_Summary_EditCallsByRegion" MasterPageFile="~/Orion/ResourceEdit.master" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_AK1_167, Resource.Title) %></h1>
    <div style="padding-left: 16px;">
    <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="true" />
    <b><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_168 %></b>
    <br/>
        <asp:DropDownList runat="server" ID="ddlCallTime" CssClass="voipSearchDropDownListOriginDestination voipSearchFont">
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBCODE_AK1_33 %>" Value="0"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBCODE_AK1_26 %>" Value="1"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBCODE_AK1_27 %>" Value="2"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBCODE_AK1_28 %>" Value="3"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBCODE_AK1_29 %>" Value="4"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBCODE_AK1_30 %>" Value="5"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBCODE_AK1_31 %>" Value="6"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBCODE_AK1_32 %>" Value="7"></asp:ListItem>
        </asp:DropDownList>
    <br/>
    <br/>
    <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" DisplayType="Primary" OnClick="SubmitClick" />
    </div>
</asp:Content>
