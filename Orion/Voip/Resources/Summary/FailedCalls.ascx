<%@ Control Language="C#" ClassName="FailedCallCCMGraph" Inherits="SolarWinds.Orion.IpSla.Web.GraphResource" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web" %>

<script runat="server">

    protected static readonly string searchPageLinkText = Resources.VNQMWebContent.VNQMWEBCODE_AK1_35;
    protected override void OnInit(EventArgs e)
    {
        OrionInclude.ModuleFile("Voip", "Voip.css");
        base.OnInit(e);
    }
    protected string ChartImageUrl
    {
        get
        {
            return GetChartImageUrl(null, "VoipFailedCallSummary");
        }
    }

    protected string CustomUrl
    {
        get { return GenerateCustomChartLink(null, "VoipFailedCallSummary"); }
    }
    
	protected override string DefaultTitle
	{
        get { return Resources.VNQMWebContent.VNQMWEBCODE_AK1_36; }
	}

	public override string HelpLinkFragment
	{
        get { return "OrionIPSLAMonitorPHResourceVoIPFailedCalls"; }
	}
    
</script>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <a href="<%= this.CustomUrl %>">
            <img src="<%= this.ChartImageUrl %>" />
       </a>
       <div class ="voipSearchPageHyperLink">
       <asp:HyperLink ID="HyperLink1" CssClass="sw-link" runat="server" NavigateUrl="~/Orion/Voip/VoipCallSearch.aspx?CallTime=Last15Minutes&CallFailed=true"><%= searchPageLinkText%></asp:HyperLink>
       </div>
    </Content>
</orion:resourceWrapper>