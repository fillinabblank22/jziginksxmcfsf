using System;
using SolarWinds.Orion.IpSla.Web.UI;

public partial class EditThwack : ResourceEditPage
{
	protected override void Initialize()
	{
		if (!string.IsNullOrEmpty(Resource.Properties["NumberOfPosts"]) && !IsPostBack)
		{
			PostsCount.Text = Resource.Properties["NumberOfPosts"];
		}
	}

	protected void Page_Load(object sender, EventArgs e)
	{
	    this.Title = String.Format(Resources.VNQMWebContent.VNQMWEBDATA_AK1_167, Resource.Title);
	}

	protected void SubmitClick(object sender, EventArgs e)
	{
         // Server validation part, note it still could be an empty string
        if (PostsCountRangeValidator.IsValid)
        {
            int count;
            if (Int32.TryParse(PostsCount.Text, out count) || string.IsNullOrEmpty(PostsCount.Text))
            {
                Resource.Properties.Clear();
                if (!string.IsNullOrEmpty(PostsCount.Text))
                {
                    Resource.Properties.Add("NumberOfPosts", count.ToString());
                }

                GoBack();
            }
        }
	}
}
