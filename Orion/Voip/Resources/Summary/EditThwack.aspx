<%@ Page Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true" CodeFile="EditThwack.aspx.cs" Inherits="EditThwack" Title="Untitled Page"%>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1 style="padding-left: 0px"><%= Page.Title %></h1>
    <div>

    <br />
    <b><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_169 %></b>
    <br/>
        <asp:TextBox ID="PostsCount" runat="server" />
    <br/>
    <asp:RangeValidator ID="PostsCountRangeValidator" runat="server" 
            ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_169 %>" Display="Dynamic" ControlToValidate="PostsCount" 
            Type="Integer" MinimumValue="1" SetFocusOnError="True" MaximumValue="1000"></asp:RangeValidator>
    <br />
    <div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" ToolTip="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_215 %>" DisplayType="Primary" OnClick="SubmitClick" />
    </div>
    </div>
</asp:Content>