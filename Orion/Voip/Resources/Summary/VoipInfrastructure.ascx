<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VoipInfrastructure.ascx.cs" Inherits="Orion_Voip_Resources_Summary_VoipInfrastructure" %>
<%@ Register TagPrefix="npm" TagName="NodeLink" Src="~/Orion/NetPerfMon/Controls/NodeLink.ascx" %>
<%@ Register TagPrefix="npm" TagName="SmallNodeStatus" Src="~/Orion/Controls/SmallNodeStatus.ascx" %>
<%@ Register TagPrefix="npm" TagName="SmallStatusIcon" Src="~/Orion/Controls/SmallStatusIcon.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
		<asp:PlaceHolder runat="server" ID="phTree" />
		<asp:PlaceHolder runat="server" ID="noNodes">
			<%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_AK1_191, "<a class=\"sw-link\" href= \"/Orion/Voip/Admin/Infrastructure.aspx\">", "</a>") %>
		</asp:PlaceHolder>
	</Content>	
</orion:resourceWrapper>