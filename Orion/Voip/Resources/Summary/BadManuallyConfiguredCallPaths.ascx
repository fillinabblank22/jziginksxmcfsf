<%@ Import namespace="SolarWinds.Orion.Web.DisplayTypes"%>
<%@ Import namespace="SolarWinds.Orion.IpSla.Web"%>
<%@ Control Language="C#" ClassName="BadManuallyConfiguredCallPaths" Inherits="SolarWinds.Orion.Web.UI.BaseResourceControl" %>
<%@ Register TagPrefix="voip" TagName="StatusIcon" Src="~/Orion/Voip/Controls/OperationStatusImage.ascx" %>

<script runat="server">
    protected override string DefaultTitle { get { return Resources.VNQMWebContent.VNQMWEBDATA_AK1_152; } }
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        IList<CallPath> badManConfCallPaths = CallPath.GetBadManuallyConfiguredCallPaths();
        if (badManConfCallPaths.Count != 0)
        {
            BadManualCallPathsGrid.DataSource = badManConfCallPaths;
            BadManualCallPathsGrid.DataBind();
        }
        else
        {
            Wrapper.Visible = false;
        }
    }
	
	public override string HelpLinkFragment
	{
        get { return "OrionVoIPMonitorPHResourceMisconfiguredCallPaths"; }
	}
</script>

<orion:resourceWrapper ID="Wrapper" runat="server"  Visible ="true">
	<Content>
		<asp:Repeater runat="server" ID="BadManualCallPathsGrid">
	        <HeaderTemplate>
		        <table class="DataGrid">
			        <thead>
				        <tr>
				            <td align="left"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_153 %></td>
					        <td/>
				        </tr>
			        </thead>
	        </HeaderTemplate>
	        <ItemTemplate>
		        <tr>
			        <td>
				        <a href="/Orion/Voip/Admin/ManageOperations.aspx">
						    <voip:StatusIcon ID="StatusIcon1" runat="server" OperationStatus='<%#Eval("Status") %>' StatusMessage='<%#Eval("StatusMessage") %>' />&nbsp;<%#Eval("Name")%>
				        </a>
			        </td>
			        <td style="text-align:right">
			            <a href="/Orion/Voip/Admin/ManageOperations.aspx"/>
			            <font color="Blue"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_154 %></font>
			        </td>
		        </tr>
	        </ItemTemplate>
	        <FooterTemplate>
		        </table>
	        </FooterTemplate>
        </asp:Repeater>
	</Content>
</orion:resourceWrapper>
