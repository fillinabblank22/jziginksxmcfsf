using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Voip_Resources_Summary_InternalTopSitesByMetric : UserControl
{
    private int _howMany;

    public int HowMany
    {
        get { return _howMany; }
        set { _howMany = value; }
    }

    private ElementsOfQualityMetrics _metric;

    public ElementsOfQualityMetrics Metric
    {
        get { return _metric; }
        set { _metric = value; }
    }

    private bool _highValuesAreBetter;

    public bool HighValuesAreBetter
    {
        get { return _highValuesAreBetter; }
        set { _highValuesAreBetter = value; }
    }

    private string _metricHeader;

    public string MetricHeader
    {
        get { return _metricHeader; }
        set { _metricHeader = value; }
    }

    private string _metricData;

    public string MetricData
    {
        get { return _metricData; }
        set { _metricData = value; }
    }

	public string GetEditUrl(ResourceInfo resource)
	{
		string netobject = Request["NetObject"];
		string url = string.Format("/Orion/Voip/Resources/EditTopCallPaths.aspx?ResourceID={0}&ObjectType=sites", resource.ID);
		if (!string.IsNullOrEmpty(netobject))
			url = string.Format("{0}&NetObject={1}", url, netobject);
		return url;
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
        SiteGrid.DataSource = SolarWinds.Orion.IpSla.Web.Site.TopNVoipSitesByWorstMetric(_howMany, _metric, _highValuesAreBetter);
        SiteGrid.DataBind();
    }
}
