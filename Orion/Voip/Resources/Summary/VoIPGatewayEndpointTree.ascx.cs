﻿using System;
using Resources;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.WebServices;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.VOIP)]
public partial class Orion_Voip_Resources_Summary_VoIPGatewayEndpointTree : BaseResourceControl
{
    protected override string DefaultTitle => VNQMWebContent.WEBDATA_RKG_3;
    public override string HelpLinkFragment => "OrionIPSLAManagerPHResourceVoIPGateways";
    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl;

    protected void Page_Load(object sender, EventArgs e)
    {

        //Wrapper.ManageButtonImage = "~/Orion/Voip/Images/Button.Manage.VoipGateways.gif";
        Wrapper.ManageButtonTarget = "~/Orion/Voip/Admin/ManageMediaGateway/Default.aspx";
        Wrapper.ManageButtonText = VNQMWebContent.WEBDATA_RKG_4;
        Wrapper.ShowManageButton = Profile.AllowAdmin || OrionConfiguration.IsDemoServer;

        var netObjectID = string.Empty;

        var provider = GetInterfaceInstance<INodeProvider>();
        if (provider != null) netObjectID = provider.Node.NetObjectID;

        AjaxTree.LoadTree(new VoIPGatewayEndpointsService(), new object[]
        {
            new object[] {},
            Resource.ID,
            netObjectID
        });
    }
}