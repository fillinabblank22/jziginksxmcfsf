﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CallsByRegion.ascx.cs"
    Inherits="Orion_Voip_Resources_Summary_CallsByRegion" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Data" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebChart.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebChart" TagPrefix="igChart" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web.Charting" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div style="min-height: 160px;">
            <asp:PlaceHolder ID="content" Visible="true" runat="server">
                <table class="ipsla_removeBottomTableBorder" cellpadding="0" cellspacing="0" border="0">
                    <tr style="border: 0 none">
                        <td style="border: solid 0 White" id="overviewPie">
                            <igChart:UltraChart runat="server" ID="pieChart" ImagePipePageName="/Orion/VoIP/Controls/ChartImagePipe.aspx"
                                BackColor="White" Height="150px" Width="150px" ChartType="PieChart3D" Transform3D-Scale="100"
                                Transform3D-XRotation="7" Transform3D-YRotation="3" Transform3D-ZRotation="-55">
                                <Tooltips Display="MouseMove" BackColor="Yellow" BorderColor="Black" Format="Custom"
                                    FormatString=" <ITEM_LABEL>: <DATA_VALUE:0> " BorderThickness="0" Font-Names="Arial,Verdana,Helvetica,sans-serif"
                                    Font-Size="8" Padding="10" />
                                <Border DrawStyle="Solid" Raised="false" Color="White" Thickness="0" />
                                <DeploymentScenario Scenario="Session" />
                                <PieChart3D OthersCategoryPercent="0" PieThickness="9" RadiusFactor="129">
                                    <Labels Visible="False" FormatString="" LeaderLinesVisible="False" BorderColor="White" />
                                </PieChart3D>
                                <Axis>
                                    <PE ElementType="None" />
                                </Axis>
                            </igChart:UltraChart>
                        </td>
                        <td style="border: 0 none; white-space: nowrap; vertical-align: middle" id="leftOverviewStatuses"
                            runat="server">
                            <asp:Repeater runat="server" ID="RegionDetailsRepearter" OnItemDataBound="RegionDetailsRepeater_DataBound">
                                <HeaderTemplate>
                                    <table cellspacing="0">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr id="trRegionDetailRow" runat="server">
                                        <td class="voipCallsbyRegionPaddingBottom">
                                            <div id="divColor" runat="server"></div>
                                        </td>
                                        <td class="NoBorder voipSearchFont voipCallsbyRegionPaddingLeft voipCallbyRegionNameAlignment">
                                            <asp:HyperLink runat="server" ID="hlRegion" Text='<%#DataBinder.Eval(Container.DataItem,"RegionName")%>'></asp:HyperLink>
                                            <asp:Label runat="server" ID="lblRegion" Text='<%#DataBinder.Eval(Container.DataItem,"RegionName")%>'></asp:Label>
                                        </td>
                                        <td class="NoBorder voipSearchFont voipCallsbyRegionPaddingBottom">
                                            <asp:HyperLink runat="server" ID="hlCalls" Text='<%#DataBinder.Eval(Container.DataItem,"CallsCount")%>'></asp:HyperLink>
                                            <asp:Label runat="server" ID="lblCalls" Text='<%#DataBinder.Eval(Container.DataItem,"CallsCount")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr>
                                        <td class="NoBorder">
                                            &nbsp;
                                        </td>
                                        <td class="NoBorder voipSearchFont voipCallsbyRegionPaddingLeft">
                                            <b><%= total %></b>
                                        </td>
                                        <td class="NoBorder voipSearchFont">
                                            <%= totalCalls %>
                                        </td>
                                    </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                        <td id="NoCalls" runat="server" visible="false" class="NoBorder voipSearchFont">
                        <%= noCalls %>
                        </td>
                    </tr>
                </table>
            </asp:PlaceHolder>
        </div>
    </Content>
</orion:resourceWrapper>
