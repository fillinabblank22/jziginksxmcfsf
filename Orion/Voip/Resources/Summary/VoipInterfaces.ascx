<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VoipInterfaces.ascx.cs" Inherits="Orion_Voip_Resources_Summary_VoipInterfaces" %>

<orion:resourceWrapper runat="server" ID="Wrapper" Visible="False">
	<Content>
		<asp:PlaceHolder runat="server" ID="noInterfaces" Visible="false">
			<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_192 %>
		</asp:PlaceHolder>
	</Content>
</orion:resourceWrapper>