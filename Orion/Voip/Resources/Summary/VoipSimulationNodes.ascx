﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VoipSimulationNodes.ascx.cs" Inherits="Orion_Voip_Resources_Summary_VoipSimulationNodes" %>
<%@ Register TagPrefix="npm" TagName="NodeLink" Src="~/Orion/NetPerfMon/Controls/NodeLink.ascx" %>
<%@ Register TagPrefix="npm" TagName="SmallNodeStatus" Src="~/Orion/Controls/SmallNodeStatus.ascx" %>
<%@ Register TagPrefix="npm" TagName="SmallStatusIcon" Src="~/Orion/Controls/SmallStatusIcon.ascx" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
		<asp:PlaceHolder runat="server" ID="phTree" />
	</Content>	
</orion:resourceWrapper>