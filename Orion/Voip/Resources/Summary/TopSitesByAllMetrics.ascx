<%@ Control Language="C#" ClassName="TopSitesByAllMetrics" Inherits="SolarWinds.Orion.Web.UI.BaseResourceControl" %>
<%@ Register Src="InternalTopSitesByMetric.ascx" TagName="InternalTopSitesByMetric"
	TagPrefix="uc1" %>
	
<script runat="server">
    protected override string DefaultTitle { get { return Resources.VNQMWebContent.VNQMWEBCODE_AK1_72; } }
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        AddStylesheet("/Orion/Voip/Voip.css");
    }

	public override string HelpLinkFragment
	{
		get { return "OrionVoIPMonitorPHResourceWorstSitesAll"; }
	}

	public override string EditURL
	{
		get { return InternalTopSitesByMetric1.GetEditUrl(this.Resource); }
	}

	protected void InitTopSites(object sender, EventArgs e)
	{
		InternalTopSitesByMetric1.HowMany = HowMany;
		InternalTopSitesByMetric2.HowMany = HowMany;
		InternalTopSitesByMetric3.HowMany = HowMany;
		InternalTopSitesByMetric4.HowMany = HowMany;
	}

	protected int HowMany { get { return GetIntProperty("HowMany", 5); } }
	public override string DisplayTitle { get { return Title.Replace("XX", HowMany.ToString()); } }
</script>

<orion:resourceWrapper ID="Wrapper" runat="server">
	<Content>
		<table class="TopCallPathsByAllMetrics">
			<tr>
				<td>
					<uc1:InternalTopSitesByMetric ID="InternalTopSitesByMetric3" runat="server" OnInit="InitTopSites" Metric="MOS_OF_QM" MetricHeader="<%$ Resources: VNQMWebContent, VNQMWEBCODE_VB1_145 %>" MetricData="MOS" HighValuesAreBetter="true" />
				</td>
				<td>
					<uc1:InternalTopSitesByMetric ID="InternalTopSitesByMetric1" runat="server" OnInit="InitTopSites" Metric="JITTER_OF_QM" MetricHeader="<%$ Resources: VNQMWebContent, VNQMWEBCODE_VB1_146 %>" MetricData="Jitter" />
				</td>
			</tr>
			<tr>
				<td>
					<uc1:InternalTopSitesByMetric ID="InternalTopSitesByMetric2" runat="server" OnInit="InitTopSites" Metric="LATENCY_OF_QM" MetricHeader="<%$ Resources: VNQMWebContent, VNQMWEBCODE_VB1_147 %>" MetricData="Latency" />
				</td>
				<td>
					<uc1:InternalTopSitesByMetric ID="InternalTopSitesByMetric4" runat="server" OnInit="InitTopSites" Metric="PACKETLOSSRATIO_OF_QM" MetricHeader="<%$ Resources: VNQMWebContent, VNQMWEBCODE_VB1_148 %>" MetricData="PacketLoss" />
				</td>
			</tr>
		</table>
	</Content>
</orion:resourceWrapper>
