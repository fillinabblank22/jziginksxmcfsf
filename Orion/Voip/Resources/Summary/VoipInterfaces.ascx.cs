using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Orion.IpSla.Common.Settings;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.VOIP)]
public partial class Orion_Voip_Resources_Summary_VoipInterfaces : VoipBaseResource
{
    private Control interfaceListControl;

    protected override void OnInit(EventArgs e)
    {
        if (!IpSlaSettings.AreInterfacesSupported)
        {
            Wrapper.Visible = true;
            noInterfaces.Visible = true;
            return;
        }

        if (!string.IsNullOrEmpty(IpSlaSettings.CoreVersion.Value))
        {
            if (IpSlaSettings.CoreVersion.Value.StartsWith("9.5"))
            {
                interfaceListControl = LoadControl("~/Orion/NetPerfMon/InternalInterfaceList.ascx");
            }
            else
            {
                interfaceListControl = LoadControl("~/Orion/NPM/InternalInterfaceList.ascx");
            }
        }
        else
        {
            interfaceListControl = LoadControl("~/Orion/NetPerfMon/InternalInterfaceList.ascx");
            if (interfaceListControl == null)
            {
                interfaceListControl = LoadControl("~/Orion/NPM/InternalInterfaceList.ascx");
            }

        }
        if (interfaceListControl != null)
        {
            interfaceListControl.Init += interfaceListControl_Init;
            this.Controls.Add(interfaceListControl);
        }
        
    }

    void interfaceListControl_Init(object sender, EventArgs e)
    {
        IList<Interface> interfaces = VoipNode.GetInterfaces();
        interfaceListControl.GetType().GetProperty("DataSource").SetValue(interfaceListControl, interfaces, null);
        interfaceListControl.GetType().GetMethod("DataBind").Invoke(interfaceListControl, null);
    }

	protected override string DefaultTitle => Resources.VNQMWebContent.VNQMWEBDATA_AK1_193;

    public override string HelpLinkFragment => "OrionVoIPMonitorPHResourceInterfaces";
}
