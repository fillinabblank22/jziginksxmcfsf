﻿using System;
using SolarWinds.Orion.IpSla.Web.UI;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_Voip_Resources_Summary_EditCallsByRegion : ResourceEditPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = String.Format(Resources.VNQMWebContent.VNQMWEBDATA_AK1_167, Resource.Title);
    }

    protected override void Initialize()
    {
        if (Resource.Properties.ContainsKey("TimePeriod") && !IsPostBack)
        {
            if (!string.IsNullOrEmpty(Resource.Properties["TimePeriod"]))
            {
                ddlCallTime.SelectedValue = Resource.Properties["TimePeriod"];
            }
        }

        resourceTitleEditor.ResourceTitle = Resource.Title;
        resourceTitleEditor.ResourceSubTitle = Resource.SubTitle;
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (this.Page.IsValid)
        {
            Resource.Properties["TimePeriod"] = ddlCallTime.SelectedValue;

            if (!resourceTitleEditor.ResourceTitle.Equals(Resource.Title))
            {
                Resource.Title = resourceTitleEditor.ResourceTitle;
            }

            if (!resourceTitleEditor.ResourceSubTitle.Equals(Resource.SubTitle))
            {
                Resource.SubTitle = resourceTitleEditor.ResourceSubTitle;
            }

            ResourcesDAL.Update(Resource);

            GoBack();
        }
    }
}