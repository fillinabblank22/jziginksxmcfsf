using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using ASP;
using Resources;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.VOIP)]
public partial class Orion_Voip_Resources_Summary_CallManagers : VoipBaseResource
{
    Dictionary<string,IList<CallManager>> _groupCCMs = new Dictionary<string, IList<CallManager>>();
    protected bool _isGroping;
    private string _group;

    protected void CallManagers_Init(object sender, EventArgs e)
	{
        AddStylesheet("/Orion/Voip/Voip.css");

        _group = this.Resource.Properties["GroupingBy"];
        
        if (!string.IsNullOrEmpty(_group))
        {
            _groupCCMs = CallManager.GetGroupCallManager(_group);
            _isGroping = true;
        }
            
        if(_groupCCMs.Count == 0)
        {
            _groupCCMs.Add(string.Empty,(List<CallManager>)CallManager.GetAllCallManagers());
            _isGroping = false;
        }

        CallManagers.DataSource = _groupCCMs.Keys;
        CallManagers.DataBind();

        CallManagers.Visible = _groupCCMs.Count > 0;

        NoCallManagers.Visible = ((!_isGroping && _groupCCMs[string.Empty].Count==0) || _groupCCMs.Count == 0);
	}
   
    protected void CallManagers_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        
        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
        {
            
            CallManagerBody ccmBody = (CallManagerBody)item.FindControl("ccmBodyId");
            Repeater rep = (Repeater)ccmBody.FindControl("CallManagerRepeater");

            var ccms = !_isGroping ? _groupCCMs[string.Empty] : _groupCCMs[(string)item.DataItem];
            ReviseCCMsPercentageStats(ccms);
            rep.DataSource = ccms;
            rep.DataBind();
        }
    }

    private void ReviseCCMsPercentageStats(IEnumerable<CallManager> ccms)
    {
        foreach (var cm in ccms)
        {
            float regPh = cm.ActivePhonesPercentage;
            float unregPh = cm.InactivePhonesPercentage;
            float rejPh = cm.RejectedPhonesPercentage;
            float regGw = cm.ActiveGatewaysPercentage;
            float unregGw = cm.InactiveGatewaysPercentage;
            float rejGw = cm.RejectedGatewaysPercentage;

            //Percentage correction for phones
            PercentageCorrection(ref regPh, ref unregPh, ref rejPh);
            //Percentage correction for gateways
            PercentageCorrection(ref regGw, ref unregGw, ref rejGw);

            cm.ActivePhonesPercentage = regPh;
            cm.InactivePhonesPercentage = unregPh;
            cm.RejectedPhonesPercentage = rejPh;
            cm.ActiveGatewaysPercentage = regGw;
            cm.InactiveGatewaysPercentage = unregGw;
            cm.RejectedGatewaysPercentage = rejGw;
        }
    }

    private void PercentageCorrection(ref float a, ref float b, ref float c)
    {
        float ar = (a > 0 && a <= 1) ? 1 : (float)Math.Round(a);
        float br = (b > 0 && b <= 1) ? 1 : (float)Math.Round(b);
        float cr = (c > 0 && c <= 1) ? 1 : (float)Math.Round(c);

        a = ar > br && ar > cr ? 100 - br - cr : ar;
        b = br >= ar && br > cr ? 100 - ar - cr : br;
        c = cr >= ar && cr >= br && cr != 0 ? 100 - ar - br : cr;
    }

    protected string MakeID(object groupName, string tag)
    {
        return $"{tag}_group{groupName}_res{this.Resource.ID}";
    }

    #region BaseResourceControl methods

    protected override string DefaultTitle => VNQMWebContent.VNQMWEBCODE_AK1_20;

    public override string HelpLinkFragment => "OrionVoIPMonitorPHResourceCallManagers";

    public override string EditURL => GetEditUrl(this.Resource);

    #endregion

    private string GetEditUrl(ResourceInfo resource)
    {
        string netobject = Request["NetObject"];
        string url = $"/Orion/Voip/Resources/EditCallManagers.aspx?ResourceID={resource.ID}";
        if (!string.IsNullOrEmpty(netobject))
            url = $"{url}&NetObject={netobject}";
        return url;
    }
}
