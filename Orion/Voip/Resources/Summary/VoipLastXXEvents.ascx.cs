﻿using System;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Events)]
public partial class Orion_Voip_Resources_Summary_VoipLastXXEvents : VoipBaseResource
{
    private int maxCount;

    protected void Page_Load(object sender, EventArgs e)
    {
        maxCount = GetIntProperty(ResourcePropertiesHelper.MaxCountProperty, 25);
        EventsControl.LoadData(VoipEvent.GetLastXXVoipEvents(maxCount));
    }

    protected override string DefaultTitle => Resources.VNQMWebContent.VNQMWEBCODE_AK1_59;

    public override string DisplayTitle => Title.Replace("XX", maxCount.ToString());


    public override string HelpLinkFragment => "OrionIPSLAMonitorPHResourceVoIPLast25VoIPEvents";

    public override string EditControlLocation => "/Orion/Voip/Controls/EditEventList.ascx";
}