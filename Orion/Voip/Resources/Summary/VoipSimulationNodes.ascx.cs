﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.IPSLA)]
public partial class Orion_Voip_Resources_Summary_VoipSimulationNodes : VoipBaseResource
{
    protected override string DefaultTitle => Resources.VNQMWebContent.VNQMWEBCODE_AK1_60;

    protected IEnumerable<NetObject> GetNodes()
    {
        return VoipNode.GetVoipSimulationNodes();
    }

    public override string HelpLinkFragment => "OrionVoIPMonitorPHSimulationNodesResource";

    protected override void OnInit(EventArgs e)
    {
        BuildNodeList();
        base.OnInit(e);
    }

    private void BuildNodeList()
    {
        var nodes = VoipNode.GetVoipSimulationNodes();
        foreach (Node n in nodes)
        {
            var myLink =
                (Orion_NetPerfMon_Controls_NodeLink) LoadControl("/Orion/NetPerfMon/Controls/NodeLink.ascx");
            myLink.Node = n;

            var status = (ASP.SmallNodeStatus) LoadControl("/Orion/Controls/SmallNodeStatus.ascx");
            status.StatusValue = n.Status;

            myLink.Content.Controls.Add(status);
            myLink.Content.Controls.Add(new LiteralControl(n.Name));

            phTree.Controls.Add(myLink);
            phTree.Controls.Add(new LiteralControl("<br />"));
        }
    }
}
