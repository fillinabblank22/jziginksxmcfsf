using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Net;

using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;

using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.IPSLA)]
public partial class Orion_Voip_Resources_Thwack : BaseResourceControl
{
    private string _thwackURL = @"https://thwackfeeds.solarwinds.com/community/feeds/threads/orion-ip-sla-manager.aspx";
    private readonly DataList _list = new DataList();
    private const int DefaultNumberOfPosts = 15;

    protected string HelpURL = string.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        HelpURL = HelpLocator.GetHelpUrl(HelpLinkFragment);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Refresh();
    }

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl;

    public string ResourceID
    {
        get { return Resource.ID.ToString(); }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public int Count
    {
        get
        {
            int count;
            if (int.TryParse(Resource.Properties["NumberOfPosts"], out count))
            {
                return count;
            }
            return DefaultNumberOfPosts;
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.VNQMWebContent.VNQMWEBCODE_AK1_50; }
    }

    public override string SubTitle
    {
        get { return string.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_51, this.Count.ToString()); }
    }

    public override string EditURL
    {
        get
        {
            return string.Format(@"/Orion/Voip/Resources/Summary/EditThwack.aspx?ResourceID={0}&ViewID={1}",
                                 this.Resource.ID, this.Resource.View.ViewID);
        }
    }

    public override string HelpLinkFragment
    {
		get { return "OrionPHResourceSearchThwackCom"; }
    }

    private static string FormatRecentPostsTable(List<PostItem> posts, int count)
    {
        StringWriter content = new StringWriter();
        HtmlTextWriter writer = new HtmlTextWriter(content);

        writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "0");
        writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing, "0");
        writer.RenderBeginTag(HtmlTextWriterTag.Table);

        int realCount = Math.Min(count, posts.Count);
        for (int i = 0; i < realCount; i++)
        {
            if (i % 2 != 0)
            {
                writer.AddStyleAttribute(HtmlTextWriterStyle.BackgroundColor, "#e4f1f8");
            }
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            writer.RenderBeginTag(HtmlTextWriterTag.Ul);


            writer.RenderBeginTag(HtmlTextWriterTag.Li);


            writer.AddAttribute(HtmlTextWriterAttribute.Href, posts[i].Link);
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.WriteEncodedText(posts[i].Title);

            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
        }
        writer.RenderEndTag();
        return content.ToString();
    }

    private void Refresh()
    {
        try
        {
            noThwackLabel.Text = "";
            noThwackLabel.CssClass = "Property";
            PostItemList itemList = GetPostItemsList();

            if (itemList != null && itemList.ItemList != null)
            {
                if (itemList.ItemList.Count > 0)
                {
                    string tableContent = FormatRecentPostsTable(itemList.ItemList,this.Count);
                    thwackRecentPostsList.InnerHtml = tableContent;
                }
                else
                {
                    noThwackLabel.Visible = true;
                    noThwackLabel.Text = Resources.VNQMWebContent.VNQMWEBCODE_AK1_52;
                }
            }
            else
            {
                noThwackLabel.Visible = true;
                noThwackLabel.Text = Resources.VNQMWebContent.VNQMWEBCODE_AK1_52;
            }
        }
        catch (Exception)
        {
            noThwackLabel.ForeColor = System.Drawing.Color.Red;
            noThwackLabel.Text = Resources.VNQMWebContent.VNQMWEBCODE_AK1_53;
        }
    }

    private PostItemList GetPostItemsList()
    {             
        XmlSerializer serializer = new XmlSerializer(typeof(PostItemList));
       
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_thwackURL);

        try
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["proxyAvailable"]))
            {
                string szport = ConfigurationManager.AppSettings["proxyPort"];
                
                if( string.IsNullOrEmpty(szport))
                    request.Proxy= new WebProxy(ConfigurationManager.AppSettings["proxyAddress"]);
                else                
                    request.Proxy = new WebProxy(ConfigurationManager.AppSettings["proxyAddress"], Convert.ToInt32(szport));

                string username = ConfigurationManager.AppSettings["userName"];
                string userpwd = ConfigurationManager.AppSettings["password"];

                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(userpwd))
                    request.Proxy.Credentials = new NetworkCredential(username, userpwd);                
            }
        }
        catch
        {
            // no proxy or bad defined
            // TODO: maybe some log event message
        }


        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        Stream dataStream = response.GetResponseStream();

        using (StreamReader reader = new StreamReader(dataStream))
        {
            return (PostItemList)serializer.Deserialize(reader);
        }
    }

    [XmlRoot("rss")]
    public class PostItemList
    {
        [XmlArray("channel")]
        [XmlArrayItem("item")]
        public List<PostItem> ItemList;
    }

    public class PostItem
    {
        private string _title;
        private string _link;

        public PostItem()
        {
        }

        [XmlElement("title")]
        public string Title
        {
            get { return _title; }
            set { _title = Environment.ExpandEnvironmentVariables(value); }
        }

        [XmlElement("link")]
        public string Link
        {
            get { return _link; }
            set { _link = Environment.ExpandEnvironmentVariables(value); }
        }
    }
}
