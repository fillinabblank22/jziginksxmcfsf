<%@ Control Language="C#" ClassName="TopCallPathsByAllMetrics" Inherits="SolarWinds.Orion.Web.UI.BaseResourceControl" %>
<%@ Register Src="InternalTopCallPathsByMetric.ascx" TagName="InternalTopCallPathsByMetric"
	TagPrefix="uc1" %>

<script runat="server">
    protected override string DefaultTitle => Resources.VNQMWebContent.VNQMWEBCODE_AK1_67;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        AddStylesheet("/Orion/Voip/Voip.css");
    }

    public override string HelpLinkFragment => "OrionIPSLAMonitorPHResourceVoIPTop5callpath";

    public override string EditURL => InternalTopCallPathsByMetric1.GetEditUrl(this.Resource);

    public override string DetachURL => $"/Orion/Voip/DetachResource.aspx?ResourceID={this.Resource.ID}&NetObject=";

    protected void InitTopCallPaths(object sender, EventArgs e)
    {
        InternalTopCallPathsByMetric1.HowMany = HowMany;
        InternalTopCallPathsByMetric2.HowMany = HowMany;
        InternalTopCallPathsByMetric3.HowMany = HowMany;
        InternalTopCallPathsByMetric4.HowMany = HowMany;
    }

    protected int HowMany => GetIntProperty("HowMany", 5);

    public override string DisplayTitle => Title.Replace("XX", HowMany.ToString());

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl; 
</script>

<orion:resourceWrapper ID="Wrapper" runat="server">
	<Content>
		<table class="TopCallPathsByAllMetrics">
			<tr>
				<td valign="top">
					<uc1:InternalTopCallPathsByMetric ID="InternalTopCallPathsByMetric3" runat="server" OnInit="InitTopCallPaths" Metric="MOS_OF_QM" MetricHeader="<%$ Resources: VNQMWebContent, VNQMWEBCODE_VB1_145 %>" MetricData="MOS" HighValuesAreBetter="true" />
				</td>
				<td valign="top">
					<uc1:InternalTopCallPathsByMetric ID="InternalTopCallPathsByMetric1" runat="server" OnInit="InitTopCallPaths" Metric="JITTER_OF_QM" MetricHeader="<%$ Resources: VNQMWebContent, VNQMWEBCODE_VB1_146 %>" MetricData="Jitter" />
				</td>
			</tr>
			<tr>
				<td valign="top">
					<uc1:InternalTopCallPathsByMetric ID="InternalTopCallPathsByMetric2" runat="server" OnInit="InitTopCallPaths" Metric="LATENCY_OF_QM" MetricHeader="<%$ Resources: VNQMWebContent, VNQMWEBCODE_VB1_147 %>" MetricData="Latency" />
				</td>
				<td valign="top">
					<uc1:InternalTopCallPathsByMetric ID="InternalTopCallPathsByMetric4" runat="server" OnInit="InitTopCallPaths" Metric="PACKETLOSSRATIO_OF_QM" MetricHeader="<%$ Resources: VNQMWebContent, VNQMWEBCODE_VB1_148 %>" MetricData="PacketLoss" />
				</td>
			</tr>
		</table>
	</Content>
</orion:resourceWrapper>
