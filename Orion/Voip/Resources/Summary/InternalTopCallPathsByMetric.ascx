<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InternalTopCallPathsByMetric.ascx.cs" Inherits="Orion_Voip_Resources_Summary_InternalTopCallPathsByMetric" %>
<%@ Register TagPrefix="iplsa" TagName="StatusIcon" Src="~/Orion/Voip/Controls/OperationStatusImage.ascx" %>

<asp:Repeater runat="server" ID="CallPathGrid">
	<HeaderTemplate>
		<table class="NeedsZebraStripes DataGrid">
			<thead>
				<tr>
					<td width="80%"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_153 %></td>
					<td style="text-align:left;white-space:nowrap"><%=MetricHeader %></td>
				</tr>
			</thead>
	</HeaderTemplate>
	<ItemTemplate>
		<tr>
			<td width="80%">
				<a href="<%#BaseResourceControl.GetViewLink((string)Eval("NetObjectID"))%>">
					<iplsa:StatusIcon ID="SmallNodeStatus1" runat="server" OperationStatus='<%#Eval("Status") %>' StatusMessage='<%#Eval("StatusMessage") %>'/>							
					<%#Eval("Name")%>
				</a>
			</td>
			<td style="text-align:left" class="<%#Eval(MetricData, "{0:class}")%>"><%#Eval(MetricData)%></td>
		</tr>
	</ItemTemplate>
	<FooterTemplate>
		</table>
	</FooterTemplate>
</asp:Repeater>
