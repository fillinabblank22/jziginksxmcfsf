using System;
using System.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Voip_Resources_Summary_InternalTopCallPathsByMetric : UserControl
{
    public int HowMany { get; set; }

    public ElementsOfQualityMetrics Metric { get; set; }

    public bool HighValuesAreBetter { get; set; }

    public string MetricHeader { get; set; }

    public string MetricData { get; set; }

    public string GetEditUrl(ResourceInfo resource)
	{
		var netobject = Request["NetObject"];
		var url = $"/Orion/Voip/Resources/EditTopCallPaths.aspx?ResourceID={resource.ID}&ObjectType=call paths";
		if (!string.IsNullOrEmpty(netobject))
			url = $"{url}&NetObject={netobject}";
		return url;
	}

	protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        var site = BaseResourceControl.GetInterfaceInstance<ISiteProvider>(this)?.Site;
        CallPathGrid.DataSource = CallPath.TopNCallPathsByWorstMetric(site, HowMany, Metric, HighValuesAreBetter);
        CallPathGrid.DataBind();
    }
}
