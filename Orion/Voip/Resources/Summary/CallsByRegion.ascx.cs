﻿using System;
using System.Data;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Infragistics.UltraChart.Resources.Appearance;
using Infragistics.UltraChart.Shared.Styles;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.Charting;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]  //hidden purposly FB 241737
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_Voip_Resources_Summary_CallsByRegion : VoipBaseResource
{
    private const string voipSearchPath = "/Orion/Voip/VoipCallSearch.aspx?OrgLocation={0}&DestLocation={1}&CallTime={2}&useOR=1";
    private static readonly string calls = Resources.VNQMWebContent.VNQMWEBCODE_AK1_79;
    protected static readonly string total = Resources.VNQMWebContent.VNQMWEBCODE_AK1_80;
    protected string noCalls = Resources.VNQMWebContent.VNQMWEBCODE_AK1_25;

    protected int totalCallCount = 0;
    protected string totalCalls = string.Empty;
    private NumericSeries series = new NumericSeries();
    private DataTable callsByRegionTable = new DataTable();

    private static readonly Color[] ChartColors = new Color[]
    {
        StatusColors.ColorUp,
        StatusColors.ColorUpWarning,        
        StatusColors.ColorUnknown,
        StatusColors.ColorUpCritical,
        StatusColors.ColorDown,
        StatusColors.ColorUnreachable,
    };

    protected void Page_Load(object sender, EventArgs e)
    {
        ConfigureChart();
        LoadData();
    }

    private void LoadData()
    {
        PrepareCallsByRegionTable();

        GetData();

        if (callsByRegionTable.Rows.Count != 0)
        {
            NoCalls.Visible = false;

            for (int count = 0; count < callsByRegionTable.Rows.Count; count++)
            {
                series.Points.Add(new NumericDataPoint(int.Parse(callsByRegionTable.Rows[count]["CallsCount"].ToString()), callsByRegionTable.Rows[count]["RegionName"].ToString(), false));
            }

            pieChart.Series.Add(series);
            pieChart.Tooltips.Display = TooltipDisplay.Never;

            totalCalls = String.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_79,  totalCallCount);

            RegionDetailsRepearter.DataSource = callsByRegionTable;
            RegionDetailsRepearter.DataBind();
        }
        else
        {
            NoCalls.Visible = true;

            series.Points.Add(new NumericDataPoint(1, "Up", false)); 
            pieChart.Series.Add(series);
            pieChart.Tooltips.Display = TooltipDisplay.Never;
        }
    }

    protected void RegionDetailsRepeater_DataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var row = (DataRowView)e.Item.DataItem;

            var htmlTableRow = (HtmlTableRow)e.Item.FindControl("trRegionDetailRow");
            var hlRegion = (HyperLink)htmlTableRow.FindControl("hlRegion");
            var hlCalls = (HyperLink)htmlTableRow.FindControl("hlCalls");
            var lblRegion = (Label)htmlTableRow.FindControl("lblRegion");
            var lblCalls = (Label)htmlTableRow.FindControl("lblCalls");

            if ((long)row["RegionID"] != 0)
            {
                lblCalls.Visible = false;
                lblRegion.Visible = false;

                string navigateUrl = this.Page.ResolveUrl(string.Format(voipSearchPath, row["RegionID"], row["RegionID"], GetCallTimeList()));

                hlRegion.NavigateUrl = navigateUrl;
                hlCalls.Text = String.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_79, hlCalls.Text);
                hlCalls.NavigateUrl = navigateUrl;
            }
            else
            {
                hlCalls.Visible = false;
                hlRegion.Visible = false;

                lblCalls.Text = String.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_79, lblCalls.Text);
            }

            var divColor = (HtmlGenericControl)e.Item.FindControl("divColor");
            divColor.Attributes.Add("style", "width: 10px; height: 10px; background-color: " + System.Drawing.ColorTranslator.ToHtml(ChartColors[e.Item.ItemIndex]));
        }
    }

    private void GetData()
    {
        DateTime toDate = DateTime.UtcNow;
        DateTime fromDate = GetFromTime(toDate);

        DataTable table = CallDetailsDAL.GetCallsbyRegion(fromDate, toDate);
        int restCallCount = 0;

        for (int count = 0; count < table.Rows.Count; count++)
        {
            if (count < 5)
            {
                DataRow dRow = callsByRegionTable.NewRow();
                dRow["RegionID"] = table.Rows[count]["RegionID"];
                dRow["RegionName"] = table.Rows[count]["RegionName"];
                dRow["CallsCount"] = table.Rows[count]["CallsCount"];
                callsByRegionTable.Rows.Add(dRow);

                totalCallCount += (int)table.Rows[count]["CallsCount"];
            }
            else
            {
                restCallCount += (int)table.Rows[count]["CallsCount"];
                totalCallCount += (int)table.Rows[count]["CallsCount"];
            }
        }

        if (restCallCount > 0)
        {
            DataRow dRow = callsByRegionTable.NewRow();
            dRow["RegionID"] = "0";
            dRow["RegionName"] = "Rest";
            dRow["CallsCount"] = restCallCount;

            callsByRegionTable.Rows.Add(dRow);
        }

        noCalls = string.Format(noCalls, Resource.SubTitle);
    }

    private CallTimeList GetCallTimeList()
    {
        CallTimeList value = CallTimeList.Last15Minutes;

        if (Resource.Properties.ContainsKey("TimePeriod"))
        {
            value = (CallTimeList)int.Parse(Resource.Properties["TimePeriod"]);
        }

        return value;
    }

    private DateTime GetFromTime(DateTime toDate)
    {
        CallTimeList value = GetCallTimeList();

        switch (value)
        {
            case CallTimeList.Last30Minutes:
                UpdateSubTitle(Resources.VNQMWebContent.VNQMWEBCODE_AK1_26);
                return toDate.AddMinutes(-30);
            case CallTimeList.LastHour:
                UpdateSubTitle(Resources.VNQMWebContent.VNQMWEBCODE_AK1_27);
                return toDate.AddHours(-1);
            case CallTimeList.Last2Hours:
                UpdateSubTitle(Resources.VNQMWebContent.VNQMWEBCODE_AK1_28);
                return toDate.AddHours(-2);
            case CallTimeList.Last12Hours:
                UpdateSubTitle(Resources.VNQMWebContent.VNQMWEBCODE_AK1_29);
                return toDate.AddHours(-12);
            case CallTimeList.LastDay:
                UpdateSubTitle(Resources.VNQMWebContent.VNQMWEBCODE_AK1_30);
                return toDate.AddDays(-1);
            case CallTimeList.Last7Days:
                UpdateSubTitle(Resources.VNQMWebContent.VNQMWEBCODE_AK1_31);
                return toDate.AddDays(-7);
            case CallTimeList.LastMonth:
                UpdateSubTitle(Resources.VNQMWebContent.VNQMWEBCODE_AK1_32);
                return toDate.AddMonths(-1);
            default:
                UpdateSubTitle(Resources.VNQMWebContent.VNQMWEBCODE_AK1_33);
                return toDate.AddMinutes(-15);
        }
    }

    private void UpdateSubTitle(string appendString)
    {
        if (string.IsNullOrEmpty(Resource.SubTitle))
        {
            Resource.SubTitle = appendString;
        }
        else
        {
            Resource.SubTitle = String.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_61, Resource.SubTitle, appendString);
        }
    }

    private void PrepareCallsByRegionTable()
    {
        callsByRegionTable.Columns.Add("RegionID", typeof(long));
        callsByRegionTable.Columns.Add("RegionName", typeof(string));
        callsByRegionTable.Columns.Add("CallsCount", typeof(long));
    }

    private void ConfigureChart()
    {
        pieChart.Transform3D.XRotation = 7;
        pieChart.Transform3D.YRotation = 3;
        pieChart.Transform3D.ZRotation = -55;
        pieChart.Transform3D.Perspective = 50;
        pieChart.Transform3D.Scale = 100;

        pieChart.PieChart3D.RadiusFactor = 129;
        pieChart.PieChart3D.PieThickness = 9;

        pieChart.ColorModel.CustomPalette = ChartColors;
        pieChart.ColorModel.ModelStyle = ColorModels.CustomLinear;
    }

    protected override string DefaultTitle
    {
        get { return Resources.VNQMWebContent.VNQMWEBCODE_AK1_34; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionIPSLAMonitorPHResourceCallsbyRegion"; }
    }

    public override string EditURL
    {
        get
        {
            return string.Format("/Orion/Voip/Resources/Summary/EditCallsByRegion.aspx?ResourceID={0}", this.Resource.ID);
        }
    }
}