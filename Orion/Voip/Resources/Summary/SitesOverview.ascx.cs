using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using ASP;
using Resources;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.VOIP)]
public partial class Orion_Voip_Resources_Summary_SitesOverview : VoipBaseResource
{
    Dictionary<string, GroupOfSites> _groupedSites = new Dictionary<string, GroupOfSites>();
    protected bool _isGroping;
    private string _group;

    protected void GroupSitesRepeater_Init(object sender, EventArgs e)
    {
        _group = Resource.Properties["GroupingBy"];
        
        if (!string.IsNullOrEmpty(_group))
        {
            _isGroping = true;
            _groupedSites = SolarWinds.Orion.IpSla.Web.Site.GetGroupedSiteWithCallPath(_group);

        }
        else
        {
            _isGroping = false;
            _groupedSites.Add(string.Empty, new GroupOfSites(SolarWinds.Orion.IpSla.Web.Site.GetAllSitesWithCallPaths()));
        }

        GroupSitesRepeater.DataSource = _groupedSites;
        GroupSitesRepeater.DataBind();

        GroupSitesRepeater.Visible = _groupedSites.Count > 0;
        NoSites.Visible = ((!_isGroping && _groupedSites[string.Empty].Sites.Count == 0) || _groupedSites.Count == 0);
    }

    protected void GroupSitesRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;

        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
        {
            InternalSiteOverview siteOverview = (InternalSiteOverview)item.FindControl("SiteOverview");
            Repeater rep = (Repeater)siteOverview.FindControl("InternalSiteRepeater");
            rep.DataSource = ((KeyValuePair<string, GroupOfSites>)e.Item.DataItem).Value.Sites;
            rep.DataBind();
        }
    }

    protected string MakeID(object groupName, string tag)
    {
        return $"{tag}_grSO{groupName}_res{this.Resource.ID}";
    }

    #region BaseResourceControl methods

    public override string HelpLinkFragment => "OrionVoIPMonitorPHResourceSitesOverview";

    protected override string DefaultTitle => VNQMWebContent.VNQMWEBCODE_AK1_49;

    public override string EditURL => GetEditUrl(this.Resource);

    #endregion

    private string GetEditUrl(ResourceInfo resource)
    {
        string netobject = Request["NetObject"];
        string url = $"/Orion/Voip/Resources/EditSitesOverview.aspx?ResourceID={resource.ID}";
        if (!string.IsNullOrEmpty(netobject))
            url = $"{url}&NetObject={netobject}";
        return url;
    }
}
