<%@ Control Language="C#" ClassName="TopSitesByMOS" Inherits="SolarWinds.Orion.Web.UI.BaseResourceControl" %>
<%@ Register Src="InternalTopSitesByMetric.ascx" TagName="InternalTopSitesByMetric"
	TagPrefix="uc1" %>

<script runat="server">
	protected override void OnInit(EventArgs e)
	{
		AddStylesheet("/Orion/Voip/Voip.css");
		base.OnInit(e);
	}
    protected override string DefaultTitle { get { return Resources.VNQMWebContent.VNQMWEBCODE_AK1_75; } }

	public override string HelpLinkFragment
	{
		get { return "OrionVoIPMonitorPHResourceWorstSitesMOS"; }
	}

	public override string EditURL
	{
		get { return InternalTopSitesByMetric1.GetEditUrl(this.Resource); }
	}

	protected void InitTopSites(object sender, EventArgs e)
	{
		InternalTopSitesByMetric1.HowMany = HowMany;
	}

	protected int HowMany { get { return GetIntProperty("HowMany", 5); } }
	public override string DisplayTitle { get { return Title.Replace("XX", HowMany.ToString()); } }
</script>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
<uc1:InternalTopSitesByMetric ID="InternalTopSitesByMetric1" runat="server" OnInit="InitTopSites" Metric="MOS_OF_QM" MetricHeader="<%$ Resources: VNQMWebContent, VNQMWEBCODE_VB1_145 %>" MetricData="MOS" HighValuesAreBetter="true" />
	</Content>
</orion:resourceWrapper>
