using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using ASP;
using Resources;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.IpSla.Common.Settings;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.VOIP)]
public partial class Orion_Voip_Resources_Summary_VoipInfrastructure : VoipBaseResource
{
    protected override bool DefaultLoadingMode => true;
    protected string Grouping1 => GetGrouping("Grouping1");

    protected string Grouping2 => GetGrouping("Grouping2");

    protected string Grouping3 => GetGrouping("Grouping3");

    protected string Filter
    {
        get
        {
            var filter = this.Resource.Properties["Filter"];
            if (string.IsNullOrEmpty(filter))
                return string.Empty;
            return filter.Trim().Replace('*', '%').Replace('?', '_');
        }
    }

    private string GetGrouping(string name)
    {
        var thisGroup = this.Resource.Properties[name];
        if (string.IsNullOrEmpty(thisGroup) || thisGroup.Equals("none", StringComparison.InvariantCultureIgnoreCase))
            return string.Empty;
        return thisGroup;
    }

    public override string EditURL
    {
        get
        {
            var url = $"/Orion/Voip/Resources/EditNodeTree.aspx?ResourceID={this.Resource.ID}";
            if (!string.IsNullOrEmpty(Request["NetObject"]))
                url = $"{url}&NetObject={Request["NetObject"]}";
            if (!IpSlaSettings.AreInterfacesSupported)
                url = $"{url}&HideInterfaceFilter=True";

            return url;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        BuildNodeTree();
        base.OnInit(e);
    }

    private void BuildNodeTree()
    {
        var allNodes = GetNodes().Cast<Node>().ToList();

        // If no grouping, just build a simple list
        if (string.IsNullOrEmpty(Grouping1))
        {
            BuildNodeList(this.phTree, allNodes);
        }
        else
        {
            BuildTreeLevel(this.phTree, allNodes, 1);
        }
    }

    protected virtual IEnumerable<NetObject> GetNodes()
    {
        var nodes = VoipNode.GetVoipInfrastructureNodes();
        noNodes.Visible = !nodes.GetEnumerator().MoveNext(); // = 'is empty'
        return nodes;
    }

    // returns the aggregated status of all the children (whose statuses may be based on their children)
    private Status BuildTreeLevel(Control thisBranch, List<Node> nodes, int treeLevel)
    {
        var groupingKey = string.Empty;
        switch (treeLevel)
        {
            case 1:
                groupingKey = this.Grouping1;
                break;
            case 2:
                groupingKey = this.Grouping2;
                break;
            case 3:
                groupingKey = this.Grouping3;
                break;
        }

        if (string.IsNullOrEmpty(groupingKey))
        {
            return BuildNodeList(thisBranch, nodes);
        }
        var groups = new Hashtable(StringComparer.InvariantCultureIgnoreCase);
        foreach (var n in nodes)
        {
            // Notice that the hashtable keys are the *values* of the nodes' grouping properties
            var myKey = n[groupingKey].ToString().Length == 0 ? VNQMWebContent.VNQMWEBCODE_AK1_58 : n[groupingKey].ToString();

            if (!groups.ContainsKey(myKey))
                groups.Add(myKey, new List<Node>());
            var myNodes = (List<Node>)groups[myKey];
            myNodes.Add(n);
        }

        var sortedKeys = groups.Keys.Cast<string>().ToList();
        sortedKeys.Sort(StringComparer.InvariantCultureIgnoreCase);

        var statuses = new List<Status>();

        foreach (var key in sortedKeys)
        {
            if (string.IsNullOrEmpty(key))
                continue;

            var myPanel = new CollapsePanel
            {
                Collapsed = true,
                CssClass = "Tree"
            };
            // a convenience for debugging; we could let the ID be auto-gen'd if we wanted

            thisBranch.Controls.Add(myPanel);

            var icon = (SmallStatusIcon)LoadControl("/Orion/Controls/SmallStatusIcon.ascx");
            icon.StatusValue = BuildTreeLevel(myPanel.CollapseBlock, (List<Node>)groups[key], treeLevel + 1);
            statuses.Add(icon.StatusValue);

            myPanel.PanelTitle.Controls.Add(icon);
            myPanel.PanelTitle.Controls.Add(new LiteralControl(
                $"<span>{Server.HtmlEncode(GetLocalizedProperty("OperationStatus", key))}</span>"));
        }

        var empties = (List<Node>)groups[string.Empty];
        if (empties != null)
        {
            BuildTreeLevel(thisBranch, empties, treeLevel + 1);
            statuses.AddRange(empties.Select(empty => empty.Status.ParentStatus));
        }

        return Status.Summarize(statuses);
    }

    private Status BuildNodeList(Control c, List<Node> nodes)
    {
        nodes.Sort((x, y) => string.Compare(x.Name, y.Name, StringComparison.OrdinalIgnoreCase));

        var statuses = new List<Status>();

        foreach (var n in nodes)
        {
            var myLink =
                    (Orion_NetPerfMon_Controls_NodeLink)LoadControl("/Orion/NetPerfMon/Controls/NodeLink.ascx");
            myLink.NodeID = n.NetObjectID;

            var status = (SmallNodeStatus)LoadControl("/Orion/Controls/SmallNodeStatus.ascx");
            status.StatusValue = n.Status;

            myLink.Content.Controls.Add(status);
            myLink.Content.Controls.Add(new LiteralControl(n.Name));

            c.Controls.Add(myLink);
            c.Controls.Add(new LiteralControl("<br />"));
            statuses.Add(n.Status.ParentStatus);
        }

        return Status.Summarize(statuses);
    }

    protected override string DefaultTitle => VNQMWebContent.VNQMWEBCODE_AK1_57;

    public override string HelpLinkFragment => "OrionVoIPMonitorPHResourceInfrastructure";

    protected string GetLocalizedProperty(string prefix, string property)
    {
        var manager = ResourceManagerRegistrar.Instance;
        var key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}
