<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VoipCollectorServices.ascx.cs" Inherits="VoipCollectorServices" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:UpdatePanel runat="server" ID="updatePanel">
            <ContentTemplate>
                <table border="0" cellpadding="2" cellspacing="0" width="500">
                    <thead>
                        <tr>
                            <td colspan="2" class="ReportHeader">
                                <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_186 %>
                            </td>
                            <td class="ReportHeader">
                                <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_187 %>
                            </td>
                            <td class="ReportHeader">
                                <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_188 %>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater runat="server" ID="collectorRepeater" EnableViewState="false" OnItemDataBound="collectorRepeater_ItemDataBound">
                            <ItemTemplate>
                                <tr>
                                    <td class="Property" width="20">
                                        <%# FormatStatusTime(Eval("StatusTime")) %>
                                    </td>
                                    <td class="Property">
                                        <%# Eval("ServerName") %>&nbsp;
                                    </td>
                                    <td class="Property">
                                        <%# GetLocalizedProperty("ServerType", Eval("ServerType").ToString())%>&nbsp;
                                    </td>
                                    <td class="Property">
                                        <%# FormatKeepAlive(Eval("KeepAlive")) %>
                                    </td>
                                    <td>
                                        <%if (Profile.AllowAdmin){%>
                                        <asp:ImageButton ID="btnDeleteCollector" runat="server" ImageUrl="~/Orion/Voip/images/Button.Close2.gif"
                                                         ToolTip="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_189 %>" />
                                        <%}%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </Content>
</orion:resourceWrapper>
