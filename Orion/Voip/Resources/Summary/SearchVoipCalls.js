﻿var queryString = '';

function origintypechanged(ddlCallOriginationType, divCallOrigination, ddlGateway, ddlRegion) {
    var divCallOriginationIPAddressorPhone = document.getElementById(divCallOrigination);
    var ddlOriginGateway = document.getElementById(ddlGateway);
    var ddlOriginRegion = document.getElementById(ddlRegion);
    var originType = ddlCallOriginationType.options[ddlCallOriginationType.selectedIndex].value;

    if (originType == 'Select' || originType == 'OrgPhoneNumber' || originType == 'OrgIPAddress') {
        divCallOriginationIPAddressorPhone.style.display = "block";
        ddlOriginGateway.style.display = "none";
        ddlOriginRegion.style.display = "none";
    } else if (originType == 'OrgLocation') {
        divCallOriginationIPAddressorPhone.style.display = "none";
        ddlOriginGateway.style.display = "none";
        ddlOriginRegion.style.display = "block";
    } else {
        divCallOriginationIPAddressorPhone.style.display = "none";
        ddlOriginGateway.style.display = "block";
        ddlOriginRegion.style.display = "none";
    }
    return true;
}

function destinationtypechanged(ddlCallDestinationType, divCallDestination, ddlGateway, ddlRegion) {
    var destinationType = ddlCallDestinationType.options[ddlCallDestinationType.selectedIndex].value;
    var divCallDestinationIPAddressorPhone = document.getElementById(divCallDestination);
    var ddlDestinationGateway = document.getElementById(ddlGateway);
    var ddlDestinationRegion = document.getElementById(ddlRegion);

    if (destinationType == 'Select' || destinationType == 'DestPhoneNumber' || destinationType == 'DestIPAddress') {
        divCallDestinationIPAddressorPhone.style.display = "block";
        ddlDestinationGateway.style.display = "none";
        ddlDestinationRegion.style.display = "none";
    } else if (destinationType == 'DestLocation') {
        divCallDestinationIPAddressorPhone.style.display = "none";
        ddlDestinationGateway.style.display = "none";
        ddlDestinationRegion.style.display = "block";
    } else {
        divCallDestinationIPAddressorPhone.style.display = "none";
        ddlDestinationGateway.style.display = "block";
        ddlDestinationRegion.style.display = "none";
    }

    return true;
}

function OnSubmit(ddlCallOrigination, ddlCallDestination, txtOrigin, txtDestination, ddlOrgRegion, ddlOrgGateway, ddlDestRegion, ddlDestGateway, ddlTime, chkbFailed, chkbMOS, ddMos, chkbJitter, ddJitter, chkbLatency, ddLatency, chkbPacketLoss, ddPacketLoss, url) 
{
    
    var ddlCallOriginationType = document.getElementById(ddlCallOrigination);
    var ddlCallDestinationType = document.getElementById(ddlCallDestination);
    var txtOriginIPAddressorPhone = document.getElementById(txtOrigin);
    var txtDestinationIPAddressorPhone = document.getElementById(txtDestination);
    var ddlOriginRegion = document.getElementById(ddlOrgRegion);
    var ddlOriginGateway = document.getElementById(ddlOrgGateway);
    var ddlDestinationRegion = document.getElementById(ddlDestRegion);
    var ddlDestinationGateway = document.getElementById(ddlDestGateway);
    var ddlCallTime = document.getElementById(ddlTime);
    var cbFailed = document.getElementById(chkbFailed);
    var cbMOS = document.getElementById(chkbMOS);
    var ddlMOS = document.getElementById(ddMos);
    var cbJitter = document.getElementById(chkbJitter);
    var ddlJitter = document.getElementById(ddJitter);
    var cbLatency = document.getElementById(chkbLatency);
    var ddlLatency = document.getElementById(ddLatency);
    var cbPacketLoss = document.getElementById(chkbPacketLoss);
    var ddlPacketLoss = document.getElementById(ddPacketLoss);

    var callOriginationCriteria = '';
    var callDestCriteria = '';
    var callTime = ddlCallTime.options[ddlCallTime.selectedIndex].value;
    var callOriginationType = ddlCallOriginationType.options[ddlCallOriginationType.selectedIndex].value;
    var callDestinationType = ddlCallDestinationType.options[ddlCallDestinationType.selectedIndex].value;
    var originRegion = ddlOriginRegion.options[ddlOriginRegion.selectedIndex].value;
    var originGateway = ddlOriginGateway.options[ddlOriginGateway.selectedIndex].value;
    var destRegion = ddlDestinationRegion.options[ddlDestinationRegion.selectedIndex].value;
    var destGateway = ddlDestinationGateway.options[ddlDestinationGateway.selectedIndex].value;
    var originIPAddressorPhoneValue = txtOriginIPAddressorPhone.value;
    var destIPAddressorPhoneValue = txtDestinationIPAddressorPhone.value;

    queryString = '?CallTime=' + callTime;
    if (callOriginationType != 'Select') {
        callOriginationCriteria = callOriginationType + '=';
        if ((callOriginationType == 'OrgPhoneNumber' || callOriginationType == 'OrgIPAddress') && (originIPAddressorPhoneValue != 'IP address or phone #')) {
            callOriginationCriteria += encodeURIComponent(originIPAddressorPhoneValue);
        }
        else if (callOriginationType == 'OrgLocation') {
            callOriginationCriteria += encodeURIComponent(originRegion);
        }else if(callOriginationType=='OrgGateway') {
            callOriginationCriteria +=  encodeURIComponent(originGateway);
        }
        queryString += '&' + callOriginationCriteria;
    }
    
    if (callDestinationType != 'Select') {
        callDestCriteria = callDestinationType + '=';
        if ((callDestinationType == 'DestPhoneNumber' || callDestinationType == 'DestIPAddress') && (destIPAddressorPhoneValue != 'IP address or phone #')) {
            callDestCriteria += encodeURIComponent(destIPAddressorPhoneValue);
        } else if (callDestinationType == 'DestLocation') {
            callDestCriteria += encodeURIComponent(destRegion);
        } else if (callDestinationType == 'DestGateway') {
            callDestCriteria +=  encodeURIComponent(destGateway);
        }
        queryString += '&' + callDestCriteria;
    }
    if (cbFailed.checked) {
        queryString += '&' + 'CallFailed=true';
    }
    if (cbMOS.checked) {
        queryString += '&' + 'MOS=' + ddlMOS.options[ddlMOS.selectedIndex].value;   
    }
    if (cbJitter.checked) {
        queryString += '&' + 'Jitter=' + ddlJitter.options[ddlJitter.selectedIndex].value;
    }
    if (cbLatency.checked) {
        queryString += '&' + 'Latency=' + ddlLatency.options[ddlLatency.selectedIndex].value;
    }
    if (cbPacketLoss.checked) {
        queryString += '&' + 'PacketLoss=' + ddlPacketLoss.options[ddlPacketLoss.selectedIndex].value;
    }

    SolarWinds.Orion.IpSla.Web.WebServices.VoipCallSearchService.Validate(callOriginationType.replace("Org", ""),
            originIPAddressorPhoneValue,originRegion,originGateway,
            callDestinationType.replace("Dest", ""),
            destIPAddressorPhoneValue,destRegion,destGateway,
            "",
            "",
            "",
            "",
            "",
            false,
            "",
            "",
            "",
            false,
            "",
            "",
            "",
            false,
            "",
            "",
            "",
            false,
            "",
            "",
            "", Function.createDelegate(this, this.oncomplete(url)),
            Function.createDelegate(this, this.onfailure));

    return false;
}

function oncomplete(url) {
    return function oncomplete(result) {
        if (result != '') {
            Ext.MessageBox.show({
                title: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_72;E=js}',
                msg: result,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.ERROR
            });
            return;
        }
        url += queryString;
        $(location).attr('href', url);

    };
}

function onfailure(error) {
    Ext.MessageBox.show({
        title: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_72;E=js}',
        msg: error.Message,
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.INFO
    });
}