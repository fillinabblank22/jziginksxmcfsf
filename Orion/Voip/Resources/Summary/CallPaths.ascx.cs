using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_Resources_Summary_CallPaths : VoipBaseResource
{
	private List<CallPath> callpaths;
	private bool useAjax;

	protected void Page_Load(object sender, EventArgs e)
	{
		if (null == ViewState["callPathsPage"])
			ViewState["callPathsPage"] = 1;
		GetCallPaths();
		if (!this.IsPostBack)
			BindCallPathsTable();

		ButtonBack.Text = string.Format(VNQMWebContent.VNQMWEBCODE_AK1_21, CallPathsDisplayed);
		ButtonNext.Text = string.Format(VNQMWebContent.VNQMWEBCODE_AK1_22, CallPathsDisplayed);
	}

    public override string DetachURL => string.Format("/Orion/DetachResource.aspx?ResourceID={0}&NetObject={1}", this.Resource.ID, !string.IsNullOrEmpty(Request.Params["NetObject"]) ? Request.Params["NetObject"] : string.Empty);

    protected void ButtonBack_Click(object sender, EventArgs e)
	{
		int pageNum = (int)ViewState["callPathsPage"];
		if (pageNum == 2)
		{
			ButtonBack.Visible = false;
		}

		ViewState["callPathsPage"] = --pageNum;
		ButtonNext.Visible = true;
		BindCallPathsTable();
	}

	protected void ButtonNext_Click(object sender, EventArgs e)
	{
		int pageNum = (int)ViewState["callPathsPage"];
		if (callpaths.Count <= ((pageNum + 1) * Config.CallPathsDisplayed))
		{
			ButtonNext.Visible = false;
		}
		ViewState["callPathsPage"] = ++pageNum;
		ButtonBack.Visible = true;
		BindCallPathsTable();
	}

	private void UseAjax(bool use)
	{
		PagesButtons.Visible = use;
		if (use)
		{
			int pageNum = int.Parse(ViewState["callPathsPage"].ToString());
			LabelPage.Text = string.Format(VNQMWebContent.VNQMWEBCODE_AK1_23, pageNum);
			callpaths = new List<CallPath>(CallPath.GetCallPathsForPage(pageNum, Config.CallPathsDisplayed, callpaths));
		}
	}

	private void GetCallPaths()
	{
		ISiteProvider siteProvider = GetInterfaceInstance<ISiteProvider>();
		if (siteProvider == null)
		{
            IRegionProvider regionProvider = GetInterfaceInstance<IRegionProvider>();
            if(regionProvider== null)
            {
                callpaths = new List<CallPath>(CallPath.GetAllCallPaths(null));
            }
            else
            {
                callpaths = new List<CallPath>(CallPath.GetAllCallPaths(regionProvider.Region.VoipRegion.RegionID));
            }
		}
		else
		{
			callpaths = new List<CallPath>(CallPath.GetCallPathsForSite(siteProvider.Site,null));
		}
		useAjax = callpaths.Count > Config.CallPathThreshold;
	}

	private void BindCallPathsTable()
	{	
		AddStylesheet("/Orion/Voip/Voip.css");
		UseAjax(useAjax);		
		CallPathGrid.Visible = callpaths.Count > 0;
		NoCallPathsMsg.Visible = callpaths.Count == 0;
		callpaths.Sort();
		CallPathGrid.DataSource = callpaths;
		CallPathGrid.DataBind();
	}

	protected override string DefaultTitle => VNQMWebContent.VNQMWEBCODE_AK1_24;

    public override string HelpLinkFragment => "OrionVoIPMonitorPHResourceCallPaths";

    protected int CallPathsDisplayed => Config.CallPathsDisplayed;

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            if (!string.IsNullOrEmpty(Request.Params["NetObject"]))
            {
                switch (getNetObjectPrefix(Request.Params["NetObject"]))
                {
                    case "VR": //Region View
                        return new Type[] {typeof (IRegionProvider)};
                    case "VS": //Site View
                        return new Type[] {typeof (ISiteProvider)};
                    default:
                        return new Type[] { };

                }
            }
            return new Type[] {};
        }
    }

    private string getNetObjectPrefix(string netObjectId)
    {
        string[] netObjectParts = netObjectId.Split(':');
			return netObjectParts[0];
    }
}
