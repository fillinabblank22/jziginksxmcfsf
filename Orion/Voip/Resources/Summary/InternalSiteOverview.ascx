<%@ Import namespace="SolarWinds.Orion.IpSla.Data"%>
<%@ Import namespace="SolarWinds.Orion.Web.UI" %>

<%@ Control Language="C#" ClassName="InternalSiteOverview" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register TagPrefix="orion" TagName="SmallStatusIcon" Src="~/Orion/Controls/SmallStatusIcon.ascx" %>
<%@ Register TagPrefix="voip" TagName="Metric" Src="~/Orion/Voip/Controls/Metric.ascx" %>
<%@ Register TagPrefix="voip" TagName="MOSBar" Src="~/Orion/Voip/Controls/MOSBar.ascx" %>
<%@ Register TagPrefix="voip" TagName="SitesOverviewBody" Src="~/Orion/Voip/Resources/SitesOverviewBody.ascx" %>

<script runat="server">
    
    protected string GetSiteText(int status, int siteID, int badCallPathCount)
    {
        OperationStatus opStatus = (OperationStatus) status;

        if (opStatus.IsUnReachable())
        {
            return Resources.VNQMWebContent.VNQMWEBCODE_AK1_37;
        }
		
        return string.Format(Resources.VNQMWebContent.VNQMWEBCODE_AK1_38, badCallPathCount);
    }
    
    protected void ExpandBtn_Click(object sender, EventArgs e)
    {
        ImageButton btn = (ImageButton)sender;
        btn.Visible = false;
        btn.NamingContainer.FindControl("Collapse").Visible = true;

        int siteID = int.Parse(btn.CommandArgument);
        SolarWinds.Orion.IpSla.Web.Site site = new SolarWinds.Orion.IpSla.Web.Site(siteID);

        Control tb1 = btn.NamingContainer.FindControl("TB1");
        tb1.Visible = true;

        LinkButton lb = (LinkButton)tb1.FindControl("L1");
        if (site.CallPaths.Count > Config.CallPathsForSiteThreshold)
            lb.PostBackUrl = BaseResourceControl.GetViewLink(site.NetObjectID);

        if (site.BadCallPaths.Count != 0)
        {
            SitesOverviewBody ctrlBadSitesBody = (SitesOverviewBody)tb1.NamingContainer.FindControl("BadSitesBody");
            Repeater rep1 = (Repeater)ctrlBadSitesBody.FindControl("CallPathsRepeater");
            rep1.DataSource = site.BadCallPaths;
            rep1.DataBind();
            ctrlBadSitesBody.Visible = true;
        }
    }

    protected void ShowCallPaths(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        btn.NamingContainer.FindControl("TB1").Visible = false;
        Control tb2 = btn.NamingContainer.FindControl("TB2");
        tb2.Visible = true;

        btn.NamingContainer.FindControl("Collapse").Visible = true;
        btn.NamingContainer.FindControl("Expand").Visible = false;

        int siteID = int.Parse(btn.CommandArgument);
        SolarWinds.Orion.IpSla.Web.Site site = new SolarWinds.Orion.IpSla.Web.Site(siteID);

        SitesOverviewBody ctrlAllSitesBody = (SitesOverviewBody)tb2.NamingContainer.FindControl("AllSitesBody");
        Repeater rep2 = (Repeater)ctrlAllSitesBody.FindControl("CallPathsRepeater");
        rep2.DataSource = site.CallPaths;
        rep2.DataBind();

    }

    protected void CollapseBtn_Click(object sender, EventArgs e)
    {
        ImageButton btn = (ImageButton)sender;
        btn.Visible = false;
        btn.NamingContainer.FindControl("Expand").Visible = true;

        btn.NamingContainer.FindControl("TB1").Visible = false;
        btn.NamingContainer.FindControl("TB2").Visible = false;
    }
    
    protected void GroupSiteOverviewUP_Init(object sender, EventArgs e)
    {
        
    }

</script>

<asp:Repeater ID="InternalSiteRepeater" runat="server">
    <ItemTemplate>
        <asp:UpdatePanel runat="server" ID="GroupSiteOverviewUP"  UpdateMode="Conditional">
            <ContentTemplate>
            <table  cellspacing="0" cellpadding="3px">
                <tr>
                    <td style="width:2%;padding-left:10px" >
                        <asp:ImageButton ID="Expand"  runat="server" CommandArgument='<%#Eval("VoipSite.ID")%>' ImageUrl="/Orion/images/Button.Expand.gif" OnClick ="ExpandBtn_Click" Visible="true" />
	                    <asp:ImageButton ID="Collapse" runat="server" CommandArgument='<%#Eval("VoipSite.ID")%>' ImageUrl="/Orion/images/Button.Collapse.gif" OnClick ="CollapseBtn_Click"  Visible="false" />
	                </td>
	                <td style="width:30%">
	                  <a href="<%#BaseResourceControl.GetViewLink((string)Eval("NetObjectID"))%>">
		                <orion:SmallStatusIcon ID="SmallNodeStatus1" runat="server" StatusValue='<%#Eval("CallPathsStatus") %>' />
	    	                <img src="/Orion/Voip/images/Icon-Site-Small.gif" />
			                <%#Eval("Name")%>
	                  </a>
	                </td>
	                <td style="width:15%" runat="server" id="CallPathsCredsRW"><%#GetSiteText((int)Eval("VoipSite.NodeStatus"), (int)Eval("VoipSite.ID"), (int)Eval("BadCallPaths.Count")) %></td>
	                <td colspan="2" ><voip:Metric ID="Metric1" runat="server" Value='<%#Eval("MOS")%>' /></td>
	                <voip:MOSBar ID="MOSBar1" runat="server" Value='<%#Eval("MOS")%>' />
	                <td style="width:10%" ><voip:Metric ID="Metric2" runat="server" Value='<%#Eval("Jitter")%>' /></td>
	                <td style="width:10%"><voip:Metric ID="Metric3" runat="server" Value='<%#Eval("Latency")%>' /></td>
	                <td style="width:10%"><voip:Metric ID="Metric4" runat="server" Value='<%#Eval("PacketLoss")%>' /></td>
                	
	                <tbody id="TB1" runat="server" visible="false">
	   	                <voip:SitesOverviewBody runat="server" ID="BadSitesBody" Visible="false" />
			                <tr class="SitesOverview_CallPath">
	    		                <td > </td>
				                <td colspan="7" id="ShowAllCallPathLink" runat="server">
		    		                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			    	                <asp:LinkButton ID="L1" runat="server" CommandArgument='<%#Eval("VoipSite.ID")%>' OnClick="ShowCallPaths" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_171 %>" />
				                </td>
			                </tr>
	                </tbody>
	                
	                <tbody id="TB2" runat="server" visible="false">
	                    <voip:SitesOverviewBody runat="server" ID="AllSitesBody"/>
	                </tbody>
                </tr>
            </table>
            </ContentTemplate>
            
            <Triggers >
            <asp:AsyncPostBackTrigger ControlID="Expand" />
            <asp:AsyncPostBackTrigger ControlID="Collapse" />
            </Triggers>
        </asp:UpdatePanel>
        
        <asp:UpdateProgress ID="NodeUpdateProgress" runat="server" AssociatedUpdatePanelID="GroupSiteOverviewUP">
             <ProgressTemplate>
                <div style ="font-family: Arial, Verdana, Helvetica, sans-serif; font-size: 8pt;text-align:center;">
                     <img style="margin-right:5px;" src="/Orion/Voip/images/loading_gen_16x16.gif" alt="<%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_44 %>" /><%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_44 %>
                </div>
             </ProgressTemplate>
        </asp:UpdateProgress> 
    </ItemTemplate>
</asp:Repeater>
