<%@ Control Language="C#" ClassName="TopCallPathsByMOS" Inherits="SolarWinds.Orion.Web.UI.BaseResourceControl" %>
<%@ Register Src="InternalTopCallPathsByMetric.ascx" TagName="InternalTopCallPathsByMetric"
	TagPrefix="uc1" %>

<script runat="server">
	protected override void OnInit(EventArgs e)
	{
		AddStylesheet("/Orion/Voip/Voip.css");
		base.OnInit(e);
	}
	protected override string DefaultTitle { get { return Resources.VNQMWebContent.VNQMWEBCODE_AK1_70; } }

	public override string HelpLinkFragment
	{
		get { return "OrionVoIPMonitorPHResourceWorstCallPathsMOS"; }
	}

	public override string EditURL
	{
		get { return InternalTopCallPathsByMetric1.GetEditUrl(this.Resource); }
	}

	protected void InitTopCallPaths(object sender, EventArgs e)
	{
		InternalTopCallPathsByMetric1.HowMany = HowMany;
	}

	protected int HowMany { get { return GetIntProperty("HowMany", 5); } }
	public override string DisplayTitle { get { return Title.Replace("XX", HowMany.ToString()); } }

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl; 

</script>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
<uc1:InternalTopCallPathsByMetric ID="InternalTopCallPathsByMetric1" runat="server" Metric="MOS_OF_QM" MetricHeader="<%$ Resources: VNQMWebContent, VNQMWEBCODE_VB1_145 %>" MetricData="MOS" HighValuesAreBetter="true" OnInit="InitTopCallPaths"/>
	</Content>
</orion:resourceWrapper>

