using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

/// <summary>
/// This web user control shows status of Voip Collector Service
/// </summary>
[ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.VOIP)]
public partial class VoipCollectorServices : VoipBaseResource
{

    private static readonly Log _log = new Log();
    private string _title = VNQMWebContent.VNQMWEBCODE_AK1_55;

    protected override string DefaultTitle => _title;

    public override string HelpLinkFragment => "OrionIPSLAMonitorPHIPSLAManagerServices";

    protected void Page_Load(object sender, EventArgs e)
    {
        PopulateList();
    }

    protected void collectorRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        switch (e.Item.ItemType)
        {
            case ListItemType.Item:
            case ListItemType.AlternatingItem:
                var btnDeleteCollector = (ImageButton)e.Item.FindControl("btnDeleteCollector");
                Debug.Assert(btnDeleteCollector != null);

                btnDeleteCollector.OnClientClick = String.Format("return confirm('{0}');", VNQMWebContent.VNQMWEBCODE_AK1_78);

                var serverType = (ServerType)DataBinder.Eval(e.Item.DataItem, "ServerType");
                if (serverType == ServerType.Unknown)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    Debug.Assert(scriptManager != null);

                    scriptManager.RegisterAsyncPostBackControl(btnDeleteCollector);

                    var serverName = DataBinder.Eval(e.Item.DataItem, "ServerName", string.Empty);
                    if (!string.IsNullOrEmpty(serverName))
                    {
                        btnDeleteCollector.Click += new DeleteCollectorHandler(this, serverName).Handler;
                    }
                }
                else
                {
                    btnDeleteCollector.Visible = false;
                }
                break;
        }
    }

    protected string FormatKeepAlive(object keepAlive)
    {
        var keepAliveDate = keepAlive as DateTime?;
        return keepAliveDate.HasValue
            ? Convert.ToDateTime(keepAliveDate.Value).ToString()
            : VNQMWebContent.VNQMWEBCODE_AK1_56;
    }

    protected string FormatStatusTime(object statusTime)
    {
        var statusTimeMinutes = statusTime as int?;
        return statusTimeMinutes.HasValue && statusTimeMinutes.Value < 15
            ? "<img name='up' src='/Orion/images/small-up.gif'> "
            : " <img name='down' src='/Orion/images/small-down.gif'>";
    }

    private void PopulateList()
    {
        try
        {
            var servers = VoipCollectorService.GetIpSlaServiceStatus();

            this.collectorRepeater.Controls.Clear();
            this.collectorRepeater.DataSource = servers.Rows.Cast<DataRow>().Select(row =>
            {
                var serverType = row["ServerType"] as string;
                return new
                {
                    ServerName = row["ServerName"] as string ?? string.Empty,
                    ServerType = serverType == null ? ServerType.Unknown : StringComparer.InvariantCultureIgnoreCase.Compare(serverType, "Primary") == 0 ? ServerType.Main : ServerType.Additional,
                    KeepAlive = row["KeepAlive"] as DateTime?,
                    StatusTime = row["StatusTime"] as int?,
                };
            });
            this.collectorRepeater.DataBind();
        }
        catch (Exception ex)
        {
            _log.DebugFormat("Caught exception when retrieving IPSLAM servers list:\n\t{0}", ex);
            this.collectorRepeater.DataBind();
        }
    }

    private class DeleteCollectorHandler
    {
        private readonly VoipCollectorServices owner;
        private readonly string serverName;

        public DeleteCollectorHandler(VoipCollectorServices owner, string serverName)
        {
            Debug.Assert(owner != null);
            Debug.Assert(!string.IsNullOrEmpty(serverName));
            this.owner = owner;
            this.serverName = serverName;
        }

        public void Handler(object sender, ImageClickEventArgs e)
        {
            VoipCollectorService.DeleteCollector(this.serverName);
            this.owner.PopulateList();
        }
    }

    private enum ServerType
    {
        Unknown,
        Main,
        Additional,
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}
