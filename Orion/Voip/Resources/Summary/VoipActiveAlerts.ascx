<%@ Control Language="C#" ClassName="VoipActiveAlerts" Inherits="SolarWinds.Orion.Web.UI.BaseResourceControl" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web" %>

<script runat="server">
    protected override string DefaultTitle { get { return Resources.VNQMWebContent.VNQMWEBCODE_AK1_54; } }
    private Dictionary<string, string> netObjectTypes = SolarWinds.Orion.Core.Common.NetObjectTypeMgr.GetNetObjectData();
    
	protected void AlertTable_Load(object sender, EventArgs e)
	{
	    AlertTable.DataSource = VoipAlert.GetAllVoIPActiveAlerts();
		AlertTable.DataBind();
	}
    
    public string GetViewLink(string triggeringObject, string triggeringObjectDetailsUrl)
    {
        if (string.IsNullOrEmpty(triggeringObjectDetailsUrl))
        {
            return triggeringObject;
        }
        return string.Format("<a href=\"{0}\">{1}</a>", triggeringObjectDetailsUrl, triggeringObject);
    }

	public override string HelpLinkFragment
	{
        get { return "OrionVoIPMonitorPHResourceActiveAlerts"; }
	}
	
	public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl;
</script>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
		<asp:Repeater ID="AlertTable" runat="server" OnLoad="AlertTable_Load">
			<HeaderTemplate>
				<table  class="NeedsZebraStripes" border="0" cellPadding="2" cellSpacing="0" width="100%">
					<thead>
						<tr>
						    <td colspan="2"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_183 %></td>
							<td><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_184 %></td>
							<td ><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_185 %></td>
						</tr>
					</thead>
			</HeaderTemplate>

			<ItemTemplate>
				<tr>
					<td><img src="/NetPerfMon/images/Event-19.gif" /></td>
					<td><%#Eval("AlertTime") %></a></td>
					<td><%#GetViewLink((string)Eval("ObjectName"), (string)Eval("ObjectID"))%></td>
					<td><%#Eval("AlertName") %></td>
				</tr>
			</ItemTemplate>

			<FooterTemplate>
				</table>
			</FooterTemplate>
		</asp:Repeater>
	</Content>
</orion:resourceWrapper>
