<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InternalTopSitesByMetric.ascx.cs" Inherits="Orion_Voip_Resources_Summary_InternalTopSitesByMetric" %>
<%@ Register TagPrefix="orion" TagName="SmallStatusIcon" Src="~/Orion/Controls/SmallStatusIcon.ascx" %>

<asp:Repeater runat="server" ID="SiteGrid">
	<HeaderTemplate>
		<table class="NeedsZebraStripes DataGrid">
			<thead>
				<tr>
					<td><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_172 %></td>
					<td style="text-align:right"><%=MetricHeader %></td>
				</tr>
			</thead>
	</HeaderTemplate>
	<ItemTemplate>
		<tr>
			<td>
				<a href="<%#BaseResourceControl.GetViewLink((string)Eval("NetObjectID"))%>">
					<orion:SmallStatusIcon ID="SmallNodeStatus1" runat="server" StatusValue='<%#Eval("CallPathsStatus") %>' />
					<%#Eval("Name")%>
				</a>
			</td>
			<td style="text-align:right" class="<%#Eval(MetricData, "{0:class}")%>"><%#Eval(MetricData)%></td>
		</tr>
	</ItemTemplate>
	<FooterTemplate>
		</table>
	</FooterTemplate>
</asp:Repeater>