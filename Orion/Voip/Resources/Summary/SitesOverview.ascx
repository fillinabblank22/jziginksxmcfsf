<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SitesOverview.ascx.cs" Inherits="Orion_Voip_Resources_Summary_SitesOverview" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web" %>

<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register TagPrefix="orion" TagName="SmallStatusIcon" Src="~/Orion/Controls/SmallStatusIcon.ascx" %>
<%@ Register TagPrefix="voip" TagName="Metric" Src="~/Orion/Voip/Controls/Metric.ascx" %>
<%@ Register TagPrefix="voip" TagName="MOSBar" Src="~/Orion/Voip/Controls/MOSBar.ascx" %>
<%@ Register TagPrefix="voip" TagName="SitesOverviewBody" Src="~/Orion/Voip/Resources/SitesOverviewBody.ascx" %>
<%@ Register TagPrefix="voip" TagName="InternalSiteOverview" Src="~/Orion/Voip/Resources/Summary/InternalSiteOverview.ascx"%>

<script type="text/javascript">
function show(id)
{
	var item = document.getElementById(id);
	item.style.display = "";
}

function hide(id)
{
	var item;
	if (id.style)
		item = id;
	else
		item = document.getElementById(id);
	item.style.display = "none";
}
</script>

<orion:resourceWrapper runat="server" ID="Wrapper" >
<Content>
    <asp:Repeater runat="server" ID="GroupSitesRepeater" OnInit="GroupSitesRepeater_Init" OnItemDataBound="GroupSitesRepeater_ItemDataBound" >
        <HeaderTemplate>
            <table   class="NeedsZebraStripes" cellspacing="0" cellpadding="3px">
					    <thead>
						    <tr>
						        <td style="width:2%"></td>
							    <td style="width:47%"></td>
							    <td style="width:15%"><%= Resources.VNQMWebContent.VNQMWEBCODE_VB1_145 %></td>
							    <td >&nbsp;</td>
							    <td style="width:10%"><%= Resources.VNQMWebContent.VNQMWEBCODE_VB1_146 %></td>
							    <td style="width:10%"><%= Resources.VNQMWebContent.VNQMWEBCODE_VB1_147 %></td>
							    <td style="width:10%"><%= Resources.VNQMWebContent.VNQMWEBCODE_VB1_148 %></td>
						    </tr>
					    </thead>
		    </table>
        </HeaderTemplate>
        <ItemTemplate>
             <table  cellspacing="0" cellpadding="3px">
                <tr style="<%# !_isGroping?"display:none":"" %>">
                   <td style=" width:2%">
                        <img id="<%#MakeID(Eval("Key"),"open") %>" class="showtoggle" src="<%=ResolveUrl("~/Orion/images/Button.Expand.gif")%>" 
				            onclick="show('<%#MakeID(Eval("Key"),"groupedSites")%>');show('<%#MakeID(Eval("Key"),"close") %>');hide(this);"   />
				       <img id="<%#MakeID(Eval("Key"), "close")%>" class="hidetoggle" src="<%=ResolveUrl("~/Orion/images/Button.Collapse.gif") %>"
						    onclick="hide('<%#MakeID(Eval("Key"),"groupedSites")%>');show('<%#MakeID(Eval("Key"), "open")%>');hide(this);" style="display:none;"/>
                   </td>
                   <td style="width:30%" >
                   <orion:SmallStatusIcon ID="GroupStatus" runat="server" StatusValue='<%#((GroupOfSites)Eval("Value")).AvgStatus %>' />
                    <%# Eval("Key")%>
                   </td>
                   <td style="width:15%; padding-left:10px">&nbsp;</td>
                   <td colspan="2"><voip:Metric ID="Metric1" runat="server" Value='<%#((GroupOfSites)Eval("Value")).AvgMOS%>' /></td>
                   <voip:MOSBar ID="MOSBar1" runat="server" Value='<%#((GroupOfSites)Eval("Value")).AvgMOS%>' />
                   <td style="width:10%"  ><voip:Metric ID="Metric2" runat="server" Value='<%#((GroupOfSites)Eval("Value")).AvgJitter%>' /></td>   
                   <td style="width:10%" ><voip:Metric ID="Metric3" runat="server" Value='<%#((GroupOfSites)Eval("Value")).AvgLatency%>' /></td>
                   <td style="width:10%" ><voip:Metric ID="Metric4" runat="server" Value='<%#((GroupOfSites)Eval("Value")).AvgPacketLoss%>' /></td>
                </tr>
            </table>
            
            <div id="<%#MakeID(Eval("Key"),"groupedSites") %>" style= "<%# _isGroping?"display:none":"" %>">
                    <voip:InternalSiteOverview ID="SiteOverview" runat="server"/>
            </div>     
        </ItemTemplate>
    </asp:Repeater>
    <asp:PlaceHolder  runat="server" ID="NoSites" Visible="false">
			<%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_AK1_175, "<a href=\"/Orion/Voip/Admin/ManageNodes/AddNodes.aspx\" class=\"sw-link\">", "</a>")%>
	</asp:PlaceHolder>
</Content>
</orion:resourceWrapper>