<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CallPaths.ascx.cs" Inherits="Orion_Voip_Resources_Summary_CallPaths" %>
<%@ Register TagPrefix="voip" TagName="StatusIcon" Src="~/Orion/Voip/Controls/OperationStatusImage.ascx" %>
<%@ Register TagPrefix="voip" TagName="Metric" Src="~/Orion/Voip/Controls/Metric.ascx" %>
<%@ Register TagPrefix="voip" TagName="MOSBar" Src="~/Orion/Voip/Controls/MOSBar.ascx" %>
<%@ Register TagPrefix="voip" TagName="NoCallPaths" Src="~/Orion/Voip/Resources/NoCallPaths.ascx" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>


<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
	    <asp:UpdatePanel runat="server" ID="CallPathsPanel" UpdateMode="Conditional">
	        <ContentTemplate>
	            <asp:UpdateProgress ID="NodeUpdateProgress" runat="server" AssociatedUpdatePanelID="CallPathsPanel">
                    <ProgressTemplate>
                    <div style ="text-align:center;">
                         <img style="margin-right:5px;" src="/Orion/Voip/images/loading_gen_16x16.gif" alt="<%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_44 %>" /><%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_44 %>
                    </div>
                   </ProgressTemplate>
                </asp:UpdateProgress>    	    
		        <asp:Repeater runat="server" ID="CallPathGrid">
			        <HeaderTemplate>
				        <table class="NeedsZebraStripes" cellspacing="0" cellpadding="3px">
					        <thead>
						        <tr>
							        <td class="voipCallPathColumnwidth"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_153 %></td>
							        <td colspan="2" class="voipCallPathColumnPadding"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_162 %></td>
							        <td class="voipCallPathColumnPadding"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_163 %></td>
							        <td class="voipCallPathColumnPadding"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_164 %></td>
							        <td class="voipCallPathPacketLossColumnAlign"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_165 %></td>
						        </tr>
					        </thead>
			        </HeaderTemplate>
			        <ItemTemplate>
				        <tr>
					        <td>
					            <a href='<%#GetViewLink((string)Eval("NetObjectID"))%>'>
						        <voip:StatusIcon ID="SmallStatusIcon1" runat="server" OperationStatus='<%#Eval("Status") %>' StatusMessage='<%#Eval("StatusMessage") %>' />&nbsp;<%#Eval("Name")%>
						        </a>
					        </td>
        				
					        <asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible='<%#(bool)Eval("HasData")&&(!(bool)Eval("IsCallPathDown"))%>'>			
						        <td><voip:Metric ID="Metric1" runat="server" Value='<%#Eval("MOS")%>' /></td>
						        <voip:MOSBar ID="MOSBar1" runat="server" Value='<%#Eval("MOS")%>' />
						        <td><voip:Metric ID="Metric2" runat="server" Value='<%#Eval("Jitter")%>' /></td>
						        <td><voip:Metric ID="Metric3" runat="server" Value='<%#Eval("Latency")%>' /></td>
						        <td class="voipCallPathPacketLossColumnAlign"><voip:Metric ID="Metric4" runat="server" Value='<%#Eval("PacketLoss")%>' /></td>
					        </asp:PlaceHolder>

					        <td id="Td1" runat="server" Visible='<%#(!(bool)Eval("HasData"))||(bool)Eval("IsCallPathDown")%>' colspan="5">
						        <%#Eval("StatusMessage")%>
					        </td>
				        </tr>
			        </ItemTemplate>
			        <FooterTemplate>
				        </table>
			        </FooterTemplate>
		        </asp:Repeater>
		        <br />
		        <table id="PagesButtons" visible="false" runat="server">
		            <tr>
		                <td style="border:none; width:25%" align="left"><asp:LinkButton ID="ButtonBack" OnClick="ButtonBack_Click"  Visible="false" runat="server" /></td>
		                <td style="border:none; font-weight:bold" align="center"><asp:Label runat="server" ID="LabelPage" Text="Page 1"></asp:Label></td>
		                <td style="border:none; width:25%" align="right"><asp:LinkButton ID="ButtonNext" OnClick="ButtonNext_Click" runat="server" /></td>		        
		            </tr>
		        </table>
		    </ContentTemplate>
		    <Triggers>
		        <asp:AsyncPostBackTrigger ControlID="ButtonBack"/>
		        <asp:AsyncPostBackTrigger ControlID="ButtonNext"/>
		    </Triggers>
		</asp:UpdatePanel>
		<voip:NoCallPaths runat="server" ID="NoCallPathsMsg" />
	</Content>
</orion:resourceWrapper>