﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VoIPGatewayEndpointTree.ascx.cs"
 Inherits="Orion_Voip_Resources_Summary_VoIPGatewayEndpointTree" %>
<%@ Register TagPrefix="ipslam" Assembly="SolarWinds.Orion.IpSla.Web" Namespace="SolarWinds.Orion.IpSla.Web.AjaxTree" %>


<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>

<ipslam:AjaxTreeControl ID="AjaxTree" runat="server">
    <WebService Path="~/Orion/Voip/Services/VoIPGatewayEndpointsService.asmx" />
</ipslam:AjaxTreeControl>

    </Content>
</orion:resourceWrapper>
