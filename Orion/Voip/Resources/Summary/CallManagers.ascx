<%@ Import namespace="SolarWinds.Orion.IpSla.Web"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CallManagers.ascx.cs" Inherits="Orion_Voip_Resources_Summary_CallManagers" %>
<%@ Register Src="~/Orion/Voip/Resources/CallManagerBody.ascx" TagPrefix="voip" TagName="CCMBody" %>

<script type="text/javascript">
function show(id)
{
	var item = document.getElementById(id);
	item.style.display = "";
}

function hide(id)
{
	var item;
	if (id.style)
		item = id;
	else
		item = document.getElementById(id);
	item.style.display = "none";
}
</script>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
		<asp:Repeater runat="server" ID="CallManagers" OnInit="CallManagers_Init" OnItemDataBound="CallManagers_ItemDataBound">
			<HeaderTemplate>
				<table border="0" cellspacing="0" cellpadding="3px">
					<thead>
						<tr >
							<td class="column-width-one-third"></td>
							<td colspan="2" align="center"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_155 %></td>
							<td colspan="2" align="center"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_156 %></td>
							<td></td>
						</tr>
						<tr>
						    <td class="Property table-header-cell-padding column-width-one-third"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_157 %></td>
						    <td class="Property table-header-cell-padding"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_158 %></td>
							<td class="Property table-header-cell-padding"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_159 %></td>
						    <td class="Property table-header-cell-padding"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_158 %></td>
						    <td class="Property table-header-cell-padding"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_159 %></td>
							<td class="Property table-header-cell-padding"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_160 %></td>
					    </tr>
					</thead>
			</HeaderTemplate>
			
			<ItemTemplate>
			<tr style="<%# !_isGroping?"display:none":"" %>">
			<td  colspan ="6">
			    <img id="<%#MakeID(Container.DataItem,"open") %>" class="showtoggle" src="<%=ResolveUrl("~/Orion/images/Button.Expand.gif")%>" 
				        onclick="show('<%#MakeID(Container.DataItem,"GROUP")%>');show('<%#MakeID(Container.DataItem,"close") %>');hide(this);"   />
							
				<img id="<%#MakeID(Container.DataItem, "close")%>" class="hidetoggle" src="<%=ResolveUrl("~/Orion/images/Button.Collapse.gif") %>"
						onclick="hide('<%#MakeID(Container.DataItem, "GROUP")%>');show('<%#MakeID(Container.DataItem, "open")%>');hide(this);" style="display:none;"/>
			    <%# _isGroping?Container.DataItem:string.Empty %>
			</td>
			</tr>
			
			<tbody  id="<%#MakeID(Container.DataItem, "GROUP")%>" style="<%# _isGroping?"display:none":"" %>"  >
				<voip:CCMBody ID="ccmBodyId" runat="server"  />
			</tbody>
			
			</ItemTemplate>
			
			<FooterTemplate>
				</table>
			</FooterTemplate>
		</asp:Repeater>
		<asp:PlaceHolder  runat="server" ID="NoCallManagers">
            <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_AK1_161, "<a href=\"/Orion/Voip/Admin/ManageCallManagers/Default.aspx\" class=\"sw-link\" >", "</a>") %>
		</asp:PlaceHolder>
	</Content>
</orion:resourceWrapper>
