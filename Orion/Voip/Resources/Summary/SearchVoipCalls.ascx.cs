﻿using System;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, ResourceTypes.VOIP)]
public partial class Orion_Voip_Resources_CallManager_SearchVoipCalls : VoipBaseResource
{

    protected static readonly string CallOrigination = Resources.VNQMWebContent.VNQMWEBCODE_AK1_39;
    protected static readonly string CallDestination = Resources.VNQMWebContent.VNQMWEBCODE_AK1_40;
    protected static readonly string CallTime = Resources.VNQMWebContent.VNQMWEBCODE_AK1_41;
    protected static readonly string CallStatus = Resources.VNQMWebContent.VNQMWEBCODE_AK1_42;
    protected static readonly string CallQuality = Resources.VNQMWebContent.VNQMWEBCODE_AK1_43;

    public override string DetachURL
    {
        get
        {
            return string.Format("DetachResource.aspx?ResourceID={0}&NetObject=", this.Resource.ID);
        }
    }
    protected override string DefaultTitle
    {
        get { return Resources.VNQMWebContent.VNQMWEBCODE_AK1_44; }

    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionIPSLAManagerPHResourceSearchVoIPCalls";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string returnUrl = this.Page.ResolveUrl("/Orion/Voip/VoipCallSearch.aspx");
           
            var param = new StringBuilder();
            param.AppendFormat("'{0}','", ddlCallOriginationType.ClientID);
            param.AppendFormat("{0}','", ddlCallDestinationType.ClientID);
            param.AppendFormat("{0}','", txtOriginIPAddressorPhone.ClientID);
            param.AppendFormat("{0}','", txtDestinationIPAddressorPhone.ClientID);
            param.AppendFormat("{0}','", ddlOriginRegion.ClientID);
            param.AppendFormat("{0}','", ddlOriginGateway.ClientID);
            param.AppendFormat("{0}','", ddlDestinationRegion.ClientID);
            param.AppendFormat("{0}','", ddlDestinationGateway.ClientID);
            param.AppendFormat("{0}','", ddlCallTime.ClientID);
            param.AppendFormat("{0}','", cbFailed.ClientID);
            param.AppendFormat("{0}','", cbMOS.ClientID);
            param.AppendFormat("{0}','", ddlMOS.ClientID);
            param.AppendFormat("{0}','", cbJitter.ClientID);
            param.AppendFormat("{0}','", ddlJitter.ClientID);
            param.AppendFormat("{0}','", cbLatency.ClientID);
            param.AppendFormat("{0}','", ddlLatency.ClientID);
            param.AppendFormat("{0}','", cbPacketLoss.ClientID);
            param.AppendFormat("{0}'", ddlPacketLoss.ClientID);
            
            ddlCallOriginationType.Attributes.Add("onChange", "origintypechanged(this,'" + divCallOriginationIPAddressorPhone.ClientID + "','" + ddlOriginGateway.ClientID + "','" + ddlOriginRegion.ClientID + "')");
            ddlCallDestinationType.Attributes.Add("onChange", "destinationtypechanged(this,'" + divCallDestinationIPAddressorPhone.ClientID + "','" + ddlDestinationGateway.ClientID + "','" + ddlDestinationRegion.ClientID + "')");
            submitButton.Attributes.Add("onclick", "return OnSubmit(" + param.ToString() + ",'"+ returnUrl + "')");
    
            DataTable gatewayTable = GatewaysDAL.Instance.GetAllGatewayRecords();

            ddlOriginGateway.DataSource = gatewayTable;
            ddlOriginGateway.DataTextField = "DisplayName";
            ddlOriginGateway.DataValueField = "ID";
            ddlOriginGateway.DataBind();
            ddlOriginGateway.Items.Insert(0, new ListItem(Resources.VNQMWebContent.VNQMWEBCODE_AK1_45, "-1"));


            ddlDestinationGateway.DataSource = gatewayTable;
            ddlDestinationGateway.DataTextField = "Name";
            ddlDestinationGateway.DataValueField = "IpAddress";
            ddlDestinationGateway.DataBind();

            ddlDestinationGateway.Items.Insert(0, new ListItem(Resources.VNQMWebContent.VNQMWEBCODE_AK1_45, "-1"));

            using (DataTable regionTable = RegionsDAL.Instance.GetAllRegionRecords())
            {
                ddlOriginRegion.DataSource = regionTable;
                ddlOriginRegion.DataTextField = "RegionName";
                ddlOriginRegion.DataValueField = "RegionID";
                ddlOriginRegion.DataBind();
                ddlOriginRegion.Items.Insert(0, new ListItem(Resources.VNQMWebContent.VNQMWEBCODE_AK1_45, "-1"));


                ddlDestinationRegion.DataSource = regionTable;
                ddlDestinationRegion.DataTextField = "RegionName";
                ddlDestinationRegion.DataValueField = "RegionID";
                ddlDestinationRegion.DataBind();

                ddlDestinationRegion.Items.Insert(0, new ListItem(Resources.VNQMWebContent.VNQMWEBCODE_AK1_45, "-1"));
            }
        }
    
    }
}