﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchVoipCalls.ascx.cs"
    Inherits="Orion_Voip_Resources_CallManager_SearchVoipCalls" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagName="IncludeExtJs" TagPrefix="orion" %>

<orion:Include ID="Include1" runat="server" File="Voip/Resources/Summary/SearchVoipCalls.js" />
<orion:IncludeExtJs ID="includeExtJs" runat="server" Version="3.4"/>

<style type="text/css">
    .VoipSearchCallsResouceColumn { padding-left: 0px !important;}
</style>

<orion:resourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="false" OnLoad="Page_Load"> 
    <Content>
        <script src="/Orion/Voip/Services/VoipCallSearchService.asmx/js" type="text/javascript"></script>
        <asp:ScriptManagerProxy ID="scriptManager" runat="server">
            <Services>
                <asp:ServiceReference Path="~/Orion/Voip/Services/VoipCallSearchService.asmx" />
            </Services>
        </asp:ScriptManagerProxy>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table style="width: auto !important;">
                    <tr>
                        <td class="VoipSearchCallsResoucePropertyHeader VoipSearchCallsResouceNoBorder">
                            <%= CallOrigination %>
                        </td>
                        <td class="VoipSearchCallsResouceColumn VoipSearchCallsResouceNoBorder">
                            <asp:DropDownList ID="ddlCallOriginationType"  runat="server" Width="182px" CssClass="voipSearchFont">
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBCODE_AK1_45 %>" Value="Select" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBCODE_VB1_143 %>" Value="OrgPhoneNumber"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_99 %>" Value="OrgIPAddress"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBCODE_VB1_37 %>" Value="OrgLocation"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_101 %>" Value="OrgGateway"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="VoipSearchCallsResoucePropertyHeader VoipSearchCallsResouceNoBorder">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="VoipSearchCallsResoucePropertyHeader VoipSearchCallsResouceNoBorder">&nbsp;</td>
                        <td class="VoipSearchCallsResouceColumn VoipSearchCallsResouceNoBorder">
                            <div id="divCallOriginationIPAddressorPhone" runat="server" class="voipSearchFont">
                                    <asp:TextBox ID="txtOriginIPAddressorPhone" runat="server" Width="180px" class="voipSearchFont"></asp:TextBox>
                                    <ajaxToolkit:TextBoxWatermarkExtender ID="txtOriginIPAddressorPhone_WatermarkExtender"
                                        runat="server" Enabled="True" TargetControlID="txtOriginIPAddressorPhone" WatermarkText="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_102 %>"
                                        WatermarkCssClass="voipSearchWaterMarkStyle voipSearchFont">
                                    </ajaxToolkit:TextBoxWatermarkExtender>
                                </div>
                                <asp:DropDownList runat="server" ID="ddlOriginRegion" Style="display: none; width: 182px"
                                    CssClass="voipSearchFont">
                                </asp:DropDownList>
                                <asp:DropDownList runat="server" ID="ddlOriginGateway" Style="display: none; width: 182px"
                                    CssClass="voipSearchFont">
                                </asp:DropDownList>
                        </td>
                        <td class="VoipSearchCallsResouceColumn VoipSearchCallsResouceNoBorder">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="VoipSearchCallsResoucePropertyHeader VoipSearchCallsResouceNoBorder">
                            <%= CallDestination %>
                        </td>
                        <td class="VoipSearchCallsResouceColumn VoipSearchCallsResouceNoBorder" style="max-width: 220px;">
                            <asp:DropDownList ID="ddlCallDestinationType" runat="server" Width="182px" CssClass="voipSearchFont">
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBCODE_AK1_45 %>" Value="Select" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBCODE_VB1_143 %>" Value="DestPhoneNumber"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_99 %>" Value="DestIPAddress"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBCODE_VB1_37 %>" Value="DestLocation"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_101 %>" Value="DestGateway"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="VoipSearchCallsResoucePropertyHeader VoipSearchCallsResouceNoBorder">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="VoipSearchCallsResoucePropertyHeader VoipSearchCallsResouceNoBorder">&nbsp;</td>
                        <td class="VoipSearchCallsResouceColumn VoipSearchCallsResouceNoBorder">
                              <div id="divCallDestinationIPAddressorPhone" runat="server" class="voipSearchFont">
                                <asp:TextBox ID="txtDestinationIPAddressorPhone" runat="server" Width="180px" class="voipSearchFont"></asp:TextBox>
                                <ajaxToolkit:TextBoxWatermarkExtender ID="txtDestinationIPAddressorPhone_WatermarkExtender"
                                    runat="server" Enabled="True" TargetControlID="txtDestinationIPAddressorPhone"
                                    WatermarkText="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_102 %>" WatermarkCssClass="voipSearchWaterMarkStyle voipSearchFont">
                                </ajaxToolkit:TextBoxWatermarkExtender>
                            </div>
                            <asp:DropDownList runat="server" ID="ddlDestinationRegion" Style="display: none;width: 182px;" CssClass="voipSearchFont">
                            </asp:DropDownList>
                            <asp:DropDownList runat="server" ID="ddlDestinationGateway" Style="display: none;width: 182px;" CssClass="voipSearchFont">
                            </asp:DropDownList>
                        </td>
                        <td class="VoipSearchCallsResouceColumn VoipSearchCallsResouceNoBorder">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="VoipSearchCallsResoucePropertyHeader VoipSearchCallsResouceNoBorder">
                            <%= CallTime %>
                        </td>
                        <td class="VoipSearchCallsResouceColumn VoipSearchCallsResouceNoBorder">
                            <asp:DropDownList runat="server" ID="ddlCallTime" Width="182px" CssClass="voipSearchFont">
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_106 %>" Value="Last15Minutes"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_107 %>" Value="Last30Minutes"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_108 %>" Value="LastHour"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_109 %>" Value="Last2Hours"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_110 %>" Value="Last12Hours"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_111 %>" Value="LastDay"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_112 %>" Value="Last7Days"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_113 %>" Value="LastMonth"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="VoipSearchCallsResoucePropertyHeader VoipSearchCallsResouceNoBorder">
                            <%= CallStatus %>
                        </td>
                        <td class="VoipSearchCallsResouceColumn VoipSearchCallsResouceNoBorder">
                            <asp:CheckBox runat="server" ID="cbFailed" />
                            <asp:Label runat="server" ID="lblFailed" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_173 %>"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="VoipSearchCallsResoucePropertyHeader VoipSearchCallsResouceNoBorder">
                            <%= CallQuality %>
                        </td>
                        <td class="VoipSearchCallsResouceColumn VoipSearchCallsResouceNoBorder">
                            <asp:CheckBox runat="server" ID="cbMOS"/>
                            <asp:Label runat="server" ID="lblMos" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_120 %>"></asp:Label>
                        </td>
                        <td  class="VoipSearchCallsResouceColumn VoipSearchCallsResouceNoBorder">
                            <asp:DropDownList runat="server" ID="ddlMOS" CssClass="voipSearchFont">
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_97 %>" Value="-All-"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_37 %>" Value="2"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_35 %>" Value="3"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_41 %>" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="VoipSearchCallsResouceNoBorder">
                        </td>
                        <td class="VoipSearchCallsResouceColumn VoipSearchCallsResouceNoBorder">
                            <asp:CheckBox runat="server" ID="cbJitter"/>
                            <asp:Label runat="server" ID="lblJitter" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_125 %>"></asp:Label>
                        </td>
                        <td class="VoipSearchCallsResouceColumn VoipSearchCallsResouceNoBorder">
                            <asp:DropDownList runat="server" ID="ddlJitter" CssClass="voipSearchFont">
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_97 %>" Value="-All-"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_37 %>" Value="2"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_35 %>" Value="3"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_41 %>" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="VoipSearchCallsResouceNoBorder">
                        </td>
                        <td class="VoipSearchCallsResouceColumn VoipSearchCallsResouceNoBorder">
                            <asp:CheckBox runat="server" ID="cbLatency"/>
                             <asp:Label runat="server" ID="lblLatency" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_123 %>"></asp:Label>
                        </td>
                        <td class="VoipSearchCallsResouceColumn VoipSearchCallsResouceNoBorder">
                            <asp:DropDownList runat="server" ID="ddlLatency" CssClass="voipSearchFont">
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_97 %>" Value="-All-"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_37 %>" Value="2"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_35 %>" Value="3"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_41 %>" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="VoipSearchCallsResouceNoBorder">
                        </td>
                        <td class="VoipSearchCallsResouceColumn VoipSearchCallsResouceNoBorder">
                            <asp:CheckBox runat="server" ID="cbPacketLoss"/>
                             <asp:Label runat="server" ID="lblPacketLoss" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_126 %>"></asp:Label>
                        </td>
                        <td class="VoipSearchCallsResouceColumn VoipSearchCallsResouceNoBorder">
                            <asp:DropDownList runat="server" ID="ddlPacketLoss" CssClass="voipSearchFont">
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_97 %>" Value="-All-"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_37 %>" Value="2"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_35 %>" Value="3"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_41 %>" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="VoipSearchCallsResouceNoBorder"></td>
                        <td class="VoipSearchCallsResouceNoBorder">
                            <div class="sw-btn-bar">
                                <orion:LocalizableButton runat="server" ID="submitButton" LocalizedText="CustomText"
                                    Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_127 %>" DisplayType="Secondary" />
                            </div>
                        </td>
                        <td class="VoipSearchCallsResouceNoBorder" style="padding-left: 17px; padding-top: 15px;"></td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </Content>
</orion:resourceWrapper>
