<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Thwack.ascx.cs" Inherits="Orion_Voip_Resources_Thwack" %>
<link rel="stylesheet" type="text/css" href="/Orion/Voip/styles/Thwack.css" />
<orion:resourceWrapper runat="server" ID="resourceWrapper" ShowHeaderBar="false">
    <Content>
        <div class="ThwackContentBorder">
            <table class="ThwackHeader ThwackTableMargin">
                <tr>
                    <td class="NoBorder">
                        <table class="ThwackTableMargin">
                            <tr>
                                <td class="ThwackCaptionColumn NoBorder" style="width: 300px;">
                                    <div>
                                        <img class="ThwackHeaderImage" src="<%= SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("Voip", "ThwackImages/ThwackLogo.png") %>" />
                                        <span class="ThwackTextHeader"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_178 %></span>
                                    </div>
                                </td>
                                <td class="NoBorder">
                                    <asp:PlaceHolder ID="phEditButton" runat="server">
                                        <orion:LocalizableButtonLink runat="server" ID="EditButton" LocalizedText="Edit" DisplayType="Resource" CssClass="HelpButton" 
                                        NavigateUrl="<%# this.EditURL %>"/>
                                    </asp:PlaceHolder>
                                </td>
                                <td class="ThwackTextHeaderRight NoBorder">
                                    <asp:PlaceHolder ID="phHelpButton" runat="server">
                                        <orion:LocalizableButtonLink runat="server" ID="HelpButton" LocalizedText="CustomText" DisplayType="Resource" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_181 %>" CssClass="HelpButton" 
                                         NavigateUrl="<%# this.HelpURL %>" Target="_blank"/>
                                    </asp:PlaceHolder>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <asp:Label ID="noThwackLabel" class="Property" runat="server" Visible="true" />
            <div runat="server" id="thwackRecentPostsList" class="ThwackContent" />
            <table class="ThwackTableMargin ThwackFooter">
                <tr>
                    <td class="ThwackFooterMiddle NoBorder">
                        <a href="http://thwack.com/forums/34.aspx" class="sw-link"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_179 %></a>
                        <a class="ThwackFooterRight sw-link" href="http://thwack.com/user/CreateUser.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_180 %></a>
                    </td>
                </tr>
            </table>
        </div>
    </Content>
</orion:resourceWrapper>
