﻿<%@ Page Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true" CodeFile="EditSipTrunks.aspx.cs" Inherits="Orion_Voip_Resources_EditSipTrunks" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <h1 style="padding-left: 0"><%= Page.Title %></h1>
    
    <asp:ValidationSummary ID="ValidationSummary" runat="server" />
    <b>
        <orion:EditResourceTitle runat="server" ID="ResourceTitleEditor" ShowSubTitle="false" />
    </b>

    <b><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_135 %></b>

    <br/> 
    
    <asp:TextBox runat="server" ID="CallManagerSipTrunksPageSize"></asp:TextBox>
    <asp:RangeValidator ID="CallManagerSipTrunksPageSizeRangeValidator" runat="server"
        ControlToValidate="CallManagerSipTrunksPageSize"
        ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_136 %>"
        MaximumValue="500"
	    MinimumValue="10"
	    Type="Integer">*</asp:RangeValidator>
    <asp:RequiredFieldValidator ID="CallManagerSipTrunksPageSizeRequiredFieldValidator" runat="server"
        ControlToValidate="CallManagerSipTrunksPageSize"
        ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_137 %>">*</asp:RequiredFieldValidator>
    
    <br />
    <br />

    <orion:LocalizableButton runat="server" ID="btnSubmit" OnClick="SubmitClick"
           LocalizedText="Submit" DisplayType="Primary" 
           ToolTip="<%$ Resources : VNQMWebContent, VNQMWEBCODE_VB1_171 %>"/>    
</asp:Content>