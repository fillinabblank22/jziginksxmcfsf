
using SolarWinds.Orion.IpSla.Data.Database;
using SolarWinds.Orion.IpSla.Web;

public partial class Graphs_MinMaxAvgLatencyGraph : CallPathGraphResource
{
    protected string ChartImageUrl
    {
        get
        {
            return GetChartImageUrl(Operation, OperationResultsKey.Latency.ToString());
        }
    }

    protected string CustomUrl
    {
        get { return GenerateCustomChartLink(Operation, OperationResultsKey.Latency.ToString()); }
    }

	protected override string DefaultTitle
	{
		get { return "MIN/MAX/AVG graph of Latency"; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionVoIPMonitorPHChartMMALatency"; }
	}
}
