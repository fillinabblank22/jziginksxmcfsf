
using SolarWinds.Orion.IpSla.Data.Database;
using SolarWinds.Orion.IpSla.Web;

public partial class Graphs_MinMaxAvgMOSGraph : CallPathGraphResource
{
    protected string ChartImageUrl
    {
        get
        {
            return GetChartImageUrl(Operation, OperationResultsKey.MOS.ToString());
        }
    }

    protected string CustomUrl
    {
        get { return GenerateCustomChartLink(Operation, OperationResultsKey.MOS.ToString()); }
    }

	protected override string DefaultTitle
	{
		get { return "MIN/MAX/AVG graph of MOS"; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionVoIPMonitorPHChartMMAMOS"; }
	}
}
