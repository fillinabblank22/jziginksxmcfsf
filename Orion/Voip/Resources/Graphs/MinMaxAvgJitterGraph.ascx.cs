
using SolarWinds.Orion.IpSla.Data.Database;
using SolarWinds.Orion.IpSla.Web;

public partial class Graphs_MinMaxAvgJitter : CallPathGraphResource
{
    protected string ChartImageUrl
    {
        get
        {
            return GetChartImageUrl(Operation, OperationResultsKey.Jitter.ToString());
        }
    }

    protected string CustomUrl
    {
        get { return GenerateCustomChartLink(Operation, OperationResultsKey.Jitter.ToString()); }
    }

	protected override string DefaultTitle
	{
		get { return "MIN/MAX/AVG graph of Jitter"; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionVoIPMonitorPHChartMMAJitter"; }
	}
}
