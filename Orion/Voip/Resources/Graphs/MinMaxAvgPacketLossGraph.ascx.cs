
using SolarWinds.Orion.IpSla.Data.Database;
using SolarWinds.Orion.IpSla.Web;

public partial class Graphs_MinMaxAvgPacketLoss : CallPathGraphResource
{
    protected string ChartImageUrl
    {
        get
        {
            return GetChartImageUrl(Operation, OperationResultsKey.PacketLoss.ToString());
        }
    }

    protected string CustomUrl
    {
        get { return GenerateCustomChartLink(Operation, OperationResultsKey.PacketLoss.ToString()); }
    }

	protected override string DefaultTitle
	{
		get { return "MIN/MAX/AVG graph of Packet Loss"; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionVoIPMonitorPHChartMMAPacketLoss"; }
	}
}
