<%@ Control Language="C#" ClassName="VoipActiveAlerts" Inherits="SolarWinds.Orion.Web.UI.BaseResourceControl" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common.i18n.Registrar" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web" %>

<script runat="server">
    protected Region region;

    protected override string DefaultTitle { get { return Resources.VNQMWebContent.VNQMWEBCODE_VB1_221; } }

    protected void AlertTable_Init(object sender, EventArgs e)
    {
        region = GetInterfaceInstance<IRegionProvider>().Region;

        using (var table = VoipAlert.GetVoIPActiveAlertsRegion(region.VoipRegion.RegionID))
        {
            AlertTable.DataSource = table;
            AlertTable.DataBind();
        }
    }

    public override string DisplayTitle
    {
        get { return String.Format(Resources.VNQMWebContent.VNQMWEBCODE_VB1_222, region.VoipRegion.RegionName); }
    }

    public string GetViewLink(string triggeringObject, string triggeringObjectDetailsUrl)
    {
        if (string.IsNullOrEmpty(triggeringObjectDetailsUrl))
        {
            return triggeringObject;
        }
        return string.Format("<a href=\"{0}\">{1}</a>", triggeringObjectDetailsUrl, triggeringObject);
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVoIPMonitorPHResourceActiveAlerts"; }
    }
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IRegionProvider) }; }
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
</script>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
		<asp:Repeater ID="AlertTable" runat="server" OnInit="AlertTable_Init">
			<HeaderTemplate>
				<table class="NeedsZebraStripes" border="0" cellPadding="2" cellSpacing="0" width="100%">
					<thead>
						<tr>
							<td width="20">&nbsp;</td>
							<td width="24%"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_140 %></td>
							<td><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_141 %></td>
                            <td><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_142 %></td>
							<td width="45%"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_143 %></td>
						</tr>
					</thead>
			</HeaderTemplate>

			<ItemTemplate>
				<tr>
					<td><img src="/NetPerfMon/images/Event-19.gif" /></td>
					<td><%#Eval("AlertTime")%></a></td>
                    <td><%#GetViewLink((string)Eval("ObjectName"), (string)Eval("ObjectID"))%></td>
                    <td><%#GetLocalizedProperty("ObjectType", Eval("ObjectType").ToString())%></a></td>
					<td><%#Eval("EventMessage")%></td>
				</tr>
			</ItemTemplate>

			<FooterTemplate>
				</table>
			</FooterTemplate>
		</asp:Repeater>
	</Content>
</orion:resourceWrapper>
