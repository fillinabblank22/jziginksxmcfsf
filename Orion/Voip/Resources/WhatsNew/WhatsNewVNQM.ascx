﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WhatsNewVNQM.ascx.cs"
    Inherits="Orion_Voip_Resources_WhatsNew_WhatsNewVNQM" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<orion:resourceWrapper ID="Wrapper" runat="server">
    <Content>
        <style type="text/css">
            #whatsnew h1
            {
                color: #675C53;
                font-size: 12px;
                font-weight: bold;
                font-family: Arial,Verdana,Helvetica,sans-serif;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            #whatsnew a
            {
                color: #336699;
            }
            #whatsnew td
            {
                border-bottom: 0px;
            }            
            #whatsnew td a img
            {
                vertical-align: baseline;
            }
            #whatsnew ul
            {
                padding-left: 22px; 
                margin-bottom: 5px;
            }                        
            #whatsnew li
            {
                list-style-type:square;
            }
        </style>
        <div id="whatsnew">
            <table>
                <colgroup>
                    <col width="auto" />
                    <col width="100%" />
                </colgroup>
                <tr>
                    <td valign="top">
                        <img ID="WhatNewIcon" style="padding-right:5px;" src="<%= SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("Voip", "../images/WhatsNew/icon_new.gif") %>" />
                    </td>
                    <td>
                        <%= this.GetVNQMWhatsNew() %>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">                        
                        <orion:LocalizableButtonLink runat="server" target="_blank" NavigateUrl="https://www.solarwinds.com/documentation/kbloader.aspx?kb=VNQM_2020-2_release_notes" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_RKG_13 %>" DisplayType="Primary" />
                        &nbsp;
                        <orion:LocalizableButton runat="server" ID="btnRemoveResource" OnClick="OnRemoveResourceClick" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_RKG_14 %>" DisplayType="Secondary" />
                    </td>
                </tr>
            </table>
        </div>
    </Content>
</orion:resourceWrapper>
