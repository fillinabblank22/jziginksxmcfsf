﻿using System;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Help)]
public partial class Orion_Voip_Resources_WhatsNew_WhatsNewVNQM : BaseResourceControl
{
    private const string VNQM_VERSION = "";

    public override string HelpLinkFragment
    {
        get
        {
            return string.Empty;
        }
    }

    protected override string DefaultTitle
    {
        get { return VNQMWebContent.VNQMWEBDATA_RKG_12; }
    }

    public sealed override string DisplayTitle
    {
        get { return String.Format(this.Title, VNQM_VERSION); }
    }

    protected void OnRemoveResourceClick(object sender, EventArgs e)
    {
        ResourceManager.DeleteById(this.Resource.ID);

        // reorder resources after delete
        var targetColumn = Resource.Column;
        var resources = ResourceManager.GetResourcesForView(Resource.View.ViewID);
        var pos = 1;

        foreach (var resource in resources)
        {
            if (resource.Column == targetColumn)
            {
                ResourceManager.SetPositionById(pos++, resource.ID);
            }
        }

        // after delete resource is still in Page.Controls, because it's a postback
        // resource. so we have to reload page
        Response.Redirect(Request.Url.AbsoluteUri);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Wrapper.ShowEditButton = false;
    }

    public string GetVNQMWhatsNew()
    {
        return string.Format(VNQMWebContent.VNQMWEBDATA_WHATSNEW_1,
            HelpHelper.GetHelpUrl("voip", "OrionIPSLAManagerAGSIPMonitoringOnCiscoCube"));
    }
}
