﻿<%@ Page Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true" CodeFile="EditGauge.aspx.cs" Inherits="Orion_Voip_Resources_EditGauge" Title="Untitled Page" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadPlaceHolder" runat="Server">
      <link rel="stylesheet" type="text/css" href="/Orion/styles/EditGauge.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <orion:Include runat="server" File="ig_webGauge.js"/>
    <orion:Include runat="server" File="ig_shared.js"/>
    <script language="javascript" type="text/javascript">
        var selectedStyle;
        function SelectGauge(styleName)
        {
            var oldSelectedStyle = document.getElementById(selectedStyle);
            if (oldSelectedStyle != null)
            { 
                oldSelectedStyle.className="DeSelectedGauge";
            }            
            var gaugesListSpan = document.getElementById("GaugesList");
            var gaugesSelect = gaugesListSpan.getElementsByTagName("select");
            var gaugeSelect = gaugesSelect[0];
            for (var i=0; i < gaugeSelect.options.length; i++)
            {
                if (gaugeSelect.options[i].value == styleName)
                {
                    gaugeSelect.selectedIndex = i;
                    break;
                }
            }
            var gaugeImageSpan = document.getElementById(styleName);
            gaugeImageSpan.className="SelectedGauge";
            selectedStyle = gaugeImageSpan.id;
        }
        
        function SelectCurrentGauge()
        {
            var gaugesListSpan = document.getElementById("GaugesList");
            var gaugesSelect = gaugesListSpan.getElementsByTagName("select");
            var gaugeSelect = gaugesSelect[0];
            var value = gaugeSelect.options[gaugeSelect.selectedIndex].value;
            SelectGauge(value);    
        }
    </script>
    <h1 style="padding-left: 0px"><%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_AK1_167, Resource.Title) %></h1>
    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSubmit">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
    <b><orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="true" /></b>
   
    <b><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_175 %></b>
    <br />
    <span id="GaugesList"><asp:DropDownList ID="stylesList" runat="server" onchange="SelectCurrentGauge()"></asp:DropDownList></span>
    <br />
    <br />
    <b><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_176 %></b>
    <br />
    <asp:TextBox ID="scaleInput" runat="server" maxlength="3"></asp:TextBox>
    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="scaleInput"
	ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_177 %>" MinimumValue="30" MaximumValue="250"
	Type="Integer" >*</asp:RangeValidator><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="scaleInput"
	ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_178 %>">*</asp:RequiredFieldValidator>
	<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="scaleInput"
	ErrorMessage="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_179 %>" Operator="DataTypeCheck" Type="Integer" >*</asp:CompareValidator>
    <br />
    <br />
    <div class="sw-btn-bar">
    <orion:LocalizableButton runat="server" ID="btnBack" ToolTip="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_180 %>" DisplayType="Secondary" LocalizedText="Back" OnClick="BackClick" />
    <orion:LocalizableButton runat="server" ID="btnSubmit" ToolTip="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_181 %>" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick" />
    </div>
    <div id="GaugeStylesPanel" runat="server" style="background-color:White; border-style:ridge; width:840px"></div>
    
    <script language="javascript" type="text/javascript">
        SelectCurrentGauge();
    </script>
    </asp:Panel>
</asp:Content>


