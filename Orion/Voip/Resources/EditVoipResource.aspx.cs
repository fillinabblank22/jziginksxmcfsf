﻿using System;
using SolarWinds.Orion.IpSla.Web.UI;

public partial class Orion_Voip_Resources_EditVoipResource : ResourceEditPage
{
    protected override void Initialize()
    {
        this.resourceTitleEditor.ResourceTitle = this.Resource.Title;
        this.resourceTitleEditor.ResourceSubTitle = this.Resource.SubTitle;
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (!resourceTitleEditor.ResourceTitle.Equals(Resource.Title) || !resourceTitleEditor.ResourceSubTitle.Equals(Resource.SubTitle))
        {
            Resource.Title = resourceTitleEditor.ResourceTitle;
            Resource.SubTitle = resourceTitleEditor.ResourceSubTitle;

            SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
        }

        GoBack();
    }
}