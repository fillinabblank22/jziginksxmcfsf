using System;
using System.Collections.Specialized;
using System.Text;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.Web;

public partial class Orion_Voip_CallManager : VoipView
{
    public override string HelpFragment
    {
        get { return "OrionIPSLAManagerPHViewVoIPCallManager"; }
    }

	protected override void OnInit(EventArgs e)
	{
		this.Title = this.ViewInfo.ViewTitle;
		this.resHost.CallManager = (CallManager)NetObject;
		this.resContainer.DataSource = this.ViewInfo;
		this.resContainer.DataBind();
		base.OnInit(e);

        OrionInclude.CoreFile("/js/OrionCore.js");
	}

	public override string ViewType
	{
		get { return "Voip CallManager"; }
	}

	public override string NetObjectIDForASP
	{
		get { return "N:" + ((CallManager)NetObject).NodeID; }
	}

	// Using the NetObjectIDForASP trick above, CallManager.aspx can host old 
	// Node Details resources. However, if you use the Edit button on one 
	// of those resources, it will redirect back to a view with a NetObject=N:xx,
	// which will send you to NodeDetails.aspx. This is confusing. This override 
	// of SelectView will detect that situation, correct the NetObject value to 
	// use the VCCM prefix and redirect you again.
	public override void SelectView()
	{
		string netObjectID = Request.QueryString["NetObject"];
		if (!string.IsNullOrEmpty(netObjectID))
		{
			string[] parts = netObjectID.ToUpperInvariant().Split(':');
			if (parts.Length == 2 && parts[0] == "N")
			{
				StringBuilder outStr = new StringBuilder("/Orion/Voip/CallManager.aspx");
				StringDictionary queryParameters = new StringDictionary();
				queryParameters["NetObject"] = "VCCM:" + parts[1];

				foreach (string queryParameter in Request.QueryString.Keys)
				{
					if (!queryParameters.ContainsKey(queryParameter))
						queryParameters[queryParameter] = Request.QueryString[queryParameter];
				}

				bool first = true;
				foreach (string param in queryParameters.Keys)
				{
					outStr.AppendFormat("{0}{1}={2}", first ? "?" : "&", param, queryParameters[param]);
					first = false;
				} 
				
				Response.Redirect(outStr.ToString());
			}
		}

		base.SelectView();
	}
}
