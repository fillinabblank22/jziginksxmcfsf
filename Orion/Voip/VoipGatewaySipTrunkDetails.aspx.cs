﻿using System;
using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Web;

namespace Orion.Voip
{
    public partial class OrionVoipGatewaySipTrunkDetails : VoipView, IVoipGatewaySipTrunkProvider
    {
        protected static readonly Log Log = new Log();
        protected override void OnInit(EventArgs e)
        {
            try
            {
                resContainer.DataSource = ViewInfo;
                resContainer.DataBind();
                base.OnInit(e);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat($"OrionVoipGatewaySipTrunkDetails: {ex}");                
            }
        }
        public override string ViewType => "Voip Gateway SIPTrunk";

        public override string HelpFragment => "";

        public VoipGatewaySipTrunk VoipGatewaySipTrunk => new VoipGatewaySipTrunk(Request.QueryString["NetObject"]);

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = string.Format(VNQMWebContent.VNQMWEB_MH_SIP_Trunk_Details_Page_Title, ViewInfo.ViewTitle, VoipGatewaySipTrunk.Name);
        }

        public override void SelectView()
        {
            base.SelectView();
            if (NetObject == null)
            {
                NetObject = new VoipGatewaySipTrunk(Request.QueryString["NetObject"]);
            }
        }
    }
}