﻿using System;
using System.Collections.Generic;

public partial class Orion_Voip_Admin_Config : System.Web.UI.MasterPage
{
    private static readonly List<string> AllowedGuestPages = new List<string>
	        {
                "/Orion/Voip/Admin/Default.aspx",
                "/Orion/Voip/Admin/ManageOperations.aspx",
                "/Orion/Voip/Admin/OperationsWizard.aspx",
                "/Orion/Voip/Admin/ManageNodes/Default.aspx",
                "/Orion/Voip/Admin/ManageNodes/AddNodes.aspx",
                "/Orion/Voip/Admin/ManageCallManagers/Default.aspx",
                "/Orion/Voip/Admin/ManageCallManagers/AddCallManagerWizard.aspx",
                "/Orion/Voip/Admin/ManageMediaGateway/Default.aspx",
                "/Orion/Voip/Admin/ManageMediaGateway/AddMediaGatewayWizard.aspx",
                "/Orion/Voip/Admin/Infrastructure.aspx",
                "/Orion/Voip/Admin/VoipSettings.aspx",
                "/Orion/Voip/Admin/Thresholds.aspx",
                "/Orion/Voip/Admin/DetailedDatabaseState.aspx",
	        };

    public bool IsDemoMode { get; set; }
    
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.IsDemoMode = false;

        if (Profile.AllowAdmin)
            return;

        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            this.IsDemoMode = true;

            if (AllowedGuestPages.Exists(s => string.Equals(s, Request.Path, StringComparison.OrdinalIgnoreCase)))
                return;
        }

        Server.Transfer(String.Format("~/Orion/Error.aspx?Message={0}", Resources.VNQMWebContent.VNQMWEBCODE_VB1_257));
    }
}
