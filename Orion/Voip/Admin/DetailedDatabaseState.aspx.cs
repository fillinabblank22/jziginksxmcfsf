using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.IpSla.Data;

public partial class Orion_Voip_DetailedDatabaseState : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //sets selected table value from TableList in ViewState
        ViewState["SelectedValue"] = TableList.SelectedValue;
       
        if (!Page.IsPostBack)
        {
            //gets database version
            DatabaseVersion.Text = DatabaseStats.GetDatabaseVersion();

            //gets VoIP schema version
            VoipSchemaVersion.Text = DatabaseStats.VoipSchemaVersion;

            //gets database space stats
            string databaseSize = string.Empty;
            string databaseUnallocatedSpace = string.Empty;
            DatabaseStats.GetDatabaseSpaceData(out databaseSize, out databaseUnallocatedSpace);
            DatabaseSize.Text = databaseSize;
            DatabaseUnallocatedSpace.Text = databaseUnallocatedSpace;

            //gets table list
            TableList.DataSource = DatabaseStats.GetAllTables();  
            TableList.DataBind();
        }
        GetTableStatsFromRequest();
    }

    /// <summary>
    /// Gets Table size, rows and schema for concrete table
    /// </summary>
    private void GetTableStatsFromRequest()
    {
        if (!string.IsNullOrEmpty(Request["table"]))
        {
            string tableName = Request["table"];

            string rows = string.Empty;
            string usedByData = string.Empty;
            string usedByIndexes = string.Empty;

            if (TableList.Items.FindByValue(tableName) != null)
            {
                TableList.SelectedValue = tableName;

                //Gets rows count, table size
                DatabaseStats.GetTableSpaceData(tableName, out rows, out usedByData, out usedByIndexes);
                RowsCount.Text = rows;
                UsedByData.Text = usedByData;
                UsedByIndexes.Text = usedByIndexes;
            }
        }
    }

    /// <summary>
    /// Redirects to the page with selected table parameter
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TableList_SelectedIndexChanged(object sender, EventArgs e)
    {
        string URL = "DetailedDatabaseState.aspx";
        URL = URL + string.Format("?table={0}", ViewState["SelectedValue"]);       
        Response.Redirect(URL);
    }
}
