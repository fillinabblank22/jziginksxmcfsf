﻿/// <reference name="MicrosoftAjax.js"/>
/// <reference path="~/Orion/js/jquery.js" />
/// <reference path="~/Orion/js/extjs/debug/Ext.js" />
/// <reference path="~/Orion/js/extjs/debug/jquery-bridge.js" />
/// <reference path="~/Orion/js/extjs/debug/ext-all-debug.js" />
/// <reference path="~/Orion/Voip/js/ScriptServiceInvoker.js" />
/// <reference path="~/Orion/Voip/js/ScriptServiceProxy.js" />

Type.registerNamespace("Orion.Voip.Admin.ProgressControl");

Orion.Voip.Admin.ProgressControl = function(element) {
    Orion.Voip.Admin.ProgressControl.initializeBase(this, [element]);
    this.progressBarContent = null;
    this.processedContent = null;
    this.totalContent = null;
    this.initialValue = 0.0;
}

Orion.Voip.Admin.ProgressControl.prototype = {

    get_progressBarContent: function() {
        return this.progressBarContent;
    },
    set_progressBarContent: function(value) {
        this.progressBarContent = value;
    },

    get_processedContent: function() {
        return this.processedContent;
    },
    set_processedContent: function(value) {
        this.processedContent = value;
    },

    get_totalContent: function() {
        return this.totalContent;
    },
    set_totalContent: function(value) {
        this.totalContent = value;
    },

    get_initialValue: function() {
        return this.initialValue;
    },
    set_initialValue: function(value) {
        this.initialValue = value;
    },

    initialize: function() {
        Orion.Voip.Admin.ProgressControl.callBaseMethod(this, 'initialize');

        this.progressBar = null;
        //this.ShowProgressBar();

        if (this.progressBar === null) {
            this.progressBar = new Ext.ProgressBar({
                id: 'ProgressBar'
            });
            this.progressBar.render(this.progressBarContent);
            this.progressBar.updateProgress(this.initialValue);
        }
    },


    UpdateProgress: function(value, processedInnerHtml, totalInnerHtml) {
        this.processedContent.innerHTML = processedInnerHtml;
        this.totalContent.innerHTML = totalInnerHtml;
        this.progressBar.updateProgress(value);
    },

    dispose: function() {
        //Add custom dispose actions here
        Orion.Voip.Admin.ProgressControl.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Admin.ProgressControl.registerClass('Orion.Voip.Admin.ProgressControl', Sys.UI.Control);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
