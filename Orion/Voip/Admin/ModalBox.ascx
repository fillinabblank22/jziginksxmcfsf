<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ModalBox.ascx.cs" Inherits="Orion_Voip_Admin_ModalBox" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:UpdatePanel runat="server" ID="upModalBox" UpdateMode="Conditional" ChildrenAsTriggers="true">
    <ContentTemplate>
        <div runat="server" id="divContainer" style="display: none;">
            <div runat="server" id="divMask">
                &nbsp;
            </div>
            <div style="position: absolute; top: 20%; left: 0px; z-index: 100; width: 100%;">
                <div runat="server" id="divDialog" style="margin-left: auto; margin-right: auto; width: 350px; height: auto; background-color: White;">
                    <asp:PlaceHolder runat="server" ID="phContents" />
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>