﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_Admin_ProgressControl : ScriptUserControlBase
{
    private bool showAnimatedGif = true;

    public string Title { get; set; }
    public string TitleWidth { get; set; }
    public string ProcessedLabel { get; set; }
    public string TotalLabel { get; set; }
    public float InitialValue { get; set;}
    public string CssClass { get; set; }
    
    public Orion_Voip_Admin_ProgressControl()
    {
        TitleWidth = "35%";
    }

    public bool ShowAnimatedGif
    {
        get { return this.showAnimatedGif; }
        set { this.showAnimatedGif = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.ShowAnimatedGif)
        {
            this.gifTd.Visible = false;
            this.labelsTd.RowSpan = 2;
        }

        if (string.IsNullOrEmpty(this.Title))
        {
            this.titleTd.Visible = false;
            this.labelsTd.RowSpan = 2;
        }
        else
        {
            this.titleTd.Width = TitleWidth;
        }

        this.processedContent.InnerHtml = this.ProcessedLabel;
        this.totalContent.InnerHtml = this.TotalLabel;
    }

    protected override void RenderWrapperElementBegin(HtmlTextWriter writer)
    {
        writer.AddAttribute(HtmlTextWriterAttribute.Id, this.ClientID);
        writer.AddAttribute(HtmlTextWriterAttribute.Class, this.CssClass);
        writer.RenderBeginTag(HtmlTextWriterTag.Div);

    }

    #region IScriptControl Members

    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        var descriptor = new ScriptControlDescriptor("Orion.Voip.Admin.ProgressControl", this.ClientID);
        descriptor.AddElementProperty("progressBarContent", this.progressBarContent.ClientID);
        descriptor.AddElementProperty("processedContent", this.processedContent.ClientID);
        descriptor.AddElementProperty("totalContent", this.totalContent.ClientID);
        descriptor.AddProperty("initialValue", this.InitialValue);
        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        yield return new ScriptReference(this.ResolveUrl("ProgressControl.js"));
    }

    #endregion
}
