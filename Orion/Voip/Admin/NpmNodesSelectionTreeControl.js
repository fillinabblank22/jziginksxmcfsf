﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Voip.Admin");

Orion.Voip.Admin.NpmNodesSelectionTreeControl = function(element) {
    Orion.Voip.Admin.NpmNodesSelectionTreeControl.initializeBase(this, [element]);
}

Orion.Voip.Admin.NpmNodesSelectionTreeControl.prototype = {

    initialize: function() {
        Orion.Voip.Admin.NpmNodesSelectionTreeControl.callBaseMethod(this, 'initialize');

        // Add custom initialization here
    },
    dispose: function() {
        //Add custom dispose actions here
        Orion.Voip.Admin.NpmNodesSelectionTreeControl.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Admin.NpmNodesSelectionTreeControl.registerClass('Orion.Voip.Admin.NpmNodesSelectionTreeControl', SolarWinds.Orion.IpSla.Web.AjaxTree.SelectableNodesAjaxTreeControl);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
