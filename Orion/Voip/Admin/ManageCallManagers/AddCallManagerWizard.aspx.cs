﻿using System;
using System.Web;
using System.Web.UI;
using SolarWinds.Orion.IpSla.Web.Wizards.CallManager;

public partial class Orion_Voip_Admin_ManageCallManagers_AddCallManagerWizard : System.Web.UI.Page, IPostBackEventHandler
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
        Response.Cache.SetNoStore();
        Response.AppendHeader("Pragma", "no-cache");

        CallManagerWizardFlow flow;
        switch ((Page.Request.QueryString["Flow"] ?? string.Empty).ToLower())
        {
            case "selectnodes":
                flow = CallManagerWizardFlow.SelectNodes;
                break;
            case "defineaxl":
                flow = CallManagerWizardFlow.DefineAxl;
                break;
            case "defineftp":
                flow = CallManagerWizardFlow.DefineFtp;
                break;
            default:
                flow = CallManagerWizardFlow.SelectNodes;
                break;
        }

        if (CallManagerWizardSession.Exists)
        {
            var session = CallManagerWizardSession.Current;
            if (session.Data.IsEmpty)
            {
                if (session.Data.Flow != flow)
                {
                    CallManagerWizardSession.NewCurrent(new CallManagerWizardSession(flow), true);
                }
            }
            else
            {
                var currentPart = session.CurrentPart;

                var referrer = Request.UrlReferrer;
                if (referrer == null || StringComparer.OrdinalIgnoreCase.Compare(referrer.AbsoluteUri, Request.Url.AbsoluteUri) != 0)
                {
                    if (currentPart.WarnBeforeJoin)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ContinueInSession", @"
Ext.onReady(function(){
    Ext.MessageBox.show({
        title: '" + Resources.VNQMWebContent.VNQMWEBCODE_VB1_10 + @"',
        icon: Ext.MessageBox.QUESTION,
        msg: '" + Resources.VNQMWebContent.VNQMWEBCODE_VB1_11 + @"',
        closable: false,
        buttons: { yes: '" + Resources.VNQMWebContent.VNQMWEBCODE_VB1_12 + @"', no: '" + Resources.VNQMWebContent.VNQMWEBCODE_VB1_13 + @"' },
        fn: function(btn, value) {
            if (btn == 'no') {"
                        + ClientScript.GetPostBackEventReference(this, "Reset") + @";
            }
        }
    });
});
", true);
                    }
                    else if (currentPart.ResetBeforeJoin)
                    {
                        session.Close();
                        CallManagerWizardSession.NewCurrent(new CallManagerWizardSession(flow), true);
                    }
                }
            }
        }
        else
        {
            CallManagerWizardSession.NewCurrent(new CallManagerWizardSession(flow), true);
        }
    }

    #region IPostBackEventHandler Members

    void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
    {
        switch (eventArgument)
        {
            case "Reset":
                CallManagerWizardSession.Current.Close();
                Response.Redirect(Request.Url.PathAndQuery);
                break;
        }
    }

    #endregion
}
