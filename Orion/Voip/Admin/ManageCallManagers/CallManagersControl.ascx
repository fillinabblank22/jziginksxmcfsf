﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CallManagersControl.ascx.cs" Inherits="Orion_Voip_Admin_ManageCallManagers_CallManagersControl" %>

<script language="javascript">
    Ext.onReady(function() {
        Ext.QuickTips.init();
    });
</script>

<style>
    #<%= grid.ClientID %> td { padding-bottom: 0px; padding-right: 0px; }
</style>

<div id="grid" runat="server" class="ExtJsGrid"></div>
<asp:HiddenField ID="selectedItems" runat="server" />