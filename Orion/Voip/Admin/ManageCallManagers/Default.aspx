﻿<%@ Page Title="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_63 %>" Language="C#" MasterPageFile="~/Orion/Voip/Admin/Config.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_Voip_Admin_ManageCallManagers_Default" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagName="IncludeExtJs" TagPrefix="orion" %>
<%@ Register src="CallManagersControl.ascx" tagname="CallManagersControl" tagprefix="ipsla" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="contentHead" ContentPlaceHolderID="contentPlaceHolderHead" Runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include ID="Include2" runat="server" Module="Voip" File="styles/Admin.css" />
    <orion:Include ID="Include3" runat="server" Module="Voip" File="Voip.css" />
    <style type="text/css">
        #footer {
            min-width:1220px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="Breadcrumbs">
    <div class="sw-hdr-breadbox">
        <span><a href="../../../admin"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_6 %></a> &gt; <a href="../default.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_VnqmSettingsLinkName %></a> &gt;</span>
    </div>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="TopRightPageLinks">
  <orion:IconLinkExportToPDF runat="server" ID="ExportToPDFLink"/>
  <orion:IconHelpButton ID="IconHelpButtonMC" runat="server" HelpUrlFragment="OrionIPSLAManagerAGAddingorDeletingCiscoCallManagerDevices" />
</asp:Content>

<asp:Content ID="contentBody" runat="server" ContentPlaceHolderID="contentPlaceHolderBody">
<div style="position:relative">
    <h1 style="font-size: large;"><% = Page.Title %></h1>
</div>
    <br />
    <ipsla:CallManagersControl ID="CallManagersControl" runat="server" />
</asp:Content>