﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_Admin_ManageCallManagers_CallManagersControl : ScriptUserControlBase, IPostBackEventHandler
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.GetScriptManager().RegisterAsyncPostBackControl(this);
    }

    public string AddCallManagersUrl { get; set; }
    public string EditCallManagersUrl { get; set; }

    #region IScriptControl Members

    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Orion.Voip.Admin.ManageCallManagers.CallManagersControl", this.ClientID);
        descriptor.AddProperty("gridHostElementId", this.grid.ClientID);
        descriptor.AddProperty("selectedItemsElementId", this.selectedItems.ClientID);
        descriptor.AddScriptProperty("InvokeAddCallManagersPostback",
            "function() { " + this.Page.ClientScript.GetPostBackEventReference(this, "AddCallManagers") + "; }"
            );
        descriptor.AddScriptProperty("InvokeEditCallManagersPostback",
            "function() { " + this.Page.ClientScript.GetPostBackEventReference(this, "EditCallManagers") + "; }"
            );
        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = this.ResolveUrl("CallManagersControl.js");
        yield return reference;
    }

    #endregion

    #region IPostBackEventHandler Members

    public void RaisePostBackEvent(string eventArgument)
    {
        switch (eventArgument)
        {
            case "AddCallManagers":
                Response.Redirect(AddCallManagersUrl);
                break;
            case "EditCallManagers":
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var list = serializer.Deserialize<List<int>>(selectedItems.Value);
                Response.Redirect(string.Format(EditCallManagersUrl, string.Join(",", list)));
                break;
        }
    }

    #endregion
}
