﻿using System;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Voip_Admin_ManageCallManagers_Default : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        CallManagersControl.AddCallManagersUrl = "/Orion/Voip/Admin/ManageCallManagers/AddCallManagerWizard.aspx";
        CallManagersControl.EditCallManagersUrl = string.Format("/Orion/Nodes/NodeProperties.aspx?Nodes={{0}}&ReturnTo={0}#CallManagerSectionStart",
            UrlHelper.ToSafeUrlParameter(Request.RawUrl));
    }
}
