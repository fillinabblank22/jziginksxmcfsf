﻿<%@ Page Title="<%$ Resources : VNQMWebContent, VNQMWEBDATA_VB1_124 %>" Language="C#" MasterPageFile="~/Orion/Voip/Admin/Config.master" AutoEventWireup="true" CodeFile="AddCallManagerWizard.aspx.cs" Inherits="Orion_Voip_Admin_ManageCallManagers_AddCallManagerWizard" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web"%>

<%@ Register src="~/orion/voip/controls/wizards/CallManager/CallManagerWizardControlContainer.ascx" tagname="CallManagerWizardControlContainer" tagprefix="ipsla" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="contentHead" ContentPlaceHolderID="contentPlaceHolderHead" Runat="Server">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" Module="Voip" File="styles/Admin.css" />
    <orion:Include runat="server" Module="Voip" File="styles/Wizard.css" />
    <orion:Include runat="server" Module="Voip" File="Voip.css" />
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="Breadcrumbs">
    <div class="sw-hdr-breadbox">
        <span><a href="../../../admin"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_6%></a> &gt; <a href="../default.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_VnqmSettingsLinkName%></a> &gt; <a href="/Orion/Voip/Admin/ManageCallManagers/Default.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_63%></a> &gt;</span>
    </div>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="TopRightPageLinks">
     <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionIPSLAManagerAdministratorGuide-AddingCallManagersToVNQM" />
</asp:Content>

<asp:Content ID="contentBody" runat="server" ContentPlaceHolderID="contentPlaceHolderBody" >

<div id="wizardControl">
<style type="text/css">
    .ipsla_GroupBox_ext{ padding-top: 0px !important;}
</style>
<ipsla:CallManagerWizardControlContainer ID="callManagerWizardControlContainer" runat="server"
    ReturnUrl="~/Orion/Voip/Admin/ManageCallManagers/Default.aspx" />
</div>
</asp:Content>