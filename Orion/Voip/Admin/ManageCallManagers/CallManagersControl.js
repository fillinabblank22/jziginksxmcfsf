﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Voip.Admin.ManageCallManagers");

Orion.Voip.Admin.ManageCallManagers.CallManagersControl = function(element) {
    Orion.Voip.Admin.ManageCallManagers.CallManagersControl.initializeBase(this, [element]);
    this.gridHostElementId = null;
    this.selectedItemsElementId = null;
    this.InvokeAddCallManagersPostback = null;
    this.InvokeEditCallManagersPostback = null;
}

Orion.Voip.Admin.ManageCallManagers.CallManagersControl.prototype = {

    AddCallManagers: function () {
        this.InvokeAddCallManagersPostback();
    },

    EditCallManagers: function () {
        if (CheckDemoMode()) return;
        
        var selections = this.callManagersGrid.selModel.getSelections();
        var selectedCMs = [];
        for (i = 0; i < this.callManagersGrid.selModel.getCount(); i++) {
            selectedCMs.push(selections[i].data.NodeID);
        }
        var encoded_array = Ext.encode(selectedCMs);
        $('#' + this.selectedItemsElementId).val(encoded_array);
        this.InvokeEditCallManagersPostback();
    },

    ConfirmRemove: function () {
        if (CheckDemoMode()) return;

        if (this.callManagersGrid.selModel.getCount() == 0) // only one operation is selected here
        {
            Ext.MessageBox.alert('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_1;E=js}', '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_2;E=js}');
        }
        else {
            Ext.MessageBox.confirm('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_3;E=js}', '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_4;E=js}', Function.createDelegate(this, this.removeSelectedCallManagers));
        }
    },

    renderCredentialsName: function (value, metadata, record) {
        if (value)
            return value;

        return '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_23;E=js}';
    },

    renderFtpStatus: function (value, metadata, record) {
        if (value)
            return '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_22;E=js}';

        return '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_23;E=js}';
    },
    
    renderHasCliCreds: function(value, meta, record) {
        if (value)
            return value;

        return '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_23;E=js}';
    },
    renderTimeZone: function (value, meta, record) {
        if (value)
            return value;

        return '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_23;E=js}';
    },
    renderCDRMonitoring: function (value, meta, record) {
        if (value)
            return '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_22;E=js}';

        return '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_23;E=js}';
    },
    renderSipTrunkMonitoring: function (value, meta, record) {
        if (value && value == 'true')
            return '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_22;E=js}';

        return '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_23;E=js}';
    },
    initialize: function () {
        Orion.Voip.Admin.ManageCallManagers.CallManagersControl.callBaseMethod(this, 'initialize');
        $(document).ready(Function.createDelegate(this, function () {
            this.initializeGrid();
        }));
    },

    initializeGrid: function () {
        // Add custom initialization here
        this.dataStore = new Ext.data.JsonStore({
            id: 'DataStore',
            url: '/Orion/Voip/Admin/ManageCallManagers/CallManagersExtGridDataProvider.ashx',
            baseParams: { task: 'select' }, // this parameter is passed for any HTTP request
            root: 'callManagers',
            totalProperty: 'total',
            idProperty: 'id',
            fields: [
                { name: 'ID', type: 'int' },
                { name: 'Name', type: 'string' },
                { name: 'Type', type: 'string' },
                { name: 'Status', type: 'string' },
                { name: 'NodeID', type: 'int' },
                { name: 'FTPCredentialsName', type: 'string' },
                { name: 'AXLCredentialsName', type: 'string' },
                { name: 'CliCredentialsName', type: 'string' },
                { name: 'TimeZone', type: 'string' },
                { name: 'CDRMonitoring', type: 'string' },
                { name: 'SipTrunkMonitoringEnabled', type: 'string' }
            ],
            sortInfo: { field: 'Name', direction: 'ASC' }
        });

        this.selectionModel = new Ext.grid.CheckboxSelectionModel({
            singleSelect: false,
            listeners: {
                'selectionchange': {
                    scope: this,
                    fn: function (p) {
                        // enable/disable edit button based on item selection count
                        var toolbar = this.callManagersGrid.getTopToolbar();
                        if (this.callManagersGrid.selModel.getCount() == 0) {
                            toolbar.items.map.removeCallManagers.disable();
                            toolbar.items.map.editCallManagers.disable();
                        } else {
                            toolbar.items.map.removeCallManagers.enable();
                            toolbar.items.map.editCallManagers.enable();
                        }
                    }
                }
            }
        });

        this.columnModel = new Ext.grid.ColumnModel(
                        [this.selectionModel,
                        {
                            header: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_5;E=js}',
                            dataIndex: 'Name',
                            width: 250
                        }, {
                            header: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_6;E=js}',
                            dataIndex: 'Type',
                            width: 200
                        }, {
                            header: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_7;E=js}',
                            dataIndex: 'Status',
                            width: 100
                        }, {
                            header: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_141;E=js}',
                            dataIndex: 'TimeZone',
                            width: 120,
                            renderer: this.renderTimeZone
                        }, {
                            header: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_8;E=js}',
                            dataIndex: 'FTPCredentialsName',
                            width: 120,
                            renderer: this.renderFtpStatus
                        }, {
                            header: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_9;E=js}',
                            dataIndex: 'AXLCredentialsName',
                            width: 160,
                            renderer: this.renderCredentialsName
                        }, {
                            header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_42;E=js}',
                            dataIndex: 'CliCredentialsName',
                            renderer: this.renderHasCliCreds,
                            width: 120
                        },{
                             header: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_142;E=js}',
                             dataIndex: 'CDRMonitoring',
                             renderer: this.renderCDRMonitoring,
                             width: 100
                        }, {
                            header: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_144;E=js}',
                            dataIndex: 'SipTrunkMonitoringEnabled',
                            renderer: this.renderSipTrunkMonitoring,
                            width: 130
                        }]
                    );
        this.columnModel.defaultSortable = true;

        this.callManagersGrid = new Ext.grid.GridPanel({
            id: 'callManagersGrid',
            height: 300,
            width: 1324,
            store: this.dataStore,
            cm: this.columnModel,
            enableColLock: false,
            selModel: this.selectionModel,
            renderTo: this.gridHostElementId,
            loadMask: true,
            stripeRows: true,
            tbar: [
                    {
                        text: '&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_AK1_10;E=js}',
                        tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_11;E=js}',
                        handler: Function.createDelegate(this, this.AddCallManagers),
                        iconCls: 'ipsla_AddCallManager'
                    },
                    '-',
                    {
                        text: '&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_AK1_12;E=js}',
                        tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_13;E=js}',
                        handler: Function.createDelegate(this, this.EditCallManagers),
                        iconCls: 'ipsla_EditCallManager',
                        id: 'editCallManagers',
                        disabled: true
                    },
                    '-',
                    {
                        text: '&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_AK1_14;E=js}',
                        tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_15;E=js}',
                        handler: Function.createDelegate(this, this.ConfirmRemove),
                        iconCls: 'ipsla_RemoveCallManager',
                        id: 'removeCallManagers',
                        disabled: true
                    }]
        });

        this.dataStore.load();
    },

    reloadGrid: function () {
        this.dataStore.reload();
    },

    removeSelectedCallManagers: function (btn) {
        if (btn == 'yes') {
            var selections = this.callManagersGrid.selModel.getSelections();
            var selectedCMs = [];
            for (i = 0; i < this.callManagersGrid.selModel.getCount(); i++) {
                selectedCMs.push(selections[i].json.ID);
            }
            var encoded_array = Ext.encode(selectedCMs);
            Ext.Ajax.request({
                waitMsg: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_16;E=js}',
                url: '/Orion/Voip/Admin/ManageCallManagers/CallManagersExtGridDataProvider.ashx',
                params: {
                    task: "delete",
                    ids: encoded_array
                },
                success: Function.createDelegate(this, function (response) {
                    var result = eval(response.responseText);
                    this.dataStore.reload();
                    switch (result) {
                        case 1:  // Success
                            break;
                        default:
                            Ext.MessageBox.alert('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_17;E=js}', '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_18;E=js}');
                            break;
                    }
                }),
                failure: function (response) {
                    var result = response.responseText;
                    Ext.MessageBox.alert('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_19;E=js}', '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_20;E=js}');
                }
            });
        }
    },

    dispose: function () {
        //Add custom dispose actions here
        Orion.Voip.Admin.ManageCallManagers.CallManagersControl.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Admin.ManageCallManagers.CallManagersControl.registerClass('Orion.Voip.Admin.ManageCallManagers.CallManagersControl', Sys.UI.Control);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
