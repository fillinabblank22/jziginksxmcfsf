<%@ Page Language="C#" MasterPageFile="~/Orion/Voip/Admin/Config.master" AutoEventWireup="true"
    CodeFile="VoipSettings.aspx.cs" Inherits="Voip_Settings" Title="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_1 %>" %>

<%@ Import Namespace="Resources" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton HelpUrlFragment="OrionVoIPMonitorPHPageSettings" ID="HelpButton1"
        runat="server" />
</asp:Content>
<asp:Content ID="contentHead" ContentPlaceHolderID="contentPlaceHolderHead" runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/styles/Admin.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/Voip.css" />
    <style type="text/css">
        input[type="checkbox"]
        {
            margin: 3px 0px;
            padding: 0px;
            width: 13px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="Breadcrumbs">
    <div class="sw-hdr-breadbox">
        <span><a href="../../admin">
            <%= VNQMWebContent.VNQMWEBDATA_VB1_6 %></a> &gt; <a href="default.aspx">
                <%= VNQMWebContent.VNQMWEBDATA_VnqmSettingsLinkName %></a> &gt;</span>
    </div>
</asp:Content>
<asp:Content ID="contentBody" ContentPlaceHolderID="contentPlaceHolderBody" runat="Server">
    <script type="text/javascript" language="javascript">
        function validatePortNumber(oSrc, args) {
            if (args.Value >= 16384 && args.Value <= 32766 || args.Value >= 49152 && args.Value <= 65534) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }

        function validateCcmCallSucessCodes(oSrc, args) {
            args.IsValid = args.Value.toString().match(/^(([0-9](;)?)*)+$/);
        }

        function validateTimespan(oSrc, args) {
            args.IsValid = args.Value.toString().match(/^[0-9]{1,3}:[0-5][0-9]:[0-5][0-9]$/);
        }

        function validateVoipGatewayThreshold(oSrc, args) {
            var critical = document.getElementById('<%= VoipGatewayCriticalLevel.ClientID %>').value;
            var warning = document.getElementById('<%= VoipGatewayWarningLevel.ClientID %>').value;

            if (parseInt(critical) <= parseInt(warning)) {
                args.IsValid = false;
            } else {
                args.IsValid = true;
            }
        }
    </script>
    <style type="text/css">
        .VoipSettingsContent
        {
            padding: 10px 20px 10px 20px;
        }
        p.SectionTitle
        {
            font-weight: bold;
            padding-top: 20px;
        }
        .PropertyHeader
        {
            width: 250px;
        }
        .Property
        {
            width: 300px;
        }
        .PropertyDescription
        {
            width: 600px;
        }
        .PropertyText
        {
            width: auto;
        }
          body{
            background:#ffffff;
        }
        h2.CustomTableHeader {
            font-size:11pt;
        }
    </style>
    <div id="ipsla_Admin">
        <h1>
            <%=Title %></h1>
        <div class="VoipSettingsContent">
            <table id="adminContentTable">
                <tr>
                    <td>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                        <h2 style="font-size: 11pt;">
                            <% =VNQMWebContent.VNQMWEBDATA_TM0_109 %>
                            </h2>
                        <table class="NeedsZebraStripes">
                            <tr>
                                <td class="PropertyHeader">
                                    <% =VNQMWebContent.VNQMWEBDATA_TM0_3 %>
                                </td>
                                <td class="Property">
                                    <asp:TextBox ID="UdpJitterPort" runat="server" Width="200px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="UdpJitterPort"
                                        ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_4 %>">*</asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="UdpJitterPort"
                                        ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_5 %>" ClientValidationFunction="validatePortNumber">*
                                    </asp:CustomValidator>
                                </td>
                                <td class="Property PropertyDescription">
                                    <%= VNQMWebContent.VNQMWEBDATA_TM0_6 %>
                                </td>
                            </tr>
                            <tr>
                                <td class="PropertyHeader">
                                    <%= VNQMWebContent.VNQMWEBDATA_TM0_7 %>
                                </td>
                                <td class="Property">
                                    <asp:DropDownList ID="JitterCodec" runat="server" Width="204px" />
                                </td>
                                <td class="Property PropertyDescription">
                                    <%= VNQMWebContent.VNQMWEBDATA_TM0_8 %>
                                </td>
                            </tr>
                            <tr>
                                <td class="PropertyHeader">
                                    <%= VNQMWebContent.VNQMWEBDATA_TM0_13 %>
                                </td>
                                <td class="Property" style="white-space: nowrap">
                                    <asp:TextBox ID="RetentionDays" runat="server" Width="200px" MaxLength="5"></asp:TextBox>
                                    <%= VNQMWebContent.VNQMWEBDATA_TM0_36 %>
                                    <asp:CompareValidator ID="CompareValidator4" runat="server" ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_14 %>"
                                        ControlToValidate="RetentionDays" Operator="GreaterThanEqual" Type="Integer"
                                        ValueToCompare="1">*</asp:CompareValidator><asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                            runat="server" ControlToValidate="RetentionDays" ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_15 %>">*</asp:RequiredFieldValidator>
                                </td>
                                <td class="Property PropertyDescription">
                                    <%= VNQMWebContent.VNQMWEBDATA_TM0_16 %>
                                </td>
                            </tr>
                            <tr>
                                <td class="PropertyHeader">
                                    <%= VNQMWebContent.VNQMWEBDATA_TM0_17 %>
                                </td>
                                <td class="Property">
                                    <asp:TextBox ID="MOSAdvantageFactor" runat="server" Width="200px"></asp:TextBox>&nbsp;
                                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="MOSAdvantageFactor"
                                        ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_18 %>" MaximumValue="20"
                                        MinimumValue="0" Type="Integer">*</asp:RangeValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="MOSAdvantageFactor"
                                        ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_19 %>">*</asp:RequiredFieldValidator>
                                </td>
                                <td class="Property PropertyDescription">
                                    <%= VNQMWebContent.VNQMWEBDATA_TM0_20 %>
                                </td>
                            </tr>
                            <tr>
                                <td class="PropertyHeader">
                                    <% =VNQMWebContent.VNQMWEBDATA_TM0_21 %>
                                </td>
                                <td class="Property">
                                    <asp:TextBox ID="TypeOfService" runat="server" Width="200px"></asp:TextBox>&nbsp;
                                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="TypeOfService"
                                        ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_22 %>" MaximumValue="255"
                                        MinimumValue="0" Type="Integer">*</asp:RangeValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TypeOfService"
                                        ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_23 %>">*</asp:RequiredFieldValidator>
                                </td>
                                <td class="Property PropertyDescription">
                                    <% =VNQMWebContent.VNQMWEBDATA_TM0_24 %>
                                </td>
                            </tr>
                            <tr>
                                <td class="PropertyHeader">
                                    <% =VNQMWebContent.VNQMWEBDATA_TM0_25 %>
                                </td>
                                <td class="Property">
                                    <div style="float: left; width: 60px;">
                                        <asp:CheckBox ID="OperationsInRunningConfig" runat="server"></asp:CheckBox>
                                    </div>
                                </td>
                                <td class="Property PropertyDescription">
                                    <%= VNQMWebContent.VNQMWEBDATA_TM0_26 %>
                                </td>
                            </tr>
                        </table>
                        <br />
                         <h2 style="font-size: 11pt;">
                            <% =VNQMWebContent.VNQMWEBDATA_TM0_110 %></h2>
                        <table class="NeedsZebraStripes">
                            <tr>
                                <td class="PropertyHeader">
                                    <%= VNQMWebContent.VNQMWEBDATA_TM0_9 %>
                                </td>
                                <td class="Property" style="white-space: nowrap">
                                    <asp:TextBox ID="TestInterval" runat="server" Width="200px" MaxLength="6"></asp:TextBox>
                                    <%= VNQMWebContent.VNQMWEBDATA_TM0_10 %>
                                    <asp:CompareValidator ID="CompareValidator3" runat="server" ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_11 %>"
                                        Operator="GreaterThanEqual" Type="Integer" ValueToCompare="1" ControlToValidate="TestInterval">*</asp:CompareValidator><asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator3" runat="server" ControlToValidate="TestInterval"
                                            ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_35 %>">*</asp:RequiredFieldValidator>
                                </td>
                                <td class="Property PropertyDescription">
                                    <%= VNQMWebContent.VNQMWEBDATA_TM0_12 %>
                                </td>
                            </tr>
                            <tr>
                                <td class="PropertyHeader">
                                    <%= VNQMWebContent.VNQMWEBDATA_TM0_27 %>
                                </td>
                                <td class="Property">
                                    <asp:TextBox ID="CcmCallSucessCodes" runat="server" Width="200px"></asp:TextBox>&nbsp;
                                    <asp:RequiredFieldValidator ID="CcmCallSucessCodesRequiredFieldValidator" runat="server"
                                        ControlToValidate="CcmCallSucessCodes" ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_28 %>">*</asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="CcmCallSucessCodesCustomValidator" runat="server" ControlToValidate="CcmCallSucessCodes"
                                        ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_29 %>" ClientValidationFunction="validateCcmCallSucessCodes">*
                                    </asp:CustomValidator>
                                </td>
                                <td class="Property PropertyDescription">
                                    <%= VNQMWebContent.VNQMWEBDATA_TM0_30 %>
                                </td>
                            </tr>
                            <tr>
                                <td class="PropertyHeader">
                                    <%= VNQMWebContent.VNQMWEBDATA_TM0_31 %>
                                </td>
                                <td class="Property" style="white-space: nowrap">
                                    <asp:TextBox ID="CdrCmrJobTimeOut" runat="server" Width="200px" MaxLength="10"></asp:TextBox>
                                    <%= VNQMWebContent.VNQMWEBDATA_TM0_37 %>
                                    <asp:RequiredFieldValidator ID="CdrCmrJobTimeOutRequiredFieldValidator" runat="server"
                                        ControlToValidate="CdrCmrJobTimeOut" ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_32 %>">*</asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="CdrCmrJobTimeOutCustomValidator" runat="server" ControlToValidate="CdrCmrJobTimeOut"
                                        ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_33 %>" ClientValidationFunction="validateTimespan">*
                                    </asp:CustomValidator>
                                </td>
                                <td class="Property PropertyDescription">
                                    <%= VNQMWebContent.VNQMWEBDATA_TM0_34 %>
                                </td>
                            </tr>
                            <tr>
                                <td class="PropertyHeader">
                                    <%= VNQMWebContent.VNQMWEBCODE_SS_18 %>
                                </td>
                                <td class="Property">
                                    <div style="float: left; width: 60px;">
                                        <asp:CheckBox ID="chbEnableAvayaDetection" runat="server"></asp:CheckBox>
                                    </div>
                                </td>
                                <td class="Property PropertyDescription">
                                    <%= VNQMWebContent.VNQMWEBCODE_SS_19 %>
                                </td>
                            </tr>
                            <tr>
                                <td class="PropertyHeader">
                                    <%= VNQMWebContent.VNQMWEBDATA_CcmDisableFtpCompression %>
                                </td>
                                <td class="Property">
                                    <div style="float: left; width: 60px;">
                                        <asp:CheckBox ID="disableFtpCompressionCheckBox" runat="server"></asp:CheckBox>
                                    </div>
                                </td>
                                <td class="Property PropertyDescription">
                                    <%= VNQMWebContent.VNQMWEBDATA_CcmDisableFtpCompressionDescription %>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <h2 style="font-size: 11pt;">
                            <% =VNQMWebContent.VNQMWEBDATA_TM0_112 %>
                        </h2>
                        <table class="NeedsZebraStripes">
                            <tr>
                                <td class="PropertyHeader">
                                    <%= VNQMWebContent.VNQMWEBDATA_PG_11 %>
                                </td>
                                <td class="Property" style="white-space: nowrap">
                                    <asp:TextBox ID="VoipGatewayPollingIntervalMinutes" runat="server" Width="200px"
                                        MaxLength="6"></asp:TextBox>
                                    <%= VNQMWebContent.VNQMWEBDATA_PG_12 %>
                                    <asp:CompareValidator ID="VoipGatewayPollingIntervalMinutesCompareValidator" runat="server"
                                        ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_PG_13 %>" Operator="GreaterThanEqual"
                                        Type="Integer" ValueToCompare="1" ControlToValidate="VoipGatewayPollingIntervalMinutes">*</asp:CompareValidator>
                                    <asp:RequiredFieldValidator ID="VoipGatewayPollingIntervalMinutesRequiredFieldValidator"
                                        runat="server" ControlToValidate="VoipGatewayPollingIntervalMinutes" ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_PG_14 %>">*</asp:RequiredFieldValidator>
                                </td>
                                <td class="Property PropertyDescription">
                                    <%= VNQMWebContent.VNQMWEBDATA_PG_15 %>
                                </td>
                            </tr>
                            <tr>
                                <td class="PropertyHeader">
                                    <%= VNQMWebContent.VNQMWEBDATA_PG_1 %>
                                </td>
                                <td class="Property" style="white-space: nowrap">
                                    <asp:TextBox ID="VoipGatewayCriticalLevel" runat="server" Width="200px" MaxLength="10"></asp:TextBox>
                                    <%= VNQMWebContent.VNQMWEBDATA_PG_16 %>
                                    <asp:RequiredFieldValidator ID="VoipGatewayCriticalLevelRequiredFieldValidator" runat="server"
                                        ControlToValidate="VoipGatewayCriticalLevel" ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_PG_5 %>">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="VoipGatewayCriticalLevelCompareValidator" runat="server"
                                        ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_PG_8 %>" ControlToValidate="VoipGatewayCriticalLevel"
                                        Operator="GreaterThanEqual" Type="Integer" ValueToCompare="0">*</asp:CompareValidator>
                                    <asp:CustomValidator ID="VoipGatewayThresholdCustomValidator1" runat="server" ControlToValidate="VoipGatewayCriticalLevel"
                                        ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_PG_7 %>" ClientValidationFunction="validateVoipGatewayThreshold">*
                                    </asp:CustomValidator>
                                </td>
                                <td class="Property PropertyDescription">
                                    <%= VNQMWebContent.VNQMWEBDATA_PG_3 %>
                                </td>
                            </tr>
                            <tr>
                                <td class="PropertyHeader">
                                    <%= VNQMWebContent.VNQMWEBDATA_PG_2%>
                                </td>
                                <td class="Property" style="white-space: nowrap">
                                    <asp:TextBox ID="VoipGatewayWarningLevel" runat="server" Width="200px" MaxLength="10"></asp:TextBox>
                                    <%= VNQMWebContent.VNQMWEBDATA_PG_16 %>
                                    <asp:RequiredFieldValidator ID="VoipGatewayWarningLevelRequiredFieldValidator" runat="server"
                                        ControlToValidate="VoipGatewayWarningLevel" ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_PG_6 %>">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="VoipGatewayWarningLevelCompareValidator" runat="server"
                                        ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_PG_9 %>" ControlToValidate="VoipGatewayWarningLevel"
                                        Operator="GreaterThanEqual" Type="Integer" ValueToCompare="0">*</asp:CompareValidator>
                                    <asp:CustomValidator ID="VoipGatewayThresholdCustomValidator2" runat="server" ControlToValidate="VoipGatewayWarningLevel"
                                        ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_PG_10 %>" ClientValidationFunction="validateVoipGatewayThreshold">*
                                    </asp:CustomValidator>
                                </td>
                                <td class="Property PropertyDescription">
                                    <%= VNQMWebContent.VNQMWEBDATA_PG_4 %>
                                </td>
                            </tr>
                            <tr>
                                <td class="PropertyHeader">
                                    <%= VNQMWebContent.VNQMWEBDATA_RKG_6 %>
                                </td>
                                <td class="Property" style="white-space: nowrap">
                                    <asp:TextBox ID="VoipGatewayRetentionDays" runat="server" Width="200px" MaxLength="5"></asp:TextBox>
                                    <%= VNQMWebContent.VNQMWEBDATA_RKG_7 %>
                                    <asp:CompareValidator ID="CompareValidatorVoipGatewayRetentionDays" runat="server"
                                        ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_RKG_8 %>" ControlToValidate="VoipGatewayRetentionDays"
                                        Operator="GreaterThanEqual" Type="Integer" ValueToCompare="1">*</asp:CompareValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorVoipGatewayRetentionDays" runat="server"
                                        ControlToValidate="VoipGatewayRetentionDays" ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_RKG_9 %>">*</asp:RequiredFieldValidator>
                                </td>
                                <td class="Property PropertyDescription">
                                    <%= VNQMWebContent.VNQMWEBDATA_RKG_10 %>
                                </td>
                            </tr>
                            <tr>
                                <td class="PropertyHeader">
                                    <%= VNQMWebContent.VNQMWEBDATA_GatewayMaxConcurrentSipCalls%>
                                </td>
                                <td class="Property" style="white-space: nowrap">
                                    <asp:TextBox ID="VoipGatewayMaxConcurrentSipCalls" runat="server" Width="200px" MaxLength="5"></asp:TextBox>
                                    <asp:CompareValidator ID="CompareValidatorVoipGatewayDefaultMaxConcurrentSipCalls" runat="server"
                                                          ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_213 %>"
                                                          ControlToValidate="VoipGatewayMaxConcurrentSipCalls"
                                                          Operator="GreaterThanEqual" Type="Integer" ValueToCompare="1" >*</asp:CompareValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorVoipGatewayDefaultMaxConcurrentSipCalls" runat="server"
                                                                ControlToValidate="VoipGatewayMaxConcurrentSipCalls"
                                                                ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBCODE_VB1_112 %>">*</asp:RequiredFieldValidator>
                                </td>
                                <td class="Property PropertyDescription">
                                    <%= VNQMWebContent.VNQMWEBDATA_GatewayMaxConcurrentSipCallsDescription %>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
            <orion:LocalizableButton ID="Default" runat="server" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_2 %>"
                DisplayType="Small" CausesValidation="false" OnClientClick="if (CheckDemoMode()) return false;"
                OnClick="Default_Click" />
            <br />
            <div class="sw-btn-bar">
                <orion:LocalizableButton ID="OkButtton" runat="server" LocalizedText="Save" OnClick="Ok_Click"
                    OnClientClick="if (CheckDemoMode()) return false;" DisplayType="Primary" />
                <orion:LocalizableButton ID="CancelButton" runat="server" LocalizedText="Cancel"
                    OnClick="Cancel_Click" DisplayType="Secondary" />
            </div>
        </div>
    </div>
</asp:Content>
