﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.IpSla.Data.OperationParameters;
using SolarWinds.Orion.IpSla.Data;
using System.Web.Script.Serialization;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;

public partial class Orion_Voip_Admin_ManageOperations : System.Web.UI.Page
{
    protected readonly string addCallManager = VNQMWebContent.VNQMWEBDATA_TM0_51;
    protected readonly string addCallManagerText = String.Format(VNQMWebContent.VNQMWEBDATA_TM0_50, 
                               "<a class='sw-link' href='/Orion/Voip/Admin/ManageCallManagers/AddCallManagerWizard.aspx'>", "</a>");

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (var dataTable = CallManagersDAL.Instance.GetAllCallManagerRecords(CallManagerType.CCM, CallManagerType.ACM))
            {
                CallManagersCount.Value = dataTable.Rows.Count.ToString();
            }
        }
    }

    protected void EditorLocationLoadButton_Click(object sender, EventArgs e)
    {
        this.DisplayEditLocation(true);

        int operationID;

        if (!int.TryParse(this.SelectedOperationID.Value, out operationID))
            return;

        int? sourceLocationId = null;
        int? targetLocationId = null;
        int? targetNodeId = null;
        using (var blProxy = BusinessLayer.GetBusinessLayer())
        {
            sourceLocationId = blProxy.GetSourceLocationID(operationID);
            targetNodeId = blProxy.GetTargetNodeID(operationID);
            if (targetNodeId != null)
            {
                NullTargetNode.Value = "False";
                targetLocationId = blProxy.GetTargetLocationID(operationID);
            }
            else
            {
                NullTargetNode.Value = "True";
            }
        }
        this.editsla.Initialize(targetNodeId, sourceLocationId, targetLocationId);
    }

    protected void EditorLocationCancelButton_Click(object sender, EventArgs e)
    {
        this.DisplayEditLocation(false);
    }

    protected void EditorLocationSubmitButton_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
            return;

        this.DisplayEditLocation(false);
        int operationID;

        if (!int.TryParse(this.SelectedOperationID.Value, out operationID))
            return;

        using (var blProxy = BusinessLayer.GetBusinessLayer())
        {
            blProxy.UpdateSourceLocationID(operationID, editsla.GetSourceLocation());
            if (NullTargetNode.Value == "False")
            {
                blProxy.UpdateTargetLocationID(operationID, editsla.GetTargetLocation());
            }
        }

        ThresholdManager.ClearThresholdCacheForOperation(operationID);
    }

    protected void EditorLoadButton_Click(object sender, EventArgs e)
    {
        this.DisplayEditor(true);

        string operationIDs = this.SelectedOperationIDs.Value;
        int[] ids = (new JavaScriptSerializer()).Deserialize<int[]>(operationIDs);
        List<OperationInstance> operationInstances;
        using (var blProxy = BusinessLayer.GetBusinessLayer())
        {
            operationInstances = blProxy.GetOperationInstances(ids);
        }
        OperationDefinitionCollection operations =
            new OperationDefinitionCollection(operationInstances.Select(oi => oi.Definition));
        this.editOp.Initialize(operations, false);
    }

    protected void EditorCancelButton_Click(object sender, EventArgs e)
    {
        this.DisplayEditor(false);
    }

    protected void EditorSubmitButton_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
            return;

        this.DisplayEditor(false);
        string operationIDs = this.SelectedOperationIDs.Value;
        int[] ids = (new JavaScriptSerializer()).Deserialize<int[]>(operationIDs);
        using (var blProxy = BusinessLayer.GetBusinessLayer())
        {
            var operationInstances = blProxy.GetOperationInstances(ids);
            var operations = new OperationDefinitionCollection(operationInstances.Select(oi => oi.Definition));
            this.editOp.Initialize(operations, true);
            this.editOp.Modify(operations);
            blProxy.UpdateOperationInstances(operationInstances);
            foreach (var instance in operationInstances) ThresholdManager.ClearThresholdCacheForOperation(instance.ID);
        }
    }

    private void DisplayEditor(bool show)
    {
        this.editDialogPanel.Visible = show;
        this.editDialogLoading.Visible = !show;
    }

    private void DisplayEditLocation(bool show)
    {
        this.editLocationPanel.Visible = show;
        this.editLocationLoading.Visible = !show;
    }
}
