﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProgressControl.ascx.cs" Inherits="Orion_Voip_Admin_ProgressControl" %>

<style type="text/css">
    .ipsla_ProgressBox{ width: 475px !important; }
</style>

<div style="padding: 5px 5px 5px 5px;">
    <table border="0" cellpadding="0" cellspacing="10px" width="100%">
        <tr>
            <td id="titleTd" runat="server" style="text-align: left;" ><%= Title %></td>
            <td id="progressBarTd" runat="server" style="padding-right: 5px"><div id="progressBarContent" runat="server" class="ipsla_ProgressBarDiscovery"/></td>
        </tr>
        <tr>
            <td id="gifTd" runat="server" style="text-align: right; vertical-align: middle; width: 90px;"><img src="/Orion/Images/throbber.gif" /></td>
            <td id="labelsTd" runat="server" style="vertical-align: middle;">
                <table  border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td><div class="discovered-items"/></td>
                        <td id="processedContent" runat="server" style="white-space: nowrap; padding-right: 10px"></td>
                    </tr>
                    <tr>
                        <td><img src="/Orion/Voip/images/ProgressBar/legend_gray.gif" alt="" /></td>
                        <td id="totalContent" runat="server" style="white-space: nowrap; padding-right: 10px"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
