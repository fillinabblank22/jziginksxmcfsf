using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Orion.IpSla.Common.Settings;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Data;
using Infragistics.WebUI.UltraWebNavigator;
using Nav = Infragistics.WebUI.UltraWebNavigator;

public partial class Voip_Infrastructure : System.Web.UI.Page
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        ReferrerRedirectorBase.Initialize(this.ViewState, IsPostBack);

        if (!IsPostBack)
        {
            LoadNodeInterfaceTree();
        }
    }

    private void LoadNodeInterfaceTree()
    {
        VendorNodeInterfaceTree.DataSource = VoipInfrastructure.GetVendorNodeInterfaceTree();
        VendorNodeInterfaceTree.DataBind();
    }

    protected void Select_Click(object sender, EventArgs e)
    {
        List<NetObject> nodes = new List<NetObject>(NetObjectFactory.GetAllObjects(typeof(SolarWinds.Orion.NPM.Web.Node)));
        foreach (NetObject o in nodes)
        {
            if (o is SolarWinds.Orion.NPM.Web.Node)
            {
                SolarWinds.Orion.NPM.Web.Node n = o as SolarWinds.Orion.NPM.Web.Node;
                VoipInfrastructure.AddNode(n.NodeID);
            }
        }
        if (IpSlaSettings.AreInterfacesSupported)
        {
            List<NetObject> interfaces =
                new List<NetObject>(NetObjectFactory.GetAllObjects(typeof (SolarWinds.Orion.NPM.Web.Interface)));
            foreach (NetObject o in interfaces)
            {
                if (o is SolarWinds.Orion.NPM.Web.Interface)
                {
                    SolarWinds.Orion.NPM.Web.Interface i = o as SolarWinds.Orion.NPM.Web.Interface;
                    VoipInfrastructure.AddInterface(i.InterfaceID);
                }
            }
        }
        LoadNodeInterfaceTree();
    }

    protected void DeSelect_Click(object sender, EventArgs e)
    {
        List<int> nodes = VoipInfrastructure.GetAllNodes();
        foreach (int currentNode in nodes)
        {
            VoipInfrastructure.RemoveNode(currentNode);
        }
        if (IpSlaSettings.AreInterfacesSupported)
        {
            List<int> interfaces = VoipInfrastructure.GetAllInterfaces();
            foreach (int currentInterface in interfaces)
            {
                VoipInfrastructure.RemoveInterface(currentInterface);
            }
        }
        LoadNodeInterfaceTree();
    }

    protected void ButtonBack_Click(object sender, EventArgs e)
    {
        ReferrerRedirectorBase.Return("Default.aspx");
    }

    protected void ButtonFinish_Click(object sender, EventArgs e)
    {
        if (ProfileHelper.DemoRestriction)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Voip_Infrastructure), "DemoModeMessage", "DemoModeMessage();", true);
            return;
        }
        ProcessNodeList(VendorNodeInterfaceTree.Nodes);
        ReferrerRedirectorBase.Return("Default.aspx");
    }

    private void ProcessNodeList(Nav.Nodes nodes)
    {
        foreach (Nav.Node node in nodes)
        {
            object key = node.DataKey;
            if (key is int)
            {
                // then it's a node id
                if (!node.Checked)
                    VoipInfrastructure.RemoveNode((int) key);
                else
                    VoipInfrastructure.AddNode((int) key);
            }
            else if (key is object[] && IpSlaSettings.AreInterfacesSupported)
            {
                object[] keys = (object[]) key;
                // keys[1] is an interface id
                if (!node.Checked)
                    VoipInfrastructure.RemoveInterface((int) keys[1]);
                else
                    VoipInfrastructure.AddInterface((int) keys[1]);
            }

            ProcessNodeList(node.Nodes);
        }
    }
}
