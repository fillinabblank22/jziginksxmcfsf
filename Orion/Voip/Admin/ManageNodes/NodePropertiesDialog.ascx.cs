using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_Admin_ManageNodes_NodePropertiesDialog : ScriptUserControlBase
{
	private static readonly Log log = new Log();

    protected override void RenderWrapperElementBegin(HtmlTextWriter writer)
    {
        writer.AddAttribute(HtmlTextWriterAttribute.Id, this.ClientID);
        writer.AddAttribute(HtmlTextWriterAttribute.Style, "display: none;");
        writer.RenderBeginTag(HtmlTextWriterTag.Div);
    }

    #region IScriptControl Members

    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        var descriptor = new ScriptControlDescriptor("Orion.Voip.Admin.ManageNodes.NodePropertiesDialog", this.ClientID);
        descriptor.AddElementProperty("editNodeBox", this.editNodeBox.ClientID);
        descriptor.AddComponentProperty("nodeProperties", this.nodeProperties.ClientID);
        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        yield return new ScriptReference(this.ResolveUrl("NodePropertiesDialog.js"));
    }

    #endregion
}
