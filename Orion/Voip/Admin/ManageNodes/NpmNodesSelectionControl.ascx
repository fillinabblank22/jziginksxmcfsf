﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NpmNodesSelectionControl.ascx.cs" Inherits="Orion_Voip_Admin_Settings_NpmNodesSelectionControl" %>

<%@ Register src="~/Orion/Voip/Admin/NpmNodesSelectionTreeControl.ascx" tagname="NpmNodesSelectionTreeControl" tagprefix="ipsla" %>

<%@ Register src="../../Controls/NodeGroupingSelector.ascx" tagname="NodeGroupingSelector" tagprefix="ipsla" %>

<div class="ipsla_AjaxTreeGroupBy">
<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_68 %><br />
<ipsla:NodeGroupingSelector ID="groupBySelector" runat="server" OnSelectedValueChanged="groupBySelector_SelectedValueChanged" />
</div>

<div class="ipsla_AjaxTreeContent x-panel" id="ajaxTreeContent" runat="server">
    <div class="x-panel-tbar" style="height:30px;"></div>
    <div class="x-panel-body">
        <ipsla:NpmNodesSelectionTreeControl ID="NpmNodesSelectionTreeControl1" runat="server">
            <WebService Path="~/Orion/Voip/Admin/NpmNodesSelectionAjaxTreeService.asmx" />
        </ipsla:NpmNodesSelectionTreeControl>
    </div>
</div>