﻿/// <reference name="MicrosoftAjax.js"/>
/// <reference path="~/Orion/js/jquery.js" />
/// <reference path="~/Orion/js/extjs/debug/Ext.js" />
/// <reference path="~/Orion/js/extjs/debug/jquery-bridge.js" />
/// <reference path="~/Orion/js/extjs/debug/ext-all-debug.js" />
/// <reference path="~/Orion/Voip/js/ScriptServiceInvoker.js" />
/// <reference path="~/Orion/Voip/js/ScriptServiceProxy.js" />
/// <reference path="~/Orion/Voip/js/FormMenu.js" />
/// <reference path="AddNodesProgressPopup.js" />
/// <reference path="WriteCredentialsErrorDialog.js" />

Type.registerNamespace("Orion.Voip.Admin.ManageNodes");
Orion.Voip.Admin.ManageNodes.AddNodesProcessControl = function(element) {
    Orion.Voip.Admin.ManageNodes.AddNodesProcessControl.initializeBase(this, [element]);

    this.invokeClosePostback = null;
    this.invokeShowResultsPostback = null;
    this.invokeSaveAndClosePostback = null;
    this.invokeSaveAndShowResultsPostback = null;

    this.showWriteCredentialsErrors = false;
    this.showResults = false;

    this.nodesAddedTagId = null;
    this.manualNodesAddedTagId = null;
    this.nodesFailedTagId = null;

    this.progressPopup = null;
    this.writeCredentialsErrorDialog = null;
    this.usageContext = 0;
    this.snmpInfo = null;
    this.snmpInfoElement = null;

    this.progressFinishedCallback = null;
    this.progressControl = null;
}

Orion.Voip.Admin.ManageNodes.AddNodesProcessControl.prototype = {

    get_usageContext: function(value) {
        return this.usageContext;
    },
    set_usageContext: function(value) {
        this.usageContext = value;
    },

    get_snmpInfo: function(value) {
        return this.snmpInfo;
    },
    set_snmpInfo: function(value) {
        this.snmpInfo = value;
    },

    get_snmpInfoElement: function(value) {
        return this.snmpInfoElement;
    },
    set_snmpInfoElement: function(value) {
        this.snmpInfoElement = value;
    },

    get_progressControl: function(value) {
        return this.progressControl;
    },
    set_progressControl: function(value) {
        this.progressControl = value;
    },

    Start: function() {
    },

    ShowProgress: function() {
        if (this.progressPopup == null) {
            this.progressPopup = $create(Orion.Voip.Admin.ManageNodes.AddNodesProgressPopup, {
                usageContext: this.usageContext,
                progressControl: this.progressControl
            });
        }
        this.progressPopup.ShowProgress(Function.createDelegate(this, this.OnProgressFinished));
    },

    OnProgressFinished: function(cancel, credentialsIssues) {
        if (cancel) {
            var title = (this.usageContext == 0 ? '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_48;E=js}' : '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_49;E=js}');
            var message = (this.usageContext == 0 ? '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_50;E=js}' : '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_51;E=js}');
            Ext.MessageBox.alert(title, message);
        }
        else {
            if (this.showWriteCredentialsErrors && credentialsIssues) {
                this.ShowWriteCredentialsErrorDialog();
            }
            else {
                this.SaveAndShowResultsIfRequired();
            }
        }
    },

    ShowWriteCredentialsErrorDialog: function() {
        this.writeCredentialsErrorDialog = $create(Orion.Voip.Admin.ManageNodes.WriteCredentialsErrorDialog, {
            snmpInfo: this.snmpInfo,
            snmpInfoElement: this.snmpInfoElement,
            WindowTitle: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_52;E=js}',
            Message: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_53;E=js}',
            Buttons: [{
                id: 'btnAddNodes',
                text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_54;E=js}',
                handler: Function.createDelegate(this, this.OnAddSelectedNodes),
                enableWhenSelection: true
            }, {
                id: 'btnCancel',
                text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_26;E=js}',
                handler: Function.createDelegate(this, this.OnCancel)
            }],
            StoreScriptServiceMethod: SolarWinds.Orion.IpSla.Web.Admin.Settings.AddNodesProcessService.GetResultsToModify,
            StoreScriptServiceMethodParams: { usageContext: this.usageContext },
            OnGridUpdateRequest: Function.createDelegate(this, function(nodeIds) {
                var succeedHandler = Function.createDelegate(this, function(resultContainer, callerData) {
                    resultContainer.methodParams[2] = false;
                    this.writeCredentialsErrorDialog.RefreshGridItems(resultContainer.result.ResponseData);
                });

                var serviceInvoker = $create(Orion.Voip.js.ScriptServiceInvoker);
                serviceInvoker = new Orion.Voip.js.ScriptServiceInvoker();
                serviceInvoker.callService(SolarWinds.Orion.IpSla.Web.Admin.Settings.AddNodesProcessService.UpdateIpSlaCapabilities,
                    [this.usageContext, nodeIds, true], { invoker: serviceInvoker }, 1000, this,
                    succeedHandler,
                    succeedHandler,
                    function(error, callerData) { callerData.cancel(); }
                );
            })
        });
        this.writeCredentialsErrorDialog.ShowDialog();
    },

    OnAddSelectedNodes: function() {
        var selectedNodes = this.writeCredentialsErrorDialog.GetSelectedNodeIds();
        SolarWinds.Orion.IpSla.Web.Admin.Settings.AddNodesProcessService.SaveIncludeNodes(
            this.usageContext, selectedNodes,
            Function.createDelegate(this, function(result, context) {
                this.writeCredentialsErrorDialog.CloseDialog();
                this.ShowResultsIfRequired();
            }),
            Function.createDelegate(this, function(error, context) {
                //todo
            })
        );
    },

    OnCancel: function() {
        SolarWinds.Orion.IpSla.Web.Admin.Settings.AddNodesProcessService.SaveIncludeNodes(
            this.usageContext, [],
            Function.createDelegate(this, function(result, context) {
                this.writeCredentialsErrorDialog.CloseDialog();
                this.ShowResultsIfRequired();
            }),
            Function.createDelegate(this, function(error, context) {
                //todo
            })
        );
    },

    ShowResultsIfRequired: function() {
        if (this.showResults) {
            this.invokeShowResultsPostback();
        }
        else {
            this.invokeClosePostback();
        }
    },

    SaveAndShowResultsIfRequired: function() {
        if (this.showResults) {
            this.invokeSaveAndShowResultsPostback();
        }
        else {
            this.invokeSaveAndClosePostback();
        }
    },

    //"Results" dialog
    ShowResultsDialog: function(successCount, manualCount, failedCount, showSimpleDialog) {
        if (showSimpleDialog != 0 && failedCount == 0) {
            Ext.MessageBox.show({
                title: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_55;E=js}',
                msg: String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_56;E=js}', successCount, failedCount),
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO,
                fn: Function.createDelegate(this, function(btn, value) {
                    this.invokeClosePostback();
                })
            });
        }
        else if (successCount == 0 && failedCount == 0 && manualCount == 0) {
            Ext.MessageBox.show({
                title: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_57;E=js}',
                msg: "@{R=VNQM.Strings;K=VNQMWEBJS_AK1_58;E=js}",
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO,
                fn: Function.createDelegate(this, function(btn, value) {
                    this.invokeClosePostback();
                })
            });
        }
        else {
            panelCount = (successCount == 0 ? 0 : 1) + (manualCount == 0 ? 0 : 1) + (failedCount == 0 ? 0 : 1);
            panelAnchor = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_59;E=js}', (100 / panelCount));
            this.addedNodesDialog = new Ext.Window({
                title: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_60;E=js}',
                layout: 'fit',
                width: 520,
                height: 500,
                minWidth: 400,
                minHeight: 300,
                closeAction: 'close',
                closable: false,
                modal: true,
                layout: 'anchor',
                items: [{
                    title: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_61;E=js}',
                    html: $get(this.nodesAddedTagId).innerHTML,
                    anchor: panelAnchor,
                    autoScroll: true,
                    hidden: (successCount == 0)
                }, {
                    title: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_62;E=js}',
                    html: $get(this.manualNodesAddedTagId).innerHTML,
                    anchor: panelAnchor,
                    autoScroll: true,
                    hidden: (manualCount == 0)
                }, {
                    title: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_57;E=js}',
                    html: $get(this.nodesFailedTagId).innerHTML,
                    anchor: panelAnchor,
                    autoScroll: true,
                    hidden: (failedCount == 0)
}],
                    buttons: [{
                        text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_63;E=js}',
                        handler: Function.createDelegate(this, function() {
                            this.addedNodesDialog.close();
                        })
}],
                        listeners: {
                            'close': {
                                scope: this,
                                fn: function(p) {
                                    this.invokeClosePostback();
                                }
                            }
                        }
                    });
                    this.addedNodesDialog.show(this);
                }
            },

            initialize: function() {
                Orion.Voip.Admin.ManageNodes.AddNodesProcessControl.callBaseMethod(this, 'initialize');
            },

            dispose: function() {
                //Add custom dispose actions here
                Orion.Voip.Admin.ManageNodes.AddNodesProcessControl.callBaseMethod(this, 'dispose');
            }
        }
Orion.Voip.Admin.ManageNodes.AddNodesProcessControl.registerClass('Orion.Voip.Admin.ManageNodes.AddNodesProcessControl', Sys.UI.Control);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
