<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodePropertiesDialog.ascx.cs" Inherits="Orion_Voip_Admin_ManageNodes_NodePropertiesDialog" %>
<%@ Register TagPrefix="ipsla" TagName="NodePropertiesControl" Src="~/Orion/Voip/Admin/ManageNodes/NodePropertiesControl.ascx" %>

<asp:ScriptManagerProxy ID="scriptManager" runat="server">
    <Services>
        <asp:ServiceReference Path="~/Orion/Voip/Services/NodePropertiesService.asmx" />
    </Services>
</asp:ScriptManagerProxy>

<div id="editNodeBox" runat="server">
    <ipsla:NodePropertiesControl ID="nodeProperties" runat="server" />
</div>
