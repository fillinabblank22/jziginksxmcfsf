﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddNodesProcessControl.ascx.cs" Inherits="Orion_Voip_Admin_ManageNodes_AddNodesProcessControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register src="../../Controls/Snmp/SnmpInfo.ascx" tagname="SnmpInfo" tagprefix="uc1" %>

<%@ Register src="../ProgressControl.ascx" tagname="ProgressControl" tagprefix="uc2" %>

<script type="text/javascript" language="javascript">
    Ext.onReady(function() {
        Ext.QuickTips.init();
    });
</script>

<asp:ScriptManagerProxy ID="smp" runat="server">
    <Services>
        <asp:ServiceReference Path="~/Orion/Voip/Admin/ManageNodes/AddNodesProcessService.asmx" />
    </Services>
    <Scripts>
        <asp:ScriptReference Path="~/Orion/Voip/js/FormMenu.js" />
        <asp:ScriptReference Path="~/Orion/Voip/js/ScriptServiceInvoker.js" />
        <asp:ScriptReference Path="~/Orion/Voip/js/ScriptServiceProxy.js" />
        <asp:ScriptReference Path="~/Orion/Voip/Admin/ManageNodes/AddNodesProgressPopup.js" />
        <asp:ScriptReference Path="~/Orion/Voip/Admin/ManageNodes/WriteCredentialsErrorDialog.js" />
    </Scripts>
</asp:ScriptManagerProxy>

<asp:UpdatePanel runat="server" ID="AddedNodesUpdatePanel" UpdateMode="Conditional">
    <ContentTemplate>
    <div id="AddedNodes" runat="server" class="ipsla_NodeManager" style="display:none">
        <div id="NodesAdded" runat="server">
            <asp:Repeater runat="server" ID="NodesAddedRepeater" OnItemDataBound="Repeater_OnItemDataBound">
                <HeaderTemplate><table border="0" width="95%" class="ipsla_NodeManager"></HeaderTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server">
                        <td style="width:12em;" class="ipsla_NodeAddSuccess">
                            <%# DataBinder.Eval(Container.DataItem, "Caption") %>
                        </td>
                        <td>
                            <%# DataBinder.Eval(Container.DataItem, "Message") %>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate></table></FooterTemplate>
            </asp:Repeater>
        </div>
        <div id="ManualNodesAdded" runat="server">
            <asp:Repeater runat="server" ID="ManualNodesAddedRepeater" OnItemDataBound="Repeater_OnItemDataBound">
                <HeaderTemplate><td><table border="0" width="95%" class="ipsla_NodeManager"></HeaderTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server">
                        <td style="width:12em;" class="ipsla_NodeAddSuccess">
                            <%# DataBinder.Eval(Container.DataItem, "Caption") %>
                        </td>
                        <td>
                            <%# DataBinder.Eval(Container.DataItem, "Message") %>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate></table></td></FooterTemplate>
            </asp:Repeater>
        </div>
        <div id="NodesFailed" runat="server">
            <asp:Repeater runat="server" ID="NodesFailedRepeater" OnItemDataBound="Repeater_OnItemDataBound">
                <HeaderTemplate><td><table border="0" width="95%" class="ipsla_NodeManager"></HeaderTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server">
                        <td style="width:12em;" class="ipsla_NodeAddError">
                            <%# DataBinder.Eval(Container.DataItem, "Caption") %>
                        </td>
                        <td>
                            <%# DataBinder.Eval(Container.DataItem, "Message") %>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate></table></td></FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
    </ContentTemplate>
</asp:UpdatePanel>

<div id="pbarBody" class="x-hidden">
    <div class="x-window-body">
        <uc2:ProgressControl ID="ProgressControl" runat="server" CssClass="ipsla_ProgressBox" />
    </div>
</div>

<div id="snmpInfoElement" runat="server" class="x-hidden">
    <uc1:SnmpInfo ID="snmpInfo" runat="server" />
</div>

