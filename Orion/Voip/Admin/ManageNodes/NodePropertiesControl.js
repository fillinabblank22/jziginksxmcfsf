﻿/// <reference name="MicrosoftAjax.js"/>
/// <reference path="~/Orion/js/jquery.js" />
/// <reference path="~/Orion/js/extjs/debug/Ext.js" />

Type.registerNamespace("Orion.Voip.Admin.ManageNodes");

Orion.Voip.Admin.ManageNodes.NodePropertiesControl = function(element) {
    Orion.Voip.Admin.ManageNodes.NodePropertiesControl.initializeBase(this, [element]);
    this.tbSiteName = null;
    this.tbSourceAddress = null;
    this.cliConnection = null;
    this.btnDefault = null;

    this.extSiteName = null;
    this.extSourceAddress = null;
}

Orion.Voip.Admin.ManageNodes.NodePropertiesControl.prototype = {

    get_EngineId: function() { return this.cliConnection.get_ValidationEngineId(); },
    set_EngineId: function(value) { this.cliConnection.set_ValidationEngineId(value); },

    get_NodeIpAddress: function() { return this.cliConnection.get_ValidationIp(); },
    set_NodeIpAddress: function(value) { this.cliConnection.set_ValidationIp(value); },

    get_SiteName: function() { return this.extSiteName.getValue(); },
    set_SiteName: function(value) { this.extSiteName.setValue(value); },

    get_SourceIpAddress: function() { return this.extSourceAddress.getValue(); },
    set_SourceIpAddress: function(value) { this.extSourceAddress.setValue(value); },

    get_Protocol: function() { return this.cliConnection.get_Protocol(); },
    set_Protocol: function(value) { this.cliConnection.set_Protocol(value); },

    get_Port: function() { return this.cliConnection.get_Port(); },
    set_Port: function(value) { this.cliConnection.set_Port(value); },

    get_Timeout: function() { return this.cliConnection.get_Timeout(); },
    set_Timeout: function(value) { this.cliConnection.set_Timeout(value); },

    get_CredentialId: function() { return this.cliConnection.get_CredentialId(); },
    set_CredentialId: function(value) { this.cliConnection.set_CredentialId(value); },

    get_CredentialName: function() { return this.cliConnection.get_CredentialName(); },
    get_Username: function() { return this.cliConnection.get_Username(); },
    get_Password: function() { return this.cliConnection.get_Password(); },
    get_EnablePassword: function() { return this.cliConnection.get_EnablePassword(); },
    get_EnableLevel: function() { return this.cliConnection.get_EnableLevel(); },

    validate: function() {
        return this.extSiteName.validate() &&
            this.extSourceAddress.validate() &&
            this.cliConnection.validate();
    },

    syncSize: function() {
        this.cliConnection.syncSize();
    },

    get_tbSiteName: function() { return this.tbSiteName; },
    set_tbSiteName: function(value) { this.tbSiteName = value; },

    get_tbSourceAddress: function() { return this.tbSourceAddress; },
    set_tbSourceAddress: function(value) { this.tbSourceAddress = value; },

    get_cliConnection: function() { return this.cliConnection; },
    set_cliConnection: function(value) { this.cliConnection = value; },

    get_btnDefault: function() { return this.btnDefault; },
    set_btnDefault: function(value) { this.btnDefault = value; },

    btnDefault_Click: function() {
        this.tbSourceAddress.value = this.cliConnection.get_ValidationIp();
    },

    initialize: function() {
        Orion.Voip.Admin.ManageNodes.NodePropertiesControl.callBaseMethod(this, 'initialize');

        // Add custom initialization here
        Ext.QuickTips.init();

        this.extSiteName = new Ext.form.TextField({
            applyTo: this.tbSiteName,
            width: 200
        });
        this.extSourceAddress = new Ext.form.TextField({
            applyTo: this.tbSourceAddress,
            width: 200
        });
    },
    dispose: function() {
        //Add custom dispose actions here
        Orion.Voip.Admin.ManageNodes.NodePropertiesControl.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Admin.ManageNodes.NodePropertiesControl.registerClass('Orion.Voip.Admin.ManageNodes.NodePropertiesControl', Sys.UI.Control);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
