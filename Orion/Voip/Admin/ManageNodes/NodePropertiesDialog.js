﻿/// <reference name="MicrosoftAjax.js"/>
/// <reference path="~/Orion/js/jquery.js" />
/// <reference path="~/Orion/js/extjs/debug/Ext.js" />

Type.registerNamespace("Orion.Voip.Services");
Type.registerNamespace("Orion.Voip.Admin.ManageNodes");

Orion.Voip.Admin.ManageNodes.NodePropertiesDialog = function(element) {
    Orion.Voip.Admin.ManageNodes.NodePropertiesDialog.initializeBase(this, [element]);

    this.editNodeBox = null;
    this.nodeProperties = null;

    this.window = null;
    this.siteId = null;
}

Orion.Voip.Admin.ManageNodes.NodePropertiesDialog.prototype = {

    Show: function(siteId, onSaved) {
        Orion.Voip.Services.NodePropertiesService.GetProperties(siteId,
            Function.createDelegate(this, function(result, context) {
                this.ShowInternal(context.SiteId, context.OnSaved);
                this.nodeProperties.set_EngineId(result.EngineId);
                this.nodeProperties.set_NodeIpAddress(result.NodeIpAddress);
                this.nodeProperties.set_SiteName(result.SiteName);
                this.nodeProperties.set_SourceIpAddress(result.SourceIpAddress);
                this.nodeProperties.set_CredentialId(result.CredentialId);                
                this.nodeProperties.set_Port(result.Port ? result.Port : 22);
                this.nodeProperties.set_Timeout(result.Timeout ? result.Timeout : 5000);
            }),
            function(error, context) {
                Ext.Msg.show({
                    title: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_53;E=js}',
                    msg: error.get_message(),
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
            },
            { SiteId: siteId, OnSaved: onSaved }
        );
    },

    ShowInternal: function(siteId, onSaved) {
        this.siteId = siteId;

        if (this.window == null) {
            this.window = new Ext.Window({
                title: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_49;E=js}',
                layout: 'fit',
                width: 640,
                height: 580,
                resizable: false,
                closeAction: 'hide',
                closable: false,
                modal: true,
                layout: 'fit',
                applyTo: $('<div id="edit-node"/>').appendTo($(document.forms[0]))[0],
                allowDomMove: false,
                items: [{
                    xtype: 'panel',
                    bodyStyle: 'padding:5px 5px 5px 5px',
                    autoScroll: true,
                    contentEl: this.editNodeBox
                }],
                buttons: [{
                    text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_41;E=js}',
                    handler: Function.createDelegate(this, function () {
                        if (CheckDemoMode()) return;
                        if (this.nodeProperties.validate()) {
                            this.SaveInternal(onSaved);
                        }
                    })
                }, {
                    text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_26;E=js}',
                    handler: Function.createDelegate(this, function () {
                        this.window.hide();
                    })
                }]
            });
            this.nodeProperties.syncSize();
        }
        this.window.show(this);
    },

    SaveInternal: function(onSaved) {
        var siteName = this.nodeProperties.get_SiteName();
        var sourceIpAddress = this.nodeProperties.get_SourceIpAddress();
        var credentialId = this.nodeProperties.get_CredentialId();
        var credentialName = this.nodeProperties.get_CredentialName();
        var connInfo = null;
        if (credentialId != null || (credentialName != null && credentialName.length > 0)) {
            connInfo = {
                CredentialId: credentialId,
                CredentialName: credentialName,
                Username: this.nodeProperties.get_Username() || '',
                Password: this.nodeProperties.get_Password() || '',
                EnablePassword: this.nodeProperties.get_EnablePassword() || '',
                EnableLevel: this.nodeProperties.get_EnableLevel(),
                Protocol: this.nodeProperties.get_Protocol(),
                Port: this.nodeProperties.get_Port(),
                Timeout: this.nodeProperties.get_Timeout()
            };
        }
        Orion.Voip.Services.NodePropertiesService.UpdateProperties(this.siteId, siteName || '', sourceIpAddress || '', connInfo,
            Function.createDelegate(this, function(result, context) {
                this.window.hide();
                if (onSaved != null)
                    onSaved();
            }),
            function(error, context) {
                Ext.Msg.show({
                    title: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_53;E=js}',
                    msg: error.get_message(),
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
            },
            null
        );
    },

    get_editNodeBox: function() { return this.editNodeBox; },
    set_editNodeBox: function(value) { this.editNodeBox = value; },

    get_nodeProperties: function() { return this.nodeProperties; },
    set_nodeProperties: function(value) { this.nodeProperties = value; },

    initialize: function() {
        Orion.Voip.Admin.ManageNodes.NodePropertiesDialog.callBaseMethod(this, 'initialize');

        // Add custom initialization here
    },
    dispose: function() {
        //Add custom dispose actions here
        Orion.Voip.Admin.ManageNodes.NodePropertiesDialog.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Admin.ManageNodes.NodePropertiesDialog.registerClass('Orion.Voip.Admin.ManageNodes.NodePropertiesDialog', Sys.UI.Control);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
