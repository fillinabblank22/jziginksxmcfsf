﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Contracts;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;
using System.Globalization;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.IpSla.Web.Admin.Settings;

public partial class Orion_Voip_Admin_ManageNodes_AddNodesProcessControl : ScriptUserControlBase, IPostBackEventHandler
{
    public event EventHandler Close;
    public event EventHandler ProcessFinished;

    public bool ShowWriteCredentialsErrors { get; set; }
    public bool ShowResults { get; set; }
    public AddNodesProcessUsageContext UsageContext { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (AddNodesProcessManager.GetCurrent(this.UsageContext).IsWorking)
            {
                ShowProgress();
            }
        }
        switch (this.UsageContext)
        {
            case AddNodesProcessUsageContext.AddNodesControl:
                //this.ProgressControl.TotalLabel = "IP SLA nodes confirmed";
                //this.ProgressControl.ProcessedLabel = "nodes tested";
                this.ProgressControl.Title = Resources.VNQMWebContent.VNQMWEBCODE_AK1_5;
                break;
            case AddNodesProcessUsageContext.StartupWizard:
                //this.ProgressControl.SucceededLabel = "IP SLA nodes discovered";
                //this.ProgressControl.ProcessedLabel = "nodes tested";
                this.ProgressControl.Title = Resources.VNQMWebContent.VNQMWEBCODE_AK1_6;
                break;
            default:
                throw new ArgumentOutOfRangeException(string.Format("[UsageContext={0}]", this.UsageContext));
        }
    }

    private void Manager_ProcessFinished(object sender, EventArgs e)
    {
        this.OnProcessFinished();
    }

    private void OnProcessFinished()
    {
        if (ProcessFinished != null)
        {
            ProcessFinished(this, EventArgs.Empty);
        }
    }

    #region IScriptControl Members
    private ScriptControlDescriptor descriptor;
    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        descriptor = new ScriptControlDescriptor("Orion.Voip.Admin.ManageNodes.AddNodesProcessControl", this.ClientID);
        descriptor.AddProperty("nodesAddedTagId", NodesAdded.ClientID);
        descriptor.AddProperty("manualNodesAddedTagId", ManualNodesAdded.ClientID);
        descriptor.AddProperty("nodesFailedTagId", NodesFailed.ClientID);
        descriptor.AddProperty("showWriteCredentialsErrors", this.ShowWriteCredentialsErrors);
        descriptor.AddProperty("showResults", this.ShowResults);
        descriptor.AddProperty("usageContext", this.UsageContext);
        descriptor.AddComponentProperty("snmpInfo", this.snmpInfo.ClientID);
        descriptor.AddComponentProperty("progressControl", this.ProgressControl.ClientID);

        descriptor.AddElementProperty("snmpInfoElement", this.snmpInfoElement.ClientID);

        descriptor.AddScriptProperty("invokeClosePostback",
            "function() { " + this.Page.ClientScript.GetPostBackEventReference(this, "Close") + "; }"
            );
        descriptor.AddScriptProperty("invokeShowResultsPostback",
            "function() { " + this.Page.ClientScript.GetPostBackEventReference(this, "ShowResults") + "; }"
            );
        descriptor.AddScriptProperty("invokeSaveAndClosePostback",
            "function() { " + this.Page.ClientScript.GetPostBackEventReference(this, "SaveAndClose") + "; }"
            );
        descriptor.AddScriptProperty("invokeSaveAndShowResultsPostback",
            "function() { " + this.Page.ClientScript.GetPostBackEventReference(this, "SaveAndShowResults") + "; }"
            );
        
        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = this.ResolveUrl("AddNodesProcessControl.js");
        yield return reference;
    }

    #endregion

    protected class NodeResult
    {
        public string Caption { get; set; }
        public string Message { get; set; }
    }

    private const string SUCCEEDED_KEY = "NodesSucceeded";
    private const string MANUALCANDIDATES_KEY = "NodesManualCandidates";
    private const string FAILED_KEY = "NodesFailed";
    
    public void AddSelectedNodes(IEnumerable<int> nodeIDs, bool autoSave)
    {
        if (AddNodesProcessManager.GetCurrent(this.UsageContext).IsWorking)
        {
            //throw new InvalidOperationException("AddNodesProcessManager is in process");
            return;
        }
        AddNodesProcessManager.GetCurrent(this.UsageContext).BeginNodeDiscovery(nodeIDs, autoSave);
        this.ShowProgress();
    }

    private void ShowProgress()
    {
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ShowProgress",
            "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').ShowProgress();});});",
            true);
    }

    //private void ShowManualNodes(IEnumerable<IdResult> manualNodes)
    //{
    //    //ShowResults(ManualNodesRepeater, manualNodes);
    //    //ManualNodesUpdatePanel.Update();
    //    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ManualNodesDialog",
    //    //    "Ext.onReady(function(){setTimeout(function(){$find('" + this.ClientID + "').ShowManualNodesDialog();});});",
    //    //    true);
    //}

    private void ShowResultsOfType(Repeater repeater, IEnumerable<NodeDiscoveryItem> results, bool showMessage)
    {
        if (results != null)
        {
            var nodeIds = results.Select<NodeDiscoveryItem, int>(result => result.NodeId);
            var nodeCaptions = NpmNodesDAL.Instance.GetNodeCaptions(nodeIds).ToArray();
            var displayData = results.Select<NodeDiscoveryItem, NodeResult>((idResult, index) => new NodeResult
            {
                Message = (showMessage ? idResult.Message ?? string.Empty : string.Empty),
                Caption = nodeCaptions[index]
            });
            repeater.DataSource = displayData;
            repeater.DataBind();
        }
    }

    private void ShowAllResults(IEnumerable<NodeDiscoveryItem> succeeded, IEnumerable<NodeDiscoveryItem> manual, IEnumerable<NodeDiscoveryItem> failed, bool manualAsFailed)
    {
        bool showSimpleDialog = false;
        int countSucceeded = 0, countFailed = 0, countManual = 0;
        if (succeeded != null && succeeded.Count() != 0)
        {
            countSucceeded = succeeded.Count();
            ShowResultsOfType(NodesAddedRepeater, succeeded, false);
            showSimpleDialog = succeeded.All(idResult => string.IsNullOrEmpty("ToDo"));
        }

        if (manualAsFailed && manual != null && manual.Count() > 0)
        {
            if (failed != null)
                failed = failed.Concat(manual);
            else
                failed = manual;
        }

        if (failed != null && failed.Count() > 0)
        {
            showSimpleDialog = false;
            countFailed = failed.Count();
            ShowResultsOfType(NodesFailedRepeater, failed, true);
        }

        if (!manualAsFailed && manual != null && manual.Count() > 0)
        {
            showSimpleDialog = false;
            countManual = manual.Count();
            ShowResultsOfType(ManualNodesAddedRepeater, manual, true);
        }

        AddedNodesUpdatePanel.Update();
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ResultsDialog",
            String.Format("Ext.onReady(function(){{setTimeout(function(){{$find('{0}').ShowResultsDialog({1},{2},{3},{4});}});}});",
            this.ClientID, countSucceeded, countManual, countFailed, showSimpleDialog.ToString().ToLower()),
            true);
        //NpmNodeSelectionSession.GetCurrent(IpSlaNodeTreeDataProvider.SESSION_KEY).Close();
    }



    protected string AddedNodeRowClass(object error)
    {
        if (error is bool && (bool)error)
            return "ipsla_NodeAddError";
        else
            return "ipsla_NodeAddSuccess";
    }


    #region IPostBackEventHandler Members

    public void RaisePostBackEvent(string eventArgument)
    {
        //IEnumerable<IdResult> succeeded, manual, failed;
        switch (eventArgument)
        {
            case "Close":
                this.OnClose();
                break;
            case "ShowResults":
                this.OnShowResults();
                break;
            case "SaveAndClose":
                this.OnSaveAndClose();
                break;
            case "SaveAndShowResults":
                this.OnSaveAndShowResults();
                break;
        }
    }
    #endregion

    private void OnClose()
    {
        if (Close != null)
        {
            Close(this, EventArgs.Empty);
        }
    }

    private void OnShowResults()
    {
        IEnumerable<NodeDiscoveryItem> succeeded, manual, failed;
        var results = AddNodesProcessManager.GetCurrent(this.UsageContext).GetNodeDiscoveryResults();
        succeeded = results.Where(r => (r.IpSlaCapability == NodeIpSlaCapability.ReadWrite));
        manual =
            results.Where(
                r =>
                (r.IpSlaCapability == NodeIpSlaCapability.ReadOnly || r.IpSlaCapability == NodeIpSlaCapability.NoAccess));
        failed = results.Where(r => (r.IpSlaCapability == NodeIpSlaCapability.NoIpSla));
        ShowAllResults(succeeded, manual, failed, false);
    }

    private void OnSaveAndShowResults()
    {
        SaveResults();
        OnShowResults();
    }

    private void OnSaveAndClose()
    {
        SaveResults();
        OnClose();
    }


    private void SaveResults()
    {
        AddNodesProcessManager.GetCurrent(this.UsageContext).SaveResults();
    }

    protected void Repeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item)
        {
            var row = (HtmlTableRow)e.Item.FindControl("row");
            row.Attributes["class"] = "alternate";
        }
    }

}
