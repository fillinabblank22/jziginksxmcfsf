﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.IpSla.Web.Admin.Settings;
using SolarWinds.Orion.IpSla.Web.WebServices.AjaxTree;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Contracts;
using SolarWinds.Orion.IpSla.Web.DAL;
using System.Data;
using System.Globalization;

public partial class Orion_Voip_Admin_Settings_AddNodesControl : ScriptUserControlBase, IPostBackEventHandler
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.GetCurrent(Page).RegisterAsyncPostBackControl(AddNodes);
        CheckAvailableNodes();

        if (this.ViewState[REDIRECTURI_KEY] == null && !Request.Url.Equals(Request.UrlReferrer))
            this.ViewState[REDIRECTURI_KEY] = Request.UrlReferrer;
        this.AddNodesProcessControl.Close += new EventHandler(AddNodesProcessControl_Close);
    }

    void AddNodesProcessControl_Close(object sender, EventArgs e)
    {
        this.Close();
    }

    private void CheckAvailableNodes()
    {
        bool nodesAvailable = (NpmNodesDAL.Instance.GetCountAllAvailableIpSlaNodes() > 0);
        this.NpmNodesSelectionControl.Visible = nodesAvailable;
        this.AddNodes.Visible = nodesAvailable;
        this.NoAvailableNodesControl.Visible = !nodesAvailable;
    }

    #region IScriptControl Members

    private ScriptControlDescriptor descriptor;
    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        descriptor = new ScriptControlDescriptor("Orion.Voip.Admin.ManageNodes.AddNodes", this.ClientID);
        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = this.ResolveUrl("AddNodesControl.js");
        yield return reference;
    }

    #endregion

    protected void Cancel_Click(object sender, EventArgs e)
    {
        Close();
    }

    private const string REDIRECTURI_KEY = "RedirectUri";
    private void Close()
    {
        NpmNodeSelectionSession.GetCurrent(IpSlaNodeTreeDataProvider.SESSION_KEY).Close();
        Uri redirectUri = (Uri)this.ViewState[REDIRECTURI_KEY];
        if (redirectUri == null)
        {
            redirectUri = new Uri(Request.Url, "/Orion/Voip/Admin/Default.aspx");
        }
        Response.Redirect(redirectUri.ToString());
    }

    protected void AddNodes_Click(object sender, EventArgs e)
    {
        if (NpmNodeSelectionSession.GetCurrent(IpSlaNodeTreeDataProvider.SESSION_KEY).SelectedNodes.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "NoNodeSelected",
                String.Format("Ext.onReady(function()Ext.MessageBox.alert('{0}', '{1}'););", Resources.VNQMWebContent.VNQMWEBCODE_AK1_1, Resources.VNQMWebContent.VNQMWEBCODE_AK1_2),
                true);
        }
        else
        {
            try
            {
                AddSelectedNodes();
            }
            catch (EndpointNotFoundException)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ModuleEngine",
                    String.Format("Ext.onReady(function()Ext.MessageBox.alert('{0}', '{1}'););", Resources.VNQMWebContent.VNQMWEBCODE_AK1_3, Resources.VNQMWebContent.VNQMWEBCODE_AK1_4),
                    true);
            }
        }
    }

    private void AddSelectedNodes()
    {
        var selectedNodes = NpmNodeSelectionSession.GetCurrent(IpSlaNodeTreeDataProvider.SESSION_KEY).SelectedNodes;
        var nodeIds = selectedNodes.Select<NodeSelectionInfo, int>(info => info.NodeId).ToArray();

        this.AddNodesProcessControl.AddSelectedNodes(nodeIds, false);
    }


    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (this.ViewState[REDIRECTURI_KEY] == null && !Request.Url.Equals(Request.UrlReferrer))
            this.ViewState[REDIRECTURI_KEY] = Request.UrlReferrer;
    }

    #region IPostBackEventHandler Members

    public void RaisePostBackEvent(string eventArgument)
    {
        switch (eventArgument)
        {
            case "Close":
                Close();
                break;
        }
    }

    #endregion

}
