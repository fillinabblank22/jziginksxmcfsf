﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Voip.Admin.ManageNodes");

// Column rendering functions
function renderName(value, meta, record) {
    return String.format('{0} {1}', getStatusIcon(record.data['Status']), value);
}

function renderType(value, meta, record) {
    return value;
}

function getStatusIcon(status) {
        return String.format('<img src="/Orion/images/StatusIcons/Small-{0}.gif" />', status);
}

function renderLicensed(value, meta, record) {
    if (value == "yes")
        return '<img src="/Orion/Voip/images/licensed_16x16.gif" /> @{R=VNQM.Strings;K=VNQMWEBJS_TM0_54;E=js}';
    else
        return '<img src="/Orion/Voip/images/licensed_no_16x16.gif" /> @{R=VNQM.Strings;K=VNQMWEBJS_TM0_55;E=js}';
}

function renderHasCliCreds(value, meta, record) {
    if (value)
        return value;

    return '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_23;E=js}';
}

Orion.Voip.Admin.ManageNodes.NodeManagerControl = function(element) {
    Orion.Voip.Admin.ManageNodes.NodeManagerControl.initializeBase(this, [element]);
    this.gridHostElementId = null;
    this.nodeIdsField = "";
    this.nodeManagerDataStore = null;
    this.nodeManagerSelectionModel = null;
    this.nodeManagerColumnModel = null;
    this.nodeManagerGrid = null;
    this.nodesToRemoveDivId = null;
    this.operationsToRemoveDivId = null;
    this.nodePropertiesDialog = null;

    this.InvokePostBack = null;
    this.InvokeRemoveNodesPostback = null;
    this.InvokeAddNodesPostback = null;
    this.InvokeDiscoverNodesPostback = null;
}

Orion.Voip.Admin.ManageNodes.NodeManagerControl.prototype = {

    get_nodeIdsField: function(value) {
        return this.nodeIdsField;
    },
    set_nodeIdsField: function(value) {
        this.nodeIdsField = $get(value);
    },

    get_nodePropertiesDialog: function() { return this.nodePropertiesDialog; },
    set_nodePropertiesDialog: function(value) { this.nodePropertiesDialog = value; },

    AddNode: function() {
        this.InvokeAddNodesPostback();
    },

    DiscoverNodes: function() {
        this.InvokeDiscoverNodesPostback();
    },

    ConfirmRemoveNodes: function() {
        var selections = this.nodeManagerGrid.selModel.getSelections();
        var selectedNodes = [];
        for (i = 0; i < this.nodeManagerGrid.selModel.getCount(); i++) {
            selectedNodes.push(selections[i].json.SiteID);
        }
        this.nodeIdsField.value = Sys.Serialization.JavaScriptSerializer.serialize(selectedNodes);
        this.InvokePostBack();
    },

    ShowConfirmRemoveNodesDialog: function() {
        this.confirmRemoveNodesDialog = new Ext.Window({
            title: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_36;E=js}',
            layout: 'fit',
            width: 520,
            height: 500,
            minWidth: 400,
            minHeight: 300,
            closeAction: 'hide',
            closable: false,
            modal: true,
            layout: 'anchor',
            items: [{
                title: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_37;E=js}',
                html: $get(this.nodesToRemoveDivId).innerHTML,
                anchor: 'top 40%',
                autoScroll: true
            }, {
                title: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_38;E=js}',
                html: $get(this.operationsToRemoveDivId).innerHTML,
                anchor: 'top 60%',
                autoScroll: true

            }],
            buttons: [{
                text: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_39;E=js}',
                handler: Function.createDelegate(this, function () {
                    if (CheckDemoMode()) return;
                    this.removeSelectedNodes();
                    this.confirmRemoveNodesDialog.hide();
                })
            }, {
                text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_26;E=js}',
                handler: Function.createDelegate(this, function () {
                    this.confirmRemoveNodesDialog.hide();
                })
            }]
        });

        this.confirmRemoveNodesDialog.show(this);
    },

    EditNode: function() {
        if (this.nodeManagerGrid.selModel.getCount() != 1)
            return;
        var siteId = this.nodeManagerGrid.selModel.getSelections()[0].json.SiteID;
        this.nodePropertiesDialog.Show(siteId, Function.createDelegate(this, function() { this.reloadGrid(); }));
    },

    _onSubmit: function() {
        if (!this.nodeIdsField)
            return true;

        if (this.saveClientState() != null) {
            this.nodeIdsField.value = this.saveClientState();
        }
        return true;
    },

    saveClientState: function() {
        var state = {};
        return Sys.Serialization.JavaScriptSerializer.serialize(state);
    },


    initialize: function() {
        Orion.Voip.Admin.ManageNodes.NodeManagerControl.callBaseMethod(this, 'initialize');
        $(document).ready(Function.createDelegate(this, function() {
            Ext.QuickTips.init();
            this.initializeGrid();
            this.nodeManagerGrid.render(this.gridHostElementId);
            this.nodesDataStore.load();
            this.handleButtonState();
        }));
    },

    initializeGrid: function() {
        // Add custom initialization here
        this.nodesDataStore = new Ext.data.Store({
            id: 'NodesDataStore',
            proxy: new Ext.data.HttpProxy({
                url: '/Orion/Voip/Admin/ManageNodes/NodeManagerExtGridDataProvider.ashx',
                method: 'POST'
            }),
            baseParams: { task: 'select' }, // this parameter is passed for any HTTP request
            reader: new Ext.data.JsonReader({
                root: 'sites',
                totalProperty: 'total',
                id: 'id'
            }, [
                { name: 'SiteID', type: 'int' },
                { name: 'SiteName', type: 'string' },
                { name: 'IpAddress', type: 'string' },
                { name: 'OpCount', type: 'int' },
                { name: 'Status', type: 'string' },
                { name: 'Licensed', type: 'string' },
                { name: 'Mode', type: 'string' },
                { name: 'CliCredentialsName', type: 'string' },
            ]),
            sortInfo: { field: 'SiteName', direction: 'ASC' }
        });

        this.nodesSelectionModel = new Ext.grid.CheckboxSelectionModel({
            singleSelect: false,
            listeners: {
                'selectionchange': {
                    scope: this,
                    fn: function(p) {
                        this.handleButtonState();
                    }
                }
            }
        });

        this.nodesColumnModel = new Ext.grid.ColumnModel(
                        [this.nodesSelectionModel,
                        {
                            header: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_34;E=js}',
                            dataIndex: 'SiteName',
                            width: 250,
                            renderer: renderName,
                            css : "vertical-align : middle;"
                        }, {
                            header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_40;E=js}',
                            dataIndex: 'IpAddress',
                            width: 130
                        }, {
                            header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_41;E=js}',
                            dataIndex: 'Licensed',
                            width: 100,
                            renderer: renderLicensed,
                            css: "vertical-align : middle;"
                        }, {
                            header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_42;E=js}',
                            dataIndex: 'CliCredentialsName',
                            renderer: renderHasCliCreds,
                            width: 170
                        }, {
                            header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_43;E=js}',
                            dataIndex: 'OpCount',
                            width: 130
                        }, {
                            header: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_44;E=js}',
                            dataIndex: 'Mode',
                            width: 180
                        }]
                    );
        this.nodesColumnModel.defaultSortable = true;

        this.nodeManagerGrid = new Ext.grid.GridPanel({
            id: 'NodeManagerGrid',
            height: 500,
            width: 990,
            store: this.nodesDataStore,
            cm: this.nodesColumnModel,
            enableColLock: false,
            selModel: this.nodesSelectionModel,
			stripeRows: true,
            tbar: [
                    {
                        id: 'btnAddNode',
                        text: '&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_TM0_45;E=js}',
                        tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_46;E=js}',
                        handler: Function.createDelegate(this, this.AddNode),
                        iconCls: 'ipsla_AddNode'
                    },
                    '-',
                    {
                        id: 'btnDiscover',
                        text: '&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_TM0_47;E=js}',
                        tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_48;E=js}',
                        handler: Function.createDelegate(this, this.DiscoverNodes),
                        iconCls: 'ipsla_DiscoverNodes'
                    },
                    '-',
                    {
                        id: 'btnEdit',
                        text: '&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_TM0_49;E=js}',
                        tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_50;E=js}',
                        handler: Function.createDelegate(this, this.EditNode),
                        iconCls: 'ipsla_EditNode'
                    },
                    '-',
                    {
                        id: 'btnRemove',
                        text: '&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_TM0_51;E=js}',
                        tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_52;E=js}',
                        handler: Function.createDelegate(this, this.ConfirmRemoveNodes),
                        iconCls: 'ipsla_RemoveNode'
                  }]
        });
    },
    
    reloadGrid: function() {
        this.nodesDataStore.reload();
    },
    
    handleButtonState: function() {
        // enable/disable edit button based on item selection count
        var toolbar = this.nodeManagerGrid.getTopToolbar();
        var map = toolbar.items.map;
        if (this.nodeManagerGrid.selModel.getCount() >= 1) {
            map.btnRemove.enable();
        }
        else {
            map.btnRemove.disable();
        }
        if (this.nodeManagerGrid.selModel.getCount() == 1) {
            map.btnEdit.enable();
        }
        else {
            map.btnEdit.disable();
        }
    },

    removeSelectedNodes: function() {
        var selections = this.nodeManagerGrid.selModel.getSelections();
        var selectedNodes = [];
        for (i = 0; i < this.nodeManagerGrid.selModel.getCount(); i++) {
            selectedNodes.push(selections[i].json.SiteID);
        }
        this.nodeIdsField.value = Sys.Serialization.JavaScriptSerializer.serialize(selectedNodes);
        this.InvokeRemoveNodesPostback();
    },
    
    dispose: function() {
        //Add custom dispose actions here
        Orion.Voip.Admin.ManageNodes.NodeManagerControl.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Admin.ManageNodes.NodeManagerControl.registerClass('Orion.Voip.Admin.ManageNodes.NodeManagerControl', Sys.UI.Control);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
