﻿/// <reference name="MicrosoftAjax.js"/>
/// <reference path="~/Orion/js/jquery.js" />
/// <reference path="~/Orion/js/extjs/debug/Ext.js" />
/// <reference path="~/Orion/js/extjs/debug/jquery-bridge.js" />
/// <reference path="~/Orion/js/extjs/debug/ext-all-debug.js" />
/// <reference path="~/Orion/Voip/js/ScriptServiceInvoker.js" />
/// <reference path="~/Orion/Voip/js/ScriptServiceProxy.js" />

Type.registerNamespace("Orion.Voip.Admin.ManageNodes");

Orion.Voip.Admin.ManageNodes.AddNodesProgressPopup = function() {
    Orion.Voip.Admin.ManageNodes.AddNodesProgressPopup.initializeBase(this);

    this.progressControl = null;
    this.addNodesProgressWindow = null;
    this.statusServiceInvoker = null;
    this.cancelServiceInvoker = null;

    this.finishedDelegate = null;
    this.isWorking = false;
    this.usageContext = 0;
}

Orion.Voip.Admin.ManageNodes.AddNodesProgressPopup.prototype = {
    get_usageContext: function (value) {
        return this.usageContext;
    },
    set_usageContext: function (value) {
        this.usageContext = value;
    },

    get_progressControl: function (value) {
        return this.progressControl;
    },
    set_progressControl: function (value) {
        this.progressControl = value;
    },

    initialize: function () {
        Orion.Voip.Admin.ManageNodes.AddNodesProgressPopup.callBaseMethod(this, 'initialize');
    },

    ShowProgress: function (finishedDelegate) {
        this.finishedDelegate = finishedDelegate;


        if (this.addNodesProgressWindow == null) {
            this.addNodesProgressWindow = new Ext.Window({
                title: this.usageContext == 0 ? '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_24;E=js}' : '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_25;E=js}',
                width: 475,
                height: 172,
                closeAction: 'hide',
                resizable: false,
                modal: true,
                closable: false,
                bodyCssClass: 'GroupBox ipsla_ProgressBarDiscovery',
                contentEl: 'pbarBody',

                buttons: [{
                    text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_26;E=js}',
                    handler: Function.createDelegate(this, function () { this.OnClose(true); })
                }]
            });
        }
        if (this.statusServiceInvoker == null) {
            this.statusServiceInvoker = $create(Orion.Voip.js.ScriptServiceInvoker);
        }
        this.isWorking = true;
        this.statusServiceInvoker = $create(Orion.Voip.js.ScriptServiceInvoker);
        this.statusServiceInvoker.callService(
        SolarWinds.Orion.IpSla.Web.Admin.Settings.AddNodesProcessService.GetAddNodesStatus,
        [this.usageContext], {}, 1000, this,
        Function.createDelegate(this, this.OnStatusSucceedNotLast),
        Function.createDelegate(this, this.OnStatusSucceedLast),
        Function.createDelegate(this, this.OnStatusError));
        this.addNodesProgressWindow.show();
    },

    OnClose: function (cancel) {
        this.statusServiceInvoker.cancel();
        this.isWorking = false;
        SolarWinds.Orion.IpSla.Web.Admin.Settings.AddNodesProcessService.CloseNodeDiscovery(
            this.usageContext, !cancel,
            Function.createDelegate(this, this.OnCloseSucceed),
            Function.createDelegate(this, this.OnCloseError),
            { cancel: cancel });

    },

    OnCloseSucceed: function (resultParams, context) {
        this.addNodesProgressWindow.hide();
        this.Finish(context.cancel);
    },

    OnCloseError: function (error, context) {
        this.HandleError(error);
    },

    OnStatusSucceedNotLast: function (resultParams, context) {
        this.HandleStatus(resultParams);
    },

    OnStatusSucceedLast: function (resultParams, context) {
        this.HandleStatus(resultParams);
        this.addNodesProgressWindow.close();
        var credentialsIssues = resultParams.result.ResponseData.CredentialsIssueNodesCount > 0;
        this.Finish(false, credentialsIssues);
    },

    OnStatusError: function (error, context) {
        this.HandleError(error);
    },

    HandleError: function (error) {
        var title = this.usageContext == 0 ? '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_27;E=js}' : '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_28;E=js}';
        Ext.MessageBox.alert(title, String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_29;E=js}', error.get_message()));
    },

    HandleStatus: function (resultContainer) {
        if (this.addNodesProgressWindow != null && !this.addNodesProgressWindow.hidden) {
            var responseData = resultContainer.result.ResponseData;
            var processedInnerHtml;
            var totalInnerHtml;
            if (this.usageContext == 0) {
                processedInnerHtml = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_30;E=js}', '<span style=\"font-weight:bold;padding:0 5px 0 5px;\">' + responseData.DiscoveredNodesCount + '</span>');
                totalInnerHtml = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_31;E=js}', '<span style=\"padding:0 5px 0 5px;\">' + responseData.TestedNodesCount, responseData.AllNodesCount + '</span>');
            }
            else {
                processedInnerHtml = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_32;E=js}', '<span style=\"font-weight:bold;padding:0 5px 0 5px;\">' + responseData.DiscoveredNodesCount + '</span>');
                totalInnerHtml = String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_31;E=js}', '<span style=\"padding:0 5px 0 5px;\">' + responseData.TestedNodesCount, responseData.AllNodesCount + '</span>');
            }
            var value = (responseData.AllNodesCount !== 0 ? responseData.DiscoveredNodesCount / responseData.AllNodesCount : 0);
            this.progressControl.UpdateProgress(value, processedInnerHtml, totalInnerHtml);
        }
        else {
            //todo - cleanup
            resultContainer.callServiceAgain = false;
        }
    },

    Finish: function (canceled, credentialsIssues) {
        if (this.finishedDelegate != null) {
            this.finishedDelegate.call(this, canceled, credentialsIssues);
        }
    },

    dispose: function () {
        //Add custom dispose actions here
        Orion.Voip.Admin.ManageNodes.AddNodesProgressPopup.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Admin.ManageNodes.AddNodesProgressPopup.registerClass('Orion.Voip.Admin.ManageNodes.AddNodesProgressPopup', Sys.Component);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
