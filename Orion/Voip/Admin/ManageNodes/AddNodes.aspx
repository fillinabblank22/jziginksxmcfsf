﻿<%@ Page Title="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_64 %>" Language="C#" MasterPageFile="~/Orion/Voip/Admin/Config.master" AutoEventWireup="true" CodeFile="AddNodes.aspx.cs" Inherits="Orion_Voip_Controls_Settings_AddNodes" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagName="IncludeExtJs" TagPrefix="orion" %>
<%@ Register src="AddNodesControl.ascx" tagname="AddNodesControl" tagprefix="ipsla" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="contentHead" ContentPlaceHolderID="contentPlaceHolderHead" Runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/styles/Admin.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/styles/Discovery.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/Voip.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/styles/SnmpInfo.css" />
    <orion:IncludeExtJs ID="IncludeExtJs" runat="server" Version="3.4" />
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="Breadcrumbs">
    <div class="sw-hdr-breadbox">
        <span><a href="../../../admin"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_6 %></a> &gt; <a href="../default.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_VnqmSettingsLinkName %></a> &gt; <a href="default.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_9 %></a> &gt;</span>
    </div>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="TopRightPageLinks">
     <orion:IconHelpButton ID="IconHelpButtonAN" runat="server" HelpUrlFragment="OrionIPSLAManagerAdministratorGuide-AddingIPSLA-CapableNodesManually" />
</asp:Content>

<asp:Content ID="contentBody" runat="server" ContentPlaceHolderID="contentPlaceHolderBody" >
    <h1 style="font-size: large;"><%=Title %></h1>
    <br/>
    <ipsla:AddNodesControl ID="AddNodesControl" runat="server" />
</asp:Content>