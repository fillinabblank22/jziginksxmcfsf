﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.NPM.Common;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.Admin.Settings;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;

public partial class Orion_Voip_Admin_Settings_NpmNodesSelectionControl : ScriptUserControlBase
{

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        NpmNodesSelectionTreeControl1.SelectedNodesListIdentifier = IpSlaNodeTreeDataProvider.PROVIDER_KEY;
    }

    protected void groupBySelector_SelectedValueChanged(object sender, EventArgs e)
    {
        RefreshTree(this.groupBySelector.SelectedValue);
    }

    public void RefreshTree(string groupBy)
    {
        NpmNodesSelectionTreeControl1.RefreshTree(groupBy, new NpmNodeSelectionAjaxTreeService());
    }

    #region ScriptControl Behaviour

    protected virtual ScriptControlDescriptor GetScriptDescriptor()
    {
        var descriptor = new ScriptControlDescriptor("Orion.Voip.Admin.ManageNodes.NpmNodesSelectionControl", this.ClientID);
        //descriptor.AddScriptProperty("WebServiceProxy", "SolarWinds.Orion.IpSla.Web.Wizards.Operations.ChooseNodesAjaxTreeService");
        //descriptor.AddScriptProperty("WebServiceProxy", JavascriptWebServiceProxy);
        descriptor.AddComponentProperty("treeControl", this.NpmNodesSelectionTreeControl1.ClientID);
        descriptor.AddElementProperty("ajaxTreeContent", this.ajaxTreeContent.ClientID);
        return descriptor;
    }

    #region IScriptControl Members

    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        yield return this.GetScriptDescriptor();
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        yield return new ScriptReference("NpmNodesSelectionControl.js");
    }

    #endregion

    #endregion

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        RefreshTree(this.groupBySelector.SelectedValue);
    }


}
