﻿<%@ Page Title="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_9 %>" Language="C#" MasterPageFile="~/Orion/Voip/Admin/Config.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_Voip_Controls_Settings_Default" %>
<%@ Import Namespace="Resources" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagName="IncludeExtJs" TagPrefix="orion" %>
<%@ Register src="NodeManagerControl.ascx" tagname="NodeManagerControl" tagprefix="uc1" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconLinkExportToPDF runat="server" ID="ExportToPDFLink"/>
    <orion:IconHelpButton HelpUrlFragment="OrionIPSLAManagerPH-ManageIPSLANodesPage" ID="HelpButton1" runat="server" />
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="Breadcrumbs">
    <div class="sw-hdr-breadbox">
        <span><a href="../../../admin"><%= VNQMWebContent.VNQMWEBDATA_VB1_6 %></a> &gt; <a href="../default.aspx"><%= VNQMWebContent.VNQMWEBDATA_VnqmSettingsLinkName %></a> &gt;</span>
    </div>
</asp:Content>

<asp:Content ID="contentHead" ContentPlaceHolderID="contentPlaceHolderHead" Runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/styles/Admin.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/Voip.css" />
    <orion:IncludeExtJs ID="IncludeExtJs" runat="server" Version="3.4" />
</asp:Content>

<asp:Content ID="contentBody" runat="server" ContentPlaceHolderID="contentPlaceHolderBody" >

<span class="IpSlaPageTitle"><%= Page.Title %></span>
    
<div style="position:relative">
	<div style="padding-top: 5px;"><%= VNQMWebContent.VNQMWEBDATA_TM0_79 %></div>
	<div id="ipsla_Admin">
	    <div class="para">
	        <div class="link">&rsaquo;&rsaquo;<a href="/Orion/Voip/Admin/OperationsWizard.aspx">&nbsp;<%= VNQMWebContent.VNQMWEBDATA_TM0_78 %></a></div>
	    </div>
    </div>
</div>
<uc1:NodeManagerControl ID="NodeManagerControl1" runat="server" />

</asp:Content>

