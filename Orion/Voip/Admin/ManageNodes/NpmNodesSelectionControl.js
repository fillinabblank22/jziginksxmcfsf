﻿/// <reference name="MicrosoftAjax.js"/>
/// <reference path="~/Orion/js/jquery.js" />
/// <reference path="~/Orion/js/extjs/debug/Ext.js" />
/// <reference path="~/Orion/js/extjs/debug/jquery-bridge.js" />
/// <reference path="~/Orion/js/extjs/debug/ext-all-debug.js" />

Type.registerNamespace("Orion.Voip.Admin.ManageNodes");

Orion.Voip.Admin.ManageNodes.NpmNodesSelectionControl = function(element) {
    Orion.Voip.Admin.ManageNodes.NpmNodesSelectionControl.initializeBase(this, [element]);
    this.panel = null;
    this.treeControl = null;
    this.ajaxTreeContent = null;
}

Orion.Voip.Admin.ManageNodes.NpmNodesSelectionControl.prototype = {

    get_treeControl: function() {
        return this.treeControl;
    },
    set_treeControl: function(value) {
        this.treeControl = value;
    },

    get_ajaxTreeContent: function() {
        return this.ajaxTreeContent;
    },
    set_ajaxTreeContent: function(value) {
        this.ajaxTreeContent = value;
    },

    initialize: function() {
        Orion.Voip.Admin.ManageNodes.NpmNodesSelectionControl.callBaseMethod(this, 'initialize');

        Ext.onReady(Function.createDelegate(this, function() {
            this.panel = new Ext.Panel({
                id: 'panel',
                applyTo: this.ajaxTreeContent,
                autoHeight: true,
                tbar: new Ext.Toolbar({
                    height: 30,
                    items: [{
                    text: '&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_AK1_64;E=js}',
                    tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_65;E=js}',
                    handler: Function.createDelegate(this, this.SelectAll),
                    iconCls: 'ipsla_SelectAll'
                    },
                    '-',
                    {
                        text: '&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_AK1_66;E=js}',
                        tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_67;E=js}',
                        handler: Function.createDelegate(this, this.DeselectAll),
                        iconCls: 'ipsla_DeselectAll'
                    }
                ]})
            });
            this.panel.show();
        }));
    },

    SelectAll: function() {
        this.treeControl.SelectAll(true);
    },

    DeselectAll: function() {
        this.treeControl.SelectAll(false);
    },

    dispose: function() {
        //Add custom dispose actions here
        Orion.Voip.Admin.ManageNodes.NpmNodesSelectionControl.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Admin.ManageNodes.NpmNodesSelectionControl.registerClass('Orion.Voip.Admin.ManageNodes.NpmNodesSelectionControl', Sys.UI.Control);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
