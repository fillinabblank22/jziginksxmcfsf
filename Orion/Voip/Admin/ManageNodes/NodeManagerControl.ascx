﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeManagerControl.ascx.cs" Inherits="Orion_Voip_Admin_Settings_NodeManagerControl" %>
<%@ Register TagPrefix="ipsla" TagName="NodePropertiesDialog" Src="~/Orion/Voip/Admin/ManageNodes/NodePropertiesDialog.ascx" %>

<style>
    #NodeManagerGrid img { vertical-align: middle; }
    #<%= grid.ClientID %> td { padding-bottom: 0px; padding-right: 0px; }
</style>

<div id="gridBox" style="width:800px;height:500px;">
<div id="grid" runat="server" class="ExtJsGrid" style="width:100%;height:100%;"></div>
</div>
<asp:UpdatePanel runat="server" ID="NodesToRemovePanel" UpdateMode="Conditional">
    <ContentTemplate>
    <div id="NodesAndOperationsToRemove" runat="server" class="ipsla_NodeManager" style="display:none">
        <div id="NodesToRemoveDiv" runat="server">
            <asp:Repeater runat="server" ID="NodesToRemoveRepeater" OnItemDataBound="ItemsToRemoveRepeater_OnItemDataBound">
                <HeaderTemplate><table border="0" width="95%" class="ipsla_NodeManager"></HeaderTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server">
                        <td><img src="<%# DataBinder.Eval(Container.DataItem, "StatusIcon") %>"
                                 alt="<%# DataBinder.Eval(Container.DataItem, "StatusText") %>"/>
                            <%# DataBinder.Eval(Container.DataItem, "SiteName") %> </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate></table></FooterTemplate>
            </asp:Repeater>
        </div>
        <div id="OperationsToRemoveDiv" runat="server">
            <asp:Repeater runat="server" ID="OperationsToRemoveRepeater" OnItemDataBound="ItemsToRemoveRepeater_OnItemDataBound">
                <HeaderTemplate>
                <table border="0" width="95%" class="ipsla_NodeManager">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr id="row" runat="server">
                        <td>
                            <img src="<%# GetOperationStatusImageUrl((short)DataBinder.Eval(Container.DataItem, "OperationStatusID")) %>"
                                 alt=""/>
                            <%# GetOperationName(DataBinder.Eval(Container.DataItem, "OperationName")) %>
                        </td>
                        <td style="width:12em;">
                            <img src="<%# GetOperationTypeImageUrl((short)DataBinder.Eval(Container.DataItem, "OperationTypeID")) %>"
                                 alt=""/>
                            <%# GetOperationTypeName((short)DataBinder.Eval(Container.DataItem, "OperationTypeID")) %>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate></table></FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
    </ContentTemplate>
    <Triggers>
    
    </Triggers>
</asp:UpdatePanel>

<ipsla:NodePropertiesDialog ID="nodePropertiesDialog" runat="server" />
