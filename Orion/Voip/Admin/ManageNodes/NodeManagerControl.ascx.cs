﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.IpSla.Web.DAL;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.IpSla.Web;
using System.Globalization;
using System.Text;
using System.Data;
using SolarWinds.Orion.IpSla.Data;
using System.Diagnostics;
using System.ServiceModel;

public partial class Orion_Voip_Admin_Settings_NodeManagerControl : ScriptUserControlBase, IPostBackEventHandler
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GetScriptManager().RegisterAsyncPostBackControl(this);
    }

    public Uri AddNodesUrl { get; set; }
    public Uri DiscoverNodesUrl { get; set; }

    #region ScriptControl Behavior

    #region IScriptControl Members

    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Orion.Voip.Admin.ManageNodes.NodeManagerControl", this.ClientID);
        descriptor.AddProperty("nodeIdsField", NodesStateKey);
        descriptor.AddProperty("gridHostElementId", this.grid.ClientID);
        descriptor.AddScriptProperty("InvokePostBack",
            "function() { " + this.Page.ClientScript.GetPostBackEventReference(this, "Refresh") + "; }"
            );
        descriptor.AddScriptProperty("InvokeRemoveNodesPostback",
            "function() { " + this.Page.ClientScript.GetPostBackEventReference(this, "RemoveNodes") + "; }"
            );
        descriptor.AddScriptProperty("InvokeAddNodesPostback",
            "function() { " + this.Page.ClientScript.GetPostBackEventReference(this, "AddNodes") + "; }"
            );
        descriptor.AddScriptProperty("InvokeDiscoverNodesPostback",
            "function() { " + this.Page.ClientScript.GetPostBackEventReference(this, "DiscoverNodes") + "; }"
            );
        descriptor.AddProperty("nodesToRemoveDivId", this.NodesToRemoveDiv.ClientID);
        descriptor.AddProperty("operationsToRemoveDivId", this.OperationsToRemoveDiv.ClientID);
        descriptor.AddComponentProperty("nodePropertiesDialog", this.nodePropertiesDialog.ClientID);
        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = this.ResolveUrl("NodeManagerControl.js");
        yield return reference;
    }

    #endregion

    private int[] siteIDs;

    protected string NodesStateKey
    {
        get { return "__NODES_" + this.ClientID; }
    }

    protected string SaveNodesToRemoveState()
    {
        var jss = new JavaScriptSerializer();
        return jss.Serialize(siteIDs);
    }

    protected void ItemsToRemoveRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item)
        {
            var row = (HtmlTableRow)e.Item.FindControl("row");
            row.Attributes["class"] = "alternate";
        }
    }

    #endregion

    /// <summary>
    /// Deserialize list of NodeIDs which should be removed,
    /// received from client.
    /// </summary>
    /// <param name="field"></param>
    protected void LoadNodeIdsClientState(string field)
    {
        var jss = new JavaScriptSerializer();
        siteIDs = jss.Deserialize<int[]>(field);
    }

    #region IPostBackEventHandler Members

    public void RaisePostBackEvent(string eventArgument)
    {
        switch (eventArgument)
        {
            case "Refresh":
                if (!string.IsNullOrEmpty(Request[NodesStateKey]))
                {
                    LoadNodeIdsClientState(Request[NodesStateKey]);
                    UpdateNodesToRemovePanel();
                }   
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "AfterRefresh",
                    "$find('" + this.ClientID + "').ShowConfirmRemoveNodesDialog();",
                    true);
                break;
            case "RemoveNodes":
                if (!string.IsNullOrEmpty(Request[NodesStateKey]))
                {
                    LoadNodeIdsClientState(Request[NodesStateKey]);
                    try
                    {
                        RemoveNodes();
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ReloadGrid",
                            "$find('" + this.ClientID + "').reloadGrid();",
                            true);
                    }
                    catch (EndpointNotFoundException)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ModuleEngineError",
                            "Ext.onReady(function(){Ext.MessageBox.alert('" + Resources.VNQMWebContent.VNQMWEBCODE_AK1_3 + "', '" + Resources.VNQMWebContent.VNQMWEBCODE_AK1_4 + "');});",
                            true);
                    }
                }
                break;
            case "AddNodes":
                Response.Redirect(AddNodesUrl.ToString());
                break;
            case "DiscoverNodes":
                Response.Redirect(DiscoverNodesUrl.ToString());
                break;
        }
    }

    private void SaveNode()
    {
    }

    private void RemoveNodes()
    {
        using (var blProxy = BusinessLayer.GetBusinessLayer())
        {
            blProxy.RemoveIpSlaNodes(siteIDs);
        }
    }

    protected class NodesToRemoveTableData
    {
        public string SiteName { get; set; }
        public string StatusIcon { get; set; }
        public string StatusText { get; set; }
    }

    private void UpdateNodesToRemovePanel()
    {
        if (siteIDs.Length > 0)
        {
            var nodesToRemoveDataList = new List<NodesToRemoveTableData>();
            foreach (int siteId in siteIDs)
            {
                SolarWinds.Orion.IpSla.Data.Site site = SitesDAL.Instance.GetSite(siteId);
                if (site != null)
                {
                    var status = new Status((OBJECT_STATUS)site.NodeStatus);
                    nodesToRemoveDataList.Add(new NodesToRemoveTableData
                        {
                            SiteName = site.Name,
                            StatusIcon = status.ToString("smallimgpath", null),
                            StatusText = status.ToString()
                        });
                }
            }
            NodesToRemoveRepeater.DataSource = nodesToRemoveDataList;
            NodesToRemoveRepeater.DataBind();


            var siteIdList = new StringBuilder();
            Array.ForEach<int>(siteIDs, siteId => siteIdList.AppendFormat("{0}{1}", siteIdList.Length > 0 ? "," : "", siteId.ToString(CultureInfo.InvariantCulture)));

            string swqlQuery = string.Format(@"SELECT DISTINCT oi.OperationInstanceID, oi.OperationName, oi.OperationTypeID, oi.OperationStatusID,
	            sourceSites.SiteID as SourceSiteID,
                sourceSites.Name as SourceSiteName
            FROM Orion.IpSla.Operations oi
            INNER JOIN Orion.IpSla.Sites sourceSites ON sourceSites.NodeID = oi.SourceNodeID
            WHERE (sourceSites.SiteID IN ({0})) AND oi.OperationStateID<>6
            ORDER BY OperationInstanceID", siteIdList.ToString());

            DataTable table = DALHelper.ExecuteQuery(swqlQuery);
            OperationsToRemoveRepeater.DataSource = table;
            OperationsToRemoveRepeater.DataBind();
            NodesToRemovePanel.Update();
        }
    }

    // Icon and text rendering methods for repeaters in nodes and operations rendering update panel
    protected string  GetOperationStatusImageUrl(int statusId)
    {
        return IpSlaOperationHelper.GetStatusImageUrl((OperationStatus)statusId);
    }

    protected string GetOperationTypeImageUrl(int operationTypeId)
    {
        return IpSlaOperationHelper.GetTypeImageUrl((OperationType)operationTypeId, false);
    }

    protected string GetOperationName(object operationName)
    {
        if (operationName == null) throw new ArgumentNullException("operationName");
        return IpSlaOperationHelper.FixName(operationName.ToString());
    }

    protected string GetOperationTypeName(int operationTypeId)
    {
        return GetLocalizedProperty("OperationType", ((OperationType)operationTypeId).ToString());
    }

    #endregion

    protected override void OnPreRender(EventArgs e)
    {
        Page.ClientScript.RegisterHiddenField(NodesStateKey, SaveNodesToRemoveState());
        base.OnPreRender(e);
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds());
    }
}
