﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddNodesControl.ascx.cs" Inherits="Orion_Voip_Admin_Settings_AddNodesControl" %>
<%@ Register src="NpmNodesSelectionControl.ascx" tagname="NpmNodesSelectionControl" tagprefix="ipsla" %>
<%@ Register src="NoAvailableNodesControl.ascx" tagname="NoAvailableNodesControl" tagprefix="ipsla" %>
<%@ Register src="AddNodesProcessControl.ascx" tagname="AddNodesProcessControl" tagprefix="ipsla" %>

<div id="nodesSelection">
    <ipsla:NpmNodesSelectionControl ID="NpmNodesSelectionControl" runat="server" />
    <ipsla:NoAvailableNodesControl ID="NoAvailableNodesControl" runat="server" />
</div>
<br />

<style type="text/css">
    .sw-btn-bar .sw-btn-cancel {margin-left: 10px;}
</style>

<div class="sw-btn-bar">
    <orion:LocalizableButton ID="AddNodes" runat="server"
           LocalizedText="CustomText" DisplayType="Primary"
           Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_65 %>"
           OnClientClick="if (CheckDemoMode()) return false;"
           OnClick="AddNodes_Click" />
    <orion:LocalizableButton ID="Cancel" runat="server"
           LocalizedText="Cancel" DisplayType="Secondary"
           OnClick="Cancel_Click" />
</div>

<ipsla:AddNodesProcessControl ID="AddNodesProcessControl" runat="server"
 ShowWriteCredentialsErrors="true" ShowResults="true" UsageContext="AddNodesControl" />


