﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Voip.Admin.ManageNodes");
Orion.Voip.Admin.ManageNodes.AddNodes = function(element) {
    Orion.Voip.Admin.ManageNodes.AddNodes.initializeBase(this, [element]);
}

Orion.Voip.Admin.ManageNodes.AddNodes.prototype = {

    initialize: function() {
        Orion.Voip.Admin.ManageNodes.AddNodes.callBaseMethod(this, 'initialize');
    },

    dispose: function() {
        //Add custom dispose actions here
        Orion.Voip.Admin.ManageNodes.AddNodes.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Admin.ManageNodes.AddNodes.registerClass('Orion.Voip.Admin.ManageNodes.AddNodes', Sys.UI.Control);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
