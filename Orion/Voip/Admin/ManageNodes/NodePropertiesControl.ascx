<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodePropertiesControl.ascx.cs" Inherits="Orion_Voip_Admin_ManageNodes_NodePropertiesControl" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="ipsla" TagName="CliConnectionControl" Src="~/Orion/Voip/Controls/Cli/CliConnectionControl.ascx" %>

<asp:ScriptManagerProxy ID="scriptManager" runat="server">
  <Services>
<%--    <asp:ServiceReference Path="~/Orion/Voip/Controls/Cli/CliConnectionControl.asmx" />
--%>  </Services>
</asp:ScriptManagerProxy>

<div class="ipsla_MaskArea">
<table class="ipsla_EditNodeBox" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr class="header">
            <td colspan="10">
                <%= VNQMWebContent.VNQMWEBDATA_TM0_80 %>
            </td>
        </tr>
        <tr class="input">
            <td class="col1Label">
                <%= VNQMWebContent.VNQMWEBDATA_TM0_82%>
            </td>
            <td colspan="9">
                <asp:TextBox ID="tbSiteName" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr class="header">
            <td colspan="10">
                <%= VNQMWebContent.VNQMWEBDATA_TM0_83%>
            </td>
        </tr>
        <tr class="input">
            <td class="col1Label">
                <%= VNQMWebContent.VNQMWEBDATA_TM0_86 %>
            </td>
            <td colspan="9">
                <asp:TextBox ID="tbSourceAddress" runat="server" style="vertical-align: middle;"></asp:TextBox>
                <orion:LocalizableButton runat="server" ID="btnDefault"  style="vertical-align: bottom;" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_87 %>" DisplayType="Small" />
            </td>
        </tr>
        <tr id="InvalidIpAdress" runat="server" class="error" visible="false">
            <td colspan="10">
                &nbsp;<%= VNQMWebContent.VNQMWEBDATA_TM0_84%>
            </td>
        </tr>
        <tr class="header">
            <td colspan="10">
                <%= VNQMWebContent.VNQMWEBDATA_TM0_85%>
            </td>
        </tr>
    </tbody>
    <ipsla:CliConnectionControl ID="cliConnection" runat="server" />
</table>
</div>