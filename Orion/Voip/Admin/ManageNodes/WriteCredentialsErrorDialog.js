﻿/// <reference name="MicrosoftAjax.js"/>
/// <reference path="~/Orion/js/jquery.js" />
/// <reference path="~/Orion/js/extjs/debug/Ext.js" />
/// <reference path="~/Orion/js/extjs/debug/jquery-bridge.js" />
/// <reference path="~/Orion/js/extjs/debug/ext-all-debug.js" />
/// <reference path="~/Orion/Voip/js/ScriptServiceInvoker.js" />
/// <reference path="~/Orion/Voip/js/ScriptServiceProxy.js" />
/// <reference path="~/Orion/Voip/js/FormMenu.js" />

Type.registerNamespace("Orion.Voip.Admin.ManageNodes");

Orion.Voip.Admin.ManageNodes.WriteCredentialsErrorDialog = function() {
    Orion.Voip.Admin.ManageNodes.WriteCredentialsErrorDialog.initializeBase(this);

    this.dialog = null;
    this.grid = null;
    this.reader = null;
    this.store = null;
    this.columnModel = null;
    this.selectionModel = null;
    this.snmpMenu = null;

    this.snmpInfo = null;
    this.snmpInfoElement = null;
    this.cliInfo = null;
    this.cliInfoElement = null;
    // defaults
    this.windowTitle = '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_33;E=js}';
    this.message = '';
    this.issueIconUrl = null;
    this.buttons = [];
    this.storeScriptServiceMethod = function() { return []; };
    this.storeScriptServiceMethodParams = {};
    this.onGridUpdateRequest = null;
};

Orion.Voip.Admin.ManageNodes.WriteCredentialsErrorDialog.prototype = {

    get_snmpInfo: function (value) {
        return this.snmpInfo;
    },
    set_snmpInfo: function (value) {
        this.snmpInfo = value;
    },

    get_snmpInfoElement: function (value) {
        return this.snmpInfoElement;
    },
    set_snmpInfoElement: function (value) {
        this.snmpInfoElement = value;
    },

    get_cliInfo: function (value) {
        return this.cliInfo;
    },
    set_cliInfo: function (value) {
        this.cliInfo = value;
    },

    get_cliInfoElement: function (value) {
        return this.cliInfoElement;
    },
    set_cliInfoElement: function (value) {
        this.cliInfoElement = value;
    },

    initialize: function () {
        Orion.Voip.Admin.ManageNodes.WriteCredentialsErrorDialog.callBaseMethod(this, 'initialize');

        // Add custom initialization here
    },

    get_WindowTitle: function () {
        return this.windowTitle;
    },
    set_WindowTitle: function (value) {
        this.windowTitle = value;
    },

    get_Message: function () {
        return this.message;
    },
    set_Message: function (value) {
        this.message = value;
    },

    get_IssueIconUrl: function () {
        return this.issueIconUrl;
    },
    set_IssueIconUrl: function (value) {
        this.issueIconUrl = value;
    },

    get_Buttons: function () {
        return this.buttons;
    },
    set_Buttons: function (value) {
        this.buttons = value != null ? value : [];
    },

    get_StoreScriptServiceMethod: function () {
        return this.storeScriptServiceMethod;
    },
    set_StoreScriptServiceMethod: function (value) {
        this.storeScriptServiceMethod = value != null ? value : function () { return []; };
    },
    get_StoreScriptServiceMethodParams: function () {
        return this.storeScriptServiceMethodParams;
    },
    set_StoreScriptServiceMethodParams: function (value) {
        this.storeScriptServiceMethodParams = value != null ? value : {};
    },

    get_OnGridUpdateRequest: function () {
        return this.onGridUpdateRequest;
    },
    set_OnGridUpdateRequest: function (value) {
        this.onGridUpdateRequest = value;
    },

    ShowDialog: function () {
        if (this.dialog == null) {
            this.reader = new Ext.data.JsonReader({
                root: 'DataTable.Rows',
                totalProperty: 'TotalRows',
                id: '0'
            }, [
                    { name: 'NodeId', type: 'int', mapping: 0 },
                    { name: 'NodeName', type: 'string', mapping: 1 },
                    { name: 'IpAddress', type: 'string', mapping: 2 },
                    { name: 'EngineId', type: 'int', mapping: 3 },
                    { name: 'Message', type: 'string', mapping: 4 },
                    { name: 'Error', type: 'boolean', mapping: 5 },
                    { name: 'InProgress', type: 'boolean', mapping: 6 }
                ]);

            this.store = new Ext.data.Store({
                id: 'store',
                proxy: new Ext.data.ScriptServiceProxy({
                    scriptServiceMethod: this.storeScriptServiceMethod
                }),
                reader: this.reader,
                sortInfo: { field: 'NodeName', direction: "ASC" },
                baseParams: this.storeScriptServiceMethodParams
            });

            this.selectionModel = new Ext.grid.CheckboxSelectionModel({ singleSelect: false });
            this.selectionModel.on("selectionchange", Function.createDelegate(this, this.UpdateButtonStates));

            this.columnModel = new Ext.grid.ColumnModel(
                [this.selectionModel,
                {
                    header: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_34;E=js}',
                    dataIndex: 'NodeName',
                    width: 150,
                    resizable: true,
                    renderer: function (value, meta, record) {
                        return '<span style="vertical-align:middle;">' + value + '</span>';
                    }
                }, {
                    header: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_35;E=js}',
                    dataIndex: 'Message',
                    width: 450,
                    resizable: true,
                    renderer: Function.createDelegate(this, function (value, meta, record) {
                        var imgSrc = '';
                        var alt = '';
                        if (record.data.InProgress) {
                            imgSrc = '/Orion/Voip/images/loading_gen_16x16.gif'
                            alt = '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_36;E=js}';
                        }
                        else if (record.data.Error) {
                            imgSrc = this.issueIconUrl || '/Orion/Voip/images/stop_16x16.gif';
                            alt = '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_37;E=js}';
                        }
                        else {
                            imgSrc = '/Orion/images/Check.Green.gif';
                            alt = '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_38;E=js}';
                        }
                        return String.format('<div style="float:left;padding-right:0.25em;"><img src=\"{0}\" alt=\"{1}\" style="vertical-align:middle;" /><span style="vertical-align:middle;display: inline-block; padding: 3px;" title="{3}"> {2}</span></div>', imgSrc, alt, value.replace('\n', '<br/>'), value);
                    })
                }, {
                    header: '',
                    fixed: true,
                    sortable: false,
                    menuDisabled: true,
                    width: 1,
                    renderer: function () { return ''; }
                }]
            );
            this.columnModel.defaultSortable = true;

            var toolbar =
                [{
                    id: "btnSelectSuccessful",
                    text: '&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_AK1_39;E=js}',
                    tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_39;E=js}',
                    handler: Function.createDelegate(this, this.SelectSuccessfulNodes),   // Confirm before deleting
                    iconCls: 'ipsla_SelectSuccessful'
                }, { id: "btnSelectErrors",
                    text: '&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_AK1_40;E=js}',
                    tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_40;E=js}',
                    handler: Function.createDelegate(this, this.SelectErrorNodes),   // Confirm before deleting
                    iconCls: 'ipsla_SelectErrors'
                }];

            if (this.cliInfoElement != null) {
                this.cliMenu = new Ext.menu.FormMenu({
                    cls: 'customMenu',
                    items: [
                        new Ext.Panel({
                            border: false,
                            frame: false,
                            monitorResize: true,
                            shadow: false,
                            autoWidth: true,
                            autoHeight: true,
                            //width: 800,
                            contentEl: this.cliInfoElement,
                            hideOnClick: false,
                            buttons: [{
                                text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_41;E=js}',
                                handler: Function.createDelegate(this, this.SaveCliCredentials)
                            },
                            {
                                text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_26;E=js}',
                                handler: Function.createDelegate(this, function () {
                                    this.cliMenu.hide();
                                })
                            }]
                        })],
                    shadow: false,
                    listeners: {
                        'beforeshow': Function.createDelegate(this, this.OnBeforeShowCliEditor),
                        'show': function (mnu){  
                            Function.createDelegate(this, this.UpdateButtonStates);
                
                            // Cancel menu navigation to preserve panel navigation. 
                            mnu.keyNav.destroy();
                        },
                        'hide': Function.createDelegate(this, this.UpdateButtonStates)
                    }
                });

                toolbar.splice(0, 0,
                    new Ext.Button({
                        id: "btnCliEdit",
                        text: this.snmpInfoElement != null ? '&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_AK1_42;E=js}' : '&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_AK1_43;E=js}',
                        tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_42;E=js}',
                        arrowAlign: 'right',
                        arrowHandler: null,
                        iconCls: 'ipsla_Edit_Creds',
                        menu: this.cliMenu
                    },
                    '-')
                );
            }

            if (this.snmpInfoElement != null) {
                this.snmpMenu = new Ext.menu.FormMenu({
                    cls: 'customMenu',
                    items: [
                        new Ext.Panel({
                            border: false,
                            frame: false,
                            monitorResize: true,
                            shadow: false,
                            autoWidth: true,
                            autoHeight: true,
                            contentEl: this.snmpInfoElement,
                            hideOnClick: false,
                            buttons: [{
                                text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_41;E=js}',
                                handler: Function.createDelegate(this, this.SaveSnmpCredentials)
                            },
                            {
                                text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_26;E=js}',
                                handler: Function.createDelegate(this, function () {
                                    this.snmpMenu.hide();
                                })
                            }]
                        })],
                    shadow: false,
                    listeners: {
                        'beforeshow': Function.createDelegate(this, this.OnBeforeShowSnmpEditor),
                        'show': Function.createDelegate(this, this.OnShowSnmpEditor),
                        'hide': Function.createDelegate(this, this.UpdateButtonStates)
                    }
                });

                if (this.cliInfoElement == null)
                    toolbar.splice(0, 0, '-');
                toolbar.splice(0, 0,
                    new Ext.Button({
                        id: "btnSnmpEdit",
                        text: this.cliInfoElement != null ? '&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_AK1_44;E=js}' : '&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_AK1_45;E=js}',
                        tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_45;E=js}',
                        arrowAlign: 'right',
                        arrowHandler: null,
                        iconCls: 'ipsla_EditCredentials',
                        menu: this.snmpMenu
                    })
                );
            }

            this.grid = new Ext.grid.EditorGridPanel({
                id: 'grid',
                store: this.store,
                cm: this.columnModel,
                selModel: this.selectionModel,
                enableColLock: false,
                title: this.get_Message(),
                anchor: '100% 100%',
                autoScroll: true,
                hidden: false,
                loadMask: true,
                tbar: toolbar
            });

            this.dialog = new Ext.Window({
                title: this.get_WindowTitle(),
                width: 770,
                height: 500,
                minWidth: 400,
                minHeight: 300,
                closeAction: 'hide',
                closable: false,
                modal: true,
                layout: 'fit',
                items: [this.grid],
                buttons: this.buttons
            });
        }

        this.dialog.show(this);
        this.store.load();
        this.UpdateButtonStates();
    },

    CloseDialog: function () {
        this.dialog.hide();
    },

    OnBeforeShowSnmpEditor: function () {
        this.FixMenuZOrder(this.snmpMenu);
        var selections = this.selectionModel.getSelections();
        var validationIp = null;
        var validationEngineId = null;
        if (this.selectionModel.getCount() == 1) {
            validationIp = selections[0].data.IpAddress;
            validationEngineId = selections[0].data.EngineId;
        }
        this.snmpInfo.set_ValidationIp(validationIp);
        this.snmpInfo.set_ValidationEngineId(validationEngineId);
        this.snmpInfo.PreFillNodeCredentials(this.GetSelectedNodeIds());
    },

    OnShowSnmpEditor: function () {
        this.UpdateButtonStates();
        if (Ext.isIE6) {
            this.snmpInfo.ddlSnmpVersion_Changed();
        }
    },

    OnBeforeShowCliEditor: function () {
        this.FixMenuZOrder(this.cliMenu);
        var selections = this.selectionModel.getSelections();
        var validationIp = null;
        var validationEngineId = null;
        if (this.selectionModel.getCount() == 1) {
            validationIp = selections[0].data.IpAddress;
            validationEngineId = selections[0].data.EngineId;
        }
        this.cliInfo.set_ValidationIp(validationIp);
        this.cliInfo.set_ValidationEngineId(validationEngineId);
        this.cliInfo.PreFillNodeCredentials(this.GetSelectedNodeIds());
    },

    FixMenuZOrder: function (menu) {
        if (!Ext.isIE6) {
            var menuEl = menu.getEl();
            var posCfg = menuEl.getPositioning();
            posCfg['z-index'] = '10500';
            menuEl.setPositioning(posCfg);
        }
    },

    UpdateButtonStates: function () {
        var count = this.selectionModel.getCount();
        var map = this.grid.getTopToolbar().items.map;

        var dropDownVisible = false;
        if (typeof (map.btnSnmpEdit) === 'object') {
            map.btnSnmpEdit.setDisabled(count === 0);
            dropDownVisible |= !map.btnSnmpEdit.menu.hidden;
        }
        if (typeof (map.btnCliEdit) === 'object') {
            map.btnCliEdit.setDisabled(count === 0);
            dropDownVisible |= !map.btnCliEdit.menu.hidden;
        }
        map.btnSelectSuccessful.setDisabled(dropDownVisible);
        map.btnSelectErrors.setDisabled(dropDownVisible);

        for (var b = 0; b < this.buttons.length; b++) {
            var btnDesc = this.buttons[b];
            if (typeof (btnDesc.id) === 'string' && typeof (btnDesc.enableWhenSelection) === 'boolean') {
                for (var i = 0; i < this.dialog.buttons.length; i++) {
                    if (this.dialog.buttons[i].id == this.buttons[b].id) {
                        this.dialog.buttons[i].setDisabled(count === 0);
                    }
                }
            }
        }
    },

    SelectSuccessfulNodes: function () {
        this.SelectNodes(false);
    },

    SelectErrorNodes: function () {
        this.SelectNodes(true);
    },

    SelectNodes: function (error) {
        //var recordsToSelect = this.store.query("Error", error, true).items;
        var recordsToSelect = this.store.queryBy(function (record, id) {
            return (!record.data.InProgress && record.data.Error == error);
        });
        this.selectionModel.selectRecords(recordsToSelect.items);
    },

    SaveSnmpCredentials: function () {
        this.snmpInfo.SaveNodeCredentials(this.GetSelectedNodeIds(),
        Function.createDelegate(this, this.OnSaveCredentialsSucceed),
        Function.createDelegate(this, this.OnSaveCredentialsError));
    },

    OnSaveCredentialsSucceed: function () {
        var nodeIds = this.GetSelectedNodeIds();

        for (var i = 0; i < nodeIds.length; i++) {
            var record = this.grid.store.getById(nodeIds[i]);
            if (record != null) {
                record.data.InProgress = true;
                record.data.Message = '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_36;E=js}';
                record.data.Error = false;
            }
        }
        this.grid.getView().refresh();

        if (this.onGridUpdateRequest != null) {
            this.onGridUpdateRequest(nodeIds);
        }

        if (this.snmpMenu) this.snmpMenu.hide();
        if (this.cliMenu) this.cliMenu.hide();
    },

    OnSaveCredentialsError: function (error) {
        Ext.MessageBox.alert('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_46;E=js}', String.format('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_47;E=js}', error.get_message()));
        if (this.snmpMenu) this.snmpMenu.hide();
        if (this.cliMenu) this.cliMenu.hide();
    },

    SaveCliCredentials: function () {
        this.cliInfo.SaveNodeCredentials(this.GetSelectedNodeIds(),
            Function.createDelegate(this, this.OnSaveCredentialsSucceed),
            Function.createDelegate(this, this.OnSaveCredentialsError));
    },


    RefreshGridItems: function (receivedData) {
        var data = this.reader.readRecords(receivedData);
        var records = data.records;

        if (records.length > 0) {
            for (var i = 0; i < records.length; i++) {
                var record = this.grid.store.getById(records[i].data.NodeId);
                if (record != null) {
                    record.data.InProgress = records[i].data.InProgress;
                    record.data.Message = (record.data.InProgress ? '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_36;E=js}' : records[i].data.Message);
                    record.data.Error = records[i].data.Error;
                }
            }
            this.grid.getView().refresh();
        }
    },

    GetSelectedNodeIds: function () {
        var selections = this.selectionModel.getSelections();
        var selectedNodes = [];
        for (var i = 0; i < this.selectionModel.getCount(); i++) {
            selectedNodes.push(selections[i].data.NodeId);
        }
        return selectedNodes;
    },

    dispose: function () {
        //Add custom dispose actions here
        Orion.Voip.Admin.ManageNodes.WriteCredentialsErrorDialog.callBaseMethod(this, 'dispose');
    }
};
Orion.Voip.Admin.ManageNodes.WriteCredentialsErrorDialog.registerClass('Orion.Voip.Admin.ManageNodes.WriteCredentialsErrorDialog', Sys.Component);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
