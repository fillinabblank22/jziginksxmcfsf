using System;
using System.Collections.Generic;
using System.Web.UI;
using AjaxControlToolkit;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_Admin_ManageNodes_NodePropertiesControl : ScriptUserControlBase
{
	private static readonly Log log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        this.InvalidIpAdress.Visible = false;
        this.tbSourceAddress.Style.Remove("background-color");

        this.btnDefault.Attributes["onclick"] = "$find('" + this.ClientID + "').btnDefault_Click(); return false;";
    }

    protected override void RenderWrapperElementBegin(HtmlTextWriter writer)
    {
        writer.AddAttribute(HtmlTextWriterAttribute.Id, this.ClientID);
        writer.AddAttribute(HtmlTextWriterAttribute.Style, "display: none;");
        writer.RenderBeginTag(HtmlTextWriterTag.Span);
    }

    protected override void RenderWrapperElementWithContent(HtmlTextWriter writer)
    {
        this.RenderWrapperElementBegin(writer);
        this.RenderWrapperElementEnd(writer);
        this.RenderContent(writer);
    }

    #region IScriptControl Members

    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        var descriptor = new ScriptControlDescriptor("Orion.Voip.Admin.ManageNodes.NodePropertiesControl", this.ClientID);
        descriptor.AddElementProperty("tbSiteName", this.tbSiteName.ClientID);
        descriptor.AddElementProperty("tbSourceAddress", this.tbSourceAddress.ClientID);
        descriptor.AddComponentProperty("cliConnection", this.cliConnection.ClientID);
        descriptor.AddElementProperty("btnDefault", this.btnDefault.ClientID);
        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        yield return new ScriptReference(this.ResolveUrl("NodePropertiesControl.js"));
    }

    #endregion
}
