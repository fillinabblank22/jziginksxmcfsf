﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_Voip_Controls_Settings_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        NodeManagerControl1.AddNodesUrl =
            new Uri(Request.Url, "AddNodes.aspx");
        NodeManagerControl1.DiscoverNodesUrl =
            new Uri(Request.Url, "../OperationsWizard.aspx?Flow=NodeDiscovery");
    }
}
