﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Voip/Admin/Config.master" AutoEventWireup="true" CodeFile="ManageOperations.aspx.cs" 
    Inherits="Orion_Voip_Admin_ManageOperations" Title="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_4 %>" %>
<%@ Import Namespace="Resources" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagName="IncludeExtJs" TagPrefix="orion" %>
<%@ Register Src="~/Orion/Voip/Controls/EditOperation.ascx" TagPrefix="voip" TagName="EditOperation" %>	
<%@ Register Src="~/Orion/Voip/Controls/EditSLALocation.ascx" TagPrefix="voip" TagName="EditSLALocation" %>	
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
	
<asp:Content ID="contentHead" ContentPlaceHolderID="contentPlaceHolderHead" Runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/Voip.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/styles/EditOperation.css" />
    <orion:Include ID="IncludeExtJs" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    
	<script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
	<script type="text/javascript" src="/Orion/Voip/js/ManageOperations.js"></script>
	
	<style type="text/css">
        .add { background-image:url(/Orion/Voip/images/add_16x16.gif) !important; }
        .editlocation { background-image:url(/Orion/Voip/images/editlocation_16x16.png) !important; }
        
        .edit { background-image:url(/Orion/Voip/images/edit_16x16.gif) !important; }
        .del { background-image:url(/Orion/Voip/images/delete_16x16.gif) !important; }
		.OperationsTable img
		{
			vertical-align: middle;
		}
		.GroupSection
		{
			background: #e2e2e1;
			padding: 5px;
			background-image: url(/Orion/Voip/images/Admin/bg.groupby.gif);
			background-repeat: repeat-x;
			
		}
		
		.Grouping
		{
			border: 1px #D0D0D0 solid;
			background: white;
		}
        
		.GroupItems
		{
			overflow: auto;
		}

		.SelectedGroupItem
		{
			background-color:#97D6FF;
			background-image:url(/Orion/Nodes/images/background/left_selection_gradient.gif);
			background-repeat:repeat-x;
			font-weight:bold;
		}
		
		li.GroupItem
		{
			padding-left: 5px;
			padding-right: 5px;
			padding-top: 2px;
			padding-bottom: 2px;
		}

		.GroupItem a
		{
			padding-left: 25px;
            background-repeat: no-repeat;
            text-overflow: ellipsis;
            overflow: hidden;
            width: 180px;
            display: block;
			
		}

        #operationsDiv td, #delDialog td { padding-right: 0; padding-bottom: 0; }
        #delDialog .x-btn-center  
        {
            padding: 0 5px;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;     
        }
        
        .ipsla_manualOperationColumn
        {
            font-weight: bold;
        }
        
        .ipsla_manualOperationNotice
        {
            font-weight: normal;
            padding: 0 0 0 22px;
            background-image: url(/orion/voip/images/icon.info_16.gif);
            background-repeat: no-repeat;
            background-position: left center;
        }
        #editDialog .main td { white-space: normal; }
        .x-panel-btns td.x-toolbar-cell { padding: 3px !important; }
        td.x-btn-mc { text-align: center !important; }
    </style>

</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="Breadcrumbs">
    <div class="sw-hdr-breadbox">
        <span><a href="../../admin"><%= VNQMWebContent.VNQMWEBDATA_VB1_6 %></a> &gt; <a href="default.aspx"><%= VNQMWebContent.VNQMWEBDATA_VnqmSettingsLinkName %></a> &gt;</span>
    </div>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton ID="HelpButton1" runat="server"  HelpUrlFragment="OrionIPSLAManagerAdministratorGuide-EditingorDeletingIPSLAOperations" />
</asp:Content>

<asp:Content ID="contentBody" runat="server" ContentPlaceHolderID="contentPlaceHolderBody" >

    
    <span class="IpSlaPageTitle"><%= Page.Title %></span>
                    
    
    <div style="width:1175px; padding-top: 10px;" id="operationsDiv">	
		<table class="OperationsTable" cellpadding="0" cellspacing="0" width="100%" style="padding-right: 10px;">
			<tr valign="top" align="left">
				<td style="padding-right: 10px;width: 210px;">
					<div class="Grouping" style="width:210px;">
						<div class="GroupSection">
							<div><%= VNQMWebContent.VNQMWEBDATA_AK1_68 %></div>
							<select id="groupByProperty" style="width:100%">
								<option value=""><%= VNQMWebContent.VNQMWEBDATA_TM0_38%></option>
								<option value="OperationStatus"><%= VNQMWebContent.VNQMWEBDATA_TM0_39 %></option>
								<option value="OperationType"><%= VNQMWebContent.VNQMWEBDATA_TM0_40 %></option>
								<option value="DisplaySource"><%= VNQMWebContent.VNQMWEBDATA_TM0_41 %></option>
								<option value="DisplayTarget"><%= VNQMWebContent.VNQMWEBDATA_TM0_42 %></option>
							</select>
						</div>
						<ul class="GroupItems" ></ul>
					</div>
				</td>
				<td id="gridCell">
					<div id="Grid"/>
				</td>
			</tr>
		</table>
	</div>

    <script type="text/javascript">
        var EditorLocationLoadButtonID = '<%=this.EditorLocationLoadButton.ClientID %>';
        var EditorLocationCancelButtonID = '<%=this.EditorLocationCancelButton.ClientID %>';
        var EditorLocationSubmitButtonID = '<%=this.EditorLocationSubmitButton.ClientID %>';
        var CallManagersCount = '<%=this.CallManagersCount.ClientID %>';
        var EditorLoadButtonID = '<%= this.EditorLoadButton.ClientID %>';
        var EditorCancelButtonID = '<%= this.EditorCancelButton.ClientID %>';
        var EditorSubmitButtonID = '<%= this.EditorSubmitButton.ClientID %>';
        var EditOperationID = '<%= this.editOp.ClientID %>';
        var SelectedOperationID = '<%= this.SelectedOperationID.ClientID %>';
        var SelectedOperationIDs = '<%= this.SelectedOperationIDs.ClientID %>';
    </script>

    <div id="editDialog" class="x-hidden">
        <div class="x-window-header"><%= VNQMWebContent.VNQMWEBDATA_TM0_43 %></div>
        
        <div class="x-window-body">
        <asp:UpdatePanel ID="upEditor" runat="server">
            <ContentTemplate>
                <asp:PlaceHolder runat="server" ID="editDialogPanel" Visible="false" >
                    <voip:EditOperation runat="server" ID="editOp" />
                    <br />
                </asp:PlaceHolder>
                <asp:PlaceHolder runat="server" ID="editDialogLoading" Visible="true" >
                    <table style="width:100%; height:300px;"><tr><td style="text-align:center; vertical-align:middle;"> 
                        <img src="/Orion/Voip/images/loading_gen_16x16.gif" style="padding-right:10px;" />
                        <%= VNQMWebContent.VNQMWEBDATA_TM0_44 %>
                    </td></tr></table>
                </asp:PlaceHolder>
                <asp:HiddenField ID="SelectedOperationIDs" runat="server" />
                <asp:Button ID="EditorLoadButton" runat="server" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_46 %>" OnClick="EditorLoadButton_Click" CssClass="x-hidden" CausesValidation="false" />
                <asp:Button ID="EditorCancelButton" runat="server" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_VB1_5 %>" OnClick="EditorCancelButton_Click" CssClass="x-hidden" CausesValidation="false" />
                <asp:Button ID="EditorSubmitButton" runat="server" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_47 %>" OnClick="EditorSubmitButton_Click" CssClass="x-hidden" />
            </ContentTemplate>
        </asp:UpdatePanel>
        </div>
    </div>
    
    <div id="editLocationDialog" class="x-hidden">
        <div class="x-window-header"><%= VNQMWebContent.VNQMWEBDATA_TM0_45 %></div>
        
        <div class="x-window-body">
        <asp:UpdatePanel ID="slaEditor" runat="server">
            <ContentTemplate>
                <asp:PlaceHolder runat="server" ID="editLocationPanel" Visible="false" >
                    <voip:EditSLALocation runat="server" ID="editsla" />
                    <br />
                </asp:PlaceHolder>
                <asp:PlaceHolder runat="server" ID="editLocationLoading" Visible="true" >
                    <table style="width:100%; height:300px;"><tr><td style="text-align:center; vertical-align:middle;"> 
                        <img src="/Orion/Voip/images/loading_gen_16x16.gif" style="padding-right:10px;" />
                        <%= VNQMWebContent.VNQMWEBDATA_TM0_44 %>
                    </td></tr></table>
                </asp:PlaceHolder>
                <asp:HiddenField ID="SelectedOperationID" runat="server" />
                <asp:HiddenField ID="NullTargetNode" runat="server" />
                <asp:Button ID="EditorLocationLoadButton" runat="server" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_46 %>" OnClick="EditorLocationLoadButton_Click"  CssClass="x-hidden" CausesValidation="false" />
                <asp:Button ID="EditorLocationCancelButton" runat="server" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_VB1_5 %>" OnClick="EditorLocationCancelButton_Click" CssClass="x-hidden" CausesValidation="false" />
                <asp:Button ID="EditorLocationSubmitButton" runat="server" Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_47 %>" OnClick="EditorLocationSubmitButton_Click" CssClass="x-hidden" />
            </ContentTemplate>
        </asp:UpdatePanel>
        </div>
    </div>
    <div id="addCallManagerDialog" class="x-hidden">
        <div class="x-window-header">
            <%= addCallManager %></div>
        <div class="x-window-body">
            <asp:UpdatePanel ID="addCallManagerUpdatePanel" runat="server">
                <ContentTemplate>
                    <asp:PlaceHolder runat="server" ID="addCallManagerPanel">
                        <div class="voipManageOperationsCallManagerDiv">
                            <%= addCallManagerText %>
                        </div>
                    </asp:PlaceHolder>
                    <asp:HiddenField ID="CallManagersCount" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
