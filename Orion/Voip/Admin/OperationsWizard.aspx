﻿<%@ Page Title="<%$ Resources : VNQMWebContent, VNQMWEBCODE_VB1_9%>" Language="C#" MasterPageFile="~/Orion/Voip/Admin/Config.master" AutoEventWireup="true" CodeFile="OperationsWizard.aspx.cs" Inherits="Orion_Voip_Admin_OperationsWizard" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web"%>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagName="IncludeExtJs" TagPrefix="orion" %>
<%@ Register src="~/orion/voip/controls/wizards/Operations/OperationsWizardControlContainer.ascx" tagname="OperationsWizardControlContainer" tagprefix="ipsla" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="contentHead" ContentPlaceHolderID="contentPlaceHolderHead" Runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/styles/Admin.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/styles/Wizard.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/styles/Discovery.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/Voip.css" />
    <orion:Include ID="IncludeExtJs" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/styles/EditOperation.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/styles/SnmpInfo.css" />
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="TopRightPageLinks">
     <orion:IconHelpButton ID="IconHelpButtonOW" runat="server" HelpUrlFragment="" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="Breadcrumbs">
    <div class="sw-hdr-breadbox">
        <span><a href="../../admin"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_6 %></a> &gt; <a href="default.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_VnqmSettingsLinkName %></a> &gt;</span>
    </div>
</asp:Content>

<asp:Content ID="contentBody" runat="server" ContentPlaceHolderID="contentPlaceHolderBody" >
<div id="wizardControl">
<ipsla:OperationsWizardControlContainer ID="operationsWizardControlContainer" runat="server"
    ReturnUrl="~/Orion/Voip/Summary.aspx" />
</div>
</asp:Content>