<%@ Page Language="C#" MasterPageFile="~/Orion/Voip/Admin/Config.master" AutoEventWireup="true" CodeFile="Infrastructure.aspx.cs" 
Inherits="Voip_Infrastructure" Title="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_58 %>" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ignav" Namespace="Infragistics.WebUI.UltraWebNavigator" Assembly="Infragistics2.WebUI.UltraWebNavigator.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>

<asp:Content ID="contentHead" ContentPlaceHolderID="contentPlaceHolderHead" Runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/styles/Admin.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/Voip.css" />
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="Breadcrumbs">
    <div class="sw-hdr-breadbox">
        <span><a href="../../admin"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_6 %></a> &gt; <a href="default.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_VnqmSettingsLinkName %></a> &gt;</span>
    </div>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionVoIPMonitorPHPageInfrastructure" />
</asp:Content>

<asp:Content ID="contentBody" runat="server" ContentPlaceHolderID="contentPlaceHolderBody" >
	<h1><%=Title %></h1>

	<div style="width: 600px;" >
	<p style="width:100%"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_59 %></p>

	<p style="width:100%"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_60 %></p>
	</div>
	
	<div style="width: 600px;">
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">

	<ContentTemplate>

	<div style="width: 400px;float:left;clear:right;padding-top: 10px;">
	<table style="border-spacing: 0px;">
        <td>
            <orion:LocalizableButton ID="Select" runat="server"
                LocalizedText="CustomText" DisplayType="Small" 
                Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_61 %>"
                ToolTip="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_61 %>"
                OnClientClick="if (CheckDemoMode()) return false;"
		        OnClick="Select_Click" />
        </td>
        <td>
            <orion:LocalizableButton ID="DeSelect" runat="server"
                LocalizedText="CustomText" DisplayType="Small"
                Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_62 %>"
                ToolTip="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_62 %>"
                OnClientClick="if (CheckDemoMode()) return false;"
		        OnClick="DeSelect_Click" />
        </td>
    </table>
	<ignav:UltraWebTree ID="VendorNodeInterfaceTree" runat="server" DefaultImage="" Indentation="20" DataMember="Vendors">
		<Levels>
			<ignav:Level Index="0" LevelCheckBoxes="False" LevelKeyField="Vendor" RelationName="VendorNode" ColumnName="Vendor" />
			<ignav:Level Index="1" LevelCheckBoxes="True" LevelKeyField="NodeID" RelationName="NodeInterface" ColumnName="Caption" CheckboxColumnName="IsInfrastructure" />
			<ignav:Level Index="2" LevelCheckBoxes="True" LevelKeyField="NodeID,InterfaceID" ColumnName="InterfaceName" CheckboxColumnName="IsInfrastructure" />
		</Levels>
	</ignav:UltraWebTree>
	</div>
	
	</ContentTemplate>
    </asp:UpdatePanel>

	</div>
	
	<div class="sw-btn-bar" style="clear:both;padding-top:24px;">
	    <orion:LocalizableButton id="ButtonBack" runat="server" onclick="ButtonBack_Click" LocalizedText="Back" DisplayType="Secondary" CausesValidation="false" />
	    <orion:LocalizableButton id="ImageButton1" runat="server" onclick="ButtonFinish_Click" LocalizedText="Ok" DisplayType="Primary" CausesValidation="false" />
	</div>
	
 </asp:Content>
