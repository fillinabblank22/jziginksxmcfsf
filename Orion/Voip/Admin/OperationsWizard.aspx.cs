﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Web.Wizards;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;

public partial class Orion_Voip_Admin_OperationsWizard : System.Web.UI.Page, IPostBackEventHandler
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
        Response.Cache.SetNoStore();
        Response.AppendHeader("Pragma", "no-cache");

        OperationsWizardFlow flow;
        switch ((Page.Request.QueryString["Flow"] ?? string.Empty).ToLower())
        {
            case "nodediscovery":
                flow = OperationsWizardFlow.NodeDiscovery;
                IconHelpButtonOW.HelpUrlFragment = "OrionIPSLAManagerAdministratorGuide-DiscoveringIPSLA-CapableNodesAutomatically";
                break;
            case "gettingstarted":
                flow = OperationsWizardFlow.GettingStarted;
                IconHelpButtonOW.HelpUrlFragment = "OrionIPSLAManagerAGOperationsAdding";
                break;
            default:
                flow = OperationsWizardFlow.OperationSetup;
                IconHelpButtonOW.HelpUrlFragment = "OrionIPSLAManagerAGOperationsAdding";                
                break;
        }

        if (OperationsWizardSession.Exists)
        {
            var session = OperationsWizardSession.Current;
            if (session.Data.IsEmpty)
            {
                if (session.Data.Flow != flow)
                {
                    OperationsWizardSession.NewCurrent(new OperationsWizardSession(flow), true);
                }
            }
            else
            {
                var currentPart = session.CurrentPart;
                System.Diagnostics.Debug.Assert(currentPart != null);

                var referrer = Request.UrlReferrer;
                if (referrer == null || StringComparer.OrdinalIgnoreCase.Compare(referrer.AbsoluteUri, Request.Url.AbsoluteUri) != 0)
                {
                    if (currentPart.WarnBeforeJoin)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ContinueInSession", @"
Ext.onReady(function(){
    Ext.MessageBox.show({
        title: '" + Resources.VNQMWebContent.VNQMWEBCODE_VB1_10 + @"',
        icon: Ext.MessageBox.QUESTION,
        msg: '" + Resources.VNQMWebContent.VNQMWEBCODE_VB1_11 + @"',
        closable: false,
        buttons: { yes: '" + Resources.VNQMWebContent.VNQMWEBCODE_VB1_12 + @"', no: '" + Resources.VNQMWebContent.VNQMWEBCODE_VB1_13 + @"' },
        fn: function(btn, value) {
            if (btn == 'no') {"
                        + ClientScript.GetPostBackEventReference(this, "Reset") + @";
            }
        }
    });
});
", true);
                    }
                    else if (currentPart.ResetBeforeJoin)
                    {
                        session.Close();
                        OperationsWizardSession.NewCurrent(new OperationsWizardSession(flow), true);
                    }
                }
            }
        }
        else
        {
            OperationsWizardSession.NewCurrent(new OperationsWizardSession(flow), true);
        }
    }

    #region IPostBackEventHandler Members

    void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
    {
        switch (eventArgument)
        {
            case "Reset":
                OperationsWizardSession.Current.Close();
                Server.Transfer(Request.Url.PathAndQuery);
                break;
        }
    }

    #endregion
}
