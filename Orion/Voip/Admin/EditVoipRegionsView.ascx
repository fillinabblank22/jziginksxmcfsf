﻿<%@ Control Language="C#" ClassName="EditVoipRegionsView" Inherits="SolarWinds.Orion.Web.UI.ProfilePropEditUserControl" %>
<%@ Register Src="~/Orion/Controls/SelectViewForViewType.ascx" TagPrefix="orion" TagName="SelectView" %>

<script runat="server">
	public override string PropertyValue
	{
		get { return ViewSelector.PropertyValue; }
		set { ViewSelector.PropertyValue = value; }
	}
</script>

<orion:SelectView runat="server" ID="ViewSelector" AllowViewsByDeviceType="false" ViewType="Voip Region" />