<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/Voip/Admin/Config.master" CodeFile="DetailedDatabaseState.aspx.cs" Inherits="Orion_Voip_DetailedDatabaseState" Title="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_49 %>" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="contentPlaceHolderHead" runat="server">
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/Voip.css" />
    <link rel="stylesheet" type="text/css" href="/WebEngine/Resources/SlateGray.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/styles/Admin.css" />
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="Breadcrumbs">
    <div class="sw-hdr-breadbox">
        <span><a href="../../admin"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_6 %></a> &gt; <a href="default.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_VnqmSettingsLinkName %></a> &gt;</span>
    </div>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionVoIPMonitorPHPageDatabaseDetails" />
</asp:Content>

<asp:Content ID="DataContent" ContentPlaceHolderID="contentPlaceHolderBody" Runat="Server">
	<h1><%=Title %></h1><br/>

    <table id="DatabaseStatsTable" border="0" cellpadding="1" cellspacing="0">
	<tr><td class="Property"><b><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_50 %></b></td><td class="Property"><asp:Label id="DatabaseVersion" runat="server" /></td></tr>
	<tr><td class="Property"><b><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_51 %></b></td><td class="Property"><asp:Label id="VoipSchemaVersion" runat="server" /></td></tr>
	<tr><td class="Property"><b><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_52 %></b></td><td class="Property"><asp:Label id="DatabaseSize" runat="server"/></td></tr>
	<tr><td class="Property"><b><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_53 %></b></td><td class="Property"><asp:Label id="DatabaseUnallocatedSpace" runat="server" /></td></tr>
	<tr><td class="Property"><b><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_54 %></b></td><td class="Property"><asp:DropDownList ID="TableList" runat="server" EnableViewState="True" AutoPostBack="True" OnSelectedIndexChanged="TableList_SelectedIndexChanged"/></td></tr>	
	<tr><td class="Property"><b><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_55 %></b></td><td class="Property"><asp:Label id="RowsCount" runat="server"/></td></tr>
	<tr><td class="Property"><b><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_56 %></b></td><td class="Property"><asp:Label id="UsedByData" runat="server"/></td></tr>
	<tr><td class="Property"><b><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_57 %></b></td><td class="Property"><asp:Label id="UsedByIndexes" runat="server"/></td></tr>
	</table>
</asp:Content>