﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Web.Wizards.Operations;
using SolarWinds.Orion.IpSla.Web.Admin.Settings;
using SolarWinds.Orion.IpSla.Web.WebServices.AjaxTree;
using SolarWinds.Orion.IpSla.Web.AjaxTree;

public partial class Orion_Voip_Admin_NpmNodesSelectionTreeControl : SelectableNodesAjaxTreeControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    #region ScriptControl Behaviour

    protected override ScriptControlDescriptor GetScriptDescriptor()
    {
        ScriptControlDescriptor descriptor = base.GetScriptDescriptor();
        descriptor.Type = "Orion.Voip.Admin.NpmNodesSelectionTreeControl";
        return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        var result = base.GetScriptReferences();
        result = result.Concat(new[] { new ScriptReference(this.ResolveUrl("NpmNodesSelectionTreeControl.js")) });
        return result;
    }

    #endregion

}
