<%@ Page Language="C#" MasterPageFile="~/Orion/Voip/Admin/Config.master" AutoEventWireup="true" CodeFile="Thresholds.aspx.cs" Inherits="Voip_Thresholds" Title="VoIP Jitter Thresholds" %>
<%@ Import Namespace="Resources" %>

<%@ Register Src="~/Orion/Voip/Controls/HelpButton.ascx" TagPrefix="voip" TagName="HelpButton" %>

<asp:content id="Content3" contentplaceholderid="TopRightPageLinks" runat="server">
    <voip:HelpButton ID="HelpButton1" runat="server" HelpUrlFragment="OrionVoIPMonitorPHPageThresholds" />
</asp:content>
<asp:content id="Content4" runat="server" contentplaceholderid="Breadcrumbs">
    <div class="sw-hdr-breadbox">
        <span id="breadcrumb"><a href="../../admin">Admin</a> &gt; <a href="default.aspx"><%= VNQMWebContent.VNQMWEBDATA_VnqmSettingsLinkName %></a> &gt;</span>
    </div>
</asp:content>
<asp:Content ID="Content1" ContentPlaceHolderID="contentPlaceHolderHead" runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/styles/Admin.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/Voip.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolderBody" runat="Server">
    <div id="ipsla_Admin">
        <h1><%=Title %></h1>
        <div class="contentBucket">
	        <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
	        <table>
		        <tr>
			        <td> </td>
			        <td>Warning</td>			
			        <td>Error</td>
			        <td>Maximum</td>
		        </tr>
        		
		        <tr>
			        <td class="PropertyHeader">MOS</td>
			        <td class="Property">
			            <asp:TextBox runat="server" MaxLength="8" Width="60px" style="text-align:right;" ID="MOSWarning"/>
			            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="MOSWarning"
					        ErrorMessage="MOS Warning cannot be left blank.">*</asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="MOSWarning"
	                        ErrorMessage="Please enter only numerical values into MOS Warning field" Operator="DataTypeCheck"
                            Type="Double">*</asp:CompareValidator>
                        <asp:RangeValidator ID="MOSWarningRangeValidator" runat="server" ControlToValidate="MOSWarning"		            
		                    Type="Double" >*</asp:RangeValidator>
		                <asp:CompareValidator ID="MOSCompareValidator" runat="server" ControlToValidate="MOSWarning"
		                    ErrorMessage="MOS Warning value should be greater than MOS Error value" ControlToCompare="MOSError" Operator="GreaterThan"
		                    Type="Double">*</asp:CompareValidator>
                    </td>
			        <td class="Property">
			            <asp:TextBox runat="server" MaxLength="8" Width="60px" style="text-align:right;" ID="MOSError"/>
			            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="MOSError"
					        ErrorMessage="MOS Error cannot be left blank.">*</asp:RequiredFieldValidator>
			            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="MOSError"
	                        ErrorMessage="Please enter only numerical values into MOS Error field" Operator="DataTypeCheck"
                            Type="Double">*</asp:CompareValidator>
                        <asp:RangeValidator ID="MOSErrorRangeValidator" runat="server" ControlToValidate="MOSError"
		                    Type="Double" >*</asp:RangeValidator>
                    </td>
		        </tr>
        		
		        <tr>
			        <td class="PropertyHeader">Jitter</td>
			        <td class="Property">
			            <asp:TextBox runat="server" MaxLength="8" Width="60px" style="text-align:right;" ID="JitterWarning"/> ms
			            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="JitterWarning"
					        ErrorMessage="Jitter Warning cannot be left blank.">*</asp:RequiredFieldValidator>
		                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="JitterWarning" 
	                        ErrorMessage="Please enter only numerical values into Jitter Warning field" Operator="DataTypeCheck"
                            Type="Double">*</asp:CompareValidator>
                        <asp:RangeValidator ID="JitterWarningRangeValidator" runat="server" ControlToValidate="JitterWarning"
		                    Type="Double" >*</asp:RangeValidator>
		                <asp:CompareValidator ID="JitterCompareValidator" runat="server" ControlToValidate="JitterWarning"
		                    ErrorMessage="Jitter Warning value should be less than Jitter Error value" ControlToCompare="JitterError" Operator="LessThan"
		                    Type="Double">*</asp:CompareValidator>
			        </td>
			        <td class="Property">
			            <asp:TextBox runat="server" MaxLength="8" Width="60px" style="text-align:right;" ID="JitterError"/> ms
			            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="JitterError"
					        ErrorMessage="Jitter Error cannot be left blank.">*</asp:RequiredFieldValidator>
    	                <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="JitterError"
	                        ErrorMessage="Please enter only numerical values into Jitter Error field" Operator="DataTypeCheck"
                            Type="Double">*</asp:CompareValidator>
                        <asp:RangeValidator ID="JitterErrorRangeValidator" runat="server" ControlToValidate="JitterError"
		                    Type="Double" >*</asp:RangeValidator>			    
			        </td>
			        <td class="Property">
			            <asp:TextBox runat="server" MaxLength="8" Width="60px" style="text-align:right;" ID="JitterErrorMax" /> ms
			            <asp:CompareValidator ID="CompareValidator9" runat="server" ControlToValidate="JitterErrorMax"
	                        ErrorMessage="Please enter only numerical values into Jitter Maximum field" Operator="DataTypeCheck"
                            Type="Double">*</asp:CompareValidator>
                        <asp:CompareValidator ID="CompareValidator12" runat="server" ControlToValidate="JitterError"
		                    ErrorMessage="Jitter Error value should be less than Jitter Maximum value" ControlToCompare="JitterErrorMax" Operator="LessThanEqual"
		                    Type="Double">*</asp:CompareValidator>
		                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="JitterErrorMax" 
		                    ErrorMessage="Jitter Maximum cannot be left blank">*</asp:RequiredFieldValidator>
			        </td>
		        </tr>
        		
		        <tr>
			        <td class="PropertyHeader">Latency</td>
			        <td class="Property">
			            <asp:TextBox runat="server" MaxLength="8" Width="60px" style="text-align:right;" ID="LatencyWarning" /> ms
			            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="LatencyWarning"
					        ErrorMessage="Latency Warning cannot be left blank.">*</asp:RequiredFieldValidator>
    	                <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="LatencyWarning"
	                        ErrorMessage="Please enter only numerical values into Latency Warning field" Operator="DataTypeCheck"
                            Type="Double">*</asp:CompareValidator>
                        <asp:RangeValidator ID="LatencyWarningRangeValidator" runat="server" ControlToValidate="LatencyWarning"
		                    Type="Double" >*</asp:RangeValidator>
		                <asp:CompareValidator ID="LatencyCompareValidator" runat="server" ControlToValidate="LatencyWarning"
		                    ErrorMessage="Latency Warning value should be less than Latency Error value" ControlToCompare="LatencyError" Operator="LessThan"
		                    Type="Double">*</asp:CompareValidator>
			        </td>
			        <td class="Property">
			            <asp:TextBox runat="server" MaxLength="8" Width="60px" style="text-align:right;" ID="LatencyError" /> ms
			            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="LatencyError"
					        ErrorMessage="Latency Error cannot be left blank.">*</asp:RequiredFieldValidator>
       	                <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="LatencyError"
	                        ErrorMessage="Please enter only numerical values into Latency Error field" Operator="DataTypeCheck"
                            Type="Double">*</asp:CompareValidator>
                        <asp:RangeValidator ID="LatencyErrorRangeValidator" runat="server" ControlToValidate="LatencyError"
		                    Type="Double" >*</asp:RangeValidator>			    
			        </td>
			        <td class="Property">
			            <asp:TextBox runat="server" MaxLength="8" Width="60px" style="text-align:right;" ID="LatencyErrorMax" /> ms
			            <asp:CompareValidator ID="CompareValidator10" runat="server" ControlToValidate="LatencyErrorMax"
	                        ErrorMessage="Please enter only numerical values into Latency Maximum field" Operator="DataTypeCheck"
                            Type="Double">*</asp:CompareValidator>
                        <asp:CompareValidator ID="CompareValidator11" runat="server" ControlToValidate="LatencyError"
		                    ErrorMessage="Latency Error value should be less than Latency Maximum value" ControlToCompare="LatencyErrorMax" Operator="LessThanEqual"
		                    Type="Double">*</asp:CompareValidator>
		                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="LatencyErrorMax" 
		                    ErrorMessage="Latency Maximum cannot be left blank">*</asp:RequiredFieldValidator>
			        </td>
		        </tr>
        		
		        <tr>
			        <td class="PropertyHeader">Packet Loss</td>
			        <td class="Property">
			            <asp:TextBox runat="server" MaxLength="8" Width="60px" style="text-align:right;" ID="PacketLossWarning" /> %
			            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="PacketLossWarning"
					        ErrorMessage="Packet Loss Warning cannot be left blank.">*</asp:RequiredFieldValidator>
      	                <asp:CompareValidator ID="CompareValidator7" runat="server" ControlToValidate="PacketLossWarning"
	                        ErrorMessage="Please enter only numerical values into Packet Loss Warning field" Operator="DataTypeCheck"
                            Type="Double">*</asp:CompareValidator>
                        <asp:RangeValidator ID="PacketLossWarningRangeValidator" runat="server" ControlToValidate="PacketLossWarning"
		                    Type="Double" >*</asp:RangeValidator>
		                <asp:CompareValidator ID="PacketLossCompareValidator" runat="server" ControlToValidate="PacketLossWarning"
		                    ErrorMessage="Packet loss Warning value should be less than Packet loss Error value" ControlToCompare="PacketLossError" Operator="LessThan"
		                    Type="Double">*</asp:CompareValidator>		    
			        </td>
			        <td class="Property">
			            <asp:TextBox runat="server" MaxLength="8" Width="60px" style="text-align:right;" ID="PacketLossError" /> %
			            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="PacketLossError"
					        ErrorMessage="Packet Loss Error cannot be left blank.">*</asp:RequiredFieldValidator>
       	                <asp:CompareValidator ID="CompareValidator8" runat="server" ControlToValidate="PacketLossError"
	                        ErrorMessage="Please enter only numerical values into Packet Loss Error field" Operator="DataTypeCheck"
                            Type="Double">*</asp:CompareValidator>
                        <asp:RangeValidator ID="PacketLossErrorRangeValidator" runat="server" ControlToValidate="PacketLossError"
		                    Type="Double" >*</asp:RangeValidator>			    
			        </td>
		        </tr>
	        </table>
             <br />
            <orion:LocalizableButton ID="ImageButton2" runat="server" DisplayType="Small"
				ToolTip="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_2 %>"
                Text="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_2 %>"
				CausesValidation="false"
                OnClientClick="if (CheckDemoMode()) return false;"
				OnClick="Default_Click" />
             <br />
        </div>
        <div class="sw-btn-bar">
            <orion:LocalizableButton id="btnOK" runat="server"
				ToolTip="OK"
                LocalizedText="OK"
                OnClientClick="if (CheckDemoMode()) return false;"
                DisplayType="Primary" 
				OnClick="btnOK_Click" />
	        <orion:LocalizableButton id="btnCancel" runat="server"
				ToolTip="Cancel"
                LocalizedText="Cancel"
				CausesValidation="false"
                DisplayType="Secondary"
				OnClick="btnCancel_Click" />
        </div>
    </div>
</asp:Content>
