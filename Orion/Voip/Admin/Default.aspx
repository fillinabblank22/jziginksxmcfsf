﻿<%@ Page Title="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_1 %>" Language="C#"
    MasterPageFile="~/Orion/Voip/Admin/Config.master" AutoEventWireup="true" CodeFile="Default.aspx.cs"
    Inherits="Orion_Voip_Admin_Default" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="contentHead" ContentPlaceHolderID="contentPlaceHolderHead" runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/styles/Admin.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/Voip.css" />
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="TopRightPageLinks">
     <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionIPSLAManagerAdministratorGuide-SolarWindsVoIPandNetworkQualityManagerSettings" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="Breadcrumbs">
    <div class="sw-hdr-breadbox">
        <span>
            <a href="../../admin">
            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_2 %>
            </a>&gt; 
        </span>
    </div>
</asp:Content>

<asp:Content ID="contentBody" runat="server" ContentPlaceHolderID="contentPlaceHolderBody">
    <div id="ipsla_Admin">
        <h1 style="margin-bottom: 0.5em;">
            <%= this.Page.Title %>
        </h1>
        <h2 style="margin-top: 0.5em; margin-bottom: 0.5em;">
            <%= this.ProductTitle %>
        </h2>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td class="column1">
                    <div class="contentBucket">
                        <div class="header">
                            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_3 %>
                        </div>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td class="iconAlignment">
                                    <img src="../images/ipsla_32x32.gif" alt="IP SLA" />
                                </td>
                                <td class="body">
                                    <div class="title">
                                           <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_4 %>
                                     </div>
                                    <div class="para">
                                        <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_5 %>
                                    </div>
                                    <div class="para">
                                        <div class="link">
                                            &raquo;&nbsp;<a href="OperationsWizard.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_6 %></a>
                                        </div>
                                        <div class="link">
                                            &raquo;&nbsp;<a href="ManageOperations.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_7 %></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="icon">
                                    &nbsp;
                                </td>
                                <td class="body">
                                    <div class="title">
                                            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_9 %>
                                    </div>
                                    <div class="para">
                                        <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_10 %>
                                    </div>
                                    <div class="para">
                                        <div class="link">
                                            &raquo;&nbsp;<a href="OperationsWizard.aspx?Flow=NodeDiscovery"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_11 %></a>
                                        </div>
                                        <div class="link">
                                            &raquo;&nbsp;<a href="ManageNodes/AddNodes.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_12 %></a>
                                        </div>
                                        <div class="link">
                                            &raquo;&nbsp;<a href="ManageNodes/Default.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_13 %></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="contentBucket">
                        <div class="header">
                            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_14 %>
                        </div>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td class="iconAlignment">
                                    <img src="<%= SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("Voip", "../images/phone_icons_32x32.png") %>"
                                        alt="<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_19 %>" />
                                </td>
                                <td class="body">
                                    <div class="title">
                                        <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_16_2%>
                                    </div>
                                    <div class="para">
                                        <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_17 %>
                                    </div>
                                    <div class="para">
                                        <div class="link">
                                            &raquo;&nbsp;<a href="ManageCallManagers/AddCallManagerWizard.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_18%></a>
                                        </div>
                                        <div class="link">
                                            &raquo;&nbsp;<a href="ManageCallManagers/Default.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_16%></a>
                                        </div>
                                    </div>
                                    <div class="title">
                                        <%= Resources.VNQMWebContent.VNQMWEBDATA_GK_2%>
                                    </div>
                                    <div class="para">
                                        <%= Resources.VNQMWebContent.VNQMWEBDATA_GK_3 %>
                                    </div>
                                    <div class="para">
                                        <div class="link">
                                            &raquo;&nbsp;<a href="ManageMediaGateway/AddMediaGatewayWizard.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_GK_8 %></a>
                                        </div>
                                        <div class="link">
                                            &raquo;&nbsp;<a href="ManageMediaGateway/Default.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_GK_9 %></a>
                                        </div>
                                    </div>
                                    <div class="title">
                                        <%= Resources.VNQMWebContent.VNQMWEBDATA_GK_1%>
                                    </div>
                                    <div class="para">
                                        <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_21 %>
                                    </div>
                                    <div class="para">
                                        <div class="link">
                                            &raquo;&nbsp;<a href="Infrastructure.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_22 %></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="contentBucket">
                        <div class="header">
                            <%= Resources.VNQMWebContent.VNQMWEBDATA_GK_10 %>
                        </div>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td class="iconAlignment">
                                    <img src="<%= SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("Voip", "../images/admin_icons_details_36x36.gif") %>"
                                        alt="<%= Resources.VNQMWebContent.VNQMWEBDATA_GK_11 %>" />
                                </td>
                                <td class="body">
                                    <div class="title">
                                        <%= Resources.VNQMWebContent.VNQMWEBDATA_GK_11%>
                                    </div>
                                    <div class="para">
                                        <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_24 %>
                                    </div>
                                    <div class="para">
                                        <div class="link">
                                            &raquo;&nbsp;<a href="VoipSettings.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_23 %></a>
                                        </div>
                                        <div class="link">
                                            &raquo;&nbsp;<a href="DetailedDatabaseState.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_25 %></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td class="column2">
                    <div class="sideBar">
                        <div class="header">
                            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_27 %>
                        </div>
                        <div class="body">
                            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_28 %>
                            <div class="para">
                                <div class="link">
                                    &raquo;&nbsp;<a href="OperationsWizard.aspx?Flow=GettingStarted"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_29 %></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sideBar">
                        <div class="header">
                            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_30 %>
                        </div>
                        <div class="body">
                            <div>
                                <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_31 %>
                            </div>
                            <div>
                                <%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_AK1_32, "<a href=\"ManageOperations.aspx\">", "</a>")%>
                            </div>
                            <div class="legend">
                                <div>
                                    <img src="../images/OperationStatusIcons/Small-Up.gif" alt="<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_33 %>" />
                                    &nbsp;<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_34 %>
                                </div>
                                <div>
                                    <img src="../images/OperationStatusIcons/Small-UpWarning.gif" alt="<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_35 %>" />
                                    &nbsp;<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_36 %>
                                </div>
                                <div>
                                    <img src="../images/OperationStatusIcons/Small-UpCritical.gif" alt="<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_37 %>" />
                                    &nbsp;<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_38 %>
                                </div>
                                <div>
                                    <img src="../images/OperationStatusIcons/Small-Down.gif" alt="<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_39 %>" />
                                    &nbsp;<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_40 %>
                                </div>
                                <div>
                                    <img src="../images/OperationStatusIcons/Small-Unknown.gif" alt="<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_41 %>" />
                                    &nbsp;<%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_42 %>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sideBar">
                        <div class="header">
                            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_43 %>
                        </div>
                        <div class="body">
                            <div class="subtitle">
                                <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_44 %>
                            </div>
                            <%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_45 %>
                            <div class="para">
                                <div class="link">
                                    &raquo;&nbsp;<a href="http://thwack.com/media/p/77451.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_46 %></a>
                                </div>
                                <div class="link">
                                    &raquo;&nbsp;<a href="https://thwack.solarwinds.com/community/network-management_tht/orion-ip-sla-manager"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_47 %></a>
                                </div>
                                <div class="link">
                                    &raquo;&nbsp;<a href="http://thwack.com/"><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_48 %></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
