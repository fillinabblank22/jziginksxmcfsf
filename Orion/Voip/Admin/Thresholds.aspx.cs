using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;

public partial class Voip_Thresholds : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			RefreshData();
		}
		SetupRangeValidators();
	}
	private void SetupRangeValidators()
	{
		MOSWarningRangeValidator.MaximumValue = MOSErrorRangeValidator.MaximumValue = GaugeConstants.MOS_MAX_VALUE.ToString();
        LatencyWarningRangeValidator.MaximumValue = LatencyErrorRangeValidator.MaximumValue = GaugeConstants.LATENCY_MAX_VALUE.ToString(); //Config.LatencyErrorMax.ToString();
        JitterWarningRangeValidator.MaximumValue = JitterErrorRangeValidator.MaximumValue = GaugeConstants.JITTER_MAX_VALUE.ToString();//Config.JitterErrorMax.ToString();//
		PacketLossWarningRangeValidator.MaximumValue = PacketLossErrorRangeValidator.MaximumValue = GaugeConstants.PACKET_LOSS_MAX_VALUE.ToString();

		MOSWarningRangeValidator.MinimumValue = MOSErrorRangeValidator.MinimumValue = GaugeConstants.MOS_MIN_VALUE.ToString();
		LatencyWarningRangeValidator.MinimumValue = LatencyErrorRangeValidator.MinimumValue = GaugeConstants.LATENCY_MIN_VALUE.ToString();
		JitterWarningRangeValidator.MinimumValue = JitterErrorRangeValidator.MinimumValue = GaugeConstants.JITTER_MIN_VALUE.ToString();
		PacketLossWarningRangeValidator.MinimumValue = PacketLossErrorRangeValidator.MinimumValue = GaugeConstants.PACKET_LOSS_MIN_VALUE.ToString();

		MOSWarningRangeValidator.ErrorMessage = string.Format("MOS Warning interval should be in the range from {0} to {1}", GaugeConstants.MOS_MIN_VALUE, GaugeConstants.MOS_MAX_VALUE);
		MOSErrorRangeValidator.ErrorMessage = string.Format("MOS Error interval should be in the range from {0} to {1}", GaugeConstants.MOS_MIN_VALUE, GaugeConstants.MOS_MAX_VALUE);
		JitterWarningRangeValidator.ErrorMessage = string.Format("Jitter Warning interval should be in the range from {0} to {1}", GaugeConstants.JITTER_MIN_VALUE, GaugeConstants.JITTER_MAX_VALUE);
		JitterErrorRangeValidator.ErrorMessage = string.Format("Jitter Error interval should be in the range from {0} to {1}", GaugeConstants.JITTER_MIN_VALUE, GaugeConstants.JITTER_MAX_VALUE);
		PacketLossWarningRangeValidator.ErrorMessage = string.Format("Packet Loss Warning interval should be in the range from {0} to {1}", GaugeConstants.PACKET_LOSS_MIN_VALUE, GaugeConstants.PACKET_LOSS_MAX_VALUE);
		PacketLossErrorRangeValidator.ErrorMessage = string.Format("Packet Loss Error interval should be in the range from {0} to {1}", GaugeConstants.PACKET_LOSS_MIN_VALUE, GaugeConstants.PACKET_LOSS_MAX_VALUE);
		LatencyWarningRangeValidator.ErrorMessage = string.Format("Latency Warning interval should be in the range from {0} to {1}", GaugeConstants.LATENCY_MIN_VALUE, GaugeConstants.LATENCY_MAX_VALUE);
        LatencyErrorRangeValidator.ErrorMessage = string.Format("Latency Error interval should be in the range from {0} to {1}", GaugeConstants.LATENCY_MIN_VALUE, GaugeConstants.LATENCY_MAX_VALUE);
	}
	private void RefreshData()
	{
        RefreshThresholdTextBox(MOSError, MOSWarning, null, ThresholdType.MOS);
        RefreshThresholdTextBox(JitterError, JitterWarning, JitterErrorMax, ThresholdType.Jitter);
        RefreshThresholdTextBox(LatencyError, LatencyWarning, LatencyErrorMax, ThresholdType.Latency);
        RefreshThresholdTextBox(PacketLossError, PacketLossWarning, null, ThresholdType.PacketLoss);
	}

    void RefreshThresholdTextBox(TextBox errorTextBox, TextBox warningTextBox, TextBox maxTextBox, ThresholdType thresholdType)
    {
        var thresholdInfo = ThresholdManager.GetThresholdForOperationType(OperationType.UdpVoipJitter, thresholdType);
        errorTextBox.Text = thresholdInfo.ErrorLevel.ToString();
        warningTextBox.Text = thresholdInfo.WarningLevel.ToString();
        if (maxTextBox != null)
        {
            maxTextBox.Text = thresholdInfo.MaxLevel.ToString();
        }
    }

    void StoreThreshold(TextBox errorTextBox, TextBox warningTextBox, TextBox maxTextBox, ThresholdType thresholdType, bool isReverse)
    {
        var thresholdInfo = ThresholdManager.GetThresholdForOperationType(OperationType.UdpVoipJitter, thresholdType);
        thresholdInfo = thresholdInfo.Clone();
        thresholdInfo.IsDefault = false;
        thresholdInfo.WarningLevel = double.Parse(warningTextBox.Text);
        thresholdInfo.ErrorLevel = double.Parse(errorTextBox.Text);
        if (maxTextBox != null)
        {
            thresholdInfo.MaxLevel = double.Parse(maxTextBox.Text);
        }
        ThresholdManager.SaveThresholdsForOperationType(OperationType.UdpVoipJitter, thresholdInfo);
    }

    private void StoreData()
    {
        StoreThreshold(MOSError, MOSWarning, null, ThresholdType.MOS, true);
        StoreThreshold(JitterError, JitterWarning, JitterErrorMax, ThresholdType.Jitter, false);
        StoreThreshold(LatencyError, LatencyWarning, LatencyErrorMax, ThresholdType.Latency, false);
        StoreThreshold(PacketLossError, PacketLossWarning, null, ThresholdType.PacketLoss, false);
    }
	
	
	protected void Default_Click(object sender, EventArgs e)
	{
		Config.RestoreDefaultThresholds();
		RefreshData();
	}

    protected void btnOK_Click(object sender, EventArgs e)
    {
        StoreData();
		Response.Redirect("Default.aspx");
    }

	protected void btnCancel_Click(object sender, EventArgs e)
	{
		Response.Redirect("Default.aspx");
	}
}
