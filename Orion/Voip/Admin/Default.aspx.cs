﻿using System;
using SolarWinds.Orion.IpSla.Common.Licensing;

public partial class Orion_Voip_Admin_Default : System.Web.UI.Page
{
    protected string ProductTitle
    {
        get
        {
            return LicenseInfo.CopyRightInfoString;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}
