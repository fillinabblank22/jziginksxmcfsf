﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Contracts;
using SolarWinds.Orion.Web.Helpers;

using System.Data;
using SolarWinds.Orion.IpSla.Common;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_Admin_VoipModuleDetails : System.Web.UI.UserControl
{
    private static Log _log;
    private static Log Logger { get { return _log ?? (_log = new Log()); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                GetModuleAndLicenseInfo("VNQM");
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error while displaying details for Orion Core module. Details: {0}", ex.ToString());
            }
        }
    }

    private void GetModuleAndLicenseInfo(string moduleName)
    {
        foreach (var module in ModuleDetailsHelper.LoadModuleInfoForEngines("Primary", false))
        {
            if (!module.ProductShortName.StartsWith(moduleName, StringComparison.OrdinalIgnoreCase))
                continue;

            var values = new Dictionary<string, string>();

            VoipDetails.Name = module.ProductDisplayName;

            values.Add(Resources.VNQMWebContent.VNQMWEBCODE_VB1_258, module.ProductName);
            values.Add(Resources.VNQMWebContent.VNQMWEBCODE_VB1_259, module.Version);
            values.Add(Resources.VNQMWebContent.VNQMWEBCODE_VB1_260, String.IsNullOrEmpty(module.HotfixVersion) ? Resources.VNQMWebContent.VNQMWEBDATA_AK1_89 : module.HotfixVersion);
            if (!String.IsNullOrEmpty(module.LicenseInfo))
                values.Add(Resources.VNQMWebContent.VNQMWEBCODE_VB1_261, module.LicenseInfo);

            AddVoipInfo(values);

            VoipDetails.DataSource = values;
			break; // we support only one "Primary" engine
        }
    }

    private void AddVoipInfo(Dictionary<string, string> values)
    {
        try
        {
            IpSlaLicenseInfo licenseInfo = LicenseHelper.GetLicenseInfo();

            values.Add(Resources.VNQMWebContent.VNQMWEBCODE_VB1_262, licenseInfo.MaxIpSlaSitesDisplay);
            values.Add(Resources.VNQMWebContent.VNQMWEBCODE_VB1_263, licenseInfo.MonitoredIpSlaSites.ToString());
            values.Add(Resources.VNQMWebContent.VNQMWEBCODE_VB1_264, licenseInfo.MaxIpPhonesDisplay);
            values.Add(Resources.VNQMWebContent.VNQMWEBCODE_VB1_265, licenseInfo.LicensedIpPhones.ToString());
            values.Add(Resources.VNQMWebContent.VNQMWEBCODE_VB1_266, licenseInfo.UnlicensedIpPhones.ToString());
        }
        catch (Exception ex)
        {
            Logger.Error("Unable to get license information.", ex);
        }
    }
}