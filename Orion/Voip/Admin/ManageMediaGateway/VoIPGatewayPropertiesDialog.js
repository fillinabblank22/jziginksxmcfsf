﻿/// <reference name="MicrosoftAjax.js"/>
/// <reference path="~/Orion/js/jquery.js" />
/// <reference path="~/Orion/js/extjs/debug/Ext.js" />

Type.registerNamespace("Orion.Voip.Services");
Type.registerNamespace("Orion.Voip.Admin.ManageMediaGateway");

Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayPropertiesDialog = function (element) {
    Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayPropertiesDialog.initializeBase(this, [element]);

    this.editGatewayBox = null;
    this.gatewayProperties = null;

    this.window = null;
    this.gatewayNodeId = null;
}

Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayPropertiesDialog.prototype = {

    Show: function (gatewayNodeId, onSaved) {

        Orion.Voip.Services.VoipGatewayPropertiesService.GetProperties(gatewayNodeId,
            Function.createDelegate(this, function (result, context) {
                this.ShowInternal(context.gatewayNodeId, context.OnSaved);
                this.gatewayProperties.set_EngineId(result.EngineId);
                this.gatewayProperties.set_NodeIpAddress(result.NodeIpAddress);
                this.gatewayProperties.set_SiteName(result.SiteName);
                this.gatewayProperties.set_CredentialId(result.CredentialId);
                this.gatewayProperties.set_Protocol(result.Protocol ? result.Protocol : 'SshAuto');
                this.gatewayProperties.set_Port(result.Port ? result.Port : 22);
                this.gatewayProperties.set_Timeout(result.Timeout ? result.Timeout : 5000);
            }),
            function (error, context) {
                Ext.Msg.show({
                    title: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_53;E=js}',
                    msg: error.get_message(),
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
            },
            { gatewayNodeId: gatewayNodeId, OnSaved: onSaved }
        );
    },

    ShowInternal: function (gatewayNodeId, onSaved) {
        this.gatewayNodeId = gatewayNodeId;

        if (this.window == null) {
            this.window = new Ext.Window({
                title: '@{R=VNQM.Strings;K=VNQMWEBJS_PG_11;E=js}',
                layout: 'fit',
                width: 640,
                height: 450,
                resizable: false,
                closeAction: 'hide',
                closable: false,
                modal: true,
                layout: 'fit',
                applyTo: $('<div id="edit-node"/>').appendTo($(document.forms[0]))[0],
                allowDomMove: false,
                items: [{
                    xtype: 'panel',
                    bodyStyle: 'padding:5px 5px 5px 5px',
                    autoScroll: true,
                    contentEl: this.editGatewayBox
                }],
                buttons: [{
                    text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_41;E=js}',
                    handler: Function.createDelegate(this, function () {
                        if (CheckDemoMode()) return;
                        if (this.gatewayProperties.validate()) {
                            this.SaveInternal(onSaved);
                        }
                    })
                }, {
                    text: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_26;E=js}',
                    handler: Function.createDelegate(this, function () {
                        this.window.hide();
                    })
                }]
            });
            this.gatewayProperties.syncSize();
        }
        this.window.show(this);
    },

    SaveInternal: function (onSaved) {
        var credentialId = this.gatewayProperties.get_CredentialId();
        var credentialName = this.gatewayProperties.get_CredentialName();
        var connInfo = null;
        if (credentialId != null || (credentialName != null && credentialName.length > 0)) {
            connInfo = {
                CredentialId: credentialId,
                CredentialName: credentialName,
                Username: this.gatewayProperties.get_Username() || '',
                Password: this.gatewayProperties.get_Password() || '',
                EnablePassword: this.gatewayProperties.get_EnablePassword() || '',
                EnableLevel: this.gatewayProperties.get_EnableLevel(),
                Protocol: this.gatewayProperties.get_Protocol(),
                Port: this.gatewayProperties.get_Port(),
                Timeout: this.gatewayProperties.get_Timeout()
            };
        }
        Orion.Voip.Services.VoipGatewayPropertiesService.UpdateProperties(this.gatewayNodeId, connInfo,
            Function.createDelegate(this, function (result, context) {
                this.window.hide();
                if (onSaved != null)
                    onSaved();
            }),
            function (error, context) {
                Ext.Msg.show({
                    title: '@{R=VNQM.Strings;K=VNQMWEBJS_TM0_53;E=js}',
                    msg: error.get_message(),
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
            },
            null
        );
    },

    get_editGatewayBox: function () { return this.editGatewayBox; },
    set_editGatewayBox: function (value) { this.editGatewayBox = value; },

    get_gatewayProperties: function () { return this.gatewayProperties; },
    set_gatewayProperties: function (value) { this.gatewayProperties = value; },

    initialize: function () {
        Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayPropertiesDialog.callBaseMethod(this, 'initialize');

        // Add custom initialization here
    },
    dispose: function () {
        //Add custom dispose actions here
        Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayPropertiesDialog.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayPropertiesDialog.registerClass('Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayPropertiesDialog', Sys.UI.Control);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
