<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VoIPGatewayPropertiesControl.ascx.cs" Inherits="Orion_Voip_Admin_ManageMediaGateway_VoIPGatewayPropertiesControl" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="ipsla" TagName="CliConnectionControl" Src="~/Orion/Voip/Controls/Cli/CliConnectionControl.ascx" %>

<asp:ScriptManagerProxy ID="scriptManager" runat="server">
  <Services>
<%--    <asp:ServiceReference Path="~/Orion/Voip/Controls/Cli/CliConnectionControl.asmx" />
--%>  </Services>
</asp:ScriptManagerProxy>

<div class="ipsla_MaskArea">
<table class="ipsla_EditNodeBox" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr class="input">
            <td class="col1Label">
                <%= VNQMWebContent.VNQMWEBDATA_PG_18 %>
            </td>
            <td colspan="9">
                <asp:TextBox ID="tbSiteName" runat="server" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr class="header">
            <td colspan="10">
                <%= VNQMWebContent.VNQMWEBDATA_PG_17 %>
            </td>
        </tr>
    </tbody>
    <ipsla:CliConnectionControl ID="cliConnection" runat="server" />
</table>
</div>