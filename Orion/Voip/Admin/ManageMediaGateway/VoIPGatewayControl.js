﻿/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("Orion.Voip.Admin.ManageMediaGateway");

Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayControl = function (element) {
    Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayControl.initializeBase(this, [element]);
    this.gridHostElementId = null;
    this.selectedItemsElementId = null;
    this.voipGatewayPropertiesDialog = null;
    this.InvokeAddVoIPGatewayPostback = null;
    this.InvokeEditVoIPGatewayPostback = null;
}

Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayControl.prototype = {

    get_voipGatewayPropertiesDialog: function () { return this.voipGatewayPropertiesDialog; },
    set_voipGatewayPropertiesDialog: function (value) { this.voipGatewayPropertiesDialog = value; },

    AddVoIPGateway: function () {
        this.InvokeAddVoIPGatewayPostback();
    },

    EditVoIPGateway: function () {
        if (CheckDemoMode()) return;

        var selections = this.voIPGatewayGrid.selModel.getSelections();
        var selectedCMs = [];
        for (i = 0; i < this.voIPGatewayGrid.selModel.getCount(); i++) {
            selectedCMs.push(selections[i].data.NodeID);
        }
        var encoded_array = Ext.encode(selectedCMs);
        $('#' + this.selectedItemsElementId).val(encoded_array);
        this.InvokeEditVoIPGatewayPostback();
    },

    ConfirmRemove: function () {
        if (CheckDemoMode()) return;

        Ext.MessageBox.confirm('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_3;E=js}', '@{R=VNQM.Strings;K=VNQMWEBJS_GK_3;E=js}', Function.createDelegate(this, this.removeSelectedGateways));
    },

    initialize: function () {
        Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayControl.callBaseMethod(this, 'initialize');
        $(document).ready(Function.createDelegate(this, function () {
            this.initializeGrid();
        }));
    },

    initializeGrid: function () {
        // Add custom initialization here
        this.dataStore = new Ext.data.JsonStore({
            id: 'DataStore',
            url: '/Orion/Voip/Admin/ManageMediaGateway/VoIPGatewayExtGridDataProvider.ashx',
            baseParams: { task: 'select' }, // this parameter is passed for any HTTP request
            root: 'voIPGatewayList',
            totalProperty: 'total',
            idProperty: 'id',
            fields: [
                { name: 'ID', type: 'int' },
                { name: 'Name', type: 'string' },
                { name: 'Status', type: 'string' },
                { name: 'NodeID', type: 'int' }
            ],
            sortInfo: { field: 'Name', direction: 'ASC' }
        });

        this.selectionModel = new Ext.grid.CheckboxSelectionModel({
            singleSelect: false,
            listeners: {
                'selectionchange': {
                    scope: this,
                    fn: function (p) {
                        var count = this.voIPGatewayGrid.selModel.getCount(), map = this.voIPGatewayGrid.getTopToolbar().items.map;

                        if (count == 1) {
                            map.btnEdit.enable();
                        }
                        else {
                            map.btnEdit.disable();
                        }

                        map.editVoIPGateways.setDisabled(count === 0);
                        map.removeVoIPGateways.setDisabled(count === 0);
                    }
                }
            }
        });

        this.columnModel = new Ext.grid.ColumnModel(
                        [this.selectionModel,
                        {
                            header: '@{R=VNQM.Strings;K=VNQMWEBJS_GK_4;E=js}',
                            dataIndex: 'Name',
                            width: 250,
                            sortable: true
                        }, {
                            header: '@{R=VNQM.Strings;K=VNQMWEBJS_GK_5;E=js}',
                            dataIndex: 'Status',
                            width: 200,
                            sortable: true
                        }]
                    );
        this.columnModel.defaultSortable = true;

        this.voIPGatewayGrid = new Ext.grid.GridPanel({
            id: 'voIPGatewayGrid',
            height: 300,
            width: 600,
            store: this.dataStore,
            cm: this.columnModel,
            enableColLock: false,
            selModel: this.selectionModel,
            renderTo: this.gridHostElementId,
            loadMask: true,
            stripeRows: true,
            tbar: [
                    {
                        text: '&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_GK_1;E=js}',
                        tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_GK_6;E=js}',
                        handler: Function.createDelegate(this, this.AddVoIPGateway),
                        iconCls: 'ipsla_AddGateway'
                    },
                    '-',
                    {
                        text: '&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_GK_7;E=js}',
                        tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_GK_8;E=js}',
                        handler: Function.createDelegate(this, this.EditVoIPGateway),
                        iconCls: 'ipsla_EditGateway',
                        id: 'editVoIPGateways',
                        disabled: true
                    },
                    '-',
                    {
                        id: 'btnEdit',
                        text: '&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_PG_12;E=js}',
                        tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_PG_12;E=js}',
                        handler: Function.createDelegate(this, this.EditNode),
                        iconCls: 'ipsla_Edit_Creds',
                        disabled: true
                    },
                    '-',
                    {
                        text: '&nbsp;@{R=VNQM.Strings;K=VNQMWEBJS_GK_9;E=js}',
                        tooltip: '@{R=VNQM.Strings;K=VNQMWEBJS_GK_10;E=js}',
                        handler: Function.createDelegate(this, this.ConfirmRemove),
                        iconCls: 'ipsla_RemoveGateway',
                        id: 'removeVoIPGateways',
                        disabled: true
                    }]
        });

        this.dataStore.load();
    },

    reloadGrid: function () {
        this.dataStore.reload();
    },

    removeSelectedGateways: function (btn) {
        if (btn == 'yes') {
            var selections = this.voIPGatewayGrid.selModel.getSelections();
            var selectedCMs = [];
            for (i = 0; i < this.voIPGatewayGrid.selModel.getCount(); i++) {
                selectedCMs.push(selections[i].json.ID);
            }
            var encoded_array = Ext.encode(selectedCMs);
            Ext.Ajax.request({
                waitMsg: '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_16;E=js}',
                url: '/Orion/Voip/Admin/ManageMediaGateway/VoIPGatewayExtGridDataProvider.ashx',
                params: {
                    task: "delete",
                    ids: encoded_array
                },
                success: Function.createDelegate(this, function (response) {
                    var result = eval(response.responseText);
                    this.dataStore.reload();
                    switch (result) {
                        case 1:  // Success
                            break;
                        default:
                            Ext.MessageBox.alert('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_17;E=js}', '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_18;E=js}');
                            break;
                    }
                }),
                failure: function (response) {
                    var result = response.responseText;
                    Ext.MessageBox.alert('@{R=VNQM.Strings;K=VNQMWEBJS_AK1_19;E=js}', '@{R=VNQM.Strings;K=VNQMWEBJS_AK1_20;E=js}');
                }
            });
        }
    },

    EditNode: function () {
        if (this.voIPGatewayGrid.selModel.getCount() != 1)
            return;
        var gatewayNodeId = this.voIPGatewayGrid.selModel.getSelections()[0].json.NodeID;
        this.voipGatewayPropertiesDialog.Show(gatewayNodeId, Function.createDelegate(this, function () { this.reloadGrid(); }));
    },

    dispose: function () {
        //Add custom dispose actions here
        Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayControl.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayControl.registerClass('Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayControl', Sys.UI.Control);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
