<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VoIPGatewayPropertiesDialog.ascx.cs" Inherits="Orion_Voip_Admin_ManageMediaGateway_VoIPGatewayPropertiesDialog" %>
<%@ Register TagPrefix="ipsla" TagName="VoIPGatewayPropertiesControl" Src="~/Orion/Voip/Admin/ManageMediaGateway/VoIPGatewayPropertiesControl.ascx" %>

<asp:ScriptManagerProxy ID="scriptManager" runat="server">
    <Services>
        <asp:ServiceReference Path="~/Orion/Voip/Services/VoipGatewayPropertiesService.asmx" />
    </Services>
</asp:ScriptManagerProxy>

<div id="editGatewayBox" runat="server">
    <ipsla:VoIPGatewayPropertiesControl ID="gatewayProperties" runat="server" />
</div>
