﻿/// <reference name="MicrosoftAjax.js"/>
/// <reference path="~/Orion/js/jquery.js" />
/// <reference path="~/Orion/js/extjs/debug/Ext.js" />

Type.registerNamespace("Orion.Voip.Admin.ManageMediaGateway");

Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayPropertiesControl = function(element) {
    Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayPropertiesControl.initializeBase(this, [element]);
    this.tbSiteName = null;
    this.cliConnection = null;

    this.extSiteName = null;
}

Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayPropertiesControl.prototype = {

    get_EngineId: function () { return this.cliConnection.get_ValidationEngineId(); },
    set_EngineId: function (value) { this.cliConnection.set_ValidationEngineId(value); },

    get_NodeIpAddress: function () { return this.cliConnection.get_ValidationIp(); },
    set_NodeIpAddress: function (value) { this.cliConnection.set_ValidationIp(value); },

    get_SiteName: function () { return this.extSiteName.getValue(); },
    set_SiteName: function (value) { this.extSiteName.setValue(value); },

    get_Protocol: function () { return this.cliConnection.get_Protocol(); },
    set_Protocol: function (value) { this.cliConnection.set_Protocol(value); },

    get_Port: function () { return this.cliConnection.get_Port(); },
    set_Port: function (value) { this.cliConnection.set_Port(value); },

    get_Timeout: function () { return this.cliConnection.get_Timeout(); },
    set_Timeout: function (value) { this.cliConnection.set_Timeout(value); },

    get_CredentialId: function () { return this.cliConnection.get_CredentialId(); },
    set_CredentialId: function (value) { this.cliConnection.set_CredentialId(value); },

    get_CredentialName: function () { return this.cliConnection.get_CredentialName(); },
    get_Username: function () { return this.cliConnection.get_Username(); },
    get_Password: function () { return this.cliConnection.get_Password(); },
    get_EnablePassword: function () { return this.cliConnection.get_EnablePassword(); },
    get_EnableLevel: function () { return this.cliConnection.get_EnableLevel(); },

    get_tbSiteName: function () { return this.tbSiteName; },
    set_tbSiteName: function (value) { this.tbSiteName = value; },

    get_cliConnection: function () { return this.cliConnection; },
    set_cliConnection: function (value) { this.cliConnection = value; },

    validate: function () {
        return this.cliConnection.validate();
    },

    syncSize: function () {
        this.cliConnection.syncSize();
    },

    initialize: function () {
        Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayPropertiesControl.callBaseMethod(this, 'initialize');

        // Add custom initialization here
        Ext.QuickTips.init();

        this.extSiteName = new Ext.form.TextField({
            applyTo: this.tbSiteName,
            width: 200
        });
    },
    dispose: function () {
        //Add custom dispose actions here
        Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayPropertiesControl.callBaseMethod(this, 'dispose');
    }
}
Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayPropertiesControl.registerClass('Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayPropertiesControl', Sys.UI.Control);

if (typeof(Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
