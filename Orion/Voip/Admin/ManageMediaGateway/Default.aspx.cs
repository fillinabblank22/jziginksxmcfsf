﻿using System;

public partial class Orion_Voip_Admin_ManageMediaGateway_Default : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        VoIPGatewaysControl.AddVoIPGatewayUrl = "/Orion/Voip/Admin/ManageMediaGateway/AddMediaGatewayWizard.aspx";
        VoIPGatewaysControl.EditVoIPGatewayUrl = string.Format("/Orion/Nodes/NodeProperties.aspx?Nodes={{0}}");
    }
}