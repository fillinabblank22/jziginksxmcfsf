﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VoIPGatewayControl.ascx.cs" Inherits="Orion_Voip_Admin_ManageMediaGateway_VoIPGatewayControl" %>
<%@ Register TagPrefix="ipsla" TagName="VoipGatewayPropertiesDialog" Src="~/Orion/Voip/Admin/ManageMediaGateway/VoipGatewayPropertiesDialog.ascx" %>

<script language="javascript" type="text/javascript">
    Ext.onReady(function() {
        Ext.QuickTips.init();
    });
</script>

<style>
    #<%= grid.ClientID %> td { padding-bottom: 0px; padding-right: 0px; }
</style>

<div id="grid" runat="server" class="ExtJsGrid"></div>
<asp:HiddenField ID="selectedItems" runat="server" />

<ipsla:VoipGatewayPropertiesDialog ID="voipGatewayPropertiesDialog" runat="server" />
