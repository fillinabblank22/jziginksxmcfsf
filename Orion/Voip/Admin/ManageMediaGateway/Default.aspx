﻿<%@ Page Title="<%$ Resources: VNQMWebContent, VNQMWEBDATA_GK_12 %>" Language="C#" MasterPageFile="~/Orion/Voip/Admin/Config.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_Voip_Admin_ManageMediaGateway_Default" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagName="IncludeExtJs" TagPrefix="orion" %>
<%@ Register src="VoIPGatewayControl.ascx" tagname="VoIPGatewaysControl" tagprefix="ipsla" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="contentHead" ContentPlaceHolderID="contentPlaceHolderHead" Runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include ID="Include2" runat="server" Module="Voip" File="styles/Admin.css" />
    <orion:Include ID="Include3" runat="server" Module="Voip" File="Voip.css" />
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="Breadcrumbs">
    <div class="sw-hdr-breadbox">
        <span><a href="../../../admin"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_6 %></a> &gt; <a href="../default.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_VnqmSettingsLinkName %></a> &gt;</span>
    </div>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="TopRightPageLinks">
  <orion:IconLinkExportToPDF runat="server" ID="ExportToPDFLink"/>
  <orion:IconHelpButton ID="IconHelpButtonMGW" runat="server" HelpUrlFragment="OrionIPSLAManagerAdministratorGuide-ManagingGateways" />
</asp:Content>

<asp:Content ID="contentBody" runat="server" ContentPlaceHolderID="contentPlaceHolderBody">
<div style="position:relative">
    <h1 style="font-size: large;"><% = Page.Title %></h1>
</div>
    <br />
    <ipsla:VoIPGatewaysControl ID="VoIPGatewaysControl" runat="server" />
</asp:Content>
