﻿using System;
using System.Web;
using System.Web.UI;
using Resources;
using SolarWinds.Orion.IpSla.Web.Wizards.MediaGateway;

public partial class Orion_Voip_Admin_ManageMediaGateway_AddMediaGatewayWizard : Page, IPostBackEventHandler
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
        Response.Cache.SetNoStore();
        Response.AppendHeader("Pragma", "no-cache");

        MediaGatewayWizardFlow flow = MediaGatewayWizardFlow.SelectNodes;

        if (MediaGatewayWizardSession.Exists)
        {
            var session = MediaGatewayWizardSession.Current;
            if (session.Data.IsEmpty)
            {
                if (session.Data.Flow != flow)
                {
                    MediaGatewayWizardSession.NewCurrent(new MediaGatewayWizardSession(flow), true);
                }
            }
            else
            {
                var currentPart = session.CurrentPart;

                var referrer = Request.UrlReferrer;
                if (referrer == null || StringComparer.OrdinalIgnoreCase.Compare(referrer.AbsoluteUri, Request.Url.AbsoluteUri) != 0)
                {
                    if (currentPart.WarnBeforeJoin)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ContinueInSession", @"
Ext.onReady(function(){
    Ext.MessageBox.show({
        title: '" + VNQMWebContent.VNQMWEBCODE_VB1_10 + @"',
        icon: Ext.MessageBox.QUESTION,
        msg: '" + VNQMWebContent.VNQMWEBCODE_VB1_11 + @"',
        closable: false,
        buttons: { yes: '" + VNQMWebContent.VNQMWEBCODE_VB1_12 + @"', no: '" + VNQMWebContent.VNQMWEBCODE_VB1_13 + @"' },
        fn: function(btn, value) {
            if (btn == 'no') {"
                        + ClientScript.GetPostBackEventReference(this, "Reset") + @";
            }
        }
    });
});
", true);
                    }
                    else if (currentPart.ResetBeforeJoin)
                    {
                        session.Close();
                        MediaGatewayWizardSession.NewCurrent(new MediaGatewayWizardSession(flow), true);
                    }
                }
            }
        }
        else
        {
            MediaGatewayWizardSession.NewCurrent(new MediaGatewayWizardSession(flow), true);
        }
    }
    #region IPostBackEventHandler Members

    void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
    {
        switch (eventArgument)
        {
            case "Reset":
                MediaGatewayWizardSession.Current.Close();
                Server.Transfer(Request.Url.PathAndQuery);
                break;
        }
    }

    #endregion
}