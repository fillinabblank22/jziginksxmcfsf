using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_Admin_ManageMediaGateway_VoIPGatewayPropertiesDialog : ScriptUserControlBase
{
	private static readonly Log log = new Log();

    protected override void RenderWrapperElementBegin(HtmlTextWriter writer)
    {
        writer.AddAttribute(HtmlTextWriterAttribute.Id, this.ClientID);
        writer.AddAttribute(HtmlTextWriterAttribute.Style, "display: none;");
        writer.RenderBeginTag(HtmlTextWriterTag.Div);
    }

    #region IScriptControl Members

    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        var descriptor = new ScriptControlDescriptor("Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayPropertiesDialog", this.ClientID);
        descriptor.AddElementProperty("editGatewayBox", this.editGatewayBox.ClientID);
        descriptor.AddComponentProperty("gatewayProperties", this.gatewayProperties.ClientID);
        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        yield return new ScriptReference(this.ResolveUrl("VoIPGatewayPropertiesDialog.js"));
    }

    #endregion
}
