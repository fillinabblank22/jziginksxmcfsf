using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_Admin_ManageMediaGateway_VoIPGatewayPropertiesControl : ScriptUserControlBase
{
	private static readonly Log log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected override void RenderWrapperElementBegin(HtmlTextWriter writer)
    {
        writer.AddAttribute(HtmlTextWriterAttribute.Id, this.ClientID);
        writer.AddAttribute(HtmlTextWriterAttribute.Style, "display: none;");
        writer.RenderBeginTag(HtmlTextWriterTag.Span);
    }

    protected override void RenderWrapperElementWithContent(HtmlTextWriter writer)
    {
        this.RenderWrapperElementBegin(writer);
        this.RenderWrapperElementEnd(writer);
        this.RenderContent(writer);
    }

    #region IScriptControl Members

    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        var descriptor = new ScriptControlDescriptor("Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayPropertiesControl", this.ClientID);
        descriptor.AddElementProperty("tbSiteName", this.tbSiteName.ClientID);
        descriptor.AddComponentProperty("cliConnection", this.cliConnection.ClientID);
        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        yield return new ScriptReference(this.ResolveUrl("VoIPGatewayPropertiesControl.js"));
    }

    #endregion
}
