﻿<%@ Page Title="<%$ Resources : VNQMWebContent, VNQMWEBDATA_GK_4 %>" Language="C#" MasterPageFile="~/Orion/Voip/Admin/Config.master" AutoEventWireup="true" CodeFile="AddMediaGatewayWizard.aspx.cs" Inherits="Orion_Voip_Admin_ManageMediaGateway_AddMediaGatewayWizard" %>
<%@ Import Namespace="SolarWinds.Orion.IpSla.Web"%>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagName="IncludeExtJs" TagPrefix="orion" %>
<%@ Register src="~/orion/voip/controls/wizards/MediaGateway/MediaGatewayWizardControlContainer.ascx" tagname="MediaGatewayWizardControlContainer" tagprefix="ipsla" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="contentHead" ContentPlaceHolderID="contentPlaceHolderHead" Runat="Server">
    <orion:Include ID="Include2" runat="server" Module="Voip" File="styles/Admin.css" />
    <orion:Include ID="Include3" runat="server" Module="Voip" File="styles/Wizard.css" />
    <orion:Include ID="Include1" runat="server" Module="Voip" File="Discovery.css" />
    <orion:Include ID="Include4" runat="server" Module="Voip" File="Voip.css" />
    <orion:IncludeExtJs ID="IncludeExtJs" runat="server" Version="3.4"/>
    
 </asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="Breadcrumbs">
    <div class="sw-hdr-breadbox">
        <span><a href="../../../admin"><%= Resources.VNQMWebContent.VNQMWEBDATA_VB1_6%></a> &gt; <a href="../default.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_VnqmSettingsLinkName%></a> 
        &gt; <a href="/Orion/Voip/Admin/ManageMediaGateway/Default.aspx"><%= Resources.VNQMWebContent.VNQMWEBDATA_GK_5%></a> &gt;</span>
    </div>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="TopRightPageLinks">
     <orion:IconHelpButton ID="IconHelpButtonAGW" runat="server" HelpUrlFragment="OrionIPSLAManagerAdministratorGuide-AddingGateways" />
</asp:Content>

<asp:Content ID="contentBody" runat="server" ContentPlaceHolderID="contentPlaceHolderBody" >
<div id="wizardControl">
<ipsla:MediaGatewayWizardControlContainer ID="mediaGatewayWizardControlContainerWizardControlContainer" runat="server"
    ReturnUrl="~/Orion/Voip/Admin/ManageMediaGateway/Default.aspx" />
</div>
</asp:Content>
