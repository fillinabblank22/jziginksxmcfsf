﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_Admin_ManageMediaGateway_VoIPGatewayControl : ScriptUserControlBase, IPostBackEventHandler
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.GetScriptManager().RegisterAsyncPostBackControl(this);
    }

    public string AddVoIPGatewayUrl { get; set; }
    public string EditVoIPGatewayUrl { get; set; }

    #region IScriptControl Members

    public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Orion.Voip.Admin.ManageMediaGateway.VoIPGatewayControl", this.ClientID);
        descriptor.AddProperty("gridHostElementId", this.grid.ClientID);
        descriptor.AddProperty("selectedItemsElementId", this.selectedItems.ClientID);
        descriptor.AddComponentProperty("voipGatewayPropertiesDialog", this.voipGatewayPropertiesDialog.ClientID);
        descriptor.AddScriptProperty("InvokeAddVoIPGatewayPostback",
            "function() { " + this.Page.ClientScript.GetPostBackEventReference(this, "AddVoIPGateway") + "; }"
            );
        descriptor.AddScriptProperty("InvokeEditVoIPGatewayPostback",
            "function() { " + this.Page.ClientScript.GetPostBackEventReference(this, "EditVoIPGateway") + "; }"
            );
        yield return descriptor;
    }

    public override IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = this.ResolveUrl("VoIPGatewayControl.js");
        yield return reference;
    }

    #endregion

    #region IPostBackEventHandler Members

    public void RaisePostBackEvent(string eventArgument)
    {
        switch (eventArgument)
        {
            case "AddVoIPGateway":
                Response.Redirect(AddVoIPGatewayUrl);
                break;
            case "EditVoIPGateway":
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var list = serializer.Deserialize<List<int>>(selectedItems.Value);
                Response.Redirect(string.Format(EditVoIPGatewayUrl, string.Join(",", list)));
                break;
        }
    }

    #endregion
}
