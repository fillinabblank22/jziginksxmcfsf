using System;
using System.Web.UI.WebControls;
using SolarWinds.Orion.IpSla.Common.Settings;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;

public partial class Voip_Settings : System.Web.UI.Page
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();   
    protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			RefreshData();
		}
	}

	private void RefreshData()
	{
		UdpJitterPort.Text = SolarWinds.Orion.IpSla.Data.Config.UdpJitterPort.ToString();

        JitterCodec.Items.Clear();
		JitterCodec.Items.Add(new ListItem("G.711a", CodecType.G711ALAW.ToString()));
		JitterCodec.Items.Add(new ListItem("G.711u", CodecType.G711ULAW.ToString()));
		JitterCodec.Items.Add(new ListItem("G.729a", CodecType.G729A.ToString()));

		CodecType currentValue = SolarWinds.Orion.IpSla.Data.Config.JitterCodec;
		foreach (ListItem item in JitterCodec.Items)
		{
			if ((CodecType)Enum.Parse(typeof(CodecType), item.Value) == currentValue)
			{
				JitterCodec.SelectedIndex = JitterCodec.Items.IndexOf(item);
				break;
			}
		}

	    RetentionDays.Text = ((int)Config.IpSlaDataRetention.TotalDays).ToString();
		MOSAdvantageFactor.Text = Config.MOSAdvantageFactor.ToString();
		TypeOfService.Text = Config.TypeOfService.ToString();
        using (var blProxy = BusinessLayer.GetBusinessLayer())
        {
            TestInterval.Text = blProxy.CCMPollingIntervalMinutes.ToString();
            OperationsInRunningConfig.Checked = blProxy.ShowOperationsInRunningConfig;
        }
	    CcmCallSucessCodes.Text = Config.CcmCallSucessCodes;
	    CdrCmrJobTimeOut.Text = Config.CdrCmrJobTimeOut.ToString();

	    VoipGatewayWarningLevel.Text = Config.VoipGatewayWarningLevel.ToString();
	    VoipGatewayCriticalLevel.Text = Config.VoipGatewayCriticalLevel.ToString();
	    VoipGatewayPollingIntervalMinutes.Text = Config.VoipGatewayPollingIntervalMinutes.ToString();
        VoipGatewayRetentionDays.Text = Config.VoipGatewayDataRetentionDays.ToString();
	    VoipGatewayMaxConcurrentSipCalls.Text = Config.VoipGatewayDefaultMaxConcurrentSipCalls.ToString();

        chbEnableAvayaDetection.Checked = Config.EnableAvayaDetection;
        disableFtpCompressionCheckBox.Checked = Config.CcmDisableFtpCompression;
    }

    private void StoreData()
    {
        Config.UdpJitterPort = int.Parse(UdpJitterPort.Text);
        Config.JitterCodec = (CodecType)Enum.Parse(typeof(CodecType), JitterCodec.SelectedValue);
		Config.MOSAdvantageFactor = int.Parse(MOSAdvantageFactor.Text);
		Config.TypeOfService = int.Parse(TypeOfService.Text);
        Config.IpSlaDataRetention = TimeSpan.FromDays(int.Parse(RetentionDays.Text));

        Config.CdrCmrJobTimeOut = TimeSpan.Parse(CdrCmrJobTimeOut.Text);
        Config.VoipGatewayCriticalLevel = int.Parse(VoipGatewayCriticalLevel.Text);
        Config.VoipGatewayWarningLevel = int.Parse(VoipGatewayWarningLevel.Text);
        Config.VoipGatewayPollingIntervalMinutes = int.Parse(VoipGatewayPollingIntervalMinutes.Text);
        Config.VoipGatewayDataRetentionDays = int.Parse(VoipGatewayRetentionDays.Text);
        Config.VoipGatewayDefaultMaxConcurrentSipCalls = int.Parse(VoipGatewayMaxConcurrentSipCalls.Text);

        bool isCcmCallSucessCodesDirty = !string.Equals(Config.CcmCallSucessCodes, CcmCallSucessCodes.Text);
        Config.CcmCallSucessCodes = CcmCallSucessCodes.Text;

        using (var blProxy = BusinessLayer.GetBusinessLayer())
        {
            blProxy.CCMPollingIntervalMinutes = int.Parse(TestInterval.Text);
            blProxy.ShowOperationsInRunningConfig = OperationsInRunningConfig.Checked;
            
            if (isCcmCallSucessCodesDirty)
                blProxy.RefreshCallsStatus();
        }

        Config.EnableAvayaDetection = chbEnableAvayaDetection.Checked;
        Config.CcmDisableFtpCompression = disableFtpCompressionCheckBox.Checked;
    }

    protected void Default_Click(object sender, EventArgs e)
	{
		Config.RestoreDefaultSettings();
		RefreshData();
	}

    protected void Ok_Click(object sender, EventArgs e)
    {
        StoreData();
        Response.Redirect("Default.aspx");
    }

    protected void Cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }
}
