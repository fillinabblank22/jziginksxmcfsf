﻿using System;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.IpSla.Data;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;

public partial class Orion_Voip_IpSlaOperation : VoipView, IIpSlaOperationProvider, INodeProvider
{
    private static readonly Log log = new Log();
    private IpSlaOperation ipSlaOperation;

    public override string HelpFragment
    {
        get { return "OrionIPSLAManagerPHViewOperationDetails"; }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        this.ViewInfo = GetViewForDeviceType();
        if (this.ViewInfo != null)
        {
            this.Title = String.Format(VNQMWebContent.VNQMWEBCODE_TM0_7,this.ViewInfo.ViewTitle, HttpUtility.HtmlEncode(IpSlaOperation.Name));

            this.resContainer.DataSource = this.ViewInfo;
            this.resContainer.DataBind();
        }

        OperationTitle.Operation = IpSlaOperation;
        //breadcrumbs:
        if (IpSlaOperation != null)
        {
            var breadCrumbs = new StringBuilder(String.Format("<a href=\"/Orion/Voip/Summary.aspx\">{0}</a> &gt; ",  VNQMWebContent.VNQMWEBDATA_AK1_93));
            //hop details
            if (IsHop)
            {
                breadCrumbs.Append(string.Format("<a href=\"{0}\">{1}</a> &gt;",
                    IpSlaOperationHelper.GetOperationDetailPageLink(IpSlaOperation.OperationInstanceID),
                    IpSlaOperationHelper.FixName(IpSlaOperation.Name)));
            }
            Master.Breadcrumb = breadCrumbs.ToString();
        }
    }

    public SolarWinds.Orion.NPM.Web.Node Node
    {
        get { return GetNode(); }
    }

    protected override ViewInfo GetViewForDeviceType()
    {
        if (null != this.NetObject)
        {
            int newViewID;

            if (Request.QueryString.AllKeys.Contains("ViewID"))
            {
                if (!int.TryParse(Request.QueryString["ViewID"], out newViewID))
                {
                    newViewID = 0;
                }
            }
            else if (Request.QueryString.AllKeys.Contains("viewid"))
            {
                if (!int.TryParse(Request.QueryString["viewid"], out newViewID))
                {
                    newViewID = 0;
                }
            }
            else
            {
                newViewID = (int)Profile.GetProfileGroup("Voip").GetPropertyValue("VoipIpSlaOperationViewID");
                if (newViewID <= 0)
                {
                    newViewID = GetViewID(GetViewType(this.IpSlaOperation), IpSlaOperation.Prefix);
                }
            }

            if (newViewID > 0)
            {
                ViewInfo view = ViewManager.GetViewById(newViewID);
                if (view == null)
                {
                    log.WarnFormat("ViewID {0} for device type {1} does not exist.", newViewID, this.IpSlaOperation.OperationType);
                }
                else
                    return view;
            }
        }

        return base.GetViewForDeviceType();
    }

    public override string ViewType
    {
        get { return "IpSlaOperationDetails"; }
    }

    private static int GetViewID(string deviceType, string objectType)
    {
        const string sql = @"
SELECT ViewID
FROM ViewsByDeviceType
WHERE RTRIM(ObjectType)=@objType AND DeviceType=@devType";

        using (SqlConnection conn = SolarWinds.Orion.Common.DatabaseFunctions.OpenNewDatabaseConnection())
        using (SqlCommand cmd = new SqlCommand(sql, conn))
        {
            cmd.Parameters.AddWithValue("@devType", deviceType.Trim());
            cmd.Parameters.AddWithValue("@objType", objectType.Trim());

            object retVal = cmd.ExecuteScalar();

            if (DBNull.Value == retVal)
                return 0;

            return Convert.ToInt32(retVal);
        }
    }

    public string ViewName
    {
        get
        {
            return ipSlaOperation.PathHopIndex.HasValue
                ? String.Format(VNQMWebContent.VNQMWEBCODE_TM0_4, ipSlaOperation.OperationTypeDescription)
                : String.Format(VNQMWebContent.VNQMWEBCODE_TM0_5, ipSlaOperation.OperationTypeDescription);
        }
    }

    public bool IsHop
    {
        get { return ipSlaOperation.PathHopIndex.HasValue; }
    }

    public string HopName
    {
        get
        {
            string notAvailableHtml = String.Format("<i>{0}</i>", VNQMWebContent.VNQMWEBCODE_VB1_134);
            var strHopIndex = notAvailableHtml;
            var strHopName = notAvailableHtml;
            var siteName = ipSlaOperation.SourceSiteName;

            if (ipSlaOperation.PathHopIndex.HasValue)
            {
                var hopDetailsTable = PathOperationResultsDAL.GetHopResults(ipSlaOperation.OperationInstanceID, ipSlaOperation.PathHopIndex.Value, ipSlaOperation.PathId, PathOperationResultsDAL.Columns.HopIpAddress | PathOperationResultsDAL.Columns.HopNodeCaption);
                if (hopDetailsTable != null && hopDetailsTable.Rows.Count > 0)
                {
                    var hopDetails = hopDetailsTable.Rows[0];
                    var hopIndex = hopDetails["HopIndex"] as int?;
                    if (hopIndex.HasValue)
                        strHopIndex = (hopIndex.Value + 1).ToString();
                    strHopName = hopDetails["HopNodeCaption"] as string ?? hopDetails["HopIpAddress"] as string ?? notAvailableHtml;
                }
            }
            return string.Format(VNQMWebContent.VNQMWEBCODE_TM0_6, strHopIndex, siteName, strHopName);
        }
    }

    public IpSlaOperation IpSlaOperation
    {
        get
        {
            if (this.ipSlaOperation == null)
                this.ipSlaOperation = CreateOperation();

            return this.ipSlaOperation;
        }
    }

    SolarWinds.Orion.NPM.Web.Node GetNode()
    {
        IpSlaOperation op = this.IpSlaOperation;
        if (op != null && op.SourceSite != null && op.SourceSite.VoipSite != null)
        {
            SolarWinds.Orion.NPM.Web.Node node = new SolarWinds.Orion.NPM.Web.Node(string.Format("N:{0}", op.SourceSite.VoipSite.NodeID));

            return node;
        }

        return null;
    }

    IpSlaOperation CreateOperation()
    {
        var netObjectID = Request["NetObject"];
        if (!string.IsNullOrEmpty(netObjectID) )
        {
            var operation = new IpSlaOperation(netObjectID);

            int i;
            operation.PathId = (int.TryParse(Request["Path"], out i) ? i : (int?)null);
            operation.PathHopIndex = (int.TryParse(Request["Hop"], out i) ? i - 1 : (int?)null);

            return operation;
        }

        return null;
    }

    private static string GetViewType(IpSlaOperation operation)
    {
        string viewType = null;
        switch (operation.OperationType)
        {
            case OperationType.Dhcp:
                viewType = "IpSlaDhcpOperation";
                break;
            case OperationType.Dns:
                viewType = "IpSlaDnsOperation";
                break;
            case OperationType.Ftp:
                viewType = "IpSlaFtpOperation";
                break;
            case OperationType.Http:
                viewType = "IpSlaHttpOperation";
                break;
            case OperationType.IcmpEcho:
                viewType = "IpSlaIcmpEchoOperation";
                break;
            case OperationType.RtpBasedVoip:
                viewType = "IpSlaRtpBasedVoipOperation";
                break;
            case OperationType.TcpConnect:
                viewType = "IpSlaTcpConnectOperation";
                break;
            case OperationType.UdpEcho:
                viewType = "IpSlaUdpEchoOperation";
                break;
            case OperationType.UdpJitter:
            case OperationType.UdpJitterCli:
                viewType = "IpSlaUdpJitterOperation";
                break;
            case OperationType.UdpVoipJitter:
            case OperationType.UdpVoipJitterCli:
                viewType = "IpSlaVoipJitterOperation";
                break;
            case OperationType.IcmpPathEcho:
                viewType = operation.PathHopIndex.HasValue
                    ? "IpSlaIcmpPathEchoOperationHop"
                    : "IpSlaIcmpPathEchoOperation";
                break;
            case OperationType.IcmpPathJitter:
                viewType = operation.PathHopIndex.HasValue
                    ? "IpSlaIcmpPathJitterOperationHop"
                    : "IpSlaIcmpPathJitterOperation";
                break;
            default:
                return null;
        }

        return viewType;
    }
}
