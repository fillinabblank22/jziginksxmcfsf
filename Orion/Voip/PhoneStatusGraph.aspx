<%@ Page Language="C#" MasterPageFile="~/Orion/Voip/VoipView.master" AutoEventWireup="true" CodeFile="PhoneStatusGraph.aspx.cs" Inherits="Orion_Voip_Resources_Graphs_PhoneStatusGraph" Title="<%$ Resources: VNQMWebContent, VNQMWEBDATA_TM0_57 %>" %>
<%@ Register Src="~/Orion/Voip/Resources/BaseStackedBarChart.ascx" TagPrefix="voip" TagName="BarChart" %>
<%@ Register TagPrefix="orion" TagName="IconHelpButton" Src="~/Orion/Controls/IconHelpButton.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="VoipPageTitle" Runat="Server">
    <h1><%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_AK1_130, PhoneName) %></h1>
    <link rel="stylesheet" type="text/css" href="/Orion/styles/OrionCSS.ashx?path=/Orion/VoIP/styles/Charts.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/Voip/styles/Charts.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="VoipMainContentPlaceHolder" Runat="Server">
<orion:Include ID="Include1" runat="server" Module="Voip" File="js/Print.js" />
<style type="text/css">
    ul, menu, dir { -webkit-padding-start: 16px; }
</style>
<div id="Printable">
    <voip:BarChart runat="server" ID="Chart" OnInit="Chart_Init" />
</div>
        <div style="padding-left:20px" id="CustomChartDiv" runat="server">
        <br />
        <asp:gridview  style="border:1px; border-style:solid;" id="VoipCustomChartTable" runat="server"></asp:gridview>
        <br />
        </div>  
        <table id="VoIPCustomChart">
            <tr>
                <td></td>
                <td colspan="2"><asp:ValidationSummary ID="ValidationSummary1" runat="server" /></td>
            </tr>
            <tr>
                <td style="width:26%"><b><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_132 %></b></td>
                <td><orion:LocalizableButton runat="server" ID="RefreshButtonTop" LocalizedText="Refresh" DisplayType="Primary" ToolTip="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_131 %>" OnClick="Refresh_Click" /></td>
            </tr>
            <tr>
                <td colspan="3">
                <hr class="Property"/>
                </td>
            </tr>
            <tr>
                <td colspan="3"><b><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_133 %></b></td>
            </tr>
            <tr>
                <td class="Tabbed"><%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_63 %></td>
                <td colspan="2"><asp:TextBox ID="TitleInput" runat="server" size="50"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="Tabbed"><%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_64 %></td>
                <td colspan="2"><asp:TextBox ID="SubTitle1Input" runat="server" size="50"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="Tabbed"><%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_65 %></td>
                <td colspan="2"><asp:TextBox ID="SubTitle2Input" runat="server" size="50"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="3">
                <hr class="Property"/>
                </td>
            </tr>
            <tr>
                <td><b><%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_67 %></b></td>
                <td colspan="2"><asp:DropDownList id="TimePeriodSelector" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td colspan="3"><%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_66 %></td>             
            </tr>
            <tr>
                <td colspan="3"><b><%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_68 %></b></td>
            </tr>
            <tr>
                <td><%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_69 %></td>
                <td><asp:TextBox ID="BeginningTimeInput" runat="server"></asp:TextBox></td>
                <td><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_134 %></td>
            </tr>
            <tr>
                <td><%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_71 %></td>
                <td><asp:TextBox ID="EndingTimeInput" runat="server"></asp:TextBox></td>
                <td><%= String.Format(Resources.VNQMWebContent.VNQMWEBDATA_AK1_146, "<b>" + DateTime.Now.Date.ToShortDateString() + "</b>", "<b>" + DateTime.Now.ToString("dd-MMM-yy") + "</b>", "<b>" + DateTime.Now.ToShortTimeString() + "</b>", "<b>" + DateTime.Now + "</b>", "<b>" + DateTime.Now.ToString("MMM dd,yyyy HH:mm") + "</b>") %></td>
            </tr>
            <tr>
                <td colspan="3">
                <hr class="Property"/>
                </td>
            </tr>
            <tr>
                <td><b><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_135 %></b></td>
                <td style="width: 12%"><asp:TextBox id="SampleIntervalInput" runat="server"></asp:TextBox><asp:RangeValidator ID="RangeValidatorSM" 
                    ControlToValidate="SampleIntervalInput"  ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_136 %>" MinimumValue="5" MaximumValue="1440"
		            type="Integer" runat="server">*</asp:RangeValidator></td>
                <td><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_137 %></td>
            </tr>
            <tr>
                <td colspan="3">
                <hr class="Property"/>
                </td>
            </tr>
            <tr>
                <td colspan="3"><b><%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_74 %></b></td>
            </tr>
            <tr>
                <td><%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_75 %></td>
                <td colspan="2"><asp:TextBox ID="WidthInput" runat="server"></asp:TextBox>
                <asp:RangeValidator ID="RangeValidatorWidth" ControlToValidate="WidthInput"  ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_138 %>" MinimumValue="100" MaximumValue="1600"
		            type="Integer" runat="server">*</asp:RangeValidator></td>
            </tr>
            <tr>
                <td><%= Resources.VNQMWebContent.VNQMWEBDATA_TM0_76 %></td>
                <td colspan="2"><asp:TextBox ID="HeightInput" runat="server"></asp:TextBox>
                <asp:RangeValidator ID="RangeValidatorHeight" ControlToValidate="HeightInput"  ErrorMessage="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_139 %>" MinimumValue="350" MaximumValue="1000"
		            type="Integer" runat="server">*</asp:RangeValidator></td>
            </tr>
            <tr>
                <td colspan="3">
                <hr class="Property"/>
                </td>
            </tr>
            <tr>
                <td><b><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_140 %></b></td>
                <td><asp:DropDownList ID="DataTableExistanceSelector" runat="server"></asp:DropDownList></td>
                <td><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_141 %></td>
            </tr>
            <tr>
                <td colspan="3">
                <hr class="Property"/>
                </td>
            </tr>
            <tr>
                <td><b><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_142 %></b></td>
                <td colspan="2"><asp:DropDownList ID="FontSizeSelector" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td colspan="3">
                <hr class="Property"/>
                </td>
            </tr>
            <tr>
                <td><b><%= Resources.VNQMWebContent.VNQMWEBDATA_AK1_143 %></b></td>
                <td colspan="2"><asp:DropDownList ID="LabelOrientationSelector" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td colspan="3">
                <hr class="Property"/>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="2"><orion:LocalizableButton runat="server" ID="RefreshButtonBottom" LocalizedText="Refresh" ToolTip="<%$ Resources: VNQMWebContent, VNQMWEBDATA_AK1_131 %>" DisplayType="Primary" OnClick="Refresh_Click" /></td>
            </tr>
        </table> 
</asp:Content>

