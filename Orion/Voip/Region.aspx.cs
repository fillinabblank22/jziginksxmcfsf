﻿using System;
using SolarWinds.Orion.IpSla.Web;

public partial class Orion_Voip_RegionDetails : VoipView, IRegionProvider
{
    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();
        base.OnInit(e);
    }
    public override string ViewType
    {
        get { return "Voip Region"; }
    }
    public override string HelpFragment
    {
        get { return "OrionIPSLAMonitorPHResourceRegionView"; }
    }

    public Region Region
    {
        get { return (Region)this.NetObject; }
    }
    protected void Page_Load(Object sender, EventArgs e)
    {
        var test = this.Region;
        Master.Breadcrumb = String.Format("<a href=\"/Orion/Voip/Summary.aspx\">{0}</a> &gt;", Resources.VNQMWebContent.VNQMWEBDATA_AK1_93);
    }
}