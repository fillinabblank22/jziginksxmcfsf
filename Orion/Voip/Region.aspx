﻿<%@ Page Language="C#"  MasterPageFile="VoipView.master" AutoEventWireup="true" CodeFile="Region.aspx.cs" Inherits="Orion_Voip_RegionDetails" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ MasterType VirtualPath="VoipView.master" %> 

<asp:Content ID="Content1"  ContentPlaceHolderID="VoipMainContentPlaceHolder" runat="server">
	<asp:ScriptManager id="ScriptManager1" runat="server" >
	</asp:ScriptManager>
	<orion:ResourceHostControl ID="ResourceHostControl1" runat="server">
		<orion:ResourceContainer runat="server" ID="resContainer" />
	</orion:ResourceHostControl>
</asp:Content>
