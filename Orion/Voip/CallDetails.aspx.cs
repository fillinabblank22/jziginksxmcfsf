﻿using System;
using SolarWinds.Orion.IpSla.Web;
using SolarWinds.Orion.IpSla.Web.DAL;


public partial class Orion_Voip_CallDetails : VoipView, ICallDetailsProvider
{
    protected readonly string UnLicensed = Resources.VNQMWebContent.VNQMWEBCODE_VB1_137;

    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();
        base.OnInit(e);
    }

    public override string ViewType
    {
        get { return "Voip CallDetails"; }
    }

    public CallDetails CallDetails
    {
        get
        {
            if (this.NetObject is CallDetails && this.NetObject != CallDetails.Empty)
            {
                return (CallDetails)this.NetObject;
            }
            else
            {
                return new CallDetails(CallDetailsDAL.Instance.GetCallDetails(int.Parse(Request["CallDetailsID"])));
            }
        }
    }

    public override void SelectView()
    {
        base.SelectView();
        // This view has assigned NetObject type so Core requires it but we don't always need it. 
        // Let's put an empty one here to prevent Core from throwing errors.
        if (this.NetObject == null)
        {
            this.NetObject = CallDetails.Empty;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Master.Breadcrumb = String.Format("<a href=\"/Orion/Voip/Summary.aspx\">{0}</a> &gt;", Resources.VNQMWebContent.VNQMWEBDATA_AK1_93);
        Page.Title = string.Format("{0} {1} &rarr; {2}", this.ViewInfo.ViewTitle, 
            (this.CallDetails.VoipCallDetails.OriginLicensed == true ? this.CallDetails.VoipCallDetails.CallingPartyNumber : UnLicensed), 
            (this.CallDetails.VoipCallDetails.DestLicensed == true ? this.CallDetails.VoipCallDetails.FinalCalledPartyNumber : UnLicensed));
    }

    public override string HelpFragment
    {
        get { return "OrionIPSLAMonitorPHViewCallDetails"; }
    }
 }