<%@ WebService Language="C#" Class="Oip" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.OrionImprovement.Common;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class Oip : WebService
{
    private static readonly SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    private void BLExceptionHandler(Exception ex)
    {
        _log.Error(ex);
        throw ex;
    }

    [WebMethod]
    public bool ChangeOptIn(bool enabled)
    {
        try
        {
            using (IOrionImprovementBusinessLayer proxy = OrionImprovementProxy.CreateOrionImprovementProxy(BLExceptionHandler))
            {
                _log.Info(string.Format("Starting to {0} Orion Improvement program.", enabled ? "enable" : "disable"));
                proxy.SetOipStatus(enabled);
            }

            using (var coreProxy = _blProxyCreator.Create(BLExceptionHandler))
            {
                var typeGuid = NotificationTypes.EvalNotificationTypeGuid;
                _log.InfoFormat("Acknowledging web notification of type {0}.", typeGuid);
                coreProxy.AcknowledgeNotificationItemsByType(typeGuid, HttpContext.Current.Profile.UserName, DateTime.UtcNow);
            }

        }
        catch (Exception ex)
        {
            BLExceptionHandler(ex);
        }

        return false;
    }
}