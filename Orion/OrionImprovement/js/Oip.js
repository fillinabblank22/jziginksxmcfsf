SW = SW || {}
SW.Orion = SW.Orion || {}
SW.Orion.OIP = SW.Orion.OIP || {}

// register event handlers for dialog buttons
$(function () {
    $(document).on("click", "#sw-oip-dialog-ok", function () {

        var checkbox = $(".sw-oip-dialog input[type=checkbox]");
        var isChecked = checkbox.is(":checked");

        // Make service call to enabled/disable OIP...
        SW.Core.Services.callWebService("/Orion/OrionImprovement/Services/Oip.asmx", "ChangeOptIn", { 'enabled': isChecked });


        if (SW.Orion.OIP.dialog) {
            SW.Orion.OIP.dialog.dialog('close');
        }
    });

    $(document).on("click", "#sw-oip-dialog-cancel", function () {
        if (SW.Orion.OIP.dialog) {
            SW.Orion.OIP.dialog.dialog('close');
        }
    });
});

SW.Orion.OIP.showDialog = function (showChecked) {

    var template =
		 "<div class='sw-oip-dialog'>"
		+ "	<div>"
		+ "		<input type='checkbox' id='oip_opt'/>"
		+ "		<label for='oip_opt'>@{R=OrionImprovement.Strings;K=WEBDATA_LH0_1;E=js}</label>"
		+ "	<div>"
		+ "	<div class='helplink'>"
		+ "		<a target='_blank' href='http://solarwinds.com/documentation/kbLoader.aspx?kb=4678&lang=@{R=Core.Strings;K=CurrentHelpLanguage;E=js}'>&raquo; @{R=Core.Strings;K=WEBDATA_AK0_404;E=js}</a>"
		+ "	</div>"
		+ "	<div class='buttons'>"
		+ "		<a id='sw-oip-dialog-ok' class='sw-btn-primary sw-btn' automation='OK' ><span class='sw-btn-c'><span class='sw-btn-t'>@{R=Core.Strings;K=WEBJS_AK0_53;E=js}</span></span></a>"
		+ "		<a id='sw-oip-dialog-cancel' class='sw-btn' automation='Cancel' ><span class='sw-btn-c'><span class='sw-btn-t'>@{R=Core.Strings;K=CommonButtonType_Cancel;E=js}</span></span></a>"
		+ "	</div>"
		+ "</div>";

    if (SW.Orion.OIP.dialog) {
        SW.Orion.OIP.dialog.dialog('open');
    }
    else {
        SW.Orion.OIP.dialog = $(template).dialog(
			{
			    'modal': true,
			    'width': 450,
			    'title': '@{R=OrionImprovement.Strings;K=WEBDATA_LH0_2;E=js}'
			});
    }

    $(".sw-oip-dialog input[type=checkbox]").prop('checked', showChecked === true);

};