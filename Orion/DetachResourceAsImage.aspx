﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/OrionMinReqs.master" %>

<script runat="server">
    
    protected override void OnInit(EventArgs e)
    {
        // What's up with this?  We want to use the Detach Resource page, but we want the url to be 
        // different from Detach Resource so that we can disable Hubble on this page but not on the
        // Detach Resource page.
        int id;
        Guid guid;
        if ((int.TryParse(Request["ReportId"], out id) || Guid.TryParse(Request["ReportGuid"], out guid))
            && Guid.TryParse(Request["ReportCellGuid"], out guid))
            HttpContext.Current.Server.Transfer("~/Orion/DetachReportCell.aspx");
        else
            HttpContext.Current.Server.Transfer("~/Orion/DetachResource.aspx");
        
        base.OnInit(e);
    }


</script>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" Runat="Server">
</asp:Content>

