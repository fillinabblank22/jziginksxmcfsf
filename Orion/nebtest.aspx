<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMinReqs.master" Title="Nested Expression Builder Test"  Inherits="System.Web.UI.Page" ValidateRequest="false" %>
<%@ Register TagPrefix="orion" TagName="PageHeader" Src="~/Orion/Controls/PageHeader.ascx" %>
<%@ Register TagPrefix="orion" TagName="PageFooter" Src="~/Orion/Controls/PageFooter.ascx" %>
<%@ Register TagPrefix="orion" TagName="FieldPicker" Src="~/Orion/Controls/FieldPicker/FieldPicker.ascx" %>


<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">
.sw-title-main { margin: 5px 0 5px 0; padding: 0; font-size: 18px; line-height: 22px; font-weight: normal; }
.sw-app-region a { color: #336699; }
.sw-app-region a:hover { color: #f99d1c; }
</style>

<orion:FieldPicker runat="server" ID="FieldPicker" ClientInstanceName="fieldPickerInput_Instance"  SingleMode="True"/>

<orion:Include runat="server" File="NestedExpressionBuilder.css" />
<orion:Include runat="server" File="NestedExpressionBuilder.js" />
	  
<script runat="server">

    public string GetFieldConstantConditionType()
    {
        var fieldConstant = new SolarWinds.Reporting.Models.Selection.Expr
        {
            NodeType = SolarWinds.Reporting.Models.Selection.ExprType.Operator,
            Child = new SolarWinds.Reporting.Models.Selection.Expr[] {
                new SolarWinds.Reporting.Models.Selection.Expr {
                    NodeType = SolarWinds.Reporting.Models.Selection.ExprType.Field
                },
                new SolarWinds.Reporting.Models.Selection.Expr {
                    NodeType = SolarWinds.Reporting.Models.Selection.ExprType.Constant
                }
            }
        };
        return Newtonsoft.Json.JsonConvert.SerializeObject(fieldConstant);
    }

    public string GetFieldFieldConditionType()
    {
        var fieldField = new SolarWinds.Reporting.Models.Selection.Expr
        {
            NodeType = SolarWinds.Reporting.Models.Selection.ExprType.Operator,
            Child = new SolarWinds.Reporting.Models.Selection.Expr[] {
                new SolarWinds.Reporting.Models.Selection.Expr {
                    NodeType = SolarWinds.Reporting.Models.Selection.ExprType.Field
                },
                new SolarWinds.Reporting.Models.Selection.Expr {
                    NodeType = SolarWinds.Reporting.Models.Selection.ExprType.Field
                }
            }
        };
        return Newtonsoft.Json.JsonConvert.SerializeObject(fieldField);
    }

</script>    
    
<script type="text/javascript">
$().ready(function () {

    var fakeDataSource = {"RefId":"c4727564-0ac1-4623-9f83-905a440304c7","Name":"Datasource 1","Type":1,"CommandText":"","MasterEntity":"Orion.Nodes","Filter":null,"EntityUri":[]};

	$('#rules_container').nested_expression_builder({
        dataSource: fakeDataSource, //not defined/used by control itself, but settings are passed to inputs for their use
		allowGroups: true,
        groupOperators: <%= Newtonsoft.Json.JsonConvert.SerializeObject((new SolarWinds.Orion.Core.Reporting.OperatorProvider()).GetGroupOperators().ToArray()) %>,
        logicalOperators: <%= Newtonsoft.Json.JsonConvert.SerializeObject((new SolarWinds.Orion.Core.Reporting.OperatorProvider()).GetLogicalOperators().ToArray()) %>,
        ruleTypes: [
            { displayName: "Add Simple Condition", expr: <%= GetFieldConstantConditionType() %> },
            { displayName: "Add Advanced Condition", expr: <%= GetFieldFieldConditionType() %> }
        ],
     //   fieldInput: SW.Core.NestedExpressionBuilder.Input.Field.Picker,
      fieldInput: SW.Core.NestedExpressionBuilder.Input.Field.DropDown, // use one or other for simple/advanced mode
        constantInputs: [] //alternate ones
	});

});
</script>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" Runat="Server">
<orion:PageHeader Minimalistic="true"  runat="server" />

<form runat="server" class="sw-app-region">

<div style="margin-top: 50px" id="rules_container"></div>

<div onclick="$('#rules_container').nested_expression_builder('getExpression'); return false;">getExpr</div>
</form>

<orion:PageFooter runat="server" />
</asp:Content>
