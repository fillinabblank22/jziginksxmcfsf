﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using Resources;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;

public partial class Orion_SRM_VolumePopup : Page
{
    protected Volume Volume { get; set; }
    public int ErrorLevel { get; set; }
    public int WarningLevel { get; set; }
    protected int WarningThresholdDays { get { return (int)SettingsDAL.GetSetting(SRMConstants.WarningThresholdDays).SettingValue; } }
    protected int CriticalThresholdDays { get { return (int)SettingsDAL.GetSetting(SRMConstants.CriticalThresholdDays).SettingValue; } }
    public int CapacityRunout { get; set; }

    protected override void OnPreInit(EventArgs e)
    {
        string netObjectID = Request.QueryString["NetObject"];
        try
        {
            this.Volume = NetObjectFactory.Create(netObjectID) as Volume;
        }
        catch (AccountLimitationException)
        {
            Server.Transfer(
                string.Format(CultureInfo.InvariantCulture, "/Orion/SRM/AccountLimitationPopup.aspx?NetObject={0}", netObjectID));
        }

        base.OnPreInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Volume != null)
        {
            HeaderLabel.Text = Volume.Name;
            StatusLabel.Text = FormatHelper.MakeBreakableString(string.Format(CultureInfo.CurrentCulture, "{0} {1}", SrmWebContent.Volume_Popup_Volume_Name, SrmStatusLabelHelper.GetStatusLabel(Volume.VolumeEntity.Status)));
            WebHelper.SetValueOrHide(VendorStatusCodeLabel, VendorStatusCodeRow, Volume.VolumeEntity.VendorStatusDescription);
            
            StoragePoolLabel.Text = string.Join(", ", ServiceLocator.GetInstance<IPoolDAL>().GetPoolsByVolumeId(Volume.VolumeEntity.VolumeID).Select(x => x.DisplayName));
            StoragePoolRow.Visible = !String.IsNullOrEmpty(StoragePoolLabel.Text);
            StorageArrayLabel.Text = Volume.StorageArray.Array.IsCluster ? String.Empty : Volume.StorageArray.Name;
            StorageArrayRow.Visible = !String.IsNullOrEmpty(StorageArrayLabel.Text);

            var dal = ServiceLocator.GetInstance<IThresholdDAL>();

            IResourceVisibilityService visibilityService = ServiceLocator.GetInstance<IResourceVisibilityService>();
            performanceTable.Visible = visibilityService.IsPerformanceAvailableForArray(this.Volume.StorageArray);

            if (performanceTable.Visible)
            {
                SetPerformanceData(this.Volume, dal);
            }


            VServerLabel.Text = string.Join(", ",  ServiceLocator.GetInstance<IVServerDAL>().GetVServersByVolumeId(Volume.VolumeEntity.VolumeID).Select(x=>x.DisplayName));
            VserverRow.Visible = !String.IsNullOrEmpty(VServerLabel.Text);
            ClusterLabel.Text = Volume.StorageArray.Array.IsCluster ? Volume.StorageArray.Name : String.Empty;
            ClusterRow.Visible = !String.IsNullOrEmpty(ClusterLabel.Text);
            
            var total = Volume.VolumeEntity.CapacityTotal;
            var consumed = Volume.VolumeEntity.CapacityAllocated;
            var remaining = Volume.VolumeEntity.CapacityFree;
            var fileSystem = Volume.VolumeEntity.CapacityFileSystem;

            TotalCapacityLabel.Text = WebHelper.FormatCapacityTotal(total);
            ConsumedCapacityLabel.Text = WebHelper.FormatCapacityTotal(consumed);
            RemainingCapacityLabel.Text =WebHelper.FormatCapacityTotal(remaining);
            if (Volume.VolumeEntity.Thin)
                WebHelper.MarkThresholds(ConsumedCapacityLabel, (float)Volume.VolumeEntity.ProvisionedCapacityPercent, dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeProvisionedPercent, Volume.VolumeEntity.VolumeID));
            if (Volume.VolumeEntity.CapacityRunout.HasValue)
            {
                CapacityRunout = WebHelper.GetDayDifference(Volume.VolumeEntity.CapacityRunout);
                ProjectedRunOutLabel.Text = WebHelper.FormatDaysDiff(CapacityRunout);
                WebHelper.MarkProjectedRunOut(
                    ProjectedRunOutLabel,
                    CapacityRunout,
                    WarningThresholdDays,
                    CriticalThresholdDays);
            }
            popupChartUserCapacity.ChartColors = new[] { PopupChartHelper.BlueChartColor, PopupChartHelper.GradientChartColor };
            popupChartUserCapacity.ChartLabelColors = new[] { PopupChartHelper.BluetFontColor, PopupChartHelper.GradientFontColor };
            popupChartUserCapacity.Values = new[] { consumed, total - consumed };
            popupChartUserCapacity.Total = total;

            ProjectedRunOutRow.Visible = Volume.VolumeEntity.Thin;

            #region Other potential issues - Marked by thresholds
            IOPSReadLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(Volume.VolumeEntity.IOPSRead ?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.VolumeDetails_IOPS_PS);
            WebHelper.MarkThresholdAndShowRow(IOPSReadLabel, IOPSReadRow, Volume.VolumeEntity.IOPSRead,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeIOPSRead, Volume.VolumeEntity.VolumeID));

            IOPSWriteLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(Volume.VolumeEntity.IOPSWrite ?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.VolumeDetails_IOPS_PS);
            WebHelper.MarkThresholdAndShowRow(IOPSWriteLabel, IOPSWriteRow, Volume.VolumeEntity.IOPSWrite,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeIOPSWrite, Volume.VolumeEntity.VolumeID));

            IOPSOtherLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(Volume.VolumeEntity.IOPSOther ?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.VolumeDetails_IOPS_PS);
            WebHelper.MarkThresholdAndShowRow(IOPSOtherLabel, IOPSOtherRow, Volume.VolumeEntity.IOPSOther,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeIOPSOther, Volume.VolumeEntity.VolumeID));
  

            ThroughputReadLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", WebHelper.FormatCapacityTotal(Volume.VolumeEntity.BytesPSRead), SrmWebContent.VolumeDetails_PerSecond);
            WebHelper.MarkThresholdAndShowRow(ThroughputReadLabel, ThroughputReadRow, Volume.VolumeEntity.BytesPSRead,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeThroughputRead, Volume.VolumeEntity.VolumeID));
            
            ThroughputWriteLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", WebHelper.FormatCapacityTotal(Volume.VolumeEntity.BytesPSWrite), SrmWebContent.VolumeDetails_PerSecond);
            WebHelper.MarkThresholdAndShowRow(ThroughputWriteLabel, ThroughputWriteRow, Volume.VolumeEntity.BytesPSWrite,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeThroughputWrite, Volume.VolumeEntity.VolumeID));
            
            LatencyReadLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(Volume.VolumeEntity.IOLatencyRead ?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.VolumeDetails_LatencyTotal_Unit);
            WebHelper.MarkThresholdAndShowRow(LatencyReadLabel, LatencyReadRow, Volume.VolumeEntity.IOLatencyRead,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeLatencyRead, Volume.VolumeEntity.VolumeID));
            
            LatencyWriteLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(Volume.VolumeEntity.IOLatencyWrite?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.VolumeDetails_LatencyTotal_Unit);
            WebHelper.MarkThresholdAndShowRow(LatencyWriteLabel, LatencyWriteRow, Volume.VolumeEntity.IOLatencyWrite,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeLatencyWrite, Volume.VolumeEntity.VolumeID));

            LatencyOtherLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(Volume.VolumeEntity.IOLatencyOther ?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.PopUp_msUnit);
            WebHelper.MarkThresholdAndShowRow(LatencyOtherLabel, LatencyOtherRow, Volume.VolumeEntity.IOLatencyOther,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeLatencyOther, Volume.VolumeEntity.VolumeID));

            IOSizeTotalLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0}", WebHelper.FormatCapacityTotal(Volume.VolumeEntity.IOSizeTotal));
            WebHelper.MarkThresholdAndShowRow(IOSizeTotalLabel, IOSizeTotalRow, Volume.VolumeEntity.IOSizeTotal,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeIOSizeTotal, Volume.VolumeEntity.VolumeID));
            
            IOSizeReadLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0}", WebHelper.FormatCapacityTotal(Volume.VolumeEntity.IOSizeRead));
            WebHelper.MarkThresholdAndShowRow(IOSizeReadLabel, IOSizeReadRow, Volume.VolumeEntity.IOSizeRead,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeIOSizeRead, Volume.VolumeEntity.VolumeID));
            
            IOSizeWriteLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0}", WebHelper.FormatCapacityTotal(Volume.VolumeEntity.IOSizeWrite));
            WebHelper.MarkThresholdAndShowRow(IOSizeWriteLabel, IOSizeWriteRow, Volume.VolumeEntity.IOSizeWrite,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeIOSizeWrite, Volume.VolumeEntity.VolumeID));

            if (!IOSizeTotalRow.Visible && !IOSizeReadRow.Visible && !IOSizeWriteRow.Visible && !IOPSReadRow.Visible && !IOPSWriteRow.Visible && !IOPSOtherRow.Visible && !LatencyReadRow.Visible && !LatencyWriteRow.Visible && !ThroughputReadRow.Visible && !ThroughputWriteRow.Visible && !LatencyOtherRow.Visible)
                OtherPotentialIssuesSection.Visible = false;
            #endregion
        }
        popupChartUserCapacity.RenderTotalLabel = false;
        popupChartUserCapacity.Height = PopupChartHelper.Height;
    }

    private void SetPerformanceData(Volume volume, IThresholdDAL thresholdDal)
    {
        NetObjectPerformance volumePerformance = ServiceLocator.GetInstance<IVolumeDAL>().GetVolumePerformance(volume.VolumeEntity.VolumeID);

        if (volumePerformance == null)
        {
            return;
        }

        TotalIOPSLabel.Text = volumePerformance.TotalIOPS.HasValue
            ? string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(volumePerformance.TotalIOPS.Value, 2, MidpointRounding.AwayFromZero), SrmWebContent.VolumeDetails_IOPS_PS)
            : SrmWebContent.Value_Unknown;
        WebHelper.MarkThresholds(TotalIOPSLabel, volume.VolumeEntity.IOPSTotal, thresholdDal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeIOPSTotal, volume.VolumeEntity.VolumeID));

        TotalThroughputLabel.Text = volumePerformance.TotalThroughput.HasValue
            ? string.Format(CultureInfo.CurrentCulture, "{0} {1}", WebHelper.FormatCapacityTotal(volumePerformance.TotalThroughput.Value), SrmWebContent.VolumeDetails_PerSecond)
            : SrmWebContent.Value_Unknown;
        WebHelper.MarkThresholds(TotalThroughputLabel, volume.VolumeEntity.BytesPSTotal, thresholdDal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeThroughputTotal, volume.VolumeEntity.VolumeID));

        TotalLatencyLabel.Text = volumePerformance.Latency.HasValue
            ? string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(volumePerformance.Latency.Value, 2, MidpointRounding.AwayFromZero), SrmWebContent.VolumeDetails_LatencyTotal_Unit)
            : SrmWebContent.Value_Unknown;
        WebHelper.MarkThresholds(TotalLatencyLabel, volume.VolumeEntity.IOLatencyTotal, thresholdDal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeLatencyTotal, volume.VolumeEntity.VolumeID));
    }
}