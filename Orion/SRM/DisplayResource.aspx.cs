﻿using System.Globalization;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.Dependencies;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;

public partial class Orion_SRM_DisplayResource : System.Web.UI.Page
{
    private readonly Log log = new Log();
    public Dictionary<string, string> Parameters;
    private string redirIdName = "redirId";

    private NetObject netObject;
    private ResourceInfo resourceInfo;
    private StandardChartResource customChart;

    protected void Page_Load(object sender, EventArgs e)
    {
        ReadParameters();
        
        InitNetObject();

        InitResource();
    }

    private void InitResource()
    {
        if (Parameters.ContainsKey("ResourceID"))
        {
            resourceInfo = ResourceManager.GetResourceByID(Convert.ToInt32(Parameters["ResourceID"]));
            this.Context.Items[typeof(ViewInfo).Name] = resourceInfo.View;
        }
        else
        {
            InitializeNewResourceInfo();
        }
    }

    private void InitializeNewResourceInfo()
    {
        resourceInfo = new ResourceInfo();
        resourceInfo.Column = 1;
        if (Parameters.ContainsKey("ViewID"))
        {
            try
            {
                resourceInfo.View = ViewManager.GetViewById(Convert.ToInt32(Parameters["ViewID"]));
            }
            catch (Exception ex)
            {
                log.Error("Cannot get View by Parameter ViewID");
                if (!Parameters.ContainsKey("NetObject"))
                    throw new InvalidOperationException("NetObject is missing in the parameters.");

                var viewType = NetObjectFactory.GetViewTypeForNetObject(Parameters["NetObject"]);
                resourceInfo.View = ViewManager.GetViewsByType(viewType).FirstOrDefault();
            }
        } 
        else
        {
            if (!Parameters.ContainsKey("NetObject"))
                throw new InvalidOperationException("NetObject is missing in parameters");

            var viewType = NetObjectFactory.GetViewTypeForNetObject(Parameters["NetObject"]);
            resourceInfo.View = ViewManager.GetViewsByType(viewType).FirstOrDefault();

            // CustomQueryTable.ascx.cs checks LimitationID, so placing fake ViewInfo to avoid crash
            if (this.Context.Items[typeof(ViewInfo).Name] == null)
            {
                this.Context.Items[typeof(ViewInfo).Name] = new ViewInfo("SRM", String.Empty, String.Empty, false, false, null);
            }
        }

        InitializeResourceFromParameters();
        SetupTitleAndBorders();
        LoadAdditionalScriptsAndStyles();

        var objectSwisType = DependencyUIItem.GetSwisEntityTypeByEntityNameOrObjectFilter(resourceInfo.Properties["EntityName"], resourceInfo.Properties["ObjectFilter"]);

        if (resourceInfo.File != null && resourceInfo.IsAspNetControl)
        {
            var path = resourceInfo.File;
            BaseResourceControl resourceControl = (BaseResourceControl)LoadControl(path);
            resourceControl.Resource = resourceInfo;            

            ResourceHostControl host = null;
            if (resourceControl is StandardChartResource)
            {
                customChart = resourceControl as StandardChartResource;
            }
            else
            {
                // case when chart is embeded into Custom Object Resource
                var embeddedPath = resourceInfo.Properties["EmbeddedObjectResource"];
                if (!string.IsNullOrEmpty(embeddedPath))
                {
                    var embeddedData = embeddedPath.Split(';');

                    Control control = LoadControl(embeddedData[0]);
                    control.ID = "ObjectResourceControl";
                    customChart = control as StandardChartResource;
                }

                if ((netObject != null) && !string.IsNullOrEmpty(objectSwisType))
                {
                    host = ResourceHostManager.GetResourceHostControl(objectSwisType, netObject);
                }
            }
            
            if (customChart != null)
            {
                customChart.Resource = resourceInfo;
            }

            if (host == null)
            {
                string netObjectViewType;
                try
                {
                    netObjectViewType = NetObjectFactory.GetViewTypeForNetObject(Parameters["NetObject"]);
                }
                catch (Exception ex)
                {
                    log.Error(string.Format("Unable to get view type for net object '{0}', resource view type will be used '{1}'", Parameters["NetObject"], resourceInfo.View.ViewType), ex);
                    netObjectViewType = resourceInfo.View.ViewType;
                }
                host = ResourceHostManager.GetSupportingControl(customChart, netObjectViewType);
            }

            this.divContainer.Controls.Add(host);
            host.LoadFromRequest();
            
            if (resourceControl != null)
                host.Controls.Add(resourceControl);
        }
    }

    private void LoadAdditionalScriptsAndStyles()
    {
        int autoRefreshSeconds = int.Parse(WebSettingsDAL.Get("Auto Refresh")) * 60;
        HtmlMeta metaTag = new HtmlMeta();
        metaTag.HttpEquiv = "REFRESH";
        metaTag.Content = string.Format("{0}; URL='{1}'", autoRefreshSeconds, this.Request.Url.ToString());
        this.Page.Header.Controls.Add(metaTag);

        OrionInclude.CoreThemeStylesheets();
    }

    private void SetupTitleAndBorders()
    {
        bool showResourceOnly = false;
        if (Parameters.ContainsKey("ResourceOnly") && Parameters["ResourceOnly"] == "1")
            showResourceOnly = true;

        if (null == this.netObject)
        {
            this.Title = Macros.ParseMacros(this.resourceInfo.Title, string.Empty);
        }
        else
        {
            if (this.netObject is SolarWinds.Orion.NPM.Web.Interface)
            {
                this.Title = string.Format(CultureInfo.CurrentCulture, "{0} - {1}", ((SolarWinds.Orion.NPM.Web.Interface)this.netObject).Caption, Macros.ParseMacros(this.resourceInfo.Title, this.netObject.NetObjectID));
            }
            else
            {
                this.Title = string.Format(CultureInfo.CurrentCulture, "{0} - {1}", this.netObject.Name, Macros.ParseMacros(this.resourceInfo.Title, this.netObject.NetObjectID));
            }
        }

        this.divContainer.Style[HtmlTextWriterStyle.Width] = string.Format(CultureInfo.InvariantCulture, "{0}px", resourceInfo.Width);

        if (!showResourceOnly)
            this.divContainer.Style[HtmlTextWriterStyle.MarginLeft] = "12px";

        if (showResourceOnly)
        {
            ResourceTitle.Visible = false;
        }
    }

    private void InitializeResourceFromParameters()
    {
 	    if (Parameters.ContainsKey("ResourceFile") && resourceInfo.File == null)
        {
            resourceInfo.File = Parameters["ResourceFile"];
        }
        if (Parameters.ContainsKey("ResourceTitle"))
        {
            resourceInfo.Title = Parameters["ResourceTitle"];
        }
        if (Parameters.ContainsKey("ResourceSubTitle"))
        {
            resourceInfo.SubTitle = Parameters["ResourceSubTitle"];
        }
        if (Parameters.ContainsKey("ResourceName"))
        {
            resourceInfo.Name = Parameters["ResourceName"];
        }

        if (Parameters.ContainsKey("Width"))
        {
            resourceInfo.View.Column1Width = int.Parse(Parameters["Width"]);
        }

        foreach (var kvp in Parameters)
        {
            resourceInfo.Properties.Add(kvp.Key, kvp.Value);
        }
    }

    private void InitNetObject()
    {
        if (Parameters.ContainsKey("NetObject"))
        {
            try
            {
                netObject = NetObjectFactory.Create(Parameters["NetObject"] );
            }
            catch (SolarWinds.Orion.Web.AccountLimitationException)
            {
                this.Page.Server.Transfer(string.Format(CultureInfo.InvariantCulture, "/Orion/AccountLimitationError.aspx?NetObject={0}", Parameters["NetObject"]));
            }            
        }
    }



    private void ReadParameters()
    {
            var redirId = Request.QueryString[redirIdName];
            Parameters = null;
            if (redirId != null)
            {
                var paramSession = (Dictionary<string, string>)Session[redirId];
                Parameters = paramSession;
            }
            if (Parameters == null)
            {
                Parameters = new Dictionary<string, string>();
            }
            foreach (var key in Request.QueryString.AllKeys)
            {
                if (key != redirIdName && !Parameters.ContainsKey(key))
                    Parameters.Add(key, Request.QueryString[key]);
            }
    }
}