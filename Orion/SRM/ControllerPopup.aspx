﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ControllerPopup.aspx.cs" Inherits="Orion_SRM_ControllerPopup" %>

<%@ Register TagPrefix="srm" TagName="PopupChart" Src="~/Orion/SRM/Controls/PopupChart.ascx" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>

<h3 class="Status<%=Enum.IsDefined(typeof (StorageControllerStatus), StorageController.StorageControllerEntity.Status)?(StorageController.StorageControllerEntity.Status).ToString():StorageControllerStatus.Unknown.ToString()%>">
    <%=SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(StorageController.Name)%></h3>
<div class="NetObjectTipBody">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2">
                <asp:label runat="server" id="StatusLabel" />
            </td>
        </tr>
        <tr id="ManufacturerRow" runat="server">
            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.StorageControllerDetails_Manufacturer%>:</th>
            <td>
                <asp:literal runat="server" id="ManufacturerLabel" />
            </td>
        </tr>
        <tr id="ModelRow" runat="server">
            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.StorageControllerDetails_Model%>:</th>
            <td>
                <asp:literal runat="server" id="ModelLabel" />
            </td>
        </tr>
        <tr id="FirmwareRow" runat="server">
            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.StorageControllerDetails_Firmware%>:</th>
            <td>
                <asp:literal runat="server" id="FirmwareLabel" />
            </td>
        </tr>
        <tr id="CPUCountRow" runat="server">
            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.StorageControllerDetails_CPUCount%>:</th>
            <td>
                <asp:literal runat="server" id="CPUCountLabel" />
            </td>
        </tr>
        <tr id="CPUFrequencyRow" runat="server">

            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.StorageControllerDetails_CPUFrequency%>:</th>
            <td>
                <asp:literal runat="server" id="CPUFrequencyLabel" />
            </td>
        </tr>
        <tr id="EthernetPortsCountRow" runat="server">
            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.StorageControllerDetails_EthernetPortsCount%>:</th>
            <td>
                <asp:literal runat="server" id="EthernetPortsCountLabel" />
            </td>
        </tr>
        <tr id="ISCSIPortsCountRow" runat="server">
            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.StorageControllerDetails_ISCSIPortsCount%>:</th>
            <td>
                <asp:literal runat="server" id="ISCSIPortsCountLabel" />
            </td>
        </tr>
        <tr id="ControllerTypeRow" runat="server">
            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.StorageControllerDetails_Type%>:</th>
            <td>
                <asp:literal runat="server" id="ControllerTypeLabel" />
            </td>
        </tr>
        <tr id="TotalMemoryRow" runat="server">
            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.StorageControllerDetails_TotalMemory%>:</th>
            <td>
                <asp:literal runat="server" id="TotalMemoryLabel" />
            </td>
        </tr>
        <tr id="TotalCacheSizeRow" runat="server">
            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.StorageControllerDetails_TotalCacheSize%>:</th>
            <td>
                <asp:literal runat="server" id="TotalCacheSizeLabel" />
            </td>
        </tr>
    </table>
    <hr id="ControllerPerfSeparator" runat="server"/>
    <table cellpadding="0" cellspacing="0">
        <tr id="DistIOPSRow" runat="server">

            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.StorageControllerDetails_IOPS_Distribution%>:</th>
            <td>
                <asp:label runat="server" id="DistIOPSLabel" />
            </td>
        </tr>
        <tr id="DistThroughputRow" runat="server">

            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.StorageControllerDetails_Throughput_Distribution%>:</th>
            <td>
                <asp:label runat="server" id="DistThroughputLabel" />
            </td>
        </tr>
        <tr id="LatencyRow" runat="server">

            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.StorageControllerDetails_Latency%>:</th>
            <td>
                <asp:label runat="server" id="LatencyLabel" />
            </td>
        </tr>
        <tr id="IOSizeRow" runat="server">

            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.StorageControllerDetails_IOSize%>:</th>
            <td>
                <asp:label runat="server" id="IOSizeLabel" />
            </td>
        </tr>
    </table>
    <div style="padding-top: 10px; font-weight: bold;">
        <asp:PlaceHolder runat="server" id="extensionPlaceholder" />
    </div>
</div>