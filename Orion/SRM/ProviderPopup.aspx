﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProviderPopup.aspx.cs" Inherits="Orion_SRM_ProviderPopup" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Import Namespace="SolarWinds.SRM.Web.NetObjects" %>

<h3 class="Status<%=StorageProvider.Provider.Status.ToString()%>">
    <%=SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(StorageProvider.Name)%></h3>
<div class="NetObjectTipBody">
   <table cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2">
                <asp:literal runat="server" id="StatusLabel" />
            </td>
        </tr>
        <tr id="NameRow" runat="server">
            <th style="min-width: 110px; text-align: left; width: 110px;"> <%= SrmWebContent.Provider_Polling_IPAddress%>:</th>
            <td colspan="2"><%= StorageProvider.Name %></td>
        </tr>
       <tr id="CimomVersionRow" runat="server">
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Provider_Cimom_Version%>:</th>
            <td colspan="2">
                <asp:literal runat="server" id="CimomVersionLabel" />
            </td>
        </tr>
       <tr id="ProviderVersionRow" runat="server">
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Provider_VersionPopup%>:</th>
            <td colspan="2">
                <asp:literal runat="server" id="ProviderVersionLabel" />
            </td>
        </tr>
       <tr id="SyncIntervalRow" runat="server">
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Provider_Sync_Interval%>:</th>
            <td colspan="2">
                <asp:literal runat="server" id="SyncIntervalLabel" />
            </td>
        </tr>
       <tr id="LastSyncTimeRow" runat="server">
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Provider_Last_SyncTime%>:</th>
            <td colspan="2">
                <asp:literal runat="server" id="LastSyncTimeLabel" />
            </td>
        </tr>
    </table>
</div>
