﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/View.master" AutoEventWireup="true" CodeFile="ArrayDetailsView.aspx.cs" Inherits="Orion_SRM_ArrayDetailsView" %>

<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web" %>
<%@ Import Namespace="SolarWinds.SRM.Web.NetObjects" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register Src="~/Orion/SRM/Controls/BreadcrumbBar.ascx" TagPrefix="orion" TagName="BreadcrumbBar" %>
<%@ Register TagPrefix="npm" TagName="IntegrationIcons" Src="~/Orion/NetPerfMon/Controls/IntegrationIcons.ascx" %>
<%@ Register TagPrefix="orion" TagName="StatusIconControl" Src="~/Orion/NetPerfMon/Controls/StatusIconControl.ascx" %>
<%@ Register TagPrefix="npm" TagName="NodeLink" Src="~/Orion/NetPerfMon/Controls/NodeLink.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ViewPageTitle">
    <orion:Include ID="Include5" runat="server" Module="SRM" File="SRM.css" />
    <orion:BreadcrumbBar runat="server" id="BreadcrumbBar" />
    <h1>
        <%= ViewInfo.IsSubView ? ViewInfo.ViewGroupTitle : ViewInfo.ViewHtmlTitle%> 
	    - 
        <img class="StatusIcon" src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.StorageArrays%>&status=<%=(int)((StorageArray)this.NetObject).Array.Status%>&size=small" />
        <a class="srm-breadcrumbbar-link" href="/Orion/View.aspx?NetObject=<%=(string)this.NetObject.NetObjectID%>">
            <%= this.NetObject.Name %>
        </a>
	    <asp:PlaceHolder ID="TitleDecorators" runat="server"></asp:PlaceHolder>	    
        <%= ViewInfo.IsSubView ? ViewInfo.ViewHtmlTitle : string.Empty%>
    </h1>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MainContentPlaceHolder">
    <orion:ResourceHostControl ID="ResourceHostControl1" runat="server">
        <orion:ResourceContainer runat="server" ID="resContainer" />
    </orion:ResourceHostControl>
</asp:Content>
