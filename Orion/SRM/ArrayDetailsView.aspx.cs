﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI.Models;

public partial class Orion_SRM_ArrayDetailsView : OrionView, IStorageArrayProvider, IModelProvider
{
    private readonly string hiddenConfigControl = "SRM-ManageVnxArrayControlDisplayState";

    protected override void OnInit(EventArgs e)
    {
        this.Title = UIHelper.NormalizeSpaces(this.ViewInfo.ViewTitle);
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    public override string ViewType
    {
        get { return "SRM Array Details"; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public StorageArray Array
    {
        get { return new StorageArray(NetObject.NetObjectID); }
    }

    public IPageModelProvider PageModelProvider
    {
        get { return new ArrayPageModelProvider(Array); }
    }

}