﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VolumePopup.aspx.cs" Inherits="Orion_SRM_VolumePopup" %>
<%@ Register TagPrefix="srm" TagName="PopupChart" Src="~/Orion/SRM/Controls/PopupChart.ascx" %>

<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>

<h3 class="Status<%=Enum.IsDefined(typeof (VolumeStatus), Volume.VolumeEntity.Status)?Volume.VolumeEntity.Status.ToString():VolumeStatus.Unknown.ToString()%>">
    <asp:literal runat="server" id="HeaderLabel" />
</h3>
<div class="NetObjectTipBody">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2">
                <asp:literal runat="server" id="StatusLabel" />
            </td>
        </tr>
         <tr runat="server" id="VendorStatusCodeRow">
                <th style="width: 110px; text-align: left; min-width: 110px;">
                    <%= SrmWebContent.VolumeDetails_VendorStatusCode%>:
                </th>
                <td>
                    <asp:Literal runat="server" ID="VendorStatusCodeLabel" /></td>
            </tr>
        <tr id="VolumeRow" runat="server">
            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.VolumeDetails_Name %>:</th>
            <td colspan="2"><%= Volume.Name %></td>
        </tr>
        <tr id="StoragePoolRow" runat="server">
            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.VolumeDetails_StoragePool%>:</th>
            <td colspan="2">
                <asp:literal runat="server" id="StoragePoolLabel" />
            </td>
        </tr>
        <tr id="StorageArrayRow" runat="server">
            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.VolumeDetails_Array%>:</th>
            <td colspan="2">
                <asp:literal runat="server" id="StorageArrayLabel" />
            </td>
        </tr>
        <tr id="VserverRow" runat="server">
            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.VolumeDetails_VServer%>:</th>
            <td colspan="2">
                <asp:literal runat="server" id="VServerLabel" />
            </td>
        </tr>
        <tr  id="ClusterRow" runat="server">
            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.VolumeDetails_Cluster%>:</th>
            <td colspan="2">
                <asp:literal runat="server" id="ClusterLabel" />
            </td>
        </tr>
    </table>

    <div runat="server" id="performanceTable">
        <hr />
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.VolumeDetails_TotalIOPS%>:</th>
                <td colspan="2">
                    <asp:Label runat="server" id="TotalIOPSLabel" />
                </td>
            </tr>
            <tr>
                <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.VolumeDetails_TotalThroughput%>:</th>
                <td colspan="2">
                    <asp:Label runat="server" id="TotalThroughputLabel" />
                </td>
            </tr>
            <tr>
                <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.VolumeDetails_TotalLatency%>:</th>
                <td colspan="2">
                    <asp:Label runat="server" id="TotalLatencyLabel" />
                </td>
            </tr>
        </table>
    </div>

    <hr />
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.VolumeDetails_TotalCapacity%>:</th>
            <td colspan="2">
                <asp:Label runat="server" id="TotalCapacityLabel" />
            </td>
        </tr>
        <tr>
            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.VolumeDetails_ConsumedCapacity%>:</th>
            <td colspan="2">
                <asp:Label runat="server" id="ConsumedCapacityLabel" />
            </td>
        </tr>
        <tr>
            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.VolumeDetails_RemainingCapacity%>:</th>
            <td colspan="2">
                <asp:Label runat="server" id="RemainingCapacityLabel" />
            </td>
        </tr>
        <tr runat="server" id="ProjectedRunOutRow">
            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.VolumeDetails_ProjectedRunOut%>:</th>
            <td colspan="2">
                <asp:Label runat="server" id="ProjectedRunOutLabel" />
            </td>
        </tr>
    </table>
    <hr style="margin-bottom: 2px; margin-top: 2px;" />
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><%= SrmWebContent.VolumeDetails_UserCapacity%>:</th>
            <td></td>
        </tr>
         <tr>
            <td colspan="2">
                <srm:PopupChart ID="popupChartUserCapacity"  runat="server" />
            </td>
        </tr>
    </table>
    <div id="OtherPotentialIssuesSection" runat="server">
        <hr style="margin-bottom: 2px; margin-top: 2px;"/>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.Popups_OtherIssues%>:</th>
                <td></td>
            </tr>
            <tr id="IOPSReadRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_ReadIOPS%>:</th>
                <td>
                    <asp:label runat="server" id="IOPSReadLabel" />
                </td>
            </tr>
            <tr id="IOPSWriteRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_WriteIOPS%>: </th>
                <td>
                    <asp:label runat="server" id="IOPSWriteLabel" />
                </td>
            </tr>
            <tr id="IOPSOtherRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_OtherIOPS%>: </th>
                <td>
                    <asp:label runat="server" id="IOPSOtherLabel" />
                </td>
            </tr>
            <tr id="ThroughputReadRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_ReadThroughput%>: </th>
                <td>
                    <asp:label runat="server" id="ThroughputReadLabel" />
                </td>
            </tr>
            <tr id="ThroughputWriteRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_WriteThroughput%>: </th>
                <td>
                    <asp:label runat="server" id="ThroughputWriteLabel" />
                </td>
            </tr>
             <tr id="LatencyReadRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_ReadLatency%>: </th>
                <td>
                    <asp:label runat="server" id="LatencyReadLabel" />
                </td>
            </tr>
             <tr id="LatencyWriteRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_WriteLatency%>: </th>
                <td>
                    <asp:label runat="server" id="LatencyWriteLabel" />
                </td>
            </tr>
            <tr id="LatencyOtherRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_OtherLatency%>: </th>
                <td>
                    <asp:label runat="server" id="LatencyOtherLabel" />
                </td>
            </tr>
            <tr id="IOSizeTotalRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_TotalIOSize%>: </th>
                <td>
                    <asp:label runat="server" id="IOSizeTotalLabel" />
                </td>
            </tr>
             <tr id="IOSizeReadRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_ReadIOSize%>: </th>
                <td>
                    <asp:label runat="server" id="IOSizeReadLabel" />
                </td>
            </tr>
             <tr id="IOSizeWriteRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_WriteIOSize%>: </th>
                <td>
                    <asp:label runat="server" id="IOSizeWriteLabel" />
                </td>
            </tr>
        </table>
    </div>
</div>
