﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SRM.Web.NetObjects;

public partial class Orion_SRM_StoragePoolDetailsView : OrionView, IPoolProvider
{
    protected override void OnInit(EventArgs e)
    {
        this.Title = UIHelper.NormalizeSpaces(this.ViewInfo.ViewTitle);
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    public override string ViewType
    {
        get { return "SRM Storage Pool Details"; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public Pool Pool
    {
        get
        {
            return new Pool(NetObject.NetObjectID);
        }
    }
}