﻿using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums.Thresholds;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_SRM_Admin_VserverThreshold : System.Web.UI.Page
{
    private const string ThresholdsKey = "SRM_Vserver_Thresholds";
    private readonly List<ThresholdsType> availableThresholdsTypes = new List<ThresholdsType>()
                                                                       {
                                                                           ThresholdsType.IOPs,
                                                                           ThresholdsType.Throughput,
                                                                           ThresholdsType.IOSize
                                                                          };

    protected override void OnLoad(EventArgs e)
    {
        ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
        base.OnLoad(e);
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            imgSubmit.OnClientClick = "demoAction('Core_Scheduler_SaveSchedule', this); return false;";
    }

    protected override void OnInit(EventArgs e)
    {
        ctrlThresholdSettings.Initialize(ThresholdsKey, availableThresholdsTypes);

        base.OnInit(e);
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            ctrlThresholdSettings.SubmitClick(sender, e);
    }
}