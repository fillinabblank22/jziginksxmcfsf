using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Enums.Thresholds;
using SolarWinds.SRM.Web.Models;
using System.Web.UI.HtmlControls;
using Resources;

public partial class Orion_SRM_Admin_ThresholdSettings : System.Web.UI.UserControl
{
    private string ThresholdsKey { get; set; }
    private List<ThresholdsType> AvailableThresholdsTypes { get; set;  }

    public void Initialize(string thresholdsKey, List<ThresholdsType> availableThresholdsTypes)
    {
        this.AvailableThresholdsTypes = availableThresholdsTypes;
        this.ThresholdsKey = thresholdsKey;

        rptThresholdsSettings.DataSource = GetThresholdsSettings();
        rptThresholdsSettings.DataBind();
    }

    private List<ThresholdsSettings> GetThresholdsSettings()
    {
        var thresholdsSettings = new List<ThresholdsSettings>();

        foreach (var thresholdsType in this.AvailableThresholdsTypes)
        {
            foreach (var readWriteSettings in thresholdsType.GetAvailableReadWriteSettings())
            {
                thresholdsSettings.Add(new ThresholdsSettings
                {
                    ThresholdsTypeTitle = InvariantString.Format(
                            Resources.SrmWebContent.Thresholds_Description_Order,
                            thresholdsType.GetLocalizedDescription(),
                            readWriteSettings.GetLocalizedDescription()),
                    OrionSettings = new OrionSettings
                    {
                        Critical = new OrionSettingWithLimits(SettingsDAL.GetSetting(GetSettingsID(thresholdsType, readWriteSettings, Severity.Critical)), thresholdsType.GetThresholdConversionConstant(), thresholdsType.MinAndMaxLimitReversed()),
                        Warning = new OrionSettingWithLimits(SettingsDAL.GetSetting(GetSettingsID(thresholdsType, readWriteSettings, Severity.Warning)), thresholdsType.GetThresholdConversionConstant(), thresholdsType.MinAndMaxLimitReversed()),
                    },
                    Units = thresholdsType.GetThresholdUnits(),
                    ConversionConstant = thresholdsType.GetThresholdConversionConstant(),
                    MinAndMaxLimitsReversed = thresholdsType.MinAndMaxLimitReversed()
                });
            }
        }

        return thresholdsSettings;
    }

    private string GetSettingsID(ThresholdsType thresholdsType, ReadWriteOther readWriteSettings, Severity severity)
    {
        return String.Join("_", (new[] 
                                {
                                    ThresholdsKey, 
                                    thresholdsType.ToString(), 
                                    readWriteSettings.GetDescription(), 
                                    severity.ToString()
                                })
                                .Where(x => !string.IsNullOrEmpty(x)));
    }

    private bool IsValid()
    {
        bool isValid = true;
        string errMsg = String.Empty;
        foreach (RepeaterItem item in rptThresholdsSettings.Items)
        {
            bool currentIsValid = true;
            var settingsCritical = (Orion_SRM_Admin_ThresholdSettingControl)item.FindControl(Severity.Critical + "Settings");
            var settingsWarning = (Orion_SRM_Admin_ThresholdSettingControl)item.FindControl(Severity.Warning + "Settings");
          
            var tbxValueCrirical = GetTbxValue(settingsCritical.Value);
            var tbxValueWarning = GetTbxValue(settingsWarning.Value);

            if (tbxValueCrirical != null && tbxValueWarning != null)
            {
                double valCritical = tbxValueCrirical.Value;
                double valWarning = tbxValueWarning.Value;
                if (!settingsCritical.MinAndMaxLimitsReversed && valCritical <= valWarning)
                {
                    errMsg = SrmWebContent.SRMSettings_GlobalSrmThresholdErrorMsgCriticalLessThanGreater;
                    currentIsValid = false;
                }
                if (settingsCritical.MinAndMaxLimitsReversed && valCritical >= valWarning)
                {
                    errMsg = SrmWebContent.SRMSettings_GlobalSrmThresholdErrorMsgCriticalGreaterThanWarning;
                    currentIsValid = false;
                }
            }
            else
                currentIsValid = false;

            var header = (HtmlTableCell)item.FindControl("validationSection");
            header.Visible = false;
            if (!currentIsValid)
            {
                header.Visible = true;
                var msg = (HtmlGenericControl)header.FindControl("failMessageContent");
                msg.InnerText = errMsg;
                isValid = false;
            }
        }
        return isValid;
    }

    public void SubmitClick(object sender, EventArgs e)
    {
        if (IsValid())
        {
            foreach (RepeaterItem item in rptThresholdsSettings.Items)
            {
                SaveSetting(item, Severity.Critical);
                SaveSetting(item, Severity.Warning);
            }
            ReferrerRedirectorBase.Return("/Orion/SRM/Admin/");
        }
    }
    private void SaveSetting(RepeaterItem item, Severity severity)
    {
        var settings = (Orion_SRM_Admin_ThresholdSettingControl)item.FindControl(severity + "Settings");
        var tbxValue = GetTbxValue(settings.Value);
        if (tbxValue != null)
        {
            double val = tbxValue.Value;
            if (!string.IsNullOrEmpty(settings.Units) && settings.ConversionConstant != 0) 
            {
                val *= settings.ConversionConstant; 
            }
            settings.Setting.SaveSetting(val);
        }
    }

    private double? GetTbxValue(string tbxValue)
    {
        double value;
        if (string.IsNullOrEmpty(tbxValue) || !double.TryParse(tbxValue, out value)) return null;

        return value;
    }
}
