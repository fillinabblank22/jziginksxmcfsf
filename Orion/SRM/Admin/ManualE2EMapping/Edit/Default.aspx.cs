﻿using System;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.UI;
using ContainerModel = SolarWinds.Orion.Core.Common.Models.Container;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web;

public partial class Orion_Admin_ManualE2E_Add_Default : ContainerWizardPageBase, IBypassAccessLimitation
{	
	protected void Page_Load(object sender, EventArgs e)
    {
        var progress = ControlHelper.FindControlRecursive(Page.Master, "ProgressIndicator");
        if (progress is IProgressIndicator)
        {

            var progressIndicator = (IProgressIndicator)progress;
            progressIndicator.Steps.Clear();
            progressIndicator.Steps.Add(Resources.SrmWebContent.E2EMapping_EditWizard_Step1Indicator);
            progressIndicator.Steps.Add(Resources.SrmWebContent.E2EMapping_EditWizard_Step2Indicator);

            progressIndicator.SelectedStep = Resources.SrmWebContent.E2EMapping_EditWizard_Step1Indicator;
            progress.DataBind();
        }

    }

    protected override bool ValidateUserInput()
    {
        return false;
    }
}
