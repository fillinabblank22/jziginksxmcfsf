﻿<%@ Page Language="C#" MasterPageFile="~/Orion/SRM/Admin/ManualE2EMapping/Edit/EditWizard.master" AutoEventWireup="true"
    CodeFile="Overview.aspx.cs" Inherits="Orion_SRM_Admin_ManualE2E_Edit_Overview" Title="<%$ Resources: SrmWebContent, E2EMapping_Step1_Title %>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="wizardContent" runat="Server">
    <orion:Include runat="server" Module="SRM" File="SRM.Common.js" />
    <orion:Include ID="Include3" runat="server" File="SRM/Admin/ManualE2EMapping/ResizableGridExtension.js" />
    <orion:Include ID="Include1" runat="server" File="SRM/Admin/ManualE2EMapping/ManualE2EMappingGrid.js" />
    <orion:Include ID="Include2" runat="server" File="SRM/Admin/ManualE2EMapping/Edit/NetObjectGrid.js" />

    <h1><%= Resources.SrmWebContent.E2EMapping_Step3_SubTitle %></h1>
    <p>
        <%= Resources.SrmWebContent.E2EMapping_Step3_Description %>
    </p>

    <h2><%= Resources.SrmWebContent.E2EMapping_Step3_SubTitle2 %></h2>
    <div id="Grid" style="margin-bottom: 15px;"></div>

    <h2><%= Resources.SrmWebContent.E2EMapping_Step3_SubTitle3 %></h2>
    <div id="gridPanel">
        <div id="NetObjectGrid"></div>
    </div>

    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton runat="server" ID="btnBack" OnClientClick="Back(); return false;" LocalizedText="Back" DisplayType="Secondary" />
        <orion:LocalizableButton runat="server" ID="btnFinish" OnClientClick="Finish(); return false;" LocalizedText="Finish" DisplayType="Primary" />
        <orion:LocalizableButton runat="server" ID="btnCancel" OnClientClick="Cancel(); return false;" LocalizedText="Cancel" DisplayType="Secondary" />
    </div>

    <script>
        var volumeIds = SW.SRM.NetObjectGrid.GetUrlParameter('volumeIds');
        var nodeIds = SW.SRM.NetObjectGrid.GetUrlParameter('nodeIds');
        var targetIds = SW.SRM.NetObjectGrid.GetUrlParameter('targetIds');
        var netObjectType = SW.SRM.NetObjectGrid.GetUrlParameter('netObjectType');

        function GetContentWidth() {
            var footerWidth = $('#footer').outerWidth();
            return Math.max($(window).width(), footerWidth);
        }
        
        $(window).load(function () {
            var footerHeight = $('#footer').outerHeight();
            var bottomWizardHeight = $('.sw-btn-bar-wizard').outerHeight();
            var bottomHeight = footerHeight + bottomWizardHeight + 15;
            var margin = 30;

            SW.SRM.ManualE2EMapping.SetVolumeIDFilter(volumeIds);
            SW.SRM.ManualE2EMapping.SetNodeIDFilter(nodeIds);
            SW.SRM.ManualE2EMapping.SetViewModeBoxHidden(true);
            SW.SRM.ManualE2EMapping.SetSearchBoxHidden(true);
            SW.SRM.ManualE2EMapping.SetToolBarHidden(true);
            SW.SRM.ManualE2EMapping.SetPagingToolbarHidden(false);
            SW.SRM.ManualE2EMapping.SetIsSelectable(false);
            SW.SRM.ManualE2EMapping.SetExpandAllRootNodes(true);
            SW.SRM.ManualE2EMapping.SetResizeFunction(function () {
                var gridOffset = $('#Grid').offset();
                var currentWidth = GetContentWidth() - gridOffset.left - margin;
                var currentHeight = 0.5 * ($(window).height() - bottomHeight - gridOffset.top - margin);
                SW.SRM.ManualE2EMapping.ResizeGrid(currentWidth, currentHeight);
            });
            SW.SRM.ManualE2EMapping.MinGridHeight(150);
            SW.SRM.ManualE2EMapping.init();

            SW.SRM.NetObjectGrid.SetNetObjectType(netObjectType);
            SW.SRM.NetObjectGrid.SetTargetIds(targetIds);
            SW.SRM.NetObjectGrid.SetIsSelectable(false);
            SW.SRM.NetObjectGrid.SetResizeFunction(function () {
                var gridOffset = $('#NetObjectGrid').offset();
                var currentWidth = GetContentWidth() - gridOffset.left - margin;
                var currentHeight = $(window).height() - bottomHeight - gridOffset.top - margin;
                SW.SRM.NetObjectGrid.ResizeGrid(currentWidth, currentHeight);
            });
            SW.SRM.NetObjectGrid.MinGridHeight(100);
            SW.SRM.NetObjectGrid.init();
        });

        function Back() {
            var location = window.location.toString().replace('Overview.aspx', 'Default.aspx');
            window.location = location;
        }

        function Finish() {
            function onCompleted() {
                //waitMsg.hide();
                //SW.SRM.ManualE2EMapping.RefreshGrid();

                if (failed == 0) {

                    var message;
                    var title = '<%= Resources.SrmWebContent.E2EMapping_Step3_UpdateDone %>';

                    if (completed == updated) {
                        message = '<%= Resources.SrmWebContent.E2EMapping_Step3_UpdateSuccessfull %>';
                    } else if (updated == 0) {
                        title =  '<%= Resources.SrmWebContent.E2EMapping_Step3_UpdateWarning %>';
                        message = '<%= Resources.SrmWebContent.E2EMapping_Step3_Update_NothingTo %>';
                    } else {
                        message = SW.Core.String.Format('<%= Resources.SrmWebContent.E2EMapping_Step3_Update_Partial_Format  %>',
                            updated, completed, (completed - updated));
                    }

                    Ext.Msg.show({
                        title: title,
                        buttons: Ext.MessageBox.OK,
                        msg: message,
                        fn: function (btn) {
                            if (btn == 'ok') {
                                window.location = '\..';
                            }
                        }
                    });
                } else {
                    Ext.Msg.show({
                        title: '<%= Resources.SrmWebContent.E2EMapping_Step3_UpdateError %>',
                        buttons: Ext.MessageBox.OK,
                        msg: '<%= Resources.SrmWebContent.E2EMapping_Step3_UpdateUnSuccessfull %>',
                        fn: function (btn) {
                            if (btn == 'ok') {
                                window.location = '\..';
                            }
                        }
                    });
                }
            }

            var volumeIdsArray = volumeIds.split(',');
            var targetIdsArray = targetIds.split(',');
            var completed = 0, failed = 0, updated = 0;
            var allCalls = (volumeIdsArray.length) * (targetIdsArray.length);
            for (var volumeIdsIndex = 0; volumeIdsIndex < volumeIdsArray.length; volumeIdsIndex++) {
                var volumeId = volumeIdsArray[volumeIdsIndex];
                for (var targetIdsIndex = 0; targetIdsIndex < targetIdsArray.length; targetIdsIndex++) {
                    var targetId = targetIdsArray[targetIdsIndex];

//                    console.log('TEST_MESSAGE Finish: ' + volumeId + ', ' + targetId + ', ' + netObjectType);

                    ORION.callWebService("/Orion/SRM/Services/ManualE2EWizardService.asmx",
                             "AddManualE2EMapping", { volumeId: volumeId, netObjectId: targetId, netObjectType: netObjectType },
                             function (args) {
                                 completed++;
                                 if (args == 1)
                                     updated++;
                                 if (completed == allCalls)
                                     onCompleted();
                             },
                             function () {
                                 failed++;
                                 completed++;
                                 if (completed == allCalls)
                                     onCompleted();
                             });
                }
            }

        }

        function Cancel() {
            window.location = '\..';
        }
    </script>
</asp:Content>

