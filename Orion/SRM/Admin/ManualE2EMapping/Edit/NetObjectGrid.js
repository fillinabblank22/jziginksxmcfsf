﻿Ext.namespace('SW');
Ext.namespace('SW.SRM');

SW.SRM.NetObjectGrid = function () {

    var selectorModel;
    var dataStore;
    var grid;
    var pagingToolbar;
    var texts = {};
    var gridColumnsModel;
    var isSelectable = true;
    var arrayNetObjectType = 'SMSA';
    var poolNetObjectType = 'SMSP';
    var lunNetObjectType = 'SML';
    var nasVolumeNetObjectType = 'SMV';
    var vserverNetObjectType = 'SMVS';
    var netObjectType = arrayNetObjectType;
    var targetIds = null;
    var filter = '1=1';
    var searchedField;
    var searchText = '%';
    var gridHeight = 300;
    var gridWidth = 300;
    var initialPage = 0;
    var pageSize = 20;
    var parentId = 'NetObjectGrid';

    function FillDataStoreWithArrays() {
        var gridStoreColumns = [
            { name: 'ID', mapping: 0 },
            { name: 'Name', mapping: 1 },
            { name: 'IPAddress', mapping: 3 },
            { name: 'Status', mapping: 2 },
            { name: 'IsCluster', mapping: 4 },
            { name: 'StatusDescription', mapping: 5 }
        ];

        // define data store for credential grid - use web service (method) for load of data
        dataStore = new ORION.WebServiceStore(
            "/Orion/SRM/Services/ManualE2EWizardService.asmx/GetAllArrays", gridStoreColumns,
            "ID");

        if (targetIds)
            filter = 'Providers.StorageArrays.StorageArrayID in (' + targetIds + ')';

        searchedField = 'Providers.StorageArrays.Name';

        gridColumnsModel[4].hidden = true;
        gridColumnsModel[5].hidden = true;
    }

    function FillDataStoreWithStoragePools() {
        var gridStoreColumns = [
            { name: 'ID', mapping: 0 },
            { name: 'Name', mapping: 1 },
            { name: 'Array', mapping: 2 },
            { name: 'Status', mapping: 3 },
            { name: 'StatusDescription', mapping: 4 }
        ];

        // define data store for credential grid - use web service (method) for load of data
        dataStore = new ORION.WebServiceStore(
            "/Orion/SRM/Services/ManualE2EWizardService.asmx/GetAllPools", gridStoreColumns,
            "ID");

        if (targetIds)
            filter = 'Pools.PoolID in (' + targetIds + ')';

        searchedField = 'Pools.Name';

        gridColumnsModel[3].hidden = true;
        gridColumnsModel[4].hidden = true;
    }

    function FillDataStoreWithLuns() {
        var gridStoreColumns = [
            { name: 'ID', mapping: 0 },
            { name: 'Name', mapping: 1 },
            { name: 'StoragePools', mapping: 2 },
            { name: 'Array', mapping: 3 },
            { name: 'Status', mapping: 4 },
            { name: 'StatusDescription', mapping: 5 }
        ];

        // define data store for credential grid - use web service (method) for load of data
        dataStore = new ORION.WebServiceStore(
            "/Orion/SRM/Services/ManualE2EWizardService.asmx/GetAllLUNs", gridStoreColumns,
            "ID");

        if (targetIds)
            filter = 'LUNID in (' + targetIds + ')';

        // Two prefixes 'Pools.LUNs.' and 'Volumes.RelyLUNs.' will be added on web service side
        searchedField = 'Name';

        gridColumnsModel[3].hidden = true;
    }

    function FillDataStoreWithNasVolumes() {
        var gridStoreColumns = [
            { name: 'ID', mapping: 0 },
            { name: 'Name', mapping: 1 },
            { name: 'StoragePools', mapping: 2 },
            { name: 'Array', mapping: 3 },
            { name: 'Status', mapping: 4 },
            { name: 'StatusDescription', mapping: 5 }
        ];

        // define data store for credential grid - use web service (method) for load of data
        dataStore = new ORION.WebServiceStore(
            "/Orion/SRM/Services/ManualE2EWizardService.asmx/GetAllNasVolumes", gridStoreColumns,
            "ID");

        if (targetIds)
            filter = 'Pools.Volumes.VolumeID in (' + targetIds + ')';

        searchedField = 'Pools.Volumes.Name';

        gridColumnsModel[3].hidden = true;
    }

    function FillDataStoreWithVservers() {
        var gridStoreColumns = [
            { name: 'ID', mapping: 0 },
            { name: 'Name', mapping: 1 },
            { name: 'StoragePools', mapping: 2 },
            { name: 'Array', mapping: 3 },
            { name: 'Status', mapping: 4 },
            { name: 'StatusDescription', mapping: 5 }
        ];

        // define data store for credential grid - use web service (method) for load of data
        dataStore = new ORION.WebServiceStore(
            "/Orion/SRM/Services/ManualE2EWizardService.asmx/GetAllVservers", gridStoreColumns,
            "ID");

        if (targetIds)
            filter = 'Providers.StorageArrays.RelyVServers.VserverID in (' + targetIds + ')';

        searchedField = 'Providers.StorageArrays.RelyVServers.Name';

        gridColumnsModel[3].hidden = true;
    }

    function SelectNetObjectType() {
        switch (netObjectType) {
            case arrayNetObjectType:
                FillDataStoreWithArrays();
                break;
            case poolNetObjectType:
                FillDataStoreWithStoragePools();
                break;
            case lunNetObjectType:
                FillDataStoreWithLuns();
                break;
            case nasVolumeNetObjectType:
                FillDataStoreWithNasVolumes();
                break;
            case vserverNetObjectType:
                FillDataStoreWithVservers();
                break;
        }
        dataStore.pageSize = pageSize;
    }

    function RenderName(value, meta, record) {
        var id = record.data.ID;
        var status = record.data.Status;
        var name = record.data.Name;
        var isCluster = record.data.IsCluster;

        return SW.SRM.Common.GetNetObjectLink(netObjectType, id, status, isCluster, name);
    }

    var gridControl = {
        reload: function () {
            grid.getStore().reload();
        },

        initTexts: function (textPar) {
            texts = textPar;
        },

        GetGrid: function () { return grid; },

        RefreshGrid: function () {
            var searchText = $('#searchInput').val() || '';
            var searchFilter = searchedField + " LIKE '%" + searchText + "%'";

            grid.store.proxy.conn.jsonData = { filter: filter, searchFilter: searchFilter };
            grid.store.load({ params: { start: (initialPage) * pageSize, limit: pageSize } });
        },

        GetUrlParameter: function (propertyName) {
            var location = document.location.toString();
            var index = location.indexOf(propertyName);
            var propertyValue;
            if (index >= 0) {
                var propertyValue = location.substring(index + propertyName.length + 1).split('&')[0];
            }
            return propertyValue;
        },

        GetSelectedIds: function () {
            var selectedItems = grid.getSelectionModel().selections.items;
            var ids = new Array();

            Ext.each(selectedItems, function (item) {
                ids.push(item.data.ID);
            });
            return ids;
        },

        SetNetObjectType: function (value) {
            if (value)
                netObjectType = value;
        },

        SetTargetIds: function (value) {
            targetIds = value;
        },

        SetIsSelectable: function (value) {
            isSelectable = value;
        },

        MinGridHeight: function (value) {
            if (typeof value == 'undefined') {
                return gridHeight;
            } else {
                gridHeight = value;
            }
        },

        MinGridWidth: function (value) {
            if (typeof value == 'undefined') {
                return gridWidth;
            } else {
                gridWidth = value;
            }
        },

        InitPageSize: function (value) {
            if (typeof value == 'undefined') {
                return pageSize;
            } else {
                pageSize = value;
            }
        },

        ApplyPageSize: function (newPageSize) {
            pageSize = pagingToolbar.pageSize = dataStore.pageSize = newPageSize;
        },

        init: function () {
            Ext.override(Ext.PagingToolbar, {
                store: dataStore,
                pageSize: pageSize,
            });


            // set selection model to radioButton
            selectorModel = new Ext.grid.RowSelectionModel({ singleSelect: true });

            gridColumnsModel = [
                        {
                            header: '', sortable: false, hidden: !isSelectable, width: 30, fixed: true, dataIndex: 'ID', menuDisabled: true,
                            renderer: function (value, meta, record)
                            {
                                var id = record.data.ID;
                                var ipAddress = record.data.IPAddress;

                                return "<input type='radio' name='rows' value='" + id + "-" + ipAddress + "' />";
                            }
                        },
                        { id: 'ID', header: '@{R=Core.Strings;K=WEBJS_VB0_21;E=js}', width: 200, hidden: true, sortable: true, dataIndex: 'ID' },
                        { id: 'Name', header: '@{R=SRM.Strings;K=E2EMapping_Step1_NameColumnLabel;E=js}', width: 200, hideable: false, sortable: true, dataIndex: 'Name', renderer: RenderName },
                        { id: 'IPAddress', header: '@{R=SRM.Strings;K=E2EMapping_Step1_IPAddressColumnLabel;E=js}', width: 200, hidden: false, sortable: true, dataIndex: 'IPAddress' },
                        { id: 'StoragePools', header: '@{R=SRM.Strings;K=E2EMapping_Step1_StoragePoolsColumnLabel;E=js}', width: 200, hidden: false, sortable: true, dataIndex: 'StoragePools' },
                        { id: 'Array', header: '@{R=SRM.Strings;K=E2EMapping_Step1_ArrayColumnLabel;E=js}', width: 200, hidden: false, sortable: true, dataIndex: 'Array' },
                        { id: 'Status', header: '@{R=SRM.Strings;K=E2EMapping_Step1_StatusColumnLabel;E=js}', width: 200, hidden: false, sortable: true, dataIndex: 'StatusDescription' }
            ];

            SelectNetObjectType();

            pagingToolbar = new Ext.PagingToolbar({
                store: dataStore,
                pageSize: pageSize,
                displayInfo: true
            });

            // define grid panel
            grid = new Ext.grid.GridPanel({

                store: dataStore,

                columns: gridColumnsModel,

                sm: selectorModel,

                viewConfig: {
                    forceFit: true,
                    deferEmptyText: false,
                    emptyText: '<div class="srm-provider-step-no-more-data-available">' + texts.noDataAvailable + '</div>' // @{R=SRM.Strings;K=Provider_Step_NoDataAvailable;E=xml}
                },

                //width has to be smaller here than div width on page
                //grid will be resized when it's rendered
                height: gridHeight,
                width: gridWidth,
                stripeRows: true,
                loadMask: { msg: '@{R=SRM.Strings;K=Provider_Step_LoadingData;E=xml}' },
                bbar: pagingToolbar
            });

            grid.getSelectionModel().on('rowselect', function (sm, rowIdx, record) {

                var id = record.data.ID;
                var ipAddress = record.data.IPAddress;

                $('input:radio[name="rows"][value="' + id + "-" + ipAddress + '"]').attr('checked', true);

            });

            grid.render(parentId);
            grid.store.proxy.conn.jsonData = { filter: filter, searchFilter: '1=1' };
            grid.store.baseParams = { start: (initialPage) * pageSize, limit: pageSize };
            grid.store.load({ params: { start: (initialPage) * pageSize, limit: pageSize } });

            SW.SRM.ResizableGridExtension.InitGrid(this, parentId);
        }
    };

    SW.SRM.ResizableGridExtension.ExtendGrid(gridControl);

    return gridControl;
}();
