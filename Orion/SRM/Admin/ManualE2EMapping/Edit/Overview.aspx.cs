﻿using System;
using System.Linq;

using SolarWinds.InformationService.Contract2;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Indications;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web;

public partial class Orion_SRM_Admin_ManualE2E_Edit_Overview : ContainerWizardPageBase, IBypassAccessLimitation
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var progress = ControlHelper.FindControlRecursive(Page.Master, "ProgressIndicator");
        if (progress is IProgressIndicator)
        {
            var progressIndicator = (IProgressIndicator)progress;
            progressIndicator.Steps.Clear();
            progressIndicator.Steps.Add(Resources.SrmWebContent.E2EMapping_EditWizard_Step1Indicator);
            progressIndicator.Steps.Add(Resources.SrmWebContent.E2EMapping_EditWizard_Step2Indicator);

            progressIndicator.SelectedStep = Resources.SrmWebContent.E2EMapping_EditWizard_Step2Indicator;
            progress.DataBind();
        }

    }
}
