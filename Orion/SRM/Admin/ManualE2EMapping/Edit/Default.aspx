﻿<%@ Page Language="C#" MasterPageFile="~/Orion/SRM/Admin/ManualE2EMapping/Edit/EditWizard.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="Orion_Admin_ManualE2E_Add_Default"
    Title="<%$ Resources: SrmWebContent, E2EMapping_Step1_Title %>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="wizardContent" runat="Server">
    <orion:Include runat="server" Module="SRM" File="SRM.Common.js" />
    <orion:Include ID="Include3" runat="server" File="SRM/Admin/ManualE2EMapping/ResizableGridExtension.js" />
    <orion:Include ID="Include1" runat="server" File="SRM/Admin/ManualE2EMapping/ManualE2EMappingGrid.js" />
    <orion:Include ID="Include2" runat="server" File="SRM/Admin/ManualE2EMapping/Edit/NetObjectGrid.js" />

    <table class="E2EManagement" width="90%">
        <tr>
            <td width="50%">
                <div style="margin: 15px 0px 0px 15px;">
                    <h2><%= Resources.SrmWebContent.E2EMapping_Step2_SubTitle1 %></h2>
                    <span id="NetObjectGridDescription" style="white-space: nowrap; margin-right: 15px;"><%= Resources.SrmWebContent.E2EMapping_Step2_Description1 %></span>

                    <div style="margin-top: 10px;">
                        <%= Resources.SrmWebContent.E2EMapping_Step2_Show %>

                        <select id="showSelect" onchange="Redirect(this)">
                            <option value="SMSA"><%= Resources.SrmWebContent.E2EMapping_Step2_ShowArrays %></option>
                            <option value="SMSP"><%= Resources.SrmWebContent.E2EMapping_Step2_ShowStoragePools %></option>
                            <option value="SML"><%= Resources.SrmWebContent.E2EMapping_Step2_ShowLUNs %></option>
                            <option value="SMV"><%= Resources.SrmWebContent.E2EMapping_Step2_ShowNasVolumes %></option>
                            <option value="SMVS"><%= Resources.SrmWebContent.E2EMapping_Step2_ShowVservers %></option>
                        </select>
                        <input id="searchInput" type="text" />
                        <orion:LocalizableButton runat="server" ID="srmSearchButton" OnClientClick="SW.SRM.NetObjectGrid.RefreshGrid(); return false;" LocalizedText="CustomText" Text="<%$ Resources: SrmWebContent, E2EMapping_Wizard_Search  %>" DisplayType="Secondary" />
                    </div>

                    <div id="gridPanel" style="margin: 10px 0px 15px 0px;">
                        <div id="NetObjectGrid" />
                    </div>
                </div>
            </td>
            <td width="30%">
                <div style="margin: 15px 15px 0px 30px;">
                    <h2><%= Resources.SrmWebContent.E2EMapping_Step2_SubTitle2 %></h2>
                    <span id="GridDescription" style="white-space: nowrap; margin-right: 15px;"><%= Resources.SrmWebContent.E2EMapping_Step2_Description2 %></span>

                    <div id="Grid" style="margin: 43px 0px 15px 0px;" />
                </div>
            </td>
        </tr>
    </table>

    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton runat="server" ID="btnLastStep" OnClientClick="GoToLastStep(); return false;" LocalizedText="Next" DisplayType="Primary" />
        <orion:LocalizableButton runat="server" ID="bthCancel" OnClientClick="Cancel(); return false;" LocalizedText="Cancel" DisplayType="Secondary" />
    </div>

    <script>
        var initializing = true;
        var netObjectType = SW.SRM.NetObjectGrid.GetUrlParameter('netObjectType');

        $('#showSelect').val(netObjectType);
        initializing = false;

        function GetBottomHeight() {
            var footerHeight = $('#footer').outerHeight();
            var bottomWizardHeight = $('.sw-btn-bar-wizard').outerHeight();
            return footerHeight + bottomWizardHeight + 15;
        }

        $(window).load(function () {
            var bottomHeight = GetBottomHeight();
            var margin = 30;

            SW.SRM.NetObjectGrid.SetNetObjectType(netObjectType);
            SW.SRM.NetObjectGrid.SetResizeFunction(function () {
                var gridOffset = $('#NetObjectGrid').offset();
                var currentWidth = 0.6 * ($(window).width() - gridOffset.left - margin);
                var currentHeight = $(window).height() - bottomHeight - gridOffset.top - margin;
                SW.SRM.NetObjectGrid.ResizeGrid(currentWidth, currentHeight);
            });
            SW.SRM.NetObjectGrid.MinGridWidth($('#NetObjectGridDescription').width());
            SW.SRM.NetObjectGrid.init();

            var volumeIds = SW.SRM.NetObjectGrid.GetUrlParameter('volumeIds');
            SW.SRM.ManualE2EMapping.SetVolumeIDFilter(volumeIds);

            var nodeIds = SW.SRM.NetObjectGrid.GetUrlParameter('nodeIds');
            SW.SRM.ManualE2EMapping.SetNodeIDFilter(nodeIds);

            SW.SRM.ManualE2EMapping.SetViewModeBoxHidden(true);
            SW.SRM.ManualE2EMapping.SetSearchBoxHidden(true);
            SW.SRM.ManualE2EMapping.SetToolBarHidden(true);
            SW.SRM.ManualE2EMapping.SetPagingToolbarHidden(false);
            SW.SRM.ManualE2EMapping.SetIsSelectable(false);
            SW.SRM.ManualE2EMapping.SetExpandAllRootNodes(true);
            SW.SRM.ManualE2EMapping.SetResizeFunction(function () {
                var gridOffset = $('#Grid').offset();
                var currentWidth = $(window).width() - gridOffset.left - margin - 25;
                var currentHeight = $(window).height() - bottomHeight - gridOffset.top - margin;
                SW.SRM.ManualE2EMapping.ResizeGrid(currentWidth, currentHeight);
            });
            SW.SRM.ManualE2EMapping.MinGridWidth($('#GridDescription').width());
            SW.SRM.ManualE2EMapping.init();
        });

        function Redirect(dropDown) {
            if (initializing)
                return;

            var propertyName = 'netObjectType';
            var newLocation;
            var location = document.location.toString();
            var index = location.indexOf(propertyName);
            if (index >= 0) {
                var suffix = location.substring(index + propertyName.length + 1);
                var endIndex = suffix.indexOf('&');
                newLocation = location.substring(0, index + propertyName.length + 1) + dropDown.value;
                if (endIndex >= 0)
                    newLocation += suffix.substring(endIndex);
            }

            window.location = newLocation || (location + '&' + propertyName + '=' + dropDown.value);
        }

        function GoToLastStep() {
            var targetIds = SW.SRM.NetObjectGrid.GetSelectedIds();
            if (targetIds.length <= 0)
                Ext.Msg.show({
                    title: '<%= Resources.SrmWebContent.E2EMapping_Step2_NoNetObjectsSelectedTitle %>',
                    buttons: Ext.MessageBox.OK,
                    msg: '<%= Resources.SrmWebContent.E2EMapping_Step2_NoNetObjectsSelected %>'
            });
            else {
                var location = window.location.toString().replace('Default.aspx', 'Overview.aspx');
                location = location.replace(new RegExp('&targetIds=[^&]*'), '');
                location += '&targetIds=' + targetIds;
                window.location = location;
            }
        }

        function Cancel() {
            window.location = '\..';
        }
    </script>
</asp:Content>

