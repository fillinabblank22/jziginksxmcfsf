﻿using System;

public partial class Orion_Admin_ManualE2E_EditWizard : System.Web.UI.MasterPage
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //ProgressIndicator.Steps.Clear();
        //ProgressIndicator.Steps.Add(Resources.SrmWebContent.E2EMapping_EditWizard_Step1Indicator);
        //ProgressIndicator.Steps.Add(Resources.SrmWebContent.E2EMapping_EditWizard_Step2Indicator);
    }

    public string HelpFragment
    {
        get
        {
            return ((Orion_Admin_ManualE2EMappings_ManualE2EMappingAdminPage)this.Master).HelpFragment;
        }
        set
        {
            ((Orion_Admin_ManualE2EMappings_ManualE2EMappingAdminPage)this.Master).HelpFragment = value;
        }
    }
}
