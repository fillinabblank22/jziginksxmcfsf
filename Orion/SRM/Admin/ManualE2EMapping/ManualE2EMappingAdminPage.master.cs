﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_Admin_ManualE2EMappings_ManualE2EMappingAdminPage : System.Web.UI.MasterPage
{
    internal bool _isOrionDemoMode = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        _isOrionDemoMode = (!Profile.AllowNodeManagement && SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer);
    }

    public string HelpFragment { get; set; }
}
