﻿<%@ Page Language="C#" MasterPageFile="~/Orion/SRM/Admin/ManualE2EMapping/ManualE2EMappingAdminPage.master"
    AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_Admin_ManualE2EMapping_ManageManualE2EMappings"
    Title="<%$ Resources: SrmWebContent, E2EMapping_Step1_Title %>" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common"%>
<%@ Import Namespace="SolarWinds.SRM.Common.ServiceProvider"%>
<%@ Import Namespace="SolarWinds.SRM.Web.Interfaces"%>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include runat="server" Module="SRM" File="SRM.Common.js" />
    <orion:Include ID="Include1" runat="server" File="Admin/Containers/DynamicQueryPreview.js" />
    <orion:Include ID="Include3" runat="server" File="SRM/Admin/ManualE2EMapping/ResizableGridExtension.js" />
    <orion:Include ID="Include2" runat="server" File="SRM/Admin/ManualE2EMapping/ManualE2EMappingGrid.js" />
    <style type="text/css">
        .x-grid3-row-selected {
            background-color: #DFE8F6 !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1><%= Resources.SrmWebContent.ManualE2EMapping_Step1_Title %></h1>
    <p><%= Resources.SrmWebContent.ManualE2EMapping_Step1_Subtitle %></p>
    <table class="E2EManagement">
        <tr>
            <td width="30px">
                <div class="GroupSection">
                    <%= Resources.SrmWebContent.ManualE2EMapping_Step1_GroupBy %>

                    <select id="groupBySelect" onchange="SW.SRM.ManualE2EMapping.GenerateGroups(this)">
                        <option value="None"><%= Resources.SrmWebContent.E2EMapping_Step1_GroupNone %></option>
                        <asp:Repeater ID="groupBy" runat="server">
                            <ItemTemplate>
                                <option value="<%# Eval("Name")%>"><%# Eval("Name").ToString()%></option>
                            </ItemTemplate>
                        </asp:Repeater>
                        <optgroup label="<%= Resources.CoreWebContent.WEBDATA_IB0_198 %>">
                            <% foreach (string prop in CustomPropertyMgr.GetGroupingPropNamesForTable("NodesCustomProperties", false))
                               { %>
                            <option value="CustomProperties.[<%=prop%>]" propertytype="<%=CustomPropertyMgr.GetTypeForProp("NodesCustomProperties", prop)%>"><%=prop%></option>
                            <%}%>
                        </optgroup>
                    </select>
                </div>

                <table id="groupByResults" width="100%">
                </table>
            </td>
            <td>
                <table>
                    <tr>
                        <td id="gridCell">
                            <div id="Grid">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="currentItem" runat="server" />
    <asp:HiddenField ID="currentPage" runat="server" />
    <asp:HiddenField ID="currentGroupBy" runat="server" />

    <%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
      {%>
    <div id="isDemoMode" style="display: none;"></div>
    <%}%>

    <script type="text/javascript">
        $(window).load(function () {
            SW.SRM.ManualE2EMapping.InitPageSize(20);
            SW.SRM.ManualE2EMapping.SetCurrentItemFieldClientID('<%= CurrentItemFieldClientID %>');
            SW.SRM.ManualE2EMapping.SetCurrentPageFieldClientID('<%= CurrentPageFieldClientID %>');
            SW.SRM.ManualE2EMapping.SetCurrentGroupByFieldClientID('<%= CurrentGroupByFieldClientID %>');
            SW.SRM.ManualE2EMapping.SetAddItemPostBack(function () { <%= AddItemPostBack %> });
            SW.SRM.ManualE2EMapping.SetEditItemPostBack(function () { <%= EditItemPostBack %> });
            SW.SRM.ManualE2EMapping.SetAddRemoveObjectsPostBack(function () { <%= AddRemoveObjectsPostBack %> });
            <% foreach (KeyValuePair<String, String> entity in UsedEntities)
           { %>
            SW.SRM.ManualE2EMapping.AddToAvailableEntities(['<%= Escape(entity.Key) %>', '<%=Escape(entity.Value) %>']);
            <% } %>
            SW.SRM.ManualE2EMapping.SetInitialGroupBy('<%= GroupBy %>');
            SW.SRM.ManualE2EMapping.SetInitialPage(<%= PageNumber %>);
            SW.SRM.ManualE2EMapping.SetUserName("<%=Context.User.Identity.Name.Trim().Replace("\\", "\\\\")%>");

            SW.SRM.ManualE2EMapping.SetEditFunction(function (selectedItems) {
                if (selectedItems == null || Object.keys(selectedItems).length == 0)
                    return;

                var volumeIds = '';
                var nodeIds = '';
                Ext.each(Object.keys(selectedItems), function (volumeId) {
                    volumeIds += volumeId + ',';
                    nodeIds += selectedItems[volumeId] + ',';
                });
                volumeIds = volumeIds.slice(0, -1);
                nodeIds = nodeIds.slice(0, -1);
                var location = 'Edit/Default.aspx?volumeIds=' + volumeIds;
                location += '&nodeIds=' + nodeIds;
                location += '&netObjectType=SMSA';
                window.location = location;
            });
            SW.SRM.ManualE2EMapping.SetResizeFunction(function () {
                var margin = 30;
                var gridOffset = $('#Grid').offset();
                var footer = $('#footer');
                var footerWidth = footer.outerWidth();
                var footerHeight = footer.outerHeight();

                var currentWidth = Math.max($(window).width(), footerWidth) - gridOffset.left - margin;
                var currentHeight = $(window).height() - gridOffset.top - footerHeight - margin;

                SW.SRM.ManualE2EMapping.ResizeGrid(currentWidth, currentHeight);
            });
            SW.SRM.ManualE2EMapping.init();
        });
    </script>

</asp:Content>
