﻿var resizableGridExtension = SW.Core.namespace("SW.SRM.ResizableGridExtension");
var rowHeight = 28;

resizableGridExtension.ExtendGrid = function(gridControl)
{
    gridControl.SetResizeFunction = function (resizeFunction) {
        if (resizeFunction)
            gridControl.OnResize = resizeFunction;
    };

    gridControl.ResizeGrid = function (newWidth, newHeight) {
        var grid = gridControl.GetGrid();
        if (newWidth && newHeight && grid) {
            grid.setWidth(Math.max(newWidth, gridControl.MinGridWidth()));

            var height = Math.max(newHeight, gridControl.MinGridHeight());
            grid.setHeight(height);

            var rowsAreaHeight = $('.x-grid3-scroller').height();
            var newPageSize = Math.floor(rowsAreaHeight / rowHeight);

            if (newPageSize != gridControl.InitPageSize()) {
                gridControl.ApplyPageSize(newPageSize);
                gridControl.RefreshGrid();
            }
        }
    };
}

resizableGridExtension.InitGrid = function (gridControl, parentId) {
    if (gridControl.OnResize)
        $(window).resize(gridControl.OnResize);

    var dataStore = gridControl.GetGrid().getStore();
    dataStore.on('load', function (store, records, successful, eOpts) {
        rowHeight = $('#' + parentId).find('.x-grid3-row').outerHeight();

        if (gridControl.OnResize)
            gridControl.OnResize();
    });
}