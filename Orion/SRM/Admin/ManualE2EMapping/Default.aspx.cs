using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Shared;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.Orion.Common;

public partial class Orion_Admin_ManualE2EMapping_ManageManualE2EMappings : System.Web.UI.Page, IBypassAccessLimitation
{
    private const string addItemArgument = "AddItem";
    private const string editItemArgument = "EditItem";
    private const string addRemoveObjectsArgument = "AddRemoveObjects";
    private Dictionary<String, String> usedEntities = new Dictionary<String, String>();

    public int PageSize { get { return ContainerHelper.GetManageGroupsPageSize(); } }

    // All modifications of containers are disabled in demo mode. Buttons are disabled using JavaScript
    // but this is server side check.
    private void CheckDemoMode()
    {       
        if (OrionConfiguration.IsDemoServer)
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.SrmWebContent.DemoModeMessage), Title = Resources.CoreWebContent.WEBDATA_VB0_567 });            
    }

	private void CheckNodeRole()
	{
        if (!Profile.AllowNodeManagement)
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_AK0_180) });
	}

    protected void Page_Load(object sender, EventArgs e)
    {
		CheckNodeRole();
        CheckDemoMode();
        ((Orion_Admin_ManualE2EMappings_ManualE2EMappingAdminPage)this.Master).HelpFragment = "OrionCorePHGroups_ManageGroups";

        AddItemPostBack = Page.ClientScript.GetPostBackEventReference(this, addItemArgument);
        EditItemPostBack = Page.ClientScript.GetPostBackEventReference(this, editItemArgument);
        AddRemoveObjectsPostBack = Page.ClientScript.GetPostBackEventReference(this, addRemoveObjectsArgument);
        var manualE2EMappingDAL = ServiceLocator.GetInstance<IManualE2EMappingDAL>();
        groupBy.DataSource = manualE2EMappingDAL.GetAllGroupBy();
        groupBy.DataBind();
    }

    protected String AddItemPostBack { get; set; }
    protected String EditItemPostBack { get; set; }
    protected String AddRemoveObjectsPostBack { get; set; }

    protected String CurrentItemFieldClientID
    {
        get { return currentItem.ClientID; }
    }
    protected String CurrentPageFieldClientID
    {
        get { return currentPage.ClientID; }
    }
    protected String CurrentGroupByFieldClientID
    {
        get { return currentGroupBy.ClientID; }
    }

    protected Dictionary<string, string> UsedEntities
    {
        get
        {
            return usedEntities;
        }
    }

    protected string GroupBy
    {
        get
        {
            if (Request["groupBy"] != null)
                return Request["groupBy"];
            else
                return null;
        }
    }

    protected Int32 PageNumber
    {
        get
        {
            int page = 0;
            if (Request["page"] != null)
                Int32.TryParse(Request["page"], out page);

            return page;
        }
    }

    private string GetReturnUrlParameter()
    {
        return UrlHelper.ToSafeUrlParameter(
            InvariantString.Format("{0}?page={1}&groupBy={2}", 
            Request.AppRelativeCurrentExecutionFilePath, 
            currentPage.Value, 
            currentGroupBy.Value));
    }

    protected string Escape(string input)
    {
        if (input == null)
            return string.Empty;

        return input.Replace(@"\", @"\\").Replace("'", @"\'");
    }

    // if you change this, you must change also row definition in ManualE2EMappingGrid.js
    [Serializable]
    private class DataGridRecord
    {
        public int ID = 0;
        public string Caption = null;
        public string IP_Address = null;
        public int Status = 0;
        public int IsExpandable = 0;
    }
}
