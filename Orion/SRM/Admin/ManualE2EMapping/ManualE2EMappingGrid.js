Ext.namespace('SW');
Ext.namespace('SW.SRM');

SW.SRM.ManualE2EMapping = function () {
    var selectorModel;
    var dataStore;
    var viewModeBox, searchBox;
    var initialGroupBy = 'none', initialViewMode = 'all';
    var initialPage = 0;
    var pagingToolbar;
    var grid;
    var initialized;
    var expandedItems = new Array();
    var availableEntities = new Array();
    var customPropertyNames = [];

    var currentItemFieldClientID = '';
    var currentPageFieldClientID = '';
    var currentGroupByFieldClientID = '';
    var addItemPostBack;
    var editItemPostBack;
    var addRemoveObjectsPostBack;

    var pageSize = 1;
    var userName = "";

    var loadingMask = null;
    var filter = '';
    var volumesFilter = '';
    var nodesfilter = '1=1';
    var selectedGroupElement;
    var editFunction;
    var selectedItems = {};

    var viewModeBoxHidden = false;
    var searchBoxHidden = false;
    var toolBarHidden = false;
    var pagingToolbarHidden = false;
    var isSelectable = true;
    var expandAllRootNodes = false;
    var gridHeight = 300;
    var gridWidth = 300;
    var parentId = 'Grid';

    function GetFilter()
    {
        return volumesFilter ? nodesfilter + ' AND ' + volumesFilter : nodesfilter;
    }

    RefreshObjects = function () {
        if (loadingMask == null) {
            loadingMask = new Ext.LoadMask(grid.el, {
                msg: "@{R=Core.Strings;K=WEBJS_TM0_1;E=js}"
            });
        }

        grid.store.on('beforeload', function () { loadingMask.show(); });
        grid.store.proxy.conn.jsonData = { viewMode: viewModeBox.getValue(), filter: GetFilter(), searchString: searchBox.getValue() };
        grid.store.baseParams = { start: (initialPage) * pageSize, limit: pageSize };
        grid.store.on('load', function () {
            UpdateExpandStates();
            loadingMask.hide();
        });

        grid.store.load({ params: { start: (initialPage) * pageSize, limit: pageSize } });
    };

    // marks all expander icons with the right class and fills expandedItems array with expanded records ids
    UpdateExpandStates = function () {
    };

    UpdateToolbarButtons = function () {
        //map.EditButton.setDisabled(false);
        //map.DeleteButton.setDisabled(false);
    };

    AddManualE2EMapping = function (item) {
        if (item)
            $('#' + currentItemFieldClientID).val(Ext.util.JSON.encode(item.data));
        else
            $('#' + currentItemFieldClientID).val('');

        $('#' + currentPageFieldClientID).val(getCurrentPageNumber());
        addItemPostBack();
    };

    AddRemoveObjects = function (item) {
        $('#' + currentItemFieldClientID).val(Ext.util.JSON.encode(item.data));
        $('#' + currentPageFieldClientID).val(getCurrentPageNumber());
        addRemoveObjectsPostBack();
    };

    DeleteManualE2EMapping = function (ids, onSuccess) {

    };

    DeleteSelectedItems = function (items) {
        var toDelete = [];
        var waitMsg = Ext.Msg.wait("@{R=Core.Strings;K=WEBJS_TM0_2;E=js}");

        var ids = Object.keys(selectedItems);
        if (ids.length > 0) {
            ORION.callWebService("/Orion/SRM/Services/ManualE2EWizardService.asmx",
                     "DropManualE2EMapping", { volumeIds: ids },
                     function () {
                         waitMsg.hide();
                         SW.SRM.ManualE2EMapping.RefreshGrid();
                     },
                     function () {
                         waitMsg.hide();
                     });
        }
        else {
            waitMsg.hide();
        }
        selectedItems = {};
    };

    GetLoadingRecord = function (level) {
        var blankRecord = Ext.data.Record.create(dataStore.fields);
        var loadingRecord = new blankRecord({
            ID: -1,
            Level: level + 1,
            Name: "@{R=Core.Strings;K=WEBJS_TM0_3;E=js}"
        });
        return loadingRecord;
    };

    GetCellID = function (item) {
        return item.data.Level + '-' + item.data.Entity + '-' + item.data.ID + '-' + item.id;
    };

    ToggleRow = function (expander, recordId) {
        var item = GetFromDataStoreById(recordId, 0);
        if (item) {
            var itemID = item.data.Path;

            if (expandedItems[recordId]) {
                CollapseRow(expander, recordId);
            } else {
                loadingMask.show();
                var items = [];
                items.push(itemID);
                //ExpandRowWithFullName(items, GetRowByCellID(itemID).data.Level);
                ExpandRow(expander, recordId);
            }
        }
    };

    function GetFromDataStoreById(recordId, level) {
        for (var i = 0; i < dataStore.data.length; i++) {
            var item = dataStore.data.itemAt(i);
            if (item.data.ID == recordId && item.data.Level == level)
                return item;
        }
    }

    function GetIndexFromDataStoreById(recordId, level) {
        for (var i = 0; i < dataStore.data.length; i++) {
            var item = dataStore.data.itemAt(i);
            if (item.data.ID == recordId && item.data.Level == level)
                return i;
        }
    }

    function ExpandAllRows() {
        for (var i = 0; i < dataStore.data.length; i++) {
            var item = dataStore.data.itemAt(i);
            if (item.data.Level == 0)
                this.ExpandRow($('#expander' + item.data.ID).get(0), item.data.ID);
        }
    }

    function refreshButtonsState() {        
        var map = grid.getTopToolbar().items.map;
        var isDisabled = Object.keys(selectedItems).length <= 0;
        map.DeleteButton.setDisabled(isDisabled);
        map.EditButton.setDisabled(isDisabled);
    }

    ExpandRow = function (expander, recordId, callback, items) {
        var rowRecord = GetFromDataStoreById(recordId, 0);
        // when changing this, change DataGridRecord accordingly in Default.aspx.cs

        var gridStoreColumns = [
            { name: 'ID', mapping: 0 },
            { name: 'Caption', mapping: 1 },
            { name: 'NetObjectType', mapping: 2 },
            { name: 'NetObjectID', mapping: 3 },
            { name: 'Name', mapping: 4 },
            { name: 'NetObjectStatus', mapping: 5 },
            { name: 'IsCluster', mapping: 6 },
            { name: 'Level', mapping: 7 },
            { name: 'Manual', mapping: 8 },
            { name: 'NodeID', mapping: 9 },
            { name: 'Status', mapping: 10 }
        ];

        var volumeTypeFilter = "(v.VolumeTypeID = 4 OR v.VolumeTypeID = 100)"; // volume types for "Fixed disk" or "Mount Point"
        if (filter != '')
            volumeTypeFilter += ' AND ' + filter;

        var store = new ORION.WebServiceStore('/Orion/SRM/Services/ManualE2EWizardService.asmx/GetServerVolumesForNode', gridStoreColumns, "ID");

        store.addListener("exception", function (dataProxy, type, action, options, response, arg) {
            var error = eval("(" + response.responseText + ")");
            Ext.Msg.show({
                title: "Manual E2E Mapping Error",
                msg: error.Message,
                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
            });

            loadingMask.hide();
        });
        store.proxy.conn.jsonData = { nodeId: rowRecord.data.ID, showMode: viewModeBox.getValue(), searchString: '', filter: volumeTypeFilter };
        dataStore.insert(GetIndexFromDataStoreById(recordId, 0) + 1, GetLoadingRecord(parseInt(rowRecord.data.Level)));

        store.addListener("load", function (store) {
            var insertIndex = GetIndexFromDataStoreById(recordId, 0) + 1;
            expandedItems[recordId] = true;
            expander.className = 'SRM-ManualE2E-expander-collapse';

            var toAdd = new Array();
            store.each(function (record) {
                toAdd.unshift(record);
            });

            dataStore.removeAt(insertIndex); // remove "loading" record
            if (toAdd.length > 0) {
                dataStore.insert(insertIndex, toAdd);
            }
                   
            UpdateExpandStates();
            loadingMask.hide();
        });
        store.load();
    };

    CollapseRow = function (expander, recordId) {
        var index = GetIndexFromDataStoreById(recordId, 0);
        var rowRecord = dataStore.data.itemAt(index);

        var limit = dataStore.getCount();
        var toRemove = new Array();

        // remove all records in a branch under current record, use Level to check this
        for (var i = index + 1; i < limit; i++) {
            var record = dataStore.getAt(i);
            if (record.data.Level > rowRecord.data.Level) {
                toRemove.push(record);
            } else {
                break;
            }
        }

        dataStore.remove(toRemove);
        delete expandedItems[recordId];
        expander.className = 'SRM-ManualE2E-expander-expand';
    };

    function renderName(value, meta, record) {
        var id = record.data.ID;
        var status = record.data.Status;
        var caption = record.data.Caption;
        var expandable = record.data.IsExpandable;
        var level = record.data.Level;
        var manual = record.data.Manual;
        var checkboxName = GetCheckboxNameFromId(id);
        var isChecked = (selectedItems[id] !== undefined);

        var result = '';
        if (level == 1) {
            result += '<span class="tree-grid-indent-block">';
            if (isSelectable) {
                result += String.format('<input type="checkbox" id="{0}" onclick="SW.SRM.ManualE2EMapping.CheckChanged(this)" {1}', checkboxName, isChecked ? 'checked' : '');
                if (manual == 0)
                    result += 'disabled ';
                result += '/>';
            }
        }

        if (expandable > 0) {
            result += '<span><img id="expander' + id + '" src="/Orion/images/Pixel.gif" class="SRM-ManualE2E-expander-expand" onclick="SW.SRM.ManualE2EMapping.toggleRow(this, \'' + id + '\');">';
        } else {
            result += '<span class="tree-grid-expander-dummy">';
        }

        result += '<img src="/Orion/StatusIcon.ashx?size=small\&entity=Orion.Volumes\&id=' + id + '\&status=' + status + '" /> ' + caption + '</span>';

        if (level == 1) {
            result += '</span>';
        }
        return result;
    }

    function renderStatusGroupByDropDown(status) {
        var result = '';
        switch (status) {
            case 0:
                result = "@{R=SRM.Strings;K=E2EMapping_Step1_StatusUnknown;E=js}";
                break;
            case 1:
                result = "@{R=SRM.Strings;K=E2EMapping_Step1_StatusUp;E=js}";
                break;
            case 2:
                result = "@{R=SRM.Strings;K=E2EMapping_Step1_StatusDown;E=js}";
                break;
            case 3:
                result = "@{R=SRM.Strings;K=E2EMapping_Step1_StatusWarning;E=js}";
                break;
            default:
                result = status;
                break;
        }
        return result;
    }

    function renderIPAddress(value, meta, record) {
        var row = record.data;
        var level = row.Level;

        var result = '';
        if (level == 0) {
            result = value;
        }
        else if (level == 1) {
            var name = row.Name;
            var manual = row.Manual;

            result += '<span class="tree-grid-indent-block">';
            result += SW.SRM.Common.GetNetObjectLink(row.NetObjectType, row.NetObjectID, row.NetObjectStatus, row.IsCluster, name);
            result += '<span class="SRM-ManualE2E-SmallerText" >';
            result += manual == true ? "@{R=SRM.Strings;K=E2EMapping_Step1_ManualMapping;E=js}" : manual == false ? "@{R=SRM.Strings;K=E2EMapping_Step1_AutomaticMapping;E=js}" : '';
            result += '</span></span>';
        }
        return result;
    }


    function GetCheckboxNameFromId(id) {
        return 'selectionCheckbox_' + id;
    }

    function GetIdFromCheckboxName(name) {
        var parts = name.split('_');
        if (parts.length > 0)
            return parts[parts.length - 1];
        return '';
    }

    function getCurrentPageNumber() {
        return Math.ceil((pagingToolbar.cursor + pagingToolbar.pageSize) / pagingToolbar.pageSize);
    }

    ORION.prefix = "Core_Containers_";

    var result = {
        toggleRow: ToggleRow,
        InitPageSize: function (value) {
            if (typeof value == 'undefined') {
                return pageSize;
            } else {
                pageSize = value;
            }
        },
        ApplyPageSize: function(newPageSize) {
            pageSize = pagingToolbar.pageSize = dataStore.pageSize = newPageSize;
        },
        CheckChanged: function (checkBox) {
            if (checkBox) {
                var id = GetIdFromCheckboxName(checkBox.id);
                var index = GetIndexFromDataStoreById(id, 1);
                if (index >= 0) {
                    var item = dataStore.data.itemAt(index);
                    var volumeId = item.data.ID;
                    if (checkBox.checked == true) {
                        var nodeId = item.data.NodeID;
                        selectedItems[volumeId] = nodeId;
                    }
                    else {
                        delete selectedItems[volumeId];
                    }
                }
            }

            refreshButtonsState();
        },
        GenerateGroups: function (select) {
            var groupByResults = $('#groupByResults');

            var propertyName = select.value;

            if (propertyName == 'None') {
                filter = '';
                nodesfilter = '';
                volumesFilter = '';
                groupByResults.html('');
                RefreshObjects();
                return;
            }

            ORION.callWebService("/Orion/SRM/Services/ManualE2EWizardService.asmx",
                     "GetServerVolumesFilter", { fieldName: propertyName },
                     function (result) {
                         groupByResults.children().remove();

                         var innerHtml = groupByResults.html();
                         for (var i = 0; i < result.Rows.length; i++) {
                             var row = result.Rows[i];

                             innerHtml += '<tr style="width: 100%"><td class="SRM-ManualE2E-ClickableResult"';
                             innerHtml += 'onClick="SW.SRM.ManualE2EMapping.ApplyGrouping(this, &quot;' + propertyName + '&quot;,&quot;' + row[0] + '&quot;)" >';
                             innerHtml += '<div>' + (renderStatusGroupByDropDown(row[0]) || 'Other') + ' (' + row[1] + ')</div></td></tr>';
                         }
                         groupByResults.html(innerHtml);
                     });
        },
        GetGrid: function () { return grid; },
        RefreshGrid: function (nodeFilter) {
            dataStore.proxy.conn.jsonData = { viewMode: viewModeBox.getValue(), filter: GetFilter(), searchString: searchBox.getValue() };
            dataStore.load({ params: { start: (initialPage) * pageSize, limit: pageSize } });
            expandedItems = new Array();
        },
        ApplyGrouping: function (tdElement, fieldName, fieldValue) {
            var fieldCondition = " = '" + fieldValue + "'";
            if (selectedGroupElement) {
                selectedGroupElement.className = 'SRM-ManualE2E-ClickableResult';
            }
            selectedGroupElement = tdElement;
            selectedGroupElement.className += ' SelectedGroupItem';
            if (fieldValue === 'null') {
                fieldCondition = ' is null';
            }
            nodesfilter = fieldValue.indexOf('?') < 0 ? "n." + fieldName + fieldCondition : "";

            selectedItems = {};
            refreshButtonsState();

            RefreshObjects();
        },
        SetViewModeBoxHidden: function (value) {
            viewModeBoxHidden = value;
        },
        SetSearchBoxHidden: function (value) {
            searchBoxHidden = value;
        },
        SetToolBarHidden: function (value) {
            toolBarHidden = value;
        },
        SetPagingToolbarHidden: function (value) {
            pagingToolbarHidden = value;
        },
        SetIsSelectable: function (value) {
            isSelectable = value;
        },
        SetExpandAllRootNodes: function (value) {
            expandAllRootNodes = value;
        },
        SetVolumeIDFilter: function (volumeIdsfilter) {
            filter = 'v.VolumeID in (' + volumeIdsfilter + ')';
            volumesFilter = 'n.Volumes.VolumeID in (' + volumeIdsfilter + ')';
        },
        SetNodeIDFilter: function (nodeIdsfilter) {
            nodesfilter = 'n.NodeID in (' + nodeIdsfilter + ')';
        },
        SetEditFunction: function (theEditFunction) {
            editFunction = theEditFunction;
        },
        MinGridHeight: function (value) {
            if (typeof value == 'undefined') {
                return gridHeight;
            } else {
                gridHeight = value;
            }
        },
        MinGridWidth: function (value) {
            if (typeof value == 'undefined') {
                return gridWidth;
            } else {
                gridWidth = value;
            }
        },
        SetCurrentItemFieldClientID: function (id) {
            currentItemFieldClientID = id;
        },
        SetCurrentPageFieldClientID: function (id) {
            currentPageFieldClientID = id;
        },
        SetCurrentGroupByFieldClientID: function (id) {
            currentGroupByFieldClientID = id;
        },
        SetAddItemPostBack: function (fn) {
            addItemPostBack = fn;
        },
        SetAddRemoveObjectsPostBack: function (fn) {
            addRemoveObjectsPostBack = fn;
        },
        SetEditItemPostBack: function (fn) {
            editItemPostBack = fn;
        },
        AddToAvailableEntities: function (item) {
            availableEntities.push(item);
        },
        SetInitialGroupBy: function (groupBy) {
            if (groupBy)
                initialGroupBy = groupBy;
        },
        SetInitialPage: function (page) {
            if (page)
                initialPage = page;
        },
        SetUserName: function (user) {
            if (user)
                userName = user;
        },
        init: function () {
            if (initialized)
                return;

            initialized = true;
            function resizeToFitContent(combo) {
                if (!combo.elMetrics) {
                    combo.elMetrics = Ext.util.TextMetrics.createInstance(combo.getEl());
                }

                var m = combo.elMetrics;
                var width = 0;
                var el = combo.el;
                var si = combo.getSize();
                var minWidth = si.width;
                combo.store.each(function (r) {
                    var text = r.get(combo.displayField);
                    width = Math.max(width, m.getWidth(text));
                }, combo);

                if (el) {
                    width += el.getBorderWidth('lr');
                    width += el.getPadding('lr');
                }

                if (combo.trigger) {
                    width += combo.trigger.getWidth();
                }

                si.width = Math.max(width, minWidth);
                combo.setSize(si);
                combo.store.on({
                    'datachange': function () { resizeToFitContent(combo); },
                    'add': function () { resizeToFitContent(combo); },
                    'remove': function () { resizeToFitContent(combo); },
                    'load': function () { resizeToFitContent(combo); },
                    'update': function () { resizeToFitContent(combo); },
                    buffer: 10,
                    scope: combo
                });
            }

            viewModeBox = new Ext.form.ComboBox({
                store: new Ext.data.ArrayStore({
                    fields: ["mode", "label"], data: [["all", "@{R=SRM.Strings;K=E2EMapping_Step1_ShowAll;E=js}"], ["unmapped", "@{R=SRM.Strings;K=E2EMapping_Step1_ShowUnmapped;E=js}"], ["mapped", "@{R=SRM.Strings;K=E2EMapping_Step1_ShowMapped;E=js}"]]
                }),
                displayField: "label", valueField: "mode", mode: "local",
                editable: true,
                triggerAction: "all",
                width: 150,
                hidden: viewModeBoxHidden,
                listeners: {
                    select: function (combo, record, index) {
                        SW.SRM.ManualE2EMapping.RefreshGrid();
                    }
                }
            });

            searchBox = new Ext.form.TextField({ hidden: searchBoxHidden });

            var toolbar = new Ext.Toolbar({
                id: 'myGridToolbar',
                style: { overflow: 'visible', width: '100%' },
                hidden: toolBarHidden,
                items: [
                        "@{R=SRM.Strings;K=E2EMapping_Step1_ShowLabel;E=js}", viewModeBox,
                        '-', {
                            id: 'EditButton',
                            disabled: true,
                            text: '@{R=SRM.Strings;K=E2EMapping_Step1_EditMappingsLabel;E=js}',
                            tooltip: '@{R=SRM.Strings;K=E2EMapping_Step1_EditMappingsLabel;E=js}',
                            iconCls: 'editContainer',
                            handler: function () {
                                //if (IsDemoMode()) return DemoModeMessage();
                                if (editFunction)
                                    editFunction(selectedItems);
                            }
                        }, '-', {
                            id: 'DeleteButton',
                            disabled: true,
                            text: '@{R=SRM.Strings;K=E2EMapping_Step1_DeleteMappingsLabel;E=js}',
                            tooltip: '@{R=SRM.Strings;K=E2EMapping_Step1_DeleteMappingsLabel;E=js}',
                            iconCls: 'deleteContainer',
                            handler: function () {
                                //if (IsDemoMode()) return DemoModeMessage();

                                Ext.Msg.confirm("@{R=SRM.Strings;K=E2EMapping_Step1_ConfirmWindowTitle;E=js}",
                                    "@{R=SRM.Strings;K=E2EMapping_Step1_ConfirmWindowText;E=js}",
                                    function (btn, text) {
                                        if (btn == 'yes') {
                                            DeleteSelectedItems(selectedItems);
                                            refreshButtonsState();
                                        }
                                    });
                            }
                        },
                        '->', searchBox,
                        '-', {
                            id: 'SearchButton',
                            tooltip: '@{R=SRM.Strings;K=E2EMapping_Step1_SearchLabel;E=js}',
                            iconCls: 'SRM-ManualE2E-Search',
                            handler: function () {
                                //if (IsDemoMode()) return DemoModeMessage();

                                SW.SRM.ManualE2EMapping.RefreshGrid();
                            }
                        }
                ]
            });

            var gridStoreColumns = [
                { name: 'ID', mapping: 0 },
                { name: 'Caption', mapping: 1 },
                { name: 'Status', mapping: 2 },
                { name: 'IP_Address', mapping: 3 },
                { name: 'IsExpandable', mapping: 4 },
                { name: 'Level', mapping: 5 }
            ];

            dataStore = new ORION.WebServiceStore('/Orion/SRM/Services/ManualE2EWizardService.asmx/GetAllNodes', gridStoreColumns, "ID");
            dataStore.pageSize = pageSize;

            dataStore.addListener("exception", function (dataProxy, type, action, options, response, arg) {
                var error = {};
                if (response.status != 200) {
                    error.Message = response.statusText;
                }
                else {
                    error = eval("(" + response.responseText + ")");
                }

                Ext.Msg.show({
                    title: "Manual E2E Mapping Error",
                    msg: error.Message,
                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                });
                loadingMask.hide();
            });

            dataStore.on('load', function (store, records, successful, eOpts) {
                if (expandAllRootNodes)
                    ExpandAllRows();
            });

            pagingToolbar = new Ext.PagingToolbar({
                id: 'myPagingToolbar',
                store: dataStore,
                pageSize: pageSize,
                displayInfo: true,
                displayMsg: '@{R=Core.Strings;K=WEBJS_TM0_5;E=js}',
                emptyMsg: "@{R=Core.Strings;K=WEBJS_TM0_6;E=js}",
                beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
                hidden: pagingToolbarHidden,
                listeners: {
                change: function () {
                    if (this.displayItem) {
                        var count = 0;
                        this.store.each(function (record) {
                            if (record.data.Level == 0)
                                count++;
                        });

                        var msg = count == 0 ?
                        this.emptyMsg :
                        String.format(
                            this.displayMsg,
                            this.cursor + 1, this.cursor + count, this.store.getTotalCount()
                        );

                        this.displayItem.setText(msg);
                    }
                }
                }
            });

            selectorModel = new Ext.grid.CheckboxSelectionModel({ hidden: true });
            selectorModel.on("selectionchange", UpdateToolbarButtons);

            var gridColumnsModel = [
                        selectorModel,
                        { id: 'ID', header: '@{R=Core.Strings;K=WEBJS_VB0_21;E=js}', width: 200, hidden: true, sortable: true, dataIndex: 'ID' },
                        { id: 'Name', header: '@{R=SRM.Strings;K=E2EMapping_Step1_NameColumnLabel;E=js}', width: 200, hideable: false, sortable: true, dataIndex: 'Caption', renderer: renderName },
                        { id: 'IPAddress', header: '@{R=SRM.Strings;K=E2EMapping_Step1_IPAddressColumnLabel;E=js}', width: 200, hidden: false, sortable: true, dataIndex: 'IP_Address', renderer: renderIPAddress }
            ];

            grid = new Ext.grid.GridPanel({
                id: 'myGridPanel',
                store: dataStore,
                columns: gridColumnsModel,
                sm: selectorModel,
                height: gridHeight,
                layout: 'fit',
                width: gridWidth,
                autoscroll: true,
                stripeRows: true,
                //loadMask: { msg: 'Loading groups ...' },
                style: { overflow: 'visible' },
                tbar: toolbar,
                bbar: pagingToolbar,
                viewConfig: {
                    autoFill: true,
                    forceFit: true
                }
            });

            grid.render(parentId);

            if (getCookie("ManualE2EMapping_GroupBy")) {
                initialGroupBy = getCookie("ManualE2EMapping_GroupBy");
            } else {
                setCookie("ManualE2EMapping_GroupBy", initialGroupBy, "months", 1);
            }

            if (getCookie("ManualE2EMapping_ViewMode")) {
                initialViewMode = getCookie("ManualE2EMapping_ViewMode");
            } else {
                setCookie("ManualE2EMapping_ViewMode", initialViewMode, "months", 1);
            }
            viewModeBox.setValue(initialViewMode);

            RefreshObjects();

            UpdateToolbarButtons();

            SW.SRM.ResizableGridExtension.InitGrid(this, parentId);

            // Set the width of the grid
            //grid.setWidth($('#gridCell').width());

            $("form").submit(function () { return false; });
        }
    };

    SW.SRM.ResizableGridExtension.ExtendGrid(result);

    return result;
}();