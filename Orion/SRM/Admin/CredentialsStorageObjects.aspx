﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CredentialsStorageObjects.aspx.cs" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" 
    Inherits="Orion_SRM_Admin_CredentialsStorageObjects" EnableViewState="true"%>

<%@ Register Src="~/Orion/SRM/Admin/StorageObjectsTabs.ascx" TagPrefix="srm" TagName="StorageObjectsTabs" %>
<%@ Register Src="~/Orion/SRM/Admin/Credentials/CredentialsSnmpVersionOne.ascx" TagPrefix="srm" TagName="CredentialsSnmpVersionOne" %>
<%@ Register Src="~/Orion/SRM/Admin/Credentials/CredentialsSnmpVersionThree.ascx" TagPrefix="srm" TagName="CredentialsSnmpVersionThree" %>
<%@ Register Src="~/Orion/SRM/Admin/Credentials/CredentialsSmis.ascx" TagPrefix="srm" TagName="CredentialsSmis" %>
<%@ Register Src="~/Orion/SRM/Admin/Credentials/CredentialsNetAppFilter.ascx" TagPrefix="srm" TagName="CredentialsNetAppFilter" %>
<%@ Register Src="~/Orion/SRM/Admin/Credentials/CredentialsDfm.ascx" TagPrefix="srm" TagName="CredentialsDfm" %>
<%@ Register Src="~/Orion/SRM/Admin/Credentials/CredentialsSmsiVnx.ascx" TagPrefix="srm" TagName="CredentialsSmsiVnx" %>
<%@ Register Src="~/Orion/SRM/Admin/Credentials/CredentialsSmsiMsa.ascx" TagPrefix="srm" TagName="CredentialsSmsiMsa" %>
<%@ Register Src="~/Orion/SRM/Admin/Credentials/CredentialsGenericHttp.ascx" TagPrefix="srm" TagName="CredentialsGenericHttp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" File="SRM.StorageObjects.js" Module="SRM" />
    <orion:Include runat="server" File="SRM.StorageObjects.Credentials.js" Module="SRM" />
    <orion:Include runat="server" File="SRM.AssignPollingEngine.js" Module="SRM" />
    <orion:Include runat="server" File="extjs/Ext.sw.grid.RadioSelectionModel.js" />
    <orion:Include runat="server" Module="SRM" File="SRM.Common.js" />
    <orion:Include runat="server" File="Admin/js/SearchField.js" />
    <orion:Include runat="server" File="StorageObjects.css" Module="SRM" />
    <orion:Include runat="server" File="Admin/Containers/Containers.css" />
    <orion:Include runat="server" File="SRM.css" Module="SRM" />
    <style>
        .itemDisabled {
            color: grey;
        }
    </style>

</asp:Content>


<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="server">
    <input type="hidden" id="ReturnToUrl" value="<%= Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(Request.Url.AbsoluteUri)) %>" />
    <h1><%= Resources.SrmWebContent.SRM_Settings_ManageStorageObjects_Title %></h1>

    <srm:StorageObjectsTabs ID="StorageObjectsTabs3" runat="server" />

    <div id="Grid">
    </div>

    <asp:ScriptManagerProxy runat="server">
        <Services>
            <asp:ServiceReference Path="~/Orion/SRM/Services/StorageManagementService.asmx" />
        </Services>
    </asp:ScriptManagerProxy>
    <script type="text/javascript">
        SW.SRM.StorageObjects.Credentials.SetDefaultPorts("<%= SolarWinds.SRM.Common.SRMConstants.DefaultHttpPort %>", "<%= SolarWinds.SRM.Common.SRMConstants.DefaultHttpsPort %>");
        SW.SRM.StorageObjects.Credentials.init();
    </script>
    <div id="addDialogSnmpVerOne" class="x-hidden">
        <div class="x-window-header"><%= Resources.SrmWebContent.StorageObjects_EnterNewCredentialsTitle%></div>
        <div id="addDialogSnmpVerOneBody" class="x-panel-body dialogBody">
            <asp:Panel runat="server" ID="CredentialsSnmpVersionOne" ClientIDMode="AutoID">
                <srm:CredentialsSnmpVersionOne runat="server" ID="SnmpVersionOneCredentials" />
            </asp:Panel>
            <span class="Error"></span>
        </div>
    </div>
    <div id="addDialogSnmpVerThree" class="x-hidden">
        <div class="x-window-header"><%= Resources.SrmWebContent.StorageObjects_EnterNewCredentialsTitle%></div>
        <div id="addDialogSnmpVerThreeBody" class="x-panel-body dialogBody">
            <asp:Panel runat="server" ID="Panel1" ClientIDMode="AutoID">
                <srm:CredentialsSnmpVersionThree runat="server" ID="CredentialsSnmpVersionThree" />
            </asp:Panel>
            <span class="Error"></span>
        </div>
    </div>
    <div id="addDialogSmis" class="x-hidden">
        <div class="x-window-header"><%= Resources.SrmWebContent.StorageObjects_EnterNewCredentialsTitle%></div>
        <div id="addDialogSmisBody" class="x-panel-body dialogBody">
            <asp:Panel runat="server" ID="Panel2" ClientIDMode="AutoID">
                <srm:CredentialsSmis runat="server" ID="CredentialsSmis" />
            </asp:Panel>
            <span class="Error"></span>
        </div>
    </div>
    <div id="addDialogNetAppFiler" class="x-hidden">
        <div class="x-window-header"><%= Resources.SrmWebContent.StorageObjects_EnterNewCredentialsTitle%></div>
        <div id="addDialogNetAppFilerBody" class="x-panel-body dialogBody">
            <asp:Panel runat="server" ID="Panel3" ClientIDMode="AutoID">
                <srm:CredentialsNetAppFilter runat="server" ID="CredentialsNetAppFiler" />
            </asp:Panel>
            <span class="Error"></span>
        </div>
    </div>
    <div id="addDialogDfm" class="x-hidden">
        <div class="x-window-header"><%= Resources.SrmWebContent.StorageObjects_EnterNewCredentialsTitle%></div>
        <div id="addDialogDfmBody" class="x-panel-body dialogBody">
            <asp:Panel runat="server" ID="Panel4" ClientIDMode="AutoID">
                <srm:CredentialsDfm runat="server" ID="CredentialsDfm" />
            </asp:Panel>
            <span class="Error"></span>
        </div>
    </div>
    <div id="addDialogVnx" class="x-hidden">
        <div class="x-window-header"><%= Resources.SrmWebContent.StorageObjects_EnterNewCredentialsTitle%></div>
        <div id="addDialogVnxBody" class="x-panel-body dialogBody">
            <asp:Panel runat="server" ID="Panel5" ClientIDMode="AutoID">
                <srm:CredentialsSmsiVnx runat="server" ID="CredentialsSmsiVnx" />
            </asp:Panel>
            <span class="Error"></span>
        </div>
    </div>
    <div id="addDialogMsa" class="x-hidden">
        <div class="x-window-header"><%= Resources.SrmWebContent.StorageObjects_EnterNewCredentialsTitle%></div>
        <div id="addDialogMsaBody" class="x-panel-body dialogBody">
            <asp:Panel runat="server" ID="Panel6" ClientIDMode="AutoID">
                <srm:CredentialsSmsiMsa runat="server" ID="CredentialsSmsiMsa" />
            </asp:Panel>
            <span class="Error"></span>
        </div>
    </div>
    <div id="addDialogIsilon" class="x-hidden">
        <div class="x-window-header"><%= Resources.SrmWebContent.StorageObjects_EnterNewCredentialsTitle%></div>
        <div id="addDialogIsilonBody" class="x-panel-body dialogBody">
            <asp:Panel runat="server" ID="Panel7" ClientIDMode="AutoID">
                <srm:CredentialsSmsiMsa runat="server" ID="CredentialsSmsiIsilon" HttpPortLabel="<%$ Resources: SrmWebContent, Add_Provider_ArrayAPIPort%>" />
            </asp:Panel>
            <span class="Error"></span>
        </div>
    </div>    
    <div id="addDialogPureStorage" class="x-hidden">
        <div class="x-window-header"><%= Resources.SrmWebContent.StorageObjects_EnterNewCredentialsTitle%></div>
        <div id="addDialogPureStorageBody" class="x-panel-body dialogBody">
            <asp:Panel runat="server" ID="Panel8" ClientIDMode="AutoID">
                <srm:CredentialsSmsiMsa runat="server" ID="CredentialsSmsiPureStorage" />
            </asp:Panel>
            <span class="Error"></span>
        </div>
    </div>  
    <div id="addDialogXtremIo" class="x-hidden">
        <div class="x-window-header"><%= Resources.SrmWebContent.StorageObjects_EnterNewCredentialsTitle%></div>
        <div id="addDialogXtremIoBody" class="x-panel-body dialogBody">
            <asp:Panel runat="server" ID="Panel9" ClientIDMode="AutoID">
                <srm:CredentialsSmsiMsa runat="server" ID="CredentialsSmsiXtremIo" />
            </asp:Panel>
            <span class="Error"></span>
        </div>
    </div>    
    <div id="addDialogEmcUnity" class="x-hidden">
        <div class="x-window-header"><%= Resources.SrmWebContent.StorageObjects_EnterNewCredentialsTitle%></div>
        <div id="addDialogEmcUnityBody" class="x-panel-body dialogBody">
            <asp:Panel runat="server" ID="Panel10" ClientIDMode="AutoID">
                <srm:CredentialsNetAppFilter runat="server" ID="CredentialsEmcUnity" />
            </asp:Panel>
            <span class="Error"></span>
        </div>
    </div>
    <div id="addDialogGenericHttp" class="x-hidden">
        <div class="x-window-header"><%= Resources.SrmWebContent.StorageObjects_EnterNewCredentialsTitle%></div>
        <div id="addDialogGenericHttpBody" class="x-panel-body dialogBody">
            <asp:Panel runat="server" ID="Panel11" ClientIDMode="AutoID">
                <srm:CredentialsGenericHttp runat="server" ID="CredentialsGenericHttp" />
            </asp:Panel>
            <span class="Error"></span>
        </div>
    </div>
    <div id="addDialogInfiniBox" class="x-hidden">
        <div class="x-window-header"><%= Resources.SrmWebContent.StorageObjects_EnterNewCredentialsTitle%></div>
        <div id="addDialogInfiniBoxBody" class="x-panel-body dialogBody">
            <asp:Panel runat="server" ID="Panel12" ClientIDMode="AutoID">
                <srm:CredentialsNetAppFilter runat="server" ID="CredentialsInfiniBox" />
            </asp:Panel>
            <span class="Error"></span>
        </div>
    </div>
    <div id="addDialogKaminario" class="x-hidden">
        <div class="x-window-header"><%= Resources.SrmWebContent.StorageObjects_EnterNewCredentialsTitle%></div>
        <div id="addDialogKaminarioBody" class="x-panel-body dialogBody">
            <asp:Panel runat="server" ID="Panel13" ClientIDMode="AutoID">
                <srm:CredentialsGenericHttp runat="server" ID="CredentialsKaminario" />
            </asp:Panel>
            <span class="Error"></span>
        </div>
    </div>
</asp:Content>