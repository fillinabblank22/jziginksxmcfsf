using System;
using SolarWinds.Orion.Web;
using System.Collections.Generic;

using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Enums.Thresholds;

public partial class Orion_SRM_Admin_PoolTresholdSettings : System.Web.UI.Page
{
    private const string ThresholdsKey = "SRM_Pool_Thresholds";
    private readonly List<ThresholdsType> availableThresholdsTypes = new List<ThresholdsType>()
                                                                       {
                                                                           ThresholdsType.IOPs,
                                                                           ThresholdsType.Throughput,
                                                                           ThresholdsType.IOSize,
                                                                           ThresholdsType.CacheHitRatio,
                                                                           ThresholdsType.ProvisionedCapacity
                                                                       };
 
    protected override void OnLoad(EventArgs e)
    {
        ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
        base.OnLoad(e);
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            imgSubmit.OnClientClick = "demoAction('Core_Scheduler_SaveSchedule', this); return false;";
    }

    protected override void OnInit(EventArgs e)
    {
        ctrlThresholdSettings.Initialize(ThresholdsKey, availableThresholdsTypes);

        base.OnInit(e);
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            ctrlThresholdSettings.SubmitClick(sender, e);
    }
}
