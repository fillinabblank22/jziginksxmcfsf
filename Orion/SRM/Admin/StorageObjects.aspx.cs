using System;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using WebAgentManager = SolarWinds.Orion.Web.Agent.AgentManager;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Common;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;

public partial class SRM_StorageObjects : System.Web.UI.Page
{
    private static readonly Log log = new Log();

    protected string Provider { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        var storageArraysDal = ServiceLocator.GetInstance<IStorageArrayDAL>();
        groupBy.DataSource = storageArraysDal.GetAllGroupBy();
        groupBy.DataBind();
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        CheckRights();
        Provider = Request.QueryString["Provider"];
    }
    // <summary> Check user rights. If they are not sufficient, then redirects to error page. </summary>
    private void CheckRights()
    {
        if (OrionConfiguration.IsDemoServer)
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.SrmWebContent.DemoModeMessage), Title = Resources.CoreWebContent.WEBDATA_VB0_567 });            
    }
}
