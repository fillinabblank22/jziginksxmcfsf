<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LicenseSummary.ascx.cs" Inherits="Orion_SRM_Admin_LicenseSummary" %>
<%@ Register Src="~/Orion/Admin/Details/ModuleDetailsBlock.ascx" TagPrefix="orion" TagName="ModuleDetailBlock" %>

<style type="text/css">
    .srm-red {
        color: red;
        font-weight: bold;
    }
</style>

<orion:ModuleDetailBlock ID="SrmDetails" runat ="server" />