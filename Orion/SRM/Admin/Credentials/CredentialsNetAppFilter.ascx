﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CredentialsNetAppFilter.ascx.cs" Inherits="Orion_SRM_Admin_Credentials_CredentialsNetAppFilter" %>
<%@ Import Namespace="Resources" %>
<asp:HiddenField runat="server" ID="netAppFilerCredId" ClientIDMode="Static" />
<table class="srm-EnrollmentWizard-providerStepInputsTable" id="StoredCredFileds">
    <tr>
        <td><%= SrmWebContent.Provider_Step_Credential_Name%>*:</td>
        <td>
            <asp:TextBox runat="server" ID="FilerCredentialNameTextBox" ClientIDMode="Static" CssClass="checkEmptyFiler" />
        </td>
    </tr>
    <tr>
        <td><%= SrmWebContent.Provider_Step_User_Name%>*:</td>
        <td>
            <asp:TextBox runat="server" ID="FilerUserNameTextBox" ClientIDMode="Static" CssClass="checkEmptyFiler" />
        </td>
    </tr>
    <tr id="checkboxPasswordFiler" style="display: none">
        <td class="label"><span><span id="labelChangeFilerPassword"><%= SrmWebContent.Change_Password_Checkbox%></span>:</span></td>
        <td>
            <span>
                <input type="checkbox" id="ChangePasswordFilerCheckBox" onchange="return OnCheckBoxChangeFiler(this);" /><%= SrmWebContent.Change_Password_Checkbox_Yes%></span>
        </td>
    </tr>
    <tr>
        <td><%= SrmWebContent.Provider_Step_Password%>*:</td>
        <td>
            <asp:TextBox runat="server" ID="FilerPasswordTextBox" ClientIDMode="Static" TextMode="Password" />
        </td>
    </tr>
    <tr>
        <td><%= SrmWebContent.Add_Provider_ConfirmPassword%>*:</td>
        <td>
            <asp:TextBox runat="server" ID="FilerConfirmPasswordTextBox" ClientIDMode="Static" TextMode="Password" />
        </td>
    </tr>
    <tr>
        <td><%= SrmWebContent.Provider_Step_Protocol%>:</td>
        <td>
            <asp:CheckBox runat="server" ID="FilerProtocolCheckBox" ClientIDMode="Static" />
        </td>
    </tr>
</table>
<script>
    function OnCheckBoxChangeFiler(checkbox) {
        var divName = $($(checkbox).closest('div'));
        if (checkbox.checked == true) {
            divName.find("#FilerPasswordTextBox").removeAttr("disabled");
            divName.find("#FilerConfirmPasswordTextBox").removeAttr("disabled");
            divName.find("#FilerPasswordTextBox").addClass("checkEmptyFiler");
            divName.find("#FilerConfirmPasswordTextBox").addClass("checkEmptyFiler");
        } else {
            divName.find("#FilerPasswordTextBox").attr("disabled", "disabled");
            divName.find("#FilerConfirmPasswordTextBox").attr("disabled", "disabled");
            divName.find("#FilerPasswordTextBox").removeClass("checkEmptyFiler");
            divName.find("#FilerConfirmPasswordTextBox").removeClass("checkEmptyFiler");
        }
    }
</script>