﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CredentialsSmsiVnx.ascx.cs" Inherits="Orion_SRM_Admin_Credentials_CredentialsSmsiVnx" %>
<%@ Import Namespace="Resources" %>
<asp:HiddenField runat="server" ID="smsiVnxCredId" ClientIDMode="Static" />
<div class="CredentialsLabel"><span><b><%= SrmWebContent.Provider_Step_Celerra_APICredentials%></b></span></div>

<table id="StoredCredFiledsTableVnx" class="srm-EnrollmentWizard-providerStepInputsTable sub-table">
    <tr>
        <td class="label"><span>
            <asp:Label ID="labelApiDisplayNameTextBox" runat="server" ClientIDMode="Static"></asp:Label>*:</span></td>
        <td>
            <span>
                <asp:TextBox ID="DisplayNameApiTextBox" runat="server" ClientIDMode="Static" CssClass="checkEmptyVnx" /></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span>
            <asp:Label ID="labelApiProviderUserName" runat="server" ClientIDMode="Static"></asp:Label>*:</span></td>
        <td>
            <span>
                <asp:TextBox ID="ArrayApiUsernameTextBox" runat="server" ClientIDMode="Static" CssClass="checkEmptyVnx" /></span>
        </td>
    </tr>
    <tr id="checkboxPasswordVnx" style="display: none">
        <td class="label"><span><span id="labelChangeVnxPassword"><%= SrmWebContent.Change_Password_Checkbox%></span>:</span></td>
        <td>
            <span>
                <input type="checkbox" id="ChangePasswordVnxCheckBox" onchange="return OnCheckBoxChangeVnx();" /><%= SrmWebContent.Change_Password_Checkbox_Yes%></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span>
            <asp:Label ID="labelApiProviderPassword" runat="server" ClientIDMode="Static"></asp:Label>*:</span></td>
        <td>
            <span>
                <asp:TextBox TextMode="Password" ID="ArrayApiPasswordTextBox" runat="server" ClientIDMode="Static" /></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span id="labelApiConfirmProviderPassword"><%= SrmWebContent.Add_Provider_ArrayAPIConfirmPassword %>*:</span></td>
        <td>
            <span>
                <asp:TextBox TextMode="Password" ID="ArrayApiConfirmPasswordTextBox" runat="server" ClientIDMode="Static" /></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span><span id="labelApiPort"><%= SrmWebContent.Add_Provider_ArrayAPIPort %></span>*:</span></td>
        <td>
            <span>
                <asp:TextBox ID="ArrayApiVnxPortTextBox" runat="server" ClientIDMode="Static" CssClass="checkEmptyVnx" /></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span><span id="labelApiManagementPath"><%= SrmWebContent.Add_Provider_ArrayAPIManagementSerivcesPath %></span>*:</span></td>
        <td>
            <span>
                <asp:TextBox ID="ArrayApiManagementServicesPathTextBox" runat="server" ClientIDMode="Static" CssClass="checkEmptyVnx" /></span>
        </td>
    </tr>
</table>
<script>
    function OnCheckBoxChangeVnx() {
        var checkbox = $('#ChangePasswordVnxCheckBox').is(':checked');
        if (checkbox == true) {
            $("#ArrayApiPasswordTextBox").removeAttr("disabled");
            $("#ArrayApiConfirmPasswordTextBox").removeAttr("disabled");
            $("#ArrayApiPasswordTextBox").addClass("checkEmptyVnx");
            $("#ArrayApiConfirmPasswordTextBox").addClass("checkEmptyVnx");
        } else {
            $("#ArrayApiPasswordTextBox").attr("disabled", "disabled");
            $("#ArrayApiConfirmPasswordTextBox").attr("disabled", "disabled");
            $("#ArrayApiPasswordTextBox").removeClass("checkEmptyVnx");
            $("#ArrayApiConfirmPasswordTextBox").removeClass("checkEmptyVnx");
        }
    }
</script>