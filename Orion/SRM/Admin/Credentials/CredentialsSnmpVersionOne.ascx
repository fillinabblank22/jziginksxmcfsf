﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CredentialsSnmpVersionOne.ascx.cs" Inherits="Orion_SRM_Admin_Credentials_CredentialsSnmpVersionOne" %>
<%@ Import Namespace="Resources" %>
<asp:HiddenField runat="server" ID="snmpCredId" ClientIDMode="Static" />
<table class="srm-EnrollmentWizard-providerStepInputsTable" id="StoredCredFiledsV1">
    <tr>
        <td><%= SrmWebContent.Provider_Step_Credential_Name%>*:</td>
        <td>
            <asp:TextBox runat="server" ID="CredentialNameTextBox" ClientIDMode="Static" CssClass="checkEmpty"/>
        </td>
    </tr>
    <tr>
        <td><%= SrmWebContent.Provider_Step_Array_Snmp_Community%>*:</td>
        <td>
            <asp:TextBox runat="server" ID="SnmpCommunityTextBox" ClientIDMode="Static" CssClass="checkEmpty"/>
        </td>
    </tr>
    <tr>
        <td><%= SrmWebContent.Provider_Step_Array_Snmp_Port%>*:</td>
        <td>
            <asp:TextBox runat="server" ID="SnmpPortTextBoxV2" ClientIDMode="Static" CssClass="checkEmpty" />
        </td>
    </tr>
</table>

