﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;

public partial class Orion_SRM_Admin_Credentials_CredentialsSmsiMsa : System.Web.UI.UserControl
{
    public string UserNameLabel { get; set; }
    public string PasswordLabel { get; set; }
    public string HttpPortLabel { get; set; }
    public string HttpsPortLabel { get; set; }
    public string UseSslLabel { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            labelApiDisplayNameTextBox.Text = SrmWebContent.Add_Provider_DisplayName;
            labelApiProviderUserName.Text = UserNameLabel ?? SrmWebContent.Add_Provider_ArrayAPIUsername;
            labelApiProviderPassword.Text = PasswordLabel ?? SrmWebContent.Add_Provider_ArrayAPIPassword;
            labelApiHttpPort.Text = HttpPortLabel ?? SrmWebContent.Add_Provider_ArrayAPIHttpPort;
            labelApiHttpsPort.Text = HttpsPortLabel ?? SrmWebContent.Add_Provider_ArrayAPIHttpsPort;
            labelApiIsHttps.Text = UseSslLabel ?? SrmWebContent.Add_Api_Provider_Protocol;
        }
    }
}