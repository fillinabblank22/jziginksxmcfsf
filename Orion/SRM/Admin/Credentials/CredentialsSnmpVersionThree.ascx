﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CredentialsSnmpVersionThree.ascx.cs" Inherits="Orion_SRM_Admin_Credentials_CredentialsSnmpVersionThree" %>
<%@ Import Namespace="Resources" %>
<asp:HiddenField runat="server" ID="snmpV3credId" ClientIDMode="Static" />
<table class="srm-EnrollmentWizard-providerStepInputsTable" id="StoredCredFiledsV3">
    <tr>
        <td ><%= SrmWebContent.Provider_Step_Credential_Name%>*:</td>
        <td>
            <asp:TextBox runat="server" id="SnmpV3CredentialNameTextBox" ClientIDMode="Static" CssClass="checkEmptySnmpV3"/>
        </td>
    </tr>
    <tr>
        <td><%= SrmWebContent.Provider_Step_Array_Snmp_User_Name%>*:</td>
        <td>
            <asp:TextBox runat="server" id="SnmpUserNameTextBox" ClientIDMode="Static" CssClass="checkEmptySnmpV3"/>
        </td>
    </tr>
    <tr>
        <td><%= SrmWebContent.Provider_Step_Array_Snmp_Context%>:</td>
        <td>
            <asp:TextBox runat="server" id="SnmpContextTextBox" TextMode="Password" ClientIDMode="Static"/>
        </td>
    </tr>
    <tr>
        <td><%= SrmWebContent.Provider_Step_Array_Snmp_Port%>*:</td>
        <td>
            <asp:TextBox runat="server" id="SnmpPortTextBoxV3" ClientIDMode="Static" CssClass="checkEmptySnmpV3"/>
        </td>
    </tr>
    <tr>
        <td><%= SrmWebContent.Provider_Step_Array_Authentication_Method%>:</td>
        <td>
            <asp:DropDownList runat="server" id="AuthenticationMethodDropDown" onChange = "return OnDropDownListChanged(this);" ClientIDMode="Static" />
        </td>
    </tr>
    <tr>
        <td />
        <td>
            <table id="AuthenticationMethodOptions">
                <tr>
                    <td class="srm-provider-step-password-column"><%= SrmWebContent.Provider_Step_PasswordKey%>*:</td>
                    <td>
                        <asp:TextBox runat="server" id="PasswordKey1TextBox" TextMode="Password" ClientIDMode="Static" class="srm-provider-step-password-input"/>
                    </td>
                </tr>
                <tr>
                    <td />
                    <td>
                        <asp:CheckBox runat="server" id="PasswordKey1CheckBox" ClientIDMode="Static" Text ="<%$ Resources:SrmWebContent ,Provider_Step_Pasword_Is_Key%>" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td><%= SrmWebContent.Provider_Step_Privacy_Encryption_Method%>:</td>
        <td>
            <asp:DropDownList runat="server" id="EncryptionMethodDropDown" onChange = "return OnDropDownListChanged();" ClientIDMode="Static" />
        </td>
    </tr>
    <tr>
        <td />
        <td>
            <table id="EncryptionMethodOptions">
                <tr>
                    <td class="srm-provider-step-password-column"><%= SrmWebContent.Provider_Step_PasswordKey%>*:</td>
                    <td>
                        <asp:TextBox runat="server" ID="PasswordKey2TextBox" TextMode="Password" ClientIDMode="Static" class="srm-provider-step-password-input"/>
                    </td>
                </tr>
                <tr>
                    <td />
                    <td>
                        <asp:CheckBox runat="server" ID="PasswordKey2CheckBox" ClientIDMode="Static" Text="<%$ Resources:SrmWebContent, Provider_Step_Pasword_Is_Key%>" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<script>
    $(document).ready(function () {
        $('#AuthenticationMethodOptions').hide();
        $('#EncryptionMethodOptions').hide();
    });

    function OnDropDownListChanged() {
        debugger
        var authMode = $('#AuthenticationMethodDropDown').val();
        if (authMode == 0) {
            $('#AuthenticationMethodOptions').hide();
            $('#PasswordKey1TextBox').removeClass("checkEmptySnmpV3");
        } else {
            $('#AuthenticationMethodOptions').show();
            $('#PasswordKey1TextBox').addClass("checkEmptySnmpV3");
        }

        var encrypMethod = $("#EncryptionMethodDropDown").val();
        if (encrypMethod == 0) {
            $('#EncryptionMethodOptions').hide();
            $('#PasswordKey2TextBox').removeClass("checkEmptySnmpV3");
        } else {
            $('#EncryptionMethodOptions').show();
            $('#PasswordKey2TextBox').addClass("checkEmptySnmpV3");
        }
    }
</script>