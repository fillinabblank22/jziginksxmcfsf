﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_SRM_Admin_Credentials_CredentialsNetAppFilter : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FilerProtocolCheckBox.Text = SrmWebContent.Provider_Step_UseHttps;
    }
}