﻿using System;
using Resources;

public partial class Orion_SRM_Admin_Credentials_CredentialsGenericHttp : System.Web.UI.UserControl
{
    public string UserNameLabel { get; set; }
    public string PasswordLabel { get; set; }
    public string GenericHttpPortLabel { get; set; }
    public string UseSslLabel { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            labelApiDisplayNameTextBox.Text = SrmWebContent.Add_Provider_DisplayName;
            labelApiProviderUserName.Text = UserNameLabel ?? SrmWebContent.Add_Provider_ArrayAPIUsername;
            labelApiProviderPassword.Text = PasswordLabel ?? SrmWebContent.Add_Provider_ArrayAPIPassword;
            labelApiGenericHttpPort.Text = GenericHttpPortLabel ?? SrmWebContent.Add_Provider_ArrayAPIPort;
            labelApiIsHttps.Text = UseSslLabel ?? SrmWebContent.Add_Api_Provider_Protocol;
        }
    }
}