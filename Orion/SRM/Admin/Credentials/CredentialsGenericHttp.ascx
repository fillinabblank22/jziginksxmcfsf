﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CredentialsGenericHttp.ascx.cs" Inherits="Orion_SRM_Admin_Credentials_CredentialsGenericHttp" %>
<%@ Import Namespace="Resources" %>
<asp:HiddenField runat="server" ID="genericHttpCredId" ClientIDMode="Static" />
<div class="CredentialsLabel"><span><b><%= SrmWebContent.Provider_Step_Generic_APICredentials%></b></span></div>

<table id="StoredCredFieldsTableGenericHttp" class="srm-EnrollmentWizard-providerStepInputsTable sub-table">
    <tr>
        <td class="label"><span>
            <asp:Label ID="labelApiDisplayNameTextBox" runat="server" ClientIDMode="Static"></asp:Label>*:</span></td>
        <td>
            <span>
                <asp:TextBox ID="DisplayNameApiTextBox" runat="server" ClientIDMode="Static" CssClass="checkEmptyGenericHttp" /></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span>
            <asp:Label ID="labelApiProviderUserName" runat="server" ClientIDMode="Static"></asp:Label>*:</span></td>
        <td>
            <span>
                <asp:TextBox ID="ArrayApiUsernameTextBox" runat="server" ClientIDMode="Static" CssClass="checkEmptyGenericHttp" /></span>
        </td>
    </tr>
    <tr id="checkboxPassword" style="display: none">
        <td class="label"><span><span id="labelChangePassword"><%= SrmWebContent.Change_Password_Checkbox%></span>:</span></td>
        <td>
            <span>
                <input type="checkbox" id="ChangePasswordCheckBox" onchange="return OnCheckBoxChangeGenericHttp(this);" /><%= SrmWebContent.Change_Password_Checkbox_Yes%></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span>
            <asp:Label ID="labelApiProviderPassword" runat="server" ClientIDMode="Static"></asp:Label>*:</span></td>
        <td>
            <span>
                <asp:TextBox TextMode="Password" ID="ArrayApiPasswordTextBox" runat="server" ClientIDMode="Static" /></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span id="labelApiConfirmProviderPassword"><%= SrmWebContent.Add_Provider_ArrayAPIConfirmPassword %>*:</span></td>
        <td>
            <span>
                <asp:TextBox TextMode="Password" ID="ArrayApiConfirmPasswordTextBox" runat="server" ClientIDMode="Static" /></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span><asp:Label ID="labelApiIsHttps" runat="server" ClientIDMode="Static"></asp:Label>:</span></td>
        <td>
            <span>
                <asp:CheckBox ID="ArrayApiIsHttpCheckbox" runat="server" ClientIDMode="Static" CssClass="checkEmptyGenericHttp"/></span>
            <span id="labelApiIsHttps"><%= SrmWebContent.Provider_Step_UseHttp_Label %></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span><asp:Label ID="labelApiGenericHttpPort" runat="server" ClientIDMode="Static" />*:</span></td>
        <td>
            <span>
                <asp:TextBox ID="ArrayApiPortTextBox" runat="server" ClientIDMode="Static" CssClass="checkEmptyGenericHttp" /></span>
        </td>
    </tr>
</table>
<script>
    function OnCheckBoxChangeGenericHttp(checkbox) {
        var divName = $($(checkbox).closest('div'));
        if (checkbox.checked == true) {
            divName.find("#ArrayApiPasswordTextBox").removeAttr("disabled");
            divName.find("#ArrayApiConfirmPasswordTextBox").removeAttr("disabled");
            divName.find("#ArrayApiPasswordTextBox").addClass("checkEmptyGenericHttp");
            divName.find("#ArrayApiConfirmPasswordTextBox").addClass("checkEmptyGenericHttp");
        } else {
            divName.find("#ArrayApiPasswordTextBox").attr("disabled", "disabled");
            divName.find("#ArrayApiConfirmPasswordTextBox").attr("disabled", "disabled");
            divName.find("#ArrayApiPasswordTextBox").removeClass("checkEmptyGenericHttp");
            divName.find("#ArrayApiConfirmPasswordTextBox").removeClass("checkEmptyGenericHttp");
        }
    }
</script>