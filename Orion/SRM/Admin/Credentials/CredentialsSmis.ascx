﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CredentialsSmis.ascx.cs" Inherits="Orion_SRM_Admin_Credentials_CredentialsSmis" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common" %>
<asp:HiddenField runat="server" ID="smisCredId" ClientIDMode="Static" />
<table id="StoredCredFiledsTable">
    <tr>
        <td class="label"><span><span id="labelDisplayName"><%= SrmWebContent.Add_Provider_DisplayName %></span>*:</span></td>
        <td>
            <span>
                <input type="text" id="DisplayNameSmisTextBox" class="checkEmptySmsi" /></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span><span id="labelProviderUserName"><%= SrmWebContent.Add_Provider_ProviderUsername %></span>*:</span></td>
        <td>
            <span>
                <input type="text" id="ProviderUsernameSmisTextBox" class="checkEmptySmsi" /></span>
        </td>
    </tr>
    <tr id="checkboxPassword" style="display: none">
        <td class="label"><span><span id="labelChangePassword"><%= SrmWebContent.Change_Password_Checkbox%></span>:</span></td>
        <td>
            <span>
                <input type="checkbox" id="ChangePasswordCheckBox" onchange="return OnCheckBoxChange();" /><%= SrmWebContent.Change_Password_Checkbox_Yes%></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span><span id="labelProviderPassword"><%= SrmWebContent.Add_Provider_Password%></span>*:</span></td>
        <td>
            <span>
                <input type="password" id="ProviderPasswordSmisTextBox" /></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span><%= SrmWebContent.Add_Provider_ConfirmPassword%>*:</span></td>
        <td>
            <span>
                <input type="password" id="ProviderConfirmPasswordSmisTextBox" /></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span><%= SrmWebContent.Add_Provider_Protocol%></span></td>
        <td>
            <span>
                <input type="checkbox" id="ProtocolSmisCheckBox" /><%= SrmWebContent.Provider_Step_UseHttp_Label %></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span><span id="labelHttpsPort"><%= SrmWebContent.Add_Provider_Https_Port%></span>*:</span></td>
        <td>
            <span>
                <input type="text" id="HttpsPortSmisTextBox" class="checkEmptySmsi" /></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span><span id="labelHttpPort"><%= SrmWebContent.Add_Provider_Http_Port%></span>*:</span></td>
        <td>
            <span>
                <input type="text" id="HttpPortSmisTextBox" class="checkEmptySmsi" /></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span><span id="labelInteropNamespace"><%= SrmWebContent.Add_Provider_Interop_Namespace%></span>*:</span></td>
        <td>
            <span>
                <input type="text" id="InteropNamespaceSmisTextBox" class="checkEmptySmsi" /></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span><span id="labelArrayNamespace"><%= SrmWebContent.Add_Provider_Array_Namespace%></span>*:</span></td>
        <td>
            <span>
                <input type="text" id="ArrayNamespaceSmisTextBox" class="checkEmptySmsi" /></span>
        </td>
    </tr>
</table>
<script>
    function OnCheckBoxChange() {
        var checkbox = $('#ChangePasswordCheckBox').is(':checked');
        if (checkbox == true) {
            $("#ProviderPasswordSmisTextBox").removeAttr("disabled");
            $("#ProviderConfirmPasswordSmisTextBox").removeAttr("disabled");
            $("#ProviderPasswordSmisTextBox").addClass("checkEmptySmsi");
            $("#ProviderConfirmPasswordSmisTextBox").addClass("checkEmptySmsi");
        } else {
            $("#ProviderPasswordSmisTextBox").attr("disabled", "disabled");
            $("#ProviderConfirmPasswordSmisTextBox").attr("disabled", "disabled");
            $("#ProviderPasswordSmisTextBox").removeClass("checkEmptySmsi");
            $("#ProviderConfirmPasswordSmisTextBox").removeClass("checkEmptySmsi");
        }
    }
</script>
