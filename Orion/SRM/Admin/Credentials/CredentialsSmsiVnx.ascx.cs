﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;

public partial class Orion_SRM_Admin_Credentials_CredentialsSmsiVnx : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            labelApiDisplayNameTextBox.Text = SrmWebContent.Add_Provider_DisplayName;
            labelApiProviderUserName.Text = SrmWebContent.Add_Provider_ArrayAPIUsername;
            labelApiProviderPassword.Text = SrmWebContent.Add_Provider_ArrayAPIPassword;
        }

    }
}