﻿using System.Globalization;
using SolarWinds.Common.Utility;
using SolarWinds.Orion.Core.Models.Credentials;
using SolarWinds.SRM.Common;
using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.SRM.Common.Enums;

public partial class Orion_SRM_Admin_Credentials_CredentialsSnmpVersionThree : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SnmpPortTextBoxV3.Text = SRMConstants.SnmpPort.ToString(CultureInfo.InvariantCulture);

            ClearDropDown(AuthenticationMethodDropDown);

            AuthenticationMethodDropDown.DataSource = Enum.GetValues(typeof(SNMPv3AuthType)).Cast<SNMPv3AuthType>()
                                                                     .Where(o => o != SNMPv3AuthType.MD5 || !FIPSHelper.FIPSRestrictionEnabled)
                                                                     .Select(o => new { Text = o.GetDescription(), Value = (byte)o });
            AuthenticationMethodDropDown.DataTextField = "Text";
            AuthenticationMethodDropDown.DataValueField = "Value";
            AuthenticationMethodDropDown.DataBind();

            ClearDropDown(EncryptionMethodDropDown);

            EncryptionMethodDropDown.DataSource = Enum.GetValues(typeof(SNMPv3PrivacyType)).Cast<SNMPv3PrivacyType>()
                                                                 .Where(o => o != SNMPv3PrivacyType.DES56 || !FIPSHelper.FIPSRestrictionEnabled)
                                                                 .Select(o => new { Text = o.GetDescription(), Value = (byte)o });
            EncryptionMethodDropDown.DataTextField = "Text";
            EncryptionMethodDropDown.DataValueField = "Value";
            EncryptionMethodDropDown.DataBind();
        }
    }

    private void ClearDropDown(DropDownList ddl)
    {
        ddl.Items.Clear();
        ddl.SelectedIndex = -1;
        ddl.SelectedValue = null;
        ddl.ClearSelection();
    }
}