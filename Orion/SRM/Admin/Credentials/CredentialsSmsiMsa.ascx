﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CredentialsSmsiMsa.ascx.cs" Inherits="Orion_SRM_Admin_Credentials_CredentialsSmsiMsa" %>
<%@ Import Namespace="Resources" %>
<asp:HiddenField runat="server" ID="smsiMsaCredId" ClientIDMode="Static" />
<div class="CredentialsLabel"><span><b><%= SrmWebContent.Provider_Step_Celerra_APICredentials%></b></span></div>

<table id="StoredCredFiledsTableMsa" class="srm-EnrollmentWizard-providerStepInputsTable sub-table">
    <tr>
        <td class="label"><span>
            <asp:Label ID="labelApiDisplayNameTextBox" runat="server" ClientIDMode="Static"></asp:Label>*:</span></td>
        <td>
            <span>
                <asp:TextBox ID="MsaDisplayNameApiTextBox" runat="server" ClientIDMode="Static" CssClass="checkEmptyMsa" /></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span>
            <asp:Label ID="labelApiProviderUserName" runat="server" ClientIDMode="Static"></asp:Label>*:</span></td>
        <td>
            <span>
                <asp:TextBox ID="MsaArrayApiUsernameTextBox" runat="server" ClientIDMode="Static" CssClass="checkEmptyMsa" /></span>
        </td>
    </tr>
    <tr id="checkboxPasswordMsa" style="display: none">
        <td class="label"><span><span id="labelChangeMsaPassword"><%= SrmWebContent.Change_Password_Checkbox%></span>:</span></td>
        <td>
            <span>
                <input type="checkbox" id="ChangePasswordMsaCheckBox" onchange="return OnCheckBoxChangeMsa(this);" /><%= SrmWebContent.Change_Password_Checkbox_Yes%></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span>
            <asp:Label ID="labelApiProviderPassword" runat="server" ClientIDMode="Static"></asp:Label>*:</span></td>
        <td>
            <span>
                <asp:TextBox TextMode="Password" ID="MsaArrayApiPasswordTextBox" runat="server" ClientIDMode="Static" /></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span id="labelApiConfirmProviderPassword"><%= SrmWebContent.Add_Provider_ArrayAPIConfirmPassword %>*:</span></td>
        <td>
            <span>
                <asp:TextBox TextMode="Password" ID="MsaArrayApiConfirmPasswordTextBox" runat="server" ClientIDMode="Static" /></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span><asp:Label ID="labelApiIsHttps" runat="server" ClientIDMode="Static"></asp:Label>:</span></td>
        <td>
            <span>
                <asp:CheckBox ID="ArrayApiIsHttpCheckbox" runat="server" ClientIDMode="Static" CssClass="checkEmptyMsa"/></span>
            <span id="labelApiIsHttps"><%= SrmWebContent.Provider_Step_UseHttp_Label %></span>
        </td>
    </tr>
    <tr ID="apiHttpsPortRow">
        <td class="label"><span><asp:Label ID="labelApiHttpsPort" runat="server" ClientIDMode="Static" />*:</span></td>
        <td>
            <span>
                <asp:TextBox ID="ArrayApiMsaHttpsPortTextBox" runat="server" ClientIDMode="Static" CssClass="checkEmptyMsa" /></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span><asp:Label ID="labelApiHttpPort" runat="server" ClientIDMode="Static" />*:</span></td>
        <td>
            <span>
                <asp:TextBox ID="ArrayApiMsaPortTextBox" runat="server" ClientIDMode="Static" CssClass="checkEmptyMsa" /></span>
        </td>
    </tr>
</table>
<script>
    function OnCheckBoxChangeMsa(checkbox) {
        var divName = $($(checkbox).closest('div'));
        if (checkbox.checked == true) {
            divName.find("#MsaArrayApiPasswordTextBox").removeAttr("disabled");
            divName.find("#MsaArrayApiConfirmPasswordTextBox").removeAttr("disabled");
            divName.find("#MsaArrayApiPasswordTextBox").addClass("checkEmptyMsa");
            divName.find("#MsaArrayApiConfirmPasswordTextBox").addClass("checkEmptyMsa");
        } else {
            divName.find("#MsaArrayApiPasswordTextBox").attr("disabled", "disabled");
            divName.find("#MsaArrayApiConfirmPasswordTextBox").attr("disabled", "disabled");
            divName.find("#MsaArrayApiPasswordTextBox").removeClass("checkEmptyMsa");
            divName.find("#MsaArrayApiConfirmPasswordTextBox").removeClass("checkEmptyMsa");
        }
    }
</script>