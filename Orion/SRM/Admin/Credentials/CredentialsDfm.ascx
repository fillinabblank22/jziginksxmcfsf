﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CredentialsDfm.ascx.cs" Inherits="Orion_SRM_Admin_Credentials_CredentialsDfm" %>
<%@ Import Namespace="Resources" %>
<asp:HiddenField runat="server" ID="dfmCredId" ClientIDMode="Static" />
<table id="StoredCredFiledsTable">
    <tr>
        <td class="label"><span><span id="labelDisplayName"><%= SrmWebContent.Add_Provider_DisplayName %></span>*:</span></td>
        <td>
            <span>
                <input type="text" id="DisplayNameDfmTextBox" class="checkEmptyDfm" /></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span><span id="labelProviderUserName"><%= SrmWebContent.Add_Provider_ProviderUsername %></span>*:</span></td>
        <td>
            <span>
                <input type="text" id="ProviderUsernameDfmTextBox" class="checkEmptyDfm" /></span>
        </td>
    </tr>
    <tr id="checkboxPasswordDfm" style="display: none">
        <td class="label"><span><span id="labelChangeDfmPassword"><%= SrmWebContent.Change_Password_Checkbox%></span>:</span></td>
        <td>
            <span>
                <input type="checkbox" id="ChangePasswordDfmCheckBox" onchange="return OnCheckBoxChangeDfm();" /><%= SrmWebContent.Change_Password_Checkbox_Yes%></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span><span id="labelProviderPassword"><%= SrmWebContent.Add_Provider_Password%></span>*:</span></td>
        <td>
            <span>
                <input type="password" id="ProviderPasswordDfmTextBox" /></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span><%= SrmWebContent.Add_Provider_ConfirmPassword%>*:</span></td>
        <td>
            <span>
                <input type="password" id="ProviderConfirmPasswordDfmTextBox" /></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span><%= SrmWebContent.Add_Provider_Protocol%></span></td>
        <td>
            <span>
                <input type="checkbox" id="ProtocolDfmCheckBox" /><%= SrmWebContent.Provider_Step_UseHttp_Label %></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span><span id="labelHttpsPort"><%= SrmWebContent.Add_Provider_Https_Port%></span>*:</span></td>
        <td>
            <span>
                <input type="text" id="HttpsPortDfmTextBox" class="checkEmptyDfm" /></span>
        </td>
    </tr>
    <tr>
        <td class="label"><span><span id="labelHttpPort"><%= SrmWebContent.Add_Provider_Http_Port%></span>*:</span></td>
        <td>
            <span>
                <input type="text" id="HttpPortDfmTextBox" class="checkEmptyDfm" /></span>
        </td>
    </tr>
</table>
<script>
    function OnCheckBoxChangeDfm() {
        var checkbox = $('#ChangePasswordDfmCheckBox').is(':checked');
        if (checkbox == true) {
            $("#ProviderPasswordDfmTextBox").removeAttr("disabled");
            $("#ProviderConfirmPasswordDfmTextBox").removeAttr("disabled");
            $("#ProviderPasswordDfmTextBox").addClass("checkEmptyDfm");
            $("#ProviderConfirmPasswordDfmTextBox").addClass("checkEmptyDfm");
        } else {
            $("#ProviderPasswordDfmTextBox").attr("disabled", "disabled");
            $("#ProviderConfirmPasswordDfmTextBox").attr("disabled", "disabled");
            $("#ProviderPasswordDfmTextBox").removeClass("checkEmptyDfm");
            $("#ProviderConfirmPasswordDfmTextBox").removeClass("checkEmptyDfm");
        }
    }
</script>
