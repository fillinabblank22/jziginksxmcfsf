﻿Ext.namespace('SW');
Ext.namespace('SW.SRM');

SW.SRM.ProviderGrid = function () {
    var selectorModel;
    var dataStore;
    var grid;
    var gridColumnsModel;
    var filter = '1=1';
    var gridHeight = 300;
    var gridWidth = 1290;
    var initialPage = 0;
    var currentPage = initialPage;
    var pageSize = 20;
    var searchBox;

    function FillDataStoreWithProviders() {
        var gridStoreColumns = [
            { name: 'ID', mapping: 0 },
            { name: 'Name', mapping: 1 },
            { name: 'IPAddress', mapping: 2 },
            { name: 'Status', mapping: 3 },
            { name: 'ArrayCount', mapping: 4 },
            { name: 'StatusDescription', mapping: 5 },
            { name: 'DetailsUrl', mapping: 6 }
        ];

        // define data store for credential grid - use web service (method) for load of data
        dataStore = new ORION.WebServiceStore(
            "/Orion/SRM/Services/StorageManagementService.asmx/GetAllProviders", gridStoreColumns, "ID");
    }

    function RefreshButtonsState() {
        var map = grid.getTopToolbar().items.map;
        var isDisabled =  !(SW.SRM.ProviderGrid.GetSelectedItemCount() > 0);
        map.DeleteButton.setDisabled(isDisabled);
        map.EditButton.setDisabled(isDisabled);
    }

    function DeleteSelectedItems(items) {
        var waitMsg = Ext.Msg.wait("@{R=SRM.Strings;K=SRM_ProviderStorageObjects_DeletingMessage;E=js}");

        var selectedItems = grid.getSelectionModel().selections.items;
        for (var key in selectedItems)
        {
            if (selectedItems[key].data && selectedItems[key].data.ArrayCount > 0)
            {
                waitMsg.hide();
                Ext.Msg.show({
                    title: "@{R=SRM.Strings;K=SRM_ProviderStorageObjects_DeletingErrorTitle;E=js}",
                    buttons: Ext.MessageBox.OK,
                    msg: "@{R=SRM.Strings;K=SRM_ProviderStorageObjects_CantDeletingMessage;E=js}"
                });
                return;
            }
        }

        var ids = SW.SRM.ProviderGrid.GetSelectedIds();
        if (ids.length > 0) {
            ORION.callWebService("/Orion/SRM/Services/StorageManagementService.asmx",
                     "DeleteProviders", { providerIds: ids },
                     function () {
                         waitMsg.hide();
                         SW.SRM.ProviderGrid.RefreshGrid();
                     },
                     function () {
                         waitMsg.hide();
                     });
        }
        else {
            waitMsg.hide();
        }
    };

    function RenderName(value, meta, record) {
        var id = record.data.ID;
        var status = record.data.Status;
        var name = record.data.Name || '';

        var entity = 'Orion.SRM.Providers';
        var result = '<img src="/Orion/StatusIcon.ashx?size=small\&entity=' + entity + '\&id=' + id + '\&status=' + status + '" class="SRM-vertical-align-middle"/>';
        result += '<span class="SRM-vertical-align-middle">';
        result += '<a href="' + record.data.DetailsUrl + '">' + name + '</a>';
        result += '</span>';

        return result;
    }
    
    return {
        RefreshGrid: function () {
            var searchedText = searchBox.getValue() || '';

            grid.store.proxy.conn.jsonData = { filter: filter, searchedText: searchedText };
            grid.store.load({ params: { start: (currentPage * pageSize), limit: pageSize } });
        },

        GetSelectedItemCount: function () {
            return grid.getSelectionModel().selections.items.length;
        },

        GetSelectedIds: function () {
            var selectedItems = grid.getSelectionModel().selections.items;
            var ids = new Array();

            Ext.each(selectedItems, function (item) {
                ids.push(item.data.ID);
            });
            return ids;
        },

        GetGrid: function () {
            return grid;
        },

        SetGridHeight: function (value) {
            gridHeight = value;
        },

        SetGridWidth: function (value) {
            gridWidth = value;
        },

        SetPageSize: function (value) {
            pageSize = value;
        },
        
        init: function () {
            pageSize = parseInt(ORION.Prefs.load('PageSize', '20'));
            var pageSizeBox = new Ext.form.NumberField({
                id: 'PageSizeField',
                enableKeyEvents: true,
                allowNegative: false,
                width: 40,
                allowBlank: false,
                minValue: 1,
                maxValue: 100,
                value: pageSize,
                listeners: {
                    scope: this,
                    'keydown': function (f, e) {
                        var k = e.getKey();
                        if (k == e.RETURN) {
                            e.stopEvent();
                            var v = parseInt(f.getValue());

                            // Perform Extended Validation
                            if (isNaN(v)) {
                                Ext.Msg.show({
                                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                    msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                                });
                                return;
                            }
                            else if (v <= 0) {
                                Ext.Msg.show({
                                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                    msg: "@{R=SRM.Strings;K=SRM_StorageObjects_MinimumWarning; E=js}",
                                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                                });
                                // Fix user input by establishing Minimum value
                                v = 1;
                                f.setValue('1');
                            }
                            else if (v > 100) {
                                Ext.Msg.show({
                                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                    msg: "@{R=SRM.Strings;K=SRM_StorageObjects_MaximumWarning; E=js}",
                                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                                });
                                // Fix user input by establishing Maximum value
                                v = 100;
                                f.setValue('100');
                            }

                            if (pageSize != v) {
                                pageSize = v;
                                pagingToolbar.pageSize = pageSize;
                                ORION.Prefs.save('PageSize', pageSize);
                                pagingToolbar.doLoad(0);
                            }

                        }
                    },
                    'focus': function (field) {
                        field.el.dom.select();
                    },
                    'change': function (f, numbox, o) {
                        var pSize = parseInt(f.getValue());

                        // Perform Extended Validation
                        if (isNaN(pSize)) {
                            Ext.Msg.show({
                                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                            });
                            return;
                        }
                        else if (pSize < 1) {
                            Ext.Msg.show({
                                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                msg: "@{R=SRM.Strings;K=SRM_StorageObjects_MinimumWarning; E=js}",
                                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                            });
                            // Fix user input by establishing Minimum value
                            pSize = 1;
                            f.setValue('1');
                        }
                        else if (pSize > 100) {
                            Ext.Msg.show({
                                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                msg: "@{R=SRM.Strings;K=SRM_StorageObjects_MaximumWarning; E=js}",
                                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                            });
                            // Fix user input by establishing Maximum value
                            pSize = 100;
                            f.setValue('100');
                        }

                        if (pagingToolbar.pageSize != pSize) { // update page size only if it is different
                            pagingToolbar.pageSize = pSize;
                            pageSize = pSize;

                            ORION.Prefs.save('PageSize', pageSize);
                            pagingToolbar.doLoad(0);
                        }
                    }
                }
            });

            searchBox = new Ext.ux.form.SearchField({
                store: dataStore,
                width: 200
            });

            var toolbar = new Ext.Toolbar({
                id: 'myGridToolbar',
                style: { overflow: 'visible', width: '100%', height: '23px' },
                items: [
                    {
                        id: 'DeleteButton',
                        disabled: true,
                        text: '@{R=SRM.Strings;K=SRM_ProviderStorageObjects_DeleteProvidersLabel;E=js}',
                        tooltip: '@{R=SRM.Strings;K=SRM_ProviderStorageObjects_DeleteProvidersLabel;E=js}',
                        iconCls: 'deleteContainer',
                        handler: function () {
                            //if (IsDemoMode()) return DemoModeMessage();

                            Ext.Msg.confirm("@{R=SRM.Strings;K=SRM_ProviderStorageObjects_ConfirmWindowTitle;E=js}",
                                "@{R=SRM.Strings;K=SRM_ProviderStorageObjects_ConfirmWindowTitle;E=js}",
                                function (btn, text) {
                                    if (btn == 'yes') {
                                        DeleteSelectedItems();
                                        RefreshButtonsState();
                                    }
                                });
                            }
                        }, '-',
                        {
                            id: 'EditButton',
                            disabled: true,
                            text: '@{R=SRM.Strings;K=SRM_ProviderStorageObjects_EditProvidersLabel;E=js}',
                            tooltip: '@{R=SRM.Strings;K=SRM_ProviderStorageObjects_EditProvidersLabel;E=js}',
                            iconCls: 'editContainer',
                            handler: function () {
                                var ids = new Array();
                                Ext.each(grid.getSelectionModel().selections.items, function (item) {
                                    ids.push("SMP:" + item.data.ID);
                                });
                                var netobj = ids.join(",");
                                location.href = '/Orion/SRM/Admin/EditProperties.aspx?NetObject=' + netobj + "&ReturnTo=" + $('#ReturnToUrl').val();
                            }
                        },
                        '->'
                ]
            });

            // set selection model to radioButton
            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", function () {
                RefreshButtonsState();
            });

            gridColumnsModel = [
                        selectorModel,
                        { id: 'ID', header: '@{R=Core.Strings;K=WEBJS_VB0_21;E=js}', width: 200, hidden: true, sortable: true, dataIndex: 'ID' },
                        { id: 'Name', header: '@{R=SRM.Strings;K=SRM_ProviderStorageObjects_NameColumn;E=js}', width: 300, hideable: false, sortable: true, dataIndex: 'Name', renderer: RenderName },
                        { id: 'IPAddress', header: '@{R=SRM.Strings;K=SRM_ProviderStorageObjects_IPAddressColumn;E=js}', width: 300, hidden: false, sortable: true, dataIndex: 'IPAddress', tdCls: 'x-cell' },
                        { id: 'StatusDescription', header: '@{R=SRM.Strings;K=SRM_ProviderStorageObjects_StatusColumn;E=js}', width: 300, hidden: false, sortable: false, dataIndex: 'StatusDescription' }
            ];

            FillDataStoreWithProviders();
            dataStore.pageSize = pageSize;

            var pagingToolbar = new Ext.PagingToolbar(
            {
                store: dataStore,
                pageSize: pageSize,
                displayInfo: true,
                displayMsg: "@{R=Core.Strings;K=WEBJS_SO0_2; E=js}",
                emptyMsg: "@{R=Core.Strings;K=WEBJS_SO0_3; E=js}",
                beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
                items: [
                '-',
                new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }),
                pageSizeBox
                ]
            });

            // define grid panel
            grid = new Ext.grid.GridPanel({

                viewConfig: {
                    forceFit: true
                },

                cls: 'SRM-StorageObjects-ProviderGrid',
                store: dataStore,

                columns: gridColumnsModel,

                sm: selectorModel,

                //width has to be smaller here than div width on page
                //grid will be resized when it's rendered
                height: gridHeight,
                width: gridWidth,
                autoExpandColumn: 'StatusDescription',
                stripeRows: true,
                loadMask: { msg: '@{R=SRM.Strings;K=Provider_Step_LoadingData;E=xml}' },
                tbar: toolbar,
                bbar: pagingToolbar
            });
            
            grid.render('Grid');
            grid.store.baseParams = { start: (initialPage * pageSize), limit: pageSize };
            SW.SRM.ProviderGrid.RefreshGrid();
        }
    };
}();
