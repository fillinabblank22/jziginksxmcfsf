using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Events;
using SolarWinds.Orion.Web.NpmAdapters;
using NodeSubType = SolarWinds.Orion.Core.Common.Models.NodeSubType;
using SNMPVersion = SolarWinds.Orion.Core.Common.Models.SNMPVersion;
using SolarWinds.Orion.Core.Common.i18n;
using SolarWinds.Orion.Core.Common.Agent;
using Node = SolarWinds.Orion.Core.Common.Models.Node;
using WebAgentManager = SolarWinds.Orion.Web.Agent.AgentManager;
using SolarWinds.Orion.Web.UI;
using Resources;

public partial class SRMSettings : System.Web.UI.Page
{
    private static readonly Log log = new Log();

    protected string ProductNameAndVersion { get; set;  }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        var prodName = SolarWinds.Orion.Core.Common.RegistrySettings.GetModuleSetting("SRM", "ProductDisplayName");
        var prodVer = SolarWinds.Orion.Core.Common.RegistrySettings.GetModuleSetting("SRM", "AssemblyVersion");
        ProductNameAndVersion = prodName + " " + string.Format(SrmWebContent.SRMSettings_BuildNumberFormat, prodVer);
    }
}
