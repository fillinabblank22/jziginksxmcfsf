using System;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using WebAgentManager = SolarWinds.Orion.Web.Agent.AgentManager;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Common;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;

public partial class SRM_ProviderStorageObjects : System.Web.UI.Page
{
    private static readonly Log log = new Log();

    protected string Provider { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}
