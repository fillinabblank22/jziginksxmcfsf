<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="NasThreshold.aspx.cs" 
    Inherits="Orion_SRM_Admin_NasVolumeTresholdSettings" Title="<%$ Resources: SrmWebContent, Nas_Volume_Threshold_PageTitle %>" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="HelpButton" %>
<%@ Register TagPrefix="Thresholds" TagName="ThresholdSettings" Src="~/Orion/SRM/Admin/ThresholdSettings.ascx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" File="Thresholds.css" Module="SRM" />
</asp:Content>

<asp:Content ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:HelpButton HelpUrlFragment="" ID="helpButton" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    
    <table class="PageHeader HeaderTable" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <h1><%= Page.Title %></h1>
            </td>
            <td align="right">
            </td>
        </tr>
    </table>
    <%= Resources.SrmWebContent.Nas_Volume_Threshold_PageSubTitle %>
    
    <Thresholds:ThresholdSettings ID="ctrlThresholdSettings" runat="server" />

    <br />
    <div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" ID="imgSubmit" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary" />
    </div>   
</asp:Content>
