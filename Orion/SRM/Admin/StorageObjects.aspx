<%@ Page Title="<%$ Resources:SrmWebContent,SRM_Settings_ManageStorageObjects_Title %>" Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master"
    AutoEventWireup="true" CodeFile="StorageObjects.aspx.cs" Inherits="SRM_StorageObjects" %>

<%@ Import Namespace="SolarWinds.Orion.Core.Common" %>
<%@ Register TagPrefix="orion" TagName="UnmanageDialog" Src="~/Orion/SRM/Controls/UnmanageDialog.ascx" %>
<%@ Register TagPrefix="orion" TagName="PollRediscoverDialog" Src="~/Orion/Controls/PollRediscoverDialog.ascx" %>
<%@ Register TagPrefix="orion" TagName="PollNowDialog" Src="~/Orion/SRM/Controls/PollNowDialog.ascx" %>
<%@ Register Src="~/Orion/SRM/Admin/StorageObjectsTabs.ascx" TagPrefix="srm" TagName="StorageObjectsTabs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />

    <orion:Include runat="server" File="SRM.StorageObjects.js" Module="SRM" />
    <orion:Include runat="server" File="SRM.AssignPollingEngine.js" Module="SRM" />
    <orion:Include runat="server" File="extjs/Ext.sw.grid.RadioSelectionModel.js" />
    <orion:Include runat="server" Module="SRM" File="SRM.Common.js" />
    <orion:Include runat="server" File="Admin/js/SearchField.js" />
    <orion:Include runat="server" File="StorageObjects.css" Module="SRM" />
    <orion:Include runat="server" File="Admin/Containers/Containers.css" />
    <orion:Include runat="server" File="SRM.css" Module="SRM" />
    <style>
        .itemDisabled {
            color: grey;
        }
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="server">
    <input type="hidden" id="ReturnToUrl" value="<%= Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(Request.Url.AbsoluteUri)) %>" />
    <h1><%= Resources.SrmWebContent.SRM_Settings_ManageStorageObjects_Title %></h1>

    <srm:StorageObjectsTabs ID="StorageObjectsTabs1" runat="server" />
    <table class="StorageObjects">
        <tr>
            <td width="200px">
                <div class="GroupSection">
                    <%= Resources.SrmWebContent.StorageObjects_GroupBy %>

                    <select id="groupBySelect" onchange="SW.SRM.StorageObjects.GenerateGroups(this)">
                        <option value="None"><%= Resources.SrmWebContent.StorageObjects_GroupNone %></option>
                        <asp:Repeater ID="groupBy" runat="server">
                            <ItemTemplate>
                                <option value="<%# Eval("Name")%>"><%# Eval("Name").ToString()%></option>
                            </ItemTemplate>
                        </asp:Repeater>
                        <optgroup label="<%= Resources.SrmWebContent.StorageObjects_StorageArrayCustomProperties %>">
                            <% foreach (string prop in CustomPropertyMgr.GetGroupingPropNamesForTable("SRM_StorageArrayCustomProperties", false))
                               { %>
                            <option value="CustomProperties.[<%=prop%>]" propertytype="<%=CustomPropertyMgr.GetTypeForProp("SRM_StorageArrayCustomProperties", prop)%>"><%=prop%></option>
                            <%}%>
                        </optgroup>
                    </select>
                </div>
                <table id="groupByResults" width="100%">
                </table>
            </td>
            <td>
                <table>
                    <tr>
                        <td id="gridCell">
                            <div id="Grid">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:ScriptManagerProxy runat="server">
        <Services>
            <asp:ServiceReference Path="~/Orion/SRM/Services/StorageManagementService.asmx" />
        </Services>
    </asp:ScriptManagerProxy>
    <script type="text/javascript">
        SW.SRM.StorageObjects.setProviderData("<%=Provider%>");
        SW.SRM.StorageObjects.init();
    </script>

    <orion:UnmanageDialog runat="server" />
    <orion:PollRediscoverDialog runat="server" />
    <orion:PollNowDialog runat="server" />
</asp:Content>
