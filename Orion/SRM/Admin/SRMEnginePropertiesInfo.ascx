﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SRMEnginePropertiesInfo.ascx.cs" Inherits="SRM_Admin_SRMEnginePropertiesInfo" %>

<asp:Repeater runat="server" ID="SRMEnginePropertiesRepeater">
    <ItemTemplate>
        <tr>
            <td><%# DataBinder.Eval(Container.DataItem, "ElementsPropertyName") %></td>
            <td><%# DataBinder.Eval(Container.DataItem, "ElementsPropertyValue") %></td>
        </tr>
        <tr>
            <td><%# DataBinder.Eval(Container.DataItem, "TotalDiskPropertyName") %></td>
            <td><%# DataBinder.Eval(Container.DataItem, "TotalDiskPropertyValue") %></td>
        </tr>
    </ItemTemplate>
</asp:Repeater>

<tr runat="server" visible="false" id="ErrorBlock">
    <td class="PropertyHeader"><%= Resources.CoreWebContent.WEBDATA_VB0_377 %></td>
    <td class="Property">
        <asp:Label runat="server" ID="ErrorLabel"></asp:Label>
    </td>
</tr>