using Resources;
using SolarWinds.Orion.Web.DAL;
using System;
using System.Globalization;
using System.Web.UI;

public partial class Orion_SRM_Admin_ThresholdSettingControl : System.Web.UI.UserControl
{
    private OrionSetting _setting;

    public string MinValue { get; set;}
    public string MaxValue { get; set; }
    public string Units { get; set; }
    public int ConversionConstant { get; set; }
    public bool MinAndMaxLimitsReversed { get; set; }

    /// <summary>
    /// checks the min and max only if this is true
    /// </summary>
    public bool DoRangeValidate { get; set; }

    public string ErrorRangeMessage
    {
        get
        {
            string title = string.Format(CultureInfo.InvariantCulture, CoreWebContent.WEBDATA_LF0_2, MinValue, MaxValue);
            return string.Format(CultureInfo.InvariantCulture, "<img src='/Orion/images/Small-Down.gif' title='{0}'/>", title);
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (DoRangeValidate)
        {
            ThesholdRangeValidator.ErrorMessage = this.ErrorRangeMessage;
            ThesholdRangeValidator.MinimumValue = MinAndMaxLimitsReversed ? this.MaxValue : this.MinValue;
            ThesholdRangeValidator.MaximumValue = MinAndMaxLimitsReversed ? this.MinValue : this.MaxValue;
            ThesholdRequiredfield.ErrorMessage = this.ErrorRangeMessage;
        }
    }

    public OrionSetting Setting
    {
        get { return _setting; }
        set 
        {
            _setting = value;

            double actualValue = Setting.SettingValue;
            string units = Setting.Units.Trim();
            if (!string.IsNullOrEmpty(Units)) 
            {
                units = Units;
                if (ConversionConstant != 0) 
                {
                    actualValue = Math.Round(actualValue / ConversionConstant);
                }
            }

            string settingType = Setting.Name.Substring(Setting.Name.LastIndexOf('-') + 1).Trim();
            this.phType.Controls.Add(new LiteralControl(settingType));

            string minMax = string.Format(CultureInfo.InvariantCulture, Resources.SrmWebContent.SRM_Thresholds_RangeTemplate, MinValue, units, MaxValue);
            this.phMinMax.Controls.Add(new LiteralControl(minMax));

            this.phDesc.Controls.Add(new LiteralControl(Setting.Description));

            this.txtValue.Text = actualValue.ToString();
        }
    }

    public string Value
    {
        get
        {
            return this.txtValue.Text;
        }
    }
}
