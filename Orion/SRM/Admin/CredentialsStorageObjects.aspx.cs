﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

public partial class Orion_SRM_Admin_CredentialsStorageObjects : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CredentialsSmsiXtremIo.UserNameLabel = SrmWebContent.Add_Provider_XtremIO_Username;
            CredentialsSmsiXtremIo.PasswordLabel = SrmWebContent.Add_Provider_XtremIO_Password;
            CredentialsSmsiXtremIo.HttpPortLabel = SrmWebContent.Add_Provider_Http_Port;
            CredentialsSmsiXtremIo.HttpsPortLabel = SrmWebContent.Add_Provider_Https_Port;
            CredentialsSmsiXtremIo.UseSslLabel = SrmWebContent.Add_Provider_Protocol;
        }
    }
}