<%@ Page Title="<%$ Resources:SrmWebContent,EditProperties_PageTitle %>" Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master"
    AutoEventWireup="true" CodeFile="EditProperties.aspx.cs" Inherits="SRM_EditProperties" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Reference Control="~/Orion/SRM/Config/Controls/AddDfmProvider.ascx" %>
<%@ Reference Control="~/Orion/SRM/Config/Controls/CelerraProviderStep.ascx" %>
<%@ Reference Control="~/Orion/SRM/Config/Controls/AddSmisAndGenericHttpProvider.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" File="EditProperties.css" Module="SRM" />
    <orion:Include runat="server" File="Discovery.css" Module="SRM" />
    <orion:Include runat="server" File="SRM.EditProperties.js" Module="SRM" />
    <orion:Include runat="server" File="SRM.ProviderChangeManager.js" Module="SRM" />
    <orion:Include ID="Include6" runat="server" Module="SRM" File="SRM.Common.js" />
    <orion:Include ID="Include1" runat="server" Module="SRM" File="SRM.Formatters.js" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="server">
    <div class="srm-EditProperties_AdminContent">
        <div class="editPropHeader titleTable">
            <h1><%= Page.Title %></h1>
            <span><%= Resources.SrmWebContent.EditProperties_SubHeader %></span>
        </div>
        
        <div class="editPropNameContainer" id="editNameContainer" runat="server"></div>
        <div runat="server" id="warningMessage" class="sw-suggestion sw-suggestion-warn sw-suggestion-icon SRM_warningMsgBox">
            <span class="sw-suggestion-icon"></span>
            <span runat="server" id="warningMessageHeader">
                <b><span runat="server" id="warningMessageHeaderContent"></span></b>
                <br/>
            </span>
            <span runat="server" id="warningMessageContent"></span>
        </div>
        <div class="editPropContainer" id="editContainer" runat="server"></div>

        <div runat="server" id="errorMessage" class="sw-suggestion sw-suggestion-fail">
            <span class="sw-suggestion-icon"></span>
            <span id="failMessageHeader">
                <b><span runat="server" id="failMessageHeaderContent"></span></b>
                <br />
            </span>
            <span runat="server" id="failMessageContent"></span>
        </div>

        <div class="editPropBottom">
            <div id="loadingImagePlaceholder">
                <asp:Image ImageUrl="~/Orion/images/loading_gen_small.gif" style="display: none;" ClientIDMode="Static" ID="loadingIcon" runat="server" AlternateText="Loading" />
            </div>
            <orion:LocalizableButton ID="BottomSubmitButton" ClientIDMode="Static" runat="server" DisplayType="Primary"
                LocalizedText="Submit" CausesValidation="true" OnClick="SubmitButton_Click" />
            <orion:LocalizableButton ID="BottomCancelButton" ClientIDMode="Static" runat="server" DisplayType="Secondary"
                LocalizedText="Cancel" CausesValidation="false" />
        </div>

        <asp:HiddenField runat="server" ID="ConnetionPropertyChangedField" ClientIDMode="Static" />
    </div>
</asp:Content>
