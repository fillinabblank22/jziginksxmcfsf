<%@ Page Title="<%$ Resources:SrmWebContent,SRM_Settings_ManageStorageObjects_Title %>" Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master"
    AutoEventWireup="true" CodeFile="ProviderStorageObjects.aspx.cs" Inherits="SRM_ProviderStorageObjects" %>

<%@ Register Src="~/Orion/SRM/Admin/StorageObjectsTabs.ascx" TagPrefix="srm" TagName="StorageObjectsTabs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
    
    <orion:Include runat="server" File="SRM/Admin/ProviderGrid.js" />
    <orion:Include runat="server" File="extjs/Ext.sw.grid.RadioSelectionModel.js" />
    <orion:Include runat="server" File="StorageObjects.css" Module="SRM" />
    <orion:Include runat="server" File="Admin/js/SearchField.js" />
    <orion:Include runat="server" File="Admin/Containers/Containers.css" />
    <orion:Include runat="server" File="SRM.css" Module="SRM" />
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="server">
    <input type="hidden" id="ReturnToUrl" value="<%= Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(Request.Url.AbsoluteUri)) %>" />
    <h1><%= Resources.SrmWebContent.SRM_Settings_ManageStorageObjects_Title %></h1>

    <srm:StorageObjectsTabs ID="StorageObjectsTabs1" runat="server" />

    <div id="Grid"></div>

    <script>

        SW.SRM.ProviderGrid.SetGridHeight(500);
        SW.SRM.ProviderGrid.init();

    </script>
</asp:Content>
