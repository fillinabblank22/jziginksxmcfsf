<%@ Control Language="C#" ClassName="StorageObjectsTabs" %>

<%@ Register Src="~/Orion/Controls/NavigationTabBar.ascx" TagPrefix="orion" TagName="NavigationTabBar" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" Assembly="OrionWeb" %>

<orion:NavigationTabBar ID="NavigationTabBar1" runat="server" Visible="true">

    <orion:NavigationTabItem Name="<%$ Resources:SrmWebContent,SRMSettings_ManageStorageObjects_ArraysTabItem %>" 
        Url="~/Orion/SRM/Admin/StorageObjects.aspx" />

    <orion:NavigationTabItem Name="<%$ Resources:SrmWebContent,SRMSettings_ManageStorageObjects_ProvidersTabItem %>" 
        Url="~/Orion/SRM/Admin/ProviderStorageObjects.aspx" />
    
    <orion:NavigationTabItem Name="<%$ Resources:SrmWebContent,SRMSettings_ManageStorageObjects_CredentialsTabItem %>" 
        Url="~/Orion/SRM/Admin/CredentialsStorageObjects.aspx" />

</orion:NavigationTabBar>
