using System;
using System.Collections.Generic;
using System.Globalization;
using System.ServiceModel;
using SolarWinds.Logging;
using Resources;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.Exceptions;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Web;

public partial class Orion_SRM_Admin_LicenseSummary : System.Web.UI.UserControl
{
    private static Log log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                GetModuleAndLicenseInfo();
            }
            catch (Exception ex)
            {
                log.Error("Error while displaying details for Orion Core module.", ex);
            }
        }
    }

    private void GetModuleAndLicenseInfo()
    {
        foreach (var module in ModuleDetailsHelper.LoadModuleInfoForEngines("Primary", false))
        {
            if (!module.ProductShortName.StartsWith(new SrmProfile().ModuleID, StringComparison.OrdinalIgnoreCase))
                continue;

            var values = new Dictionary<string, string>();

            SrmDetails.Name = module.ProductDisplayName;

            values.Add(SrmWebContent.LicenseDetails_Module_Name, module.ProductName);
            values.Add(SrmWebContent.LicenseDetails_Version, module.Version);
            values.Add(SrmWebContent.LicenseDetails_ServicePack, String.IsNullOrEmpty(module.HotfixVersion) ? SrmWebContent.LicenseDetails_ServicePack_None : module.HotfixVersion);

            AddSrmInfo(values);

            SrmDetails.DataSource = values;
            break; // we support only one "Primary" engine
        }
    }

    private void AddSrmInfo(Dictionary<string, string> values)
    {
        using (var proxy = BusinessLayerFactory.Create())
        {
            try
            {
                LicenseEntity lic = proxy.GetLicenseInfo();

                if (lic.IsEval)
                {
                    string licInfo = String.Format(CultureInfo.InvariantCulture, "<img alt=\"\" src=\"/Orion/images/icon.info.gif\" style=\"vertical-align:middle\">{0}", SrmWebContent.LicenseDetails_LicenseInfoContent);
                    values.Add(SrmWebContent.LicenseDetails_LicenseInfo, String.Format(CultureInfo.InvariantCulture, licInfo, lic.EvalDaysLeft));
                }

                if (lic.IsExpired)
                {
                    values[SrmWebContent.LicenseDetails_LicenseInfo] = String.Format(CultureInfo.CurrentCulture, "<span class=\"srm-red\">{0}</span>", SrmWebContent.AddStorage_ArrayStep_LicenseExpiredExceptionMessage);
                }
                else
                {
                    int licensed = lic.UsedBySRMCount + lic.UsedSharedCount + lic.UsedBySTMCount;
                    int unlicensed = Math.Max(0, lic.CurrentDiskCount - licensed);

                    String allowedDiskNumber = lic.IsEval ?
                                               SrmWebContent.SRMEnrollmentWizard_LicenseBar_Unlimited :
                                               lic.AllowedDiskCount.ToString(CultureInfo.CurrentCulture);

                    String availableNumberOfDisks = lic.IsEval ?
                                                    SrmWebContent.SRMEnrollmentWizard_LicenseBar_Unlimited :
                                                    lic.FreeCount.ToString(CultureInfo.CurrentCulture);

                    values.Add(SrmWebContent.LicenseDetail_AllowedNumberDisks, allowedDiskNumber);
                    values.Add(SrmWebContent.LicenseDetail_TotalNumberDisks, lic.CurrentDiskCount.ToString(CultureInfo.CurrentCulture));
                    if (lic.IsStandaloneModel)
                    {
                        values.Add(SrmWebContent.LicenseDetail_LicensedNumberDisks, lic.UsedBySRMCount.ToString(CultureInfo.CurrentCulture));
                    }
                    else
                    {
                        values.Add(SrmWebContent.LicenseDetail_LicensedNumberSRMDisks, lic.UsedBySRMCount.ToString(CultureInfo.CurrentCulture));
                        values.Add(SrmWebContent.LicenseDetail_LicensedNumberDisksStm, lic.UsedBySTMCount.ToString(CultureInfo.CurrentCulture));
                        values.Add(SrmWebContent.LicenseDetail_LicensedNumberDisksShared, lic.UsedSharedCount.ToString(CultureInfo.CurrentCulture));
                    }
                    values.Add(SrmWebContent.LicenseDetail_UnlicensedNumberDisks, unlicensed.ToString(CultureInfo.CurrentCulture));
                    values.Add(SrmWebContent.LicenseDetail_AvailableNumberDisks, availableNumberOfDisks);
                }
            }
            catch (FaultException<SrmLicenseNotFound> ex)
            {
                log.Error("License is not avialable.", ex);
                values[SrmWebContent.LicenseDetails_LicenseInfo] = String.Format(CultureInfo.CurrentCulture, "<a class=\"srm-red\" href={1} runat=\"server\" rel=\"noopener noreferrer\" target=\"_blank\">{0}</a>", SrmWebContent.LicensePage_GenericLicenseFailure, HelpHelper.GetHelpUrl("SRMPHLicenseSummaryGenericLicenseFailure"));
            }
            catch (Exception ex)
            {
                log.Error("Exception occured during getting license.", ex);
                values[SrmWebContent.LicenseDetails_LicenseInfo] = String.Format(CultureInfo.CurrentCulture, "<a class=\"srm-red\" href={1} runat=\"server\" rel=\"noopener noreferrer\" target=\"_blank\">{0}</a>", SrmWebContent.LicensePage_GenericLicenseFailure, HelpHelper.GetHelpUrl("SRMPHLicenseSummaryGenericLicenseFailure"));
            }
        }
    }
}
