using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Linq;
using System.Linq.Expressions;

using Resources;

using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using WebAgentManager = SolarWinds.Orion.Web.Agent.AgentManager;
using SolarWinds.SRM.Web.Interfaces;
using System.Web.UI.HtmlControls;
using SolarWinds.SRM.Web;

public partial class SRM_EditProperties : System.Web.UI.Page
{
    private static readonly Log log = new Log();
    private IEnumerable<ICreateEditControl> createEditControlList;
    private List<string> NetObjectIds = new List<string>();
    private StringBuilder clientValidationList;
    private readonly List<KeyValuePair<string, string>> allChildControlWarnings = new List<KeyValuePair<string, string>>();

    protected string ReturnTo;

    protected void Page_Load(object sender, EventArgs e)
    {
        allChildControlWarnings.Clear();

        //retrieve warning messages from created controls
        var ctrs = WebHelper.FindControl(this, c => c is IEditControl).ToArray();
        foreach (Control ctrl in ctrs)
        {
            ctrl.Load += (ctrlEditControl, args) =>
            {
                var editControlWarnings = ((IEditControl)ctrlEditControl).GetWarnings();
                if (editControlWarnings != null)
                    this.allChildControlWarnings.AddRange(editControlWarnings);

                if (this.allChildControlWarnings.Any())
                {
                    warningMessageHeaderContent.InnerText = this.allChildControlWarnings.Select(pair => pair.Key).FirstOrDefault(header => !String.IsNullOrEmpty(header));
                    warningMessageHeader.Visible = !String.IsNullOrWhiteSpace(warningMessageHeaderContent.InnerText);
                    
                    warningMessageContent.InnerHtml = String.Join("<br/>", 
                        this.allChildControlWarnings.Select(pair => pair.Value).Distinct().Where(messages => !String.IsNullOrEmpty(messages)).Select(s => String.Format("<span>{0}</span>", s)).ToArray()
                    );
                    warningMessage.Visible = true;
                }
            };
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.createEditControlList = new List<ICreateEditControl>(new[] {
            (ICreateEditControl)LoadControl("~/Orion/SRM/Controls/EditName.ascx"),
            (ICreateEditControl)LoadControl("~/Orion/SRM/Config/Controls/EditProvider.ascx"),
            (ICreateEditControl)LoadControl("~/Orion/SRM/Config/Controls/SmisProvider.ascx"),
            (ICreateEditControl)LoadControl("~/Orion/SRM/Config/Controls/EmbeddedSmisProvider.ascx"),
            (ICreateEditControl)LoadControl("~/Orion/SRM/Config/Controls/CelerraProviderStep.ascx"),
            (ICreateEditControl)LoadControl("~/Orion/SRM/Config/Controls/GenericSnmpProviderStep.ascx"),
            (ICreateEditControl)LoadControl("~/Orion/SRM/Config/Controls/IsilonProviderStep.ascx"),
            (ICreateEditControl)LoadControl("~/Orion/SRM/Config/Controls/XtremIoEdit.ascx"),
            (ICreateEditControl)LoadControl("~/Orion/SRM/Config/Controls/GenericRestProviderStep.ascx"),
            (ICreateEditControl)LoadControl("~/Orion/SRM/Config/Controls/NetAppProviderStep.ascx"),
            (ICreateEditControl)LoadControl("~/Orion/SRM/Config/Controls/DfmProvider.ascx"),
            (ICreateEditControl)LoadControl("~/Orion/SRM/Config/Controls/ExternalProvider.ascx"),
            (ICreateEditControl)LoadControl("~/Orion/SRM/Config/Controls/AddSmisAndGenericHttpProvider.ascx"),
            (ICreateEditControl)LoadControl("~/Orion/SRM/Controls/PollingProperties.ascx"),
            // Once we add support for dependencies, drop this comment to put the Dependencies control back to the page
            //(ICreateEditControl)LoadControl("~/Orion/SRM/Controls/Dependencies.ascx"),
            (ICreateEditControl)LoadControl("~/Orion/SRM/Controls/PollingPropertiesVserver.ascx"),
            (ICreateEditControl)LoadControl("~/Orion/SRM/Controls/PollingPropertiesStorageController.ascx"),
            (ICreateEditControl)LoadControl("~/Orion/SRM/Controls/HardwareHealthProperties.ascx"),
            (ICreateEditControl)LoadControl("~/Orion/SRM/Controls/EditCustomProperties.ascx"),
            (ICreateEditControl)LoadControl("~/Orion/SRM/Controls/ThresholdControl.ascx"),
            (ICreateEditControl)LoadControl("~/Orion/SRM/Controls/EditPropertiesAdvanced.ascx"),
        });
        errorMessage.Visible = false;
        warningMessage.Visible = false;

        AddNetObjectIfValid(Request.QueryString["NetObject"]);

        EnsureNetObjectsExists();

        RenderControls();

        BindButtons();
    }

    private void BindButtons()
    {
        ReturnTo = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(Request.QueryString["ReturnTo"]));
        this.BottomCancelButton.OnClientClick = string.Format("javascript: SW.SRM.EditProperties.redirect(\'{0}\'); return false;", ReturnTo);
        this.BottomSubmitButton.OnClientClick = clientValidationList.ToString();
    }

    private void RenderControls()
    {
        var namesSB = new StringBuilder();
        foreach (var netObjectID in NetObjectIds)
        {
            var no = NetObjectFactory.Create(netObjectID, false);
            namesSB.AppendFormat("<li>{0}</li>", no.Name);
        }
       
        var ul = new System.Web.UI.HtmlControls.HtmlGenericControl("ul");
        ul.InnerHtml = namesSB.ToString();
        editNameContainer.Controls.Add(ul);

        clientValidationList = new StringBuilder();
        clientValidationList.Append("javascript: return SW.SRM.EditProperties.validate([");
        bool first = true;
        foreach (var createEC in createEditControlList)
        {
            var createdNO = createEC.InitFromNetObjects(NetObjectIds);
            
            // If control does not support the netobjects
            if (createdNO == null)
                continue;

            var ctrl = (Control)createdNO;
            var containerHost = new System.Web.UI.HtmlControls.HtmlGenericControl("div") { ClientIDMode = System.Web.UI.ClientIDMode.Static };
            containerHost.Attributes["class"] = "EditPropControlHost";
            containerHost.Controls.Add(ctrl);
            editContainer.Controls.Add(containerHost);

            var jsVal = createdNO.ValidateJSFunction();
            if (jsVal != null)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    clientValidationList.Append(", ");
                }
                clientValidationList.AppendFormat("function() {{ return {0} }}", jsVal);
            }
        }
        clientValidationList.Append("],");
        var submitScript = Page.ClientScript.GetPostBackEventReference(BottomSubmitButton, BottomSubmitButton.ClientID);

        clientValidationList.AppendFormat(" function() {{ {0} }}", submitScript);
        clientValidationList.Append(");");

        editContainer.Focus();
    }

    private void EnsureNetObjectsExists()
    {
        if (NetObjectIds.Count == 0)
        {
            log.Debug("Redirecting to Manage Storage Objects, because list of valid NetObjectIDs is empty.");
            Response.Redirect("~/Orion/SRM/Admin/StorageObjects.aspx");
        }

        var uniqueNOTypes = NetObjectIds.Select(no => no.Split(':')[0]).Distinct();
        if (uniqueNOTypes.Count() < 1 && uniqueNOTypes.Count() > 1)
        {
            log.Debug("Redirecting to Manage Storage Objects, too few or too many NetObject prefixes passed");
            Response.Redirect("~/Orion/SRM/Admin/StorageObjects.aspx");
        }
    }

    private void AddNetObjectIfValid(string netObj)
    {
        if (netObj != null)
            NetObjectIds.AddRange(netObj.Split(','));
    }
    
    protected void SubmitButton_Click(object sender, EventArgs e)
    {
        bool connectionPropertyChanged = false;
        Boolean.TryParse(ConnetionPropertyChangedField.Value, out connectionPropertyChanged);

        var ctrs = WebHelper.FindControl(this, c => c is IEditControl).ToArray();
        // Server validation
        foreach (Control ctrl in ctrs)
        {
            var ctrlEditControl = (IEditControl)(ctrl);
            var res = ctrlEditControl.Validate(connectionPropertyChanged);
            if (res)
            {
                continue;
            }

            if (ctrl as IConfigurationControl != null)
            {
                this.failMessageHeaderContent.InnerText = SrmWebContent.Provider_Step_Server_Validation_Title;
                this.failMessageContent.InnerText = ((IConfigurationControl)ctrl).ValidationError;
            }

            if (ctrl as IValidationMessage != null)
            {
                this.failMessageHeaderContent.InnerText = ((IValidationMessage) ctrl).ValidationMessage;
            }
            

            errorMessage.Visible = true;
            return;
        }
     
        // Submit changes
        foreach (Control ctrl in ctrs)
        {
            var ctrlEditControl = (IEditControl)ctrl;
            ctrlEditControl.Update(connectionPropertyChanged);
        }

        if (ReturnTo != null)
            Response.Redirect(ReturnTo);
    }
}
