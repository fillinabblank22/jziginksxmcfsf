<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ThresholdSettingControl.ascx.cs" Inherits="Orion_SRM_Admin_ThresholdSettingControl" %>

<tr>
    <th><asp:PlaceHolder runat="server" ID="phType" /></th>
    <td class="Property"><asp:TextBox runat="server" ID="txtValue" Columns="6" /><%if (this.DoRangeValidate)
      {%>
    
    <asp:RangeValidator ID="ThesholdRangeValidator" runat="server" ControlToValidate="txtValue" MinimumValue="0" MaximumValue="2000000000" Type="Double" Display="Dynamic"></asp:RangeValidator>
    <asp:RequiredFieldValidator runat="server" ID="ThesholdRequiredfield" ControlToValidate="txtValue"></asp:RequiredFieldValidator>
    <% } %>   </td>
    <td><asp:PlaceHolder runat="server" ID="phMinMax" /></td>
    <td class="allowWrap"><asp:PlaceHolder runat="server" ID="phDesc" /></td>

</tr>

