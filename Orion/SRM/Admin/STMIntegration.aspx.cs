using System;
using System.Globalization;
using SolarWinds.Logging;
using Resources;
using SolarWinds.SRM.Common.BL;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.SRM.Common.Models;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Internationalization.Exceptions;

public partial class STMIntegration : System.Web.UI.Page
{
    private static readonly Log log = new Log();

    protected Boolean IsIntegrationEnabled { get; set; }
    protected STMIntegrationEntity IntegrationDetails { get; set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        CheckRights();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            btnDisbale.Text = SrmWebContent.StorageManagerIntegration_DisableIntegration;
            Refresh();
        }
    }

    // <summary> Check user rights. If they are not sufficient, then redirects to error page. </summary>
    private void CheckRights()
    {
        if (OrionConfiguration.IsDemoServer && !Profile.AllowAdmin)
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.SrmWebContent.DemoModeMessage), Title = Resources.CoreWebContent.WEBDATA_VB0_567 });            
    }

    private void Refresh()
    {
        using (var proxy = BusinessLayerFactory.CreateMain())
        {
            this.IntegrationDetails = proxy.GetSTMIntegrationDetails();
            txtServerName.Text = this.IntegrationDetails.HostName;
            txtPort.Text = this.IntegrationDetails.Port.ToString(CultureInfo.CurrentCulture);
            txtUserName.Text = String.Empty;
            txtPassword.Text = String.Empty;

            this.ContentTable.Visible = !this.IntegrationDetails.IntegrationEnabled;
        }
    }

    protected void imgCancelButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Orion/SRM/Admin/Default.aspx");
    }

    protected void imgSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            using (var proxy = BusinessLayerFactory.CreateMain())
            {
                var creds = new UsernamePasswordCredential()
                {
                    Username = txtUserName.Text.Trim(),
                    Password = txtPassword.Text.Trim()
                };
                string hostName = txtServerName.Text.Trim();
                ushort portNumber;
                if (!ushort.TryParse(txtPort.Text.Trim(), out portNumber))
                {
                    portNumber = 443;
                }

                var message = proxy.CreateSTMIntegration(hostName, portNumber, creds);
                if (String.IsNullOrEmpty(message))
                {
                    Response.Redirect("/Orion/SRM/Admin/Default.aspx");
                }
                else
                {
                    Refresh();
                    txtUserName.Text = creds.Username;
                    txtPassword.Text = creds.Password;

                    string script = String.Format(CultureInfo.CurrentCulture, "Ext.Msg.alert('{0}', '{1}');",
                        /* {0} */ SrmWebContent.SRMSettings_SetupStorageManagerIntegration,
                        /* {1} */ message.Replace("'", "\\'"));

                    this.ClientScript.RegisterStartupScript(this.GetType(), "ResponseMessage", script, true);
                }
            }
        }
        catch (Exception ex)
        {
            log.Error("Exception occured during submitting STM Integration", ex);
            throw;
        }
    }

    protected void btnDisbale_Click(object sender, EventArgs e)
    {
        try
        {
            using (var proxy = BusinessLayerFactory.CreateMain())
            {
                proxy.DisableSTMIntegration();
                Refresh();
            }
        }
        catch (Exception ex)
        {
            log.Error("Exception occured during disabling STM Integration", ex);
            throw;
        }
    }
}
