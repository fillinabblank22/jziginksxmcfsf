<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" Title="<%$ Resources:SrmWebContent,SRMSettings_PageTitle %>" 
    CodeFile="~/Orion/SRM/Admin/Default.aspx.cs" Inherits="SRMSettings" %>
<%@ Register Src="~/Orion/Controls/HelpButton.ascx" TagPrefix="orion" TagName="HelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" File="Settings.css" Module="SRM" />
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <table class="HeaderTable" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <h1><%=Page.Title %></h1>
                <div class="version"> <%= this.ProductNameAndVersion %> </div>
            </td>
            <td align="right">
            </td>
        </tr>
    </table>
    <div class="srmSettings">
        <!-- Getting Started with SRM -->
        <div class="ContentBucket">
            <table>
                <tr>
                    <td class="iconCls">
                        <img src="/Orion/SRM/Images/srmgettingstarted.png" />
                    </td>
                    <td>
                        <h3><%= Resources.SrmWebContent.SRMSettings_GettingStarted %></h3>
                        <span><%= Resources.SrmWebContent.SRMSettings_GettingStartedSubline %></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ul class="horizontal">
                            <li><a href="/Orion/SRM/Config/DeviceTypeStep.aspx"><%= Resources.SrmWebContent.SRMSettings_AddStorageDevices %></a></li>
                            <li><a href="/Orion/SRM/Admin/ManualE2EMapping/Default.aspx"><%= Resources.SrmWebContent.SRMSettings_MapServerVolumes %></a></li>
                            <li><a href="/Orion/SRM/Admin/StorageObjects.aspx"><%= Resources.SrmWebContent.SRMSettings_ManageStorageObjects %></a></li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>

        <!-- Global SRM Settings -->
        <div class="ContentBucket">
            <table>
                <tr>
                    <td class="iconCls">
                        <img src="/Orion/SRM/Images/globalsrmsettings.png" />
                    </td>
                    <td>
                        <h3><%= Resources.SrmWebContent.SRMSettings_GlobalSRMSettings %></h3>
                        <span><%= Resources.SrmWebContent.SRMSettings_GlobalSRMSettingssubline %></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ul class="vertical">
                            <li><a href="/Orion/SRM/Admin/ArrayThreshold.aspx"><%= Resources.SrmWebContent.SRMSettings_GlobalArrayThreshold %></a></li>
                            <li><a href="/Orion/SRM/Admin/PoolThreshold.aspx"><%= Resources.SrmWebContent.SRMSettings_GlobalStoragePoolThresholds %></a></li>
                            <li><a href="/Orion/SRM/Admin/LunThreshold.aspx"><%= Resources.SrmWebContent.SRMSettings_GlobalLUNThresholds %></a></li>
                            <li><a href="/Orion/SRM/Admin/NasThreshold.aspx"><%= Resources.SrmWebContent.SRMSettings_GlobalNASVolumeThresholds %></a></li>
                            <li><a href="/Orion/SRM/Admin/ShareThreshold.aspx"><%= Resources.SrmWebContent.SRMSettings_GlobalFileShareThresholds %></a></li>
                            <li><a href="/Orion/SRM/Admin/VserverThreshold.aspx"><%= Resources.SrmWebContent.SRMSettings_GlobalVserverThresholds %></a></li>
                            <li><a href="/Orion/SRM/Admin/StorageControllerThreshold.aspx"><%= Resources.SrmWebContent.SRMSettings_GlobalStorageControllerThresholds %></a></li>
                            <li><a href="/Orion/SRM/Admin/StorageControllerPortThreshold.aspx"><%= Resources.SrmWebContent.SRMSettings_GlobalStorageControllerPortThresholds %></a></li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>

        <!-- License Summary -->
        <div class="ContentBucket">
            <table>
                <tr>
                    <td class="iconCls">
                        <img src="/Orion/SRM/Images/LicenseSummary.png" />
                    </td>
                    <td>
                        <h3><%= Resources.SrmWebContent.SRMSettings_LicenseSummaryHeading %></h3>
                        <span><%= Resources.SrmWebContent.SRMSettings_LicenseSummaryHeadingSubline %></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ul class="horizontal">
                            <li><a href="/Orion/SRM/Admin/STMIntegration.aspx"><%= Resources.SrmWebContent.SRMSettings_ManageSTMIntegration %></a></li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>

        <!-- Groups -->
        <div class="ContentBucket">
            <table>
                <tr>
                    <td class="iconCls">
                        <img src="/Orion/SRM/Images/groups.png" />
                    </td>
                    <td>
                        <h3><%= Resources.SrmWebContent.SRMSettings_Groups %></h3>
                        <span><%= Resources.SrmWebContent.SRMSettings_GroupsSubline %></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ul class="horizontal">
                            <li><a href="/Orion/Admin/Containers/Default.aspx"><%= Resources.SrmWebContent.SRMSettings_ManageGroups %></a></li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>

        <!-- Credentials -->
        <div class="ContentBucket">
            <table> 
                <tr>
                    <td class="iconCls">
                      <img src="/Orion/SRM/images/StorageCredentials.gif" class="BucketIcon">
                    </td>
                    <td>
                       <h3><%= Resources.SrmWebContent.SRMSettings_ManageCredentials_Title %></h3>
                        <span><%= Resources.SrmWebContent.SRMSettings_ManageCredentials_Subtitle %></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                      <hr />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                      <ul class="horizontal">
                            <li><a href="/Orion/SRM/Admin/CredentialsStorageObjects.aspx"><%= Resources.SrmWebContent.SRMSettings_ManageCredentials_Link %></a></li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>

        <!-- thwack Community -->
        <div class="ContentBucket">
            <table>
                <tr>
                    <td class="iconCls">
                        <img src="/Orion/SRM/Images/thwack.png" />
                    </td>
                    <td>
                        <h3><%= Resources.SrmWebContent.SRMSettings_thwackCommunity %></h3>
                        <span><%= Resources.SrmWebContent.SRMSettings_thwackCommunitySubline %></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ul class="horizontal">
                            <li><a href="https://thwack.solarwinds.com/community/cloud-virtualization-storage_tht/storage-manager" rel="noopener noreferrer" target="_blank"><%= Resources.SrmWebContent.SRMSettings_SRMThwackForum %></a></li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>

    </div>
    
</asp:Content>

