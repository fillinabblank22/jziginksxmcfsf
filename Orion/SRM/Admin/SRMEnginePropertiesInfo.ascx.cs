﻿using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Admin.Engines;
using SolarWinds.Orion.Web.InformationService;

public partial class SRM_Admin_SRMEnginePropertiesInfo : EngineInfoBaseControl
{
    private readonly static Log _log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            LoadEngineProperties();
        }
        catch (Exception ex)
        {
            _log.Error(string.Format("Error fetching details about EngineID {0}.", EngineID), ex);
            ShowErrorBlock(ex);
        }
    }

    private void LoadEngineProperties()
    {
        using (var swis = InformationServiceProxy.CreateV3())
        {
            string query = @"SELECT @StorageArrayElements as ElementsPropertyName, COUNT(StorageArrayID) as ElementsPropertyValue,
                                    @StorageArraysTotalDisks as TotalDiskPropertyName, IsNull(SUM(Disks), 0) as TotalDiskPropertyValue
                                     FROM Orion.SRM.StorageArrays
                                     WHERE EngineID=@EngineID";

            DataTable engineProperties = swis.Query(query, new Dictionary<string, object> { { "EngineID", EngineID },
                                                                                            { "StorageArrayElements", Resources.SrmWebContent.PollingEngines_StorageArrayElements},
                                                                                            { "StorageArraysTotalDisks", Resources.SrmWebContent.PollingEngines_StorageArraysTotalDisks}});
            SRMEnginePropertiesRepeater.DataSource = engineProperties;
            SRMEnginePropertiesRepeater.DataBind();
        }
    }


    private void ShowErrorBlock(Exception ex)
    {
        SRMEnginePropertiesRepeater.Visible = false;
        ErrorBlock.Visible = true;
        ErrorLabel.Text = (ex is LocalizedExceptionBase) ? (ex as LocalizedExceptionBase).LocalizedMessage : ex.Message;
    }
}
