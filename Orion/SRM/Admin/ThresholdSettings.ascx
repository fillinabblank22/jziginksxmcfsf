<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ThresholdSettings.ascx.cs" Inherits="Orion_SRM_Admin_ThresholdSettings" %>
<%@ Register TagPrefix="SRM" TagName="ThresholdSettingControl" Src="~/Orion/SRM/Admin/ThresholdSettingControl.ascx" %>
    
<table cellspacing="0" id="thresholdTable">
    <asp:Repeater id="rptThresholdsSettings" runat="server">
        <ItemTemplate>
                <tr>
                    <td colspan="4"><h2><%#Eval("ThresholdsTypeTitle") %></h2></td>
                </tr>
            
                <SRM:ThresholdSettingControl ID="criticalSettings" runat="server" DoRangeValidate='True' Units='<%#Eval("Units") %>' ConversionConstant='<%#Eval("ConversionConstant") %>' MinAndMaxLimitsReversed='<%#Eval("MinAndMaxLimitsReversed") %>' MinValue='<%#Eval("OrionSettings.Critical.Min") %>' MaxValue='<%#Eval("OrionSettings.Critical.Max") %>' Setting='<%#Eval("OrionSettings.Critical.Setting") %>' />    
                <SRM:ThresholdSettingControl ID="warningSettings" runat="server" DoRangeValidate='True' Units='<%#Eval("Units") %>' ConversionConstant='<%#Eval("ConversionConstant") %>' MinAndMaxLimitsReversed='<%#Eval("MinAndMaxLimitsReversed") %>' MinValue='<%#Eval("OrionSettings.Warning.Min") %>' MaxValue='<%#Eval("OrionSettings.Warning.Max") %>' Setting='<%#Eval("OrionSettings.Warning.Setting") %>' />

            <td colspan="4" runat="server" visible="false" id="validationSection">
                <div runat="server" id="errorMessage" class="sw-suggestion sw-suggestion-fail">
                    <span class="sw-suggestion-icon"></span>
                    <span runat="server" id="failMessageContent"></span>
                </div>
            </td>
        </ItemTemplate>
    </asp:Repeater>
</table>