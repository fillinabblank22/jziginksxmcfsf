<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" Title="<%$ Resources:SrmWebContent,SRMSettings_SetupStorageManagerIntegration %>"
    CodeFile="~/Orion/SRM/Admin/STMIntegration.aspx.cs" Inherits="STMIntegration" %>
<%@ Register Src="~/Orion/Controls/HelpButton.ascx" TagPrefix="orion" TagName="HelpButton" %>
<%@ Import Namespace="Resources" %>
<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" File="Admin.css" />
    <orion:Include runat="server" Module="SRM" File="SRM.css" />
    <orion:Include runat="server" Module="SRM" File="Settings.css" />
    <orion:Include runat="server" Module="SRM" File="SRM.Common.js" />
    <orion:Include Framework="Ext" FrameworkVersion="3.4" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <style type="text/css">
        #integrationIsEnabled {
            padding: 5px 10px 5px 10px;
        }

        #integrationIsEnabled > span {
            vertical-align: middle;
            font-size: 12px;
        }

        #integrationIsEnabled > a {
            vertical-align: middle;
        }

        table.HeaderTable h1 {
            font-size: large;
        }
    </style>
    <script>
        var request;

        $(document).ready(function () {
            enableButtons(true);
            if ('<%= this.IntegrationDetails.IntegrationEnabled %>' === 'True')
                $('#integrationIsEnabled > span').text('<%= SrmWebContent.StorageManagerIntegration_IsEnabled %> "<%= this.IntegrationDetails.HostName %>:<%= this.IntegrationDetails.Port %>".');
            else
                $('#integrationIsEnabled').hide();
        });

        var submit = function () {
            __doPostBack('<%= imgSubmit.UniqueID %>', '');
        };

        var onTestButtonClick = function (onSuccess) {
            var hostNameValue = $('#txtServerName').val();
            var portValue = $('#txtPort').val();
            var userNameValue = $('#txtUserName').val();

            if ($.trim(String(hostNameValue)) === '') {
                showFailMessageBox("<%= SrmWebContent.StorageManagerIntegration_EmptyField %>".replace('{0}', $('#labelServerName').text()));
                return;
            }
            if ($.trim(String(portValue)) === '') {
                showFailMessageBox("<%= SrmWebContent.StorageManagerIntegration_EmptyField %>".replace('{0}', $('#labelPort').text()));
                return;
            }
            var portValueInt = parseInt(portValue);
            if (!$.isNumeric(portValueInt) || Math.floor(portValueInt) !== portValueInt || portValueInt < 1 || portValueInt > 65535) {
                showFailMessageBox("<%= SrmWebContent.StorageManagerIntegration_PortValidation %>".replace('{0}', $('#labelPort').text()));
                return;
            }
            if ($.trim(String(userNameValue)) === '') {
                showFailMessageBox("<%= SrmWebContent.StorageManagerIntegration_EmptyField %>".replace('{0}', $('#labelUserName').text()));
                return;
            }

            hideMessageBoxes();
            $('#testingConnectionMsg').show();
            enableButtons(false);

            request = SW.SRM.Common.CallSrmWebService(
                '/Orion/SRM/Services/SRMCommonService.asmx',
                'STMIntegration_TestConnection',
                {
                    hostName: $('#txtServerName').val(),
                    port: $('#txtPort').val(),
                    userName: $('#txtUserName').val(),
                    password: $('#txtPassword').val()
                },
                function (result) {
                    $('#testingConnectionMsg').hide();
                    if (result.IsSuccess) {
                        showPassMessageBox('<%= SrmWebContent.StorageManagerIntegration_ConnectionSuccess%>');
                        if (onSuccess) {
                            onSuccess();
                        }
                    } else {
                        showFailMessageBox(result.Message);
                    }
                    enableButtons(true);
                },
                function () {
                    $('#testingConnectionMsg').hide();
                    enableButtons(true);

                    if (request.status === 0) {
                        return;
                    }

                    showFailMessageBox('<%= SrmWebContent.STMIntegration_Unexpected_Error%>');
                });
        };

            var enableButtons = function (state) {
                if (state === true) {
                    $('#TestConnectionButton').removeClass('sw-btn-disabled');
                    $('#TestConnectionButton').attr('href', 'javascript:onTestButtonClick();');

                    $('#submitButton').removeClass('sw-btn-disabled');
                    $('#submitButton').attr('href', 'javascript:onTestButtonClick(submit);');
                }
                else {
                    $('#TestConnectionButton').addClass('sw-btn-disabled');
                    $('#TestConnectionButton').attr('href', 'javascript:void(0);');

                    $('#submitButton').addClass('sw-btn-disabled');
                    $('#submitButton').attr('href', 'javascript:void(0);');
                }
            };

            var onCancelClick = function () {
                enableButtons(true);
                $('#testingConnectionMsg').hide();
                if (request) {
                    request.abort();
                }
            };

            var hideMessageBoxes = function () {
                $('#testConnectionPassMsg').hide();
                $('#testConnectionFailMsg').hide();
            };

            var showPassMessageBox = function (text) {
                $('#passMessageContent').empty();
                $('#passMessageContent').append(text);
                $('#testConnectionPassMsg').show();
            };

            var showFailMessageBox = function (message) {
                $('#testConnectionPassMsg').hide();

                var $failMessageHeaderContent = $('#failMessageHeaderContent');
                $failMessageHeaderContent.empty();
                $failMessageHeaderContent.append('<%= SrmWebContent.STMIntegration_Connecting_Error %>:');

                var $failMessageContent = $('#failMessageContent');
                $failMessageContent.empty();
                $failMessageContent.append(message);

                $('#failMessageHeader').show();
                $('#testConnectionFailMsg').show();
            };
    </script>

    <table class="HeaderTable">
        <tr>
            <td>
                <h1><%= SrmWebContent.SRMSettings_SetupStorageManagerIntegration %></h1>
            </td>
        </tr>
        <tr>
            <td>
                <%= SrmWebContent.StorageManagerIntegration_MachineCredential %>
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <div id="integrationIsEnabled" class="sw-suggestion">
                    <span></span>
                    <orion:LocalizableButton ID="btnDisbale" runat="server" DisplayType="Small" OnClick="btnDisbale_Click" LocalizedText="CustomText" CausesValidation="false" />
                </div>
            </td>
        </tr>
        <tr class="linkLearn">
            <td>
                <a href="http://www.solarwinds.com/documentation/kbloader.aspx?kb=5872" rel="noopener noreferrer" target="_blank" class="helpLink"><%= SrmWebContent.StorageManagerIntegration_LearnMore %></a>
            </td>
        </tr>
    </table>
    <div id="ContentTable" class="connectionSettings" runat="server">
        <div class="ContentBucket">
            <table class="connectionDetails">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td><span id="labelServerName"><%= SrmWebContent.StorageManagerIntegration_ServerName %></span>:</td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtServerName" ClientIDMode="Static" />
                                    <label><%= SrmWebContent.StorageManagerIntegration_LocationOfStorageManager %></label>
                                </td>
                            </tr>
                            <tr>
                                <td><span id="labelPort"><%= SrmWebContent.StorageManagerIntegration_Port %></span>:</td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtPort" ClientIDMode="Static" />
                                    <label><%= SrmWebContent.StorageManagerIntegration_DefaultPort %></label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td colspan="2">
                                    <h2><%= SrmWebContent.StorageManagerIntegration_StorageManagerCredentials %></h2>
                                    <span><%= SrmWebContent.StorageManagerIntegration_StorageManagerServer %></span>
                                </td>
                            </tr>
                            <tr>
                                <td><span id="labelUserName"><%= SrmWebContent.StorageManagerIntegration_UserName %></span>:</td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtUserName" ClientIDMode="Static" />
                            </tr>
                            <tr>
                                <td><%= SrmWebContent.StorageManagerIntegration_Password %>:
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtPassword" ClientIDMode="Static" TextMode="Password" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <a id="TestConnectionButton" class="sw-btn-small sw-btn">
                                        <span class="sw-btn-c">
                                            <span class="sw-btn-t"><%= SrmWebContent.StorageManagerIntegration_TestConnection %></span>
                                        </span>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div id="testingConnectionMsg" class="sw-suggestion srm-testing-connection-msgbox">
                                        <img src="/Orion/images/loading_gen_16x16.gif" /><b><%= SrmWebContent.STMIntegration_Testing_Connections %></b>
                                        <a onclick="return onCancelClick();"><%= SrmWebContent.STMIntegration_Cancel_Testing_Connection %></a>
                                    </div>
                                    <div id="testConnectionFailMsg" class="sw-suggestion sw-suggestion-fail srm-testing-connection-msgbox">
                                        <span class="sw-suggestion-icon"></span>
                                        <span id="failMessageHeader">
                                            <b><span id="failMessageHeaderContent"></span></b>
                                            <br />
                                        </span>
                                        <span id="failMessageContent"></span>
                                    </div>
                                    <div id="testConnectionPassMsg" class="sw-suggestion sw-suggestion-pass srm-testing-connection-msgbox">
                                        <span class="sw-suggestion-icon"></span><span id="passMessageContent"></span>
                                    </div>
                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>
            </table>

        </div>

        <div class="sw-btn-bar">
            <orion:LocalizableButton runat="server" ID="imgSubmit" LocalizedText="Submit" DisplayType="Primary" Visible="false" OnClick="imgSubmit_Click" />
            <a id="submitButton" class="sw-btn-primary sw-btn">
                <span class="sw-btn-c">
                    <span class="sw-btn-t"><%= Resources.CoreWebContent.WEBDATA_VB0_311 %></span>
                </span>
            </a>
            <orion:LocalizableButton ID="imgCancelButton" runat="server" DisplayType="Secondary" LocalizedText="Cancel" CausesValidation="false" OnClick="imgCancelButton_Click" />
        </div>

    </div>
</asp:Content>

