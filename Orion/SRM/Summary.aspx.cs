using System;

using SolarWinds.Orion.Web.UI;

public partial class Summary : OrionView
{
    protected override void OnInit(EventArgs e)
    {
        this.Title = this.ViewInfo.ViewTitle;
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    public override string ViewType
    {
        get { return "SRM Summary"; }
    }
}