﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PollingPropertiesVserver.ascx.cs" Inherits="Orion_SRM_Controls_PollingPropertiesVserver" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Import Namespace="SolarWinds.SRM.Web" %>
<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include runat="server" File="SRM.EditProperties.SmisProvider.css" Module="SRM" />
<orion:Include runat="server" File="SRM.css" Module="SRM" />
<orion:Include runat="server" File="SRM.EditProperties.PollingProperties.js" Module="SRM" />
<orion:Include runat="server" File="OrionCore.js" Section="Bottom" SpecificOrder="1" />
<orion:Include runat="server" File="Nodes/js/ChangeEngineDialog.js" Section="Bottom" SpecificOrder="2" />
<orion:Include runat="server" File="extjs/Ext.sw.grid.RadioSelectionModel.js" />


<div class="EditPropControlHost">
    <span><%= SrmWebContent.EditProperties_VServer_PollingProperties %></span>

    <div class="PollingPropertiesSection">
        <div class="srm-main-content smisprovider">
            <table>
                <tr>
                    <td class="col1">
                        <table>
                            <tr>
                                <td class="label"><span><span id="labelVserverType">Vserver Type</span>:</span></td>
                                <td>
                                    <asp:Label ID="txtserverType" runat="server" CssClass="boldText" />
                                </td>
                            </tr>
                            <tr>
                                <td class="label"><span><span id="labelProvider"><%= SrmWebContent.EditProperties_VServer_Cluster %></span>:</span>
                                    <div id="loadingImageSmisPlaceholder">
                                        <asp:Image ImageUrl="~/Orion/images/loading_gen_small.gif" Style="display: none;" ClientIDMode="Static" ID="loadingIconSmisProvider" runat="server" AlternateText="Loading" />
                                    </div>
                                </td>
                                <td class="SRM_StatusImage">
                                    <img class="StatusIcon" src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.VServers%>&status=<%=(int)VServerStatus%>&size=small" />
                                    <asp:HyperLink style="vertical-align: top;" ID="ClusterHyperLink" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <asp:Image runat="server" ID="image" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
