﻿using SolarWinds.SRM.Common;
using System;
using System.Collections.Generic;
using System.Linq;

public partial class Orion_SRM_Controls_AjaxTree : System.Web.UI.UserControl
{
    private string serviceName = "null";
    private string initialValue = "null";
    private string batchSize = SRMConstants.AjaxTreeBatchSize.ToString();

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Wrapper.ShowEditButton = Wrapper.ShowEditButton && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
    }

    public string ServicePath 
    {
        get 
        { 
            return this.TreeScriptManager.Services.First().Path; 
        }
        set 
        { 
            this.TreeScriptManager.Services.First().Path = value; 

            string[] pathParts = value.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            if (pathParts.Length > 0)
            {
                string[] fileNameParts = pathParts.Last().Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                if (fileNameParts.Length > 0)
                {
                    ServiceName = fileNameParts.First();
                }
            }
        }
    }

    public string ServiceName { get { return serviceName; } private set { serviceName = value; } }
    public string ResourceId { get; set; }
    public string[] Grouping { get; set; }
    public string InitialValue { get { return initialValue; } set { initialValue = value; } }
    public string RememberExpandedGroups { get; set; }
    public string ExpandRootLevel { get; set; }
    public string ShowMoreSingularMessage { get; set; }
    public string ShowMorePluralMessage { get; set; }
    public string Filters { get; set; }
    public string ExpandAll { get; set; }
    public string BatchSize { get { return batchSize; } set { batchSize = value; } }
}