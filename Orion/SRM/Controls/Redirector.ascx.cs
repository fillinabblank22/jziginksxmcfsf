﻿using SolarWinds.Orion.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_SRM_Controls_Redirector : System.Web.UI.UserControl
{
    // This is handling Post-Redirect-Get pattern
    // arguments are sent using POST to redirectPostUrl, which passes them using Get to targetUrl

    string redirButtonResourceID = "redirButtonResourceID";
    // the Control ID "redirParamsResourceID" is used in RedirectGet to gather passed values
    string redirParamsResourceID = "redirParamsResourceID";
    private System.Web.UI.WebControls.Button redirButton;
    private System.Web.UI.WebControls.HiddenField redirParams;

    // SW.SRM.Redirector.Redirect call example
    /*
        var arguments = {
            // Page where redirector will Redirect with Get request
            targetUrl: '/SRM/DisplayResource.aspx',
            queryString: {
                netObject: 'N:1',
            },
            params : {
                param1: 'test1',
                param2: 'test2'
            }
        }
        SW.SRM.Redirector.Redirect(arguments);
     */

    string javaScriptRedirectorInit = @"$(function() { SW.SRM.Redirector.init('{0}', '{1}', '{2}'); })";

    protected void Page_Load(object sender, EventArgs e)
    {
        UserControl redirector = sender as UserControl;
        if (redirector != null && redirector.Visible == false)
            return;

        if (!Page.IsPostBack)
        {
            // Include just once per page
            redirButton = (System.Web.UI.WebControls.Button)ControlHelper.FindControlRecursive(Page, redirButtonResourceID);
            redirParams = (System.Web.UI.WebControls.HiddenField)ControlHelper.FindControlRecursive(Page, redirParamsResourceID);
            if (redirButton == null)
            {
                // for cross page post
                redirButton = new System.Web.UI.WebControls.Button();
                redirButton.ID = redirButtonResourceID;
                redirButton.PostBackUrl = "/Orion/SRM/RedirectGet.aspx";
                redirButton.UseSubmitBehavior = false;
                redirButton.Style.Add("display", "none");
                this.Controls.Add(redirButton);
                // for parameters in CPP
                redirParams = new System.Web.UI.WebControls.HiddenField();
                redirParams.ID = redirParamsResourceID;
                this.Controls.Add(redirParams);

                var formattedInitJS = javaScriptRedirectorInit.Replace("{0}", redirButton.ClientID)
                                                              .Replace("{1}", redirParams.ClientID)
                                                              .Replace("{2}", this.Page.Form.Name);
                OrionInclude.ModuleFile("SRM","Redirector.js", OrionInclude.Section.Top);
                OrionInclude.InlineJs(formattedInitJS);
            }
        }
    }
}