using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Web;
using CustomPropertyMgr = SolarWinds.Orion.Core.Common.CustomPropertyMgr;

public partial class Orion_SRM_Controls_CustomPropertyList : System.Web.UI.UserControl
{
	private const char separator = ',';
	private ResourceInfo resource;
	private int netObjectId;

    Log log = new Log();

    protected void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}

	public ResourceInfo Resource
	{
		get { return resource; }
		set { resource = value; }
	}

	public int NetObjectId
	{
		get { return netObjectId; }
		set { netObjectId = value; }
	}

    public Func<int, ICollection<string>, Dictionary<string, object>> CustomPropertyLoader { get; set; }
    public Func<int, string, string> EditCustomPropertiesLinkGenerator { get; set; }
    public string CustomPropertyTableName { get; set; }
    public string EntityName { get; set; }

	protected void Page_Load(object sender, EventArgs e)
	{
		if (this.netObjectId != 0)
		{
			IDictionary<string, object> customProperties = null;
			List<string> displayProperties = new List<string>();

            // load custom properties allowed for entity detail
            IEnumerable<string> entityDetailCustomProperties = null;
		    IDictionary<string, CustomProperty> customPropertiesList;

		    if (string.IsNullOrEmpty(EntityName))
		        customPropertiesList =
		            CustomPropertyMgr.GetCustomPropertiesForTable(CustomPropertyTableName)
		                .ToDictionary(it => it.PropertyName, it => it);
            else 
                customPropertiesList = 
		            CustomPropertyMgr.GetCustomPropertiesForEntity(EntityName)
		                .ToDictionary(it => it.PropertyName, it => it);

            entityDetailCustomProperties = customPropertiesList.Where(cp => cp.Value.IsUsageAllowed(CustomPropertyUsage.IsForEntityDetailName)).Select(cp => cp.Value.PropertyName);

			string propertyList = this.Resource.Properties["PropertyList"];
			if (!String.IsNullOrEmpty(propertyList))
			{
				foreach (string item in propertyList.Split(separator))
				{
					displayProperties.Add(item.Trim());
				}
                // only CPs that are allowed for entity detail page canbe displayed
			    displayProperties = displayProperties.Intersect(entityDetailCustomProperties).ToList();
			}
			else
			{
			    displayProperties.AddRange(entityDetailCustomProperties);
			}

			try
			{
                customProperties = CustomPropertyLoader(netObjectId, displayProperties);
                this.ManageLink.NavigateUrl = EditCustomPropertiesLinkGenerator(netObjectId, ReferrerRedirectorBase.GetReturnUrl());
			}
			catch (Exception exception)
			{
                log.Error(exception);
                this.netObjectError.Visible = true;
				return;
			}

			bool isIE6 = ((Request != null) && Request.Browser.Type.Equals("IE6", StringComparison.InvariantCultureIgnoreCase));

			SortedDictionary<string, string> showProperties = new SortedDictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

			if (customProperties == null)
			{
				//cannot be null
				//todo make it as literal to change error messsage
				this.netObjectError.Visible = true;
				return;
			}

            foreach (var kvp in customProperties)
			{
                string value = DefaultSanitizer.SanitizeHtml(kvp.Value)?.ToString();
                if (!String.IsNullOrEmpty(value))
				{
					// replace \n with <br/> for "memo" custom properties:
                    var propType = customPropertiesList[kvp.Key].PropertyType;
					if (propType == typeof(StringBuilder))
						value = FormatHelper.ConvertNewLinesToBr(value);

					string keyBreakable = FormatHelper.MakeBreakableString(kvp.Key, isIE6);
					string valueBreakable = FormatHelper.MakeBreakableString(value, isIE6);
				    showProperties[keyBreakable] = valueBreakable;
				}
			}
			this.CustomPropertyTable.DataSource = showProperties;
			this.CustomPropertyTable.DataBind();

            this.Manage.Visible = Profile.AllowNodeManagement && (customProperties.Count > 0) && (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer);
		}
		else
		{
			this.netObjectError.Visible = true;
		}
	}
}