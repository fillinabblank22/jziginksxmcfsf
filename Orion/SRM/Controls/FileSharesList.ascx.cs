﻿using System;
using System.Web.UI;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;

namespace Orion.SRM.Controls
{
    public partial class Orion_SRM_Controls_FileSharesList : UserControl
    {
        public ResourceSearchControl SearchControl { get; set; }
        public int ResourceID { get; set; }
        public string Where { get; set; }
        private string Query { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Query = InvariantString.Format(@"SELECT fs.Caption AS Name,
                                            concat('/Orion/StatusIcon.ashx?entity={1}&status=', ISNULL(fs.Volume.Status, 0)) AS [_IconFor_Name],
                                            '/Orion/View.aspx?NetObject={2}:' + ToString(fs.FileShareID) AS [_LinkFor_Name],
                                            h.Host AS Host, 
                                            fs.Path, 
                                            fs.Volume.CapacityTotal AS VolumeSize,
                                            IsNull(fs.Quota,-1) AS Quota,
                                            IsNull(fs.Used, fs.Volume.CapacityFileSystem) As Used,
                                            ROUND(CASE WHEN Quota IS NULL AND fs.Volume.CapacityTotal = 0 THEN 0 
                                                ELSE (IsNull(Used, fs.Volume.CapacityFileSystem)  * 1.0  / IsNull(Quota, fs.Volume.CapacityTotal )) END * 100, 3) 
                                                AS UsedPercent,
                                            IsNull(fs.Free, (CASE WHEN Quota IS NULL THEN fs.Volume.CapacityTotal ELSE Quota END) - IsNull(Used, fs.Volume.CapacityFileSystem)) As Free, 
                                            DayDiff(getUtcDate(), fs.Volume.CapacityRunout)  AS CapacityRunout
                                            FROM Orion.SRM.FileShares (nolock=true) as fs 
                                            Join 
                                            (SELECT FileShareID,
                                            CASE 
                                                WHEN Count(FileShares.FileServer.FileServerIdentifications.IPAddress) > 1 THEN ('Multiple') 
                                                WHEN Count(FileShares.FileServer.FileServerIdentifications.IPAddress) = 1 THEN (Max(FileShares.FileServer.FileServerIdentifications.IPAddress))
                                                ELSE ''
                                            END as Host
                                            FROM Orion.SRM.FileShares (nolock=true)
                                            GROUP by FileShareID) as h
                                            on fs.FileShareID = h.FileShareID
                                            {0}"
                , Where, SwisEntities.FileShare, FileShares.NetObjectPrefix);
            CustomTable.UniqueClientID = ParentResourceId;
            CustomTable.SWQL = Query;
            CustomTable.SearchSWQL = SearchSWQL;
            WarningThresholdDays = (int)SettingsDAL.GetSetting(SRMConstants.WarningThresholdDays).SettingValue;
            CriticalThresholdDays = (int)SettingsDAL.GetSetting(SRMConstants.CriticalThresholdDays).SettingValue;

        }

        protected int WarningThresholdDays { get; set; }
        protected int CriticalThresholdDays { get; set; }
        public int ParentResourceId { get; set; }

        public string SearchSWQL
        {
            get { return string.Concat(Query, " AND fs.Caption LIKE '%${SEARCH_STRING}%'"); }
        }
    }
}
