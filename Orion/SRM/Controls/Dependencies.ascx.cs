﻿using SolarWinds.Orion.Web;
using SolarWinds.SRM.Web.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_SRM_Controls_Dependencies : System.Web.UI.UserControl, ICreateEditControl, IEditControl
{
    List<string> supportedNetObjects = new List<string>(new[] { SolarWinds.SRM.Web.NetObjects.StorageArray.NetObjectPrefix });

    List<string> listOfNetObjectIDs;

    public IEditControl InitFromNetObjects(List<string> listOfNetObjectIDs)
    {
        // We need to support all passed in NetObject Prefixes
        var doesNotSupportOne = listOfNetObjectIDs.Exists(n => !supportedNetObjects.Contains(n.Split(':')[0]));
        if (doesNotSupportOne)
            return null;

        this.listOfNetObjectIDs = listOfNetObjectIDs;
        var distinctNetObjects = listOfNetObjectIDs.Select(n => n.Split(':')[0]).Distinct().ToList();
        if (distinctNetObjects.Count > 1)
            return null;

        var prefix = distinctNetObjects[0];
        var ids = listOfNetObjectIDs.Where(n => n.Split(':')[0] == prefix).Select(n => int.Parse(n.Split(':')[1])).ToList();
        dependenciesBox.ObjectPrefix = prefix;
        dependenciesBox.ObjectIds = ids;
        
        return this;        
    }

    public string ValidateJSFunction()
    {
        return null;
    }

    public bool Validate(bool connectionPropertyChanged)
    {
        return true;
    }

    public KeyValuePair<string, string>[] GetWarnings()
    {
        return null;
    }

    public void Update(bool connectionPropertyChanged)
    {
        // Just displaying
    }
}