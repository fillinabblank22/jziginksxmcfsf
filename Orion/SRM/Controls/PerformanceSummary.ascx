﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PerformanceSummary.ascx.cs" Inherits="Orion_SRM_Controls_PerformanceSummary" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Resources" %>
<%@ Register Src="~/Orion/SRM/Controls/Redirector.ascx" TagPrefix="srm" TagName="Redirector" %>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" Module="SRM" File="SRM.Common.js" />
<orion:Include runat="server" Module="SRM" File="Charts/Charts.SRM.PerformanceSummary.js" />
<orion:Include runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include runat="server" Module="SRM" File="PerformanceSummary.css" />
<orion:Include runat="server" Module="SRM" File="SRM.Common.js" />
<orion:Include runat="server" Module="SRM" File="Redirector.js" />

<srm:Redirector ID="Redirector1" runat="server" />
<div class="srm-lunperformancesummary-container">
    
    <asp:Panel ID="EntityInfo" runat="server" class="srm-performancesummary-entity-content">
        <span><b><%=EntityName %></b></span>
    </asp:Panel>

    <asp:Panel ID="ChartTitle" runat="server" class="srm-performancesummary-charttitle-content">
        <span><b><%=ChartTitleProperty %></b></span>
    </asp:Panel>

    <asp:Panel ID="ChartSubTitle" runat="server" class="srm-performancesummary-charttitle-content">
        <span><%=ChartSubTitleProperty %></span>
    </asp:Panel>

    <div id="subtitile<%= ResourceID%>" class="srm-lunperformancesummary-subtitle"></div>

    <!-- Dynamically calculated Attributes -->
    <style type="text/css">
        .srm-lunperformancesummary-main-chart {
            top: <%= ChartTopPosition%>px;
        }
    </style>

    <asp:Panel ID="chart" runat="server" class="srm-lunperformancesummary-main-chart">
    </asp:Panel>

    <asp:Panel ID="pnlIOPS" runat="server">
        <div>
            <div id="parent<%= ResourceID%>_IOPS" class="srm-lunperformancesummary-counter srm-performance-summary-iops-container">
                <table>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="srm-lunperformancesummary-detail-image">
                                        <span runat="server" id="IOPSExpander">
                                            <img id="expander<%= ResourceID%>_IOPS" onclick="chart_<%=ResourceID%>.expanderChangedHandler('IOPS');" src="/Orion/SRM/images/Button.Expand.gif" alt="[+]" />
                                        </span>
                                    </td>
                                    <td class="srm-lunperformancesummary-detail-text" data-drilldown-id="IOPS<%=ResourceID%>"><span title="<%= SrmWebContent.Lun_Performance_Summary_IOPS_Tooltip %>"><%=SrmWebContent.Lun_Performance_Summary_IOPS%></span></td>
                                    <td class="srm-lunperformancesummary-detail-unit"><span><%=SrmWebContent.Lun_Performance_Summary_IOPS_Unit%></span></td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <div></div>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="children<%= ResourceID%>_IOPS" class="srm-lunperformancesummary-detail">
                <div class="srm-lunperformancesummary-counter srm-lunperformancesummary-child-counter" runat="server" id="pnlIOPSRead">
                    <table>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="srm-lunperformancesummary-detail-image"></td>
                                        <td class="srm-lunperformancesummary-detail-text"><span></span></td>
                                        <td class="srm-lunperformancesummary-detail-unit"><span><%=SrmWebContent.Lun_Performance_Summary_IOPS_Read%></span></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <div></div>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="srm-lunperformancesummary-counter srm-lunperformancesummary-child-counter" runat="server" id="pnlIOPSWrite">
                    <table>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="srm-lunperformancesummary-detail-image"></td>
                                        <td class="srm-lunperformancesummary-detail-text"><span></span></td>
                                        <td class="srm-lunperformancesummary-detail-unit"><span><%=SrmWebContent.Lun_Performance_Summary_IOPS_Write%></span></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <div></div>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="srm-lunperformancesummary-counter srm-lunperformancesummary-child-counter" runat="server" id="pnlIOPSOther">
                    <table>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="srm-lunperformancesummary-detail-image"></td>
                                        <td class="srm-lunperformancesummary-detail-text"><span></span></td>
                                        <td class="srm-lunperformancesummary-detail-unit"><span><%=SrmWebContent.Lun_Performance_Summary_IOPS_Other%></span></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <div></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlLatency" runat="server" Visible="false">
        <div>
            <div id="parent<%= ResourceID%>_Latency" class="srm-lunperformancesummary-counter">
                <table>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="srm-lunperformancesummary-detail-image">
                                        <span runat="server" id="LatencyExpander">
                                            <img id="expander<%= ResourceID%>_Latency" onclick="chart_<%=ResourceID%>.expanderChangedHandler('Latency');" src="/Orion/SRM/images/Button.Expand.gif" alt="[+]" />
                                        </span>
                                    </td>
                                    <td class="srm-lunperformancesummary-detail-text" data-drilldown-id="Latency<%=ResourceID%>"><span title="<%= SrmWebContent.Lun_Performance_Summary_Latency_Tooltip %>"><%=SrmWebContent.Lun_Performance_Summary_Latency%></span></td>
                                    <td class="srm-lunperformancesummary-detail-unit"><span><%=SrmWebContent.Lun_Performance_Summary_Latency_Unit%></span></td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <div></div>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="children<%= ResourceID%>_Latency" class="srm-lunperformancesummary-detail">
                <div class="srm-lunperformancesummary-counter srm-lunperformancesummary-child-counter" runat="server" id="pnlLatencyRead">
                    <table>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="srm-lunperformancesummary-detail-image"></td>
                                        <td class="srm-lunperformancesummary-detail-text"><span></span></td>
                                        <td class="srm-lunperformancesummary-detail-unit"><span><%=SrmWebContent.Lun_Performance_Summary_Latency_Read%></span></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <div></div>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="srm-lunperformancesummary-counter srm-lunperformancesummary-child-counter" runat="server" id="pnlLatencyWrite">
                    <table>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="srm-lunperformancesummary-detail-image"></td>
                                        <td class="srm-lunperformancesummary-detail-text"><span></span></td>
                                        <td class="srm-lunperformancesummary-detail-unit"><span><%=SrmWebContent.Lun_Performance_Summary_Latency_Write%></span></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <div></div>
                            </td>
                        </tr>
                    </table>
                </div>
                
                <div class="srm-lunperformancesummary-counter srm-lunperformancesummary-child-counter" runat="server" id="pnlLatencyOther">
                    <table>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="srm-lunperformancesummary-detail-image"></td>
                                        <td class="srm-lunperformancesummary-detail-text"><span></span></td>
                                        <td class="srm-lunperformancesummary-detail-unit"><span><%=SrmWebContent.Lun_Performance_Summary_Latency_Other%></span></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <div></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlThroughput" runat="server" Visible="false">
        <div>
            <div id="parent<%= ResourceID%>_Throughput" class="srm-lunperformancesummary-counter">
                <table>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="srm-lunperformancesummary-detail-image">
                                        <span runat="server" id="ThroughputExpander">
                                            <img id="expander<%= ResourceID%>_Throughput" onclick="chart_<%=ResourceID%>.expanderChangedHandler('Throughput');" src="/Orion/SRM/images/Button.Expand.gif" alt="[+]" />
                                        </span>
                                    </td>
                                    <td class="srm-lunperformancesummary-detail-text" data-drilldown-id="Throughput<%=ResourceID%>"><span title="<%= SrmWebContent.Lun_Performance_Summary_Throughput_Tooltip %>"><%=SrmWebContent.Lun_Performance_Summary_Throughput%></span></td>
                                    <td class="srm-lunperformancesummary-detail-unit"><span><%=SrmWebContent.Lun_Performance_Summary_Throughput_Unit%></span></td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <div></div>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="children<%= ResourceID%>_Throughput"  class="srm-lunperformancesummary-detail">
                <div class="srm-lunperformancesummary-counter srm-lunperformancesummary-child-counter" runat="server" id="pnlThroughputRead">
                    <table>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="srm-lunperformancesummary-detail-image"></td>
                                        <td class="srm-lunperformancesummary-detail-text"><span></span></td>
                                        <td class="srm-lunperformancesummary-detail-unit"><span><%=SrmWebContent.Lun_Performance_Summary_Throughput_Read%></span></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <div></div>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="srm-lunperformancesummary-counter srm-lunperformancesummary-child-counter" runat="server" id="pnlThroughputWrite">
                    <table>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="srm-lunperformancesummary-detail-image"></td>
                                        <td class="srm-lunperformancesummary-detail-text"><span></span></td>
                                        <td class="srm-lunperformancesummary-detail-unit"><span><%=SrmWebContent.Lun_Performance_Summary_Throughput_Write%></span></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <div></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlIOSize" runat="server" Visible="false">
        <div>
            <div id="parent<%= ResourceID%>_IOSize" class="srm-lunperformancesummary-counter">
                <table>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="srm-lunperformancesummary-detail-image">
                                        <span runat="server" id="IOSizeExpander">
                                            <img id="expander<%= ResourceID%>_IOSize" onclick="chart_<%=ResourceID%>.expanderChangedHandler('IOSize');" src="/Orion/SRM/images/Button.Expand.gif" alt="[+]" />
                                        </span>
                                    </td>
                                    <td class="srm-lunperformancesummary-detail-text" data-drilldown-id="IOSize<%=ResourceID%>"><span title="<%= SrmWebContent.Lun_Performance_Summary_IOSize_Tooltip %>"><%=SrmWebContent.Lun_Performance_Summary_IOSize%></span></td>
                                    <td class="srm-lunperformancesummary-detail-unit"><span><%=SrmWebContent.Lun_Performance_Summary_IOSize_Unit%></span></td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <div></div>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="children<%= ResourceID%>_IOSize"  class="srm-lunperformancesummary-detail">
                <div class="srm-lunperformancesummary-counter srm-lunperformancesummary-child-counter" runat="server" id="pnlIOSizeRead">
                    <table>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="srm-lunperformancesummary-detail-image"></td>
                                        <td class="srm-lunperformancesummary-detail-text"><span></span></td>
                                        <td class="srm-lunperformancesummary-detail-unit"><span><%=SrmWebContent.Lun_Performance_Summary_IOSize_Read%></span></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <div></div>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="srm-lunperformancesummary-counter srm-lunperformancesummary-child-counter" runat="server" id="pnlIOSizeWrite">
                    <table>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="srm-lunperformancesummary-detail-image"></td>
                                        <td class="srm-lunperformancesummary-detail-text"><span></span></td>
                                        <td class="srm-lunperformancesummary-detail-unit"><span><%=SrmWebContent.Lun_Performance_Summary_IOSize_Write%></span></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <div></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlQueueLength" runat="server" Visible="false">
        <div>
            <div id="parent<%= ResourceID%>_QueueLength" class="srm-lunperformancesummary-counter">
                <table>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="srm-lunperformancesummary-detail-image"></td>
                                    <td class="srm-lunperformancesummary-detail-text" data-drilldown-id="QueueLength<%=ResourceID%>"><span><%=SrmWebContent.Lun_Performance_Summary_QueueLength%></span></td>
                                    <td class="srm-lunperformancesummary-detail-unit"><span><%=SrmWebContent.Lun_Performance_Summary_QueueLength_Unit%></span></td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <div></div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlIOPSRatio" runat="server" Visible="false">
        <div>
            <div class="srm-lunperformancesummary-counter">
                <table>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="srm-lunperformancesummary-detail-image"></td>
                                    <td class="srm-lunperformancesummary-detail-text" data-drilldown-id="IOPSRatio<%=ResourceID%>"><span title="<%= SrmWebContent.Lun_Performance_Summary_IOPSRatio_Tooltip %>"><%=SrmWebContent.Lun_Performance_Summary_IOPSRatio%></span></td>
                                    <td class="srm-lunperformancesummary-detail-unit"><span><%=SrmWebContent.Lun_Performance_Summary_IOPSRatio_Unit%></span></td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <div></div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlUtilization" runat="server" Visible="false">
        <div>
            <div class="srm-lunperformancesummary-counter">
                <table>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="srm-lunperformancesummary-detail-image"></td>
                                    <td class="srm-lunperformancesummary-detail-text" data-drilldown-id="Utilization<%=ResourceID%>"><span title="<%= SrmWebContent.Lun_Performance_Summary_Utilization_Tooltip %>"><%=SrmWebContent.Lun_Performance_Summary_Utilization%></span></td>
                                    <td class="srm-lunperformancesummary-detail-unit"><span><%=SrmWebContent.Lun_Performance_Summary_Utilization_Unit%></span></td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <div></div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlDiskBusy" runat="server" Visible="false">
        <div>
            <div class="srm-lunperformancesummary-counter">
                <table>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="srm-lunperformancesummary-detail-image"></td>
                                    <td class="srm-lunperformancesummary-detail-text" data-drilldown-id="DiskBusy<%=ResourceID%>"><span><%=SrmWebContent.Lun_Performance_Summary_DiskBusy%></span></td>
                                    <td class="srm-lunperformancesummary-detail-unit"><span><%=SrmWebContent.Lun_Performance_Summary_DiskBusy_Unit%></span></td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <div></div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
   
    <asp:Panel ID="pnlCacheHitRatio" runat="server" Visible="false">
        <div>
            <div id="parent<%= ResourceID%>_CacheHitRatio" class="srm-lunperformancesummary-counter">
                <table>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td class="srm-lunperformancesummary-detail-image">
                                        <span runat="server" id="CacheHitRatioExpander">
                                            <img id="expander<%= ResourceID%>_CacheHitRatio" onclick="chart_<%=ResourceID%>.expanderChangedHandler('CacheHitRatio');" src="/Orion/SRM/images/Button.Expand.gif" alt="[+]" />
                                        </span>
                                    </td>
                                    <td class="srm-lunperformancesummary-detail-text" data-drilldown-id="CacheHitRatio<%=ResourceID%>"><span title="<%= SrmWebContent.Lun_Performance_Summary_CacheHitRatio_Tooltip %>"><%=SrmWebContent.Lun_Performance_Summary_CacheHitRatio%></span></td>
                                    <td class="srm-lunperformancesummary-detail-unit"><span><%=SrmWebContent.Lun_Performance_Summary_CacheHitRatio_Unit%></span></td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <div></div>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="children<%= ResourceID%>_CacheHitRatio"  class="srm-lunperformancesummary-detail">
                <div class="srm-lunperformancesummary-counter srm-lunperformancesummary-child-counter" runat="server" id="RatioRead">
                    <table>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="srm-lunperformancesummary-detail-image"></td>
                                        <td class="srm-lunperformancesummary-detail-text"><span></span></td>
                                        <td class="srm-lunperformancesummary-detail-unit"><span><%=SrmWebContent.Lun_Performance_Summary_Ratio_Read%></span></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <div></div>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="srm-lunperformancesummary-counter srm-lunperformancesummary-child-counter" runat="server" id="RatioWrite">
                    <table>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="srm-lunperformancesummary-detail-image"></td>
                                        <td class="srm-lunperformancesummary-detail-text"><span></span></td>
                                        <td class="srm-lunperformancesummary-detail-unit"><span><%=SrmWebContent.Lun_Performance_Summary_Ratio_Write%></span></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <div></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </asp:Panel>

    <div class="srm-lunperformancesummary-scrollbar"></div>
</div>
<script type="text/javascript">
    var chart_<%=ResourceID%>;
    var detachedResource = '/Orion/SRM/Resources/PerformanceDrillDown/PerformanceDrillDownChart.ascx';
    var performanceServiceMethod = '/Orion/SRM/Services/PerformanceDrillDownService.asmx/GetPerformance';
    var resourceWidth = 800;

    $(function () {
        chart_<%=ResourceID%> = new SW.SRM.Charts.PerformanceSummary();
        chart_<%=ResourceID%>.init(<%=Data%>);
    });

    SW.SRM.ClickOnIOPS = function() {
        redirectToPage(
            detachedResource,
            '<%=CustomIOPSResourceTitle%>',
            resourceWidth,
            '<%=CustomIOPSChartTitle%>',
            '<%=SrmWebContent.PerformanceDrillDown_LineChartType%>',
            '<%=SrmWebContent.PerformanceDrillDown_IOPS_Unit%>',
            performanceServiceMethod,
            'IOPS'
        );
    };

    SW.SRM.ClickOnLatency = function() {
        redirectToPage(
            detachedResource,
            '<%=CustomLatencyResourceTitle%>',
            resourceWidth,
            '<%=CustomLatencyChartTitle%>',
            '<%=SrmWebContent.PerformanceDrillDown_LineChartType%>',
            '<%=SrmWebContent.PerformanceDrillDown_Latency_Unit%>',
            performanceServiceMethod,
            'Latency'
        );
    };

    SW.SRM.ClickOnThroughput = function() {
        redirectToPage(
            detachedResource,
            '<%=CustomThroughputResourceTitle%>',
            resourceWidth,
            '<%=CustomThroughputChartTitle%>',
            '<%=SrmWebContent.PerformanceDrillDown_LineChartType%>',
            '<%=SrmWebContent.PerformanceDrillDown_Throughput_Unit%>',
            performanceServiceMethod,
            'Throughput'
        );
    };

    SW.SRM.ClickOnIOSize = function() {
        redirectToPage(
            detachedResource,
            '<%=CustomIOSizeResourceTitle%>',
            resourceWidth,
            '<%=CustomIOSizeChartTitle%>',
            '<%=SrmWebContent.PerformanceDrillDown_LineChartType%>',
            '<%=SrmWebContent.PerformanceDrillDown_IOSize_Unit%>',
            performanceServiceMethod,
            'IOSize'
        );
    };

    SW.SRM.ClickOnQueueLength = function() {
        redirectToPage(
            detachedResource,
            '<%=CustomQueueLengthResourceTitle%>',
            resourceWidth,
            '<%=CustomQueueLengthChartTitle%>',
            '<%=SrmWebContent.PerformanceDrillDown_LineChartType%>',
            '<%=SrmWebContent.PerformanceDrillDown_QueueLength_Unit%>',
            performanceServiceMethod,
            'QueueLength'
        );
    };

    SW.SRM.ClickOnIOPSRatio = function() {
        redirectToPage(
            detachedResource,
            '<%=CustomIOPSRatioResourceTitle%>',
            resourceWidth,
            '<%=CustomIOPSRatioChartTitle%>',
            '<%=SrmWebContent.PerformanceDrillDown_AreaChartType%>',
            '<%=SrmWebContent.PerformanceDrillDown_IOPSRatio_Unit%>',
            performanceServiceMethod,
            'IOPSRatio'
        );
    };

    SW.SRM.ClickOnUtilization = function() {
        redirectToPage(
            detachedResource,
            '<%=CustomUtilizationResourceTitle%>',
            resourceWidth,
            '<%=CustomUtilizationChartTitle%>',
            '<%=SrmWebContent.PerformanceDrillDown_AreaChartType%>',
            '<%=SrmWebContent.PerformanceDrillDown_Utilization_Unit%>',
            performanceServiceMethod,
            'Utilization'
        );
    };

    SW.SRM.ClickOnDiskBusy = function() {
        redirectToPage(
            detachedResource,
            '<%=CustomDiskBusyResourceTitle%>',
            resourceWidth,
            '<%=CustomDiskBusyChartTitle%>',
            '<%=SrmWebContent.PerformanceDrillDown_AreaChartType%>',
            '<%=SrmWebContent.PerformanceDrillDown_DiskBusy_Unit%>',
            performanceServiceMethod,
            'DiskBusy'
        );
    };

    SW.SRM.ClickOnCacheHitRatio = function() {
        redirectToPage(
            detachedResource,
            '<%=CustomCacheHitRatioResourceTitle%>',
            resourceWidth,
            '<%=CustomCacheHitRatioChartTitle%>',
            '<%=SrmWebContent.PerformanceDrillDown_LineChartType%>',
            '<%=SrmWebContent.PerformanceDrillDown_CacheHitRatio_Unit%>',
            performanceServiceMethod,
            'CacheHitRatio'
        );
    };

    function redirectToPage(resourceFile, resourceTitle, width, chartTitle, chartType, yAxisTitle, dataBindMethod, requestParameter) {
        var args = {
            parameters: {
                ResourceFile: resourceFile,
                ResourceTitle: resourceTitle,
                Width: width,
                ChartTitle: chartTitle,
                ChartType: chartType,
                YAxisTitle: yAxisTitle,
                DataBindMethod: dataBindMethod,
                RequestParameter: requestParameter,
                <%=Constants.DaysOfDataToLoadPropertyKey%>: <%= DaysOfDataToLoad%>,
                <%=Constants.SampleSizeInMinutesPropertyKey%>:<%=SampleSizeInMinutes%>
            }
        };
        SW.SRM.Redirector.Redirect(args);
    };
    
    SW.SRM.Redirector.bindRedirectClick($('*[data-drilldown-id=\'IOPS<%=ResourceID%>\']'), SW.SRM.ClickOnIOPS);
    SW.SRM.Redirector.bindRedirectClick($('*[data-drilldown-id=\'Latency<%=ResourceID%>\']'), SW.SRM.ClickOnLatency);
    SW.SRM.Redirector.bindRedirectClick($('*[data-drilldown-id=\'Throughput<%=ResourceID%>\']'), SW.SRM.ClickOnThroughput);
    SW.SRM.Redirector.bindRedirectClick($('*[data-drilldown-id=\'IOSize<%=ResourceID%>\']'), SW.SRM.ClickOnIOSize);
    SW.SRM.Redirector.bindRedirectClick($('*[data-drilldown-id=\'QueueLength<%=ResourceID%>\']'), SW.SRM.ClickOnQueueLength);
    SW.SRM.Redirector.bindRedirectClick($('*[data-drilldown-id=\'IOPSRatio<%=ResourceID%>\']'), SW.SRM.ClickOnIOPSRatio);
    SW.SRM.Redirector.bindRedirectClick($('*[data-drilldown-id=\'Utilization<%=ResourceID%>\']'), SW.SRM.ClickOnUtilization);
    SW.SRM.Redirector.bindRedirectClick($('*[data-drilldown-id=\'DiskBusy<%=ResourceID%>\']'), SW.SRM.ClickOnDiskBusy);
    SW.SRM.Redirector.bindRedirectClick($('*[data-drilldown-id=\'CacheHitRatio<%=ResourceID%>\']'), SW.SRM.ClickOnCacheHitRatio);
</script>
