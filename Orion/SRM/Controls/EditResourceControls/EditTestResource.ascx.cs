﻿using System;
using System.Collections.Generic;
using System.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_SRM_Controls_EditResourceControls_EditTestResource : BaseResourceEditControl
{
    private const string TestResourceSettingKey = "TestResourceSetting";
    
    private Dictionary<string, object> _properties = new Dictionary<string, object>();

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        _properties.Add(TestResourceSettingKey, this.SettingCheckBox.Checked);

        bool settingValueBool = false;
        string settingValue = Resource.Properties[TestResourceSettingKey];

        
        if (!String.IsNullOrEmpty(settingValue))
        {
            Boolean.TryParse(settingValue, out settingValueBool);
        }

        this.SettingCheckBox.Checked = settingValueBool;
        
        if (HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            this.SettingCheckBox.Attributes.Add("onclick",
                String.Format("javascript:SaveData('{0}', this.checked ? 'True' : 'False');", TestResourceSettingKey));
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            _properties[TestResourceSettingKey] = this.SettingCheckBox.Checked;
            return _properties;
        }
    }
}
