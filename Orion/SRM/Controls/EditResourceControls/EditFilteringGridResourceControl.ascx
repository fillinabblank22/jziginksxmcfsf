﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditFilteringGridResourceControl.ascx.cs" Inherits="Orion_SRM_Controls_EditFilteringGridResourceControl" %>
<%@ Register TagPrefix="srm" TagName="PageSizeEditor"  Src="~/Orion/SRM/Controls/EditResourceControls/PageSizeEditor.ascx" %>

<srm:PageSizeEditor runat="server" ID="PageSizeEditor1" MinValueOfMaxCount="1" MaxValueOfMaxCount="50" />

<br />
<p>
    <b><%=Resources.SrmWebContent.FilterAllStorageObjects_subHeading %></b><br />
    <asp:TextBox runat="server" ID="SwqlFilter" Width="330"></asp:TextBox>
</p>

<p><%=Resources.SrmWebContent.FilterAllStorageObjects_text %></p>

<div style="font-size:8pt">
    <%=Resources.SrmWebContent.FilterAllStorageObjects_filterExamples %>
    <ul>
        <li><%=Resources.SrmWebContent.FilterAllStorageObjects_firstExample %></li>
        <li><%=Resources.SrmWebContent.FilterAllStorageObjects_secondExample %></li>
    </ul>
</div>