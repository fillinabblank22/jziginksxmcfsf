﻿using System;
using System.Linq;
using System.Text;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common;
using SolarWinds.SRM.Web.NetObjects;
using Volume = SolarWinds.SRM.Web.NetObjects.Volume;

public partial class Orion_SRM_Controls_EditResourceControls_EditCustomPropertyList : BaseResourceEditControl
{
	private readonly Dictionary<string, object> _properties = new Dictionary<string, object>();
	private const char separator = ',';

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		_properties.Add("PropertyList", string.Empty);

		string customPropertiesTableName = null;
        string customPropertiesEntityName = null;

        string NetObjectPrefix = NetObjectID.Split(new[] { ':' }, StringSplitOptions.None).FirstOrDefault();

	    switch (NetObjectPrefix)
	    {
            case StorageArray.NetObjectPrefix:
                customPropertiesEntityName = "Orion.SRM.StorageArrayCustomProperties";
                break;
            case Lun.NetObjectPrefix:
                customPropertiesEntityName = "Orion.SRM.LUNCustomProperties";
                break;
            case Pool.NetObjectPrefix:
                customPropertiesEntityName = "Orion.SRM.PoolCustomProperties";
                break;
            case Volume.NetObjectPrefix:
                customPropertiesEntityName = "Orion.SRM.VolumeCustomProperties";
                break;
            case StorageProvider.NetObjectPrefix:
                customPropertiesEntityName = "Orion.SRM.ProviderCustomProperties";
                break;
            case FileShares.NetObjectPrefix:
                customPropertiesEntityName = "Orion.SRM.FileShareCustomProperties";
                break;
            case VServer.NetObjectPrefix:
                customPropertiesEntityName = "Orion.SRM.VServersCustomProperties";
                break;
	    }

        IEnumerable<CustomProperty> customProperties = null;
	    if (!String.IsNullOrEmpty(customPropertiesEntityName))
	    {
	        customProperties = from cp in CustomPropertyMgr.GetCustomPropertiesForEntity(customPropertiesEntityName)
	            where cp.IsUsageAllowed(CustomPropertyUsage.IsForEntityDetailName)
	            orderby cp.PropertyName
	            select cp;
	    }
	    else if (!String.IsNullOrEmpty(customPropertiesTableName))
        {
            customProperties = from cp in CustomPropertyMgr.GetCustomPropertiesForTable(customPropertiesTableName)
                               where cp.IsUsageAllowed(CustomPropertyUsage.IsForEntityDetailName)
                               orderby cp.PropertyName
                               select cp;
        }
	    else
	    {
	        throw new ApplicationException(String.Format(CultureInfo.InvariantCulture, "TableName or EntityName aren't defined for NetObjectID '{0}'", NetObjectID));
	    }

		if (customProperties.Count() > 0)
		{
			this.selectedProperties.DataSource = customProperties;
			this.selectedProperties.DataTextField = "PropertyName";
			this.selectedProperties.DataValueField = "PropertyName";
			this.selectedProperties.DataBind();
			return;
		}
		this.noPropertiesMessage.Visible = true;
		this.propertiesList.Visible = false;
	}

	protected void SelectedProperties_DataBound(object sender, EventArgs e)
	{
		if (!Resource.Properties.ContainsKey("PropertyList"))
		{
			// by default all properties should be selected
			foreach (ListItem item in this.selectedProperties.Items)
				item.Selected = true;
		}
		else
		{
			string properties = Resource.Properties["PropertyList"];
			if (!String.IsNullOrEmpty(properties))
				foreach (string property in (properties.Split(separator)))
				{
					ListItem item = this.selectedProperties.Items.FindByValue(property.Trim());
					if (item != null)
						item.Selected = true;
				}
		}

        if (System.Web.HttpContext.Current.Request.Url.AbsolutePath.Contains("EditCustomObjectResource.aspx"))
        {
            StringBuilder value = new StringBuilder();
            var plus = "";
            for (var i =0; i< this.selectedProperties.Items.Count; i++)
            {
                value.AppendFormat("{1}(this.rows[{0}].children[0].children[0].checked ? this.rows[{0}].children[0].children[1].innerHTML + ',' : '')", i, plus);
                plus = "+";
            }
            selectedProperties.Attributes.Add("onclick", "javascript:SaveData('PropertyList', " + value + ");");
        }
	}

	public override Dictionary<string, object> Properties
	{
		get 
		{
			List<string> selected = new List<string>();
			foreach (ListItem item in this.selectedProperties.Items)
			{
				if (item.Selected)
				{
					selected.Add(item.Value);
				}
			}
			_properties["PropertyList"] = String.Join(separator.ToString(), selected.ToArray());

			return _properties;
		}
	}
}
