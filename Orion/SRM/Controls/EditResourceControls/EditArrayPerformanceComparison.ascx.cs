﻿using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Resources;
using System;
using System.Collections.Generic;
using System.Globalization;

public partial class Orion_SRM_Controls_EditResourceControls_EditArrayPerformanceComparison : BaseResourceEditControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!this.IsPostBack)
        {
            txtDaysOfDataToLoad.Text = Resource.Properties[ArrayPerformanceComparison.DaysOfDataToLoadPropertyKey] ?? ArrayPerformanceComparison.DaysOfDataToLoadDefault.ToString(CultureInfo.CurrentUICulture);
            if (WebHelper.IsCustomObjectResource())
            {
                txtDaysOfDataToLoad.Attributes.Add("onchange", "SaveData('" + ArrayPerformanceComparison.DaysOfDataToLoadPropertyKey + "', this.value);");
            }

            txtSampleSizeInMinutes.Text = Resource.Properties[ArrayPerformanceComparison.SampleSizeInMinutesPropertyKey] ?? ArrayPerformanceComparison.SampleSizeInMinutesDefault.ToString(CultureInfo.CurrentUICulture);
            if (WebHelper.IsCustomObjectResource())
            {
                txtSampleSizeInMinutes.Attributes.Add("onchange", "SaveData('" + ArrayPerformanceComparison.SampleSizeInMinutesPropertyKey + "', this.value);");
            }
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            Int32 daysOfDataToLoadDefault;
            if (!Int32.TryParse(txtDaysOfDataToLoad.Text, out daysOfDataToLoadDefault))
                daysOfDataToLoadDefault = ArrayPerformanceComparison.DaysOfDataToLoadDefault;

            properties.Add(ArrayPerformanceComparison.DaysOfDataToLoadPropertyKey, daysOfDataToLoadDefault);


            Int32 sampleSizeInMinutesDefault;
            if (!Int32.TryParse(txtSampleSizeInMinutes.Text, out sampleSizeInMinutesDefault))
                sampleSizeInMinutesDefault = ArrayPerformanceComparison.SampleSizeInMinutesDefault;

            properties.Add(ArrayPerformanceComparison.SampleSizeInMinutesPropertyKey, sampleSizeInMinutesDefault);

            return properties;
        }
    }
}