﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditTestResource.ascx.cs" Inherits="Orion_SRM_Controls_EditResourceControls_EditTestResource" EnableViewState="true" %>

<table border="0">
    <tr>
        <td style="text-align: right">
            <asp:Label runat="server" ID="FakeSettingLabel" Text="<%$ Resources: SrmWebContent, Fake_Setting_Name%>" Font-Bold="true" />
        </td>
        <td style="padding-left: 10px">
            <asp:CheckBox ID="SettingCheckBox" runat="server" />
        </td>
    </tr>
</table>