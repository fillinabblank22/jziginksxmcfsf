﻿using System.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.SRM.Web;
using System.Collections.Generic;

public partial class EditHistogramChartControl : UserControl
{
    private ChartResourceSettings _chartResourceSettings;
    private ChartSettingsDAL _settingsDal;

    private string PropertyKey { get; set; }

    public void Initialize(ResourceInfo resource, Dictionary<string, string> dropDownValues, string propertyKey, string defaultValue)
    {
        Initialize(resource, dropDownValues, propertyKey, defaultValue, true);
    }

    public void Initialize(ResourceInfo resource, Dictionary<string, string> dropDownValues, string propertyKey, string defaultValue, bool hasAdvancedSection)
    {
        _settingsDal = new ChartSettingsDAL();
        _chartResourceSettings = ChartResourceSettings.FromResource(resource, _settingsDal);

        PropertyKey = propertyKey;
        
        if (!this.IsPostBack)
        {
            ddlDefaultZoom.DataSource = dropDownValues;
            ddlDefaultZoom.DataBind();

            ddlDefaultZoom.SelectedValue = resource.Properties[PropertyKey] ?? defaultValue;
            ChartTitle.Text = _chartResourceSettings.Title;
            ChartSubtitle.Text = _chartResourceSettings.SubTitle;

            if (WebHelper.IsCustomObjectResource())
            {
                ddlDefaultZoom.Attributes.Add("onchange", "SaveData('" + PropertyKey + "', this.options[this.selectedIndex].value);");
            }
        }

        AdvancedPanel.Visible = hasAdvancedSection;
    }

    public Dictionary<string, object> Properties
    {
        get
        {
            var properties = new Dictionary<string, object>();

            properties.Add(PropertyKey, ddlDefaultZoom.SelectedValue);
            properties["ShowTitle"] = ChartTitle.Text.Length > 0 || ChartSubtitle.Text.Length > 0 ? "1" : "0";
            properties["ChartTitle"] = ChartTitle.Text;
            properties["ChartSubTitle"] = ChartSubtitle.Text;

            return properties;
        }
    }
}