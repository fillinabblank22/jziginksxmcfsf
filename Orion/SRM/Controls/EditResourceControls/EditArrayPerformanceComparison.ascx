﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditArrayPerformanceComparison.ascx.cs" Inherits="Orion_SRM_Controls_EditResourceControls_EditArrayPerformanceComparison" %>

<b><%=Resources.SrmWebContent.PerformanceEdit_DaysOfDataToLoad %>:<br /></b>
<asp:TextBox runat="server" ID="txtDaysOfDataToLoad" />
<br />
<br />
<b><%=Resources.SrmWebContent.PerformanceEdit_SampleSizeInMinutes %>:<br /></b>
<asp:TextBox runat="server" ID="txtSampleSizeInMinutes" />