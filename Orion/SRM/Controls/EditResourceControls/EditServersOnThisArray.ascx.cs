﻿using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.DAL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;

public partial class Orion_SRM_Controls_EditResourceControls_EditAllStorageObjects : BaseResourceEditControl
{
    private const string FamilyDirecotryConst = "FamilyDirectory";
    private const string NameConst = "Name";
    private const string StatusConst = "Status"; 

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        rememberStateCheckbox.Text = Resources.SrmWebContent.SummaryDetails_EditStorages_RememberExpandedStateLabel;
        expandRootLevelCheckbox.Text = Resources.SrmWebContent.SummaryDetails_EditStorages_ExpandRootLevelLabel;

        this.orderByList.Items.Add(new ListItem(Resources.SrmWebContent.EditAllStorageObjects_Name, NameConst));
        this.orderByList.Items.Add(new ListItem(Resources.SrmWebContent.EditAllStorageObjects_Status, StatusConst));

        this.rememberStateCheckbox.Checked = Boolean.Parse(Resource.Properties[ResourcePropertiesKeys.RememberCollapseState] ?? "true");
        if (WebHelper.IsCustomObjectResource())
        {
            rememberStateCheckbox.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.RememberCollapseState + "', this.value);");
        }
        this.expandRootLevelCheckbox.Checked = Boolean.Parse(Resource.Properties[ResourcePropertiesKeys.ExpandRootLevel] ?? "false");
        if (WebHelper.IsCustomObjectResource())
        {
            expandRootLevelCheckbox.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.ExpandRootLevel + "', this.value);");
        }
        this.orderByList.SelectedValue = Resource.Properties[ResourcePropertiesKeys.Ordering];
        if (WebHelper.IsCustomObjectResource())
        {
            orderByList.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.Ordering + "', this.value);");
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            // if grouping or ordering changed, clear remembered expansion state because it's not valid anymore
            if (Resource.Properties.ContainsKey(ResourcePropertiesKeys.Ordering) &&
                Resource.Properties[ResourcePropertiesKeys.Ordering] != this.orderByList.SelectedValue)
            {
                TreeStateManager manager = new TreeStateManager(Context.Session, Resource.ID);
                manager.Clear();
            }

            properties[ResourcePropertiesKeys.RememberCollapseState] = this.rememberStateCheckbox.Checked.ToString(CultureInfo.InvariantCulture);
            properties[ResourcePropertiesKeys.ExpandRootLevel] = this.expandRootLevelCheckbox.Checked.ToString(CultureInfo.InvariantCulture);
            properties[ResourcePropertiesKeys.Ordering] = this.orderByList.SelectedValue;

            // We changed stuff so clear out the expanded tree node information
            if (!this.rememberStateCheckbox.Checked)
            {
                TreeStateManager manager = new TreeStateManager(Context.Session, Resource.ID);
                manager.Clear();
            }

            return properties;
        }
    }
}