﻿using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web;
using System;
using System.Collections.Generic;

public partial class Orion_SRM_Controls_EditFilteringGridResourceControl : BaseResourceEditControl
{
    public Orion_SRM_Controls_EditFilteringGridResourceControl()
    {
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        SwqlFilter.Text = Resource.Properties[ResourcePropertiesKeys.SwqlFilter] ?? "";
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            var properties = new Dictionary<string, object> { { ResourcePropertiesKeys.SwqlFilter, SwqlFilter.Text } };
            PageSizeEditor1.FillProperties(properties);

            return properties;
        }
    }
}