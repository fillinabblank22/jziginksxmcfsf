﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditLunLatencyPerformance.ascx.cs" Inherits="Orion_SRM_Controls_EditResourceControls_EditLunLatencyPerformance" %>

<b><%=Resources.SrmWebContent.LunLatencyPerformanceEdit_DaysOfDataToLoad %>:<br /></b>
<asp:TextBox runat="server" ID="txtDaysOfDataToLoad" />
<br />
<br />
<b><%=Resources.SrmWebContent.LunLatencyPerformanceEdit_SampleSizeInMinutes %>:<br /></b>
<asp:TextBox runat="server" ID="txtSampleSizeInMinutes" />