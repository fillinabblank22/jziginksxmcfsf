﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PageSizeEditor.ascx.cs" Inherits="Orion_SRM_Controls_EditResourceControls_PageSizeEditor" %>

<b><%=Resources.SrmWebContent.PageSizeEditor_SpecifyPageSize %></b><br /><br />
<%=Resources.SrmWebContent.PageSizeEditor_PageSize %>:<br />
<asp:TextBox runat="server" ID="txtPageSize" />
<asp:RangeValidator ID="vldWarning" runat="server" ControlToValidate="txtPageSize" EnableClientScript="true"></asp:RangeValidator><br />
