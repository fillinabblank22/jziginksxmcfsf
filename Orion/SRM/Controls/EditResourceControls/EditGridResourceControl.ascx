﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditGridResourceControl.ascx.cs" Inherits="Orion_SRM_Controls_EditResourceControls_EditGridResourceControl" %>
<%@ Register TagPrefix="srm" TagName="PageSizeEditor"  Src="~/Orion/SRM/Controls/EditResourceControls/PageSizeEditor.ascx" %>

<srm:PageSizeEditor runat="server" ID="PageSizeEditor1" MinValueOfMaxCount="1" MaxValueOfMaxCount="50" />
