﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;

using ASP;

using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

/// <summary>
/// This file is copied from EditTopXX from core.
/// </summary>
public partial class Orion_SRM_Controls_EditResourceControls_EditTopXX : BaseResourceEditControl
{
    private const string MAX_RECORDS_PROPERTY_NAME = "MaxRecords";
    private const int DEFAULT_VALUE_OF_MAX_COUNT = 10;
    private const int MIN_VALUE_OF_MAX_COUNT = 1;
    private const int MAX_VALUE_OF_MAX_COUNT = 50;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.NetObjectType = GetLocalizedProperty("Entity", this.Resource.Properties["Type"]);

        if (!this.IsPostBack)
        {
            vldWarning.ErrorMessage = String.Format(CultureInfo.CurrentUICulture, Resources.SrmWebContent.TopXXEdit_RangeValidationMessage, MIN_VALUE_OF_MAX_COUNT, MAX_VALUE_OF_MAX_COUNT);
            vldWarning.MinimumValue = MIN_VALUE_OF_MAX_COUNT.ToString(CultureInfo.CurrentUICulture);
            vldWarning.MaximumValue = MAX_VALUE_OF_MAX_COUNT.ToString(CultureInfo.CurrentUICulture);
            vldWarning.Type = ValidationDataType.Integer;

            int max;
            if (!Int32.TryParse(this.Resource.Properties[MAX_RECORDS_PROPERTY_NAME], out max))
            {
                this.Resource.Properties[MAX_RECORDS_PROPERTY_NAME] = DEFAULT_VALUE_OF_MAX_COUNT.ToString(CultureInfo.CurrentUICulture);
            }
            this.maxCount.Text = this.Resource.Properties[MAX_RECORDS_PROPERTY_NAME];
            this.maxCount.Attributes.Add("onchange", "SaveData('" + MAX_RECORDS_PROPERTY_NAME + "', this.value);");
        }
    }

    public override Dictionary<string, object> Properties
	{
		get
		{
			Dictionary<string, object> properties = new Dictionary<string, object>();

            var titleEditControl = ControlHelper.FindControlRecursive(this.Page, "TitleEditControl");
            if (titleEditControl != null)
            {
                properties["Title"] = ((EditResourceTitle)titleEditControl).ResourceTitle;
                properties["SubTitle"] = ((EditResourceTitle)titleEditControl).ResourceSubTitle;
            }
            properties[MAX_RECORDS_PROPERTY_NAME] = this.maxCount.Text;

		    return properties;
		}
	}

    private string netObjectType;

    protected string NetObjectType
    {
        get { return this.netObjectType; }
        set { this.netObjectType = value; }
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}
