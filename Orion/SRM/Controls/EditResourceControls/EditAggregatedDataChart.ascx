﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditAggregatedDataChart.ascx.cs" Inherits="EditHistogramChartControl" %>

<style type="text/css">
    .sw-res-editor .sectionHeader {
        font-weight: bold;
        padding-top: 20px;
        padding-bottom: 8px;
    }
    
    .advanced {
        margin-left: 20px;
        padding: 20px;
        background-color: #e4f1f8;
        width: 550px;
        display: none;
    }
</style>

<script type="text/javascript">

    (function () {

        $(function () {
            $('#advancedBtn').click(function () {
                var btn = $('#advancedBtn');
                var isExpanded = btn.attr('data-expanded') == 1;

                if (isExpanded) {
                    // collapse
                    btn.attr('data-expanded', 0);
                    btn.attr('src', '/Orion/images/Button.Expand.gif');
                    $('.advanced').slideUp();
                } else {
                    // expand
                    btn.attr('data-expanded', 1);
                    btn.attr('src', '/Orion/images/Button.Collapse.gif');
                    $('.advanced').slideDown();
                }
            });
        });
    })();

</script>

<b><%=Resources.SrmWebContent.LatencyHistogramEdit_DefaultZoom %></b><br /><br />
<%=Resources.SrmWebContent.LatencyHistogramEdit_DefaultZoomLabel %>:<br />
<asp:DropDownList runat="server" ID="ddlDefaultZoom" DataValueField="Key" DataTextField="Value"/>

<div runat="server" ID="AdvancedPanel">
<div runat="server" ID="SectionAdvanced" class="sectionHeader">
    <img src="/Orion/images/Button.Expand.gif" id="advancedBtn" data-expanded="0" alt=""/> <%= Resources.CoreWebContent.WEBDATA_TM0_216%>
</div>

<div class="advanced">
    <%= Resources.CoreWebContent.WEBDATA_IB0_119%>
    <div class="sw-res-editor-row">
        <asp:TextBox runat="server" ID="ChartTitle"></asp:TextBox>

    </div>

    <%= Resources.CoreWebContent.WEBDATA_IB0_120%>
    <div class="sw-res-editor-row">
        <asp:TextBox runat="server" ID="ChartSubtitle"></asp:TextBox>
    </div>
</div>
</div>