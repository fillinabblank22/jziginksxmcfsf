﻿using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Resources;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;

public partial class Orion_SRM_Controls_EditResourceControls_EditBlockCapacitySummary : BaseResourceEditControl
{
    private const int MIN_VALUE_OF_MAX_COUNT = 1;
    private const int MAX_VALUE_OF_MAX_COUNT = 50;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        vldWarning.ErrorMessage = String.Format(CultureInfo.CurrentUICulture, Resources.SrmWebContent.BlockCapacitySummaryEdit_RangeValidationMessage, MIN_VALUE_OF_MAX_COUNT, MAX_VALUE_OF_MAX_COUNT);
        vldWarning.MinimumValue = MIN_VALUE_OF_MAX_COUNT.ToString();
        vldWarning.MaximumValue = MAX_VALUE_OF_MAX_COUNT.ToString();
        vldWarning.Type = ValidationDataType.Integer;

        txtPageSize.Text = Resource.Properties[BlockCapacitySummary.PageSizePropertyKey] ?? BlockCapacitySummary.PageSizeDefault.ToString(CultureInfo.CurrentUICulture);
        if (WebHelper.IsCustomObjectResource())
        {
            txtPageSize.Attributes.Add("onchange", "SaveData('" + BlockCapacitySummary.PageSizePropertyKey + "', this.value);");
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            Int32 pageSize;
            if (!Int32.TryParse(txtPageSize.Text, out pageSize))
                pageSize = BlockCapacitySummary.PageSizeDefault;

            properties.Add(BlockCapacitySummary.PageSizePropertyKey, pageSize);

            SrmSessionManager.SetBlockCapacitySummaryLoadedCount(Resource.ID, pageSize);

            return properties;
        }
    }
}