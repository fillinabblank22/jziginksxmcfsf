﻿using System;
using System.Linq;

using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;

using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Web.Enums;
using SolarWinds.SRM.Web.Resources;

public partial class Orion_SRM_Controls_EditResourceControls_EditArrayStatusHistogram : BaseResourceEditControl
{
    protected override void OnInit(EventArgs e)
    {
        editControl.Initialize(Resource, new[] {TimeRangeEnum.OneHour, TimeRangeEnum.TwelveHours, TimeRangeEnum.TwentyFourHours}.ToDictionary(x => x.GetDescription(), x => x.GetLocalizedDescription()), ArrayPerformanceComparison.ZoomValuePropertyKey, ArrayPerformanceComparison.ZoomDefault, false);
    }
    
    public override Dictionary<string, object> Properties
    {
        get
        {
            return editControl.Properties;
        }
    }
}