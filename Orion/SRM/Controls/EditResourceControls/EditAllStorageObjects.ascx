﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditAllStorageObjects.ascx.cs" Inherits="Orion_SRM_Controls_EditResourceControls_EditAllStorageObjects" %>
<%@ Register TagPrefix="srm" TagName="PageSizeEditor"  Src="~/Orion/SRM/Controls/EditResourceControls/PageSizeEditor.ascx" %>

<br />
<b><%=Resources.SrmWebContent.EditAllStorageObjects_GroupByHeader %></b>

<br />
<%=Resources.SrmWebContent.EditAllStorageObjects_GroupByHint %>
<p>
    <%=Resources.SrmWebContent.EditAllStorageObjects_GroupLevel1 %><br />
    <asp:DropDownList ID="groupByLvl1" runat="server" Width="183" />
</p>
<p>
    <%=Resources.SrmWebContent.EditAllStorageObjects_GroupLevel2 %><br />
    <asp:DropDownList ID="groupByLvl2" runat="server" Width="183" />
</p>

<asp:CompareValidator id="groupCompareValidator" 
                      ControlToValidate="groupByLvl1" 
                      ControlToCompare="groupByLvl2"
                      Type="String" 
                      operator="NotEqual"
                      runat="server"/>

<br />
<p>
    <b><%=Resources.SrmWebContent.EditAllStorageObjects_OrderBy %></b><br />
    <asp:DropDownList ID="orderByList" runat="server" Width="183" />
</p>

<p> 
    <asp:CheckBox runat="server" ID="rememberStateCheckbox" />
</p>

<p> 
    <asp:CheckBox runat="server" ID="expandRootLevelCheckbox" />
</p>

<br />
<p>
    <srm:PageSizeEditor runat="server" ID="PageSizeEditor" MinValueOfMaxCount="1" MaxValueOfMaxCount="50" DefaultPageSize="10" />
</p>

<br />
<p>
    <b><%=Resources.SrmWebContent.EditAllStorageObjects_FilterAllTitle %></b><br />
    <asp:TextBox runat="server" ID="SwqlFilter" Width="330"></asp:TextBox>
</p>

<p><%=Resources.SrmWebContent.EditAllStorageObjects_FilterAllDescription %></p>

<div style="font-size:8pt">
    <%=Resources.SrmWebContent.EditAllStorageObjects_FilterAllExample %>
    <ul>
        <li>StorageArray.Name = 'NFCXTREMIO'</li>
        <li>Lun.UserCaption like '%ESX%'</li>
        <li>Pool.Status > 0</li>
        <li>StorageArray.CustomProperties.PropertyName = 'property value'</li>
    </ul>
</div>