﻿using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Enums;
using SolarWinds.SRM.Web.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

public partial class Orion_SRM_Controls_EditResourceControls_EditLatencyHistogram : BaseResourceEditControl
{
    protected override void OnInit(EventArgs e)
    {
        editControl.Initialize(Resource, new[] { DataTimeRangeEnum.DataLastPoll, DataTimeRangeEnum.DataOneHour, DataTimeRangeEnum.DataTwelveHours, DataTimeRangeEnum.DataTwentyFourHours }.ToDictionary(x => x.GetDescription(), x => x.GetLocalizedDescription()), LatencyHistogram.DefaultZoomPropertyKey, LatencyHistogram.DefaultZoomDefaultValue);
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            return editControl.Properties;
        }
    }
}