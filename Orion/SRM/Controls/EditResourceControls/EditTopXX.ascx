﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditTopXX.ascx.cs" Inherits="Orion_SRM_Controls_EditResourceControls_EditTopXX" %>
<%@ Register TagPrefix="orion" TagName="TitleEdit" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

    <div>
        <b><%= String.Format(Resources.CoreWebContent.WEBDATA_TM0_56, HttpUtility.HtmlEncode(this.NetObjectType)) %></b><br />
        <asp:TextBox runat="server" ID="maxCount" Columns="5"/>
        <asp:RangeValidator ID="vldWarning" runat="server" ControlToValidate="maxCount" EnableClientScript="true"></asp:RangeValidator>
    </div>
