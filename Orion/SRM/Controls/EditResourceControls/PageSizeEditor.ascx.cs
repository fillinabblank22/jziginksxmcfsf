﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Resources;

public partial class Orion_SRM_Controls_EditResourceControls_PageSizeEditor : UserControl
{
    public int MinValueOfMaxCount { get; set; }
    public int MaxValueOfMaxCount { get; set; }
    public int DefaultPageSize { get; set; }

    public BaseResourceEditControl ParentResource
    {
        get
        {
            return WebHelper.FindParentControl(this, parent => parent is BaseResourceEditControl) as BaseResourceEditControl;
        }
    }

    public Orion_SRM_Controls_EditResourceControls_PageSizeEditor()
    {
        DefaultPageSize = PageSizeHelper.DefaultPageSize;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        vldWarning.ErrorMessage = String.Format(CultureInfo.CurrentUICulture, SrmWebContent.PageSizeEditor_RangeValidationMessage, MinValueOfMaxCount, MaxValueOfMaxCount);
        vldWarning.MinimumValue = MinValueOfMaxCount.ToString();
        vldWarning.MaximumValue = MaxValueOfMaxCount.ToString();
        vldWarning.Type = ValidationDataType.Integer;

        txtPageSize.Text = ParentResource.Resource.Properties[PageSizeHelper.PropertyKey] ?? DefaultPageSize.ToString(CultureInfo.CurrentUICulture);
        if (WebHelper.IsCustomObjectResource())
        {
            txtPageSize.Attributes.Add("onchange", "SaveData('" + PageSizeHelper.PropertyKey + "', this.value);");
        }
    }

    public void FillProperties(Dictionary<string, object> properties)
    {
        Int32 pageSize;
        if (!Int32.TryParse(txtPageSize.Text, out pageSize))
            pageSize = DefaultPageSize;

        properties.Add(PageSizeHelper.PropertyKey, pageSize);
    }
}