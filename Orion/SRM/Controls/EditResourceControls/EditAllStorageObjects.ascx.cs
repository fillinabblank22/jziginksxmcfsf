﻿using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.DAL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;
using Castle.Core.Internal;
using SolarWinds.SRM.Web.Resources;
using System.Collections;
using System.Diagnostics;
using System.Linq;
using SolarWinds.Orion.Core.Common;
using SolarWinds.SRM.Common.Helpers;

public partial class Orion_SRM_Controls_EditResourceControls_EditAllStorageObjects : BaseResourceEditControl
{
    private const string VendorDirecotryConst = "Vendor";
    private const string ModelDirecotryConst = "Model";
    private const string DefaultGroupByOption = VendorDirecotryConst;
    private const string NameConst = "Name";
    private const string StatusConst = "Status"; 

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        rememberStateCheckbox.Text = Resources.SrmWebContent.SummaryDetails_EditStorages_RememberExpandedStateLabel;
        expandRootLevelCheckbox.Text = Resources.SrmWebContent.SummaryDetails_EditStorages_ExpandRootLevelLabel;

        // probably can be considered as deprecated because of SRM-14374 
        SwisMetadataDAL dal = new SwisMetadataDAL();
        foreach (SwisMetadataDAL.EntityProperty property in dal.GetEntityProperties(SwisEntities.StorageArrays, SwisMetadataDAL.PropertyType.GroupBy))
        {
            this.groupByLvl1.Items.Add(new ListItem(property.DisplayName, property.QualifiedName));
        }

        FillGroupByControl(groupByLvl1);
        FillGroupByControl(groupByLvl2);
        SelectGroupByControls(groupByLvl1, groupByLvl2);
        groupCompareValidator.Text = Resources.SrmWebContent.EditAllStorageObjects_IdenticalGroupingsValidationMessage;

        this.orderByList.Items.Add(new ListItem(Resources.SrmWebContent.EditAllStorageObjects_Name, NameConst));
        this.orderByList.Items.Add(new ListItem(Resources.SrmWebContent.EditAllStorageObjects_Status, StatusConst));

        this.rememberStateCheckbox.Checked = Boolean.Parse(Resource.Properties[ResourcePropertiesKeys.RememberCollapseState] ?? "true");
        if (WebHelper.IsCustomObjectResource())
        {
            rememberStateCheckbox.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.RememberCollapseState + "', this.value);");
        }
        this.expandRootLevelCheckbox.Checked = Boolean.Parse(Resource.Properties[ResourcePropertiesKeys.ExpandRootLevel] ?? "false");
        if (WebHelper.IsCustomObjectResource())
        {
            expandRootLevelCheckbox.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.ExpandRootLevel + "', this.value);");
        }
        this.groupByLvl1.SelectedValue = Resource.Properties[ResourcePropertiesKeys.Grouping] ?? "VendorDirectory";
        if (WebHelper.IsCustomObjectResource())
        {
            groupByLvl1.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.Grouping + "', this.value);");
        }
        this.orderByList.SelectedValue = Resource.Properties[ResourcePropertiesKeys.Ordering];
        if (WebHelper.IsCustomObjectResource())
        {
            orderByList.Attributes.Add("onchange", "SaveData('" + ResourcePropertiesKeys.Ordering + "', this.value);");
        }

        SwqlFilter.Text = Resource.Properties[ResourcePropertiesKeys.SwqlFilter] ?? "";
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            // if grouping or ordering changed, clear remembered expansion state because it's not valid anymore
            if ((Resource.Properties.ContainsKey(ResourcePropertiesKeys.Grouping) &&
                Resource.Properties[ResourcePropertiesKeys.Grouping] != groupByLvl1.SelectedValue)
             || (Resource.Properties.ContainsKey(ResourcePropertiesKeys.Grouping) &&
                Resource.Properties[ResourcePropertiesKeys.Grouping] != groupByLvl2.SelectedValue)
             || (Resource.Properties.ContainsKey(ResourcePropertiesKeys.Ordering) &&
                Resource.Properties[ResourcePropertiesKeys.Ordering] != orderByList.SelectedValue))
            {
                TreeStateManager manager = new TreeStateManager(Context.Session, Resource.ID);
                manager.Clear();
            }

            properties[ResourcePropertiesKeys.RememberCollapseState] = this.rememberStateCheckbox.Checked.ToString(CultureInfo.InvariantCulture);
            properties[ResourcePropertiesKeys.ExpandRootLevel] = this.expandRootLevelCheckbox.Checked.ToString(CultureInfo.InvariantCulture);
            properties[ResourcePropertiesKeys.Grouping] = ResourceInfoExtensions.GenSubProperties(groupByLvl1.SelectedValue, groupByLvl2.SelectedValue);
            properties[ResourcePropertiesKeys.Ordering] = this.orderByList.SelectedValue;
            properties[ResourcePropertiesKeys.SwqlFilter] = this.SwqlFilter.Text;

            // We changed stuff so clear out the expanded tree node information
            if (!this.rememberStateCheckbox.Checked)
            {
                TreeStateManager manager = new TreeStateManager(Context.Session, Resource.ID);
                manager.Clear();
            }

            PageSizeEditor.FillProperties(properties);

            return properties;
        }
    }
    
    private void FillGroupByControl(DropDownList control)
    {
        var defaultProperties = new List<ListItem>
            {
                new ListItem (Resources.SrmWebContent.EditAllStorageObjects_None,                           string.Empty ),
                new ListItem (Resources.SrmWebContent.SummaryDetails_EditStorages_TemplateVendorConst,      VendorDirecotryConst ),
                new ListItem (Resources.SrmWebContent.SummaryDetails_EditStorages_TemplateModelConst,       ModelDirecotryConst )
            };

        control.Items.AddRange(defaultProperties.Union(new GroupByStorageObjectsHelper().GetCustomPropertyListItems()).ToArray());
    }

    private void SelectGroupByControls(params DropDownList[] controls)
    {
        string[] groupings = Resource.GetSubProperties(ResourcePropertiesKeys.Grouping, DefaultGroupByOption);
        for (int i = 0; i < controls.Length; i++)
        {
            controls[i].SelectedValue = groupings.Length > i ? groupings[i] : string.Empty;
        }
    }
}