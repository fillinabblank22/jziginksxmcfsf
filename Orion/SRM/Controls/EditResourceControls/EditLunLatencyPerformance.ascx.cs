﻿using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Resources;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_SRM_Controls_EditResourceControls_EditLunLatencyPerformance : BaseResourceEditControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!this.IsPostBack)
        {
            txtDaysOfDataToLoad.Text = Resource.Properties[LunLatencyPerformance.DaysOfDataToLoadPropertyKey] ?? LunLatencyPerformance.DaysOfDataToLoadDefault.ToString(CultureInfo.CurrentUICulture);
            if (WebHelper.IsCustomObjectResource())
            {
                txtDaysOfDataToLoad.Attributes.Add("onchange", "SaveData('" + LunLatencyPerformance.DaysOfDataToLoadPropertyKey + "', this.value);");
            }

            txtSampleSizeInMinutes.Text = Resource.Properties[LunLatencyPerformance.SampleSizeInMinutesPropertyKey] ?? LunLatencyPerformance.SampleSizeInMinutesDefault.ToString(CultureInfo.CurrentUICulture);
            if (WebHelper.IsCustomObjectResource())
            {
                txtSampleSizeInMinutes.Attributes.Add("onchange", "SaveData('" + LunLatencyPerformance.SampleSizeInMinutesPropertyKey + "', this.value);");
            }
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            Int32 daysOfDataToLoadDefault;
            if (!Int32.TryParse(txtDaysOfDataToLoad.Text, out daysOfDataToLoadDefault))
                daysOfDataToLoadDefault = LunLatencyPerformance.DaysOfDataToLoadDefault;

            properties.Add(LunLatencyPerformance.DaysOfDataToLoadPropertyKey, daysOfDataToLoadDefault);


            Int32 sampleSizeInMinutesDefault;
            if (!Int32.TryParse(txtSampleSizeInMinutes.Text, out sampleSizeInMinutesDefault))
                sampleSizeInMinutesDefault = LunLatencyPerformance.SampleSizeInMinutesDefault;

            properties.Add(LunLatencyPerformance.SampleSizeInMinutesPropertyKey, sampleSizeInMinutesDefault);

            return properties;
        }
    }
}