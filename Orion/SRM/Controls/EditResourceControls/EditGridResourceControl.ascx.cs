﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;

public partial class Orion_SRM_Controls_EditResourceControls_EditGridResourceControl : BaseResourceEditControl
{
    public Orion_SRM_Controls_EditResourceControls_EditGridResourceControl()
    {
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!this.IsPostBack)
        {
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            PageSizeEditor1.FillProperties(properties);

            return properties;
        }
    }
}