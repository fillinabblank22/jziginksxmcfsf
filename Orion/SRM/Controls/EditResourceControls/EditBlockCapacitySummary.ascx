﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditBlockCapacitySummary.ascx.cs" Inherits="Orion_SRM_Controls_EditResourceControls_EditBlockCapacitySummary" %>

<b><%=Resources.SrmWebContent.BlockCapacitySummaryEdit_SpecifyPageSize %></b><br /><br />
<%=Resources.SrmWebContent.BlockCapacitySummaryEdit_PageSize %>:<br />
<asp:TextBox runat="server" ID="txtPageSize" />
<asp:RangeValidator ID="vldWarning" runat="server" ControlToValidate="txtPageSize" EnableClientScript="true"></asp:RangeValidator><br />
