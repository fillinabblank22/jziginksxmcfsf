﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditServersOnThisArray.ascx.cs" Inherits="Orion_SRM_Controls_EditResourceControls_EditAllStorageObjects" %>

<p>
    <b><%=Resources.SrmWebContent.EditAllStorageObjects_OrderBy %></b><br />
    <asp:DropDownList ID="orderByList" runat="server" Width="183" />
</p>

<p> 
    <asp:CheckBox runat="server" ID="rememberStateCheckbox" />
</p>

<p> 
    <asp:CheckBox runat="server" ID="expandRootLevelCheckbox" />
</p>