﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditName.ascx.cs" Inherits="Orion_SRM_Controls_EditName" %>
<%@ Import Namespace="Resources" %>
<orion:Include runat="server" File="EditProperties.EditName.css" Module="SRM" />


<div class="editNameSection">
     <table>
            <tr>
                <td>
                    <div class="label">
                        <%= SrmWebContent.EditSection_Name%>:
                    </div>
                </td>
                <td>
                    <asp:TextBox MaxLength="100" ID="editBox" ClientIDMode="Static" runat="server"  />
                </td>
            </tr>
     </table>
</div>