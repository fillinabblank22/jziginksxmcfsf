﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GoogleTagManager.ascx.cs" Inherits="Orion_SRM_Controls_GoogleTagManager" %>
<% if (OipInstalled && !CollectingDisabled) { %>
    <script type="text/javascript">
        dataLayer.push(<%=SerializedPendoData%>)
    </script>
<% } %>