﻿using System;
using SolarWinds.Orion.Core.Common.ExpressionEvaluator;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Web.Interfaces;
using System.Collections.Generic;
using System.Linq;

public partial class Orion_SRM_Controls_EditName : System.Web.UI.UserControl, ICreateEditControl, IEditControl
{
    private readonly List<string> supportedNetObjects = TableNameAndIdFieldName.Keys.ToList();

    private string netObjectType;
    private int netObjectId;

    private static readonly Dictionary<string, Tuple<string, string>> TableNameAndIdFieldName =
            new Dictionary<string, Tuple<string, string>>()
                {
                    { SolarWinds.SRM.Web.NetObjects.Lun.NetObjectPrefix, new Tuple<string, string>(SRMConstants.DatabaseTables.LUN, "LunID")},
                    { SolarWinds.SRM.Web.NetObjects.Pool.NetObjectPrefix, new Tuple<string, string>(SRMConstants.DatabaseTables.Pool, "PoolID")},
                    { SolarWinds.SRM.Web.NetObjects.StorageArray.NetObjectPrefix, new Tuple<string, string>(SRMConstants.DatabaseTables.StorageArray, "StorageArrayID")},
                    { SolarWinds.SRM.Web.NetObjects.VServer.NetObjectPrefix, new Tuple<string, string>(SRMConstants.DatabaseTables.VServer, "VServerID")},
                    { SolarWinds.SRM.Web.NetObjects.FileShares.NetObjectPrefix, new Tuple<string, string>(SRMConstants.DatabaseTables.FileShare, "FileShareID")},
                    { SolarWinds.SRM.Web.NetObjects.Volume.NetObjectPrefix, new Tuple<string, string>(SRMConstants.DatabaseTables.Volume, "VolumeID")},
                    { SolarWinds.SRM.Web.NetObjects.StorageProvider.NetObjectPrefix, new Tuple<string, string>(SRMConstants.DatabaseTables.Provider, "ProviderID")},
                    { SolarWinds.SRM.Web.NetObjects.StorageController.NetObjectPrefix, new Tuple<string, string>(SRMConstants.DatabaseTables.StorageController, "StorageControllerID")},
                };

    public IEditControl InitFromNetObjects(List<string> listOfNetObjectIDs)
    {
        if (listOfNetObjectIDs.Count != 1)
        {
            // We are not allowing multiple name change.
            return null;
        }

        var netObjectIDParts = listOfNetObjectIDs.First().Split(':');
        this.netObjectType = netObjectIDParts[0];

        // We need to support all passed in NetObject Prefixes
        if (!supportedNetObjects.Contains(netObjectType))
        {
            return null;
        }
        
        this.netObjectId = int.Parse(netObjectIDParts[1]);
        this.FillControl();
        return this;
    }

    private void FillControl()
    {
        var tableNameAndIdFieldNameValues = this.GetNetObjectTableAndIdField();
        using (var proxy = BusinessLayerFactory.Create())
        {
            string text = proxy.GetNetObjectCaption(tableNameAndIdFieldNameValues.Item1, tableNameAndIdFieldNameValues.Item2, netObjectId);
            editBox.Text = text.Length > editBox.MaxLength ? text.Substring(0, editBox.MaxLength) : text;
        }
    }

    public string ValidateJSFunction()
    {
        return null;
    }

    public bool Validate(bool connectionPropertyChanged)
    {
        return true;
    }

    public KeyValuePair<string, string>[] GetWarnings()
    {
        return null;
    }

    public void Update(bool connectionPropertyChanged)
    {
        var text = WebSecurityHelper.SanitizeHtml(editBox.Text);
        var tableNameAndIdFieldNameValues = this.GetNetObjectTableAndIdField();
        
        using (var proxy = BusinessLayerFactory.Create())
        {
            proxy.UpdateNetObjectCaption(tableNameAndIdFieldNameValues.Item1, tableNameAndIdFieldNameValues.Item2, netObjectId, text);
        }
    }

    private Tuple<string, string> GetNetObjectTableAndIdField()
    {
        Tuple<string, string> tableNameAndIdField;
        if (!TableNameAndIdFieldName.TryGetValue(netObjectType, out tableNameAndIdField))
        {
            throw new InvalidInputException(InvariantString.Format("Unknow netObject type {0}", netObjectType));
        }

        return tableNameAndIdField;
    }
}