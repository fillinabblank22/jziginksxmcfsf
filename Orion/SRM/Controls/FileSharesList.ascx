﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FileSharesList.ascx.cs" Inherits="Orion.SRM.Controls.Orion_SRM_Controls_FileSharesList" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Resources" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>

<orion:CustomQueryTable runat="server" ID="CustomTable" />
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include ID="Include1" runat="server" Module="SRM" File="SRM.Common.js" />
<orion:Include ID="Include3" runat="server" Module="SRM" File="SRM.css" />
<script type="text/javascript">
    $(function () {
        SW.Core.Resources.CustomQuery.initialize(
            {
                uniqueId: '<%= CustomTable.UniqueClientID %>',
                initialPage: 0,
                rowsPerPage: <%= this.GetResourcePageSize() %>,
                allowSort: true,
                columnSettings: {
                    "Name": {
                        header: SW.SRM.Formatters.FormatColumnTitle("<%=SrmWebContent.FileSharesList_Column_Header_Name%>"),
                        cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                            return "SRM_Icon_Cell srm_word_wrap";
                        }
                    },
                    "Host": {
                        header: SW.SRM.Formatters.FormatColumnTitle("<%=SrmWebContent.FileSharesList_Column_Header_Host%>"),
                        cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "srm_word_wrap"; }
                    },
                    "Path": {
                        header: SW.SRM.Formatters.FormatColumnTitle("<%=SrmWebContent.FileSharesList_Column_Header_Path%>"),
                        cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "srm_word_wrap"; }
                    },
                    "VolumeSize": {
                        header: SW.SRM.Formatters.FormatColumnTitle("<%=SrmWebContent.FileSharesList_Column_Header_VolumeSize%>"),
                        formatter: function (value) {
                            return SW.SRM.Formatters.FormatCapacity(value);
                        },
                        cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "SRM_NoWrap"; }
                     },
                    "Quota": {
                        header: SW.SRM.Formatters.FormatColumnTitle("<%=SrmWebContent.FileSharesList_Column_Header_Quota%>"),
                        formatter: function (value) {
                            return SW.SRM.Formatters.FormatCapacity(value);
                        },
                        cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "SRM_NoWrap"; }
                    },
                    "Used": {
                        header: SW.SRM.Formatters.FormatColumnTitle("<%=SrmWebContent.FileSharesList_Column_Header_Used%>"),
                        formatter: function (value) {
                            return SW.SRM.Formatters.FormatCapacity(value);
                        },
                        cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                            return "SRM_NoWrap " +
                                SW.SRM.Formatters.FormatCellStyle(rowArray[8],
                                <%=ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.FileShareFileSystemUsedCapacityPercent)%>,
                                <%=ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.FileShareFileSystemUsedCapacityPercent)%>);
                        }
                    },
                    "UsedPercent": {
                        header: SW.SRM.Formatters.FormatColumnTitle("<%=SrmWebContent.FileSharesList_Column_Header_UsedPercent%>"),
                        formatter: SW.SRM.Formatters.FormatPercentageColumnValue,
                        cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                            return "SRM_NoWrap " + 
                                SW.SRM.Formatters.FormatCellStyle(cellValue,
                                <%=ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.FileShareFileSystemUsedCapacityPercent)%>,
                                <%=ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.FileShareFileSystemUsedCapacityPercent)%>);
                        }
                    },
                    "Free": {
                        header: SW.SRM.Formatters.FormatColumnTitle("<%=SrmWebContent.FileSharesList_Column_Header_Free%>"),
                        formatter: function (value) {
                            return SW.SRM.Formatters.FormatCapacity(value);
                        },
                        cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "SRM_NoWrap"; }
                    },
                    "CapacityRunout": {
                        header: SW.SRM.Formatters.FormatColumnTitle("<%=SrmWebContent.FileSharesList_Column_Header_CapacityRunout%>"),
                        isHtml: true,
                        formatter: function (value) {
                            return SW.SRM.Formatters.FormatDaysDiff(value);
                        },
                        cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                            return "SRM_NoWrap " + 
                                SW.SRM.Formatters.FormatCapacityRunoutCellStyle(cellValue, <%= WarningThresholdDays %>, <%= CriticalThresholdDays %>);
                        }
                    }
                },
                searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                searchButtonId: '<%= SearchControl.SearchButtonClientID %>'
            });
        SW.Core.Resources.CustomQuery.refresh("<%=CustomTable.UniqueClientID %>");
    });
 </script>



