﻿using System;
using Newtonsoft.Json;
using SolarWinds.Logging;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;

public partial class Orion_SRM_Controls_GoogleTagManager : System.Web.UI.UserControl
{
    protected bool OipInstalled = false;
    protected bool CollectingDisabled = false;
    protected string SerializedPendoData;
    private static readonly Log log = new Log();

    protected override void OnInit(EventArgs e)
    {
        try
        {
            using (Microsoft.Win32.RegistryKey key =
                Microsoft.Win32.Registry.LocalMachine.OpenSubKey(string.Format("Software\\{0}SolarWinds\\Orion\\Improvement", IntPtr.Size == 8 ? "Wow6432Node\\" : string.Empty)))
            {
                if (key != null)
                {
                    OipInstalled = (int)key.GetValue("OptIn", 0) != 0;
                    CollectingDisabled = (int)key.GetValue("StopCollecting", 0) != 0;
                }
            }
        }
        catch (Exception exc)
        {
            log.Error($"Error during reading registry key: {exc}");
        }
        base.OnInit(e);
    }

    protected void  Page_Load(object sender, EventArgs e)
    {
        try
        {
            var pendoData = ServiceLocator.GetInstance<IPendoDataDAL>().GetPendoData();

            this.SerializedPendoData = JsonConvert.SerializeObject(pendoData,
                new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
        }
        catch (Exception ex)
        {
            log.Error($"Error during retriving pendodata: {ex}");
        }
        
    }
}