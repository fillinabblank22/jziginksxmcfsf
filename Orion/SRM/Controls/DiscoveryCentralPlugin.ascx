﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DiscoveryCentralPlugin.ascx.cs" Inherits="Orion.SRM.Controls.DiscoveryCentralPlugin" %>
<%@ Import Namespace="Resources" %>
<%@ Register Src="~/Orion/Admin/ManagedNetObjectsInfo.ascx" TagPrefix="orion" TagName="ManagedNetObjectsInfo" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<div class="coreDiscoveryIcon">
<asp:Image runat="server" ImageUrl="~/Orion/SRM/images/SRM_DiscoveryCentral_Icon.png"/>
</div>

<div class="coreDiscoveryPluginBody">
    <h2>
        <%= SrmWebContent.AddStorage_DiscoveryTitle %></h2>
    <div>
        <%= SrmWebContent.AddStorage_DiscoverySubTitle %></div>
    <span class="LinkArrow">&#0187;</span>
    <orion:HelpLink ID="DiscoveryHelpLink" runat="server" HelpUrlFragment="SRMAG_DiscoveryCentral"
        HelpDescription="<%$ Resources: SrmWebContent, Discovery_LearnMore %>" CssClass="helpLink" />
    
    <orion:ManagedNetObjectsInfo ID="ArraysInfo" runat="server" EntityName="<%$ Resources: SrmWebContent, Discovery_StorageArraysCount %>" NumberOfElements="<%# ArraysCount %>" />    
    <orion:ManagedNetObjectsInfo ID="DisksInfo" runat="server" EntityName="<%$ Resources: SrmWebContent, Discovery_StorageDisksCount %>" NumberOfElements="<%# DisksCount %>" />
    
    
    <div class="sw-btn-bar">
        <orion:LocalizableButtonLink runat="server" Text="<%$ Resources: SrmWebContent, Discovery_AddStorageDevice %>" 
                                     NavigateUrl="~/Orion/SRM/Config/DeviceTypeStep.aspx" DisplayType="Secondary" />
    </div>
</div>
