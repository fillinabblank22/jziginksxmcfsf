﻿using System;
using System.Collections.Generic;
using System.Linq;
using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.DAL;
using SolarWinds.SRM.Web.Interfaces;

namespace Orion.SRM.Controls
{
    public partial class DiscoveryCentralPlugin : System.Web.UI.UserControl
    {
        private static readonly Log log = new Log();

        protected int DisksCount { get; set; }
        public int ArraysCount { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                using (var proxy = BusinessLayerFactory.Create())
                {
                    LicenseEntity lic = proxy.GetLicenseInfo();
                    DisksCount = lic.CurrentDiskCount;
                    List<string> licensedObjects = proxy.GetLicensedObjects();
                    ArraysCount = licensedObjects != null ? licensedObjects.Count : 0;
                    
                    if (lic.UsedBySRMCount == 0)
                    {
                        var excluded = new HashSet<String>
                        {
                            "Orion".ToLowerInvariant(), 
                            "VIM".ToLowerInvariant(),
							"EOC".ToLowerInvariant(),
                            new SrmProfile().ModuleID.ToLowerInvariant()
                        };
                        var modulesDescription = OrionModuleManager.GetInstalledModules();
                        var modulesCount = modulesDescription.Count(m => !excluded.Contains(m.ProductTag.ToLowerInvariant()));

                        if (modulesCount == 0)
                        {
                            // redirect only in case SRM is installed and no other modules are installed
                            Response.Redirect("~/Orion/SRM/Config/DeviceTypeStep.aspx");
                        }
                    }
                }
                DataBind();
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Cannot get number of managed Disks. Details: {0}", ex);
            }
        }
    }
}