﻿using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.Core.Common.ExpressionEvaluator;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.SRM.Web.UI;

public partial class Orion_SRM_Controls_HardwareHealthProperties : System.Web.UI.UserControl, ICreateEditControl, IEditControl, IConfigurationControl
{
    private List<NetObjectUniqueIdentifier>     NetObjectIDs;
    private Dictionary<int, bool>               HardwareHealthCapableArrays;
    private static readonly IHardwareHealthDAL  HardwareHealthDal = ServiceLocator.GetInstance<IHardwareHealthDAL>();

    public IEditControl InitFromNetObjects(List<string> listOfNetObjectIDs) // ICreateEditControl
    {
        if (!listOfNetObjectIDs.Any())
        {
            return null;
        }

        this.NetObjectIDs = listOfNetObjectIDs.Select(n => new NetObjectUniqueIdentifier(n)).ToList();

        bool allNetObjectsAreStorageArrays = this.NetObjectIDs.Any() &&
                                             this.NetObjectIDs.All(n => n.Prefix == StorageArray.NetObjectPrefix);
        if (!allNetObjectsAreStorageArrays)
        {
            return null;
        }

        // Detect, if there are StorageArrays that supports HwH Diagnostic
        this.HardwareHealthCapableArrays = DetectHardwareHealthCapableArrays(this.NetObjectIDs.Select(n => n.ID).ToArray());
        if (this.HardwareHealthCapableArrays.Count == 0)
        {
            return null; // No HwH-capable devices => nothing to edit
        }

        if (this.NetObjectIDs.Count() == 1) // Single Edit
        {
            HWH_SingleOptions.Visible = true;
            HWH_MultipleOptions.Visible = false;
        }
        else if (this.NetObjectIDs.Count() > 1) // Multiple Edit
        {
            HWH_SingleOptions.Visible = false;
            HWH_MultipleOptions.Visible = true;
        }
        else
        {
            return null; // Nothing to Edit
        }

        this.FillControl();
        return this;
    }

    public void FillControl()
    {
        if (this.NetObjectIDs.Count == 1) // Single Edit
        {
            int netObjectIndex = NetObjectIDs.First().ID;
            bool hwhEnabled = this.HardwareHealthCapableArrays[netObjectIndex];
            cbHwHStatus.Checked = hwhEnabled;
        }
        else if (this.NetObjectIDs.Count > 1) // Multiple Edit
        {
            int nCapableDevices = this.HardwareHealthCapableArrays.Count;                 // Devices that supports HwH
            int nEnabledDevices = this.HardwareHealthCapableArrays.Count(x => x.Value);   // Devices that have enabled HwH status

            string strSummary = string.Format(Resources.SrmWebContent.EditProperties_HwH_Summary, nEnabledDevices, nCapableDevices);
            labelHwHSummary.Text = strSummary;
        }
    }

    private void Page_PreRender(object sender, EventArgs e)
    {
        if (cbHwHGroup.Checked)
            HWH_SummaryDiv.Attributes["class"] = "SRM_HiddenField";
        else
            HWH_RadioGroup.Attributes["class"] = "SRM_HiddenField";
    }

    public string ValidationError { get; private set; } // IConfigurationControl

    public void SaveValues() // IConfigurationControl
    {
        this.Update(true);
    }

    public bool ValidateUserInput() // IConfigurationControl
    {
        // Return TRUE if user's input is OK
        // Return FALSE and establish this.ValidationError property with error if user provided incorrect values

        return true;
    }
    
    public string ValidateJSFunction() // IEditControl
    {
        // Return name of a valid the JavaScript function to be called when user press Submit button, or NULL otherwise
        return null;
    }

    public bool Validate(bool connectionPropertyChanged) // IEditControl
    {
        return ValidateUserInput();
    }

    public KeyValuePair<string, string>[] GetWarnings() // IEditControl
    {
        return null;
    }

    public void Update(bool connectionPropertyChanged) // IEditControl
    {
        // Called by Framework: Prompt to Commit Changes 

        if (!this.NetObjectIDs.Any())
        {
            return;
        }

        int[] idsToUpdate = this.HardwareHealthCapableArrays.Keys.ToArray();

        if (this.NetObjectIDs.Count == 1) // Single Edit
        {
            bool bEnableHWH = cbHwHStatus.Checked;
            SetHWHPolling(idsToUpdate, bEnableHWH);
        }
        else if (this.NetObjectIDs.Count > 1) // Multiple Edit
        {
            if (cbHwHGroup.Checked) // Does multiple edit checkBox was checked ?
            {
                bool bEnableHWH = radioEnableAll.Checked;
                SetHWHPolling(idsToUpdate, bEnableHWH);
            }
        }
    }

    private Dictionary<int, bool> DetectHardwareHealthCapableArrays(int[] idsToCheck)
    {
        Dictionary<int, bool> capableIds = new Dictionary<int, bool>();

        foreach (int nID in idsToCheck)
        {
            bool? hwhEnabled = HardwareHealthDal.IsHWHPollingEnabled(nID);
            if (hwhEnabled.HasValue)
                capableIds.Add(nID, hwhEnabled.Value);
        }

        return capableIds;
    }

    private void SetHWHPolling(int[] idsToUpdate, bool bEnableHWH)
    {
        foreach (int storageArrayID in idsToUpdate)
        {
            HardwareHealthDal.SetHWHPolling(storageArrayID, bEnableHWH);
        }
    }

}