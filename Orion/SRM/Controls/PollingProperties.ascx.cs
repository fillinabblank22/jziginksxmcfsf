﻿using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.Core.Common.ExpressionEvaluator;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.SRM.Web.EnrollmentWizard;
using SolarWinds.SRM.Web.UI;

public partial class Orion_SRM_Controls_PollingProperties : System.Web.UI.UserControl, ICreateEditControl, IEditControl, IConfigurationControl, IFillControl
{
    protected const int PollingMax = 60 * 24;
    protected const int PollingMin = 1;
    protected const int TopologyPollingMin = 30;

    private readonly List<string> supportedNetObjects = TableNameAndIdFieldName.Select(t => t.Key).ToList();
    private static readonly Dictionary<string, Tuple<string, string>> TableNameAndIdFieldName =
            new Dictionary<string, Tuple<string, string>>()
                {
                    { StorageArray.NetObjectPrefix, new Tuple<string, string>(SRMConstants.DatabaseTables.StorageArray, "StorageArrayID")},
                    { StorageProvider.NetObjectPrefix, new Tuple<string, string>(SRMConstants.DatabaseTables.Provider, "ProviderID")},
                };

    private readonly FlowType[] unsupportedArrayFlowTypes = new[] { FlowType.SmisEmcUnified, FlowType.NetAppDfm, FlowType.XtremIO };

    public string ValidationError { get; private set; }

    public List<NetObjectUniqueIdentifier> ListOfNetObjectIDs;

    private bool IsArray()
    {
        return ListOfNetObjectIDs != null && ListOfNetObjectIDs.Any() &&
               ListOfNetObjectIDs.First().Prefix.Equals(StorageArray.NetObjectPrefix, StringComparison.InvariantCultureIgnoreCase);
    }

    protected bool IsKaminario
    {
        get
        {
            return WizardWorkflowHelper.ConfigurationInfo.DeviceGroup?.GroupId == new Guid("cf0c83be-2076-4eed-a61e-1e55c9708953");
        }
    }

    protected bool IsControllerPollingEnabled
    {
        get
        {
            return this.IsArray() && ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayDetails(ListOfNetObjectIDs.First().ID)?.ControllersPollingFeature == StorageControllerPollingFeature.Enabled;
        }
    }

    public IEditControl InitFromNetObjects(List<string> listOfNetObjectIDs)
    {
        if (!listOfNetObjectIDs.Any())
        {
            return null;
        }

        ListOfNetObjectIDs = listOfNetObjectIDs.Select(n => new NetObjectUniqueIdentifier(n)).ToList();

        var allNetObjectsAreSupportedAndOfTheSameType = ListOfNetObjectIDs.Any()
                                                        && ListOfNetObjectIDs.All(n =>
                                                            supportedNetObjects.Contains(n.Prefix)
                                                            && ListOfNetObjectIDs.First().Prefix == n.Prefix);
        if (!allNetObjectsAreSupportedAndOfTheSameType)
        {
            return null;
        }

        CapacityPollingFrequencyRow.Visible = this.IsArray();
        TopologyPollingFrequencyRow.Visible = this.IsArray();
        ControllerPollingFrequencyRow.Visible = this.IsControllerPollingEnabled;

        if (this.ListOfNetObjectIDs.Count() != 1)
        {
            pollingEngineContainer.Visible = false;

            cbSelectedPerformance.Visible = true;
            cbSelectedCapacity.Visible = true;
            cbSelectedTopology.Visible = true;
            cbSelectedController.Visible = true;

            editBoxPerf.Enabled = false;
            editBoxCap.Enabled = false;
            editBoxTopology.Enabled = false;
            editBoxController.Enabled = false;

            return this;
        }

        if (this.IsArray())
        {
            // Polling properties will not be displayed on array with external SMI-S Block and NetApp DFM providers, because it will be hosted in control itself
            var deviceGroup = ServiceLocator.GetInstance<IDeviceGroupDAL>().GetDeviceGroupDetailsByArrayId(this.ListOfNetObjectIDs.First().ID);
            if (this.unsupportedArrayFlowTypes.Contains(deviceGroup.FlowType) || (deviceGroup.FlowType == FlowType.Smis && deviceGroup.ProviderLocationOrOnboard == ProviderLocation.External))
            {
                return null;
            }
        }

        this.FillControl();
        return this;
    }
    protected void cbSelectedPerformance_CheckedChanged(object sender, EventArgs e)
    {
        editBoxPerf.Enabled = cbSelectedPerformance.Checked;
    }
    protected void cbSelectedCapacity_CheckedChanged(object sender, EventArgs e)
    {
        editBoxCap.Enabled = cbSelectedCapacity.Checked;
    }
    protected void cbSelectedTopology_CheckedChanged(object sender, EventArgs e)
    {
        editBoxTopology.Enabled = cbSelectedTopology.Checked;
    }

    protected void cbSelectedController_CheckedChanged(object sender, EventArgs e)
    {
        editBoxController.Enabled = cbSelectedController.Checked;
    }

    public void FillControl()
    {
        var engines = ServiceLocator.GetInstance<IEngineDAL>().GetEngines().ToDictionary(d => d.EngineID);

        this.engineChange.Visible = engines.Count > 1;

        var netObject = NetObjectFactory.Create(ListOfNetObjectIDs.First().WholeID, true);

        var engineID = netObject.GetProperty<int>("EngineID");
        var engine = engines[engineID];
        var textEngineStatusImg = InvariantString.Format("/NetPerfMon/images/{0}", CommonWebHelper.GetEngineStatusImage(engine.KeepAlive));
        var textEngineName = engine.ServerName;
        var textEngineType = string.Format(CultureInfo.CurrentCulture, "({0})", ResourceLookup("ServerType", engine.ServerType));

        var performanceIntervalFieldName = IsArray() ? "StatCollection" : "PollingInterval";
        editBoxPerf.Text = netObject.GetProperty<int>(performanceIntervalFieldName).ToString(CultureInfo.InvariantCulture);

        if (netObject.ObjectProperties.ContainsKey("RediscoveryInterval"))
        {
            editBoxCap.Text = netObject.GetProperty<int>("RediscoveryInterval").ToString(CultureInfo.InvariantCulture);
        }

        if (netObject.ObjectProperties.ContainsKey("TopologyInterval"))
        {
            editBoxTopology.Text = netObject.GetProperty<int>("TopologyInterval").ToString(CultureInfo.InvariantCulture);
        }

        if (netObject.ObjectProperties.ContainsKey("ControllerInterval"))
        {
            editBoxController.Text = netObject.GetProperty<int>("ControllerInterval").ToString(CultureInfo.InvariantCulture);
        }

        pollEngineDescr.InnerHtml = InvariantString.Format("<a href=\"/Orion/Admin/Details/Engines.aspx\"><img src='{0}' />&nbsp;<span style=\"vertical-align: top;\">{1} {2}</span></a>", textEngineStatusImg, textEngineName, textEngineType);
        hfPollingEngineId.Value = engineID.ToString();
        ControllerPollingFrequencyRow.Visible = this.IsControllerPollingEnabled;
    }

    private string ResourceLookup(string prefix, string value)
    {
        const string managerid = "Core.Strings";
        var manager = ResourceManagerRegistrar.Instance;
        var key = manager.CleanResxKey(prefix, value.Trim());
        var result = manager.SearchAll(key, new[] { managerid });
        return String.IsNullOrEmpty(result) ? value : result;
    }

    public string ValidateJSFunction()
    {
        return "SW.SRM.EditProperties.PollingProperties.InitValidation()";
    }

    public void SaveValues()
    {
        this.Update(true);
    }

    public bool ValidateUserInput()
    {
        return ValidateTextBoxValue(editBoxPerf.Text.Trim(), SrmWebContent.EditProperties_PollingProperties_PerformancePollingFrequency, cbSelectedPerformance)
            && (!CapacityPollingFrequencyRow.Visible || ValidateTextBoxValue(editBoxCap.Text.Trim(), SrmWebContent.EditProperties_PollingProperties_CapacityPollingFrequency, cbSelectedCapacity))
            && (!TopologyPollingFrequencyRow.Visible || ValidateTextBoxValue(editBoxTopology.Text.Trim(), SrmWebContent.EditProperties_PollingProperties_TopologyPollingFrequency, cbSelectedTopology, TopologyPollingMin))
            && (!ControllerPollingFrequencyRow.Visible || !IsControllerPollingEnabled || ValidateTextBoxValue(this.editBoxController.Text.Trim(), SrmWebContent.EditProperties_PollingProperties_ControllerFrequency, this.cbSelectedController));
    }

    public bool Validate(bool connectionPropertyChanged)
    {
        return ValidateUserInput();
    }

    public KeyValuePair<string, string>[] GetWarnings()
    {
        return null;
    }

    public bool ValidateTextBoxValue(string editBoxValue, string editBoxLabel, CheckBox validate, int pollingMinimum = PollingMin)
    {
        if (!validate.Checked && validate.Visible)
        {
            return true;
        }

        if (string.IsNullOrEmpty(editBoxValue))
        {
            this.ValidationError = String.Format(CultureInfo.CurrentCulture, SrmWebContent.PollingProperties_RequiredFieldValidator, editBoxLabel);
            return false;
        }

        int value;
        if (!int.TryParse(editBoxValue, out value))
        {
            this.ValidationError = String.Format(CultureInfo.CurrentCulture, SrmWebContent.PollingProperties_InvalidNumber_ErrorMessage, editBoxLabel);
            return false;
        }

        if (value < pollingMinimum || value > PollingMax)
        {
            this.ValidationError = String.Format(CultureInfo.CurrentCulture, SrmWebContent.PollingProperties_InvalidInterval_ErrorMessage, editBoxLabel, pollingMinimum, PollingMax);
            return false;
        }

        if (IsKaminario)
        {
            return this.ValidateKaminarioRange(value, editBoxLabel);
        }

        return true;
    }

    private bool ValidateKaminarioRange(int value, string field)
    {
        if (value <= 60 && (value % 5) != 0)
        {
            this.ValidationError = String.Format(CultureInfo.CurrentCulture,
                SrmWebContent.PollingProperties_InvalidIntervalKaminario_ErrorMessage, field, PollingMin, 60, 5);
            return false;
        }
        else if (value > 60 && (value % 60) != 0)
        {
            this.ValidationError = String.Format(CultureInfo.CurrentCulture,
                SrmWebContent.PollingProperties_InvalidIntervalKaminario_ErrorMessage, field, 60, PollingMax, 60);
            return false;
        }

        return true;
    }

    public void Update(bool connectionPropertyChanged)
    {
        if (!ListOfNetObjectIDs.Any())
        {
            return;
        }

        var idsToUpdate = ListOfNetObjectIDs.Select(n => n.ID).ToArray();

        UpdateEngineId(idsToUpdate);
        UpdatePollingIntervals(idsToUpdate);
    }

    private void UpdatePollingIntervals(int[] idsToUpdate)
    {
        int performancePollingFrequency;
        int.TryParse(WebSecurityHelper.SanitizeHtml(editBoxPerf.Text), out performancePollingFrequency);

        using (var proxy = BusinessLayerFactory.Create())
        {
            if (IsArray())
            {
                int capacityPollingFrequency;
                int.TryParse(WebSecurityHelper.SanitizeHtml(editBoxCap.Text), out capacityPollingFrequency); 
                int topologyPollingFrequency;
                int.TryParse(WebSecurityHelper.SanitizeHtml(editBoxTopology.Text), out topologyPollingFrequency);
                int controllerPollingFrequency;
                int.TryParse(WebSecurityHelper.SanitizeHtml(editBoxController.Text), out controllerPollingFrequency);

                if (capacityPollingFrequency <= 0 && performancePollingFrequency <= 0 && topologyPollingFrequency <= 0 && controllerPollingFrequency <= 0)
                    return;
                
                foreach (var netObjectId in idsToUpdate)
                {
                    proxy.UpdateArrayPollingIntervals(netObjectId, performancePollingFrequency, capacityPollingFrequency, topologyPollingFrequency, controllerPollingFrequency);
                }
            }
            else
            {
                proxy.UpdateProviderPollingProperties(
                        idsToUpdate,
                        performancePollingFrequency);
            }
        }
    }

    private void UpdateEngineId(int[] idsToUpdate)
    {
        int engineID;
        if (!pollingEngineContainer.Visible || !int.TryParse(this.hfPollingEngineId.Value, out engineID) || engineID <= 0)
        {
            return;
        }

        using (var proxy = BusinessLayerFactory.Create())
        {
            var tableNameAndField = this.GetNetObjectTableAndIdField(this.ListOfNetObjectIDs.First().Prefix);
            proxy.UpdatePollingEngine(tableNameAndField.Item1, tableNameAndField.Item2, idsToUpdate, engineID);
        }
    }

    private Tuple<string, string> GetNetObjectTableAndIdField(string netObjecPrefix)
    {
        Tuple<string, string> tableNameAndIdField;
        if (!TableNameAndIdFieldName.TryGetValue(netObjecPrefix, out tableNameAndIdField))
        {
            throw new InvalidInputException(InvariantString.Format("Unknow netObject type {0}", netObjecPrefix));
        }

        return tableNameAndIdField;
    }
}