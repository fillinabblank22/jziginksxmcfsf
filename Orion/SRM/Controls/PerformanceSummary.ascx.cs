﻿using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Resources;
using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Web.UI;

public partial class Orion_SRM_Controls_PerformanceSummary : UserControl
{
    #region Fields

    private static readonly JavaScriptSerializer serializer = new JavaScriptSerializer {MaxJsonLength = 0x400000};
    private TemplatePropertyAvailabilityHelper templateAvailability = new TemplatePropertyAvailabilityHelper();

    #endregion

    #region Properties

    /// <summary>
    /// Gets or sets resource id property.
    /// </summary>
    public int ResourceID { get; set; }

    /// <summary>
    /// Gets or sets resource width property.
    /// </summary>
    public int ResourceWidth { get; set; }

    /// <summary>
    /// Gets or sets databind method property.
    /// </summary>
    public string DataBindMethod { get; set; }

    /// <summary>
    /// Gets or sets IOPS chart enable property.
    /// </summary>
    public bool IOPSEnable
    {
        get { return templateAvailability.IOPSEnable; }
        set { templateAvailability.IOPSEnable = value; }
    }

    /// <summary>
    /// Gets or sets Latency chart enable property.
    /// </summary>
    public bool LatencyEnable
    {
        get { return templateAvailability.LatencyEnable; }
        set { templateAvailability.LatencyEnable = value; }
    }
    /// <summary>
    /// Gets or sets Throughput chart enable property.
    /// </summary>
    public bool ThroughputEnable
    {
        get { return templateAvailability.ThroughputEnable; }
        set { templateAvailability.ThroughputEnable = value; }
    }

    /// <summary>
    /// Gets or sets IOSize chart enable property.
    /// </summary>
    public bool IOSizeEnable
    {
        get { return templateAvailability.IOSizeEnable; }
        set { templateAvailability.IOSizeEnable = value; }
    }

    /// <summary>
    /// Gets or sets Utilization chart enable property.
    /// </summary>
    public bool UtilizationEnable
    {
        get { return templateAvailability.UtilizationEnable; }
        set { templateAvailability.UtilizationEnable = value; }
    }

    /// <summary>
    /// Gets or sets QueueLength chart enable property.
    /// </summary>
    public bool QueueLengthEnable
    {
        get { return templateAvailability.QueueLengthEnable; }
        set { templateAvailability.QueueLengthEnable = value; }
    }

    /// <summary>
    /// Gets or sets IOPSRatio chart enable property.
    /// </summary>
    public bool IOPSRatioEnable
    {
        get { return templateAvailability.IOPSRatioEnable; }
        set { templateAvailability.IOPSRatioEnable = value; }
    }

    /// <summary>
    /// Gets or sets Disks Busy chart enable property.
    /// </summary>
    public bool DiskBusyEnable
    {
        get { return templateAvailability.DiskBusyEnable; }
        set { templateAvailability.DiskBusyEnable = value; }
    }

    /// <summary>
    /// Gets or sets Cache Hit Ratio chart enable property.
    /// </summary>
    public bool CacheHitRatioEnable
    {
        get { return templateAvailability.CacheHitRatioEnable; }
        set { templateAvailability.CacheHitRatioEnable = value; }
    }

    /// <summary>
    /// Gets or sets IOPS resource title property.
    /// </summary>
    public string CustomIOPSResourceTitle { get; set; }

    /// <summary>
    /// Gets or sets IOPS chart title property.
    /// </summary>
    public string CustomIOPSChartTitle { get; set; }

    /// <summary>
    /// Gets or sets Latency resource title property.
    /// </summary>
    public string CustomLatencyResourceTitle { get; set; }

    /// <summary>
    /// Gets or sets Latency chart title property.
    /// </summary>
    public string CustomLatencyChartTitle { get; set; }

    /// <summary>
    /// Gets or sets Throughput resource title property.
    /// </summary>
    public string CustomThroughputResourceTitle { get; set; }

    /// <summary>
    /// Gets or sets Throughput chart title property.
    /// </summary>
    public string CustomThroughputChartTitle { get; set; }

    /// <summary>
    /// Gets or sets IOSize resource title property.
    /// </summary>
    public string CustomIOSizeResourceTitle { get; set; }

    /// <summary>
    /// Gets or sets IOSize chart title property.
    /// </summary>
    public string CustomIOSizeChartTitle { get; set; }

    /// <summary>
    /// Gets or sets QueueLength resource title property.
    /// </summary>
    public string CustomQueueLengthResourceTitle { get; set; }

    /// <summary>
    /// Gets or sets QueueLength chart title property.
    /// </summary>
    public string CustomQueueLengthChartTitle { get; set; }

    /// <summary>
    /// Gets or sets IOPSRatio resource title property.
    /// </summary>
    public string CustomIOPSRatioResourceTitle { get; set; }

    /// <summary>
    /// Gets or sets IOPSRatio chart title property.
    /// </summary>
    public string CustomIOPSRatioChartTitle { get; set; }

    /// <summary>
    /// Gets or sets DiskBusy resource title property.
    /// </summary>
    public string CustomDiskBusyResourceTitle { get; set; }

    /// <summary>
    /// Gets or sets DiskBusy chart title property.
    /// </summary>
    public string CustomDiskBusyChartTitle { get; set; }

    /// <summary>
    /// Gets or sets CacheHitRatio resource title property.
    /// </summary>
    public string CustomCacheHitRatioResourceTitle { get; set; }

    /// <summary>
    /// Gets or sets CacheHitRatio chart title property.
    /// </summary>
    public string CustomCacheHitRatioChartTitle { get; set; }

    /// <summary>
    /// Gets or sets Utilization resource title property.
    /// </summary>
    public string CustomUtilizationResourceTitle { get; set; }

    /// <summary>
    /// Gets or sets Utilization chart title property.
    /// </summary>
    public string CustomUtilizationChartTitle { get; set; }

    /// <summary>
    /// Gets or sets Entity Name property.
    /// </summary>
    public string EntityName { get; set; }

    /// <summary>
    /// Gets or sets Sample Size In Minutes property.
    /// </summary>
    public int SampleSizeInMinutes { get; set; }

    /// <summary>
    /// Days Of Data To Load
    /// </summary>
    public int DaysOfDataToLoad { get; set; }

    public string InitialZoom { get; set; }

    public string ChartTitleProperty { get; set; }

    public string ChartSubTitleProperty { get; set; }

    /// <summary>
    /// Get Top coordinate of the HIChart control to be rendered above HTML layout
    /// 
    /// NOTE: Chart Height and Vertical position can also be maintained through WebService
    ///       when Chart data are request through PerformanceSummaryService.asmx
    /// </summary>
    protected int ChartTopPosition
    {
        get
        {
            int nTop = 7; // Initial value that was taken from PerformanceSummary.CSS

            if (EntityInfo.Visible)
                nTop += 20; // height is taken from CSS

            if (ChartTitle.Visible)
                nTop += 20; // height is taken from CSS

            if (ChartSubTitle.Visible)
                nTop += 20; // height is taken from CSS

            return nTop;
        }
    }

    /// <summary>
    /// Gets Data property for resource.
    /// </summary>
    protected string Data
    {
        get
        {
            return
                serializer.Serialize(
                    new
                    {
                        ResourceId = ResourceID,
                        NetObjectId = Request["NetObject"],
                        RenderTo = chart.ClientID,
                        Selected = SrmSessionManager.GetPerformanceSummaryExpanderState(ResourceID),
                        ChartWidth = ResourceWidth - 180 - 8 - 30, // Resource width - column names width - internal padding? - resource padding
                        DataBindMethod = DataBindMethod,
                        SampleSizeInMinutes,
                        DaysOfDataToLoad,
                        InitialZoom,
                        BlocksVisible,
                        ChartTitle = String.Empty, // not needed since we will render ChartTitle inside HTML
                        ChartSubTitle = String.Empty // not needed since we will render ChartSubTitle inside HTML
                    });
        }
    }

    #endregion


    #region Constructor

    /// <summary>
    /// Constructor.
    /// </summary>
    public Orion_SRM_Controls_PerformanceSummary()
    {
        IOPSEnable = false;
        LatencyEnable = false;
        ThroughputEnable = false;
        IOSizeEnable = false;
        QueueLengthEnable = false;
        IOPSRatioEnable = false;
        DiskBusyEnable = false;
        CacheHitRatioEnable = false;
        UtilizationEnable = false;
    }

    #endregion


    #region Methods

    /// <summary>
    /// Page init event handler.
    /// </summary>
    protected void Page_Init(object sender, EventArgs e)
    {
        WebHelper.IncludeCharts();
    }


    /// <summary>
    /// Page load event handler.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        templateAvailability.SetAppropriateEntity(Request.QueryString.Get("NetObject"));

        using (var proxy = BusinessLayerFactory.Create())
        {
            templateAvailability.SetTemplatesAvailability(proxy.GetPropertyAvailability, new List<string> { TemplatePropertyAvailabilityHelper.PerformanceCategory, TemplatePropertyAvailabilityHelper.ControllerTemplateEntity });
        }

        pnlIOPS.Visible = templateAvailability.IOPSVisible;
        pnlIOPSRead.Visible = templateAvailability.IOPSReadVisible;
        pnlIOPSWrite.Visible = templateAvailability.IOPSWriteVisible;
        pnlIOPSOther.Visible = templateAvailability.IOPSOtherVisible;

        pnlLatency.Visible = templateAvailability.LatencyVisible;
        pnlLatencyRead.Visible = templateAvailability.LatencyReadVisible;
        pnlLatencyWrite.Visible = templateAvailability.LatencyWriteVisible;
        pnlLatencyOther.Visible = templateAvailability.LatencyOtherVisible;

        pnlThroughput.Visible = templateAvailability.ThroughputVisible;
        pnlThroughputRead.Visible = templateAvailability.ThroughputReadVisible;
        pnlThroughputWrite.Visible = templateAvailability.ThroughputWriteVisible;

        pnlIOSize.Visible = templateAvailability.IOSizeVisible;
        pnlIOSizeRead.Visible = templateAvailability.IOSizeReadVisible;
        pnlIOSizeWrite.Visible = templateAvailability.IOSizeWriteVisible;

        pnlUtilization.Visible = templateAvailability.UtilizationVisible;

        pnlQueueLength.Visible = templateAvailability.QueueLengthVisible;
        pnlIOPSRatio.Visible = templateAvailability.IOPSRatioVisible;
        pnlDiskBusy.Visible = templateAvailability.DiskBusyVisible;

        pnlCacheHitRatio.Visible = templateAvailability.CacheHitRatioVisible;

        LatencyExpander.Visible = pnlLatencyRead.Visible || pnlLatencyWrite.Visible || pnlLatencyOther.Visible;
        IOPSExpander.Visible = pnlIOPSRead.Visible || pnlIOPSWrite.Visible || pnlIOPSOther.Visible;
        ThroughputExpander.Visible = pnlThroughputRead.Visible || pnlThroughputWrite.Visible;
        IOSizeExpander.Visible = pnlIOSizeWrite.Visible || pnlIOSizeRead.Visible;

        EntityInfo.Visible = !String.IsNullOrWhiteSpace(EntityName);
        ChartTitle.Visible = !String.IsNullOrWhiteSpace(ChartTitleProperty);
        ChartSubTitle.Visible = !String.IsNullOrWhiteSpace(ChartSubTitleProperty);
    }

    private void AddToBlocksVisible(List<string> blocksVisible, bool isVisible, string block)
    {
        if (isVisible)
            blocksVisible.Add(block);
    }

    public List<string> BlocksVisible
    {
        get
        {
            var blocksVisible = new List<string>();
            AddToBlocksVisible(blocksVisible, pnlIOPS.Visible, PerformanceSummary.BlockIops);
            AddToBlocksVisible(blocksVisible, pnlIOPSRead.Visible,PerformanceSummary.BlockIopsRead);
            AddToBlocksVisible(blocksVisible, pnlIOPSWrite.Visible,PerformanceSummary.BlockIopsWrite);
            AddToBlocksVisible(blocksVisible, pnlIOPSOther.Visible,PerformanceSummary.BlockIopsOther);

            AddToBlocksVisible(blocksVisible, pnlLatency.Visible,PerformanceSummary.BlockLatency);
            AddToBlocksVisible(blocksVisible, pnlLatencyRead.Visible,PerformanceSummary.BlockLatencyRead);
            AddToBlocksVisible(blocksVisible, pnlLatencyWrite.Visible,PerformanceSummary.BlockLatencyWrite);
            AddToBlocksVisible(blocksVisible, pnlLatencyOther.Visible,PerformanceSummary.BlockLatencyOther);

            AddToBlocksVisible(blocksVisible, pnlThroughput.Visible,PerformanceSummary.BlockThroughput);
            AddToBlocksVisible(blocksVisible, pnlThroughputRead.Visible,PerformanceSummary.BlockThroughputRead);
            AddToBlocksVisible(blocksVisible, pnlThroughputWrite.Visible,PerformanceSummary.BlockThroughputWrite);

            AddToBlocksVisible(blocksVisible, pnlIOSize.Visible,PerformanceSummary.BlockIosize);
            AddToBlocksVisible(blocksVisible, pnlIOSizeRead.Visible,PerformanceSummary.BlockIosizeRead);
            AddToBlocksVisible(blocksVisible, pnlIOSizeWrite.Visible,PerformanceSummary.BlockIosizeWrite);

            AddToBlocksVisible(blocksVisible, pnlQueueLength.Visible,PerformanceSummary.BlockQueuelength);
            AddToBlocksVisible(blocksVisible, pnlIOPSRatio.Visible,PerformanceSummary.BlockIopsratio);
            AddToBlocksVisible(blocksVisible, pnlUtilization.Visible,PerformanceSummary.BlockUtilization);
            AddToBlocksVisible(blocksVisible, pnlDiskBusy.Visible,PerformanceSummary.BlockDiskbusy);
            AddToBlocksVisible(blocksVisible, pnlCacheHitRatio.Visible,PerformanceSummary.BlockCachehitratio);
            AddToBlocksVisible(blocksVisible, pnlCacheHitRatio.Visible, PerformanceSummary.BlockRatioRead);
            AddToBlocksVisible(blocksVisible, pnlCacheHitRatio.Visible, PerformanceSummary.BlockRatioWrite);
            return blocksVisible;
        }
    }

    #endregion
}
