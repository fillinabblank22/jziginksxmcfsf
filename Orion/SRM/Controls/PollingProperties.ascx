﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PollingProperties.ascx.cs" Inherits="Orion_SRM_Controls_PollingProperties" %>
<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include runat="server" File="OrionCore.js" Section="Bottom" SpecificOrder="1" />
<orion:Include runat="server" File="SRM.AssignPollingEngine.js" Section="Bottom" SpecificOrder="2" Module="SRM" />
<orion:Include runat="server" File="extjs/Ext.sw.grid.RadioSelectionModel.js" />
<orion:Include runat="server" File="EditProperties.PollingProperties.css" Module="SRM" />
<orion:Include runat="server" File="SRM.EditProperties.PollingProperties.js" Module="SRM" />

<div class="PollingPropertiesSection">
    <div id="pollingValidationFailMsg" class="row sw-suggestion sw-suggestion-fail srm-testing-connection-msgbox">
        <span class="sw-suggestion-icon"></span>
        <span id="pollingFailMessageHeader">
            <b><%= Resources.SrmWebContent.Provider_Step_Server_Validation_Title %></b>
            <br />
        </span>
        <span id="pollingFailMessageContent"></span>
    </div>
    <div class="row">
        <asp:CheckBox runat="server" CssClass="labelCheckbox" ID="cbSelectedPerformance" ClientIDMode="Static" AutoPostBack="true" Visible="False" OnCheckedChanged="cbSelectedPerformance_CheckedChanged" />
        <asp:Label ID="labelEditBoxPerformance" class="label"><%= Resources.SrmWebContent.EditProperties_PollingProperties_PerformancePollingFrequency %></asp:Label>
        <asp:TextBox ID="editBoxPerf" ClientIDMode="Static" runat="server" />
        <%= Resources.SrmWebContent.EditProperties_PollingProperties_PerformancePollingFrequency_units %>
        <asp:Label ID="editBoxPerfValidation" runat="server" CssClass="validationError" ClientIDMode="Static" />
    </div>

    <div class="row" ClientIDMode="Static" id="CapacityPollingFrequencyRow" runat="server" >
        <asp:CheckBox runat="server" CssClass="labelCheckbox" ID="cbSelectedCapacity" ClientIDMode="Static" AutoPostBack="true" Visible="False" OnCheckedChanged="cbSelectedCapacity_CheckedChanged" />
        <asp:Label ID="labelEditBoxCapacity" class="label"><%= Resources.SrmWebContent.EditProperties_PollingProperties_CapacityPollingFrequency %></asp:Label>
        <asp:TextBox ID="editBoxCap" ClientIDMode="Static" runat="server" />
        <%= Resources.SrmWebContent.EditProperties_PollingProperties_CapacityPollingFrequency_units %>
        <asp:Label ID="editBoxCapValidation" runat="server" CssClass="validationError" ClientIDMode="Static" />
    </div>
    
    <div class="row" ClientIDMode="Static" id="TopologyPollingFrequencyRow" runat="server">
        <asp:CheckBox runat="server" CssClass="labelCheckbox" ID="cbSelectedTopology" ClientIDMode="Static" AutoPostBack="true" Visible="False" OnCheckedChanged="cbSelectedTopology_CheckedChanged" />
        <asp:Label ID="labelEditBoxTopology" class="label"><%= Resources.SrmWebContent.EditProperties_PollingProperties_TopologyPollingFrequency %></asp:Label>
        <asp:TextBox ID="editBoxTopology" ClientIDMode="Static" runat="server" />
        <%= Resources.SrmWebContent.EditProperties_PollingProperties_TopologyPollingFrequency_units %>
        <asp:Label ID="editBoxTopologyValidation" runat="server" CssClass="validationError" ClientIDMode="Static" />
    </div>
    
    <div class="row" ClientIDMode="Static" id="ControllerPollingFrequencyRow" runat="server">
        <asp:CheckBox runat="server" CssClass="labelCheckbox" ID="cbSelectedController" ClientIDMode="Static" AutoPostBack="true" Visible="False" OnCheckedChanged="cbSelectedController_CheckedChanged" />
        <asp:Label ID="labelEditBoxController" class="label"><%= Resources.SrmWebContent.EditProperties_PollingProperties_ControllerFrequency %></asp:Label>
        <asp:TextBox ID="editBoxController" ClientIDMode="Static" runat="server" />
        <%= Resources.SrmWebContent.EditProperties_PollingProperties_ControllerFrequency_units %>
        <asp:Label ID="Label1" runat="server" CssClass="validationError" ClientIDMode="Static" />
    </div>

    <div class="row">
        <div id="pollingEngineContainer" class="pollingEngineContainer" runat="server">
            <asp:Label ID="labelEditBoxPollingEngine" class="label vertically-center"><%= Resources.SrmWebContent.EditProperties_PollingProperties_PollingEngine %></asp:Label>
            <span id="pollEngineDescr" runat="server" clientidmode="Static" />
            <span id="engineChange" runat="server" class="vertically-center" clientidmode="Static" visible="false">
                <orion:LocalizableButton runat="server" DisplayType="Small" LocalizedText="CustomText" CssClass="vertically-center" Text="<%$ Resources: SrmWebContent, PollingProperties_ChangePollingEngine %>" ID="changePollEngineLink" OnClientClick="return false;" />
                <asp:HiddenField runat="server" ID="hfPollingEngineId" />
                <input type="hidden" id="selPollingEngineId" value='<%= hfPollingEngineId.ClientID %>' />
            </span>

        </div>
    </div>
</div>

<script type="text/javascript">
    var performancePollingMax = parseInt("<%= PollingMax %>");
    var performancePollingMin = parseInt("<%= PollingMin %>");
    var topologyPollingMin = parseInt("<%= TopologyPollingMin %>");
    var numberOfNetObjects = parseInt("<%= ListOfNetObjectIDs.Count() %>");

    $(function () {
        SW.SRM.AssignPollingEngine.init(function (engId, displayLink) {
            ORION.callWebService("/Orion/SRM/Services/EnrollmentWizardService.asmx",
                "SetPollingEngineToSession",
                {
                    'engineId': engId
                },
                function (result) {}
                );
        });
<%
    if (IsKaminario)
    {
%>
        $('#editBoxPerf').attr("checkKaminario", true);
        $('#editBoxPerf').change(function () {
            var value = parseInt($(this).val());
            if (SW.SRM.EditProperties.PollingProperties.validateKaminarioPerformancePollingRange(value, '<%= Resources.SrmWebContent.EditProperties_PollingProperties_PerformancePollingFrequency %>')) {
                $("#pollingValidationFailMsg").hide();
            }
        });
<% 
    } 
%>
    });
</script>
