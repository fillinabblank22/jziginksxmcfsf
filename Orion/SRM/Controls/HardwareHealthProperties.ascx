﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HardwareHealthProperties.ascx.cs" Inherits="Orion_SRM_Controls_HardwareHealthProperties" %>
<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include runat="server" File="OrionCore.js" Section="Bottom" SpecificOrder="1" />
<orion:Include runat="server" Module="SRM" File="SRM.Common.js" />
<orion:Include runat="server" Module="SRM" File="SRM.css" />

<script type="text/javascript">

    var bHwHGroupVisible = <%= cbHwHGroup.Checked ? "true" : "false"%>;

    <% // Called when Page was Loaded %>
    $(function () {
        SW.SRM.Common.SetVisible($("div[id='" + "HWH_RadioGroup" + "']"), bHwHGroupVisible)
        SW.SRM.Common.SetVisible($("div[id='" + "HWH_SummaryDiv" + "']"), !bHwHGroupVisible)
    });

    function toggleHWH_RadioGroup(btn) {
        bHwHGroupVisible = !bHwHGroupVisible;
        SW.SRM.Common.SetVisible($("div[id='" + "HWH_RadioGroup" + "']"), bHwHGroupVisible)
        SW.SRM.Common.SetVisible($("div[id='" + "HWH_SummaryDiv" + "']"), !bHwHGroupVisible)
    }
</script>

<div class="PollingPropertiesSection">

    <%// Hardware Health Section for Single Edit %>
    <div class="row" ID="HWH_SingleOptions" runat="server">
        <asp:Label runat="server" class="label"><%= Resources.SrmWebContent.EditProperties_HwH_Title %></asp:Label>
        <asp:CheckBox runat="server" CssClass="labelCheckbox" ID="cbHwHStatus" ClientIDMode="Static" />
        <br style="clear:left"/>
    </div>

    <%// Hardware Health Section for Multiple Edit %>
    <div class="row" ID="HWH_MultipleOptions" runat="server" visible="false">
        <asp:CheckBox runat="server" CssClass="labelCheckbox" ID="cbHwHGroup" AutoPostBack="false" onclick="toggleHWH_RadioGroup(this)" />
        <asp:Label runat="server" class="label"><%= Resources.SrmWebContent.EditProperties_HwH_Title %></asp:Label>

        <div runat="server" ID="HWH_SummaryDiv" ClientIDMode="Static" >
            <asp:Label runat="server" ID="labelHwHSummary" ClientIDMode="Static" class="label"/>
            <br style="clear:left"/>
        </div>

        <div runat="server" ID="HWH_RadioGroup" ClientIDMode="Static" >
            <asp:RadioButton ID="radioEnableAll" runat="server" AutoPostBack="False" GroupName="HWH_Polling" CssClass="labelCheckbox" Checked="True" Text=""/>
            <%= Resources.SrmWebContent.EditProperties_HwH_EnabeAll %>
            <br style="clear:left"/>
            <asp:Label runat="server" class="labelCheckbox">&nbsp</asp:Label>   <%// dummy label to shift RadioButton %>
            <asp:Label runat="server" class="label">&nbsp</asp:Label>           <%// dummy label to shift RadioButton %>
            <asp:RadioButton ID="radioDisableAll" runat="server" AutoPostBack="False" GroupName="HWH_Polling" CssClass="labelCheckbox" Text=""/>
            <%= Resources.SrmWebContent.EditProperties_HwH_DisableAll %>
        </div>
    </div>

</div>
