﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManageNetObjectButton.ascx.cs" Inherits="Orion_SRM_Controls_ManageNetObjectButton" %>
<%@ Import Namespace="Resources" %>
<a class="EditResourceButton sw-btn-resource sw-btn" id="manageBtn" runat="server">
    <span class="sw-btn-c">
        <span class="sw-btn-t"><%=SrmWebContent.DetailsPage_ManageButton%></span>
    </span>
</a>