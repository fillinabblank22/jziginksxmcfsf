﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SRM_CustomProperties.ascx.cs" Inherits="Orion_SRM_Controls_SRM_CustomProperties" %>
<%@ Register Src="~/Orion/Controls/EditCpValueControl.ascx" TagPrefix="orion" TagName="EditCpValueControl" %>
<orion:Include ID="Include1" runat="server" File="CustomPropertyEditor.css" />
<style type="text/css">
    .linktoCPE {
        text-align: right;
        float: right;
    }

    .blueBox {
        background-color: #ecedee;
        width: 100%;
        padding-top: 10px;
        padding-bottom: 10px;
    }
</style>
<div class="contentBlock">
    <div runat="server" id="CustomPropertiesHeader">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td class="contentBlockHeader">
                    <asp:Label runat="server"><%= Resources.SrmWebContent.CustomProperties_EditNetObject %></asp:Label>
                </td>

                <td class="linktoCPE">
                    <span>
                        <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="" />
                        <a href="/Orion/Admin/CPE/Default.aspx" class="SRM_manageLink" rel="noopener noreferrer" target="_blank">
                            <%= Resources.SrmWebContent.ManageCustomProperties_EditNetObject %></a> </span>
                </td>
            </tr>
        </table>
    </div>
    <table class="blueBox cpRepeaterTable">
        <asp:Repeater runat="server" ID="repCustomProperties">
            <ItemTemplate>
                <tr>
                    <td class="leftLabelColumn" style="vertical-align: text-top;">
                        <asp:CheckBox runat="server" ID="cbSelectedProperty" AutoPostBack="true" OnCheckedChanged="cbSelectedProperty_CheckedChanged" />&nbsp;
                        <asp:Literal runat="server" ID="litPropertyName"></asp:Literal>:
                    </td>
                    <td class="rightInputColumn">
                        <orion:EditCpValueControl ID="PropertyValue" runat="server" />
                        <span class="helpfulText" style="padding: 0px;">
                            <asp:Literal ID="descriptionText" runat="server" Text="" />
                        </span>
                        <asp:PlaceHolder runat="server" ID="placeHolder"></asp:PlaceHolder>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
</div>
