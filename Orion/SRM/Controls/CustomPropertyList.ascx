<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomPropertyList.ascx.cs" Inherits="Orion_SRM_Controls_CustomPropertyList" %>

<div>
    <div runat="server" id="Manage" class="NodeManagementIcons ui-helper-clearfix" visible="false" style="margin: 4px 0px 4px 0px;">
        <asp:HyperLink runat="server" ID="ManageLink" Font-Size="8pt">
            <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="<%= Resources.CoreWebContent.WEBDATA_AK0_28 %>" style="vertical-align: text-bottom;"/> <%= Resources.CoreWebContent.WEBDATA_VB0_185 %>
        </asp:HyperLink>
    </div>
    
    <asp:Repeater runat="server" ID="CustomPropertyTable">
        <HeaderTemplate>
            <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%"><tbody>
        </HeaderTemplate>
        <ItemTemplate>
                <tr>
                    <td class="PropertyHeader" style="vertical-align:top;"><%# Eval("Key") %></td>
                    <td class="Property"><%# Eval("Value") %></td>
                </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
                <tr style="background-color: #f8f8f8;">
                    <td class="PropertyHeader" style="vertical-align:top;"><%# Eval("Key") %></td>
                    <td class="Property"><%# Eval("Value") %></td>
                </tr>
        </AlternatingItemTemplate>
        <FooterTemplate>
            </tbody></table>
        </FooterTemplate>
    </asp:Repeater>
</div>

<div runat="server" id="netObjectError" visible="false" style="color: Red;"><%= Resources.SrmWebContent.CustomProperties_NetObjectNotSelected %></div>
