﻿using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

public partial class Orion_SRM_Controls_SRM_CustomProperties : System.Web.UI.UserControl
{
    private CustomProperty[] tableCustomProperties = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadCustomProperties();
        }
    }

    #region public properties
    public string CustomPropertiesTable
    {
        get
        {
            return (string)ViewState["CustomPropertiesTable"];
        }
        set
        {
            ViewState["CustomPropertiesTable"] = value;
        }
    }

    private CustomProperty[] TableCustomProperties => tableCustomProperties ??
                                                      (tableCustomProperties =
                                                          CustomPropertyMgr.GetCustomPropertiesForTable(CustomPropertiesTable).ToArray());

    public Func<int, ICollection<string>, Dictionary<string, object>> CustomPropertyLoader { get; set; }
    public IList<int> EntityIds { set; get; }
    public Dictionary<string, object> CustomProperties
    {
        get
        {
            Dictionary<string, object> result = new Dictionary<string, object>();

            foreach (RepeaterItem item in repCustomProperties.Items)
            {
                CheckBox cbSelectedProperty = (CheckBox)item.FindControl("cbSelectedProperty");
                if (!cbSelectedProperty.Checked && cbSelectedProperty.Visible)
                    continue;

                Literal litPropertyName = (Literal)item.FindControl("litPropertyName");
                var txtCustomPropertyValue = (Orion_Controls_EditCpValueControl)item.FindControl("PropertyValue");

                string customPropertyValueAsText = txtCustomPropertyValue.Text;
                object customPropertyValueAsObject;

                CustomProperty customProperty = TableCustomProperties.Single(p => p.PropertyName == litPropertyName.Text);

                if (String.IsNullOrEmpty(customPropertyValueAsText) && !customProperty.Mandatory
                        && customProperty.PropertyType != typeof(Boolean)) // 'NULL' values aren't allowed for Non-Mandatory BOOLEAN cp
                {
                    customPropertyValueAsObject = null;
                }
                else
                {
                    customPropertyValueAsObject = CustomPropertyMgr.ChangeType(customPropertyValueAsText, customProperty.PropertyType);
                }

                result.Add(litPropertyName.Text, customPropertyValueAsObject);
            }
            return result;
        }
        set
        {
            if (value == null) return;
            foreach (RepeaterItem item in repCustomProperties.Items)
            {
                Literal litPropertyName = (Literal)item.FindControl("litPropertyName");
                if (!value.ContainsKey(litPropertyName.Text)) continue;

                var txtCustomPropertyValue = (Orion_Controls_EditCpValueControl)item.FindControl("PropertyValue");
                if (txtCustomPropertyValue != null)
                    txtCustomPropertyValue.Text = value[litPropertyName.Text].ToString();
            }
        }
    }
    #endregion

    #region private methods
    private void LoadCustomProperties()
    {
        IList<string> customProperties = CustomPropertyMgr.GetPropNamesForTableClean(CustomPropertiesTable);

        CustomPropertiesHeader.Visible = customProperties.Any();

        IList<int> ids = EntityIds ?? new List<int>();
        var allCustomPropertiesValues = new Dictionary<string, object>();
        if (ids.Count == 1)
        {
            allCustomPropertiesValues = this.CustomPropertyLoader(ids[0], customProperties);
        }

        repCustomProperties.DataSource = customProperties;

        repCustomProperties.ItemDataBound += (sender, e) =>
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox cbSelectedProperty = (CheckBox)e.Item.FindControl("cbSelectedProperty");
                Literal litCustomPropertyName = (Literal)e.Item.FindControl("litPropertyName");
                var customPropertyValueControl = (Orion_Controls_EditCpValueControl)e.Item.FindControl("PropertyValue");
                Literal txtDescr = (Literal)e.Item.FindControl("descriptionText");

                litCustomPropertyName.Text = e.Item.DataItem.ToString();

                CustomProperty customProperty = TableCustomProperties.Single(p => p.PropertyName == e.Item.DataItem.ToString());
                customPropertyValueControl.Configure(true, customProperty);

                txtDescr.Text = DefaultSanitizer.SanitizeHtml(customProperty.Description)?.ToString();

                if (ids.Count == 0 && customProperty.Mandatory)
                {
                    customPropertyValueControl.Text = string.Empty;
                }
                else if (ids.Count == 1)
                {
                    object customPropertyValue = null;
                    allCustomPropertiesValues.TryGetValue(customProperty.PropertyName, out customPropertyValue);

                    customPropertyValueControl.Text = customPropertyValue != null ? customPropertyValue.ToString() : String.Empty;
                    cbSelectedProperty.Visible = false;
                }
                if (ids.Count > 1)
                {
                    customPropertyValueControl.Text = String.Empty;
                    customPropertyValueControl.Enabled = false;
                }
            }
        };

        repCustomProperties.DataBind();
    }
    #endregion

    public bool Validate()
    {
        return Page.IsValid;
    }

    protected void cbSelectedProperty_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox cbSelected = (CheckBox)sender;

        var txtPropertyValue = (Orion_Controls_EditCpValueControl)cbSelected.Parent.FindControl("PropertyValue");
        txtPropertyValue.Enabled = cbSelected.Checked;
    }
}