﻿using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.SRM.Web;

public partial class Orion_SRM_Controls_PopupChart : UserControl
{
    [PersistenceMode(PersistenceMode.Attribute)]
    public Unit Height { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string[] ChartColors { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string[] ChartLabelColors { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public long[] Values { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public long Total { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool RenderTotalLabel { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public long Oversubscribed { get; set; }

    protected override void OnPreRender(EventArgs e)
    {
        Panel lastPanel = null;
        int aggregatedPercent = 0;

        if (Values != null)
        {
            for (int i = 0; i < Values.Length; i++)
            {
                var legend = new Label();
                var panel = new Panel();
                var percentValue = Total == 0 ? 0 : Math.Abs((int)Math.Round((double)Values[i] * 100 / (Total + Oversubscribed), MidpointRounding.AwayFromZero));
                legend.Text = WebHelper.FormatCapacityTotal(Values[i]) + " /";
                legend.Attributes["style"] = ChartLabelColors[i];
                panel.Height = Height;
                if (percentValue != 0)
                {
                    panel.Attributes["style"] = string.Format(CultureInfo.InvariantCulture, "margin-left: {0}%;{1}", aggregatedPercent, ChartColors[i]);
                }
                aggregatedPercent += percentValue;
                if (lastPanel == null)
                {
                    OuterBorder.Controls.Add(panel);
                }
                else
                {
                    lastPanel.Controls.Add(panel);
                }
                lastPanel = panel;
                legendPanel.Controls.Add(legend);
            }
        }

        var legendTotal = new Label
        {
            Text = WebHelper.FormatCapacityTotal(Total)
        };
        legendPanel.Controls.Add(legendTotal);

        if (RenderTotalLabel)
        {
            var legendTotalLabel = new Label
            {
                Text = string.Format(CultureInfo.CurrentCulture, " {0}", SrmWebContent.PopupChart_TotalLabel)
            };
            legendPanel.Controls.Add(legendTotalLabel);
        }
      
    }
}