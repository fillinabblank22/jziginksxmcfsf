﻿using Castle.Core.Internal;
using SolarWinds.Orion.Core.Common;
using SolarWinds.SRM.Common;
using System;
using System.Linq;
using System.Text;

using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.NetObjects;

public partial class Orion_SRM_Controls_ManageNetObjectButton : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var netObject = Request.QueryString["NetObject"];
        manageBtn.Visible = !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
        if (netObject.IsNullOrEmpty())
        {
            manageBtn.Visible = false;
            return;
        }
        var netObjectId = netObject.Split(new[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
        var netObjectType = netObjectId
                            .First()
                            .Trim()
                            .ToUpperInvariant();

        string returnPage;
        switch (netObjectType)
        {
            case StorageArray.NetObjectPrefix:
                var array = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayDetails(int.Parse(netObjectId[1]));

                returnPage = array.IsCluster 
                            ? "/Orion/SRM/ClusterDetailsView.aspx"
                            : "/Orion/SRM/ArrayDetailsView.aspx";
                break;
            case FileShares.NetObjectPrefix:
                returnPage = "/Orion/SRM/ArrayDetailsView.aspx";
                break;
            case Lun.NetObjectPrefix:
                returnPage = "/Orion/SRM/LunDetailsView.aspx";
                break;
            case Pool.NetObjectPrefix:
                returnPage = "/Orion/SRM/StoragePoolDetailsView.aspx";
                break;
            case Volume.NetObjectPrefix:
                returnPage = "/Orion/SRM/NasVolumeDetailsView.aspx";
                break;
            case VServer.NetObjectPrefix:
                returnPage = "/Orion/SRM/VServerDetailsView.aspx";
                break;
            case StorageProvider.NetObjectPrefix:
                returnPage = "/Orion/SRM/ProviderDetailsView.aspx";
                break;
            case StorageController.NetObjectPrefix:
                returnPage = "/Orion/SRM/ControllerDetailsView.aspx";
                break;
            default: throw new InvalidParamaterValueException(InvariantString.Format("Unknow NetObject prefix {0}", netObjectType));
        }

        returnPage = Convert.ToBase64String(Encoding.ASCII.GetBytes(InvariantString.Format(
            "{0}{1}?NetObject={2}",
            Request.Url.GetLeftPart(UriPartial.Authority),
            returnPage,
            netObject)));

        manageBtn.HRef = InvariantString.Format("/Orion/SRM/Admin/EditProperties.aspx?NetObject={0}&ReturnTo={1}",
                            netObject,
                            returnPage);
    }
}