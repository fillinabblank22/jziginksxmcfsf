﻿using System.Web.UI.WebControls;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.SRM.Common.Models;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

public partial class Orion_SRM_Controls_PollingPropertiesStorageController : System.Web.UI.UserControl, ICreateEditControl, IEditControl, IConfigurationControl
{
    private List<NetObjectUniqueIdentifier> NetObjectIDs;
    public IEditControl InitFromNetObjects(List<string> listOfNetObjectIDs) // ICreateEditControl
    {
        if (!listOfNetObjectIDs.Any())
        {
            return null;
        }

        this.NetObjectIDs = listOfNetObjectIDs.Select(n => new NetObjectUniqueIdentifier(n)).ToList();

        bool allNetObjectsAreStorageArrays = this.NetObjectIDs.Any() &&
                                             this.NetObjectIDs.All(n => n.Prefix == StorageArray.NetObjectPrefix);
        if (!allNetObjectsAreStorageArrays)
        {
            return null;
        }
        
        this.FillControl();
        return this;
    }

    public void FillControl()
    {
        ArrayEntity array = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayDetails(NetObjectIDs.First().ID);
        controllerPollingEnabledChbox.Checked = array?.ControllersPollingFeature == StorageControllerPollingFeature.Enabled;
        SetResourceVisibility();
    }

    public string ValidationError { get; private set; }

    public void SaveValues()
    {
        this.Update(true);
    }

    public bool ValidateUserInput()
    {
        return true;
    }
    
    public string ValidateJSFunction()
    {
        return null;
    }

    public bool Validate(bool connectionPropertyChanged)
    {
        return ValidateUserInput();
    }

    public KeyValuePair<string, string>[] GetWarnings()
    {
        return null;
    }

    public void Update(bool connectionPropertyChanged)
    {
        ArrayEntity array = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayDetails(NetObjectIDs.First().ID);

        using (var proxy = BusinessLayerFactory.Create())
        {
            if (array?.ControllersPollingFeature != StorageControllerPollingFeature.NotSupported)
                proxy.UpdateStorageControllerPollingFeature(NetObjectIDs.First().ID, controllerPollingEnabledChbox.Checked);
        }

        using (var proxy = CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(ex => { throw ex; }, array.EngineID, false))
        {
            proxy.JobNowByJobKey(NetObjectIDs.First().WholeID, "controller");
        }
    }

    private void SetResourceVisibility()
    {
        ArrayEntity array = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayDetails(NetObjectIDs.First().ID);
        this.Visible = array?.ControllersPollingFeature != StorageControllerPollingFeature.NotSupported;
    }



}