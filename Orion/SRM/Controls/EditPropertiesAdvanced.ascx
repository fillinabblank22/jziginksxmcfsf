﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditPropertiesAdvanced.ascx.cs" Inherits="Orion_SRM_Controls_EditPropertiesAdvanced" %>
<orion:Include runat="server" File="SRM.EditProperties.Advanced.js" Module="SRM" />
<orion:Include runat="server" File="EditProperties.Advanced.css" Module="SRM" />
<div class="EditPropertiesAdvanced PollingPropertiesSection">
    <div>
      <img src="/Orion/images/Button.Expand.gif" class="collapsible" data-collapse-target="advancedSettings"/>
        <span><asp:Label runat="server" ID="lblAdvanced" /><br/></span>
    </div>
    <div id="advancedSettings">
        <div class="innerSettings">
            <asp:CheckBox runat="server" ID="multipleAdvChange" Checked="false" ClientIDMode="Static" />
            <table class="advancedMultipleSettings">
                <tr>
                    <td class="firstCol"><asp:Label runat="server" ID="lblDebugLogging"/></td>
                    <td><asp:DropDownList runat="server" ID="ddlDebugLogging" ClientIDMode="Static"/></td>
                </tr>
                <tr>
                    <td><asp:Label id="lblNumberOfFiles" runat="server"/></td>
                    <td><asp:TextBox runat="server" ID="txtDebugFiles" ClientIdMode="Static"></asp:TextBox>
                        <br/>
                        <asp:Label runat="server" ID="lblDebugFilesLabel"></asp:Label>
                    </td>
                    <asp:customvalidator ID="RangeValidator" ControlToValidate="txtDebugFiles" runat="server"
                        EnableClientScript="true" onservervalidate="Customvalidator1_ServerValidate"></asp:customvalidator>
                </tr>
                <tr ID="openPullParams" runat="server">
                    <td class="firstCol"><asp:Label runat="server" ID="lblOpenAndPullEnabled"/></td>
                    <td><asp:DropDownList runat="server" ID="ddlOpenAndPullEnabled" ClientIDMode="Static"/></td>
                </tr>
            </table>
        </div>
    </div>
</div>