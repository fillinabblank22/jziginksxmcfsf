﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BreadcrumbBar.ascx.cs" Inherits="Orion_SRM_Controls_BreadcrumbBar" %>
<div>
    <%if (!CommonWebHelper.IsBreadcrumbsDisabled)
      { %>
    <div style="margin-left: 10px">
        <orion:PluggableDropDownMapPath ID="SiteMapPath" runat="server" SiteMapKey="SRMNetObjectDetails" LoadSiblingsOnDemand="true" />
    </div>
    <%} %>
</div>