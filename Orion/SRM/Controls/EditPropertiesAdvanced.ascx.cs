﻿using Resources;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

public partial class Orion_SRM_Controls_EditPropertiesAdvanced : System.Web.UI.UserControl, ICreateEditControl, IEditControl
{
    protected const int CustomLoggingFilesMax = 100;
    protected const int CustomLoggingFilesMin = 1;

    private readonly List<string> supportedNetObjects = new List<string>(new[] { StorageArray.NetObjectPrefix });

    public string ValidationError { get; private set; }

    public List<NetObjectUniqueIdentifier> ListOfNetObjectIDs;

    public IEditControl InitFromNetObjects(List<string> listOfNetObjectIDs)
    {
        if (!listOfNetObjectIDs.Any())
        {
            return null;
        }

        ListOfNetObjectIDs = listOfNetObjectIDs.Select(n => new NetObjectUniqueIdentifier(n)).ToList();

        var allNetObjectsAreSupportedAndOfTheSameType = ListOfNetObjectIDs.Any()
                                                        && ListOfNetObjectIDs.All(n =>
                                                            supportedNetObjects.Contains(n.Prefix)
                                                            && ListOfNetObjectIDs.First().Prefix == n.Prefix);
        if (!allNetObjectsAreSupportedAndOfTheSameType)
        {
            return null;
        }
        
        this.FillControl();

        return this;
    }
    
    public void FillControl()
    {
        multipleAdvChange.Visible = (ListOfNetObjectIDs.Count > 1);
        multipleAdvChange.Text = Resources.SrmWebContent.EditPropertiesAdvancedChangeForAllObjects;

        ddlDebugLogging.DataSource = new List<string>(new[] { Resources.SrmWebContent.EditPropertiesAdvancedLoggingOn, Resources.SrmWebContent.EditPropertiesAdvancedLoggingOff});
        ddlDebugLogging.DataBind();

        ddlOpenAndPullEnabled.DataSource = new List<string>(new[] { Resources.SrmWebContent.EditPropertiesAdvancedOpenAndPullEnabled, Resources.SrmWebContent.EditPropertiesAdvancedOpenAndPullDisabled });
        ddlOpenAndPullEnabled.DataBind();

        bool isOpenAndPullVisible = false;
        var array = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayDetails(ListOfNetObjectIDs[0].ID);
        string arrayFlowType = array.FlowType.ToString().ToLowerInvariant();
        if (arrayFlowType.Contains(SRMConstants.SmisKeyWord))
        {
            isOpenAndPullVisible = true;
        }
        openPullParams.Visible = isOpenAndPullVisible;

        var debugLogging = false;
        var debugNumberOfFilesToKeep = 20;
        var openPullEnabled = false;
        if (ListOfNetObjectIDs.Count == 1)
        {
            using (var proxy = BusinessLayerFactory.Create())
            {
                var dummybool = false;
                if (bool.TryParse(proxy.GetStorageArrayProperty(ListOfNetObjectIDs[0].ID, SRMConstants.CustomLoggingEnabled), out dummybool))
                {
                    debugLogging = dummybool;
                }

                var dummyint = 0;
                if (int.TryParse(proxy.GetStorageArrayProperty(ListOfNetObjectIDs[0].ID, SRMConstants.NumberOfLogFilesToKeep), out dummyint))
                {
                    debugNumberOfFilesToKeep = dummyint;
                }

                if (isOpenAndPullVisible) 
                { 
                    dummybool = false;
                    if (bool.TryParse(proxy.GetStorageArrayProperty(ListOfNetObjectIDs[0].ID, SRMConstants.OpenAndPullEnabled), out dummybool))
                    {
                        openPullEnabled = dummybool;
                    }
                    else 
                    {
                        openPullEnabled = true;
                    }
                }
            }
        }
        ddlDebugLogging.SelectedIndex = debugLogging ? 0 : 1;
        ddlOpenAndPullEnabled.SelectedIndex = openPullEnabled ? 0 : 1;
        txtDebugFiles.Text = debugNumberOfFilesToKeep.ToString(CultureInfo.InvariantCulture);
        RangeValidator.ErrorMessage = Resources.SrmWebContent.EditPropertiesLogMaxFilesRangeValidator;
        lblAdvanced.Text = Resources.SrmWebContent.EditPropertiesAdvanced;
        lblDebugLogging.Text = Resources.SrmWebContent.EditPropertiesAdvancedDebugLogging;
        lblNumberOfFiles.Text = Resources.SrmWebContent.EditPropertiesAdvancedNumberOfFilesToKeep;
        lblDebugFilesLabel.Text = Resources.SrmWebContent.EditPropertiesAdvancedNrFilesLabel;
        lblOpenAndPullEnabled.Text = Resources.SrmWebContent.EditPropertiesAdvancedOpenAndPull;
    }

    public string ValidateJSFunction()
    {
        return null; 
    }

    public bool Validate(bool connectionPropertyChanged)
    {
        if (multipleAdvChange.Visible && !multipleAdvChange.Checked)
            return true;

        return Page.IsValid;
    }

    public KeyValuePair<string, string>[] GetWarnings()
    {
        return null;
    }

    public void Update(bool connectionPropertyChanged)
    {
        if (!ListOfNetObjectIDs.Any() || (multipleAdvChange.Visible && !multipleAdvChange.Checked))
        {
            return;
        }

        var idsToUpdate = ListOfNetObjectIDs.Select(n => n.ID).ToArray();

        var customLoggingEnabled = ddlDebugLogging.SelectedIndex == 0;
        var openAndPullEnabled = ddlOpenAndPullEnabled.SelectedIndex == 0;
        var customLogFiles = int.Parse(txtDebugFiles.Text);
        var properties = new Dictionary<string, string> {{SRMConstants.CustomLoggingEnabled, customLoggingEnabled.ToString()}, 
                                                         {SRMConstants.NumberOfLogFilesToKeep, customLogFiles.ToString(CultureInfo.InvariantCulture)},
                                                         {SRMConstants.OpenAndPullEnabled, openAndPullEnabled.ToString()}};
        using (var proxy = BusinessLayerFactory.Create())
        {
            foreach (var id in idsToUpdate)
            {
                proxy.UpdateStorageArrayProperties(id, properties);
            }
        }
    }

    protected void Customvalidator1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        int result;
        if (int.TryParse(txtDebugFiles.Text, out result))
        {
            if (1 <= result && result <= 100)
                args.IsValid = true;
            else
                args.IsValid = false;
            return;
        }
        args.IsValid = false;
    }
}