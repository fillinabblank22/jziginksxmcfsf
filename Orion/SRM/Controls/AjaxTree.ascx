﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AjaxTree.ascx.cs" Inherits="Orion_SRM_Controls_AjaxTree" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="SolarWinds.SRM.Web" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<orion:Include ID="Include1" runat="server" Module="SRM" File="AjaxTree.css" />
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.Formatters.js" />

<asp:ScriptManagerProxy id="TreeScriptManager" runat="server">
	<Services>
		<asp:ServiceReference />
	</Services>
	<Scripts>
		<asp:ScriptReference Path="/Orion/SRM/js/AjaxTree.js" />
	</Scripts>
</asp:ScriptManagerProxy>

<div id="ResourceVisibilityDiv" >
	<orion:resourceWrapper runat="server" ID="Wrapper">
		<Content>
		    <script type="text/javascript">
			$(function () {
			    function VisibilityChangedHandler (isVisible) {
			        var visibility = isVisible ? 'visible' : 'collapse';
			        var display = isVisible ? 'block' : 'none';

			        $('#ResourceVisibilityDiv').css('visibility', visibility);
			        $('#ResourceVisibilityDiv').css('display', display);
			    }

			    SRM.AjaxTree.Load(eval('<%=this.ServiceName %>'),
		                                    '<%=this.tree.ClientID %>',
		                                    '<%=this.ResourceId %>',
		                                    'SRMTreeNode-',
		                                    'TreeNode',
		                                    'NoDataContent',
		                                    '<%=this.RememberExpandedGroups.ToString(CultureInfo.InvariantCulture).ToLower() %>',
		                                    '<%=BatchSize %>',
		                                    "<%=this.Filters %>",
		                                    '<%=this.ShowMoreSingularMessage %>',
		                                    '<%=this.ShowMorePluralMessage %>',
		                                    false,
		                                    true,
			                                '<%=this.ExpandRootLevel.ToString(CultureInfo.InvariantCulture).ToLower() %>',
			                                '<%=false %>',
		                                    <%= JsonConvert.SerializeObject(Grouping) %>,
		                                    '<%=this.InitialValue %>',
		                                    VisibilityChangedHandler,
		                                    '<%=this.ExpandAll %>');
			});
		    </script>
	
			<div ID="tree" runat="server">
			<img alt="Loading..." src="/Orion/images/AJAX-Loader.gif" /> <span><%= Resources.SrmWebContent.SummaryDetails_Storages_loadingLabel %></span>
			</div>

		    <div id="SRMTreeNode-<%=this.ResourceId %>-TreeNode" class="SRM-AjaxTree-HiddenElement">
		    <div class="SRM-TreeNode-itemHeader">
		        <span class="SRM-TreeNode-itemDescription"></span>
		        <a href="javascript:void(0);" class="SRM-TreeNode-expandLink">
				    <span class="SRM-TreeNode-expandButtonWrapper">
		                <img src="/Orion/images/Button.Expand.gif" class="SRM-TreeNode-expandButton" />
				    </span>
				</a>
		        <a class="SRM-TreeNode-detailsLink">
		              <img id="SRM-TreeNode-statusIcon" class="SRM-TreeNode-statusIcon"/>
				      <span class="SRM-TreeNode-itemName"></span>
		        </a>
		        <div class="SRM-TreeNode-graph">		
                    <table class="SRM-AjaxTree-HiddenElement">
                        <tr>
                            <td class="BackgroundRect" />
                            <td class="ForegroundRect" />
                        </tr>
			        </table> 
                    <span id="SRM-TreeNode-size" class="SRM-AjaxTree-HiddenElement"></span>
				    <span id="SRM-TreeNode-type" class="SRM-AjaxTree-HiddenElement"></span>
		        </div>
		        <div class="SRM-Cleaner"></div>
		    </div>
			    <div class="SRM-TreeNode-children" class="SRM-AjaxTree-HiddenElement"></div>
			</div>
		</Content>	
	</orion:resourceWrapper>
</div>
