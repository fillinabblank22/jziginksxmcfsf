﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ThresholdControl.ascx.cs" Inherits="Orion_SRM_Controls_ThresholdControl" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Models" %>

<%@ Register Src="~/Orion/Controls/ThresholdControl.ascx" TagPrefix="orion" TagName="ThresholdControl" %>
<%@ Register Src="~/Orion/Controls/BaselineDetails.ascx" TagPrefix="orion" TagName="BaselineDetails" %>

<style type="text/css">
     .leftLabelColumn { width: 247px }
     .linktoThresholds { float: right}
     .linkToThresholdsTable { width: 100% }
</style>

<table class="linkToThresholdsTable">
    <tr>
        <td><%= Resources.SrmWebContent.EditThresholdControl_Title %></td>
        <td class="linktoThresholds">
            <span>
                <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="" />
                <a href="localhost" target="_blank" rel="noopener noreferrer" class="SRM_manageLink" id="thresholdLink" runat="server">
                    <%= Resources.SrmWebContent.EditThresholdControl_Global_Settings_Link_Title%></a>
            </span>
        </td>
    </tr>
</table>
<asp:Repeater runat="server" ID="repThresholds" OnItemDataBound="repThresholds_ItemDataBound">
    <ItemTemplate>
         <orion:ThresholdControl EnableViewState="True" runat="server" ID="thresholdControl"
            DisplayName='<%#((SrmThresholds)Container.DataItem).DisplayName%>' 
            ThresholdName='<%#((SrmThresholds)Container.DataItem).BaseThreshold.ThresholdName%>' 
            Unit='<%#((SrmThresholds)Container.DataItem).Unit%>' 
            />
    </ItemTemplate>
</asp:Repeater>

<orion:BaselineDetails runat="server" ID="BaselineDetails" />