﻿using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Common.Models;


public partial class Orion_SRM_Controls_PollingPropertiesVserver : System.Web.UI.UserControl, ICreateEditControl, IEditControl
{
    readonly List<string> supportedNetObjects = new List<string>(new[] { SolarWinds.SRM.Web.NetObjects.VServer.NetObjectPrefix });

    public List<string> ListOfNetObjectIDs;
    private int smvsID;
    private VServerEntity vserver;
    protected VServerStatus VServerStatus { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        image.ImageUrl = "/orion/srm/images/NetApp_Cluster_Array.png";
        loadingIconSmisProvider.AlternateText = SrmWebContent.EditProperties_SmisProvider_Loading;
    }

    public IEditControl InitFromNetObjects(List<string> listOfNetObjectIDs)
    {

        var doesNotSupportOne = listOfNetObjectIDs.Exists(n => !supportedNetObjects.Contains(n.Split(':')[0]));
        if (doesNotSupportOne)
        {
            return null;
        }

        smvsID = int.Parse(listOfNetObjectIDs[0].Split(':')[1]);
        vserver = ServiceLocator.GetInstance<IVServerDAL>().GetVServerDetails(smvsID);

        FillControl();
        return this;
    }

    public void FillControl()
    {
        txtserverType.Text = SrmWebContent.EditProperties_VServer_Type;
        VServerStatus = vserver.Status;
        var parentArray = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayDetails(vserver.StorageArrayID);
        ClusterHyperLink.Text = parentArray.DisplayName;
        ClusterHyperLink.NavigateUrl = InvariantString.Format("/Orion/View.aspx?NetObject={0}:{1}", SolarWinds.SRM.Web.NetObjects.StorageArray.NetObjectPrefix, parentArray.StorageArrayId);
    }

    public string ValidateJSFunction()
    {
        return null;
    }

    public bool Validate(bool connectionPropertyChanged)
    {
        return true;
    }

    public KeyValuePair<string, string>[] GetWarnings()
    {
        return null;
    }

    public void Update(bool connectionPropertyChanged)
    {

    }
}