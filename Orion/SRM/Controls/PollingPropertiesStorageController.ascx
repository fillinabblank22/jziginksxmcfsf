﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/Orion/SRM/Controls/PollingPropertiesStorageController.ascx.cs" Inherits="Orion_SRM_Controls_PollingPropertiesStorageController" %>
<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include runat="server" File="OrionCore.js" Section="Bottom" SpecificOrder="1" />
<orion:Include runat="server" Module="SRM" File="SRM.Common.js" />
<orion:Include runat="server" Module="SRM" File="SRM.css" />

<div class="PollingPropertiesSection">
    <div class="row" runat="server">
        <asp:Label runat="server" class="label"><%= Resources.SrmWebContent.EditProperties_StorageControllerPollingFlag %></asp:Label>
        <asp:CheckBox runat="server" CssClass="labelCheckbox" ID="controllerPollingEnabledChbox" ClientIDMode="Static" />
        <br style="clear:left"/>
    </div>
</div>
