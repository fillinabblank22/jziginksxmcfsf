﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Dependencies.ascx.cs" Inherits="Orion_SRM_Controls_Dependencies" %>
<%@ Register TagPrefix="orion" TagName="Dependency" Src="~/Orion/Nodes/Controls/DependenciesControl.ascx" %>

<div class="editNameSection">
    <orion:Dependency ID="dependenciesBox" ClientIDMode="Static" runat="server" />
</div>