﻿//using EO.Pdf.Internal;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.Thresholds;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.SRM.Web.Resources;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.Orion.Core.Common.Models.Thresholds;
using CoreThreshold = SolarWinds.Orion.Core.Common.Models.Thresholds.Threshold;

public partial class Orion_SRM_Controls_ThresholdControl : System.Web.UI.UserControl, ICreateEditControl, IEditControl, IValidationMessage
{
    private static readonly Log _log = new Log();
    private string _entityType;
    private List<int> _netObjectList = new List<int>();
    private List<SrmThresholds> _thresholds;
    private TemplatePropertyAvailabilityHelper templateAvailability;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Visible)
            {
                repThresholds.DataSource = _thresholds = GetThresholds();
                repThresholds.DataBind();
            }
        }
    }

    protected void repThresholds_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var control = e.Item.FindControl("thresholdControl") as ThresholdControl;
            if (control == null)
                return;

            var item = e.Item.DataItem as SrmThresholds;
            if (item == null) throw new InvalidCastException("'item' must be type of 'SrmThresholds'");

            control.ObjectIDs = item.BaseThreshold.InstanceId.ToString(CultureInfo.InvariantCulture);
            control.ThresholdName = item.BaseThreshold.ThresholdName;
            control.ThisThresholdType = item.BaseThreshold.ThresholdType;
            control.ThisThresholdOperator = item.BaseThreshold.ThresholdOperator;
            control.GlobalWarningValue = item.GlobalWarningValue.HasValue ? item.GlobalWarningValue.Value.ToString(CultureInfo.InvariantCulture) : null;
            control.GlobalCriticalValue = item.GlobalCriticalValue.HasValue ? item.GlobalCriticalValue.Value.ToString(CultureInfo.InvariantCulture) : null;

            if (item.BaseThreshold.ThresholdType == ThresholdType.Static)
            {
                control.WarningValue = item.Level1Value.HasValue ? item.Level1Value.Value.ToString(CultureInfo.InvariantCulture) : null;
                control.CriticalValue = item.Level2Value.HasValue ? item.Level2Value.Value.ToString(CultureInfo.InvariantCulture) : null;
            }
            else if (item.BaseThreshold.ThresholdType == ThresholdType.Dynamic)
            {
                control.WarningValue = item.Level1Formula;
                control.CriticalValue = item.Level2Formula;
            }
        }
    }
    
    private void SelectNetObjectType(string netObjectPrefix)
    {
        switch (netObjectPrefix)
        {
            case Lun.NetObjectPrefix :
                _entityType = "Orion.SRM.LUNs";
                thresholdLink.HRef = @"/Orion/SRM/Admin/LunThreshold.aspx";
                break;
            case Volume.NetObjectPrefix:
                _entityType = "Orion.SRM.Volumes";
                thresholdLink.HRef = @"/Orion/SRM/Admin/NasThreshold.aspx";
                break;
            case VServer.NetObjectPrefix:
                _entityType = "Orion.SRM.Vservers";
                thresholdLink.HRef = @"/Orion/SRM/Admin/VserverThreshold.aspx";
                break;
            case StorageArray.NetObjectPrefix:
                _entityType = "Orion.SRM.StorageArrays";
                thresholdLink.HRef = @"/Orion/SRM/Admin/ArrayThreshold.aspx";
                break;
            case Pool.NetObjectPrefix:
                _entityType = "Orion.SRM.Pools";
                thresholdLink.HRef = @"/Orion/SRM/Admin/PoolThreshold.aspx";
                break;
            case StorageController.NetObjectPrefix:
                _entityType = "Orion.SRM.StorageControllers";
                thresholdLink.HRef = @"/Orion/SRM/Admin/StorageControllerThreshold.aspx";
                break;
            default :
                Visible = false;
                break;
        }
    }

    public IEditControl InitFromNetObjects(List<string> listOfNetObjectIDs)
    {
        if (listOfNetObjectIDs.Count > 0)
        {
            string allowedObjectType = listOfNetObjectIDs.First().Split(':')[0];
            SelectNetObjectType(allowedObjectType);
            foreach (var netObject in listOfNetObjectIDs)
            {
                if( allowedObjectType == netObject.Split(':')[0])
                    _netObjectList.Add(Convert.ToInt32(netObject.Split(':')[1]));
            }
        }
        return this;
    }

    public string ValidateJSFunction()
    {
        return null;
    }

    public bool Validate(bool connectionPropertyChanged)
    {
        foreach (RepeaterItem threshold in repThresholds.Items)
        {
            var control = threshold.FindControl("thresholdControl") as ThresholdControl;
            if (control != null && !control.IsValid)
                return false;
        }
        return true;
    }

    public KeyValuePair<string, string>[] GetWarnings()
    {
        return null;
    }

    private void GetTemplateAvailablility()
    {
        templateAvailability = new TemplatePropertyAvailabilityHelper(this.Request.QueryString.Get("NetObject"));
        using (var proxy = BusinessLayerFactory.Create())
        {
            templateAvailability.SetTemplatesAvailability(proxy.GetPropertyAvailability, new List<string>() { TemplatePropertyAvailabilityHelper.PerformanceCategory, TemplatePropertyAvailabilityHelper.ControllerCategory });
        }
    }

    private List<SrmThresholds> GetThresholds()
    {
        int netObjectId = _netObjectList.FirstOrDefault();
        var thresholds = ServiceLocator.GetInstance<IThresholdDAL>().GetThresholds(_entityType, netObjectId);

        Dictionary<string, SrmThresholds> templateProperties = ThresholdControlHelper.GetTemplateProperties(thresholds);

        if(templateAvailability == null)
            GetTemplateAvailablility();

        var availableThreshold = (from templateProperty in templateProperties
                                  where templateAvailability.IsPropertyAvailable(templateProperty.Key)
                                  select templateProperty.Value).ToList();

        return availableThreshold;
    }

    private bool IsExistingThreshold(CoreThreshold threshold)
    {
        if (!Visible)
            return false;
        if (_thresholds == null)
            _thresholds = GetThresholds();
        if (_thresholds != null)
        {
            var findedThreshold = _thresholds.FindAll(
                item =>
                    item.BaseThreshold.InstanceId == threshold.InstanceId && item.BaseThreshold.ThresholdType == threshold.ThresholdType &&
                    item.BaseThreshold.ThresholdName == threshold.ThresholdName);

            if (threshold.ThresholdType != ThresholdType.Global)
            {
                var findedWithThresholdOperator =
                    findedThreshold.FindAll(item => item.BaseThreshold.ThresholdOperator == threshold.ThresholdOperator);
                if (threshold.ThresholdType == ThresholdType.Static)
                {
                    return
                        findedWithThresholdOperator.Exists(
                            item =>
                                item.Level1Value.ToString() ==
                                SolarWinds.Orion.Core.Common.Thresholds.ThresholdsHelper.FromNumeric(threshold.Warning) &&
                                item.Level2Value.ToString() ==
                                SolarWinds.Orion.Core.Common.Thresholds.ThresholdsHelper.FromNumeric(threshold.Critical));
                }

                if (threshold.ThresholdType == ThresholdType.Dynamic)
                {
                    return
                        findedWithThresholdOperator.Exists(
                            item =>
                                item.Level1Formula == threshold.WarningFormula &&
                                item.Level2Formula == threshold.CriticalFormula);
                }
                return findedWithThresholdOperator.Count > 0;
            }
            return findedThreshold.Count > 0;
        }
        return false;
    }

    public void Update(bool connectionPropertyChanged)
    {
        var thresholdsToSave = new List<CoreThreshold>();
        foreach (RepeaterItem threshold in repThresholds.Items)
        {
            var control = threshold.FindControl("thresholdControl") as ThresholdControl;
            if (control != null)
            {
                var item = new CoreThreshold();

                item.InstanceId = Convert.ToInt32(control.ObjectIDs);
                item.ThresholdType = control.ThisThresholdType;
                item.ThresholdOperator = (control.ThisThresholdType == ThresholdType.Global) ? control.GlobalThresholdOperator : control.ThisThresholdOperator;
                item.ThresholdName = control.ThresholdName;
                
                double warningValue;
                double criticalValue;

                if (item.ThresholdType == ThresholdType.Dynamic)
                {
                    if (double.TryParse(control.WarningValue, out warningValue) &&
                        double.TryParse(control.CriticalValue, out criticalValue))
                    {
                        item.ThresholdType = ThresholdType.Static;
                        item.Warning = warningValue;
                        item.Critical = criticalValue;
                    }
                    else
                    {
                        item.ThresholdType = ThresholdType.Dynamic;
                        item.WarningFormula = control.WarningValue;
                        item.CriticalFormula = control.CriticalValue;

                        ComputeThresholdValues(item);
                    }
                }
                thresholdsToSave.Add(item);
            }
        }
        using (
            ICoreBusinessLayer proxy =
                CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(BusinessLayerExceptionHandler))
        {
            foreach (var coreThresholds in thresholdsToSave)
            {
                if (!IsExistingThreshold(coreThresholds))
                {
                    foreach (var netObject in _netObjectList)
                    {
                        coreThresholds.InstanceId = netObject;
                        proxy.SetThreshold(coreThresholds);    
                    }
                }
            }
        }
    }

    private static void ComputeThresholdValues(CoreThreshold threshold)
    {
        try
        {
            var request = new SolarWinds.Orion.Web.Model.Thresholds.ComputeThresholdRequest()
            {
                ThresholdName = threshold.ThresholdName,
                InstancesId = new[] {threshold.InstanceId},
                Operator = threshold.ThresholdOperator,
                CriticalFormula = threshold.CriticalFormula,
                WarningFormula = threshold.WarningFormula
            };
            var response = new SolarWinds.Orion.Web.Controllers.ThresholdsController().Compute(request);
            if (response.IsComputed)
            {
                threshold.Warning = response.WarningThreshold;
                threshold.Critical = response.CriticalThreshold;
            }
        }
        catch (Exception e)
        {
            _log.Error(string.Format("Can't compute current values for threshold {0}.", threshold), e);
        }
    }

    protected void BusinessLayerExceptionHandler(Exception ex)
    {
        _log.Error(ex);
    }

    public string ValidationMessage
    {
        get { return Resources.SrmWebContent.EditThresholdControl_Validation_WarningMessage; }
    }
}