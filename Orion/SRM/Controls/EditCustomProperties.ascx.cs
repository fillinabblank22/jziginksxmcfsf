﻿using System.Linq;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Web.Interfaces;
using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.SRM.Web;


public partial class Orion_SRM_Controls_EditCustomProperties : UserControl, ICreateEditControl, IEditControl, IConfigurationControl
{
    private Log log = new Log();
    
    static readonly List<string> supportedNetObjects = new List<string>(new[] { VServer.NetObjectPrefix, StorageArray.NetObjectPrefix, FileShares.NetObjectPrefix, Lun.NetObjectPrefix, Pool.NetObjectPrefix, StorageProvider.NetObjectPrefix, Volume.NetObjectPrefix });
    public List<string> ListOfNetObjectIDs;
    private string CustomPropertyTableName { get; set; }
    private string CustomPropertyKeyColumn { get; set; }
    private string SrmEntityName { get; set; }
    private Func<int, NetObject> getNetobject { get; set; }

    public IEditControl InitFromNetObjects(List<string> listOfNetObjectIDs)
    {
        var distinctNetObjects = listOfNetObjectIDs.Select(x => x.Split(':')[0]).Distinct();
        if (distinctNetObjects.Count() > 1)
            return null;

        var doesNotSupportOne = listOfNetObjectIDs.Exists(n => !supportedNetObjects.Contains(n.Split(':')[0]));
        if (doesNotSupportOne)
        {
            return null;
        }
        this.ListOfNetObjectIDs = listOfNetObjectIDs;

        return this;
    }
    
    public string ValidateJSFunction()
    {
        return null;
    }

    public bool Validate(bool connectionPropertyChanged)
    {
        return CustomProperties.Validate();
    }

    public KeyValuePair<string, string>[] GetWarnings()
    {
        return null;
    }

    public void Update(bool connectionPropertyChanged)
    {
        using (var proxy = BusinessLayerFactory.Create())
        {
            var propertiesToUpdate = CustomProperties.CustomProperties;

            var entityUris =
                ListOfNetObjectIDs.Select(
                    netObject => SolarWinds.SRM.Common.Helpers.SwisUriHelper.GetUri(SrmEntityName, CustomPropertyKeyColumn, int.Parse(netObject.Split(':')[1])) )
                    .ToArray();

            proxy.UpdateEntityCustomProperties(entityUris, propertiesToUpdate);
        }

        foreach (var cp in CustomProperties.CustomProperties)
        {
            if (cp.Value != null && !string.IsNullOrEmpty(cp.Value.ToString())) //no need to run update query for empty cps
            {
                // update restricted values if needed
                var customProperty = CustomPropertyMgr.GetCustomProperty(CustomPropertyTableName, cp.Key);
                CustomPropertyHelper.UpdateRestrictedValuesIfNeeded(customProperty,
                    new[] { CustomPropertyHelper.GetInvariantCultureString((cp.Value != null) ? cp.Value.ToString() : string.Empty, customProperty.PropertyType) });
            }
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        var listId = new List<int>();
        foreach (var item in ListOfNetObjectIDs)
        {
            listId.Add(int.Parse(item.Split(':')[1]));
        }

        // We support just one NetObject type
        this.SetCustomProperties(ListOfNetObjectIDs[0].Split(':')[0]);

        CustomProperties.CustomPropertiesTable = CustomPropertyTableName;
        CustomProperties.EntityIds = listId;

        CustomProperties.CustomPropertyLoader = (netObjectId, displayProperties) =>
        {
            NetObject netObject = this.getNetobject(netObjectId);
            var values = CustomPropertyHelper.GetCustomPropertiesForObject(netObjectId, displayProperties, this.SrmEntityName, this.CustomPropertyKeyColumn, netObject);
            if (values.Count == 0)
            {
                this.Visible = false;
            }
            return values;
        };
    }

    private void SetCustomProperties(string netObject)
    {
        switch (netObject)
        {
            case VServer.NetObjectPrefix:
                {
                    CustomPropertyTableName = "SRM_VServersCustomProperties";
                    CustomPropertyKeyColumn = "VServerID";
                    SrmEntityName = SwisEntities.VServers;
                    getNetobject = (netObjectId) => new VServer(netObjectId);
                    break;
                }
            case StorageArray.NetObjectPrefix:
                {
                    CustomPropertyTableName = "SRM_StorageArrayCustomProperties";
                    CustomPropertyKeyColumn = "StorageArrayID";
                    SrmEntityName = SwisEntities.StorageArrays;
                    getNetobject = (netObjectId) => new StorageArray(netObjectId);
                    break;
                }
            case Lun.NetObjectPrefix:
                {
                    CustomPropertyTableName = "SRM_LUNCustomProperties";
                    CustomPropertyKeyColumn = "LUNID";
                    SrmEntityName = SwisEntities.Lun;
                    getNetobject = (netObjectId) => new Lun(netObjectId);
                    break;
                }
            case FileShares.NetObjectPrefix:
                {
                    CustomPropertyTableName = "SRM_FileShareCustomProperties";
                    CustomPropertyKeyColumn = "FileShareID";
                    SrmEntityName = SwisEntities.FileShare;
                    getNetobject = (netObjectId) => new FileShares(netObjectId);
                    break;
                }
            case Pool.NetObjectPrefix:
                {
                    CustomPropertyTableName = "SRM_PoolCustomProperties";
                    CustomPropertyKeyColumn = "PoolID";
                    SrmEntityName = SwisEntities.Pools;
                    getNetobject = (netObjectId) => new Pool(netObjectId);
                    break;
                }
            case StorageProvider.NetObjectPrefix:
                {
                    CustomPropertyTableName = "SRM_ProviderCustomProperties";
                    CustomPropertyKeyColumn = "ProviderID";
                    SrmEntityName = SwisEntities.Providers;
                    getNetobject = (netObjectId) => new StorageProvider(netObjectId);
                    break;
                }
            case Volume.NetObjectPrefix:
                {
                    CustomPropertyTableName = "SRM_VolumeCustomProperties";
                    CustomPropertyKeyColumn = "VolumeID";
                    SrmEntityName = SwisEntities.Volume;
                    getNetobject = (netObjectId) => new Volume(netObjectId);
                    break;
                }
            default:
                {
                    log.Error("Not supported NetObject type passed to SRM EditCustomproperties:" + netObject);
                    throw new Exception("Not supported NetObject");
                }
        }
    }
    public string ValidationError { get; private set; }

    public void SaveValues()
    {
       this.Update(true);
    }

    public bool ValidateUserInput()
    {
        return Page.IsValid;
    }
}