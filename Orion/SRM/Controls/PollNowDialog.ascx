﻿<%@ Control Language="C#" CodeFile="PollNowDialog.ascx.cs" Inherits="Orion_SRM_Controls_PollNowDialog" %>
<%@ Register TagPrefix="orion" TagName="ProgressDialog" Src="~/Orion/Controls/ProgressDialog.ascx" %>
<orion:ProgressDialog ID="ProgressDialog" runat="server" /> 
<orion:include runat="server" File="NodeMNG.css" />
<style>
    .NetObjectText {
        padding-bottom: 10px;
    }
    .ItemProcceed {
        font-weight: bold;
    }
</style>

<asp:ScriptManagerProxy runat="server">
    <Services>
        <asp:ServiceReference path="~/Orion/SRM/Services/StorageManagementService.asmx" />
    </Services>
</asp:ScriptManagerProxy>

<script type="text/javascript">
    
        ProgressDlg =function (options) {
            var width = 600;
            var statusBox = $('<div></div>').addClass('StatusDialog').width(width);
            var statusContent = $('<div></div>').appendTo(statusBox);
            var progressBarOuter = $('<div></div>').addClass('ProgressBarOuter').appendTo(statusContent);
            var progressBarInner = $('<div></div>').addClass('ProgressBarInner').appendTo(progressBarOuter);
            var statusText = $('<div></div>').addClass('NetObjectText').appendTo(statusContent);
            var cancelButtonPlaceholder = $('<div></div>').addClass('NetObjectText').appendTo(statusContent);

            $('<orion:LocalizableButtonLink runat="server" LocalizedText="Cancel" DisplayType="Secondary" style="float:right; margin-right:5px;" />').appendTo(cancelButtonPlaceholder).click(function () { statusBox.dialog("destroy").remove(); });
            $('body').append(statusBox);
            var numFinished = 0, numSucceeded = 0, numErrors = 0, numOfItems = options.items.length;
            var onFinished = function (succeeded) {
                progressBarInner.width((++numFinished / numOfItems) * 100.0 + '%');
                if (succeeded)
                    numSucceeded++;
                else
                    numErrors++;
                statusText.html('&nbsp;&nbsp;<span class="ItemProcceed">' + String.format('<%= ControlHelper.EncodeJsString(Resources.SrmWebContent.ManageStorageObjects_PollNow) %>', numFinished, numOfItems) + '</span>');
                if (numFinished === numOfItems) {
                    statusText.html('&nbsp;&nbsp;<span class="ItemProcceed">' + String.format('<%= ControlHelper.EncodeJsString(Resources.SrmWebContent.ManageStorageObjects_PollFinished) %>') + '</span>');
                    if( numSucceeded > 0)
                        statusText.append('&nbsp;<span class="iconLink success">' + String.format('<%= ControlHelper.EncodeJsString(Resources.SrmWebContent.ManageStorageObjects_PollNowSucceed) %>', numSucceeded, numOfItems) + '</span>');
                    if (numErrors > 0)
                        statusText.append('&nbsp;<span class="iconLink failed">' + String.format('<%= ControlHelper.EncodeJsString(Resources.SrmWebContent.ManageStorageObjects_PollNowFailed) %>', numErrors, numOfItems) + '</span>');
                }
                if (succeeded && numSucceeded >= options.items.length)
                    setTimeout("$('.StatusDialog').fadeOut('slow', function() { $('.StatusDialog').dialog('destroy').remove(); })", 1000);
            };
            $.each(options.items, function () {
                var item = this;
                statusText.append('&nbsp;&nbsp;');
                console.log('item = ' + item + ', jobKey = ' + options.jobKey);
                if (options.jobKey == undefined) {
                    options.serverMethod(options.getArg(item), function () {
                        options.afterSucceeded(item);
                        onFinished(true);
                    }, function (error) {
                        onFinished(false);
                    });
                } else {
                    options.serverMethod(options.getArg(item), options.jobKey, function () {
                        options.afterSucceeded(item);
                        onFinished(true);
                    }, function (error) {
                        onFinished(false);
                    });
                }
                
            });

            statusBox.dialog({
                width: width, height: statusContent.height() + 115, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: options.title
            });
        }

        function showDialogPollNow(ids) {
            ProgressDlg({
                title: '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_4) %>',
                items: ids,
                serverMethod: StorageManagementService.PollNetObjNow,
                getArg: function(id) { return id; },
                sucessMessage: '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_5) %> <br />',
                failMessage: '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_6) %> <br />',
                afterSucceeded: function(id) { setTimeout(function() { window.location.reload(true); }, 9000); }
            });
            return false;
    }

        function showDialogPollForJobNow(ids, jobKey) {
            ProgressDlg({
                title: '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_4) %>',
                items: ids,
                serverMethod: StorageManagementService.PollByJobKeyNetObjNow,
                jobKey: jobKey,
                getArg: function(id) { return id; },
                sucessMessage: '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_5) %> <br />',
                failMessage: '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_6) %> <br />',
                afterSucceeded: function(id) { setTimeout(function() { window.location.reload(true); }, 9000); }
            });
            return false;
        }

</script>