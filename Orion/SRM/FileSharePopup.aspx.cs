﻿using System;
using System.Linq;
using System.Web.UI;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;
using System.Globalization;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Core.Web;


public partial class Orion_SRM_FileSharePopup : Page
{
    protected FileShares FileShares { get; set; }
    public string StatusText { get; set; }
    protected int WarningThresholdDays { get { return (int)SettingsDAL.GetSetting(SRMConstants.WarningThresholdDays).SettingValue; } }
    protected int CriticalThresholdDays { get { return (int)SettingsDAL.GetSetting(SRMConstants.CriticalThresholdDays).SettingValue; } }
    public int CapacityRunout { get; set; }

    protected override void OnPreInit(EventArgs e)
    {
        string netObjectID = Request.QueryString["NetObject"];
        try
        {
            this.FileShares = NetObjectFactory.Create(netObjectID) as FileShares;
        }
        catch (AccountLimitationException)
        {
            Server.Transfer(
                string.Format(CultureInfo.InvariantCulture, "/Orion/SRM/AccountLimitationPopup.aspx?NetObject={0}", netObjectID));
        }

        base.OnPreInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.FileShares != null)
        {
            FileSharesEntity fileShares = this.FileShares.FileSharesEntity;
            var fileServerIdentification = ServiceLocator.GetInstance<IFileServerIdentificationDAL>().GetFileServerIdentificationByFileServerId(fileShares.FileServerID).FirstOrDefault();
            var volume = ServiceLocator.GetInstance<IVolumeDAL>().GetVolumeDetails(fileShares.VolumeID);
            var array = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayDetails(volume.StorageArrayID);
            var ipAddress = fileServerIdentification != null ? fileServerIdentification.IpAddress : string.Empty;
            this.HeaderLabel.Text = fileShares.DisplayName;
            this.StatusLabel.Text = FormatHelper.MakeBreakableString(string.Format(CultureInfo.CurrentCulture, "{0} {1}", SrmWebContent.File_Share_Popup_Share_Name, SrmStatusLabelHelper.GetStatusLabel(volume.Status)));
            WebHelper.SetValueOrHide(VendorStatusLabel, VendorStatusRow, volume.VendorStatusDescription);
            this.NameLabel.Text = fileShares.DisplayName;
            this.VolumeLabel.Text = volume.DisplayName;
            VolumeRow.Visible = !String.IsNullOrEmpty(VolumeLabel.Text);
            if (array.IsCluster)
            {
                ClusterLabel.Text = array.DisplayName;
                var vServer = ServiceLocator.GetInstance<IVServerDAL>().GetVServersByVolumeId(volume.VolumeID);
                if (vServer != null)
                    this.VServerLabel.Text = String.Join(", ", vServer.Select(x => x.DisplayName));
            }
            else
                ArrayLabel.Text = array.IsCluster ? String.Empty : array.DisplayName;
            ArrayRow.Visible = !String.IsNullOrEmpty(ArrayLabel.Text);
            VserverRow.Visible = !String.IsNullOrEmpty(VServerLabel.Text);
            ClusterRow.Visible = !String.IsNullOrEmpty(ClusterLabel.Text);

            CapacityRunout = WebHelper.GetDayDifference(volume.CapacityRunout);
            ProjectedRunOutLabel.Text = WebHelper.FormatDaysDiff(CapacityRunout);
            WebHelper.MarkProjectedRunOut(this.ProjectedRunOutLabel, CapacityRunout, WarningThresholdDays, CriticalThresholdDays);

            var volumePerformance = ServiceLocator.GetInstance<IVolumeDAL>().GetVolumePerformance(volume.VolumeID);
            var dal = ServiceLocator.GetInstance<IThresholdDAL>();
            this.TotalIopsLabel.Text = volumePerformance.TotalIOPS.HasValue
                ? string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(volumePerformance.TotalIOPS.Value, 2, MidpointRounding.AwayFromZero), Resources.SrmWebContent.PopUp_IOPSUnit)
                : SrmWebContent.Value_Unknown;
            WebHelper.MarkThresholds(TotalIopsLabel, volume.IOPSTotal, dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeIOPSTotal, volume.VolumeID));
            this.TotalThroughputLabel.Text = volumePerformance.TotalThroughput.HasValue
                ? string.Format(CultureInfo.CurrentCulture, "{0} {1}", WebHelper.FormatCapacityTotal(volumePerformance.TotalThroughput.Value), Resources.SrmWebContent.PopUp_secUnit)
                : SrmWebContent.Value_Unknown;
            WebHelper.MarkThresholds(TotalThroughputLabel, volume.BytesPSTotal, dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeThroughputTotal, volume.VolumeID));
            this.TotalLatencyLabel.Text = volumePerformance.Latency.HasValue
                ? string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(volumePerformance.Latency.Value, 2, MidpointRounding.AwayFromZero), Resources.SrmWebContent.PopUp_msUnit)
                : SrmWebContent.Value_Unknown;
            WebHelper.MarkThresholds(TotalLatencyLabel, volume.IOLatencyTotal, dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeLatencyTotal, volume.VolumeID));

            this.VolumeCapacityLabel.Text = WebHelper.FormatCapacityTotal(volume.CapacityTotal);
            this.QuotaCapacityLabel.Text = WebHelper.FormatCapacityTotal(fileShares.Quota);
            this.UsedCapacityLabel.Text = WebHelper.FormatCapacityTotal(fileShares.ShareOrVolumeUsedCapacity);
            WebHelper.MarkThresholds(UsedCapacityLabel, (float)fileShares.UsedCapacityPercent, dal.GetThresholdDynamicOrGlobal(ThresholdDataType.FileShareFileSystemUsedCapacityPercent, fileShares.FileShareID));

            popupUserCapacity.ChartColors = new[] { PopupChartHelper.BlueChartColor, PopupChartHelper.GradientChartColor };
            popupUserCapacity.ChartLabelColors = new[] { PopupChartHelper.BluetFontColor, PopupChartHelper.GradientFontColor };
            popupUserCapacity.Values = new[] { fileShares.ShareOrVolumeUsedCapacity ?? 0, fileShares.QuotaOrVolumeTotalCapacity - (fileShares.ShareOrVolumeUsedCapacity ?? 0) };
            popupUserCapacity.Total = volume.CapacityTotal;
            popupUserCapacity.RenderTotalLabel = false;
            popupUserCapacity.Height = PopupChartHelper.Height;

            #region Other potential issues - Marked by thresholds

            IOPSReadLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(volume.IOPSRead ?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.VolumeDetails_IOPS_PS);
            WebHelper.MarkThresholdAndShowRow(IOPSReadLabel, IOPSReadRow, volume.IOPSRead,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeIOPSRead, volume.VolumeID));

            IOPSWriteLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(volume.IOPSWrite ?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.VolumeDetails_IOPS_PS);
            WebHelper.MarkThresholdAndShowRow(IOPSWriteLabel, IOPSWriteRow, volume.IOPSWrite,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeIOPSWrite, volume.VolumeID));

            ThroughputReadLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", WebHelper.FormatCapacityTotal(volume.BytesPSRead), SrmWebContent.VolumeDetails_PerSecond);
            WebHelper.MarkThresholdAndShowRow(ThroughputReadLabel, ThroughputReadRow, volume.BytesPSRead,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeThroughputRead, volume.VolumeID));

            ThroughputWriteLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", WebHelper.FormatCapacityTotal(volume.BytesPSWrite), SrmWebContent.VolumeDetails_PerSecond);
            WebHelper.MarkThresholdAndShowRow(ThroughputWriteLabel, ThroughputWriteRow, volume.BytesPSWrite,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeThroughputWrite, volume.VolumeID));

            LatencyReadLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(volume.IOLatencyRead ?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.VolumeDetails_LatencyTotal_Unit);
            WebHelper.MarkThresholdAndShowRow(LatencyReadLabel, LatencyReadRow, volume.IOLatencyRead,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeLatencyRead, volume.VolumeID));

            LatencyWriteLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(volume.IOLatencyWrite ?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.VolumeDetails_LatencyTotal_Unit);
            WebHelper.MarkThresholdAndShowRow(LatencyWriteLabel, LatencyWriteRow, volume.IOLatencyWrite,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeLatencyWrite, volume.VolumeID));

            IOSizeTotalLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0}", WebHelper.FormatCapacityTotal(volume.IOSizeTotal));
            WebHelper.MarkThresholdAndShowRow(IOSizeTotalLabel, IOSizeTotalRow, volume.IOSizeTotal,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeIOSizeTotal, volume.VolumeID));

            IOSizeReadLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0}", WebHelper.FormatCapacityTotal(volume.IOSizeRead));
            WebHelper.MarkThresholdAndShowRow(IOSizeReadLabel, IOSizeReadRow, volume.IOSizeRead,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeIOSizeRead, volume.VolumeID));

            IOSizeWriteLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0}", WebHelper.FormatCapacityTotal(volume.IOSizeWrite));
            WebHelper.MarkThresholdAndShowRow(IOSizeWriteLabel, IOSizeWriteRow, volume.IOSizeWrite,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeIOSizeWrite, volume.VolumeID));

            if (!ThroughputReadRow.Visible && !ThroughputWriteRow.Visible && !IOSizeTotalRow.Visible && !IOSizeReadRow.Visible && !IOSizeWriteRow.Visible && !IOPSReadRow.Visible && !IOPSWriteRow.Visible && !LatencyReadRow.Visible && !LatencyWriteRow.Visible)
                OtherPotentialIssuesSection.Visible = false;

            #endregion

        }
    }
}