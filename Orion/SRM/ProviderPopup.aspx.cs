﻿using System;
using System.Globalization;
using System.Web.UI;
using Resources;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Web;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Web.NetObjects;

public partial class Orion_SRM_ProviderPopup : Page
{
    protected StorageProvider StorageProvider { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.StorageProvider = NetObjectFactory.Create(Request["NetObject"], true) as StorageProvider;
        if (StorageProvider != null)
        {
            var strProviderType = String.Format("{0} {1}", StorageProvider.Provider.ProviderType.GetDescription(), SrmWebContent.Provider_Popup_Provider_Name);
            StatusLabel.Text = FormatHelper.MakeBreakableString(string.Format(CultureInfo.CurrentCulture, "{0} {1}", strProviderType, SrmStatusLabelHelper.GetStatusLabel(StorageProvider.Provider.Status)));
            ProviderVersionLabel.Text = FormatHelper.MakeBreakableString(StorageProvider.Provider.Version ?? string.Empty);
            LastSyncTimeLabel.Text = StorageProvider.Provider.LastSync.HasValue ? Utils.FormatDateTimeForDisplayingOnWebsite(StorageProvider.Provider.LastSync.Value.ToLocalTime(), true) : String.Empty;
            NameRow.Visible = !String.IsNullOrEmpty(StorageProvider.Name);
            CimomVersionRow.Visible = !String.IsNullOrEmpty(CimomVersionLabel.Text);
            ProviderVersionRow.Visible = !String.IsNullOrEmpty(ProviderVersionLabel.Text);
            SyncIntervalRow.Visible = !String.IsNullOrEmpty(SyncIntervalLabel.Text);
            LastSyncTimeRow.Visible = !String.IsNullOrEmpty(LastSyncTimeLabel.Text);
        }
        
    }
}