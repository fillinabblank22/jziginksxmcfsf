﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;

public partial class Orion_SRM_RedirectGet : System.Web.UI.Page
{
    string redirectorHiddenFieldID = "redirParamsResourceID";
    protected void Page_Load(object sender, EventArgs e)
    {
        // find our hiddenfield without instantiating PreviousPage
        var key = Request.Form.AllKeys.FirstOrDefault(k => k.EndsWith(redirectorHiddenFieldID));
        
        if (key == null)
        {
            return;
        }
        var value = Request.Form[key];
        if (value == null)
        {
            return;
        }

        var targetUrl = CreateUrl(value);
        Redirect(targetUrl);
    }


    public class RedirectorArguments
    {
        public string targetUrl { get; set; }

        public Dictionary<string, string> queryString { get; set; }

        public Dictionary<string, string> parameters { get; set;  }
    }



    /// <summary>
    /// Redirect-Get part of the Post-Redirect-Get  pattern
    /// </summary>
    public string CreateUrl(string redirectorArguments)
    {
        var response = (RedirectorArguments)(new JavaScriptSerializer()).Deserialize(redirectorArguments, typeof(RedirectorArguments));
        var targetUrl = response.targetUrl;
        var queryString = response.queryString;
        var parameters = response.parameters;
		
		var guid = Guid.NewGuid();
        Session[guid.ToString()] = parameters;
        var url = targetUrl + "?redirId=" + guid;

        if ((response.queryString == null || String.IsNullOrEmpty(response.queryString["NetObject"])) && response.parameters.ContainsKey("NetObject"))
        {
            Dictionary<string, string> qs = new Dictionary<string, string>();
            qs.Add("NetObject", response.parameters["NetObject"]);
            queryString = response.queryString = qs;
        }
        if (queryString.Keys.Count > 0)
        {
            var queryStringStr = queryString.Aggregate(new StringBuilder(), (sb, kvp) => sb.AppendFormat("&{0}={1}", Server.UrlEncode(kvp.Key), Server.UrlEncode(kvp.Value))).ToString();
            url += queryStringStr;
        }
		return url;
    }
	
	public void Redirect(string urlWithId)
	{
		Response.Redirect(urlWithId);
	}
	
}

