﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.NetObjects;
using System.Globalization;
using System.Linq;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.SRM.Common;

public partial class Orion_SRM_AccountLimitationPopup : Page
{
    protected string Caption { get; set; }
    protected string MessageText { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Caption = Resources.SrmWebContent.AccountLimitationPopup_Caption;
        this.MessageText = Resources.SrmWebContent.AccountLimitationPopup_Message;
    }
}