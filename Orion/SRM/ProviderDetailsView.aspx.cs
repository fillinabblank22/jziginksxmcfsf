﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web.DAL;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.NetObjects;

public partial class Orion_SRM_ProviderDetailsView : OrionView, IStorageProviderProvider
{
    protected override void OnInit(EventArgs e)
    {
        this.Title = this.ViewInfo.ViewTitle;
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    public override string ViewType
    {
        get { return "SRM Provider Details"; }
    }

    public StorageProvider Provider
    {
        get
        {
            return new StorageProvider(NetObject.NetObjectID);
        }
    }


}