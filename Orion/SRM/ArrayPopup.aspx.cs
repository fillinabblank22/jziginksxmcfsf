﻿using System;
using System.Web.UI;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.NetObjects;
using System.Globalization;
using SolarWinds.SRM.Web;
using System.IO;
using SolarWinds.SRM.Web.Models;

public partial class Orion_SRM_ArrayPopup : Page
{
    protected StorageArray StorageArray { get; set; }
    protected const string hardwareHealthTooltipExtension = "/Orion/HardwareHealth/NodePopupExtension.ascx";

    protected override void OnPreInit(EventArgs e)
    {
        string netObjectID = Request.QueryString["NetObject"];
        try
        {
            this.StorageArray = NetObjectFactory.Create(netObjectID) as StorageArray;
        }
        catch (AccountLimitationException)
        {
            Server.Transfer(
                string.Format(CultureInfo.InvariantCulture, "/Orion/SRM/AccountLimitationPopup.aspx?NetObject={0}", netObjectID));
        }

        base.OnPreInit(e);
    }




    protected void Page_Load(object sender, EventArgs e)
    {

        if (this.StorageArray != null)
        {
            ArrayEntity entity = StorageArray.Array;

            AddHardwareHealthTooltip();

            var strDeviceType = entity.IsCluster
                ? SrmWebContent.ArrayDetails_ArrayPopup_Cluster
                : SrmWebContent.ArrayDetails_ArrayPopup_Array;
            StatusLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", strDeviceType,
                SrmStatusLabelHelper.GetStatusLabel(entity.Status));
            WebHelper.SetValueOrHide(VendorStatusLabel, VendorStatusRow, entity.VendorStatusDescription);
            ManufacturerLabel.Text = entity.Manufacturer;
            ManufacturerRow.Visible = !String.IsNullOrEmpty(ManufacturerLabel.Text);
            ModelLabel.Text = entity.Model;
            ModelRow.Visible = !String.IsNullOrEmpty(ModelLabel.Text);
            IPAddressLabel.Text =
                ServiceLocator.GetInstance<IStorageArrayIPDAL>()
                    .GetAggregateArrayIPDataByArrayID(entity.StorageArrayId.Value, 5)
                    .IPAddress;
            IPAddressRow.Visible = !String.IsNullOrEmpty(IPAddressLabel.Text);

            var dal = ServiceLocator.GetInstance<IThresholdDAL>();

            WebHelper.SetValueOrHide(DataReductionLabel, dataReductionDiv,
                WebHelper.FormatDataReduction(entity.DataReduction));

            IResourceVisibilityService visibilityService = ServiceLocator.GetInstance<IResourceVisibilityService>();
            performanceTable.Visible = visibilityService.IsPerformanceAvailableForArray(this.StorageArray);

            if (performanceTable.Visible)
            {
                SetPerformanceData(entity, dal);
            }

            double total = entity.CapacityTotal;
            long spare = entity.CapacityRawSpare;
            long used = entity.CapacityRawUsed;
            double free = entity.CapacityRawFree;
            popupChart.ChartColors = new[]
            {PopupChartHelper.PurpleChartColor, PopupChartHelper.BlueChartColor, PopupChartHelper.GradientChartColor};
            popupChart.ChartLabelColors = new[]
            {PopupChartHelper.PurpleFontColor, PopupChartHelper.BluetFontColor, PopupChartHelper.GradientFontColor};
            popupChart.Values = new[] {spare, used, (long) free};
            popupChart.Total = (long) total;
            popupChart.RenderTotalLabel = true;
            popupChart.Height = PopupChartHelper.Height;

            total = entity.CapacityUserTotal;
            used = entity.CapacityUserUsed;
            free = entity.CapacityUserFree;
            popupChartUserSummary.ChartColors = new[]
            {PopupChartHelper.BlueChartColor, PopupChartHelper.GradientChartColor};
            popupChartUserSummary.ChartLabelColors = new[]
            {PopupChartHelper.BluetFontColor, PopupChartHelper.GradientFontColor};
            popupChartUserSummary.Values = new[] {used, (long) free};
            popupChartUserSummary.Total = (long) total;
            popupChartUserSummary.RenderTotalLabel = true;
            popupChartUserSummary.Height = PopupChartHelper.Height;

            #region Other potential issues - Marked by thresholds

            IOPSReadLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}",
                Math.Round(entity.IOPSRead ?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.PopUp_IOPSUnit);
            WebHelper.MarkThresholdAndShowRow(IOPSReadLabel, IOPSReadRow, entity.IOPSRead,
                dal.GetThresholdDynamicOrGlobal(ThresholdDataType.ArrayIOPSRead, entity.StorageArrayId.Value));

            IOPSWriteLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}",
                Math.Round(entity.IOPSWrite ?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.PopUp_IOPSUnit);
            WebHelper.MarkThresholdAndShowRow(IOPSWriteLabel, IOPSWriteRow, entity.IOPSWrite,
                dal.GetThresholdDynamicOrGlobal(ThresholdDataType.ArrayIOPSWrite, entity.StorageArrayId.Value));

            IOPSOtherLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}",
                Math.Round(entity.IOPSOther ?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.PopUp_IOPSUnit);
            WebHelper.MarkThresholdAndShowRow(IOPSOtherLabel, IOPSOtherRow, entity.IOPSOther,
                dal.GetThresholdDynamicOrGlobal(ThresholdDataType.ArrayIOPSOther, entity.StorageArrayId.Value));

            ThroughputReadLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}",
                WebHelper.FormatCapacityTotal(entity.BytesPSRead), SrmWebContent.PopUp_secUnit);
            WebHelper.MarkThresholdAndShowRow(ThroughputReadLabel, ThroughputReadRow, entity.BytesPSRead,
                dal.GetThresholdDynamicOrGlobal(ThresholdDataType.ArrayThroughputRead, entity.StorageArrayId.Value));

            ThroughputWriteLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}",
                WebHelper.FormatCapacityTotal(entity.BytesPSWrite), SrmWebContent.PopUp_secUnit);
            WebHelper.MarkThresholdAndShowRow(ThroughputWriteLabel, ThroughputWriteRow, entity.BytesPSWrite,
                dal.GetThresholdDynamicOrGlobal(ThresholdDataType.ArrayThroughputWrite, entity.StorageArrayId.Value));

            IOSizeTotalLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0}",
                WebHelper.FormatCapacityTotal(entity.IOSizeTotal));
            WebHelper.MarkThresholdAndShowRow(IOSizeTotalLabel, IOSizeTotalRow, entity.IOSizeTotal,
                dal.GetThresholdDynamicOrGlobal(ThresholdDataType.ArrayIOSizeTotal, entity.StorageArrayId.Value));

            IOSizeReadLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0}",
                WebHelper.FormatCapacityTotal(entity.IOSizeRead));
            WebHelper.MarkThresholdAndShowRow(IOSizeReadLabel, IOSizeReadRow, entity.IOSizeRead,
                dal.GetThresholdDynamicOrGlobal(ThresholdDataType.ArrayIOSizeRead, entity.StorageArrayId.Value));

            IOSizeWriteLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0}",
                WebHelper.FormatCapacityTotal(entity.IOSizeWrite));
            WebHelper.MarkThresholdAndShowRow(IOSizeWriteLabel, IOSizeWriteRow, entity.IOSizeWrite,
                dal.GetThresholdDynamicOrGlobal(ThresholdDataType.ArrayIOSizeWrite, entity.StorageArrayId.Value));


            if (!IOSizeTotalRow.Visible && !IOSizeReadRow.Visible && !IOSizeWriteRow.Visible && !IOPSReadRow.Visible &&
                !IOPSWriteRow.Visible && !IOPSOtherRow.Visible && !ThroughputReadRow.Visible &&
                !ThroughputWriteRow.Visible)
                OtherPotentialIssuesSection.Visible = false;

            #endregion
        }
    }

    private void SetPerformanceData(ArrayEntity arrayEntity, IThresholdDAL thresholdDal)
    {
        NetObjectPerformance arrayPerformance =
            ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayPerformance(arrayEntity.StorageArrayId.Value);

        if (arrayPerformance == null)
        {
            return;
        }

        AggIopsLabel.Text = arrayPerformance.TotalIOPS.HasValue
            ? string.Format(CultureInfo.CurrentCulture, "{0:N0} {1}", arrayPerformance.TotalIOPS,
                SrmWebContent.ArrayDetails_IOPS_PS)
            : SrmWebContent.Value_Unknown;
        WebHelper.MarkThresholds(AggIopsLabel, arrayEntity.IOPSTotal,
            thresholdDal.GetThresholdDynamicOrGlobal(ThresholdDataType.ArrayIOPSTotal, arrayEntity.StorageArrayId.Value));

        AvgThroughputLabel.Text = arrayPerformance.TotalThroughput.HasValue
            ? string.Format(CultureInfo.CurrentCulture, "{0} {1}",
                WebHelper.FormatCapacityTotal(arrayEntity.BytesPSTotal), SrmWebContent.ArrayDetails_PerSecond)
            : SrmWebContent.Value_Unknown;
        WebHelper.MarkThresholds(AvgThroughputLabel, arrayEntity.BytesPSTotal,
            thresholdDal.GetThresholdDynamicOrGlobal(ThresholdDataType.ArrayThroughputTotal, arrayEntity.StorageArrayId.Value));
    }

    protected void AddHardwareHealthTooltip()
    {
        StringWriter sw = new StringWriter();
        HtmlTextWriter writer = new HtmlTextWriter(sw);
        Control control = Page.LoadControl(hardwareHealthTooltipExtension);
        control.RenderControl(writer);
        extensionPlaceholder.Controls.Add(control);
    }


}
