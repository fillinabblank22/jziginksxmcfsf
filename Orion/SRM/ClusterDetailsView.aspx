﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/View.master" AutoEventWireup="true" CodeFile="ClusterDetailsView.aspx.cs" Inherits="Orion_SRM_ClusterDetailsView" %>
<%@ Import Namespace="SolarWinds.SRM.Web" %>
<%@ Import Namespace="SolarWinds.SRM.Web.NetObjects" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register Src="~/Orion/SRM/Controls/BreadcrumbBar.ascx" TagPrefix="orion" TagName="BreadcrumbBar" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ViewPageTitle" Runat="Server">
    <orion:BreadcrumbBar runat="server" id="BreadcrumbBar" />
      <h1>
        <%= ViewInfo.IsSubView ? ViewInfo.ViewGroupTitle : ViewInfo.ViewHtmlTitle%> 
	    - 
        <img class="StatusIcon" src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.Clusters%>&status=<%=(int)((StorageArray)this.NetObject).Array.Status%>&size=small" />
        <a class="srm-breadcrumbbar-link" href="/Orion/View.aspx?NetObject=<%=(string)this.NetObject.NetObjectID%>">
            <%= this.NetObject.Name %>
        </a>
	    <asp:PlaceHolder ID="TitleDecorators" runat="server"></asp:PlaceHolder>	    
        <%= ViewInfo.IsSubView ? ViewInfo.ViewHtmlTitle : string.Empty%>
    </h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
     <orion:ResourceHostControl ID="ResourceHostControl1" runat="server">
        <orion:ResourceContainer runat="server" ID="resContainer" />
    </orion:ResourceHostControl>
</asp:Content>