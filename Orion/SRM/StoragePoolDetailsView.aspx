﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/View.master" AutoEventWireup="true" CodeFile="StoragePoolDetailsView.aspx.cs" Inherits="Orion_SRM_StoragePoolDetailsView" %>
<%@ Import Namespace="SolarWinds.SRM.Web" %>
<%@ Import Namespace="SolarWinds.SRM.Web.NetObjects" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register Src="~/Orion/SRM/Controls/BreadcrumbBar.ascx" TagPrefix="orion" TagName="BreadcrumbBar" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ViewPageTitle">
    <orion:Include ID="Include5" runat="server" Module="SRM" File="SRM.css" />
    <orion:BreadcrumbBar runat="server" id="BreadcrumbBar" />
	<h1>
	    <%= ViewInfo.IsSubView ? ViewInfo.ViewGroupTitle : ViewInfo.ViewHtmlTitle%> 
	    - 
        <img class="StatusIcon" src="/Orion/StatusIcon.ashx?entity=<%=((StorageArray)this.NetObject.Parent).Array.IsCluster?SwisEntities.Clusters:SwisEntities.StorageArrays%>&status=<%=(int)((StorageArray)this.NetObject.Parent).Array.Status%>&size=small" />
        <a class="srm-breadcrumbbar-link" href="/Orion/View.aspx?NetObject=<%=(string)this.NetObject.Parent.NetObjectID%>">
            <%= this.NetObject.Parent.Name %>
        </a>
        - 
        <img class="StatusIcon" src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.Pools%>&status=<%=(int)((Pool)this.NetObject).PoolEntity.Status%>&size=small" />
        <a class="srm-breadcrumbbar-link" href="/Orion/View.aspx?NetObject=<%=(string)this.NetObject.NetObjectID%>">
            <%= this.NetObject.Name %>
        </a>
	    <asp:PlaceHolder ID="TitleDecorators" runat="server"></asp:PlaceHolder>	    
        <%= ViewInfo.IsSubView ? ViewInfo.ViewHtmlTitle : string.Empty%>
	</h1>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MainContentPlaceHolder">
	<orion:ResourceHostControl ID="ResourceHostControl1" runat="server">
		<orion:ResourceContainer runat="server" ID="resContainer" />
	</orion:ResourceHostControl>
</asp:Content>