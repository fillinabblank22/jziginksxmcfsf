﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AccountLimitationPopup.aspx.cs" Inherits="Orion_SRM_AccountLimitationPopup" %>

<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>

<h3 class="StatusCritical">
    <%= this.Caption %>
</h3>

<div class="NetObjectTipBody">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th style="min-width: 110px; text-align: left; width: 110px;">
                <%= this.MessageText %>
            </th>

            <td colspan="2">
            </td>
        </tr>
    </table>
</div>
