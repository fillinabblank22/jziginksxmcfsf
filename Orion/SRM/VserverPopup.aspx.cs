﻿using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.NetObjects;
using System;
using System.Globalization;

public partial class Orion_SRM_VserverPopup : System.Web.UI.Page
{
    protected VServer VServer { get; set; }

    protected override void OnPreInit(EventArgs e)
    {
        string netObjectID = Request.QueryString["NetObject"];
        try
        {
            this.VServer = NetObjectFactory.Create(netObjectID) as VServer;
        }
        catch (AccountLimitationException)
        {
            Server.Transfer(
                string.Format(CultureInfo.InvariantCulture, "/Orion/SRM/AccountLimitationPopup.aspx?NetObject={0}", netObjectID));
        }

        base.OnPreInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.VServer != null)
        {
            var entity = VServer.VServerEntity;
            this.StatusLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", SrmWebContent.Vserver_Popup_Vserver_Name, SrmStatusLabelHelper.GetStatusLabel(entity.Status));
            WebHelper.SetValueOrHide(VendorStatusLabel, VendorStatusRow, entity.VendorStatusDescription);

            this.ManufacturerLabel.Text = VServer.StorageArray.Array.Manufacturer;
            this.ModelLabel.Text = VServer.StorageArray.Array.Model;
            this.IPaddressLabel.Text = entity.IPAddress;

            ManufacturerRow.Visible = !String.IsNullOrEmpty(ManufacturerLabel.Text);
            ModelRow.Visible = !String.IsNullOrEmpty(ModelLabel.Text);
            IPaddressRow.Visible = !String.IsNullOrEmpty(IPaddressLabel.Text);

            var vServerPerformance = ServiceLocator.GetInstance<IVServerDAL>().GetVServerPerformance(entity.VServerID);
            var dal = ServiceLocator.GetInstance<IThresholdDAL>();

            this.AggIopsLabel.Text = vServerPerformance.TotalIOPS.HasValue
                ? string.Format(CultureInfo.CurrentCulture, "{0:N0} {1}", vServerPerformance.TotalIOPS, SrmWebContent.PopUp_IOPSUnit)
                : SrmWebContent.Value_Unknown;
            WebHelper.MarkThresholds(AggIopsLabel, entity.IOPSTotal, dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VserverIOPSTotal, entity.VServerID));

            this.AvgThroughputLabel.Text = vServerPerformance.TotalThroughput.HasValue
                ? string.Format(CultureInfo.CurrentCulture, "{0} {1}", WebHelper.FormatCapacityTotal(vServerPerformance.TotalThroughput), SrmWebContent.PopUp_secUnit)
                : SrmWebContent.Value_Unknown;
            WebHelper.MarkThresholds(AvgThroughputLabel, entity.BytesPSTotal, dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VserverThroughputTotal, entity.VServerID));

            long remaining;
            long used;
            ServiceLocator.GetInstance<IChartsDAL>().GetAggregatedNASVolumeCapacity(VServer.NetObjectPrefix, entity.VServerID, out used, out remaining);

            long total = remaining + used;

            this.NasCapacityLabel.Text = WebHelper.FormatCapacityTotal(total);
            this.UsedCapacityLabel.Text = WebHelper.FormatCapacityTotal(used);

            popupChart.ChartColors = new[] { PopupChartHelper.BlueChartColor, PopupChartHelper.GradientChartColor };
            popupChart.ChartLabelColors = new[] { PopupChartHelper.BluetFontColor, PopupChartHelper.GradientFontColor };
            popupChart.Values = new[] { used, remaining };
            popupChart.Total = total;
            popupChart.RenderTotalLabel = true;
            popupChart.Height = PopupChartHelper.Height;

            #region Other potential issues - Marked by thresholds

            IOPSReadLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(entity.IOPSRead ?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.PopUp_IOPSUnit);
            WebHelper.MarkThresholdAndShowRow(IOPSReadLabel, IOPSReadRow, entity.IOPSRead,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VserverIOPSRead, entity.VServerID));

            IOPSWriteLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(entity.IOPSWrite ?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.PopUp_IOPSUnit);
            WebHelper.MarkThresholdAndShowRow(IOPSWriteLabel, IOPSWriteRow, entity.IOPSWrite,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VserverIOPSWrite, entity.VServerID));

            IOPSOtherLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(entity.IOPSOther ?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.PopUp_IOPSUnit);
            WebHelper.MarkThresholdAndShowRow(IOPSOtherLabel, IOPSOtherRow, entity.IOPSOther,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VserverIOPSOther, entity.VServerID));

            ThroughputReadLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", WebHelper.FormatCapacityTotal(entity.BytesPSRead), SrmWebContent.PopUp_secUnit);
            WebHelper.MarkThresholdAndShowRow(ThroughputReadLabel, ThroughputReadRow, entity.BytesPSRead,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VserverThroughputRead, entity.VServerID));

            ThroughputWriteLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", WebHelper.FormatCapacityTotal(entity.BytesPSWrite), SrmWebContent.PopUp_secUnit);
            WebHelper.MarkThresholdAndShowRow(ThroughputWriteLabel, ThroughputWriteRow, entity.BytesPSWrite,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VserverThroughputWrite, entity.VServerID));

            IOSizeTotalLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0}", WebHelper.FormatCapacityTotal(entity.IOSizeTotal));
            WebHelper.MarkThresholdAndShowRow(IOSizeTotalLabel, IOSizeTotalRow, entity.IOSizeTotal,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VserverIOSizeTotal, entity.VServerID));

            IOSizeReadLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0}", WebHelper.FormatCapacityTotal(entity.IOSizeRead));
            WebHelper.MarkThresholdAndShowRow(IOSizeReadLabel, IOSizeReadRow, entity.IOSizeRead,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VserverIOSizeRead, entity.VServerID));

            IOSizeWriteLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0}", WebHelper.FormatCapacityTotal(entity.IOSizeWrite));
            WebHelper.MarkThresholdAndShowRow(IOSizeWriteLabel, IOSizeWriteRow, entity.IOSizeWrite,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VserverIOSizeWrite, entity.VServerID));


            if (!IOSizeTotalRow.Visible && !IOSizeReadRow.Visible && !IOSizeWriteRow.Visible && !IOPSReadRow.Visible && !IOPSWriteRow.Visible && !IOPSOtherRow.Visible && !ThroughputReadRow.Visible && !ThroughputWriteRow.Visible)
                OtherPotentialIssuesSection.Visible = false;

            #endregion
        }
    }
}