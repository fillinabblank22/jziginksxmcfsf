﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ArrayPopup.aspx.cs" Inherits="Orion_SRM_ArrayPopup" %>

<%@ Register TagPrefix="srm" TagName="PopupChart" Src="~/Orion/SRM/Controls/PopupChart.ascx" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>

<h3 class="Status<%=Enum.IsDefined(typeof (StorageArrayStatus), StorageArray.Array.Status)?(StorageArray.Array.Status).ToString():StorageArrayStatus.Unknown.ToString()%>">
    <%=SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(StorageArray.Name)%></h3>
<div class="NetObjectTipBody">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2">
                <asp:label runat="server" id="StatusLabel" />
            </td>
        </tr>
        <tr runat="server" id="VendorStatusRow">
            <th style="width: 110px; text-align: left; min-width: 110px;">
                <%= SrmWebContent.ArrayDetails_VendorStatusCode%>:
            </th>
            <td>
                <asp:literal runat="server" id="VendorStatusLabel" />
            </td>
        </tr>
        <tr id="ManufacturerRow" runat="server">
            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.ArrayDetails_Manufacturer%>:</th>
            <td>
                <asp:literal runat="server" id="ManufacturerLabel" />
            </td>
        </tr>
        <tr id="ModelRow" runat="server">
            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.ArrayDetails_Model%>:</th>
            <td>
                <asp:literal runat="server" id="ModelLabel" />
            </td>
        </tr>
        <tr id="IPAddressRow" runat="server">
            <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.ArrayDetails_IP_Address%>:</th>
            <td>
                <asp:literal runat="server" id="IPAddressLabel" />
            </td>
        </tr>
    </table>

    <div runat="server" id="performanceTable">
        <hr/>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.ArrayDetails_AggregateIOPS%>:</th>
                <td>
                    <asp:label runat="server" id="AggIopsLabel" />
                </td>
            </tr>
            <tr>
                <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.ArrayDetails_AvgThroughput%>:</th>
                <td>
                    <asp:label runat="server" id="AvgThroughputLabel" />
                </td>
            </tr>
        </table>
    </div>

    <div runat="server" id="dataReductionDiv">
        <hr style="margin-bottom: 2px; margin-top: 2px;" />
        <table cellpadding="0" cellspacing="0">
            <tr runat="server" id="DataReductionRow">
                <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.UsableCapacityDataReduction%>:</th>
                <td>
                    <asp:literal runat="server" id="DataReductionLabel" />
                </td>
            </tr>
        </table>
    </div>
    
    <hr style="margin-bottom: 2px; margin-top: 2px;" />
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><%= SrmWebContent.ArrayDetails_RawDiskCapacity%>:</th>
        </tr>
        <tr>
            <td colspan="2">
                <srm:PopupChart ID="popupChart" runat="server" />
            </td>
        </tr>
    </table>
    <hr style="margin-bottom: 2px; margin-top: 2px;" />
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><%= SrmWebContent.UserCapacity_Title%>:</th>
        </tr>
        <tr>
            <td colspan="2">
                <srm:PopupChart ID="popupChartUserSummary" runat="server" />
            </td>
        </tr>
    </table>
    <div id="OtherPotentialIssuesSection" runat="server">
        <hr style="margin-bottom: 2px; margin-top: 2px;" />
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.Popups_OtherIssues%>:</th>
                <td></td>
            </tr>
            <tr id="IOPSReadRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_ReadIOPS%>:</th>
                <td>
                    <asp:label runat="server" id="IOPSReadLabel" />
                </td>
            </tr>
            <tr id="IOPSWriteRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_WriteIOPS%>: </th>
                <td>
                    <asp:label runat="server" id="IOPSWriteLabel" />
                </td>
            </tr>
            <tr id="IOPSOtherRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_OtherIOPS%>: </th>
                <td>
                    <asp:label runat="server" id="IOPSOtherLabel" />
                </td>
            </tr>
            <tr id="ThroughputReadRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_ReadThroughput%>: </th>
                <td>
                    <asp:label runat="server" id="ThroughputReadLabel" />
                </td>
            </tr>
            <tr id="ThroughputWriteRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_WriteThroughput%>: </th>
                <td>
                    <asp:label runat="server" id="ThroughputWriteLabel" />
                </td>
            </tr>
            <tr id="IOSizeTotalRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_TotalIOSize%>: </th>
                <td>
                    <asp:label runat="server" id="IOSizeTotalLabel" />
                </td>
            </tr>
            <tr id="IOSizeReadRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_ReadIOSize%>: </th>
                <td>
                    <asp:label runat="server" id="IOSizeReadLabel" />
                </td>
            </tr>
            <tr id="IOSizeWriteRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_WriteIOSize%>: </th>
                <td>
                    <asp:label runat="server" id="IOSizeWriteLabel" />
                </td>
            </tr>
        </table>
        </div>
        <div style="padding-top: 10px; font-weight: bold;">
            <asp:PlaceHolder runat="server" id="extensionPlaceholder" />
        </div>
    </div>
