﻿using System;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI.Models;

public partial class Orion_SRM_FileStorageView : OrionView, IModelProvider
{
    protected override void OnInit(EventArgs e)
    {
        this.Title = UIHelper.NormalizeSpaces(this.ViewInfo.ViewTitle);
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    public override string ViewType
    {
        get { return "SRM Array File Storage Details"; }
    }

    public StorageArray Array
    {
        get { return new StorageArray(NetObject.NetObjectID); }
    }

    public IPageModelProvider PageModelProvider
    {
        get { return new ArrayPageModelProvider(Array); }
    }
}