﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LunPopup.aspx.cs" Inherits="Orion_SRM_LunPopup" %>

<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Register TagPrefix="srm" TagName="PopupChart" Src="~/Orion/SRM/Controls/PopupChart.ascx" %>

<h3 class="Status<%=(Lun.LUN!=null && Enum.IsDefined(typeof (LunStatus), Lun.LUN.Status))?Lun.LUN.Status.ToString():LunStatus.Unknown.ToString()%>">
    <%=SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(Lun.Name)%>
</h3>

<div class="NetObjectTipBody">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2">
                <asp:literal runat="server" id="StatusLabel" />
            </td>
        </tr>
        <tr runat="server" id="VendorStatusRow">
            <th style="width: 110px; text-align: left; min-width: 110px;">
                <%= SrmWebContent.LunDetails_VendorStatusCode%>:
            </th>
            <td>
                <asp:literal runat="server" id="VendorStatusLabel" />
            </td>
        </tr>
        <tr id="UUIDRow" runat="server">
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Lun_UUID %>:</th>
            <td>
                <asp:literal runat="server" id="UUIDLabel" />
            </td>
        </tr>
        <tr id="DefaultControllerRow" runat="server">
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Lun_DefaultController%>:</th>
            <td>
                <asp:literal runat="server" id="DefaultControllerLabel" />
            </td>
        </tr>
        <tr id="CurrentControllerRow" runat="server">
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Lun_CurrentController%>:</th>
            <td>
                <asp:literal runat="server" id="CurrentControllerLabel" />
            </td>
        </tr>
        <tr id="ParentVolumeRow" runat="server">
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Lun_ParentVolume%>:</th>
            <td>
                <asp:literal runat="server" id="ParentVolumeLabel" />
            </td>
        </tr>
        <tr id="StoragePoolRow" runat="server">
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Lun_Storage_Pool%>:</th>
            <td>
                <asp:literal runat="server" id="StoragePoolLabel" />
            </td>
        </tr>
        <tr id="ArrayRow" runat="server">
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Lun_Array%>:</th>
            <td>
                <asp:literal runat="server" id="ArrayLabel" />
            </td>
        </tr>
        <tr id="VserverRow" runat="server">
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Lun_vServer%>:</th>
            <td>
                <asp:literal runat="server" id="VServerLabel" />
            </td>
        </tr>
        <tr id="ClusterRow" runat="server">
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Lun_Cluster%>:</th>
            <td>
                <asp:literal runat="server" id="ClusterLabel" />
            </td>
        </tr>
        <tr runat="server" id="trRAIDConfig" visible="false">
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Lun_RAID%>:</th>
            <td>
                <asp:literal runat="server" id="RaidConfigLabel" />
            </td>
        </tr>
        <tr runat="server" id="trProtocol" visible="false">
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Lun_Protocol%>:</th>
            <td>
                <asp:literal runat="server" id="ProtocolLabel" />
            </td>
        </tr>
    </table>

    <div runat="server" id="performanceTable">
        <hr />
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Lun_TotalIOPS %>:</th>
                <td>
                    <asp:label runat="server" id="TotalIopsLabel" />
                </td>
            </tr>
            <tr>
                <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Lun_TotalThroughput%>:</th>
                <td>
                    <asp:label runat="server" id="TotalThroughputLabel" />
                </td>
            </tr>
            <tr>
                <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Lun_TotalLatency%>:</th>
                <td>
                    <asp:label runat="server" id="TotalLatencyLabel" />
                </td>
            </tr>
        </table>
    </div>

    <hr />
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Lun_TotalCapacity %>:</th>
            <td>
                <asp:literal runat="server" id="TotalCapacityLabel" />
            </td>
        </tr>
        <tr>
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Lun_FileSystemUsed%>:</th>
            <td>
                <asp:literal runat="server" id="FileSystemUsedLabel" />
            </td>
        </tr>
        <tr>
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.LUN_Consumed_Capacity%>:</th>
            <td>
                <asp:literal runat="server" id="ConsumedCapacityLabel" />
            </td>
        </tr>
        <!--
        <tr runat="server" id="TotalReductionRow">
            <th style="width: 110px; text-align: left;"><%= SrmWebContent.UsableCapacityTotalReduction %>:</th>
            <td>
                <asp:label runat="server" id="lblTotalReduction" />
            </td>
        </tr>
        -->
        <tr runat="server" ID="ProjectedRunOutRow">
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Lun_ProjectedRunOut%>:</th>
            <td>
                <asp:label runat="server" id="ProjectedRunOutLabel" />
            </td>
        </tr>
    </table>
    <hr style="margin-bottom: 2px; margin-top: 2px;" />
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><%= SrmWebContent.LUN_UserCapacity%>:</th>
            <td></td>
        </tr>
        <tr>
            <td colspan="2">
                <srm:PopupChart ID="popupChart" runat="server" />
            </td>
        </tr>
    </table>
    <div id="OtherPotentialIssuesSection" runat="server">
        <hr style="margin-bottom: 2px; margin-top: 2px;" />
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.Popups_OtherIssues%>:</th>
                <td></td>
            </tr>
            <tr id="IOPSReadRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_ReadIOPS%>:</th>
                <td>
                    <asp:label runat="server" id="IOPSReadLabel" />
                </td>
            </tr>
            <tr id="IOPSWriteRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_WriteIOPS%>: </th>
                <td>
                    <asp:label runat="server" id="IOPSWriteLabel" />
                </td>
            </tr>
            <tr id="ThroughputReadRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_ReadThroughput%>: </th>
                <td>
                    <asp:label runat="server" id="ThroughputReadLabel" />
                </td>
            </tr>
            <tr id="ThroughputWriteRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_WriteThroughput%>: </th>
                <td>
                    <asp:label runat="server" id="ThroughputWriteLabel" />
                </td>
            </tr>
            <tr id="LatencyReadRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_ReadLatency%>: </th>
                <td>
                    <asp:label runat="server" id="LatencyReadLabel" />
                </td>
            </tr>
            <tr id="LatencyWriteRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_WriteLatency%>: </th>
                <td>
                    <asp:label runat="server" id="LatencyWriteLabel" />
                </td>
            </tr>
            <tr id="LatencyOtherRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_OtherLatency%>: </th>
                <td>
                    <asp:label runat="server" id="LatencyOtherLabel" />
                </td>
            </tr>
            <tr id="IOSizeTotalRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_TotalIOSize%>: </th>
                <td>
                    <asp:label runat="server" id="IOSizeTotalLabel" />
                </td>
            </tr>
            <tr id="IOSizeReadRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_ReadIOSize%>: </th>
                <td>
                    <asp:label runat="server" id="IOSizeReadLabel" />
                </td>
            </tr>
            <tr id="IOSizeWriteRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_WriteIOSize%>: </th>
                <td>
                    <asp:label runat="server" id="IOSizeWriteLabel" />
                </td>
            </tr>
        </table>
    </div>
</div>
