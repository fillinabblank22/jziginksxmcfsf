﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/View.master" AutoEventWireup="true" CodeFile="~/Orion/SRM/ControllerPortDetailsView.aspx.cs" Inherits="ControllerPortDetailsView" %>

<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web" %>
<%@ Import Namespace="SolarWinds.SRM.Web.NetObjects" %>
<%@ Register Src="~/Orion/SRM/Controls/BreadcrumbBar.ascx" TagPrefix="orion" TagName="BreadcrumbBar" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ViewPageTitle">
    <orion:Include ID="Include5" runat="server" Module="SRM" File="SRM.css" />
    <orion:BreadcrumbBar runat="server" id="BreadcrumbBar" />
    <h1>
        <%= ViewInfo.IsSubView ? ViewInfo.ViewGroupTitle : ViewInfo.ViewHtmlTitle%>
        -  <%= this.NetObject.Name %>
        <asp:PlaceHolder ID="TitleDecorators" runat="server"></asp:PlaceHolder>	    
        <%= ViewInfo.IsSubView ? ViewInfo.ViewHtmlTitle : string.Empty%>
    </h1>
</asp:Content>
