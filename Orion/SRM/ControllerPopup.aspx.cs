﻿using System;
using System.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.NetObjects;
using System.Globalization;
using SolarWinds.SRM.Web;
using System.IO;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.Logging;
using Resources;

public partial class Orion_SRM_ControllerPopup : Page
{
    private const int NumberOfFractDigits = 2;
    private static Log _log;
    private static Log Logger { get { return _log ?? (_log = new Log()); } }

    protected StorageController StorageController { get; set; }
    protected const string hardwareHealthTooltipExtension = "/Orion/HardwareHealth/NodePopupExtension.ascx";

    protected override void OnPreInit(EventArgs e)
    {
        string netObjectID = Request.QueryString["NetObject"];

        try
        {
            this.StorageController = NetObjectFactory.Create(netObjectID) as StorageController;
        }
        catch (AccountLimitationException)
        {
            Server.Transfer(string.Format(CultureInfo.InvariantCulture, "/Orion/SRM/AccountLimitationPopup.aspx?NetObject={0}", netObjectID));
            Logger.ErrorFormat("Could not create StorageController object for NetObject {0}", netObjectID);
        }

        base.OnPreInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.StorageController != null)
        {
            ControllerPerfSeparator.Visible = false;
            DistIOPSRow.Visible = false;
            DistThroughputRow.Visible = false;
            IOSizeRow.Visible = false;
            LatencyRow.Visible = false;

            StorageControllerEntity entity = this.StorageController.StorageControllerEntity;

            AddHardwareHealthTooltip();

            StatusLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", SrmWebContent.Controller, 
                SrmStatusLabelHelper.GetStatusLabel(entity.Status.HasValue ? entity.Status.Value : StorageControllerStatus.Unknown));
            WebHelper.SetValueOrHide(ManufacturerLabel, ManufacturerRow, entity.Manufacturer);
            WebHelper.SetValueOrHide(ModelLabel, ModelRow, entity.Model);
            WebHelper.SetValueOrHide(FirmwareLabel, FirmwareRow, entity.Firmware);
            WebHelper.SetValueOrHide(CPUCountLabel, CPUCountRow, entity.CPUCount);
            WebHelper.SetValueOrHide(CPUFrequencyLabel, CPUFrequencyRow, entity.CPUFrequency);
            WebHelper.SetValueOrHide(EthernetPortsCountLabel, EthernetPortsCountRow, entity.EthernetPortCount);
            WebHelper.SetValueOrHide(ISCSIPortsCountLabel, ISCSIPortsCountRow, entity.ISCSIPortCount);
            WebHelper.SetValueOrHide(ControllerTypeLabel, ControllerTypeRow, entity.Type);

            if (entity.TotalMemory.HasValue)
            {
                TotalMemoryLabel.Text = WebHelper.FormatCapacityTotal(entity.TotalMemory);
            }
            else
            {
                TotalMemoryRow.Visible = false;
            }

            if (entity.TotalCacheSize.HasValue)
            {
                TotalCacheSizeLabel.Text = WebHelper.FormatCapacityTotal(entity.TotalCacheSize);
            }
            else
            {
                TotalCacheSizeRow.Visible = false;
            }

            if (!entity.StorageControllerID.HasValue)
            {
                Logger.Error("Storage Controller ID missing.");
                return;
            }

            StorageControllerPerformance controllerPerformance = ServiceLocator.GetInstance<IStorageControllerDAL>().GetControllerPerformance(entity.StorageControllerID.Value);
            if (controllerPerformance == null)
            {
                Logger.WarnFormat("No performance statistics found for Controller {0}", entity.StorageControllerID.Value);
                return;
            }

            if (controllerPerformance.IOPSDistribution.HasValue)
            {
                DistIOPSLabel.Text = WebHelper.FormatPercentValue(controllerPerformance.IOPSDistribution);
                DistIOPSRow.Visible = true;
            }

            if (controllerPerformance.ThroughputDistribution.HasValue)
            {
                DistThroughputLabel.Text = WebHelper.FormatPercentValue(controllerPerformance.ThroughputDistribution);
                DistThroughputRow.Visible = true;
            }

            if (controllerPerformance.IOSize.HasValue)
            {
                IOSizeLabel.Text = WebHelper.FormatCapacityTotal(controllerPerformance.IOSize);
                IOSizeRow.Visible = true;
            }

            if (controllerPerformance.Latency.HasValue)
            {
                LatencyLabel.Text = WebHelper.FormatLatencyTotal(controllerPerformance.Latency.Value, NumberOfFractDigits);
                LatencyRow.Visible = true;
            }

            ControllerPerfSeparator.Visible = DistIOPSRow.Visible || DistThroughputRow.Visible || IOSizeRow.Visible || LatencyRow.Visible;
        }
    }

    protected void AddHardwareHealthTooltip()
    {
        StringWriter sw = new StringWriter();
        HtmlTextWriter writer = new HtmlTextWriter(sw);
        Control control = Page.LoadControl(hardwareHealthTooltipExtension);
        control.RenderControl(writer);
        extensionPlaceholder.Controls.Add(control);
    }
}
