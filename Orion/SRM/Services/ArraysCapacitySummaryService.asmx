﻿<%@ WebService Language="C#" Class="ArraysCapacitySummaryService" %>

using System;
using System.Collections.Generic;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.SRM.Web.Resources;
using System.Reflection;
using SolarWinds.SRM.Web.Charting;
using SolarWinds.Orion.Web;
using System.Data;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ArraysCapacitySummaryService : WebService
{
    private static readonly Log log = new Log();
    
    [WebMethod(EnableSession = true)]
    public PageableDataTable GetData(int resourceId, string dataSourceMethod, int fromIndex, int pageSize, string arrayNameFilter)
    {
        ResourceInfo resource = ResourceManager.GetResourceByID(resourceId);
        Context.Items[typeof(ViewInfo).Name] = resource.View;

        try
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("SrmChartDataResults", typeof(SrmChartDataResults)));
            DataRow dataRow;
            
            Action<IEnumerable<ArraysCapacitySummary.ArraySummaryCapacityData>> summaryCallback = (summary) =>
            {
                foreach (var detail in summary)
                    {
                        var serieOverhead = new SrmDataSeries() { Label = Resources.SrmWebContent.ArrayCapacitySummary_ArrayOverhead, TagName = "columnOverhead", NetObjectId = detail.NetObjectId };
                        var serieSpare = new SrmDataSeries() { Label = Resources.SrmWebContent.ArrayCapacitySummary_Spare, TagName = "columnSpare", NetObjectId = detail.NetObjectId };
                        var serieUsed = new SrmDataSeries() { Label = Resources.SrmWebContent.ArrayCapacitySummary_Used, TagName = "columnUsed", NetObjectId = detail.NetObjectId };
                        var serieRemaining = new SrmDataSeries() { Label = Resources.SrmWebContent.ArrayCapacitySummary_Remaining, TagName = "columnRemaining", NetObjectId = detail.NetObjectId };
                        serieOverhead.AddPoint(new SrmDataPoint(Math.Max(0, detail.Overhead)));
                        serieSpare.AddPoint(new SrmDataPoint(Math.Max(0, detail.Spare)));
                        serieUsed.AddPoint(new SrmDataPoint(Math.Max(0, detail.Used)));
                        serieRemaining.AddPoint(new SrmDataPoint(Math.Max(0, detail.Remaining)));
                        dynamic options = new JsonObject();
                        options.chart.title = detail.Name;
                        options.chart.capacitytotal = detail.Total;
                        SrmChartDataResults srmChartDataResults = new SrmChartDataResults(serieRemaining,
                            serieUsed,
                            serieSpare,
                            serieOverhead) { ChartOptionsOverride = options };
                    
                        dataRow = dataTable.NewRow();
                        dataRow["SrmChartDataResults"] = srmChartDataResults;
                        dataTable.Rows.Add(dataRow);
                    }
            };
            
            MethodInfo dataSource = typeof(ArraysCapacitySummary).GetMethod(dataSourceMethod, BindingFlags.Instance | BindingFlags.Public);
            int totalCount = (int)dataSource.Invoke(new ArraysCapacitySummary(), new object[] { summaryCallback, fromIndex, pageSize, arrayNameFilter });
           
            return new PageableDataTable(dataTable, totalCount);;
        }
        catch (Exception ex)
        {
            log.Error("Exception occured during getting data for Array Capacity Summary resource.", ex);
            throw;
        }
    }
}
