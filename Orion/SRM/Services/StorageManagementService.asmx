﻿<%@ WebService Language="C#" Class="StorageManagementService" %>
using System;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.Common.Utility;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Models.Credentials;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.BL;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.ServiceProvider;
using System.Data;
using SolarWinds.SRM.Web.Interfaces;
using System.Globalization;
using System.Collections.Generic;
using System.Web;
using SolarWinds.SRM.Common.Credentials;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class StorageManagementService : WebService
{
    private static readonly Log log = new Log();
    private const string EmptyPassword = "@Empty@";

    public Credential Credential { get; set; }

    private void CheckNodeManagement()
    {
        if (!((ProfileCommon)HttpContext.Current.Profile).AllowNodeManagement)
            throw new UnauthorizedAccessException("You must have the Allow Node Management right to perform this operation.");
    }

    private void CheckAllowUnmanage()
    {
        if (!((ProfileCommon)HttpContext.Current.Profile).AllowUnmanage)
            throw new UnauthorizedAccessException("You must have the Allow Account to Unmanage objects to perform this operation.");
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string PollNetObjNow(string netObjectId)
    {
        CheckNodeManagement();

        try
        {
            log.InfoFormat("Calling PollNow function for {0}", netObjectId);
            using (var proxy = CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(ex => { throw ex; }, GetEngineIdForNetObject(netObjectId), false))
            {
                proxy.PollNow(netObjectId);
            }

            // Publish PollNow event
            // IndicationPublisher.CreateV3().ReportIndication(new NetObjectOperationIndication(IndicationType.Orion_PollNow, GetNetObjectIdString(netObjectIds), SolarWinds.Orion.Web.NetObjectFactory.GetSWISTypeByNetObject(GetNetObjectIdString(netObjectIds))));
        }
        catch (Exception ex)
        {
            log.Error("Error in PollNow function.", ex);
            throw new Exception(String.Format("Poll now failed for netobject {0}.", netObjectId));
        }

        return String.Empty;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string PollByJobKeyNetObjNow(string netObjectId, string jobKey)
    {
        CheckNodeManagement();

        try
        {
            log.InfoFormat("Calling PollByJobKeyNetObjNow function for {0} with jobKey {1}", netObjectId, jobKey);
            using (var proxy = CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(ex => { throw ex; }, GetEngineIdForNetObject(netObjectId), false))
            {
                proxy.JobNowByJobKey(netObjectId, jobKey);
            }
        }
        catch (Exception ex)
        {
            log.Error("Error in PollByJobKeyNetObjNow function.", ex);
            throw new Exception(String.Format("PollByJobKeyNetObjNow failed for netobject {0}.", netObjectId));
        }

        return String.Empty;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string RediscoverNetObjNow(string netObjectId)
    {
        CheckNodeManagement();

        try
        {
            log.InfoFormat("Calling Rediscover function for {0}", netObjectId);
            using (var proxy = CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(ex => { throw ex; }, GetEngineIdForNetObject(netObjectId), false))
            {
                proxy.Rediscover(netObjectId);
            }

            // Publish RediscoverNow event
            // IndicationPublisher.CreateV3().ReportIndication(new NetObjectOperationIndication(IndicationType.Orion_Rediscovery, GetNetObjectIdString(modified), SolarWinds.Orion.Web.NetObjectFactory.GetSWISTypeByNetObject(GetNetObjectIdString(modified))));

        }
        catch (Exception ex)
        {
            log.Error("Error in Rediscover function.", ex);
            throw new Exception(String.Format("Rediscover failed for netobject {0}.", netObjectId));
        }

        return String.Empty;
    }

    private int GetEngineIdForNetObject(string netObjectId)
    {
        int engineId;
        var parts = netObjectId.Split(':');

        if (parts.Length == 3 && int.TryParse(parts[2], out engineId))
            return engineId;

        int id;
        var allowed = new List<string>() { "SMSA" };
        if (parts.Length != 2 || !allowed.Contains(parts[0]) || !int.TryParse(parts[1], out id))
            throw new ArgumentException(string.Format("'{0}' is not a valid net object id", netObjectId));

        using (var proxy = BusinessLayerFactory.Create())
            return proxy.GetArray(id).EngineID;
    }


    [WebMethod(EnableSession = true)]
    public void UnmanageStorageArray(int[] ids, DateTime unmanageFrom, DateTime unmanageUntil)
    {
        log.DebugFormat("Unmanaging StorageArrays:{0} From:'{1}' Until:'{2}'", String.Join(", ", ids), unmanageFrom, unmanageUntil);

        CheckAllowUnmanage();

        using (var proxy = BusinessLayerFactory.Create())
        {
            proxy.UnmanageArrays(ids, unmanageFrom, unmanageUntil);
        }
    }

    [WebMethod(EnableSession = true)]
    public void RemanageStorageArrays(int[] ids)
    {
        log.DebugFormat("Remanaging StorageArrays:{0}", String.Join(", ", ids));

        CheckAllowUnmanage();

        using (var proxy = BusinessLayerFactory.Create())
        {
            proxy.RemanageArrays(ids);
        }
    }

    [WebMethod]
    public PageableDataTable GetAllArrays(string property, string type, string value, string search)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        if (string.IsNullOrEmpty(sortColumn))
            sortColumn = "ID";

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        if (pageSize == 0)
            pageSize = 20;
        string sortOrder = string.Format(CultureInfo.InvariantCulture, " {0} {1}", sortColumn, sortDirection);
        string whereCondition = GetWhereConditionForManageStorageObjects(search, property);

        var dal = ServiceLocator.GetInstance<IStorageArrayDAL>();
        DataTable dataTable = dal.GetAllStorageObjects(whereCondition, startRowNumber, pageSize, sortOrder);

        long rowTotal = dataTable.Rows.Count > 0 ? (long)dataTable.Rows[0]["TotalRows"] : 0;

        return new PageableDataTable(dataTable.DefaultView.ToTable(), rowTotal);
    }

    [WebMethod]
    public PageableDataTable GetAllProviders(string filter, string searchedText)
    {
        int startRowNumber = 0;
        int pageSize = 0;

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];
        if (string.IsNullOrEmpty(sortColumn))
            sortColumn = "ID";

        string sortOrder = string.Format(CultureInfo.InvariantCulture, " {0} {1}", sortColumn, sortDirection);
        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        var providerDAL = ServiceLocator.GetInstance<IStorageProviderDAL>();
        DataTable dataTable = providerDAL.GetPageOfExternalProviders(startRowNumber, startRowNumber + pageSize, searchedText, sortOrder);
        long totalCount = dataTable.Rows.Count > 0 ? (long)dataTable.ExtendedProperties["TotalRows"] : 0;

        return new PageableDataTable(dataTable, totalCount);
    }

    [WebMethod]
    public void DeleteStorageArray(int[] storageArrayIds)
    {
        log.InfoFormat("Delete StorageArrays:{0}'", String.Join(", ", storageArrayIds));

        CheckNodeManagement();

        using (var proxy = BusinessLayerFactory.Create())
        {
            proxy.DeleteArrays(storageArrayIds);
        }
    }

    private string GetColumnName(string fieldName)
    {
        var columnName = fieldName.Split(new[] { '.' }).Last();

        if (fieldName.Contains("["))
        {
            var columnNameRegex = new Regex(@"\[(.*?)\]");
            return columnNameRegex.Matches(columnName)[0].Groups[1].Value;
        }

        return columnName;
    }

    [WebMethod]
    public void DeleteProviders(IEnumerable<int> providerIds)
    {
        log.InfoFormat("Delete Provider:{0}'", String.Join(", ", providerIds));

        CheckNodeManagement();

        using (var proxy = BusinessLayerFactory.Create())
        {
            proxy.DeleteProviders(providerIds);
        }
    }

    [WebMethod]
    public DataTable GetArraysFilter(string fieldName)
    {
        var arrayDal = ServiceLocator.GetInstance<IStorageArrayDAL>();

        return arrayDal.GetArraysFilter(fieldName, GetColumnName(fieldName));
    }

    private string GetWhereConditionForManageStorageObjects(string search, string field)
    {
        string whereCondition = string.Empty;
        if (!string.IsNullOrEmpty(search))
        {
            whereCondition = InvariantString.Format(@"WHERE (Name LIKE '{0}'
                             OR [A].Providers.IPAddress LIKE '{0}'
                             OR [A].Providers.DisplayName LIKE '{0}'
                             OR [A].Engine.ServerName LIKE '{0}')", search);
        }

        if (!string.IsNullOrEmpty(field))
        {
            if (string.IsNullOrEmpty(whereCondition))
            {
                whereCondition += "WHERE (" + field + " )";
            }
            else
            {
                whereCondition += "AND (" + field + " )";
            }

        }
        return whereCondition;
    }

    [WebMethod()]
    public PageableDataTable GetEnginesTable()
    {
        int pageSize = 0;
        int startRowNumber = 0;
        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        return ServiceLocator.GetInstance<IEngineDAL>().GetPagebleEngines(
            Context.Request.QueryString["sort"], Context.Request.QueryString["dir"],
            startRowNumber + 1, Math.Max(startRowNumber + pageSize, 15));
    }

    [WebMethod()]
    public int UpdatePollingEngine(int engineId, int[] ids)
    {
        CheckNodeManagement();

        int updatedCount = 0;
        using (var proxy = BusinessLayerFactory.Create())
        {
            updatedCount = proxy.UpdatePollingEngine(SRMConstants.DatabaseTables.StorageArray, "StorageArrayID", ids, engineId);
        }
        return updatedCount;
    }

    [WebMethod()]
    public PageableDataTable GetAllCredentials(string property, string type, string value, string search)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        if (string.IsNullOrEmpty(sortColumn))
            sortColumn = "ID";

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        if (pageSize == 0)
            pageSize = 20;

        var credDal = ServiceLocator.GetInstance<ICredentialPropertiesDAL>();

        DataTable dataTable = credDal.GetAllCredentials(startRowNumber, pageSize, sortColumn, sortDirection, search);

        dataTable.Columns.Add("CredentialTypeName", typeof (string));

        foreach (DataRow row in dataTable.Rows)
        {
            string credentialTypeClass = row["CredentialType"].ToString();
            var credentialType = CredentialTypeResolver.GetCredentialType(credentialTypeClass);
            row["CredentialTypeName"] = credentialType.GetLocalizedDescription();
        }

        var rowTotal = dataTable.Rows.Count > 0 ? Convert.ToInt64(dataTable.Rows[0]["TotalRows"]) : 0;

        return new PageableDataTable(dataTable, rowTotal);
    }

    [WebMethod()]
    public void DeleteCredentials(int[] credentialIds)
    {
        AuthorizationChecker.AllowAdmin();

        log.InfoFormat("Delete Credential: {0}'", String.Join(", ", credentialIds));

        CheckCredentialsUsage(credentialIds);

        using (var proxy = BusinessLayerFactory.Create())
        {
            proxy.DeleteCredentials(credentialIds);
        }
    }

    private void CheckCredentialsUsage(int[] credentialIds)
    {
        var credentialPropertiesDAL = ServiceLocator.GetInstance<ICredentialPropertiesDAL>();
        foreach (int id in credentialIds)
        {
            int usageCount = credentialPropertiesDAL.GetUsageCountForId(id);
            if (usageCount > 0)
                throw new Exception(String.Format("Credential with ID '{0}' cannot be deleted.", id));
        }
    }

    [WebMethod(EnableSession = true)]
    public bool CheckIfCredentialNameExists(string displayName)
    {
        using (var proxy = BusinessLayerFactory.Create())
        {
            return proxy.CheckIfCredentialNameExists(displayName);
        }
    }

    [WebMethod()]
    public void CreateOrUpdateSnmpCredentials(string credId, string name, string community, int port)
    {
        CheckNodeManagement();

        if (string.IsNullOrEmpty(credId))
        {
            Credential = new SrmSnmpCredentialsV2() { Name = name, Community = community, Port = port };
            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(Credential, CredentialType.SNMP);
                Credential.ID = id;
            }
        }
        else
        {
            int credentialId = -1;
            Int32.TryParse(credId, out credentialId);
            using (var proxy = BusinessLayerFactory.Create())
            {
                Credential = proxy.GetCredential(credentialId, CredentialType.SNMP);
                var cred = (SrmSnmpCredentialsV2)Credential;
                cred.Name = name;
                cred.Community = community;
                cred.Port = port;
                proxy.UpdateCredential(cred, CredentialType.SNMP);
            }
        }

    }

    [WebMethod()]
    public void CreateOrUpdateSnmpV3Credentials(string credId, string name, string userName, string context, string authenticationType, string privacyType,
        string authenticationPassword, bool authenticationKeyIsPassword, string privacyPassword, bool privacyKeyIsPassword, int port)
    {
        CheckNodeManagement();

        byte authenticationValue;
        SNMPv3AuthType authenticationTypeV3 = SNMPv3AuthType.None;
        if (byte.TryParse(authenticationType, out authenticationValue))
        {
            authenticationTypeV3 = (SNMPv3AuthType)authenticationValue;
        }

        byte privacyValue;
        SNMPv3PrivacyType privacyTypeV3 = SNMPv3PrivacyType.None;
        if (byte.TryParse(privacyType, out privacyValue))
        {
            privacyTypeV3 = (SNMPv3PrivacyType)privacyValue;
        }

        if (string.IsNullOrEmpty(credId))
        {
            Credential = new SrmSnmpCredentialsV3()
            {
                Name = name,
                UserName = userName,
                Context = context,
                AuthenticationType = authenticationTypeV3,
                PrivacyType = privacyTypeV3,
                AuthenticationPassword = authenticationPassword,
                AuthenticationKeyIsPassword = authenticationKeyIsPassword,
                PrivacyPassword = privacyPassword,
                PrivacyKeyIsPassword = privacyKeyIsPassword,
                Port = port
            };

            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(Credential, CredentialType.SnmpV3);
                Credential.ID = id;
            }
        }
        else
        {
            int credentialId = -1;
            Int32.TryParse(credId, out credentialId);
            using (var proxy = BusinessLayerFactory.Create())
            {
                Credential = proxy.GetCredential(credentialId, CredentialType.SnmpV3);
                var cred = (SrmSnmpCredentialsV3)Credential;
                cred.Name = name;
                cred.UserName = userName;
                cred.Context = context;
                cred.AuthenticationType = authenticationTypeV3;
                cred.PrivacyType = privacyTypeV3;

                if(authenticationPassword != EmptyPassword)
                    cred.AuthenticationPassword = authenticationPassword;

                cred.AuthenticationKeyIsPassword = authenticationKeyIsPassword;

                if (privacyPassword != EmptyPassword)
                    cred.PrivacyPassword = privacyPassword;

                cred.PrivacyKeyIsPassword = privacyKeyIsPassword;
                cred.Port = port;
                proxy.UpdateCredential(cred, CredentialType.SnmpV3);
            }

        }
    }

    [WebMethod()]
    public void CreateOrUpdateSmsiCredentials(string credId, string name, string userName, string password, bool useSsl, int httpsPort,
        int httpPort, string interopNamespace, string arrayNamespace, bool changePassword)
    {
        CheckNodeManagement();

        if (string.IsNullOrEmpty(credId))
        {
            Credential = new SmisCredentials()
            {
                Name = name,
                Username = userName,
                Password = password,
                UseSSL = useSsl,
                HttpPort = (UInt32)httpPort,
                HttpsPort = (UInt32)httpsPort,
                InteropNamespace = interopNamespace,
                Namespace = arrayNamespace
            };

            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(Credential, CredentialType.SMIS);
                Credential.ID = id;
            }
        }
        else
        {
            int credentialId = -1;
            Int32.TryParse(credId, out credentialId);
            using (var proxy = BusinessLayerFactory.Create())
            {
                Credential = proxy.GetCredential(credentialId, CredentialType.SMIS);
                var cred = (SmisCredentials)Credential;
                cred.Name = name;
                cred.Username = userName;
                cred.Password = changePassword ? password : cred.Password;
                cred.UseSSL = useSsl;
                cred.HttpPort = (UInt32) httpPort;
                cred.HttpsPort = (UInt32) httpsPort;
                cred.InteropNamespace = interopNamespace;
                cred.Namespace = arrayNamespace;

                proxy.UpdateCredential(cred, CredentialType.SMIS);
            }
        }

    }

    [WebMethod()]
    public void CreateOrUpdateNetAppFilerCredentials(string credId, string name, string userName, string password, bool useSsl, bool changePassword)
    {
        CheckNodeManagement();

        if (string.IsNullOrEmpty(credId))
        {
            Credential = new NetAppOntapCredentials()
            {
                Name = name,
                Username = userName,
                Password = password,
                UseSsl = useSsl
            };

            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(Credential, CredentialType.NetAppFiler);
                Credential.ID = id;
            }
        }
        else
        {
            int credentialId = -1;
            Int32.TryParse(credId, out credentialId);
            using (var proxy = BusinessLayerFactory.Create())
            {
                Credential = proxy.GetCredential(credentialId, CredentialType.NetAppFiler);
                var cred = (NetAppOntapCredentials)Credential;
                cred.Name = name;
                cred.Username = userName;
                cred.Password = changePassword ? password : cred.Password;
                cred.UseSsl = useSsl;
                proxy.UpdateCredential(cred, CredentialType.NetAppFiler);
            }

        }

    }

    [WebMethod()]
    public void CreateOrUpdateEmcUnityCredentials(string credId, string name, string userName, string password, bool useSsl, bool changePassword)
    {
        CheckNodeManagement();

        if (string.IsNullOrEmpty(credId))
        {
            Credential = new EmcUnityHttpCredential()
            {
                Name = name,
                Username = userName,
                Password = password,
                UseSsl = useSsl
            };

            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(Credential, CredentialType.UnityHttp);
                Credential.ID = id;
            }
        }
        else
        {
            int credentialId = -1;
            Int32.TryParse(credId, out credentialId);
            using (var proxy = BusinessLayerFactory.Create())
            {
                Credential = proxy.GetCredential(credentialId, CredentialType.UnityHttp);
                var cred = (EmcUnityHttpCredential)Credential;
                cred.Name = name;
                cred.Username = userName;
                cred.Password = changePassword ? password : cred.Password;
                cred.UseSsl = useSsl;
                proxy.UpdateCredential(cred, CredentialType.UnityHttp);
            }

        }

    }

    [WebMethod()]
    public void CreateOrUpdateGenericCredentials(string credId, string name, string userName, string password, bool useSsl, int port, bool changePassword)
    {
        CheckNodeManagement();

        if (string.IsNullOrEmpty(credId))
        {
            Credential = new GenericHttpCredential()
            {
                Name = name,
                Username = userName,
                Password = password,
                UseSsl = useSsl
            };
            ((IHttpCredential)Credential).Port = port;

            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(Credential, CredentialType.GenericHttp);
                Credential.ID = id;
            }
        }
        else
        {
            int credentialId = -1;
            Int32.TryParse(credId, out credentialId);
            using (var proxy = BusinessLayerFactory.Create())
            {
                Credential = proxy.GetCredential(credentialId, CredentialType.GenericHttp);
                var cred = (GenericHttpCredential)Credential;
                cred.Name = name;
                cred.Username = userName;
                cred.Password = changePassword ? password : cred.Password;
                cred.UseSsl = useSsl;
                ((IHttpCredential)cred).Port = port;

                proxy.UpdateCredential(cred, CredentialType.GenericHttp);
            }

        }

    }

    [WebMethod()]
    public void CreateOrUpdateInfiniBoxCredentials(string credId, string name, string userName, string password, bool useSsl, bool changePassword)
    {
        CheckNodeManagement();

        if (string.IsNullOrEmpty(credId))
        {
            Credential = new InfinidatHttpCredential()
            {
                Name = name,
                Username = userName,
                Password = password,
                UseSsl = useSsl
            };

            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(Credential, CredentialType.InfinidatHttp);
                Credential.ID = id;
            }
        }
        else
        {
            int credentialId = -1;
            Int32.TryParse(credId, out credentialId);
            using (var proxy = BusinessLayerFactory.Create())
            {
                Credential = proxy.GetCredential(credentialId, CredentialType.InfinidatHttp);
                var cred = (InfinidatHttpCredential)Credential;
                cred.Name = name;
                cred.Username = userName;
                cred.Password = changePassword ? password : cred.Password;
                cred.UseSsl = useSsl;
                proxy.UpdateCredential(cred, CredentialType.InfinidatHttp);
            }
        }
    }

    [WebMethod()]
    public void CreateOrUpdateDfmCredentials(string credId, string name, string userName, string password, bool useSsl, int httpsPort, int httpPort, bool changePassword)
    {
        CheckNodeManagement();

        if (string.IsNullOrEmpty(credId))
        {
            Credential = new NetAppDfmCredentials()
            {
                Name = name,
                Username = userName,
                Password = password,
                UseSsl = useSsl,
                HttpPort = httpPort,
                HttpsPort = httpsPort
            };

            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(Credential, CredentialType.NetAppDfm);
                Credential.ID = id;
            }
        }
        else
        {
            int credentialId = -1;
            Int32.TryParse(credId, out credentialId);
            using (var proxy = BusinessLayerFactory.Create())
            {
                Credential = proxy.GetCredential(credentialId, CredentialType.NetAppDfm);
                var cred = (NetAppDfmCredentials)Credential;
                cred.Name = name;
                cred.Username = userName;
                cred.Password = changePassword ? password : cred.Password;
                cred.UseSsl = useSsl;
                cred.HttpPort = httpPort;
                cred.HttpsPort = httpsPort;
                proxy.UpdateCredential(cred, CredentialType.NetAppDfm);
            }

        }

    }
    [WebMethod()]
    public void CreateOrUpdateVnxCredentials(string credId, string name, string userName, string password, string path, int port, bool changePassword)
    {
        CheckNodeManagement();

        if (string.IsNullOrEmpty(credId))
        {
            Credential = new VnxCredential()
            {
                Name = name,
                Username = userName,
                Password = password,
                Port = port,
                PathToManagementServices = path
            };

            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(Credential, CredentialType.VNX);
                Credential.ID = id;
            }
        }
        else
        {
            int credentialId = -1;
            Int32.TryParse(credId, out credentialId);
            using (var proxy = BusinessLayerFactory.Create())
            {
                Credential = proxy.GetCredential(credentialId, CredentialType.VNX);
                var cred = (VnxCredential)Credential;
                cred.Name = name;
                cred.Username = userName;
                cred.Password = changePassword ? password : cred.Password;
                cred.Port = port;
                cred.PathToManagementServices = path;
                proxy.UpdateCredential(cred, CredentialType.VNX);
            }

        }

    }
    [WebMethod()]
    public void CreateOrUpdateMsaCredentials(string credId, string name, string userName, string password, bool isHttp, int httpPort, int httpsPort, bool changePassword)
    {
        CheckNodeManagement();

        if (string.IsNullOrEmpty(credId))
        {
            Credential = new MsaCredential()
            {
                Name = name,
                Username = userName,
                Password = password,
                HttpPort = httpPort,
                HttpsPort = httpsPort,
                UseSsl = !isHttp
            };

            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(Credential, CredentialType.MSA);
                Credential.ID = id;
            }
        }
        else
        {
            int credentialId = -1;
            Int32.TryParse(credId, out credentialId);
            using (var proxy = BusinessLayerFactory.Create())
            {
                Credential = proxy.GetCredential(credentialId, CredentialType.MSA);
                var cred = (MsaCredential)Credential;
                cred.Name = name;
                cred.Username = userName;
                cred.Password = changePassword ? password : cred.Password;
                cred.HttpPort = httpPort;
                cred.HttpsPort = httpsPort;
                cred.UseSsl = !isHttp;
                proxy.UpdateCredential(cred, CredentialType.MSA);
            }

        }

    }

    [WebMethod()]
    public void CreateOrUpdateIsilonCredentials(string credId, string name, string userName, string password, bool isHttp, int port, bool changePassword)
    {
        CheckNodeManagement();

        if (string.IsNullOrEmpty(credId))
        {
            Credential = new IsilonHttpCredential()
            {
                Name = name,
                Username = userName,
                Password = password,
                Port = port,
                UseSsl = !isHttp
            };

            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(Credential, CredentialType.IsilonHttp);
                Credential.ID = id;
            }
        }
        else
        {
            int credentialId = -1;
            Int32.TryParse(credId, out credentialId);
            using (var proxy = BusinessLayerFactory.Create())
            {
                Credential = proxy.GetCredential(credentialId, CredentialType.IsilonHttp);
                var cred = (IsilonHttpCredential)Credential;
                cred.Name = name;
                cred.Username = userName;
                cred.Password = changePassword ? password : cred.Password;
                cred.Port = port;
                cred.UseSsl = !isHttp;
                proxy.UpdateCredential(cred, CredentialType.IsilonHttp);
            }

        }
    }

    [WebMethod()]
    public void CreateOrUpdatePureStorageCredentials(string credId, string name, string userName, string password, bool isHttp, int httpPort, int httpsPort, bool changePassword)
    {
        CheckNodeManagement();

        if (string.IsNullOrEmpty(credId))
        {
            Credential = new PureStorageHttpCredential()
            {
                Name = name,
                Username = userName,
                Password = password,
                HttpPort = httpPort,
                HttpsPort = httpsPort,
                UseSsl = !isHttp
            };

            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(Credential, CredentialType.PureStorageHttp);
                Credential.ID = id;
            }
        }
        else
        {
            int credentialId = -1;
            Int32.TryParse(credId, out credentialId);
            using (var proxy = BusinessLayerFactory.Create())
            {
                Credential = proxy.GetCredential(credentialId, CredentialType.PureStorageHttp);
                var cred = (PureStorageHttpCredential)Credential;
                cred.Name = name;
                cred.Username = userName;
                cred.Password = changePassword ? password : cred.Password;
                cred.HttpPort = httpPort;
                cred.HttpsPort = httpsPort;
                cred.UseSsl = !isHttp;
                proxy.UpdateCredential(cred, CredentialType.PureStorageHttp);
            }

        }

    }

    [WebMethod()]
    public void CreateOrUpdateXtremIOCredentials(string credId, string name, string userName, string password, bool isHttp, int httpPort, int httpsPort, bool changePassword)
    {
        CheckNodeManagement();

        if (string.IsNullOrEmpty(credId))
        {
            Credential = new XtremIoHttpCredential()
            {
                Name = name,
                Username = userName,
                Password = password,
                HttpPort = httpPort,
                HttpsPort = httpsPort,
                UseSsl = !isHttp
            };

            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(Credential, CredentialType.XtremIoHttp);
                Credential.ID = id;
            }
        }
        else
        {
            int credentialId = -1;
            Int32.TryParse(credId, out credentialId);
            using (var proxy = BusinessLayerFactory.Create())
            {
                Credential = proxy.GetCredential(credentialId, CredentialType.XtremIoHttp);
                var cred = (XtremIoHttpCredential)Credential;
                cred.Name = name;
                cred.Username = userName;
                cred.Password = changePassword ? password : cred.Password;
                cred.HttpPort = httpPort;
                cred.HttpsPort = httpsPort;
                cred.UseSsl = !isHttp;
                proxy.UpdateCredential(cred, CredentialType.XtremIoHttp);
            }
        }
    }

    [WebMethod()]
    public void CreateOrUpdateKaminarioCredentials(string credId, string name, string userName, string password, bool useSsl, int port, bool changePassword)
    {
        CheckNodeManagement();

        if (string.IsNullOrEmpty(credId))
        {
            Credential = new GenericHttpCredential()
            {
                Name = name,
                Username = userName,
                Password = password,
                UseSsl = useSsl,
                Port = port
            };

            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(Credential, CredentialType.GenericHttp);
                Credential.ID = id;
            }
        }
        else
        {
            int credentialId = -1;
            Int32.TryParse(credId, out credentialId);
            using (var proxy = BusinessLayerFactory.Create())
            {
                var credential = proxy.GetCredential(credentialId, CredentialType.GenericHttp) as GenericHttpCredential;
                if (credential == null)
                {
                    throw new ArgumentException($"Cannot find credential id={credId}");
                }
                credential.Name = name;
                credential.Username = userName;
                credential.Password = changePassword ? password : credential.Password;
                credential.UseSsl = useSsl;
                credential.Port = port;

                proxy.UpdateCredential(credential, CredentialType.GenericHttp);
                Credential = credential;
            }
        }
    }

    [WebMethod()]
    public JsonObject GetCredentialsProperty(string credId)
    {
        int credentialId;
        Int32.TryParse(credId, out credentialId);
        var credDal = ServiceLocator.GetInstance<ICredentialPropertiesDAL>();
        dynamic properties = new JsonObject();

        var credentialType = credDal.GetTypenameForId(credentialId);

        var credentialNetType = (from asm in AppDomain.CurrentDomain.GetAssemblies()
                                 let type = asm.GetType(credentialType)
                                 where type != null
                                 select type).FirstOrDefault();

        DataTable dataTable = credDal.GetAllPropertiesForId(credentialId);

        foreach (DataRow row in dataTable.Rows)
        {
            string rowName = row["Name"].ToString();
            var propertyType = typeof (string);
            if (credentialNetType != null)
            {
                var property = credentialNetType.GetProperty(rowName);
                propertyType = property.PropertyType;
            }

            switch (rowName.ToLower())
            {
                case "community":
                    {
                        properties.Community = row["Value"];
                        break;
                    }
                case "authenticationkeyispassword":
                    {
                        properties.AuthenticationKeyIsPassword = row["Value"];
                        break;
                    }
                case "authenticationpassword":
                    {
                        properties.AuthenticationPassword = EmptyPassword;
                        break;
                    }
                case "authenticationtype":
                    {
                        var authenticationTypes = new HashSet<String>(Enum.GetValues(typeof(SNMPv3AuthType)).Cast<SNMPv3AuthType>()
                                                                          .Where(o => o != SNMPv3AuthType.MD5 || !FIPSHelper.FIPSRestrictionEnabled)
                                                                          .Select(o => o.GetDescription().ToLowerInvariant()));

                        if (!authenticationTypes.Contains(row["Value"].ToString().ToLowerInvariant()))
                        {
                            return null;
                        }

                        properties.AuthenticationType = row["Value"];
                        break;
                    }
                case "context":
                    {
                        properties.Context = row["Value"];
                        break;
                    }
                case "interopnamespace":
                    {
                        properties.InteropNamespace = row["Value"];
                        break;
                    }
                case "namespace":
                    {
                        properties.Namespace = row["Value"];
                        break;
                    }
                case "pathtomanagementservices":
                    {
                        properties.PathToManagementServices = row["Value"];
                        break;
                    }
                case "password":
                    {
                        properties.Password = string.Empty;
                        break;
                    }
                case "port":
                    {
                        properties.Port = row["Value"];
                        break;
                    }
                case "httpport":
                    {
                        properties.HttpPort = row["Value"];
                        break;
                    }
                case "httpsport":
                    {
                        properties.HttpsPort = row["Value"];
                        break;
                    }
                case "privacykeyispassword":
                    {
                        properties.PrivacyKeyIsPassword = row["Value"];
                        break;
                    }
                case "privacypassword":
                    {
                        properties.PrivacyPassword = EmptyPassword;
                        break;
                    }
                case "privacytype":
                    {
                        var provacyTypes = new HashSet<String>(Enum.GetValues(typeof(SNMPv3PrivacyType)).Cast<SNMPv3PrivacyType>()
                                                                   .Where(o => o != SNMPv3PrivacyType.DES56 || !FIPSHelper.FIPSRestrictionEnabled)
                                                                   .Select(o => o.GetDescription().ToLowerInvariant()));

                        if (!provacyTypes.Contains(row["Value"].ToString().ToLowerInvariant()))
                        {
                            return null;
                        }

                        properties.PrivacyType = row["Value"];
                        break;
                    }
                case "target":
                    {
                        properties.Target = row["Value"];
                        break;
                    }
                case "username":
                    {
                        properties.Username = row["Value"];
                        break;
                    }
                case "usessl":
                    {
                        properties.UseSSL = row["Value"];
                        break;
                    }
                default:
                    {
                        // Return type as .net types, not strings
                        var converter = TypeDescriptor.GetConverter(propertyType);
                        var rowValue = row["Value"].ToString();
                        ((JsonObject)properties).Dictionary[rowName] = converter.ConvertFromInvariantString(rowValue);
                        break;
                    }
            }

        }
        properties.Id = credentialId;
        return properties;
    }

}
