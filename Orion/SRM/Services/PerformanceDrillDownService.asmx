﻿<%@ WebService Language="C#" Class="PerformanceDrillDownService" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;

using Resources;

using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Models.Thresholds;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.Resources;
using SolarWinds.SRM.Web;
using Threshold = SolarWinds.SRM.Web.Models.Threshold;
using V2DataPoint = SolarWinds.Orion.Web.Charting.v2.DataPoint;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class PerformanceDrillDownService : ChartDataWebService
{
    private static readonly Log Log = new Log();
    
    private const string totalCategoryKey = "Total";
    private const string readCategoryKey = "Read";
    private const string writeCategoryKey = "Write";
    private const string otherCategoryKey = "Other";
    private const string iopsCategory = "IOPS";
    private const string latencyCategory = "Latency";
    private const string throughputCategory = "Throughput";
    private const string ioSizeCategory = "IOSize";
    private const string queueLengthCategory = "QueueLength";
    private const string iopsRatioCategory = "IOPSRatio";
    private const string diskBusyCategory = "DiskBusy";
    private const string cacheHitRatioCategory = "CacheHitRatio";
    private const string utilizationCategory = "Utilization";

    private readonly Dictionary<string, string> drilldownChartsI18n = new Dictionary<string, string>
        {
            { "IOPS_Total", SrmWebContent.DrillDown_IOPS_Total },
            { "IOPS_Read", SrmWebContent.DrillDown_IOPS_Read },
            { "IOPS_Write", SrmWebContent.DrillDown_IOPS_Write },
            { "IOPS_Other", SrmWebContent.DrillDown_IOPS_Other },
            { "Latency_Total", SrmWebContent.DrillDown_Latency_Total },
            { "Latency_Read", SrmWebContent.DrillDown_Latency_Read },
            { "Latency_Write", SrmWebContent.DrillDown_Latency_Write },
            { "Latency_Other", SrmWebContent.DrillDown_Latency_Other },
            { "Throughput_Total", SrmWebContent.DrillDown_Throughput_Total },
            { "Throughput_Read", SrmWebContent.DrillDown_Throughput_Read },
            { "Throughput_Write", SrmWebContent.DrillDown_Throughput_Write },
            { "IOSize_Total", SrmWebContent.DrillDown_IOSize_Total },
            { "IOSize_Read", SrmWebContent.DrillDown_IOSize_Read },
            { "IOSize_Write", SrmWebContent.DrillDown_IOSize_Write },
            { "QueueLength_Total", SrmWebContent.DrillDown_QueueLength },
            { "IOPSRatio_Total", SrmWebContent.DrillDown_IOPSRatio },
            { "DiskBusy_Total", SrmWebContent.DrillDown_DiskBusy },
            { "CacheHitRatio_Total", SrmWebContent.DrillDown_CacheHitRatio_Total },
            { "CacheHitRatio_Read", SrmWebContent.DrillDown_CacheHitRatio_Read },
            { "CacheHitRatio_Write", SrmWebContent.DrillDown_CacheHitRatio_Write },
            { "Utilization_Total", SrmWebContent.DrillDown_Utilization }
        };

    private class DataSeriesWithThreshold : DataSeries
    {
        public Threshold Threshold { get; set; }
    }

    [WebMethod]
    public ChartDataResults GetPerformance(ChartDataRequest request)
    {
        string netObjectPrefix = request.NetObjectIds[0].Split(':')[0];
        int netObjectID = int.Parse(request.NetObjectIds[0].Split(':')[1]);
        string requestParameter = (string)request.AllSettings["requestParameter"];
        int daysOfDataToLoad = (int)request.AllSettings["daysOfDataToLoad"];
        if (netObjectPrefix.Equals(FileShares.NetObjectPrefix))
        {
            netObjectID = ServiceLocator.GetInstance<IFileSharesDAL>().GetFileSharesDetails(netObjectID).VolumeID;
        }

        int sampleSizeInMinutes = request.SampleSizeInMinutes;

        IList<PerformanceStatisticsModel> performanceStatisticsModelList = new List<PerformanceStatisticsModel>();
        DateTime endTime = DateTime.UtcNow;
        DateTime startTime = endTime.AddDays(-daysOfDataToLoad);

        switch (netObjectPrefix.ToUpper())
        {
            case Lun.NetObjectPrefix:
                performanceStatisticsModelList = ServiceLocator.GetInstance<ILunDAL>().GetLunStatisticsByTime(netObjectID, startTime, endTime);
                break;
            case Volume.NetObjectPrefix:
            case FileShares.NetObjectPrefix:
                performanceStatisticsModelList = ServiceLocator.GetInstance<IVolumeDAL>().GetVolumeStatisticsByTime(netObjectID, startTime, endTime);
                break;
            case Pool.NetObjectPrefix:
                performanceStatisticsModelList = ServiceLocator.GetInstance<IPoolDAL>().GetPoolStatisticsByTime(netObjectID, startTime, endTime);
                break;
            case StorageArray.NetObjectPrefix:
                performanceStatisticsModelList = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayStatisticsByTime(netObjectID, startTime, endTime);
                break;
            case VServer.NetObjectPrefix:
                performanceStatisticsModelList = ServiceLocator.GetInstance<IVServerDAL>().GetVServerStatisticsByTime(netObjectID, startTime, endTime);
                break;
            case StorageController.NetObjectPrefix:
                performanceStatisticsModelList = ServiceLocator.GetInstance<IStorageControllerDAL>().GetStorageControllerStatisticsByTime(netObjectID, startTime, endTime);
                break;
        }

        List<DataSeriesWithThreshold> series = new List<DataSeriesWithThreshold>();
        Dictionary<string, List<KeyValuePair<DateTime, double?>>> groupedCategory = new Dictionary<string, List<KeyValuePair<DateTime, double?>>>();

        switch (requestParameter)
        {
            case iopsCategory:
                groupedCategory = GetIOPSData(performanceStatisticsModelList, sampleSizeInMinutes);
                break;
            case latencyCategory:
                groupedCategory = GetLatencyData(performanceStatisticsModelList, sampleSizeInMinutes);
                break;
            case throughputCategory:
                groupedCategory = GetThroughputData(performanceStatisticsModelList, sampleSizeInMinutes);
                break;
            case ioSizeCategory:
                groupedCategory = GetIOSizeData(performanceStatisticsModelList, sampleSizeInMinutes);
                break;
            case queueLengthCategory:
                groupedCategory = GetQueueLengthData(performanceStatisticsModelList, sampleSizeInMinutes);
                break;
            case iopsRatioCategory:
                groupedCategory = GetIOPSRatioData(performanceStatisticsModelList, sampleSizeInMinutes);
                break;
            case diskBusyCategory:
                groupedCategory = GetDiskBusyData(performanceStatisticsModelList, sampleSizeInMinutes);
                break;
            case cacheHitRatioCategory:
                groupedCategory = GetCacheHitRatioData(performanceStatisticsModelList, sampleSizeInMinutes);
                break;
            case utilizationCategory:
                groupedCategory = GetUtilizationData(performanceStatisticsModelList, sampleSizeInMinutes);
                break;
        }

        dynamic options = new JsonObject();
        int customPlotBandsCount = 0;
        options.CustomPlotBands = JsonObject.CreateJsonArray(groupedCategory.Count);
        foreach (var groupedRow in groupedCategory)
        {
            List<KeyValuePair<DateTime, double?>> temp = groupedRow.Value;

            DataSeriesWithThreshold chartSeries = CreateSeriesWithThreshold(requestParameter, groupedRow.Key);

            chartSeries.Threshold = GetThresholdValues(netObjectPrefix, netObjectID, requestParameter, groupedRow.Key);

            var groupedData = temp.GroupBy(s => s.Key).Select(grp => new KeyValuePair<DateTime, double?>(grp.Key, grp.Average(p => p.Value))).ToList();
            chartSeries.Data = WebHelper.SetDatePoints(request.SampleSizeInMinutes, startTime, endTime, groupedData).ToList();
            series.Add(chartSeries);


            options.CustomPlotBands[customPlotBandsCount].Warning = chartSeries.Threshold.WarnLevel;
            options.CustomPlotBands[customPlotBandsCount].Critical = chartSeries.Threshold.CriticalLevel;
            options.CustomPlotBands[customPlotBandsCount].Operator = chartSeries.Threshold.ThresholdOperator.ToString();
            options.CustomPlotBands[customPlotBandsCount].SeriesName = groupedRow.Key;
            customPlotBandsCount++;
        }
        return new ChartDataResults(series) { ChartOptionsOverride = options };
    }


    /// <summary>
    /// Get threshold value by category and net object prefix.
    /// </summary>
    /// <param name="netObjectId">NetObjectId.</param>
    /// <param name="netObjectPrefix">NetObjectPrefix value.</param>
    /// <param name="category">Performance category value.</param>
    /// <returns>Threshold Model.</returns>
    private Threshold GetThresholdValues(string netObjectPrefix, int netObjectId, string category, string field)
    {
        Threshold threshold = new Threshold();
        switch (netObjectPrefix.ToUpper())
        {
            case Lun.NetObjectPrefix:
                switch (category)
                {
                    case iopsCategory:
                        threshold = GetThreshold(netObjectId, iopsCategory, field, new Dictionary<string, ThresholdDataType>
                        {
                            {totalCategoryKey, ThresholdDataType.LunIOPSTotal},
                            {readCategoryKey, ThresholdDataType.LunIOPSRead},
                            {writeCategoryKey, ThresholdDataType.LunIOPSWrite},
                            {otherCategoryKey, ThresholdDataType.LunIOPSOther}
                        });
                        break;
                    case latencyCategory:
                        threshold = GetThreshold(netObjectId, latencyCategory, field, new Dictionary<string, ThresholdDataType>
                        {
                            {totalCategoryKey, ThresholdDataType.LunLatencyTotal},
                            {readCategoryKey, ThresholdDataType.LunLatencyRead},
                            {writeCategoryKey, ThresholdDataType.LunLatencyWrite},
                            {otherCategoryKey, ThresholdDataType.LunLatencyOther}
                        });
                        break;
                    case throughputCategory:
                        threshold = GetThreshold(netObjectId, throughputCategory, field, new Dictionary<string, ThresholdDataType>
                        {
                            {totalCategoryKey, ThresholdDataType.LunThroughputTotal},
                            {readCategoryKey, ThresholdDataType.LunThroughputRead},
                            {writeCategoryKey, ThresholdDataType.LunThroughputWrite}
                        });
                        break;
                    case ioSizeCategory:
                        threshold = GetThreshold(netObjectId, ioSizeCategory, field, new Dictionary<string, ThresholdDataType>
                        {
                            {totalCategoryKey, ThresholdDataType.LunIOSizeTotal},
                            {readCategoryKey, ThresholdDataType.LunIOSizeRead},
                            {writeCategoryKey, ThresholdDataType.LunIOSizeWrite}
                        });
                        break;
                    case queueLengthCategory:
                        threshold.ThresholdOperator = ThresholdOperatorEnum.Greater;
                        threshold.WarnLevel = ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.LunQueqeLengthTotal);
                        threshold.CriticalLevel = ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.LunQueqeLengthTotal);
                        break;
                    case iopsRatioCategory:
                        threshold.ThresholdOperator = ThresholdOperatorEnum.Greater;
                        threshold.WarnLevel = ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.LunIOPSRatio);
                        threshold.CriticalLevel = ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.LunIOPSRatio);
                        break;
                    case diskBusyCategory:
                        threshold.ThresholdOperator = ThresholdOperatorEnum.Greater;
                        threshold.WarnLevel = ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.LunDiskBusy);
                        threshold.CriticalLevel = ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.LunDiskBusy);
                        break;
                    case cacheHitRatioCategory:
                        threshold.ThresholdOperator = ThresholdOperatorEnum.Less;
                        threshold.WarnLevel = ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.LunCacheHitRatio);
                        threshold.CriticalLevel = ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.LunCacheHitRatio);
                        break;
                }
                break;
            case Volume.NetObjectPrefix:
            case FileShares.NetObjectPrefix: // file share shows volume data, so these are identical
                switch (category)
                {
                    case iopsCategory:
                        threshold = GetThreshold(netObjectId, iopsCategory, field, new Dictionary<string, ThresholdDataType>
                        {
                            {totalCategoryKey, ThresholdDataType.VolumeIOPSTotal},
                            {readCategoryKey, ThresholdDataType.VolumeIOPSRead},
                            {writeCategoryKey, ThresholdDataType.VolumeIOPSWrite},
                            {otherCategoryKey, ThresholdDataType.VolumeIOPSOther}
                        });
                        break;
                    case latencyCategory:
                        threshold = GetThreshold(netObjectId, latencyCategory, field, new Dictionary<string, ThresholdDataType>
                        {
                            {totalCategoryKey, ThresholdDataType.VolumeLatencyTotal},
                            {readCategoryKey, ThresholdDataType.VolumeLatencyRead},
                            {writeCategoryKey, ThresholdDataType.VolumeLatencyWrite},
                            {otherCategoryKey, ThresholdDataType.VolumeLatencyOther}
                        });
                        break;
                    case throughputCategory:
                        threshold = GetThreshold(netObjectId, throughputCategory, field, new Dictionary<string, ThresholdDataType>
                        {
                            {totalCategoryKey, ThresholdDataType.VolumeThroughputTotal},
                            {readCategoryKey, ThresholdDataType.VolumeThroughputRead},
                            {writeCategoryKey, ThresholdDataType.VolumeThroughputWrite}
                        });
                        break;
                    case ioSizeCategory:
                        threshold = GetThreshold(netObjectId, ioSizeCategory, field, new Dictionary<string, ThresholdDataType>
                        {
                            {totalCategoryKey, ThresholdDataType.VolumeIOSizeTotal},
                            {readCategoryKey, ThresholdDataType.VolumeIOSizeRead},
                            {writeCategoryKey, ThresholdDataType.VolumeIOSizeWrite}
                        });
                        break;
                    case queueLengthCategory:
                        threshold.ThresholdOperator = ThresholdOperatorEnum.Greater;
                        threshold.WarnLevel = ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.VolumeQueqeLengthTotal);
                        threshold.CriticalLevel = ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.VolumeQueqeLengthTotal);
                        break;
                    case iopsRatioCategory:
                        threshold.ThresholdOperator = ThresholdOperatorEnum.Greater;
                        threshold.WarnLevel = ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.VolumeIOPSRatio);
                        threshold.CriticalLevel = ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.VolumeIOPSRatio);
                        break;
                }
                break;
            case Pool.NetObjectPrefix:
                switch (category)
                {
                    case iopsCategory:
                        threshold = GetThreshold(netObjectId, iopsCategory, field, new Dictionary<string, ThresholdDataType>
                        {
                            {totalCategoryKey, ThresholdDataType.PoolIOPSTotal},
                            {readCategoryKey, ThresholdDataType.PoolIOPSRead},
                            {writeCategoryKey, ThresholdDataType.PoolIOPSWrite},
                            {otherCategoryKey, ThresholdDataType.PoolIOPSOther}
                        });
                        break;
                    case throughputCategory:
                        threshold = GetThreshold(netObjectId, throughputCategory, field, new Dictionary<string, ThresholdDataType>
                        {
                            {totalCategoryKey, ThresholdDataType.PoolThroughputTotal},
                            {readCategoryKey, ThresholdDataType.PoolThroughputRead},
                            {writeCategoryKey, ThresholdDataType.PoolThroughputWrite}
                        });
                        break;
                    case ioSizeCategory:
                        threshold = GetThreshold(netObjectId, ioSizeCategory, field, new Dictionary<string, ThresholdDataType>
                        {
                            {totalCategoryKey, ThresholdDataType.PoolIOSizeTotal},
                            {readCategoryKey, ThresholdDataType.PoolIOSizeRead},
                            {writeCategoryKey, ThresholdDataType.PoolIOSizeWrite}
                        });
                        break;
                    case cacheHitRatioCategory:
                        threshold.ThresholdOperator = ThresholdOperatorEnum.Less;
                        threshold.WarnLevel = ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.PoolCacheHitRatio);
                        threshold.CriticalLevel = ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.PoolCacheHitRatio);
                        break;
                }
                break;
            case StorageArray.NetObjectPrefix:
                switch (category)
                {
                    case iopsCategory:
                        threshold = GetThreshold(netObjectId, iopsCategory, field, new Dictionary<string, ThresholdDataType>
                        {
                            {totalCategoryKey, ThresholdDataType.ArrayIOPSTotal},
                            {readCategoryKey, ThresholdDataType.ArrayIOPSRead},
                            {writeCategoryKey, ThresholdDataType.ArrayIOPSWrite},
                            {otherCategoryKey, ThresholdDataType.ArrayIOPSOther}
                        });
                        break;
                    case throughputCategory:
                        threshold = GetThreshold(netObjectId, throughputCategory, field, new Dictionary<string, ThresholdDataType>
                        {
                            {totalCategoryKey, ThresholdDataType.ArrayThroughputTotal},
                            {readCategoryKey, ThresholdDataType.ArrayThroughputRead},
                            {writeCategoryKey, ThresholdDataType.ArrayThroughputWrite}
                        });
                        break;
                    case ioSizeCategory:
                        threshold = GetThreshold(netObjectId, ioSizeCategory, field, new Dictionary<string, ThresholdDataType>
                        {
                            {totalCategoryKey, ThresholdDataType.ArrayIOSizeTotal},
                            {readCategoryKey, ThresholdDataType.ArrayIOSizeRead},
                            {writeCategoryKey, ThresholdDataType.ArrayIOSizeWrite}
                        });
                        break;
                    case cacheHitRatioCategory:
                        threshold.ThresholdOperator = ThresholdOperatorEnum.Less;
                        threshold.WarnLevel = ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.ArrayCacheHitRatio);
                        threshold.CriticalLevel = ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.ArrayCacheHitRatio);
                        break;
                }
                break;
            case VServer.NetObjectPrefix:
                switch (category)
                {
                    case iopsCategory:
                        threshold = GetThreshold(netObjectId, iopsCategory, field, new Dictionary<string, ThresholdDataType>
                        {
                            {totalCategoryKey, ThresholdDataType.VserverIOPSTotal},
                            {readCategoryKey, ThresholdDataType.VserverIOPSRead},
                            {writeCategoryKey, ThresholdDataType.VserverIOPSWrite},
                            {otherCategoryKey, ThresholdDataType.VserverIOPSOther}
                        });
                        break;
                    case throughputCategory:
                        threshold = GetThreshold(netObjectId, throughputCategory, field, new Dictionary<string, ThresholdDataType>
                        {
                            {totalCategoryKey, ThresholdDataType.VserverThroughputTotal},
                            {readCategoryKey, ThresholdDataType.VserverThroughputRead},
                            {writeCategoryKey, ThresholdDataType.VserverThroughputWrite}
                        });
                        break;
                    case ioSizeCategory:
                        threshold = GetThreshold(netObjectId, ioSizeCategory, field, new Dictionary<string, ThresholdDataType>
                        {
                            {totalCategoryKey, ThresholdDataType.VserverIOSizeTotal},
                            {readCategoryKey, ThresholdDataType.VserverIOSizeRead},
                            {writeCategoryKey, ThresholdDataType.VserverIOSizeWrite}
                        });
                        break;
                }
                break;
        }
        
        if (threshold == null)
        {
            Log.ErrorFormat("Threshold not found: {0}:{1} {2} - {3}",
                            netObjectPrefix,
                            netObjectId,
                            category,
                            field);

            threshold = Threshold.Safe;
        }
        
        return threshold;
    }

    private Threshold GetThreshold(int netObjectId, string category, string field, Dictionary<string, ThresholdDataType> thresholdDataTypes)
    {
        Threshold threshold;
        var tDal = ServiceLocator.GetInstance<IThresholdDAL>();

        switch (field)
        {
            case totalCategoryKey: threshold = tDal.GetThresholdOrSafe(thresholdDataTypes[totalCategoryKey], netObjectId); break;
            case readCategoryKey: threshold = tDal.GetThresholdOrSafe(thresholdDataTypes[readCategoryKey], netObjectId); break;
            case writeCategoryKey: threshold = tDal.GetThresholdOrSafe(thresholdDataTypes[writeCategoryKey], netObjectId); break;
            case otherCategoryKey: threshold = tDal.GetThresholdOrSafe(thresholdDataTypes[otherCategoryKey], netObjectId); break;
            default: throw new NotSupportedException(field);
        }

        return threshold;
    }


    /// <summary>
    /// Get IOPS performance data.
    /// </summary>
    /// <param name="statisticsModels">Collection of PerformanceStatisticsModels.</param>
    /// <param name="sampleSizeInMinutes">Sample Size In Minutes.</param>
    /// <returns>Dictionary of performance data.</returns>
    private Dictionary<string, List<KeyValuePair<DateTime, double?>>> GetIOPSData(IEnumerable<PerformanceStatisticsModel> statisticsModels, int sampleSizeInMinutes)
    {
        Dictionary<string, List<KeyValuePair<DateTime, double?>>> groupedCategory = new Dictionary<string, List<KeyValuePair<DateTime, double?>>>();
        List<KeyValuePair<DateTime, double?>> totalCategoryList = new List<KeyValuePair<DateTime, double?>>();
        List<KeyValuePair<DateTime, double?>> readCategoryList = new List<KeyValuePair<DateTime, double?>>();
        List<KeyValuePair<DateTime, double?>> writeCategoryList = new List<KeyValuePair<DateTime, double?>>();
        List<KeyValuePair<DateTime, double?>> otherCategoryList = new List<KeyValuePair<DateTime, double?>>();

        foreach (PerformanceStatisticsModel model in statisticsModels)
        {
            DateTime datetime = WebHelper.ToSampleDateTime(model.Timestamp, sampleSizeInMinutes);
            totalCategoryList.Add(new KeyValuePair<DateTime, double?>(datetime, model.IOPSTotal));
            readCategoryList.Add(new KeyValuePair<DateTime, double?>(datetime, model.IOPSRead));
            writeCategoryList.Add(new KeyValuePair<DateTime, double?>(datetime, model.IOPSWrite));
            otherCategoryList.Add(new KeyValuePair<DateTime, double?>(datetime, model.IOPSOther));
        }
        groupedCategory.Add(totalCategoryKey, totalCategoryList);
        groupedCategory.Add(readCategoryKey, readCategoryList);
        groupedCategory.Add(writeCategoryKey, writeCategoryList);
        groupedCategory.Add(otherCategoryKey, otherCategoryList);
        return groupedCategory;
    }


    /// <summary>
    /// Get Latency performance data.
    /// </summary>
    /// <param name="statisticsModels">Collection of PerformanceStatisticsModels.</param>
    /// <param name="sampleSizeInMinutes">Sample Size In Minutes.</param>
    /// <returns>Dictionary of performance data.</returns>
    private Dictionary<string, List<KeyValuePair<DateTime, double?>>> GetLatencyData(IEnumerable<PerformanceStatisticsModel> statisticsModels, int sampleSizeInMinutes)
    {
        Dictionary<string, List<KeyValuePair<DateTime, double?>>> groupedCategory = new Dictionary<string, List<KeyValuePair<DateTime, double?>>>();
        List<KeyValuePair<DateTime, double?>> totalCategoryList = new List<KeyValuePair<DateTime, double?>>();
        List<KeyValuePair<DateTime, double?>> readCategoryList = new List<KeyValuePair<DateTime, double?>>();
        List<KeyValuePair<DateTime, double?>> writeCategoryList = new List<KeyValuePair<DateTime, double?>>();
        List<KeyValuePair<DateTime, double?>> otherCategoryList = new List<KeyValuePair<DateTime, double?>>();

        foreach (PerformanceStatisticsModel model in statisticsModels)
        {
            DateTime datetime = WebHelper.ToSampleDateTime(model.Timestamp, sampleSizeInMinutes);
            totalCategoryList.Add(new KeyValuePair<DateTime, double?>(datetime, model.IOLatencyTotal));
            readCategoryList.Add(new KeyValuePair<DateTime, double?>(datetime, model.IOLatencyRead));
            writeCategoryList.Add(new KeyValuePair<DateTime, double?>(datetime, model.IOLatencyWrite));
            otherCategoryList.Add(new KeyValuePair<DateTime, double?>(datetime, model.IOLatencyOther));
        }
        groupedCategory.Add(totalCategoryKey, totalCategoryList);
        groupedCategory.Add(readCategoryKey, readCategoryList);
        groupedCategory.Add(writeCategoryKey, writeCategoryList);
        groupedCategory.Add(otherCategoryKey, otherCategoryList);
        return groupedCategory;
    }


    /// <summary>
    /// Get Throughput performance data.
    /// </summary>
    /// <param name="statisticsModels">Collection of PerformanceStatisticsModels.</param>
    /// <param name="sampleSizeInMinutes">Sample Size In Minutes.</param>
    /// <returns>Dictionary of performance data.</returns>
    private Dictionary<string, List<KeyValuePair<DateTime, double?>>> GetThroughputData(IEnumerable<PerformanceStatisticsModel> statisticsModels, int sampleSizeInMinutes)
    {
        Dictionary<string, List<KeyValuePair<DateTime, double?>>> groupedCategory = new Dictionary<string, List<KeyValuePair<DateTime, double?>>>();
        List<KeyValuePair<DateTime, double?>> totalCategoryList = new List<KeyValuePair<DateTime, double?>>();
        List<KeyValuePair<DateTime, double?>> readCategoryList = new List<KeyValuePair<DateTime, double?>>();
        List<KeyValuePair<DateTime, double?>> writeCategoryList = new List<KeyValuePair<DateTime, double?>>();

        foreach (PerformanceStatisticsModel model in statisticsModels)
        {
            DateTime datetime = WebHelper.ToSampleDateTime(model.Timestamp, sampleSizeInMinutes);
            totalCategoryList.Add(new KeyValuePair<DateTime, double?>(datetime, model.BytesPSTotal));
            readCategoryList.Add(new KeyValuePair<DateTime, double?>(datetime, model.BytesPSRead));
            writeCategoryList.Add(new KeyValuePair<DateTime, double?>(datetime, model.BytesPSWrite));
        }
        groupedCategory.Add(totalCategoryKey, totalCategoryList);
        groupedCategory.Add(readCategoryKey, readCategoryList);
        groupedCategory.Add(writeCategoryKey, writeCategoryList);
        return groupedCategory;
    }


    /// <summary>
    /// Get IOSize performance data.
    /// </summary>
    /// <param name="statisticsModels">Collection of PerformanceStatisticsModels.</param>
    /// <param name="sampleSizeInMinutes">Sample Size In Minutes.</param>
    /// <returns>Dictionary of performance data.</returns>
    private Dictionary<string, List<KeyValuePair<DateTime, double?>>> GetIOSizeData(IEnumerable<PerformanceStatisticsModel> statisticsModels, int sampleSizeInMinutes)
    {
        Dictionary<string, List<KeyValuePair<DateTime, double?>>> groupedCategory = new Dictionary<string, List<KeyValuePair<DateTime, double?>>>();
        List<KeyValuePair<DateTime, double?>> totalCategoryList = new List<KeyValuePair<DateTime, double?>>();
        List<KeyValuePair<DateTime, double?>> readCategoryList = new List<KeyValuePair<DateTime, double?>>();
        List<KeyValuePair<DateTime, double?>> writeCategoryList = new List<KeyValuePair<DateTime, double?>>();

        foreach (PerformanceStatisticsModel model in statisticsModels)
        {
            DateTime datetime = WebHelper.ToSampleDateTime(model.Timestamp, sampleSizeInMinutes);
            totalCategoryList.Add(new KeyValuePair<DateTime, double?>(datetime, model.IOSizeTotal));
            readCategoryList.Add(new KeyValuePair<DateTime, double?>(datetime, model.IOSizeRead));
            writeCategoryList.Add(new KeyValuePair<DateTime, double?>(datetime, model.IOSizeWrite));
        }
        groupedCategory.Add(totalCategoryKey, totalCategoryList);
        groupedCategory.Add(readCategoryKey, readCategoryList);
        groupedCategory.Add(writeCategoryKey, writeCategoryList);
        return groupedCategory;
    }


    /// <summary>
    /// Get Queue Length performance data.
    /// </summary>
    /// <param name="statisticsModels">Collection of PerformanceStatisticsModels.</param>
    /// <param name="sampleSizeInMinutes">Sample Size In Minutes.</param>
    /// <returns>Dictionary of performance data.</returns>
    private Dictionary<string, List<KeyValuePair<DateTime, double?>>> GetQueueLengthData(IEnumerable<PerformanceStatisticsModel> statisticsModels, int sampleSizeInMinutes)
    {
        Dictionary<string, List<KeyValuePair<DateTime, double?>>> groupedCategory = new Dictionary<string, List<KeyValuePair<DateTime, double?>>>();
        List<KeyValuePair<DateTime, double?>> totalCategoryList = new List<KeyValuePair<DateTime, double?>>();

        foreach (PerformanceStatisticsModel model in statisticsModels)
        {
            DateTime datetime = WebHelper.ToSampleDateTime(model.Timestamp, sampleSizeInMinutes);

            totalCategoryList.Add(new KeyValuePair<DateTime, double?>(datetime, model.QueueLength));
        }
        groupedCategory.Add(totalCategoryKey, totalCategoryList);
        return groupedCategory;
    }


    /// <summary>
    /// Get IOPS Ratio performance data.
    /// </summary>
    /// <param name="statisticsModels">Collection of PerformanceStatisticsModels.</param>
    /// <param name="sampleSizeInMinutes">Sample Size In Minutes.</param>
    /// <returns>Dictionary of performance data.</returns>
    private Dictionary<string, List<KeyValuePair<DateTime, double?>>> GetIOPSRatioData(IEnumerable<PerformanceStatisticsModel> statisticsModels, int sampleSizeInMinutes)
    {
        Dictionary<string, List<KeyValuePair<DateTime, double?>>> groupedCategory = new Dictionary<string, List<KeyValuePair<DateTime, double?>>>();
        List<KeyValuePair<DateTime, double?>> totalCategoryList = new List<KeyValuePair<DateTime, double?>>();

        totalCategoryList.AddRange(
            from PerformanceStatisticsModel model in statisticsModels
            let datetime = WebHelper.ToSampleDateTime(model.Timestamp, sampleSizeInMinutes)
            select new KeyValuePair<DateTime, double?>(datetime, model.IOPSRatio));
        groupedCategory.Add(totalCategoryKey, totalCategoryList);
        return groupedCategory;
    }


    /// <summary>
    /// Get Disk Busy performance data.
    /// </summary>
    /// <param name="statisticsModels">Collection of PerformanceStatisticsModels.</param>
    /// <param name="sampleSizeInMinutes">Sample Size In Minutes.</param>
    /// <returns>Dictionary of performance data.</returns>
    private Dictionary<string, List<KeyValuePair<DateTime, double?>>> GetDiskBusyData(IEnumerable<PerformanceStatisticsModel> statisticsModels, int sampleSizeInMinutes)
    {
        Dictionary<string, List<KeyValuePair<DateTime, double?>>> groupedCategory = new Dictionary<string, List<KeyValuePair<DateTime, double?>>>();
        List<KeyValuePair<DateTime, double?>> totalCategoryList = new List<KeyValuePair<DateTime, double?>>();

        totalCategoryList.AddRange(
            from PerformanceStatisticsModel model in statisticsModels
            select WebHelper.ToSampleDateTime(model.Timestamp, sampleSizeInMinutes)
                into datetime
                select new KeyValuePair<DateTime, double?>(datetime, 0));
        groupedCategory.Add(totalCategoryKey, totalCategoryList);
        return groupedCategory;
    }


    /// <summary>
    /// Get Cache Hit Ratio performance data.
    /// </summary>
    /// <param name="statisticsModels">Collection of PerformanceStatisticsModels.</param>
    /// <param name="sampleSizeInMinutes">Sample Size In Minutes.</param>
    /// <returns>Dictionary of performance data.</returns>
    private Dictionary<string, List<KeyValuePair<DateTime, double?>>> GetCacheHitRatioData(IEnumerable<PerformanceStatisticsModel> statisticsModels, int sampleSizeInMinutes)
    {
        Dictionary<string, List<KeyValuePair<DateTime, double?>>> groupedCategory = new Dictionary<string, List<KeyValuePair<DateTime, double?>>>();
        List<KeyValuePair<DateTime, double?>> totalCategoryList = new List<KeyValuePair<DateTime, double?>>();
        List<KeyValuePair<DateTime, double?>> readCategoryList = new List<KeyValuePair<DateTime, double?>>();
        List<KeyValuePair<DateTime, double?>> writeCategoryList = new List<KeyValuePair<DateTime, double?>>();

        foreach (PerformanceStatisticsModel model in statisticsModels)
        {
            DateTime datetime = WebHelper.ToSampleDateTime(model.Timestamp, sampleSizeInMinutes);
            totalCategoryList.Add(new KeyValuePair<DateTime, double?>(datetime, model.CacheHitRatio));
            readCategoryList.Add(new KeyValuePair<DateTime, double?>(datetime, model.RatioRead));
            writeCategoryList.Add(new KeyValuePair<DateTime, double?>(datetime, model.RatioWrite));
        }

        groupedCategory.Add(totalCategoryKey, totalCategoryList);
        groupedCategory.Add(readCategoryKey, readCategoryList);
        groupedCategory.Add(writeCategoryKey, writeCategoryList);
        return groupedCategory;
    }


    /// <summary>
    /// Get Utilization performance data.
    /// </summary>
    /// <param name="statisticsModels">Collection of PerformanceStatisticsModels.</param>
    /// <param name="sampleSizeInMinutes">Sample Size In Minutes.</param>
    /// <returns>Dictionary of performance data.</returns>
    private Dictionary<string, List<KeyValuePair<DateTime, double?>>> GetUtilizationData(IEnumerable<PerformanceStatisticsModel> statisticsModels, int sampleSizeInMinutes)
    {
        Dictionary<string, List<KeyValuePair<DateTime, double?>>> groupedCategory = new Dictionary<string, List<KeyValuePair<DateTime, double?>>>();
        List<KeyValuePair<DateTime, double?>> totalCategoryList = new List<KeyValuePair<DateTime, double?>>();

        foreach (PerformanceStatisticsModel model in statisticsModels)
        {
            DateTime datetime = WebHelper.ToSampleDateTime(model.Timestamp, sampleSizeInMinutes);
            totalCategoryList.Add(new KeyValuePair<DateTime, double?>(datetime, model.Utilization));
        }
        groupedCategory.Add(totalCategoryKey, totalCategoryList);
        return groupedCategory;
    }


    /// <summary>
    /// Create DataSeriesWithThreshold series.
    /// </summary>
    /// <param name="section">Section name (IOPS, Latency, etc.).</param>
    /// <param name="subsection">Subsection name (Total, Read, Write, Other).</param>
    /// <returns>DataSeriesWithThreshold Model.</returns>
    private DataSeriesWithThreshold CreateSeriesWithThreshold(string section, string subsection)
    {
        DataSeriesWithThreshold serie = new DataSeriesWithThreshold
            {
                IsCustomDataSerie = false,
                ShowTrendLine = false,
                ShowPercentileLine = false,
                Data = new List<V2DataPoint>(),
                CustomProperties = null,
                TagName = subsection,
                Label = drilldownChartsI18n[InvariantString.Format("{0}_{1}", section, subsection)]
            };
        return serie;
    }
}