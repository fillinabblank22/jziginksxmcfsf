﻿<%@ WebService Language="C#" Class="PerformanceSummaryService" %>

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Models.Thresholds;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.Resources;
using SolarWinds.SRM.Web.Models;
using System.Reflection;
using SolarWinds.SRM.Web;
using Threshold = SolarWinds.SRM.Web.Models.Threshold;
using V2DataSeries = SolarWinds.Orion.Web.Charting.v2.DataSeries;
using V2DataPoint = SolarWinds.Orion.Web.Charting.v2.DataPoint;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class PerformanceSummaryService : ChartDataWebService
{
    private static readonly Log log = new Log();
    private static readonly String criticalBackgroundColor = "#FFD3D6";
    private static readonly String criticalLineColor = "#F7A2AD";
    private static readonly String warningBackgroundColor = "#FFEBD6";
    private static readonly String warningLineColor = "#FFDFB5";
    private static readonly String lineColor = "#1879AD";

    private static readonly int yAxisHeight = 30;
    private static readonly int yAxisMargin = 20;
    private static readonly int chartTopMargin = 7;
    private static readonly int chartSubtitleHeight = 27;

    [WebMethod(EnableSession = true)]
    public ChartDataResults GetData(ChartDataRequest request)
    {
        bool isLoad = Convert.ToBoolean(request.AllSettings["isLoad"]);
        string selectedItem = (string)request.AllSettings["item"];
        bool state = Convert.ToBoolean(request.AllSettings["state"]);
        int resourceId = Convert.ToInt32(request.AllSettings["resourceId"]);
        string dataBindMethod = (string)request.AllSettings["dataBindMethod"];
        int sampleSizeInMinutes = Convert.ToInt32(request.AllSettings["sampleSizeInMinutes"]);
        int daysOfDataToLoad = Convert.ToInt32(request.AllSettings["daysOfDataToLoad"]);
        bool showSubtitle = false; // Will be TRUE if we want to preserve some height for the title
                                   // Initial space reservation is done in the UI form (PerformanceSummary.ascx.cs through ChartTopPosition property)

        HashSet<string> blocksVisible = new HashSet<string>(((IEnumerable)request.AllSettings["blocksVisible"]).Cast<object>()
                                 .Select(x => x.ToString()).ToArray());


        if (SrmSessionManager.GetPerformanceSummaryExpanderState(resourceId) == null)
        {
            SrmSessionManager.SetPerformanceSummaryExpanderState(resourceId, new List<string>());
        }
        if (!isLoad)
        {
            SrmSessionManager.GetPerformanceSummaryExpanderState(resourceId).Remove(selectedItem);
            if (state)
            {
                SrmSessionManager.GetPerformanceSummaryExpanderState(resourceId).Add(selectedItem);
            }
        }
        List<string> selectedItems = SrmSessionManager.GetPerformanceSummaryExpanderState(resourceId);

        int id = int.Parse(request.NetObjectIds[0].Split(':')[1]);
        string prefix = request.NetObjectIds[0].Split(':')[0];

        // On Share view we show Performance Summary of parent volume, so have to use volume id rather than share id
        if (prefix.Equals(FileShares.NetObjectPrefix, StringComparison.InvariantCultureIgnoreCase))
        {
            id = ServiceLocator.GetInstance<IFileSharesDAL>().GetFileSharesDetails(id).VolumeID;
        }

        MethodInfo dataSource = typeof(PerformanceSummary).GetMethod(dataBindMethod, BindingFlags.Instance | BindingFlags.Public);
        PerformanceSummaryStructure allData = (PerformanceSummaryStructure)dataSource.Invoke(new PerformanceSummary(), new object[] { id, sampleSizeInMinutes, daysOfDataToLoad, blocksVisible });
        var data = new List<DataSeriesWithThreshold>();

        if (allData.IsEmpty())
        {
            return new ChartDataResults(data);
        }

        foreach (var item in allData.Enumerate(selectedItems))
        {
            data.Add(new DataSeriesWithThreshold()
            {
                TagName = item.TagName,
                Label = item.Name,
                Data = item.Data,
                Threshold = item.Threshold
            });
        }

        dynamic options = new JsonObject();
        JsonObject seriesTemplates = new JsonObject();

        options.yAxis = JsonObject.CreateJsonArray(data.Count);
        options.seriesTemplates = JsonObject.CreateJsonArray(data.Count);

        for (int i = 0; i < data.Count; i++)
        {
            seriesTemplates.Dictionary[data[i].TagName] = new { yAxis = i, color = lineColor, marker = new { symbol = "circle" } };

            options.yAxis[i].labels.enabled = false;
            options.yAxis[i].gridLineWidth = 1;
            options.yAxis[i].lineWidth = 0;
            options.yAxis[i].startOnTick = false;
            options.yAxis[i].endOnTick = false;
            options.yAxis[i].tickInterval = data[i].Data.Count == 0 ? 1 : Convert.ToInt64(data[i].Data.Max(d => d.IsNullPoint ? 0 : d[1]) / 4);
            options.yAxis[i].minPadding = 0;
            options.yAxis[i].maxPadding = 0;
            options.yAxis[i].top = chartTopMargin + (showSubtitle ? chartSubtitleHeight : 0) + 26 + yAxisMargin / 2 + i * (yAxisHeight + yAxisMargin); //chart margin + rangeSelector + margin/2 + i * (div height + margin)
            options.yAxis[i].offset = 0;
            options.yAxis[i].height = yAxisHeight;

            AddThresholdBands(data[i], options.yAxis[i]);
        }
        options.chart.height = options.yAxis[data.Count - 1].top + options.yAxis[data.Count - 1].height + 80;
        options.seriesTemplates = seriesTemplates;

        return new ChartDataResults(data) { ChartOptionsOverride = options };
    }

    private void AddThresholdBands(DataSeriesWithThreshold serie, dynamic yAxis)
    {
        if (serie.Threshold == null)
        {
            return;
        }

        var isGreater = (serie.Threshold.ThresholdOperator == ThresholdOperatorEnum.Greater) || (serie.Threshold.ThresholdOperator == ThresholdOperatorEnum.GreaterOrEqual);
        var isLess = (serie.Threshold.ThresholdOperator == ThresholdOperatorEnum.Less) || (serie.Threshold.ThresholdOperator == ThresholdOperatorEnum.LessOrEqual);

        int thresholdCount = new[] { serie.Threshold.WarnLevel, serie.Threshold.CriticalLevel }.Count(i => i.HasValue);
        if (thresholdCount != 0)
        {
            if (serie.HasData && serie.Data.Any(p => p.Count > 1))
            {
                yAxis.plotBands = JsonObject.CreateJsonArray(thresholdCount);
                yAxis.plotLines = JsonObject.CreateJsonArray(thresholdCount * 2);

                if (isGreater)
                {
                    int bandIndex = 0;
                    int lineIndex = 0;
                    double defValue = serie.Data.Max(i => i.IsNullPoint ? 0 : i[1]);
                    //double defValue = 0;
                    if (serie.Threshold.WarnLevel.HasValue)
                    {
                        SetThreshold(true,
                                     serie.Threshold.WarnLevel.Value,
                                     serie.Threshold.CriticalLevel.HasValue ? serie.Threshold.CriticalLevel.Value : defValue,
                                     yAxis,
                                     ref bandIndex,
                                     ref lineIndex);
                    }

                    if (serie.Threshold.CriticalLevel.HasValue)
                    {
                        SetThreshold(false,
                                     serie.Threshold.CriticalLevel.Value,
                                     defValue,
                                     yAxis,
                                     ref bandIndex,
                                     ref lineIndex);
                    }
                }
                else if (isLess)
                {
                    int bandIndex = 0;
                    int lineIndex = 0;
                    double defValue = serie.Data.Min(i => i.IsNullPoint ? 0 : i[1]);
                    //double defValue = 0;
                    if (serie.Threshold.WarnLevel.HasValue)
                    {
                        SetThreshold(true,
                                     serie.Threshold.CriticalLevel.HasValue ? serie.Threshold.CriticalLevel.Value : defValue,
                                     serie.Threshold.WarnLevel.Value,
                                     yAxis,
                                     ref bandIndex,
                                     ref lineIndex);
                    }

                    if (serie.Threshold.CriticalLevel.HasValue)
                    {
                        SetThreshold(false,
                                     defValue,
                                     serie.Threshold.CriticalLevel.Value,
                                     yAxis,
                                     ref bandIndex,
                                     ref lineIndex);
                    }
                }
            }
        }
    }

    private void SetThreshold(bool isWarning, double from, double to, dynamic yAxis, ref int bandIndex, ref int lineIndex)
    {
        yAxis.plotBands[bandIndex].from = from;
        yAxis.plotBands[bandIndex].to = to;
        yAxis.plotBands[bandIndex].color = isWarning ? warningBackgroundColor : criticalBackgroundColor;
        yAxis.plotBands[bandIndex].zIndex = 1;

        yAxis.plotLines[lineIndex].value = yAxis.plotBands[bandIndex].from;
        yAxis.plotLines[lineIndex].color = isWarning ? warningLineColor : criticalLineColor;
        yAxis.plotLines[lineIndex].width = 1;
        yAxis.plotLines[lineIndex].zIndex = 2;
        lineIndex++;
        yAxis.plotLines[lineIndex].value = yAxis.plotBands[bandIndex].to;
        yAxis.plotLines[lineIndex].color = isWarning ? warningLineColor : criticalLineColor;
        yAxis.plotLines[lineIndex].width = 1;
        yAxis.plotLines[lineIndex].zIndex = 2;
        lineIndex++;

        bandIndex++;
    }

    private class DataSeriesWithThreshold : DataSeries
    {
        public Threshold Threshold { get; set; }
    }
}
