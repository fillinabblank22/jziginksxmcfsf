﻿<%@ WebService Language="C#" Class="OrionVolumeService" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI.MobileControls;
using SolarWinds.Logging;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.DAL;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.Resources;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.None)]
[ScriptService]
public class OrionVolumeService : WebService
{
    private static readonly Log log = new Log();

    [WebMethod]
    public IEnumerable<OrionVolume> GetOrionVolumeInfo(List<string> ids)
    {
        return ServiceLocator.GetInstance<ITopologyDAL>().GetOrionVolumeBySRMNetObject(ids.Distinct().ToList());
    }
}
