﻿<%@ WebService Language="C#" Class="EnrollmentWizardService" %>

using System;
using System.Text;
using System.Linq;
using System.Data;
using System.Globalization;
using System.Collections.Generic;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using Ninject.Activation;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.Credentials;
using SolarWinds.SRM.Common.Discovery;
using SolarWinds.SRM.Common.Helpers;
using SolarWinds.SRM.Common.UnifiedResult;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Web.EnrollmentWizard;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Resources;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class EnrollmentWizardService : System.Web.Services.WebService
{
    private static readonly Log log = new Log();

    [WebMethod(EnableSession = true)]
    public string ImportDiscovery()
    {
        if (!WizardWorkflowHelper.ValidateSession())
            return null;

        String resultTemplate = "{{\"errorMessage\":\"{0}\",\"url\":\"{1}\"}}";
        String importMessage = string.Empty;

        try
        {
            Configuration configuration = WizardWorkflowHelper.ConfigurationInfo;
            log.DebugFormat(CultureInfo.InvariantCulture, "Discovery: Importing...\r\nPoller ID: {0}\r\nProvider: {1}\r\nArray: {2}",
                            configuration.PollingConfig.EngineID,
                            String.Join("\r\nProvider: ", configuration.ProviderConfiguration.SelectedProviders.Select(p => InvariantString.Format("IP Address: {0}, Credential Name: {1}", p.IPAddress, p.CredentialName))),
                            String.Join("\r\nArray: ", configuration.ProviderConfiguration.DiscoveryProvider.Arrays.Select(a => InvariantString.Format("Local ID: {0}, ID: {1}, Name: {2}, Serial Number: {3}, Selected: {4}", a.StorageArrayId ?? -1, a.ID, a.Name, a.SerialNumber, a.IsSelected))));

            CheckCredentialsAvailability(configuration);

            using (var proxy = BusinessLayerFactory.Create())
            {
                proxy.DiscoveryImport(configuration.PollingConfig.EngineID,
                    configuration.ProviderConfiguration.SelectedProviders.Select(p => new ProviderInfo(p)
                    {
                        DeviceGroupID = configuration.DeviceGroup.GroupId,
                        ProviderLocation = configuration.DeviceGroup.ProviderLocationOrOnboard
                    }).ToList(),
                                      configuration.ProviderConfiguration.DiscoveryProvider.Arrays);
            }

            log.Debug("Discovery: Results are imported");

            String returnUrl = WizardWorkflowHelper.ReturnPageUrl;
            if (String.IsNullOrEmpty(returnUrl))
            {
                returnUrl = "/Orion/SRM/Summary.aspx";
            }

            WizardWorkflowHelper.Reset();
            importMessage = String.Format(CultureInfo.InvariantCulture, resultTemplate, String.Empty, returnUrl);
            return importMessage;
        }
        catch (Exception ex)
        {
            importMessage = String.Format(CultureInfo.InvariantCulture, resultTemplate, ex.Message, String.Empty);
            return importMessage;
        }
        finally
        {
            using (var proxy = BusinessLayerFactory.CreateMain())
            {
                proxy.LogOIP("OIP EW logging: SummaryStep, " + importMessage);
            }
        }
    }

    private void CheckCredentialsAvailability(Configuration configuration)
    {
        var credentialPropertiesDAL = ServiceLocator.GetInstance<ICredentialPropertiesDAL>();
        foreach (EnrollmentWizardProvider provider in configuration.ProviderConfiguration.SelectedProviders)
        {
            if (!credentialPropertiesDAL.CredentialExists(provider.CredentialID))
                throw new Exception(String.Format("Credential '{0}' is not available. It was probably deleted.", provider.CredentialName));
        }
    }

    [WebMethod(EnableSession = true)]
    public CallResult StartDiscoveryJob(Guid groupId)
    {
        var discoveryId = Guid.Empty;

        Configuration configInfo = WizardWorkflowHelper.ConfigurationInfo;
        using (var proxy = BusinessLayerFactory.Create(configInfo.PollingConfig.ServerName, configInfo.PollingConfig.BusinessLayerPort, true))
        {
            var providers = configInfo.ProviderConfiguration.SelectedProviders.Select(p => new ConnectionInfo(p)).ToList();

            IDictionary<Guid, DeviceGroup> groups = ServiceLocator.GetInstance<IGroupDAL>().GetDeviceGroups();
            if (groups[groupId].FlowType == FlowType.SmisVnx)
            {
                var unifiedGroupId = groups.Values.First(g => g.FlowType == FlowType.SmisEmcUnified).GroupId;
                discoveryId = proxy.SubmitDiscoveryJob(providers, new[] { groupId, unifiedGroupId });
            }
            else
            {
                discoveryId = proxy.SubmitDiscoveryJob(providers, new[] { groupId });
            }

            configInfo.ProviderConfiguration.DiscoveryId = discoveryId;
        }
        var isSuccess = discoveryId != Guid.Empty;
        return new CallResult()
        {
            IsDone = true,
            IsSuccess = isSuccess,
            ErrorMessage = isSuccess ? string.Empty : SrmWebContent.Provider_Step_Unexpected_Error
        };
    }

    [WebMethod(EnableSession = true)]
    public TestCredentialsResult StartDiscoveryMulti(Guid[] providerGuids, Guid groupId, int engineId)
    {
        bool isDone = false;
        var connInfos = new List<ConnectionInfo>();
        Guid discoveryId = Guid.Empty;
        string errorMessage = null;

        try
        {
            var engine = ServiceLocator.GetInstance<IEngineDAL>().GetEngine(engineId);
            using (var proxy = BusinessLayerFactory.Create(engine.ServerName, engine.BusinessLayerPort, true))
            {
                foreach (var guid in providerGuids)
                {
                    var provider = SrmSessionManager.GetProvider(guid);
                    var connInfo = new ConnectionInfo(provider);
                    connInfos.Add(connInfo);

                    //if the provider is got from database, credential object can be empty, so load it here
                    if (provider.Credential == null)
                    {
                        provider.Credential = proxy.GetCredential(provider.CredentialID, provider.CredentialType);
                    }
                }

                IDictionary<Guid, DeviceGroup> groups = ServiceLocator.GetInstance<IGroupDAL>().GetDeviceGroups();
                if (groups[groupId].FlowType == FlowType.SmisVnx)
                {
                    var unifiedGroupId = groups.Values.First(g => g.FlowType == FlowType.SmisEmcUnified).GroupId;
                    discoveryId = proxy.SubmitDiscoveryJob(connInfos, new[] { groupId, unifiedGroupId });
                }
                else
                {
                    discoveryId = proxy.SubmitDiscoveryJob(connInfos, new[] { groupId });
                }
            }
        }
        catch (Exception exception)
        {
            log.Error("Exception occured during discovery.", exception);
            errorMessage = exception.Message;
        }

        return new TestCredentialsResult(TestCredentialsHelper.GetSeeLogsMessage(), discoveryId != Guid.Empty, errorMessage) { ProviderGuid = discoveryId };
    }

    [WebMethod(EnableSession = true)]
    public object GetDiscoveryJobResult()
    {
        if (!WizardWorkflowHelper.ValidateSession())
            return null;

        log.Debug("Discovery: Getting Discovery Job Result");

        Configuration configuration = WizardWorkflowHelper.ConfigurationInfo;
        EnrollmentWizardProvider provider = configuration.ProviderConfiguration.DiscoveryProvider;
        Guid discoveryId = configuration.ProviderConfiguration.DiscoveryId;
        if (discoveryId.Equals(Guid.Empty))
        {
            var builder = new StringBuilder();
            provider.Arrays.Where(a => a.IsSelected && !a.IsAlreadyManaged).ToList().ForEach(array => builder.Append("{").Append(array.UniqueArrayId).Append("}"));
            return new
            {
                isDone = true,
                errorMessage = string.Empty,
                selectedArray = builder.ToString()
            };
        }

        try
        {
            using (var proxy = BusinessLayerFactory.Create(configuration.PollingConfig.ServerName, configuration.PollingConfig.BusinessLayerPort, true))
            {
                DiscoveryResults result = proxy.GetDiscoveryJobResult(discoveryId);

                if (result == null)
                {
                    return new
                    {
                        isDone = false,
                        errorMessage = string.Empty,
                        selectedArray = string.Empty
                    };
                }

                if (result.StorageArrays.Any())
                {
                    log.DebugFormat(CultureInfo.InvariantCulture, "Discovery: Got Discovery Job result:\r\nArray: {0}",
                        String.Join("\r\nArray: ", result.StorageArrays.Select(a => InvariantString.Format("ID: {0}, Name: {1}, Serial Number: {2}", a.ID, a.Name, a.SerialNumber))));

                    configuration.ProviderConfiguration.DiscoveryId = Guid.Empty;
                    provider.Arrays.Clear();

                    var selectedArrayInDatabse = new List<SelectedArrayEntity>();
                    if (provider.ID.HasValue)
                    {
                        foreach (var array in ServiceLocator.GetInstance<IStorageArrayDAL>().GetAllArrays(provider.ID.Value))
                        {
                            var selectedArray = new SelectedArrayEntity(array)
                            {
                                IsSelected = true,
                                StorageArrayId = array.StorageArrayId,
                                IsAlreadyManaged = true
                            };

                            selectedArrayInDatabse.Add(selectedArray);
                            provider.Arrays.Add(selectedArray);
                        }
                    }

                    var alreadyExistingIDList = new HashSet<string>(ServiceLocator.GetInstance<IStorageArrayDAL>().GetAllArraysByIDs(result.StorageArrays.Select(p => p.ID).ToList()).Select(s => s.ID));

                    foreach (var discoveredArray in result.StorageArrays)
                    {
                        ArrayEntity array = selectedArrayInDatabse.FirstOrDefault(a => a.Equals(discoveredArray));

                        if (array == null)
                        {
                            //if discovered array is not in database, add it to the list with not selected flag and null StorageArrayId
                            provider.Arrays.Add(new SelectedArrayEntity(discoveredArray)
                            {
                                IsAlreadyManaged = alreadyExistingIDList.Contains(discoveredArray.ID)
                            });
                        }
                    }

                    log.DebugFormat(CultureInfo.InvariantCulture, "Discovery: Merged Arrays:\r\nArray: {0}",
                        String.Join("\r\nArray: ", provider.Arrays.Select(a => InvariantString.Format("Local ID: {0}, ID: {1}, Name: {2}, Serial Number: {3}, Selected: {4}", a.StorageArrayId ?? -1, a.ID, a.Name, a.SerialNumber, a.IsSelected))));

                    var builder = new StringBuilder();
                    provider.Arrays.Where(a => a.IsSelected && !a.IsAlreadyManaged).ToList().ForEach(array => builder.Append("{").Append(array.UniqueArrayId).Append("}"));

                    return new
                    {
                        isDone = true,
                        errorMessage = string.Empty,
                        selectedArray = builder.ToString()
                    };
                }
                else
                {
                    configuration.ProviderConfiguration.DiscoveryId = Guid.Empty;

                    return new
                    {
                        isDone = true,
                        errorMessage = ConstructErrorMessageWithLink(configuration),
                        selectedArray = String.Empty,
                        discoveryResult = result.Results
                    };
                }
            }
        }
        catch (Exception ex)
        {
            log.Error("Exception occured during discovery.", ex);
            configuration.ProviderConfiguration.DiscoveryId = Guid.Empty;

            return new
            {
                isDone = true,
                errorMessage = ConstructErrorMessageWithLink(configuration),
                selectedArray = String.Empty,
                discoveryResult = new Dictionary<string, string>() { { ex.GetType().Name, ex.Message } }
            };
        }
    }

    private string ConstructErrorMessageWithLink(Configuration configuration)
    {
        string link = String.Format(CultureInfo.InvariantCulture, "<a href='http://www.solarwinds.com/documentation/kbloader.aspx?kb=6015' target='_blank' class='helpLink'>SRM - No compatible arrays found</a>");
        if (configuration.DeviceGroup != null && configuration.DeviceGroup.ProviderLocationOrOnboard == ProviderLocation.External)
        {
            return String.Format(CultureInfo.InvariantCulture, SrmWebContent.SRM_Discovery_NoStorageFound, link);
        }
        else
        {
            return String.Format(CultureInfo.InvariantCulture, SrmWebContent.SRM_Discovery_NoStorageFound_OnBoard, link);
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetAllDiscoveryJobResultsById(Guid discoveryGuid, int engineId)
    {
        // stared by Edit properties validation
        String resultTemplate = "{{\"isDone\":{0},\"errorMessage\":\"{1}\",\"selectedArray\":{2}}}";

        try
        {
            IList<Tuple<string, string, SRMDiscoveryJobResultCode>> jobResults;
            List<SelectedArrayEntity> resultArrays = GetAllDiscoveryArraysById(discoveryGuid, engineId, out jobResults);

            bool providerBusyError = jobResults.Any(tuple => tuple.Item3 == SRMDiscoveryJobResultCode.ProviderBusy); ;

            // store jobresult & storagearrays for later when we will be importing
            SrmSessionManager.SetDiscoveryResult(discoveryGuid, new KeyValuePair<SRMDiscoveryJobResultCode, List<SelectedArrayEntity>>(
                providerBusyError ? SRMDiscoveryJobResultCode.ProviderBusy : SRMDiscoveryJobResultCode.NotAppllicable,
                resultArrays
            ));

            if (resultArrays == null)
            {
                return String.Format(CultureInfo.InvariantCulture, resultTemplate, "false", providerBusyError ? SrmWebContent.Provider_Step_ProviderBusy : SrmWebContent.EditProperties_DeviceValidationErrorText, "\"\"");
            }

            var arraySns = "[" + string.Join(",", resultArrays.Select(a => "\"" + a.SerialNumber + "\"").ToArray()) + "]";

            return String.Format(CultureInfo.InvariantCulture, resultTemplate, "true", String.Empty, arraySns);
        }
        catch (Exception ex)
        {
            log.Error("Exception occured during discovery.", ex);
            return String.Format(CultureInfo.InvariantCulture, resultTemplate
                                , "true"
                                , ex.Message
                                , "null");
        }
    }

    private List<SelectedArrayEntity> GetAllDiscoveryArraysById(Guid discoveryGuid, int engineId, out IList<Tuple<String, String, SRMDiscoveryJobResultCode>> jobResults)
    {
        jobResults = new List<Tuple<string, string, SRMDiscoveryJobResultCode>>();

        var engine = ServiceLocator.GetInstance<IEngineDAL>().GetEngine(engineId);
        using (var proxy = BusinessLayerFactory.Create(engine.ServerName, engine.BusinessLayerPort, true))
        {
            DiscoveryResults results = proxy.GetDiscoveryJobResult(discoveryGuid);
            if (results == null || results.StorageArrays == null)
            {
                return null;
            }

            jobResults = results.Results;

            var resultArrays = new List<SelectedArrayEntity>();

            var selectedArrayInDatabase = new List<SelectedArrayEntity>();
            var arraysBySN = ServiceLocator.GetInstance<IStorageArrayDAL>().GetAllArraysBySN(results.StorageArrays.Select(a => a.SerialNumber).ToList());

            foreach (var array in arraysBySN)
            {
                var selectedArray = new SelectedArrayEntity(array)
                {
                    IsSelected = true,
                    StorageArrayId = array.StorageArrayId
                };

                selectedArrayInDatabase.Add(selectedArray);
                resultArrays.Add(selectedArray);
            }

            foreach (var discoveredArray in results.StorageArrays)
            {
                ArrayEntity array = selectedArrayInDatabase.FirstOrDefault(a => a.Equals(discoveredArray));
                if (array == null)
                {
                    //if discovered array is not in database, add it to the list with not selected flag and null StorageArrayId
                    resultArrays.Add(new SelectedArrayEntity(discoveredArray));
                }
            }

            return resultArrays;
        }
    }

    [WebMethod(EnableSession = true)]
    public CallResult CheckArrayIsInDiscoveryJobResults(string arrayID, string arraySerialNumber)
    {
        try
        {
            Configuration configInfo = WizardWorkflowHelper.ConfigurationInfo;
            IList<Tuple<String, String, SRMDiscoveryJobResultCode>> jobResults;
            List<SelectedArrayEntity> resultingArrays = GetAllDiscoveryArraysById(configInfo.ProviderConfiguration.DiscoveryId, configInfo.PollingConfig.EngineID, out jobResults);

            var isDone = resultingArrays != null;

            var array = ArrayExistsInDiscoveryJobResult(resultingArrays, arrayID, arraySerialNumber);

            var errorMessage = !isDone || array != null
                                   ? string.Empty
                                   : SrmWebContent.EditProperties_DeviceValidationErrorText;

            return new CallResult(isDone, errorMessage, array == null ? null : array.TemplateId.ToString());
        }

        catch (Exception ex)
        {
            log.Error("Exception occured during discovery.", ex);

            Configuration configuration = WizardWorkflowHelper.ConfigurationInfo;

            configuration.ProviderConfiguration.DiscoveryId = Guid.Empty;

            return new CallResult(true, ConstructErrorMessageWithLink(configuration));
        }
    }

    private SelectedArrayEntity ArrayExistsInDiscoveryJobResult(List<SelectedArrayEntity> resultingArrays, string arrayID, string arraySerialNumber)
    {
        if (resultingArrays == null)
        {
            return null;
        }

        if ((new[] { FlowType.SmisVnx, FlowType.SmisEmcUnified }).Contains(WizardWorkflowHelper.ConfigurationInfo.DeviceGroup.FlowType))
        {
            //For Celerra device compare by SerialNumber
            return resultingArrays.FirstOrDefault(
                a => a.SerialNumber.Equals(arraySerialNumber, StringComparison.InvariantCultureIgnoreCase)
                ||  a.SerialNumber.StartsWith("File: " + arraySerialNumber, StringComparison.InvariantCultureIgnoreCase));
        }

        //For all other devices compare by ID
        var array = resultingArrays.FirstOrDefault(a => a.ID.Equals(arrayID, StringComparison.InvariantCultureIgnoreCase));
        return array;
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetArrays()
    {
        if (!WizardWorkflowHelper.ValidateSession())
            return null;

        try
        {
            int pageSize;
            int startRowNumber;

            string sortColumn = this.Context.Request.QueryString["sort"];
            string sortDirection = this.Context.Request.QueryString["dir"];

            Int32.TryParse(this.Context.Request.QueryString["start"], out startRowNumber);
            Int32.TryParse(this.Context.Request.QueryString["limit"], out pageSize);

            if (pageSize < 10)
            {
                pageSize = 10;
            }

            DataTable table = new DataTable();
            table.Columns.Add("UniqueArrayId", typeof(Guid));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Type", typeof(string));
            table.Columns.Add("Disks", typeof(string));
            table.Columns.Add("Provider", typeof(string));
            table.Columns.Add("AlreadyManaged", typeof(bool));
            table.Columns.Add("IsLicensed", typeof(bool));


            Configuration configuration = WizardWorkflowHelper.ConfigurationInfo;
            List<SelectedArrayEntity> arrays = configuration.ProviderConfiguration.DiscoveryProvider.Arrays.ToList();
            string providerName = configuration.ProviderConfiguration.DiscoveryProvider.FriendlyName;
            if (arrays.Count > 0)
            {
                HashSet<String> licensedObjects;
                using (var proxy = BusinessLayerFactory.Create())
                {
                    licensedObjects = new HashSet<String>(proxy.GetLicensedObjects());
                }

                List<SelectedArrayEntity> sortedArrays = SortArrays(arrays, sortColumn, sortDirection);
                for (int i = startRowNumber; i < startRowNumber + pageSize && i < sortedArrays.Count; i++)
                {
                    var r = table.NewRow();
                    r["UniqueArrayId"] = sortedArrays[i].UniqueArrayId;
                    r["Name"] = sortedArrays[i].Name + (!sortedArrays[i].IsAlreadyManaged ? String.Empty : " (Already managed) ");
                    r["Type"] = configuration.DeviceGroup.Name;
                    r["Disks"] = sortedArrays[i].Disks;
                    r["Provider"] = providerName;
                    r["AlreadyManaged"] = sortedArrays[i].IsAlreadyManaged;
                    r["IsLicensed"] = licensedObjects.Contains(sortedArrays[i].ID);
                    table.Rows.Add(r);
                }
            }

            return new PageableDataTable(table, arrays.Count);
        }
        catch (Exception ex)
        {
            log.Error("Exception occured when getting arrays", ex);
            throw;
        }
    }


    private List<SelectedArrayEntity> SortArrays(IEnumerable<SelectedArrayEntity> arraySource, string sortColumn, string direction)
    {
        bool asc = direction == "ASC";

        if (sortColumn == "Name")
        {
            if (asc)
            {
                return arraySource.OrderBy((c) => c.Name).ToList();
            }
            return arraySource.OrderByDescending((c) => c.Name).ToList();
        }
        if (sortColumn == "Disks")
        {
            if (asc)
            {
                return arraySource.OrderBy((c) => c.Disks).ToList();
            }
            return arraySource.OrderByDescending((c) => c.Disks).ToList();
        }
        return arraySource.ToList();
    }

    [WebMethod]
    public Guid GetNewGuid()
    {
        return Guid.NewGuid();
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetProviders()
    {
        if (!WizardWorkflowHelper.ValidateSession())
            return null;

        try
        {
            int pageSize;
            int startRowNumber;

            string sortColumn = this.Context.Request.QueryString["sort"];
            string sortDirection = this.Context.Request.QueryString["dir"];

            Int32.TryParse(this.Context.Request.QueryString["start"], out startRowNumber);
            Int32.TryParse(this.Context.Request.QueryString["limit"], out pageSize);

            if (pageSize < 10)
            {
                pageSize = 10;
            }

            DataTable table = new DataTable();
            table.Columns.Add("UniqueProviderIdentifier", typeof(string));
            table.Columns.Add("FriendlyName", typeof(string));
            table.Columns.Add("CredentialName", typeof(string));

            Configuration configuration = WizardWorkflowHelper.ConfigurationInfo;
            ProviderHelper.InitAvailableProviders();

            var providerEntities = new List<EnrollmentWizardProvider>();
            providerEntities.AddRange(configuration.ProviderConfiguration.SelectedProviders);
            providerEntities.AddRange(configuration.ProviderConfiguration.AvailableProviderEntities);

            providerEntities = providerEntities.Distinct()
                .Where(p => p.ProviderLocation == ProviderLocation.External)
                .ToList();

            configuration.ProviderConfiguration.AvailableProviderEntities = providerEntities;

            if (providerEntities.Count > 0)
            {
                List<DataRow> rows =
                    SortProviders(providerEntities, sortColumn, sortDirection)
                        .GroupBy(x => x.ProviderGroupID)
                        .SelectMany(x =>
                        {
                            if (x.Key == Guid.Empty)
                            {
                                return x.Select(p =>
                                {
                                    var row = table.NewRow();
                                    row["UniqueProviderIdentifier"] = p.UniqueProviderIdentifier;
                                    row["FriendlyName"] = InvariantString.Format("{0}_{1}", (int)p.Status, p.FriendlyName);
                                    row["CredentialName"] = p.Credential.Name;
                                    return row;
                                }).ToList();
                            }
                            else
                            {
                                var firstProvider = x.FirstOrDefault();
                                var row = table.NewRow();
                                row["UniqueProviderIdentifier"] = string.Join(";", x.Select(p => p.UniqueProviderIdentifier));
                                row["FriendlyName"] = InvariantString.Format("{0}_{1}", (int)firstProvider.Status, firstProvider.FriendlyName);
                                row["CredentialName"] = string.Join(", ", x.Select(p => p.Credential.Name));
                                return new List<DataRow>() { row };
                            }
                        })
                        .ToList();

                for (int i = startRowNumber; i < startRowNumber + pageSize && i < rows.Count; i++)
                {
                    table.Rows.Add(rows[i]);
                }
            }

            return new PageableDataTable(table, providerEntities.Count);
        }
        catch (Exception ex)
        {
            log.Error("Exception occured when getting arrays", ex);
            throw;
        }
    }

    [WebMethod]
    public object GetStoredCredentials(CredentialType type)
    {
        var result = new List<KeyValuePair<String, String>>();

        using (var proxy = BusinessLayerFactory.Create())
        {
            IDictionary<int, string> credentialsList = proxy.GetCredentialNames(type);
            foreach (KeyValuePair<int, string> cred in credentialsList)
            {
                result.Add(new KeyValuePair<String, String>(cred.Key.ToString(), cred.Value));
            }
        }

        return new
        {
            Default = HttpUtility.HtmlEncode(Resources.SrmWebContent.Provider_Step_New_Credential),
            Credentials = result
        };
    }

    private Guid SaveSmisProviderInternal(string ipAddress,
        string displayName,
        string userName,
        string password,
        bool useHttp,
        int httpPort,
        int httpsPort,
        string interopNamespace,
        string arrayNamespace,
        int credentialID,
        Guid? providerGroupId)
    {
        if (!WizardWorkflowHelper.ValidateSession())
            return new Guid();

        var credential = GetOrCreateCredential(
            displayName,
            userName,
            password,
            useHttp,
            httpPort,
            httpsPort,
            interopNamespace,
            arrayNamespace,
            credentialID);

        ProviderTypeEnum? providerType = (WizardWorkflowHelper.ConfigurationInfo?.DeviceGroup)?.ProviderTypeOrBlock;
        EnrollmentWizardProvider provider = CreateSmisProvider(ipAddress, credential, providerType ?? ProviderTypeEnum.Block, null, providerGroupId);
        WizardWorkflowHelper.ConfigurationInfo.ProviderConfiguration.SelectedProviders.Add(provider);
        return provider.UniqueProviderIdentifier;
    }

    [WebMethod(EnableSession = true)]
    public Guid SaveSmisProviderWithGuid(string ipAddress,
        string displayName,
        string userName,
        string password,
        bool useHttp,
        int httpPort,
        int httpsPort,
        string interopNamespace,
        string arrayNamespace,
        int credentialID,
        Guid providerGroupId)
    {
        return this.SaveSmisProviderInternal(ipAddress,
            displayName,
            userName,
            password,
            useHttp,
            httpPort,
            httpsPort,
            interopNamespace,
            arrayNamespace,
            credentialID,
            providerGroupId);
    }

    [WebMethod(EnableSession = true)]
    public Guid SaveSmisProvider(string ipAddress,
        string displayName,
        string userName,
        string password,
        bool useHttp,
        int httpPort,
        int httpsPort,
        string interopNamespace,
        string arrayNamespace,
        int credentialID)
    {
        return this.SaveSmisProviderInternal(ipAddress,
            displayName,
            userName,
            password,
            useHttp,
            httpPort,
            httpsPort,
            interopNamespace,
            arrayNamespace,
            credentialID,
            null);
    }

    [WebMethod(EnableSession = true)]
    public Guid SaveGenericHttpProvider(string ipAddress,
        string credentialName,
        string userName,
        string password,
        bool useHttp,
        int port,
        int credentialID,
        Guid providerGroupId)
    {
        if (!WizardWorkflowHelper.ValidateSession())
            return new Guid();

        var credential = GetOrCreateCredential(new GenericHttpCredential()
        {
            ID = credentialID,
            Name = credentialName,
            Username = userName,
            Password = password,
            Port = port,
            UseSsl = !useHttp
        });

        ProviderTypeEnum? providerType = (WizardWorkflowHelper.ConfigurationInfo?.DeviceGroup)?.ProviderTypeOrBlock;
        ProviderLocation? providerLocation = (WizardWorkflowHelper.ConfigurationInfo?.DeviceGroup)?.ProviderLocationOrOnboard;
        var providerEntity = new EnrollmentWizardProvider
        {
            ProviderType = providerType ?? ProviderTypeEnum.Unified,
            ProviderLocation = providerLocation ?? ProviderLocation.External,
            ProviderGroupID = providerGroupId,
            Credential = credential,
            CredentialType = CredentialType.GenericHttp,
            CredentialID = credential.ID.Value,
            IPAddress = ipAddress.Trim()
        };
        
        WizardWorkflowHelper.ConfigurationInfo.ProviderConfiguration.SelectedProviders.Add(providerEntity);
        return providerEntity.UniqueProviderIdentifier;
    }


    /// <summary>
    /// Invoked from Edit Properties
    /// </summary>
    [WebMethod(EnableSession = true)]
    public Guid GetSmisProviderEdit(string ipAddress,
                                 string displayName,
                                 string userName,
                                 string password,
                                 bool useHttp,
                                 int httpPort,
                                 int httpsPort,
                                 string interopNamespace,
                                 string arrayNamespace,
                                 int credentialID,
                                 int arrayId)
    {
        var credential = GetOrCreateCredential(
            displayName,
            userName,
            password,
            useHttp,
            httpPort,
            httpsPort,
            interopNamespace,
            arrayNamespace,
            credentialID);

        EnrollmentWizardProvider provider = CreateSmisProvider(ipAddress, credential, ProviderTypeEnum.Block, arrayId, null);

        SrmSessionManager.SetProvider(provider.UniqueProviderIdentifier, provider);
        return provider.UniqueProviderIdentifier;
    }

    private static Credential GetOrCreateCredential(
        string displayName,
        string userName,
        string password,
        bool useHttp,
        int httpPort,
        int httpsPort,
        string interopNamespace,
        string arrayNamespace,
        int credentialId)
    {
        Credential credential;

        if (credentialId <= 0)
        {
            credential = new SmisCredentials()
            {
                Name = displayName,
                Username = userName,
                Password = password,
                HttpPort = (UInt32)httpPort,
                HttpsPort = (UInt32)httpsPort,
                UseSSL = !useHttp,
                InteropNamespace = interopNamespace,
                Namespace = arrayNamespace
            };
            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(credential, CredentialType.SMIS);
                credential.ID = id;
            }
        }
        else
        {
            using (var proxy = BusinessLayerFactory.Create())
            {
                credential = proxy.GetCredential(credentialId, CredentialType.SMIS);
            }
        }

        return credential;
    }

    private static EnrollmentWizardProvider CreateSmisProvider(string ipAddress, 
        Credential credential, 
        ProviderTypeEnum providerType, 
        int? arrayId,
        Guid? providerGroupId)
    {
        if (arrayId.HasValue)
        {
            var arrayProviders = ServiceLocator.GetInstance<IStorageProviderDAL>().GetAllProvidersByArrayId(arrayId.Value);
            var searchedProviders = arrayProviders.Where(p => p.IPAddress == ipAddress
                                                        && p.CredentialType == CredentialType.SMIS
                                                        && p.ProviderType == providerType
                                                        && p.CredentialID == credential.ID.Value);

            if (searchedProviders.Any())
            {
                var provider = searchedProviders.First();
                return new EnrollmentWizardProvider(new ProviderEntityWithCredName(provider))
                {
                    Credential = credential,
                    CredentialName = credential.Name,
                    ProviderType = provider.ProviderType,
                    ProviderGroupID = providerGroupId ?? Guid.Empty
                };
            }
        }

        return new EnrollmentWizardProvider()
        {
            IPAddress = ipAddress,
            CredentialType = CredentialType.SMIS,
            ProviderType = providerType,
            Credential = credential,
            CredentialID = credential.ID.Value,
            CredentialName = credential.Name,
            ProviderGroupID = providerGroupId ?? Guid.Empty
        };
    }

    /// <summary>
    /// Invoked from Edit Properties
    /// </summary>
    [WebMethod(EnableSession = true)]
    public Guid GetVnxProviderEdit(string credentialName, int credentialID,
        string userName,
        string password,
        int httpsPort,
        string ip,
        string managementSerivcesPath,
        int arrayId)
    {
        var providerEntity = GetVnxProvider(credentialName, credentialID, userName, password, httpsPort, ip, managementSerivcesPath, arrayId, ProviderTypeEnum.File);
        SrmSessionManager.SetProvider(providerEntity.UniqueProviderIdentifier, providerEntity);
        return providerEntity.UniqueProviderIdentifier;
    }

    private static EnrollmentWizardProvider GetVnxProvider(string credentialName, int credentialID,
                                     string userName,
                                     string password,
                                     int httpsPort,
                                     string ip,
                                     string managementSerivcesPath,
                                     int? arrayId,
                                     ProviderTypeEnum providerType)
    {
        Credential credential;

        if (credentialID <= 0)
        {
            // todo filling for others
            credential = new VnxCredential()
            {
                Name = credentialName,
                Username = userName,
                Password = password,
                Port = httpsPort,
                PathToManagementServices = managementSerivcesPath
            };
            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(credential, CredentialType.VNX);
                credential.ID = id;
            }
        }
        else
        {
            using (var proxy = BusinessLayerFactory.Create())
            {
                credential = proxy.GetCredential(credentialID, CredentialType.VNX);
            }
        }

        if (arrayId.HasValue)
        {
            var providers = ServiceLocator.GetInstance<IStorageProviderDAL>().GetAllProvidersByArrayId(arrayId.Value);
            var providerEntities = providers.Where(p => p.IPAddress == ip && p.CredentialType == CredentialType.VNX &&
                                                p.ProviderType == providerType && p.CredentialID == credential.ID.Value);
            if (providerEntities.Any())
            {
                var firstProvider = providerEntities.First();
                var ewp = new EnrollmentWizardProvider(new ProviderEntityWithCredName(firstProvider));
                ewp.Credential = credential;
                ewp.CredentialName = credential.Name;
                ewp.ProviderType = firstProvider.ProviderType;
                return ewp;
            }
        }
        var providerEntity = new EnrollmentWizardProvider()
        {
            IPAddress = ip,
            CredentialType = CredentialType.VNX,
            ProviderType = providerType,
            Credential = credential,
            CredentialID = credential.ID.Value,
            CredentialName = credential.Name
        };
        return providerEntity;
    }

    private List<EnrollmentWizardProvider> SortProviders(IEnumerable<EnrollmentWizardProvider> providerSource, string sortColumn, string direction)
    {
        bool asc = direction == "ASC";

        if (sortColumn == "FriendlyName")
        {
            if (asc)
            {
                return providerSource.OrderBy((c) => c.FriendlyName).ToList();
            }
            return providerSource.OrderByDescending((c) => c.FriendlyName).ToList();
        }
        if (sortColumn == "MonitoredArrayCount")
        {
            if (asc)
            {
                return providerSource.OrderBy((c) => c.MonitoredArrayCount).ToList();
            }
            return providerSource.OrderByDescending((c) => c.MonitoredArrayCount).ToList();
        }
        if (sortColumn == "CredentialName")
        {
            if (asc)
            {
                return providerSource.OrderBy((c) => c.CredentialName).ToList();
            }
            return providerSource.OrderByDescending((c) => c.CredentialName).ToList();
        }
        return providerSource.ToList();
    }

    [WebMethod(EnableSession = true)]
    public TestCredentialsResult TestNetAppFilerCredentials(string credentialId, string credentialName, string useSsl, string password, string username, string ip, bool saveProvider, Guid groupId)
    {
        if (!WizardWorkflowHelper.ValidateSession())
            return null;

        var groupDal = ServiceLocator.GetInstance<IDeviceGroupDAL>();
        var flowType = groupDal.GetDeviceGroupDetails(groupId).FlowType;

        if (flowType == FlowType.EmcUnity)
        {
            return TestCredentialsHelper.TestEmcUnityCredentials(credentialId, credentialName, useSsl, password, username, ip, saveProvider);
        }
        if (flowType == FlowType.InfiniBox)
        {
            return TestCredentialsHelper.TestInfiniBoxCredentials(credentialId, credentialName, useSsl, password,
                username, ip, saveProvider);
        }
        else
        {
            return TestCredentialsHelper.TestNetAppFilerCredentials(credentialId, credentialName, useSsl, password, username, ip, saveProvider);
        }
    }

    [WebMethod(EnableSession = true)]
    public TestCredentialsResult TestXtremIoCredentials(string credentialId, string credentialName, bool useHttp, string password, string username, int httpPort, int httpsPort, string ip)
    {
        if (!WizardWorkflowHelper.ValidateSession())
            return null;

        return
            TestCredentialsHelper.TestXtremIoCredentials(credentialId, credentialName, useHttp, password, username, httpPort, httpsPort, ip);
    }

    [WebMethod(EnableSession = true)]
    public TestCredentialsResult TestSnmpV1Credentials(string credentialID, string credentialName, string communityString, string ip, string port, bool saveProvider, Guid groupId)
    {
        if (!WizardWorkflowHelper.ValidateSession())
            return null;

        var providerType = ProviderTypeEnum.Block;
        var groupDal = ServiceLocator.GetInstance<IDeviceGroupDAL>();
        var flowType = groupDal.GetDeviceGroupDetails(groupId).FlowType;

        if (flowType == FlowType.SnmpRestIsilon)
        {
            providerType = ProviderTypeEnum.File;
        }

        return TestCredentialsHelper.TestSnmpV1Credentials(credentialID, credentialName, communityString, ip, port, saveProvider, providerType);
    }

    [WebMethod(EnableSession = true)]
    public TestCredentialsResult TestSnmpV3Credentials(string credentialID, string credentialName,
        string userName, string context, string authenticationValue, string privacyValue,
        string authenticationPassword, string authenticationKeyIsPassword,
        string privacyPassword, string privacyKeyIsPassword, string ip, string port, bool saveProvider, Guid groupId)
    {
        if (!WizardWorkflowHelper.ValidateSession())
            return null;

        var providerType = ProviderTypeEnum.Block;
        var groupDal = ServiceLocator.GetInstance<IDeviceGroupDAL>();
        var flowType = groupDal.GetDeviceGroupDetails(groupId).FlowType;

        if (flowType == FlowType.SnmpRestIsilon)
        {
            providerType = ProviderTypeEnum.File;
        }

        return TestCredentialsHelper.TestSnmpV3Credentials(credentialID, credentialName,
            userName, context, authenticationValue, privacyValue,
            authenticationPassword, authenticationKeyIsPassword,
            privacyPassword, privacyKeyIsPassword, ip, port, saveProvider, providerType);
    }

    [WebMethod(EnableSession = true)]
    public TestCredentialsResult TestSmisCredentials(int credentialID,
                                      string credentialName,
                                      string userName,
                                      string password,
                                      bool useHttp,
                                      int httpPort,
                                      int httpsPort,
                                      string interopNamespace,
                                      string arrayNamespace,
                                      string ip)
    {
        return TestSmisCredentialsExtended(credentialID,
                                    credentialName,
                                    userName,
                                    password,
                                    useHttp,
                                    httpPort,
                                    httpsPort,
                                    interopNamespace,
                                    arrayNamespace,
                                    ip,
                                    ProviderTypeEnum.Block,
                                    false);
    }

    [WebMethod(EnableSession = true)]
    public TestCredentialsResult TestSmisCredentialsProvider(int credentialID,
                                      string credentialName,
                                      string userName,
                                      string password,
                                      bool useHttp,
                                      int httpPort,
                                      int httpsPort,
                                      string interopNamespace,
                                      string arrayNamespace,
                                      string ip,
                                      string engineid,
                                      int arrayId)
    {
        this.FillConfigurationSessionVariableWithEngineInfo(engineid);

        var testResult = TestCredentialsHelper.TestSmisCredentials(credentialID,
                                                   credentialName,
                                                   userName,
                                                   password,
                                                   useHttp,
                                                   httpPort,
                                                   httpsPort,
                                                   interopNamespace,
                                                   arrayNamespace,
                                                   ip,
                                                   ProviderTypeEnum.Block);

        var ewpGuid = GetSmisProviderEdit(ip, credentialName, userName, password, useHttp, httpPort, httpsPort, interopNamespace, arrayNamespace, credentialID, arrayId);
        testResult.ProviderGuid = ewpGuid;

        return testResult;
    }

    [WebMethod(EnableSession = true)]
    public TestCredentialsResult TestNetAppDfmCredentialsProvider(string credentialId,
                                                                    string credentialName,
                                                                    bool useSsl,
                                                                    int httpPort,
                                                                    int httpsPort,
                                                                    string password,
                                                                    string username,
                                                                    string ip,
                                                                    string engineid,
                                                                    bool saveProvider
                                                                    )
    {
        this.FillConfigurationSessionVariableWithEngineInfo(engineid);

        var testResult = TestCredentialsHelper.TestNetAppDfmCredentials(credentialId,
                                                   credentialName,
                                                   useSsl,
                                                   httpPort,
                                                   httpsPort,
                                                   password,
                                                   username,
                                                   ip,
                                                   saveProvider);
        return testResult;
    }

    [WebMethod(EnableSession = true)]
    public TestCredentialsResult TestXtremIoCredentialsProvider(string credentialId,
                                                                string credentialName,
                                                                bool useHttp,
                                                                int httpPort,
                                                                int httpsPort,
                                                                string password,
                                                                string username,
                                                                string ip,
                                                                string engineid,
                                                                bool saveProvider)
    {
        this.FillConfigurationSessionVariableWithEngineInfo(engineid);

        var testResult = TestCredentialsHelper.TestXtremIoCredentials(credentialId,
                                                   credentialName,
                                                   useHttp,
                                                   password,
                                                   username,
                                                   httpPort,
                                                   httpsPort,
                                                   ip,
                                                   saveProvider);
        return testResult;
    }

    [WebMethod(EnableSession = true)]
    public TestCredentialsResult TestSmisCredentialsExtended(int credentialID,
                                      string credentialName,
                                      string userName,
                                      string password,
                                      bool useHttp,
                                      int httpPort,
                                      int httpsPort,
                                      string interopNamespace,
                                      string arrayNamespace,
                                      string ip,
                                      ProviderTypeEnum providerType,
                                      bool isEdit)
    {
        if (!WizardWorkflowHelper.ValidateSession())
            return null;

        return TestCredentialsHelper.TestSmisCredentials(credentialID,
                                                   credentialName,
                                                   userName,
                                                   password,
                                                   useHttp,
                                                   httpPort,
                                                   httpsPort,
                                                   interopNamespace,
                                                   arrayNamespace,
                                                   ip,
                                                   providerType,
                                                   ProviderLocation.External,
                                                   isEdit);
    }

    [WebMethod(EnableSession = true)]
    public TestCredentialsResult TestSmisCredentialsExtendedDevice(int credentialID,
                                      string credentialName,
                                      string userName,
                                      string password,
                                      bool useHttp,
                                      int httpPort,
                                      int httpsPort,
                                      string interopNamespace,
                                      string arrayNamespace,
                                      string ip,
                                      Guid groupId,
                                      bool isEdit)
    {
        if (!WizardWorkflowHelper.ValidateSession())
            return null;

        var groupDal = ServiceLocator.GetInstance<IDeviceGroupDAL>();
        var flowType = groupDal.GetDeviceGroupDetails(groupId).FlowType;
        ProviderTypeEnum smisCredentialsProviderType = ProviderTypeEnum.Block;
        ProviderLocation providerLocation = ProviderLocation.External;

        if (flowType == FlowType.SmisVnx)
        {
            smisCredentialsProviderType = ProviderTypeEnum.File;
            // VNX SMI-S File is Onboard provider
            providerLocation = ProviderLocation.Onboard;
        }

        return TestCredentialsHelper.TestSmisCredentials(credentialID,
                                                   credentialName,
                                                   userName,
                                                   password,
                                                   useHttp,
                                                   httpPort,
                                                   httpsPort,
                                                   interopNamespace,
                                                   arrayNamespace,
                                                   ip,
                                                   smisCredentialsProviderType,
                                                   providerLocation,
                                                   isEdit);
    }

    [WebMethod(EnableSession = true)]
    public TestCredentialsResult TestVnxCredentialsExtended(int credentialID,
                                     string credentialName,
                                     string userName,
                                     string password,
                                     int httpsPort,
                                     int httpPort,
                                     string ip,
                                     string managementServicesPath,
                                    bool apiProtocolIsHttps,
                                    bool saveProviderWhenSuccessful,
                                    Guid groupId)
    {
        if (!WizardWorkflowHelper.ValidateSession())
            return null;

        var groupDal = ServiceLocator.GetInstance<IDeviceGroupDAL>();
        var flowType = groupDal.GetDeviceGroupDetails(groupId).FlowType;

        if (flowType == FlowType.SmisMsa)
        {
            return TestCredentialsHelper.TestMsaCredentials(credentialID,
                                                  credentialName,
                                                  userName,
                                                  password,
                                                  httpsPort,
                                                  httpPort,
                                                  ip,
                                                  apiProtocolIsHttps,
                                                  saveProviderWhenSuccessful);
        }
        else if (flowType == FlowType.SmisPureStorage)
        {
            return TestCredentialsHelper.TestPureStorageCredentials(credentialID,
                                                  credentialName,
                                                  userName,
                                                  password,
                                                  httpsPort,
                                                  httpPort,
                                                  ip,
                                                  apiProtocolIsHttps,
                                                  saveProviderWhenSuccessful);
        }

        return TestCredentialsHelper.TestVnxCredentials(credentialID,
                                                  credentialName,
                                                  userName,
                                                  password,
                                                  httpsPort,
                                                  ip,
                                                  managementServicesPath,
                                                  saveProviderWhenSuccessful);
    }

    private void FillConfigurationSessionVariableWithEngineInfo(string engineid)
    {
        var engines = ServiceLocator.GetInstance<IEngineDAL>().GetEngines();
        var eid = int.Parse(engineid);
        var engine = engines.First(e => e.EngineID == eid);

        Configuration configuration = WizardWorkflowHelper.ConfigurationInfo;
        configuration.PollingConfig.ServerName = engine.ServerName;
        configuration.PollingConfig.BusinessLayerPort = engine.BusinessLayerPort;
    }

    [WebMethod(EnableSession = true)]
    public TestCredentialsResult TestVnxCredentialsProvider(int credentialID,
                                     string credentialName,
                                     string userName,
                                     string password,
                                     int httpsPort,
                                     string ip,
                                     string managementServicesPath,
                                     string engineid,
                                     int arrayId)
    {
        this.FillConfigurationSessionVariableWithEngineInfo(engineid);

        var testResult = TestCredentialsHelper.TestVnxCredentials(credentialID,
                                                  credentialName,
                                                  userName,
                                                  password,
                                                  httpsPort,
                                                  ip,
                                                  managementServicesPath);

        var ewpGuid = GetVnxProviderEdit(credentialName, credentialID, userName, password, httpsPort, ip, managementServicesPath, arrayId);
        testResult.ProviderGuid = ewpGuid;

        return testResult;
    }

    private Credential GetOrCreateCredential<T>(T credential) where T : UsernamePasswordCredential
    {
        var credentialType = typeof(T).GetCustomAttribute(typeof(CredentialTypeAttribute)) as CredentialTypeAttribute;
        if (credentialType == null)
        {
            log.Error($"Cannot read credential type of credential {nameof(T)}. {credential}");
            throw new SolarWinds.SRM.Common.Exceptions.SRMException($"Cannot read credential type of credential {nameof(T)}");
        }

        if (credential.ID <= 0)
        {
            using (var proxy = BusinessLayerFactory.Create())
            {
                credential.ID = null;
                int id = proxy.AddCredential(credential, credentialType.CredentialType);
                credential.ID = id;
                return credential;
            }
        }
        else
        {
            using (var proxy = BusinessLayerFactory.Create())
            {
                return proxy.GetCredential(credential.ID.Value, credentialType.CredentialType);
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public Guid SaveDfmProvider(string ipAddress,
                                 string displayName,
                                 string userName,
                                 string password,
                                 bool useHttp,
                                 int httpPort,
                                 int httpsPort,
                                 int credentialID)
    {
        if (!WizardWorkflowHelper.ValidateSession())
            return new Guid();

        Credential credential;

        if (credentialID <= 0)
        {
            credential = new NetAppDfmCredentials()
            {
                Name = displayName,
                Username = userName,
                Password = password,
                UseSsl = !useHttp,
                HttpPort = httpPort,
                HttpsPort = httpsPort
            };
            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(credential, CredentialType.NetAppDfm);
                credential.ID = id;
            }
        }
        else
        {
            using (var proxy = BusinessLayerFactory.Create())
            {
                credential = proxy.GetCredential(credentialID, CredentialType.NetAppDfm);
            }
        }

        var config = WizardWorkflowHelper.ConfigurationInfo;
        bool isCluster;
        if (!bool.TryParse(config.DeviceGroup.Properties["IsCluster"], out isCluster))
        {
            isCluster = false;
        }

        var providerEntity = new EnrollmentWizardProvider()
        {
            IPAddress = ipAddress,
            CredentialType = CredentialType.NetAppDfm,
            ProviderType = ProviderTypeEnum.Unified,
            Credential = credential,
            CredentialID = credential.ID.Value,
            CredentialName = credential.Name,
            PropertyBag = new Dictionary<string, object>() { { "ArrayIsCluster", isCluster } }
        };

        ProviderHelper.SaveProvider(providerEntity);
        return providerEntity.UniqueProviderIdentifier;
    }

    [WebMethod(EnableSession = true)]
    public Guid SaveXtremIoProvider(string ipAddress,
                                    string displayName,
                                    string userName,
                                    string password,
                                    bool useHttp,
                                    int httpPort,
                                    int httpsPort,
                                    int credentialID)
    {
        if (!WizardWorkflowHelper.ValidateSession())
            return new Guid();

        Credential credential;

        if (credentialID <= 0)
        {
            credential = new XtremIoHttpCredential()
            {
                Name = displayName,
                Username = userName,
                Password = password,
                UseSsl = !useHttp,
                HttpPort = httpPort,
                HttpsPort = httpsPort
            };
            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(credential, CredentialType.XtremIoHttp);
                credential.ID = id;
            }
        }
        else
        {
            using (var proxy = BusinessLayerFactory.Create())
            {
                credential = proxy.GetCredential(credentialID, CredentialType.XtremIoHttp);
            }
        }

        var providerEntity = new EnrollmentWizardProvider()
        {
            IPAddress = ipAddress,
            CredentialType = CredentialType.XtremIoHttp,
            ProviderType = ProviderTypeEnum.Block,
            Credential = credential,
            CredentialID = credential.ID.Value,
            CredentialName = credential.Name,
        };

        ProviderHelper.SaveProvider(providerEntity);
        return providerEntity.UniqueProviderIdentifier;
    }

    [WebMethod(EnableSession = true)]
    public TestCredentialsResult TestNetAppDfmCredentials(string credentialID,
                                      string credentialName,
                                      string userName,
                                      string password,
                                      bool useHttp,
                                      int httpPort,
                                      int httpsPort,
                                      string ip)
    {
        if (!WizardWorkflowHelper.ValidateSession())
            return null;

        return TestCredentialsHelper.TestNetAppDfmCredentials(credentialID, credentialName, !useHttp, httpPort, httpsPort, password, userName, ip);
    }

    [WebMethod(EnableSession = true)]
    public TestCredentialsResult TestIsilonHttpCredentials(string credentialID,
                                      string credentialName,
                                      string userName,
                                      string password,
                                      bool useHttp,
                                      int port,
                                      string ip,
                                      bool saveProvider)
    {
        if (!WizardWorkflowHelper.ValidateSession())
            return null;

        return TestCredentialsHelper.TestIsilonHttpCredentials(credentialID, credentialName, port, useHttp, password, userName, ip, saveProvider);
    }

    [WebMethod(EnableSession = true)]
    public TestCredentialsResult TestGenericHttpCredentials(
                                      string deviceGroupID,
                                      string credentialID,
                                      string credentialName,
                                      string userName,
                                      string password,
                                      bool useHttp,
                                      int port,
                                      string ip,
                                      bool saveProvider)
    {
        if (!WizardWorkflowHelper.ValidateSession())
            return null;

        return  TestCredentialsHelper.TestGenericHttpCredentials(deviceGroupID, credentialID, credentialName, userName, password, useHttp, port, ip, saveProvider);
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetSnapshots(string ipAddress, string port, string userName, string password, string templateId)
    {
        if (!WizardWorkflowHelper.ValidateSession())
            return null;

        try
        {
            IList<ArraySnapshot> arraySnapshots = null;
            using (var proxy = BusinessLayerFactory.Create())
            {
                arraySnapshots = proxy.DiscoveryResponder(ipAddress, port, userName, password, new Guid(templateId));
            }

            DataTable table = new DataTable();
            if (arraySnapshots != null)
            {
                table.Columns.Add("UniqueSnapshotIdentifier", typeof(string));
                table.Columns.Add("ArrayName", typeof(string));
                table.Columns.Add("Vendor", typeof(string));
                table.Columns.Add("ArrayID", typeof(string));
                table.Columns.Add("IPAddress", typeof(string));
                table.Columns.Add("Created", typeof(string));
                table.Columns.Add("PollInterval", typeof(string));
                table.Columns.Add("SnaphotID", typeof(string));
                table.Columns.Add("IsCluster", typeof(string));

                foreach (var arrSnapshot in arraySnapshots)
                {
                    string snapshotID = arrSnapshot.SnaphotID.ToString(CultureInfo.InvariantCulture);
                    var r = table.NewRow();
                    r["UniqueSnapshotIdentifier"] = snapshotID;
                    r["ArrayName"] = arrSnapshot.ArrayName;
                    r["Vendor"] = arrSnapshot.ArrayVendor;
                    r["ArrayID"] = arrSnapshot.ArrayDeviceID;
                    r["IPAddress"] = arrSnapshot.IPAddress;
                    r["Created"] = arrSnapshot.Created.ToString(CultureInfo.InvariantCulture);
                    r["PollInterval"] = arrSnapshot.PollInterval.ToString(CultureInfo.InvariantCulture);
                    r["SnaphotID"] = snapshotID;
                    r["IsCluster"] = arrSnapshot.ArrayIsCluster;
                    table.Rows.Add(r);
                }
            }

            return new PageableDataTable(table, table.Rows.Count);
        }
        catch (Exception ex)
        {
            log.Error("Exception occured when getting arrays", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public bool StoreResponderStorageArray(string ipAddress, string port, int snapshotId, string storageArrayId, string arrayName, string vendor, bool isCluster, string templateId, string userName, string password)
    {
        using (var proxy = BusinessLayerFactory.Create())
        {
            try
            {
                proxy.StoreResponderArray(ipAddress, port, snapshotId, storageArrayId, arrayName, vendor, isCluster, new Guid(templateId), userName, password);
                return true;
            }
            catch (Exception ex)
            {
                log.Error("Storing of Responder Storage Array was not successful.", ex);
                return false;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public void SetPollingEngineToSession(int engineId)
    {
        var engine = ServiceLocator.GetInstance<IEngineDAL>().GetEngine(engineId);

        Configuration configuration = WizardWorkflowHelper.ConfigurationInfo;
        configuration.PollingConfig.BusinessLayerPort = engine.BusinessLayerPort;
        configuration.PollingConfig.EngineID = engine.EngineID;
        configuration.PollingConfig.ServerName = engine.ServerName;
    }
}
