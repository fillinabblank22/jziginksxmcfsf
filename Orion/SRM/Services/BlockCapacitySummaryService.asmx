﻿<%@ WebService Language="C#" Class="BlockCapacitySummaryService" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.SRM.Web.Resources;
using System.Reflection;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Charting;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class BlockCapacitySummaryService : ChartDataWebService
{
    private const string DisplayNone = "none";
    private static readonly Log log = new Log();
    private static readonly string transparentColor = "rgba(255, 255, 255, 0)";

    private static double GetPercent(double value, double total)
    {
        return total == 0 ? 0 : Math.Min(100, Math.Max(0, value) * 100.0 / total);
    }

    /// <summary>
    /// Without limitation to 100%, used with oversubscribed capacity
    /// </summary>
    /// <param name="value"></param>
    /// <param name="total"></param>
    /// <returns></returns>
    private static double GetPercentWithNoLimit(double value, double total)
    {
        return total == 0 ? 0 : Math.Max(0, value) * 100.0 / total;
    }

    [WebMethod(EnableSession = true)]
    public SrmChartDataResults GetData(ChartDataRequest request)
    {
        try
        {
            Dictionary<string, object> initData = (Dictionary<string, object>)request.AllSettings["initData"];
            var legends = initData["LegendMapping"] as Dictionary<string, object>;


            int resourceWidth = Convert.ToInt32(initData["ResourceWidth"]);
            int resourceId = Convert.ToInt32(initData["ResourceId"]);
            bool getMoreData = Convert.ToBoolean(request.AllSettings["getMoreData"]);
            int pageSize = Convert.ToInt32(initData["PageSize"]);
            bool isExpanded = Convert.ToBoolean(initData["Expanded"]);
            string dataSourceMethod = (string)initData["DataSourceMethod"];

            string resourceNetObjectId;
            var resourceInfo = ResourceManager.GetResourceByID(resourceId);
            if (resourceInfo.IsCustomObjectResource())
            {
                resourceNetObjectId = resourceInfo.Properties["NetObjectID"];
            }
            else
            {
                resourceNetObjectId = request.NetObjectIds[0];
            }

            //increase loaded count
            int? loadedCount = SrmSessionManager.GetBlockCapacitySummaryLoadedCount(resourceId);
            if (loadedCount == null)
            {
                loadedCount = pageSize;
            }
            int loaded = Math.Max(pageSize, loadedCount.Value);
            if (getMoreData)
            {
                loaded += pageSize;
            }
            SrmSessionManager.SetBlockCapacitySummaryLoadedCount(resourceId, loaded);

            // update expander state
            SrmSessionManager.SetBlockCapacitySummaryExpanderState(resourceId, isExpanded);

            var serieOrange = new SrmDataSeries { Label = Resources.SrmWebContent.BlockCapacitySummary_ArrayOverhead, TagName = "columnOrange", NetObjectId = resourceNetObjectId };
            var seriePurple = new SrmDataSeries { Label = Resources.SrmWebContent.BlockCapacitySummary_Spare, TagName = "columnPurple", NetObjectId = resourceNetObjectId };
            var serieDarkBlue = new SrmDataSeries { Label = Resources.SrmWebContent.BlockCapacitySummary_Provisioned, TagName = "columnDarkBlue", NetObjectId = resourceNetObjectId };
            var serieGrey = new SrmDataSeries { Label = Resources.SrmWebContent.BlockCapacitySummary_Remaining, TagName = "columnGrey", NetObjectId = resourceNetObjectId };
            var seriePink = new SrmDataSeries { Label = Resources.SrmWebContent.BlockCapacitySummary_OverSubscribed, TagName = "columnPink", NetObjectId = resourceNetObjectId };
            var serieLightBlue = new SrmDataSeries { Label = Resources.SrmWebContent.BlockCapacitySummary_Consumed, TagName = "columnLightBlue", NetObjectId = resourceNetObjectId };
            var serieTransparent = new SrmDataSeries { TagName = "columnTransparent", NetObjectId = resourceNetObjectId };
            var metadataSerie = seriePink;

            bool hasData = false;
            Action<BlockCapacitySummary.SummaryCapacityData> summaryCallback = (summary) =>
            {
                hasData = Math.Max(0, summary.Orange) +
                          Math.Max(0, summary.Purple) +
                          Math.Max(0, summary.DarkBlue) +
                          Math.Max(0, summary.Grey) +
                          Math.Max(0, summary.Pink) +
                          Math.Max(0, summary.LightBlue) != 0;

                serieOrange.AddPoint(new SrmDataPoint(Math.Max(0, summary.Orange))
                {
                    Percent = GetPercent(summary.Orange, summary.Total),
                    Display = legends.ContainsKey(serieOrange.TagName) ? string.Empty : DisplayNone
                });
                seriePurple.AddPoint(new SrmDataPoint(Math.Max(0, summary.Purple))
                {
                    Percent = GetPercent(summary.Purple, summary.Total),
                    Display = legends.ContainsKey(seriePurple.TagName) ? string.Empty : DisplayNone
                });
                serieDarkBlue.AddPoint(new SrmDataPoint(Math.Max(0, summary.DarkBlue - summary.LightBlue))
                {
                    Percent = GetPercent(summary.DarkBlue, summary.Total),
                    Display = legends.ContainsKey(serieDarkBlue.TagName) ? string.Empty : DisplayNone
                });
                serieLightBlue.AddPoint(new SrmDataPoint(Math.Max(0, Math.Min(summary.LightBlue, summary.DarkBlue)))
                {
                    Percent = GetPercent(Math.Min(summary.LightBlue, summary.DarkBlue), summary.Total),
                    Display = legends.ContainsKey(serieLightBlue.TagName) ? string.Empty : DisplayNone
                });
                serieGrey.AddPoint(new SrmDataPoint(Math.Max(0, summary.Grey))
                {
                    Percent = GetPercent(summary.Grey, summary.Total),
                    Display = legends.ContainsKey(serieGrey.TagName) ? string.Empty : DisplayNone
                });
                seriePink.AddPoint(new SrmDataPoint(Math.Max(0, summary.Pink))
                {
                    Percent = GetPercentWithNoLimit(summary.Pink, summary.Total),
                    Display = legends.ContainsKey(seriePink.TagName) ? string.Empty : DisplayNone
                });
                serieTransparent.AddPoint(new SrmDataPoint(0, transparentColor) { Display = DisplayNone });
            };

            bool hasDetails = false;
            Action<int, IEnumerable<BlockCapacitySummary.DetailCapacityData>> detailsCallback = (totalCount, details) =>
            {
                hasDetails = totalCount != 0;

                if (isExpanded)
                {
                    metadataSerie.Data[0].Add("dataLeft", totalCount - loaded);
                    double offset = 0;
                    foreach (var detail in details.OrderByDescending(o => o.DarkBlue).Take(loaded))
                    {
                        serieTransparent.AddPoint(new SrmDataPoint(offset, transparentColor) { Display = DisplayNone });
                        serieOrange.AddPoint(new SrmDataPoint((double)serieOrange.Data[0]["y"], transparentColor) { Display = DisplayNone });
                        serieDarkBlue.AddPoint(new SrmDataPoint(Math.Max(0, detail.DarkBlue)) { Percent = GetPercent(detail.DarkBlue, detail.Total) });
                        serieGrey.AddPoint(new SrmDataPoint(Math.Max(0, detail.Grey)) { Percent = GetPercent(detail.Grey, detail.Total) });
                        seriePink.AddPoint(new SrmDataPoint(detail.Name, Math.Max(0, detail.Pink)) { Display = DisplayNone });
                        serieLightBlue.AddPoint(new SrmDataPoint(0, transparentColor) { Display = DisplayNone });
                        offset += Convert.ToDouble(Math.Max(0, detail.DarkBlue));
                    }
                }
            };

            Int32 objectId;
            if (!String.IsNullOrEmpty(resourceNetObjectId))
            {
                objectId = Int32.Parse(resourceNetObjectId.Split(':')[1]);
            }
            else throw new ArgumentNullException("netObjectId");

            MethodInfo dataSource = typeof(BlockCapacitySummary).GetMethod(dataSourceMethod, BindingFlags.Instance | BindingFlags.Public);
            dataSource.Invoke(new BlockCapacitySummary(), new object[] { objectId, summaryCallback, detailsCallback });

            dynamic customProperties = new JsonObject();
            customProperties.hasDetails = hasDetails;
            metadataSerie.CustomProperties = customProperties;

            dynamic options = new JsonObject();
            options.chart.height = metadataSerie.Data.Count * 25;
            options.chart.width = resourceWidth - (hasDetails ? 105 : 16);
            options.chart.marginTop = -1 * (2 + (seriePink.Data.Count / 4.05));
            options.plotOptions.series.stacking = hasDetails ? "normal" : "percent";

            if (hasData)
            {
                // do not add, remove or change order of series w/o updating Charts.SRM.BlockCapacitySummary.js
                return new SrmChartDataResults(seriePink,
                                               serieGrey,
                                               serieDarkBlue,
                                               seriePurple,
                                               serieLightBlue,
                                               serieOrange,
                                               serieTransparent) { ChartOptionsOverride = options };
            }
            else
            {
                return new SrmChartDataResults(new SrmDataSeries()) { ChartOptionsOverride = options };
            }
        }
        catch (Exception ex)
        {
            log.Error("Exception occured during getting data for Capacity Summary resource.", ex);
            throw;
        }
    }
}
