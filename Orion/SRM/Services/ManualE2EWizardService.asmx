﻿<%@ WebService Language="C#" Class="ManualE2EWizardService" %>
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.Logging;
using System.Collections.Generic;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common.ServiceProvider;
using System.Data;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.UnifiedResult;
using System;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ManualE2EWizardService : System.Web.Services.WebService
{
    private static readonly Log log = new Log();

    [WebMethod()]
    public int AddManualE2EMapping(int volumeId, int netObjectId, string netObjectType)
    {
        AuthorizationChecker.AllowNodeManagement();
        using (var proxy = BusinessLayerFactory.Create())
        {
            return proxy.AddManualE2EMapping(volumeId, netObjectId, netObjectType);
        }
    }

    [WebMethod()]
    public void DropManualE2EMapping(IEnumerable<int> volumeIds)
    {
        AuthorizationChecker.AllowNodeManagement();
        using (var proxy = BusinessLayerFactory.Create())
        {
            proxy.DropManualE2EMapping(volumeIds);
        }
    }

    [WebMethod()]
    public PageableDataTable GetAllNodes(string viewMode, string filter, string searchString)
    {
        
        var manualE2EMappingDAL = ServiceLocator.GetInstance<IManualE2EMappingDAL>();
        DataTable dataTable = manualE2EMappingDAL.GetAllNodes(viewMode, filter, searchString);

        return GetSelectedPage(dataTable);
    }
    
    private PageableDataTable GetSelectedPage(DataTable dataTable)
    {
        int totalRows = dataTable.Rows.Count;
        int startRowNumber = 0;
        int pageSize = 0;
        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);

        if (startRowNumber == null)
        {
            throw new ArgumentNullException("startRowNumber");
        }
        if (pageSize == null)
        {
            throw new ArgumentNullException("pageSize");
        }

        int endIndex = startRowNumber + pageSize;
        for (int i = dataTable.Rows.Count - 1; i >= 0; i--)
        {
            if (i < startRowNumber || i >= endIndex)
            {
                dataTable.Rows.RemoveAt(i);
            }
        }

        PageableDataTable pageableDataTable = new PageableDataTable(dataTable, totalRows);
        return pageableDataTable;
    }

    [WebMethod()]
    public PageableDataTable GetServerVolumesForNode(int nodeId, string showMode, string searchString, string filter)
    {
        var manualE2EMappingDAL = ServiceLocator.GetInstance<IManualE2EMappingDAL>();
        DataTable dataTable = manualE2EMappingDAL.GetServerVolumesForNode(nodeId, showMode, searchString, filter);
        return new PageableDataTable(dataTable, dataTable.Rows.Count);
    }

    [WebMethod()]
    public DataTable GetServerVolumesFilter(string fieldName)
    {
        var manualE2EMappingDAL = ServiceLocator.GetInstance<IManualE2EMappingDAL>();
        return manualE2EMappingDAL.GetServerVolumesFilter(fieldName);
    }

    [WebMethod()]
    public PageableDataTable GetAllArrays(string filter, string searchFilter)
    {
        var manualE2EMappingDAL = ServiceLocator.GetInstance<IManualE2EMappingDAL>();
        DataTable dataTable = manualE2EMappingDAL.GetAllArrays(filter, searchFilter);

        return GetSelectedPage(dataTable);
    }

    [WebMethod()]
    public PageableDataTable GetAllPools(string filter, string searchFilter)
    {
        var manualE2EMappingDAL = ServiceLocator.GetInstance<IManualE2EMappingDAL>();
        DataTable dataTable = manualE2EMappingDAL.GetAllPools(filter, searchFilter);

        return GetSelectedPage(dataTable);
    }

    [WebMethod()]
    public PageableDataTable GetAllLUNs(string filter, string searchFilter)
    {
        var manualE2EMappingDAL = ServiceLocator.GetInstance<IManualE2EMappingDAL>();
        DataTable dataTable = manualE2EMappingDAL.GetAllLUNs(filter, searchFilter);

        return GetSelectedPage(dataTable);
    }

    [WebMethod()]
    public PageableDataTable GetAllNasVolumes(string filter, string searchFilter)
    {
        var manualE2EMappingDAL = ServiceLocator.GetInstance<IManualE2EMappingDAL>();
        DataTable dataTable = manualE2EMappingDAL.GetAllNasVolumes(filter, searchFilter);

        return GetSelectedPage(dataTable);
    }

    [WebMethod()]
    public PageableDataTable GetAllVservers(string filter, string searchFilter)
    {
        var manualE2EMappingDAL = ServiceLocator.GetInstance<IManualE2EMappingDAL>();
        DataTable dataTable = manualE2EMappingDAL.GetAllVservers(filter, searchFilter);

        return GetSelectedPage(dataTable);
    }
}
