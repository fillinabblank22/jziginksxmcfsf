﻿<%@ WebService Language="C#" Class="ArrayPerformanceComparisonService" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;

using SolarWinds.Logging;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.Resources;
using SolarWinds.SRM.Web;
using V2DataSeries = SolarWinds.Orion.Web.Charting.v2.DataSeries;
using V2DataPoint = SolarWinds.Orion.Web.Charting.v2.DataPoint;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ArrayPerformanceComparisonService : ChartDataWebService 
{
    private static readonly Log log = new Log();

    [WebMethod(EnableSession = true)]
    public ChartDataResults GetData(ChartDataRequest request)
    {
        int daysOfDataToLoad = request.DaysOfDataToLoad;
        if (daysOfDataToLoad == 0 && request.AllSettings.ContainsKey("daysOfDataToLoad"))
        {
            daysOfDataToLoad = (int)request.AllSettings["daysOfDataToLoad"];
        }

        string netObjectPrefix = request.NetObjectIds[0].Split(':')[0];
        int netOjectId = int.Parse(request.NetObjectIds[0].Split(':')[1]);        
        DateTime startTime = DateTime.UtcNow.AddDays(-daysOfDataToLoad);
 	    DateTime endTime = DateTime.UtcNow;

        string firstComparisonChoice = (string)request.AllSettings["firstComparisonChoice"];
        string secondComparisonChoice = (string)request.AllSettings["secondComparisonChoice"];
        DataTable data = ServiceLocator.GetInstance<IChartsDAL>()
            .GetArrayPerformanceComparisonData(netObjectPrefix, netOjectId, firstComparisonChoice, secondComparisonChoice, startTime, endTime);

        List<KeyValuePair<DateTime, Tuple<double, double>>> listData = new List<KeyValuePair<DateTime, Tuple<double, double>>>();
        
        var firstSeries = CreateSeries(ArrayPerformanceComparison.ComparisonChoices[firstComparisonChoice], "firstComparisonChoice");
        var secondSeries = CreateSeries(ArrayPerformanceComparison.ComparisonChoices[secondComparisonChoice], "secondComparisonChoice");

        // Break date-time values into buckets SampleSizeInMinutes long, setting all date-time values in the bucket to the same value.
        // Having same date-time value will allow us to group by it to calculate avg/sum.
        foreach (DataRow poolData in data.Rows)
        {
            if (poolData["ObservationTimestamp"] is DBNull)
            {
                continue;
            }
            DateTime observationTimestamp = (DateTime)poolData["ObservationTimestamp"];

            // Skip points with NULL values
            if (poolData[firstComparisonChoice] is DBNull && poolData[secondComparisonChoice] is DBNull)
            {
                continue;
            }
            
            // Fix dateTime according to SampleSizeInMinutes. Lets say we have 15:01, 15:02, 15:03, 15:04, 16:01 and interval is 3 minutes.
            // All values which fall into interval will be the same.
            // After fix date time values will be "rounded" to interval 3, so like 15:01, 15:01, 15:01, 15:04, 16:01
            DateTime dateTime = WebHelper.ToSampleDateTime(observationTimestamp, request.SampleSizeInMinutes);
            double first = poolData[firstComparisonChoice] is DBNull ? 0 : Convert.ToDouble(poolData[firstComparisonChoice]);
            double second = poolData[secondComparisonChoice] is DBNull ? 0 : Convert.ToDouble(poolData[secondComparisonChoice]);
            listData.Add(new KeyValuePair<DateTime, Tuple<double, double>>(dateTime, new Tuple<double, double>(first, second)));
        }

        var firstData =
            listData.GroupBy(date => date.Key)
                .Select(
                    grp =>
                        new KeyValuePair<DateTime, double>(grp.Key,
                            FormatData(firstComparisonChoice,
                                IsTransferredChoice(firstComparisonChoice)
                                    ? grp.Sum(p => p.Value.Item1)
                                    : grp.Average(p => p.Value.Item1))))
                .ToList();

        var secondData =
            listData.GroupBy(date => date.Key)
                .Select(
                    grp =>
                        new KeyValuePair<DateTime, double>(grp.Key,
                            FormatData(secondComparisonChoice,
                                IsTransferredChoice(secondComparisonChoice)
                                    ? grp.Sum(p => p.Value.Item2)
                                    : grp.Average(p => p.Value.Item2))))
                .ToList();

        if (firstData.Count != 0 && secondData.Count != 0)
        {
            firstSeries.Data = WebHelper.SetDatePoints(request.SampleSizeInMinutes, startTime, endTime, firstData).ToList();
            secondSeries.Data = WebHelper.SetDatePoints(request.SampleSizeInMinutes, startTime, endTime, secondData).ToList();
        }
        
        var series = new List<V2DataSeries> { firstSeries, secondSeries };

        dynamic options = new JsonObject();
        options.yAxis = JsonObject.CreateJsonArray(1);

        return new ChartDataResults(series) { ChartOptionsOverride = options };
    }

    private bool IsTransferredChoice(string choiceValue)
    {
        return ArrayPerformanceComparison.ComparisonChoicesBytesRead == choiceValue ||
               ArrayPerformanceComparison.ComparisonChoicesBytesWrite == choiceValue ||
               ArrayPerformanceComparison.ComparisonChoicesBytesTotal == choiceValue;
    }
    
    private double FormatData(string name, double value)
    {
        double result;
        switch (name)
        {
            case ArrayPerformanceComparison.ComparisonChoicesBytesPSRead:
            case ArrayPerformanceComparison.ComparisonChoicesBytesPSWrite:
            case ArrayPerformanceComparison.ComparisonChoicesBytesPSTotal:
            case ArrayPerformanceComparison.ComparisonChoicesBytesRead:
            case ArrayPerformanceComparison.ComparisonChoicesBytesWrite:
            case ArrayPerformanceComparison.ComparisonChoicesBytesTotal:
                result = value / SRMConstants.BytesInMB;
                break;
            
            case ArrayPerformanceComparison.ComparisonChoicesIOPSRead:
            case ArrayPerformanceComparison.ComparisonChoicesIOPSWrite:
            case ArrayPerformanceComparison.ComparisonChoicesIOPSOther:
            case ArrayPerformanceComparison.ComparisonChoicesIOPSTotal:
                result = Math.Round(value, 2, MidpointRounding.AwayFromZero);
                break;
            
            default:
                log.ErrorFormat("Unexpected formatting name {0}", name);
                result = value;
                break;
        }
        
        return result;
    }

    private static DataSeries CreateSeries(string label, string tagName)
    {
        return new V2DataSeries()
        {
            Label = label,
            IsCustomDataSerie = false,
            ShowTrendLine = false,
            ShowPercentileLine = false,
            Data = new List<V2DataPoint>(),
            CustomProperties = null,
            TagName = tagName
        };
    }
}
