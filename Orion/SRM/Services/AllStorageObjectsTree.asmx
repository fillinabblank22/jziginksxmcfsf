﻿<%@ WebService Language="C#" Class="AllStorageObjectsTree" %>

using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Linq;
using System.Text.RegularExpressions;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Controllers.Alerts.Resources;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.DAL;
using SolarWinds.SRM.Common.Enums;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using SolarWinds.Data;
using SolarWinds.Orion.Core.Common.Extensions;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Services;
using SolarWinds.SRM.Common.Helpers;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Web.Resources;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class AllStorageObjectsTree : TreeServiceBase, IAjaxTreeService
{
    private const string VendorDirectoryConst = "VendorDirectory";

    private const string FilteredQueryErrorMessage = "Cannot display content because the filter '{0}' returns the following SWQL error:{1}{2}";

    /// <param name="resourceId">ID of resource with tree. This is used to get resource settings.</param>
    [WebMethod(EnableSession = true)]
    public IEnumerable<AjaxTreeNode> GetTreeNodes(int resourceId, string itemType, string itemValue, int fromIndex, int limit, string filter, AjaxTreeNode[] groupings)
    {
        ResourceInfo resource = ResourceManager.GetResourceByID(resourceId);
        Context.Items[typeof(ViewInfo).Name] = resource.View;

        var swqlFilter = resource.Properties[ResourcePropertiesKeys.SwqlFilter];
        if (string.IsNullOrEmpty(swqlFilter))
        {
            swqlFilter = "1=1";
        }

        Dictionary<string, string> filters = new Dictionary<string, string>() { { ResourcePropertiesKeys.SwqlFilter,  swqlFilter} };
        string orderBy = resource.Properties[ResourcePropertiesKeys.Ordering] ?? "Status DESC, Name ASC";

        // fresh approach to design service body, should be rewritten as well in newer versions 
        AjaxTreeNodeResult nodeResult = GetDataByType(resourceId, itemType, groupings, orderBy, fromIndex, limit, filters);
        var nodes = new List<AjaxTreeNode>(nodeResult.Items);
        int totalCount = nodeResult.AllCount;

        try
        {
            if (String.IsNullOrEmpty(itemType))
            {
                var dal = new StorageArrayDAL();
                DataTable dataTable = dal.GetAllArrays(orderBy, fromIndex, limit, swqlFilter);
                nodes.AddRange(CreateAjaxTreeNodesFromTable(dataTable, SwisEntities.StorageArrays, AjaxTreeNodeType.StorageArray.ToString(), resourceId.ToString(CultureInfo.InvariantCulture), filters, NodeWithLabelAndStatusConst));
                totalCount = dal.GetAllArraysCount(swqlFilter);
            }
            else if (itemType == AjaxTreeNodeType.StorageArray.ToString())
            {
                if (GetTreeNodesCount(resourceId, AjaxTreeNodeType.VserversDirectory.ToString(), itemValue, filters) > 0)
                {
                    var node = new AjaxTreeNode
                    {
                        Id = GetUniqueIdFromId(itemValue, AjaxTreeNodeType.VserversDirectory.ToString()),
                        Name = Resources.SrmWebContent.Resource_AllStorageObjectsTree_VserversConst,
                        Entity = null,
                        IsExpandable = true,
                        ItemType = AjaxTreeNodeType.VserversDirectory.ToString(),
                        Description = null,
                        ClassName = NodeWithLabelConst
                    };
                    nodes.Add(node);
                }

                if (GetTreeNodesCount(resourceId, AjaxTreeNodeType.PoolsDirectory.ToString(), itemValue, filters) > 0)
                {
                    var node = new AjaxTreeNode
                    {
                        Id = GetUniqueIdFromId(itemValue, AjaxTreeNodeType.PoolsDirectory.ToString()),
                        Name = Resources.SrmWebContent.Resource_AllStorageObjectsTree_StoragePoolsConst,
                        Entity = null,
                        IsExpandable = true,
                        ItemType = AjaxTreeNodeType.PoolsDirectory.ToString(),
                        Description = null,
                        ClassName = NodeWithLabelConst
                    };
                    nodes.Add(node);
                }
            }
            else if (itemType == AjaxTreeNodeType.PoolsDirectory.ToString())
            {
                var dal = new PoolDAL();
                int? id = GetIdFromUniqueId(itemValue);
                if (id != null)
                {
                    DataTable dataTable = dal.GetRootPoolsByArrayId((int)id, orderBy, fromIndex, limit, swqlFilter);
                    nodes.AddRange(CreateAjaxTreeNodesFromTable(dataTable, SwisEntities.Pools, AjaxTreeNodeType.Pool.ToString(), resourceId.ToString(CultureInfo.InvariantCulture), filters, NodeWithLabelAndStatusConst));
                    totalCount = dal.GetRootPoolsByArrayIdCount((int)id, swqlFilter);
                }
            }
            else if (itemType == AjaxTreeNodeType.ParentPoolDirectory.ToString())
            {
                var dal = new PoolDAL();
                int? id = GetIdFromUniqueId(itemValue);
                if (id != null)
                {
                    DataTable dataTable = dal.GetPoolsByParentPoolId((int)id, orderBy, fromIndex, limit, swqlFilter);
                    nodes.AddRange(CreateAjaxTreeNodesFromTable(dataTable, SwisEntities.Pools, AjaxTreeNodeType.Pool.ToString(), resourceId.ToString(CultureInfo.InvariantCulture), filters, NodeWithLabelAndStatusConst));
                    totalCount = dal.GetPoolsByParentPoolIdCount((int)id, swqlFilter);
                }
            }
            else if (itemType == AjaxTreeNodeType.Pool.ToString())
            {
                AjaxTreeNode node;
                if (GetTreeNodesCount(resourceId, AjaxTreeNodeType.ParentPoolDirectory.ToString(), itemValue, filters) > 0)
                {
                    node = new AjaxTreeNode
                    {
                        Id = GetUniqueIdFromId(itemValue, AjaxTreeNodeType.ParentPoolDirectory.ToString()),
                        Name = Resources.SrmWebContent.Resource_AllStorageObjectsTree_StoragePoolsConst,
                        Entity = null,
                        IsExpandable = true,
                        ItemType = AjaxTreeNodeType.ParentPoolDirectory.ToString(),
                        Description = null,
                        ClassName = NodeWithLabelConst
                    };
                    nodes.Add(node);
                }

                if (GetTreeNodesCount(resourceId, AjaxTreeNodeType.VolumesDirectory.ToString(), itemValue, filters) > 0)
                {
                    node = new AjaxTreeNode
                    {
                        Id = GetUniqueIdFromId(itemValue, AjaxTreeNodeType.VolumesDirectory.ToString()),
                        Name = Resources.SrmWebContent.Resource_AllStorageObjectsTree_VolumesConst,
                        Entity = null,
                        IsExpandable = true,
                        ItemType = AjaxTreeNodeType.VolumesDirectory.ToString(),
                        Description = null,
                        ClassName = NodeWithLabelConst
                    };
                    nodes.Add(node);
                }

                if (GetTreeNodesCount(resourceId, AjaxTreeNodeType.LUNsDirectory.ToString(), itemValue, filters) > 0)
                {
                    node = new AjaxTreeNode
                    {
                        Id = GetUniqueIdFromId(itemValue, AjaxTreeNodeType.LUNsDirectory.ToString()),
                        Name = Resources.SrmWebContent.Resource_AllStorageObjectsTree_LUNsConst,
                        Entity = null,
                        IsExpandable = true,
                        ItemType = AjaxTreeNodeType.LUNsDirectory.ToString(),
                        Description = null,
                        ClassName = NodeWithLabelConst
                    };
                    nodes.Add(node);
                }
            }
            else if (itemType == AjaxTreeNodeType.VolumesDirectory.ToString())
            {
                var dal = new VolumeDAL();
                int? id = GetIdFromUniqueId(itemValue);
                if (id != null)
                {
                    DataTable dataTable = dal.GetVolumesByPoolId((int)id, orderBy, fromIndex, limit, swqlFilter);
                    nodes.AddRange(CreateAjaxTreeNodesFromTable(dataTable, SwisEntities.Volume, AjaxTreeNodeType.Volume.ToString(), resourceId.ToString(CultureInfo.InvariantCulture), filters, NodeWithLabelAndStatusConst));
                    totalCount = dal.GetVolumesByPoolIdCount((int)id, swqlFilter);
                }
            }
            else if (itemType == AjaxTreeNodeType.LUNsDirectory.ToString())
            {
                var dal = new LunDAL();
                int? id = GetIdFromUniqueId(itemValue);
                if (id != null)
                {
                    DataTable dataTable = dal.GetLunsByPoolId((int)id, orderBy, fromIndex, limit, swqlFilter);
                    nodes.AddRange(CreateAjaxTreeNodesFromTable(dataTable, SwisEntities.Lun, AjaxTreeNodeType.LUN.ToString(), resourceId.ToString(CultureInfo.InvariantCulture), filters, NodeWithLabelAndStatusConst));
                    totalCount = dal.GetLunsByPoolIdCount((int)id, swqlFilter);
                }
            }
            else if (itemType == VendorDirectoryConst)
            {
                // get root level, grouping values are not of AjaxTreeNodeType

                IStorageArrayDAL dal = ServiceLocator.GetInstance<IStorageArrayDAL>();
                Dictionary<string, int> vendors = dal.GetDistinctVendorsAndStatuses(orderBy, fromIndex, limit, swqlFilter);
                foreach (var vendor in vendors)
                {
                    nodes.Add(new AjaxTreeNode
                    {
                        Id = vendor.Key,
                        Name = vendor.Key,
                        Entity = SwisEntities.FakeFamilies,
                        IsExpandable = true,
                        ItemType = AjaxTreeNodeType.Vendor.ToString(),
                        Description = vendor.Key,
                        ClassName = NodeWithLabelAndStatusHeaderConst,
                        Status = vendor.Value
                    });
                }
                totalCount = dal.GetDistinctVendorsCount(swqlFilter);
            }
            else if (itemType == AjaxTreeNodeType.VserversDirectory.ToString())
            {
                var dal = new VServerDAL();
                int? id = GetIdFromUniqueId(itemValue);
                if (id != null)
                {
                    DataTable vservers = dal.GetVserversByArrayId(id, orderBy, fromIndex, limit, swqlFilter);
                    nodes.AddRange(CreateAjaxTreeNodesFromTable(vservers, SwisEntities.VServers,
                        AjaxTreeNodeType.Vservers.ToString(), resourceId.ToString(CultureInfo.InvariantCulture), filters,
                        NodeWithLabelAndStatusConst));
                    totalCount = dal.GetVserversCountByArrayId((int)id, swqlFilter);
                }
            }
            else if (itemType == AjaxTreeNodeType.Vservers.ToString())
            {
                AjaxTreeNode node;
                if (GetTreeNodesCount(resourceId, AjaxTreeNodeType.VolumesDirectoryForVserver.ToString(), itemValue, filters) > 0)
                {
                    node = new AjaxTreeNode
                    {
                        Id = GetUniqueIdFromId(itemValue, AjaxTreeNodeType.VolumesDirectoryForVserver.ToString()),
                        Name = Resources.SrmWebContent.Resource_AllStorageObjectsTree_VolumesConst,
                        Entity = null,
                        IsExpandable = true,
                        ItemType = AjaxTreeNodeType.VolumesDirectoryForVserver.ToString(),
                        Description = null,
                        ClassName = NodeWithLabelConst
                    };
                    nodes.Add(node);
                }
            }
            else if (itemType == AjaxTreeNodeType.VolumesDirectoryForVserver.ToString())
            {
                var dal = new VolumeDAL();
                int? id = GetIdFromUniqueId(itemValue);
                if (id != null)
                {
                    DataTable dataTable = dal.GetVolumesByVserverId((int)id, orderBy, fromIndex, limit, swqlFilter);
                    nodes.AddRange(CreateAjaxTreeNodesFromTable(dataTable, SwisEntities.Volume, AjaxTreeNodeType.Volume.ToString(), resourceId.ToString(CultureInfo.InvariantCulture), filters, NodeWithLabelAndStatusConst));
                    totalCount = dal.GetVolumesByVserverIdCount((int)id, swqlFilter);
                }
            }
        }
        catch(SwisQueryException ex)
        {
            throw new SwisException(StringHelper.FormatInvariant(FilteredQueryErrorMessage, swqlFilter, Environment.NewLine, ex.Message));
        }

        // If there are more nodes than "limit", we should add "Show more" item in tree. It is represented by empty node.
        if ((totalCount > 0) && (totalCount > fromIndex + limit))
        {

            var showMoreNode = new AjaxTreeNode
            {
                Id = "EmptyNode",
                Name = (totalCount - fromIndex - limit).ToString(CultureInfo.CurrentCulture),
                ClassName = "SRM-HomeView-AllStorageObjects-NodeWithLabel"
            };
            return nodes.Concat(new[] { showMoreNode });
        }

        return nodes;
    }

    protected override int GetTreeNodesCount(int resourceId, string itemType, string itemValue, Dictionary<string, string> filters)
    {
        string swqlFilter = filters[ResourcePropertiesKeys.SwqlFilter];
        int totalCount = 0;

        if (itemType == AjaxTreeNodeType.PoolsDirectory.ToString() || itemType == AjaxTreeNodeType.StorageArray.ToString())
        {
            var dal = new PoolDAL();
            totalCount += dal.GetRootPoolsByArrayIdCount(Int32.Parse(itemValue), swqlFilter);
        }

        if (itemType == AjaxTreeNodeType.VolumesDirectory.ToString() || itemType == AjaxTreeNodeType.Pool.ToString())
        {
            var dal = new VolumeDAL();
            int? id = GetIdFromUniqueId(itemValue);
            if (id != null)
            {
                totalCount += dal.GetVolumesByPoolIdCount((int)id, swqlFilter);
            }
        }

        if (itemType == AjaxTreeNodeType.ParentPoolDirectory.ToString() || itemType == AjaxTreeNodeType.Pool.ToString())
        {
            var dal = new PoolDAL();
            int? id = GetIdFromUniqueId(itemValue);
            if (id != null)
            {
                totalCount += dal.GetPoolsByParentPoolIdCount((int)id, swqlFilter);
            }
        }

        if (itemType == AjaxTreeNodeType.LUNsDirectory.ToString() || itemType == AjaxTreeNodeType.Pool.ToString())
        {
            var dal = new LunDAL();
            int? id = GetIdFromUniqueId(itemValue);
            if (id != null)
            {
                totalCount += dal.GetLunsByPoolIdCount((int)id, swqlFilter);
            }
        }

        if (itemType == AjaxTreeNodeType.Vservers.ToString() || itemType == AjaxTreeNodeType.VolumesDirectoryForVserver.ToString())
        {
            var dal = new VolumeDAL();
            int? id = GetIdFromUniqueId(itemValue);
            if (id != null)
            {
                totalCount += dal.GetVolumesByVserverIdCount((int)id, swqlFilter);
            }
        }

        if (itemType == AjaxTreeNodeType.VserversDirectory.ToString() || itemType == AjaxTreeNodeType.StorageArray.ToString())
        {
            var dal = new VServerDAL();
            int? id = GetIdFromUniqueId(itemValue);
            if (id != null)
            {
                totalCount += dal.GetVserversCountByArrayId((int)id, swqlFilter);
            }
        }

        return totalCount;
    }

    [WebMethod]
    public bool GetShowGettingStarted(int resourceId, string filter)
    {
        IStorageArrayDAL dal = ServiceLocator.GetInstance<IStorageArrayDAL>();

        // check if number of transactions without filter is zero))
        return !dal.GetIds().Any();
    }

    private AjaxTreeNodeResult GetDataByType(int resourceId, string type, AjaxTreeNode[] value, string orderBy, int fromIndex, int limit, Dictionary<string, string> filters)
    {
        if (string.IsNullOrEmpty(type))
        {
            return new AjaxTreeNodeResult { Items = new List<AjaxTreeNode>() };
        }

        var filter = filters[ResourcePropertiesKeys.SwqlFilter];

        if (type.Contains(GroupByStorageObjectsHelper.CustomPropertyPrefix))
        {
            return new GroupByStorageObjectsHelper().GetCustomPropertyGroups(type, value, orderBy, fromIndex, limit, filter, NodeWithLabelAndStatusHeaderConst);
        }

        switch ((AjaxTreeNodeType)Enum.Parse(typeof (AjaxTreeNodeType), type))
        {
            case AjaxTreeNodeType.Model:
                return new GroupByStorageObjectsHelper().GetCustomPropertyGroups(type, value, orderBy, fromIndex, limit, filter, NodeWithLabelAndStatusHeaderConst);
            case AjaxTreeNodeType.Vendor:
                return new GroupByStorageObjectsHelper().GetCustomPropertyGroups(type, value, orderBy, fromIndex, limit, filter, NodeWithLabelAndStatusHeaderConst);
            case AjaxTreeNodeType.StorageArrayDirectory:
                var result = new AjaxTreeNodeResult();
                result.Items = CreateAjaxTreeNodesFromTable(
                        new GroupByStorageObjectsHelper().GetArraysByGrouping(value, orderBy, fromIndex, limit, filter, NodeWithLabelAndStatusConst),
                        SwisEntities.StorageArrays,
                        AjaxTreeNodeType.StorageArray.ToString(),
                        resourceId.ToString(CultureInfo.InvariantCulture),
                        filters,
                        NodeWithLabelAndStatusConst);

                result.AllCount = new GroupByStorageObjectsHelper().GetArraysByGroupingCount(value, filter);

                return result;

            default:
                return new AjaxTreeNodeResult { Items = new List<AjaxTreeNode>() };
        }
    }
}