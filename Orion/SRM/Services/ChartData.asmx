﻿<%@ WebService Language="C#" Class="ChartData" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Resources;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using System.Reflection;
using System.Drawing;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.Resources;
using SolarWinds.SRM.Web.Charting;
using SolarWinds.SRM.Web;
using SolarWinds.Logging;
using System.Threading.Tasks;
using System.Collections;
using V2DataSeries = SolarWinds.Orion.Web.Charting.v2.DataSeries;
using V2DataPoint = SolarWinds.Orion.Web.Charting.v2.DataPoint;
using Threshold = SolarWinds.SRM.Web.Models.Threshold;
using SolarWinds.Orion.Web.InformationService;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ChartData : ChartDataWebService
{
    private static readonly Log log = new Log();

    [WebMethod]
    public ChartDataResults GetCapacityRiskData(List<object[]> data)
    {
        var series = new List<DataSeries>();
        var currentContext = HttpContext.Current;

        Parallel.ForEach<object[]>(data, (item) =>
        {
            HttpContext.Current = currentContext;

            try
            {
                var capacityRisk = new CapacityRisk();

                Int64 totalCapacity = Convert.ToInt64(item[11]);
                Double? offset = item[13] == null ? default(double?) : Convert.ToDouble(item[13]);
                Double? slope = item[14] == null ? default(double?) : Convert.ToDouble(item[14]);

                DateTime? capacityRunOutWarning = capacityRisk.ComputeCapacityRunOut(thresholdLevel: (int)item[8],
                                                                                     totalCapacity: totalCapacity,
                                                                                     offset: offset,
                                                                                     slope: slope);
                DateTime? capacityRunOutCritical = capacityRisk.ComputeCapacityRunOut(thresholdLevel: (int)item[9],
                                                                                      totalCapacity: totalCapacity,
                                                                                      offset: offset,
                                                                                      slope: slope);
                DateTime? capacityRunOutTotal = capacityRisk.ComputeCapacityRunOut(thresholdLevel: (int)item[10],
                                                                                   totalCapacity: totalCapacity,
                                                                                   offset: offset,
                                                                                   slope: slope);

                dynamic properties = new JsonObject();
                properties.capacityRunOutDateWarning = WebHelper.GetDayDifference(capacityRunOutWarning);
                properties.capacityRunOutDateCritical = WebHelper.GetDayDifference(capacityRunOutCritical);
                properties.capacityRunOutDateTotal = WebHelper.GetDayDifference(capacityRunOutTotal);

                var serie = new DataSeries()
                {
                    TagName = string.Concat(item[4], "_", item[0]),
                    Label = (string)item[1],
                    CustomProperties = properties
                };
                lock (((ICollection)series).SyncRoot)
                {
                    series.Add(serie);
                }

                double dataSlope;
                IEnumerable<V2DataPoint> points = capacityRisk.GetData((StorageObject)item[4], (int)item[0], out dataSlope);
                foreach (V2DataPoint point in points)
                {
                    serie.AddPoint(point);
                }

                properties.slope = totalCapacity == 0 ? 0 : dataSlope * 100 / Convert.ToDouble(totalCapacity);
            }
            catch (Exception ex)
            {
                log.Error(String.Format(CultureInfo.InvariantCulture, "Exception occured during getting information for charts and thresholds for Capacity Risk resource. Row: {0}", String.Join(", ", item)), ex);
                throw;
            }
        });

        return new ChartDataResults(series);
    }

    private static SrmDataSeries PrepareNasVolumeCapacityChartResult(Int64 freeCapacity, Int64 usedCapacity)
    {
        SrmDataSeries serie = new SrmDataSeries();
        if (usedCapacity != 0 || freeCapacity != 0)
        {
            serie.AddPoint(new SrmDataPoint(SrmWebContent.AggregatedNASVolumeCapacity_Used, usedCapacity, "#1C61A3"));
            serie.AddPoint(new SrmDataPoint(SrmWebContent.AggregatedNASVolumeCapacity_Free, freeCapacity, "#B4B4B4"));
        }
        return serie;
    }

    [WebMethod]
    public SrmChartDataResults GetAllAggregatedNASVolumeCapacity(ChartDataRequest request)
    {
        Int64 freeCapacity;
        Int64 usedCapacity;

        ServiceLocator.GetInstance<IChartsDAL>().GetAllAggregatedNASVolumeCapacity(out usedCapacity, out freeCapacity);
        return new SrmChartDataResults(PrepareNasVolumeCapacityChartResult(freeCapacity, usedCapacity));
    }

    [WebMethod]
    public SrmChartDataResults GetAggregatedNASVolumeCapacity(ChartDataRequest request)
    {
        Int64 freeCapacity;
        Int64 usedCapacity;

        string netObjectPrefix = request.NetObjectIds[0].Split(':')[0];
        int netObjectId = int.Parse(request.NetObjectIds[0].Split(':')[1]);
        ServiceLocator.GetInstance<IChartsDAL>().GetAggregatedNASVolumeCapacity(netObjectPrefix, netObjectId, out usedCapacity, out freeCapacity);
        return new SrmChartDataResults(PrepareNasVolumeCapacityChartResult(freeCapacity, usedCapacity));
    }

    [WebMethod]
    public ChartDataResults GetLunIOPSPerformance(ChartDataRequest request)
    {
        int lunID = int.Parse(request.NetObjectIds[0].Split(':')[1]);
        DateTime startTime = DateTime.UtcNow.AddDays(-request.DaysOfDataToLoad);
        DateTime endTime = DateTime.UtcNow;
        DataTable data = ServiceLocator.GetInstance<IChartsDAL>().GetLunIOPSPerformance(lunID, startTime, endTime);

        if (data.Rows.Count == 0)
        {
            return new ChartDataResults(new List<V2DataSeries>());
        }

        var serieTotal = ChartThresholdHelper.CreateSeriesWithThreshold(SrmWebContent.Lun_Chart_Total_Label_IOPS);
        var serieRead = ChartThresholdHelper.CreateSeriesWithThreshold(SrmWebContent.Lun_Chart_Read_Label_IOPS);
        var serieWrite = ChartThresholdHelper.CreateSeriesWithThreshold(SrmWebContent.Lun_Chart_Write_Label_IOPS);
        var serieOther = ChartThresholdHelper.CreateSeriesWithThreshold(SrmWebContent.Lun_Chart_Other_Label_IOPS);

        List<KeyValuePair<DateTime, Iops>> listData = new List<KeyValuePair<DateTime, Iops>>();
        foreach (DataRow row in data.Rows)
        {
            DateTime observationTimestamp = (DateTime)row["ObservationTimestamp"];
            double dateTimeOA = observationTimestamp.ToOADate();
            DateTime dateTime = DateTime.FromOADate((Math.Floor(dateTimeOA / request.SampleSizeInMinutes * 1440) * request.SampleSizeInMinutes / 1440.0));
            double total = row["IOPSTotal"] is DBNull ? 0 : (Single)row["IOPSTotal"];
            double read = row["IOPSRead"] is DBNull ? 0 : (Single)row["IOPSRead"];
            double write = row["IOPSWrite"] is DBNull ? 0 : (Single)row["IOPSWrite"];
            double other = total - (read + write);
            listData.Add(new KeyValuePair<DateTime, Iops>(dateTime, new Iops { Total = total, Read = read, Write = write, Other = other }));
        }

        var groupedData = listData.GroupBy(s => s.Key).Select(grp => new KeyValuePair<DateTime, Iops>(grp.Key,
            new Iops
            {
                Read = grp.Average(p => p.Value.Read),
                Write = grp.Average(p => p.Value.Write),
                Total = grp.Average(p => p.Value.Total),
                Other = grp.Average(p => p.Value.Other)
            })).ToList();

        var totalData = groupedData.Select(d => new KeyValuePair<DateTime, double>(d.Key, d.Value.Total)).ToList();
        serieTotal.Data = WebHelper.SetDatePoints(request.SampleSizeInMinutes, startTime, endTime, totalData).ToList();

        var readData = groupedData.Select(d => new KeyValuePair<DateTime, double>(d.Key, d.Value.Read)).ToList();
        serieRead.Data = WebHelper.SetDatePoints(request.SampleSizeInMinutes, startTime, endTime, readData).ToList();

        var writeData = groupedData.Select(d => new KeyValuePair<DateTime, double>(d.Key, d.Value.Write)).ToList();
        serieWrite.Data = WebHelper.SetDatePoints(request.SampleSizeInMinutes, startTime, endTime, writeData).ToList();

        var otherData = groupedData.Select(d => new KeyValuePair<DateTime, double>(d.Key, d.Value.Other)).ToList();
        serieOther.Data = WebHelper.SetDatePoints(request.SampleSizeInMinutes, startTime, endTime, otherData).ToList();

        var series = new List<V2DataSeries>();
        series.Add(serieTotal);
        series.Add(serieRead);
        series.Add(serieWrite);
        series.Add(serieOther);
        var result = new ChartDataResults(series);
        return result;
    }

    private class Iops
    {
        public double Total { get; set; }
        public double Read { get; set; }
        public double Write { get; set; }
        public double Other { get; set; }
    }


    [WebMethod]
    public ChartDataResults GetLunThroughputPerformance(ChartDataRequest request)
    {
        int lunID = int.Parse(request.NetObjectIds[0].Split(':')[1]);
        DateTime endTime = DateTime.UtcNow;
        DateTime startTime = endTime.AddDays(-request.DaysOfDataToLoad);
        DataTable data = ServiceLocator.GetInstance<IChartsDAL>().GetLunThroughputPerformance(lunID, startTime, endTime);

        if (data.Rows.Count == 0)
        {
            return new ChartDataResults(new List<V2DataSeries>());
        }

        var serieTotal = ChartThresholdHelper.CreateSeriesWithThreshold(SrmWebContent.Lun_Chart_Total_Label_Throughput);
        var serieRead = ChartThresholdHelper.CreateSeriesWithThreshold(SrmWebContent.Lun_Chart_Read_Label_Throughput);
        var serieWrite = ChartThresholdHelper.CreateSeriesWithThreshold(SrmWebContent.Lun_Chart_Write_Label_Throughput);

        List<KeyValuePair<DateTime, BytesPS>> listData = new List<KeyValuePair<DateTime, BytesPS>>();
        foreach (DataRow row in data.Rows)
        {
            DateTime dateTime = WebHelper.ToSampleDateTime((DateTime)row["ObservationTimestamp"], request.SampleSizeInMinutes);
            double? total = row["BytesPSTotal"] is DBNull ? null : (Single?)row["BytesPSTotal"] / SRMConstants.BytesInMB;
            double? read = row["BytesPSRead"] is DBNull ? null : (Single?)row["BytesPSRead"] / SRMConstants.BytesInMB;
            double? write = row["BytesPSWrite"] is DBNull ? null : (Single?)row["BytesPSWrite"] / SRMConstants.BytesInMB;
            listData.Add(new KeyValuePair<DateTime, BytesPS>(dateTime, new BytesPS { Total = total, Read = read, Write = write }));
        }

        var groupedDataTotal = listData.GroupBy(s => s.Key).Select(grp => new KeyValuePair<DateTime, double?>(grp.Key, grp.Average(p => p.Value.Total))).ToList();
        var groupedDataRead = listData.GroupBy(s => s.Key).Select(grp => new KeyValuePair<DateTime, double?>(grp.Key, grp.Average(p => p.Value.Read))).ToList();
        var groupedDataWrite = listData.GroupBy(s => s.Key).Select(grp => new KeyValuePair<DateTime, double?>(grp.Key, grp.Average(p => p.Value.Write))).ToList();

        serieTotal.Data = WebHelper.SetDatePoints(request.SampleSizeInMinutes, startTime, endTime, groupedDataTotal).ToList();
        serieRead.Data = WebHelper.SetDatePoints(request.SampleSizeInMinutes, startTime, endTime, groupedDataRead).ToList();
        serieWrite.Data = WebHelper.SetDatePoints(request.SampleSizeInMinutes, startTime, endTime, groupedDataWrite).ToList();

        var series = new List<V2DataSeries>();
        series.Add(serieTotal);
        series.Add(serieRead);
        series.Add(serieWrite);
        var result = new ChartDataResults(series);
        return result;
    }

    private class BytesPS
    {
        public double? Total { get; set; }
        public double? Read { get; set; }
        public double? Write { get; set; }
    }


    [WebMethod]
    public ChartDataResults GetLunIOSize(ChartDataRequest request)
    {
        int lunID = int.Parse(request.NetObjectIds[0].Split(':')[1]);
        DateTime startTime = DateTime.UtcNow.AddDays(-request.DaysOfDataToLoad);
        DateTime endTime = DateTime.UtcNow;
        DataTable data = ServiceLocator.GetInstance<IChartsDAL>().GetLunIOSize(lunID, startTime, endTime);

        if (data.Rows.Count == 0)
        {
            return new ChartDataResults(new List<V2DataSeries>());
        }

        var serieTotal = ChartThresholdHelper.CreateSeriesWithThreshold(SrmWebContent.Lun_Chart_Total_Label_Latency);
        var serieRead = ChartThresholdHelper.CreateSeriesWithThreshold(SrmWebContent.Lun_Chart_Read_Label_Latency);
        var serieWrite = ChartThresholdHelper.CreateSeriesWithThreshold(SrmWebContent.Lun_Chart_Write_Label_Latency);
        var serieOther = ChartThresholdHelper.CreateSeriesWithThreshold(SrmWebContent.Lun_Chart_Other_Label_Latency);

        List<KeyValuePair<DateTime, IOLatency>> listData = new List<KeyValuePair<DateTime, IOLatency>>();
        foreach (DataRow row in data.Rows)
        {
            DateTime dateTime = WebHelper.ToSampleDateTime((DateTime)row["ObservationTimestamp"], request.SampleSizeInMinutes);
            double? total = row["IOLatencyTotal"] is DBNull ? null : (Single?)row["IOLatencyTotal"];
            double? read = row["IOLatencyRead"] is DBNull ? null : (Single?)row["IOLatencyRead"];
            double? write = row["IOLatencyWrite"] is DBNull ? null : (Single?)row["IOLatencyWrite"];
            double? other = total - (read + write);
            listData.Add(new KeyValuePair<DateTime, IOLatency>(dateTime, new IOLatency { Total = total, Read = read, Write = write, Other = other }));
        }

        var groupedDataTotal = listData.GroupBy(s => s.Key).Select(grp => new KeyValuePair<DateTime, double?>(grp.Key, grp.Average(p => p.Value.Total))).ToList();
        var groupedDataRead = listData.GroupBy(s => s.Key).Select(grp => new KeyValuePair<DateTime, double?>(grp.Key, grp.Average(p => p.Value.Read))).ToList();
        var groupedDataWrite = listData.GroupBy(s => s.Key).Select(grp => new KeyValuePair<DateTime, double?>(grp.Key, grp.Average(p => p.Value.Write))).ToList();
        var groupedDataOther = listData.GroupBy(s => s.Key).Select(grp => new KeyValuePair<DateTime, double?>(grp.Key, grp.Average(p => p.Value.Other))).ToList();

        serieTotal.Data = WebHelper.SetDatePoints(request.SampleSizeInMinutes, startTime, endTime, groupedDataTotal).ToList();
        serieRead.Data = WebHelper.SetDatePoints(request.SampleSizeInMinutes, startTime, endTime, groupedDataRead).ToList();
        serieWrite.Data = WebHelper.SetDatePoints(request.SampleSizeInMinutes, startTime, endTime, groupedDataWrite).ToList();
        serieOther.Data = WebHelper.SetDatePoints(request.SampleSizeInMinutes, startTime, endTime, groupedDataOther).ToList();

        var series = new List<V2DataSeries>();
        series.Add(serieTotal);
        series.Add(serieRead);
        series.Add(serieWrite);
        series.Add(serieOther);
        var result = new ChartDataResults(series);
        return result;
    }

    private class IOLatency
    {
        public double? Total { get; set; }
        public double? Read { get; set; }
        public double? Write { get; set; }
        public double? Other { get; set; }
    }

    [WebMethod]
    public ChartDataResults GetArraySizeAggregatedIOPS(ChartDataRequest request)
    {
        Func<int, DataTable> loader = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArraySizeAggregatedIOPS;
        return this.GetArrayMetricBarChart(request, loader);
    }

    [WebMethod]
    public ChartDataResults GetVserverSizeAggregatedIOPS(ChartDataRequest request)
    {
        Func<int, DataTable> loader = ServiceLocator.GetInstance<IVServerDAL>().GetVserverSizeAggregatedIops;
        return this.GetArrayMetricBarChart(request, loader);
    }

    [WebMethod]
    public ChartDataResults GetArraySizeAverageThroughput(ChartDataRequest request)
    {
        Func<int, DataTable> loader = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArraySizeAverageThroughput;
        return this.GetArrayMetricBarChart(request, loader);
    }

    [WebMethod]
    public ChartDataResults GetVserverSizeAverageThroughput(ChartDataRequest request)
    {
        Func<int, DataTable> loader = ServiceLocator.GetInstance<IVServerDAL>().GetVServerSizeAverageThroughput;
        return this.GetArrayMetricBarChart(request, loader);
    }

    private ChartDataResults GetArrayMetricBarChart(ChartDataRequest request, Func<int, DataTable> loader)
    {
        int arrayId = int.Parse(request.NetObjectIds[0].Split(':')[1]);

        var data = loader(arrayId);
        var serieData = CreateBarSeries();
        serieData.Data = new List<V2DataPoint>();

        if (data.Rows.Count > 0)
        {
            int timeIndex = data.Columns["ObservationTimestamp"].Ordinal;
            int valueIndex = data.Columns["Total"].Ordinal;

            foreach (DataRow row in data.Rows)
            {
                var time = row.Get<DateTime>(timeIndex, DateTime.MinValue).ToLocalTime();
                var value = row.Get<double?>(valueIndex, null);

                var point = value == null
                                ? DataPoint.CreateNullPoint(time.ToLocalTime())
                                : DataPoint.CreatePoint(time.ToLocalTime(), (double)value);
                serieData.Data.Add(point);
            }

            var lastDate = data.Rows[data.Rows.Count - 1].Get<DateTime>(timeIndex).ToLocalTime().AddMinutes(+30);

            serieData.Data.Add(DataPoint.CreateNullPoint(lastDate));
        }

        var series = new List<V2DataSeries>()
        {
            serieData
        };

        return new ChartDataResults(series);
    }

    [WebMethod]
    public SrmChartDataResults GetLatencyHistogram(ChartDataRequest request)
    {
        string warningBarColor = "#FF9E18";
        string criticalBarColor = "#E71829";

        int objectId = int.Parse(request.NetObjectIds[0].Split(':')[1]);
        string dataSourceMethod = (string)((IDictionary<String, Object>)request.AllSettings["ResourceProperties"])["datasourcemethod"];

        MethodInfo dataSource = typeof(LatencyHistogram).GetMethod(dataSourceMethod, BindingFlags.Instance | BindingFlags.Public);
        var data = (LatencyHistogram.LatencyHistogramData)dataSource.Invoke(new LatencyHistogram(), new object[] { objectId });

        var categories = new Point[]
        {
            new Point(0, 5),
            new Point(6, 10),
            new Point(11, 15),
            new Point(16, 20),
            new Point(21, 30),
            new Point(31, 50),
            new Point(51, 100),
            new Point(101, 500),
            new Point(501, 1000),
            new Point(1000, int.MaxValue)
        };

        dynamic options = new JsonObject();
        options.xAxis.categories = categories.Select(k => String.Concat(k.X.ToString(CultureInfo.CurrentUICulture), k.Y == int.MaxValue ? "+" : String.Format(CultureInfo.CurrentUICulture, "-{0}", k.Y))).ToArray();
        options.xAxis.max = categories.Length - 1;
        options.yAxis = JsonObject.CreateJsonArray(1);
        options.yAxis[0].title.text = data.YAxisTitle;

        if (data.Series.SelectMany(s => s.Value.Data).All(d => d < 0))
        {
            return new SrmChartDataResults(new SrmDataSeries()) { ChartOptionsOverride = options };
        }
        else
        {
            return new SrmChartDataResults()
            {
                ChartOptionsOverride = options,
                DataSeries = data.Series.Select(s =>
                {
                    var serie = new SrmDataSeries()
                    {
                        TagName = s.Key.GetDescription(),
                        NetObjectId = request.NetObjectIds[0],
                        Label = data.YAxisTitle
                    };

                    var histogram = categories.ToDictionary(p => p, i => 0);
                    foreach (var item in s.Value.Data)
                    {
                        Point key = histogram.Keys.FirstOrDefault(r => item >= r.X && item <= r.Y);
                        if (key != default(Point))
                        {
                            histogram[key]++;
                        }
                    }

                    foreach (var item in histogram)
                    {
                        var color = string.Empty;
                        if (s.Value.Threshold != null && s.Value.Threshold.WarnLevel.HasValue && item.Key.X >= s.Value.Threshold.WarnLevel)
                        {
                            color = warningBarColor;
                        }
                        if (s.Value.Threshold != null && s.Value.Threshold.CriticalLevel.HasValue && item.Key.X >= s.Value.Threshold.CriticalLevel)
                        {
                            color = criticalBarColor;
                        }
                        serie.AddPoint(new SrmDataPoint(item.Value, color));
                    }
                    serie.Data[0].Add("min", DataPoint.ToUnixTime(s.Value.MinTime.ToLocalTime()));
                    serie.Data[0].Add("max", DataPoint.ToUnixTime(s.Value.MaxTime.ToLocalTime()));

                    return serie;
                }).ToArray()
            };
        }
    }

    private static DataSeries CreateBarSeries()
    {
        var serieData = new V2DataSeries()
        {
            TagName = "Data",
            ComputedFrom = null,
            IsCustomDataSerie = false,
            NetObjectId = null,
            ShowTrendLine = false,
            ShowPercentileLine = false,
            Data = new List<V2DataPoint>(),
            CustomProperties = null,
        };
        return serieData;
    }

    [WebMethod]
    public ChartDataResults GetTop10VolumesByTotalIOPS(ChartDataRequest request)
    {
        string netObjectPrefix = request.NetObjectIds[0].Split(':')[0];
        var netObjectId = int.Parse(request.NetObjectIds[0].Split(':')[1]);
        var startTime = DateTime.UtcNow.AddDays(-request.DaysOfDataToLoad);
        var endTime = DateTime.UtcNow;
        int maxRecords = int.Parse(((IDictionary<String, Object>)request.AllSettings["ResourceProperties"])["maxrecords"].ToString());

        var values = ServiceLocator.GetInstance<IChartsDAL>().GetTopTenVolumesTotalIOPSPerformance(netObjectPrefix, netObjectId, maxRecords, startTime, endTime);
        var series = ApplySampleInterval(values, startTime, endTime, request.SampleSizeInMinutes, "VolumeID", "Name", "Caption", "ObservationTimestamp", "IOPSTotal",
            (input) => input.Average(i => i.Value));

        dynamic options = new JsonObject();
        options.yAxis = JsonObject.CreateJsonArray(1);
        if (series.Any())
        {
            ChartThresholdHelper.AddThresholdBands(series.First(), options.yAxis[0]);
        }
        return new ChartDataResults(series) { ChartOptionsOverride = options };
    }

    /// <summary>
    /// Groups data by sample interval
    /// </summary>
    /// <param name="table">input data</param>
    /// <param name="startTime">start date time of the interval</param>
    /// <param name="endTime">end date time of the interval</param>
    /// <param name="interval">sample interval in minutes</param>
    /// <param name="idColumn">column that holds NAS ID</param>
    /// <param name="nameColumn">column that holds serie name</param>
    /// <param name="captionColumn">column that holds serie label</param>
    /// <param name="timeColumn">datetime column</param>
    /// <param name="valueColumn">column with values</param>
    /// <param name="operation">how to aggregate data within the group (sum,avg,...)</param>
    /// <returns></returns>
    private static IEnumerable<DataSeriesWithThreshold> ApplySampleInterval(DataTable table, DateTime startTime, DateTime endTime, int interval, string idColumn, string nameColumn, string captionColumn, string timeColumn, string valueColumn, Func<IEnumerable<KeyValuePair<DateTime, float>>, float> operation)
    {
        if (table == null)
            throw new ArgumentNullException("table");
        if (interval <= 0)
            throw new ArgumentOutOfRangeException("interval");
        if (operation == null)
            throw new ArgumentNullException("operation");

        var result = new List<DataSeriesWithThreshold>();
        var series = new List<Tuple<string, string, List<KeyValuePair<DateTime, float>>, string>>();

        foreach (DataRow row in table.Rows)
        {
            // skip null values
            if (row[valueColumn] is DBNull)
                continue;

            var id = row[idColumn].ToString();
            var name = (string)row[nameColumn];
            var caption = (string)row[captionColumn];

            bool containsId = series.Any(c => c.Item1 == id);
            if (!containsId)
            {
                var values = new List<KeyValuePair<DateTime, float>>();
                series.Add(new Tuple<string, string, List<KeyValuePair<DateTime, float>>, string>(id, name, values, caption));
            }

            var sampleTime = WebHelper.ToSampleDateTime((DateTime)row[timeColumn], interval);
            series.Single(s => s.Item1 == id).Item3.Add(new KeyValuePair<DateTime, float>(sampleTime, (float)row[valueColumn]));
        }

        foreach (var serie in series)
        {
            DataSeriesWithThreshold chartSerie = ChartThresholdHelper.CreateSeriesWithThreshold(serie.Item2, serie.Item4);
            var groupedData = serie.Item3.GroupBy(s => s.Key)
                .Select(grp => new KeyValuePair<DateTime, double>(grp.Key, operation(grp)))
                .ToList();

            chartSerie.Data = WebHelper.SetDatePoints(interval, startTime, endTime, groupedData).ToList();

            result.Add(chartSerie);
        }
        return result;
    }

    [WebMethod]
    public ChartDataResults GetIOPSPerformancePerVolumeRead(ChartDataRequest request)
    {
        return GetIOPSPerformancePerVolumeByField(request, "IOPSRead");
    }

    [WebMethod]
    public ChartDataResults GetIOPSPerformancePerVolumeWrite(ChartDataRequest request)
    {
        return GetIOPSPerformancePerVolumeByField(request, "IOPSWrite");
    }

    [WebMethod]
    public ChartDataResults GetIOPSPerformancePerVolumeOther(ChartDataRequest request)
    {
        return GetIOPSPerformancePerVolumeByField(request, "IOPSOther");
    }

    [WebMethod]
    public ChartDataResults GetIOPSPerformancePerVolumeTotal(ChartDataRequest request)
    {
        return GetIOPSPerformancePerVolumeByField(request, "IOPSTotal");
    }

    private ChartDataResults GetIOPSPerformancePerVolumeByField(ChartDataRequest request, string field)
    {
        var tDal = ServiceLocator.GetInstance<IThresholdDAL>();
        var cDal = ServiceLocator.GetInstance<IChartsDAL>();

        var netObjectParts = request.NetObjectIds[0].Split(':');
        int poolID = cDal.DeterminePoolIDByRelatedNetObject(netObjectParts[0], int.Parse(netObjectParts[1]));
        DataTable volumeIops = null;
        DateTime startTime = DateTime.UtcNow.AddDays(-((int)request.AllSettings["daysOfDataToLoad"]));
        DateTime endTime = DateTime.UtcNow;
        int sampleSizeInMinutes = ((int)request.AllSettings["sampleSizeInMinutes"]);
        int numberOfSeriesToShow = (int)request.AllSettings["numberOfSeriesToShow"];
        string poolName = string.Empty;
        string poolCaption = string.Empty;
        string poolStatus = string.Empty;
        dynamic customProperties;

        var series = new List<V2DataSeries>();

        Threshold threshold;

        switch (field)
        {
            case "IOPSTotal":
                volumeIops = cDal.GetIOPSPerformancePerVolumeTotal(poolID, startTime, endTime);
                threshold = tDal.GetThresholdOrSafe(ThresholdDataType.PoolIOPSTotal, poolID);
                break;
            case "IOPSOther":
                volumeIops = cDal.GetIOPSPerformancePerVolumeOther(poolID, startTime, endTime);
                threshold = tDal.GetThresholdOrSafe(ThresholdDataType.PoolIOPSOther, poolID);
                break;
            case "IOPSWrite":
                volumeIops = cDal.GetIOPSPerformancePerVolumeWrite(poolID, startTime, endTime);
                threshold = tDal.GetThresholdOrSafe(ThresholdDataType.PoolIOPSWrite, poolID);
                break;
            case "IOPSRead":
                volumeIops = cDal.GetIOPSPerformancePerVolumeRead(poolID, startTime, endTime);
                threshold = tDal.GetThresholdOrSafe(ThresholdDataType.PoolIOPSRead, poolID);
                break;
            default: throw new NotSupportedException(field);
        }

        dynamic options = new JsonObject();
        options.hideResource = false;

        if (volumeIops.Rows.Count == 0)
        {
            if (cDal.GetVolumesCountPerPool(poolID) == 0)
            {
                options.hideResource = true;
                return new ChartDataResults(new List<V2DataSeries>() { new V2DataSeries() { Data = new List<DataPoint>() { DataPoint.CreatePoint(0, 0) } } }) { ChartOptionsOverride = options };
            }
            return new ChartDataResults(series);
        }

        Dictionary<Tuple<int, string, string>, List<KeyValuePair<DateTime, double>>> groupedVolumes = new Dictionary<Tuple<int, string, string>, List<KeyValuePair<DateTime, double>>>();
        Dictionary<Tuple<int, string, string>, KeyValuePair<string, string>> volumeStatuses = new Dictionary<Tuple<int, string, string>, KeyValuePair<string, string>>();
        foreach (DataRow volumedata in volumeIops.Rows)
        {
            Tuple<int, string, string> volumeIdNameTuple = new Tuple<int, string, string>((int)(volumedata["VolumeID"]), (string)volumedata["Name"], (string)volumedata["Caption"]);
            if (!groupedVolumes.ContainsKey(volumeIdNameTuple))
            {
                groupedVolumes[volumeIdNameTuple] = new List<KeyValuePair<DateTime, double>>();
            }
            List<KeyValuePair<DateTime, double>> temp = groupedVolumes[volumeIdNameTuple];
            DateTime observationTimestamp = (DateTime)volumedata["ObservationTimestamp"];
            DateTime datetime = WebHelper.ToSampleDateTime(observationTimestamp, sampleSizeInMinutes);
            double total = volumedata[field] is DBNull ? 0 : (Single)volumedata[field];
            temp.Add(new KeyValuePair<DateTime, double>(datetime, total));
            if (string.IsNullOrEmpty(poolName))
            {
                poolName = (string)volumedata["PoolName"];
                poolCaption = (string)volumedata["PoolCaption"];
            }
            if (string.IsNullOrEmpty(poolStatus))
            {
                poolStatus = volumedata["PoolStatus"].ToString();
            }
            if (!volumeStatuses.ContainsKey(volumeIdNameTuple))
            {
                volumeStatuses[volumeIdNameTuple] = new KeyValuePair<string, string>(volumedata["VolumeID"].ToString(),
                    volumedata["VolumeStatus"].ToString());
            }
        }
        DataSeriesWithThreshold poolSeries = ChartThresholdHelper.CreateSeriesWithThreshold(poolName, poolCaption);
        series.Add(poolSeries);
        Dictionary<DateTime, double> poolDictionaty = new Dictionary<DateTime, double>();
        foreach (var groupedVolume in groupedVolumes)
        {
            List<KeyValuePair<DateTime, double>> temp = groupedVolume.Value;
            V2DataSeries volumeSeries = ChartThresholdHelper.CreateSeriesWithThreshold(groupedVolume.Key.Item2, groupedVolume.Key.Item3);
            var groupedData = temp.GroupBy(s => s.Key).Select(grp => new KeyValuePair<DateTime, double>(grp.Key, grp.Average(p => p.Value))).ToList();
            volumeSeries.Data = WebHelper.SetDatePoints(sampleSizeInMinutes, startTime, endTime, groupedData).ToList();
            foreach (KeyValuePair<DateTime, double> volumeData in groupedData)
            {
                if (!poolDictionaty.ContainsKey(volumeData.Key))
                {
                    poolDictionaty[volumeData.Key] = 0;
                }
                poolDictionaty[volumeData.Key] += volumeData.Value;
            }
            customProperties = new JsonObject();
            customProperties.CustomProperty = String.Empty;
            customProperties.Status = volumeStatuses[groupedVolume.Key].Value;
            customProperties.Id = volumeStatuses[groupedVolume.Key].Key;
            customProperties.SwisEntity = SwisEntities.Volume;
            volumeSeries.CustomProperties = customProperties;
            series.Add(volumeSeries);
        }

        var poolData = poolDictionaty.Select(d => new KeyValuePair<DateTime, double>(d.Key, d.Value)).ToList();
        poolSeries.Data = WebHelper.SetDatePoints(sampleSizeInMinutes, startTime, endTime, poolData).ToList();
        customProperties = new JsonObject();
        customProperties.CustomProperty = "Pool";
        customProperties.Status = poolStatus;
        customProperties.Id = poolID;
        customProperties.SwisEntity = SwisEntities.Pools;
        poolSeries.CustomProperties = customProperties;
        poolSeries.Threshold = threshold;

        options.yAxis = JsonObject.CreateJsonArray(1);
        if (series.Count > 0)
        {
            ChartThresholdHelper.AddThresholdBands(poolSeries, options.yAxis[0]);
        }

        var largestSeries = series.Select(s => new KeyValuePair<V2DataSeries, double>(s, s.Data.Max(d => d.IsNullPoint ? 0 : d.Max))).OrderByDescending(s => s.Value).Select(t => t.Key).Take(numberOfSeriesToShow != 0 ? numberOfSeriesToShow : series.Count).ToList();
        var result = new ChartDataResults(largestSeries) { ChartOptionsOverride = options };
        return result;
    }

    [WebMethod]
    public ChartDataResults GetLunPerformanceByField(ChartDataRequest request)
    {
        var poolID = (int)(request.AllSettings["poolID"] ?? -1);

        var cDal = ServiceLocator.GetInstance<IChartsDAL>();

        if (poolID < 0)
        {
            var netObjectParts = request.NetObjectIds[0].Split(':');
            poolID = cDal.DeterminePoolIDByRelatedNetObject(netObjectParts[0], int.Parse(netObjectParts[1]));
        }

        //if there are no pools mapped to current LUN then just returning empty model to show "Data is not available"
        if (poolID < 0) return new ChartDataResults(new List<V2DataSeries>());

        var fieldName = (StatisticFields)Enum.Parse(typeof(StatisticFields), InvariantString.ToString(request.AllSettings["filterField"]));

        var chartRange = new ChartDateRange(DateTime.UtcNow.AddDays(-((int)request.AllSettings["daysOfDataToLoad"])), ((int)request.AllSettings["sampleSizeInMinutes"]), ((int)request.AllSettings["numberOfSeriesToShow"]));

        DataTable performanceValues = cDal.GetLunPerformance(poolID, chartRange.StartTime, chartRange.EndTime, fieldName, chartRange.SampleSizeInMinutes);

        var uniqueIdentifier = new NetObjectUniqueIdentifier(request.NetObjectIds[0]);
        var threshold = ChartThresholdHelper.GetThreshold(fieldName, uniqueIdentifier.Prefix, uniqueIdentifier.ID);

        return EntityStatisticsHelper.GetEntityPerformanceByField(
            poolID,
            uniqueIdentifier.ID,
            performanceValues,
            threshold,
            SwisEntities.Lun,
            (bool)(request.AllSettings["showParent"] ?? false),
            chartRange);
    }

    [WebMethod]
    public ChartDataResults GetIOPSPerformancePerLunRead(ChartDataRequest request)
    {
        return GetIOPSPerformancePerLunByField(request, "IOPSRead");
    }

    [WebMethod]
    public ChartDataResults GetIOPSPerformancePerLunWrite(ChartDataRequest request)
    {
        return GetIOPSPerformancePerLunByField(request, "IOPSWrite");
    }

    [WebMethod]
    public ChartDataResults GetIOPSPerformancePerLunOther(ChartDataRequest request)
    {
        return GetIOPSPerformancePerLunByField(request, "IOPSOther");
    }

    [WebMethod]
    public ChartDataResults GetIOPSPerformancePerLunTotal(ChartDataRequest request)
    {
        return GetIOPSPerformancePerLunByField(request, "IOPSTotal");
    }

    private ChartDataResults GetIOPSPerformancePerLunByField(ChartDataRequest request, string field)
    {
        var tDal = ServiceLocator.GetInstance<IThresholdDAL>();
        var cDal = ServiceLocator.GetInstance<IChartsDAL>();

        var netObjectParts = request.NetObjectIds[0].Split(':');
        var poolID = cDal.DeterminePoolIDByRelatedNetObject(netObjectParts[0], int.Parse(netObjectParts[1]));
        DataTable lunIops = null;
        DateTime startTime = DateTime.UtcNow.AddDays(-((int)request.AllSettings["daysOfDataToLoad"]));
        DateTime endTime = DateTime.UtcNow;
        int sampleSizeInMinutes = ((int)request.AllSettings["sampleSizeInMinutes"]);
        int numberOfSeriesToShow = (int)request.AllSettings["numberOfSeriesToShow"];
        string poolName = string.Empty;
        string poolCaption = string.Empty;
        string poolStatus = string.Empty;
        dynamic customProperties;

        var series = new List<V2DataSeries>();

        Threshold threshold;

        switch (field)
        {
            case "IOPSTotal":
                lunIops = cDal.GetIOPSPerformancePerLunTotal(poolID, startTime, endTime);
                threshold = tDal.GetThresholdOrSafe(ThresholdDataType.PoolIOPSTotal, poolID);
                break;
            case "IOPSOther":
                lunIops = cDal.GetIOPSPerformancePerLunOther(poolID, startTime, endTime);
                threshold = tDal.GetThresholdOrSafe(ThresholdDataType.PoolIOPSOther, poolID);
                break;
            case "IOPSWrite":
                lunIops = cDal.GetIOPSPerformancePerLunWrite(poolID, startTime, endTime);
                threshold = tDal.GetThresholdOrSafe(ThresholdDataType.PoolIOPSWrite, poolID);
                break;
            case "IOPSRead":
                lunIops = cDal.GetIOPSPerformancePerLunRead(poolID, startTime, endTime);
                threshold = tDal.GetThresholdOrSafe(ThresholdDataType.PoolIOPSRead, poolID);
                break;
            default: throw new NotSupportedException(field);
        }

        if (lunIops.Rows.Count == 0)
        {
            return new ChartDataResults(series);
        }

        Dictionary<Tuple<int, string, string>, List<KeyValuePair<DateTime, double>>> groupedLuns = new Dictionary<Tuple<int, string, string>, List<KeyValuePair<DateTime, double>>>();
        Dictionary<Tuple<int, string, string>, KeyValuePair<string, string>> lunStatuses = new Dictionary<Tuple<int, string, string>, KeyValuePair<string, string>>();

        foreach (DataRow lunData in lunIops.Rows)
        {
            Tuple<int, string, string> lunIdNameCaptionTuple = new Tuple<int, string, string>((int)(lunData["LunID"]), (string)lunData["Name"], (string)lunData["Caption"]);

            if (!groupedLuns.ContainsKey(lunIdNameCaptionTuple))
            {
                groupedLuns[lunIdNameCaptionTuple] = new List<KeyValuePair<DateTime, double>>();
            }
            List<KeyValuePair<DateTime, double>> temp = groupedLuns[lunIdNameCaptionTuple];
            DateTime observationTimestamp = (DateTime)lunData["ObservationTimestamp"];
            DateTime datetime = WebHelper.ToSampleDateTime(observationTimestamp, sampleSizeInMinutes);
            double total = lunData[field] is DBNull ? 0 : (Single)lunData[field];
            temp.Add(new KeyValuePair<DateTime, double>(datetime, total));
            if (string.IsNullOrEmpty(poolName))
            {
                poolName = (string)lunData["PoolName"];
                poolCaption = (string)lunData["PoolCaption"];
            }
            if (string.IsNullOrEmpty(poolStatus))
            {
                poolStatus = lunData["PoolStatus"].ToString();
            }
            if (!lunStatuses.ContainsKey(lunIdNameCaptionTuple))
            {
                lunStatuses[lunIdNameCaptionTuple] = new KeyValuePair<string, string>(lunData["LunID"].ToString(),
                    lunData["LunStatus"].ToString());
            }
        }
        DataSeriesWithThreshold poolSeries = ChartThresholdHelper.CreateSeriesWithThreshold(poolName, poolCaption);
        series.Add(poolSeries);
        Dictionary<DateTime, double> poolDictionaty = new Dictionary<DateTime, double>();
        foreach (var groupedLun in groupedLuns)
        {
            List<KeyValuePair<DateTime, double>> temp = groupedLun.Value;
            V2DataSeries lunSeries = ChartThresholdHelper.CreateSeriesWithThreshold(groupedLun.Key.Item2, groupedLun.Key.Item3);
            var groupedData = temp.GroupBy(s => s.Key).Select(grp => new KeyValuePair<DateTime, double>(grp.Key, grp.Average(p => p.Value))).ToList();
            lunSeries.Data = WebHelper.SetDatePoints(sampleSizeInMinutes, startTime, endTime, groupedData).ToList();
            customProperties = new JsonObject();
            customProperties.CustomProperty = String.Empty;
            customProperties.Status = lunStatuses[groupedLun.Key].Value;
            customProperties.Id = lunStatuses[groupedLun.Key].Key;
            customProperties.SwisEntity = SwisEntities.Lun;
            lunSeries.CustomProperties = customProperties;
            foreach (KeyValuePair<DateTime, double> lunData in groupedData)
            {
                if (!poolDictionaty.ContainsKey(lunData.Key))
                {
                    poolDictionaty[lunData.Key] = 0;
                }
                poolDictionaty[lunData.Key] += lunData.Value;
            }
            series.Add(lunSeries);
        }
        var poolData = poolDictionaty.Select(d => new KeyValuePair<DateTime, double>(d.Key, d.Value)).ToList();
        poolSeries.Data = WebHelper.SetDatePoints(sampleSizeInMinutes, startTime, endTime, poolData).ToList();
        customProperties = new JsonObject();
        customProperties.CustomProperty = "Pool";
        customProperties.Status = poolStatus;
        customProperties.Id = poolID;
        customProperties.SwisEntity = SwisEntities.Pools;
        poolSeries.CustomProperties = customProperties;
        poolSeries.Threshold = threshold;
        dynamic options = new JsonObject();
        options.yAxis = JsonObject.CreateJsonArray(1);
        if (series.Count > 0)
        {
            ChartThresholdHelper.AddThresholdBands(poolSeries, options.yAxis[0]);
        }

        var largestSeries = series.Select(s => new KeyValuePair<V2DataSeries, double>(s, s.Data.Max(d => d.IsNullPoint ? 0 : d.Max))).OrderByDescending(s => s.Value).Select(t => t.Key).Take(numberOfSeriesToShow != 0 ? numberOfSeriesToShow : series.Count).ToList();
        var result = new ChartDataResults(largestSeries) { ChartOptionsOverride = options };
        return result;
    }

    [WebMethod]
    public ChartDataResults GetPerformancePerRelatedNASVolumes(ChartDataRequest request)
    {
        var poolID = (int)(request.AllSettings["poolID"] ?? -1);

        var cDal = ServiceLocator.GetInstance<IChartsDAL>();

        if (poolID < 0)
        {
            var netObjectParts = request.NetObjectIds[0].Split(':');
            poolID = cDal.DeterminePoolIDByRelatedNetObject(netObjectParts[0], int.Parse(netObjectParts[1]));
        }

        //if there are no pools mapped to current LUN then just returning empty model to show "Data is not available"
        if (poolID < 0) return new ChartDataResults(new List<V2DataSeries>());

        var chartRange = new ChartDateRange(DateTime.UtcNow.AddDays(-((int)request.AllSettings["daysOfDataToLoad"])), ((int)request.AllSettings["sampleSizeInMinutes"]), ((int)request.AllSettings["numberOfSeriesToShow"]));
        var fieldName = (StatisticFields)Enum.Parse(typeof(StatisticFields), request.AllSettings["filterField"].ToString());

        DataTable performanceValues = cDal.GetPerformancePerRelatedNASVolumes(poolID, GetPerformanceValueFieldName(fieldName), chartRange.StartTime, chartRange.EndTime, chartRange.SampleSizeInMinutes);

        var uniqueIdentifier = new NetObjectUniqueIdentifier(request.NetObjectIds[0]);
        var threshold = ChartThresholdHelper.GetThreshold(fieldName, uniqueIdentifier.Prefix, uniqueIdentifier.ID);

        return EntityStatisticsHelper.GetEntityPerformanceByField(
            poolID,
            uniqueIdentifier.ID,
            performanceValues,
            threshold,
            SwisEntities.Volume,
            (bool)request.AllSettings["hasParent"],
            chartRange);
    }

    private static string GetPerformanceValueFieldName(StatisticFields fieldName)
    {
        switch (fieldName)
        {
            case StatisticFields.IOPSOther:
                return "IOPSOther";
            case StatisticFields.IOLatencyOther:
                return "IOLatencyOther";
            default:
                return fieldName.ToString();
        }
    }



    private void ValidateMap(Dictionary<string, string> keyToColumnMap)
    {
        var expectedKeys = new[] {"id", "name", "caption", "data", "status"};
        foreach (var expectedKey in expectedKeys)
        {
            if (!keyToColumnMap.Keys.Contains(expectedKey))
            {
                throw new ArgumentException($"There is no expected key: '{expectedKey}'");
            }
        }
    }

    private ChartDataResults GetTopDataValues(ChartDataRequest request, Dictionary<string, string> keyToColumnMap, Func<string, int, int, DateTime, DateTime, DataTable> getDataCallback)
    {
        const double bytesInMB = 1048576.0;
        this.ValidateMap(keyToColumnMap);

        string netObjectPrefix = request.NetObjectIds[0].Split(':')[0];
        var netObjectId = int.Parse(request.NetObjectIds[0].Split(':')[1]);
        DataTable data;

        DateTime endTime = DateTime.UtcNow;
        DateTime startTime = endTime.AddDays(-request.DaysOfDataToLoad);

        int maxRecords = int.Parse(((IDictionary<String, Object>)request.AllSettings["ResourceProperties"])["maxrecords"].ToString());

        var series = new List<DataSeriesWithThreshold>();
        data = getDataCallback(netObjectPrefix, netObjectId, maxRecords, startTime, endTime);

        Dictionary<Tuple<int, string, string>, List<KeyValuePair<DateTime, double>>> groupedEntities = new Dictionary<Tuple<int, string, string>, List<KeyValuePair<DateTime, double>>>();
        Dictionary<Tuple<int, string, string>, KeyValuePair<string, string>> entityStatuses = new Dictionary<Tuple<int, string, string>, KeyValuePair<string, string>>();

        foreach (DataRow row in data.Rows)
        {
            Tuple<int, string, string> idNameCaptionTuple = new Tuple<int, string, string>(
                (int) row[keyToColumnMap["id"]],
                (string) row[keyToColumnMap["name"]],
                (string) row[keyToColumnMap["caption"]]);

            if (!groupedEntities.ContainsKey(idNameCaptionTuple))
            {
                groupedEntities[idNameCaptionTuple] = new List<KeyValuePair<DateTime, double>>();
            }
            List<KeyValuePair<DateTime, double>> temp = groupedEntities[idNameCaptionTuple];

            DateTime observationTimestamp = (DateTime)row["ObservationTimestamp"];
            DateTime datetime = WebHelper.ToSampleDateTime(observationTimestamp, request.SampleSizeInMinutes);

            double total = row[keyToColumnMap["data"]] is DBNull ? 0 : (Single)row[keyToColumnMap["data"]];
            if (keyToColumnMap["data"].StartsWith("BytesPS"))
            {
                total /= bytesInMB;
            }

            temp.Add(new KeyValuePair<DateTime, double>(datetime, total));

            if (!entityStatuses.ContainsKey(idNameCaptionTuple))
            {
                entityStatuses[idNameCaptionTuple] = new KeyValuePair<string, string>(row[keyToColumnMap["id"]].ToString(), row[keyToColumnMap["status"]].ToString());
            }
        }

        foreach (var groupedEntity in groupedEntities)
        {
            List<KeyValuePair<DateTime, double>> temp = groupedEntity.Value;
            var groupedData = temp.GroupBy(s => s.Key).Select(grp => new KeyValuePair<DateTime, double>(grp.Key, grp.Average(p => p.Value))).ToList();

            DataSeriesWithThreshold entitySeries = ChartThresholdHelper.CreateSeriesWithThreshold(groupedEntity.Key.Item2, groupedEntity.Key.Item3);
            entitySeries.Data = WebHelper.SetDatePoints(request.SampleSizeInMinutes, startTime, endTime, groupedData).ToList();

            series.Add(entitySeries);
        }

        dynamic options = new JsonObject();
        options.yAxis = JsonObject.CreateJsonArray(1);

        var result = new ChartDataResults(series) { ChartOptionsOverride = options };
        return result;
    }

    [WebMethod]
    public ChartDataResults GetTopTenLunsTotalIOPSPerformance(ChartDataRequest request)
    {
        var keyToColumnMap = new Dictionary<string, string>
        {
            { "id", "LunID" },
            { "name", "Name" },
            { "caption", "Caption" },
            { "status", "LunStatus" },
            { "data", "IOPSTotal" },
        };

        return GetTopDataValues(request, keyToColumnMap,
            ServiceLocator.GetInstance<IChartsDAL>().GetTopTenLunsTotalIOPSPerformance);
    }

    [WebMethod]
    public ChartDataResults GetTopStorageControllerPortsTotalIOPSPerformance(ChartDataRequest request)
    {
        var keyToColumnMap = new Dictionary<string, string>
        {
            { "id", "StorageControllerPortID" },
            { "name", "Name" },
            { "caption", "Name" },
            { "status", "PortStatus" },
            { "data", "IOPSTotal" },
        };

        return GetTopDataValues(request, keyToColumnMap,
        ServiceLocator.GetInstance<IChartsDAL>().GetTopStorageControllerPortsTotalIOPSPerformance);
    }

    [WebMethod]
    public ChartDataResults GetTopStorageControllerPortsThroughputPerformance(ChartDataRequest request)
    {
        var keyToColumnMap = new Dictionary<string, string>
        {
            { "id", "StorageControllerPortID" },
            { "name", "Name" },
            { "caption", "Name" },
            { "status", "PortStatus" },
            { "data", "BytesPSTotal" },
        };

        return GetTopDataValues(request, keyToColumnMap,
            ServiceLocator.GetInstance<IChartsDAL>().GetTopStorageControllerPortsBytesPSTotalPerformance);
    }

    [WebMethod]
    public ChartDataResults GetStorageArrayIOPSTotal(ChartDataRequest request)
    {
        return SimpleLoadStorageArrayData(request, "IOPSTotal", "IOPSTotal");
    }

    [WebMethod]
    public ChartDataResults GetStorageArrayIOPSRead(ChartDataRequest request)
    {
        return SimpleLoadStorageArrayData(request, "IOPSRead", "IOPSRead");
    }

    [WebMethod]
    public ChartDataResults GetStorageArrayIOPSWrite(ChartDataRequest request)
    {
        return SimpleLoadStorageArrayData(request, "IOPSWrite", "IOPSWrite");
    }

    [WebMethod]
    public ChartDataResults GetStorageArrayIOSizeTotal(ChartDataRequest request)
    {
        return SimpleLoadStorageArrayData(request, "IOSizeTotal", "IOSizeTotal");
    }

    [WebMethod]
    public ChartDataResults GetStorageArrayIOSizeRead(ChartDataRequest request)
    {
        return SimpleLoadStorageArrayData(request, "IOSizeRead", "IOSizeRead");
    }

    [WebMethod]
    public ChartDataResults GetStorageArrayIOSizeWrite(ChartDataRequest request)
    {
        return SimpleLoadStorageArrayData(request, "IOSizeWrite", "IOSizeWrite");
    }

    [WebMethod]
    public ChartDataResults GetStorageArrayBytesPSTotal(ChartDataRequest request)
    {
        return SimpleLoadStorageArrayData(request, "BytesPSTotal", "BytesPSTotal");
    }

    [WebMethod]
    public ChartDataResults GetStorageArrayBytesPSRead(ChartDataRequest request)
    {
        return SimpleLoadStorageArrayData(request, "BytesPSRead", "BytesPSRead");
    }

    [WebMethod]
    public ChartDataResults GetStorageArrayBytesPSWrite(ChartDataRequest request)
    {
        return SimpleLoadStorageArrayData(request, "BytesPSWrite", "BytesPSWrite");
    }

    [WebMethod]
    public ChartDataResults GetLunIOPSTotal(ChartDataRequest request)
    {
        return SimpleLoadLunData(request, "IOPSTotal", "IOPSTotal");
    }

    [WebMethod]
    public ChartDataResults GetLunIOPSRead(ChartDataRequest request)
    {
        return SimpleLoadLunData(request, "IOPSRead", "IOPSRead");
    }

    [WebMethod]
    public ChartDataResults GetLunIOPSWrite(ChartDataRequest request)
    {
        return SimpleLoadLunData(request, "IOPSWrite", "IOPSWrite");
    }

    [WebMethod]
    public ChartDataResults GetLunIOSizeTotal(ChartDataRequest request)
    {
        return SimpleLoadLunData(request, "IOSizeTotal", "IOSizeTotal");
    }

    [WebMethod]
    public ChartDataResults GetLunIOSizeRead(ChartDataRequest request)
    {
        return SimpleLoadLunData(request, "IOSizeRead", "IOSizeRead");
    }

    [WebMethod]
    public ChartDataResults GetLunIOSizeWrite(ChartDataRequest request)
    {
        return SimpleLoadLunData(request, "IOSizeWrite", "IOSizeWrite");
    }

    [WebMethod]
    public ChartDataResults GetLunBytesPSTotal(ChartDataRequest request)
    {
        return SimpleLoadLunData(request, "BytesPSTotal", "BytesPSTotal");
    }

    [WebMethod]
    public ChartDataResults GetLunBytesPSRead(ChartDataRequest request)
    {
        return SimpleLoadLunData(request, "BytesPSRead", "BytesPSRead");
    }

    [WebMethod]
    public ChartDataResults GetLunBytesPSWrite(ChartDataRequest request)
    {
        return SimpleLoadLunData(request, "BytesPSWrite", "BytesPSWrite");
    }

    [WebMethod]
    public ChartDataResults GetLunIOLatencyTotal(ChartDataRequest request)
    {
        return SimpleLoadLunData(request, "IOLatencyTotal", "IOLatencyTotal");
    }

    [WebMethod]
    public ChartDataResults GetPoolIOPSTotal(ChartDataRequest request)
    {
        return SimpleLoadPoolData(request, "IOPSTotal", "IOPSTotal");
    }

    [WebMethod]
    public ChartDataResults GetPoolIOPSRead(ChartDataRequest request)
    {
        return SimpleLoadPoolData(request, "IOPSRead", "IOPSRead");
    }

    [WebMethod]
    public ChartDataResults GetPoolIOPSWrite(ChartDataRequest request)
    {
        return SimpleLoadPoolData(request, "IOPSWrite", "IOPSWrite");
    }

    [WebMethod]
    public ChartDataResults GetPoolIOPSOther(ChartDataRequest request)
    {
        return SimpleLoadPoolData(request, "IOPSOther", "IOPSOther");
    }

    [WebMethod]
    public ChartDataResults GetPoolIOSizeTotal(ChartDataRequest request)
    {
        return SimpleLoadPoolData(request, "IOSizeTotal", "IOSizeTotal");
    }

    [WebMethod]
    public ChartDataResults GetPoolIOSizeRead(ChartDataRequest request)
    {
        return SimpleLoadPoolData(request, "IOSizeRead", "IOSizeRead");
    }

    [WebMethod]
    public ChartDataResults GetPoolIOSizeWrite(ChartDataRequest request)
    {
        return SimpleLoadPoolData(request, "IOSizeWrite", "IOSizeWrite");
    }

    [WebMethod]
    public ChartDataResults GetPoolBytesPSTotal(ChartDataRequest request)
    {
        return SimpleLoadPoolData(request, "BytesPSTotal", "BytesPSTotal");
    }

    [WebMethod]
    public ChartDataResults GetPoolBytesPSRead(ChartDataRequest request)
    {
        return SimpleLoadPoolData(request, "BytesPSRead", "BytesPSRead");
    }

    [WebMethod]
    public ChartDataResults GetPoolBytesPSWrite(ChartDataRequest request)
    {
        return SimpleLoadPoolData(request, "BytesPSWrite", "BytesPSWrite");
    }

    [WebMethod]
    public ChartDataResults GetVServerIOPSTotal(ChartDataRequest request)
    {
        return SimpleLoadVServerData(request, "IOPSTotal", "IOPSTotal");
    }

    [WebMethod]
    public ChartDataResults GetVServerIOPSRead(ChartDataRequest request)
    {
        return SimpleLoadVServerData(request, "IOPSRead", "IOPSRead");
    }

    [WebMethod]
    public ChartDataResults GetVServerIOPSWrite(ChartDataRequest request)
    {
        return SimpleLoadVServerData(request, "IOPSWrite", "IOPSWrite");
    }

    [WebMethod]
    public ChartDataResults GetVServerIOPSOther(ChartDataRequest request)
    {
        return SimpleLoadVServerData(request, "IOPSOther", "IOPSOther");
    }

    [WebMethod]
    public ChartDataResults GetVServerIOSizeTotal(ChartDataRequest request)
    {
        return SimpleLoadVServerData(request, "IOSizeTotal", "IOSizeTotal");
    }

    [WebMethod]
    public ChartDataResults GetVServerIOSizeRead(ChartDataRequest request)
    {
        return SimpleLoadVServerData(request, "IOSizeRead", "IOSizeRead");
    }

    [WebMethod]
    public ChartDataResults GetVServerIOSizeWrite(ChartDataRequest request)
    {
        return SimpleLoadVServerData(request, "IOSizeWrite", "IOSizeWrite");
    }

    [WebMethod]
    public ChartDataResults GetVServerBytesPSTotal(ChartDataRequest request)
    {
        return SimpleLoadVServerData(request, "BytesPSTotal", "BytesPSTotal");
    }

    [WebMethod]
    public ChartDataResults GetVServerBytesPSRead(ChartDataRequest request)
    {
        return SimpleLoadVServerData(request, "BytesPSRead", "BytesPSRead");
    }

    [WebMethod]
    public ChartDataResults GetVServerBytesPSWrite(ChartDataRequest request)
    {
        return SimpleLoadVServerData(request, "BytesPSWrite", "BytesPSWrite");
    }

    [WebMethod]
    public ChartDataResults GetVolumeIOPSTotal(ChartDataRequest request)
    {
        return SimpleLoadVolumeData(request, "IOPSTotal", "IOPSTotal");
    }

    [WebMethod]
    public ChartDataResults GetVolumeIOPSRead(ChartDataRequest request)
    {
        return SimpleLoadVolumeData(request, "IOPSRead", "IOPSRead");
    }

    [WebMethod]
    public ChartDataResults GetVolumeIOPSWrite(ChartDataRequest request)
    {
        return SimpleLoadVolumeData(request, "IOPSWrite", "IOPSWrite");
    }

    [WebMethod]
    public ChartDataResults GetVolumeIOPSOther(ChartDataRequest request)
    {
        return SimpleLoadVolumeData(request, "IOPSOther", "IOPSOther");
    }

    [WebMethod]
    public ChartDataResults GetVolumeIOLatencyTotal(ChartDataRequest request)
    {
        return SimpleLoadVolumeData(request, "IOLatencyTotal", "IOLatencyTotal");
    }

    [WebMethod]
    public ChartDataResults GetVolumeIOLatencyRead(ChartDataRequest request)
    {
        return SimpleLoadVolumeData(request, "IOLatencyRead", "IOLatencyRead");
    }

    [WebMethod]
    public ChartDataResults GetVolumeIOLatencyWrite(ChartDataRequest request)
    {
        return SimpleLoadVolumeData(request, "IOLatencyWrite", "IOLatencyWrite");
    }

    [WebMethod]
    public ChartDataResults GetVolumeIOLatencyOther(ChartDataRequest request)
    {
        return SimpleLoadVolumeData(request, "IOLatencyOther", "IOLatencyOther");
    }

    [WebMethod]
    public ChartDataResults GetVolumeIOSizeTotal(ChartDataRequest request)
    {
        return SimpleLoadVolumeData(request, "IOSizeTotal", "IOSizeTotal");
    }

    [WebMethod]
    public ChartDataResults GetVolumeIOSizeRead(ChartDataRequest request)
    {
        return SimpleLoadVolumeData(request, "IOSizeRead", "IOSizeRead");
    }

    [WebMethod]
    public ChartDataResults GetVolumeIOSizeWrite(ChartDataRequest request)
    {
        return SimpleLoadVolumeData(request, "IOSizeWrite", "IOSizeWrite");
    }

    [WebMethod]
    public ChartDataResults GetVolumeBytesPSTotal(ChartDataRequest request)
    {
        return SimpleLoadVolumeData(request, "BytesPSTotal", "BytesPSTotal");
    }

    [WebMethod]
    public ChartDataResults GetVolumeBytesPSRead(ChartDataRequest request)
    {
        return SimpleLoadVolumeData(request, "BytesPSRead", "BytesPSRead");
    }

    [WebMethod]
    public ChartDataResults GetVolumeBytesPSWrite(ChartDataRequest request)
    {
        return SimpleLoadVolumeData(request, "BytesPSWrite", "BytesPSWrite");
    }

    [WebMethod]
    public ChartDataResults GetStorageControllerIOPSTotal(ChartDataRequest request)
    {
        return SimpleLoadStorageControllerData(request, "IOPSTotal", "IOPSTotal");
    }

    [WebMethod]
    public ChartDataResults GetStorageControllerIOPSRead(ChartDataRequest request)
    {
        return SimpleLoadStorageControllerData(request, "IOPSRead", "IOPSRead");
    }

    [WebMethod]
    public ChartDataResults GetStorageControllerIOPSWrite(ChartDataRequest request)
    {
        return SimpleLoadStorageControllerData(request, "IOPSWrite", "IOPSWrite");
    }

    [WebMethod]
    public ChartDataResults GetStorageControllerIOPSDistribution(ChartDataRequest request)
    {
        return SimpleLoadStorageControllerData(request, "IOPSDistribution", "IOPSDistribution");
    }


    [WebMethod]
    public ChartDataResults GetStorageControllerIOSizeTotal(ChartDataRequest request)
    {
        return SimpleLoadStorageControllerData(request, "IOSizeTotal", "IOSizeTotal");
    }

    [WebMethod]
    public ChartDataResults GetStorageControllerIOSizeRead(ChartDataRequest request)
    {
        return SimpleLoadStorageControllerData(request, "IOSizeRead", "IOSizeRead");
    }

    [WebMethod]
    public ChartDataResults GetStorageControllerIOSizeWrite(ChartDataRequest request)
    {
        return SimpleLoadStorageControllerData(request, "IOSizeWrite", "IOSizeWrite");
    }

    [WebMethod]
    public ChartDataResults GetStorageControllerBytesPSTotal(ChartDataRequest request)
    {
        return SimpleLoadStorageControllerData(request, "BytesPSTotal", "BytesPSTotal");
    }

    [WebMethod]
    public ChartDataResults GetStorageControllerBytesPSRead(ChartDataRequest request)
    {
        return SimpleLoadStorageControllerData(request, "BytesPSRead", "BytesPSRead");
    }

    [WebMethod]
    public ChartDataResults GetStorageControllerBytesPSWrite(ChartDataRequest request)
    {
        return SimpleLoadStorageControllerData(request, "BytesPSWrite", "BytesPSWrite");
    }

    [WebMethod]
    public ChartDataResults GetStorageControllerBytesPSDistribution(ChartDataRequest request)
    {
        return SimpleLoadStorageControllerData(request, "BytesPSDistribution", "BytesPSDistribution");
    }

    private ChartDataResults SimpleLoadStorageArrayData(ChartDataRequest request, string property, string seriesTag)
    {

        string strSql = string.Format(@"SELECT Stats.DateTime, Stats.{0} 
                                        FROM SRM_StorageArrayStatistics AS Stats
                                        WHERE Stats.StorageArrayID = @NetObjectId AND Stats.DateTime <= @EndTime AND Stats.DateTime >= @StartTime 
                                        ORDER BY Stats.DateTime", 
                                        property);
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, String.Empty, DateTimeKind.Utc, seriesTag);
        
        return result;
    } 

    private ChartDataResults SimpleLoadStorageControllerData(ChartDataRequest request, string property, string seriesTag)
    {

        string strSql = string.Format(@"SELECT Stats.DateTime, Stats.{0} 
                                        FROM SRM_StorageControllerStatistics AS Stats
                                        WHERE Stats.StorageControllerID = @NetObjectId AND Stats.DateTime <= @EndTime AND Stats.DateTime >= @StartTime 
                                        ORDER BY Stats.DateTime", 
            property);
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, String.Empty, DateTimeKind.Utc, seriesTag);
        
        return result;
    } 

    private ChartDataResults SimpleLoadLunData(ChartDataRequest request, string property, string seriesTag)
    {

        string strSql = string.Format(@"SELECT Stats.DateTime, Stats.{0} 
                                        FROM SRM_LUNStatistics AS Stats
                                        WHERE Stats.LUNID = @NetObjectId AND Stats.DateTime <= @EndTime AND Stats.DateTime >= @StartTime 
                                        ORDER BY Stats.DateTime", 
            property);
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, String.Empty, DateTimeKind.Utc, seriesTag);
        
        return result;
    } 

    private ChartDataResults SimpleLoadPoolData(ChartDataRequest request, string property, string seriesTag)
    {

        string strSql = string.Format(@"SELECT Stats.DateTime, Stats.{0} 
                                        FROM SRM_PoolStatistics AS Stats
                                        WHERE Stats.PoolID = @NetObjectId AND Stats.DateTime <= @EndTime AND Stats.DateTime >= @StartTime 
                                        ORDER BY Stats.DateTime", 
            property);
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, String.Empty, DateTimeKind.Utc, seriesTag);
        
        return result;
    } 

    private ChartDataResults SimpleLoadVServerData(ChartDataRequest request, string property, string seriesTag)
    {

        string strSql = string.Format(@"SELECT Stats.DateTime, Stats.{0} 
                                        FROM SRM_VServerStatistics AS Stats
                                        WHERE Stats.VserverID = @NetObjectId AND Stats.DateTime <= @EndTime AND Stats.DateTime >= @StartTime 
                                        ORDER BY Stats.DateTime", 
            property);
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, String.Empty, DateTimeKind.Utc, seriesTag);
        
        return result;
    }

    private ChartDataResults SimpleLoadVolumeData(ChartDataRequest request, string property, string seriesTag)
    {

        string strSql = string.Format(@"SELECT Stats.DateTime, Stats.{0} 
                                        FROM SRM_VolumeStatistics AS Stats
                                        WHERE Stats.VolumeID = @NetObjectId AND Stats.DateTime <= @EndTime AND Stats.DateTime >= @StartTime 
                                        ORDER BY Stats.DateTime", 
            property);
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, String.Empty, DateTimeKind.Utc, seriesTag);
        
        return result;
    }
}
