﻿<%@ WebService Language="C#" Class="SRMCommonService" %>

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Linq;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.SRM.Common.Enums;
using System.ComponentModel;
using System.Data;
using SolarWinds.Logging;
using SolarWinds.SRM.Common.BL;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.EnrollmentWizard;
using SolarWinds.SRM.Web.Interfaces;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class SRMCommonService : WebService
{
    private static readonly Log log = new Log();

    [WebMethod()]
    public TestCredentialsResult STMIntegration_TestConnection(String hostName, String port, String userName, String password)
    {
        try
        {
            using (var proxy = BusinessLayerFactory.CreateMain())
            {
                var creds = new UsernamePasswordCredential()
                {
                    Username = userName.Trim(),
                    Password = password.Trim()
                };
                hostName = hostName.Trim();
                ushort portNumber;
                if (!ushort.TryParse(port.Trim(), out portNumber))
                {
                    portNumber = 443;
                }

                STMConnectionTestResult testResult = proxy.TestSTMConnection(hostName, portNumber, creds);
                return new TestCredentialsResult(testResult.GetLocalizedDescription(), testResult == STMConnectionTestResult.Success);
            }
        }
        catch (Exception ex)
        {
            log.Error("Exception occured during testing credential in STM integration", ex);
            return new TestCredentialsResult(STMConnectionTestResult.GenericFailure.GetLocalizedDescription(), false);
        }
    }
}

