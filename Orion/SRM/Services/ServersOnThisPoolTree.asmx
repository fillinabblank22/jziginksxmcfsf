﻿<%@ WebService Language="C#" Class="ServersOnThisPoolTree" %>

using System;
using System.Collections.Generic;
using System.Web.Script.Services;
using System.Web.Services;
using System.Linq;
using System.Globalization;
using SolarWinds.Orion.Web;
using SolarWinds.Shared;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.DAL;
using SolarWinds.SRM.Common.Enums;
using System.ComponentModel;
using System.Data;
using System.Web.Script.Serialization;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.Services;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ServersOnThisPoolTree : TreeFilterServiceBase, IAjaxTreeService
{
    [WebMethod(EnableSession = true)]
    ///<param name="resourceId">ID of resource with tree. This is used to get resource settings.</param>
    public IEnumerable<AjaxTreeNode> GetTreeNodes(int resourceId, string itemType, string itemValue, int fromIndex, int limit, string serializedFilters, AjaxTreeNode[] groupings)
    {
        Dictionary<string, string> filters = new JavaScriptSerializer().Deserialize(serializedFilters, typeof(Dictionary<string, string>)) as Dictionary<string, string>;

        ResourceInfo resource = ResourceManager.GetResourceByID(resourceId);
        Context.Items[typeof(ViewInfo).Name] = resource.View;

        string grouping = resource.Properties[ResourcePropertiesKeys.Grouping] ?? String.Empty;
        string orderBy = resource.Properties[ResourcePropertiesKeys.Ordering] ?? "Name";

        List<AjaxTreeNode> nodes = new List<AjaxTreeNode>();

        int totalCount = 0;

        if (String.IsNullOrEmpty(itemType))
        {
            TopologyDAL dal = new TopologyDAL();
            string filter = GetFilter(filters, AjaxTreeNodeType.Topology);

            // get pool ID
            int id = Int32.Parse(itemValue);
            
            // get Nodes by PoolID from SRM_Topology
            string poolNetObjectPrefix = NetObjectFactory.GetNetObjectPrefixBySWISType("Orion.SRM.Pools");
            string poolFilter = filter + string.Format(CultureInfo.InvariantCulture, " AND (NetObjectType = '{0}')", poolNetObjectPrefix);
            DataTable poolDt = dal.GetNodesByNetObjectId(id, orderBy, fromIndex, limit, poolFilter);
            poolDt.PrimaryKey = new DataColumn[] { poolDt.Columns["ID"] };

            // get Nodes by LUNID in Pool
            DataTable lunsDt = dal.GetNodesByLunsInPool(id, orderBy, fromIndex, limit, filter);
            lunsDt.PrimaryKey = new DataColumn[] { lunsDt.Columns["ID"] };

            // merge Pools Nodes with LUNs Nodes 
            poolDt.Merge(lunsDt);
            
            // get Nodes by NASVolumes in Pool
            DataTable volumesDt = dal.GetNodesByVolumesInPool(id, orderBy, fromIndex, limit, poolFilter); 
            volumesDt.PrimaryKey = new DataColumn[] { volumesDt.Columns["ID"] };

            // merge Pools Nodes with Volumes Nodes 
            poolDt.Merge(lunsDt);

            nodes.AddRange(CreateAjaxTreeNodesFromTable(poolDt, SwisEntities.Nodes, AjaxTreeNodeType.Topology.ToString(), resourceId.ToString(), filters, NodeWithLabelAndStatusHeaderConst));
            totalCount = dal.GetNodesByNetObjectIdCount(id, filter);
        }
        else if (itemType == AjaxTreeNodeType.Topology.ToString())
        {
            AjaxTreeNode node;
            if (GetTreeNodesCount(resourceId, AjaxTreeNodeType.ApplicationsDirectory.ToString(), itemValue, filters) > 0)
            {
                node = new AjaxTreeNode()
                {
                    Id = GetUniqueIdFromId(itemValue, AjaxTreeNodeType.ApplicationsDirectory.ToString()),
                    Name = Resources.SrmWebContent.Resource_ServersOnThisArray_ApplicationsConst,
                    Entity = null,
                    IsExpandable = true,
                    ItemType = AjaxTreeNodeType.ApplicationsDirectory.ToString(),
                    Description = null,
                    ClassName = NodeWithLabelConst
                };
                nodes.Add(node);
            }

            if (GetTreeNodesCount(resourceId, AjaxTreeNodeType.ServerVolumesDirectory.ToString(), itemValue, filters) > 0)
            {
                node = new AjaxTreeNode()
                {
                    Id = GetUniqueIdFromId(itemValue, AjaxTreeNodeType.ServerVolumesDirectory.ToString()),
                    Name = Resources.SrmWebContent.Resource_ServersOnThisArray_ServerVolumesConst,
                    Entity = null,
                    IsExpandable = true,
                    ItemType = AjaxTreeNodeType.ServerVolumesDirectory.ToString(),
                    Description = null,
                    ClassName = NodeWithLabelConst
                };
                nodes.Add(node);
            }
        }
        else if (itemType == AjaxTreeNodeType.ApplicationsDirectory.ToString())
        {
            TopologyDAL dal = new TopologyDAL();
            int? id = GetIdFromUniqueId(itemValue);
            if (id != null)
            {
                string filter = GetFilter(filters, AjaxTreeNodeType.ServerVolumesDirectory);
                DataTable dataTableLabels = dal.GetVolumesByNodeId((int)id, orderBy, fromIndex, limit, filter);
                List<int> volumeIDs = new List<int>();
                foreach (DataRow row in dataTableLabels.Rows) 
                {
                    volumeIDs.Add(Convert.ToInt32(row[0]));
                }

                // SQLSERVER Applications
                DataTable dataTableSQL = dal.GetSQLSERVERApplicationsByNodeIdAndVolumeId((int)id, volumeIDs, orderBy, fromIndex, limit, filter);
                
                // Exchange Applications
                DataTable dataTableExchange = dal.GetExchangeApplicationsByNodeIdAndVolumeID((int)id, volumeIDs, orderBy, fromIndex, limit, filter);

                //IIS Apps
                DataTable dataTableIIS = dal.GetIISApplicationsByNodeIdAndVolumeId((int)id, volumeIDs, orderBy, fromIndex, limit, filter);
                DataTable dataTableIISLogs = dal.GetIISApplicationsLogsByNodeIdAndVolumeId((int)id, volumeIDs, orderBy, fromIndex, limit, filter);

                dataTableSQL.PrimaryKey = new DataColumn[] { dataTableSQL.Columns["ID"] };

                dataTableSQL.Merge(dataTableExchange);
                dataTableSQL.Merge(dataTableIIS);
                dataTableSQL.Merge(dataTableIISLogs);
                
                nodes.AddRange(CreateAjaxTreeNodesFromTable(dataTableSQL, SwisEntities.Applications, AjaxTreeNodeType.Applications.ToString(), resourceId.ToString(), filters, NodeWithLabelAndStatusConst));
                totalCount = dataTableSQL.Rows.Count;
            }
        }
        else if (itemType == AjaxTreeNodeType.ServerVolumesDirectory.ToString())
        {
            TopologyDAL dal = new TopologyDAL();
            int? id = GetIdFromUniqueId(itemValue);
            if (id != null)
            {
                string filter = GetFilter(filters, AjaxTreeNodeType.ServerVolumesDirectory);
                DataTable dataTable = dal.GetVolumesByNodeId((int)id, orderBy, fromIndex, limit, filter);

                dataTable = RemoveVolumesRowsDuplicities(dataTable);
                dataTable = AddNetObjectNameToVolumes(dataTable);
                
                nodes.AddRange(CreateAjaxTreeNodesFromTable(dataTable, SwisEntities.ServerVolumes, AjaxTreeNodeType.ServerVolumes.ToString(), resourceId.ToString(), filters, NodeWithLabelAndStatusAndGraphConst));
                totalCount = dal.GetVolumesByNodeIdCount((int)id, filter);
            }
        }
       
        // If there are more nodes than "limit", we should add "Show more" item in tree. It is represented by empty node.
        if ((totalCount > 0) && (totalCount > fromIndex + limit))
        {
            var additionalCount = totalCount - (limit + fromIndex);
            AjaxTreeNode showMoreNode = new AjaxTreeNode()
            {
                Id = "EmptyNode",
                Name = (additionalCount).ToString(CultureInfo.InvariantCulture),
                ClassName = "SRM-HomeView-AllStorageObjects-NodeWithLabel"
            };

            return nodes.Concat(new[] { showMoreNode });
        }

        return nodes;
    }

    [WebMethod(EnableSession = false)]
    ///<param name="resourceId">ID of resource with tree. This is used to get resource settings.</param>
    protected override int GetTreeNodesCount(int resourceId, string itemType, string itemValue, Dictionary<string, string> filters)
    {
        List<AjaxTreeNode> nodes = new List<AjaxTreeNode>();

        int totalCount = 0;

        if (String.IsNullOrEmpty(itemType))
        {
            StorageArrayDAL dal = new StorageArrayDAL();
            string filter = GetFilter(filters, AjaxTreeNodeType.StorageArray);
            totalCount += dal.GetAllArraysCount(filter);
        }

        if (itemType == AjaxTreeNodeType.ApplicationsDirectory.ToString() || itemType == AjaxTreeNodeType.Topology.ToString())
        {
            IEnumerable<ModuleInfo> modules0 = OrionModuleManager.GetInstalledModules();
            
            IEnumerable<ModuleInfo> modules = OrionModuleManager.GetInstalledModules().Where(x => x.ProductShortName == "SAM");
            if (modules.Count() > 0)
            {
                TopologyDAL dal = new TopologyDAL();
                int? id = GetIdFromUniqueId(itemValue);
                if (id != null)
                {
                    string filter = GetFilter(filters, AjaxTreeNodeType.ServerVolumesDirectory);
                    totalCount += dal.GetApplicationsByNodeIdCount((int)id, filter);
                }
            }
        }

        if (itemType == AjaxTreeNodeType.ServerVolumesDirectory.ToString() || itemType == AjaxTreeNodeType.Topology.ToString())
        {
            TopologyDAL dal = new TopologyDAL();
            int? id = GetIdFromUniqueId(itemValue);
            if (id != null)
            {
                string filter = GetFilter(filters, AjaxTreeNodeType.ServerVolumesDirectory);
                totalCount += dal.GetVolumesByNodeIdCount((int)id, filter);
            }
        }

        return totalCount;
    }

    [WebMethod(EnableSession = false)]
    public bool GetShowGettingStarted(int resourceId, string filter)
    {
        StorageArrayDAL dal = new StorageArrayDAL();
        ResourceInfo resource = ResourceManager.GetResourceByID(resourceId);

        // check if number of transactions without filter is zero))
        return dal.GetIds().Count() == 0;
    }
}

