﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FileSharePopup.aspx.cs" Inherits="Orion_SRM_FileSharePopup" %>

<%@ Import Namespace="SolarWinds.Orion.Web.DisplayTypes" %>
<%@ Register TagPrefix="srm" TagName="PopupChart" Src="~/Orion/SRM/Controls/PopupChart.ascx" %>

<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>

<h3 class="Status<%=Enum.IsDefined(typeof (FileShareStatus), FileShares.FileSharesEntity.Status)?FileShares.FileSharesEntity.Status.ToString():FileShareStatus.Unknown.ToString()%>">
    <asp:literal runat="server" id="HeaderLabel" />
</h3>
<div class="NetObjectTipBody">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2">
                <asp:literal runat="server" id="StatusLabel" />
            </td>
        </tr>
         <tr runat="server" id ="VendorStatusRow">
                <th style="width: 110px; text-align: left; min-width: 110px;">
                    <%= SrmWebContent.FileSharesDetails_VendorStatus%>:
                </th>
                <td>
                    <asp:Literal runat="server" ID="VendorStatusLabel" />
                </td>
            </tr>
        <tr>
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.FileSharesDetails_Name%>:</th>
            <td>
                <asp:literal runat="server" id="NameLabel" />
            </td>
        </tr>
        <tr id="VolumeRow" runat="server">
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.FileSharesDetails_Volume%>:</th>
            <td>
                <asp:literal runat="server" id="VolumeLabel" />
            </td>
        </tr>
        <tr id="ArrayRow" runat="server">
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.FileSharesDetails_Array%>:</th>
            <td>
                <asp:literal runat="server" id="ArrayLabel" />
            </td>
        </tr>
        <tr id="VserverRow" runat="server">
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.FileSharesDetails_vServer%>:</th>
            <td>
                <asp:literal runat="server" id="VServerLabel" />
            </td>
        </tr>
        <tr id="ClusterRow" runat="server">
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.FileSharesDetails_Cluster%>:</th>
            <td>
                <asp:literal runat="server" id="ClusterLabel" />
            </td>
        </tr>
    </table>
    <hr />
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.FileSharesDetails_TotalIOPS%>:</th>
            <td>
                <asp:Label runat="server" id="TotalIopsLabel" />
            </td>
        </tr>
        <tr>
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.FileSharesDetails_TotalThroughput%>:</th>
            <td>
                <asp:Label runat="server" id="TotalThroughputLabel" />
            </td>
        </tr>
        <tr>
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.FileSharesDetails_TotalLatency%>:</th>
            <td>
                <asp:Label runat="server" id="TotalLatencyLabel" />
            </td>
        </tr>
    </table>
    <hr />
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.FileSharesDetails_VolumeCapacity%>:</th>
            <td>
                <asp:literal runat="server" id="VolumeCapacityLabel" />
            </td>
        </tr>
        <tr>
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.FileSharesDetails_QuotaCapacity%>:</th>
            <td>
                <asp:literal runat="server" id="QuotaCapacityLabel" />
            </td>
        </tr>
        <tr>
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.FileSharesDetails_UsedCapacity%>:</th>
            <td>
                <asp:label runat="server" id="UsedCapacityLabel" />
            </td>
        </tr>
        <tr>
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.FileSharesDetails_ProjectedRunOut%>:</th>
            <td>
                <asp:label runat="server" id="ProjectedRunOutLabel" />
            </td>
        </tr>
    </table>
    <hr style="margin-bottom: 2px; margin-top: 2px;" />
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th colspan="2"><%= SrmWebContent.FileSharesDetails_UserCapacity%></th>
        </tr>
        <tr>
            <td colspan="2">
                <srm:PopupChart ID="popupUserCapacity" runat="server" />
            </td>
        </tr>
    </table>
     <div id="OtherPotentialIssuesSection" runat="server">
        <hr style="margin-bottom: 2px; margin-top: 2px;"/>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.Popups_OtherIssues%>:</th>
                <td></td>
            </tr>
            <tr id="IOPSReadRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_ReadIOPS%>:</th>
                <td>
                    <asp:label runat="server" id="IOPSReadLabel" />
                </td>
            </tr>
            <tr id="IOPSWriteRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_WriteIOPS%>: </th>
                <td>
                    <asp:label runat="server" id="IOPSWriteLabel" />
                </td>
            </tr>
            <tr id="ThroughputReadRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_ReadThroughput%>: </th>
                <td>
                    <asp:label runat="server" id="ThroughputReadLabel" />
                </td>
            </tr>
            <tr id="ThroughputWriteRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_WriteThroughput%>: </th>
                <td>
                    <asp:label runat="server" id="ThroughputWriteLabel" />
                </td>
            </tr>
             <tr id="LatencyReadRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_ReadLatency%>: </th>
                <td>
                    <asp:label runat="server" id="LatencyReadLabel" />
                </td>
            </tr>
             <tr id="LatencyWriteRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_WriteLatency%>: </th>
                <td>
                    <asp:label runat="server" id="LatencyWriteLabel" />
                </td>
            </tr>
            <tr id="IOSizeTotalRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_TotalIOSize%>: </th>
                <td>
                    <asp:label runat="server" id="IOSizeTotalLabel" />
                </td>
            </tr>
             <tr id="IOSizeReadRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_ReadIOSize%>: </th>
                <td>
                    <asp:label runat="server" id="IOSizeReadLabel" />
                </td>
            </tr>
             <tr id="IOSizeWriteRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_WriteIOSize%>: </th>
                <td>
                    <asp:label runat="server" id="IOSizeWriteLabel" />
                </td>
            </tr>
        </table>
    </div>
</div>
