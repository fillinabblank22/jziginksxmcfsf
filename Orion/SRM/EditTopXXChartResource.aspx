﻿<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" 
    CodeFile="EditTopXXChartResource.aspx.cs" Inherits="Orion_Charts_EditTopXXChartResource" Title="-"  %>

<%@ Register TagPrefix="orion" TagName="TitleEdit" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadPlaceHolder" runat="Server">
    <orion:Include runat="server" File="EditGauge.css" />
    <orion:Include ID="Include5" runat="server" Module="SRM" File="SRM.css" />
    <orion:Include runat="server" File="ig_webGauge.js" />
    <orion:Include runat="server" File="ig_shared.js" />
    <orion:Include runat="server" File="GaugeScript.js" />
    <script type="text/javascript" language="javascript">
        var TitleTextBoxID = '<%=TitleEditControl.ResourceTitleID%>';
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>
        <%= Resources.CoreWebContent.WEBDATA_AK0_261 %> <asp:Label ID="ResourceName" runat="server" Text="-" CssClass="SRM_EditTopXXChartResource_ResourceNameLabel" /></h1>
    <table border="0" class="SRM_EditTopXXChartResource_Table" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <orion:TitleEdit runat="server" ID="TitleEditControl" />
            </td>
        </tr>
        <tr>
            <td>
                <%-- Placeholder for include EditTopXX.ascx edit control --%>
                <asp:PlaceHolder runat="server" ID="phResourceEditTopXXControl"></asp:PlaceHolder>
            </td>
        </tr>
        <tr>
            <td>
                <%-- Placeholder for include EditChart.ascx edit control --%>
                <asp:PlaceHolder runat="server" ID="phResourceEditControl"></asp:PlaceHolder>
            </td>
        </tr>
        <tr>
            <td>
                <div class="sw-btn-bar"><orion:LocalizableButton ID="btnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitClick"/></div>
            </td>
        </tr>
    </table>
</asp:Content>

