﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/OrionMasterPage.master" CodeFile="ProfilerIframe.aspx.cs" Inherits="Orion_SRM_ProfilerIframe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPlaceHolder" runat="server">
    <script type="text/javascript">
        $(function () {
            var resize = function () {
                $("#profilerFrame").height($(window).height() - $("#profilerFrame").offset().top - parseInt($("#content").css("padding-bottom"), 10));
            };
            $(window).resize(resize);
            resize();
        });
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="placeHolder" runat="server">
    <iframe id="profilerFrame" src="<%=url%>" name="text" width="100%" frameborder="0" allowtransparency="true" seamless="seamless"></iframe>
</asp:Content>
