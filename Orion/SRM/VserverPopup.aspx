﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VserverPopup.aspx.cs" Inherits="Orion_SRM_VserverPopup" %>

<%@ Register TagPrefix="srm" TagName="PopupChart" Src="~/Orion/SRM/Controls/PopupChart.ascx" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>

<h3 class="Status<%=Enum.IsDefined(typeof (VServerStatus), VServer.VServerEntity.Status)?VServer.VServerEntity.Status.ToString():VServerStatus.Unknown.ToString()%>">
    <%=SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(VServer.Name)%>
</h3>

<div class="NetObjectTipBody">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2">
                <asp:label runat="server" id="StatusLabel" />
            </td>
        </tr>
         <tr runat="server" id ="VendorStatusRow">
                <th style="width: 110px; text-align: left; min-width: 110px;">
                    <%= SrmWebContent.VserverDetails_VendorStatusCode%>:
                </th>
                <td>
                    <asp:Literal runat="server" ID="VendorStatusLabel" />
                </td>
            </tr>
        <tr id="ManufacturerRow" runat="server">
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Vserver_Manufacturer%>:</th>
            <td>
                <asp:literal runat="server" id="ManufacturerLabel" />
            </td>
        </tr>
        <tr  id="ModelRow" runat="server">
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Vserver_Model%>:</th>
            <td>
                <asp:literal runat="server" id="ModelLabel" />
            </td>
        </tr>
        <tr  id="IPaddressRow" runat="server">
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Vserver_IPaddress%>:</th>
            <td>
                <asp:literal runat="server" id="IPaddressLabel" />
            </td>
        </tr>
    </table>
    <hr />
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Vserver_AggregateIOPS%>:</th>
            <td>
                <asp:Label runat="server" id="AggIopsLabel" />
            </td>
        </tr>
        <tr>
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Vserver_AvgThroughput%>:</th>
            <td>
                <asp:Label runat="server" id="AvgThroughputLabel" />
            </td>
        </tr>
    </table>
    <hr />
     <table cellpadding="0" cellspacing="0">
        <tr>
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Vserver_NasCapacity%>:</th>
            <td>
                <asp:literal runat="server" id="NasCapacityLabel" />
            </td>
        </tr>
        <tr>
            <th style="min-width: 110px; text-align: left; width: 110px;"><%= SrmWebContent.Vserver_UsedCapacity%>:</th>
            <td>
                <asp:literal runat="server" id="UsedCapacityLabel" />
            </td>
        </tr>
    </table>
    <hr />
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><%= SrmWebContent.Vserver_AggregateNasVolumeCapacity%>:</th>
            <td></td>
        </tr>
        <tr>
            <td colspan="2">
                <srm:PopupChart ID="popupChart"  runat="server" />
            </td>
        </tr>
    </table>
     <div id="OtherPotentialIssuesSection" runat="server">
        <hr style="margin-bottom: 2px; margin-top: 2px;"/>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.Popups_OtherIssues%>:</th>
                <td></td>
            </tr>
            <tr id="IOPSReadRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_ReadIOPS%>:</th>
                <td>
                    <asp:label runat="server" id="IOPSReadLabel" />
                </td>
            </tr>
            <tr id="IOPSWriteRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_WriteIOPS%>: </th>
                <td>
                    <asp:label runat="server" id="IOPSWriteLabel" />
                </td>
            </tr>
            <tr id="IOPSOtherRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_OtherIOPS%>: </th>
                <td>
                    <asp:label runat="server" id="IOPSOtherLabel" />
                </td>
            </tr>
            <tr id="ThroughputReadRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_ReadThroughput%>: </th>
                <td>
                    <asp:label runat="server" id="ThroughputReadLabel" />
                </td>
            </tr>
            <tr id="ThroughputWriteRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_WriteThroughput%>: </th>
                <td>
                    <asp:label runat="server" id="ThroughputWriteLabel" />
                </td>
            </tr>
            <tr id="IOSizeTotalRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_TotalIOSize%>: </th>
                <td>
                    <asp:label runat="server" id="IOSizeTotalLabel" />
                </td>
            </tr>
             <tr id="IOSizeReadRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_ReadIOSize%>: </th>
                <td>
                    <asp:label runat="server" id="IOSizeReadLabel" />
                </td>
            </tr>
             <tr id="IOSizeWriteRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_WriteIOSize%>: </th>
                <td>
                    <asp:label runat="server" id="IOSizeWriteLabel" />
                </td>
            </tr>
        </table>
    </div>
</div>
