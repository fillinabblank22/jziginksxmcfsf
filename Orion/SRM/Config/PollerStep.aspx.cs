﻿using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Web.EnrollmentWizard;
using SolarWinds.SRM.Web.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_SRM_Config_PollerStep : WizardPage, IStep
{
    public string Step { get { return "PollerStep"; } }

    protected override bool ValidateUserInput()
    {
        List<IConfigurationControl> controls = GetControls<IConfigurationControl>();
        bool result = true;
        foreach (var configurationControl in controls)
        {
            result &= configurationControl.ValidateUserInput();
        }
        return result;
    }

    protected override void Next()
    {
        List<IConfigurationControl> controls = GetControls<IConfigurationControl>();
        foreach (var configurationControl in controls)
        {
            configurationControl.SaveValues();
        }

        Configuration config = WizardWorkflowHelper.ConfigurationInfo;
        log.DebugFormat(CultureInfo.InvariantCulture, "EnrollmentWizard - Selected poller: {0}:{1}, Poller ID: {3}", config.PollingConfig.ServerName, config.PollingConfig.BusinessLayerPort, config.PollingConfig.EngineID);
        
        var message = String.Format(CultureInfo.InvariantCulture, "OIP EW logging: PollerStep, Selected poller: {0}:{1}, Poller ID: {2}",
                      config.PollingConfig.ServerName, config.PollingConfig.BusinessLayerPort, config.PollingConfig.EngineID);
        
        using (var proxy = BusinessLayerFactory.CreateMain())
        {
            proxy.LogOIP(message);
        }
    }
}