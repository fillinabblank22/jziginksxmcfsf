﻿using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.EnrollmentWizard;
using SolarWinds.SRM.Web.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Script.Serialization;

public partial class Orion_SRM_Config_DeviceTypeStep : WizardPage, IStep
{
    public string Step { get { return "DeviceTypeStep"; } }

    protected string Groups { get; set; }

    private Configuration configuration = null;
    private string WhereIsMyDeviceLink = "OrionSRMAGaddarray";

    protected override void OnInit(EventArgs e)
    {
        configuration = WizardWorkflowHelper.ConfigurationInfo;
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Groups = "[]";
        if (!this.IsPostBack)
        {
            List<string> disabledEnrollmentGroups = new List<String>();
            if (!Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["IsGenericSMISEnabled"]))
                disabledEnrollmentGroups.Add("d81ec9bf-844a-47a1-84db-afb3368890f4");
            if (!Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["IsGenericOnboardSMISEnabled"]))
                disabledEnrollmentGroups.Add("d2ed22f4-af3e-4765-a8b3-0730be7469a5");
            if (!Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["IsDellCompelentLegacyEnabled"]))
                disabledEnrollmentGroups.Add(SRMConstants.DellCompelentSmisDeviceGroup.ToLower());

            IDeviceGroupDAL dal = ServiceLocator.GetInstance<IDeviceGroupDAL>();
            List<DeviceGroup> groups = dal.GetDeviceGroups().Where(x => !disabledEnrollmentGroups.Contains(x.GroupId.ToString())).ToList();

            whereLink.HRef = HelpHelper.GetHelpUrl(WhereIsMyDeviceLink);

            foreach (var group in groups)
            {
                if (group.LinkURL != null && !group.LinkURL.StartsWith("http", StringComparison.InvariantCultureIgnoreCase))
                {
                    group.LinkURL = HelpHelper.GetHelpUrl(group.LinkURL);
                }
            }
            this.Groups = new JavaScriptSerializer().Serialize(groups.ToDictionary(g => g.GroupId.ToString(), g=>g));

            foreach (var deviceGroup in groups)
            {
                bool isRecommended = false;
                string property;
                if (deviceGroup.Properties != null && deviceGroup.Properties.TryGetValue("isRecommended", out property))
                {
                    if (!bool.TryParse(property, out isRecommended))
                    {
                        isRecommended = false;
                    }
                }

                if (isRecommended)
                {
                    deviceGroup.Name += InvariantString.Format(" {0}", SrmWebContent.DeviceType_Recommended);
                }
            }

            bool isResponderEnabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["IsResponderEnabled"]);
            if (isResponderEnabled)
            {
                groups.Add(new DeviceGroup() { Name = "Responder", FlowType = FlowType.Responder });
            }
            groups = groups.OrderBy(o => o.Name).ToList();

            dropDownDeviceGroup.Width = 330;
            dropDownDeviceGroup.DataSource = groups;
            dropDownDeviceGroup.DataTextField = "Name";
            dropDownDeviceGroup.DataValueField = "GroupId";
            dropDownDeviceGroup.DataBind();
            dropDownDeviceGroup.Attributes.Add("onChange", "return OnSelectedIndexChange(this.options[this.selectedIndex].value);");
            dropDownDeviceGroup.SelectedValue = groups[0].GroupId.ToString();
        }
    }

    protected override bool ValidateUserInput()
    {
        Guid value;
        return Guid.TryParse(dropDownDeviceGroup.SelectedValue, out value);
    }

    protected override void Next()
    {
        Guid value;

        if (Guid.TryParse(dropDownDeviceGroup.SelectedValue, out value))
        {
            if (value == Guid.Empty)
            {
                configuration.DeviceGroup = new DeviceGroup() { Name = "Responder", FlowType = FlowType.Responder };
            }
            else
            {
                IDeviceGroupDAL dal = ServiceLocator.GetInstance<IDeviceGroupDAL>();
                configuration.DeviceGroup = dal.GetDeviceGroupDetails(value);
            }
        }

        configuration.ProviderConfiguration.AvailableProviderEntities = null;
        configuration.ProviderConfiguration.SelectedProviders.Clear();

        log.DebugFormat(CultureInfo.InvariantCulture, "EnrollmentWizard - Selected device group: {0}", configuration.DeviceGroup);

        using (var proxy = BusinessLayerFactory.CreateMain())
        {
            var message = String.Format(CultureInfo.InvariantCulture, "OIP EW logging: DeviceTypeStep, Selected device group: {0}", configuration.DeviceGroup);
            proxy.LogOIP(message);
        }
    }
}