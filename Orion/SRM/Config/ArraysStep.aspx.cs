﻿using System.Globalization;
using System.Linq;
using System.Text;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Web.EnrollmentWizard;
using System;
using Resources;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.BL;

public partial class Orion_SRM_Config_ArraysStep : WizardPage, IStep
{
    public string Step { get { return "ArraysStep"; } }

    protected bool IsExternalProvider
    {
        get
        {
            return configuration.DeviceGroup.FlowType == FlowType.Smis ||
                   configuration.DeviceGroup.FlowType == FlowType.SmisEmcUnified ||
                   configuration.DeviceGroup.FlowType == FlowType.NetAppDfm ||
                   configuration.DeviceGroup.FlowType == FlowType.XtremIO;
        }
    }

    private Configuration configuration = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        configuration = WizardWorkflowHelper.ConfigurationInfo;
        if (!this.IsPostBack)
        {
            var builder = new StringBuilder();
            foreach (var array in configuration.ProviderConfiguration.DiscoveryProvider.Arrays)
            {
                if (array.IsSelected)
                {
                    builder.Append("{");
                    builder.Append(array.UniqueArrayId);
                    builder.Append("}");
                }
            }
            SelectedArrayIDs.Value = builder.ToString();
        }
    }

    protected override bool ValidateUserInput()
    {
        if (!IsExternalProvider)
        {
            SelectedArrayIDs.Value = String.Empty;
            foreach (var array in configuration.ProviderConfiguration.DiscoveryProvider.Arrays)
            {
                if (!array.IsAlreadyManaged)
                {
                    SelectedArrayIDs.Value += "{" + array.UniqueArrayId + "}";
                }
            }
        }
              
        bool selection = !string.IsNullOrEmpty(SelectedArrayIDs.Value);
        bool license = licenseBar.Validation();

        foreach (var array in configuration.ProviderConfiguration.DiscoveryProvider.Arrays)
        {
            array.IsSelected = SelectedArrayIDs.Value.Contains(InvariantString.Format("{{{0}}}", array.UniqueArrayId));
        }

        if (!selection)
        {
            string message;
            if ( configuration.ProviderConfiguration.DiscoveryProvider.Arrays.Any() && (!IsExternalProvider || configuration.ProviderConfiguration.DiscoveryProvider.Arrays.Any(a => a.IsSelected)))
            {
                message = String.Format(CultureInfo.CurrentCulture, "Ext.Msg.alert('{0}', '{1}');",
                    /* {0} */ SrmWebContent.Array_Step_Server_Validation_ArrayAlreadyManaged_Title,
                    /* {1} */ SrmWebContent.Array_Step_Server_Validation_ArrayAlreadyManaged_Message);
            }
            else
            {
                message = String.Format(CultureInfo.CurrentCulture, "Ext.Msg.alert('{0}', '{1}');",
                    /* {0} */ SrmWebContent.Array_Step_Server_Validation_Title,
                    /* {1} */ SrmWebContent.Array_Step_Server_Validation_NoArraySelected_Message);
            }

            this.ClientScript.RegisterStartupScript(this.GetType(), "SelectionValidationMessage", message, true);
        }
        else if (!license)
        {
            string message = String.Format(CultureInfo.CurrentCulture, "Ext.Msg.alert('{0}', '{1}');",
                /* {0} */ SrmWebContent.Array_Step_Server_Validation_Title,
                /* {1} */ SrmWebContent.Array_Step_Server_Validation_Licensing_Message);

            this.ClientScript.RegisterStartupScript(this.GetType(), "LicenseValidationMessage", message, true);
        }

        return selection && license;
    }

    protected override void Next()
    {
        StringBuilder oipMessages = new StringBuilder();
        string selectedArrays = SelectedArrayIDs.Value;
        foreach (var array in configuration.ProviderConfiguration.DiscoveryProvider.Arrays)
        {
            array.IsSelected = 
                selectedArrays.Contains(string.Format(CultureInfo.InvariantCulture,"{{{0}}}", array.UniqueArrayId));

            log.DebugFormat(CultureInfo.InvariantCulture, "Array selection with ID: {0}, Name: {1}, Serial Number: {2} is: {3}", 
                            array.ID, array.Name, array.SerialNumber, array.IsSelected);

            oipMessages.Append(String.Format(CultureInfo.InvariantCulture, 
                "OIP EW logging: ArrayStep,  Array selection with ID: {0}, Name: {1}, Serial Number: {2} is: {3}",
                array.ID, array.Name, array.SerialNumber, array.IsSelected));
            oipMessages.AppendLine();
        }

        using (var proxy = BusinessLayerFactory.CreateMain())
        {
            proxy.LogOIP(oipMessages.ToString());
        }
    }
}
