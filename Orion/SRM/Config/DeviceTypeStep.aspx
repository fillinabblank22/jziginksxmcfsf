﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DeviceTypeStep.aspx.cs" Inherits="Orion_SRM_Config_DeviceTypeStep" MasterPageFile="~/Orion/SRM/Config/EnrollmentWizardPage.master" Title="<%$ Resources: SrmWebContent, AddStorage_Title %>" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>



<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <div class="srm-EnrollmentWizardPage_master_AdminContent">
        
        <span class="ActionName"><%= Resources.SrmWebContent.AddStorage_DeviceTypeStep_Title %></span><br />
        <span><%= Resources.SrmWebContent.AddStorage_DeviceTypeStep_SubTitle %></span>
        <br />

        <div class="srm-enrollment-wizard-content">
            <table>
            <tbody>
                <tr>
                    <td>
                        <%= Resources.SrmWebContent.AddStorage_DeviceTypeStep_ChooseArrayType %>:
                    </td>
                    <td> 
                        <asp:DropDownList runat="server" ID="dropDownDeviceGroup" ClientIDMode="Static"/>
                        <span><%= SrmWebContent.DeviceTypeStep_WhereIsMyDevice_Text%> </span>
                            <a id ="whereLink" runat="server" rel="noopener noreferrer" target="_blank"><%= SrmWebContent.DeviceTypeStep_WhereIsMyDevice_LinkText%></a>
                       
                        <table id="srm-devicetype-info">
                        <tbody>
                            <tr>
                                <td>
                                    <img src="/Orion/SRM/images/hint_icon_u43.png" />
                                </td>
                                <td>
                                    <div>
                                        <span id="FamilyTypeName" class="bigfont uppercase"></span>
                                        <span id="FamilyTypeDescription"></span>
                                        <span><a id="FamilyTypeLink" rel="noopener noreferrer" target="_blank"></a></span>
                                    </div>
                                    <div class="familyimage">
                                        <img id="FamilyTypeImg" src=""/>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
            </table>
        </div>
    </div>

    <table class="NavigationButtons">
        <tr>
            <td>
                <div class="sw-btn-bar-wizard">
                    <orion:LocalizableButton ID="NextButton" runat="server" DisplayType="Primary"
                        LocalizedText="Next" OnClick="imgbNext_Click" />
                    <orion:LocalizableButton ID="CancelButton" runat="server" DisplayType="Secondary"
                        LocalizedText="Cancel" OnClick="imgbCancel_Click" CausesValidation="false" />
                </div>
            </td>
        </tr>
    </table>

    <script>
        var groups = <%= this.Groups %>;

        function OnSelectedIndexChange(val) {
            var strDescription = '';
            var strTipPicture = '';
            var strLinkName = '';
            var strLinkURL = '';
            var strName = '';

            $("#FamilyTypeLink").show();
            $("#FamilyTypeImg").show();

            if (groups[val] != null) {
                strDescription = groups[val].Description;
                strTipPicture = groups[val].TipPicture;
                strLinkName = groups[val].LinkName;
                strLinkURL = groups[val].LinkURL;
                strName = groups[val].Name;
            } else {
                $("#FamilyTypeLink").hide();
                $("#FamilyTypeImg").hide();
            }
            $("#FamilyTypeName").text(strName);
            $("#FamilyTypeDescription").text(strDescription);
            $("#FamilyTypeLink").text(strLinkName);
            $("#FamilyTypeLink").attr("href", strLinkURL);
            $("#FamilyTypeImg").attr("src", strTipPicture);
        }

        $(document).ready(function () {
            OnSelectedIndexChange($("#dropDownDeviceGroup").val());
        });
    </script>
</asp:Content>


