﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProviderStep.aspx.cs" Inherits="Orion_SRM_Config_ProviderStep" MasterPageFile="~/Orion/SRM/Config/EnrollmentWizardPage.master" Title="<%$ Resources: SrmWebContent, AddStorage_Title %>" %>

<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Reference Control="~/Orion/SRM/Config/Controls/NetAppProviderStep.ascx" %>
<%@ Reference Control="~/Orion/SRM/Config/Controls/ProviderStep.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
<orion:Include ID="Include3" runat="server" File="SRM.Formatters.js" Module="SRM" />
<orion:Include runat="server" File="SRM.ProviderChangeManager.js" Module="SRM" />
    
    <script>
        var onClickNextButtonEvent = null;

        function providerStepSubmit() {
            __doPostBack('<%= NextButton.UniqueID %>', '');
        }

        function onNextButtonClick() {
            if (onClickNextButtonEvent) {
                onClickNextButtonEvent();
            }
            else {
                providerStepSubmit();
            }
        }
    </script>

    <div class="srm-EnrollmentWizardPage_master_AdminContent">
        <span class="ActionName"><%= Resources.SrmWebContent.AddStorage_ProviderStep_Title %></span><br />
        <asp:PlaceHolder ID="contentBoxPlaceHolder" runat="server"/>
    </div>

    <table class="NavigationButtons">
        <tr>
            <td>
                <div class="sw-btn-bar-wizard">
                    <orion:LocalizableButton ID="BackButton" runat="server" DisplayType="Secondary"
                        LocalizedText="Back" OnClick="imgbBack_Click" CausesValidation="false" />
                    <orion:LocalizableButton ID="NextButton" runat="server" DisplayType="Primary" Visible="false"
                        LocalizedText="Next" OnClick="imgbNext_Click" />
                    <a class="sw-btn-primary sw-btn sw-btn-primary sw-btn" href="javascript:onNextButtonClick()">
                        <span class="sw-btn-c">
                            <span class="sw-btn-t"><%= Resources.CoreWebContent.WEBDATA_VB0_260 %></span>
                        </span>
                    </a>
                    <orion:LocalizableButton ID="CancelButton" runat="server" DisplayType="Secondary"
                        LocalizedText="Cancel" OnClick="imgbCancel_Click" CausesValidation="false" />
                </div>
            </td>
        </tr>
    </table>

</asp:Content>
