﻿using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common.Helpers;
using SolarWinds.SRM.Web.EnrollmentWizard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.BL;
using System.Globalization;
using Resources;
using SolarWinds.SRM.Common;

public partial class Orion_SRM_Config_ProviderStep : WizardPage, IStep
{
    public string Step { get { return "ProviderStep"; } }

    private Configuration configuration;

    private const string ExternalProviderControl = "~/Orion/SRM/Config/Controls/ProviderStep.ascx";

    protected void Page_Load(object sender, EventArgs e)
    {
        Control control;
        configuration = WizardWorkflowHelper.ConfigurationInfo;

        if (configuration.ProviderConfiguration.SelectedProviders != null)
        {
            configuration.ProviderConfiguration.SelectedProviders.Clear();
        }

        switch (configuration.DeviceGroup.FlowType)
        {
            case FlowType.GenericSnmp:
                control = LoadControl("~/Orion/SRM/Config/Controls/GenericSnmpProviderStep.ascx");
                break;
            case FlowType.NetAppFiler:
            case FlowType.EmcUnity:
            case FlowType.InfiniBox:
                control = LoadControl("~/Orion/SRM/Config/Controls/NetAppProviderStep.ascx");
                break;
            case FlowType.SmisVnx:
            case FlowType.SmisMsa:
            case FlowType.SmisPureStorage:
                control = LoadControl("~/Orion/SRM/Config/Controls/CelerraProviderStep.ascx");
                break;
            case FlowType.Responder:
                control = LoadControl("~/Orion/SRM/Config/Controls/ResponderProviderStep.ascx");
                break;
            case FlowType.Smis:
                control = LoadControl(this.configuration.DeviceGroup.ProviderLocationOrOnboard == ProviderLocation.External
                    ? ExternalProviderControl 
                    : "~/Orion/SRM/Config/Controls/EmbeddedSmisProvider.ascx");
                break;
            case FlowType.SmisEmcUnified:
            case FlowType.NetAppDfm:
            case FlowType.XtremIO:
            case FlowType.SmisAndGenericHttpExternal:
                control = LoadControl(ExternalProviderControl);
                break;
            case FlowType.SnmpRestIsilon:
                control = LoadControl("~/Orion/SRM/Config/Controls/IsilonProviderStep.ascx");
                break;
            case FlowType.GenericRest:
                control = LoadControl("~/Orion/SRM/Config/Controls/GenericRestProviderStep.ascx");
                break;
            default:
                throw new NotSupportedException(InvariantString.Format("Please specify steps for this FlowType: {0}", configuration.DeviceGroup.FlowType.ToString()));
        }
        contentBoxPlaceHolder.Controls.Add(control);
    }

    protected override bool ValidateUserInput()
    {
        string message = string.Empty;
        bool result = true;
        foreach (var configurationControl in GetControls<IConfigurationControl>())
        {
            result &= configurationControl.ValidateUserInput();
            if (String.IsNullOrEmpty(message))
            {
                message = configurationControl.ValidationError;
            }
        }

        if (!String.IsNullOrEmpty(message))
        {
            string script = String.Format(CultureInfo.CurrentCulture, "Ext.Msg.alert('{0}', '{1}');",
                /* {0} */ SrmWebContent.Provider_Step_Server_Validation_Title,
                /* {1} */ message.Replace("'", "\\'"));

            this.ClientScript.RegisterStartupScript(this.GetType(), "ValidationMessage", script, true);
        }

        return result;
    }

    protected override void Next()
    {
        foreach (var configurationControl in GetControls<IConfigurationControl>())
        {
            configurationControl.SaveValues();
        }

        var providersInfoText = String.Join("\r\nProvider: ", configuration.ProviderConfiguration.SelectedProviders.Select(p => InvariantString.Format("IP Address: {0}, Credential Name: {1}", p.IPAddress, p.CredentialName)));
        log.DebugFormat(CultureInfo.InvariantCulture, "Discovery: Selected Providers...\r\nProvider: {0}", providersInfoText);

        using (var proxy = BusinessLayerFactory.CreateMain())
        {
            proxy.LogOIP(String.Format(CultureInfo.InvariantCulture, "OIP EW logging: ProviderStep, Discovery: Selected Providers...\r\nProvider: {0}", providersInfoText));
        }

        using (var proxy = BusinessLayerFactory.Create(configuration.PollingConfig.ServerName, configuration.PollingConfig.BusinessLayerPort, true))
        {
            configuration.ProviderConfiguration.DiscoveryId = Guid.Empty;
            foreach (var selectedProvider in configuration.ProviderConfiguration.SelectedProviders)
            {
                //if the provider is got from database, credential object can be empty, so load it here
                if (selectedProvider.ID.HasValue && selectedProvider.Credential == null)
                {
                    selectedProvider.Credential = proxy.GetCredential(selectedProvider.CredentialID, selectedProvider.CredentialType);
                }
            }

            List<ConnectionInfo> selectedProviders = configuration.ProviderConfiguration.SelectedProviders.Where(p => p.Arrays.Count == 0)
                                                                                                          .Select(p => new ConnectionInfo(p))
                                                                                                          .ToList();
            if (selectedProviders.Count != 0)
            {
                log.Debug("Submitting discovery job into the Business Layer");
                configuration.ProviderConfiguration.DiscoveryId = proxy.SubmitDiscoveryJob(selectedProviders, new[]{ configuration.DeviceGroup.GroupId});
            }
        }
    }
}