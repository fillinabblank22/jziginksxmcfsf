﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SummaryStep.aspx.cs" Inherits="Orion_SRM_Config_SummaryStep" MasterPageFile="~/Orion/SRM/Config/EnrollmentWizardPage.master" Title="<%$ Resources: SrmWebContent, AddStorage_Title %>" %>

<%@ Register Src="~/Orion/SRM/Config/Controls/LicenseBar.ascx" TagPrefix="srm" TagName="LicenseBar" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Helpers" %>
<%@ Import Namespace="SolarWinds.SRM.Web.EnrollmentWizard" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <div class="srm-EnrollmentWizardPage_master_AdminContent">
        <orion:Include ID="Include1" runat="server" File="OrionCore.js" />
        <script type="text/javascript">
            function importDiscovery() {
                ORION.callWebService("/Orion/SRM/Services/EnrollmentWizardService.asmx", "ImportDiscovery", {}, function (result) {
                    var ret = JSON.parse(result);
                    if (ret.errorMessage == null || ret.errorMessage.length == 0) {
                        Ext.Msg.alert('<%= Resources.SrmWebContent.AddStorage_SummaryStep_Title %>', '<%= Resources.SrmWebContent.Summary_Step_ImportSuccess_MessageBox %>', function(btn, text){
                            window.location = ret.url;
                        });
                    } else {
                        Ext.Msg.alert('<%= Resources.SrmWebContent.AddStorage_SummaryStep_Title %>', '<%= Resources.SrmWebContent.Summary_Step_ImportFailed_MessageBox %><br/><br/>' + ret.errorMessage);
                    }
                });
            };
        </script>

        <span class="ActionName"><%= Resources.SrmWebContent.AddStorage_SummaryStep_Title %></span><br />
        <span><%= SubTitle %></span>
        <br />

        <div class="srm-enrollment-wizard-content">
            <table class="srm-summarystep-maintable">
                <tr>
                    <td id="srm-configuration-controls-first-column"><%= Resources.SrmWebContent.Summary_Step_Array_Type%>:</td>
                    <td><b>
                        <asp:Label ID="labelArrayType" runat="server" /></b></td>
                </tr>
                <tr>
                    <td id="srm-configuration-controls-first-column2"><%= Resources.SrmWebContent.Summary_Step_Provider%>:</td>
                    <td>
                        <asp:Repeater ID="ProviderRepeater" runat="server">
                            <ItemTemplate>
                                <div><b><%# Container.DataItem %></b></div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
                <tr>
                    <td id="srm-configuration-controls-first-column3"><%= Resources.SrmWebContent.Summary_Step_Arrays%>:</td>
                    <td>
                        <asp:Repeater ID="SummaryStepRepeater" runat="server">
                            <HeaderTemplate>
                                <table class="srm-summarystep-innertable">
                                    <tr>
                                        <th><%= Resources.SrmWebContent.Summary_Step_Array_Header%></th>
                                        <th><%= Resources.SrmWebContent.Summary_Step_Array_Type_Header%></th>
                                        <th><%= Resources.SrmWebContent.Summary_Step_Array_Disks_Header%></th>
                                    </tr>
                            </HeaderTemplate>

                            <ItemTemplate>
                                <tr>
                                    <td><%# Eval("Name") %></td>
                                    <td><%= WizardWorkflowHelper.ConfigurationInfo.DeviceGroup.Name %></td>
                                    <td><%# Eval("Disks") %></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
                <tr>
                    <td id="srm-configuration-controls-first-column4"><%= Resources.SrmWebContent.Summary_Step_Licensing%>:</td>
                    <td>
                        <srm:LicenseBar runat="server" ID="licenseBar" />
                    </td>
                </tr>
                <tr>
                    <td id="srm-configuration-controls-first-column5"><%= Resources.SrmWebContent.Summary_Step_Poller%>:</td>
                    <td><b>
                        <asp:Label ID="labelPollerName" runat="server" /></b></td>
                </tr>
            </table>
        </div>
    </div>

    <table class="NavigationButtons">
        <tr>
            <td>
                <div class="sw-btn-bar-wizard">
                    <orion:LocalizableButton ID="BackButton" runat="server" DisplayType="Secondary"
                        LocalizedText="Back" OnClick="imgbBack_Click" CausesValidation="false" />
                    <a class="sw-btn-primary sw-btn sw-btn-primary sw-btn" href="javascript:importDiscovery()" automation="Import">
                        <span class="sw-btn-c">
                            <span class="sw-btn-t"><%= Resources.SrmWebContent.Summary_Step_Finish_Button_Text %></span>
                        </span>
                    </a>
                    <orion:LocalizableButton ID="CancelButton" runat="server" DisplayType="Secondary"
                        LocalizedText="Cancel" OnClick="imgbCancel_Click" CausesValidation="false" />
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
