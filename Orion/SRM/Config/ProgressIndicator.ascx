<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProgressIndicator.ascx.cs"
    Inherits="Orion_SRM_Config_ProgressIndicator" %>
    
<orion:Include runat="server" File="ProgressIndicator.css" />

<div class="ProgressIndicator" id="srm-ProgressIndicator">
    <asp:PlaceHolder ID="phPluginImages" runat="server"></asp:PlaceHolder>
</div>
