﻿using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.NPM.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web.EnrollmentWizard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_SRM_EnrollmentWizardPage : System.Web.UI.MasterPage
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        CheckRights();

        WizardWorkflowHelper.LoadWizardSteps(new ConfigWizardStepsManager());
        this.ProgressIndicator1.Reload();

        AdminSiteMapRenderer.OverrideBreadcrumbURL = "~/Orion/SRM/Admin/Default.aspx";
    }

    // <summary> Check user rights. If they are not sufficient, then redirects to error page. </summary>
    private void CheckRights()
    {
        if (!Profile.AllowNodeManagement)
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_AK0_17), Title = Resources.CoreWebContent.WEBDATA_VB0_567 });
        if (OrionConfiguration.IsDemoServer)
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.SrmWebContent.DemoModeMessage), Title = Resources.CoreWebContent.WEBDATA_VB0_567 });               
    }

    protected void OrionSiteMapPath_OnInit(object sender, EventArgs e)
    {
        if (!CommonWebHelper.IsBreadcrumbsDisabled)
        {
            string viewID = (Request["ViewID"] != null) ? Request["ViewID"].ToString() : null;
            string returnTo = (Request["ReturnTo"] != null) ? Request["ReturnTo"].ToString() : null;
            string accountID = (Request["AccountID"] != null) ? Request["AccountID"].ToString() : null;
            var renderer = new SolarWinds.Orion.NPM.Web.UI.AdminSiteMapRenderer();
            renderer.SetUpData(new KeyValuePair<string, string>("ViewID", viewID), new KeyValuePair<string, string>("ReturnTo", returnTo), new KeyValuePair<string, string>("AccountID", accountID));
            OrionSiteMapPath.SetUpRenderer(renderer);
        }
    }
}
