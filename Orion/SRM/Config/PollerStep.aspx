﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PollerStep.aspx.cs" Inherits="Orion_SRM_Config_PollerStep" MasterPageFile="~/Orion/SRM/Config/EnrollmentWizardPage.master" Title="<%$ Resources: SrmWebContent, AddStorage_Title %>" %>

<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>

<%@ Register Src="~/Orion/SRM/Config/Controls/ChoosePollerControl.ascx" TagPrefix="srm" TagName="ChoosePollerControl" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <script type="text/javascript">
        var onClickNextButtonEvent = null;

        function stepSubmit() {
            __doPostBack('<%= NextButton.UniqueID %>', '');
        }

        function onNextButtonClick() {
            if (onClickNextButtonEvent) {
                onClickNextButtonEvent();
            }
            else {
                stepSubmit();
            }
        }
    </script>

    <div class="srm-EnrollmentWizardPage_master_AdminContent">
        <span class="ActionName"><%= Resources.SrmWebContent.AddStorage_PollerStep_Title %></span><br />
        <%= Resources.SrmWebContent.AddStorage_PollerStep_SubTitle %>

        <div class="srm-enrollment-wizard-content">
            <srm:ChoosePollerControl runat="server" ID="choosePollerControl" Visible="true" />
        </div>
    </div>

    <table class="NavigationButtons">
        <tr>
            <td>
                <div class="sw-btn-bar-wizard">
                    <orion:LocalizableButton ID="BackButton" runat="server" DisplayType="Secondary"
                        LocalizedText="Back" OnClick="imgbBack_Click" CausesValidation="false" />
                    <orion:LocalizableButton ID="NextButton" runat="server" DisplayType="Primary" Visible="false"
                        LocalizedText="Next" OnClick="imgbNext_Click" />
                    <a class="sw-btn-primary sw-btn sw-btn-primary sw-btn" href="javascript:onNextButtonClick()">
                        <span class="sw-btn-c">
                            <span class="sw-btn-t"><%= Resources.CoreWebContent.WEBDATA_VB0_260 %></span>
                        </span>
                    </a>
                    <orion:LocalizableButton ID="CancelButton" runat="server" DisplayType="Secondary"
                        LocalizedText="Cancel" OnClick="imgbCancel_Click" CausesValidation="false" />
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
