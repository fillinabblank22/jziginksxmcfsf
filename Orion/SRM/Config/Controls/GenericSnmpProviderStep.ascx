﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GenericSnmpProviderStep.ascx.cs" Inherits="Orion_SRM_Config_Controls_GenericSnmpProviderStep" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common" %>
<%@ Register Src="~/Orion/SRM/Config/Controls/SnmpVersionOneCredentialsControl.ascx" TagPrefix="srm" TagName="SnmpVersionOneCredentials" %>
<%@ Register Src="~/Orion/SRM/Config/Controls/SnmpVersionThreeCredentialsControl.ascx" TagPrefix="srm" TagName="SnmpVersionThreeCredentials" %>
<%@ Register Src="~/Orion/SRM/Config/Controls/TestConnectionMessages.ascx" TagPrefix="UIInfo" TagName="TestConnectionMessages" %>
<%@ Register TagPrefix="UIInfo" TagName="DeviceType" Src="~/Orion/SRM/Config/Controls/DeviceType.ascx" %>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include ID="Include" runat="server" File="SRM/Config/Controls/DiscoveryValidation.js" />
<orion:Include runat="server" File="Discovery.css" Module="SRM" />

<span><%= SubTitle%></span>
<div class="srm-enrollment-wizard-content">
    <div class="srm-main-content PollingPropertiesSection">
        <table class="srm-EnrollmentWizard-providerStepContentTable">
            <tr>
                <td>
                    <table class="srm-EnrollmentWizard-providerStepInputsTable">
                        <tr>
                            <td><%= SrmWebContent.Provider_Step_Array_Type%>:</td>
                            <td>
                                <b>
                                    <asp:Label runat="server" ID="ArrayTypeLabel" />
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td><%= SrmWebContent.Provider_Step_Array_Ip%>*:</td>
                            <td>
                                <asp:TextBox runat="server" ID="IpAddressTextBox" ClientIDMode="Static" />
                            </td>
                        </tr>
                        <tr ID="snmpVersionRow" runat="server">
                            <td><%= SrmWebContent.Provider_Step_Snmp_Version%>*:</td>
                            <td>
                                <asp:DropDownList runat="server" ID="SnmpVersionDropDown" ClientIDMode="Static" onchange="return OnSelectedIndexChange(this.options[this.selectedIndex].value);" />
                            </td>
                        </tr>
                    </table>

                    <asp:Panel runat="server" ID="SnmpVersionOneCredentialsPanel" ClientIDMode="Static" class="credentialsPanelProviderStep">
                        <srm:SnmpVersionOneCredentials runat="server" ID="SnmpVersionOneCredentials" />
                    </asp:Panel>
                    <asp:Panel runat="server" ID="SnmpVersionThreeCredentialsPanel" ClientIDMode="Static" class="credentialsPanelProviderStep">
                        <srm:SnmpVersionThreeCredentials runat="server" ID="SnmpVersionThreeCredentials" IsContextRequired="False" />
                    </asp:Panel>

                    <table class="srm-EnrollmentWizard-providerStepInputsTable">
                        <tr>
                            <td/>
                            <td>
                                <input type="button" id="TestConnectionButton" class="providerStepButton" value="<%= SrmWebContent.Provider_Step_Test_Connection %>" onclick="OnTestButtonClick(false);" />
                            </td>
                        </tr>
                    </table>
                    <UIInfo:TestConnectionMessages runat="server" ID="TestConnectionMessages" JsCancellationFunction="javascript:OnCancelClick();"/>
                </td>
                <td>
                    <UIInfo:DeviceType runat="server" ID="deviceInfo"></UIInfo:DeviceType>
                </td>
            </tr>
        </table>
    </div>
</div>

<asp:HiddenField runat="server" ID="SelectedCredential" ClientIDMode="Static" />

<script type="text/javascript">
    var dellProvider = SW.Core.namespace("SW.SRM.EditProperties.Dell");

    onClickNextButtonEvent = function () {
        var val = $('#SnmpVersionDropDown').val();
        switch (val) {
            case '<%=SnmpV3%>':
                OnTestV3Credentials(true, false);
                break;
            default:
                OnTestV1Credentials(true, false);
                break;
        }
    };

    function OnCredentialSelected(ddlId, fn) {
        var ddl = $('#' + ddlId);
        var ddlText = $(ddl).find(':selected').val();
        if (ddlText != "<%= SRMConstants.NewCredential %>") {
            fn(ddl);
        }
    }

    function OnSelectedIndexChange(val) {
        HideMessageBoxes();
        switch (val) {
            case '<%=SnmpV3%>':
                OnCredentialSelected('SnmpCredentialDropDownSnmpV3', OnSelectedCredentialChangeSnmpV3);
                $('#SnmpVersionOneCredentialsPanel').hide();
                $('#SnmpVersionThreeCredentialsPanel').show();
                $('#SelectedCredential').val('V3');
                break;
            default:
                OnCredentialSelected('SnmpCredentialDropDown', OnSelectedCredentialChange);
                $('#SnmpVersionOneCredentialsPanel').show();
                $('#SnmpVersionThreeCredentialsPanel').hide();
                $('#SelectedCredential').val('V1');
                break;
        }
    }
    $(document).ready(function () {
        $(".srm-testing-connection-msgbox").hide();
        var val = $('#SnmpVersionDropDown').val();
        OnSelectedIndexChange(val);
    });

    function OnSelectedCredentialChange(ddl) {
        HideMessageBoxes();
        $('#SnmpCommunityTextBox').val('');
        $('#SnmpPortTextBoxV2').val('');

        if (ddl.value != "<%= SRMConstants.NewCredential %>") {
            $('#StoredCredFiledsV1').hide();
        } else {
            $('#StoredCredFiledsV1').show();
        }
    }

    function OnSelectedCredentialChangeSnmpV3(ddl) {
        HideMessageBoxes();
        $('#PasswordKey1TextBox').val('');
        $('#PasswordKey2TextBox').val('');
        $('#SnmpContextTextBox').val('');
        $('#SnmpPortTextBoxV3').val('');
        $('#SnmpUserNameTextBox').val('');
        $('#SnmpV3CredentialNameTextBox').val('');
        $('#PasswordKey1CheckBox').attr('checked', false);
        $('#PasswordKey2CheckBox').attr('checked', false);

        if (ddl.value != "<%= SRMConstants.NewCredential %>") {
            $('#StoredCredFiledsV3').hide();
        } else {
            $('#StoredCredFiledsV3').show();
        }
    }

    function OnTestButtonClick(runDiscovery) {
        var val = $('#SnmpVersionDropDown').val();
        switch (val) {
        case '<%=SnmpV3%>':
            OnTestV3Credentials(false, runDiscovery);
            break;
        default:
            OnTestV1Credentials(false, runDiscovery);
            break;
        }
    }

    dellProvider.ValidateCredentials = function () {
        var testCredentialDeferred = $.Deferred();
        SW.SRM.DiscoveryValidation.init("<%= ArrayID %>", "<%= ArraySerialNumber %>", "<%= DeviceGroupId %>", request, testCredentialDeferred, "<%= SrmWebContent.Provider_Step_Unexpected_Error%>", ShowFailMessageBox);
        var globalDiscoveryPromise = $.Deferred();
        OnTestButtonClick(true);

        $.when(testCredentialDeferred).then(function (x) {
            return globalDiscoveryPromise.resolve();

        }, function (x) {
            return globalDiscoveryPromise.reject();
        });

        return globalDiscoveryPromise.promise();
    }

    dellProvider.MonitoredControlIds = ['IpAddressTextBox', 'SnmpVersionDropDown', 'SnmpCredentialDropDown', 'SnmpCommunityTextBox', 'SnmpCredentialDropDownSnmpV3', 'SnmpUserNameTextBox', 'SnmpContextTextBox'];
    SW.SRM.EditProperties.ProviderChangeManager.InitProvider(dellProvider, dellProvider.ValidateCredentials);

    var request;

    function OnTestV1Credentials(isRequiredPostBack, runDiscovery) {
        $('#TestConnectionButton').attr("disabled", true);
        var ip = $.trim(String($("#IpAddressTextBox").val()));
        var credentialName = $("#CredentialNameTextBox").val();
        var communityString = $("#SnmpCommunityTextBox").val();
        var credentialID = $('#SnmpCredentialDropDown').val();
        var port = $("#SnmpPortTextBoxV2").val();

        if (!ValidateFormV1(ip, credentialName, communityString, port)) {
            $('#TestConnectionButton').attr("disabled", false);
            SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
            return;
        }

        HideMessageBoxes();

        $("#testingConnectionMsg").show();
        request = SW.SRM.Common.CallSrmWebService('/Orion/SRM/Services/EnrollmentWizardService.asmx', 'TestSnmpV1Credentials',
            {
                credentialID: credentialID, credentialName: credentialName, communityString: communityString, ip: ip, port: port, saveProvider: runDiscovery === true,  groupId: "<%= DeviceGroupId %>"
            },
            function (result) {
                $("#testingConnectionMsg").hide();
                $('#TestConnectionButton').attr("disabled", false);
                if (result.IsSuccess) {
                    ShowPassMessageBox(result.Message);
                    if (isRequiredPostBack) {
                        providerStepSubmit();
                    }
                    if (runDiscovery)
                        SW.SRM.DiscoveryValidation.startDiscovery();

                } else {
                    ShowFailMessageBox(result.Message, result.ErrorMessage);
                    SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
                }
            },
            function () {
                $("#testingConnectionMsg").hide();
                $('#TestConnectionButton').attr("disabled", false);

                if (request.status !== 0) {
                    ShowFailMessageBox("<%= SrmWebContent.Provider_Step_Unexpected_Error%>");
                }
                SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
            });
    }

    function OnTestV3Credentials(isRequiredPostBack, runDiscovery) {
        $('#TestConnectionButton').attr("disabled", true);
        var ip = $.trim(String($("#IpAddressTextBox").val()));
        var deviceGroupId = "<%= DeviceGroupId %>";

        ValidateV3Controls(ip, deviceGroupId, isRequiredPostBack, runDiscovery);
    }

    function OnCancelClick() {
        $("#testingConnectionMsg").hide();
        if (request) {
            request.abort();
        }
    }

    function HideMessageBoxes() {
        $("#testConnectionPassMsg").hide();
        $("#testConnectionFailMsg").hide();
    }

    function ShowPassMessageBox(text) {
        $("#passMessageContent").empty();
        $("#passMessageContent").append(text);
        $("#testConnectionPassMsg").show();
    }

    function ShowFailMessageBox(message, errorMessage) {
        var ip = $("#IpAddressTextBox").val();
        var connErrorMsg = "<%= Resources.SrmWebContent.Provider_Step_Provider_Connecting_Error %>:";

        var title = connErrorMsg.concat(" ").concat(ip);
        SW.SRM.Common.ShowError(title, message, errorMessage);
    }

    function selectCredentials(ddl, credentialName) {
        ddl.find('option:contains("' + credentialName + '")').attr("selected", true);
        ddl.trigger('change');
    }

    function focusCredentialsDisplayName(txtBox) {
        txtBox.focus();
    };

    function comboHasText(ddl, text) {
        var exists = false;
        ddl.find('option').each(function () {
            if ($(this).text().toLowerCase() == text.toLowerCase()) {
                exists = true;
                return false;
            }
        });
        return exists;
    }

    function ValidateFormV1(ipAddress, credentialName, communityString, port) {
        var title = "<%= Resources.SrmWebContent.ProviderStep_Validation_Error %>";

        if (!ipAddress.trim()) {
            SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.ProviderStep_ProviderIpAddress_Required_Field_Validator %>");
            return false;
        }

        if ($("#SnmpCredentialDropDown").val().trim() == "<%= SRMConstants.NewCredential %>") {
            if (!credentialName.trim()) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.ProviderStep_ProviderCredentialNameStr_Required_Field_Validator %>");
                return false;
            }

            if (!communityString.trim()) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.ProviderStep_ProviderCommunityStr_Required_Field_Validator %>");
                return false;
            }

            if (!port.trim()) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.ProviderStep_ProviderPortStr_Required_Field_Validator %>");
                return false;
            }

            var alreadyExists = comboHasText($('#SnmpCredentialDropDown'), credentialName);
            if (alreadyExists) {
                title = "<%= SrmWebContent.Add_Provider_CredentialNameAlreadyExistHeader %>";
                var message = "<a href='javascript:selectCredentials($(&quot;#SnmpCredentialDropDown&quot;), &quot;" + credentialName + "&quot;)'><%= SrmWebContent.Add_Provider_CredentialNameAlreadyExist_LoadSaved %></a> or <a href='javascript:focusCredentialsDisplayName($(&quot;#SnmpCommunityTextBox&quot;))'><%= SrmWebContent.Add_Provider_CredentialNameAlreadyExist_RenameDuplicate %></a>";
                SW.SRM.Common.ShowError(title, message);
                return false;
            }
        }

        return true;
    }

</script>
