﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.EnrollmentWizard;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;

public partial class Orion_SRM_Config_Controls_ResponderProviderStep : UserControl, IConfigurationControl
{
    private Configuration configuration = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        configuration = WizardWorkflowHelper.ConfigurationInfo;
        if (!IsPostBack)
        {
            ArrayTypeLabel.Text = configuration.DeviceGroup.Name;
            IpAddressTextBox.Text = SRMConstants.ResponderDefaultHost;
            PortTextBox.Text = SRMConstants.ResponderPort.ToString(CultureInfo.InvariantCulture);

            var dal = ServiceLocator.GetInstance<ITemplateDAL>();
            IEnumerable<TemplateEntity> templates = dal.GetDeviceTypes();
            var templateEntities = templates.Where(template => !string.IsNullOrEmpty(template.DeviceType)).OrderBy(o => o.DeviceType);

            var deviceTypeDatasource = new Dictionary<string, string>();
            foreach (var template in templateEntities)
            {
                if (deviceTypeDatasource.ContainsKey(template.DeviceType))
                {
                    deviceTypeDatasource.Add(string.Format(CultureInfo.InvariantCulture, "{0} ({1})", template.DeviceType, template.TemplateId), template.TemplateId.ToString());
                }
                else
                {
                    deviceTypeDatasource.Add(template.DeviceType, template.TemplateId.ToString());
                }
            }

            DeviceTypeDropDown.DataSource = deviceTypeDatasource;
            DeviceTypeDropDown.DataTextField = "Key";
            DeviceTypeDropDown.DataValueField = "Value";
            DeviceTypeDropDown.DataBind();
        }
    }

    public string ValidationError
    {
        get { return string.Empty; }
    }

    public void SaveValues()
    {
        Response.Redirect("~/Orion/SRM/Summary.aspx");
    }

    public bool ValidateUserInput()
    {
        return true;
    }
}