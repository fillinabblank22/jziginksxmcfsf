﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddDfmProvider.ascx.cs" Inherits="Orion_SRM_Config_Controls_AddDfmProvider" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Register TagPrefix="UIInfo" TagName="DeviceType" Src="~/Orion/SRM/Config/Controls/DeviceType.ascx" %>
<%@ Register Src="~/Orion/SRM/Config/Controls/TestConnectionMessages.ascx" TagPrefix="UIInfo" TagName="TestConnectionMessages" %>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="SRM/Config/Controls/AddDfmProvider.js" />
<orion:Include runat="server" File="SRM/Config/Controls/SRM.Expander.js" />

<style type="text/css">
    .srm-main-content {
        padding: 10px;
    }
    .srm-main-content table {
        width: 100%;
        table-layout: fixed;
    }
    .srm-main-content td {
        vertical-align: top;
        padding: 0px 10px 3px 0px;
    }
    .srm-main-content td span {
        white-space: nowrap;
    }
    .srm-main-content > table > tbody > tr > td > table > tbody > tr > td:first-child {
        width: 200px;
    }

    #ProtocolCheckBox {
        width: 15px;
        height: 15px;
        padding: 0px;
        vertical-align: middle;
    }

    #HttpsPortTextBox, #HttpPortTextBox {
        width: 75px;
    }

    #expanderElement {
        padding: 0px 0px 10px 0px;
    }

    #expanderElement a {
        vertical-align: middle;
        color: black;
        text-decoration: none;
    }

    #expanderElement a:hover {
        color: black;        
        text-decoration: none;
    }

    #expanderElement img {
        vertical-align: middle;
        padding-right: 10px;
    }
    #imgDfm {
        width: 530px;
    }
</style>

<script type="text/javascript">
    $(function () {
        SW.SRM.AddDfmProvider.init('<%= (int)CredentialType.NetAppDfm %>',
                                   '<%= SrmWebContent.Provider_Step_Provider_Connecting_Error %>',
                                   '<%  = SrmWebContent.Provider_Step_Unexpected_Error %>',
                                   '<%= SrmWebContent.Provider_Step_New_Credential %>');
        expanderChangedHandler();
    });
    
</script>
<asp:HiddenField runat="server" ID="txtProviderNoDataAvailable" ClientIDMode="Static"/>
<div class="srm-main-content">
    <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="label"><span><span id="labelIpAddress"><%= SrmWebContent.Add_Provider_IP_Address_Hostname%></span>*:</span></td>
                        <td>
                            <span><input type="text" id="IpAddressOrHostnameTextBox" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><%= SrmWebContent.Add_Provider_Choose_Credential%>*:</span></td>
                        <td>
                            <span><select id="ChooseCredentialDdl" onchange="ChooseCredentialDdl_OnChange(this)" /></span>
                        </td>
                    </tr>
                </table>
                <table id="StoredCredFiledsTable">
                    <tr>
                        <td class="label"><span><span id="labelDisplayName"><%= SrmWebContent.Add_Provider_DisplayName %></span>*:</span></td>
                        <td>
                            <span><input type="text" id="DisplayNameTextBox" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><span id="labelProviderUserName"><%= SrmWebContent.Add_Provider_ProviderUsername %></span>*:</span></td>
                        <td>
                            <span><input type="text" id="ProviderUsernameTextBox" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><span id="labelProviderPassword"><%= SrmWebContent.Add_Provider_Password%></span>*:</span></td>
                        <td>
                            <span><input type="password" id="ProviderPasswordTextBox" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><%= SrmWebContent.Add_Provider_ConfirmPassword%>*:</span></td>
                        <td>
                            <span><input type="password" id="ProviderConfirmPasswordTextBox" /></span>
                        </td>
                    </tr>
                </table>
                <div id="expanderElement"><a href="javascript:expanderChangedHandler();"><img src="/Orion/SRM/images/Button.Expand.gif" alt="[+]" /><%= SrmWebContent.Provider_Step_AddProvider_Advanced %></a></div>
                <table id="AdvancedCredFiledsTable">
                    <tr>
                        <td class="label"><span><%= SrmWebContent.Add_Provider_Protocol%></span></td>
                        <td>
                            <span><input type="checkbox" id="ProtocolCheckBox" /><%= SrmWebContent.Provider_Step_UseHttp_Label %></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><span id="labelHttpsPort"><%= SrmWebContent.Add_Provider_Https_Port%></span>*:</span></td>
                        <td>
                            <span><input type="text" id="HttpsPortTextBox" runat="server" clientidmode="Static" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><span id="labelHttpPort"><%= SrmWebContent.Add_Provider_Http_Port%></span>*:</span></td>
                        <td>
                            <span><input type="text" id="HttpPortTextBox" runat="server" clientidmode="Static" /></span>
                        </td>
                    </tr>
                </table>
                <input type="button" id="testConnectionButton" class="providerStepButton" value="<%= SrmWebContent.Provider_Step_Test_Connection %>" onclick="SW.SRM.AddDfmProvider.testCredential();" />
                
                <UIInfo:TestConnectionMessages runat="server" ID="TestConnectionMessages" JsCancellationFunction="javascript:SW.SRM.AddDfmProvider.cancelTestCredential();" />
                
            </td>
            <td id="imgDfm">
                <UIInfo:DeviceType runat="server" ID="deviceInfo"></UIInfo:DeviceType>
            </td>
        </tr>
    </table>
</div>
