﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="XtremIoEdit.ascx.cs" Inherits="Orion_SRM_Config_Controls_XtremIoEdit" %>
<%@ Register TagPrefix="srm" TagName="PollingProperties" Src="~/Orion/SRM/Controls/PollingProperties.ascx" %>
<%@ Register TagPrefix="UIInfo" TagName="DeviceType" Src="~/Orion/SRM/Config/Controls/DeviceType.ascx" %>
<%@ Register TagPrefix="UIInfo" TagName="TestConnectionMessages" Src="~/Orion/SRM/Config/Controls/TestConnectionMessages.ascx" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web" %>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="SRM/Config/Controls/DiscoveryValidation.js" ID="Include" />
<orion:Include runat="server" File="SRM.EditProperties.XtremIoProvider.js" Module="SRM" />

<orion:Include runat="server" File="SRM.EditProperties.SmisProvider.css" Module="SRM" />
<orion:Include runat="server" File="SRM.css" Module="SRM" />
<orion:Include runat="server" File="Discovery.css" Module="SRM" />

<asp:PlaceHolder runat="server" ID="hfPlaceholder"></asp:PlaceHolder>
<div class="srm-main-content smisprovider">
    <span><%= SrmWebContent.EditProperties_XtremIo_EditArray_PollingProperties %></span>
    <table>
        <tr>
            <td class="col1">
                <table>
                    <tr>
                        <td class="label"><span><span id="labelArrayType">Array Type</span>:</span></td>
                        <td>
                            <asp:Label ID="txtArrayType" runat="server" CssClass="boldText" />
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <span><span id="labelProvider"><%= SrmWebContent.XtremIo_XMS %></span>:</span>
                        </td>
                        <td>
                            <img class="StatusIcon" src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.Providers%>&status=<%=(int)(XtremIoProvider.Status)%>&size=small" />
                            <asp:HyperLink ID="hlProvider" runat="server" /><br/>
                            <input type="button" id="testConnectionXtremIoButton" class="providerStepButton" value="<%= SrmWebContent.Provider_Step_Test_Connection %>" onclick="SW.SRM.EditProperties.XtremIoProvider.ValidateConnection();" />
                        </td>
                    </tr>
                </table>
                <UIInfo:TestConnectionMessages runat="server" ID="TestConnectionMessages" JsCancellationFunction="javascript:SW.SRM.EditProperties.XtremIoProvider.onCancelClick();"/>
                <srm:PollingProperties runat="server" ID="PollingProperties" />
            </td>
            <td>
               <UIInfo:DeviceType runat="server" ID="deviceInfo"></UIInfo:DeviceType>
            </td>
        </tr>
    </table>
</div>
