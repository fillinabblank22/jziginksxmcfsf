﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.Credentials;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.EnrollmentWizard;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.Resources;

public partial class Orion_SRM_Config_Controls_GenericRestProviderStep : UserControl, IConfigurationControl, ICreateEditControl, IEditControl
{
    private static readonly IDeviceGroupDAL DeviceGroupDal = ServiceLocator.GetInstance<IDeviceGroupDAL>();

    private Configuration configuration
    {
        get
        {
            return WizardWorkflowHelper.ConfigurationInfo;
        }
    }

    protected string ArrayID
    {
        get
        {
            return array == null ? string.Empty : array.ID;
        }
    }

    protected string ArraySerialNumber
    {
        get
        {
            return array == null ? string.Empty : array.SerialNumber;
        }
    }

    protected Guid DeviceGroupId
    {
        get
        {
            return configuration.DeviceGroup.GroupId;
        }
    }

    // keys to group's properties
    private const string ApiCredentialsTooltipKey = "ApiCredentialsTooltip";
    private const string ChooseDataSourceSubtitleKey = "ChooseDataSourceSubtitle";
    private const string HttpsPortKey = "HttpsPort";

    private ArrayEntity array;

    private List<ProviderEntity> providers;
    private readonly List<KeyValuePair<string, string>> warnings = new List<KeyValuePair<string, string>>();
    private static readonly Log log = new Log();

    private string SelectedGenericHttpCredentials { get; set; }

    public string SubTitle { get; set; }

    public string ValidationError { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.ArrayTypeName.Text = configuration.DeviceGroup.Name;
            deviceInfo.InitWithDevice(configuration.DeviceGroup);

            // Generic Http
            labelGenericHttpDisplayNameTextBox.Text = SrmWebContent.Add_Provider_DisplayName;
            labelGenericHttpProviderUserName.Text = SrmWebContent.Add_Provider_ArrayAPIUsername;
            labelGenericHttpProviderPassword.Text = SrmWebContent.Add_Provider_ArrayAPIPassword;

            //HttpPortTextBox.Text = SRMConstants.DefaultIsilonPort.ToString();

            HttpPortTextBox.Text = configuration.DeviceGroup.Properties[HttpsPortKey];

            if (configuration.DeviceGroup.Properties.ContainsKey(ApiCredentialsTooltipKey))
            {
                ApiCredentialsTooltip.Visible = true;
                ApiCredentialsTooltip.ToolTip = configuration.DeviceGroup.Properties[ApiCredentialsTooltipKey];
            }

            if (configuration.DeviceGroup.Properties.ContainsKey(ChooseDataSourceSubtitleKey))
            {
                SubTitle = configuration.DeviceGroup.Properties[ChooseDataSourceSubtitleKey];
            }

            EnrollmentWizardProvider providerEntity = null;
            if (configuration.ProviderConfiguration.SelectedProviders.Count > 0)
            {
                providerEntity = configuration.ProviderConfiguration.SelectedProviders[0];
            }

            if (providerEntity != null)
            {
                IpAddressTextBox.Text = providerEntity.IPAddress;

            }

            // Generic Http
            FillCredentials(this.ChooseCredentialGenericHttp, CredentialType.GenericHttp, this.SelectedGenericHttpCredentials);
        }
    }

    public KeyValuePair<string, string>[] GetWarnings()
    {
        return this.warnings.ToArray();
    }

    public void Update(bool connectionPropertyChanged)
    {
        SaveValues();

        var toDiscovery = new List<SelectedArrayEntity>
                              {
                                  new SelectedArrayEntity(array)
                                    {
                                        IsSelected = true,
                                        StorageArrayId = this.array.StorageArrayId
                                    }
                              };

        using (var proxy = BusinessLayerFactory.Create())
        {
            proxy.DiscoveryImport(array.EngineID, ProviderHelper.GetProviderInfoOfWizardProviders(this.providers, configuration.ProviderConfiguration.SelectedProviders), toDiscovery);
        }
    }

    public void SaveValues()
    {
        SaveGenericHttpProvider(this.IpAddressTextBox.Text.Trim(),
                                this.DisplayNameTextBox.Text,
                                this.ArrayApiUsernameTextBox.Text,
                                this.ArrayApiPasswordTextBox.Text,
                                Int32.Parse(this.HttpPortTextBox.Text),
                                ProtocolCheckBox.Checked,
                                this.ChooseCredentialGenericHttp.SelectedValue);
    }

    private bool HandleValidationError(string message)
    {
        this.ValidationError = message;
        this.ClearSelectedProviders();
        return false;
    }

    public bool ValidateUserInput()
    {
        if (String.IsNullOrEmpty(IpAddressTextBox.Text))
        {
            return HandleValidationError(String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, SrmWebContent.Provider_Step_Array_Ip));
        }

        if (ChooseCredentialGenericHttp.SelectedIndex == 0 && String.IsNullOrEmpty(DisplayNameTextBox.Text))
        {
            return HandleValidationError(String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, labelGenericHttpDisplayNameTextBox.Text));
        }
        else if (ChooseCredentialGenericHttp.SelectedIndex == 0 && String.IsNullOrEmpty(ArrayApiUsernameTextBox.Text))
        {
            return HandleValidationError(String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, labelGenericHttpProviderUserName.Text));
        }
        else if (ChooseCredentialGenericHttp.SelectedIndex == 0 && String.IsNullOrEmpty(ArrayApiPasswordTextBox.Text))
        {
            return HandleValidationError(String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, labelGenericHttpProviderPassword.Text));
        }
        else if (ChooseCredentialGenericHttp.SelectedIndex == 0 && ArrayApiPasswordTextBox.Text != ArrayApiConfirmPasswordTextBox.Text)
        {
            return HandleValidationError(SrmWebContent.Add_Provider_ConfirmApiPassword_CompareField_Validator);
        }

        if (ChooseCredentialGenericHttp.SelectedIndex == 0 && this.ChooseCredentialGenericHttp.Items.Cast<ListItem>().Any(item => this.DisplayNameTextBox.Text.Equals(item.Text, StringComparison.InvariantCultureIgnoreCase)))
        {
            return HandleValidationError(SrmWebContent.Provider_Step_Celerra_Validation_DuplicateAPICredentials);
        }

        return true;
    }

    private void FillCredentials(DropDownList dropDownList, CredentialType type, string selectedCredentials)
    {
        var items = new List<KeyValuePair<String, String>>() { new KeyValuePair<String, String>(SRMConstants.NewCredential, SrmWebContent.Provider_Step_New_Credential) };
        using (var proxy = BusinessLayerFactory.Create())
        {
            IDictionary<int, string> credentialsList = proxy.GetCredentialNames(type);
            foreach (KeyValuePair<int, string> cred in credentialsList)
            {
                items.Add(new KeyValuePair<String, String>(cred.Key.ToString(CultureInfo.InvariantCulture), cred.Value));
            }
        }

        dropDownList.DataTextField = "Value";
        dropDownList.DataValueField = "Key";
        dropDownList.DataSource = items;

        if (items.Any(pair => pair.Key == selectedCredentials))
        {
            dropDownList.SelectedValue = selectedCredentials;
        }

        dropDownList.DataBind();
    }


    public void SaveGenericHttpProvider(string ipAddress,
                                string displayName,
                                string userName,
                                string password,
                                int port,
                                bool useHttp,
                                string credentialIDstring)
    {
        GenericHttpCredential apiCredential;

        if (credentialIDstring != SRMConstants.NewCredential)
        {
            int credentialID = -1;
            Int32.TryParse(credentialIDstring, out credentialID);
            using (var proxy = BusinessLayerFactory.Create())
            {
                apiCredential = (GenericHttpCredential)proxy.GetCredential(credentialID, CredentialType.GenericHttp);
            }
        }
        else
        {
            apiCredential = new GenericHttpCredential()
            {
                Name = displayName,
                Username = userName,
                Password = password,
                Port = port,
                UseSsl = !useHttp
            };

            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(apiCredential, CredentialType.GenericHttp);
                apiCredential.ID = id;
            }
        }

        var providerEntity = new EnrollmentWizardProvider
        {
            ProviderType = configuration?.DeviceGroup?.GetProviderTypeOrDefault(ProviderTypeEnum.Unified) ?? ProviderTypeEnum.Unified,
            ProviderLocation = configuration?.DeviceGroup?.GetProviderLocationOrDefault(ProviderLocation.Onboard) ?? ProviderLocation.Onboard,
            IPAddress = IpAddressTextBox.Text.Trim()
        };

        providerEntity.CredentialID = apiCredential.ID.Value;
        providerEntity.CredentialType = CredentialType.GenericHttp;
        providerEntity.Credential = apiCredential;

        UpdateProviders(providerEntity);
    }

    private void UpdateProviders(EnrollmentWizardProvider newProvider, IList<CredentialType> credentialTypes = null)
    {
        configuration.ProviderConfiguration.SelectedProviders.RemoveAll(p => p.ProviderType == newProvider.ProviderType &&
            (credentialTypes != null ? credentialTypes.Contains(p.CredentialType) : p.CredentialType == newProvider.CredentialType));
        configuration.ProviderConfiguration.SelectedProviders.Add(newProvider);
    }

    public IEditControl InitFromNetObjects(List<string> listOfNetObjectIDs)
    {
        // Supports just one NetObject ID and just SMSA
        var supported = listOfNetObjectIDs.Count() == 1 && listOfNetObjectIDs[0].Split(':')[0] == StorageArray.NetObjectPrefix;
        if (!supported)
        {
            return null;
        }

        var arrayId = int.Parse(listOfNetObjectIDs[0].Split(':')[1]);
        array = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayDetails(arrayId);

        // Supporting just Generic REST
        if (array.FlowType != FlowType.GenericRest)
            return null;

        FillControl();

        return this;
    }

    private void FillControl()
    {
        this.providers = ServiceLocator.GetInstance<IStorageProviderDAL>().GetAllProvidersByArrayId(array.StorageArrayId);

        var genericHttpProvider = this.providers.FirstOrDefault(p => p.CredentialType == CredentialType.GenericHttp);

        if (genericHttpProvider != null)
        {
            if (string.IsNullOrEmpty(IpAddressTextBox.Text))
            IpAddressTextBox.Text = genericHttpProvider.IPAddress;
            SelectedGenericHttpCredentials = genericHttpProvider.CredentialID.ToString();

            GenericHttpCredential credentials;
            using (var proxy = BusinessLayerFactory.Create())
            {
                credentials = (GenericHttpCredential)proxy.GetCredential(genericHttpProvider.CredentialID, CredentialType.GenericHttp);
            }

            if (credentials != null)
            {
                genericHttpProvider.Credential = credentials;
                HttpPortTextBox.Text = credentials.Port.ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                //Display warning about missing credentials
                this.warnings.Add(new KeyValuePair<string, string>("", String.Format(SrmWebContent.Credentials_Missing_Format, SrmWebContent.Provider_Step_Isilon_APICredentials)));

                // Credentials are missing. User should enter new valid credentials
                log.InfoFormat("Credentials with ID {0} are missing. User is being asked to enter new credentials for provider {1}", genericHttpProvider.CredentialID, genericHttpProvider.Name);
            }
        }

        RefillConfiguration();
    }

    private void RefillConfiguration()
    {
        var engine = ServiceLocator.GetInstance<IEngineDAL>().GetEngine(array.EngineID);

        configuration.DeviceGroup = DeviceGroupDal.GetDeviceGroupDetailsByArrayId(array.StorageArrayId.Value);
        configuration.PollingConfig.BusinessLayerPort = engine.BusinessLayerPort;
        configuration.PollingConfig.EngineID = engine.EngineID;
        configuration.PollingConfig.ServerName = engine.ServerName;
        configuration.ProviderConfiguration.AvailableProviderEntities = null;
        if (!IsPostBack)
        {
            ClearSelectedProviders();
        }
    }

    private void ClearSelectedProviders()
    {
        configuration.ProviderConfiguration.SelectedProviders.RemoveAll(a => true);
    }

    public string ValidateJSFunction()
    {
        return "SW.SRM.EditProperties.GenericHttp.InitValidation();";
    }

    public bool Validate(bool connectionPropertyChanged)
    {
        var shouldValidate = !AllFieldsEmpty();

        //If we don't want to validate this settings just skip the validation -> return true
        if (!shouldValidate)
        {
            return true;
        }

        return this.ValidateUserInput();
    }

    private bool AllFieldsEmpty()
    {
        var listOfTextboxes = new List<TextBox>()
                                  {
                                      this.IpAddressTextBox,
                                      this.DisplayNameTextBox,
                                      this.ArrayApiUsernameTextBox,
                                      this.ArrayApiPasswordTextBox,
                                      this.ArrayApiConfirmPasswordTextBox,
                                      this.HttpPortTextBox
                                  };

        var listOfDropDowns = new List<DropDownList>() { this.ChooseCredentialGenericHttp };

        return listOfTextboxes.All(tb => string.IsNullOrEmpty(tb.Text))
               && listOfDropDowns.All(dd => dd.SelectedIndex == 0);
    }

}
