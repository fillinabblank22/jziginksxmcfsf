﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.EnrollmentWizard;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;

public partial class Orion_SRM_Config_Controls_ExternalProvider : UserControl, ICreateEditControl, IEditControl
{
    protected string providerLabel = string.Empty;
    protected string arrayType = string.Empty;
    protected readonly List<KeyValuePair<string, string>> Warnings = new List<KeyValuePair<string, string>>();
    public DeviceGroup DeviceGroup => WizardWorkflowHelper.ConfigurationInfo.DeviceGroup;
    protected ArrayEntity array;
    protected ProviderEntity restProvider;
    protected ProviderEntity smisProvider;
    private string HiddenFieldSmisConfig = "HiddenFieldSmisConfig";
    private string HiddenFieldRestConfig = "HiddenFieldRestConfig";
    private string HiddenFieldArrayConfig = "HiddenFieldArrayConfig";


    public KeyValuePair<string, string>[] GetWarnings()
    {
        return this.Warnings.ToArray();
    }

    public IEditControl InitFromNetObjects(List<string> listOfNetObjectIDs)
    {
        if (listOfNetObjectIDs.Count() != 1)
        {
            return null;
        }

        var netObjectId = new NetObjectUniqueIdentifier(listOfNetObjectIDs.First());

        if (netObjectId.Prefix != StorageArray.NetObjectPrefix)
        {
            return null;
        }

        this.array = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayDetails(netObjectId.ID);
       
        if (this.array.FlowType != FlowType.SmisAndGenericHttpExternal)
        {
            return null;
        }

        this.providerLabel = SrmWebContent.Dell_Storage_Manager;
        this.arrayType = this.DeviceGroup.Name;

        if (!this.TryGetProviders())
        {
            return null;
        }

        this.FillControl();

        return this;

    }

    private bool TryGetProviders()
    {
        var providers = ServiceLocator.GetInstance<IStorageProviderDAL>().GetAllProvidersByArrayId(this.array.StorageArrayId);
        this.restProvider = providers.FirstOrDefault(x => x.CredentialType == CredentialType.GenericHttp);
        this.smisProvider = providers.FirstOrDefault(x => x.CredentialType == CredentialType.SMIS);

        if (this.smisProvider == null || this.restProvider == null)
        {
            return false;
        }

        return true;
    }

    private void FillControl()
    {
        hlProvider.NavigateUrl = string.Format(CultureInfo.InvariantCulture, "~/Orion/SRM/ProviderDetailsView.aspx?NetObject=SMP:{0}", this.smisProvider.ID);
        hlProvider.Text = this.restProvider.FriendlyName;
        if (this.hlProvider.Controls.Count == 0)
        {
            this.hlProvider.Controls.Add(providerStatusIcon);
            this.hlProvider.Controls.Add(providerName);
        }

        providerStatusIcon.ImageUrl = InvariantString.Format(
            "~/Orion/StatusIcon.ashx?size=small&entity=Orion.SRM.Providers&id={0}&status={1}",
            this.smisProvider.ID,
            (int)this.smisProvider.Status);
        providerName.InnerHtml = this.smisProvider.FriendlyName;

        
        HiddenField hiddenFieldSmisConfig = new HiddenField
        {
            ClientIDMode = ClientIDMode.Static,
            ID = HiddenFieldSmisConfig
        };
        this.hfPlaceholder.Controls.Add(hiddenFieldSmisConfig);
        hiddenFieldSmisConfig.Value = this.smisProvider.ToJSONconfiguration();
        
        HiddenField hiddenFieldRestConfig = new HiddenField
        {
            ClientIDMode = ClientIDMode.Static,
            ID = HiddenFieldRestConfig
        };
        this.hfPlaceholder.Controls.Add(hiddenFieldRestConfig);
        hiddenFieldRestConfig.Value = this.restProvider.ToJSONconfiguration();

        HiddenField hiddenFieldArrayConfig = new HiddenField
        {
            ClientIDMode = ClientIDMode.Static,
            ID = this.HiddenFieldArrayConfig
        };
        this.hfPlaceholder.Controls.Add(hiddenFieldArrayConfig);
        hiddenFieldArrayConfig.Value = this.array.StorageArrayId.ToString();
    }


    public void Update(bool connectionPropertyChanged)
    {
        //throw new NotImplementedException();
    }

    public bool Validate(bool connectionPropertyChanged)
    {
        //throw new NotImplementedException();
        return true;
    }

    public string ValidateJSFunction()
    {
        return "SW.SRM.AddSmisAndHttpProvider.testCredentials()";

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.uxDeviceTypeInformation.InitWithDevice(DeviceGroup);

    }
}