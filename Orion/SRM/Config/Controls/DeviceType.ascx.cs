﻿using System.Web.UI;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SRM.Common.Models;

namespace Orion.SRM.Config.Controls
{
    public partial class Orion_SRM_Config_DeviceType : UserControl
    {
        public void InitWithDevice(DeviceGroup deviceGroup)
        {
            this.image.ImageUrl = deviceGroup.TipPicture;
            FamilyTypeName.Text = deviceGroup.Name;
            FamilyTypeDescription.Text = deviceGroup.Description;
            FamilyTypeLink.HRef = HelpHelper.GetHelpUrl(deviceGroup.LinkURL);
            FamilyTypeLink.InnerText = deviceGroup.LinkName;
        }

        public void AdaptToUnifiedDevice(string name, string arrayDescription, string linkDesc)
        {
            FamilyTypeName.Text = name;
            FamilyTypeLink.InnerText = linkDesc;
            FamilyTypeDescription.Text = arrayDescription;
        }
    }
}