﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TestConnectionMessages.ascx.cs" Inherits="Orion_SRM_Config_Controls_TestConnectionMessages" %>
<%@ Import Namespace="Resources" %>
<orion:Include ID="Include" runat="server" File="SRM/Config/Controls/SRM.TestConnectionUI.js" />

<div id="<%= ContainerId %>" >
    <div id="testingConnectionMsg" class="sw-suggestion SRM_testingConnectionMsgBox srm-testing-connection-msgbox">
        <img src="/Orion/images/loading_gen_16x16.gif" /><b><%= SrmWebContent.Provider_Step_Testing_Connections %></b>
        <a href="<%= JsCancellationFunction %>" ><%= SrmWebContent.Provider_Step_Cancel_Testing_Connection %></a>
    </div>
    <div id="runningDiscoveryMsg" class="sw-suggestion SRM_testingConnectionMsgBox srm-testing-connection-msgbox">
        <img src="/Orion/images/loading_gen_16x16.gif" /><b><%= SrmWebContent.Provider_Step_ArrayStep_DiscoveryRunning %></b>
    </div>
    <div id="testConnectionFailMsg" class="sw-suggestion sw-suggestion-fail SRM_testingConnectionMsgBox srm-testing-connection-msgbox">
        <span class="sw-suggestion-icon"></span>
        <span id="failMessageHeader">
            <b><span id="failMessageHeaderContent"></span></b><br />
        </span>
        <span id="failMessageContent"></span>
    </div>
    <div id="testConnectionPassMsg" class="sw-suggestion sw-suggestion-pass SRM_testingConnectionMsgBox srm-testing-connection-msgbox">
        <span class="sw-suggestion-icon"></span><span id="passMessageContent"><%= SrmWebContent.Provider_Step_Connection_Success %></span>
    </div>
</div>