﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Web.EnrollmentWizard;

public partial class Orion_SRM_Config_Controls_AddXtremIoProvider : System.Web.UI.UserControl
{
    public string initTextsJs = InvariantString.Format("SW.SRM.ProviderSelector.initTexts( {{ noDataAvailable: '{0}' }});", SrmWebContent.Provider_Step_NoDataAvailableXtremIo);
    
    public DeviceGroup DeviceGroup { get; set; }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        deviceInfo.InitWithDevice(DeviceGroup);
        OrionInclude.InlineJs(initTextsJs);

        var configuration = WizardWorkflowHelper.ConfigurationInfo;

        HttpPortTextBox.Value = configuration.DeviceGroup.Properties["HttpPort"];
        HttpsPortTextBox.Value = configuration.DeviceGroup.Properties["HttpsPort"];
    }

    public string AddProviderToolbarLabel = SrmWebContent.Provider_Step_Add_XtremIo_XMS_Btn_Label;
    public string Provider_Step_Add_Provider_Title = SrmWebContent.Provider_Step_Add_XtremIo_XMS_Provider_Title;
}
