﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SnmpVersionOneCredentialsControl.ascx.cs" Inherits="Orion_SRM_Config_Controls_SnmpVersionOneCredentialsControl" %>
<%@ Import Namespace="Resources" %>
<table class="srm-EnrollmentWizard-providerStepInputsTable">
    <tr>
        <td><%= SrmWebContent.Provider_Step_Snmp_Credential%>*:</td>
        <td>
            <asp:DropDownList runat="server" ID="SnmpCredentialDropDown" onChange="return OnSelectedCredentialChange(this);" ClientIDMode="Static" />
        </td>
    </tr>
</table>
<table class="srm-EnrollmentWizard-providerStepInputsTable" id="StoredCredFiledsV1">
    <tr>
        <td><%= SrmWebContent.Provider_Step_Credential_Name%>*:</td>
        <td>
            <asp:TextBox runat="server" id="CredentialNameTextBox" onChange="return OnSelectedValueChange();" ClientIDMode="Static" />
        </td>
    </tr>
    <tr>
        <td><%= SrmWebContent.Provider_Step_Array_Snmp_Community%>*:</td>
        <td>
            <asp:TextBox runat="server" ID="SnmpCommunityTextBox" onChange="return OnSelectedValueChange();" ClientIDMode="Static" />
        </td>
    </tr>
    <tr>
        <td><%= SrmWebContent.Provider_Step_Array_Snmp_Port%>*:</td>
        <td>
            <asp:TextBox runat="server" ID="SnmpPortTextBoxV2" onChange="return OnSelectedValueChange();" ClientIDMode="Static" />
        </td>
    </tr>
</table>
<script>
    function OnSelectedValueChange() {
    }
</script>
