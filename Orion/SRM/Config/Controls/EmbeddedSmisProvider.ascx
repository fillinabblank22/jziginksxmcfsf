﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmbeddedSmisProvider.ascx.cs" Inherits="Orion_SRM_Config_Controls_EmbeddedSmisProvider" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Import Namespace="SolarWinds.SRM.Web.EnrollmentWizard" %>

<%@ Register Src="~/Orion/SRM/Config/Controls/ProviderForm.ascx" TagPrefix="UIInfo" TagName="AddProvider" %>
<orion:Include ID="Include" runat="server" File="SRM/Config/Controls/DiscoveryValidation.js" />
<orion:Include ID="SmisProviderJs" runat="server" File="SRM/Config/Controls/EmbeddedSmisProvider.js" />

<orion:Include runat="server" File="Discovery.css" Module="SRM" />
<orion:Include runat="server" File="SRM.EditProperties.SmisProvider.css" Module="SRM" />

<script type="text/javascript">
    $(document).ready(function () {
        SW.SRM.EmbeddedSmisProvider.init('<%= ProviderTypeEnum.Block.ToString() %>',
                                    '<%= SrmWebContent.Provider_Step_New_Credential %>',
                                    '<%= SrmWebContent.Provider_Step_Unexpected_Error %>',
                                    '<%= ArrayID %>',
                                    '<%= ArraySerialNumber %>',
                                    '<%= WizardWorkflowHelper.ConfigurationInfo.DeviceGroup.GroupId %>');

        $('#ChooseCredentialDdl').trigger('change');
    });

    TestProviderCredential = function() {
        SW.SRM.EmbeddedSmisProvider.testSmisCredential(null, false);
    };

    onClickNextButtonEvent = function () {
        SW.SRM.EmbeddedSmisProvider.testSmisCredential(providerStepSubmit, false);
    };
</script>

<span><%= SubTitle %></span>
<uiinfo:addprovider runat="server" id="ProviderControl" DisplayArrayType="True" />