﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddSmisProvider.ascx.cs" Inherits="Orion_SRM_Config_Controls_AddSmisProvider" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>

<%@ Register TagPrefix="UIInfo" TagName="DeviceType" Src="~/Orion/SRM/Config/Controls/DeviceType.ascx" %>
<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="SRM/Config/Controls/AddSmisProvider.js" />
<orion:Include runat="server" File="SRM/Config/Controls/SRM.Expander.js" />

<style type="text/css">
    .srm-main-content {
        padding: 10px;
    }
    .srm-main-content table {
        width: 100%;
        table-layout: fixed;
    }
    .srm-main-content td {
        vertical-align: top;
    }
    .srm-main-content td span {
        white-space: nowrap;
    }
    .srm-main-content > table > tbody > tr > td > table > tbody > tr > td:first-child {
        width: 200px;
    }

    #ProtocolCheckBox {
        width: 15px;
        height: 15px;
        padding: 0px;
        vertical-align: middle;
    }

    #HttpsPortTextBox, #HttpPortTextBox {
        width: 75px;
    }

    #expanderElement {
        padding: 0px 0px 10px 0px;
    }

    #expanderElement a {
        vertical-align: middle;
        color: black;
        text-decoration: none;
    }

    #expanderElement a:hover {
        color: black;        
        text-decoration: none;
    }

    #expanderElement img {
        vertical-align: middle;
        padding-right: 10px;
    }

    #imgSmsi {
        width: 630px;
    }
</style>

<script type="text/javascript">
    $(function () {
        SW.SRM.AddSmisProvider.init('<%= (int)CredentialType.SMIS %>',
                                    '<%= SrmWebContent.Provider_Step_Provider_Connecting_Error %>:',
                                    '<%= SrmWebContent.Provider_Step_Unexpected_Error %>',
                                    '<%= SrmWebContent.Provider_Step_New_Credential %>',
                                    '<%= this.EngineId %>');
    });
</script>
<asp:PlaceHolder runat="server" ID="hfPlaceholder"></asp:PlaceHolder>
<input type="hidden" id="TestConnectionErrorJSDelegate" value="<%= this.TestConnectionErrorJSDelegate %>" />
<div class="srm-main-content">
    <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="label"><span><span id="labelIpAddress"><%= SrmWebContent.Add_Provider_IP_Address_Hostname %></span>*:</span></td>
                        <td>
                            <span><input type="text" id="IpAddressOrHostnameTextBox" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><%= SrmWebContent.Add_Provider_Choose_Credential%>*:</span></td>
                        <td>
                            <span><select id="ChooseCredentialDdl" onchange="ChooseCredentialDdl_OnChange(this)" /></span>
                        </td>
                    </tr>
                </table>
                <table id="StoredCredFiledsTable">
                    <tr>
                        <td class="label"><span><span id="labelDisplayName"><%= SrmWebContent.Add_Provider_DisplayName %></span>*:</span></td>
                        <td>
                            <span><input type="text" id="DisplayNameTextBox" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><span id="labelProviderUserName"><%= SrmWebContent.Add_Provider_ProviderUsername %></span>*:</span></td>
                        <td>
                            <span><input type="text" id="ProviderUsernameTextBox" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><span id="labelProviderPassword"><%= SrmWebContent.Add_Provider_Password%></span>*:</span></td>
                        <td>
                            <span><input type="password" id="ProviderPasswordTextBox" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><%= SrmWebContent.Add_Provider_ConfirmPassword%>*:</span></td>
                        <td>
                            <span><input type="password" id="ProviderConfirmPasswordTextBox" /></span>
                        </td>
                    </tr>
                </table>
                <div id="expanderElement"><a href="javascript:expanderChangedHandler();"><img src="/Orion/SRM/images/Button.Expand.gif" alt="[+]" /><%= SrmWebContent.Provider_Step_AddProvider_Advanced %></a></div>
                <table id="AdvancedCredFiledsTable">
                    <tr>
                        <td class="label"><span><%= SrmWebContent.Add_Provider_Protocol%></span></td>
                        <td>
                            <span><input type="checkbox" id="ProtocolCheckBox" /><%= SrmWebContent.Provider_Step_UseHttp_Label %></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><span id="labelHttpsPort"><%= SrmWebContent.Add_Provider_Https_Port%></span>*:</span></td>
                        <td>
                            <span><input type="text" id="HttpsPortTextBox" value="<%= SRMConstants.DefaultSmisHttpsPort.ToString() %>" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><span id="labelHttpPort"><%= SrmWebContent.Add_Provider_Http_Port%></span>*:</span></td>
                        <td>
                            <span><input type="text" id="HttpPortTextBox" value="<%= SRMConstants.DefaultSmisHttpPort.ToString() %>" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><span id="labelInteropNamespace"><%= SrmWebContent.Add_Provider_Interop_Namespace%></span>*:</span></td>
                        <td>
                            <span><input type="text" id="InteropNamespaceTextBox" value="<%= this.InteropNamespace %>" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><span id="labelArrayNamespace"><%= SrmWebContent.Add_Provider_Array_Namespace%></span>*:</span></td>
                        <td>
                            <span><input type="text" id="ArrayNamespaceTextBox" value="<%= this.ArrayNamespace %>" /></span>
                        </td>
                    </tr>
                </table>
                <input type="button" id="testConnectionButton" class="providerStepButton" value="<%= SrmWebContent.Provider_Step_Test_Connection %>" onclick="SW.SRM.AddSmisProvider.testCredential();" />
                <div id="testingConnectionMsg" class="sw-suggestion SRM_testingConnectionMsgBox">
                    <img src="/Orion/images/loading_gen_16x16.gif" /><b><%= SrmWebContent.Provider_Step_Testing_Connections %></b>
                    <a href="javascript:SW.SRM.AddSmisProvider.cancelTestCredential();"><%= SrmWebContent.Provider_Step_Cancel_Testing_Connection %></a>
                </div>
                <div id="testConnectionFailMsg" class="sw-suggestion sw-suggestion-fail SRM_testingConnectionMsgBox">
                    <span class="sw-suggestion-icon"></span>
                    <span id="failMessageHeader">
                        <b><span id="failMessageHeaderContent"></span></b><br />
                    </span>
                    <span id="failMessageContent"></span>
                </div>
                <div id="testConnectionPassMsg" class="sw-suggestion sw-suggestion-pass SRM_testingConnectionMsgBox">
                    <span class="sw-suggestion-icon"></span><span id="passMessageContent"><%= SrmWebContent.Provider_Step_Connection_Success %></span>
                </div>
            </td>
            <td id="imgSmsi">
                <UIInfo:DeviceType runat="server" ID="deviceInfo"></UIInfo:DeviceType>
            </td>
        </tr>
    </table>
</div>
