﻿SW = SW || {};
SW.SRM = SW.SRM || {};
SW.SRM.AddCelerraProvider = function () {
    var credentialsSmisValidation = function ($selectSmisCredential, displayNameVar, usernameVar, passwordVar, confirmPasswordVar, ipAddressOrHostnameTextBoxVar) {
        if ($.trim(String(ipAddressOrHostnameTextBoxVar)) == '') {
            return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelIpAddress').text());
        }

        if ($selectSmisCredential.val() == null
         || $selectSmisCredential.val().length == 0
         || $selectSmisCredential.val() == newCredentialsConstant) {
            if ($.trim(String(usernameVar)) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelSmisProviderUserName').text());
            }

            if ($.trim(String(displayNameVar)) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelSmisDisplayName').text());
            }
            
            var selectedCredentialOptionsArr = $selectSmisCredential.children().map(function () { return this.text.toLowerCase(); }).get();
            var credentialNameExists = $.inArray(displayNameVar.toLowerCase(), selectedCredentialOptionsArr) >= 0;
            if (credentialNameExists) {
                return "<a href='javascript:SW.SRM.AddCelerraProvider.loadSMISCredentials(&quot;"
                    + displayNameVar
                    + "&quot;)'>@{R=SRM.Strings;K=Add_Provider_CredentialNameAlreadyExist_LoadSaved; E=js}</a> or <a href='javascript:SW.SRM.AddCelerraProvider.focusSMISCredentialsDisplayName()'>@{R=SRM.Strings;K=Add_Provider_CredentialNameAlreadyExist_RenameDuplicate; E=js}</a>";
            }

            if ($.trim(String(passwordVar)) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelSmisProviderPassword').text());
            }

            if (!(passwordVar === confirmPasswordVar)) {
                return "@{R=SRM.Strings;K=Add_Provider_ConfirmSMISPassword_CompareField_Validator; E=js}";
            }

            var httpPort = $("#HttpPortTextBox").val();
            if ($.trim(String(httpPort)) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelSmisHttpPort').text());
            }
            var httpPortInt = parseInt(httpPort);
            if (!$.isNumeric(httpPortInt) || Math.floor(httpPortInt) !== httpPortInt || httpPortInt < 1 || httpPortInt > 65535) {
                return "@{R=SRM.Strings;K=Add_Provider_NotValidPort_Validator; E=js}".replace('{0}', $('#labelSmisHttpPort').text());
            }

            var httpsPort = $("#HttpsPortTextBox").val();
            if ($.trim(String(httpsPort)) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelSmisHttpsPort').text());
            }
            var httpsPortInt = parseInt(httpsPort);
            if (!$.isNumeric(httpsPortInt) || Math.floor(httpsPortInt) !== httpsPortInt || httpsPortInt < 1 || httpsPortInt > 65535) {
                return "@{R=SRM.Strings;K=Add_Provider_NotValidPort_Validator; E=js}".replace('{0}', $('#labelSmisHttpsPort').text());
            }

            if ($.trim(String($("#InteropNamespaceTextBox").val())) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelSmisInteropNamespace').text());
            }

            if ($.trim(String($("#ArrayNamespaceTextBox").val())) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelSmisArrayNamespace').text());
            }
        }

        return "";
    };

    var credentialsApiValidation = function ($selectCredential, displayNameVar, usernameVar, passwordVar, confirmPasswordVar, ipAddressOrHostnameTextBoxVar) {
        if ($.trim(String(ipAddressOrHostnameTextBoxVar)) == '') {
            return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelIpAddress').text());
        }

        if ($selectCredential.val() == null
         || $selectCredential.val().length == 0
         || $selectCredential.val() == newCredentialsConstant) {
            if ($.trim(String(usernameVar)) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelApiProviderUserName').text());
            }

            if ($.trim(String(displayNameVar)) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelApiDisplayNameTextBox').text());
            }
            
            var selectedCredentialOptionsArr = $selectCredential.children().map(function () { return this.text.toLowerCase(); }).get();
            var credentialNameExists = $.inArray(displayNameVar.toLowerCase(), selectedCredentialOptionsArr) >= 0;
            if (credentialNameExists) {
                return "<a href='javascript:SW.SRM.AddCelerraProvider.loadAPICredentials(&quot;"
                    + displayNameVar
                    + "&quot;)'>@{R=SRM.Strings;K=Add_Provider_CredentialNameAlreadyExist_LoadSaved; E=js}</a> or <a href='javascript:SW.SRM.AddCelerraProvider.focusAPICredentialsDisplayName()'>@{R=SRM.Strings;K=Add_Provider_CredentialNameAlreadyExist_RenameDuplicate; E=js}</a>";
            }

            if ($.trim(String(passwordVar)) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelApiProviderPassword').text());
            }

            if (!(passwordVar === confirmPasswordVar)) {
                return "@{R=SRM.Strings;K=Add_Provider_ConfirmApiPassword_CompareField_Validator; E=js}";
            }

            var httpPort = $("#ArrayApiHttpPortTextBox").val();
            var httpsPort = $("#ArrayApiHttpsPortTextBox").val();
            if ($.trim(String(httpPort)) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelApiHttpPort').text());
            }
            if ($.trim(String(httpsPort)) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelApiHttpsPort').text());
            }
            var httpPortInt = parseInt(httpPort);
            var httpsPortInt = parseInt(httpsPort);
            if (!$.isNumeric(httpPortInt) || Math.floor(httpPortInt) !== httpPortInt || httpPortInt < 1 || httpPortInt > 65535) {
                return "@{R=SRM.Strings;K=Add_Provider_NotValidPort_Validator; E=js}".replace('{0}', $('#labelApiHttpPort').text());
            }
            if (!$.isNumeric(httpsPortInt) || Math.floor(httpsPortInt) !== httpsPortInt || httpsPortInt < 1 || httpsPortInt > 65535) {
                return "@{R=SRM.Strings;K=Add_Provider_NotValidPort_Validator; E=js}".replace('{0}', $('#labelApiHttpsPort').text());
            }

            if ($.trim(String($("#ArrayApiManagementServicesPathTextBox").val())) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelApiManagementPath').text());
            }
        }

        return "";
    };

    var request,
        connectionTestFailedTitle,
        connectionTestUnexpectedFailedMessage,
        newCredentialsConstant,
        arrayID,
        arraySerialNumber,
        groupId; 

    var formatSmisError = function (message) {
        return "@{R=SRM.Strings;K=ConfigurationWizard_Provider_Step_Celerra_SMISCredentials_ErrorFormat; E=js}".replace('{0}', message);
    };

    var formatVnxError = function (message) {
        return "@{R=SRM.Strings;K=ConfigurationWizard_Provider_Step_Celerra_APICredentials_ErrorFormat; E=js}".replace('{0}', message);
    };

    var successValidation = function(result) {
        $('#hfEditPropertiesVNXFileOrUnifiedTemplateID').val(result.Message);
    };

    return {
        getTestConnectionMessagesDiv: function () {
            return $("#testCelerraProvider");
        },
        getTestConnectionButton: function () {
            return $("#testConnectionButton");
        },
        focusSMISCredentialsDisplayName: function () {
            $("#DisplayNameTextBox").focus();
        },
        focusAPICredentialsDisplayName: function () {
            $("#DisplayNameVnxTextBox").focus();
        },
        loadSMISCredentials: function (credentialName) {
            $('#ChooseCredentialSmis').find('option:contains("' + credentialName + '")').attr("selected", true);
            $('#ChooseCredentialSmis').trigger('change');
        },
        loadAPICredentials: function (credentialName) {
            $('#ChooseCredentialVnx').find('option:contains("' + credentialName + '")').attr("selected", true);
            $('#ChooseCredentialVnx').trigger('change');
        },
        cancelTestCredential: function () {
            if (request) {
                var requestToAbort = request;
                request = null;
                requestToAbort.abort();
            }

            SW.SRM.AddCelerraProvider.handleError();
        },
        showFailMessageBox : function (message, errorMessage) {
            var title = connectionTestFailedTitle.concat(" ").concat($("#IpAddressOrHostnameTextBox").val());
            SW.SRM.TestConnectionUI.ShowError(title, message, errorMessage, SW.SRM.AddCelerraProvider);
        },
        handleError : function (message, runDiscovery, errorMessage) {
            SW.SRM.TestConnectionUI.HandleError(SW.SRM.AddCelerraProvider);
            if (request == null || request.status !== 0) {
                SW.SRM.AddCelerraProvider.showFailMessageBox(message, errorMessage);
            }
            SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
        },
        testCredential: function (onSuccess, onError) {
            SW.SRM.AddCelerraProvider.testSmisCredential(onSuccess, onError, false);
        },
        testCredentialsAndValidateDiscovery: function () {
            var testingDeffered = $.Deferred();

            if (typeof SW.SRM.EditProperties.SmisProvider !== 'undefined' && !smisProvider.IsCelerraFilled()) {
                return testingDeffered.resolve();
            }

            SW.SRM.DiscoveryValidation.init(arrayID, arraySerialNumber, groupId, request, testingDeffered, connectionTestUnexpectedFailedMessage, SW.SRM.AddCelerraProvider.handleError, successValidation);
            var globalDiscoveryPromise = $.Deferred();
            SW.SRM.AddCelerraProvider.testSmisCredential(null, null, true);
            $.when(testingDeffered).then(
                function (x) {
                    return globalDiscoveryPromise.resolve();
                },
                function () {
                    return globalDiscoveryPromise.reject();
                });

            return globalDiscoveryPromise.promise();
        },
        testSmisCredential: function (onSuccess, onError, isEdit) {
            request = null;
            var displayNameVar = $("#DisplayNameTextBox").val();
            var usernameVar = $("#ProviderUsernameTextBox").val();
            var passwordVar = $("#ProviderPasswordTextBox").val();
            var confirmPasswordVar = $("#ProviderConfirmPasswordTextBox").val();
            var ipAddressOrHostnameTextBoxVar = $.trim(String($("#IpAddressOrHostnameTextBox").val()));
            var namespaceVar = $("#InteropNamespaceTextBox").val();
            var arrayNamespaceVar = $("#ArrayNamespaceTextBox").val();
            var httpsPortVar = $("#HttpsPortTextBox").val();
            var httpPortVar = $("#HttpPortTextBox").val();
            var protocolVar = $("#ProtocolCheckBox").is(':checked');
            var $selectSmisCredential = $("#ChooseCredentialSmis");

            var error = credentialsSmisValidation($selectSmisCredential, displayNameVar, usernameVar, passwordVar, confirmPasswordVar, ipAddressOrHostnameTextBoxVar);
            if (error == null || error.length == 0) {
                SW.SRM.TestConnectionUI.HideError(SW.SRM.AddCelerraProvider);
            } else {
                SW.SRM.AddCelerraProvider.handleError(formatSmisError(error), isEdit);
                return;
            }

            SW.SRM.TestConnectionUI.HidePass(SW.SRM.AddCelerraProvider);
            SW.SRM.TestConnectionUI.ShowLoading(true, SW.SRM.AddCelerraProvider);

            var credentialId = $selectSmisCredential.val() == null || $selectSmisCredential.val().length == 0 ? -1 : parseInt($selectSmisCredential.val());
            request = SW.SRM.Common.CallSrmWebService("/Orion/SRM/Services/EnrollmentWizardService.asmx",
                                        "TestSmisCredentialsExtendedDevice",
                                        {
                                            'credentialID': credentialId,
                                            'credentialName': displayNameVar,
                                            'userName': usernameVar,
                                            'password': passwordVar,
                                            'useHttp': protocolVar,
                                            'httpPort': parseInt(httpPortVar),
                                            'httpsPort': parseInt(httpsPortVar),
                                            'interopNamespace': namespaceVar,
                                            'arrayNamespace': arrayNamespaceVar,
                                            'ip': ipAddressOrHostnameTextBoxVar,
                                            'groupId': groupId,
                                            'isEdit': isEdit
                                        },
                                        function (result) {
                                            if (result.IsSuccess) {
                                                SW.SRM.AddCelerraProvider.testVnxCredentials(onSuccess, onError, isEdit);
                                            }
                                            else {
                                                SW.SRM.AddCelerraProvider.handleError(formatSmisError(result.Message), isEdit, result.ErrorMessage);
                                                if (onError) {
                                                    onError();
                                                }
                                            }
                                        },
                                        function() {
                                            SW.SRM.AddCelerraProvider.handleError(formatSmisError(connectionTestUnexpectedFailedMessage), isEdit);
                                            if (onError) {
                                                onError();
                                            }
                                        });
        },
        testVnxCredentials: function (onSuccess, onError, isEdit) {
            var displayApiNameVar = $("#DisplayNameVnxTextBox").val();
            var arrayApiUsernameVar = $("#ArrayApiUsernameTextBox").val();
            var arrayApiPasswordVar = $("#ArrayApiPasswordTextBox").val();
            var arrayApiConfirmPasswordVar = $("#ArrayApiConfirmPasswordTextBox").val();
            var arrayApiHttpPortVar = $("#ArrayApiHttpPortTextBox").val();
            var arrayApiHttpsPortVar = $("#ArrayApiHttpsPortTextBox").val();
            var arrayManagementServicesPathVar = $("#ArrayApiManagementServicesPathTextBox").val();
            if (arrayManagementServicesPathVar === undefined)
                arrayManagementServicesPathVar = null;
            var $selectApiCredential = $("#ChooseCredentialVnx");
            var ipAddressOrHostnameTextBoxVar = $.trim(String($("#IpAddressOrHostnameTextBox").val()));
            var apiProtocolIsHttps = !$('#ApiProtocolCheckBox').is(':checked');

            var error = credentialsApiValidation($selectApiCredential, displayApiNameVar, arrayApiUsernameVar, arrayApiPasswordVar, arrayApiConfirmPasswordVar, ipAddressOrHostnameTextBoxVar);
            if (error == null || error.length == 0) {
                SW.SRM.TestConnectionUI.HideError(SW.SRM.AddCelerraProvider);
            } else {
                SW.SRM.AddCelerraProvider.handleError(formatVnxError(error));
                return;
            }
            var credentialsId = $selectApiCredential.val() == null || $selectApiCredential.val().length == 0 ? -1 : parseInt($selectApiCredential.val());

            request = SW.SRM.Common.CallSrmWebService("/Orion/SRM/Services/EnrollmentWizardService.asmx",
                                        "TestVnxCredentialsExtended",
                                        {
                                            'credentialID': credentialsId,
                                            'credentialName': displayApiNameVar,
                                            'userName': arrayApiUsernameVar,
                                            'password': arrayApiPasswordVar,
                                            'httpsPort': parseInt(arrayApiHttpsPortVar),
                                            'httpPort': parseInt(arrayApiHttpPortVar),
                                            'ip': ipAddressOrHostnameTextBoxVar,
                                            'managementServicesPath': arrayManagementServicesPathVar,
                                            'saveProviderWhenSuccessful': isEdit,
                                            'apiProtocolIsHttps': apiProtocolIsHttps,
                                            'groupId': groupId,
                                        },
                                        function (result) {

                                            SW.SRM.TestConnectionUI.ShowLoading(false, SW.SRM.AddCelerraProvider);

                                            if (result.IsSuccess) {
                                                SW.SRM.TestConnectionUI.ShowPassMessageBox(null, SW.SRM.AddCelerraProvider);
                                                if (onSuccess) {
                                                    onSuccess();
                                                }
                                                if (isEdit) {
                                                    SW.SRM.DiscoveryValidation.startDiscovery(SW.SRM.AddCelerraProvider);
                                                }
                                            }
                                            else {
                                                SW.SRM.AddCelerraProvider.handleError(formatVnxError(result.Message), isEdit, result.ErrorMessage);
                                                if (onError) {
                                                    onError();
                                                }
                                            }
                                        },
                                        function () {
                                            SW.SRM.AddCelerraProvider.handleError(formatVnxError(connectionTestUnexpectedFailedMessage), isEdit);
                                            if (onError) {
                                                onError();
                                            }
                                        });
        },
        init: function (testFailedTitle, testUnexpectedFailedMessage, _newCredentialsConstant, _arrayID, _arraySerialNumber, deviceGroupId) {
            connectionTestFailedTitle = testFailedTitle;
            connectionTestUnexpectedFailedMessage = testUnexpectedFailedMessage;
            arrayID = _arrayID;
            arraySerialNumber = _arraySerialNumber;
            groupId = deviceGroupId;
            newCredentialsConstant = _newCredentialsConstant;

            this.MonitoredControlIds = ['IpAddressOrHostnameTextBox', 'ChooseCredentialSmis', 'ProviderUsernameTextBox', 'ProviderPasswordTextBox', 'ChooseCredentialVnx', 'ArrayApiUsernameTextBox', 'ArrayApiPasswordTextBox'];
            SW.SRM.EditProperties.ProviderChangeManager.InitProvider(this, this.testCredentialsAndValidateDiscovery);
        }
    };
}();