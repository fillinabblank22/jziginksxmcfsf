﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IsilonProviderStep.ascx.cs" Inherits="Orion_SRM_Config_Controls_IsilonProviderStep" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common" %>
<%@ Register Src="~/Orion/SRM/Config/Controls/SnmpVersionOneCredentialsControl.ascx" TagPrefix="srm" TagName="SnmpVersionOneCredentials" %>
<%@ Register Src="~/Orion/SRM/Config/Controls/SnmpVersionThreeCredentialsControl.ascx" TagPrefix="srm" TagName="SnmpVersionThreeCredentials" %>
<%@ Register Src="~/Orion/SRM/Config/Controls/TestConnectionMessages.ascx" TagPrefix="UIInfo" TagName="TestConnectionMessages" %>
<%@ Register TagPrefix="UIInfo" TagName="DeviceType" Src="~/Orion/SRM/Config/Controls/DeviceType.ascx" %>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include ID="Include" runat="server" File="SRM/Config/Controls/DiscoveryValidation.js" />
<orion:Include runat="server" File="Discovery.css" Module="SRM" />
<orion:Include runat="server" File="IsilonProviderStep.css" Module="SRM" />

<span><%= SubTitle%></span>

<div class="srm-enrollment-wizard-content">
    <div class="srm-main-content PollingPropertiesSection">
        <table class="srm-EnrollmentWizard-providerStepContentTable">
            <tr>
                <td>
                    <table class="srm-EnrollmentWizard-providerStepInputsTable">
                        <tr>
                            <td><%= SrmWebContent.Provider_Step_Array_Type%>:</td>
                            <td>
                                <b>
                                    <asp:Label runat="server" ID="ArrayTypeName" />
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td><%= SrmWebContent.Provider_Step_Array_Ip%>*:</td>
                            <td>
                                <asp:TextBox runat="server" ID="IpAddressTextBox" ClientIDMode="Static" />
                            </td>
                        </tr>
                    </table>

                    <div class="CredentialsLabel"><span><b><%= SrmWebContent.ProviderStep_Provider_SNMPCredentials%></b></span>&nbsp;<asp:Image ID="SNMPCredentialsTooltip" src="/Orion/images/Icon.Info.gif" Visible="false" runat="server" ClientIDMode="Static" /></div>
                    <table class="srm-EnrollmentWizard-providerStepInputsTable">
                        <tr> 
                            <!-- This will be displayed again in vNext. -->
                            <td><%= SrmWebContent.Provider_Step_Snmp_Version%>*:</td>
                            <td>
                                <asp:DropDownList runat="server" ID="SnmpVersionDropDown" ClientIDMode="Static" onchange="return OnSelectedIndexChange(this.options[this.selectedIndex].value);" />
                            </td>
                        </tr>
                    </table>

                    <asp:Panel runat="server" ID="SnmpVersionOneCredentialsPanel" ClientIDMode="Static" class="credentialsPanelProviderStep">
                        <srm:SnmpVersionOneCredentials runat="server" ID="SnmpVersionOneCredentials" />
                    </asp:Panel>
                    <asp:Panel runat="server" ID="SnmpVersionThreeCredentialsPanel" ClientIDMode="Static" class="credentialsPanelProviderStep">
                        <srm:SnmpVersionThreeCredentials runat="server" ID="SnmpVersionThreeCredentials" IsContextRequired="True" />
                    </asp:Panel>

                    <div class="CredentialsLabel"><span><b><%= SrmWebContent.Provider_Step_Isilon_APICredentials%></b></span>&nbsp;<asp:Image ID="ApiCredentialsTooltip" src="/Orion/images/Icon.Info.gif" Visible="false" runat="server" ClientIDMode="Static" /></div>
                    <table class="srm-EnrollmentWizard-providerStepInputsTable sub-table">
                        <tr>
                            <td><span><%= SrmWebContent.Provider_Step_Isilon_ChooseAPICredentials%>*:</span></td>
                            <td>
                                <span>
                                    <asp:DropDownList ID="ChooseCredentialEmcHttp" onchange="ChooseCredentialEmcHttp_OnChange(this)" runat="server" ClientIDMode="Static" /></span>
                            </td>
                        </tr>
                    </table>
                    <table id="StoredCredFieldsTableEmcHttp" class="srm-EnrollmentWizard-providerStepInputsTable sub-table">
                        <tr>
                            <td><span>
                                <asp:Label ID="labelEmcHttpDisplayNameTextBox" runat="server" ClientIDMode="Static"></asp:Label>*:</span></td>
                            <td>
                                <span>
                                    <asp:TextBox ID="DisplayNameVnxTextBox" runat="server" ClientIDMode="Static" /></span>
                            </td>
                        </tr>
                        <tr>
                            <td><span>
                                <asp:Label ID="labelEmcHttpProviderUserName" runat="server" ClientIDMode="Static"></asp:Label>*:</span></td>
                            <td>
                                <span>
                                    <asp:TextBox ID="ArrayApiUsernameTextBox" runat="server" ClientIDMode="Static" /></span>
                            </td>
                        </tr>
                        <tr>
                            <td><span>
                                <asp:Label ID="labelEmcHttpProviderPassword" runat="server" ClientIDMode="Static"></asp:Label>*:</span></td>
                            <td>
                                <span>
                                    <asp:TextBox TextMode="Password" ID="ArrayApiPasswordTextBox" runat="server" ClientIDMode="Static" /></span>
                            </td>
                        </tr>
                        <tr>
                            <td><span id="labelEmcHttpConfirmProviderPassword"><%= SrmWebContent.Add_Provider_ArrayAPIConfirmPassword %>*:</span></td>
                            <td>
                                <span>
                                    <asp:TextBox TextMode="Password" ID="ArrayApiConfirmPasswordTextBox" runat="server" ClientIDMode="Static" /></span>
                            </td>
                        </tr>
                    </table>

                    <table id="AdvancedIsilonHttpApiCredFiledsTable" class="srm-EnrollmentWizard-providerStepInputsTable sub-table">
                        <tr>
                            <td><span><%= SrmWebContent.Add_Provider_Protocol%></span></td>
                            <td>
                                <span><input type="checkbox" id="ProtocolCheckBox" class="ProtocolCheckBox" runat="server" ClientIDMode="Static"/><label for="ProtocolCheckBox"> <%= SrmWebContent.Provider_Step_UseHttp_Label %></label></span>
                            </td>
                        </tr>
                        <tr>
                            <td><span><span id="labelEmcHttpPort"><%= SrmWebContent.Add_Provider_ArrayAPIPort%></span>*:</span></td>
                            <td>
                                <span>
                                    <asp:TextBox ID="HttpPortTextBox" runat="server" ClientIDMode="Static" /></span>
                            </td>
                        </tr>
                    </table>

                    <table class="srm-EnrollmentWizard-providerStepInputsTable">
                        <tr>
                            <td />
                            <td>
                                <input type="button" id="TestConnectionButton" class="providerStepButton" value="<%= SrmWebContent.Provider_Step_Test_Connection %>" onclick="OnTestButtonClick(false);" />
                            </td>
                        </tr>
                    </table>
                    <UIInfo:TestConnectionMessages runat="server" ID="TestConnectionMessages" JsCancellationFunction="javascript:OnCancelClick();" />
                </td>
                <td>
                    <UIInfo:DeviceType runat="server" ID="deviceInfo"></UIInfo:DeviceType>
                </td>
            </tr>
        </table>
    </div>
</div>

<asp:HiddenField runat="server" ID="SelectedSnmpCredential" ClientIDMode="Static" />

<script type="text/javascript">
    var emcHttpProvider = SW.Core.namespace("SW.SRM.EditProperties.EmcHttp");

    onClickNextButtonEvent = function () {
        var val = $('#SnmpVersionDropDown').val();
        switch (val) {
            case '<%=SnmpV1V2%>':
                OnTestV1Credentials(true, false);
                break;
            default:
                OnTestV3Credentials(true, false);
                break;
        }
    };

    function ChooseCredentialEmcHttp_OnChange(ddl) {
        var $containerElement = $('#AdvancedIsilonHttpApiCredFiledsTable');

        if (ddl.value !== "<%= SRMConstants.NewCredential %>") {
            $("#StoredCredFieldsTableEmcHttp").hide();
            $containerElement.hide();
        } else {
            $("#StoredCredFieldsTableEmcHttp").show();
            $containerElement.show();
        }
    }


    function OnCredentialSelected(ddlId, fn) {
        var ddl = $('#' + ddlId);
        var ddlText = $(ddl).find(':selected').val();
        if (ddlText != "<%= SRMConstants.NewCredential %>") {
            fn(ddl);
        }
    }

    function OnSelectedIndexChange(val) {
        HideMessageBoxes();
        switch (val) {
            case '<%=SnmpV1V2%>':
                OnCredentialSelected('SnmpCredentialDropDown', OnSelectedCredentialChange);
                $('#SnmpVersionOneCredentialsPanel').show();
                $('#SnmpVersionThreeCredentialsPanel').hide();
                $('#SelectedSnmpCredential').val('V1');
                break;
            default:
                OnCredentialSelected('SnmpCredentialDropDownSnmpV3', OnSelectedCredentialChangeSnmpV3);
                $('#SnmpVersionOneCredentialsPanel').hide();
                $('#SnmpVersionThreeCredentialsPanel').show();
                $('#SelectedSnmpCredential').val('V3');
                break;
        }
    }
    $(document).ready(function () {
        $(".srm-testing-connection-msgbox").hide();
        var val = $('#SnmpVersionDropDown').val();
        OnSelectedIndexChange(val);

        ChooseCredentialEmcHttp_OnChange({ value: $('#ChooseCredentialEmcHttp').val() });
    });

    function OnSelectedCredentialChange(ddl) {
        HideMessageBoxes();
        $('#SnmpCommunityTextBox').val('');
        $('#SnmpPortTextBoxV2').val('');

        if (ddl.value != "<%= SRMConstants.NewCredential %>") {
            $('#StoredCredFiledsV1').hide();
        } else {
            $('#StoredCredFiledsV1').show();
        }
    }

    function OnSelectedCredentialChangeSnmpV3(ddl) {
        HideMessageBoxes();
        $('#PasswordKey1TextBox').val('');
        $('#PasswordKey2TextBox').val('');
        $('#SnmpContextTextBox').val('');
        $('#SnmpPortTextBoxV3').val('');
        $('#SnmpUserNameTextBox').val('');
        $('#SnmpV3CredentialNameTextBox').val('');
        $('#PasswordKey1CheckBox').attr('checked', false);
        $('#PasswordKey2CheckBox').attr('checked', false);

        if (ddl.value != "<%= SRMConstants.NewCredential %>") {
            $('#StoredCredFiledsV3').hide();
        } else {
            $('#StoredCredFiledsV3').show();
        }
    }

    function OnTestButtonClick(runDiscovery) {
        var val = $('#SnmpVersionDropDown').val();
        switch (val) {
            case '<%=SnmpV1V2%>':
                OnTestV1Credentials(false, runDiscovery);
                break;
            default:
                OnTestV3Credentials(false, runDiscovery);
                break;
        }
    }

    emcHttpProvider.ValidateCredentials = function () {
        var testCredentialDeferred = $.Deferred();
        SW.SRM.DiscoveryValidation.init("<%= ArrayID %>", "<%= ArraySerialNumber %>", "<%= DeviceGroupId %>", request, testCredentialDeferred, "<%= SrmWebContent.Provider_Step_Unexpected_Error%>", ShowFailMessageBox);
        var globalDiscoveryPromise = $.Deferred();
        OnTestButtonClick(true);

        $.when(testCredentialDeferred).then(function (x) {
            return globalDiscoveryPromise.resolve();

        }, function (x) {
            return globalDiscoveryPromise.reject();
        });

        return globalDiscoveryPromise.promise();
    }

    emcHttpProvider.MonitoredControlIds = [
        'pollEngineDescr',
        'IpAddressTextBox',
        'SnmpVersionDropDown', 'SnmpCredentialDropDown', 'SnmpCommunityTextBox', 'SnmpPortTextBoxV2',
        'SnmpCredentialDropDownSnmpV3', 'SnmpUserNameTextBox', 'SnmpContextTextBox',
        'ChooseCredentialEmcHttp', 'ArrayApiUsernameTextBox', 'ArrayApiPasswordTextBox'
    ];
    SW.SRM.EditProperties.ProviderChangeManager.InitProvider(emcHttpProvider, emcHttpProvider.ValidateCredentials);

    var request;

    function OnTestV1Credentials(isRequiredPostBack, runDiscovery) {
        $('#TestConnectionButton').attr("disabled", true);
        var ip = $.trim(String($("#IpAddressTextBox").val()));
        var credentialName = $("#CredentialNameTextBox").val();
        var communityString = $("#SnmpCommunityTextBox").val();
        var credentialID = $('#SnmpCredentialDropDown').val();
        var port = $("#SnmpPortTextBoxV2").val();

        var apiCredentialID = $('#ChooseCredentialEmcHttp').val();
        var apiCredentialName = $("#DisplayNameVnxTextBox").val();
        var apiUsername = $("#ArrayApiUsernameTextBox").val();
        var apiPassword = $("#ArrayApiPasswordTextBox").val();
        var apiConfirmPassword = $("#ArrayApiConfirmPasswordTextBox").val();
        var apiPort = $("#HttpPortTextBox").val();
        var apiProtocol = $("#ProtocolCheckBox").is(':checked');

        if (!ValidateFormV1(ip, credentialName, communityString, port)
                || !ValidateHttpAPIForm(ip, apiCredentialName, apiUsername, apiPassword, apiConfirmPassword, apiPort, apiProtocol)) {
            $('#TestConnectionButton').attr("disabled", false);
            SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
            return;
        }

        HideMessageBoxes();

        $("#testingConnectionMsg").show();
        request = SW.SRM.Common.CallSrmWebService('/Orion/SRM/Services/EnrollmentWizardService.asmx', 'TestSnmpV1Credentials',
            {
                credentialID: credentialID, credentialName: credentialName, communityString: communityString, ip: ip, port: port, saveProvider: runDiscovery === true, groupId: "<%= DeviceGroupId %>"
            },
            function(result) {
                if (result.IsSuccess) {
                    OnTestIsilonHttpCredentials(isRequiredPostBack, runDiscovery, apiCredentialID, apiCredentialName, apiUsername, apiPassword, apiPort, apiProtocol, ip, runDiscovery === true);
                } else {
                    $("#testingConnectionMsg").hide();
                    $('#TestConnectionButton').attr("disabled", false);

                    ShowFailMessageBox(result.Message, result.ErrorMessage);
                    SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
                }
            },
            function () {
                $("#testingConnectionMsg").hide();
                $('#TestConnectionButton').attr("disabled", false);

                if (request.status !== 0) {
                    ShowFailMessageBox("<%= SrmWebContent.Provider_Step_Unexpected_Error%>");
                }
                SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
            });
    }

    function OnTestV3Credentials(isRequiredPostBack, runDiscovery) {
        $('#TestConnectionButton').attr("disabled", true);
        var ip = $.trim(String($("#IpAddressTextBox").val()));
        var credentialID = $('#SnmpCredentialDropDownSnmpV3').val();
        var credentialName = $("#SnmpV3CredentialNameTextBox").val();
        var userName = $("#SnmpUserNameTextBox").val();
        var context = $("#SnmpContextTextBox").val();
        var authenticationValue = $("#AuthenticationMethodDropDown").val();
        var privacyValue = $("#EncryptionMethodDropDown").val();
        var authenticationPassword = $("#PasswordKey1TextBox").val();
        var authenticationKeyIsPassword = $("#PasswordKey1CheckBox").is(':checked');
        var privacyPassword = $("#PasswordKey2TextBox").val();
        var privacyKeyIsPassword = $("#PasswordKey2CheckBox").is(':checked');
        var port = $("#SnmpPortTextBoxV3").val();

        var apiCredentialID = $('#ChooseCredentialEmcHttp').val();
        var apiCredentialName = $("#DisplayNameVnxTextBox").val();
        var apiUsername = $("#ArrayApiUsernameTextBox").val();
        var apiPassword = $("#ArrayApiPasswordTextBox").val();
        var apiConfirmPassword = $("#ArrayApiConfirmPasswordTextBox").val();
        var apiPort = $("#HttpPortTextBox").val();
        var apiProtocol = $("#ProtocolCheckBox").is(':checked');

        if (!ValidateFormV3(ip, userName, context, authenticationValue, privacyValue, authenticationPassword, privacyPassword, port)
                || !ValidateHttpAPIForm(ip, apiCredentialName, apiUsername, apiPassword, apiConfirmPassword, apiPort, apiProtocol)) {
            $('#TestConnectionButton').attr("disabled", false);
            SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
            return;
        }

        HideMessageBoxes();

        $("#testingConnectionMsg").show();
        request = SW.SRM.Common.CallSrmWebService('/Orion/SRM/Services/EnrollmentWizardService.asmx', 'TestSnmpV3Credentials',
            {
                credentialID: credentialID, credentialName: credentialName, userName: userName,
                context: context, authenticationValue: authenticationValue, privacyValue: privacyValue,
                authenticationPassword: authenticationPassword, authenticationKeyIsPassword: authenticationKeyIsPassword,
                privacyPassword: privacyPassword, privacyKeyIsPassword: privacyKeyIsPassword, ip: ip, port: port, saveProvider: runDiscovery, groupId: "<%= DeviceGroupId %>"
            },
            function (result) {
                if (result.IsSuccess) {
                    OnTestIsilonHttpCredentials(isRequiredPostBack, runDiscovery, apiCredentialID, apiCredentialName, apiUsername, apiPassword, apiPort, apiProtocol, ip, runDiscovery);
                } else {
                    $("#testingConnectionMsg").hide();
                    $('#TestConnectionButton').attr("disabled", false);

                    ShowFailMessageBox(result.Message, result.ErrorMessage);
                    SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
                }
            },
            function () {
                $("#testingConnectionMsg").hide();
                $('#TestConnectionButton').attr("disabled", false);

                if (request.status !== 0) {
                    ShowFailMessageBox("<%= SrmWebContent.Provider_Step_Unexpected_Error%>");
                    SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
                }
            });
    }

    function OnTestIsilonHttpCredentials(isRequiredPostBack, runDiscovery, credentialID, credentialName, username, password, port, protocol, ip, runDiscovery) {
     
        request = SW.SRM.Common.CallSrmWebService('/Orion/SRM/Services/EnrollmentWizardService.asmx', 'TestIsilonHttpCredentials',
            {
                credentialID: credentialID, credentialName: credentialName, userName: username, ip: ip, port: port, useHttp: protocol, password: password, saveProvider: runDiscovery === true
            },
            function (result) {
                $("#testingConnectionMsg").hide();
                $('#TestConnectionButton').attr("disabled", false);
                if (result.IsSuccess) {
                    ShowPassMessageBox(result.Message);
                    if (isRequiredPostBack) {
                        providerStepSubmit();
                    }
                    if (runDiscovery)
                        SW.SRM.DiscoveryValidation.startDiscovery();

                    else
                        SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
                } else {
                    ShowFailMessageBox(result.Message, result.ErrorMessage);
                    SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
                }
            },
            function () {
                $("#testingConnectionMsg").hide();
                $('#TestConnectionButton').attr("disabled", false);

                if (request.status !== 0) {
                    ShowFailMessageBox("<%= SrmWebContent.Provider_Step_Unexpected_Error%>");
                }
                SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
            });
    }

    function OnCancelClick() {
                $("#testingConnectionMsg").hide();
                if (request) {
                    request.abort();
                }
            }

            function HideMessageBoxes() {
                $("#testConnectionPassMsg").hide();
                $("#testConnectionFailMsg").hide();
            }

            function ShowPassMessageBox(text) {
                $("#passMessageContent").empty();
                $("#passMessageContent").append(text);
                $("#testConnectionPassMsg").show();
            }

            function ShowFailMessageBox(message, errorMessage) {
                var ip = $("#IpAddressTextBox").val();
                var connErrorMsg = "<%= Resources.SrmWebContent.Provider_Step_Provider_Connecting_Error %>:";

        var title = connErrorMsg.concat(" ").concat(ip);
        SW.SRM.Common.ShowError(title, message, errorMessage);
    }

    function selectCredentials(ddl, credentialName) {
        ddl.find('option:contains("' + credentialName + '")').attr("selected", true);
        ddl.trigger('change');
    }

    function focusCredentialsDisplayName(txtBox) {
        txtBox.focus();
    };

    function comboHasText(ddl, text) {
        var exists = false;
        ddl.find('option').each(function () {
            if ($(this).text().toLowerCase() == text.toLowerCase()) {
                exists = true;
                return false;
            }
        });
        return exists;
    }

    function ValidateFormV1(ipAddress, credentialName, communityString, port) {
        var title = "<%= Resources.SrmWebContent.ProviderStep_Validation_Error %>";

        if (!ipAddress.trim()) {
            SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.ProviderStep_ProviderIpAddress_Required_Field_Validator %>");
            return false;
        }

        if ($("#SnmpCredentialDropDown").val().trim() == "<%= SRMConstants.NewCredential %>") {
            if (!credentialName.trim()) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.ProviderStep_ProviderCredentialNameStr_Required_Field_Validator %>");
                return false;
            }

            if (!communityString.trim()) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.ProviderStep_ProviderCommunityStr_Required_Field_Validator %>");
                return false;
            }

            if (!port.trim()) {
                    SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.ProviderStep_ProviderPortStr_Required_Field_Validator %>");
                return false;
            }

            var portInt = parseInt(port);
            if (!$.isNumeric(portInt) || Math.floor(portInt) !== portInt || portInt < 1 || portInt > 65535 || portInt.toString() !== port) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.Add_Provider_NotValidPort_Validator %>".replace('{0}',
                    "<%= SrmWebContent.Provider_Step_Array_Snmp_Port%>"));
                return false;
            }

            var alreadyExists = comboHasText($('#SnmpCredentialDropDown'), credentialName);
            if (alreadyExists) {
                title = "<%= SrmWebContent.Add_Provider_CredentialNameAlreadyExistHeader %>";
                var message = "<a href='javascript:selectCredentials($(&quot;#SnmpCredentialDropDown&quot;), &quot;" + credentialName + "&quot;)'><%= 
                    SrmWebContent.Add_Provider_CredentialNameAlreadyExist_LoadSaved %></a> or <a href='javascript:focusCredentialsDisplayName($(&quot;#CredentialNameTextBox&quot;))'><%= 
                    SrmWebContent.Add_Provider_CredentialNameAlreadyExist_RenameDuplicate %></a>";
                SW.SRM.Common.ShowError(title, message);
                return false;
            }
        }

        return true;
    }

    function ValidateFormV3(ipAddress, username, context, isAuthenticationRequired, isPrivacyRequired, authenticationPassword, privacyPassword, port) {
        var title = "<%= Resources.SrmWebContent.ProviderStep_Validation_Error %>";

        if (!ipAddress.trim()) {
            SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.ProviderStep_ProviderIpAddress_Required_Field_Validator %>");
            return false;
        }

        if ($("#SnmpCredentialDropDownSnmpV3").val().trim() == "<%= SRMConstants.NewCredential %>") {
            var credDisplayName = $('#SnmpV3CredentialNameTextBox').val();

            if (!credDisplayName.trim()) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.ProviderStep_ProviderCredentialNameStr_Required_Field_Validator %>");
                return false;
            }

            var alreadyExists = comboHasText($('#SnmpCredentialDropDownSnmpV3'), credDisplayName);
            if (alreadyExists) {
                title = "<%= SrmWebContent.Add_Provider_CredentialNameAlreadyExistHeader %>";
                var message = "<a href='javascript:selectCredentials($(&quot;#SnmpCredentialDropDownSnmpV3&quot;),&quot;" + credDisplayName + "&quot;)'><%= 
                    SrmWebContent.Add_Provider_CredentialNameAlreadyExist_LoadSaved %></a> or <a href='javascript:focusCredentialsDisplayName($(&quot;#SnmpV3CredentialNameTextBox&quot;))'><%= 
                    SrmWebContent.Add_Provider_CredentialNameAlreadyExist_RenameDuplicate %></a>";
                SW.SRM.Common.ShowError(title, message);
                return false;
            }

            if (!username.trim()) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.ProviderStep_ProviderUsername_Required_Field_Validator %>");
                return false;
            }

            if (!context.trim()) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.ProviderStep_ProviderSNMPContext_Required_Field_Validator %>");
                return false;
            }

            if (!port.trim()) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.ProviderStep_ProviderPortStr_Required_Field_Validator %>");
                return false;
            }

            var portInt = parseInt(port);
            if (!$.isNumeric(portInt) || Math.floor(portInt) !== portInt || portInt < 1 || portInt > 65535 || portInt.toString() !== port) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.Add_Provider_NotValidPort_Validator %>".replace('{0}',
                    "<%= SrmWebContent.Provider_Step_Array_Snmp_Port%>"));
                return false;
            }

            if (isAuthenticationRequired != "<%= ((int)SolarWinds.Orion.Core.Common.Models.SNMPv3AuthType.None).ToString() %>") {
                if (!authenticationPassword.trim()) {
                    SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.ProviderStep_ProviderAuthenticationPassword_Required_Field_Validator %>");
                    return false;
                }
            }

            if (isPrivacyRequired != "<%= ((int)SolarWinds.Orion.Core.Common.Models.SNMPv3PrivacyType.None).ToString() %>") {
                if (!privacyPassword.trim()) {
                    SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.ProviderStep_ProviderPrivacyPassword_Required_Field_Validator %>");
                    return false;
                }
            }
        }
        return true;
    }

    function ValidateHttpAPIForm(ipAddress, credentialName, usernameVar, passwordVar, confirmPasswordVar, port, protocol) {
        var title = "<%= Resources.SrmWebContent.ProviderStep_Validation_Error %>";

        if (!ipAddress.trim()) {
            SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.ProviderStep_ProviderIpAddress_Required_Field_Validator %>");
                return false;
            }

        if ($("#ChooseCredentialEmcHttp").val().trim() == "<%= SRMConstants.NewCredential %>") {
            if (!credentialName.trim()) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.ProviderStep_ProviderCredentialNameStr_Required_Field_Validator %>");
                    return false;
                }

            if (!usernameVar.trim()) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.Add_Provider_Required_Field_Validator %>".replace('{0}', $("#labelEmcHttpProviderUserName").text()));
                return false;
            }

            if (!passwordVar.trim()) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.Add_Provider_Required_Field_Validator %>".replace('{0}', $("#labelEmcHttpProviderPassword").text()));
                return false;
            }

            if (!(passwordVar === confirmPasswordVar)) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.Add_Provider_ConfirmApiPassword_CompareField_Validator %>");
                return false;
            }

            if (!port.trim()) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.ProviderStep_ProviderPortStr_Required_Field_Validator %>");
                return false;
            }

            var portInt = parseInt(port);
            if (!$.isNumeric(portInt) || Math.floor(portInt) !== portInt || portInt < 1 || portInt > 65535 || portInt.toString() !== port) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.Add_Provider_NotValidPort_Validator %>".replace('{0}', $("#labelEmcHttpPort").text()));
                return false;
            }

            var alreadyExists = comboHasText($('#ChooseCredentialEmcHttp'), credentialName);
            if (alreadyExists) {
                    title = "<%= SrmWebContent.Add_Provider_CredentialNameAlreadyExistHeader %>";
                var message = "<a href='javascript:selectCredentials($(&quot;#ChooseCredentialEmcHttp&quot;), &quot;" + credentialName + "&quot;)'><%= 
                        SrmWebContent.Add_Provider_CredentialNameAlreadyExist_LoadSaved %></a> or <a href='javascript:focusCredentialsDisplayName($(&quot;#labelEmcHttpDisplayNameTextBox&quot;))'><%= 
                        SrmWebContent.Add_Provider_CredentialNameAlreadyExist_RenameDuplicate %></a>";
                SW.SRM.Common.ShowError(title, message);
                return false;
            }
        }

        return true;
    }

</script>
