﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.UI;

using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.EnrollmentWizard;
using SolarWinds.SRM.Web.Interfaces;
using Resources;
using SolarWinds.SRM.Common;

public partial class Orion_SRM_Config_Controls_ProviderStep : System.Web.UI.UserControl, IConfigurationControl
{
    private Configuration configuration;

    private const string ChooseDataSourceSubtitleKey = "ChooseDataSourceSubtitle";
    private string DataSourceSubTitle
    {
        get
        {
             return configuration.DeviceGroup.Properties.ContainsKey(ChooseDataSourceSubtitleKey)
                ? configuration.DeviceGroup.Properties[ChooseDataSourceSubtitleKey]
                : null;
        }
    }
    private Control providerControl;

    public string Provider_Step_Add_Btn_Label;
    public string Provider_Step_Add_Provider_Title;
    private static readonly IDeviceGroupDAL DeviceGroupDal = ServiceLocator.GetInstance<IDeviceGroupDAL>();
    public DeviceGroup Group { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        configuration = WizardWorkflowHelper.ConfigurationInfo;

        this.ArrayTypeLabel.Text = configuration.DeviceGroup.Name;
        this.Group = DeviceGroupDal.GetDeviceGroupDetails(configuration.DeviceGroup.GroupId);
        this.familyTypeLink.HRef = HelpHelper.GetHelpUrl(this.Group.LinkURL);
      
        if (configuration.DeviceGroup.FlowType == FlowType.Smis || configuration.DeviceGroup.FlowType == FlowType.SmisEmcUnified)
        {
            this.providerSubTitle.Text = DataSourceSubTitle ?? string.Format(CultureInfo.InvariantCulture,
                SrmWebContent.AddStorage_ProviderStep_EmcClariion_SubTitle,
                configuration.DeviceGroup.Name);

            providerControl = LoadControl("~/Orion/SRM/Config/Controls/AddSmisProvider.ascx");
            var provider = ((Orion_SRM_Config_Controls_AddSmisProvider)providerControl);
            provider.DeviceGroup = configuration.DeviceGroup;
            this.Provider_Step_Add_Btn_Label = provider.AddProviderToolbarLabel;
            this.Provider_Step_Add_Provider_Title = provider.Provider_Step_Add_Provider_Title;
        }
        else if (configuration.DeviceGroup.FlowType == FlowType.NetAppDfm)
        {
            this.providerSubTitle.Text = SrmWebContent.AddStorage_ProviderStep_DFM_SubTitle;

            providerControl = LoadControl("~/Orion/SRM/Config/Controls/AddDfmProvider.ascx");
            var provider = ((Orion_SRM_Config_Controls_AddDfmProvider)providerControl);
            provider.DeviceGroup = configuration.DeviceGroup;
            this.Provider_Step_Add_Btn_Label = provider.AddProviderToolbarLabel;
            this.Provider_Step_Add_Provider_Title = provider.Provider_Step_Add_Provider_Title;
        }
        else if (configuration.DeviceGroup.FlowType == FlowType.XtremIO)
        {
            this.providerSubTitle.Text = string.Format(CultureInfo.InvariantCulture,
                SrmWebContent.AddStorage_ProviderStep_XtremIo_SubTitle,
                configuration.DeviceGroup.Name);

            providerControl = LoadControl("~/Orion/SRM/Config/Controls/AddXtremIoProvider.ascx");
            var provider = ((Orion_SRM_Config_Controls_AddXtremIoProvider)providerControl);
            provider.DeviceGroup = configuration.DeviceGroup;
            this.Provider_Step_Add_Btn_Label = provider.AddProviderToolbarLabel;
            this.Provider_Step_Add_Provider_Title = provider.Provider_Step_Add_Provider_Title;
        } 
        else if (this.configuration.DeviceGroup.FlowType == FlowType.SmisAndGenericHttpExternal)
        {
            this.providerSubTitle.Text = string.Format(CultureInfo.InvariantCulture,
                SrmWebContent.AddStorage_ProviderStep_SmisAndGenericHttpExternal_SubTitle,
                configuration.DeviceGroup.Name);

            providerControl = LoadControl("~/Orion/SRM/Config/Controls/AddSmisAndGenericHttpProvider.ascx");
            var provider = ((Orion_SRM_Config_Controls_AddSmisAndGenericHttpProvider)providerControl);
            this.Provider_Step_Add_Btn_Label = provider.AddProviderToolbarLabel;
            this.Provider_Step_Add_Provider_Title = provider.Provider_Step_Add_Provider_Title;
        }

        AddProviderBody.Controls.Add(providerControl);

        if (configuration.ProviderConfiguration.SelectedProviders.Count != 0)
        {
            SelectedProviderID.Value = configuration.ProviderConfiguration.SelectedProviders[0].UniqueProviderIdentifier.ToString();
        }
    }

    public void SaveValues()
    {
        configuration.ProviderConfiguration.SelectedProviders.Clear();

        var providers =
            SelectedProviderID.Value.Split(new []{ SRMConstants.ProviderIdDelimiter }, StringSplitOptions.RemoveEmptyEntries)
                .Select(id => GetProviderById(Guid.Parse(id)));

        configuration.ProviderConfiguration.SelectedProviders.AddRange(providers);
    }

    public string ValidationError { get; private set; }
    public bool ValidateUserInput()
    {
        var selectedProviderIds = SelectedProviderID.Value;
        var ids = selectedProviderIds.Split(new[] {SRMConstants.ProviderIdDelimiter}, StringSplitOptions.RemoveEmptyEntries);

        Guid uniqueProviderIdentifier;
        foreach (var id in ids)
        {
            if (!Guid.TryParse(id, out uniqueProviderIdentifier))
            {
                this.ValidationError = SrmWebContent.Provider_Step_Server_Validation_Message;
                return false;
            }
        }

        return true;
    }

    private EnrollmentWizardProvider GetProviderById(Guid uniqueProviderIdentifier)
    {
        return configuration.ProviderConfiguration.AvailableProviderEntities.FirstOrDefault((p) => p.UniqueProviderIdentifier.Equals(uniqueProviderIdentifier));
    }
}
