﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddSmisAndGenericHttpProvider.ascx.cs" Inherits="Orion_SRM_Config_Controls_AddSmisAndGenericHttpProvider" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Import Namespace="SolarWinds.SRM.Common" %>
<%@ Register TagPrefix="UIInfo" TagName="DeviceType" Src="~/Orion/SRM/Config/Controls/DeviceType.ascx" %>
<%@ Register Src="~/Orion/SRM/Config/Controls/TestConnectionMessages.ascx" TagPrefix="UIInfo" TagName="TestConnectionMessages" %>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="SRM/Config/Controls/DiscoveryValidation.js" />
<orion:Include runat="server" File="SRM/Config/Controls/AddSmisAndGenericHttpProvider.js" />
<orion:Include runat="server" File="SmisAndGenericHttpStep.css" Module="SRM" />


<asp:HiddenField runat="server" ID="txtProviderNoDataAvailable" ClientIDMode="Static" />
<div class="srm-main-content">
    <table class="srm-EnrollmentWizard-providerStepContentTable">
        <tr>
            <td class="providerPropertiesColumn">
                <table class="srm-EnrollmentWizard-providerStepInputsTable">
                    <tr>
                        <td class="label">
                            <span>
                                <asp:Label ID="uxIpAddressOrHostnameLabel"
                                    Text="<%$ Resources:SrmWebContent, Add_Provider_IP_Address_Hostname %>"
                                    ClientIDMode="Static"
                                    runat="server"></asp:Label>*:</span>
                        </td>
                        <td>
                            <span>
                                <asp:TextBox ID="uxIpAddressOrHostname" ClientIDMode="Static" runat="server" /></span>
                        </td>
                    </tr>
                </table>
                <div class="CredentialsLabel">
                    <span><b><%= SrmWebContent.Provider_Step_Celerra_SMISCredentials%></b></span>&nbsp;
                    <asp:Image ID="Image1" src="/Orion/images/Icon.Info.gif" Visible="false" runat="server" ClientIDMode="Static" />
                </div>
                <table class="srm-EnrollmentWizard-providerStepInputsTable sub-table">
                    <tr>
                        <td class="label"><span><%= SrmWebContent.Add_Provider_Choose_Credential %>*:</span></td>
                        <td>
                            <span>
                                <asp:DropDownList runat="server"
                                    ID="uxSmisCredentialsChoice"
                                    OnChange="SW.SRM.AddSmisAndHttpProvider.toggleSettingsVisibility()"
                                    ClientIDMode="Static" />
                            </span>
                        </td>
                    </tr>
                </table>
                <table id="uxSmisCredentialsContainer" class="srm-EnrollmentWizard-providerStepInputsTable sub-table">
                    <tr>
                        <td class="label"><span>
                            <asp:Label ID="uxSmisCredentialNameLabel"
                                Text="<%$ Resources:SrmWebContent, Add_Provider_DisplayName %>"
                                ClientIDMode="Static"
                                runat="server"></asp:Label>*:</span>
                        </td>
                        <td>
                            <span>
                                <asp:TextBox ID="uxSmisCredentialName" runat="server" ClientIDMode="Static" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span>
                            <asp:Label ID="uxSmisUsernameLabel"
                                Text="<%$ Resources:SrmWebContent, Add_Provider_UsernameLabel %>"
                                ClientIDMode="Static"
                                runat="server"></asp:Label>*:</span></td>
                        <td>
                            <span>
                                <asp:TextBox ID="uxSmisUsername"
                                    runat="server"
                                    ClientIDMode="Static"
                                    onkeyup="SW.SRM.AddSmisAndHttpProvider.copySmisUsernameToApi();" />
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span>
                            <asp:Label ID="uxSmisPasswordLabel"
                                Text="<%$ Resources:SrmWebContent, Add_Provider_PasswordLabel %>"
                                ClientIDMode="Static"
                                runat="server"></asp:Label>*:</span></td>
                        <td>
                            <span>
                                <asp:TextBox TextMode="Password"
                                    ID="uxSmisPassword"
                                    runat="server"
                                    ClientIDMode="Static"
                                    onkeyup="SW.SRM.AddSmisAndHttpProvider.copySmisPasswordToApi();" />
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><%= SrmWebContent.Add_Provider_PasswordConfirmLabel %>*:</span></td>
                        <td>
                            <span>
                                <asp:TextBox TextMode="Password"
                                    ID="uxSmisPasswordConfirm"
                                    runat="server"
                                    ClientIDMode="Static"
                                    onkeyup="SW.SRM.AddSmisAndHttpProvider.copySmisPasswordConfirmToApi();" />
                            </span>
                        </td>
                    </tr>
                </table>
                <div class="CredentialsLabel">
                    <span><b><%= SrmWebContent.Provider_Step_Celerra_APICredentials %></b></span>&nbsp;
                    <asp:Image ID="Image2" src="/Orion/images/Icon.Info.gif" Visible="false" runat="server" ClientIDMode="Static" />
                </div>
                <div id="uxApiUseSmisCredentialsContainer">
                    <span>
                        <input type="checkbox"
                            id="uxApiUseSmisCredentials"
                            runat="server"
                            clientidmode="Static"
                            onchange="SW.SRM.AddSmisAndHttpProvider.useSmisCredentialsForApi($(this).is(':checked'));" />
                        <span id="UseSameCredentialsLabel"><%= SrmWebContent.Add_Provider_UseSmisUsernameAndPassword %></span>
                    </span>
                </div>
                <table class="srm-EnrollmentWizard-providerStepInputsTable sub-table">
                    <tr>
                        <td class="label"><span><%= SrmWebContent.Add_Provider_Choose_Credential %>*:</span></td>
                        <td>
                            <span>
                                <asp:DropDownList ID="uxApiCredentialsChoice"
                                    onchange="SW.SRM.AddSmisAndHttpProvider.toggleSettingsVisibility()"
                                    runat="server"
                                    ClientIDMode="Static" />
                            </span>
                        </td>
                    </tr>
                </table>
                <table id="uxApiCredentialsContainer" class="srm-EnrollmentWizard-providerStepInputsTable sub-table">
                    <tr>
                        <td class="label">
                            <span>
                                <asp:Label ID="uxApiCredentialNameLabel"
                                    runat="server"
                                    ClientIDMode="Static"
                                    Text="<%$ Resources:SrmWebContent, Add_Provider_DisplayName %>"></asp:Label>*:</span>
                        </td>
                        <td>
                            <span>
                                <asp:TextBox ID="uxApiCredentialName" runat="server" ClientIDMode="Static" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <span>
                                <asp:Label ID="uxApiUsernameLabel"
                                    runat="server"
                                    ClientIDMode="Static"
                                    Text="<%$ Resources:SrmWebContent, Add_Provider_UsernameLabel %>"></asp:Label>*:</span>
                        </td>
                        <td>
                            <span>
                                <asp:TextBox ID="uxApiUsername" runat="server" ClientIDMode="Static" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <span>
                                <asp:Label ID="uxApiPasswordLabel"
                                    runat="server"
                                    ClientIDMode="Static"
                                    Text="<%$ Resources:SrmWebContent, Add_Provider_PasswordLabel %>"></asp:Label>*:</span>
                        </td>
                        <td>
                            <span>
                                <asp:TextBox ID="uxApiPassword" TextMode="Password" runat="server" ClientIDMode="Static" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <span>
                                <asp:Label ID="uxApiPasswordConfirmLabel"
                                    runat="server"
                                    ClientIDMode="Static"
                                    Text="<%$ Resources:SrmWebContent, Add_Provider_ArrayAPIConfirmPassword %>" />*:
                            </span>
                        </td>
                        <td>
                            <span>
                                <asp:TextBox TextMode="Password" ID="uxApiPasswordConfirm" runat="server" ClientIDMode="Static" /></span>
                        </td>
                    </tr>
                </table>
                <div id="uxExpander">
                    <a href="javascript:SW.SRM.AddSmisAndHttpProvider.toggleExpander();">
                        <img src="/Orion/SRM/images/Button.Expand.gif" alt="[+]" /><%= SrmWebContent.Provider_Step_AddProvider_Advanced %>
                    </a>
                </div>
                <div class="advanced-settings-wrapper">
                    <div class="first" id="uxAdvancedSmisSettingsContainer">
                        <div class="CredentialsLabel">
                            <span><b>SMI-S</b></span>&nbsp;
                            <asp:Image ID="Image3" src="/Orion/images/Icon.Info.gif" Visible="false" runat="server" ClientIDMode="Static" />
                        </div>
                        <table class="srm-EnrollmentWizard-providerStepInputsTable sub-table">
                            <tr>
                                <td class="label"><span><%= SrmWebContent.Add_Provider_Protocol%></span></td>
                                <td>
                                    <span>
                                        <input type="checkbox"
                                            id="uxSmisUseHttp"
                                            class="ProtocolCheckBox"
                                            runat="server"
                                            clientidmode="Static" />
                                        <label for="uxSmisUseHttp"><%= SrmWebContent.Provider_Step_UseHttp_Label %></label>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="label"><span><span id="uxSmisHttpPortLabel"><%= SrmWebContent.Add_Provider_Http_Port%></span>*:</span></td>
                                <td><span>
                                    <asp:TextBox ID="uxSmisHttpPort" runat="server" ClientIDMode="Static" /></span></td>
                            </tr>
                            <tr>
                                <td class="label"><span><span id="uxSmisHttpsPortLabel"><%= SrmWebContent.Add_Provider_Https_Port%></span>*:</span></td>
                                <td><span>
                                    <asp:TextBox ID="uxSmisHttpsPort" runat="server" ClientIDMode="Static" /></span></td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <span><span id="uxSmisInteropNamespaceLabel"><%= SrmWebContent.Add_Provider_Interop_Namespace%></span>*:</span>
                                </td>
                                <td>
                                    <span>
                                        <asp:TextBox ID="uxSmisInteropNamespace" runat="server" ClientIDMode="Static" /></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <span><span id="uxSmisArrayNamespaceLabel"><%= SrmWebContent.Add_Provider_Array_Namespace%></span>*:</span>
                                </td>
                                <td>
                                    <span>
                                        <asp:TextBox ID="uxSmisArrayNamespace" runat="server" ClientIDMode="Static" /></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="second" id="uxAdvancedApiSettingsContainer">
                        <div class="CredentialsLabel">
                            <span><b>API</b></span>&nbsp;
                            <asp:Image ID="Image4" src="/Orion/images/Icon.Info.gif" Visible="false" runat="server" ClientIDMode="Static" />
                        </div>
                        <table class="srm-EnrollmentWizard-providerStepInputsTable sub-table">
                            <tr runat="server" id="uxApiProtocolChoiceRow">
                                <td class="label"><span><%= SrmWebContent.Add_Api_Provider_Protocol%></span></td>
                                <td>
                                    <span>
                                        <input type="checkbox" id="uxApiUseHttp" class="ProtocolCheckBox" runat="server" clientidmode="Static" /><label for="ApiProtocolCheckBox"> <%= SrmWebContent.Provider_Step_UseHttp_Label %></label></span>
                                </td>
                            </tr>
                            <tr runat="server" id="uxApiHttpPortRow">
                                <td class="label"><span><span id="uxApiHttpPortLabel"><%= SrmWebContent.Add_Provider_ArrayAPIHttpPort %></span>*:</span></td>
                                <td>
                                    <span>
                                        <asp:TextBox ID="uxApiHttpPort" runat="server" ClientIDMode="Static" /></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="label"><span><span id="uxApiHttpsPortLabel"><%= SrmWebContent.Add_Provider_ArrayAPIHttpsPort %></span>*:</span></td>
                                <td>
                                    <span>
                                        <asp:TextBox ID="uxApiHttpsPort" runat="server" ClientIDMode="Static" /></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <table class="PollingPropertiesSection">
                    <tr>
                        <td></td>
                        <td>
                            <input 
                                type="button"
                                id="uxTestConnection"
                                class="providerStepButton"
                                value="<%= SrmWebContent.Provider_Step_Test_Connection %>"
                                onclick="<%= this.testCredentialMethod %>" />
                        </td>
                    </tr>
                </table>
                <UIInfo:TestConnectionMessages runat="server"
                    ID="uxProviderTestMessages"
                    ContainerId="testProvider"
                    JsCancellationFunction="javascript:SW.SRM.AddSmisAndHttpProvider.cancelTestCredentials();" />
            </td>
            <td class="providerPropertiesColumn">
                <UIInfo:DeviceType runat="server" ID="uxDeviceTypeInformation" />
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        SW.SRM.AddSmisAndHttpProvider.init({ 
            deviceGroupId: '<%= DeviceGroup.GroupId %>',
            connectionTestFailedTitle: '<%= SrmWebContent.Provider_Step_Provider_Connecting_Error %>',
            connectionTestUnexpectedFailedMessage: '<%= SrmWebContent.Provider_Step_Unexpected_Error %>',
            smisHttpPort: <%= DeviceGroup.SmisHttpPort %>,
            smisHttpsPort: <%= DeviceGroup.SmisHttpsPort %>,
            smisArrayNamespace: '<%= DeviceGroup.DefaultNamespace %>',
            smisInteropNamespace: '<%= DeviceGroup.InteropNamespace %>',
            apiHttpPort: <%= DeviceGroup.ApiHttpPort %>,
            apiHttpsPort: <%= DeviceGroup.ApiHttpsPort %>,
            isEditProviderPrortiesPage: '<%= this.isInitializedFromNetObjectID %>'
        });
        $("#addNewProviderBtn").click(function () {
            SW.SRM.AddSmisAndHttpProvider.showDialog();
            return false;
        });

        $("#addNewProviderLabel").click(function () {
            SW.SRM.AddSmisAndHttpProvider.showDialog();
            return false;
        });

        $(".srm-testing-connection-msgbox").hide();
    
    });
</script>
