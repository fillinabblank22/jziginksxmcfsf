﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GenericRestProviderStep.ascx.cs" Inherits="Orion_SRM_Config_Controls_GenericRestProviderStep" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common" %>
<%@ Register Src="~/Orion/SRM/Config/Controls/TestConnectionMessages.ascx" TagPrefix="UIInfo" TagName="TestConnectionMessages" %>
<%@ Register TagPrefix="UIInfo" TagName="DeviceType" Src="~/Orion/SRM/Config/Controls/DeviceType.ascx" %>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include ID="Include" runat="server" File="SRM/Config/Controls/DiscoveryValidation.js" />
<orion:Include runat="server" File="Discovery.css" Module="SRM" />
<orion:Include runat="server" File="GenericRestProviderStep.css" Module="SRM" />

<span><%= SubTitle%></span>

<div class="srm-enrollment-wizard-content">
    <div class="srm-main-content PollingPropertiesSection">
        <table class="srm-EnrollmentWizard-providerStepContentTable">
            <tr>
                <td>
                    <table class="srm-EnrollmentWizard-providerStepInputsTable">
                        <tr>
                            <td><%= SrmWebContent.Provider_Step_Array_Type%>:</td>
                            <td>
                                <b>
                                    <asp:Label runat="server" ID="ArrayTypeName" />
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td><%= SrmWebContent.Provider_Step_Array_Ip%>*:</td>
                            <td>
                                <asp:TextBox runat="server" ID="IpAddressTextBox" ClientIDMode="Static" />
                            </td>
                        </tr>
                    </table>

                    <div class="CredentialsLabel"><span><b><%= SrmWebContent.Provider_Step_Generic_APICredentials%></b></span>&nbsp;<asp:Image ID="ApiCredentialsTooltip" src="/Orion/images/Icon.Info.gif" Visible="false" runat="server" ClientIDMode="Static" /></div>
                    <table class="srm-EnrollmentWizard-providerStepInputsTable sub-table">
                        <tr>
                            <td><span><%= SrmWebContent.Provider_Step_Generic_ChooseAPICredentials%>*:</span></td>
                            <td>
                                <span>
                                    <asp:DropDownList ID="ChooseCredentialGenericHttp" onchange="ChooseCredentialGenericHttp_OnChange(this)" runat="server" ClientIDMode="Static" /></span>
                            </td>
                        </tr>
                    </table>
                    <table id="StoredCredFieldsTableGenericHttp" class="srm-EnrollmentWizard-providerStepInputsTable sub-table">
                        <tr>
                            <td><span>
                                <asp:Label ID="labelGenericHttpDisplayNameTextBox" runat="server" ClientIDMode="Static"></asp:Label>*:</span></td>
                            <td>
                                <span>
                                    <asp:TextBox ID="DisplayNameTextBox" runat="server" ClientIDMode="Static" /></span>
                            </td>
                        </tr>
                        <tr>
                            <td><span>
                                <asp:Label ID="labelGenericHttpProviderUserName" runat="server" ClientIDMode="Static"></asp:Label>*:</span></td>
                            <td>
                                <span>
                                    <asp:TextBox ID="ArrayApiUsernameTextBox" runat="server" ClientIDMode="Static" /></span>
                            </td>
                        </tr>
                        <tr>
                            <td><span>
                                <asp:Label ID="labelGenericHttpProviderPassword" runat="server" ClientIDMode="Static"></asp:Label>*:</span></td>
                            <td>
                                <span>
                                    <asp:TextBox TextMode="Password" ID="ArrayApiPasswordTextBox" runat="server" ClientIDMode="Static" /></span>
                            </td>
                        </tr>
                        <tr>
                            <td><span id="labelGenericHttpConfirmProviderPassword"><%= SrmWebContent.Add_Provider_ArrayAPIConfirmPassword %>*:</span></td>
                            <td>
                                <span>
                                    <asp:TextBox TextMode="Password" ID="ArrayApiConfirmPasswordTextBox" runat="server" ClientIDMode="Static" /></span>
                            </td>
                        </tr>
                    </table>

                    <table id="AdvancedGenericHttpApiCredFiledsTable" class="srm-EnrollmentWizard-providerStepInputsTable sub-table">
                        <tr>
                            <td><span><%= SrmWebContent.Add_Provider_Protocol%></span></td>
                            <td>
                                <span><input type="checkbox" id="ProtocolCheckBox" class="ProtocolCheckBox" runat="server" ClientIDMode="Static"/><label for="ProtocolCheckBox"> <%= SrmWebContent.Provider_Step_UseHttp_Label %></label></span>
                            </td>
                        </tr>
                        <tr>
                            <td><span><span id="labelGenericHttpPort"><%= SrmWebContent.Add_Provider_ArrayAPIPort%></span>*:</span></td>
                            <td>
                                <span>
                                    <asp:TextBox ID="HttpPortTextBox" runat="server" ClientIDMode="Static" /></span>
                            </td>
                        </tr>
                    </table>

                    <table class="srm-EnrollmentWizard-providerStepInputsTable">
                        <tr>
                            <td />
                            <td>
                                <input type="button" id="TestConnectionButton" class="providerStepButton" value="<%= SrmWebContent.Provider_Step_Test_Connection %>" onclick="OnTestButtonClick(false);" />
                            </td>
                        </tr>
                    </table>
                    <UIInfo:TestConnectionMessages runat="server" ID="TestConnectionMessages" JsCancellationFunction="javascript:OnCancelClick();" />
                </td>
                <td>
                    <UIInfo:DeviceType runat="server" ID="deviceInfo"></UIInfo:DeviceType>
                </td>
            </tr>
        </table>
    </div>
</div>

<script type="text/javascript">
    var genericHttpProvider = SW.Core.namespace("SW.SRM.EditProperties.GenericHttp");

    onClickNextButtonEvent = function () {
        OnTestHttpCredentials(true, false);
    };

    function ChooseCredentialGenericHttp_OnChange(ddl) {
        var $containerElement = $('#AdvancedGenericHttpApiCredFiledsTable');

        if (ddl.value !== "<%= SRMConstants.NewCredential %>") {
            $("#StoredCredFieldsTableGenericHttp").hide();
            $containerElement.hide();
        } else {
            $("#StoredCredFieldsTableGenericHttp").show();
            $containerElement.show();
        }
    }


    function OnCredentialSelected(ddlId, fn) {
        var ddl = $('#' + ddlId);
        var ddlText = $(ddl).find(':selected').val();
        if (ddlText != "<%= SRMConstants.NewCredential %>") {
            fn(ddl);
        }
    }

    $(document).ready(function () {
        $(".srm-testing-connection-msgbox").hide();
        ChooseCredentialGenericHttp_OnChange({ value: $('#ChooseCredentialGenericHttp').val() });
    });

    function OnTestButtonClick(runDiscovery) {
        OnTestHttpCredentials(false, runDiscovery);
    }

    genericHttpProvider.ValidateCredentials = function () {
        var testCredentialDeferred = $.Deferred();
        SW.SRM.DiscoveryValidation.init("<%= ArrayID %>", "<%= ArraySerialNumber %>", "<%= DeviceGroupId %>", request, testCredentialDeferred, "<%= SrmWebContent.Provider_Step_Unexpected_Error%>", ShowFailMessageBox);
        var globalDiscoveryPromise = $.Deferred();
        OnTestButtonClick(true);

        $.when(testCredentialDeferred).then(function (x) {
            return globalDiscoveryPromise.resolve();

        }, function (x) {
            return globalDiscoveryPromise.reject();
        });

        return globalDiscoveryPromise.promise();
    }

    genericHttpProvider.MonitoredControlIds = [
        'pollEngineDescr',
        'IpAddressTextBox',
        'ChooseCredentialGenericHttp', 'ArrayApiUsernameTextBox', 'ArrayApiPasswordTextBox'
    ];
    SW.SRM.EditProperties.ProviderChangeManager.InitProvider(genericHttpProvider, genericHttpProvider.ValidateCredentials);

    var request;

    function OnTestHttpCredentials(isRequiredPostBack, runDiscovery) {

        $('#TestConnectionButton').attr("disabled", true);

        var ip = $.trim(String($("#IpAddressTextBox").val()));

        var apiCredentialID = $('#ChooseCredentialGenericHttp').val();
        var apiCredentialName = $("#DisplayNameTextBox").val();
        var apiUsername = $("#ArrayApiUsernameTextBox").val();
        var apiPassword = $("#ArrayApiPasswordTextBox").val();
        var apiConfirmPassword = $("#ArrayApiConfirmPasswordTextBox").val();
        var apiPort = $("#HttpPortTextBox").val();
        var apiProtocol = $("#ProtocolCheckBox").is(':checked');

        if (!ValidateHttpAPIForm(ip, apiCredentialName, apiUsername, apiPassword, apiConfirmPassword, apiPort, apiProtocol)) {
            $('#TestConnectionButton').attr("disabled", false);
            SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
            return;
        }

        HideMessageBoxes();

        $("#testingConnectionMsg").show();

        request = SW.SRM.Common.CallSrmWebService('/Orion/SRM/Services/EnrollmentWizardService.asmx', 'TestGenericHttpCredentials',
            {
                deviceGroupID: '<%= DeviceGroupId%>',
                credentialID: apiCredentialID,
                credentialName: apiCredentialName,
                userName: apiUsername,
                ip: ip,
                port: apiPort,
                useHttp: apiProtocol,
                password: apiPassword,
                saveProvider: runDiscovery === true
            },

            function (result) {
                $("#testingConnectionMsg").hide();
                $('#TestConnectionButton').attr("disabled", false);
                if (result.IsSuccess) {
                    ShowPassMessageBox(result.Message);
                    if (isRequiredPostBack) {
                        providerStepSubmit();
                    }
                    if (runDiscovery)
                        SW.SRM.DiscoveryValidation.startDiscovery();

                    else
                        SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
                } else {
                    ShowFailMessageBox(result.Message, result.ErrorMessage);
                    SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
                }
            },
            function () {
                $("#testingConnectionMsg").hide();
                $('#TestConnectionButton').attr("disabled", false);

                if (request.status !== 0) {
                    ShowFailMessageBox("<%= SrmWebContent.Provider_Step_Unexpected_Error%>");
                }
                SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
            });
    }

    function OnCancelClick() {
                $("#testingConnectionMsg").hide();
                if (request) {
                    request.abort();
                }
            }

            function HideMessageBoxes() {
                $("#testConnectionPassMsg").hide();
                $("#testConnectionFailMsg").hide();
            }

            function ShowPassMessageBox(text) {
                $("#passMessageContent").empty();
                $("#passMessageContent").append(text);
                $("#testConnectionPassMsg").show();
            }

            function ShowFailMessageBox(message, errorMessage) {
                var ip = $("#IpAddressTextBox").val();
                var connErrorMsg = "<%= Resources.SrmWebContent.Provider_Step_Provider_Connecting_Error %>:";

        var title = connErrorMsg.concat(" ").concat(ip);
        SW.SRM.Common.ShowError(title, message, errorMessage);
    }

    function selectCredentials(ddl, credentialName) {
        ddl.find('option:contains("' + credentialName + '")').attr("selected", true);
        ddl.trigger('change');
    }

    function focusCredentialsDisplayName(txtBox) {
        txtBox.focus();
    };

    function comboHasText(ddl, text) {
        var exists = false;
        ddl.find('option').each(function () {
            if ($(this).text().toLowerCase() == text.toLowerCase()) {
                exists = true;
                return false;
            }
        });
        return exists;
    }

    function ValidateHttpAPIForm(ipAddress, credentialName, usernameVar, passwordVar, confirmPasswordVar, port, protocol) {
        var title = "<%= Resources.SrmWebContent.ProviderStep_Validation_Error %>";

        if (!ipAddress.trim()) {
            SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.ProviderStep_ProviderIpAddress_Required_Field_Validator %>");
                return false;
            }

        if ($("#ChooseCredentialGenericHttp").val().trim() == "<%= SRMConstants.NewCredential %>") {
            if (!credentialName.trim()) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.ProviderStep_ProviderCredentialNameStr_Required_Field_Validator %>");
                    return false;
                }

            if (!usernameVar.trim()) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.Add_Provider_Required_Field_Validator %>".replace('{0}', $("#labelGenericHttpProviderUserName").text()));
                return false;
            }

            if (!passwordVar.trim()) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.Add_Provider_Required_Field_Validator %>".replace('{0}', $("#labelGenericHttpProviderPassword").text()));
                return false;
            }

            if (!(passwordVar === confirmPasswordVar)) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.Add_Provider_ConfirmApiPassword_CompareField_Validator %>");
                return false;
            }

            if (!port.trim()) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.ProviderStep_ProviderPortStr_Required_Field_Validator %>");
                return false;
            }

            var portInt = parseInt(port);
            if (!$.isNumeric(portInt) || Math.floor(portInt) !== portInt || portInt < 1 || portInt > 65535 || portInt.toString() !== port) {
                SW.SRM.Common.ShowError(title, "<%= Resources.SrmWebContent.Add_Provider_NotValidPort_Validator %>".replace('{0}', $("#labelGenericHttpPort").text()));
                return false;
            }

            var alreadyExists = comboHasText($('#ChooseCredentialGenericHttp'), credentialName);
            if (alreadyExists) {
                    title = "<%= SrmWebContent.Add_Provider_CredentialNameAlreadyExistHeader %>";
                var message = "<a href='javascript:selectCredentials($(&quot;#ChooseCredentialGenericHttp&quot;), &quot;" + credentialName + "&quot;)'><%= 
                        SrmWebContent.Add_Provider_CredentialNameAlreadyExist_LoadSaved %></a> or <a href='javascript:focusCredentialsDisplayName($(&quot;#labelGenericHttpDisplayNameTextBox&quot;))'><%= 
                        SrmWebContent.Add_Provider_CredentialNameAlreadyExist_RenameDuplicate %></a>";
                SW.SRM.Common.ShowError(title, message);
                return false;
            }
        }

        return true;
    }

</script>
