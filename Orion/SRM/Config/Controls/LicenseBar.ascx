﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LicenseBar.ascx.cs" Inherits="Orion_SRM_Config_Controls_LicenseBar" %>

<orion:Include runat="server" File="SRM/Config/Controls/LicenseBar.js" />

<script type="text/javascript">
    $(document).ready(function() {
        SW.SRM.LicenseBar.init('<%= Resources.SrmWebContent.SRMEnrollmentWizard_LicenseBar_LicenseDetails %>',
                               '<%= Resources.SrmWebContent.SRMEnrollmentWizard_LicenseBar_Unlimited %>',
                               <%= IsEval ? 1 : 0 %>,
                               <%= IsExpired ? 1 : 0 %>);
    });
</script>
<style type="text/css">
    #licenseExceeded {
        display: none;
    }

    #licenseBreakerLine {
        margin-top: 5px;
    }
</style>

<b><span id="licenseContent"></span></b>
<div id="licenseBreakerLine"></div>
<span id="licenseExceeded">
    <span class="sw-suggestion sw-suggestion-fail">
        <span class="sw-suggestion-icon"></span>
        <asp:Label ID="ErrorMessageLabel" runat="server" />
    </span>
</span>

<asp:HiddenField ID="hiddenRequired" runat="server" Value="0" ClientIDMode="Static" />
<asp:HiddenField ID="hiddenInUse" runat="server" Value="0" ClientIDMode="Static" />
<asp:HiddenField ID="hiddenTotal" runat="server" Value="0" ClientIDMode="Static" />
<asp:HiddenField ID="hiddenLicViolation" runat="server" Value="0" ClientIDMode="Static" />
