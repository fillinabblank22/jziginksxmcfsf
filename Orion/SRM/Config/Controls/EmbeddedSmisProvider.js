﻿/// <reference path="~/Orion/js/jquery-1.7.1/jquery.js" />
SW = SW || {};
SW.SRM = SW.SRM || {};

SW.SRM.EmbeddedSmisProvider = function () {
    var providerType,
        connectionTestUnexpectedFailedMessage,
        newCredentialsConstant,
        request,
        arrayID,
        arraySerialNumber,
        groupId;

    var formatSmisError = function (message) {
        return "@{R=SRM.Strings;K=ConfigurationWizard_Provider_Step_Celerra_SMISCredentials_ErrorFormat; E=js}".replace('{0}', message);
    };

    var handleError = function (message, runDiscovery, errorMessage) {
        $("#testingConnectionMsg").hide();
        $('#runningDiscoveryMsg').hide();
        $("#testConnectionButton").removeAttr('disabled');

        if (request == null || request.status !== 0) {
            showFailMessageBox(message, errorMessage);
        }
        SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
    };

    var credentialsValidation = function (selectedCredential, selectedCredentialOptionsArr, displayNameVar, usernameVar, passwordVar, confirmPasswordVar, ipAddressOrHostnameTextBoxVar) {        
        if ($.trim(String(ipAddressOrHostnameTextBoxVar)) == '') {
            return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelIpAddress').text());
        }
        if (selectedCredential == null || selectedCredential.length == 0 || selectedCredential == newCredentialsConstant) {
            if (!displayNameVar.trim()) {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelDisplayName').text());
            }

            if (!usernameVar.trim()) {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelProviderUserName').text());
            }

            var credentialNameExists = $.inArray(displayNameVar.toLowerCase(), selectedCredentialOptionsArr) >= 0;
            if (credentialNameExists) {
                return "<a href='javascript:SW.SRM.EmbeddedSmisProvider.loadCredentials(&quot;"
                    + displayNameVar
                    + "&quot;)'>@{R=SRM.Strings;K=Add_Provider_CredentialNameAlreadyExist_LoadSaved; E=js}</a> or <a href='javascript:SW.SRM.EmbeddedSmisProvider.focusCredentialsDisplayName()'>@{R=SRM.Strings;K=Add_Provider_CredentialNameAlreadyExist_RenameDuplicate; E=js}</a>";
            }

            if ($.trim(String(passwordVar)) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelProviderPassword').text());
            }

            if (!(passwordVar === confirmPasswordVar)) {
                return "@{R=SRM.Strings;K=Add_Provider_ConfirmPassword_CompareField_Validator; E=js}";
            }

            var httpsPort = $("#HttpsPortTextBox").val();
            if ($.trim(String(httpsPort)) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelHttpsPort').text());
            }

            var httpsPortInt = parseInt(httpsPort);
            if (!$.isNumeric(httpsPortInt) || Math.floor(httpsPortInt) !== httpsPortInt || httpsPortInt < 1 || httpsPortInt > 65535) {
                return "@{R=SRM.Strings;K=Add_Provider_NotValidPort_Validator; E=js}".replace('{0}', $('#labelHttpsPort').text());
            }


            var httpPort = $("#HttpPortTextBox").val();
            if ($.trim(String(httpPort)) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelHttpPort').text());
            }

            var httpPortInt = parseInt(httpPort);
            if (!$.isNumeric(httpPortInt) || Math.floor(httpPortInt) !== httpPortInt || httpPortInt < 1 || httpPortInt > 65535) {
                return "@{R=SRM.Strings;K=Add_Provider_NotValidPort_Validator; E=js}".replace('{0}', $('#labelHttpPort').text());
            }

            if ($.trim(String($("#InteropNamespaceTextBox").val())) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelInteropNamespace').text());
            }

            if ($.trim(String($("#ArrayNamespaceTextBox").val())) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelArrayNamespace').text());
            }
        }
        return "";
    };

    var hideError = function () {
        $("#failMessageHeader").hide();
        $("#testConnectionFailMsg").hide();
    };

    var showPassMessageBox = function () {
        $("#testConnectionPassMsg").show();
    };

    var showFailMessageBox = function (message, errorMessage) {
        var title = "@{R=SRM.Strings;K=Provider_Step_Provider_Connecting_Error; E=js}".concat(" ").concat($("#IpAddressOrHostnameTextBox").val());
        SW.SRM.Common.ShowError(title, message, errorMessage);
    };

    var saveButtonExists = function () {
        return $('#save').length > 0;
    };
    var disableSaveButton = function () {
        if (saveButtonExists()) {
            Ext.getCmp('save').disable();
        }
    };
    var enableSaveButton = function () {
        if (saveButtonExists()) {
            Ext.getCmp('save').enable();
        }
    };

    return {
        testCredentialDeferred : null,
        focusCredentialsDisplayName: function () {
            $("#DisplayNameTextBox").focus();
        },
        loadCredentials: function (credentialName) {
            $('#ChooseCredentialDdl').find('option:contains("'+credentialName+'")').attr("selected",true);
            $('#ChooseCredentialDdl').trigger('change');
        },
        loadCredentialsByID: function (id) {
            $('#ChooseCredentialDdl option[value = "' + id + '"]').attr("selected", true);
            $('#ChooseCredentialDdl').trigger('change');
        },
        cancelTestCredential: function () {
            $("#testingConnectionMsg").hide();
            $("#testConnectionButton").removeAttr('disabled');
            enableSaveButton();

            if (request) {
                request.abort();
                request = null;
            }
        },

        testSmisCredential: function(onSuccess, isEdit) {
            $("#testConnectionFailMsg").hide();
            $("#testConnectionPassMsg").hide();

            request = null;
            var displayNameVar = $("#DisplayNameTextBox").val();
            var usernameVar = $("#ProviderUsernameTextBox").val();
            var passwordVar = $("#ProviderPasswordTextBox").val();
            var confirmPasswordVar = $("#ProviderConfirmPasswordTextBox").val();
            var ipAddressOrHostnameTextBoxVar = $.trim(String($("#IpAddressOrHostnameTextBox").val()));
            var namespaceVar = $("#InteropNamespaceTextBox").val();
            var arrayNamespaceVar = $("#ArrayNamespaceTextBox").val();
            var httpsPortVar = $("#HttpsPortTextBox").val();
            var httpPortVar = $("#HttpPortTextBox").val();
            var protocolVar = $("#ProtocolCheckBox").is(':checked');
            var selectedCredential = $("#ChooseCredentialDdl").val();
            var selectedCredentialText = $("#ChooseCredentialDdl option:selected").text();
            var selectedCredentialOptionsArr = $("#ChooseCredentialDdl option").map(function () { return this.text.toLowerCase(); }).get();

            var error = credentialsValidation(selectedCredentialText, selectedCredentialOptionsArr, displayNameVar, usernameVar, passwordVar, confirmPasswordVar, ipAddressOrHostnameTextBoxVar);
            if (error == null || error.length == 0) {
                hideError();
            } else {
                handleError(formatSmisError(error), isEdit);
                return;
            }

            $("#testingConnectionMsg").show();
            $("#testConnectionButton").attr('disabled', 'disabled');
            disableSaveButton();

            var credentialId = selectedCredential == null || selectedCredential.length == 0 ? -1 : parseInt(selectedCredential);
            request = SW.SRM.Common.CallSrmWebService("/Orion/SRM/Services/EnrollmentWizardService.asmx",
                "TestSmisCredentialsExtended",
                {
                    'credentialID': credentialId,
                    'credentialName': displayNameVar,
                    'userName': usernameVar,
                    'password': passwordVar,
                    'useHttp': protocolVar,
                    'httpPort': parseInt(httpPortVar),
                    'httpsPort': parseInt(httpsPortVar),
                    'interopNamespace': namespaceVar,
                    'arrayNamespace': arrayNamespaceVar,
                    'ip': ipAddressOrHostnameTextBoxVar,
                    'providerType': providerType,
                    'isEdit': isEdit
                },
                function (result) {
                    $("#testingConnectionMsg").hide();
                    $("#testConnectionButton").removeAttr('disabled');

                    if (result.IsSuccess) {
                        showPassMessageBox();
                        if (onSuccess) {
                            onSuccess();
                        }
                    } else {
                        handleError(formatSmisError(result.Message), isEdit, result.ErrorMessage);
                    }
                },
                function() {
                    handleError(formatSmisError(connectionTestUnexpectedFailedMessage), isEdit);
                });
        },

        testCredentialsAndValidateDiscovery: function () {
            var testingDeffered = $.Deferred();

            SW.SRM.DiscoveryValidation.init(arrayID, arraySerialNumber, groupId, request, testingDeffered, connectionTestUnexpectedFailedMessage, handleError);
            var globalDiscoveryPromise = $.Deferred();
            SW.SRM.EmbeddedSmisProvider.testSmisCredential(SW.SRM.DiscoveryValidation.startDiscovery, true);
            $.when(testingDeffered).then(
                function(x) {
                    return globalDiscoveryPromise.resolve();
                },
                function() {
                    return globalDiscoveryPromise.reject();
                });

            return globalDiscoveryPromise.promise();
        },

        init: function (_providerType, _newCredentialConstant, _connectionTestUnexpectedFailedMessage, _arrayID, _arraySerialNumber, _groupId) {
            providerType = _providerType;
            connectionTestUnexpectedFailedMessage = _connectionTestUnexpectedFailedMessage;
            newCredentialsConstant = _newCredentialConstant;
            arrayID = _arrayID;
            arraySerialNumber = _arraySerialNumber;
            groupId = _groupId;

            this.MonitoredControlIds = ['IpAddressOrHostnameTextBox', 'ChooseCredentialDdl', 'ProviderUsernameTextBox', 'ProviderPasswordTextBox'];
            SW.SRM.EditProperties.ProviderChangeManager.InitProvider(this, this.testCredentialsAndValidateDiscovery);
        }
    };
}();


