﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChoosePollerControl.ascx.cs" Inherits="Orion_SRM_Config_Controls_ChoosePollerControl" %>
<%@ Import Namespace="Resources" %>
<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include runat="server" File="OrionCore.js" Section="Bottom" SpecificOrder="1" />
<orion:Include runat="server" File="SRM.AssignPollingEngine.js" Section="Bottom" SpecificOrder="2" Module="SRM" />
<orion:Include runat="server" File="extjs/Ext.sw.grid.RadioSelectionModel.js" />

<style type="text/css">
    #pollEngineDescr a:visited, #pollEngineDescr a:active, #pollEngineDescr a:hover {
        color: black;
        text-decoration: none;
    }
</style>

<table>
    <tr>
        <td>
            <span><%= Resources.SrmWebContent.Poller_Device_Name %></span>
        </td>
        <td>
            <asp:Label ID="pollEngineDescr" runat="server" ClientIDMode="Static"></asp:Label>
            <span id="engineChange" runat="server" clientidmode="Static">
                <orion:LocalizableButton runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ Resources: SrmWebContent, PollingProperties_ChangePollingEngine %>" ID="changePollEngineLink" OnClientClick="return false;" />
                <asp:HiddenField runat="server" ID="hfPollingEngineId" />
                <input type="hidden" id="selPollingEngineId" value='<%= hfPollingEngineId.ClientID %>' />
            </span>
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            <div id="failMsg" class="sw-suggestion sw-suggestion-fail">
                <span class="sw-suggestion-icon"></span>
                <span id="failMessageContent"><%= SrmWebContent.PollerStep_EngineIsNotAvailable %></span>
            </div>
        </td>
    </tr>
</table>

<script type="text/javascript">
    $(function () {
        $('#failMsg').hide();

        SW.SRM.AssignPollingEngine.init(function (engId, displayLink) {
            $('#failMsg').hide();
        });

        onClickNextButtonEvent = function () {
            var isUp = $('#pollEngineDescr').html().indexOf('Up.gif') > -1;
            if (isUp) {
                stepSubmit();
            } else {
                $('#failMsg').show();
            }
        };
    });
</script>
