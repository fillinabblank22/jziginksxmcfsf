﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Resources;
using SolarWinds.Orion.Core.Models;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.Credentials;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.EnrollmentWizard;
using SolarWinds.SRM.Web.Interfaces;
using System.Web.UI.WebControls;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.Resources;

public partial class Orion_SRM_Config_Controls_NetAppProviderStep : System.Web.UI.UserControl, IConfigurationControl, ICreateEditControl, IEditControl
{
    private Configuration configuration = WizardWorkflowHelper.ConfigurationInfo;
    private ArrayEntity array;
    private List<ProviderEntity> providers;
    private readonly List<KeyValuePair<string, string>> warnings = new List<KeyValuePair<string, string>>();
    private string SelectedCredentials;

    protected string ArrayID
    {
        get
        {
            return array == null ? string.Empty : array.ID;
        }
    }
    protected string ArraySerialNumber
    {
        get
        {
            return array == null ? string.Empty : array.SerialNumber;
        }
    }

    protected Guid DeviceGroupId
    {
        get
        {
            return configuration.DeviceGroup.GroupId;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var type = GetDeviceType(configuration.DeviceGroup.FlowType);         
            ArrayTypeLabel.Text = configuration.DeviceGroup.Name;
            FillCredentials(ArrayCredentialDropDown, type, SelectedCredentials);

            deviceInfo.InitWithDevice(configuration.DeviceGroup);

            NetAppSubTitle.Text = string.Format(SrmWebContent.AddStorage_ProviderStep_NetAppFiler_SubTitle,configuration.DeviceGroup.Name);
            
            ProtocolCheckBox.Text = SrmWebContent.Provider_Step_UseHttps;
            ProtocolCheckBox.Checked = configuration.DeviceGroup.FlowType == FlowType.EmcUnity;
        }
    }

    private void FillCredentials(DropDownList dropDownList, CredentialType type, string selectedValue)
    {
        var items = new List<KeyValuePair<String, String>>() { new KeyValuePair<String, String>(SrmWebContent.Provider_Step_New_Credential, SrmWebContent.Provider_Step_New_Credential) };
        using (var proxy = BusinessLayerFactory.Create())
        {
            IDictionary<int, string> credentialsList = proxy.GetCredentialNames(type);
            foreach (KeyValuePair<int, string> cred in credentialsList)
            {
                items.Add(new KeyValuePair<String, String>(cred.Key.ToString(CultureInfo.InvariantCulture), cred.Value));
            }
        }

        dropDownList.DataTextField = "Value";
        dropDownList.DataValueField = "Key";
        dropDownList.DataSource = items;

        if (items.Any(pair => pair.Key == selectedValue))
        {
            dropDownList.SelectedValue = selectedValue;
        }
        else
        {
            //Display warning about missing credentials
            this.warnings.Add(new KeyValuePair<string, string>("", SrmWebContent.Credentials_Missing));
        }

        dropDownList.DataBind();
    }

    public void SaveValues()
    {
        this.SaveValues(null);
    }

    private void SaveValues(int? credentialId)
    {
        var credentials = credentialId == null
                              ? this.SaveCredentials()
                              : this.SaveCredentials(credentialId.Value.ToString(CultureInfo.InvariantCulture));

        this.UpdateProvider(credentials);
    }

    private void RefillConfiguration()
    {
        var engine = ServiceLocator.GetInstance<IEngineDAL>().GetEngine(array.EngineID);

        configuration.DeviceGroup = ServiceLocator.GetInstance<IDeviceGroupDAL>().GetDeviceGroupDetailsByArrayId(array.StorageArrayId.Value);
        configuration.PollingConfig.BusinessLayerPort = engine.BusinessLayerPort;
        configuration.PollingConfig.EngineID = engine.EngineID;
        configuration.PollingConfig.ServerName = engine.ServerName;
        if (!IsPostBack)
        {
            ClearSelectedProviders();
        }
    }

    private void ClearSelectedProviders()
    {
        configuration.ProviderConfiguration.SelectedProviders.RemoveAll(a => true);
    }

    private void UpdateProvider(Credential credentials, int? credentialID = null)
    {
        if (array != null)
            this.RefillConfiguration();

        var type = GetDeviceType(configuration.DeviceGroup.FlowType);

        var providerEntity = new EnrollmentWizardProvider
        {
            IPAddress = IpAddressTextBox.Text.Trim(),
            CredentialType = type,
            ProviderType = ProviderTypeEnum.Unified,
            ProviderLocation = ProviderLocation.Onboard,
            Credential = credentials,
            CredentialID = credentialID ?? credentials.ID.Value
        };

        configuration.ProviderConfiguration.SelectedProviders.Clear();
        configuration.ProviderConfiguration.SelectedProviders.Add(providerEntity);
    }

    private Credential GetCredential(int credentialID)
    {
        var type = GetDeviceType(configuration.DeviceGroup.FlowType);

        using (var proxy = BusinessLayerFactory.Create())
        {
            return proxy.GetCredential(credentialID, type);
        }
    }

    public Credential SaveCredentials(string selectedCredentialID)
    {
        Credential credential = null;
        var type = CredentialType.NetAppFiler;
        if (selectedCredentialID != SrmWebContent.Provider_Step_New_Credential)
        {
            int credentialID = -1;
            if (Int32.TryParse(selectedCredentialID, out credentialID))
                credential = GetCredential(credentialID);
        }
        else
        {
            switch (configuration.DeviceGroup.FlowType)
            {
                case FlowType.EmcUnity:
                    type = CredentialType.UnityHttp;
                    credential = new EmcUnityHttpCredential
                    {
                        Name = CredentialNameTextBox.Text,
                        UseSsl = ProtocolCheckBox.Checked,
                        Password = PasswordTextBox.Text,
                        Username = UserNameTextBox.Text
                    };
                    break;
                case FlowType.InfiniBox:
                    type = CredentialType.InfinidatHttp;
                    credential = new InfinidatHttpCredential
                    {
                        Name = CredentialNameTextBox.Text,
                        UseSsl = ProtocolCheckBox.Checked,
                        Password = PasswordTextBox.Text,
                        Username = UserNameTextBox.Text
                    };
                    break;
                default:
                    credential = new NetAppOntapCredentials
                    {
                        Name = CredentialNameTextBox.Text,
                        UseSsl = ProtocolCheckBox.Checked,
                        Password = PasswordTextBox.Text,
                        Username = UserNameTextBox.Text
                    };
                    break;
            }

            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(credential,type);
                credential.ID = id;
            }
        }

        return credential;
    }

    public Credential SaveCredentials()
    {
        return SaveCredentials(ArrayCredentialDropDown.SelectedValue);
    }

    public string ValidationError { get; private set; }
    public bool ValidateUserInput()
    {
        if (string.IsNullOrEmpty(IpAddressTextBox.Text))
        {
            return false;
        }
        if (ArrayCredentialDropDown.SelectedValue == SrmWebContent.Provider_Step_New_Credential)
        {
            if (String.IsNullOrEmpty(CredentialNameTextBox.Text))
            {
                this.ValidationError = String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, SrmWebContent.Provider_Step_Credential_Name);
                return false;
            }
            else if (String.IsNullOrEmpty(UserNameTextBox.Text))
            {
                this.ValidationError = String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, SrmWebContent.Provider_Step_User_Name);
                return false;
            }
            else if (String.IsNullOrEmpty(PasswordTextBox.Text))
            {
                this.ValidationError = String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, SrmWebContent.Provider_Step_Password);
                return false;
            }
        }
        return true;
    }

    public IEditControl InitFromNetObjects(List<string> listOfNetObjectIDs)
    {
        // Supports just one NetObject ID and just SMSA
        var supported = listOfNetObjectIDs.Count == 1 && listOfNetObjectIDs[0].Split(':')[0] == SolarWinds.SRM.Web.NetObjects.StorageArray.NetObjectPrefix;
        if (!supported)
        {
            return null;
        }

        var arrayId = int.Parse(listOfNetObjectIDs[0].Split(':')[1]);
        array = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayDetails(arrayId);

        // Supporting just NetApp
        List<FlowType> flowTypes = new List<FlowType>() {FlowType.NetAppFiler, FlowType.EmcUnity, FlowType.InfiniBox};
        if (!flowTypes.Contains(array.FlowType))
            return null;

        FillControl();

        return this;
    }

    private void FillControl()
    {
        this.providers = ServiceLocator.GetInstance<IStorageProviderDAL>().GetAllProvidersByArrayId(array.StorageArrayId);

        if (this.providers.Any())
        {
            var provider = this.providers.First();
            IpAddressTextBox.Text = provider.IPAddress;
            SelectedCredentials = provider.CredentialID.ToString();
            //since credentials can be missing pass credentialID also 
            UpdateProvider(GetCredential(provider.CredentialID), provider.CredentialID);
        }
    }

    private CredentialType GetDeviceType(FlowType flowType)
    {
        switch (flowType)
        {
            case FlowType.EmcUnity:
                return CredentialType.UnityHttp;
            case FlowType.InfiniBox:
                return CredentialType.InfinidatHttp;
            default:
                return CredentialType.NetAppFiler;
        }
    }

    public string ValidateJSFunction()
    {
        return "SW.SRM.EditProperties.NetApp.InitValidation()";
    }

    public bool Validate(bool connectionPropertyChanged)
    {
        return ValidateUserInput();
    }

    public KeyValuePair<string, string>[] GetWarnings()
    {
        return this.warnings.ToArray();
    }

    public void Update(bool connectionPropertyChanged)
    {
        SaveValues();
        var toDiscovery = new List<SelectedArrayEntity>
                              {
                                  new SelectedArrayEntity(array)
                                    {
                                        IsSelected = true,
                                        StorageArrayId = this.array.StorageArrayId
                                    }
                              };

        using (var proxy = BusinessLayerFactory.Create())
        {
            proxy.DiscoveryImport(array.EngineID, ProviderHelper.GetProviderInfoOfWizardProviders(this.providers, configuration.ProviderConfiguration.SelectedProviders), toDiscovery);
        }
    }
}
