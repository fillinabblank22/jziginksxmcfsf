﻿/// <reference path="~/Orion/js/jquery-1.7.1/jquery.js" />
SW = SW || {};
SW.SRM = SW.SRM || {};

SW.SRM.AddSmisProvider = function () {
    var dialog,
        engineId;

    var findProvider = function (providerIP, credName) {
        var gridStore;
        // If we are hosted on Edit properties, we cannot fetch the credentialName/IP from the grid
        if ($('#Grid >:first-child').length == 0) {
            gridStore = new ORION.WebServiceStore(
                "/Orion/SRM/Services/EnrollmentWizardService.asmx/GetProviders",
                [
                { name: 'UniqueProviderIdentifier', mapping: 0 },
                { name: 'FriendlyName', mapping: 1 },
                { name: 'MonitoredArrayCount', mapping: 2 },
                { name: 'CredentialName', mapping: 3 }
                ],
                "FriendlyName");
        } else {
            // Find provider in the ExtJS grid store
            var extGridId = $('#Grid >:first-child')[0].id;
            var grid = Ext.getCmp(extGridId);
            gridStore = grid.store;
        }
        var existingProvider = null;
        gridStore.each(function (rec) {
            if (rec.get("CredentialName") == credName && rec.get("FriendlyName").split('_')[1] == providerIP) {
                existingProvider = rec.get("FriendlyName").split('_')[1];
                return false;
            }
        });
        return existingProvider;
    }

   var submitValidation = function (selectedCredential, selectedCredentialOptionsArr, displayNameVar, usernameVar, passwordVar, confirmPasswordVar, ipAddressOrHostnameTextBoxVar, selectedCredentialText) {
        var message = credentialsValidation(selectedCredentialText, selectedCredentialOptionsArr, displayNameVar, usernameVar, passwordVar, confirmPasswordVar, ipAddressOrHostnameTextBoxVar);
        if (message != "") {
            return { 
                    valid: false,
                    message: message
                };
            }
        
        var existingProviderMessage = existingProviderValidation(selectedCredential, displayNameVar, ipAddressOrHostnameTextBoxVar, selectedCredentialText);
        if (existingProviderMessage !== "") {
            return {
                valid: false,
                title: "@{R=SRM.Strings;K=Add_Provider_CredentialNameAlreadyExistHeader; E=js}",
                message: existingProviderMessage
            };
        }
        
        if (selectedCredential == null || selectedCredential.length == 0) {
            if ($.trim(String(displayNameVar)) == '') {
                return {
                        valid: false,
                        message: "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelDisplayName').text())
                    };
            }
        }
        
        return {
            valid: true
        };
   };

   var existingProviderValidation = function (selectedCredential, displayNameVar, ipAddressOrHostnameTextBoxVar, selectedCredentialText) {

       var credName = (selectedCredential == null || selectedCredential.length === 0) ? displayNameVar : selectedCredentialText;
       var existingProvider = findProvider(ipAddressOrHostnameTextBoxVar, credName);
       if (existingProvider != null) {
           return SW.Core.String.Format("@{R=SRM.Strings;K=Add_Provider_CredentialNameAlreadyUsedByProvider; E=js}", existingProvider, credName);
       }

       return "";
   };

    var credentialsValidation = function (selectedCredential, selectedCredentialOptionsArr, displayNameVar, usernameVar, passwordVar, confirmPasswordVar, ipAddressOrHostnameTextBoxVar) {        
        if ($.trim(String(ipAddressOrHostnameTextBoxVar)) == '') {
            return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelIpAddress').text());
        }
        if (selectedCredential == null || selectedCredential.length == 0 || selectedCredential == newCredentialsConstant) {
            if (!displayNameVar.trim()) {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelDisplayName').text());
            }

            if (!usernameVar.trim()) {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelProviderUserName').text());
            }

            var credentialNameExists = $.inArray(displayNameVar.toLowerCase(), selectedCredentialOptionsArr) >= 0;
            if (credentialNameExists) {
                return "<a href='javascript:SW.SRM.AddSmisProvider.loadCredentials(&quot;"
                    + displayNameVar
                    + "&quot;)'>@{R=SRM.Strings;K=Add_Provider_CredentialNameAlreadyExist_LoadSaved; E=js}</a> or <a href='javascript:SW.SRM.AddSmisProvider.focusCredentialsDisplayName()'>@{R=SRM.Strings;K=Add_Provider_CredentialNameAlreadyExist_RenameDuplicate; E=js}</a>";
            }

            if ($.trim(String(passwordVar)) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelProviderPassword').text());
            }

            if (!(passwordVar === confirmPasswordVar)) {
                return "@{R=SRM.Strings;K=Add_Provider_ConfirmPassword_CompareField_Validator; E=js}";
            }

            var httpsPort = $("#HttpsPortTextBox").val();
            if ($.trim(String(httpsPort)) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelHttpsPort').text());
            }

            var httpsPortInt = parseInt(httpsPort);
            if (!$.isNumeric(httpsPortInt) || Math.floor(httpsPortInt) !== httpsPortInt || httpsPortInt < 1 || httpsPortInt > 65535) {
                return "@{R=SRM.Strings;K=Add_Provider_NotValidPort_Validator; E=js}".replace('{0}', $('#labelHttpsPort').text());
            }


            var httpPort = $("#HttpPortTextBox").val();
            if ($.trim(String(httpPort)) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelHttpPort').text());
            }

            var httpPortInt = parseInt(httpPort);
            if (!$.isNumeric(httpPortInt) || Math.floor(httpPortInt) !== httpPortInt || httpPortInt < 1 || httpPortInt > 65535) {
                return "@{R=SRM.Strings;K=Add_Provider_NotValidPort_Validator; E=js}".replace('{0}', $('#labelHttpPort').text());
            }

            if ($.trim(String($("#InteropNamespaceTextBox").val())) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelInteropNamespace').text());
            }

            if ($.trim(String($("#ArrayNamespaceTextBox").val())) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelArrayNamespace').text());
            }
        }
        return "";
    };

    var defaultHttpPort,
        defaultHttpsPort,
        defaultNamespace,
        request,
        connectionTestFailedTitle,
        connectionTestUnexpectedFailedMessage,
        newCredentialsConstant;

    var saveDefaults = function () {
        defaultNamespace = $("#InteropNamespaceTextBox").val();
        defaultArrayNamespace = $("#ArrayNamespaceTextBox").val();
        defaultHttpsPort = $("#HttpsPortTextBox").val();
        defaultHttpPort = $("#HttpPortTextBox").val();
    };

    var setForm = function (model) {
        $("#testingConnectionMsg").hide();
        $("#testConnectionFailMsg").hide();
        $("#testConnectionPassMsg").hide();

        $("#StoredCredFiledsTable").show();
        $("#AdvancedCredFiledsTable").hide();
        $("#expanderElement").show();
        $("#DisplayNameTextBox").val('');
        $("#ProviderUsernameTextBox").val('');
        $("#ProviderPasswordTextBox").val('');
        $("#ProviderConfirmPasswordTextBox").val('');
        $("#IpAddressOrHostnameTextBox").val(model.IP);
        $("#InteropNamespaceTextBox").val(defaultNamespace);
        $("#ArrayNamespaceTextBox").val(defaultArrayNamespace);
        $("#HttpsPortTextBox").val(defaultHttpsPort);
        $("#HttpPortTextBox").val(defaultHttpPort);
        $("#ProtocolCheckBox").removeAttr('checked');

        var $expanderImgElement = $('#expanderElement img');
        $expanderImgElement.attr('src', '/Orion/SRM/images/Button.Expand.gif');
        $expanderImgElement.attr('alt', '[+]');
    };

    var clearForm = function () {
        $("#testingConnectionMsg").hide();
        $("#testConnectionFailMsg").hide();
        $("#testConnectionPassMsg").hide();

        $("#StoredCredFiledsTable").show();
        $("#AdvancedCredFiledsTable").hide();
        $("#expanderElement").show();
        $("#DisplayNameTextBox").val('');
        $("#ProviderUsernameTextBox").val('');
        $("#ProviderPasswordTextBox").val('');
        $("#ProviderConfirmPasswordTextBox").val('');
        $("#IpAddressOrHostnameTextBox").val('');
        $("#InteropNamespaceTextBox").val(defaultNamespace);
        $("#ArrayNamespaceTextBox").val(defaultArrayNamespace);
        $("#HttpsPortTextBox").val(defaultHttpsPort);
        $("#HttpPortTextBox").val(defaultHttpPort);
        $("#ProtocolCheckBox").removeAttr('checked');

        var $expanderImgElement = $('#expanderElement img');
        $expanderImgElement.attr('src', '/Orion/SRM/images/Button.Expand.gif');
        $expanderImgElement.attr('alt', '[+]');
    };

    var showError = function (title, message, errorMessage) {
        SW.SRM.Common.ShowError(title, message, errorMessage)

        var delegate = $("#TestConnectionErrorJSDelegate").val();
        if ($.trim(String(delegate)) != '') {
            eval(delegate + '();');
        }
    };

    // Calling this method is specified in Groups.xml so this will be called only 
    // in case we are dealing with dell compellent device.
    var dellCompellentTestConnectionErrorDelegate = function () {
        if ($("#ProviderPasswordTextBox").val().length > 8) {
            var $failMessageContent = $("#failMessageContent");
            $failMessageContent.append('<br/>@{R=SRM.Strings;K=DellCompellentErrorMessage_PasswordLongerThanEightChars;E=js}');
        }
    };

    var hideError = function () {
        $("#failMessageHeader").hide();
        $("#testConnectionFailMsg").hide();
    };

    var showPassMessageBox = function () {
        $("#testConnectionPassMsg").show();
    };

    var showFailMessageBox = function (message, errorMessage) {
        var title = connectionTestFailedTitle.concat(" ").concat($("#IpAddressOrHostnameTextBox").val());
        showError(title, message, errorMessage);
    };

    var saveButtonExists = function () {
        return $('#save').length > 0;
    };
    var disableSaveButton = function () {
        if (saveButtonExists()) {
            Ext.getCmp('save').disable();
        }
    };
    var enableSaveButton = function () {
        if (saveButtonExists()) {
            Ext.getCmp('save').enable();
        }
    };

    return {
        testCredentialDeferred : null,
        focusCredentialsDisplayName: function () {
            $("#DisplayNameTextBox").focus();
        },
        loadCredentials: function (credentialName) {
            $('#ChooseCredentialDdl').find('option:contains("'+credentialName+'")').attr("selected",true);
            $('#ChooseCredentialDdl').trigger('change');
        },
        loadCredentialsByID: function (id) {
            $('#ChooseCredentialDdl option[value = "' + id + '"]').attr("selected", true);
            $('#ChooseCredentialDdl').trigger('change');
        },
        cancelTestCredential: function () {
            $("#testingConnectionMsg").hide();
            $("#testConnectionButton").removeAttr('disabled');
            enableSaveButton();

            if (request) {
                request.abort();
                request = null;
            }
        },
        testCredential: function (onSuccess) {
            var resultingDeferred = $.Deferred();
            $("#testConnectionFailMsg").hide();
            $("#testConnectionPassMsg").hide();

            var displayNameVar = $("#DisplayNameTextBox").val();
            var usernameVar = $("#ProviderUsernameTextBox").val();
            var passwordVar = $("#ProviderPasswordTextBox").val();
            var confirmPasswordVar = $("#ProviderConfirmPasswordTextBox").val();
            var ipAddressOrHostnameTextBoxVar = $.trim(String($("#IpAddressOrHostnameTextBox").val()));
            var namespaceVar = $("#InteropNamespaceTextBox").val();
            var arrayNamespaceVar = $("#ArrayNamespaceTextBox").val();
            var httpsPortVar = $("#HttpsPortTextBox").val();
            var httpPortVar = $("#HttpPortTextBox").val();
            var protocolVar = $("#ProtocolCheckBox").is(':checked');
            var selectedCredential = $("#ChooseCredentialDdl").val();
            var selectedCredentialText = $("#ChooseCredentialDdl option:selected").text();
            var selectedCredentialOptionsArr = $("#ChooseCredentialDdl option").map(function () { return this.text.toLowerCase(); }).get();
            
            var error = credentialsValidation(selectedCredentialText, selectedCredentialOptionsArr, displayNameVar, usernameVar, passwordVar, confirmPasswordVar, ipAddressOrHostnameTextBoxVar);

            if (error == null || error.length === 0) {
                hideError();
            } else {
                showError('@{R=SRM.Strings;K=Add_Provider_ValidationError_Title; E=js}', error);
                return resultingDeferred.reject();
            }

            error = existingProviderValidation(selectedCredential, displayNameVar, ipAddressOrHostnameTextBoxVar, selectedCredentialText);
            if (error == null || error.length === 0) {
            } else {
                // Show 'The credential display name already exists.' error despite successful/failed connection test 
                showError('@{R=SRM.Strings;K=Add_Provider_CredentialNameAlreadyExistHeader; E=js}', error);
            }

            $("#testingConnectionMsg").show();
            $("#testConnectionButton").attr('disabled', 'disabled');

            disableSaveButton();

            var credentialId = selectedCredential == null || selectedCredential.length == 0 ? -1 : parseInt(selectedCredential);
            var testSmisCredentialsMethodName = "TestSmisCredentials";
            var testSmisCredentialsIn = {
                'credentialID': credentialId,
                'credentialName': displayNameVar,
                'userName': usernameVar,
                'password': passwordVar,
                'useHttp': protocolVar,
                'httpPort': parseInt(httpPortVar),
                'httpsPort': parseInt(httpsPortVar),
                'interopNamespace': namespaceVar,
                'arrayNamespace': arrayNamespaceVar,
                'ip': ipAddressOrHostnameTextBoxVar
            };
            if ($('#hfEditPropertiesCfg').length == 1)
            {
                testSmisCredentialsMethodName = "TestSmisCredentialsNoSession";
                var cfg = JSON.parse($('#hfEditPropertiesCfg').val());
                testSmisCredentialsIn['engineid'] = cfg.EngineID;
            }

            request = SW.SRM.Common.CallSrmWebService("/Orion/SRM/Services/EnrollmentWizardService.asmx",
                                        testSmisCredentialsMethodName,
                                        testSmisCredentialsIn
                                        ,
                                        function (result) {
                                            $("#testingConnectionMsg").hide();
                                            $("#testConnectionButton").removeAttr('disabled');
                                            enableSaveButton();

                                            if (result.IsSuccess) {
                                                showPassMessageBox();

                                                var namespaceChanged = result.PropertyBag['NamespaceChanged'];
                                                if (namespaceChanged) {
                                                    $("#ArrayNamespaceTextBox").val(namespaceChanged);
                                                }

                                                if (onSuccess) {
                                                    onSuccess();
                                                }
                                                resultingDeferred.resolve();
                                            } else {
                                                showFailMessageBox(result.Message, result.ErrorMessage);
                                                if ($('#hfEditPropertiesCfg').length == 1) {
                                                    testCredentialDeferred.reject();
                                                }
                                                resultingDeferred.reject();
                                            }
                                        },
                                        function () {
                                            $("#testingConnectionMsg").hide();
                                            $("#testConnectionButton").removeAttr('disabled');
                                            enableSaveButton();

                                            if (request.status !== 0) {
                                                showFailMessageBox(connectionTestUnexpectedFailedMessage);
                                            }
                                            
                                            resultingDeferred.reject();
                                        });
            return resultingDeferred.promise();
        },

        saveClickButtonHandler: function (groupId, arraySerialNumber) {
            hideError();
            $("#testConnectionFailMsg").hide();
            $("#testConnectionPassMsg").hide();

            var displayNameVar = $("#DisplayNameTextBox").val();
            var usernameVar = $("#ProviderUsernameTextBox").val();
            var passwordVar = $("#ProviderPasswordTextBox").val();
            var confirmPasswordVar = $("#ProviderConfirmPasswordTextBox").val();
            var ipAddressOrHostnameTextBoxVar = $.trim(String($("#IpAddressOrHostnameTextBox").val()));
            var namespaceVar = $("#InteropNamespaceTextBox").val();
            var arrayNamespaceVar = $("#ArrayNamespaceTextBox").val();
            var httpsPortVar = $("#HttpsPortTextBox").val();
            var httpPortVar = $("#HttpPortTextBox").val();
            var protocolVar = $("#ProtocolCheckBox").is(':checked');
            var selectedCredential = $("#ChooseCredentialDdl").val();
            var selectedCredentialText = $("#ChooseCredentialDdl option:selected").text();
            // Get options text values as array
            var selectedCredentialOptionsArr = $("#ChooseCredentialDdl option").map(function() { return this.text.toLowerCase(); }).get();

            var valResult = submitValidation(selectedCredential, selectedCredentialOptionsArr, displayNameVar, usernameVar, passwordVar, confirmPasswordVar, ipAddressOrHostnameTextBoxVar, selectedCredentialText);
            if (valResult.valid == true) {
                hideError();
            } else {
                var errorTitle = valResult.title;
                if (errorTitle == null)
                    errorTitle = '@{R=SRM.Strings;K=Add_Provider_ValidationError_Title; E=js}';
                                        
                showError(errorTitle, valResult.message);
                return;
            }

            testCredentialDeferred = $.Deferred();
            var arraySn = arraySerialNumber;

            function validateDiscovery(discoveryGuid, enrollmentWizardProviderGuid) {
                $('#hfProviderGuid').val(enrollmentWizardProviderGuid);
                $('#hfDiscoveryGuid').val(discoveryGuid);

                ORION.callWebService("/Orion/SRM/Services/EnrollmentWizardService.asmx",
                                        "GetAllDiscoveryJobResultsById",
                                        {
                                            'discoveryGuid': discoveryGuid,
                                            'providerGuid': enrollmentWizardProviderGuid,
                                            'groupId': groupId,
                                            'engineId': engineId
                                        },
                                        function (result) {
                                            var resJson = JSON.parse(result);
                                            if (resJson.isDone != true) {
                                                setTimeout(function () { validateDiscovery(discoveryGuid, enrollmentWizardProviderGuid); }, 1000);
                                            } else {
                                                // Check if SNs are the same
                                                var foundArraySn = false;
                                                $.each(resJson.selectedArray, function (i, e) {
                                                    if (e == arraySerialNumber) {
                                                        foundArraySn = true;
                                                        return false;
                                                    }
                                                });
                                                if (foundArraySn) {
                                                    testCredentialDeferred.resolve();
                                                } else
                                                {
                                                    testCredentialDeferred.reject();
                                                }
                                            }
                                        });
            }


            function startDiscovery(enrollmentWizardProviderGUID) {
                ORION.callWebService("/Orion/SRM/Services/EnrollmentWizardService.asmx",
                                        "StartDiscoveryMulti",
                                        {
                                            'providerGuid': [ enrollmentWizardProviderGUID ],
                                            'groupId': groupId,
                                            'engineId': engineId
                                        },
                                        function (result) {
                                                if (result.IsSuccess) {
                                                    validateDiscovery(result.ProviderGuid, enrollmentWizardProviderGUID);
                                                } else {
                                                    showFailMessageBox(result.Message, result.ErrorMessage);
                                                }
                                            }
                                        );
            }

            function saveCredentials() {
                var credentialId = selectedCredential == null || selectedCredential.length == 0 ? -1 : parseInt(selectedCredential);
                var methodName = "SaveSmisProvider";
                var callArgs = {
                    'ipAddress': ipAddressOrHostnameTextBoxVar,
                    'displayName': displayNameVar,
                    'userName': usernameVar,
                    'password': passwordVar,
                    'useHttp': protocolVar,
                    'httpPort': parseInt(httpPortVar),
                    'httpsPort': parseInt(httpsPortVar),
                    'interopNamespace': namespaceVar,
                    'arrayNamespace': arrayNamespaceVar,
                    'credentialID': credentialId
                };

                if ($('#hfEditPropertiesCfg').length == 1) {
                    methodName = "GetSmisProviderEdit";
                    var cfg = JSON.parse($('#hfEditPropertiesCfg').val());
                    callArgs["arrayId"] = cfg.ArrayId;
                }
                ORION.callWebService("/Orion/SRM/Services/EnrollmentWizardService.asmx",
                                        methodName,
                                        callArgs,
                                        function (result) {
                                            if ($('#hfEditPropertiesCfg').length == 0) {
                                                SW.SRM.ProviderSelector.setDefaultProvider(result);
                                                SW.SRM.ProviderSelector.reload();
                                                dialog.hide();
                                            }
                                            else {
                                                // When hosted in EP
                                                startDiscovery(result);
                                            }
                                        });
            }

            SW.SRM.AddSmisProvider.testCredential(saveCredentials);

            if ($('#hfEditPropertiesCfg').length == 1) {
                // We are hosted in Edit Properties, and started as validation function, which will finish in a while
                // when test will pass
                return testCredentialDeferred.promise();
            }
        },

        init: function (type, _connectionTestFailedTitle, _connectionTestUnexpectedFailedMessage, _newCredentialsConstant, _engineId) {
            connectionTestFailedTitle = _connectionTestFailedTitle;
            connectionTestUnexpectedFailedMessage = _connectionTestUnexpectedFailedMessage;
            newCredentialsConstant = _newCredentialsConstant;
            engineId = _engineId;
            saveDefaults();

            this.MonitoredControlIds = ['pollEngineDescr', 'IpAddressOrHostnameTextBox', 'ChooseCredentialDdl'];
            SW.SRM.EditProperties.ProviderChangeManager.InitProvider(this, this.testCredential);

            function reloadCredentials(onSucc) {
                $('#ChooseCredentialDdl option').remove();
                ORION.callWebService("/Orion/SRM/Services/EnrollmentWizardService.asmx",
                                     "GetStoredCredentials",
                                     {
                                         'type': type
                                     },
                                     function (result) {
                                         $('<option value="">' + result.Default + '</option>').appendTo($('#ChooseCredentialDdl'));
                                         $.each(result.Credentials, function (index, item) {
                                             $('<option value="' + item.Key + '">' + item.Value + '</option>').appendTo($('#ChooseCredentialDdl'));
                                         });
                                         if (typeof(onSucc) != "undefined")
                                             onSucc.call();
                                     });
            }

            

            function ShowSmisProviderDialog() {
                reloadCredentials();

                if (!dialog) {
                    dialog = new Ext.Window({
                        applyTo: 'AddProviderFrame',
                        id: 'dialog',
                        layout: 'fit',
                        width: 1100,
                        height: 600,
                        modal: true,
                        closeAction: 'hide',
                        plain: true,
                        resizable: false,
                        items: new Ext.BoxComponent({
                            applyTo: 'AddProviderBody'
                        }),
                        buttons: [
                            {
                                id: 'save',
                                text: '@{R=SRM.Strings;K=Add_Provider_Save_ButtonText; E=js}',
                                handler: SW.SRM.AddSmisProvider.saveClickButtonHandler
                            },
                            {
                                text: '@{R=SRM.Strings;K=Add_Provider_Close_ButtonText; E=js}',
                                style: 'margin-left: 5px;',
                                handler: function () {
                                    SW.SRM.AddSmisProvider.cancelTestCredential();
                                    dialog.hide();
                                }
                            }],
                        center: function () {
                            var pos = this.el.getCenterXY(true);
                            this.setPosition(pos[0], pos[1]);
                        }
                    });
                }
                dialog.center();
                dialog.show();

                clearForm();
            };

            if ($('#hfEditPropertiesCfg').length == 0) {
                $("#addNewProviderBtn").click(function() {
                    ShowSmisProviderDialog();
                    return false;
                });
                $("#addNewProviderLabel").click(function() {
                    ShowSmisProviderDialog();
                    return false;
                });
            }
            else {
                // Hosted in Edit Properties
                var cfg = JSON.parse($('#hfEditPropertiesCfg').val());
                setForm(cfg);

                function credentialsLoaded() {
                    SW.SRM.AddSmisProvider.loadCredentialsByID(cfg.CredentialID);
                }

                reloadCredentials(credentialsLoaded);                
            }
        }

    };
}();


