﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.Credentials;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.EnrollmentWizard;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.Resources;

public partial class Orion_SRM_Config_Controls_AddSmisAndGenericHttpProvider : UserControl,
    ICreateEditControl, IEditControl
{
    private static readonly Log log = new Log();
    private readonly List<KeyValuePair<string, string>> warnings = new List<KeyValuePair<string, string>>();

    public string AddProviderToolbarLabel = SrmWebContent.Provider_Step_Add_SmisAndGenericHttp_Btn_Label;
    protected string initFromEditProviderJS = "";
    protected bool isInitializedFromNetObjectID;
    protected bool isLegacyDellProvider = false;
    private NetObjectUniqueIdentifier netObjectUniqueIdentifier;
    public string Provider_Step_Add_Provider_Title = SrmWebContent.Provider_Step_Add_SMISAndGenericHttp_Provider_Title;
    protected ProviderEntity restProvider;
    protected ProviderEntity smisProvider;
    protected string testCredentialMethod = "SW.SRM.AddSmisAndHttpProvider.testCredentials();";

    public DeviceGroup DeviceGroup => WizardWorkflowHelper.ConfigurationInfo.DeviceGroup ?? ServiceLocator
                                          .GetInstance<IDeviceGroupDAL>()
                                          .GetDeviceGroupDetails(this.smisProvider.DeviceGroupID);

    private Configuration Configuration => WizardWorkflowHelper.ConfigurationInfo;

    public string ValidationError { get; private set; }

    public bool ValidateUserInput()
    {
        if (string.IsNullOrEmpty(this.uxIpAddressOrHostname.Text))
        {
            return this.HandleValidationError(string.Format(CultureInfo.CurrentCulture,
                SrmWebContent.Add_Provider_Required_Field_Validator, this.uxIpAddressOrHostnameLabel.Text));
        }

        if (this.uxApiUseSmisCredentials.Checked)
        {
            this.uxSmisUsername.Text = this.uxApiUsername.Text;
            this.uxSmisPassword.Text = this.uxApiPassword.Text;
            this.uxSmisPasswordConfirm.Text = this.uxApiPasswordConfirm.Text;
        }

        if (this.uxSmisCredentialsChoice.SelectedIndex == 0)
        {
            if (string.IsNullOrEmpty(this.uxSmisCredentialName.Text))
            {
                return this.HandleValidationError(string.Format(CultureInfo.CurrentCulture,
                    SrmWebContent.Add_Provider_Required_Field_Validator, this.uxSmisCredentialNameLabel.Text));
            }

            if (string.IsNullOrEmpty(this.uxSmisUsername.Text))
            {
                return this.HandleValidationError(string.Format(CultureInfo.CurrentCulture,
                    SrmWebContent.Add_Provider_Required_Field_Validator, this.uxSmisUsernameLabel.Text));
            }

            if (string.IsNullOrEmpty(this.uxSmisPassword.Text))
            {
                return this.HandleValidationError(string.Format(CultureInfo.CurrentCulture,
                    SrmWebContent.Add_Provider_Required_Field_Validator, this.uxSmisPasswordLabel.Text));
            }

            if (string.IsNullOrEmpty(this.uxSmisPasswordConfirm.Text) &&
                this.uxSmisPassword.Text != this.uxSmisPasswordConfirm.Text)
            {
                return this.HandleValidationError(SrmWebContent.Add_Provider_ConfirmPassword_CompareField_Validator);
            }
        }

        if (this.uxApiCredentialsChoice.SelectedIndex == 0)
        {
            if (string.IsNullOrEmpty(this.uxApiCredentialName.Text))
            {
                return this.HandleValidationError(string.Format(CultureInfo.CurrentCulture,
                    SrmWebContent.Add_Provider_Required_Field_Validator, this.uxApiCredentialNameLabel.Text));
            }

            if (string.IsNullOrEmpty(this.uxApiUsername.Text))
            {
                return this.HandleValidationError(string.Format(CultureInfo.CurrentCulture,
                    SrmWebContent.Add_Provider_Required_Field_Validator, this.uxApiUsernameLabel.Text));
            }

            if (string.IsNullOrEmpty(this.uxApiPassword.Text))
            {
                return this.HandleValidationError(string.Format(CultureInfo.CurrentCulture,
                    SrmWebContent.Add_Provider_Required_Field_Validator, this.uxApiPasswordLabel.Text));
            }

            if (string.IsNullOrEmpty(this.uxApiPasswordConfirm.Text) &&
                this.uxApiPassword.Text != this.uxApiPasswordConfirm.Text)
            {
                return this.HandleValidationError(SrmWebContent.Add_Provider_ConfirmPassword_CompareField_Validator);
            }
        }

        return true;
    }

    public IEditControl InitFromNetObjects(List<string> listOfNetObjectIDs)
    {
        if (listOfNetObjectIDs.Count() != 1)
        {
            return null;
        }

        this.netObjectUniqueIdentifier = new NetObjectUniqueIdentifier(listOfNetObjectIDs.First());

        if (this.netObjectUniqueIdentifier.Prefix != StorageProvider.NetObjectPrefix)
        {
            return null;
        }

        if (!this.TryGetProviders())
        {
            return null;
        }

        if (!this.isLegacyDellProvider)
        {
            if (this.smisProvider.ProviderGroupID.Equals(Guid.Empty) ||
                this.restProvider.ProviderGroupID.Equals(Guid.Empty) ||
                this.smisProvider.ProviderGroupID != this.restProvider.ProviderGroupID)
            {
                return null;
            }
        }

        this.CheckProviderCredentials(this.smisProvider);

        if (!this.isLegacyDellProvider)
        {
            this.CheckProviderCredentials(this.restProvider);
        }

        this.isInitializedFromNetObjectID = true;
        this.testCredentialMethod = "SW.SRM.AddSmisAndHttpProvider.testCredentialsFromEditProviders();";

        this.RefillConfiguration();
        return this;
    }

    public string ValidateJSFunction() => "SW.SRM.AddSmisAndHttpProvider.testCredentialsFromEditProviders";

    public bool Validate(bool connectionPropertyChanged)
    {
        //Is users is using existing credentials we don`t need to validate
        if (this.uxSmisCredentialsChoice.SelectedIndex != 0 && this.uxApiCredentialsChoice.SelectedIndex != 0)
        {
            return true;
        }

        return this.ValidateUserInput();
    }

    public KeyValuePair<string, string>[] GetWarnings() => this.warnings.ToArray();

    public void Update(bool connectionPropertyChanged)
    {
        if (this.IsPostBack)
        {
            EnrollmentWizardProvider updatedSmisProvider = null;
            EnrollmentWizardProvider updatedRestProvider = null;
            if (this.smisProvider.CredentialID.ToString() != this.uxSmisCredentialsChoice.SelectedValue)
            {
                updatedSmisProvider = this.SaveSmisProvider(
                    this.uxIpAddressOrHostname.Text.Trim(),
                    this.uxSmisCredentialName.Text,
                    this.uxSmisUsername.Text,
                    this.uxSmisPassword.Text,
                    this.uxSmisUseHttp.Checked,
                    int.Parse(this.uxSmisHttpPort.Text),
                    int.Parse(this.uxSmisHttpsPort.Text),
                    this.uxSmisInteropNamespace.Text,
                    this.uxSmisArrayNamespace.Text,
                    this.uxSmisCredentialsChoice.SelectedValue);
            }

            if (this.isLegacyDellProvider ||
                this.restProvider.CredentialID.ToString() != this.uxApiCredentialsChoice.SelectedValue)
            {
                updatedRestProvider = this.SaveGenericRest(
                    this.uxIpAddressOrHostname.Text.Trim(),
                    this.uxApiCredentialName.Text,
                    this.uxApiUsername.Text,
                    this.uxApiPassword.Text,
                    int.Parse(this.uxApiHttpsPort.Text),
                    this.uxApiCredentialsChoice.SelectedValue);
            }

            if (!this.isLegacyDellProvider)
            {
                if (updatedSmisProvider != null)
                {
                    ProviderHelper.UpdateProviderInfo(updatedSmisProvider);
                }

                if (updatedRestProvider != null)
                {
                    ProviderHelper.UpdateProviderInfo(updatedRestProvider);
                }
            }
            else
            {
                if (updatedRestProvider != null)
                {
                    this.upgradeLegacyCompelentToNewVersion(
                        updatedSmisProvider ?? new EnrollmentWizardProvider(this.smisProvider), updatedRestProvider);
                }
            }
        }
    }

    private void upgradeLegacyCompelentToNewVersion(EnrollmentWizardProvider updatedSmisProvider,
        EnrollmentWizardProvider updatedRestProvider)
    {
        DeviceGroup restAndSmisDeviceGroup = ServiceLocator.GetInstance<IDeviceGroupDAL>()
            .GetDeviceGroupByFlowType(FlowType.SmisAndGenericHttpExternal);

        Guid newProivderGroupGuid = Guid.NewGuid();

        updatedSmisProvider.DeviceGroupID = restAndSmisDeviceGroup.GroupId;
        updatedRestProvider.DeviceGroupID = restAndSmisDeviceGroup.GroupId;
        updatedSmisProvider.ProviderGroupID = newProivderGroupGuid;
        updatedRestProvider.ProviderGroupID = newProivderGroupGuid;
        ProviderHelper.UpdateProviderInfo(updatedSmisProvider);

        List<ProviderInfo> providers = ProviderHelper.GetProviderInfoOfWizardProviders(
            new List<ProviderEntity> {this.smisProvider},
            new List<EnrollmentWizardProvider> {updatedSmisProvider, updatedRestProvider});

        using (ISrmBusinessLayer proxy = BusinessLayerFactory.Create())
        {
            List<ArrayEntity> arraysOnProvider = ServiceLocator.GetInstance<IStorageArrayDAL>()
                .GetAllArrays(this.smisProvider.ID.Value).ToList();
            List<SelectedArrayEntity> selectedArray = new List<SelectedArrayEntity>();
            arraysOnProvider.ForEach(a => selectedArray.Add(this.updateArraysToSmisAndDell(a)));
            proxy.DiscoveryImport(this.smisProvider.EngineID, providers, selectedArray);
        }
    }

    private SelectedArrayEntity updateArraysToSmisAndDell(ArrayEntity inputArray)
    {
        inputArray.SupportsHardwareHealth = true;
        inputArray.TemplateId = new Guid(SRMConstants.DellCompelentRestAndSmisTemplateID);
        return new SelectedArrayEntity(inputArray)
        {
            IsSelected = true,
            StorageArrayId = inputArray.StorageArrayId
        };
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.uxDeviceTypeInformation.InitWithDevice(this.DeviceGroup);
        if (!this.isInitializedFromNetObjectID)
        {
            this.InitializeEmptyGridText();
        }
        else
        {
            this.InjectPageReadyJS();
            if (!this.IsPostBack)
            {
                string restCredentialID = null;
                if (!this.isLegacyDellProvider)
                {
                    restCredentialID = this.restProvider.CredentialID.ToString();
                }

                this.FillCredentials(this.uxApiCredentialsChoice, CredentialType.GenericHttp,
                    restCredentialID);
                this.FillCredentials(this.uxSmisCredentialsChoice, CredentialType.SMIS,
                    this.smisProvider.CredentialID.ToString());
            }

            this.InitilizeFields();
        }

        this.InitializeFieldValues();
    }

    private void InjectPageReadyJS()
    {
        this.initFromEditProviderJS = "SW.SRM.AddSmisAndHttpProvider.initFromEditProvider()";
    }

    private void InitializeEmptyGridText()
    {
        string initTextsJs = InvariantString.Format("SW.SRM.ProviderSelector.initTexts( {{ noDataAvailable: '{0}' }});",
            SrmWebContent.Provider_Step_NoDataAvailableSmisAndGenericHttp);
        OrionInclude.InlineJs(initTextsJs);
    }

    private void InitializeFieldValues()
    {
        if (this.DeviceGroup.IsHttpsOnly)
        {
            this.uxApiProtocolChoiceRow.Visible = false;
            this.uxApiHttpPortRow.Visible = false;
        }

  }

    private void CheckProviderCredentials(ProviderEntity provider)
    {
        using (ISrmBusinessLayer proxy = BusinessLayerFactory.Create())
        {
            provider.Credential =
                proxy.GetCredential(provider.CredentialID, provider.CredentialType);
        }

        if (provider.Credential == null)
        {
            this.warnings.Add(new KeyValuePair<string, string>("", SrmWebContent.Credentials_Missing));
            log.InfoFormat(
                "Credentials with ID {0} are missing. User is being asked to enter new credentials for provider name: {1}",
                provider.CredentialID, provider.Name);
        }
    }

    private bool TryGetProviders()
    {
        var storageProviderDAL = ServiceLocator.GetInstance<IStorageProviderDAL>();
        var providerFromNetObjectID =
            new ProviderEntityWithCredName(storageProviderDAL.GetProviderDetails(this.netObjectUniqueIdentifier.ID));
        this.isLegacyDellProvider = this.CheckIfLegacyDellCompelent(providerFromNetObjectID.DeviceGroupID);
        if (this.isLegacyDellProvider && providerFromNetObjectID.CredentialType == CredentialType.SMIS)
        {
            this.smisProvider = providerFromNetObjectID;
            return true;
        }

        if (providerFromNetObjectID.ProviderGroupID != Guid.Empty)
        {
            List<ProviderEntity> providers =
                storageProviderDAL.GetAllProvidersByProviderGroupID(
                    new ProviderEntityWithCredName(
                        storageProviderDAL.GetProviderDetails(this.netObjectUniqueIdentifier.ID)).ProviderGroupID);
            if (providers.Count == 2)
            {
                this.smisProvider = providers.FirstOrDefault(x => x.CredentialType == CredentialType.SMIS);
                this.restProvider = providers.FirstOrDefault(x => x.CredentialType == CredentialType.GenericHttp);
                return true;
            }
        }

        return false;
    }

    private void InitilizeFields()
    {
        this.uxSmisHttpPort.Text = this.DeviceGroup.SmisHttpPort.ToString();
        this.uxSmisHttpsPort.Text = this.DeviceGroup.SmisHttpsPort.ToString();
        this.uxSmisInteropNamespace.Text = this.DeviceGroup.InteropNamespace;
        this.uxSmisArrayNamespace.Text = this.DeviceGroup.DefaultNamespace;
        if (this.isLegacyDellProvider)
        {
            this.uxApiHttpsPort.Text = SRMConstants.DefaultCompelentApiHttpsPort.ToString();
        }
        else
        {
            this.uxApiHttpsPort.Text = this.DeviceGroup.ApiHttpsPort.ToString();
        }

        this.uxIpAddressOrHostname.Text = this.smisProvider.IPAddress;
    }

    private EnrollmentWizardProvider SaveGenericRest(string ipAddress,
        string displayName,
        string userName,
        string password,
        int port,
        string credentialIDstring)
    {
        GenericHttpCredential apiCredential;
        int credentialID;

        if ((displayName ?? userName ?? password) == null)
        {
            return null;
        }

        if (!int.TryParse(credentialIDstring, out credentialID))
        {
            credentialID = 0;
        }

        if (credentialID <= 0)
        {
            apiCredential = new GenericHttpCredential
            {
                Name = displayName,
                Username = userName,
                Password = password,
                Port = port,
                UseSsl = true
            };

            using (ISrmBusinessLayer proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(apiCredential, CredentialType.GenericHttp);
                apiCredential.ID = id;
            }
        }
        else
        {
            using (ISrmBusinessLayer proxy = BusinessLayerFactory.Create())
            {
                apiCredential = (GenericHttpCredential) proxy.GetCredential(credentialID, CredentialType.GenericHttp);
                if (apiCredential == null)
                {
                    this.AddWarningForMissingCredentials(credentialID);
                    return null;
                }
            }
        }

        var providerEntity = new EnrollmentWizardProvider
        {
            ProviderType = ProviderTypeEnum.Block,
            ProviderLocation = ProviderLocation.External,
            IPAddress = ipAddress
        };

        providerEntity.CredentialID = apiCredential.ID.Value;
        providerEntity.CredentialType = CredentialType.GenericHttp;
        providerEntity.Credential = apiCredential;
        if (!this.isLegacyDellProvider)
        {
            providerEntity.ID = this.restProvider.ID;
        }

        return providerEntity;
    }

    private void AddWarningForMissingCredentials(int credentialID)
    {
        this.warnings.Add(new KeyValuePair<string, string>("", SrmWebContent.Credentials_Missing));
        log.InfoFormat(
            "Credentials with ID {0} are missing. User is being asked to enter new credentials",
            credentialID.ToString());
    }

    private EnrollmentWizardProvider SaveSmisProvider(string ipAddress,
        string displayName,
        string userName,
        string password,
        bool useHttp,
        int httpPort,
        int httpsPort,
        string interopNamespace,
        string arrayNamespace,
        string credentialIDstring)
    {
        Credential credential;
        if ((displayName ?? userName ?? password ?? interopNamespace ?? arrayNamespace) == null)
        {
            return null;
        }

        int credentialID;
        if (!int.TryParse(credentialIDstring, out credentialID))
        {
            credentialID = 0;
        }

        if (credentialID <= 0)
        {
            credential = new SmisCredentials
            {
                Name = displayName,
                Username = userName,
                Password = password,
                HttpPort = (uint) httpPort,
                HttpsPort = (uint) httpsPort,
                UseSSL = !useHttp,
                InteropNamespace = interopNamespace,
                Namespace = arrayNamespace
            };

            using (ISrmBusinessLayer proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(credential, CredentialType.SMIS);
                credential.ID = id;
            }
        }
        else
        {
            using (ISrmBusinessLayer proxy = BusinessLayerFactory.Create())
            {
                credential = proxy.GetCredential(credentialID, CredentialType.SMIS);
                if (credential == null)
                {
                    this.AddWarningForMissingCredentials(credentialID);
                }

                return null;
            }
        }

        var providerEntity = new EnrollmentWizardProvider
        {
            IPAddress = ipAddress,
            CredentialType = this.smisProvider.CredentialType,
            ProviderType = this.smisProvider.ProviderType,
            ProviderLocation = this.smisProvider.ProviderLocation,
            Credential = credential,
            CredentialID = credential.ID.Value
        };
        providerEntity.ID = this.smisProvider.ID;
        return providerEntity;
    }


    private void RefillConfiguration()
    {
        WizardWorkflowHelper.ConfigurationInfo.DeviceGroup = this.DeviceGroup;
        WizardWorkflowHelper.ConfigurationInfo.ProviderConfiguration.AvailableProviderEntities = null;

        EngineEntity engine = ServiceLocator.GetInstance<IEngineDAL>().GetEngine(this.smisProvider.EngineID);

        WizardWorkflowHelper.ConfigurationInfo.PollingConfig.BusinessLayerPort = engine.BusinessLayerPort;
        WizardWorkflowHelper.ConfigurationInfo.PollingConfig.EngineID = engine.EngineID;
        WizardWorkflowHelper.ConfigurationInfo.PollingConfig.ServerName = engine.ServerName;
    }

    private void FillCredentials(DropDownList dropDownList, CredentialType type, string selectedCredentials)
    {
        List<KeyValuePair<string, string>> items = new List<KeyValuePair<string, string>>
        {
            new KeyValuePair<string, string>("", SrmWebContent.Provider_Step_New_Credential)
        };
        using (ISrmBusinessLayer proxy = BusinessLayerFactory.Create())
        {
            IDictionary<int, string> credentialsList = proxy.GetCredentialNames(type);
            foreach (KeyValuePair<int, string> cred in credentialsList)
            {
                items.Add(new KeyValuePair<string, string>(cred.Key.ToString(CultureInfo.InvariantCulture),
                    cred.Value));
            }
        }

        dropDownList.DataTextField = "Value";
        dropDownList.DataValueField = "Key";
        dropDownList.DataSource = items;

        if (!string.IsNullOrEmpty(selectedCredentials) && items.Any(pair => pair.Key == selectedCredentials))
        {
            dropDownList.SelectedValue = selectedCredentials;
        }

        dropDownList.DataBind();
    }

    private bool CheckIfLegacyDellCompelent(Guid DeviceGroupID) =>
        DeviceGroupID == new Guid(SRMConstants.DellCompelentSmisDeviceGroup);

    private bool HandleValidationError(string message)
    {
        this.ValidationError = message;
        return false;
    }
}