﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Web.EnrollmentWizard;
using SolarWinds.SRM.Common.Models;

public partial class Orion_SRM_Config_Controls_AddDfmProvider : System.Web.UI.UserControl
{
    public string initTextsJs = InvariantString.Format("SW.SRM.ProviderSelector.initTexts( {{ noDataAvailable: '{0}' }});", SrmWebContent.Provider_Step_NoDataAvailableDfm);
    public DeviceGroup DeviceGroup { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        deviceInfo.InitWithDevice(DeviceGroup);
        OrionInclude.InlineJs(initTextsJs);

        var configuration = WizardWorkflowHelper.ConfigurationInfo;

        HttpPortTextBox.Value = configuration.DeviceGroup.Properties["HttpPort"];
        HttpsPortTextBox.Value = configuration.DeviceGroup.Properties["HttpsPort"];
    }

    public string AddProviderToolbarLabel = SrmWebContent.Provider_Step_Add_Dfm_Btn_Label;
    public string Provider_Step_Add_Provider_Title = SrmWebContent.Provider_Step_Add_Dfm_Provider_Title;
}
