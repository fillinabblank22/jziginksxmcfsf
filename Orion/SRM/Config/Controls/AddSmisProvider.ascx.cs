﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json.Linq;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Helpers;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.EnrollmentWizard;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.NetObjects;

public partial class Orion_SRM_Config_Controls_AddSmisProvider : UserControl
{
    private ArrayEntity array;

    protected int EngineId {
        get
        {
            return array == null ? WizardWorkflowHelper.ConfigurationInfo.PollingConfig.EngineID : array.EngineID;
        }
    }

    private bool hostedOnEditProperties = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        deviceInfo.InitWithDevice(DeviceGroup);
        if (!this.hostedOnEditProperties)
        {
            string initTextsJs = string.Format(CultureInfo.InvariantCulture,
                "SW.SRM.ProviderSelector.initTexts( {{ noDataAvailable: '{0}' }});",
                SrmWebContent.Provider_Step_NoDataAvailableSMIS);
            OrionInclude.InlineJs(initTextsJs);
        }
    }

    public string ArrayNamespace
    {
        get
        {
            string defaultNameSpace = WebSettingsDAL.GetValue(SRMConstants.SmisArrayNamespaceSettingID, string.Empty);
            
            return DeviceGroup == null
                        ? defaultNameSpace
                        : DeviceGroup.GetProperty(SRMConstants.DefaultArrayNamespacePropertyName, defaultNameSpace);
        }
    }

    public string TestConnectionErrorJSDelegate
    {
        get
        {
            return DeviceGroup == null
                        ? string.Empty
                        : DeviceGroup.GetProperty(SRMConstants.TestConnectionErrorJSDelegateName, string.Empty);
        }
    }

    public string InteropNamespace
    {
        get
        {
            return DeviceGroup.InteropNamespace;
        }
    }

    public string AddProviderToolbarLabel = SrmWebContent.Provider_Step_Add_Btn_Label;
    public string Provider_Step_Add_Provider_Title = SrmWebContent.Provider_Step_Add_Smis_Provider_Title;
    public DeviceGroup DeviceGroup { get; set; }

    private void FillControl()
    {
        this.hostedOnEditProperties = true;
        // Add Scripts/Styles included by hosted page
        OrionInclude.ModuleFile("SRM", "Discovery.css");
        OrionInclude.ModuleFile("SRM", "SRM.css");
        OrionInclude.ModuleFile("SRM", "EditProperties.SmisProvider.css");

        HiddenField hfCfg = new HiddenField();
        hfCfg.ClientIDMode = ClientIDMode.Static;
        hfCfg.ID = "hfEditPropertiesCfg";
        this.hfPlaceholder.Controls.Add(hfCfg);
        
        HiddenField hfProviderGuid = new HiddenField();
        hfProviderGuid.ClientIDMode = ClientIDMode.Static;
        hfProviderGuid.ID = "hfProviderGuid";
        this.hfPlaceholder.Controls.Add(hfProviderGuid);

        HiddenField hfDiscoveryGuid = new HiddenField();
        hfDiscoveryGuid.ClientIDMode = ClientIDMode.Static;
        hfDiscoveryGuid.ID = "hfDiscoveryGuid";
        this.hfPlaceholder.Controls.Add(hfDiscoveryGuid);

        var jsSer = new JavaScriptSerializer();
        var providers = ServiceLocator.GetInstance<IStorageProviderDAL>().GetAllProvidersByArrayId(array.StorageArrayId);
        
        var blockProvider = providers.First(p => p.ProviderType == ProviderTypeEnum.Block);
        hfCfg.Value = jsSer.Serialize(new
        {
            ArrayId = array.StorageArrayId,
            IP = blockProvider.IPAddress,
            CredentialID = blockProvider.CredentialID,
            EngineId = array.EngineID
        });
    }
}
