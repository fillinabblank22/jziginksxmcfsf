﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SnmpVersionThreeCredentialsControl.ascx.cs" Inherits="Orion_SRM_Config_Controls_SnmpVersionThreeCredentialsControl" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common" %>

<orion:Include runat="server" Module="SRM"  File="Helpers.js" />

<table class="srm-EnrollmentWizard-providerStepInputsTable">
    <tr>
        <td><%= SrmWebContent.Provider_Step_Snmp_Credential%>*:</td>
        <td>
            <asp:DropDownList runat="server" id="SnmpCredentialDropDownSnmpV3" onChange = "return OnSelectedCredentialChangeSnmpV3(this);" ClientIDMode="Static" />
        </td>
    </tr>
</table>
<table class="srm-EnrollmentWizard-providerStepInputsTable" id="StoredCredFiledsV3">
    <tr>
        <td ><%= SrmWebContent.Provider_Step_Credential_Name%>*:</td>
        <td>
            <asp:TextBox runat="server" id="SnmpV3CredentialNameTextBox" ClientIDMode="Static" onChange = "return OnSelectedValueChangeSnmpV3();"/>
        </td>
    </tr>
    <tr>
        <td><%= SrmWebContent.Provider_Step_Array_Snmp_User_Name%>*:</td>
        <td>
            <asp:TextBox runat="server" id="SnmpUserNameTextBox" onChange = "return OnSelectedValueChangeSnmpV3();" ClientIDMode="Static"/>
        </td>
    </tr>
    <tr>
        <td id="arraySnmpContext" ><%= SrmWebContent.Provider_Step_Array_Snmp_Context%>*:</td>
        <td>
            <asp:TextBox runat="server" id="SnmpContextTextBox" onChange = "return OnSelectedValueChangeSnmpV3();" TextMode="Password" ClientIDMode="Static"/>
        </td>
    </tr>
    <tr>
        <td><%= SrmWebContent.Provider_Step_Array_Snmp_Port%>*:</td>
        <td>
            <asp:TextBox runat="server" id="SnmpPortTextBoxV3" onChange = "return OnSelectedValueChangeSnmpV3();" ClientIDMode="Static"/>
        </td>
    </tr>
    <tr>
        <td><%= SrmWebContent.Provider_Step_Array_Authentication_Method%>:</td>
        <td>
            <asp:DropDownList runat="server" id="AuthenticationMethodDropDown" onChange = "return OnDropDownListChanged(this);"  ClientIDMode="Static" />
        </td>
    </tr>
    <tr>
        <td />
        <td>
            <table id="AuthenticationMethodOptions">
                <tr>
                    <td class="srm-provider-step-password-column"><%= SrmWebContent.Provider_Step_PasswordKey%>:</td>
                    <td>
                        <asp:TextBox runat="server" id="PasswordKey1TextBox" onChange = "return OnSelectedValueChangeSnmpV3();"  TextMode="Password" ClientIDMode="Static" class="srm-provider-step-password-input"/>
                    </td>
                </tr>
                <tr>
                    <td />
                    <td>
                        <asp:CheckBox runat="server" id="PasswordKey1CheckBox" onChange = "return OnSelectedValueChangeSnmpV3();" ClientIDMode="Static" Text ="<%$ Resources:SrmWebContent ,Provider_Step_Pasword_Is_Key%>"/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td><%= SrmWebContent.Provider_Step_Privacy_Encryption_Method%>:</td>
        <td>
            <asp:DropDownList runat="server" id="EncryptionMethodDropDown" onChange = "return OnDropDownListChanged();" ClientIDMode="Static" />
        </td>
    </tr>
    <tr>
        <td />
        <td>
            <table id="EncryptionMethodOptions">
                <tr>
                    <td class="srm-provider-step-password-column"><%= SrmWebContent.Provider_Step_PasswordKey%>:</td>
                    <td>
                        <asp:TextBox runat="server" ID="PasswordKey2TextBox" onChange="return OnSelectedValueChangeSnmpV3();" TextMode="Password" ClientIDMode="Static" class="srm-provider-step-password-input" />
                    </td>
                </tr>
                <tr>
                    <td />
                    <td>
                        <asp:CheckBox runat="server" ID="PasswordKey2CheckBox" onChange="return OnSelectedValueChangeSnmpV3();" ClientIDMode="Static" Text="<%$ Resources:SrmWebContent, Provider_Step_Pasword_Is_Key%>" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<script>
    $(document).ready(function () {
        $('#AuthenticationMethodOptions').hide();
        $('#EncryptionMethodOptions').hide();

        if ('<%= IsContextRequired %>' == "False") {
            $('#arraySnmpContext').text("Array SNMP Context:");
        }
        });

    function OnDropDownListChanged() {
        OnSelectedValueChangeSnmpV3();

        var authMode = $('#AuthenticationMethodDropDown').val();
        if (authMode == 0) {
            $('#AuthenticationMethodOptions').hide();
        } else {
            $('#AuthenticationMethodOptions').show();
        }

        var encrypMethod = $("#EncryptionMethodDropDown").val();
        if (encrypMethod == 0) {
            $('#EncryptionMethodOptions').hide();
        } else {
            $('#EncryptionMethodOptions').show();
        }
    }
    
    function OnSelectedValueChangeSnmpV3() {

    }

    function ValidateV3Controls(ip, deviceGroupId, isRequiredPostBack, runDiscovery) {

        var credentialID = $('#SnmpCredentialDropDownSnmpV3').val();
        var credentialName = $("#SnmpV3CredentialNameTextBox").val();
        var userName = $("#SnmpUserNameTextBox").val();
        var context = $("#SnmpContextTextBox").val();
        var authenticationValue = $("#AuthenticationMethodDropDown").val();
        var privacyValue = $("#EncryptionMethodDropDown").val();
        var authenticationPassword = $("#PasswordKey1TextBox").val();
        var authenticationKeyIsPassword = $("#PasswordKey1CheckBox").is(':checked');
        var privacyPassword = $("#PasswordKey2TextBox").val();
        var privacyKeyIsPassword = $("#PasswordKey2CheckBox").is(':checked');
        var port = $("#SnmpPortTextBoxV3").val();

        if (!ValidateFormV3(ip,
            userName,
            context,
            authenticationValue,
            privacyValue,
            authenticationPassword,
            privacyPassword,
            port)) {
            $('#TestConnectionButton').attr("disabled", false);
            SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
            return;
        }

        HideMessageBoxes();

        $("#testingConnectionMsg").show();
        request = SW.SRM.Common.CallSrmWebService('/Orion/SRM/Services/EnrollmentWizardService.asmx',
            'TestSnmpV3Credentials',
            {
                credentialID: credentialID,
                credentialName: credentialName,
                userName: userName,
                context: context,
                authenticationValue: authenticationValue,
                privacyValue: privacyValue,
                authenticationPassword: authenticationPassword,
                authenticationKeyIsPassword: authenticationKeyIsPassword,
                privacyPassword: privacyPassword,
                privacyKeyIsPassword: privacyKeyIsPassword,
                ip: ip,
                port: port,
                saveProvider: runDiscovery,
                groupId: deviceGroupId
            },
            function onSuccessCallback(result) {
                $("#testingConnectionMsg").hide();
                $('#TestConnectionButton').attr("disabled", false);
                if (result.IsSuccess) {
                    ShowPassMessageBox(result.Message);
                    if (isRequiredPostBack) {
                        providerStepSubmit();
                    }
                    if (runDiscovery)
                        SW.SRM.DiscoveryValidation.startDiscovery();
                    else
                        SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
                } else {
                    ShowFailMessageBox(result.Message, result.ErrorMessage);
                    SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
                }
            },
            function onFailedCallback() {
                $("#testingConnectionMsg").hide();
                $('#TestConnectionButton').attr("disabled", false);

                if (request.status !== 0) {
                    ShowFailMessageBox("<%= SrmWebContent.Provider_Step_Unexpected_Error%>");
                    SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
                }
            });
    }
    
    function ValidateFormV3(ip, username, context, isAuthenticationRequired, isPrivacyRequired, authenticationPassword, privacyPassword, port) {

        var title = "<%= Resources.SrmWebContent.ProviderStep_Validation_Error %>";

        if (!ip.trim()) {
            SW.SRM.Common.ShowError(title,
                "<%= Resources.SrmWebContent.ProviderStep_ProviderIpAddress_Required_Field_Validator %>");
            return false;
        }


        if ($("#SnmpCredentialDropDownSnmpV3").val().trim() == "<%= SRMConstants.NewCredential %>") {

            var credDisplayName = $('#SnmpV3CredentialNameTextBox').val();

            if (!credDisplayName.trim()) {
                SW.SRM.Common.ShowError(title,
                    "<%= Resources.SrmWebContent.ProviderStep_ProviderCredentialNameStr_Required_Field_Validator %>");
                return false;
            }

            var alreadyExists = comboHasText($('#SnmpCredentialDropDownSnmpV3'), credDisplayName);
            if (alreadyExists) {
                title = "<%= SrmWebContent.Add_Provider_CredentialNameAlreadyExistHeader %>";
                var message =
                    "<a href='javascript:selectCredentials($(&quot;#SnmpCredentialDropDownSnmpV3&quot;),&quot;" +
                        credDisplayName +
                        "&quot;)'><%= SrmWebContent.Add_Provider_CredentialNameAlreadyExist_LoadSaved %></a> or <a href='javascript:focusCredentialsDisplayName($(&quot;#SnmpV3CredentialNameTextBox&quot;))'><%= SrmWebContent.Add_Provider_CredentialNameAlreadyExist_RenameDuplicate %></a>";
                SW.SRM.Common.ShowError(title, message);
                return false;
            }

            if (!username.trim()) {
                SW.SRM.Common.ShowError(title,
                    "<%= Resources.SrmWebContent.ProviderStep_ProviderUsername_Required_Field_Validator %>");
                return false;
            }

            if ("<%= IsContextRequired %>" === "True" && !context.trim()) {
                SW.SRM.Common.ShowError(title,
                    "<%= Resources.SrmWebContent.ProviderStep_ProviderSNMPContext_Required_Field_Validator %>");
                return false;
            }

            if (!port.trim()) {
                SW.SRM.Common.ShowError(title,
                    "<%= Resources.SrmWebContent.ProviderStep_ProviderPortStr_Required_Field_Validator %>");
                return false;
            }

            if (isAuthenticationRequired !=
                "<%= ((int)SolarWinds.Orion.Core.Common.Models.SNMPv3AuthType.None).ToString() %>") {
                if (!authenticationPassword.trim()) {
                    SW.SRM.Common.ShowError(title,
                        "<%= Resources.SrmWebContent.ProviderStep_ProviderAuthenticationPassword_Required_Field_Validator %>");
                    return false;
                }
            }

            if (isPrivacyRequired !=
                "<%= ((int)SolarWinds.Orion.Core.Common.Models.SNMPv3PrivacyType.None).ToString() %>") {
                if (!privacyPassword.trim()) {
                    SW.SRM.Common.ShowError(title,
                        "<%= Resources.SrmWebContent.ProviderStep_ProviderPrivacyPassword_Required_Field_Validator %>");
                    return false;
                }
            }
        }
        return true;
    }
</script>


