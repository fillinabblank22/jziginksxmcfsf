﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExternalProvider.ascx.cs" Inherits="Orion_SRM_Config_Controls_ExternalProvider" %>
<%@ Register TagPrefix="UIInfo" TagName="DeviceType" Src="~/Orion/SRM/Config/Controls/DeviceType.ascx" %>
<%@ Register TagPrefix="UIInfo" TagName="TestConnectionMessages" Src="~/Orion/SRM/Config/Controls/TestConnectionMessages.ascx" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="SRM/Config/Controls/AddSmisAndGenericHttpProvider.js" />
<orion:Include runat="server" File="SRM.EditProperties.SmisProvider.css" Module="SRM" />
<orion:Include runat="server" File="SRM.css" Module="SRM" />
<orion:Include runat="server" File="Discovery.css" Module="SRM" />

<asp:PlaceHolder runat="server" ID="hfPlaceholder"></asp:PlaceHolder>
<div class="srm-main-content smisprovider">
    <table>
        <tr>
            <td class="providerPropertiesColumn">
                <table>
                    <tr>
                        <td class="label"><span id="labelArrayType">Array Type:</span></td>
                        <td class="boldText"><span><%= this.arrayType %></span></td>
                    </tr>
                    <tr>
                        <td class="label"><span><span id="labelProvider"><%= this.providerLabel %></span>:</span>
                            <div id="loadingImageSmisPlaceholder">
                                <asp:Image ImageUrl="~/Orion/images/loading_gen_small.gif" style="display: none;" ClientIDMode="Static" ID="loadingIconSmisProvider" runat="server" AlternateText="Loading" />
                            </div>
                        </td>
                        <td>
                            <asp:HyperLink ID="hlProvider" runat="server" >
                                <asp:Image ID="providerStatusIcon" runat="server" />
                                <span id="providerName" runat="server"></span>
                            </asp:HyperLink>
                            <br/>
                            <input type="button" id="testConnectionSmisButton" class="providerStepButton" value="<%= SrmWebContent.Provider_Step_Test_Connection %>" onclick="SW.SRM.AddSmisAndHttpProvider.testCrendialsFromEditProperties();" />
                        </td>
                    </tr>
                </table>
                <UIInfo:TestConnectionMessages runat="server" ID="TestConnectionMessages" ContainerId="testSmisProvider" JsCancellationFunction="javascript:SW.SRM.AddSmisAndHttpProvider.cancelTestCredentials();"/>
            </td>
            <td>
                <UIInfo:DeviceType runat="server" ID="uxDeviceTypeInformation" />
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        SW.SRM.AddSmisAndHttpProvider.init({ 
            deviceGroupId: '<%= DeviceGroup.GroupId %>',
            connectionTestFailedTitle: '<%= SrmWebContent.Provider_Step_Provider_Connecting_Error %>',
            connectionTestUnexpectedFailedMessage: '<%= SrmWebContent.Provider_Step_Unexpected_Error %>',
            smisHttpPort: <%= DeviceGroup.SmisHttpPort %>,
            smisHttpsPort: <%= DeviceGroup.SmisHttpsPort %>,
            smisArrayNamespace: '<%= DeviceGroup.DefaultNamespace %>',
            smisInteropNamespace: '<%= DeviceGroup.InteropNamespace %>',
            apiHttpPort: <%= DeviceGroup.ApiHttpPort %>,
            apiHttpsPort: <%= DeviceGroup.ApiHttpsPort %>,
        });

        $("#addNewProviderBtn").click(function () {
            SW.SRM.AddSmisAndHttpProvider.showDialog();
            return false;
        });

        $("#addNewProviderLabel").click(function () {
            SW.SRM.AddSmisAndHttpProvider.showDialog();
            return false;
        });

        $(".srm-testing-connection-msgbox").hide();
    });
</script>