﻿Ext.namespace('SW');
Ext.namespace('SW.SRM');

SW.SRM.LicenseBar = function() {
    var selected = new Array(),
        internalIsExpired,
        internalLicenseDetailsText,
        internalUnlimitedText,
        internalIsEval;

    var renderFunction = function() {
        var licenseContent = $("#licenseContent");
        var requiredField = $("#hiddenRequired").val();
        var inUseField = $("#hiddenInUse").val();
        var totalField = $("#hiddenTotal").val();
       
        var remaining = parseInt(totalField) - parseInt(inUseField);
        if (selected.length === 0) {
            if (remaining < 0 && requiredField > 0) {
                $("#licenseExceeded").show();
                $("#hiddenLicViolation").val('1');
            } else {
                $("#licenseExceeded").hide();
                $("#hiddenLicViolation").val('0');
            }
        }

        var licenseDetails = internalLicenseDetailsText;
        licenseDetails = licenseDetails.replace('{0}', requiredField);
        licenseDetails = licenseDetails.replace('{1}', inUseField);
        licenseDetails = licenseDetails.replace('{2}', Math.max(0, remaining));
        licenseDetails = licenseDetails.replace('{3}', totalField);

        licenseContent.empty();
        
        if (internalIsExpired == 1) {
            $("#licenseExceeded").show();
            $("#licenseBreakerLine").hide();
            $("#hiddenLicViolation").val('1');
            licenseContent.hide();
        } else if (internalIsEval == 1) {
            licenseContent.append(internalUnlimitedText);
        } else {
            licenseContent.append(licenseDetails);
        }
    };

    return {
        init: function (licenseDetailsText, unlimitedText, isEval, isExpired) {
            internalLicenseDetailsText = licenseDetailsText;
            internalUnlimitedText = unlimitedText;
            internalIsEval = isEval;
            internalIsExpired = isExpired;

            $("#licenseExceeded").hide();
            $("#hiddenLicViolation").val('0');

            if (SW.SRM.ArraySelector != undefined) {
                SW.SRM.ArraySelector.onLicenseUpdate = function () {
                    selected = new Array();
                    $.each(SW.SRM.ArraySelector.getGrid().getSelectionModel().selections.items, function (id, item) {
                        if (item.data.IsLicensed !== true) {
                            selected.push(parseInt(item.data.Disks));
                        }
                    });

                    $("#licenseExceeded").hide();
                    $("#hiddenLicViolation").val('0');

                    var required = 0;
                    selected.sort(function (a, b) { return b - a; });
                    $.each(selected, function (id, disks) {
                        var remaining = parseInt($("#hiddenTotal").val()) - (parseInt($("#hiddenInUse").val()) + parseInt(required));
                        if (remaining <= 0) {
                            $("#licenseExceeded").show();
                            $("#hiddenLicViolation").val('1');
                        }

                        required += parseInt(disks);
                    });

                    $("#hiddenRequired").val(required);

                    renderFunction();
                };
            }

            renderFunction();
        }
    };
}();