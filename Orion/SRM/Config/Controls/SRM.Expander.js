﻿/// <reference path="~/Orion/js/jquery-1.7.1/jquery.js" />


function ChooseCredentialDdl_OnChange(ddl) {
    var $containerElement = $("#AdvancedCredFiledsTable");
    if (ddl.value) {
        $("#StoredCredFiledsTable").hide();
        $('#expanderElement').hide();
        $containerElement.hide();
        $("#testConnectionFailMsg").hide();
        $("#testConnectionPassMsg").hide();
    } else {
        $("#StoredCredFiledsTable").show();
        $('#expanderElement').show();
        $("#testConnectionFailMsg").hide();
        $("#testConnectionPassMsg").hide();

        if ($('#expanderElement img').attr('src') == '/Orion/SRM/images/Button.Collapse.gif') {
            $containerElement.show();
        } else {
            $containerElement.hide();
        }
    }
}

function expanderChangedHandler() {
    var $containerElement = $('#AdvancedCredFiledsTable');
    var $expanderImgElement = $('#expanderElement img');
    if (!$containerElement.is(':visible')) {
        $containerElement.show();
        $expanderImgElement.attr('src', '/Orion/SRM/images/Button.Collapse.gif');
        $expanderImgElement.attr('alt', '[-]');
    } else {
        $containerElement.hide();
        $expanderImgElement.attr('src', '/Orion/SRM/images/Button.Expand.gif');
        $expanderImgElement.attr('alt', '[+]');
    }
}