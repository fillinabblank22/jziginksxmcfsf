﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using Resources;
using SolarWinds.Logging;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.Credentials;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.EnrollmentWizard;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.Resources;

public partial class Orion_SRM_Config_Controls_EditProvider : UserControl, IConfigurationControl, ICreateEditControl, IEditControl
{
    private static readonly Log log = new Log();
    private NetObjectUniqueIdentifier netObjectUniqueIdentifier;
    private ProviderEntityWithCredName provider;
    private ProviderInfo providerFromForm;
    private DeviceGroup deviceGroup;
    private readonly List<KeyValuePair<string, string>> warnings = new List<KeyValuePair<string, string>>();

    protected FlowType FlowType
    {
        get
        {
            return deviceGroup.FlowType;
        }
    }

    protected int EngineId
    {
        get
        {
            return provider.EngineID;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var provider = String.Empty;
        if (FlowType == FlowType.NetAppDfm)
        {
            provider = "AddDfmProvider";
        }
        else if (FlowType == FlowType.XtremIO)
        {
            provider = "AddXtremIoProvider";
        }
        else
        {
            provider = "AddSmisProvider";
        }
        this.ProviderControl.JsCancellationFunction = InvariantString.Format("javascript:SW.SRM.{0}.cancelTestCredential();", provider);
    }

    public string ValidationError { get; private set; }

    public void SaveValues()
    {
    }

    public bool ValidateUserInput()
    {
        ValidationError = this.ProviderControl.GetUserInputValidationErrors().FirstOrDefault(e => !string.IsNullOrEmpty(e));

        return string.IsNullOrEmpty(ValidationError);
    }

    public IEditControl InitFromNetObjects(List<string> listOfNetObjectIDs)
    {
        // Supports just one NetObject ID and just SMP
        if (listOfNetObjectIDs.Count() != 1 || (netObjectUniqueIdentifier = new NetObjectUniqueIdentifier(listOfNetObjectIDs.First())).Prefix != SolarWinds.SRM.Web.NetObjects.StorageProvider.NetObjectPrefix)
        {
            return null;
        }

        provider = new ProviderEntityWithCredName(ServiceLocator.GetInstance<IStorageProviderDAL>().GetProviderDetails(netObjectUniqueIdentifier.ID));
        deviceGroup = ServiceLocator.GetInstance<IDeviceGroupDAL>().GetDeviceGroupDetails(provider.DeviceGroupID);

        // Supporting just DFM and SMI-S Block and Unified 
        if (provider.CredentialType != CredentialType.XtremIoHttp 
            && provider.CredentialType != CredentialType.NetAppDfm 
            && (provider.CredentialType != CredentialType.SMIS || provider.ProviderType == ProviderTypeEnum.File)
            || this.provider.DeviceGroupID == new Guid(SRMConstants.DellCompelentSmisDeviceGroup)
            || !this.provider.ProviderGroupID.Equals(Guid.Empty))
        {
            return null;
        }
        

        using (var proxy = BusinessLayerFactory.Create())
        {
            provider.Credential = proxy.GetCredential(provider.CredentialID, provider.CredentialType);
        }

        if (provider.Credential == null)
        {
            //Display warning about missing credentials
            this.warnings.Add(new KeyValuePair<string, string>("", SrmWebContent.Credentials_Missing));

            // Credentials are missing. User should enter new valid credentials
            log.InfoFormat("Credentials with ID {0} are missing. User is being asked to enter new credentials for provider {1}", provider.CredentialID, provider.Name);
        }

        RefillConfiguration();
        FillControl();

        return this;
    }

    private void FillControl()
    {
        this.ProviderControl.IPAddressOrHostName = string.IsNullOrEmpty(provider.HostName) ? provider.IPAddress : provider.HostName;

        IDictionary<int, string> existingCredentials;
        using (var proxy = BusinessLayerFactory.Create())
        {
            existingCredentials = proxy.GetCredentialNames(provider.CredentialType);
        }

        var credentialDisplayNames = new Dictionary<string, string>() { { string.Empty, Resources.SrmWebContent.Provider_Step_New_Credential } };

        foreach (var credential in existingCredentials)
        {
            credentialDisplayNames.Add(credential.Key.ToString(CultureInfo.InvariantCulture), credential.Value);
        }
        this.ProviderControl.FillCredentialsList(credentialDisplayNames, provider.CredentialID);
    }

    private void RefillConfiguration()
    {
        WizardWorkflowHelper.ConfigurationInfo.DeviceGroup = deviceGroup;
        WizardWorkflowHelper.ConfigurationInfo.ProviderConfiguration.AvailableProviderEntities = null;

        var engine = ServiceLocator.GetInstance<IEngineDAL>().GetEngine(provider.EngineID);

        WizardWorkflowHelper.ConfigurationInfo.PollingConfig.BusinessLayerPort = engine.BusinessLayerPort;
        WizardWorkflowHelper.ConfigurationInfo.PollingConfig.EngineID = engine.EngineID;
        WizardWorkflowHelper.ConfigurationInfo.PollingConfig.ServerName = engine.ServerName;
    }

    public string ValidateJSFunction()
    {
        if (FlowType == FlowType.NetAppDfm)
        {
            return "SW.SRM.AddDfmProvider.InitValidation()";
        }
        if (FlowType == FlowType.XtremIO)
        {
            return "SW.SRM.AddXtremIoProvider.InitValidation()";
        }
        return "SW.SRM.AddSmisProvider.InitValidation()";
    }

    public bool Validate(bool connectionPropertyChanged)
    {
        if (!this.ValidateUserInput())
        {
            return false;
        }

        providerFromForm = this.ProviderControl.GetProviderInfo();
        if (providerFromForm == null)
        {
            ValidationError = InvariantString.Format(SrmWebContent.EditProviderUnableToGetProviderInfo);
            return false;
        }

        providerFromForm.CredentialType = provider.CredentialType;
        providerFromForm.ProviderType = provider.ProviderType;

        if (providerFromForm.IPAddress != provider.IPAddress && ProviderHelper.ProviderExistsAlready(providerFromForm))
        {
            ValidationError = InvariantString.Format(SrmWebContent.EditProviderProviderExistsAlready);
            return false;
        }

        providerFromForm.ID = provider.ID;

        return true;
    }

    public KeyValuePair<string, string>[] GetWarnings()
    {
        return this.warnings.ToArray();
    }

    public void Update(bool connectionPropertyChanged)
    {
        using (var proxy = BusinessLayerFactory.Create())
        {
            proxy.UpdateProvider(providerFromForm);
        }
    }
}
