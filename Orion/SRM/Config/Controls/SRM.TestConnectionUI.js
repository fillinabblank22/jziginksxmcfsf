﻿var testConnectionUI = SW.Core.namespace("SW.SRM.TestConnectionUI");

testConnectionUI.HideError = function (provider) {
    SW.SRM.TestConnectionUI.Get("#failMessageHeader", provider).hide();
    SW.SRM.TestConnectionUI.Get("#testConnectionFailMsg", provider).hide();
};

testConnectionUI.HidePass = function (provider) {
    SW.SRM.TestConnectionUI.Get("#testConnectionPassMsg", provider).hide();
};

testConnectionUI.ShowPassMessageBox = function (text, provider) {
    if (text) {
        var passMessageContent = SW.SRM.TestConnectionUI.Get('#passMessageContent', provider);
        passMessageContent.empty();
        passMessageContent.append(text);
    }
    SW.SRM.TestConnectionUI.Get('#testConnectionPassMsg', provider).show();
};

testConnectionUI.ShowDiscovery = function (provider) {
    SW.SRM.TestConnectionUI.Get('#runningDiscoveryMsg', provider).show();
};

testConnectionUI.HideDiscovery = function (provider) {
    SW.SRM.TestConnectionUI.Get('#runningDiscoveryMsg', provider).hide();
};

testConnectionUI.Get = function (elementId, provider) {
    var result = null;
    
    if (provider && provider.getTestConnectionMessagesDiv) {
        var parentDiv = provider.getTestConnectionMessagesDiv();
        if (parentDiv) {
            result = parentDiv.find(elementId);
        } else {
            alert("Method getTestConnectionMessagesDiv() is not defined for specified provider");
        }
    }

    if (!result) {
        result = $(elementId);
    }

    return result;
},

testConnectionUI.ShowLoading = function (visible, provider) {
    var testConnectionButton = null;
    if (provider && provider.getTestConnectionButton) {
        testConnectionButton = provider.getTestConnectionButton();
    }

    if (visible) {
        SW.SRM.TestConnectionUI.Get('#testingConnectionMsg', provider).show();
        if (testConnectionButton) {
            testConnectionButton.attr('disabled', 'disabled');
        }
    } else {
        SW.SRM.TestConnectionUI.Get('#testingConnectionMsg', provider).hide();
        if (testConnectionButton) {
            testConnectionButton.removeAttr('disabled');
        }
    }
};

testConnectionUI.HandleError = function (provider) {
    SW.SRM.TestConnectionUI.Get('#testingConnectionMsg', provider).hide();
    SW.SRM.TestConnectionUI.Get('#runningDiscoveryMsg', provider).hide();

    var testConnectionButton = null;
    if (provider && provider.getTestConnectionButton) {
        testConnectionButton = provider.getTestConnectionButton();
    }

    if (testConnectionButton) {
        testConnectionButton.removeAttr('disabled');
    }
};

testConnectionUI.ShowError = function (title, message, errorMessage, provider) {
    var $failMessageHeaderContent = SW.SRM.TestConnectionUI.Get("#failMessageHeaderContent", provider);
    $failMessageHeaderContent.empty();
    $failMessageHeaderContent.append(title);

    var $failMessageContent = SW.SRM.TestConnectionUI.Get("#failMessageContent", provider);
    $failMessageContent.empty();
    $failMessageContent.append(message);

    var providerStepTestConnectionFailedSeeLogs = SW.SRM.TestConnectionUI.Get("#providerStepTestConnectionFailedSeeLogs", provider);

    if (errorMessage && errorMessage.length < 50) {
        $failMessageContent.append(" ").append(errorMessage);
        providerStepTestConnectionFailedSeeLogs.remove();
    }
    else if (errorMessage) {
        providerStepTestConnectionFailedSeeLogs.before("<span>" + SW.SRM.Formatters.ShortenText(errorMessage, 40) + "... </span><br/>");
        providerStepTestConnectionFailedSeeLogs.click(function () {
            Ext.Msg.show({
                title: title,
                msg: errorMessage,
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK
            });
        });
    }

    SW.SRM.TestConnectionUI.Get("#failMessageHeader", provider).show();
    SW.SRM.TestConnectionUI.Get("#testConnectionFailMsg", provider).show();
}

testConnectionUI.ShowErrorEditProperties = function (title, message, errorMessage, provider) {
    var $failMessageHeaderContent = SW.SRM.TestConnectionUI.Get("#failMessageHeaderContent", provider);
    $failMessageHeaderContent.empty();
    $failMessageHeaderContent.append(title);

    var $failMessageContent = SW.SRM.TestConnectionUI.Get("#failMessageContent", provider);
    $failMessageContent.empty();
    $failMessageContent.append(message);

    SW.SRM.TestConnectionUI.Get("#testConnectionPassMsg", provider).hide();
    SW.SRM.TestConnectionUI.Get("#failMessageHeader", provider).show();
    SW.SRM.TestConnectionUI.Get("#testConnectionFailMsg", provider).show();

    var providerStepTestConnectionFailedSeeLogs = SW.SRM.TestConnectionUI.Get("#providerStepTestConnectionFailedSeeLogs", provider);

    if (errorMessage) {
        if (errorMessage.length < 50) {
            $failMessageContent.append(" ").append(errorMessage);
            providerStepTestConnectionFailedSeeLogs.remove();
        }
        else {
            providerStepTestConnectionFailedSeeLogs.before("<span>" + SW.SRM.Formatters.ShortenText(errorMessage, 40) + "... </span><br/>");
            providerStepTestConnectionFailedSeeLogs.click(function () {
                Ext.Msg.show({
                    title: title,
                    msg: errorMessage,
                    icon: Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK
                });
            });
        }
    }
}