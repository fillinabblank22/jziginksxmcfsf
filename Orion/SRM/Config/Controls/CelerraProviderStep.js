﻿function enableUseSameCredCheckBox(state) {
    if (state) {
        $("#UseSameCredentials").removeAttr('disabled');
        $("#UseSameCredentialsLabel").removeClass('unselectable');
    }
    else {
        $("#UseSameCredentials").removeAttr('checked');
        $("#UseSameCredentials").attr('disabled', 'disabled');
        $("#UseSameCredentialsLabel").addClass('unselectable');
    }
}

function text_OnChanged($self, $related) {
    if ($('#UseSameCredentials').is(':checked')) {
        $related.val($self.val());
    }
}

function enableCredentials(state) {
    if (state) {
        $("#ArrayApiUsernameTextBox").removeAttr('disabled');
        $("#labelApiProviderUserName").removeClass('unselectable');
        $("#ArrayApiPasswordTextBox").removeAttr('disabled');
        $("#labelApiProviderPassword").removeClass('unselectable');

        $("#ArrayApiConfirmPasswordTextBox").removeAttr('disabled');
        $("#labelApiConfirmProviderPassword").removeClass('unselectable');
    }
    else {
        $("#ArrayApiUsernameTextBox").attr('disabled', 'disabled');
        $("#labelApiProviderUserName").addClass('unselectable');

        $("#ArrayApiPasswordTextBox").attr('disabled', 'disabled');
        $("#labelApiProviderPassword").addClass('unselectable');

        $("#ArrayApiConfirmPasswordTextBox").attr('disabled', 'disabled');
        $("#labelApiConfirmProviderPassword").addClass('unselectable');

        text_OnChanged($('#ProviderUsernameTextBox'), $('#ArrayApiUsernameTextBox'));
        text_OnChanged($('#ProviderPasswordTextBox'), $('#ArrayApiPasswordTextBox'));
        text_OnChanged($('#ProviderConfirmPasswordTextBox'), $('#ArrayApiConfirmPasswordTextBox'));
    }
}

function ChooseCredentialSmis_OnChange(ddl) {
    var $containerElement = $("#AdvancedSmisCredFiledsTable");

    if (ddl.value) {
        enableUseSameCredCheckBox(false);
        enableCredentials(true);

        $("#StoredCredFiledsTable").hide('slow');
        $containerElement.hide('slow');

        if ($('#ChooseCredentialVnx').val()) {
            $('#expanderElement').hide('slow');
        }
    } else {
        enableUseSameCredCheckBox(true);

        $("#StoredCredFiledsTable").show('slow');
        $('#expanderElement').show('slow');

        if ($('#expanderElement img').attr('src') === '/Orion/SRM/images/Button.Collapse.gif') {
            $containerElement.show('slow');
        } else {
            $containerElement.hide('slow');
        }
    }
}

function ChooseCredentialVnx_OnChange(ddl) {
    var $containerElement = $('#AdvancedApiCredFiledsTable');

    if (ddl.value) {
        enableUseSameCredCheckBox(false);
        enableCredentials(true);

        $("#StoredCredFiledsTableVnx").hide('slow');
        $containerElement.hide('slow');

        if ($('#ChooseCredentialSmis').val()) {
            $('#expanderElement').hide('slow');
        }
    } else {
        if (!$('#ChooseCredentialSmis').val()) {
            enableUseSameCredCheckBox(true);
        }

        $("#StoredCredFiledsTableVnx").show('slow');
        $('#expanderElement').show('slow');

        if ($('#expanderElement img').attr('src') === '/Orion/SRM/images/Button.Collapse.gif') {
            $containerElement.show('slow');
        } else {
            $containerElement.hide('slow');
        }
    }
}

function expanderChangedHandler() {
    var $apiContainerElement = $('#AdvancedApiCredFiledsTable');
    var $smisContainerElement = $('#AdvancedSmisCredFiledsTable');

    var $expanderImgElement = $('#expanderElement img');
    if ($expanderImgElement.attr('src') === '/Orion/SRM/images/Button.Expand.gif') {
        if (!$('#ChooseCredentialSmis').val()) {
            $smisContainerElement.show('slow');
        }
        if (!$('#ChooseCredentialVnx').val()) {
            $apiContainerElement.show('slow');
        }
        $expanderImgElement.attr('src', '/Orion/SRM/images/Button.Collapse.gif');
        $expanderImgElement.attr('alt', '[-]');
    } else {
        $smisContainerElement.hide('slow');
        $apiContainerElement.hide('slow');
        $expanderImgElement.attr('src', '/Orion/SRM/images/Button.Expand.gif');
        $expanderImgElement.attr('alt', '[+]');
    }
}