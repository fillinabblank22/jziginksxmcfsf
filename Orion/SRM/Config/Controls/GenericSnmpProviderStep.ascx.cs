﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.EnrollmentWizard;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Common.Models;
using System.Globalization;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.Resources;

public partial class Orion_SRM_Config_Controls_GenericSnmpProviderStep : UserControl, IConfigurationControl, ICreateEditControl, IEditControl
{
    private Configuration configuration
    {
        get
        {
            return WizardWorkflowHelper.ConfigurationInfo;
        }
    }

    protected string ArrayID
    {
        get
        {
            return array == null ? string.Empty : array.ID;
        }
    }

    protected string ArraySerialNumber
    {
        get
        {
            return array == null ? string.Empty : array.SerialNumber;
        }
    }

    protected Guid DeviceGroupId
    {
        get
        {
            return configuration.DeviceGroup.GroupId;
        }
    }
    
    private ArrayEntity array;
    private List<ProviderEntity> providers;
    private readonly List<KeyValuePair<string, string>> warnings = new List<KeyValuePair<string, string>>();
    private static readonly Log log = new Log();
    protected const string SnmpV1V2 = "SNMP v1, v2";
    protected const string SnmpV3 = "SNMP v3";
    protected const string DellEqualLogic = "Dell EqualLogic PS Series";
    protected const string DataDomain = "EMC Data Domain";

    private bool VersionOneSelected
    {
        get
        {
            return SelectedCredential.Value == "V1";
        }
    }

    public string SubTitle { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.ArrayTypeLabel.Text = configuration.DeviceGroup.Name;
        deviceInfo.InitWithDevice(configuration.DeviceGroup);
        if (!IsPostBack)
        {
            SnmpVersionDropDown.Items.Add(new ListItem(SrmWebContent.Provider_Step_Snmp_V1_Credentials, SnmpV1V2));
            SnmpVersionDropDown.Items.Add(new ListItem(SrmWebContent.Provider_Step_Snmp_V3_Credentials, SnmpV3));

            EnrollmentWizardProvider providerEntity = null;
            if (configuration.ProviderConfiguration.SelectedProviders.Count > 0)
            {
                providerEntity= configuration.ProviderConfiguration.SelectedProviders[0];
            }

            if (providerEntity != null)
            {
                IpAddressTextBox.Text = providerEntity.IPAddress;
                if(providerEntity.CredentialType == CredentialType.SNMP)
                {
                    SnmpVersionDropDown.SelectedIndex = 0;
                    SnmpVersionOneCredentials.Credential = providerEntity.Credential;
                }
                else if (providerEntity.CredentialType == CredentialType.SnmpV3)
                {
                    SnmpVersionDropDown.SelectedIndex = 1;
                    SnmpVersionThreeCredentials.Credential = providerEntity.Credential;
                }
            }

            switch (this.ArrayTypeLabel.Text)
            {
                case DellEqualLogic:
                    SubTitle = SrmWebContent.AddStorage_ProviderStep_DellEquallogic_SubTitle;
                    break;
                case DataDomain:
                    SubTitle = SrmWebContent.AddStorage_ProviderStep_EMCDataDomain_SubTitle;
                    break;
                default:
                    throw new ArgumentException("SubTitle is unknown - there is no corresponding ArrayTypeLabel");
            }

            snmpVersionRow.Visible = configuration.DeviceGroup.EnableV3;
        }
    }

    private void UpdateProvider(Credential credentials, int? credentialID = null)
    {
        var providerEntity = new EnrollmentWizardProvider
                                {
                                    ProviderType = configuration.DeviceGroup.ProviderTypeOrBlock,
                                    ProviderLocation = ProviderLocation.Onboard,
                                    IPAddress = IpAddressTextBox.Text,
                                    Credential = credentials,
                                    CredentialID = credentialID ?? credentials.ID.Value
                                };
        if (VersionOneSelected)
        {
            providerEntity.CredentialType = CredentialType.SNMP;
        }
        else
        {
            providerEntity.CredentialType = CredentialType.SnmpV3;
        }

        if (array != null && !IsPostBack)
            this.RefillConfiguration();

        configuration.ProviderConfiguration.SelectedProviders.Clear();
        configuration.ProviderConfiguration.SelectedProviders.Add(providerEntity);
    }

    private Credential SaveCredentials()
    {
        if (VersionOneSelected) 
        {
           return SnmpVersionOneCredentials.SaveValue();
        }
        else
        {
            return SnmpVersionThreeCredentials.SaveValue();   
        }
    }

    public void SaveValues()
    {
        var credentials = this.SaveCredentials();
        this.UpdateProvider(credentials);
    }

    public string ValidationError { get; private set; }
    public bool ValidateUserInput()
    {
        if (String.IsNullOrEmpty(IpAddressTextBox.Text))
        {
            this.ValidationError = String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, SrmWebContent.Provider_Step_Array_Ip);
            return false;
        }
        else if (VersionOneSelected)
        {
            if (!SnmpVersionOneCredentials.ValidateUserInput())
            {
                this.ValidationError = SnmpVersionOneCredentials.ValidationError;
                return false;
            }
            else 
            {
                return true;
            }
        }
        else
        {
            if (!SnmpVersionThreeCredentials.ValidateUserInput())
            {
                this.ValidationError = SnmpVersionThreeCredentials.ValidationError;
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    public IEditControl InitFromNetObjects(List<string> listOfNetObjectIDs)
    {
        // Supports just one NetObject ID and just SMSA
        var supported = listOfNetObjectIDs.Count() == 1 && listOfNetObjectIDs[0].Split(':')[0] == SolarWinds.SRM.Web.NetObjects.StorageArray.NetObjectPrefix;
        if (!supported)
        {
            return null;
        }

        var arrayId = int.Parse(listOfNetObjectIDs[0].Split(':')[1]);
        array = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayDetails(arrayId);

        // Supporting just Dell
        if (array.FlowType != FlowType.GenericSnmp)
            return null;

        FillControl();

        return this;
    }

    private void FillControl()
    {
        this.providers = ServiceLocator.GetInstance<IStorageProviderDAL>().GetAllProvidersByArrayId(array.StorageArrayId);

        if (this.providers.Any())
        {
            var provider = this.providers.First();
            IpAddressTextBox.Text = provider.IPAddress;

            SelectedCredential.Value = provider.CredentialType == CredentialType.SNMP ? "V1" : "V3";

            Credential credential;
            if (provider.CredentialType == CredentialType.SNMP)
            {
                SnmpVersionDropDown.SelectedIndex = 0;
                credential = SnmpVersionOneCredentials.SaveValue(provider.CredentialID.ToString(CultureInfo.InvariantCulture));
                SnmpVersionOneCredentials.FillForm();
            }
            else
            {
                SnmpVersionDropDown.SelectedIndex = 1;
                credential = SnmpVersionThreeCredentials.SaveValue(provider.CredentialID);
                SnmpVersionThreeCredentials.FillForm();
            }

            if (credential == null)
            {
                UpdateProvider(credential, provider.CredentialID);

                //Display warning about missing credentials
                this.warnings.Add(new KeyValuePair<string, string>("", String.Format(SrmWebContent.Credentials_Missing_Format, SrmWebContent.ProviderStep_Provider_SNMPCredentials)));

                // Credentials are missing. User should enter new valid credentials
                log.InfoFormat("Credentials with ID {0} are missing. User is being asked to enter new credentials for provider {1}", provider.CredentialID, provider.Name);
            }
            else
            {
                UpdateProvider(credential);    
            }
        }
    }

    private void RefillConfiguration()
    {
        var engine = ServiceLocator.GetInstance<IEngineDAL>().GetEngine(array.EngineID);

        configuration.DeviceGroup = ServiceLocator.GetInstance<IDeviceGroupDAL>().GetDeviceGroupDetailsByArrayId(array.StorageArrayId.Value);
        configuration.PollingConfig.BusinessLayerPort = engine.BusinessLayerPort;
        configuration.PollingConfig.EngineID = engine.EngineID;
        configuration.PollingConfig.ServerName = engine.ServerName;
        configuration.ProviderConfiguration.SelectedProviders.RemoveAll(a => true);
    }

    public string ValidateJSFunction()
    {
        return "SW.SRM.EditProperties.Dell.InitValidation()";
    }

    public bool Validate(bool connectionPropertyChanged)
    {
        return ValidateUserInput();
    }

    public KeyValuePair<string, string>[] GetWarnings()
    {
        return this.warnings.ToArray();
    }

    public void Update(bool connectionPropertyChanged)
    {
        SaveValues();
        var toDiscovery = new List<SelectedArrayEntity>
                              {
                                  new SelectedArrayEntity(array)
                                    {
                                        IsSelected = true,
                                        StorageArrayId = this.array.StorageArrayId
                                    }
                              };


        using (var proxy = BusinessLayerFactory.Create())
        {
            proxy.DiscoveryImport(array.EngineID, ProviderHelper.GetProviderInfoOfWizardProviders(this.providers, configuration.ProviderConfiguration.SelectedProviders), toDiscovery);
        }
    }
}
