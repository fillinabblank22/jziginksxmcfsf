﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DeviceType.ascx.cs" Inherits="Orion.SRM.Config.Controls.Orion_SRM_Config_DeviceType"%>
<div class="srm-enrollment-wizard-content EnrollmentWizardPage_master_GroupBox" >
                    <table id="srm-devicetype-info" cellspacing="0" >
                        <tbody>
                            <tr>
                                <td>
                                    <img src="/Orion/SRM/images/hint_icon_u43.png" />
                                </td>
                                <td>
                                    <div>
                                        <asp:Label id="FamilyTypeName" class="bigfont uppercase" runat="server"></asp:Label>
                                        <asp:Label id="FamilyTypeDescription" runat="server"></asp:Label>
                                        <span><a target="_blank" rel="noopener noreferrer" id="FamilyTypeLink" runat="server"></a></span>
                                    </div>
                                    <div class="familyimage">
                                        <asp:Image id="image" runat="server"/>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        </table>
                        </div>



