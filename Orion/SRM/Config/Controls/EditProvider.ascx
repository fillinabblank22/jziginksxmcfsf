﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditProvider.ascx.cs" Inherits="Orion_SRM_Config_Controls_EditProvider" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>

<%@ Register Src="~/Orion/SRM/Config/Controls/ProviderForm.ascx" TagPrefix="UIInfo" TagName="AddDFMProvider" %>
<orion:Include ID="Include" runat="server" File="SRM/Config/Controls/DiscoveryValidation.js" />
<orion:Include ID="DfmProviderJs" runat="server" File="SRM/Config/Controls/AddDfmProvider.js" />
<orion:Include ID="SmisProviderJs" runat="server" File="SRM/Config/Controls/AddSmisProvider.js" />
<orion:Include ID="XtremIOProviderJs" runat="server" File="SRM/Config/Controls/AddXtremIoProvider.js" />

<orion:Include runat="server" File="Discovery.css" Module="SRM" />
<orion:Include runat="server" File="SRM.EditProperties.SmisProvider.css" Module="SRM" />

<script type="text/javascript">
    $(document).ready(function () {
        <% if (FlowType == FlowType.NetAppDfm) { %>
            SW.SRM.AddDfmProvider.init('<%= (int)CredentialType.NetAppDfm %>',
                                       '<%= SrmWebContent.Provider_Step_Provider_Connecting_Error %>',
                                       '<%  = SrmWebContent.Provider_Step_Unexpected_Error %>',
                                       '<%= SrmWebContent.Provider_Step_New_Credential %>');
        <% } else if (FlowType == FlowType.XtremIO) { %>
            SW.SRM.AddXtremIoProvider.init('<%= (int)CredentialType.XtremIoHttp %>',
                                           '<%= SrmWebContent.Provider_Step_Provider_Connecting_Error %>:',
                                           '<%= SrmWebContent.Provider_Step_Unexpected_Error %>',
                                           '<%= SrmWebContent.Provider_Step_New_Credential %>');
        <% } else { %>
            SW.SRM.AddSmisProvider.init('<%= (int)CredentialType.SMIS %>',
                                        '<%= SrmWebContent.Provider_Step_Provider_Connecting_Error %>:',
                                        '<%= SrmWebContent.Provider_Step_Unexpected_Error %>',
                                        '<%= SrmWebContent.Provider_Step_New_Credential %>',
                                        '<%= this.EngineId %>');
        <% } %>

        $('#ChooseCredentialDdl').trigger('change');
    });

    TestProviderCredential = function () {
        <% if (FlowType == FlowType.NetAppDfm) { %>
            SW.SRM.AddDfmProvider.testCredential();
        <% } else if (FlowType == FlowType.XtremIO) { %>
            SW.SRM.AddXtremIoProvider.testCredential();
        <% } else  { %>
            SW.SRM.AddSmisProvider.testCredential();
        <% } %>
    }
</script>

<uiinfo:adddfmprovider runat="server" id="ProviderControl" />