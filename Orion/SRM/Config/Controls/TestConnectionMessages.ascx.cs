﻿public partial class Orion_SRM_Config_Controls_TestConnectionMessages : System.Web.UI.UserControl
{
    public Orion_SRM_Config_Controls_TestConnectionMessages()
    {
        ContainerId = "notSpecifiedIdForTestConnectionMessages";
    }
    public string ContainerId { get; set; }
    public string JsCancellationFunction { get; set; }
}
