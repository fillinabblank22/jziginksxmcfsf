﻿SW = SW || {};
SW.SRM = SW.SRM || {};

SW.SRM.AddDfmProvider = function () {
    var dialog;
        
    var submitValidation = function ($selectCredential, selectedCredentialOptionsArr, displayNameVar, usernameVar, passwordVar, confirmPasswordVar, ipAddressOrHostnameTextBoxVar, selectedCredentialText) {
        var message = credentialsValidation($selectCredential, displayNameVar, usernameVar, passwordVar, confirmPasswordVar, ipAddressOrHostnameTextBoxVar);
        if (message != "") {
            return { 
                    valid: false,
                    message: message
                };
        }
        var credName = ($selectCredential.val() == null || $selectCredential.val().length == 0) ? displayNameVar : selectedCredentialText;
        var extGridId = $('#Grid >:first-child')[0].id;
        var grid = Ext.getCmp(extGridId);
        var gridStore = grid.store;
        var existsInStore = false;
        var existingProvider = false;
        gridStore.each(function (rec) {
            //friendlyName contains a status prefix, we just need the IP/hostname
            var friendlyName = rec.get("FriendlyName").split('_')[1];
            if (rec.get("CredentialName") == credName && friendlyName == ipAddressOrHostnameTextBoxVar) {
                existsInStore = true;
                existingProvider = friendlyName;
                return false;
            }
        });
        if (existsInStore) {
            return {
                valid: false,
                title: "@{R=SRM.Strings;K=Add_Provider_CredentialNameAlreadyExistHeader; E=js}",
                message: SW.Core.String.Format("@{R=SRM.Strings;K=Add_Provider_CredentialNameAlreadyUsedByProvider; E=js}", existingProvider, credName)
            };
        }

        if ($selectCredential.val() == null || $selectCredential.val().length == 0) {
            if ($.trim(String(displayNameVar)) == '') {
                return {
                        valid: false,
                        message: "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelDisplayName').text())
                    };
            }  
        }
        
        return {
            valid: true
        };
    };
    
    var credentialsValidation = function ($selectSmisCredential, displayNameVar, usernameVar, passwordVar, confirmPasswordVar, ipAddressOrHostnameTextBoxVar) {
        if ($.trim(String(ipAddressOrHostnameTextBoxVar)) == '') {
            return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelIpAddress').text());
        }
        if ($selectSmisCredential.val() == null
         || $selectSmisCredential.val().length == 0
         || $selectSmisCredential.val() == newCredentialsConstant) {
            if ($.trim(String(usernameVar)) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelProviderUserName').text());
            }

            if ($.trim(String(displayNameVar)) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelDisplayName').text());
            }

            var selectedCredentialOptionsArr = $selectSmisCredential.children().map(function () { return this.text.toLowerCase(); }).get();
            var credentialNameExists = $.inArray(displayNameVar.toLowerCase(), selectedCredentialOptionsArr) >= 0;
            if (credentialNameExists) {
                return "<a href='javascript:SW.SRM.AddDfmProvider.loadCredentials(&quot;" + displayNameVar + "&quot;)'>@{R=SRM.Strings;K=Add_Provider_CredentialNameAlreadyExist_LoadSaved; E=js}</a> or <a href='javascript:SW.SRM.AddDfmProvider.focusCredentialsDisplayName()'>@{R=SRM.Strings;K=Add_Provider_CredentialNameAlreadyExist_RenameDuplicate; E=js}</a>";
            }

            if ($.trim(String(passwordVar)) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelProviderPassword').text());
            }

            if (!(passwordVar === confirmPasswordVar)) {
                return "@{R=SRM.Strings;K=Add_Provider_ConfirmPassword_CompareField_Validator; E=js}";
            }

            var httpsPort = $("#HttpsPortTextBox").val();
            if ($.trim(String(httpsPort)) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelHttpsPort').text());
            }

            var httpsPortInt = parseInt(httpsPort);
            if (!$.isNumeric(httpsPortInt) || Math.floor(httpsPortInt) !== httpsPortInt || httpsPortInt < 1 || httpsPortInt > 65535) {
                return "@{R=SRM.Strings;K=Add_Provider_NotValidPort_Validator; E=js}".replace('{0}', $('#labelHttpsPort').text());
            }

            var httpPort = $("#HttpPortTextBox").val();
            if ($.trim(String(httpPort)) == '') {
                return "@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}".replace('{0}', $('#labelHttpPort').text());
            }

            var httpPortInt = parseInt(httpPort);
            if (!$.isNumeric(httpPortInt) || Math.floor(httpPortInt) !== httpPortInt || httpPortInt < 1 || httpPortInt > 65535) {
                return "@{R=SRM.Strings;K=Add_Provider_NotValidPort_Validator; E=js}".replace('{0}', $('#labelHttpPort').text());
            }
        }

        return "";
    };

    var request,
        connectionTestFailedTitle,
        connectionTestUnexpectedFailedMessage,
        newCredentialsConstant;

    var clearForm = function () {
        $("#testingConnectionMsg").hide();
        $("#testConnectionFailMsg").hide();
        $("#testConnectionPassMsg").hide();

        $("#StoredCredFiledsTable").show();
        $("#DisplayNameTextBox").val('');
        $("#ProviderUsernameTextBox").val('');
        $("#ProviderPasswordTextBox").val('');
        $("#ProviderConfirmPasswordTextBox").val('');
        $("#IpAddressOrHostnameTextBox").val('');
        $("#ProtocolCheckBox").removeAttr('checked');

        var $expanderImgElement = $('#expanderElement img');
        $expanderImgElement.attr('src', '/Orion/SRM/images/Button.Expand.gif');
        $expanderImgElement.attr('alt', '[+]');
    }; 

    var hideError = function () {
        $("#failMessageHeader").hide();
        $("#testConnectionFailMsg").hide();
    };

    var showPassMessageBox = function () {
        $("#testConnectionPassMsg").show();
    };

    var showFailMessageBox = function (message, errorMessage) {
        var title = connectionTestFailedTitle.concat(" ").concat($("#IpAddressOrHostnameTextBox").val());
        SW.SRM.Common.ShowError(title, message, errorMessage);
    };
    
    var saveButtonExists = function () {
        return $('#save').length > 0;
    };
    var disableSaveButton = function() {
        if (saveButtonExists()) {
            Ext.getCmp('save').disable();
        }
    };
    var enableSaveButton = function() {
        if (saveButtonExists()) {
            Ext.getCmp('save').enable();
        }
    };

    return {
        focusCredentialsDisplayName : function() {
            $("#DisplayNameTextBox").focus();
        },
        loadCredentials : function(credentialName) {
            $('#ChooseCredentialDdl').find('option:contains("'+credentialName+'")').attr("selected",true);
            $('#ChooseCredentialDdl').trigger('change');
        },
        cancelTestCredential: function () {
            $("#testingConnectionMsg").hide();
            $("#testConnectionButton").removeAttr('disabled');
            enableSaveButton();

            if (request) {
                request.abort();
            }
        },
        testCredential: function (onSuccess) {
            var resultingDeferred = $.Deferred();
            $("#testConnectionFailMsg").hide();
            $("#testConnectionPassMsg").hide();

            var usernameVar = $("#ProviderUsernameTextBox").val();
            var passwordVar = $("#ProviderPasswordTextBox").val();
            var confirmPasswordVar = $("#ProviderConfirmPasswordTextBox").val();
            var ipAddressOrHostnameTextBoxVar = $.trim(String($("#IpAddressOrHostnameTextBox").val()));
            var protocolVar = $("#ProtocolCheckBox").is(':checked');
            var $selectCredential = $("#ChooseCredentialDdl");
            var displayName = $("#DisplayNameTextBox").val();

            var error = credentialsValidation($selectCredential, displayName, usernameVar, passwordVar, confirmPasswordVar, ipAddressOrHostnameTextBoxVar);
            if (error == null || error.length == 0) {
                hideError();
            } else {
                SW.SRM.Common.ShowError('@{R=SRM.Strings;K=Add_Provider_ValidationError_Title; E=js}', error);
                return resultingDeferred.reject();
            }

            $("#testingConnectionMsg").show();
            $("#testConnectionButton").attr('disabled', 'disabled');

            disableSaveButton();

            var credentialId = $selectCredential.val() == null || $selectCredential.val().length == 0 ? -1 : parseInt($selectCredential.val());
            request = SW.SRM.Common.CallSrmWebService("/Orion/SRM/Services/EnrollmentWizardService.asmx",
                                        "TestNetAppDfmCredentials",
                                        {
                                            'credentialID': credentialId,
                                            'credentialName': displayName,
                                            'userName': usernameVar,
                                            'password': passwordVar,
                                            'useHttp': protocolVar,
                                            'httpPort': $("#HttpPortTextBox").val(),
                                            'httpsPort': $("#HttpsPortTextBox").val(),
                                            'ip': ipAddressOrHostnameTextBoxVar
                                        },
                                        function (result) {
                                            $("#testingConnectionMsg").hide();
                                            $("#testConnectionButton").removeAttr('disabled');
                                            enableSaveButton();

                                            if (result.IsSuccess) {
                                                showPassMessageBox();

                                                if (onSuccess) {
                                                    onSuccess();
                                                }

                                                resultingDeferred.resolve();
                                            } else {
                                                showFailMessageBox(result.Message, result.ErrorMessage);
                                                resultingDeferred.reject();
                                            }
                                        },
                                        function () {
                                            $("#testingConnectionMsg").hide();
                                            $("#testConnectionButton").removeAttr('disabled');
                                            enableSaveButton();

                                            if (request.status !== 0) {
                                                showFailMessageBox(connectionTestUnexpectedFailedMessage);
                                            }

                                            resultingDeferred.reject();
                                        });
            return resultingDeferred.promise();
        },
        init: function (type, _connectionTestFailedTitle, _connectionTestUnexpectedFailedMessage, _newCredentialsConstant) {
            connectionTestFailedTitle = _connectionTestFailedTitle;
            connectionTestUnexpectedFailedMessage = _connectionTestUnexpectedFailedMessage;
            newCredentialsConstant = _newCredentialsConstant;

            this.MonitoredControlIds = ['pollEngineDescr', 'IpAddressOrHostnameTextBox', 'ChooseCredentialDdl'];
            SW.SRM.EditProperties.ProviderChangeManager.InitProvider(this, this.testCredential);

            function ShowDfmProviderDialog() {
                $('#ChooseCredentialDdl option').remove();
                ORION.callWebService("/Orion/SRM/Services/EnrollmentWizardService.asmx",
                                     "GetStoredCredentials",
                                     {
                                         'type': type
                                     },
                                     function (result) {
                                         $('<option value="">' + result.Default + '</option>').appendTo($('#ChooseCredentialDdl'));
                                         $.each(result.Credentials, function (index, item) {
                                             $('<option value="' + item.Key + '">' + item.Value + '</option>').appendTo($('#ChooseCredentialDdl'));
                                         });
                                     });

                if (!dialog) {
                    dialog = new Ext.Window({
                        applyTo: 'AddProviderFrame',
                        id: 'dialog',
                        layout: 'fit',
                        width: 1000,
                        height: 620,
                        modal: true,
                        closeAction: 'hide',
                        plain: true,
                        resizable: false,
                        items: new Ext.BoxComponent({
                            applyTo: 'AddProviderBody'
                        }),
                        buttons: [
                            {
                                id: 'save',
                                text: '@{R=SRM.Strings;K=Add_Provider_Save_ButtonText; E=js}',
                                handler: function () {
                                    var displayNameVar = $("#DisplayNameTextBox").val();
                                    var usernameVar = $("#ProviderUsernameTextBox").val();
                                    var passwordVar = $("#ProviderPasswordTextBox").val();
                                    var confirmPasswordVar = $("#ProviderConfirmPasswordTextBox").val();
                                    var ipAddressOrHostnameTextBoxVar = $.trim(String($("#IpAddressOrHostnameTextBox").val()));
                                    var protocolVar = $("#ProtocolCheckBox").is(':checked');
                                    var $selectCredential = $("#ChooseCredentialDdl");
                                    var selectedCredentialText = $("#ChooseCredentialDdl option:selected").text();
                                    // Get options text values as array
									var selectedCredentialOptionsArr = $("#ChooseCredentialDdl option[value!='']").map(function() { return this.text.toLowerCase(); }).get();

									var valResult = submitValidation($selectCredential, selectedCredentialOptionsArr, displayNameVar, usernameVar, passwordVar, confirmPasswordVar, ipAddressOrHostnameTextBoxVar, selectedCredentialText);
                                    if (valResult.valid == true) {
                                        hideError();
                                    } else {
                                        var errorTitle = valResult.title;
                                        if (errorTitle == null)
                                            errorTitle = '@{R=SRM.Strings;K=Add_Provider_ValidationError_Title; E=js}';
                                        
                                        SW.SRM.Common.ShowError(errorTitle, valResult.message);
                                        return;
                                    }

                                    SW.SRM.AddDfmProvider.testCredential(function () {
                                        var credentialId = $selectCredential.val() == null || $selectCredential.val().length == 0 ? -1 : parseInt($selectCredential.val());
                                        ORION.callWebService("/Orion/SRM/Services/EnrollmentWizardService.asmx",
                                                             "SaveDfmProvider",
                                                             {
                                                                 'ipAddress': ipAddressOrHostnameTextBoxVar,
                                                                 'displayName': displayNameVar,
                                                                 'userName': usernameVar,
                                                                 'password': passwordVar,
                                                                 'useHttp': protocolVar,
                                                                 'httpPort': $("#HttpPortTextBox").val(),
                                                                 'httpsPort': $("#HttpsPortTextBox").val(),
                                                                 'credentialID': credentialId
                                                             },
                                                             function (result) {
                                                                 SW.SRM.ProviderSelector.setDefaultProvider(result);
                                                                 SW.SRM.ProviderSelector.reload();
                                                                 dialog.hide();
                                                             });
                                    });
                                }
                            },
                            {
                                text: '@{R=SRM.Strings;K=Add_Provider_Close_ButtonText; E=js}',
                                style: 'margin-left: 5px;',
                                handler: function () {
                                    SW.SRM.AddDfmProvider.cancelTestCredential();
                                    dialog.hide();
                                }
                            }],
                        center: function () {
                            var pos = this.el.getCenterXY(true);
                            this.setPosition(pos[0], pos[1]);
                        }
                    });
                }

                dialog.center();
                dialog.show();

                clearForm();
            };
            
            $("#addNewProviderBtn").click(function () {
                ShowDfmProviderDialog();
                return false;
            });
            $("#addNewProviderLabel").click(function () {
                ShowDfmProviderDialog();
                return false;
            });
        }
    };
}();


