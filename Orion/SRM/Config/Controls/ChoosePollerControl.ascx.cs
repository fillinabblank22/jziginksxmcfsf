﻿using System.Globalization;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.EnrollmentWizard;
using SolarWinds.SRM.Web.Interfaces;
using System;
using SolarWinds.SRM.Web.Models;

public partial class Orion_SRM_Config_Controls_ChoosePollerControl : System.Web.UI.UserControl, IConfigurationControl
{
    private Configuration configuration = null;


    protected void Page_Load(object sender, EventArgs e)
    {
        configuration = WizardWorkflowHelper.ConfigurationInfo;

        if (!IsPostBack)
        {
            EngineEntity engine = configuration.PollingConfig.EngineID == 0
                ? ServiceLocator.GetInstance<IEngineDAL>().GetPrimaryEngine()
                : ServiceLocator.GetInstance<IEngineDAL>().GetEngine(configuration.PollingConfig.EngineID);

            string textEngineStatusImg = null;
            string textEngineName = null;
            string textEngineType = null;

            textEngineStatusImg = InvariantString.Format("/NetPerfMon/images/{0}", CommonWebHelper.GetEngineStatusImage(engine.KeepAlive));
            textEngineName = engine.ServerName;
            textEngineType = string.Format(CultureInfo.CurrentCulture, "({0})", ResourceLookup("ServerType", engine.ServerType));

            pollEngineDescr.Text = InvariantString.Format("<a href=\"/Orion/Admin/Details/Engines.aspx\"><img src='{0}' />&nbsp;<span style=\"vertical-align: top;\">{1} {2}</span></a>", textEngineStatusImg, textEngineName, textEngineType);
            hfPollingEngineId.Value = engine.EngineID.ToString(CultureInfo.InvariantCulture);
        }
    }

    private string ResourceLookup(string prefix, string value)
    {
        const string managerid = "Core.Strings";
        var manager = ResourceManagerRegistrar.Instance;
        var key = manager.CleanResxKey(prefix, value.Trim());
        var result = manager.SearchAll(key, new[] { managerid });
        return String.IsNullOrEmpty(result) ? value : result;
    }

    public void SaveValues()
    {
        if (!this.Visible)
        {
            return;
        }

        int engineId;
        int.TryParse(hfPollingEngineId.Value, out engineId);

        EngineEntity engine = ServiceLocator.GetInstance<IEngineDAL>().GetEngine(engineId);
        configuration.PollingConfig.EngineID = engine.EngineID;
        configuration.PollingConfig.ServerName = engine.ServerName;
        configuration.PollingConfig.BusinessLayerPort = engine.BusinessLayerPort;
    }


    public string ValidationError { get; private set; }
    public bool ValidateUserInput()
    {
        int engineId;
        return int.TryParse(hfPollingEngineId.Value, out engineId);
    }
}