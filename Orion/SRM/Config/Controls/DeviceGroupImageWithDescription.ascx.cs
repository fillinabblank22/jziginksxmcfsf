﻿using System;
using System.Web.UI;

using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Models;

public partial class DeviceGroupImageWithDescription : UserControl
{
    public string ImageUrl { get; set; }

    public void Initialize(DeviceGroup deviceGroup)
    {
        this.image.ImageUrl = ImageUrl;
        FamilyTypeName.InnerText = deviceGroup.Name;
        FamilyTypeDescription.InnerText = deviceGroup.Description;
        FamilyTypeLink.HRef = InvariantString.Format("http://www.solarwinds.com/documentation/helpLoader.aspx?lang={0}&topic={1}.htm", Resources.CoreWebContent.CurrentHelpLanguage, deviceGroup.LinkURL);
        FamilyTypeLink.InnerText = deviceGroup.LinkName;
    }
}
