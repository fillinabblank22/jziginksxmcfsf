﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.Credentials;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.EnrollmentWizard;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.Resources;

public partial class Orion_SRM_Config_Controls_IsilonProviderStep : UserControl, IConfigurationControl, ICreateEditControl, IEditControl
{
    private static readonly IDeviceGroupDAL DeviceGroupDal = ServiceLocator.GetInstance<IDeviceGroupDAL>();

    private Configuration configuration
    {
        get
        {
            return WizardWorkflowHelper.ConfigurationInfo;
        }
    }

    protected string ArrayID
    {
        get
        {
            return array == null ? string.Empty : array.ID;
        }
    }

    protected string ArraySerialNumber
    {
        get
        {
            return array == null ? string.Empty : array.SerialNumber;
        }
    }

    protected Guid DeviceGroupId
    {
        get
        {
            return configuration.DeviceGroup.GroupId;
        }
    }

    // keys to group's properties
    private const string SnmpCredentialsTooltipKey = "SnmpCredentialsTooltip";
    private const string ApiCredentialsTooltipKey = "ApiCredentialsTooltip";
    private const string ChooseDataSourceSubtitleKey = "ChooseDataSourceSubtitle";

    private ArrayEntity array;

    private List<ProviderEntity> providers;
    private readonly List<KeyValuePair<string, string>> warnings = new List<KeyValuePair<string, string>>();
    private static readonly Log log = new Log();

    private string SelectedEmcHttpCredentials { get; set; }

    protected const string SnmpV1V2 = "SNMP v1, v2";
    protected const string SnmpV3 = "SNMP v3";

    public string SubTitle { get; set; }

    public string ValidationError { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.ArrayTypeName.Text = configuration.DeviceGroup.Name;
            deviceInfo.InitWithDevice(configuration.DeviceGroup);

            //SNMP
            PopulateSnmpVersionDropdown();

            //EMC Http
            labelEmcHttpDisplayNameTextBox.Text = SrmWebContent.Add_Provider_DisplayName;
            labelEmcHttpProviderUserName.Text = SrmWebContent.Add_Provider_ArrayAPIUsername;
            labelEmcHttpProviderPassword.Text = SrmWebContent.Add_Provider_ArrayAPIPassword;

            HttpPortTextBox.Text = SRMConstants.DefaultIsilonPort.ToString();

            if (configuration.DeviceGroup.Properties.ContainsKey(SnmpCredentialsTooltipKey))
            {
                SNMPCredentialsTooltip.Visible = true;
                SNMPCredentialsTooltip.ToolTip = configuration.DeviceGroup.Properties[SnmpCredentialsTooltipKey];
            }

            if (configuration.DeviceGroup.Properties.ContainsKey(ApiCredentialsTooltipKey))
            {
                ApiCredentialsTooltip.Visible = true;
                ApiCredentialsTooltip.ToolTip = configuration.DeviceGroup.Properties[ApiCredentialsTooltipKey];
            }

            if (configuration.DeviceGroup.Properties.ContainsKey(ChooseDataSourceSubtitleKey))
            {
                SubTitle = configuration.DeviceGroup.Properties[ChooseDataSourceSubtitleKey];
            }

            EnrollmentWizardProvider providerEntity = null;
            if (configuration.ProviderConfiguration.SelectedProviders.Count > 0)
            {
                providerEntity = configuration.ProviderConfiguration.SelectedProviders[0];
            }

            if (providerEntity != null)
            {
                IpAddressTextBox.Text = providerEntity.IPAddress;

                //SNMP
                if (providerEntity.CredentialType == CredentialType.SNMP)
                {
                    SnmpVersionDropDown.SelectedIndex = 0;
                    SnmpVersionOneCredentials.Credential = providerEntity.Credential;
                }
                else if (providerEntity.CredentialType == CredentialType.SnmpV3)
                {
                    SnmpVersionDropDown.SelectedIndex = 1;
                    SnmpVersionThreeCredentials.Credential = providerEntity.Credential;
                }
            }

            //EMC Http
            FillCredentials(this.ChooseCredentialEmcHttp, CredentialType.IsilonHttp, SelectedEmcHttpCredentials);
        }
    }

    private void PopulateSnmpVersionDropdown()
    {
        if (SnmpVersionDropDown.Items.Count == 0)
        {
            SnmpVersionDropDown.Items.Add(new ListItem(SrmWebContent.Provider_Step_Snmp_V1_Credentials, SnmpV1V2));
            SnmpVersionDropDown.Items.Add(new ListItem(SrmWebContent.Provider_Step_Snmp_V3_Credentials, SnmpV3));
        }
    }

    public KeyValuePair<string, string>[] GetWarnings()
    {
        return this.warnings.ToArray();
    }

    public void Update(bool connectionPropertyChanged)
    {
        SaveValues();

        var toDiscovery = new List<SelectedArrayEntity>
                              {
                                  new SelectedArrayEntity(array)
                                    {
                                        IsSelected = true,
                                        StorageArrayId = this.array.StorageArrayId
                                    }
                              };

        using (var proxy = BusinessLayerFactory.Create())
        {
            proxy.DiscoveryImport(array.EngineID, ProviderHelper.GetProviderInfoOfWizardProviders(this.providers, configuration.ProviderConfiguration.SelectedProviders), toDiscovery);
        }
    }

    public void SaveValues()
    {
        SaveSNMPProvider();

        SaveEmcHttpProvider(this.IpAddressTextBox.Text.Trim(),
                                this.DisplayNameVnxTextBox.Text,
                                this.ArrayApiUsernameTextBox.Text,
                                this.ArrayApiPasswordTextBox.Text,
                                Int32.Parse(this.HttpPortTextBox.Text),
                                ProtocolCheckBox.Checked,
                                this.ChooseCredentialEmcHttp.SelectedValue);
    }

    private bool HandleValidationError(string message)
    {
        this.ValidationError = message;
        this.ClearSelectedProviders();
        return false;
    }

    public bool ValidateUserInput()
    {
        if (String.IsNullOrEmpty(IpAddressTextBox.Text))
        {
            return HandleValidationError(String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, SrmWebContent.Provider_Step_Array_Ip));
        }

        if (SNMPVersionOneSelected)
        {
            if (!SnmpVersionOneCredentials.ValidateUserInput())
            {
                return HandleValidationError(SnmpVersionOneCredentials.ValidationError);
            }
        }
        else
        {
            if (!SnmpVersionThreeCredentials.ValidateUserInput())
            {
                return HandleValidationError(SnmpVersionThreeCredentials.ValidationError);
            }
        }


        if (ChooseCredentialEmcHttp.SelectedIndex == 0 && String.IsNullOrEmpty(DisplayNameVnxTextBox.Text))
        {
            return HandleValidationError(String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, labelEmcHttpDisplayNameTextBox.Text));
        }
        else if (ChooseCredentialEmcHttp.SelectedIndex == 0 && String.IsNullOrEmpty(ArrayApiUsernameTextBox.Text))
        {
            return HandleValidationError(String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, labelEmcHttpProviderUserName.Text));
        }
        else if (ChooseCredentialEmcHttp.SelectedIndex == 0 && String.IsNullOrEmpty(ArrayApiPasswordTextBox.Text))
        {
            return HandleValidationError(String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, labelEmcHttpProviderPassword.Text));
        }
        else if (ChooseCredentialEmcHttp.SelectedIndex == 0 && ArrayApiPasswordTextBox.Text != ArrayApiConfirmPasswordTextBox.Text)
        {
            return HandleValidationError(SrmWebContent.Add_Provider_ConfirmApiPassword_CompareField_Validator);
        }

        if (ChooseCredentialEmcHttp.SelectedIndex == 0 && this.ChooseCredentialEmcHttp.Items.Cast<ListItem>().Any(item => this.DisplayNameVnxTextBox.Text.Equals(item.Text, StringComparison.InvariantCultureIgnoreCase)))
        {
            return HandleValidationError(SrmWebContent.Provider_Step_Celerra_Validation_DuplicateAPICredentials);
        }

        return true;
    }

    private void FillCredentials(DropDownList dropDownList, CredentialType type, string selectedCredentials)
    {
        var items = new List<KeyValuePair<String, String>>() { new KeyValuePair<String, String>(SRMConstants.NewCredential, SrmWebContent.Provider_Step_New_Credential) };
        using (var proxy = BusinessLayerFactory.Create())
        {
            IDictionary<int, string> credentialsList = proxy.GetCredentialNames(type);
            foreach (KeyValuePair<int, string> cred in credentialsList)
            {
                items.Add(new KeyValuePair<String, String>(cred.Key.ToString(CultureInfo.InvariantCulture), cred.Value));
            }
        }

        dropDownList.DataTextField = "Value";
        dropDownList.DataValueField = "Key";
        dropDownList.DataSource = items;

        if (items.Any(pair => pair.Key == selectedCredentials))
        {
            dropDownList.SelectedValue = selectedCredentials;
        }

        dropDownList.DataBind();
    }


    public void SaveEmcHttpProvider(string ipAddress,
                                string displayName,
                                string userName,
                                string password,
                                int port,
                                bool useHttp,
                                string credentialIDstring)
    {
        IsilonHttpCredential apiCredential;

        if (credentialIDstring != SRMConstants.NewCredential)
        {
            int credentialID = -1;
            Int32.TryParse(credentialIDstring, out credentialID);
            using (var proxy = BusinessLayerFactory.Create())
            {
                apiCredential = (IsilonHttpCredential)proxy.GetCredential(credentialID, CredentialType.IsilonHttp);
            }
        }
        else
        {
            apiCredential = new IsilonHttpCredential()
            {
                Name = displayName,
                Username = userName,
                Password = password,
                Port = port,
                UseSsl = !useHttp
            };

            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(apiCredential, CredentialType.IsilonHttp);
                apiCredential.ID = id;
            }
        }

        var providerEntity = new EnrollmentWizardProvider
        {
            ProviderType = ProviderTypeEnum.File,
            ProviderLocation = ProviderLocation.Onboard,
            IPAddress = IpAddressTextBox.Text.Trim()
        };

        providerEntity.CredentialID = apiCredential.ID.Value;
        providerEntity.CredentialType = CredentialType.IsilonHttp;
        providerEntity.Credential = apiCredential;

        UpdateProviders(providerEntity);
    }

    private void UpdateProviders(EnrollmentWizardProvider newProvider, IList<CredentialType> credentialTypes = null)
    {
        configuration.ProviderConfiguration.SelectedProviders.RemoveAll(p => p.ProviderType == newProvider.ProviderType &&
            (credentialTypes != null ? credentialTypes.Contains(p.CredentialType) : p.CredentialType == newProvider.CredentialType));
        configuration.ProviderConfiguration.SelectedProviders.Add(newProvider);
    }

    public IEditControl InitFromNetObjects(List<string> listOfNetObjectIDs)
    {
        // Supports just one NetObject ID and just SMSA
        var supported = listOfNetObjectIDs.Count() == 1 && listOfNetObjectIDs[0].Split(':')[0] == StorageArray.NetObjectPrefix;
        if (!supported)
        {
            return null;
        }

        var arrayId = int.Parse(listOfNetObjectIDs[0].Split(':')[1]);
        array = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayDetails(arrayId);

        // Supporting just Emc Isilon
        if (array.FlowType != FlowType.SnmpRestIsilon)
            return null;

        FillControl();

        return this;
    }

    private void FillControl()
    {
        this.providers = ServiceLocator.GetInstance<IStorageProviderDAL>().GetAllProvidersByArrayId(array.StorageArrayId);

        var snmpProvider = this.providers.FirstOrDefault(p => p.CredentialType == CredentialType.SNMP || p.CredentialType == CredentialType.SnmpV3);
        var emcHttpProvider = this.providers.FirstOrDefault(p => p.CredentialType == CredentialType.IsilonHttp);

        if (snmpProvider != null)
        {
            IpAddressTextBox.Text = snmpProvider.IPAddress;

            SelectedSnmpCredential.Value = snmpProvider.CredentialType == CredentialType.SNMP ? "V1" : "V3";

            PopulateSnmpVersionDropdown();

            Credential snmpCredential;
            if (snmpProvider.CredentialType == CredentialType.SNMP)
            {
                SnmpVersionDropDown.SelectedIndex = 0;
                snmpCredential = SnmpVersionOneCredentials.SaveValue(snmpProvider.CredentialID.ToString(CultureInfo.InvariantCulture));
                SnmpVersionOneCredentials.FillForm();
            }
            else
            {
                SnmpVersionDropDown.SelectedIndex = 1;
                snmpCredential = SnmpVersionThreeCredentials.SaveValue(snmpProvider.CredentialID);
                SnmpVersionThreeCredentials.FillForm();
            }

            if (snmpCredential == null)
            {
                UpdateSNMPProvider(snmpCredential, snmpProvider.CredentialID);

                //Display warning about missing credentials
                this.warnings.Add(new KeyValuePair<string, string>("", String.Format(SrmWebContent.Credentials_Missing_Format, SrmWebContent.ProviderStep_Provider_SNMPCredentials)));

                // Credentials are missing. User should enter new valid credentials
                log.InfoFormat("Credentials with ID {0} are missing. User is being asked to enter new credentials for provider {1}", snmpProvider.CredentialID, snmpProvider.Name);
            }
            else
            {
                UpdateSNMPProvider(snmpCredential);
            }
        }

        if (emcHttpProvider != null)
        {
            IpAddressTextBox.Text = IpAddressTextBox.Text ?? emcHttpProvider.IPAddress;
            SelectedEmcHttpCredentials = emcHttpProvider.CredentialID.ToString();

            IsilonHttpCredential credentials;
            using (var proxy = BusinessLayerFactory.Create())
            {
                credentials = (IsilonHttpCredential)proxy.GetCredential(emcHttpProvider.CredentialID, CredentialType.IsilonHttp);
            }

            if (credentials != null)
            {
                emcHttpProvider.Credential = credentials;
                HttpPortTextBox.Text = credentials.Port.ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                //Display warning about missing credentials
                this.warnings.Add(new KeyValuePair<string, string>("", String.Format(SrmWebContent.Credentials_Missing_Format, SrmWebContent.Provider_Step_Isilon_APICredentials)));

                // Credentials are missing. User should enter new valid credentials
                log.InfoFormat("Credentials with ID {0} are missing. User is being asked to enter new credentials for provider {1}", emcHttpProvider.CredentialID, emcHttpProvider.Name);
            }
        }

        RefillConfiguration();
    }

    private void RefillConfiguration()
    {
        var engine = ServiceLocator.GetInstance<IEngineDAL>().GetEngine(array.EngineID);

        configuration.DeviceGroup = DeviceGroupDal.GetDeviceGroupDetailsByArrayId(array.StorageArrayId.Value);
        configuration.PollingConfig.BusinessLayerPort = engine.BusinessLayerPort;
        configuration.PollingConfig.EngineID = engine.EngineID;
        configuration.PollingConfig.ServerName = engine.ServerName;
        configuration.ProviderConfiguration.AvailableProviderEntities = null;
        if (!IsPostBack)
        {
            ClearSelectedProviders();
        }
    }

    private void ClearSelectedProviders()
    {
        configuration.ProviderConfiguration.SelectedProviders.RemoveAll(a => true);
    }

    public string ValidateJSFunction()
    {
        return "SW.SRM.EditProperties.EmcHttp.InitValidation();";
    }

    public bool Validate(bool connectionPropertyChanged)
    {
        var shouldValidate = !AllFieldsEmpty();

        //If we don't want to validate this settings just skip the validation -> return true
        if (!shouldValidate)
        {
            return true;
        }

        return this.ValidateUserInput();
    }

    private bool AllFieldsEmpty()
    {
        var listOfTextboxes = new List<TextBox>()
                                  {
                                      this.IpAddressTextBox,
                                      this.DisplayNameVnxTextBox,
                                      this.ArrayApiUsernameTextBox,
                                      this.ArrayApiPasswordTextBox,
                                      this.ArrayApiConfirmPasswordTextBox,
                                      this.HttpPortTextBox
                                  };

        var listOfDropDowns = new List<DropDownList>() { this.ChooseCredentialEmcHttp };

        return listOfTextboxes.All(tb => string.IsNullOrEmpty(tb.Text))
               && listOfDropDowns.All(dd => dd.SelectedIndex == 0);
    }

    #region SNMP methods

    private bool SNMPVersionOneSelected
    {
        get
        {
            return SelectedSnmpCredential.Value == "V1";
        }
    }

    private void UpdateSNMPProvider(Credential credentials, int? credentialID = null)
    {
        var providerEntity = new EnrollmentWizardProvider
        {
            ProviderType = ProviderTypeEnum.File,
            ProviderLocation = ProviderLocation.Onboard,
            IPAddress = IpAddressTextBox.Text.Trim(),
            Credential = credentials,
            CredentialID = credentialID ?? credentials.ID.Value
        };

        if (SNMPVersionOneSelected)
        {
            providerEntity.CredentialType = CredentialType.SNMP;
        }
        else
        {
            providerEntity.CredentialType = CredentialType.SnmpV3;
        }

        if (array != null)
            this.RefillConfiguration();

        UpdateProviders(providerEntity, new[] { CredentialType.SNMP, CredentialType.SnmpV3 });
    }

    private Credential SaveSNMPCredentials()
    {
        if (SNMPVersionOneSelected)
        {
            return SnmpVersionOneCredentials.SaveValue();
        }
        else
        {
            return SnmpVersionThreeCredentials.SaveValue();
        }
    }

    public void SaveSNMPProvider()
    {
        var credentials = this.SaveSNMPCredentials();
        this.UpdateSNMPProvider(credentials);
    }

    #endregion
}
