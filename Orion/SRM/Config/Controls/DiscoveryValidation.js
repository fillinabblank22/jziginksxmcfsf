﻿SW = SW || {};
SW.SRM = SW.SRM || {};

SW.SRM.DiscoveryValidation = function () {
    var request, testingDeffered, connectionTestUnexpectedFailedMessage, errorHandlingFunction, arrayID, arraySerialNumber, groupId, successHandlingFunction;

    var handleError = function (message, provider) {
        SW.SRM.TestConnectionUI.HideDiscovery(provider);
        errorHandlingFunction(message);
        testingDeffered.reject();
    }

    var result = {
        validateDiscovery: function (provider) {
            request = SW.SRM.Common.CallSrmWebService("/Orion/SRM/Services/EnrollmentWizardService.asmx",
                "CheckArrayIsInDiscoveryJobResults",
                {
                    'arrayID' : arrayID,
                    'arraySerialNumber': arraySerialNumber
                },
                function(result) {
                    if (!result.IsDone) {
                        setTimeout(function () { SW.SRM.DiscoveryValidation.validateDiscovery(provider); }, 1000);
                    } else {
                        if (result.IsSuccess) {
                            SW.SRM.TestConnectionUI.HideDiscovery(provider);

                            if (successHandlingFunction) {
                                successHandlingFunction(result);
                            }

                            testingDeffered.resolve();
                        } else {
                            handleError(result.ErrorMessage, provider);
                        }
                    }
                },
                function() {
                    handleError(connectionTestUnexpectedFailedMessage, provider);
                });
        },
        startDiscovery: function (provider) {
            SW.SRM.TestConnectionUI.ShowDiscovery(provider);
            
            request = SW.SRM.Common.CallSrmWebService("/Orion/SRM/Services/EnrollmentWizardService.asmx",
                "StartDiscoveryJob",
                {
                    'groupId': groupId
                },
                function(result) {
                    if (result.IsSuccess) {
                        SW.SRM.DiscoveryValidation.validateDiscovery(provider);
                    } else {
                        handleError(result.ErrorMessage, provider);
                    }
                },
                function() {
                    handleError(connectionTestUnexpectedFailedMessage, provider);
                });
        },
        rejectDeferred: function(runDiscovery) {
            if (runDiscovery) 
                testingDeffered.reject();
        },
        init: function (idOfArray, serialNumberOfArray, devicedGroupId, ajaxRequest, defferedInstance, connectionTestUnexpectedErrorMessage, callBackOnFailure, callBackOnSuccess) {
            request = ajaxRequest;
            testingDeffered = defferedInstance;
            connectionTestUnexpectedFailedMessage = connectionTestUnexpectedErrorMessage;
            errorHandlingFunction = callBackOnFailure;
            successHandlingFunction = callBackOnSuccess;
            arrayID = idOfArray;
            arraySerialNumber = serialNumberOfArray;
            groupId = devicedGroupId;
        }
    }

    result.MonitoredControlIds = ['IpAddressTextBox', 'SnmpCredentialDropDown', 'SnmpCommunityTextBox', 'SnmpCredentialDropDownSnmpV3', 'SnmpUserNameTextBox', 'SnmpContextTextBox'];
    SW.SRM.EditProperties.ProviderChangeManager.InitProvider(result, result.validateDiscovery);

    return result;
}();