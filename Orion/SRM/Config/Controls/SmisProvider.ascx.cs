﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json.Linq;
using Resources;

using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.EnrollmentWizard;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.Logging;
using System.Text.RegularExpressions;
using SolarWinds.SRM.Common.Discovery;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.Resources;

public partial class Orion_SRM_Config_Controls_SmisProvider : UpdateProviderBase, ICreateEditControl, IEditControl
{
    private List<ProviderEntity> providers;

    protected string storagePollingPropertiesLabel = string.Empty;

    public ProviderEntity smisProvider
    {
        get { return primaryProvider; }
        set { primaryProvider = value; }
    }

    public Orion_SRM_Config_Controls_SmisProvider()
    {
        HiddenFieldEditPropertiesID = "hfEditPropertiesSmisCfg";
        HiddenFieldProviderGuidID = "hfSmisProviderGuid";
        HiddenFieldDiscoveryGuidID = "hfDiscoveryGuid";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        deviceInfo.InitWithDevice(ServiceLocator.GetInstance<IDeviceGroupDAL>().GetDeviceGroupDetailsByArrayId(array.StorageArrayId.Value));
        this.loadingIconSmisProvider.AlternateText = SrmWebContent.EditProperties_SmisProvider_Loading;
    }

    public IEditControl InitFromNetObjects(List<string> listOfNetObjectIDs)
    {
        // Supports just one NetObject ID and just SMSA
        if (listOfNetObjectIDs.Count() != 1)
        {
            return null;
        }

        var netObjectId = new NetObjectUniqueIdentifier(listOfNetObjectIDs.First());

        if (netObjectId.Prefix != StorageArray.NetObjectPrefix)
        {
            return null;
        }

        array = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayDetails(netObjectId.ID);

        // Supporting just EMCVNX
        if (array.FlowType != FlowType.Smis && array.FlowType != FlowType.SmisEmcUnified)
        {
            return null;
        }

        this.providers = ServiceLocator.GetInstance<IStorageProviderDAL>().GetAllProvidersByArrayId(array.StorageArrayId);

        // Needs to contain at least Clarion Block (Smis credentials) provider
        this.smisProvider = this.providers.FirstOrDefault(
            p => (p.ProviderType == ProviderTypeEnum.Block || p.ProviderType == ProviderTypeEnum.Unified)
                && p.CredentialType == CredentialType.SMIS
                && p.ProviderLocation == ProviderLocation.External);

        if (this.smisProvider == null)
        {
            return null;
        }
        else
        {
            this.storagePollingPropertiesLabel = this.smisProvider.ProviderType == ProviderTypeEnum.Unified
                ? SrmWebContent.EditProperties_SmisProvider_UnifiedStoragePollingProperties
                : SrmWebContent.EditProperties_SmisProvider_BlockStoragePollingProperties;
        }

        PollingProperties.ListOfNetObjectIDs = new List<NetObjectUniqueIdentifier>() { netObjectId };

        FillControl();

        return this;
    }

    private void FillControl()
    {
        DeviceGroup dDeviceGroup;
        if (base.FillControlForPrimaryProvider(this.hfPlaceholder, hlProvider, this.PollingProperties, out dDeviceGroup))
        {
            txtArrayType.Text = dDeviceGroup.Name;
            hlProvider.Text = "";

            // Re-adding child elements after they are cleared on Hyperling.Text property change.           
            if (this.hlProvider.Controls.Count == 0)
            {
                this.hlProvider.Controls.Add(providerStatusIcon);
                this.hlProvider.Controls.Add(providerName);
            }

            providerStatusIcon.ImageUrl = InvariantString.Format(
                "~/Orion/StatusIcon.ashx?size=small&entity=Orion.SRM.Providers&id={0}&status={1}",
                this.smisProvider.ID,
                (int)this.smisProvider.Status);
            providerName.InnerHtml = this.smisProvider.FriendlyName;                      
        }
    }

    protected override void OnMissingCredentials()
    {
        // In case two Block & File SMIS providers exist display particular warning for each
        var smisFileProviderExists = this.providers.Any(p => p.CredentialType == CredentialType.SMIS && p.ProviderType == ProviderTypeEnum.File);

        //Display warning about missing credentials
        this.Warnings.Add(new KeyValuePair<string, string>("", String.Format(SrmWebContent.Credentials_Missing_Format,
            smisFileProviderExists ? SrmWebContent.Credentials_SMIS_BlockStorage : SrmWebContent.Provider_Step_Celerra_SMISCredentials)));
    }

    public string ValidateJSFunction()
    {
        // The extenally started validation by Submit button will start connection validation + discovery
        return "SW.SRM.EditProperties.SmisProvider.InitValidation();";
    }

    public bool Validate(bool connectionPropertyChanged)
    {
        if (!connectionPropertyChanged)
            return true;

        // This will validate either Array Block, or Array Unified discovery results, depending whether Celerra section was filled
        // For both, validation will just check Serial numbers, they will be started with different providers (Array Block = 1 provider, Array unified = 2 new providers)
        var discoveryHF = (HiddenField)(WebHelper.FindControl(hfPlaceholder, c => c.ID == "hfDiscoveryGuid").First());
        var discoveryGuid = Guid.Parse(discoveryHF.Value);
        var hfEditPropertiesCfg = ((HiddenField)WebHelper.FindControl(hfPlaceholder, c => c.ID == "hfEditPropertiesSmisCfg").First()).Value;

        var discoveryResult = SrmSessionManager.GetDiscoveryResult(discoveryGuid);
        if (discoveryResult == null)
            return false;

        var discoveryArrays = discoveryResult.Value.Value;
        if (discoveryArrays == null)
        {
            // No storage arrays found during discovery
            return false;
        }

        var regex = new Regex("File:[\\s]([\\w]+)[\\s]Block:[\\s]([\\w]+)");

        JObject editPropertiesCfg = JObject.Parse(hfEditPropertiesCfg);
        bool validateSNOnly = editPropertiesCfg["validateSN"] == null ? false : (editPropertiesCfg["validateSN"].Value<bool>());

        // List of Arrays, whose SerialNumber = "File: APM00114104374 Block: APM00114104374"
        if (discoveryArrays.Exists(a =>
        {
            if (validateSNOnly)
            {
                return string.Equals(a.SerialNumber, array.SerialNumber, StringComparison.InvariantCultureIgnoreCase);
            }
            else
            {
                var match = regex.Match(a.SerialNumber);
                return match.Success && match.Groups.Count == 3 && match.Groups[2].Value == array.SerialNumber;
            }
        }))
        {
            return true;
        }

        ShowValidationError(
            discoveryResult.Value.Key == SRMDiscoveryJobResultCode.ProviderBusy ? SrmWebContent.Provider_Step_ProviderBusy : SrmWebContent.EditProperties_DeviceValidationErrorHeading,
            SrmWebContent.EditProperties_DeviceValidationErrorText);

        Log.DebugFormat("Discovered Array SerialNumbers ({0}) does not contain currently edited Array SN {1}", string.Join(", ", discoveryArrays.Select(a => a.SerialNumber)), array.SerialNumber);
        return false;
    }

    public KeyValuePair<string, string>[] GetWarnings()
    {
        return this.Warnings.ToArray();
    }

    private void ShowValidationError(string title, string msg)
    {
        String scriptText = string.Format("$( function () {{ SW.SRM.EditProperties.SmisProvider.showError('{0}','{1}'); }} );", title, msg);

        Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
           "ShowValidationError", scriptText, true);
    }

    public void Update(bool connectionPropertyChanged)
    {
        if (!connectionPropertyChanged)
            return;

        // Either Stores changes for Array Block (just Clarion Block), or import results of Array Unified (existing Clarion Block + new Celerra Block and Celerra File) discovery
        // Stores Clarion SMIS-S, Celerra SMI-S and Celerra VNX providers
        // DiscoveryImport should turn Array Block into Array Unified when it receives these providers

        var selectedProviders = new List<EnrollmentWizardProvider>();
        // This provider wasn't changed
        using (var proxy = SolarWinds.SRM.Common.BL.BusinessLayerFactory.Create())
        {
            this.smisProvider.Credential = proxy.GetCredential(this.smisProvider.CredentialID, this.smisProvider.CredentialType);
            selectedProviders.Add(new EnrollmentWizardProvider(this.smisProvider));
        }

        // Reads session variables set previously
        selectedProviders.AddRange(WizardWorkflowHelper.ConfigurationInfo.ProviderConfiguration.SelectedProviders);

        var discoveryGuid = Guid.Parse(((HiddenField)WebHelper.FindControl(hfPlaceholder, c => c.ID == HiddenFieldDiscoveryGuidID).First()).Value);
        var discovery = SrmSessionManager.GetDiscoveryResult(discoveryGuid);
        if (discovery == null || discovery.Value.Value == null)
        {
            Log.ErrorFormat("Discovery result with GUID {0} does not contain a discovery result", discoveryGuid);
            return;
        }

        // Instead of discovered array, sending setting array ID (from DB) and will update in Discovery
        var toDiscovery = new List<SelectedArrayEntity>();
        // This is not making deep copy, but I will get the array later in DiscoveryImport by StorageArrayId
        var selectedArray = new SelectedArrayEntity(array);
        selectedArray.IsSelected = true;
        selectedArray.StorageArrayId = array.StorageArrayId;

        toDiscovery.Add(selectedArray);

        var hfEditPropertiesCfg = ((HiddenField)WebHelper.FindControl(hfPlaceholder, c => c.ID == HiddenFieldEditPropertiesID).First()).Value;

        JObject editPropertiesCfg = JObject.Parse(hfEditPropertiesCfg);
        int engineID = editPropertiesCfg["EngineId"].Value<int>();

        using (var proxy = SolarWinds.SRM.Common.BL.BusinessLayerFactory.Create())
        {
            var passingProviders = ProviderHelper.GetProviderInfoOfWizardProviders(this.providers, selectedProviders);

            UpdateArraysIfPromotingToUnified(toDiscovery, passingProviders, engineID);

            proxy.DiscoveryImport(engineID, passingProviders, toDiscovery);
        }
    }

    private bool PromoteArraysToUnified(List<ProviderInfo> passingProviders)
    {
        return passingProviders.Count() == 3
               && passingProviders.Exists(p => p.CredentialType == CredentialType.SMIS && p.ProviderType == ProviderTypeEnum.Block)
               && passingProviders.Exists(p => p.CredentialType == CredentialType.SMIS && p.ProviderType == ProviderTypeEnum.File)
               && passingProviders.Exists(p => p.CredentialType == CredentialType.VNX && p.ProviderType == ProviderTypeEnum.File);
    }

    private void UpdateArraysIfPromotingToUnified(IEnumerable<SelectedArrayEntity> passingArrays, List<ProviderInfo> passingProviders, int engineId)
    {
        if (!PromoteArraysToUnified(passingProviders))
        {
            return;
        }

        var templateId = ((HiddenField)WebHelper.FindControl(Page, c => c.ID == "hfEditPropertiesVNXFileOrUnifiedTemplateID").First()).Value;

        // only clariion arrays can be promoted to the unified
        foreach (var a in passingArrays)
        {
            a.EngineID = engineId;
            a.Type = ArrayType.Unified;
            a.TemplateId = new Guid(templateId);
        }
    }
}