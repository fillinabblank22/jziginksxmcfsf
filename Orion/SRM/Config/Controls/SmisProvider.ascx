﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SmisProvider.ascx.cs" Inherits="Orion_SRM_Config_Controls_SmisProvider" %>
<%@ Register TagPrefix="srm" TagName="PollingProperties" Src="~/Orion/SRM/Controls/PollingProperties.ascx" %>
<%@ Register TagPrefix="UIInfo" TagName="DeviceType" Src="~/Orion/SRM/Config/Controls/DeviceType.ascx" %>
<%@ Register TagPrefix="UIInfo" TagName="TestConnectionMessages" Src="~/Orion/SRM/Config/Controls/TestConnectionMessages.ascx" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="SRM.EditProperties.SmisProvider.js" Module="SRM" />
<orion:Include runat="server" File="SRM.EditProperties.SmisProvider.css" Module="SRM" />
<orion:Include runat="server" File="SRM.css" Module="SRM" />
<orion:Include runat="server" File="Discovery.css" Module="SRM" />

<asp:PlaceHolder runat="server" ID="hfPlaceholder"></asp:PlaceHolder>
<div class="srm-main-content smisprovider">
    <span><%= storagePollingPropertiesLabel %></span>
    <table>
        <tr>
            <td class="providerPropertiesColumn">
                <table>
                    <tr>
                        <td class="label"><span><span id="labelArrayType">Array Type</span>:</span></td>
                        <td>
                            <asp:Label ID="txtArrayType" runat="server" CssClass="boldText" />
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><span id="labelProvider"><%= SrmWebContent.SMI_S_Provider %></span>:</span>
                            <div id="loadingImageSmisPlaceholder">
                                <asp:Image ImageUrl="~/Orion/images/loading_gen_small.gif" style="display: none;" ClientIDMode="Static" ID="loadingIconSmisProvider" runat="server" AlternateText="Loading" />
                            </div>
                        </td>
                        <td>
                            <asp:HyperLink ID="hlProvider" runat="server" >
                                <asp:Image ID="providerStatusIcon" runat="server" />
                                <span id="providerName" runat="server"></span>
                            </asp:HyperLink>
                            <br/>
                            <input type="button" id="testConnectionSmisButton" class="providerStepButton" value="<%= SrmWebContent.Provider_Step_Test_Connection %>" onclick="SW.SRM.EditProperties.SmisProvider.ValidateClarionBlockConnection();" />
                        </td>
                    </tr>
                </table>
                <UIInfo:TestConnectionMessages runat="server" ID="TestConnectionMessages" ContainerId="testSmisProvider" JsCancellationFunction="javascript:SW.SRM.EditProperties.SmisProvider.onCancelClick();"/>
                <srm:PollingProperties runat="server" ID="PollingProperties" />
            </td>
            <td>
                <UIInfo:DeviceType runat="server" ID="deviceInfo"></UIInfo:DeviceType>
            </td>
        </tr>
    </table>
</div>
