﻿using Resources;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.Credentials;
using SolarWinds.SRM.Common.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;

public partial class Orion_SRM_Config_Controls_SnmpVersionOneCredentialsControl : System.Web.UI.UserControl
{
    public Credential Credential { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SnmpPortTextBoxV2.Text = SRMConstants.SnmpPort.ToString(CultureInfo.InvariantCulture);

            PopulateDropDown();
        }
    }

    private void PopulateDropDown()
    {
        IDictionary<int, string> credentialsList;
        using (var proxy = BusinessLayerFactory.Create())
        {
            credentialsList = proxy.GetCredentialNames(CredentialType.SNMP);
        }

        SnmpCredentialDropDown.Items.Clear();
        SnmpCredentialDropDown.Items.Add(new ListItem(SrmWebContent.Provider_Step_New_Credential, SRMConstants.NewCredential));

        foreach (KeyValuePair<int, string> cred in credentialsList)
        {
            ListItem item = new ListItem(cred.Value, cred.Key.ToString(CultureInfo.InvariantCulture));
            SnmpCredentialDropDown.Items.Add(item);
        }

        if (Credential != null && Credential.ID.HasValue)
        {
            SnmpCredentialDropDown.SelectedValue = Credential.ID.Value.ToString();
        }
    }

    public void FillForm()
    {
        if (this.Credential == null) return;

        SnmpCommunityTextBox.Text = ((SrmSnmpCredentialsV2)Credential).Community;
        SnmpPortTextBoxV2.Text = ((SrmSnmpCredentialsV2)Credential).Port.ToString(CultureInfo.InvariantCulture);
    }

    public Credential SaveValue(string selectedCredentialID)
    {
        if (selectedCredentialID != SRMConstants.NewCredential)
        {
            int credentialID = -1;
            if (Int32.TryParse(selectedCredentialID, out credentialID))
            {
                using (var proxy = BusinessLayerFactory.Create())
                {
                    Credential = proxy.GetCredential(credentialID, CredentialType.SNMP);
                }
            }
        }
        else
        {
            int port;
            Int32.TryParse(SnmpPortTextBoxV2.Text, out port);

            Credential = new SrmSnmpCredentialsV2()
            {
                Name = CredentialNameTextBox.Text,
                Community = SnmpCommunityTextBox.Text,
                Port = port
            };

            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(Credential, CredentialType.SNMP);
                Credential.ID = id;
            }
        }

        return Credential;
    }

    public Credential SaveValue()
    {
        return this.SaveValue(SnmpCredentialDropDown.SelectedValue);
    }

    public string ValidationError { get; private set; }
    public bool ValidateUserInput()
    {
        if (SnmpCredentialDropDown.SelectedValue == SRMConstants.NewCredential)
        {
            if (String.IsNullOrEmpty(CredentialNameTextBox.Text))
            {
                this.ValidationError = String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, SrmWebContent.Provider_Step_Credential_Name);
                return false;
            }

            if (String.IsNullOrEmpty(SnmpCommunityTextBox.Text))
            {
                this.ValidationError = String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, SrmWebContent.Provider_Step_Array_Snmp_Community);
                return false;
            }

            if (String.IsNullOrEmpty(SnmpPortTextBoxV2.Text))
            {
                this.ValidationError = String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, SrmWebContent.Provider_Step_Array_Snmp_Port);
                return false;
            }
        }
        return true;
    }
}
