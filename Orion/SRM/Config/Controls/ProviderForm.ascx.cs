﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI;
using Resources;
using SolarWinds.Common.Net;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.Credentials;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Helpers;
using SolarWinds.SRM.Web.EnrollmentWizard;
using SolarWinds.SRM.Common.Models;

public partial class Orion_SRM_Config_Controls_ProviderForm : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var configuration = WizardWorkflowHelper.ConfigurationInfo;
        deviceInfo.InitWithDevice(configuration.DeviceGroup);
        ArrayTypeLabel.Text = configuration.DeviceGroup.Name;
        trArrayType.Visible = DisplayArrayType;
        trInteropNamespace.Visible = configuration.DeviceGroup.FlowType != FlowType.NetAppDfm && configuration.DeviceGroup.FlowType != FlowType.XtremIO;
        trArrayNamespace.Visible = this.trInteropNamespace.Visible;


        if (IsPostBack)
        {
            return;
        }

        if (configuration.DeviceGroup.FlowType == FlowType.XtremIO)
        {
            labelIpAddress.Text = SrmWebContent.Provider_Step_XtremIO_IPAddress;
            labelProviderUserName.Text = SrmWebContent.Add_Provider_XtremIO_Username;
            labelProviderPassword.Text = SrmWebContent.Add_Provider_XtremIO_Password;
        }
        else
        {
            labelIpAddress.Text = SrmWebContent.Add_Provider_IP_Address_Hostname;
            labelProviderUserName.Text = SrmWebContent.Add_Provider_ProviderUsername;
            labelProviderPassword.Text = SrmWebContent.Add_Provider_Password;
        }

        HttpPortTextBox.Value = configuration.DeviceGroup.GetProperty("HttpPort", SRMConstants.DefaultSmisHttpPort.ToString(CultureInfo.InvariantCulture));
        HttpsPortTextBox.Value = configuration.DeviceGroup.GetProperty("HttpsPort", SRMConstants.DefaultSmisHttpsPort.ToString(CultureInfo.InvariantCulture));

        if (!this.trArrayNamespace.Visible)
        {
            return;
        }

        this.ArrayNamespaceTextBox.Value = configuration.DeviceGroup.GetProperty(
                    SRMConstants.DefaultArrayNamespacePropertyName,
                    WebSettingsDAL.GetValue(SRMConstants.SmisArrayNamespaceSettingID, string.Empty));
        this.InteropNamespaceTextBox.Value = configuration.DeviceGroup.InteropNamespace;

        if (configuration.DeviceGroup.ProviderLocationOrOnboard == ProviderLocation.Onboard)
        {
            labelIpAddress.Text = SrmWebContent.Provider_Step_Array_Ip;
        }
    }

    public string AddProviderToolbarLabel = SrmWebContent.Provider_Step_Add_Dfm_Btn_Label;
    public string Provider_Step_Add_Provider_Title = SrmWebContent.Provider_Step_Add_Dfm_Provider_Title;

    public string JsCancellationFunction
    {
        set
        {
            TestConnectionMessages.JsCancellationFunction = value;
        }
    }

    public bool DisplayArrayType { get; set; }

    public string IPAddressOrHostName
    {
        get
        {
            return WebSecurityHelper.SanitizeHtml(this.IpAddressOrHostnameTextBox.Value);
        }
        set
        {
            IpAddressOrHostnameTextBox.Value = Server.HtmlEncode(value);
        }
    }

    public bool FillCredentialsList(IDictionary<string, string> credentialsList, int credentialID = 0)
    {
        bool credentialsWasSelected = true;
        ChooseCredentialDdl.DataSource = credentialsList;
        ChooseCredentialDdl.DataTextField = "Value";
        ChooseCredentialDdl.DataValueField = "Key";
        if (credentialID > 0)
        {
            if (credentialsList.ContainsKey(credentialID.ToString(CultureInfo.InvariantCulture)))
            {
                ChooseCredentialDdl.SelectedValue = credentialID.ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                credentialsWasSelected = false;
            }
        }
        ChooseCredentialDdl.DataBind();
        return credentialsWasSelected;
    }

    private int GetPort(string port)
    {
        int resultPort;
        return int.TryParse(port, out resultPort) ? resultPort : 0;
    }

    private int GetSelectedCredentialId()
    {
        int credentialId;
        return int.TryParse(ChooseCredentialDdl.SelectedValue, out credentialId) ? credentialId : 0;
    }

    private Credential GetDfmCredentials()
    {
        int credentialId = GetSelectedCredentialId();

        int httpPort = GetPort(this.HttpPortTextBox.Value);
        if (httpPort < 1)
        {
            return null;
        }

        int httpsPort = GetPort(this.HttpsPortTextBox.Value);
        if (httpsPort < 1)
        {
            return null;
        }

        if (credentialId < 1)
        {
            var credential = new NetAppDfmCredentials()
                                        {
                                            Name = WebSecurityHelper.SanitizeHtml(this.DisplayNameTextBox.Value),
                                            Username = WebSecurityHelper.SanitizeHtml(this.ProviderUsernameTextBox.Value),
                                            Password = WebSecurityHelper.SanitizeHtml(this.ProviderPasswordTextBox.Value),
                                            HttpPort = httpPort,
                                            HttpsPort = httpsPort,
                                            UseSsl = !this.ProtocolCheckBox.Checked
                                        };

            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(credential, CredentialType.NetAppDfm);
                credential.ID = id;
            }

            return credential;
        }

        using (var proxy = BusinessLayerFactory.Create())
        {
            return proxy.GetCredential(credentialId, CredentialType.NetAppDfm);
        }
    }

    private Credential GetSmisCredentials()
    {
        int credentialId = GetSelectedCredentialId();

        int httpPort = GetPort(this.HttpPortTextBox.Value);
        if (httpPort < 1)
        {
            return null;
        }

        int httpsPort = GetPort(this.HttpsPortTextBox.Value);
        if (httpsPort < 1)
        {
            return null;
        }

        if (credentialId < 1)
        {
            var credential = new SmisCredentials()
            {
                Name = WebSecurityHelper.SanitizeHtml(this.DisplayNameTextBox.Value),
                Username = WebSecurityHelper.SanitizeHtml(this.ProviderUsernameTextBox.Value),
                Password = WebSecurityHelper.SanitizeHtml(this.ProviderPasswordTextBox.Value),
                HttpPort = (uint)httpPort,
                HttpsPort = (uint)httpsPort,
                UseSSL = !this.ProtocolCheckBox.Checked,
                InteropNamespace = WebSecurityHelper.SanitizeHtml(this.InteropNamespaceTextBox.Value),
                Namespace = WebSecurityHelper.SanitizeHtml(this.ArrayNamespaceTextBox.Value),
            };

            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(credential, CredentialType.SMIS);
                credential.ID = id;
            }

            return credential;
        }

        using (var proxy = BusinessLayerFactory.Create())
        {
            return proxy.GetCredential(credentialId, CredentialType.SMIS);
        }
    }

    private Credential GetXtremIoCredentials()
    {
        int credentialId = GetSelectedCredentialId();

        int httpPort = GetPort(this.HttpPortTextBox.Value);
        if (httpPort < 1)
        {
            return null;
        }

        int httpsPort = GetPort(this.HttpsPortTextBox.Value);
        if (httpsPort < 1)
        {
            return null;
        }

        if (credentialId < 1)
        {
            var credential = new XtremIoHttpCredential()
            {
                Name = WebSecurityHelper.SanitizeHtml(this.DisplayNameTextBox.Value),
                Username = WebSecurityHelper.SanitizeHtml(this.ProviderUsernameTextBox.Value),
                Password = WebSecurityHelper.SanitizeHtml(this.ProviderPasswordTextBox.Value),
                HttpPort = httpPort,
                HttpsPort = httpsPort,
                UseSsl = !this.ProtocolCheckBox.Checked
            };

            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(credential, CredentialType.XtremIoHttp);
                credential.ID = id;
            }

            return credential;
        }

        using (var proxy = BusinessLayerFactory.Create())
        {
            return proxy.GetCredential(credentialId, CredentialType.XtremIoHttp);
        }
    }

    public ProviderInfo GetProviderInfo()
    {
        Credential credential;
        try
        {
            if (WizardWorkflowHelper.ConfigurationInfo.DeviceGroup.FlowType == FlowType.NetAppDfm)
            {
                credential = this.GetDfmCredentials();
            }
            else if (WizardWorkflowHelper.ConfigurationInfo.DeviceGroup.FlowType == FlowType.XtremIO)
            {
                credential = this.GetXtremIoCredentials();
            }
            else
            {
                credential = this.GetSmisCredentials();
            }
        }
        catch (Exception)
        {
            return null;
        }

        if (credential == null)
        {
            return null;
        }

        var ipAddressAndHostName = new IPAddressAndHostName(this.IpAddressOrHostnameTextBox.Value.Trim());

        return new ProviderInfo()
        {
            IPAddress = ipAddressAndHostName.IPAddress,
            HostName = ipAddressAndHostName.HostName,
            Credential = credential
        };
    }

    private string GetErrorIfEmpty(string value, string label)
    {
        return string.IsNullOrEmpty(value.Trim())
            ? string.Format(SrmWebContent.Add_Provider_Required_Field_Validator, label)
            : string.Empty;
    }

    public List<string> GetUserInputValidationErrors()
    {
        var validationErrors = new List<string>
                                   {
                                       this.GetErrorIfEmpty(
                                           this.IpAddressOrHostnameTextBox.Value,
                                           SrmWebContent.Add_Provider_IP_Address_Hostname)
                                   };

        if (this.ChooseCredentialDdl.SelectedIndex == 0)
        {
            validationErrors.Add(GetErrorIfEmpty(this.DisplayNameTextBox.Value, SrmWebContent.Add_Provider_DisplayName));
            validationErrors.Add(GetErrorIfEmpty(this.ProviderUsernameTextBox.Value, SrmWebContent.Add_Provider_ProviderUsername));
            validationErrors.Add(GetErrorIfEmpty(this.ProviderPasswordTextBox.Value, SrmWebContent.Add_Provider_Password));
            validationErrors.Add(GetErrorIfEmpty(
                    this.ProviderConfirmPasswordTextBox.Value,
                    SrmWebContent.Add_Provider_ConfirmPassword));

            if (ProviderPasswordTextBox.Value != ProviderConfirmPasswordTextBox.Value)
            {
                validationErrors.Add(SrmWebContent.Add_Provider_ConfirmPassword_CompareField_Validator);
            }
        }

        validationErrors.Add(
            this.ProtocolCheckBox.Checked
                ? this.GetErrorIfEmpty(this.HttpPortTextBox.Value, SrmWebContent.Add_Provider_Http_Port)
                : this.GetErrorIfEmpty(this.HttpsPortTextBox.Value, SrmWebContent.Add_Provider_Https_Port));

        return validationErrors;
    }
}
