﻿using Resources;
using SolarWinds.Common.Utility;
using SolarWinds.Orion.Core.Models.Credentials;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.Credentials;
using SolarWinds.SRM.Common.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

public partial class Orion_SRM_Config_Controls_SnmpVersionThreeCredentialsControl : System.Web.UI.UserControl
{
    public Credential Credential { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SnmpPortTextBoxV3.Text = SRMConstants.SnmpPort.ToString(CultureInfo.InvariantCulture);

            ClearDropDown(AuthenticationMethodDropDown);

            AuthenticationMethodDropDown.DataSource = Enum.GetValues(typeof(SNMPv3AuthType)).Cast<SNMPv3AuthType>()
                                                                     .Where(o => o != SNMPv3AuthType.MD5 || !FIPSHelper.FIPSRestrictionEnabled)
                                                                     .Select(o => new { Text = o.GetDescription(), Value = (byte)o });
            AuthenticationMethodDropDown.DataTextField = "Text";
            AuthenticationMethodDropDown.DataValueField = "Value";
            AuthenticationMethodDropDown.DataBind();

            ClearDropDown(EncryptionMethodDropDown);

            EncryptionMethodDropDown.DataSource = Enum.GetValues(typeof(SNMPv3PrivacyType)).Cast<SNMPv3PrivacyType>()
                                                                 .Where(o => o != SNMPv3PrivacyType.DES56 || !FIPSHelper.FIPSRestrictionEnabled)
                                                                 .Select(o => new { Text = o.GetDescription(), Value = (byte)o });
            EncryptionMethodDropDown.DataTextField = "Text";
            EncryptionMethodDropDown.DataValueField = "Value";
            EncryptionMethodDropDown.DataBind();

            PopulateSnmpV3DropDown();

            FillForm();
        }
    }

    public bool IsContextRequired { get; set; }

    private void PopulateSnmpV3DropDown()
    {
        if (SnmpCredentialDropDownSnmpV3.Items.Count == 0)
        {
            SnmpCredentialDropDownSnmpV3.Items.Clear();
            SnmpCredentialDropDownSnmpV3.Items.Add(new ListItem(SrmWebContent.Provider_Step_New_Credential,
                SRMConstants.NewCredential));

            IDictionary<int, string> credentialsList;
            using (var proxy = BusinessLayerFactory.Create())
            {
                credentialsList = proxy.GetCredentialNames(CredentialType.SnmpV3);
            }

            foreach (KeyValuePair<int, string> cred in credentialsList)
            {
                ListItem item = new ListItem(cred.Value, cred.Key.ToString(CultureInfo.InvariantCulture));
                SnmpCredentialDropDownSnmpV3.Items.Add(item);
            }

            if (Credential != null && Credential.ID.HasValue)
            {
                SnmpCredentialDropDownSnmpV3.SelectedValue = Credential.ID.Value.ToString();
            }
        }
    }

    private void ClearDropDown(DropDownList ddl)
    {
        ddl.Items.Clear();
        ddl.SelectedIndex = -1;
        ddl.SelectedValue = null;
        ddl.ClearSelection();
    }

    public void FillForm()
    {
        if (this.Credential == null) return;

        var credential = (SrmSnmpCredentialsV3)Credential;

        SnmpCredentialDropDownSnmpV3.SelectedValue = credential.ID.ToString();
        AuthenticationMethodDropDown.SelectedValue = credential.AuthenticationType.ToString();
        EncryptionMethodDropDown.SelectedValue = credential.PrivacyType.ToString();
        SnmpUserNameTextBox.Text = credential.UserName;
        SnmpContextTextBox.Text = credential.Context;
        PasswordKey1TextBox.Text = credential.AuthenticationPassword;
        PasswordKey1CheckBox.Checked = credential.AuthenticationKeyIsPassword;
        PasswordKey2TextBox.Text = credential.PrivacyPassword;
        PasswordKey2CheckBox.Checked = credential.PrivacyKeyIsPassword;
        SnmpUserNameTextBox.Text = credential.UserName;
        SnmpPortTextBoxV3.Text = credential.Port.ToString(CultureInfo.InvariantCulture);
    }

    public Credential SaveValue()
    {
        byte authenticationValue;
        SNMPv3AuthType authenticationType = SNMPv3AuthType.None;
        if (byte.TryParse(AuthenticationMethodDropDown.SelectedValue, out authenticationValue))
        {
            authenticationType = (SNMPv3AuthType)authenticationValue;
        }

        byte privacyValue;
        SNMPv3PrivacyType privacyType = SNMPv3PrivacyType.None;
        if (byte.TryParse(EncryptionMethodDropDown.SelectedValue, out privacyValue))
        {
            privacyType = (SNMPv3PrivacyType)privacyValue;
        }

        return this.SaveValue(SnmpCredentialDropDownSnmpV3.SelectedValue, authenticationType, privacyType);
    }

    public Credential SaveValue(int selectedCredentialID)
    {
        SrmSnmpCredentialsV3 credential;
        using (var proxy = BusinessLayerFactory.Create())
        {
            credential = (SrmSnmpCredentialsV3)proxy.GetCredential(selectedCredentialID, CredentialType.SnmpV3);
        }

        return this.SaveValue(selectedCredentialID.ToString(CultureInfo.InvariantCulture), credential.AuthenticationType, credential.PrivacyType);
    }

    public Credential SaveValue(string selectedCredentialID, SNMPv3AuthType authenticationType, SNMPv3PrivacyType privacyType)
    {
        if (selectedCredentialID != SRMConstants.NewCredential)
        {
            int credentialID = -1;
            Int32.TryParse(selectedCredentialID, out credentialID);
            using (var proxy = BusinessLayerFactory.Create())
            {
                Credential = proxy.GetCredential(credentialID, CredentialType.SnmpV3);
            }
        }
        else
        {
            int port;
            Int32.TryParse(SnmpPortTextBoxV3.Text, out port);

            Credential = new SrmSnmpCredentialsV3()
            {
                Name = SnmpV3CredentialNameTextBox.Text,
                UserName = SnmpUserNameTextBox.Text,
                Context = SnmpContextTextBox.Text,
                AuthenticationType = authenticationType,
                PrivacyType = privacyType,
                AuthenticationPassword = PasswordKey1TextBox.Text,
                AuthenticationKeyIsPassword = PasswordKey1CheckBox.Checked,
                PrivacyPassword = PasswordKey2TextBox.Text,
                PrivacyKeyIsPassword = PasswordKey2CheckBox.Checked,
                Port = port
            };
            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(Credential, CredentialType.SnmpV3);
                Credential.ID = id;
            }
        }
        return Credential;
    }

    public string ValidationError { get; private set; }
    public bool ValidateUserInput()
    {
        if (SnmpCredentialDropDownSnmpV3.SelectedValue == SRMConstants.NewCredential)
        {
            if (AuthenticationMethodDropDown.SelectedValue != ((int)SNMPv3AuthType.None).ToString())
            {
                if (string.IsNullOrEmpty(PasswordKey1TextBox.Text))
                {
                    this.ValidationError = String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, SrmWebContent.Provider_Step_PasswordKey);
                    return false;
                }
            }

            if (EncryptionMethodDropDown.SelectedValue != ((int)SNMPv3PrivacyType.None).ToString())
            {
                if (string.IsNullOrEmpty(PasswordKey2TextBox.Text))
                {
                    this.ValidationError = String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, SrmWebContent.Provider_Step_PasswordKey);
                    return false;
                }
            }

            if (string.IsNullOrEmpty(SnmpV3CredentialNameTextBox.Text))
            {
                this.ValidationError = String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, SrmWebContent.Provider_Step_Credential_Name);
                return false;
            }
            else if (string.IsNullOrEmpty(SnmpUserNameTextBox.Text))
            {
                this.ValidationError = String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, SrmWebContent.Provider_Step_Array_Snmp_User_Name);
                return false;
            }
            else if (IsContextRequired && string.IsNullOrEmpty(SnmpContextTextBox.Text))
            {
                this.ValidationError = String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, SrmWebContent.Provider_Step_Array_Snmp_Context);
                return false;
            }
            else if (string.IsNullOrEmpty(SnmpPortTextBoxV3.Text))
            {
                this.ValidationError = String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, SrmWebContent.Provider_Step_Array_Snmp_Port);
                return false;
            }
            else
            {
                return true;
            }
        }
        return true;
    }
}
