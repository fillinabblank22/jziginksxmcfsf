<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CelerraProviderStep.ascx.cs" Inherits="Orion_SRM_Config_Controls_CelerraProviderStep" %>
<%@ Import Namespace="Resources" %>
<%@ Register Src="~/Orion/SRM/Config/Controls/TestConnectionMessages.ascx" TagPrefix="UIInfo" TagName="TestConnectionMessages" %>
<%@ Register Src="~/Orion/SRM/Config/Controls/DeviceType.ascx" TagPrefix="UIInfo" TagName="DeviceType" %>


<orion:Include ID="Include1" runat="server" File="OrionCore.js" />
<orion:Include ID="Include2" runat="server" File="SRM/Config/Controls/AddCelerraProvider.js" />
<orion:Include ID="Include" runat="server" File="SRM/Config/Controls/DiscoveryValidation.js" />
<orion:Include runat="server" File="Discovery.css" Module="SRM" />
<orion:Include runat="server" File="CelerraProviderStep.css" Module="SRM" />



<script type="text/javascript">
    onClickNextButtonEvent = function () {
        SW.SRM.AddCelerraProvider.testCredential(providerStepSubmit);
    };

    $(function () {
        ChooseCredentialVnx_OnChange({ value: $('#ChooseCredentialVnx').val() });
        ChooseCredentialSmis_OnChange({ value: $('#ChooseCredentialSmis').val() });
        enableCredentials(!$('#UseSameCredentials').is(':checked'));

        SW.SRM.AddCelerraProvider.init('<%= SrmWebContent.Provider_Step_Provider_Connecting_Error %>:', '<%= SrmWebContent.Provider_Step_Unexpected_Error %>', null, '<%= ArrayID %>', '<%= ArraySerialNumber %>', '<%= this.DeviceGroupId %>');
    });

    function enableUseSameCredCheckBox(state) {
        if (state) {
            $("#UseSameCredentials").removeAttr('disabled');
            $("#UseSameCredentialsLabel").removeClass('unselectable');
        }
        else {
            $("#UseSameCredentials").removeAttr('checked');
            $("#UseSameCredentials").attr('disabled', 'disabled');
            $("#UseSameCredentialsLabel").addClass('unselectable');
        }
    }

    function text_OnChanged($self, $related) {
        if ($('#UseSameCredentials').is(':checked')) {
            $related.val($self.val());
        }
    }

    function enableCredentials(state) {
        if (state) {
            $("#ArrayApiUsernameTextBox").removeAttr('disabled');
            $("#labelApiProviderUserName").removeClass('unselectable');
            $("#ArrayApiPasswordTextBox").removeAttr('disabled');
            $("#labelApiProviderPassword").removeClass('unselectable');

            $("#ArrayApiConfirmPasswordTextBox").removeAttr('disabled');
            $("#labelApiConfirmProviderPassword").removeClass('unselectable');
        }
        else {
            $("#ArrayApiUsernameTextBox").attr('disabled', 'disabled');
            $("#labelApiProviderUserName").addClass('unselectable');

            $("#ArrayApiPasswordTextBox").attr('disabled', 'disabled');
            $("#labelApiProviderPassword").addClass('unselectable');

            $("#ArrayApiConfirmPasswordTextBox").attr('disabled', 'disabled');
            $("#labelApiConfirmProviderPassword").addClass('unselectable');

            text_OnChanged($('#ProviderUsernameTextBox'), $('#ArrayApiUsernameTextBox'));
            text_OnChanged($('#ProviderPasswordTextBox'), $('#ArrayApiPasswordTextBox'));
            text_OnChanged($('#ProviderConfirmPasswordTextBox'), $('#ArrayApiConfirmPasswordTextBox'));
        }
    }

    function ChooseCredentialSmis_OnChange(ddl) {
        var $containerElement = $("#AdvancedSmisCredFiledsTable");

        if (ddl.value) {
            enableUseSameCredCheckBox(false);
            enableCredentials(true);

            $("#StoredCredFiledsTable").hide('slow');
            $containerElement.hide('slow');

            if ($('#ChooseCredentialVnx').val()) {
                $('#expanderElement').hide('slow');
            }
        } else {
            enableUseSameCredCheckBox(true);

            $("#StoredCredFiledsTable").show('slow');
            $('#expanderElement').show('slow');

            if ($('#expanderElement img').attr('src') === '/Orion/SRM/images/Button.Collapse.gif') {
                $containerElement.show('slow');
            } else {
                $containerElement.hide('slow');
            }
        }
    }

    function ChooseCredentialVnx_OnChange(ddl) {
        var $containerElement = $('#AdvancedApiCredFiledsTable');

        if (ddl.value) {
            enableUseSameCredCheckBox(false);
            enableCredentials(true);

            $("#StoredCredFiledsTableVnx").hide('slow');
            $containerElement.hide('slow');

            if ($('#ChooseCredentialSmis').val()) {
                $('#expanderElement').hide('slow');
            }
        } else {
            if (!$('#ChooseCredentialSmis').val()) {
                enableUseSameCredCheckBox(true);
            }

            $("#StoredCredFiledsTableVnx").show('slow');
            $('#expanderElement').show('slow');

            if ($('#expanderElement img').attr('src') === '/Orion/SRM/images/Button.Collapse.gif') {
                $containerElement.show('slow');
            } else {
                $containerElement.hide('slow');
            }
        }
    }

    function expanderChangedHandler() {
        var $apiContainerElement = $('#AdvancedApiCredFiledsTable');
        var $smisContainerElement = $('#AdvancedSmisCredFiledsTable');

        var $expanderImgElement = $('#expanderElement img');
        if ($expanderImgElement.attr('src') === '/Orion/SRM/images/Button.Expand.gif') {
            if (!$('#ChooseCredentialSmis').val()) {
                $smisContainerElement.show('slow');
            }
            if (!$('#ChooseCredentialVnx').val()) {
                $apiContainerElement.show('slow');
            }
            $expanderImgElement.attr('src', '/Orion/SRM/images/Button.Collapse.gif');
            $expanderImgElement.attr('alt', '[-]');
        } else {
            $smisContainerElement.hide('slow');
            $apiContainerElement.hide('slow');
            $expanderImgElement.attr('src', '/Orion/SRM/images/Button.Expand.gif');
            $expanderImgElement.attr('alt', '[+]');
        }
    }
</script>


<span><%= SubTitle%></span>
<asp:PlaceHolder runat="server" ID="hfPlaceholder"></asp:PlaceHolder>
<div class="srm-enrollment-wizard-content PollingPropertiesSection">
    <div class="srm-main-content">
        <table class="srm-EnrollmentWizard-providerStepContentTable">
            <tr>
                <td class="providerPropertiesColumn">
                    <table class="srm-EnrollmentWizard-providerStepInputsTable">
                        <tr>
                            <td class="label"><span><%= SrmWebContent.Provider_Step_Array_Type%>:</span></td>
                            <td><b><span runat="server" id="ArrayTypeName"></span></b></td>
                        </tr>
                        <tr>
                            <td class="label"><span><asp:Label id="labelIpAddress" runat="server" ClientIDMode="Static"></asp:Label>*:</span></td>
                            <td>
                                <span><asp:TextBox id="IpAddressOrHostnameTextBox" runat="server" ClientIDMode="Static" /></span>
                            </td>
                        </tr>
                    </table>
                    <div class="CredentialsLabel"><span><b><%= SrmWebContent.Provider_Step_Celerra_SMISCredentials%></b></span>&nbsp;<asp:Image id="SmisCredentialsTooltip" src="/Orion/images/Icon.Info.gif" visible="false" runat="server" ClientIDMode="Static" /></div>
                    <table class="srm-EnrollmentWizard-providerStepInputsTable sub-table">
                        <tr>
                            <td class="label"><span><%= SrmWebContent.Provider_Step_Celerra_ChooseSMISCredentials%>*:</span></td>
                            <td>
                                <span><asp:DropDownList id="ChooseCredentialSmis" onchange="ChooseCredentialSmis_OnChange(this)" runat="server" ClientIDMode="Static"/></span>
                            </td>
                        </tr>
                    </table>
                    <table id="StoredCredFiledsTable" class="srm-EnrollmentWizard-providerStepInputsTable sub-table">
                        <tr>
                            <td class="label"><span><asp:Label id="labelSmisDisplayName" runat="server" ClientIDMode="Static"></asp:Label>*:</span></td>
                            <td>
                                <span><asp:TextBox id="DisplayNameTextBox" runat="server" ClientIDMode="Static"/></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"><span><asp:Label id="labelSmisProviderUserName" runat="server" ClientIDMode="Static"></asp:Label>*:</span></td>
                            <td>
                                <span><asp:TextBox id="ProviderUsernameTextBox" runat="server" ClientIDMode="Static" onkeyup="text_OnChanged($('#ProviderUsernameTextBox'), $('#ArrayApiUsernameTextBox'));"/></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"><span><asp:Label id="labelSmisProviderPassword" runat="server" ClientIDMode="Static"></asp:Label>*:</span></td>
                            <td>
                                <span><asp:TextBox TextMode="Password" id="ProviderPasswordTextBox" runat="server" ClientIDMode="Static" onkeyup="text_OnChanged($('#ProviderPasswordTextBox'), $('#ArrayApiPasswordTextBox'));"/></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"><span><%= SrmWebContent.Add_Provider_ConfirmPassword%>*:</span></td>
                            <td>
                                <span><asp:TextBox TextMode="Password" id="ProviderConfirmPasswordTextBox" runat="server" ClientIDMode="Static" onkeyup="text_OnChanged($('#ProviderConfirmPasswordTextBox'), $('#ArrayApiConfirmPasswordTextBox'));"/></span>
                            </td>
                        </tr>
                    </table>
                    <div class="CredentialsLabel"><span><b><%= SrmWebContent.Provider_Step_Celerra_APICredentials%></b></span>&nbsp;<asp:Image id="ApiCredentialsTooltip" src="/Orion/images/Icon.Info.gif" visible="false" runat="server" ClientIDMode="Static" /></div>
                    <div class="UseSameCredentialsDiv"><span><input type="checkbox" id="UseSameCredentials" runat="server" ClientIDMode="Static" onchange="enableCredentials(!$(this).is(':checked'));"/><span id="UseSameCredentialsLabel"><%= SrmWebContent.Provider_Step_Celerra_UseTheSameCredentials %></span></span></div>
                    <table class="srm-EnrollmentWizard-providerStepInputsTable sub-table">               
                        <tr>
                            <td class="label"><span><%= SrmWebContent.Provider_Step_Celerra_ChooseAPICredentials%>*:</span></td>
                            <td>
                                <span><asp:DropDownList id="ChooseCredentialVnx" onchange="ChooseCredentialVnx_OnChange(this)" runat="server" ClientIDMode="Static"/></span>
                            </td>
                        </tr>
                    </table>
                    <table id="StoredCredFiledsTableVnx" class="srm-EnrollmentWizard-providerStepInputsTable sub-table">   
                        <tr>
                            <td class="label"><span><asp:Label id="labelApiDisplayNameTextBox" runat="server" ClientIDMode="Static"></asp:Label>*:</span></td>
                            <td>
                                <span><asp:TextBox id="DisplayNameVnxTextBox" runat="server" ClientIDMode="Static"/></span>
                            </td>
                        </tr>  
                        <tr>
                            <td class="label"><span><asp:Label id="labelApiProviderUserName" runat="server" ClientIDMode="Static"></asp:Label>*:</span></td>
                            <td>
                                <span><asp:TextBox id="ArrayApiUsernameTextBox" runat="server" ClientIDMode="Static"/></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"><span><asp:Label id="labelApiProviderPassword" runat="server" ClientIDMode="Static"></asp:Label>*:</span></td>
                            <td>
                                <span><asp:TextBox TextMode="Password" id="ArrayApiPasswordTextBox" runat="server" ClientIDMode="Static"/></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"><span id="labelApiConfirmProviderPassword"><%= SrmWebContent.Add_Provider_ArrayAPIConfirmPassword %>*:</span></td>
                            <td>
                                <span><asp:TextBox TextMode="Password" id="ArrayApiConfirmPasswordTextBox" runat="server" ClientIDMode="Static"/></span>
                            </td>
                        </tr>
                    </table>
                    <div id="expanderElement"><a href="javascript:expanderChangedHandler();"><img src="/Orion/SRM/images/Button.Expand.gif" alt="[+]" /><%= SrmWebContent.Provider_Step_AddProvider_Advanced %></a></div>
                    <table id="AdvancedSmisCredFiledsTable" class="srm-EnrollmentWizard-providerStepInputsTable">
                        <tr>
                            <td class="label"><span><%= SrmWebContent.Add_Provider_Protocol%></span></td>
                            <td>
                                <span><input type="checkbox" id="ProtocolCheckBox" class="ProtocolCheckBox" runat="server" ClientIDMode="Static"/><label for="ProtocolCheckBox"> <%= SrmWebContent.Provider_Step_UseHttp_Label %></label></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"><span><span id="labelSmisHttpPort"><%= SrmWebContent.Add_Provider_Http_Port%></span>*:</span></td>
                            <td>
                                <span><asp:TextBox id="HttpPortTextBox" runat="server" ClientIDMode="Static"/></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"><span><span id="labelSmisHttpsPort"><%= SrmWebContent.Add_Provider_Https_Port%></span>*:</span></td>
                            <td>
                                <span><asp:TextBox id="HttpsPortTextBox" runat="server" ClientIDMode="Static"/></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"><span><span id="labelSmisInteropNamespace"><%= SrmWebContent.Add_Provider_Interop_Namespace%></span>*:</span></td>
                            <td>
                                <span><asp:TextBox id="InteropNamespaceTextBox" runat="server" ClientIDMode="Static"/></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"><span><span id="labelSmisArrayNamespace"><%= SrmWebContent.Add_Provider_Array_Namespace%></span>*:</span></td>
                            <td>
                                <span><asp:TextBox id="ArrayNamespaceTextBox" runat="server" ClientIDMode="Static"/></span>
                            </td>
                        </tr>
                    </table>
                    <table id="AdvancedApiCredFiledsTable" class="srm-EnrollmentWizard-providerStepInputsTable" runat="server" ClientIDMode="Static">
                        <tr runat="server" id="ArrayApiHttpPortRow" >
                            <td class="label"><span><span id="labelApiPort"><%= SrmWebContent.Add_Provider_ArrayAPIHttpPort %></span>*:</span></td>
                            <td>
                                <span><asp:TextBox id="ArrayApiHttpPortTextBox" runat="server" ClientIDMode="Static"/></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"><span><span id="labelApiHttpsPort"><%= SrmWebContent.Add_Provider_ArrayAPIHttpsPort %></span>*:</span></td>
                            <td>
                                <span><asp:TextBox id="ArrayApiHttpsPortTextBox" runat="server" ClientIDMode="Static"/></span>
                            </td>
                        </tr>
                        <tr runat="server" id="apiManagementPathRow">
                            <td class="label"><span><span id="labelApiManagementPath"><%= SrmWebContent.Add_Provider_ArrayAPIManagementSerivcesPath %></span>*:</span></td>
                            <td>
                                <span><asp:TextBox id="ArrayApiManagementServicesPathTextBox" runat="server" ClientIDMode="Static"/></span>
                            </td>
                        </tr>
                        <tr runat="server" id="apiProtocolSchemaRow">
                            <td class="label"><span><%= SrmWebContent.Add_Api_Provider_Protocol%></span></td>
                            <td>
                                <span><input type="checkbox" id="ApiProtocolCheckBox" class="ProtocolCheckBox" runat="server" ClientIDMode="Static"/><label for="ApiProtocolCheckBox"> <%= SrmWebContent.Provider_Step_UseHttp_Label %></label></span>
                            </td>
                        </tr>
                    </table>
                    
                    <table class="PollingPropertiesSection">
                        <tr>
                            <td></td>
                            <td><input type="button" id="testConnectionButton" class="providerStepButton" value="<%= SrmWebContent.Provider_Step_Test_Connection %>" onclick="SW.SRM.AddCelerraProvider.testCredential();" /></td>
                        </tr>
                    </table>
                    <UIInfo:TestConnectionMessages runat="server" ID="TestConnectionMessages" ContainerId="testCelerraProvider" JsCancellationFunction="javascript:SW.SRM.AddCelerraProvider.cancelTestCredential();"/>
                </td>
                 <td class="providerPropertiesColumn">
                     <UIInfo:DeviceType runat="server" ID="DeviceGroupImageWithDescription" />
                </td>
            </tr>
        </table>
    </div>
</div>
