﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json.Linq;
using Resources;
using SolarWinds.Logging;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.EnrollmentWizard;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.Resources;

public partial class Orion_SRM_Config_Controls_XtremIoEdit : UpdateProviderBase, ICreateEditControl, IEditControl
{
    public ProviderEntity XtremIoProvider
    {
        get { return primaryProvider; }
        set { primaryProvider = value; }
    }

    public Orion_SRM_Config_Controls_XtremIoEdit()
    {
        HiddenFieldEditPropertiesID = "hfEditPropertiesXtremIoCfg";
        HiddenFieldProviderGuidID = "hfXtremIoProviderGuid";
    }
   
    protected void Page_Load(object sender, EventArgs e)
    {
        deviceInfo.InitWithDevice(ServiceLocator.GetInstance<IDeviceGroupDAL>().GetDeviceGroupDetailsByArrayId(array.StorageArrayId.Value));
    }


    public IEditControl InitFromNetObjects(List<string> listOfNetObjectIDs)
    {
        // Supports just one NetObject ID and just SMSA
        if (listOfNetObjectIDs.Count() != 1 )
        {
            return null;
        }

        var netObjectId = new NetObjectUniqueIdentifier(listOfNetObjectIDs.First());

        if (netObjectId.Prefix != SolarWinds.SRM.Web.NetObjects.StorageArray.NetObjectPrefix)
        {
            return null;
        }

        array = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayDetails(netObjectId.ID);

        if (array.FlowType != FlowType.XtremIO)
            return null;

        var providers = ServiceLocator.GetInstance<IStorageProviderDAL>().GetAllProvidersByArrayId(array.StorageArrayId);
        XtremIoProvider = providers.FirstOrDefault(p => p.CredentialType == CredentialType.XtremIoHttp);

        if (XtremIoProvider == null)
            return null;

        PollingProperties.ListOfNetObjectIDs = new List<NetObjectUniqueIdentifier>() {netObjectId}; 

        FillControl();
        return this;
    }

    private void FillControl()
    {
        DeviceGroup dDeviceGroup;
        if (base.FillControlForPrimaryProvider(this.hfPlaceholder, hlProvider, this.PollingProperties, out dDeviceGroup))
        {
            txtArrayType.Text = dDeviceGroup.Name;
        }
    }

    public string ValidateJSFunction()
    {
        return "SW.SRM.EditProperties.XtremIoProvider.InitValidation()";
    }

    public bool Validate(bool connectionPropertyChanged)
    {
        return true;
    }

    public KeyValuePair<string, string>[] GetWarnings()
    {
        return this.Warnings.ToArray();
    }

    private void ShowValidationError(string title, string msg)
    {
        String scriptText = string.Format("$( function () {{ SW.SRM.EditProperties.XtremIoProvider.showError('{0}','{1}'); }} );", title, msg);

        Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
           "ShowValidationError", scriptText, true);
    }

    public void Update(bool connectionPropertyChanged)
    {
    }
}
