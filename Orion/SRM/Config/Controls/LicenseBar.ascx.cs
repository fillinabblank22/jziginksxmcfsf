﻿using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.Exceptions;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_SRM_Config_Controls_LicenseBar : System.Web.UI.UserControl
{
    private static readonly Log log = new Log();

    public bool IsExpired
    {
        get;
        set;
    }

    protected bool IsEval
    {
        get;
        set;
    }

    public int Required
    {
        get { return GetValue(hiddenRequired); }
        set { hiddenRequired.Value = value.ToString(CultureInfo.InvariantCulture); }
    }

    public int Total
    {
        get { return GetValue(hiddenTotal); }
        set { hiddenTotal.Value = value.ToString(CultureInfo.InvariantCulture); }
    }

    public int InUse
    {
        get { return GetValue(hiddenInUse); }
        set { hiddenInUse.Value = value.ToString(CultureInfo.InvariantCulture); }
    }

    protected string ErrorMessage
    {
        get { return ErrorMessageLabel.Text; }
        set { ErrorMessageLabel.Text = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        using (var proxy = BusinessLayerFactory.Create())
        {
            try
            {
                LicenseEntity license = proxy.GetLicenseInfo();

                this.Total = license.AllowedDiskCount;
                this.InUse = license.CurrentDiskCount;
                this.IsEval = license.IsEval;

                if (license.IsExpired)
                {
                    log.Warn("License is expired.");

                    this.ErrorMessage = SrmWebContent.AddStorage_ArrayStep_LicenseExpiredExceptionMessage;
                    this.IsExpired = true;
                }
            }
            catch (FaultException<SrmLicenseNotFound> ex)
            {
                log.Error("License is not avialable.", ex);

                this.ErrorMessage = String.Format(CultureInfo.CurrentCulture, "<a href={1} runat=\"server\" rel=\"noopener noreferrer\" target=\"_blank\">{0}</a>", SrmWebContent.LicensePage_GenericLicenseFailure, HelpHelper.GetHelpUrl("SRMPHLicenseSummaryGenericLicenseFailure"));
                this.IsExpired = true;
            }
            catch (Exception ex)
            {
                log.Error("Problem with licensing.", ex);

                this.ErrorMessage = String.Format(CultureInfo.CurrentCulture, "<a href={1} runat=\"server\" rel=\"noopener noreferrer\" target=\"_blank\">{0}</a>", SrmWebContent.LicensePage_GenericLicenseFailure, HelpHelper.GetHelpUrl("SRMPHLicenseSummaryGenericLicenseFailure"));
                this.IsExpired = true;
            }
        }

        //Set default error message.
        if (String.IsNullOrEmpty(this.ErrorMessage))
        {
            var alreadyExceeded = String.Format(CultureInfo.InvariantCulture,
                                                SrmWebContent.SRMEnrollmentWizard_LicenseBar_NoExternalProviderLicenseExceeded,
                                                HelpHelper.GetHelpUrl("SRMAG_LicenseCapacity"));

            if (this.Total - this.InUse < 0 // already using more licensed elements than it's available
                || this.Total == 0) // Voltron special case, all license slots occupied by e.g. nodes 
            {
                this.ErrorMessage = alreadyExceeded;
            }
            else
            {
                this.ErrorMessage = SrmWebContent.SRMEnrollmentWizard_LicenseBar_LicenseExceeded;
            }
        }
    }

    public bool Validation()
    {
        return !this.IsExpired && GetValue(hiddenLicViolation) == 0;
    }

    private int GetValue(HiddenField field)
    {
        int value;
        if (!int.TryParse(field.Value, out value))
        {
            value = 0;
        }

        return value;
    }
}