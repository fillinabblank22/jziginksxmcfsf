﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProviderStep.ascx.cs" Inherits="Orion_SRM_Config_Controls_ProviderStep" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web" %>

<%@ Reference Control="~/Orion/SRM/Config/Controls/AddSmisProvider.ascx" %>
<%@ Reference Control="~/Orion/SRM/Config/Controls/AddDfmProvider.ascx" %>
<%@ Reference Control="~/Orion/SRM/Config/Controls/AddXtremIoProvider.ascx" %>
<%@ Reference Control="~/Orion/SRM/Config/Controls/AddSmisAndGenericHttpProvider.ascx" %>

<orion:Include ID="Include1" runat="server" File="OrionCore.js" />
<orion:Include ID="Include2" runat="server" File="ProviderStep.js" Module="SRM" />
<orion:Include ID="Include3" runat="server" File="SRM.Formatters.js" Module="SRM" />

<script type="text/javascript">
    var swisEntityProviders = "<%=SwisEntities.Providers%>";
</script>

<div>
    <asp:Label runat="server" ID="providerSubTitle" />
    <span><a target="_blank" rel="noopener noreferrer" id="familyTypeLink" runat="server"><%= Group.LinkName%></a></span>
</div>
<div class="srm-enrollment-wizard-content">
    <table>
        <tr>
            <td><span><%= SrmWebContent.Provider_Step_Array_Type%>:</span></td>
            <td><b>
                <asp:Label runat="server" ID="ArrayTypeLabel" />
            </b></td>
        </tr>
        <tr>
            <td><span><%= SrmWebContent.Provider_Step_Choose_Providers%>:</span></td>
            <td>
                <div class="x-toolbar">
                    <img src="/Orion/SRM/images/u412.png" id="addNewProviderBtn" />
                    <span id="addNewProviderLabel"><%=this.Provider_Step_Add_Btn_Label %></span>
                </div>
                <div id="gridPanel">
                    <div id="Grid" />
                </div>
            </td>
        </tr>
    </table>

    <div id="AddProviderFrame" class="x-hidden">
        <div class="x-window-header"><%= this.Provider_Step_Add_Provider_Title %></div>
        <div id="AddProviderBody" class="x-panel-body dialogBody" runat="server" clientidmode="Static">
        </div>
    </div>

    <asp:HiddenField ID="SelectedProviderID" runat="server" ClientIDMode="Static" />
</div>
