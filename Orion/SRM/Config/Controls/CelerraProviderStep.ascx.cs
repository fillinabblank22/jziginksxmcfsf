﻿using System;
using System.Linq;
using System.Web.UI;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SRM.Common.Credentials;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.EnrollmentWizard;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Common.BL;
using System.Collections.Generic;
using SolarWinds.SRM.Common;
using System.Web.UI.WebControls;
using System.Globalization;
using Resources;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.Resources;

public partial class Orion_SRM_Config_Controls_CelerraProviderStep : System.Web.UI.UserControl, IConfigurationControl, ICreateEditControl, IEditControl
{
    private static readonly IDeviceGroupDAL DeviceGroupDal = ServiceLocator.GetInstance<IDeviceGroupDAL>();
    private readonly DeviceGroup celleraDeviceGroup = DeviceGroupDal.GetDeviceGroupByFlowType(FlowType.SmisVnx);

    private readonly List<KeyValuePair<string, string>> warnings = new List<KeyValuePair<string, string>>();

    // keys to group's properties
    private const string SmisCredentialsTooltipKey = "SmisCredentialsTooltip";
    private const string ApiCredentialsTooltipKey = "ApiCredentialsTooltip";
    private const string ChooseDataSourceSubtitleKey = "ChooseDataSourceSubtitle";

    private Configuration configuration
    {
        get
        {
            return WizardWorkflowHelper.ConfigurationInfo;
        }
    }
    private ArrayEntity array;

    protected string ArrayID
    {
        get
        {
            return array == null ? string.Empty : array.ID;
        }
    }

    protected string ArraySerialNumber
    {
        get
        {
            return array == null ? string.Empty : array.SerialNumber;
        }
    }

    protected Guid DeviceGroupId
    {
        get
        {
            return (array != null) && (array.FlowType == FlowType.SmisEmcUnified) ? celleraDeviceGroup.GroupId : configuration.DeviceGroup.GroupId;
        }
    }

    private string SelectedSmisCredentials { get; set; }
    private string SelectedVnxCredentials { get; set; }
    private List<ProviderEntity> providers;

    private bool IsCelerra
    {
        get
        {
            return array != null && array.FlowType == FlowType.SmisVnx;
        }
    }

    public string SubTitle { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            labelIpAddress.Text = SrmWebContent.Add_Array_IP_Address_Hostname;
            labelSmisDisplayName.Text = SrmWebContent.Add_Provider_DisplayName;
            labelSmisProviderUserName.Text = SrmWebContent.Provider_Step_Celerra_SMISUsername;
            labelSmisProviderPassword.Text = SrmWebContent.Provider_Step_Celerra_SMISPassword;
            labelApiDisplayNameTextBox.Text = SrmWebContent.Add_Provider_DisplayName;
            labelApiProviderUserName.Text = SrmWebContent.Add_Provider_ArrayAPIUsername;
            labelApiProviderPassword.Text = SrmWebContent.Add_Provider_ArrayAPIPassword;

            HttpsPortTextBox.Text = SRMConstants.DefaultSmisHttpsPort.ToString();
            HttpPortTextBox.Text = SRMConstants.DefaultSmisHttpPort.ToString();
            ApiProtocolCheckBox.Checked = true;

            if (configuration.DeviceGroup.Properties.ContainsKey(SmisCredentialsTooltipKey))
            {
                SmisCredentialsTooltip.Visible = true;
                SmisCredentialsTooltip.ToolTip = configuration.DeviceGroup.Properties[SmisCredentialsTooltipKey];
            }

            if (configuration.DeviceGroup.Properties.ContainsKey(ApiCredentialsTooltipKey))
            {
                ApiCredentialsTooltip.Visible = true;
                ApiCredentialsTooltip.ToolTip = configuration.DeviceGroup.Properties[ApiCredentialsTooltipKey];
            }

            FillCredentials(this.ChooseCredentialSmis, CredentialType.SMIS);

            if (configuration.DeviceGroup.FlowType == FlowType.SmisMsa || configuration.DeviceGroup.FlowType == FlowType.SmisPureStorage) 
            {
                // MSA + DotHill + Pure Storage specific
                apiManagementPathRow.Visible = false;
                apiProtocolSchemaRow.Visible = true;
                InteropNamespaceTextBox.Text = configuration.DeviceGroup.Properties[SRMConstants.DefaultInteropNamespacePropertyName];
                ArrayNamespaceTextBox.Text = configuration.DeviceGroup.Properties[SRMConstants.DefaultArrayNamespacePropertyName];
                ArrayTypeName.InnerText = configuration.DeviceGroup.Name;
                SubTitle = string.Empty;
                if (configuration.DeviceGroup.Properties.ContainsKey(ChooseDataSourceSubtitleKey))
                {
                    SubTitle = configuration.DeviceGroup.Properties[ChooseDataSourceSubtitleKey];
                }
                DeviceGroupImageWithDescription.InitWithDevice(configuration.DeviceGroup);
                
                if (configuration.DeviceGroup.FlowType == FlowType.SmisMsa)
                {
                    ArrayApiHttpPortTextBox.Text = SRMConstants.DefaultHttpPort.ToString();
                    ArrayApiHttpsPortTextBox.Text = SRMConstants.DefaultHttpsPort.ToString();
                    FillCredentials(this.ChooseCredentialVnx, CredentialType.MSA);
                }
                else if (configuration.DeviceGroup.FlowType == FlowType.SmisPureStorage)
                {
                    this.ApiProtocolCheckBox.Checked = false;
                    ArrayApiHttpPortTextBox.Text = SRMConstants.DefaultHttpPort.ToString();
                    ArrayApiHttpsPortTextBox.Text = SRMConstants.DefaultHttpsPort.ToString();
                    FillCredentials(this.ChooseCredentialVnx, CredentialType.PureStorageHttp);
                }
            }
            else
            {
                // VNX
                ArrayApiManagementServicesPathTextBox.Text = SRMConstants.ManagementServicesPath;
                InteropNamespaceTextBox.Text = SRMConstants.InteropNamespace;
                ArrayNamespaceTextBox.Text = WebSettingsDAL.GetValue<string>(SRMConstants.CelerraArrayNamespaceSettingID, String.Empty);
                ArrayApiHttpPortRow.Style.Add("display", "none");
                ArrayApiHttpPortTextBox.Text = SRMConstants.DefaultHttpPort.ToString();
                ArrayApiHttpsPortTextBox.Text = SRMConstants.DefaultHttpsPort.ToString();
                ApiProtocolCheckBox.Checked = false;
                apiProtocolSchemaRow.Visible = false;
                ArrayTypeName.InnerText = celleraDeviceGroup.Name;
                SubTitle = SrmWebContent.AddStorage_ProviderStep_Celerra_SubTitle;
                DeviceGroupImageWithDescription.InitWithDevice(celleraDeviceGroup);
                FillCredentials(this.ChooseCredentialVnx, CredentialType.VNX);
            }


            if (configuration.DeviceGroup.FlowType == FlowType.SmisEmcUnified)
            {
                DeviceGroupImageWithDescription.AdaptToUnifiedDevice(configuration.DeviceGroup.Name, SrmWebContent.EMC_VNX_for_File_Description, SrmWebContent.EMC_VNX_for_File_LinkText);
                ArrayTypeName.InnerText = SrmWebContent.EMC_VNX_for_File_Name;
                SubTitle = SrmWebContent.AddStorage_ProviderStep_EMC_SubTitle;
            }
        }
    }

    public void SaveValues()
    {
        SaveSmisProvider(this.IpAddressOrHostnameTextBox.Text.Trim(),
                                this.DisplayNameTextBox.Text,
                                this.ProviderUsernameTextBox.Text,
                                this.ProviderPasswordTextBox.Text,
                                this.ProtocolCheckBox.Checked,
                                Int32.Parse(this.HttpPortTextBox.Text),
                                Int32.Parse(this.HttpsPortTextBox.Text),
                                this.InteropNamespaceTextBox.Text,
                                this.ArrayNamespaceTextBox.Text,
                                this.ChooseCredentialSmis.SelectedValue);

        SaveVnxProvider(this.IpAddressOrHostnameTextBox.Text.Trim(),
                                this.DisplayNameVnxTextBox.Text,
                                this.ArrayApiUsernameTextBox.Text,
                                this.ArrayApiPasswordTextBox.Text,
                                Int32.Parse(this.ArrayApiHttpPortTextBox.Text),
                                Int32.Parse(this.ArrayApiHttpsPortTextBox.Text),
                                this.ArrayApiManagementServicesPathTextBox.Text,
                                !this.ApiProtocolCheckBox.Checked,
                                this.ChooseCredentialVnx.SelectedValue);
    }

    private bool HandleValidationError(string message)
    {
        this.ValidationError = message;
        this.ClearSelectedProviders();
        return false;
    }

    public string ValidationError { get; private set; }
    public bool ValidateUserInput()
    {
        if (UseSameCredentials.Checked)
        {
            ArrayApiUsernameTextBox.Text = ProviderUsernameTextBox.Text;
            ArrayApiPasswordTextBox.Text = ProviderPasswordTextBox.Text;
            ArrayApiConfirmPasswordTextBox.Text = ProviderConfirmPasswordTextBox.Text;
        }

        if (String.IsNullOrEmpty(IpAddressOrHostnameTextBox.Text))
        {
            return HandleValidationError(String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, labelIpAddress.Text));
        }
        else if (ChooseCredentialSmis.SelectedIndex == 0 && String.IsNullOrEmpty(DisplayNameTextBox.Text))
        {
            return HandleValidationError(String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, labelSmisDisplayName.Text));
        }
        else if (ChooseCredentialSmis.SelectedIndex == 0 && String.IsNullOrEmpty(ProviderUsernameTextBox.Text))
        {
            return HandleValidationError(String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, labelSmisProviderUserName.Text));
        }
        else if (ChooseCredentialSmis.SelectedIndex == 0 && String.IsNullOrEmpty(ProviderPasswordTextBox.Text))
        {
            return HandleValidationError(String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, labelSmisProviderPassword.Text));
        }
        else if (ChooseCredentialSmis.SelectedIndex == 0 && ProviderPasswordTextBox.Text != ProviderConfirmPasswordTextBox.Text)
        {
            return HandleValidationError(SrmWebContent.Add_Provider_ConfirmPassword_CompareField_Validator);
        }
        else if (ChooseCredentialVnx.SelectedIndex == 0 && String.IsNullOrEmpty(DisplayNameVnxTextBox.Text))
        {
            return HandleValidationError(String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, labelApiDisplayNameTextBox.Text));
        }
        else if (ChooseCredentialVnx.SelectedIndex == 0 && String.IsNullOrEmpty(ArrayApiUsernameTextBox.Text))
        {
            return HandleValidationError(String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, labelApiProviderUserName.Text));
        }
        else if (ChooseCredentialVnx.SelectedIndex == 0 && String.IsNullOrEmpty(ArrayApiPasswordTextBox.Text))
        {
            return HandleValidationError(String.Format(CultureInfo.CurrentCulture, SrmWebContent.Add_Provider_Required_Field_Validator, labelApiProviderPassword.Text));
        }
        else if (ChooseCredentialVnx.SelectedIndex == 0 && ArrayApiPasswordTextBox.Text != ArrayApiConfirmPasswordTextBox.Text)
        {
            return HandleValidationError(SrmWebContent.Add_Provider_ConfirmApiPassword_CompareField_Validator);
        }

        if (ChooseCredentialSmis.SelectedIndex == 0 && this.ChooseCredentialSmis.Items.Cast<ListItem>().Any(item => this.DisplayNameTextBox.Text.Equals(item.Text, StringComparison.InvariantCultureIgnoreCase)))
        {
            return HandleValidationError(SrmWebContent.Provider_Step_Celerra_Validation_DuplicateSMISCredentials);
        }

        if (ChooseCredentialVnx.SelectedIndex == 0 && this.ChooseCredentialVnx.Items.Cast<ListItem>().Any(item => this.DisplayNameVnxTextBox.Text.Equals(item.Text, StringComparison.InvariantCultureIgnoreCase)))
        {
            return HandleValidationError(SrmWebContent.Provider_Step_Celerra_Validation_DuplicateAPICredentials);
        }

        return true;
    }

    public KeyValuePair<string, string>[] GetWarnings()
    {
        return this.warnings.ToArray();
    }

    private void FillCredentials(DropDownList dropDownList, CredentialType type)
    {
        var selectedValue = type == CredentialType.SMIS ? SelectedSmisCredentials : SelectedVnxCredentials;

        var items = new List<KeyValuePair<String, String>>() { new KeyValuePair<String, String>(null, Resources.SrmWebContent.Provider_Step_New_Credential) };
        using (var proxy = BusinessLayerFactory.Create())
        {
            IDictionary<int, string> credentialsList = proxy.GetCredentialNames(type);
            foreach (KeyValuePair<int, string> cred in credentialsList)
            {
                items.Add(new KeyValuePair<String, String>(cred.Key.ToString(CultureInfo.InvariantCulture), cred.Value));
            }
        }

        dropDownList.DataTextField = "Value";
        dropDownList.DataValueField = "Key";
        dropDownList.DataSource = items;

        if (items.Any(pair => pair.Key == selectedValue))
        {
            dropDownList.SelectedValue = selectedValue;
        }
        else
        {
            // In case two Block & File SMIS providers exist display particular warning for each
            var smisBlockProviderExists = this.providers.Any(p => p.ProviderType == ProviderTypeEnum.Block && p.CredentialType == CredentialType.SMIS && p.ProviderLocation == ProviderLocation.External);

            //Display warning about missing credentials
            this.warnings.Add(new KeyValuePair<string, string>("", String.Format(SrmWebContent.Credentials_Missing_Format, type == CredentialType.SMIS 
                ? (smisBlockProviderExists ? SrmWebContent.Credentials_SMIS_FileStorage : SrmWebContent.Provider_Step_Celerra_SMISCredentials)
                : SrmWebContent.Provider_Step_Celerra_APICredentials)
            ));
        }

        dropDownList.DataBind(); 
    }

    private void SaveSmisProvider(string ipAddress,
                                string displayName,
                                string userName,
                                string password,
                                bool useHttp,
                                int httpPort,
                                int httpsPort,
                                string interopNamespace,
                                string arrayNamespace,
                                string credentialIDstring)
    {
        Credential credential;
        ProviderTypeEnum providerType = ProviderTypeEnum.File;

        if (configuration.DeviceGroup.FlowType == FlowType.SmisMsa || configuration.DeviceGroup.FlowType == FlowType.SmisPureStorage)
        {
            providerType = ProviderTypeEnum.Block;
        }

        int credentialID;
        if (!int.TryParse(credentialIDstring, out credentialID))
            credentialID = 0;

        if (credentialID <= 0)
        {
            credential = new SmisCredentials()
            {
                Name = displayName,
                Username = userName,
                Password = password,
                HttpPort = (UInt32)httpPort,
                HttpsPort = (UInt32)httpsPort,
                UseSSL = !useHttp,
                InteropNamespace = interopNamespace,
                Namespace = arrayNamespace
            };

            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(credential, CredentialType.SMIS);
                credential.ID = id;
            }
        }
        else
        {
            using (var proxy = BusinessLayerFactory.Create())
            {
                credential = proxy.GetCredential(credentialID, CredentialType.SMIS);
            }
        }

        var providerEntity = new EnrollmentWizardProvider()
        {
            IPAddress = ipAddress,
            CredentialType = CredentialType.SMIS,
            ProviderType = providerType,
            ProviderLocation = ProviderLocation.Onboard,
            Credential = credential,
            CredentialID = credential.ID.Value
        };

        UpdateProviders(providerEntity);
    }

    public void SaveVnxProvider(string ipAddress,
                                string displayName,
                                string userName,
                                string password,
                                int httpPort,
                                int httpsPort,
                                string managementSerivcesPath,
                                bool schemaIsHttps,
                                string credentialIDstring)
    {
        Credential credential;
        var credentialTypeCredential = CredentialType.VNX;
        var credentialTypeProvider = CredentialType.VNX;
        var providerType = ProviderTypeEnum.File;

        if (configuration.DeviceGroup.FlowType == FlowType.SmisMsa)
        {
            credentialTypeCredential = CredentialType.MSA;
            credentialTypeProvider = CredentialType.MSA;
            providerType = ProviderTypeEnum.Block;
        }
        else if (configuration.DeviceGroup.FlowType == FlowType.SmisPureStorage)
        {
            credentialTypeCredential = CredentialType.PureStorageHttp;
            credentialTypeProvider = CredentialType.PureStorageHttp;
            providerType = ProviderTypeEnum.Block;
        }

        int credentialID;
        if (!int.TryParse(credentialIDstring, out credentialID))
            credentialID = 0;

        if (credentialID <= 0)
        {
            if (configuration.DeviceGroup.FlowType == FlowType.SmisMsa)
            {
                credential = new MsaCredential()
                {
                    Name = displayName,
                    Username = userName,
                    Password = password,
                    HttpPort = httpPort,
                    HttpsPort = httpsPort,
                    UseSsl = schemaIsHttps
                };
            }
            else if (configuration.DeviceGroup.FlowType == FlowType.SmisPureStorage)
            {
                credential = new PureStorageHttpCredential()
                {
                    Name = displayName,
                    Username = userName,
                    Password = password,
                    HttpPort = httpPort,
                    HttpsPort = httpsPort,
                    UseSsl = schemaIsHttps
                };
            }
            else
            {
                credential = new VnxCredential()
                {
                    Name = displayName,
                    Username = userName,
                    Password = password,
                    Port = httpsPort,
                    PathToManagementServices = managementSerivcesPath
                };
            }
            using (var proxy = BusinessLayerFactory.Create())
            {
                int id = proxy.AddCredential(credential, credentialTypeCredential);
                credential.ID = id;
            }
        }
        else
        {
            using (var proxy = BusinessLayerFactory.Create())
            {
                credential = proxy.GetCredential(credentialID, credentialTypeCredential);
            }
        }

        var providerEntity = new EnrollmentWizardProvider()
        {
            IPAddress = ipAddress,
            CredentialType = credentialTypeProvider,
            ProviderType = providerType,
            ProviderLocation = ProviderLocation.Onboard,
            Credential = credential,
            CredentialID = credential.ID.Value,
            ConnectionTimeout = TimeSpan.FromSeconds(20)
        };

        UpdateProviders(providerEntity);
    }

    private void UpdateProviders(EnrollmentWizardProvider newProvider)
    {
        configuration.ProviderConfiguration.SelectedProviders.RemoveAll(p => p.CredentialType == newProvider.CredentialType && p.ProviderType == newProvider.ProviderType);
        configuration.ProviderConfiguration.SelectedProviders.Add(newProvider);
    }

    public IEditControl InitFromNetObjects(List<string> listOfNetObjectIDs)
    {
        // Supports just one NetObject ID and just SMSA
        var supported = listOfNetObjectIDs.Count() == 1 && listOfNetObjectIDs[0].Split(':')[0] == StorageArray.NetObjectPrefix;
        if (!supported)
        {
            return null;
        }

        var arrayID = int.Parse(listOfNetObjectIDs[0].Split(':')[1]);
        array = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayDetails(arrayID);

        // Supporting just EmcCelerra
        if (array.FlowType != FlowType.SmisEmcUnified && array.FlowType != FlowType.SmisVnx && array.FlowType != FlowType.SmisMsa && array.FlowType != FlowType.SmisPureStorage)
        {
            return null;
        }

        FillControl();

        return this;
    }

    private void FillControl()
    {
        var msaFlowDeviceGroups = ServiceLocator.GetInstance<IDeviceGroupDAL>().GetDeviceGroups().Where(d => d.FlowType == FlowType.SmisMsa).Select(d => d.GroupId);
        var pureStorageFlowDeviceGroups = ServiceLocator.GetInstance<IDeviceGroupDAL>().GetDeviceGroups().Where(d => d.FlowType == FlowType.SmisPureStorage).Select(d => d.GroupId);

        this.providers = ServiceLocator.GetInstance<IStorageProviderDAL>().GetAllProvidersByArrayId(array.StorageArrayId);

        var smisProvider = this.providers.FirstOrDefault(p => p.CredentialType == CredentialType.SMIS && p.ProviderType == ProviderTypeEnum.File)
            ?? this.providers.FirstOrDefault(p => msaFlowDeviceGroups.Contains(p.DeviceGroupID) && p.CredentialType == CredentialType.SMIS && p.ProviderType == ProviderTypeEnum.Block)
            ?? this.providers.FirstOrDefault(p => pureStorageFlowDeviceGroups.Contains(p.DeviceGroupID) && p.CredentialType == CredentialType.SMIS && p.ProviderType == ProviderTypeEnum.Block);
        var vnxProvider = this.providers.FirstOrDefault(p => p.CredentialType == CredentialType.VNX && p.ProviderType == ProviderTypeEnum.File);
        var msaProvider = this.providers.FirstOrDefault(p => p.CredentialType == CredentialType.MSA && p.ProviderType == ProviderTypeEnum.Block);
        var pureStorageProvider = this.providers.FirstOrDefault(p => p.CredentialType == CredentialType.PureStorageHttp && p.ProviderType == ProviderTypeEnum.Block);

        this.hfPlaceholder.Controls.Add(new HiddenField()
        {
            ClientIDMode = ClientIDMode.Static,
            ID = "hfEditPropertiesVNXFileOrUnifiedTemplateID"
        });

        if (smisProvider != null)
        {
            IpAddressOrHostnameTextBox.Text = smisProvider.IPAddress;

            SelectedSmisCredentials = smisProvider.CredentialID.ToString();

            SmisCredentials credentials;
            using (var proxy = BusinessLayerFactory.Create())
            {
                credentials = (SmisCredentials)proxy.GetCredential(smisProvider.CredentialID, CredentialType.SMIS);
            }

            if (credentials != null)
            {
                smisProvider.Credential = credentials;

                this.ProtocolCheckBox.Checked = !credentials.UseSSL;
                this.InteropNamespaceTextBox.Text = credentials.InteropNamespace;
                this.ArrayNamespaceTextBox.Text = credentials.Namespace;
                var portTextbox = credentials.UseSSL ? this.HttpsPortTextBox : this.HttpPortTextBox;
                portTextbox.Text = credentials.Port.ToString(CultureInfo.InvariantCulture);
            }
        }

        if (vnxProvider != null)
        {
            IpAddressOrHostnameTextBox.Text = IpAddressOrHostnameTextBox.Text ?? vnxProvider.IPAddress;
            SelectedVnxCredentials = vnxProvider.CredentialID.ToString();

            VnxCredential credentials;
            using (var proxy = BusinessLayerFactory.Create())
            {
                credentials = (VnxCredential)proxy.GetCredential(vnxProvider.CredentialID, CredentialType.VNX);
            }

            if (credentials != null)
            {
                vnxProvider.Credential = credentials;
                this.ArrayApiHttpPortTextBox.Text = credentials.HttpPort.ToString(CultureInfo.InvariantCulture);
                this.ArrayApiHttpsPortTextBox.Text = credentials.HttpsPort.ToString(CultureInfo.InvariantCulture);
                this.ArrayApiManagementServicesPathTextBox.Text = credentials.PathToManagementServices;
            }
        }

        if (msaProvider != null)
        {
            IpAddressOrHostnameTextBox.Text = IpAddressOrHostnameTextBox.Text ?? msaProvider.IPAddress;
            SelectedVnxCredentials = msaProvider.CredentialID.ToString();

            MsaCredential credentials;
            using (var proxy = BusinessLayerFactory.Create())
            {
                credentials = (MsaCredential)proxy.GetCredential(msaProvider.CredentialID, CredentialType.MSA);
            }

            if (credentials != null)
            {
                msaProvider.Credential = credentials;
                this.ArrayApiHttpPortTextBox.Text = credentials.HttpPort.ToString(CultureInfo.InvariantCulture);
                this.ArrayApiHttpsPortTextBox.Text = credentials.HttpsPort.ToString(CultureInfo.InvariantCulture);
                this.ApiProtocolCheckBox.Checked = !credentials.UseSsl;
            }
        }

        if (pureStorageProvider != null)
        {
            IpAddressOrHostnameTextBox.Text = IpAddressOrHostnameTextBox.Text ?? pureStorageProvider.IPAddress;
            SelectedVnxCredentials = pureStorageProvider.CredentialID.ToString();

            PureStorageHttpCredential credentials;
            using (var proxy = BusinessLayerFactory.Create())
            {
                credentials = (PureStorageHttpCredential)proxy.GetCredential(pureStorageProvider.CredentialID, CredentialType.PureStorageHttp);
            }

            if (credentials != null)
            {
                pureStorageProvider.Credential = credentials;
                this.ArrayApiHttpPortTextBox.Text = credentials.HttpPort.ToString(CultureInfo.InvariantCulture);
                this.ArrayApiHttpsPortTextBox.Text = credentials.HttpsPort.ToString(CultureInfo.InvariantCulture);
                this.ApiProtocolCheckBox.Checked = !credentials.UseSsl;
            }
        }

        RefillConfiguration();
    }

    private void RefillConfiguration()
    {
        var engine = ServiceLocator.GetInstance<IEngineDAL>().GetEngine(array.EngineID);

        configuration.DeviceGroup = DeviceGroupDal.GetDeviceGroupDetailsByArrayId(array.StorageArrayId.Value);
        configuration.PollingConfig.BusinessLayerPort = engine.BusinessLayerPort;
        configuration.PollingConfig.EngineID = engine.EngineID;
        configuration.PollingConfig.ServerName = engine.ServerName;
        configuration.ProviderConfiguration.AvailableProviderEntities = null;
        if (!IsPostBack)
        {
            ClearSelectedProviders();
        }
    }

    private void ClearSelectedProviders()
    {
        configuration.ProviderConfiguration.SelectedProviders.RemoveAll(a => true);
    }

    public string ValidateJSFunction()
    {
        return "SW.SRM.AddCelerraProvider.InitValidation();";
    }

    public bool Validate(bool connectionPropertyChanged)
    {
        //We want to validate this if
        //  a) we're editing Celerra device as itself (not as part of Clariion)
        //  b) there was some Celerra providers info defined before 
        //  c) any of the Celerra required fields is filled
        Func<ProviderEntity, bool> smisFileProvider = x => x.CredentialType == CredentialType.SMIS && x.ProviderType == ProviderTypeEnum.File;
        
        var shouldValidate = IsCelerra || this.providers.Any(smisFileProvider) || !AllFieldsEmpty();

        //If we don't want to validate this settings just skip the validation -> return true
        if (!shouldValidate)
        {
            return true;
        }

        return this.ValidateUserInput();
    }

    private bool AllFieldsEmpty()
    {
        var listOfTextboxes = new List<TextBox>()
                                  {
                                      this.IpAddressOrHostnameTextBox,
                                      this.DisplayNameTextBox,
                                      this.ProviderUsernameTextBox,
                                      this.ProviderPasswordTextBox,
                                      this.ProviderConfirmPasswordTextBox,
                                      this.DisplayNameVnxTextBox,
                                      this.ArrayApiUsernameTextBox,
                                      this.ArrayApiPasswordTextBox,
                                      this.ArrayApiConfirmPasswordTextBox
                                  };

        var listOfDropDowns = new List<DropDownList>() { this.ChooseCredentialSmis, this.ChooseCredentialVnx };

        return listOfTextboxes.All(tb => string.IsNullOrEmpty(tb.Text))
               && listOfDropDowns.All(dd => dd.SelectedIndex == 0);
    }

    public void Update(bool connectionPropertyChanged)
    {
        //if we edit Clariion the update is done in the SmisProvider.ascx.cs
        if (array.FlowType == FlowType.SmisVnx || array.FlowType == FlowType.SmisEmcUnified)
        {
            return;
        }

        SaveValues();

        var toDiscovery = new List<SelectedArrayEntity>
                              {
                                  new SelectedArrayEntity(array)
                                    {
                                        IsSelected = true,
                                        StorageArrayId = this.array.StorageArrayId
                                    }
                              };

        using (var proxy = BusinessLayerFactory.Create())
        {
            proxy.DiscoveryImport(array.EngineID, ProviderHelper.GetProviderInfoOfWizardProviders(this.providers, configuration.ProviderConfiguration.SelectedProviders), toDiscovery);
        }
    }
}
