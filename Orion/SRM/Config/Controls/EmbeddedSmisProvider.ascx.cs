﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using Resources;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.EnrollmentWizard;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.Resources;

public partial class Orion_SRM_Config_Controls_EmbeddedSmisProvider : UserControl, IConfigurationControl, ICreateEditControl, IEditControl
{
    private ProviderInfo providerFrom;
    private ArrayEntity array;
    private List<ProviderEntity> providers;
    private ProviderEntity provider;
    private readonly List<KeyValuePair<string, string>> warnings = new List<KeyValuePair<string, string>>();
    protected string SubTitle { get; set; }

    protected string ArrayID
    {
        get
        {
            return array == null ? string.Empty : array.ID;
        }
    }

    protected string ArraySerialNumber
    {
        get
        {
            return array == null ? string.Empty : array.SerialNumber;
        }
    }

    private Configuration configuration
    {
        get
        {
            return WizardWorkflowHelper.ConfigurationInfo;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IDictionary<int, string> existingCredentials;
            using (var proxy = BusinessLayerFactory.Create())
            {
                existingCredentials = proxy.GetCredentialNames(CredentialType.SMIS);
            }

            var credentialDisplayNames = new Dictionary<string, string>()
            {
                {string.Empty, Resources.SrmWebContent.Provider_Step_New_Credential}
            };

            foreach (var credential in existingCredentials)
            {
                credentialDisplayNames.Add(credential.Key.ToString(CultureInfo.InvariantCulture), credential.Value);
            }
            if (!this.ProviderControl.FillCredentialsList(credentialDisplayNames, provider == null ? 0 : provider.CredentialID))
            {
                this.warnings.Add(new KeyValuePair<string, string>("", SrmWebContent.Credentials_Missing));
            }
        }

        this.ProviderControl.JsCancellationFunction = "javascript:SW.SRM.EmbeddedSmisProvider.cancelTestCredential();";

        SubTitle = string.Format(
                            CultureInfo.CurrentCulture,
                            Resources.SrmWebContent.AddStorage_ProviderStep_EmbeddedSmis_SubTitle,
                            WizardWorkflowHelper.ConfigurationInfo.DeviceGroup.Name);
    }

    public string ValidationError { get; private set; }

    public void SaveValues()
    {
        var providerFromForm = ProviderControl.GetProviderInfo();

        WizardWorkflowHelper.ConfigurationInfo.ProviderConfiguration.SelectedProviders.RemoveAll(p => true);
        WizardWorkflowHelper.ConfigurationInfo.ProviderConfiguration.SelectedProviders.Add(
	        new EnrollmentWizardProvider()
            {
                IPAddress = providerFromForm.IPAddress,
                HostName = providerFromForm.HostName,
                CredentialType = CredentialType.SMIS,
                ProviderType = this.configuration.DeviceGroup.ProviderTypeOrBlock,
                DeviceGroupID = configuration.DeviceGroup.GroupId,
                TemplateID = provider == null ? Guid.Empty : provider.TemplateID,
                ProviderLocation = ProviderLocation.Onboard,
                Credential = providerFromForm.Credential,
                CredentialID = providerFromForm.Credential.ID.Value,
                ConnectionTimeout = TimeSpan.FromSeconds(20)
            });
    }

    public bool ValidateUserInput()
    {
        ValidationError = this.ProviderControl.GetUserInputValidationErrors().FirstOrDefault(e => !string.IsNullOrEmpty(e));

        return string.IsNullOrEmpty(ValidationError);
    }

    public IEditControl InitFromNetObjects(List<string> listOfNetObjectIDs)
    {
        NetObjectUniqueIdentifier netObject;
        // Supports just one NetObject ID and just SMSA
        if (listOfNetObjectIDs.Count() != 1
            || (netObject = new NetObjectUniqueIdentifier(listOfNetObjectIDs.First())).ID <= 0
            || !netObject.Prefix.Equals(StorageArray.NetObjectPrefix, StringComparison.InvariantCultureIgnoreCase))
        {
            return null;
        }

        array = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayDetails(netObject.ID);

        this.RefillConfiguration();

        // Supporting just Embedded SMI-S providers
        if (configuration.DeviceGroup.FlowType != FlowType.Smis || this.configuration.DeviceGroup.ProviderLocationOrOnboard != ProviderLocation.Onboard)
        {
            return null;
        }

        FillControl();

        return this;
    }

    private void FillControl()
    {
        providers = ServiceLocator.GetInstance<IStorageProviderDAL>().GetAllProvidersByArrayId(array.StorageArrayId);

        if (providers.Count != 1)
        {
            return;
        }

        provider = providers.First();

        this.ProviderControl.IPAddressOrHostName = string.IsNullOrEmpty(provider.HostName) ? provider.IPAddress : provider.HostName;
    }

    private void RefillConfiguration()
    {
        var engine = ServiceLocator.GetInstance<IEngineDAL>().GetEngine(array.EngineID);

        configuration.DeviceGroup = ServiceLocator.GetInstance<IDeviceGroupDAL>().GetDeviceGroupDetailsByArrayId(array.StorageArrayId.Value);
        configuration.PollingConfig.BusinessLayerPort = engine.BusinessLayerPort;
        configuration.PollingConfig.EngineID = engine.EngineID;
        configuration.PollingConfig.ServerName = engine.ServerName;
        configuration.ProviderConfiguration.AvailableProviderEntities = null;
        if (!IsPostBack)
        {
            configuration.ProviderConfiguration.SelectedProviders.RemoveAll(a => true);
        }
    }

    public string ValidateJSFunction()
    {
        return "SW.SRM.EmbeddedSmisProvider.InitValidation()";
    }

    public bool Validate(bool connectionPropertyChanged)
    {
        return this.ValidateUserInput();
    }

    public KeyValuePair<string, string>[] GetWarnings()
    {
        return this.warnings.ToArray();
    }

    public void Update(bool connectionPropertyChanged)
    {
        SaveValues();

        var toDiscovery = new List<SelectedArrayEntity>
                              {
                                  new SelectedArrayEntity(array)
                                    {
                                        IsSelected = true,
                                        StorageArrayId = this.array.StorageArrayId
                                    }
                              };

        using (var proxy = BusinessLayerFactory.Create())
        {
            proxy.DiscoveryImport(array.EngineID, ProviderHelper.GetProviderInfoOfWizardProviders(this.providers, configuration.ProviderConfiguration.SelectedProviders), toDiscovery);
        }
    }
}
