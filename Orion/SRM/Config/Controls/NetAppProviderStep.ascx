﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetAppProviderStep.ascx.cs" Inherits="Orion_SRM_Config_Controls_NetAppProviderStep" %>
<%@ Import Namespace="Resources" %>
<%@ Register Src="~/Orion/SRM/Config/Controls/TestConnectionMessages.ascx" TagPrefix="UIInfo" TagName="TestConnectionMessages" %>
<%@ Register TagPrefix="UIInfo" TagName="DeviceType" Src="~/Orion/SRM/Config/Controls/DeviceType.ascx" %>

<orion:Include ID="Include" runat="server" File="SRM/Config/Controls/DiscoveryValidation.js" />
<orion:Include runat="server" File="Discovery.css" Module="SRM" />

<asp:Label runat="server" ID="NetAppSubTitle"></asp:Label>
<style type="text/css">
    #testingConnectionMsg span {
        white-space: normal;
    }

    #testConnectionFailMsg span {
        white-space: normal;
    }

    #testConnectionPassMsg span {
        white-space: normal;
    }
</style>
<div class="srm-enrollment-wizard-content">
    <div class="srm-main-content PollingPropertiesSection">
        <table class="srm-EnrollmentWizard-providerStepContentTable">
            <tr>
                <td>
                    <table class="srm-EnrollmentWizard-providerStepInputsTable">
                        <tr>
                            <td><%= SrmWebContent.Provider_Step_Array_Type%>:</td>
                            <td>
                                <b>
                                    <asp:Label runat="server" ID="ArrayTypeLabel" />
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td><%= SrmWebContent.Provider_Step_Array_Ip%>*:</td>
                            <td>
                                <asp:TextBox runat="server" ID="IpAddressTextBox" ClientIDMode="Static" />
                            </td>
                        </tr>
                        <tr>
                            <td><%= SrmWebContent.Provider_Step_Array_Credential%>*:</td>
                            <td>
                                <asp:DropDownList runat="server" ID="ArrayCredentialDropDown" onChange="return OnSelectedIndexChange(this);" ClientIDMode="Static" />
                            </td>
                        </tr>
                    </table>
                    <table class="srm-EnrollmentWizard-providerStepInputsTable" id="StoredCredFileds">
                        <tr>
                            <td><%= SrmWebContent.Provider_Step_Credential_Name%>*:</td>
                            <td>
                                <asp:TextBox runat="server" ID="CredentialNameTextBox" ClientIDMode="Static" onChange="return OnSelectedValueChange();" />
                            </td>
                        </tr>
                        <tr>
                            <td><%= SrmWebContent.Provider_Step_User_Name%>*:</td>
                            <td>
                                <asp:TextBox runat="server" ID="UserNameTextBox" ClientIDMode="Static" onChange="return OnSelectedValueChange();" />
                            </td>
                        </tr>
                        <tr>
                            <td><%= SrmWebContent.Provider_Step_Password%>*:</td>
                            <td>
                                <asp:TextBox runat="server" ID="PasswordTextBox" ClientIDMode="Static" TextMode="Password" onChange="return OnSelectedValueChange();" />
                            </td>
                        </tr>
                        <tr>
                            <td><%= SrmWebContent.Add_Provider_ConfirmPassword%>*:</td>
                            <td>
                                <asp:TextBox runat="server" ID="ConfirmPasswordTextBox" ClientIDMode="Static" TextMode="Password" onChange="return OnSelectedValueChange();" />
                            </td>
                        </tr>
                        <tr>
                            <td><%= SrmWebContent.Provider_Step_Protocol%>:</td>
                            <td>
                                <asp:CheckBox runat="server" ID="ProtocolCheckBox" onChange="return OnSelectedValueChange();" ClientIDMode="Static" />
                            </td>
                        </tr>
                    </table>
                    <table class="srm-EnrollmentWizard-providerStepInputsTable">
                        <tr>
                            <td />
                            <td>
                                <input type="button" id="TestConnectionButton" class="providerStepButton" value="<%= SrmWebContent.Provider_Step_Test_Connection %>" onclick="OnTestButtonClick();" />
                            </td>
                        </tr>
                    </table>
                    <UIInfo:TestConnectionMessages runat="server" ID="TestConnectionMessages" JsCancellationFunction="javascript:OnCancelClick();" />
                </td>
                <td>
                    <UIInfo:DeviceType runat="server" ID="deviceInfo"></UIInfo:DeviceType>
                </td>
            </tr>
        </table>
    </div>
</div>

<script>
    var netAppProvider = SW.Core.namespace("SW.SRM.EditProperties.NetApp");

    $(function () {
        var ddl = $('#ArrayCredentialDropDown');
        var ddlText = $(ddl).find(':selected').text();
        if (ddlText != "<%= SrmWebContent.Provider_Step_New_Credential %>") {
            OnSelectedIndexChange(ddl);
        }
    });

    onClickNextButtonEvent = function () {
        OnTestButtonClick(true, false);
    };
    
    netAppProvider.ValidateCredentials = function () {
        var testCredentialDeferred = $.Deferred();
        SW.SRM.DiscoveryValidation.init("<%= ArrayID %>", "<%= ArraySerialNumber %>", "<%= this.DeviceGroupId %>", request, testCredentialDeferred, "<%= SrmWebContent.Provider_Step_Unexpected_Error%>", ShowFailMessageBox);
        var globalDiscoveryPromise = $.Deferred();
        OnTestButtonClick(false, true);
        $.when(testCredentialDeferred).then(function (x) {
            return globalDiscoveryPromise.resolve();

        }, function () {
            return globalDiscoveryPromise.reject();
        });

        return globalDiscoveryPromise.promise();
    };

    netAppProvider.MonitoredControlIds = ['IpAddressTextBox', 'ArrayCredentialDropDown', 'UserNameTextBox', 'PasswordTextBox'];
    SW.SRM.EditProperties.ProviderChangeManager.InitProvider(netAppProvider, netAppProvider.ValidateCredentials);

    function OnSelectedIndexChange(ddl) {
        $("#testConnectionPassMsg").hide();
        $("#testConnectionFailMsg").hide();

        $("#UserNameTextBox").val('');
        $("#PasswordTextBox").val('');
        $("#CredentialNameTextBox").val('');
        $("#ProtocolCheckBox").attr('checked', false);
        $("#ConfirmPasswordTextBox").val('');

        if (ddl.value != "<%= SrmWebContent.Provider_Step_New_Credential %>") {
            $('#StoredCredFileds').hide();
        } else {
            $('#StoredCredFileds').show();
        }
        $('#TestConnectionButton').attr("disabled", false);
    }

    function OnSelectedValueChange() {
        $('#ArrayCredentialDropDown').get(0).selectedIndex = 0;
    }

    function ValidateForm(ipAddress, credentialName, username, password, confirmPassword, $selectCredential) {
        var title = "<%= SrmWebContent.ProviderStep_Validation_Error %>";

        if (!ipAddress.trim()) {
            SW.SRM.Common.ShowError(title, "<%= SrmWebContent.ProviderStep_ProviderIpAddress_Required_Field_Validator %>");
            return false;
        }

        if ($("#ArrayCredentialDropDown").val().trim() == "<%= SrmWebContent.Provider_Step_New_Credential %>") {
            if (!username.trim()) {
                SW.SRM.Common.ShowError(title, "<%= SrmWebContent.ProviderStep_ProviderUsername_Required_Field_Validator %>");
                return false;
            }

            if (!credentialName.trim()) {
                SW.SRM.Common.ShowError(title, "<%= SrmWebContent.ProviderStep_ProviderCredentialNameStr_Required_Field_Validator %>");
                return false;
            }

            var selectedCredentialOptionsArr = $selectCredential.children().map(function () { return this.text.toLowerCase(); }).get();
            var credentialNameExists = $.inArray(credentialName.toLowerCase(), selectedCredentialOptionsArr) >= 0;
            if (credentialNameExists) {
                SW.SRM.Common.ShowError(
                    "<%= SrmWebContent.Add_Provider_CredentialNameAlreadyExistHeader %>",
                    "<a href='javascript:loadCredentials(&quot;" + credentialName + "&quot;)'><%= SrmWebContent.Add_Provider_CredentialNameAlreadyExist_LoadSaved %></a> or <a href='javascript:focusCredentialsDisplayName()'><%= SrmWebContent.Add_Provider_CredentialNameAlreadyExist_RenameDuplicate %></a>"
                    );
                return false;
            }

            if (!password.trim()) {
                SW.SRM.Common.ShowError(title, "<%= SrmWebContent.ProviderStep_ProviderPassword_Required_Field_Validator %>");
                return false;
            }

            if (password.trim() != confirmPassword.trim()) {
                SW.SRM.Common.ShowError(title, "<%= SrmWebContent.Add_Provider_ConfirmPassword_CompareField_Validator %>");
                return false;
            }
        }

        return true;
    }

    var request;

    function focusCredentialsDisplayName() {
        $("#CredentialNameTextBox").focus();
    }

    function loadCredentials(credentialName) {
        $('#ArrayCredentialDropDown').find('option:contains("' + credentialName + '")').attr("selected", true);
        $('#ArrayCredentialDropDown').trigger('change');
    }

    function OnTestButtonClick(isRequiredPostBack, runDiscovery) {
        request = null;
        $('#TestConnectionButton').attr("disabled", true);
        var credentialName = $("#CredentialNameTextBox").val();
        var useSsl = $("#ProtocolCheckBox").is(':checked');
        var password = $("#PasswordTextBox").val();
        var confirmPassword = $("#ConfirmPasswordTextBox").val();
        var userName = $("#UserNameTextBox").val();
        var ip = $.trim(String($("#IpAddressTextBox").val()));
        var $selectCredential = $("#ArrayCredentialDropDown");

        if (!ValidateForm(ip, credentialName, userName, password, confirmPassword, $selectCredential)) {
            $('#TestConnectionButton').attr("disabled", false);
            SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
            return;
        }

        $("#testConnectionPassMsg").hide();
        $("#testConnectionFailMsg").hide();

        $("#testingConnectionMsg").show();

        request = SW.SRM.Common.CallSrmWebService('/Orion/SRM/Services/EnrollmentWizardService.asmx', 'TestNetAppFilerCredentials',
            {
                credentialId: $selectCredential.val(), credentialName: credentialName, useSsl: useSsl, password: password, username: userName, ip: ip, saveProvider: runDiscovery === true, groupId: "<%= this.DeviceGroupId %>" 
            },
            function (result) {
                $("#testingConnectionMsg").hide();
                $('#TestConnectionButton').attr("disabled", false);
                if (result.IsSuccess) {
                    ShowPassMessageBox(result.Message);
                    if (isRequiredPostBack) {
                        providerStepSubmit();
                    }

                    if (runDiscovery) {
                        SW.SRM.DiscoveryValidation.startDiscovery();
                    }
                } else {
                    ShowFailMessageBox(result.Message, result.ErrorMessage);
                    SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
                }
            },
            function (result) {
                $("#testingConnectionMsg").hide();
                $('#TestConnectionButton').attr("disabled", false);

                if (request.status !== 0) {
                    ShowFailMessageBox("<%= SrmWebContent.Provider_Step_Unexpected_Error%>");
                }
                SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
            });
    }

    function OnCancelClick() {
        $("#testingConnectionMsg").hide();
        if (request) {
            request.abort();
        }
    }

    function ShowPassMessageBox(text) {
        $("#passMessageContent").empty();
        $("#passMessageContent").append(text);
        $("#testConnectionPassMsg").show();
    }

    function ShowFailMessageBox(message, errorMessage) {
        var ip = $("#IpAddressTextBox").val();
        var connErrorMsg = "<%= SrmWebContent.Provider_Step_Provider_Connecting_Error %>:";

        var title = connErrorMsg.concat(" ").concat(ip);
        SW.SRM.Common.ShowError(title, message, errorMessage);
    }
</script>


