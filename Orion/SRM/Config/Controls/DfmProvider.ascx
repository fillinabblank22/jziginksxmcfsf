﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DfmProvider.ascx.cs" Inherits="Orion_SRM_Config_Controls_DfmProvider" %>
<%@ Register TagPrefix="srm" TagName="PollingProperties" Src="~/Orion/SRM/Controls/PollingProperties.ascx" %>
<%@ Register TagPrefix="UIInfo" TagName="DeviceType" Src="~/Orion/SRM/Config/Controls/DeviceType.ascx" %>
<%@ Register TagPrefix="UIInfo" TagName="TestConnectionMessages" Src="~/Orion/SRM/Config/Controls/TestConnectionMessages.ascx" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web" %>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="SRM/Config/Controls/DiscoveryValidation.js" ID="Include" />
<orion:Include runat="server" File="SRM.EditProperties.DfmProvider.js" Module="SRM" />

<orion:Include runat="server" File="SRM.EditProperties.SmisProvider.css" Module="SRM" />
<orion:Include runat="server" File="SRM.css" Module="SRM" />
<orion:Include runat="server" File="Discovery.css" Module="SRM" />

<asp:PlaceHolder runat="server" ID="hfPlaceholder"></asp:PlaceHolder>
<div class="srm-main-content smisprovider">
    <span><%= SrmWebContent.EditProperties_DfmProvider_PollingProperties %></span>
    <table>
        <tr>
            <td class="col1">
                <table>
                    <tr>
                        <td class="label"><span><span id="labelArrayType">Array Type</span>:</span></td>
                        <td>
                            <asp:Label ID="txtArrayType" runat="server" CssClass="boldText" />
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><span id="labelProvider"><%= SrmWebContent.DFM_Provider %></span>:</span>
                            <div id="loadingImageSmisPlaceholder">
                                <asp:Image ImageUrl="~/Orion/images/loading_gen_small.gif" style="display: none;" ClientIDMode="Static" ID="loadingIconDfmProvider" runat="server" AlternateText="Loading" />
                            </div>
                        </td>
                        <td>
                             <img class="StatusIcon" src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.Providers%>&status=<%=(int)(dfmProvider.Status)%>&size=small" />
                            <asp:HyperLink ID="hlProvider" runat="server" /><br/>
                            <input type="button" id="testConnectionSmisButton" class="providerStepButton" value="<%= SrmWebContent.Provider_Step_Test_Connection %>" onclick="SW.SRM.EditProperties.DfmProvider.ValidateConnection();" />
                        </td>
                    </tr>
                </table>
                <UIInfo:TestConnectionMessages runat="server" ID="TestConnectionMessages" JsCancellationFunction="javascript:SW.SRM.EditProperties.DfmProvider.onCancelClick();"/>
                <srm:PollingProperties runat="server" ID="PollingProperties" />
            </td>
            <td>
               <UIInfo:DeviceType runat="server" ID="deviceInfo"></UIInfo:DeviceType>
            </td>
        </tr>
    </table>
</div>
