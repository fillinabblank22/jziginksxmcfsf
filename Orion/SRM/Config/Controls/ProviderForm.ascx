﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProviderForm.ascx.cs" Inherits="Orion_SRM_Config_Controls_ProviderForm" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Register TagPrefix="UIInfo" TagName="DeviceType" Src="~/Orion/SRM/Config/Controls/DeviceType.ascx" %>
<%@ Register Src="~/Orion/SRM/Config/Controls/TestConnectionMessages.ascx" TagPrefix="UIInfo" TagName="TestConnectionMessages" %>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="SRM/Config/Controls/SRM.Expander.js" />
<orion:Include runat="server" File="Provider.css" Module="SRM" />

<asp:HiddenField runat="server" ID="txtProviderNoDataAvailable" ClientIDMode="Static"/>
<div class="srm-main-content smisprovider" >
    <table>
        <tr>
            <td class="css_double-column-layout">
                <table>
                    <tr id="trArrayType" runat="server">
                        <td><%= SrmWebContent.Provider_Step_Array_Type%>:</td>
                        <td>
                            <b>
                                <asp:Label runat="server" ID="ArrayTypeLabel" />
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><asp:Label runat="server" id="labelIpAddress" ClientIDMode="Static"></asp:Label>*:</span></td>
                        <td>
                            <span><input type="text" id="IpAddressOrHostnameTextBox" runat="server" clientidmode="Static"/></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><%= SrmWebContent.Add_Provider_Choose_Credential%>*:</span></td>
                        <td>
                            <span><asp:DropDownList id="ChooseCredentialDdl" onchange="ChooseCredentialDdl_OnChange(this)" runat="server" clientidmode="Static"/></span>
                        </td>
                    </tr>
                </table>
                <table id="StoredCredFiledsTable">
                    <tr>
                        <td class="label"><span><span id="labelDisplayName"><%= SrmWebContent.Add_Provider_DisplayName %></span>*:</span></td>
                        <td>
                            <span><input type="text" id="DisplayNameTextBox" runat="server" clientidmode="Static"/></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><asp:Label runat="server" id="labelProviderUserName" ClientIDMode="Static"></asp:Label>*:</span></td>
                        <td>
                            <span><input type="text" id="ProviderUsernameTextBox" runat="server" clientidmode="Static"/></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><asp:Label runat="server" id="labelProviderPassword" ClientIDMode="Static"></asp:Label>*:</span></td>
                        <td>
                            <span><input type="password" id="ProviderPasswordTextBox" runat="server" clientidmode="Static"/></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><%= SrmWebContent.Add_Provider_ConfirmPassword%>*:</span></td>
                        <td>
                            <span><input type="password" id="ProviderConfirmPasswordTextBox" runat="server" clientidmode="Static"/></span>
                        </td>
                    </tr>
                </table>
                <div id="expanderElement"><a href="javascript:expanderChangedHandler();"><img src="/Orion/SRM/images/Button.Expand.gif" alt="[+]" /><%= SrmWebContent.Provider_Step_AddProvider_Advanced %></a></div>
                <table id="AdvancedCredFiledsTable">
                    <tr>
                        <td class="label"><span><%= SrmWebContent.Add_Provider_Protocol%></span></td>
                        <td>
                            <span><input type="checkbox" id="ProtocolCheckBox" runat="server" clientidmode="Static"/><%= SrmWebContent.Provider_Step_UseHttp_Label %></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><span id="labelHttpsPort"><%= SrmWebContent.Add_Provider_Https_Port%></span>*:</span></td>
                        <td>
                            <span><input type="text" id="HttpsPortTextBox" runat="server" clientidmode="Static" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><span><span id="labelHttpPort"><%= SrmWebContent.Add_Provider_Http_Port%></span>*:</span></td>
                        <td>
                            <span><input type="text" id="HttpPortTextBox" runat="server" clientidmode="Static" /></span>
                        </td>
                    </tr>
                    <tr runat="server" id="trInteropNamespace">
                        <td class="label"><span><span id="labelInteropNamespace"><%= SrmWebContent.Add_Provider_Interop_Namespace%></span>*:</span></td>
                        <td>
                            <span><input type="text" id="InteropNamespaceTextBox" runat="server" clientidmode="Static" /></span>
                        </td>
                    </tr>
                    <tr runat="server" id="trArrayNamespace">
                        <td class="label"><span><span id="labelArrayNamespace"><%= SrmWebContent.Add_Provider_Array_Namespace%></span>*:</span></td>
                        <td>
                            <span><input type="text" id="ArrayNamespaceTextBox" runat="server" clientidmode="Static" /></span>
                        </td>
                    </tr>
                </table>
                <input type="button" id="testConnectionButton" class="providerStepButton" value="<%= SrmWebContent.Provider_Step_Test_Connection %>" onclick="TestProviderCredential();" />
                
                <UIInfo:TestConnectionMessages runat="server" ID="TestConnectionMessages" />
                
            </td>
            <td id="imgDfm" class="smisprovider css_double-column-layout">
                <UIInfo:DeviceType runat="server" ID="deviceInfo"></UIInfo:DeviceType>
            </td>
        </tr>
    </table>
</div>
