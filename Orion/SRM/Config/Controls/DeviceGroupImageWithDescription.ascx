﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DeviceGroupImageWithDescription.ascx.cs" Inherits="DeviceGroupImageWithDescription" %>

<%--<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="SRM.EditProperties.SmisProvider.js" Module="SRM" />
<orion:Include runat="server" File="SRM.EditProperties.SmisProvider.css" Module="SRM" />
<orion:Include runat="server" File="SRM.EditProperties.SmisProvider.css" Module="SRM" />
<orion:Include runat="server" File="SRM.css" Module="SRM" />--%>
<orion:Include runat="server" File="Discovery.css" Module="SRM" />

<table id="srm-devicetype-info">
        <tbody>
            <tr>
                <td>
                    <img src="/Orion/SRM/images/hint_icon_u43.png" />
                </td>
                <td>
                    <div>
                        <span id="FamilyTypeName" runat="server" class="bigfont uppercase"></span>
                        <span id="FamilyTypeDescription" runat="server" ></span>
                        <span><a id="FamilyTypeLink" rel="noopener noreferrer" target="_blank" runat="server" ></a></span>
                    </div>
                    <div class="familyimage">
                        <asp:Image runat="server" ID="image" />
                    </div>
                </td>
            </tr>
        </tbody>
</table>
