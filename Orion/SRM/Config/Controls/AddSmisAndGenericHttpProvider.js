﻿(function(ns) {
    'use strict';

    function Validator() {
        var validators = [];
        function add(validation, message) {
            validators.push({ validation: validation, message: message });
        }
        function validate() {
            var error = '';
            $.each(validators, function(idx, validator) {
                var isValid = validator.validation();
                if (!isValid) {
                    error = validator.message;
                    return false;
                }
            });
            
            return {
                isValid: error === '',
                error: error
            };
        }

        return {
            add: add,
            validate: validate
        };
    }
    
    var dialog,
        credentialType = {
            SMIS: 'SMIS',
            GenericHttp: 'GenericHttp'
        },
        fields = {},
        config = {},
        activeRequests = [],
        testingConnectionElement,
        testConnectionFailElement,
        testConnectionSuccessElement,
        cancel = false;

    ns.init = function (opts) {
        opts = opts || {};
        config.newCredentialDefaultText = opts.newCredentialDefaultText || '<New Credential>';
        config.deviceGroupId = opts.deviceGroupId;
        config.connectionTestUnexpectedFailedMessage = opts.connectionTestUnexpectedFailedMessage || 'Unexpected error occured.';
        config.connectionTestFailedTitle = opts.connectionTestFailedTitle || 'Connection error';
        config.providerGroupIdDelimiter = opts.providerGroupIdDelimiter || ';';
        config.animationSpeed = 200;

        fields.getProvidersGrid = function() { return Ext.getCmp($('#Grid >:first-child')[0].id); };
        fields.getSaveButton = function() { return Ext.getCmp('save'); };
        fields.getFieldsForProtocol = function(protocol) {
            return protocol === fields.smis.type ? fields.smis : fields.api;
        };

        fields.testConnectionButton = {
            element: $('#uxTestConnection'),
            switch: function(enable) { this.element.prop('disabled', !enable); },
            disable: function() { this.switch(false); },
            enable: function() { this.switch(true); }
        };
        fields.providerIp = $('#uxIpAddressOrHostname');
        fields.providerIpLabel = $('#uxIpAddressOrHostnameLabel');
        fields.smis = {
            defaults: {
                httpPort: opts.smisHttpPort || 5988,
                httpsPort: opts.smisHttpsPort || 5989,
                arrayNamespace: opts.smisArrayNamespace || '',
                interopNamespace: opts.smisInteropNamespace || ''
            },
            advancedSettings: $('#uxAdvancedSmisSettingsContainer'),
            settings: $('#uxSmisCredentialsContainer'),
            credentials: $('#uxSmisCredentialsChoice'),
            credentialName: $('#uxSmisCredentialName'),
            username: $('#uxSmisUsername'),
            password: $('#uxSmisPassword'),
            passwordConfirm: $('#uxSmisPasswordConfirm'),
            useHttp: $('#uxSmisUseHttp'),
            httpPort: $('#uxSmisHttpPort'),
            httpsPort: $('#uxSmisHttpsPort'),
            interopNamespace: $('#uxSmisInteropNamespace'),
            arrayNamespace: $('#uxSmisArrayNamespace'),
            labels: {
                credentialName: $('#uxSmisCredentialNameLabel'), 
                username: $('#uxSmisUsernameLabel'),
                password: $('#uxSmisPasswordLabel'),
                passwordConfirm: $('#uxSmisPasswordConfirmLabel'),
                httpPort: $('#uxSmisHttpPortLabel'),
                httpsPort: $('#uxSmisHttpsPortLabel')
            },
            type: credentialType.SMIS
        };

        fields.expander = {
            container: $('#uxExpander'),
            img: $('#uxExpander img'),
            expandButtonGif: '/Orion/SRM/images/Button.Expand.gif',
            collapseButtonGif: '/Orion/SRM/images/Button.Collapse.gif',
            collapse: function() {
                fields.smis.advancedSettings.hide(config.animationSpeed);
                fields.api.advancedSettings.hide(config.animationSpeed);
                fields.expander.img.attr('src', this.expandButtonGif);
                fields.expander.img.attr('alt', '[+]');
            },
            expand: function() {
                if (!fields.smis.credentials.val()) {
                    fields.smis.advancedSettings.show(config.animationSpeed);
                }
                if (!fields.api.credentials.val()) {
                    fields.api.advancedSettings.show(config.animationSpeed);
                }
    
                fields.expander.img.attr('src', this.collapseButtonGif);
                fields.expander.img.attr('alt', '[-]');
            },
            isCollapsed: function() {
                return fields.expander.img.attr('src') === this.expandButtonGif;
            }
        };

        fields.api = {
            defaults: {
                httpPort: opts.apiHttpPort || 80,
                httpsPort: opts.apiHttpsPort || 443
            },
            advancedSettings: $('#uxAdvancedApiSettingsContainer'),
            settings: $('#uxApiCredentialsContainer'),
            credentials: $('#uxApiCredentialsChoice'),
            credentialName: $('#uxApiCredentialName'),
            username: $('#uxApiUsername'),
            password: $('#uxApiPassword'),
            passwordConfirm: $('#uxApiPasswordConfirm'),
            useSmisCredentials: $('#uxApiUseSmisCredentials'),
            useSmisCredentialsContainer: $('#uxApiUseSmisCredentialsContainer'),
            useHttp: $('#uxApiUseHttp'),
            httpPort: $('#uxApiHttpPort'),
            httpsPort: $('#uxApiHttpsPort'),
            labels: {
                credentialName: $('#uxApiCredentialNameLabel'),
                username: $('#uxApiUsernameLabel'),
                password: $('#uxApiPasswordLabel'),
                passwordConfirm: $('#uxApiPasswordConfirmLabel'),
                httpPort: $('#uxApiHttpPortLabel'),
                httpsPort: $('#uxApiHttpsPortLabel')
            },
            type: credentialType.GenericHttp
        };

        testingConnectionElement = $('#testingConnectionMsg');
        testConnectionFailElement = $('#testConnectionFailMsg');
        testConnectionSuccessElement = $('#testConnectionPassMsg');

        $.each(fields.smis, function(k, v) { if (v.hasOwnProperty('selector')) {
            v.change(function() { 
                switchSaveButton(false);
            });
        }});

        $.each(fields.api, function(k, v) { if (v.hasOwnProperty('selector')) {
            v.change(function() { 
                switchSaveButton(false);
            });
        }
        });
        if (opts.isEditProviderPrortiesPage) {
            initFromEditProvider();
        };
    };

    function isFormValid() {
        SW.SRM.TestConnectionUI.HidePass(ns);

        var validator = new Validator();
        validator.add(function() { return fields.providerIp.val().trim(); }, createErrorMessage(fields.providerIpLabel));
        
        addBasicFieldsValidators(validator, fields.smis);
        addBasicFieldsValidators(validator, fields.api);

        var result = validator.validate();
        if (!result.isValid) {
            handleValidationError(result.error);
            return false;
        }

        return true;
    }

    function addBasicFieldsValidators(validator, protocolFields) {
        var isExistingCredentialChosen = protocolFields.credentials.val() !== '';
        if (isExistingCredentialChosen) {
            return;
        }
        
        validator.add(function() { return protocolFields.credentialName.val().trim(); }, createErrorMessage(protocolFields.labels.credentialName));
        validator.add(function() { return !dropdownContainsText(protocolFields.credentials, protocolFields.credentialName); }, 
            '<a href=\'javascript:SW.SRM.AddSmisAndHttpProvider.loadCredential(&quot;' + protocolFields.type + '&quot;, &quot;' + protocolFields.credentialName.val() + '&quot;)\'>@{R=SRM.Strings;K=Add_Provider_CredentialNameAlreadyExist_LoadSaved; E=js}</a> or <a href=\'javascript:SW.SRM.AddSmisAndHttpProvider.focusCredentialDisplayName(&quot;' + protocolFields.type + '&quot;)\'>@{R=SRM.Strings;K=Add_Provider_CredentialNameAlreadyExist_RenameDuplicate; E=js}</a>');
        validator.add(function() { return protocolFields.username.val().trim(); }, createErrorMessage(protocolFields.labels.username));
        validator.add(function() { return protocolFields.password.val().trim(); }, createErrorMessage(protocolFields.labels.password));
        validator.add(function() { return protocolFields.password.val() === protocolFields.passwordConfirm.val(); }, 
            '@{R=SRM.Strings;K=Add_Provider_ConfirmPassword_CompareField_Validator; E=js}');
        if (protocolFields.type !== credentialType.GenericHttp && (typeof protocolFields.httpPort !== 'undefined')) {
            validator.add(function() { return protocolFields.httpPort.val().trim(); }, createErrorMessage(protocolFields.labels.httpPort));
            validator.add(function() { return isPortValid(protocolFields.httpPort.val()); }, 
                '@{R=SRM.Strings;K=Add_Provider_NotValidPort_Validator; E=js}'.replace('{0}', protocolFields.labels.httpPort.text()));
        }
        validator.add(function() { return protocolFields.httpsPort.val().trim(); }, createErrorMessage(protocolFields.labels.httpsPort));
        validator.add(function() { return isPortValid(protocolFields.httpsPort.val()); }, 
            '@{R=SRM.Strings;K=Add_Provider_NotValidPort_Validator; E=js}'.replace('{0}', protocolFields.labels.httpsPort.text()));
    }

    function dropdownContainsText(dropdownElement, credentialNameElement) {
        var contains = false;
        dropdownElement.find('option').each(function() {
            if ($(this).text().toLowerCase() === credentialNameElement.val().toLowerCase()) {
                contains = true;
                return false;
            }
        });

        return contains;
    }

    function isPortValid(port) {
        var parsedPort = parseInt(port);
        if (!$.isNumeric(parsedPort) ||
            Math.floor(parsedPort) !== parsedPort ||
            parsedPort < 1 ||
            parsedPort > 65535 ||
            parsedPort.toString() !== port) {
            return false;
        }

        return true;
    }

    function createErrorMessage(fieldLabel) {
        return '@{R=SRM.Strings;K=Add_Provider_Required_Field_Validator; E=js}'.replace('{0}', fieldLabel.text());
    }

    function areProvidersUnique() {
        var getIpOrHostnameFromFriendlyName = function(friendlyName) {
            return friendlyName.split('_')[1];
        };

        var getCredentialName = function(providerFields) {
            return (providerFields.credentials.val() == null || providerFields.credentials.val().length === 0) ? 
                providerFields.credentialName.val() : 
                providerFields.credentials.text();
        };

        var credentialNames = [ getCredentialName(fields.smis), getCredentialName(fields.api) ],
            gridStore = fields.getProvidersGrid().store,
            existsInStore = false,
            existingProvider,
            existingCredentialName;

        gridStore.each(function (row) {
            var ipOrHostname = getIpOrHostnameFromFriendlyName(row.get('FriendlyName'));
            var existingCredentialIndex = $.inArray(row.get('CredentialName'), credentialNames);
            if (ipOrHostname === fields.providerIp.val() && existingCredentialIndex !== -1) {
                existsInStore = true;
                existingProvider = ipOrHostname;
                existingCredentialName = credentialNames[existingCredentialIndex];
                return false;
            }
        });

        if (existsInStore) {
            handleValidationError(SW.Core.String.Format('@{R=SRM.Strings;K=Add_Provider_CredentialNameAlreadyUsedByProvider; E=js}', existingProvider, existingCredentialName));
            return false;
        }

        return true;
    }

    function loadCredentialDropdowns() {
        clearCredentialsDropdowns();
        loadCredentialDropdown(credentialType.SMIS, fields.smis.credentials);
        loadCredentialDropdown(credentialType.GenericHttp, fields.api.credentials);
    }

    function clearCredentialsDropdowns() {
        fields.smis.credentials.find('option').remove();
        fields.api.credentials.find('option').remove();
    }

    function handleError(message, errorMessage) {
        SW.SRM.TestConnectionUI.HandleError(ns);
        var title = config.connectionTestFailedTitle.concat(' ').concat(fields.providerIp.val());
        SW.SRM.TestConnectionUI.ShowError(title, message, errorMessage, ns);
    }

    function handleValidationError(message, errorMessage) {
        SW.SRM.Common.ShowError('@{R=SRM.Strings;K=Add_Provider_ValidationError_Title; E=js}', message, errorMessage);
    }

    function switchSaveButton(enable) {
        var saveButton = fields.getSaveButton();
        if (typeof saveButton === 'undefined' || saveButton === null) {
            return;
        }

        if (enable) {
            saveButton.enable();
        } else {
            saveButton.disable();
        }
    }

    function runCancellation() {
        var request = activeRequests.pop();
        while (typeof request !== 'undefined' && request !== null) {
            request.abort();
            request = activeRequests.pop(); 
        }

        SW.SRM.TestConnectionUI.HideError(ns);
        SW.SRM.TestConnectionUI.ShowLoading(false, ns);
    }

    function clearForm() {
        ns.toggleSettingsVisibility(0);

        testingConnectionElement.hide();
        testConnectionFailElement.hide();
        testConnectionSuccessElement.hide();

        var resetFields = function(fields) {
            $.each(fields, function(k, v) {
                v.val('');
            });
        }; 

        var resetDefaults = function(fields) {
            if (!fields.defaults) { return; }
            $.each(fields.defaults, function(k, v) {
                fields[k].val(v);
            });
        };

        resetFields([ fields.providerIp,
            fields.smis.username,
            fields.smis.password,
            fields.smis.passwordConfirm,
            fields.smis.credentialName,
            fields.smis.credentials,
            fields.api.username,
            fields.api.password,
            fields.api.passwordConfirm,
            fields.api.credentialName,
            fields.api.credentials ]);

        resetDefaults(fields.smis);
        resetDefaults(fields.api);

        fields.api.useSmisCredentials.prop('checked');
        ns.useSmisCredentialsForApi(false);

        fields.smis.useHttp.prop('checked', false);
        fields.expander.collapse();
    }

    function copyTextWhenUsingSmisCredentials(from, to) {
        if (fields.api.useSmisCredentials.is(':checked')) {
            to.val(from.val());
        }
    }

    function loadCredentialDropdown(credentialType, destinationDropdown) {
        ORION.callWebService('/Orion/SRM/Services/EnrollmentWizardService.asmx',
            'GetStoredCredentials',
            { 'type': credentialType },
            function(result) {
                $('<option value="">' + result.Default + '</option>').appendTo(destinationDropdown);
                $.each(
                    result.Credentials,
                    function(index, item) {
                        $('<option value="' + item.Key + '">' + item.Value + '</option>').appendTo(destinationDropdown);
                    }
                );
            }
        );
    }

    function testSmisCredentials() {
        var credentialId = fields.smis.credentials.val() === null || fields.smis.credentials.val().length === 0 ? -1 : parseInt(fields.smis.credentials.val());

        var dfd = $.Deferred();
        var request = SW.SRM.Common.CallSrmWebService(
            '/Orion/SRM/Services/EnrollmentWizardService.asmx',
            'TestSmisCredentialsExtendedDevice',
            {
                'credentialID': credentialId,
                'credentialName': fields.smis.credentialName.val(),
                'userName': fields.smis.username.val(),
                'password': fields.smis.password.val(),
                'useHttp': fields.smis.useHttp.is(':checked'),
                'httpPort': parseInt(fields.smis.httpPort.val()) || 5988,
                'httpsPort': parseInt(fields.smis.httpsPort.val()) || 5989,
                'interopNamespace': fields.smis.interopNamespace.val(),
                'arrayNamespace': fields.smis.arrayNamespace.val(),
                'ip': fields.providerIp.val(),
                'groupId': config.deviceGroupId,
                'isEdit': false
            },
            function (result) {
                if (cancel) {
                    dfd.reject();
                    return;
                }

                if (result.IsSuccess) {
                    dfd.resolve(result);
                }
                else {
                    dfd.reject(result);
                }

                activeRequests.splice(activeRequests.indexOf(request), 1);
            },
            function() {
                dfd.reject();
            }
        );

        activeRequests.push(request);
        return dfd.promise();
    }

    function testApiCredentials() {
        var credentialValue = fields.api.credentials.val(),
            credentialId = credentialValue == null || credentialValue === 0 ? -1 : parseInt(credentialValue),
            useHttp = fields.api.useHttp.length ? fields.api.useHttp.is(':checked') : false,
            portElement = useHttp ? fields.api.httpPort : fields.api.httpsPort;

        var dfd = $.Deferred();
        var request = SW.SRM.Common.CallSrmWebService(
            '/Orion/SRM/Services/EnrollmentWizardService.asmx',
            'TestGenericHttpCredentials',
            {
                'deviceGroupID': config.deviceGroupId,
                'credentialID': credentialId,
                'credentialName': fields.api.credentialName.val(),
                'userName': fields.api.username.val(),
                'password': fields.api.password.val(),
                'useHttp': useHttp,
                'port': parseInt(portElement.val()) || 0,
                'ip': fields.providerIp.val(),
                'saveProvider': false
            },
            function (result) {
                if (cancel) {
                    dfd.reject();
                    return;
                }

                if (result.IsSuccess) {
                    dfd.resolve(result);
                }
                else {
                    dfd.reject(result);
                }

                activeRequests.splice(activeRequests.indexOf(request), 1);
            },
            function() {
                dfd.reject();
            });

        activeRequests.push(request);
        return dfd.promise();
    }

    function saveApiProvider(providerGroupId) {
        var credentialValue = fields.api.credentials.val(),
            credentialId = credentialValue === null || credentialValue.length === 0 ? -1 : parseInt(credentialValue),
            useHttp = fields.api.useHttp.length ? fields.api.useHttp.is(':checked') : false,
            portElement = useHttp ? fields.api.httpPort : fields.api.httpsPort;

        var dfd = $.Deferred();
        SW.SRM.Common.CallSrmWebService(
            '/Orion/SRM/Services/EnrollmentWizardService.asmx',
            'SaveGenericHttpProvider',
            {
                'credentialID': credentialId,
                'credentialName': fields.api.credentialName.val(),
                'userName': fields.api.username.val(),
                'password': fields.api.password.val(),
                'useHttp': useHttp,
                'port': parseInt(portElement.val()),
                'ipAddress': fields.providerIp.val(),
                'providerGroupId': providerGroupId 
            },
            function (providerId) {
                dfd.resolve(providerId);
            },
            function() {
                dfd.reject();
            });

        return dfd.promise();
    }

    function saveSmisProvider(providerGroupId) {
        var credentialId = fields.smis.credentials.val() === null || fields.smis.credentials.val().length === 0 ? -1 : parseInt(fields.smis.credentials.val());

        var dfd = $.Deferred();
        SW.SRM.Common.CallSrmWebService(
            '/Orion/SRM/Services/EnrollmentWizardService.asmx',
            'SaveSmisProviderWithGuid',
            {
                'credentialID': credentialId,
                'displayName': fields.smis.credentialName.val(),
                'userName': fields.smis.username.val(),
                'password': fields.smis.password.val(),
                'useHttp': fields.smis.useHttp.is(':checked'),
                'httpPort': parseInt(fields.smis.httpPort.val()),
                'httpsPort': parseInt(fields.smis.httpsPort.val()),
                'interopNamespace': fields.smis.interopNamespace.val(),
                'arrayNamespace': fields.smis.arrayNamespace.val(),
                'ipAddress': fields.providerIp.val(),
                'providerGroupId': providerGroupId 
            },
            function (providerId) {
                dfd.resolve(providerId);
            },
            function() {
                dfd.reject();
            }
        );

        return dfd.promise();
    }

    function getNewGuid() {
        var dfd = $.Deferred();
        var request = SW.SRM.Common.CallSrmWebService(
            '/Orion/SRM/Services/EnrollmentWizardService.asmx',
            'GetNewGuid', {},
            function (guid) {
                if (cancel) {
                    dfd.reject();
                }

                dfd.resolve(guid);
                activeRequests.splice(activeRequests.indexOf(request), 1);
            },
            function() {
                dfd.reject();
            }
        );

        activeRequests.push(request);
        return dfd.promise();
    }

    function saveProviders(smisResult, apiResult) {
        $.when(getNewGuid())
            .done(function(guid) {
                $.when(saveSmisProvider(guid), saveApiProvider(guid))
                    .done(function(smisProviderId, apiProviderId) {
                        SW.SRM.TestConnectionUI.ShowPassMessageBox(null, ns);
                        SW.SRM.ProviderSelector.setDefaultProvider(smisProviderId + config.providerGroupIdDelimiter + apiProviderId);
                        SW.SRM.ProviderSelector.reload();
                        dialog.hide();
                        fields.testConnectionButton.enable();
                    })
                    .fail(function() {
                        fields.testConnectionButton.enable();
                        SW.SRM.TestConnectionUI.ShowLoading(false, ns);
                        if (typeof smisResult !== 'undefined' && smisResult !== null) {
                            handleError(smisResult.Message, smisResult.ErrorMessage);
                        } else if (typeof apiResult !== 'undefined' && apiResult !== null) {
                            handleError(apiResult.Message, apiResult.ErrorMessage);
                        } else {
                            handleError(config.connectionTestUnexpectedFailedMessage);
                        }
                    });
            })
            .fail(function() {
                fields.testConnectionButton.enable();
                if (cancel) { cancel = false; return; }

                handleError(config.connectionTestUnexpectedFailedMessage);
            });
    }

    function testSmisCrentialsFromEditProperties() {
        var cfg = JSON.parse($('#HiddenFieldSmisConfig').val());
        var arrayID = $('#HiddenFieldArrayConfig').val();

        var cached = $.Deferred();
        var request = SW.SRM.Common.CallSrmWebService("/Orion/SRM/Services/EnrollmentWizardService.asmx",
            'TestSmisCredentialsProvider',
            {
                'credentialID': cfg.CredentialID,
                'ip': cfg.IPAddress,
                'credentialName': null,
                'userName': null,
                'password': null,
                'useHttp': false,
                'httpPort': 0,
                'httpsPort': 0,
                'interopNamespace': null,
                'arrayNamespace': null,
                'engineid': cfg.EngineID,
                'arrayId': arrayID
            },
            function (result) {
                if (cancel) {
                    cached.reject();
                    return;
                }

                if (result.IsSuccess) {
                    cached.resolve(result);
                }
                else {
                    cached.reject(result);
                }

                activeRequests.splice(activeRequests.indexOf(request), 1);
            },
            function () {
                cached.reject();
            }
        );

        activeRequests.push(request);
        return cached.promise();
    }
    
    function testApiCredentialsFromEditProperties() {
        var cfg = JSON.parse($('#HiddenFieldRestConfig').val());
        var dfd = $.Deferred();
        var request = SW.SRM.Common.CallSrmWebService(
            '/Orion/SRM/Services/EnrollmentWizardService.asmx',
            'TestGenericHttpCredentials',
            {
                'deviceGroupID': cfg.DeviceGroupID,
                'credentialID': cfg.CredentialID,
                'credentialName': null,
                'userName': null,
                'password': null,
                'useHttp': false,
                'port': 0,
                'ip': cfg.IPAddress,
                'saveProvider': false
            },
            function (result) {
                if (cancel) {
                    dfd.reject();
                    return;
                }

                if (result.IsSuccess) {
                    dfd.resolve(result);
                }
                else {
                    dfd.reject(result);
                }

                activeRequests.splice(activeRequests.indexOf(request), 1);
            },
            function () {
                dfd.reject();
            });

        activeRequests.push(request);
        return dfd.promise();
    }

    ns.testCredentials = function (doSaveProviders) {
        cancel = false;
        doSaveProviders = doSaveProviders || false;
        if (isFormValid() && areProvidersUnique()) {
            SW.SRM.TestConnectionUI.HideError(ns);
        } else {
            return false;
        }

        switchSaveButton(false);
        fields.testConnectionButton.disable();
        SW.SRM.TestConnectionUI.HidePass(ns);
        SW.SRM.TestConnectionUI.ShowLoading(true, ns);

        return $.when(testSmisCredentials(doSaveProviders), testApiCredentials(doSaveProviders))
            .done(function(smisResult, apiResult) {                   
                SW.SRM.TestConnectionUI.ShowLoading(false, ns);
                SW.SRM.TestConnectionUI.ShowPassMessageBox(null, ns);
                if (doSaveProviders) {
                    saveProviders(smisResult, apiResult);
                } else {
                    fields.testConnectionButton.enable();
                    switchSaveButton(true);
                }
            })
            .fail(function(smisResult, apiResult) {
                fields.testConnectionButton.enable();
                if (cancel) { cancel = false; return; } 

                SW.SRM.TestConnectionUI.ShowLoading(false, ns);
                if (typeof smisResult !== 'undefined' && smisResult !== null) {
                    handleError(smisResult.Message, smisResult.ErrorMessage);
                } else if (typeof apiResult !== 'undefined' && apiResult !== null) {
                    handleError(apiResult.Message, apiResult.ErrorMessage);
                } else {
                    handleError(config.connectionTestUnexpectedFailedMessage);
                }
            });
    };

    ns.testCrendialsFromEditProperties = function() {
        cancel = false;
        var doSaveProviders = false;

        SW.SRM.TestConnectionUI.HidePass(ns);
        SW.SRM.TestConnectionUI.ShowLoading(true, ns);
        return $.when(testSmisCrentialsFromEditProperties(doSaveProviders),
                testApiCredentialsFromEditProperties(doSaveProviders))
            .done(function(smisResult, apiResult) {
                SW.SRM.TestConnectionUI.ShowLoading(false, ns);
                SW.SRM.TestConnectionUI.ShowPassMessageBox(null, ns);
                if (doSaveProviders) {
                    saveProviders(smisResult, apiResult);
                } else {
                    fields.testConnectionButton.enable();
                    switchSaveButton(true);
                }
            })
            .fail(function(smisResult, apiResult) {
                fields.testConnectionButton.enable();
                if (cancel) {
                    cancel = false;
                    return;
                }

                SW.SRM.TestConnectionUI.ShowLoading(false, ns);
                if (typeof smisResult !== 'undefined' && smisResult !== null) {
                    handleError(smisResult.Message, smisResult.ErrorMessage);
                } else if (typeof apiResult !== 'undefined' && apiResult !== null) {
                    handleError(apiResult.Message, apiResult.ErrorMessage);
                } else {
                    handleError(config.connectionTestUnexpectedFailedMessage);
                }
            });
    };

    ns.cancelTestCredentials = function(){
        cancel = true;
        runCancellation();
    };

    var copySmisCredentialsToApi = function() {
        ns.copySmisUsernameToApi();
        ns.copySmisPasswordToApi();
        ns.copySmisPasswordConfirmToApi();
    };

    ns.loadCredential = function(protocol, credentialName) {
        var protocolFields = fields.getFieldsForProtocol(protocol);
        var credentialId = protocolFields.credentials.find('option:contains(' + credentialName + ')').val();
        protocolFields.credentials.val(credentialId);
        protocolFields.credentials.trigger('change');
        SW.SRM.TestConnectionUI.HideError(ns);
    };

    ns.focusCredentialDisplayName = function(protocol) {
        var protocolFields = fields.getFieldsForProtocol(protocol);
        protocolFields.credentialName.focus();
        SW.SRM.TestConnectionUI.HideError(ns);
    };

    ns.showDialog = function() {
        loadCredentialDropdowns();

        if (!dialog) {
            dialog = new Ext.Window({
                applyTo: 'AddProviderFrame',
                id: 'dialog',
                layout: 'fit',
                width: 1000,
                height: 900,
                modal: true,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'AddProviderBody'
                }),
                buttons: [
                    {
                        id: 'save',
                        disabled: true,
                        text: '@{R=SRM.Strings;K=Add_Provider_Save_ButtonText; E=js}',
                        handler: function () {
                            ns.testCredentials(true);
                        }
                    },
                    {
                        text: '@{R=SRM.Strings;K=Add_Provider_Close_ButtonText; E=js}',
                        style: 'margin-left: 5px;',
                        handler: function () {
                            ns.cancelTestCredentials();
                            dialog.hide();
                        }
                    }],
                center: function () {
                    var pos = this.el.getCenterXY(true);
                    this.setPosition(pos[0], pos[1]);
                }
            });

            window.dialog = dialog;
        }

        dialog.center();
        dialog.show();

        clearForm();
    };

    ns.copySmisUsernameToApi = function() {
        copyTextWhenUsingSmisCredentials(fields.smis.username, fields.api.username);
    };

    ns.copySmisPasswordToApi = function() {
        copyTextWhenUsingSmisCredentials(fields.smis.password, fields.api.password);
    };

    ns.copySmisPasswordConfirmToApi = function() {
        copyTextWhenUsingSmisCredentials(fields.smis.passwordConfirm, fields.api.passwordConfirm);
    };

    ns.useSmisCredentialsForApi = function(use) {
        var fieldsToToggle = [fields.api.username, fields.api.password, fields.api.passwordConfirm];
        var labelsToToggle = [fields.api.labels.username, fields.api.labels.password, fields.api.labels.passwordConfirm];

        if (!use) {
            $.each(fieldsToToggle,
                function(i, element) {
                    element.removeAttr('disabled');
                });

            $.each(labelsToToggle,
                function(i, element) {
                    element.removeClass('unselectable');
                });
        } else {
            $.each(fieldsToToggle,
                function(i, element) {
                    element.attr('disabled', 'disabled');
                });

            $.each(labelsToToggle,
                function(i, element) {
                    element.addClass('unselectable');
                });

            copySmisCredentialsToApi();
        }
    };

    ns.toggleSettingsVisibility = function(speed) {
        speed = typeof speed === 'undefined' || speed === null ? config.animationSpeed : speed;
        var toggleAdvancedSettings = function() { 
            var smisSettingsVisible = fields.smis.settings.is(':visible'),
                apiSettingsVisible = fields.api.settings.is(':visible');

            if (!smisSettingsVisible && !apiSettingsVisible) {
                fields.expander.container.hide();
            } else {
                fields.expander.container.show();
            }

            if (!smisSettingsVisible || !apiSettingsVisible) {
                fields.api.useSmisCredentialsContainer.hide();
                if (fields.api.useSmisCredentials.prop('checked')) {
                    fields.api.useSmisCredentials.click();
                }
            } else {
                fields.api.useSmisCredentialsContainer.show();
            }
        };

        var toggleVisibility = function(protocolFields) {
            if (protocolFields.credentials.val() || protocolFields.credentials.val() === "<New Credential>") {
                protocolFields.advancedSettings.hide(speed);
                protocolFields.settings.hide(speed, toggleAdvancedSettings);
            } else {
                if (!fields.expander.isCollapsed()) {
                    protocolFields.advancedSettings.show(speed);
                }
                protocolFields.settings.show(speed, toggleAdvancedSettings);
            }
        };

        toggleVisibility(fields.smis);
        toggleVisibility(fields.api);
    };

    function initFromEditProvider() {
       TriggerOnChangeEvent();
        $('#ConnetionPropertyChangedField').val('true');
    }
    function TriggerOnChangeEvent() {
        $("#uxSmisCredentialsChoice").trigger('change');
        $("#uxApiCredentialsChoice").trigger('change');
    }
    ns.testCredentialsFromEditProviders = function (doSaveProviders) {
        cancel = false;
        doSaveProviders = doSaveProviders || false;
        if (isFormValid()){
            SW.SRM.TestConnectionUI.HideError(ns);
        } else {
            return false;
        }

        fields.testConnectionButton.disable();
        SW.SRM.TestConnectionUI.HidePass(ns);
        SW.SRM.TestConnectionUI.ShowLoading(true, ns);

        return $.when(testSmisCredentialsFromEditCredentials(doSaveProviders), testApiCredentials(doSaveProviders))
            .done(function (smisResult, apiResult) {
                SW.SRM.TestConnectionUI.ShowLoading(false, ns);
                SW.SRM.TestConnectionUI.ShowPassMessageBox(null, ns);
                if (doSaveProviders) {
                    saveProviders(smisResult, apiResult);
                } else {
                    fields.testConnectionButton.enable();
                    switchSaveButton(true);
                }
            })
            .fail(function (smisResult, apiResult) {
                fields.testConnectionButton.enable();
                if (cancel) { cancel = false; return; }

                SW.SRM.TestConnectionUI.ShowLoading(false, ns);
                if (typeof smisResult !== 'undefined' && smisResult !== null) {
                    handleError(smisResult.Message, smisResult.ErrorMessage);
                } else if (typeof apiResult !== 'undefined' && apiResult !== null) {
                    handleError(apiResult.Message, apiResult.ErrorMessage);
                } else {
                    handleError(config.connectionTestUnexpectedFailedMessage);
                }
            });
    };

    function testSmisCredentialsFromEditCredentials() {
        var credentialId = fields.smis.credentials.val() === null || fields.smis.credentials.val().length === 0 ? -1 : parseInt(fields.smis.credentials.val());

        var cached = $.Deferred();
        var request = SW.SRM.Common.CallSrmWebService(
            '/Orion/SRM/Services/EnrollmentWizardService.asmx',
            'TestSmisCredentials',
            {
                'credentialID': credentialId,
                'credentialName': fields.smis.credentialName.val(),
                'userName': fields.smis.username.val(),
                'password': fields.smis.password.val(),
                'useHttp': fields.smis.useHttp.is(':checked'),
                'httpPort': parseInt(fields.smis.httpPort.val()) || 5988,
                'httpsPort': parseInt(fields.smis.httpsPort.val()) || 5989,
                'interopNamespace': fields.smis.interopNamespace.val(),
                'arrayNamespace': fields.smis.arrayNamespace.val(),
                'ip': fields.providerIp.val()
            },
            function (result) {
                if (cancel) {
                    cached.reject();
                    return;
                }

                if (result.IsSuccess) {
                    cached.resolve(result);
                }
                else {
                    cached.reject(result);
                }

                activeRequests.splice(activeRequests.indexOf(request), 1);
            },
            function () {
                cached.reject();
            }
        );

        activeRequests.push(request);
        return cached.promise();
    }
    ns.toggleExpander = function() {
        if (fields.expander.isCollapsed()) {
            fields.expander.expand();
        } else {
            fields.expander.collapse();
        }
    };
})(SW.Core.namespace('SW.SRM.AddSmisAndHttpProvider'));