﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResponderProviderStep.ascx.cs" Inherits="Orion_SRM_Config_Controls_ResponderProviderStep" %>
<%@ Import Namespace="Resources" %>

<orion:include ID="Include1" runat="server" file="OrionCore.js" />
<orion:Include ID="Include3" runat="server" File="ResponderStep.js" Module="SRM" />

<style type="text/css">
</style>

<script type="text/javascript">

    onClickNextButtonEvent = function () {
        SW.SRM.SnapshotSelector.storeStorageArray();
    };

    function RunDiscovery() {
        SW.SRM.SnapshotSelector.reload();
    };

</script>

<span><%= SrmWebContent.AddStorage_ProviderStep_Responder_SubTitle%></span>
<div class="srm-enrollment-wizard-content">
    <div class="srm-main-content">
        <table class="srm-EnrollmentWizard-providerStepContentTable">
            <tr>
                <td>
                    <table class="srm-EnrollmentWizard-providerStepInputsTable">
                        <tr>
                            <td><%= SrmWebContent.Provider_Step_Array_Type%>:</td>
                            <td>
                                <b>
                                    <asp:Label runat="server" ID="ArrayTypeLabel" />
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td><%= SrmWebContent.Provider_Step_Responder_IP%>*:</td>
                            <td>
                                <asp:TextBox runat="server" ID="IpAddressTextBox" ClientIDMode="Static" />
                            </td>
                        </tr>
                        <tr>
                            <td><%= SrmWebContent.Provider_Step_Responder_Port%>*:</td>
                            <td>
                                <asp:TextBox runat="server" ID="PortTextBox" ClientIDMode="Static" />
                            </td>
                        </tr>
                        <tr>
                            <td><%= SrmWebContent.Provider_Step_Responder_UserName%>*:</td>
                            <td>
                                <asp:TextBox runat="server" ID="UserNameTextBox" ClientIDMode="Static" />
                            </td>
                        </tr>
                        <tr>
                            <td><%= SrmWebContent.Provider_Step_Responder_Password%>*:</td>
                            <td>
                                <asp:TextBox runat="server" ID="PasswordTextBox" TextMode="Password" ClientIDMode="Static" />
                            </td>
                        </tr>
                        <tr>
                            <td><%= SrmWebContent.Provider_Step_Responder_SelectDeviceType%>*:</td>
                            <td>
                                <asp:DropDownList runat="server" ID="DeviceTypeDropDown" ClientIDMode="Static" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="button" id="runDiscoveryButton" class="providerStepButton" value="<%= SrmWebContent.Provider_Step_Responder_RunDiscoveryBtn%>" onclick="RunDiscovery();" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>

                </td>
            </tr>
        </table>
        <div id="gridPanel">
            <div id="Grid" />
        </div>
    </div>
</div>

<asp:HiddenField ID="SelectedSnapshotID" runat="server" ClientIDMode="Static" />
