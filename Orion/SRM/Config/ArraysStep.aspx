﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ArraysStep.aspx.cs" Inherits="Orion_SRM_Config_ArraysStep" MasterPageFile="~/Orion/SRM/Config/EnrollmentWizardPage.master" Title="<%$ Resources: SrmWebContent, AddStorage_Title %>" %>

<%@ Register Src="~/Orion/SRM/Config/Controls/LicenseBar.ascx" TagPrefix="srm" TagName="LicenseBar" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" File="OrionMinReqs.js" />
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="ArrayStep.js" Module="SRM" />
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <div class="srm-EnrollmentWizardPage_master_AdminContent">
        <table>
            <tr>
                <td>
                    <span class="ActionName"><%= Resources.SrmWebContent.AddStorage_ArrayStep_Title %></span><br />
                    <%= Resources.SrmWebContent.AddStorage_ArrayStep_SubTitle %>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <div class="x-toolbar" ></div>
                    <div id="gridPanel" >
                        <div id="Grid" />
                    </div>
                    <div class="srm-enrollment-wizard-discovery-result-link"><a href="javascript:SW.SRM.ArraySelector.showDiscoveryResult();" ><%= Resources.SrmWebContent.AddStorage_ArrayStep_DiscoveryResults %></a></div>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="LicenseWrapper">
                        <span><%= Resources.SrmWebContent.Array_Step_Licensing%>:</span><span class="smr-arraystep-licensing"><srm:LicenseBar runat="server" ID="licenseBar" /></span>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    
    <asp:HiddenField ID="SelectedArrayIDs" runat="server" ClientIDMode="Static" />
    
    <table class="NavigationButtons">
        <tr>
            <td>
                <div class="sw-btn-bar-wizard">
                    <orion:LocalizableButton ID="BackButton" runat="server" DisplayType="Secondary"
                        LocalizedText="Back" OnClick="imgbBack_Click" CausesValidation="false" />
                    <orion:LocalizableButton ID="NextButton" runat="server" DisplayType="Primary"
                        LocalizedText="Next" OnClick="imgbNext_Click" />
                    <orion:LocalizableButton ID="CancelButton" runat="server" DisplayType="Secondary"
                        LocalizedText="Cancel" OnClick="imgbCancel_Click" CausesValidation="false" />
                </div>
            </td>
        </tr>
    </table>
    
    <script type="text/javascript">
        $(function () {
            SW.SRM.ArraySelector.init('Grid', $('#gridPanel').width(), <%= IsExternalProvider ? 1 : 0%>);
        });
    </script>
</asp:Content>
