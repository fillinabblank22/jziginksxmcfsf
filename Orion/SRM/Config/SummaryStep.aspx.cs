﻿using System.Globalization;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Web.EnrollmentWizard;
using System;
using System.Linq;
using System.Collections.Generic;
using SolarWinds.SRM.Common.BL;

public partial class Orion_SRM_Config_SummaryStep : WizardPage, IStep
{
    public string Step { get { return "SummaryStep"; } }

    private Configuration configuration;

    protected void Page_Load(object sender, EventArgs e)
    {
        configuration = WizardWorkflowHelper.ConfigurationInfo;

        if (!this.IsPostBack)
        {
            labelArrayType.Text = configuration.DeviceGroup.Name;
            labelPollerName.Text = configuration.PollingConfig.ServerName;

            ProviderRepeater.DataSource = configuration.ProviderConfiguration.SelectedProviders.Select(p => p.FriendlyName).Distinct().ToList();
            ProviderRepeater.DataBind();

            SummaryStepRepeater.DataSource = configuration.ProviderConfiguration.DiscoveryProvider.Arrays.Where(a => a.IsSelected);
            SummaryStepRepeater.DataBind();

            HashSet<String> licensedObjects;
            using (var proxy = BusinessLayerFactory.Create())
            {
                licensedObjects = new HashSet<String>(proxy.GetLicensedObjects());
            }

            licenseBar.Required = configuration.ProviderConfiguration.DiscoveryProvider.Arrays.Where(a => a.IsSelected && !licensedObjects.Contains(a.ID)).Sum(a => a.Disks);
        }
    }

    public string SubTitle
    {
        get
        {
            return String.Format(CultureInfo.InvariantCulture, SrmWebContent.AddStorage_SummaryStep_SubTitle,
                SrmWebContent.Summary_Step_Finish_Button_Text);
        }
    }

    protected override bool ValidateUserInput()
    {
        return true;
    }

    protected override void Next()
    {
    }
}