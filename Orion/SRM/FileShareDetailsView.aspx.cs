﻿using System;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web.NetObjects;

public partial class Orion_SRM_FileShareDetailsView : OrionView, IFileSharesProvider
{
    protected override void OnInit(EventArgs e)
    {
        this.Title = UIHelper.NormalizeSpaces(this.ViewInfo.ViewTitle);
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();
        base.OnInit(e);
    }

    public override string ViewType
    {
        get { return "SRM File Share Details"; }
    }

    public FileShares FileShares
    {
        get
        {
            return new FileShares(NetObject.NetObjectID);
        }
    }
}