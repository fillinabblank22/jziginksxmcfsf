﻿using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.SRM.Common;

public partial class Orion_SRM_ClusterDetailsView : OrionView, IStorageArrayProvider, IModelProvider
{
    protected override void OnInit(EventArgs e)
    {
        this.Title = SolarWinds.Orion.Web.Helpers.UIHelper.NormalizeSpaces(this.ViewInfo.ViewTitle);
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    public override void SelectView()
    {
        string netObject = this.Request.QueryString["NetObject"];
        if (netObject.Split(':')[0] == StorageArray.NetObjectPrefix)
        {
            try
            {
                this.NetObject = NetObjectFactory.Create(netObject);
            }
            catch (AccountLimitationException ex)
            {
                this.Server.Transfer(string.Format((IFormatProvider)CultureInfo.InvariantCulture, "/Orion/AccountLimitationError.aspx?NetObject={0}", new object[1]
          {
            (object) netObject
          }));
            }
            catch (IndexOutOfRangeException ex)
            {
                OrionErrorPageBase.TransferToErrorPage(this.Context, new ErrorInspectorDetails()
                {
                    Error = (Exception)ex
                });
            }

            if (this.Array.Array.IsCluster)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["ViewID"]))
                    this.ViewInfo = ViewManager.GetViewById(Convert.ToInt32(this.Request.QueryString["ViewID"]));
                else
                    this.ViewInfo = ViewManager.GetViewsByType(this.ViewType).First();
            }
            else
            {
                base.SelectView();
            }
        }
        else
            base.SelectView();

        string propertyNameByViewType = ViewManager.GetUserPropertyNameByViewType(this.ViewType);
        ProfileBase profile = HttpContext.Current.Profile;
        int viewId = (int)profile.GetPropertyValue(propertyNameByViewType);
        if (viewId == 0)
        {
            OrionErrorPageBase.TransferToErrorPage(this.Context, new ErrorInspectorDetails()
            {
                Error = new LocalizedExceptionBase((Func<string>) (() => SrmWebContent.RestrictedPageMessage)),
                Title = SrmWebContent.RestrictedPageTitle
            });
        }
        
        if (viewId == -1)
        {
            this.ViewInfo = this.GetViewForDeviceType();
        }
        else
        {
            this.ViewInfo = ViewManager.GetViewById(this.ViewInfo.ViewID);
            if (this.ViewInfo != null)
                return;
            this.ViewInfo = this.GetDefaultViewForViewType();
        }
    }

    public override string ViewType
    {
        get { return "SRM Cluster Details"; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    public StorageArray Array
    {
        get { return new StorageArray(NetObject.NetObjectID); }
    }
    public IPageModelProvider PageModelProvider
    {
        get { return new ArrayPageModelProvider(Array); }
    }
}