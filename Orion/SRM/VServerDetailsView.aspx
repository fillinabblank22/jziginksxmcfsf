﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/View.master" AutoEventWireup="true" CodeFile="VServerDetailsView.aspx.cs" Inherits="Orion_SRM_VServerDetailsView" %>

<%@ Import Namespace="SolarWinds.SRM.Web" %>
<%@ Import Namespace="SolarWinds.SRM.Web.NetObjects" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register Src="~/Orion/SRM/Controls/BreadcrumbBar.ascx" TagPrefix="orion" TagName="BreadcrumbBar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ViewPageTitle" runat="Server">
    <orion:BreadcrumbBar runat="server" ID="BreadcrumbBar" />
    <h1>
        <%= ViewInfo.IsSubView ? ViewInfo.ViewGroupTitle : ViewInfo.ViewHtmlTitle%> 
	    - 
        <img class="StatusIcon" src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.VServers%>&status=<%=(int)((VServer)this.NetObject).VServerEntity.Status%>&size=small" />
        <a class="srm-breadcrumbbar-link" href="/Orion/View.aspx?NetObject=<%=(string)this.NetObject.NetObjectID%>">
            <%= this.NetObject.Name %>
        </a>
        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
        <%= ViewInfo.IsSubView ? ViewInfo.ViewHtmlTitle : string.Empty%>
    </h1>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="Server">
    <orion:ResourceHostControl ID="ResourceHostControl2" runat="server">
        <orion:ResourceContainer runat="server" ID="resContainer" />
    </orion:ResourceHostControl>
</asp:Content>

