﻿using System;
using System.Web;
using SolarWinds.Orion.Web.UI;

public partial class Srm_View : System.Web.UI.MasterPage
{
    public string GetRequestString()
    {
        return
            string.Format("/Orion/Admin/CustomizeView.aspx?ViewID={0}{1}&ReturnTo={2}", ((OrionView)Page).ViewInfo.ViewID, (!string.IsNullOrEmpty(Request["NetObject"]) ? string.Format("&NetObject={0}", HttpUtility.UrlEncode(Request["NetObject"])) : ""), ((OrionView)Page).ReturnUrl);
    }

    public string HelpFragment { get; set; }
}
