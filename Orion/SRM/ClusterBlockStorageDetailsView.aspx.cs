﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web.NetObjects;

public partial class Orion_SRM_ClusterBlockStorageDetailsView : OrionView, IStorageArrayProvider
{
    protected override void OnInit(EventArgs e)
    {
        this.Title = SolarWinds.Orion.Web.Helpers.UIHelper.NormalizeSpaces(this.ViewInfo.ViewTitle);
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    public override string ViewType
    {
        get { return "SRM Cluster Block Storage Details"; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public StorageArray Array
    {
        get { return new StorageArray(NetObject.NetObjectID); }
    }
}