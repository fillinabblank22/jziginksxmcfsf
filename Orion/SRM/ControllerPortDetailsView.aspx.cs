﻿using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SRM.Web.NetObjects;

public partial class ControllerPortDetailsView : OrionView
{
    public override string ViewType
    {
        get
        {
            return "SRM Storage Controller Port Details";
        }
    }

    protected override void OnInit(EventArgs e)
    {
        this.Title = UIHelper.NormalizeSpaces(this.ViewInfo.ViewTitle);

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // We redirect to the parent (Storage Controller), because currently we do not have StorageControllerPortDetailsView
        // The port details are presented inside the port list resource on the ControllerDetailsView
        Response.Redirect(
            $"ControllerDetailsView.aspx?NetObject={StorageController.NetObjectPrefix}:{this.StorageControllerPort.StorageControllerPortEntity.StorageControllerId}");
    }

    public StorageControllerPort StorageControllerPort
    {
        get { return new StorageControllerPort(NetObject.NetObjectID); }
    }
}