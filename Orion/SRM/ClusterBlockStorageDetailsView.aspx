﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/View.master" AutoEventWireup="true" CodeFile="ClusterBlockStorageDetailsView.aspx.cs" Inherits="Orion_SRM_ClusterBlockStorageDetailsView" %>
<%@ Import Namespace="SolarWinds.SRM.Web" %>
<%@ Import Namespace="SolarWinds.SRM.Web.NetObjects" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ViewPageTitle" Runat="Server">
      <h1>
        <%= ViewInfo.IsSubView ? ViewInfo.ViewGroupTitle : ViewInfo.ViewHtmlTitle%> 
	    - 
        <img class="StatusIcon" src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.Clusters%>&status=<%=(int)((StorageArray)this.NetObject).Array.Status%>&size=small" />
        <%= this.NetObject.Name %>
	    <asp:PlaceHolder ID="TitleDecorators" runat="server"></asp:PlaceHolder>	    
        <%= ViewInfo.IsSubView ? ViewInfo.ViewHtmlTitle : string.Empty%>
    </h1>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" Runat="Server">
     <orion:ResourceHostControl ID="ResourceHostControl1" runat="server">
        <orion:ResourceContainer runat="server" ID="resContainer" />
    </orion:ResourceHostControl>
</asp:Content>

