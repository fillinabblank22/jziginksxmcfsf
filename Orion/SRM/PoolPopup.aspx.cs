﻿using System;
using System.Globalization;
using System.Web.UI;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Web.Models;

public partial class Orion_SRM_PoolPopup : Page
{
    protected Pool Pool { get; set; }
    public int ErrorLevel { get; set; }
    public int WarningLevel { get; set; }
    /// <summary>
    /// Gets or sets critical threshold days property.
    /// </summary>
    protected int CriticalThresholdDays { get; set; }
    /// <summary>
    /// Gets or sets warning threshold days property.
    /// </summary>
    protected int WarningThresholdDays { get; set; }
    /// <summary>
    /// Gets or sets capacity projected runout property.
    /// </summary>
    public int CapacityProjectedRunout { get; set; }

    protected override void OnPreInit(EventArgs e)
    {
        string netObjectID = Request.QueryString["NetObject"];
        try
        {
            this.Pool = NetObjectFactory.Create(netObjectID) as Pool;
        }
        catch (AccountLimitationException)
        {
            Server.Transfer(
                string.Format(CultureInfo.InvariantCulture, "/Orion/SRM/AccountLimitationPopup.aspx?NetObject={0}", netObjectID));
        }

        base.OnPreInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        WarningThresholdDays = (int)SettingsDAL.GetSetting(SRMConstants.WarningThresholdDays).SettingValue;
        CriticalThresholdDays = (int)SettingsDAL.GetSetting(SRMConstants.CriticalThresholdDays).SettingValue;

        if (Pool != null)
        {
            StatusLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", SrmWebContent.Pool_Popup_Pool_Name, SrmStatusLabelHelper.GetStatusLabel(Pool.PoolEntity.Status));
            WebHelper.SetValueOrHide(VendorStatusLabel, VendorStatusRow, Pool.PoolEntity.VendorStatusDescription);
            if( Pool.StorageArray.Array.IsCluster )
                ClusterLabel.Text = Pool.StorageArray.Name;
            else
                ArrayLabel.Text = Pool.StorageArray.Name;
            ArrayRow.Visible = !String.IsNullOrEmpty(ArrayLabel.Text);
            ClusterRow.Visible = !String.IsNullOrEmpty(ClusterLabel.Text);
            TypeLabel.Text = GetPoolType(Pool.PoolEntity.Type);
            TypeRow.Visible = !String.IsNullOrEmpty(TypeLabel.Text);

            if (!String.IsNullOrEmpty(Pool.PoolEntity.RAIDType))
            {
                lblRAIDConfig.Text = Pool.PoolEntity.RAIDType;
                trRaidConfig.Visible = true;
            }

            var dal = ServiceLocator.GetInstance<IThresholdDAL>();
            lblUserCapcity.Text = WebHelper.FormatCapacityTotal(Pool.PoolEntity.CapacityUserTotal);
            lblUsed.Text = WebHelper.FormatCapacityTotal(Pool.PoolEntity.CapacityUserUsed);
            WebHelper.MarkThresholds(lblUsed, (float)Pool.PoolEntity.ProvisionedCapacityPercent, dal.GetThresholdDynamicOrGlobal(ThresholdDataType.PoolProvisionedPercent, Pool.PoolEntity.PoolID));

            IResourceVisibilityService visibilityService = ServiceLocator.GetInstance<IResourceVisibilityService>();
            performanceTable.Visible = visibilityService.IsPerformanceAvailableForArray(this.Pool.StorageArray);

            if (performanceTable.Visible)
            {
                SetPerformanceData(this.Pool, dal);
            }

            if (Pool.PoolEntity.CapacityRunOut.HasValue)
            {
                CapacityProjectedRunout = WebHelper.GetDayDifference(Pool.PoolEntity.CapacityRunOut.Value);
                lblProjectedRunOut.Text = WebHelper.FormatDaysDiff(CapacityProjectedRunout);
                WebHelper.MarkProjectedRunOut(
                    lblProjectedRunOut,
                    CapacityProjectedRunout,
                    WarningThresholdDays,
                    CriticalThresholdDays);
            }

            WebHelper.SetValueOrHide(lblDataReduction, DataReductionRow, WebHelper.FormatDataReduction(Pool.PoolEntity.DataReduction));
            //WebHelper.SetValueOrHide(lblTotalReduction, TotalReductionRow, WebHelper.FormatDataReduction(Pool.PoolEntity.TotalReduction));
            
            popupChartUserSummary.ChartColors = new[] { PopupChartHelper.BlueChartColor, PopupChartHelper.GradientChartColor, PopupChartHelper.PinkChartColor };
            popupChartUserSummary.ChartLabelColors = new[] { PopupChartHelper.BluetFontColor, PopupChartHelper.GradientFontColor, PopupChartHelper.PinkFontColor };
            popupChartUserSummary.Values = new[] { /*Used*/Pool.PoolEntity.CapacityUserUsed, /*free*/Pool.PoolEntity.CapacityUserFree, /*oversubscribe*/Pool.PoolEntity.CapacityOversubscribed };
            popupChartUserSummary.Total = /*Total*/Pool.PoolEntity.CapacityUserTotal;
            popupChartUserSummary.RenderTotalLabel = true;
            popupChartUserSummary.Height = PopupChartHelper.Height;
            popupChartUserSummary.Oversubscribed = Pool.PoolEntity.CapacityOversubscribed;


            #region Other potential issues - Marked by thresholds

            IOPSReadLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(Pool.PoolEntity.IOPSRead ?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.PopUp_IOPSUnit);
            WebHelper.MarkThresholdAndShowRow(IOPSReadLabel, IOPSReadRow, Pool.PoolEntity.IOPSRead,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.PoolIOPSRead, Pool.PoolEntity.PoolID));

            IOPSWriteLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(Pool.PoolEntity.IOPSWrite ?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.PopUp_IOPSUnit);
            WebHelper.MarkThresholdAndShowRow(IOPSWriteLabel, IOPSWriteRow, Pool.PoolEntity.IOPSWrite,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.PoolIOPSWrite, Pool.PoolEntity.PoolID));
          
            ThroughputReadLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", WebHelper.FormatCapacityTotal(Pool.PoolEntity.BytesPSRead), SrmWebContent.PopUp_secUnit);
            WebHelper.MarkThresholdAndShowRow(ThroughputReadLabel, ThroughputReadRow, Pool.PoolEntity.BytesPSRead,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.PoolThroughputRead, Pool.PoolEntity.PoolID));
            
            ThroughputWriteLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", WebHelper.FormatCapacityTotal(Pool.PoolEntity.BytesPSWrite), SrmWebContent.PopUp_secUnit);
            WebHelper.MarkThresholdAndShowRow(ThroughputWriteLabel, ThroughputWriteRow, Pool.PoolEntity.BytesPSWrite,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.PoolThroughputWrite, Pool.PoolEntity.PoolID));
           
            IOSizeTotalLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0}", WebHelper.FormatCapacityTotal(Pool.PoolEntity.IOSizeTotal));
            WebHelper.MarkThresholdAndShowRow(IOSizeTotalLabel, IOSizeTotalRow, Pool.PoolEntity.IOSizeTotal,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.PoolIOSizeTotal, Pool.PoolEntity.PoolID));
           
            IOSizeReadLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0}", WebHelper.FormatCapacityTotal(Pool.PoolEntity.IOSizeRead));
            WebHelper.MarkThresholdAndShowRow(IOSizeReadLabel, IOSizeReadRow, Pool.PoolEntity.IOSizeRead,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.PoolIOSizeRead, Pool.PoolEntity.PoolID));
            
            IOSizeWriteLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0}", WebHelper.FormatCapacityTotal(Pool.PoolEntity.IOSizeWrite));
            WebHelper.MarkThresholdAndShowRow(IOSizeWriteLabel, IOSizeWriteRow, Pool.PoolEntity.IOSizeWrite,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.PoolIOSizeWrite, Pool.PoolEntity.PoolID));


            if (!IOSizeTotalRow.Visible && !IOSizeReadRow.Visible && !IOSizeWriteRow.Visible && !IOPSReadRow.Visible && !IOPSWriteRow.Visible && !ThroughputReadRow.Visible && !ThroughputWriteRow.Visible)
                OtherPotentialIssuesSection.Visible = false;
            #endregion
        }
    }

    private void SetPerformanceData(Pool pool, IThresholdDAL thresholdDal)
    {
        NetObjectPerformance poolPerformance = ServiceLocator.GetInstance<IPoolDAL>().GetPoolPerformance(pool.PoolEntity.PoolID);

        if (poolPerformance == null)
        {
            return;
        }

        lblTotalIOPS.Text = poolPerformance.TotalIOPS.HasValue
            ? string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round((Single)poolPerformance.TotalIOPS, MidpointRounding.AwayFromZero), SrmWebContent.PopUp_IOPSUnit)
            : SrmWebContent.Value_Unknown;
        WebHelper.MarkThresholds(lblTotalIOPS, pool.PoolEntity.IOPSTotal, thresholdDal.GetThresholdDynamicOrGlobal(ThresholdDataType.PoolIOPSTotal, pool.PoolEntity.PoolID));

        lblTotalThroughput.Text = poolPerformance.TotalThroughput.HasValue
            ? string.Format(CultureInfo.CurrentCulture, "{0} {1}", WebHelper.FormatCapacityTotal((Single)poolPerformance.TotalThroughput), SrmWebContent.PopUp_secUnit)
            : SrmWebContent.Value_Unknown;
        WebHelper.MarkThresholds(lblTotalThroughput, pool.PoolEntity.BytesPSTotal, thresholdDal.GetThresholdDynamicOrGlobal(ThresholdDataType.PoolThroughputTotal, pool.PoolEntity.PoolID));

        if (poolPerformance.Latency.HasValue)
        {
            lblTotalLatency.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round((Single)poolPerformance.Latency, MidpointRounding.AwayFromZero), SrmWebContent.PopUp_msUnit);
        }
        else
        {
            totalLatencyRow.Visible = false;
        }
    }

    private string GetPoolType(PoolType StorageGroupType)
    {
        string resultType = string.Empty;
        switch (StorageGroupType)
        {
            case PoolType.RaidGroup:
                resultType = SrmWebContent.Pools_List_RaidGroup;
                break;
            case PoolType.StoragePool:
                resultType = SrmWebContent.Pools_List_StoragePool;
                break;
            case PoolType.Aggregate:
                resultType = SrmWebContent.Pools_List_AggregatePool;
                break;
        }
        return resultType;
    }
}