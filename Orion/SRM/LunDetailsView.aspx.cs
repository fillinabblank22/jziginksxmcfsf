﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web.NetObjects;

public partial class Orion_SRM_LUNDetailsView : OrionView, ILunProvider
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = this.ViewInfo.ViewTitle;
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    public override string ViewType
    {
        get { return "SRM LUN Details"; }
    }

    public Lun Lun
    {
        get { return new Lun(NetObject.NetObjectID); }
    }
}