﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/View.master" CodeFile="ProviderDetailsView.aspx.cs" Inherits="Orion_SRM_ProviderDetailsView" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="orion" TagName="Refresher" Src="~/Orion/Controls/Refresher.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ViewPageTitle">
	<h1>
	    <%= ViewInfo.ViewGroupTitle%>
        <%= ViewInfo.ViewHtmlTitle %> 
	</h1>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MainContentPlaceHolder">
	<orion:ResourceHostControl ID="ResourceHostControl1" runat="server">
		<orion:ResourceContainer runat="server" ID="resContainer" />
	</orion:ResourceHostControl>
</asp:Content>