﻿using System;
using System.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Helpers;
// NOTE:  This class is basically the same as the /Orion/NetPerfMon/Resources/EditResource.aspx page.  That 
//        page is used for resouces that just provide a edit control.  Charts would like to do this, but that
//        means that our edit control would get used on the Edit Custom Object Resource page.  This page does
//        not support edit controls that make use of view state. (The page is horrible becuase of many different
//        reasons, but the lack of view state is the excuse I'm using today).  This page is designed just for 
//        editing charts but we'll keep the main part of the page in a user control so that it will be easier
//        to move back to that once the COR edit page is fixed.
using SolarWinds.SRM.Web.Resources;

public partial class Orion_Charts_EditTopXXChartResource : Page
{
    private BaseResourceEditControl _editChartControl;
    private BaseResourceEditControl _editTopXXControl;
    private bool showSectionAdvanced = true;

    public ResourceInfo Resource { get; set; }
    public string NetObjectId { get; set; }
    public int ViewId { get; set; }
    public string EditXx { get; private set; }
    public bool IsEditXx { get; private set; }
    
    public void SubmitClick(object sender, EventArgs e)
    {
        if (Resource == null)
            return;

        Page.Validate();
        if (!Page.IsValid)
            return;

        string resTitle = TitleEditControl.ResourceTitle;
        string resSubTitle = TitleEditControl.ResourceSubTitle;

        if (_editChartControl != null)
        {
            foreach (var property in _editChartControl.Properties)
            {
                if (Resource.Properties.ContainsKey(property.Key))
                    Resource.Properties[property.Key] = property.Value.ToString();
                else
                    Resource.Properties.Add(property.Key, property.Value.ToString());
            }

            if (string.IsNullOrEmpty(resTitle) && !string.IsNullOrEmpty(_editChartControl.DefaultResourceTitle))
                resTitle = _editChartControl.DefaultResourceTitle;
            if (string.IsNullOrEmpty(resSubTitle) && !string.IsNullOrEmpty(_editChartControl.DefaultResourceSubTitle))
                resSubTitle = _editChartControl.DefaultResourceSubTitle;
            
            //Right now edit page does not support custom date ranges
            Resource.Properties.Remove("DateFromUtc");
            Resource.Properties.Remove("DateToUtc");
        }

        if ((_editTopXXControl != null) && (string.IsNullOrEmpty(EditXx) || IsEditXx))
        {
            foreach (var property in _editTopXXControl.Properties)
            {
                if (Resource.Properties.ContainsKey(property.Key))
                    Resource.Properties[property.Key] = property.Value.ToString();
                else
                    Resource.Properties.Add(property.Key, property.Value.ToString());
            }
            if (string.IsNullOrEmpty(resTitle) && !string.IsNullOrEmpty(_editTopXXControl.DefaultResourceTitle))
                resTitle = _editTopXXControl.DefaultResourceTitle;
            if (string.IsNullOrEmpty(resSubTitle) && !string.IsNullOrEmpty(_editTopXXControl.DefaultResourceSubTitle))
                resSubTitle = _editTopXXControl.DefaultResourceSubTitle;
        }

        if (String.IsNullOrEmpty(resTitle))
        {
            //The original localized title was destroyed. Let's fetch the control's default.
            using (var resourceControl = (BaseResourceControl) LoadControl(Resource.File))
            {
                Resource.Title = resourceControl.Title;
            }
        }
        else
        {
            Resource.Title = resTitle;
        }

        Resource.SubTitle = resSubTitle;

        ResourcesDAL.Update(Resource);

        Response.Redirect(
            BaseResourceControl.GetEditPageReturnUrl(
                Request.QueryString,
                ((ViewId > 0) ? ViewId : Resource.View.ViewID).ToString()));
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        int resourceId;
        Int32.TryParse(Request.QueryString["ResourceID"], out resourceId);
        Resource = ResourceManager.GetResourceByID(resourceId);

        int viewId;
        Int32.TryParse(Request.QueryString["ViewID"], out viewId);
        ViewId = viewId;

        NetObjectId = Request.QueryString["NetObject"];
        EditXx = Request.QueryString["EditXX"];
        
        bool isEdit;
        if (bool.TryParse(EditXx, out isEdit))
        {
            IsEditXx = isEdit;
        }

        if (Request.QueryString["ShowSectionAdvanced"] != null)
        {
            showSectionAdvanced = Convert.ToBoolean(Request.QueryString["ShowSectionAdvanced"]);
        }

        LoadResourceEditControl();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Resource != null)
        {
            if (!IsPostBack)
            {
                TitleEditControl.ResourceTitle = Resource.Title;
                TitleEditControl.ResourceSubTitle = Resource.SubTitle;
                ResourceName.Text = Resource.Title;
            }
            Title = String.Format(Resources.CoreWebContent.WEBCODE_AK0_88, Resource.Title);
        }
    }

    private void LoadResourceEditControl()
    {
        if (Resource == null)
            return;
        
        const string editControlLocation = "/Orion/NetPerfMon/Controls/EditResourceControls/EditChart.ascx";

        _editChartControl = LoadControl(editControlLocation) as BaseResourceEditControl;

        if (_editChartControl != null)
        {
            Control sectionAdvanced = _editChartControl.FindControl("SectionAdvanced");
            if (sectionAdvanced != null)
            {
                sectionAdvanced.Visible = showSectionAdvanced;
            }
            TitleEditControl.ShowSubTitle = _editChartControl.ShowSubTitle;
            TitleEditControl.ShowSubTitleHintMessage = _editChartControl.ShowSubTitleHintMessage;
            TitleEditControl.SubTitleHintMessage = _editChartControl.SubTitleHintMessage;
            _editChartControl.Resource = Resource;
            _editChartControl.NetObjectID = NetObjectId;
            phResourceEditControl.Controls.Add(_editChartControl.TemplateControl);
        }

        if (string.IsNullOrEmpty(EditXx) || IsEditXx)
        {
	        // Editor from core is fine-tuned and has hardcodes for nodes/interfaces, so we should use custom one from SRM
            const string editTopXXControlLocation = "/Orion/SRM/Controls/EditResourceControls/EditTopXX.ascx";

            _editTopXXControl = LoadControl(editTopXXControlLocation) as BaseResourceEditControl;

            if (_editTopXXControl != null)
            {
                _editTopXXControl.Resource = Resource;
                _editTopXXControl.NetObjectID = NetObjectId;
                phResourceEditTopXXControl.Controls.Add(_editTopXXControl.TemplateControl);
            }
        }

    }
}