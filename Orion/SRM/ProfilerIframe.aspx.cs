﻿using System;
using System.IO;
using System.Net;
using SolarWinds.Logging;
using System.Data;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Helpers;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;

public partial class Orion_SRM_ProfilerIframe : OrionView
{
    private readonly static Log log = new Log();

    protected string url;

    public override string ViewType
    {
        get { return "Storage Main Console"; }
    }

    protected override void OnInit(EventArgs e)
    {
        this.Title = this.ViewInfo.ViewTitle;

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["url"]))
        {
            STMIntegrationEntity details = null;
            using (var proxy = BusinessLayerFactory.CreateMain())
            {
                details = proxy.GetSTMIntegrationDetails();
            }

            string loginPath = InvariantString.Format("LoginServlet?loginState=checkLogin&u={0}&p={1}",
                                Server.UrlEncode(SrmEncryptionHelper.Encrypt(details.Credentials.Username, SrmEncryptionHelper.StmKey)),
                                Server.UrlEncode(SrmEncryptionHelper.Encrypt(details.Credentials.Password, SrmEncryptionHelper.StmKey)));
            string redirectPathAndQuery = Request["url"];

            // Get the URL which can be used to link the customer back to Orion.
            string orionStmSettingsUrl = GetOrionStorageManagerSettingsUrl();


            string orionStmSettingsUrlParam = InvariantString.Format("&orionStmSettingsUrl={0}",
                                               orionStmSettingsUrl);

            string urlString = InvariantString.Format("{0}{1}{2}&redirectUrl={3}",
                                details.WebSiteUrl,
                                loginPath,
                                orionStmSettingsUrlParam,
                                redirectPathAndQuery);
            url = urlString;
        }
        else
        {
            log.Error("Url was not specified.");
            throw new ArgumentException("You must specify a url.");
        }
    }

    private string GetOrionStorageManagerSettingsUrl()
    {
        // Default to null.
        string retOrionUrl = null;
        DataTable results = ServiceLocator.GetInstance<IWebsiteDAL>().GetSettingsTypePrimary();

        string serverName = null;
        string port = null;
        bool sslEnabled = false;
        foreach (DataRow row in results.Rows)
        {
            serverName = (string)row["ServerName"];
            port = row["Port"].ToString();
            sslEnabled = Convert.ToBoolean(row["SSLEnabled"]);
        }

        if (!string.IsNullOrEmpty(serverName) && !string.IsNullOrEmpty(port))
        {
            // Set the protocol.
            string protocol = "http";
            if (sslEnabled)
            {
                protocol = "https";
            }

            // Build the URL.
            retOrionUrl = InvariantString.Format("{0}://{1}:{2}/Orion/SRM/Admin/STMIntegration.aspx", protocol, serverName, port);
        }

        return retOrionUrl;
    }
}
