﻿using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.SRM.Web.NetObjects;

public partial class ControllerDetailsView : OrionView, IStorageControllerProvider
{
    public override string ViewType
    {
        get
        {
             return "SRM Controller Details"; 
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = UIHelper.NormalizeSpaces(this.ViewInfo.ViewTitle);
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    public StorageController StorageController
    {
        get { return new StorageController(NetObject.NetObjectID); }
    }

}