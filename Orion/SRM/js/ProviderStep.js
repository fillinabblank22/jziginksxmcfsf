﻿Ext.namespace('SW');
Ext.namespace('SW.SRM');

SW.SRM.ProviderSelector = function () {

    var selectorModel;
    var dataStore;
    var grid;
    var pageSizeNum;
    var texts = {};

    ORION.prefix = "SRM.ProviderSelector_";
    
    function onAfterPageChange() {
        var value = $("#SelectedProviderID").val();

        // mark selected, or preselect the first one
        grid.store.each(function (record, id) {
            if (value == '') {
                value = record.data.UniqueProviderIdentifier;
            }
            if (value == record.data.UniqueProviderIdentifier) {
                grid.getSelectionModel().selectRecords([record], true);
            }
        });

    };
    
    function onRowSelected(sm, rowIdx, r) {
        var value = $("#SelectedProviderID").val();
        if (value == r.data.UniqueProviderIdentifier) {
            return;
        }
        value = r.data.UniqueProviderIdentifier;
        $('input[type=radio]')[rowIdx].checked = 'checked';
        $("#SelectedProviderID").val(value);
    };

    return {
        setDefaultProvider: function (uniqueProviderIdentifier) {
            $("#SelectedProviderID").val(uniqueProviderIdentifier);
        },
        reload: function() {
            grid.getStore().reload();
        },

        initTexts: function(textPar) {
            texts = textPar;
        },


        afterRender: function() {
            SRM.ProviderSelector.superclass.afterRender.apply(this, arguments);
            this.el.on('click', this.checkRadioClick, this);
        },

        checkRadioClick: function (event) {
            if (event.getTarget('input[type="radio"]')) {
                var value = $("#SelectedProviderID").val();
                if (value == r.data.UniqueProviderIdentifier) {
                    return;
                }
                value = r.data.UniqueProviderIdentifier;

                $("#SelectedProviderID").val(value);
            }
        },

        init: function () {

            // load default page size from Orion settings
            pageSizeNum = parseInt(ORION.Prefs.load('PageSize', '10'));

            Ext.override(Ext.PagingToolbar, {
                addPageSizer: function () {
                    // add a combobox to the toolbar
                    var store = new Ext.data.SimpleStore({
                        fields: ['pageSize'],
                        data: [[10], [20], [30], [40], [50]]
                    });
                    var combo = new Ext.form.ComboBox({
                        regex: /^\d*$/,
                        store: store,
                        displayField: 'pageSize',
                        mode: 'local',
                        triggerAction: 'all',
                        selectOnFocus: true,
                        width: 50,
                        editable: false,
                        value: pageSizeNum
                    });
                    this.addField(combo);

                    combo.on("select", function (c, record) {
                        this.pageSize = record.get("pageSize");
                        this.cursor = 0;
                        ORION.Prefs.save('PageSize', this.pageSize);
                        this.doRefresh();
                    }, this);
                }
            });


            // set selection model to checkbox
            selectorModel = new Ext.grid.RowSelectionModel();

            // define data store for credential grid - use web service (method) for load of data
            dataStore = new ORION.WebServiceStore(
                "/Orion/SRM/Services/EnrollmentWizardService.asmx/GetProviders",
                [
                { name: 'UniqueProviderIdentifier', mapping: 0 },
                { name: 'FriendlyName', mapping: 1 },
                { name: 'CredentialName', mapping: 2 }
                ],
                "FriendlyName");


            // define grid panel
            grid = new Ext.grid.GridPanel({

                store: dataStore,

                columns: [                    
                    {
                        header: '', sortable: false, hideable: false, width: 30, fixed: true, dataIndex: 'UniqueProviderIdentifier', menuDisabled: true, renderer: function (value) {
                            var value1 = $("#SelectedProviderID").val();
                            return "<input type='radio' name = 'primaryRadio' " + ((value == value1) ? "checked='checked'" : "") + "/>";
                        }
                    },
                    { header: '@{R=SRM.Strings;K=Provider_Step_Header_Col_Provider;E=xml}', hideable: false, sortable: true, dataIndex: 'FriendlyName', menuDisabled: true, renderer: function(value) {
                            var statusAndName = value.split('_');
                            return String.format("<img src='/Orion/StatusIcon.ashx?entity={0}&status={1}&size=small' />&nbsp;{2}", swisEntityProviders, statusAndName[0], statusAndName[1]);
                        }
                    },
                    { header: '@{R=SRM.Strings;K=Provider_Step_Header_Col_Credentials;E=xml}', hideable: false, sortable: true, dataIndex: 'CredentialName', menuDisabled: true }
                ],

                sm: selectorModel,

                viewConfig: {
                    forceFit: true,
                    deferEmptyText: false,
                    emptyText: '<div class="srm-provider-step-no-more-data-available">' + texts.noDataAvailable + '</div>' // @{R=SRM.Strings;K=Provider_Step_NoDataAvailable;E=xml}
                },

                //width has to be smaller here than div width on page
                //grid will be resized when it's rendered
                width: 400,
                autoHeight: true,
                stripeRows: true,
                loadMask: { msg: '@{R=SRM.Strings;K=Provider_Step_LoadingData;E=xml}' },
                bbar: new Ext.PagingToolbar({
                    store: dataStore,
                    pageSize: pageSizeNum,
                    displayInfo: true,
                    listeners:
                        {
                            change: onAfterPageChange
                        }
                })
            });
            
            grid.getView().on('refresh', function () {
                var obj = $('.x-grid3-hd-checker');
                if (obj && obj.hasClass('x-grid3-hd-checker-on'))
                    obj.removeClass('x-grid3-hd-checker-on');
            });
            
            grid.getSelectionModel().on('rowselect', onRowSelected);

            grid.render('Grid');
            grid.bottomToolbar.addPageSizer();
            grid.setWidth($('#gridPanel').width());
            grid.store.proxy.conn.jsonData = { };
            grid.store.load();
        }
    };

} ();


Ext.onReady(SW.SRM.ProviderSelector.init, SW.SRM.ProviderSelector);
