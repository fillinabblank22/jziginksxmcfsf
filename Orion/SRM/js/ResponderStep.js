﻿SW = SW || {};
SW.SRM = SW.SRM || {};
SW.SRM.SnapshotSelector = function () {

    var selectorModel;
    var dataStore;
    var grid;
    var pageSizeNum;
    var texts = {};

    ORION.prefix = "SRM.SnapshotSelector_";

    function onAfterPageChange() {
        var value = $("#SelectedSnapshotID").val();
        grid.store.each(function (record, id) {
            if (value == record.data.UniqueSnapshotIdentifier) {
                grid.getSelectionModel().selectRecords([record], true);
            }
        });
    };

    function onRowSelected(sm, rowIdx, r) {
        var value = $("#SelectedSnapshotID").val();
        if (value == r.data.UniqueSnapshotIdentifier) {
            return;
        }
        value = r.data.UniqueSnapshotIdentifier;

        $("#SelectedSnapshotID").val(value);
    };

    return {
        reload: function () {
            grid.store.proxy.conn.jsonData = {
                ipAddress: $('#IpAddressTextBox').val(),
                userName: $('#UserNameTextBox').val(),
                password: $('#PasswordTextBox').val(),
                port: $('#PortTextBox').val(),
                templateId: $('#DeviceTypeDropDown').val()
            };
            grid.getStore().reload();
        },

        initTexts: function (textPar) {
            texts = textPar;
        },

        afterRender: function () {
            SRM.ProviderSelector.superclass.afterRender.apply(this, arguments);
            this.el.on('click', this.checkRadioClick, this);
        },

        checkRadioClick: function (event) {
            if (event.getTarget('input[type="radio"]')) {
                var value = $("#SelectedSnapshotID").val();
                if (value == r.data.UniqueSnapshotIdentifier) {
                    return;
                }
                value = r.data.UniqueSnapshotIdentifier;

                $("#SelectedSnapshotID").val(value);
            }
        },

        init: function () {

            // load default page size from Orion settings
            pageSizeNum = parseInt(ORION.Prefs.load('PageSize', '10'));

            Ext.override(Ext.PagingToolbar, {
                addPageSizer: function () {
                    // add a combobox to the toolbar
                    var store = new Ext.data.SimpleStore({
                        fields: ['pageSize'],
                        data: [[10], [20], [30], [40], [50]]
                    });
                    var combo = new Ext.form.ComboBox({
                        regex: /^\d*$/,
                        store: store,
                        displayField: 'pageSize',
                        mode: 'local',
                        triggerAction: 'all',
                        selectOnFocus: true,
                        width: 50,
                        editable: false,
                        value: pageSizeNum
                    });
                    this.addField(combo);

                    combo.on("select", function (c, record) {
                        this.pageSize = record.get("pageSize");
                        this.cursor = 0;
                        ORION.Prefs.save('PageSize', this.pageSize);
                        this.doRefresh();
                    }, this);
                }
            });

            // set selection model to checkbox
            selectorModel = new Ext.grid.RowSelectionModel();

            // define data store for credential grid - use web service (method) for load of data
            dataStore = new ORION.WebServiceStore(
                "/Orion/SRM/Services/EnrollmentWizardService.asmx/GetSnapshots",
                [
                { name: 'UniqueSnapshotIdentifier', mapping: 0 },
                { name: 'ArrayName', mapping: 1 },
                { name: 'Vendor', mapping: 2 },
                { name: 'ArrayID', mapping: 3 },
                { name: 'IPAddress', mapping: 4 },
                { name: 'Created', mapping: 5 },
                { name: 'PollInterval', mapping: 6 },
                { name: 'SnaphotID', mapping: 7 },
                { name: 'IsCluster', mapping: 8 }
                ],
                "FriendlyName");

            // define grid panel
            grid = new Ext.grid.GridPanel({

                store: dataStore,

                columns: [
                    {
                        header: '', sortable: false, hideable: false, width: 30, fixed: true, dataIndex: 'UniqueSnapshotIdentifier', menuDisabled: true, renderer: function (value) {
                            var value1 = $("#SelectedSnapshotID").val();
                            return "<input type='radio' name = 'primaryRadio' " + ((value == value1) ? "checked='checked'" : "") + "/>";
                        }
                    },
                    { header: '@{R=SRM.Strings;K=Provider_Step_Header_Col_ArrayName;E=xml}', hideable: false, sortable: false, dataIndex: 'ArrayName', menuDisabled: true },
                    { header: '@{R=SRM.Strings;K=Provider_Step_Header_Col_ArrayVendor;E=xml}', hideable: false, sortable: false, dataIndex: 'Vendor', menuDisabled: true },
                    { header: '@{R=SRM.Strings;K=Provider_Step_Header_Col_StorageArrayID;E=xml}', hideable: false, sortable: false, dataIndex: 'ArrayID', menuDisabled: true },
                    { header: '@{R=SRM.Strings;K=Provider_Step_Header_Col_ArrayIP;E=xml}', hideable: false, sortable: false, dataIndex: 'IPAddress', menuDisabled: true },
                    { header: '@{R=SRM.Strings;K=Provider_Step_Header_Col_SnapshotDateTime;E=xml}', hideable: false, sortable: false, dataIndex: 'Created', menuDisabled: true },
                    { header: '@{R=SRM.Strings;K=Provider_Step_Header_Col_PollInterval;E=xml}', hideable: false, sortable: false, dataIndex: 'PollInterval', menuDisabled: true },
                    { header: '@{R=SRM.Strings;K=Provider_Step_Header_Col_SnapshotID;E=xml}', hideable: false, sortable: false, dataIndex: 'SnaphotID', menuDisabled: true }
                ],

                sm: selectorModel,

                viewConfig: {
                    forceFit: true,
                    deferEmptyText: false,
                    emptyText: '<div class="srm-provider-step-no-more-data-available">No snapshots.</div>'
                },

                //width has to be smaller here than div width on page
                //grid will be resized when it's rendered
                width: 400,
                autoHeight: true,
                stripeRows: true,
                loadMask: { msg: '@{R=SRM.Strings;K=Provider_Step_LoadingData;E=xml}' },
                bbar: new Ext.PagingToolbar({
                    store: dataStore,
                    pageSize: pageSizeNum,
                    displayInfo: true,
                    listeners:
                        {
                            change: onAfterPageChange
                        }
                })
            });

            grid.getView().on('refresh', function () {
                var obj = $('.x-grid3-hd-checker');
                if (obj && obj.hasClass('x-grid3-hd-checker-on'))
                    obj.removeClass('x-grid3-hd-checker-on');
            });

            grid.getSelectionModel().on('rowselect', onRowSelected);

            grid.render('Grid');
            grid.bottomToolbar.addPageSizer();
            grid.setWidth($('#gridPanel').width());
            grid.store.proxy.conn.jsonData = {
                ipAddress: $('#IpAddressTextBox').val(),
                port: $('#PortTextBox').val(),
                userName: $('#UserNameTextBox').val(),
                password: $('#PasswordTextBox').val(),
                templateId: $('#DeviceTypeDropDown').val()
            };
        },

        storeStorageArray: function () {
            var snapshotID = grid.getSelectionModel().selections.items[0].data.SnaphotID;
            var arrayName = grid.getSelectionModel().selections.items[0].data.ArrayName;
            var arrayID = grid.getSelectionModel().selections.items[0].data.ArrayID;
            var vendor = grid.getSelectionModel().selections.items[0].data.Vendor;
            var isCluster = grid.getSelectionModel().selections.items[0].data.IsCluster;
            var ipAddress = $('#IpAddressTextBox').val();
            var port = $('#PortTextBox').val();
            var templateId = $('#DeviceTypeDropDown').val();
            var userName = $('#UserNameTextBox').val();
            var password = $('#PasswordTextBox').val();

            ORION.callWebService(
                     "/Orion/SRM/Services/EnrollmentWizardService.asmx",
                     "StoreResponderStorageArray",
                     {
                         ipAddress: ipAddress,
                         port: port,
                         snapshotId: snapshotID,
                         storageArrayId: arrayID,
                         arrayName: arrayName,
                         vendor: vendor,
                         isCluster: isCluster,
                         templateId: templateId,
                         userName: userName,
                         password: password
                     },
                     function (isSuccess) {
                         if (isSuccess) {
                             Ext.Msg.alert(
                                 '@{R=SRM.Strings;K=AddStorage_ArraysStep_MessageBoxTitle;E=xml}',
                                 '@{R=SRM.Strings;K=Provider_Step_Msg_ImportingResponderArray_Success;E=xml}',
                                 function (btn, text) {
                                     providerStepSubmit();
                                 }
                             );
                         } else {
                             Ext.Msg.alert('@{R=SRM.Strings;K=AddStorage_ArraysStep_MessageBoxTitle;E=xml}', '@{R=SRM.Strings;K=Provider_Step_Msg_ImportingResponderArray_NotSuccess;E=xml}');
                         }
                     });
        }
    };

}();

Ext.onReady(SW.SRM.SnapshotSelector.init, SW.SRM.SnapshotSelector);