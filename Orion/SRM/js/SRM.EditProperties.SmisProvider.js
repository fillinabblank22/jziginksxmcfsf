﻿
var smisProvider = SW.Core.namespace("SW.SRM.EditProperties.SmisProvider");

var request;

smisProvider.getTestConnectionMessagesDiv = function() {
    return $("#testSmisProvider");
};

smisProvider.getTestConnectionButton = function() {
    return $("#testConnectionSmisButton");
};

smisProvider.onCancelClick = function () {
    if (request) {
        var requestToAbort = request;
        request = null;
        requestToAbort.abort();
    }
};

smisProvider.ValidateClarionBlockConnection = function () {
    // Validates Existing Clarion Block provider connection only
    var testCredentialDeferred = $.Deferred();
    var testGlobalCredentialDeferred = $.Deferred();

    var cfg = JSON.parse($('#hfEditPropertiesSmisCfg').val());
    var credentialId = cfg.CredentialId;
    var ip = cfg.IP;

    var engineid = $('#selPollingEngineId').length == 0 ? cfg.EngineId : $('#' + $('#selPollingEngineId').val()).val();

    request = SW.SRM.Common.CallSrmWebService("/Orion/SRM/Services/EnrollmentWizardService.asmx",
                                'TestSmisCredentialsProvider',
                                {
                                    'credentialID': credentialId,
                                    'ip': ip,
                                    'credentialName': null,
                                    'userName': null,
                                    'password': null,
                                    'useHttp': false,
                                    'httpPort': 0,
                                    'httpsPort': 0,
                                    'interopNamespace': null,
                                    'arrayNamespace': null,
                                    'engineid': engineid,
                                    'arrayId': cfg.ArrayId
                                },
                                function (result) {
                                    if (result.IsSuccess) {
                                        SW.SRM.TestConnectionUI.ShowLoading(false, SW.SRM.EditProperties.SmisProvider);
                                        SW.SRM.TestConnectionUI.ShowPassMessageBox(result.Message, SW.SRM.EditProperties.SmisProvider);
                                        testCredentialDeferred.resolve();
                                    }
                                    else {

                                        SW.SRM.TestConnectionUI.ShowLoading(false, SW.SRM.EditProperties.SmisProvider);

                                        var title = "@{R=SRM.Strings;K=Provider_Step_Provider_Connecting_Error; E=js}".concat(' ').concat(ip);
                                        SW.SRM.Common.ShowErrorEditProperties(title, result.Message, result.ErrorMessage);
                                        testCredentialDeferred.reject();
                                    }
                                },
                                function () {
                                    SW.SRM.TestConnectionUI.HandleError(SW.SRM.EditProperties.SmisProvider);

                                    if (request != null && request.status !== 0) {
                                        SW.SRM.Common.ShowErrorEditProperties('Error during AJAX call');
                                    }
                                    
                                    testCredentialDeferred.reject();
                                });

    $.when(testCredentialDeferred).then(
    function (x) {
        SW.SRM.TestConnectionUI.ShowLoading(false, SW.SRM.EditProperties.SmisProvider);
        testGlobalCredentialDeferred.resolve();
    },
    function () {
        SW.SRM.TestConnectionUI.ShowLoading(false, SW.SRM.EditProperties.SmisProvider);
        testGlobalCredentialDeferred.reject();
    });

    SW.SRM.TestConnectionUI.HideError(SW.SRM.EditProperties.SmisProvider);
    SW.SRM.TestConnectionUI.HidePass(SW.SRM.EditProperties.SmisProvider);
    SW.SRM.TestConnectionUI.ShowLoading(true, SW.SRM.EditProperties.SmisProvider);

    return testGlobalCredentialDeferred.promise();
};


smisProvider.ValidateDiscovery = function() {
    // This is the validation started by Submit buton

    var globalDiscoveryPromise = $.Deferred();
    if (!pollingProperties.ValidateUserInput()) {
        return globalDiscoveryPromise.reject();
    }

    // If Celerra block + file is filled then start Clarion Block discovery + Clarion File discovery (2 providers) in parallel
    // If Celerra block + file is empty, then validate just Clarion block discovery

    var cfg = JSON.parse($('#hfEditPropertiesSmisCfg').val());
    var engineid = $('#selPollingEngineId').length == 0 ? cfg.EngineId : $('#' + $('#selPollingEngineId').val()).val();

    var p1 = $.Deferred();
        p = {
            deferred: p1,
            providers: [
                {
                    hfName: 'hfSmisProviderGuid'
                }
            ],
            groupId: cfg.DeviceGroupId,
            validateBlock: 'true',
            arraySN: cfg.ArraySerialNumber,
            engineId: engineid,
            ip : cfg.IP
        };

        SW.SRM.TestConnectionUI.HideError(SW.SRM.EditProperties.SmisProvider);
        SW.SRM.TestConnectionUI.HidePass(SW.SRM.EditProperties.SmisProvider);
        SW.SRM.TestConnectionUI.ShowLoading(true, SW.SRM.EditProperties.SmisProvider);
        SW.SRM.EditProperties.SmisProvider.startDiscovery(p);

        $.when(p1).then(function (x) {
            SW.SRM.TestConnectionUI.ShowLoading(false, SW.SRM.EditProperties.SmisProvider);

            // indicate that I will validate just SN, not in the block part of SN
            cfg['validateSN'] = true;
            $('#hfEditPropertiesSmisCfg').val(JSON.stringify(cfg));

            globalDiscoveryPromise.resolve();
        }, function (x) {
            SW.SRM.TestConnectionUI.ShowLoading(false, SW.SRM.EditProperties.SmisProvider);
            globalDiscoveryPromise.reject();
        });

        return globalDiscoveryPromise.promise();
};

smisProvider.MonitoredControlIds = ['pollEngineDescr'];
SW.SRM.EditProperties.ProviderChangeManager.InitProvider(smisProvider, smisProvider.ValidateDiscovery);

smisProvider.IsCelerraFilled = function () {
    var celerraBlockProviderFilled = $('#ChooseCredentialSmis').val() != "" || $('#DisplayNameTextBox').val() != "";
    var celerraFileProviderFilled = $('#ChooseCredentialVnx').val() != "" || $('#DisplayNameVnxTextBox').val() != "";
    return celerraBlockProviderFilled || celerraFileProviderFilled;
};


smisProvider.startDiscovery = function(pp) {
    var providerGuids = [];
    $.each(pp.providers, function(i, e) { providerGuids.push($('#' + e.hfName).val()); });

    var providersArr = providerGuids;
    var groupId = pp.groupId;

    ORION.callWebService("/Orion/SRM/Services/EnrollmentWizardService.asmx",
        "StartDiscoveryMulti",
        {
            'providerGuids': providersArr,
            'groupId': groupId,
            'engineId': pp.engineId
        },
        function (result) {

            if (result.IsSuccess) {
                pp['discoveryGuid'] = result.ProviderGuid;
                SW.SRM.EditProperties.SmisProvider.validateDiscovery(pp);
            } else {
                var title = "@{R=SRM.Strings;K=Provider_Step_Provider_Connecting_Error; E=js}".concat(' ').concat(pp.ip);
                SW.SRM.Common.ShowErrorEditProperties(title, result.Message, result.ErrorMessage);
                pp.deferred.reject();
            }
        },
        function() {
            // Error while starting discovery
            pp.deferred.reject();
        });

    return pp.deferred.promise();
};

smisProvider.validateDiscovery = function(pp) {

    ORION.callWebService("/Orion/SRM/Services/EnrollmentWizardService.asmx",
        "GetAllDiscoveryJobResultsById",
        {
            'discoveryGuid': pp.discoveryGuid,
            'engineId': pp.engineId
        },
        function(result) {
            var resJson = JSON.parse(result);
            if (resJson.isDone != true) {
                setTimeout(function() { SW.SRM.EditProperties.SmisProvider.validateDiscovery(pp); }, 1000);
            } else {

                if (pp.validateBlockInSN || pp.validateBlock) {
                    // Server Side validation will use stored results
                    // It will be set only for Unified validation
                    $('#hfDiscoveryGuid').val(pp.discoveryGuid);
                }
                // Sets passed guids into passed HiddenFields
                //$.each(pp.providers, function(i, e) {
                //    $('#' + e.hfName).val(e.guid);
                //});

                var re = new RegExp('File:[\\s]([\\w]+)[\\s]Block:[\\s]([\\w]+)', 'g');
                // Check if SNs are the same
                var foundArraySn = false;
                if (resJson.selectedArray) {
                    $.each(resJson.selectedArray, function(i, e) {
                        if (pp.validateBlockInSN) {
                            // VNX discovery during promoting
                            var arr = re.exec(e); // e = 'File: APM00114104374 Block: APM00114104374'
                            var block = arr[2];
                            if (block == pp.arraySN) {
                                foundArraySn = true;
                                return false;
                            }
                        } else {
                            if (e == pp.arraySN) {
                                foundArraySn = true;
                                return false;
                            }
                        }
                    });
                }
                
                if (foundArraySn) {
                    SW.SRM.TestConnectionUI.ShowPassMessageBox(null, SW.SRM.EditProperties.SmisProvider);
                    pp.deferred.resolve();
                } else {
                    if (resJson.errorMessage) {
                        SW.SRM.Common.ShowErrorEditProperties(resJson.errorMessage);
                    } else {
                        SW.SRM.Common.ShowErrorEditProperties('@{R=SRM.Strings;K=EditProperties_DeviceValidationErrorText;E=js}');
                    }
                    pp.deferred.reject();
                }
            }
        });
};
