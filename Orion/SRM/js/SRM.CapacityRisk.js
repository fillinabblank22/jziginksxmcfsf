﻿SW = SW || {};
SW.SRM = SW.SRM || {};

SW.SRM.CapacityRisk = function () {
    var resourceUniqueId;
    var swqlFilter;
    var getContentTablePointer = function () {
        return $('#Grid-' + resourceUniqueId);
    };
    var getErrorMessagePointer = function () {
        return $('#ErrorMsg-' + resourceUniqueId);
    };

    this.initialize = function (_uniqueId, _searchTextBoxId, _searchButtonId, _typeMap, _rowsPerPage, _swqlFilter) {
        resourceUniqueId = _uniqueId;
        swqlFilter = _swqlFilter.replace(/(?:[{}\s])*(?:1=1)*/g, '');

        SW.Core.Resources.CustomQuery.initialize(
							{
							    uniqueId: _uniqueId,
							    initialPage: 0,
							    rowsPerPage: _rowsPerPage,
							    allowSort: false,
							    columnSettings: {
							        "Resource": {
							            header: SW.SRM.Formatters.FormatColumnTitle('@{R=SRM.Strings;K=CapacityRisk_Resource_Column;E=js}'),
							            cellCssClassProvider: function (value, row, cellInfo) { return "SRM_Icon_Cell srm_word_wrap"; }
							        },
							        "Type": {
							            header: SW.SRM.Formatters.FormatColumnTitle('@{R=SRM.Strings;K=CapacityRisk_Type_Column;E=js}'),
							            formatter: function (cellValue) {
							                return cellValue;
							            }
							        },
							        "Array": {
							            header: SW.SRM.Formatters.FormatColumnTitle('@{R=SRM.Strings;K=CapacityRisk_Array_Column;E=js}'),
							            cellCssClassProvider: function (value, row, cellInfo) { return "srm_word_wrap"; }
							        },
							        "Last7Days": {
							            header: SW.SRM.Formatters.FormatColumnTitle('@{R=SRM.Strings;K=CapacityRisk_LastSevenDays_Column;E=js}'),
							            isHtml: true,
							            formatter: function (cellValue, row, cellInfo) {
							                row[4] = _typeMap[row[4]];
							                var placeHolderId = 'chart_placeHolder_' + _uniqueId + '_' + row[4] + '_' + row[0];

							                return '<div class="srm-capacityrisk-chart" id="' + placeHolderId + '"></div>';
							            },
							            cellCssClassProvider: function (value, row, cellInfo) {
							                return row[12] >= row[10] ? 'SRM_Grid_Total_Cell'
										: SW.SRM.Formatters.FormatCellStyle(row[12], row[8], row[9]);
							            }
							        },
							        "Warning": {
							            header: SW.SRM.Formatters.FormatColumnTitle('@{R=SRM.Strings;K=CapacityRisk_Warning_Column;E=js}'),
							            isHtml: true,
							            formatter: function (cellValue, row, cellInfo) {
							                return '> ' + thresholdFormatter(cellValue, row, cellInfo, 'warning_placeHolder_' + _uniqueId);
							            },
							            cellCssClassProvider: function () {
							                return '';
							            }
							        },
							        "Critical": {
							            header: SW.SRM.Formatters.FormatColumnTitle('@{R=SRM.Strings;K=CapacityRisk_Critical_Column;E=js}'),
							            isHtml: true,
							            formatter: function (cellValue, row, cellInfo) {
							                return '> ' + thresholdFormatter(cellValue, row, cellInfo, 'critical_placeHolder_' + _uniqueId);
							            },
							            cellCssClassProvider: function () {
							                return '';
							            }
							        },
							        "ATCapacity": {
							            header: SW.SRM.Formatters.FormatColumnTitle('@{R=SRM.Strings;K=CapacityRisk_ATCapacity_Column;E=js}'),
							            isHtml: true,
							            formatter: function (cellValue, row, cellInfo) {
							                return thresholdFormatter(cellValue, row, cellInfo, 'atcapacity_placeHolder_' + _uniqueId);
							            },
							            cellCssClassProvider: function () {
							                return '';
							            }
							        }
							    },
							    onLoad: function (rows) {
							        if ((swqlFilter && swqlFilter.length > 0) && (!rows || rows.length <= 0)) {
							            showCustomErrorText("@{R=SRM.Strings;K=FilterableResources_FilterSupressAllData;E=xml}");

							            return;
							        }

							        ORION.callWebService("/Orion/SRM/Services/ChartData.asmx", "GetCapacityRiskData", { data: rows }, function (result) {
							            $.each(rows, function (index, row) {
							                var serie = $.grep(result.DataSeries, function (x) { return x.TagName.toUpperCase() === (row[4] + '_' + row[0]).toUpperCase() })[0];

							                var warningPlaceHolderId = 'warning_placeHolder_' + _uniqueId + '_' + row[4] + '_' + row[0];
							                setCapacityRunOut(serie.CustomProperties.capacityRunOutDateWarning, warningPlaceHolderId, row[8], row[12]);

							                var criticalPlaceHolderId = 'critical_placeHolder_' + _uniqueId + '_' + row[4] + '_' + row[0];
							                setCapacityRunOut(serie.CustomProperties.capacityRunOutDateCritical, criticalPlaceHolderId, row[9], row[12]);

							                var totalPlaceHolderId = 'atcapacity_placeHolder_' + _uniqueId + '_' + row[4] + '_' + row[0];
							                setCapacityRunOut(serie.CustomProperties.capacityRunOutDateTotal, totalPlaceHolderId, row[10], row[12]);

							                var chartPlaceHolderId = 'chart_placeHolder_' + _uniqueId + '_' + row[4] + '_' + row[0];
							                new SW.SRM.Charts.CapacityRisk().createChart({
							                    placeHolderId: chartPlaceHolderId,
							                    usedInPercent: row[12],
							                    slopeChartData: serie.CustomProperties.slope,
							                    data: serie.Data,
							                    name: serie.Label
							                });
							            });
							        });
							    },
							    searchTextBoxId: _searchTextBoxId,
							    searchButtonId: _searchButtonId
							});

        $('#OrderBy-' + _uniqueId).val('[_UsedCapacityPercentage] DESC, [Resource] ASC');
        SW.Core.Resources.CustomQuery.refresh(_uniqueId);
    };

    var showCustomErrorText = function (text) {
        getContentTablePointer().hide();
        getErrorMessagePointer().text(text).show();
    };

    var thresholdFormatter = function (cellValue, row, cellInfo, placeholderPrefix) {
        var runout = '';
        if (cellInfo.cellCssClassProvider(cellValue, row, cellInfo)) {
            runout = '@{R=SRM.Strings;K=CapacityRisk_Now_Value;E=js}';
        }

        var placeHolderId = placeholderPrefix + '_' + row[4] + '_' + row[0];
        return SW.SRM.Formatters.FormatPercentageColumnValue(cellValue) + '<span id="' + placeHolderId + '_in"></span><br /><span id="' + placeHolderId + '" class="srm-capacityrisk-threshold-days-left">' + runout + '</span>';
    };

    var setCapacityRunOut = function (value, elementId, thresholdValue, currentUsage) {
        var $element = $('#' + elementId);
        var $elementIn = $('#' + elementId + '_in');

        $element.removeClass('SRM_Unknown_Cell');
        if ($element.text()) {
            return;
        }

        if (SW.SRM.Formatters.IsRunoutCollectingData(value)) {
            $element.text('@{R=SRM.Strings;K=CapacityRisk_CollectingData_Value;E=js}');
            $element.addClass('SRM_Unknown_Cell');
            $element.removeClass('srm-capacityrisk-threshold-days-left');
            return;
        }

        if (currentUsage >= thresholdValue) {
            var text = "@{R=SRM.Strings;K=CapacityRisk_Now_Value;E=js}";
            $element.text(text);
            return;
        }

        $element.text(SW.SRM.Formatters.FormatDaysDiff(value));
        $elementIn.text(' @{R=SRM.Strings;K=CapacityRisk_In_Label;E=js}');
    };
};
