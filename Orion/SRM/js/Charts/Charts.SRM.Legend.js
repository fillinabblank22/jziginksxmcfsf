SW = SW || {};
SW.SRM = SW.SRM || {};
SW.SRM.Charts = SW.SRM.Charts || {};
SW.SRM.Charts.Legend = SW.SRM.Charts.Legend || {};
(function (legend) {

    legend.toggleSeries = function(series) {
        series.visible ? series.hide() : series.show();
    };

    legend.addCheckbox = function(series, container) {
        
        var check = $('<input type="checkbox"/>')
                    .click(function() { legend.toggleSeries(series); });
        
        if (series.visible)
            check.attr("checked", 'checked');

        check.appendTo(container);
    };

    legend.addTitle = function(series, container, nameOverride) {
        var label = $('<span/>');
        var title = nameOverride || SW.Core.Charts.htmlDecode(series.name);
        label.text(title);
        label.appendTo(container);
    };

    legend.addLegendSymbol = function(series, container, colorOverride) {
        var symbolWidth = 16;
        var symbolHeight = 12;
        var label = $('<span class="legendSymbol" />');
        var renderer = new Highcharts.Renderer(label[0], symbolWidth, symbolHeight + 3);
        var symbolColor = colorOverride || series.color;

        renderer.rect(0, 3, symbolWidth, symbolHeight, 2)
                .attr({
                    stroke: symbolColor,
                    fill: symbolColor
                })
                .add();

        label.appendTo(container);
    };
    
    legend.addText = function (text, container) {
        var label = $('<span/>');

        label.text(text);
        label.appendTo(container);
    };

    legend.createTableLegend = function(chart, dataUrlParameters, legendContainerId, interactiveMode,seriesHeader,operationHeader, operation) {
        var table = $('#' + legendContainerId);
        
        if (typeof(interactiveMode) === 'undefined') {
            interactiveMode = true;
        }
        
        var headerRow = $('<tr />');
        $('<th class="SRM_tableLegendHeaderFirst"></th>').appendTo(headerRow);
        
        $('<th class="SRM_tableLegendHeaderSecond"></th>').appendTo(headerRow);
        var th = $('<th class="SRM_tableLegendHeader"/>').appendTo(headerRow);
        legend.addText(seriesHeader, th);
        th = $('<th class="SRM_tableLegendHeader"/>').appendTo(headerRow);
        legend.addText(operationHeader, th);
        headerRow.appendTo(table);

        $.each(chart.series, function(index, series) {
            if (!series.options.showInLegend)
                return;
            
            var row = $('<tr/>');
            
            if (interactiveMode) {
                var td = $('<td/>').appendTo(row);
                legend.addCheckbox(series, td);
            }
                    
            td = $('<td/>').appendTo(row);
            legend.addLegendSymbol(series, td);
            
            td = $('<td/>').appendTo(row);
            legend.addTitle(series, td);
            td = $('<td/>').appendTo(row);
            legend.addText(operation, td);
            row.appendTo(table);
           
        });
    };
    
}(SW.SRM.Charts.Legend));