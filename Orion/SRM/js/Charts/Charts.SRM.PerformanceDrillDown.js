﻿SW = SW || {};
SW.SRM = SW.SRM || {};
SW.SRM.Charts = SW.SRM.Charts || {};


SW.SRM.Charts.PerformanceDrillDown = function () {
    var netObjectId, resId, daysOfDataToLoad, sampleSizeInMinutes, yAxisTitle, chartTitle, dataBindMethod, chartWidth, chartType, requestParameter;
    var chartId, filterField;
    var hasParent = false;
    var legendContainerId;
    var legendControlTemplate = '<div class="srm-performance-legend-item"><input type="checkbox" checked="checked" class="srm-performance-legend-check-box" /><div class="srm-performance-legend-marker" style="background-color: {color};"></div><a href="javascript:void(0);">{label}</a><div>';
    var criticalBackgroundColor = "#FFD3D6";
    var criticalLineColor = "#F7A2AD";
    var warningBackgroundColor = "#FFEBD6";
    var warningLineColor = "#FFDFB5";
    
    var chartConfig = function () {
        return {
            chart: {
                type: chartType,
                width: chartWidth,
                events: {
                    redraw: function() {
                        setChartPlotbands(this);
                    }
                }
            },
            yAxis: [
                {
                    title: {
                        margin: 10,
                        text: yAxisTitle,
                        style: "text-transform:none;"
                    },
                    labels: {
                        align: "right",
                        x: -8
                    },
                    min: 0,
                    startOnTick: false,
                    endOnTick: true,
                    tickWidth: 0,
                    showLastLabel: true,
                    unit: getUnit(),
                    minTickInterval: requestParameter == "Throughput" || requestParameter == "IOSize" ? 10 : null
                }
            ],
            plotOptions: {
                series: {
                    marker: {
                        enabled: false
                    }
                },
                line: {
                    pointPadding: 0,
                    borderColor: "#000000",
                    lineWidth: 1,
                    groupPadding: 0
                },
                area: {
                    lineColor: "#2484b7",
                    borderWidth: 1,
                    color: '#c8f1fb',
                }
            },
            rangeSelector: {
              selected: 2 
            },
            xAxis: {
                minRange: 3600 * 1000 // one hour
            },
            seriesTemplates: {},
            tooltip: {
                shared: true,
                formatter: chartTooltipFormatter,
                positioner: function (boxWidth, boxHeight, point) {
                    var boxX = point.plotX;
                    var boxY = point.plotY;
                    if (this.chart.plotWidth > point.plotX + boxWidth - 60) {
                        boxX = boxX + 10;
                    } else {
                        boxX = boxX - 40 - boxWidth;
                    }

                    if (this.chart.plotHeight > point.plotY + boxHeight) {
                        boxY = boxY + 40;
                    } else {
                        boxY = boxY + 10 - boxHeight;
                    }

                    return { x: boxX, y: boxY };
                }
            }
        };
    };

    function getUnit() {
        if (requestParameter == "CacheHitRatio" || requestParameter == "DiskBusy" || requestParameter == "IOPSRatio" || requestParameter == "Utilization") {
            return "@{R=SRM.Strings;K=Value_Unit_Percentage;E=js}";
        }
        if (requestParameter == "IOSize") {
            return "ioSize";
        }
        if (requestParameter == "Throughput") {
            return "throughput";
        }
        return "";
    }

    var RequestParameterToSeriesName = {
        'IOPS':'@{R=SRM.Strings;K=Lun_Performance_Summary_IOPS;E=js}',
        'CacheHitRatio':'@{R=SRM.Strings;K=Lun_Performance_Summary_CacheHitRatio;E=js}',
        'Latency': '@{R=SRM.Strings;K=Lun_Performance_Summary_Latency;E=js}',
        'IOPSRatio':'@{R=SRM.Strings;K=Lun_Performance_Summary_IOPSRatio;E=js}',
        'QueueLength':'@{R=SRM.Strings;K=Lun_Performance_Summary_QueueLength;E=js}',
        'DiskBusy': '@{R=SRM.Strings;K=Lun_Performance_Summary_DiskBusy;E=js}',
        'IOSize':'@{R=SRM.Strings;K=Lun_Performance_Summary_IOSize;E=js}',
        'Throughput': '@{R=SRM.Strings;K=Lun_Performance_Summary_Throughput;E=js}',
        'Utilization': '@{R=SRM.Strings;K=Lun_Performance_Summary_Utilization;E=js}'
    };

    function chartTooltipFormatter() {
        var lang = Highcharts.getOptions().lang;
        var date = Highcharts.dateFormat(lang.tooltipDateFormat, new Date(this.x));
        var s = '<table class="srm-performance_tooltip highcharts-tooltip"><tr><td colspan="2">' + date + '</td></tr>';
        $.each(this.points, function (i, point) {
            s += '<tr><td style = "color: ' + point.series.color + '">' + point.series.name + ': </td><td><b>' +
                SW.SRM.Formatters.FormatYChartValue(point.y, RequestParameterToSeriesName[requestParameter]) + '</b></td></tr>';
        });
        s += '</table>';
        return s;
    };

    function getCustomPlotBands(chartObj) {
        var selected = [];
        for (var i = 0; i < chartObj.options.CustomPlotBands.length; i++) {
            var plotBand = chartObj.options.CustomPlotBands[i];
            if (plotBand.SeriesName == 'Total' && getSeriesVisibleByName(chartObj, plotBand.SeriesName)) {
                return plotBand;
            }
            if (getSeriesVisibleByName(chartObj, plotBand.SeriesName)) {
                selected.push(plotBand);
            }
        }
        return selected.length == 1 ? selected[0] : null;
    }

    function getSeriesVisibleByName(chartObj, seriesName) {
        for (var i = 0; i < chartObj.series.length; i++) {
            if (chartObj.series[i].name == seriesName) {
                return chartObj.series[i].visible;
            }
        }
        return false;
    }

    function setChartPlotbands(chartObj) {
        var yAxisIndex = 0;
        var maxYValue = chartObj.yAxis[yAxisIndex].max;
        var minYValue = chartObj.yAxis[yAxisIndex].min;
        chartObj.yAxis[yAxisIndex].removePlotBand();
        if (maxYValue !== null && maxYValue !== 'undefined') {

            if (chartObj.options.CustomPlotBands !== null &&
                chartObj.options.CustomPlotBands !== 'undefined' &&
                chartObj.options.CustomPlotBands.length > 0) {
                var plotBand = getCustomPlotBands(chartObj);
                if (plotBand == null) {
                    return;
                }

                var warningValue = plotBand.Warning;
                var criticalValue = plotBand.Critical;
                var operatorValue = plotBand.Operator;

                var isGreater = operatorValue === 'Greater' || operatorValue === 'GreaterOrEqual';
                var isLess = operatorValue === 'Less' || operatorValue === 'LessOrEqual';

                if (criticalValue !== null && criticalValue > 0 && warningValue !== null && warningValue > 0) {

                    if (isGreater) {
                        if (warningValue < criticalValue) {
                            addThreshold(chartObj, true, warningValue, criticalValue, yAxisIndex);
                        }
                        if (criticalValue < maxYValue) {
                            addThreshold(chartObj, false, criticalValue, maxYValue, yAxisIndex);
                        }
                    } else if (isLess) {
                        if (criticalValue < warningValue) {
                            addThreshold(chartObj, true, criticalValue, warningValue, yAxisIndex);
                        }
                        if (minYValue < criticalValue) {
                            addThreshold(chartObj, false, minYValue, criticalValue, yAxisIndex);
                        }
                    }
                }
            }
        }
    }

    function addThreshold(chartObj, isWarning, from, to, yAxisIndex) {
        var plotbandColor;
        var plotlinecolor;
        
        if (isWarning === 'true' || isWarning === true) {
            plotbandColor = warningBackgroundColor;
            plotlinecolor = warningLineColor;
        } else {
            plotbandColor = criticalBackgroundColor;
            plotlinecolor = criticalLineColor;
        }

        chartObj.yAxis[yAxisIndex].addPlotBand({
            from: from,
            to: to,
            color: plotbandColor,
            zIndex: 1
        });
        
        chartObj.yAxis[yAxisIndex].addPlotLine({
            value: from,
            color: plotlinecolor,
            zIndex: 2,
            width: 1
        });
        
        chartObj.yAxis[yAxisIndex].addPlotLine({
            value: to,
            color: plotlinecolor,
            zIndex: 2,
            width: 1
        });
    }

    function setSerieVisibility(checkBox, serie) {
        if (serie.visible) {
            checkBox.removeAttr('checked');
            serie.hide();
        } else {
            checkBox.attr('checked', 'checked');
            serie.show();
        }
    };
    
    function setLegendAttr(serie) {
        var legend = legendControlTemplate.replace('{color}', serie.color).replace('{label}', serie.name);
        if (serie.options.customProperties == 'IsCurrentItem') {
            legend = legend.replace('srm-performance-legend-item', 'srm-performance-legend-current_item');
        }

        $(legend).click(function () {setSerieVisibility($(this).find('input'), serie);}).appendTo('#' + legendContainerId);
    };

    var createChart = function () {
        var refresh = function () {
            var params = {
                'renderTo': chartId,
                'dataUrl': dataBindMethod,
                'netObjectIds': [netObjectId],
                'daysOfDataToLoad': daysOfDataToLoad,
                'sampleSizeInMinutes': sampleSizeInMinutes,
                'title': chartTitle == null || typeof chartTitle === 'undefined' ? '' : chartTitle,
                'showTitles': true,
                'subtitle': "$[ZoomRange]",
                'hasParent': hasParent,
                'filterField': filterField != undefined ? filterField : "",
                'chartWidth': chartWidth,
                'initialZoom': '24h',
                'requestParameter': requestParameter
            };

            SW.Core.Charts.dataFormatters.ioSize =
                function (value, axis, decimalPlaces) {
                    var valueInKB = value / bytesInKB;
                    return Highcharts.numberFormat(valueInKB, 2);
                };

            SW.Core.Charts.dataFormatters.throughput =
                function(value, axis, decimalPlaces) {
                    var valueInMB = value / bytesInMB;
                    return Highcharts.numberFormat(valueInMB, 2);
                };
            
            SW.Core.Charts.initializeStandardChart(params, chartConfig(), function () {
                var chart = $('#' + chartId).data();
              
                $('#' + legendContainerId).empty();
                $.each(chart.series, function (i, serie) {
                    if (serie.name == 'Navigator') {
                        return;
                    }
                    setLegendAttr(serie);
                    
                });
            });
        };
        if (typeof(SW.Core.View) != 'undefined' && typeof(SW.Core.View.AddOnRefresh) != 'undefined') {
            SW.Core.View.AddOnRefresh(refresh, chartId);
        }
        refresh();
    };

    this.init = function (info) {
        netObjectId = info.NetObjectId;
        resId = info.ResourceId;
        sampleSizeInMinutes = info.SampleSizeInMinutes;
        daysOfDataToLoad = info.DaysOfDataToLoad;
        yAxisTitle = info.YAxisTitle;
        chartTitle = info.ChartTitle;
        dataBindMethod = info.DataBindMethod;
        chartId = 'srm-performance-chart-' + resId;
        legendContainerId = 'srm-performance-legend-' + resId;
        filterField = info.FilterField;
        hasParent = info.HasParent;
        chartWidth = info.ChartWidth;
        chartType = info.ChartType;
        requestParameter = info.RequestParameter;
        createChart();
    };
};
