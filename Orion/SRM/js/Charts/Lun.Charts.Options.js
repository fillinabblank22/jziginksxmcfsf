﻿SW = SW || {};
SW.SRM = SW.SRM || {};
SW.SRM.Charts = SW.SRM.Charts || {};
SW.SRM.Charts.Options = SW.SRM.Charts.Options || {};

(function (ns) {
    var otherTagColor = "Grey";
    /*IOPS Performance chart options*/
	ns.LunIOPSPerformance = function () {
		return {
		    title: { text: "@{R=SRM.Strings;K=SRM_Chart_IOPS_Title;E=js}" },
		    subtitle: { text: "@{R=SRM.Strings;K=SRM_Chart_IOPS_Title;E=js}" },
			yAxis: [{
				title: {
					margin: 10,
					text: "@{R=SRM.Strings;K=SRM_Chart_yAxis_Title_Iops;E=js}",
					style: "text-transform:none;"
				},
				labels: { align: "right", x: -8 },
				min: 0,
				startOnTick: false,
				endOnTick: true
			}],
			xAxis: {
			    labels: {
			        enabled: true
			    },
			    gridLineWidth: 0,
			    lineWidth: 0,
			    tickWidth: 0,
			    type: 'datetime',
			    minRange: 3600 * 1000 //one hour
			},
			plotOptions: {
				line: {
					pointPadding: 0,
					borderColor: "#000000",
					borderWidth: 1,
					groupPadding: 0
				}
			},
			seriesTemplates: {
				Total: {
					type: "line",
					zIndex: 2
				},
				Read: {
					type: "line",
					zIndex: 2
				},
				Write: {
					type: "line",
					zIndex: 2
				},
				Other: {
					type: "line",
					zIndex: 2,
					color: otherTagColor
				},
				Data_Navigator: {
					name: "Navigator",
					id: "navigator",
					showInLegend: false,
					visible: false
				}
			}
		};
	}();

    /*Throughput Performance chart options*/
	ns.LunThroughputPerformance = function () {
	    return {
		    title: { text: "@{R=SRM.Strings;K=SRM_Chart_Throughput_Title;E=js}" },
		    subtitle: { text: "@{R=SRM.Strings;K=SRM_Chart_Throughput_Title;E=js}" },
			yAxis: [{
				title: {
					margin: 10,
					text: "@{R=SRM.Strings;K=SRM_Chart_yAxis_Title_Throughput;E=js}",
					style: "text-transform:none;"
				},
				labels: { align: "right", x: -8 },
				min: 0,
				startOnTick: false,
				endOnTick: true
			}],
	        xAxis: {
	            labels: {
	                enabled: true
	            },
	            gridLineWidth: 0,
	            lineWidth: 0,
	            tickWidth: 0,
	            type: 'datetime',
	            minRange: 3600 * 1000 //one hour
	        },
			plotOptions: {
				line: {
					pointPadding: 0,
					borderColor: "#000000",
					borderWidth: 1,
					groupPadding: 0
				}
			},
			seriesTemplates: {
				Total: {
					type: "line",
					zIndex: 2
				},
				Read: {
					type: "line",
					zIndex: 2
				},
				Write: {
					type: "line",
					zIndex: 2
				},
				Data_Navigator: {
					name: "Navigator",
					id: "navigator",
					showInLegend: false,
					visible: false
				}
			}
		};
	}();

    /*LunIO Performance chart options*/
	ns.LunIOSize = function () {
		return {
		    title: { text: "@{R=SRM.Strings;K=SRM_Chart_IOLatency_Title;E=js}" },
		    subtitle: { text: "@{R=SRM.Strings;K=SRM_Chart_IOLatency_Title;E=js}" },
			yAxis: [{
				title: {
					margin: 10,
					text: "@{R=SRM.Strings;K=SRM_Chart_yAxis_Title_Latency;E=js}",
					style: "text-transform:none;"
				},
				labels: { align: "right", x: -8 },
				min: 0,
				startOnTick: false,
				endOnTick: true
			}],
			xAxis: {
			    labels: {
			        enabled: true
			    },
			    gridLineWidth: 0,
			    lineWidth: 0,
			    tickWidth: 0,
			    type: 'datetime',
			    minRange: 3600 * 1000 //one hour
			},
			plotOptions: {
				line: {
					pointPadding: 0,
					borderColor: "#000000",
					borderWidth: 1,
					groupPadding: 0
				}
			},
			seriesTemplates: {
				Total: {
					type: "line",
					zIndex: 2
				},
				Read: {
					type: "line",
					zIndex: 2
				},
				Write: {
					type: "line",
					zIndex: 2
				},
				Other: {
				    type: "line",
				    zIndex: 2,
				    color: otherTagColor
				},
				Data_Navigator: {
					name: "Navigator",
					id: "navigator",
					showInLegend: false,
					visible: false
				}
			},
		};
	}();
    
    /*Total IOPS Performance chart options*/
    ns.LunTotalIOPSPerformance = function () {
        return {
            title: { text: "@{R=SRM.Strings;K=SRM_Chart_IOPS_Title;E=js}" },
            subtitle: { text: "@{R=SRM.Strings;K=SRM_Chart_IOPS_Title;E=js}" },
            yAxis: [{
                title: {
                    margin: 10,
                    text: "@{R=SRM.Strings;K=SRM_Chart_yAxis_Title_Iops;E=js}",
                    style: "text-transform:none;"
                },
                labels: { align: "right", x: -8 },
                min: 0,
                startOnTick: false,
                endOnTick: true
            }],
            xAxis: {
                labels: {
                    enabled: true
                },
                gridLineWidth: 0,
                lineWidth: 0,
                tickWidth: 0,
                type: 'datetime',
                minRange: 3600 * 1000 //one hour
            },
            plotOptions: {
                line: {
                    pointPadding: 0,
                    borderColor: "#000000",
                    borderWidth: 1,
                    groupPadding: 0
                }
            },
            seriesTemplates: {
                Data_Navigator: {
                    name: "@{R=SRM.Strings;K=SRM_Chart_Total_IOPS_Navigator;E=js}",
                    id: "navigator",
                    showInLegend: false,
                    visible: false
                }
            }
        };
    }();




}(SW.SRM.Charts.Options));

