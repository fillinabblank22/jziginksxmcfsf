﻿SW = SW || {};
SW.SRM = SW.SRM || {};
SW.SRM.Charts = SW.SRM.Charts || {};

SW.SRM.Charts.Performance = function () {
    var netObjectId, resId, daysOfDataToLoad, sampleSizeInMinutes, yAxisTitle, chartTitle, chartSubTitle, dataUrl, chartInitialZoom, numberOfSeriesToShow;
    var chartId, poolID, filterField, isLatency;
    var showParent = true;
    var hasParent = false;
    var legendContainerId;
    var legendControlTemplateWithStatusIcon = '<div class="srm-performance-legend-item"><input type="checkbox" checked="checked" class="srm-performance-legend-check-box" /><div class="srm-performance-legend-marker" style="background-color: {color};"></div><a href="javascript:void(0);"><img src="/Orion/StatusIcon.ashx?size=small\&entity={entity}\&id={entityID}\&status={status}" class="srm-performance-legend-status-icon" />&nbsp;{label}</a><div>';
    
    var chartConfig = function() {
        return {
            chart: {
                type: 'line',
                events: {
                    load: function() {
                        SW.SRM.Common.ChartSelectCurrentZoomButton(this);
                        var chart = $('#' + chartId).data();
                        var performanceBtns = $('#srm-performance-chart-buttons-' + resId);
                        if (performanceBtns.length) {
                            if (chart.rangeSelector.zoomText) {
                                // Highchart changes top spacing based on title length, so have to position dynamically
                                var customHeader = $('#' + chartId).parent().find(".srm-custom-header-content");
                                performanceBtns.css('margin-top', chart.rangeSelector.zoomText.y + 3 - (customHeader.length > 0 ? 0 : 23) + 'px');
                            }
                        }
                    }
                }
            },
            title: {
                useHTML: true
            },
            yAxis: [{
                title: {
                    margin: 10,
                    text: yAxisTitle,
                    style: "text-transform:none;"
                },
                labels: { align: "right", x: -8},
                min: 0,
                max:0.1,
                startOnTick: false,
                endOnTick: true,
                minTickInterval: 1
            }],
            plotOptions: {
                line: {
                    pointPadding: 0,
                    borderColor: "#000000",
                    borderWidth: 1,
                    groupPadding: 0
                }
            },
            rangeSelector: {
                buttons: [{ tag: '1h' }, { tag: '12h' }, { tag: '24h' }] // can't use localized value, so add tags
            },
            xAxis: {
                minRange: 3600 * 1000 //one hour
            },
            tooltip: {
                formatter: chartTooltipFormatter,
            },
            seriesTemplates: {
                Data_Navigator: {
                    name: 'Navigator',
                    id: 'navigator',
                    showInLegend: false,
                    visible: false
                }
            }
        };
    };

    var chartTooltipFormatter = function () {
        var s = '<table><tr><td><b>' + SW.SRM.Formatters.FormatDate(new Date(this.x)) + '</b></td></tr>';
        $.each(this.points, function (i, point) {
            if (point.series.options.customProperties != null && point.series.options.customProperties.CustomProperty == 'Pool' && !showParent)
                return;
            s += '<tr class="' + point.series.swState + '"><td style = "color: ' + point.series.color + '" >' + point.series.name + ': </td><td><b>' + SW.SRM.Formatters.FormatYChartValue(point.y, isLatency === true ? '@{R=SRM.Strings;K=Lun_Performance_Summary_Latency;E=js}' : point.series.name) + '</b></td></tr>';
        });
        s += '</table>';
        return s;
    };
    
    function setSerieVisibility(checkBox, serie) {
        if (serie.visible) {
            checkBox.removeAttr('checked');
            serie.hide();
        } else {
            checkBox.attr('checked', 'checked');
            serie.show();
        }
    };
    
    function setLegendAttr(serie) {
        if (serie.options.customProperties != null) {
            var legend = legendControlTemplateWithStatusIcon.replace('{color}', serie.color).replace('{entity}', serie.options.customProperties.SwisEntity).replace('{entityID}', serie.options.customProperties.Id).replace('{status}', serie.options.customProperties.Status).replace('{label}', serie.name);
            if (serie.options.customProperties.CustomProperty == 'IsCurrentItem') {
                legend = legend.replace('srm-performance-legend-item', 'srm-performance-legend-current_item');
            }
            if (serie.options.customProperties.CustomProperty == 'Pool' && !showParent) {
                legend = legend.replace('srm-performance-legend-item', 'srm-performance-legend-item-display-none');
            }
            $(legend).click(function() { setSerieVisibility($(this).find('input'), serie); }).appendTo('#' + legendContainerId);
        }
    };

    var createChart = function () {
        var refresh = function () {
            var params = {
                'renderTo': chartId,
                'dataUrl': dataUrl,
                'netObjectIds': [netObjectId],
                'daysOfDataToLoad': daysOfDataToLoad,
                'sampleSizeInMinutes': sampleSizeInMinutes,
                'title': chartTitle == null || typeof chartTitle === 'undefined' ? '' : chartTitle,
                'showTitles': true,
                'subtitle': chartSubTitle != "" ? chartSubTitle : "$[ZoomRange]",
                'poolID': poolID,
                'showParent': showParent,
                'hasParent': hasParent,
                'filterField': filterField != undefined ? filterField : "",
                'initialZoom': chartInitialZoom,
                'numberOfSeriesToShow': numberOfSeriesToShow
            };
            SW.Core.Charts.initializeStandardChart(params, SW.SRM.Common.TooltipHighlighter(chartConfig()), function () {
                var chart = $('#' + chartId).data();

                if (chart.options.hideResource === true) {
                    $("div.ResourceWrapper[resourceid='" + resId + "']").hide();
                }

                var performanceBtns = $('#srm-performance-chart-buttons-' + resId);
                if (performanceBtns.length) {
                    performanceBtns.css('margin-left', chart.chartWidth - 225 + 'px');
                }
                $('#' + legendContainerId).empty();
                $.each(chart.series, function (i, serie) {
                    if (serie.name == 'Navigator') {
                        return;
                    }
                    setLegendAttr(serie);
                });
                var wrapperHeight = $('#srm-performance-chart-' + resId).closest('.ResourceWrapper').height();
                $('#srm-performance-chart-' + resId).closest('.ResourceWrapper').css('min-height', wrapperHeight);
            });
        };
        if (typeof(SW.Core.View) != 'undefined' && typeof(SW.Core.View.AddOnRefresh) != 'undefined') {
            SW.Core.View.AddOnRefresh(refresh, chartId);
        }
        refresh();
    };

    this.init = function (latency, info) {
        isLatency = latency;
        netObjectId = info.NetObjectId;
        resId = info.ResourceId;
        sampleSizeInMinutes = info.SampleSizeInMinutes;
        numberOfSeriesToShow = info.NumberOfSeriesToShow;
        daysOfDataToLoad = info.DaysOfDataToLoad;
        yAxisTitle = info.YAxisTitle;
        chartTitle = info.ChartTitle;
        chartSubTitle = info.ChartSubTitle ? info.ChartSubTitle : "";
        dataUrl = info.DataUrl;
        poolID = info.PoolID;
        chartId = 'srm-performance-chart-' + resId;
        legendContainerId = 'srm-performance-legend-' + resId;
        filterField = info.FilterField;
        hasParent = info.HasParent;
        showParent = info.ShowParent != undefined ? info.ShowParent : true;
        chartInitialZoom = info.ChartInitialZoom;
        createChart();
    };
};
