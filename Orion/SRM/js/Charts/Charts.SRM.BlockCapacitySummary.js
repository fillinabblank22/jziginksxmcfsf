﻿SW = SW || {};
SW.SRM = SW.SRM || {};
SW.SRM.Charts = SW.SRM.Charts || {};

SW.SRM.Charts.BlockCapacitySummary = function () {
    var initData,
        tableContainerId,
        objectNameId,
        legendContainerId,
        detailContainerId,
        expanderImageId,
        transparentColor = 'rgba(255, 255, 255, 0)',

        legendControlTemplate = '<div class="srm-blockcapacitysummary-legend-item" title="{tooltip}"><div class="srm-blockcapacitysummary-legend-marker" style="background-color: {color};">{ie7fix}</div>{label}<div>',
        loadNextPoolsTemplate = '<div id="{id}" class="srm-blockcapacitysummary-load-next-items"><a href="javascript:void(0);">@{R=SRM.Strings;K=BlockCapacitySummary_LoadNextItems;E=js}</a></div>',
        tooltipTemplate = '<tr style="display:{point.display}; line-height: 90%; font-weight: bold;"><td style="border: 0; font-size: 12px; color: {series.color}; padding-top: 4px;">{series.name}:&nbsp;</td><td style="border: 0px; font-size: 12px; padding: 4px 0px 0px 5px;"><b>{point.percent}</b></td></td></tr>',
        tooltipLightBlueTemplate = '<tr style="display:{point.display}; line-height: 90%; font-weight: bold;"><td style="border: 0; font-size: 12px; color: {series.color}; padding: 4px 0px 8px 20px;">{series.name}:&nbsp;</td><td style="border: 0px; font-size: 12px; padding: 4px 0px 8px 5px;"><b>{point.percent} (@{R=SRM.Strings;K=BlockCapacitySummary_Total;E=js})</b></td></td></tr>';

    var getChartConfig = function () {
        return {
            highchartsChartType: "chart",
            chart: {
                type: 'bar',
                margin: 0,
                spacing: 0,
                borderWidth: 0,
                plotBorderWidth: 0,
                backgroundColor: transparentColor,
                plotBackgroundColor: transparentColor,
                height: 0,
                zoomType: null
            },
            xAxis: {
                labels: { enabled: false },
                gridLineWidth: 0,
                lineWidth: 0,
                tickWidth: 0
            },
            yAxis: [{
                labels: { enabled: false },
                gridLineWidth: 0,
                lineWidth: 0,
                tickWidth: 0,
                tickInterval: null,
                title: { text: null },
                unit: 'bytes',
                max: 1
            }],
            legend: { enabled: false },
            scrollbar: { enabled: false },
            rangeSelector: { enabled: false },
            navigator: { enabled: false },
            loading: {
                labelStyle: {
                    paddingTop: '0px',
                    top: '0px'
                }
            },
            tooltip: {
                shared: true,
                positioner: function (boxWidth, boxHeight, point) {
                    var boxX = point.plotX;
                    var boxY = point.plotY;
                    if (this.chart.plotWidth > point.plotX + boxWidth - 60) {
                        boxX = boxX - 20;
                    } else {
                        boxX = boxX - 40 - boxWidth;
                    }

                    if (this.chart.plotHeight > point.plotY + boxHeight) {
                        boxY = boxY + 10;
                    } else {
                        boxY = boxY + 10 - boxHeight;
                    }

                    return { x: boxX, y: boxY };
                },
                formatter: function () {
                    var lightBlueRow = '';

                    var s = '<table class="srm-blockcapacitysummary-tooltip">';
                    $.each(this.points, function (i, point) {
                        if (typeof (initData.LegendMapping) !== "undefined") {
                            var tagName = point.series.options.uniqueId.replace(initData.NetObjectId, '');
                            if (typeof (initData.LegendMapping[tagName]) !== "undefined") {
                                if (tagName === "columnLightBlue") {
                                    lightBlueRow = tooltipLightBlueTemplate.replace('{point.display}', point.point.display)
                                                                         .replace('{series.color}', point.series.color)
                                                                         .replace('{series.name}', point.series.name)
                                                                         .replace('{point.percent}', SW.SRM.Formatters.FormatPercentageColumnValue(point.point.percent, 0));
                                } else { 
                                    s += tooltipTemplate.replace('{point.display}', point.point.display)
                                                        .replace('{series.color}', point.series.color)
                                                        .replace('{series.name}', point.series.name)
                                                        .replace('{point.percent}', SW.SRM.Formatters.FormatPercentageColumnValue(point.point.percent, 0));
                                    if (tagName === "columnDarkBlue")
                                        s += '{LightBluePlaceHolder}';
                                }
                            }
                        }
                    });
                    s += '</table>';
                    return s.replace('{LightBluePlaceHolder}', lightBlueRow);
                },
                positioningEnabled: true
            },
            plotOptions: {
                series: {
                    stacking: "normal",
                    animation: true,
                    stickyTracking: false,
                    groupPadding: 0,
                    pointPadding: 0,
                    borderWidth: 0,
                    shadow: false,
                    pointWidth: 15
                }
            },
            series: [],
            seriesTemplates: {
                columnPink: {
                            color: '#F3939A'
                },
                columnGrey: {
                    color: {
                            linearGradient: { x1: 1, x2: 0, y1: 0, y2: 0 },
                            stops: [[0, '#E2E2E2'], [1, '#F6F6F6']]
                    }
                },
                columnDarkBlue: {
                    color: '#006CA9'
                },
                columnPurple: {
                    color: '#93008B'
                },
                columnOrange: {
                    color: '#F99D1C'
                },
                columnLightBlue: {
                    color: '#00A0F1'
                },
                columnTransparent: {
                    color: transparentColor
                }
            }
    };
};

var getMoreDataElementId = function () {
    return detailContainerId + '-more-data';
};

var createChart = function () {
    var refresh = function () {
        loadData(false);
    };
    if (typeof (SW.Core.View) !== 'undefined' && typeof (SW.Core.View.AddOnRefresh) !== 'undefined') {
        SW.Core.View.AddOnRefresh(refresh, initData.RenderTo);
    }
    refresh();
};

var loadData = function (getMoreData) {
    var detailParams = {
        'renderTo': initData.RenderTo,
        'dataUrl': "/Orion/SRM/Services/BlockCapacitySummaryService.asmx/GetData",
        'netObjectIds': [initData.NetObjectId],

        'title': '',
        'showTitle': false,
        'subtitle': '',
        'initData': initData,
        'getMoreData': getMoreData
    };

    var config = getChartConfig();

    var isIe = $('html').hasClass('sw-is-msie');
    var isIe7 = $('html').hasClass('sw-is-msie-7');
    var isIe8 = $('html').hasClass('sw-is-msie-8');
    var isIe11 = $('html').hasClass('sw-is-mozilla-11');
    var isChrome = /chrom(e|ium)/.test(navigator.userAgent.toLowerCase());

    // Fixing IE7 and IE8 linear gradient color
    if (isIe7 || isIe8) {
        $.extend(true, config, { seriesTemplates: { columnGrey: { color: { linearGradient: { x2: 1 } } } } });
    }

    SW.Core.Charts.initializeStandardChart(detailParams, config, function () {
        var chart = $('#' + initData.RenderTo).data();

        $('#' + objectNameId).show();
        $('#' + tableContainerId).show();
        $('#' + expanderImageId).show();

        if (typeof chart.series == 'undefined' || chart.series.length == 0 || chart.series[0].data.length == 0) {
            setExpander(false);

            $('#' + initData.RenderTo).removeClass('srm-blockcapacitysummary-chart');
            $('#' + initData.RenderTo).addClass('srm-blockcapacitysummary-chart-no-data');
            $('#' + tableContainerId).hide();

            return;
        }

        setExpander(initData.Expanded);

        var metadataSerie = chart.series[0];
        var lightBlueSerie = chart.series[4];

        $('#' + legendContainerId).empty();
        chart.series.reverse();
        $.each(chart.series, function (i, serie) {
            var tagName = serie.options.uniqueId.replace(initData.NetObjectId, '');
            if (typeof (initData.LegendMapping) !== "undefined") {
                if (typeof (initData.LegendMapping[tagName]) !== "undefined") {
                    var tooltip = '';
                    if (typeof (initData.LegendMapping[tagName]) === "string") {
                        serie.name = initData.LegendMapping[tagName];
                        tooltip = '';
                    }
                    else {
                        serie.name = initData.LegendMapping[tagName][0];
                        tooltip = initData.LegendMapping[tagName][1];
                    }

                    var value = serie.yData[0];
                    if (tagName === "columnDarkBlue") {
                        value += lightBlueSerie.yData[0];
                    }

                    $(legendControlTemplate.replace('{color}', serie.color.stops ? serie.color.stops[0][1] : serie.color)
                                           .replace('{ie7fix}', isIe7 ? '&nbsp;&nbsp;&nbsp;&nbsp;' : '')
                                           .replace('{label}', serie.name + (value === 0 ? '' : ' (' + SW.SRM.Formatters.FormatCapacity(value) + ')'))
                                           .replace('{tooltip}', tooltip ? tooltip : ''))
                                           .appendTo('#' + legendContainerId);
                }
            }
        });

        if (metadataSerie.options.customProperties.hasDetails === false) {
            $('#' + expanderImageId).hide();
            $('#' + objectNameId).hide();
            $('#' + initData.RenderTo).css('left', '16px');
            setExpander(false);
        }

        var $objectName = $('#' + objectNameId);
        shortText($objectName, parseInt($objectName.css('max-width')) - 5);

        $('#' + detailContainerId).empty();
        $.each(metadataSerie.data, function (index, data) {
            if (data.name) {
                var $div = $('<div><span>' + data.name + '</span></div>');
                //this padding updates has been figured out by experimental testing 
                //for the particular browsers: IE7, IE8, later IE, others are for Chrome and FF
                if (isIe7) {
                    //IE7
                    if (index % 10 == 0 || index % 4 == 0) {
                        $div.css('padding-bottom', '11px');
                    } else if (index % 3 == 0) {
                        $div.css('padding-bottom', '12px');
                    }
                } else if (isIe8) {
                    //IE8
                    $div.css('padding-bottom', '13px');
                    if (index % 5 == 0) {
                        $div.css('padding-bottom', '13px');
                    } else if (index % 4 == 0) {
                        $div.css('padding-bottom', '12px');
                    }
                } else if (isIe) {
                    //later IE (9, 10)
                    $div.css('padding-top', '1px');
                    $div.css('padding-bottom', '12px');
                    if (index % 7 == 0) {
                        $div.css('padding-bottom', '11px');
                    } else if (index % 5 == 0) {
                        $div.css('padding-bottom', '10px');
                    }
                } else if(isIe11) {
                    //IE11
                    if (index % 4 == 0 || index % 10 == 0) {
                        $div.css('padding-bottom', '11px');
                    } else {
                        $div.css('padding-bottom', '10px');
                    }
                } else if (isChrome) {
                    //Chrome
                    if (index % 5 == 0) {
                        $div.css('padding-bottom', '10px');
                    } else {
                        $div.css('padding-bottom', '11px');
                    }
                } else {
                    //FF
                    if (index % 10 == 0) {
                        $div.css('padding-bottom', '12px');
                    }
                    else if (index % 2 == 0 || index % 3 == 0) {
                        $div.css('padding-bottom', '11px');
                    }
                }
                $div.appendTo('#' + detailContainerId);
            }
        });

        $.each($('#' + detailContainerId + ' div span a'), function (index, span) {
            shortText($(span), maxTextWidth(chart, index + 1));
        });

        var $getMoreDataElement = $('#' + getMoreDataElementId());
        if (typeof ($getMoreDataElement) !== 'undefined') {
            $getMoreDataElement.remove();
        }
        if (metadataSerie.data[0].dataLeft) {
            var moreDataCount = Math.min(initData.PageSize, metadataSerie.data[0].dataLeft);
            if (moreDataCount > 0) {
                $(loadNextPoolsTemplate.replace('{id}', getMoreDataElementId())
                                       .replace('{0}', moreDataCount))
                                       .appendTo('#' + detailContainerId);

                $('#' + getMoreDataElementId() + ' a').click(function() {
                    loadData(true);
                });
            }
        }
    });

    // hack for IE7 and IE8 where core creates the loader by itself (instead of highcharts)
    // need to change it's padding-top css attribute as it is done for Highcharts in config option
    var $loaderDiv = $('#' + initData.RenderTo + ' .chartLoadingMessage');
    if ($loaderDiv) {
        $loaderDiv.css('padding-top', config.loading.labelStyle.paddingTop);
    }
};

var maxTextWidth = function (chart, index) {
    var units = 0;
    units += chart.series[0].data[index].y; // serieOffset
    units += chart.series[1].data[index].y; // serieArrayOverhead

    return units * ((chart.plotWidth * 0.75) / chart.yAxis[0].dataMax) - 2 + parseInt($('#' + chart.options.chart.renderTo).css('left'));
};

var shortText = function ($trimmed, lengthInPixels) {
    var text = $trimmed.text();
    $trimmed.attr('title', text);

    if ($trimmed.width() > lengthInPixels) {
        $trimmed.text(text + '...');
        while ($trimmed.width() > lengthInPixels) {
            if (text.length == 0) {
                return;
            }

            text = text.substring(0, text.length - 1);
            $trimmed.text(text + '...');
        }
    }
};

var setExpander = function (state) {
    var $containerElement = $('#' + detailContainerId);
    var $expanderElement = $('#' + expanderImageId);
    if (state) {
        $containerElement.show();
        $expanderElement.attr('src', '/Orion/SRM/images/Button.Collapse.gif');
        $expanderElement.attr('alt', '[-]');
    } else {
        $containerElement.hide();
        $expanderElement.attr('src', '/Orion/SRM/images/Button.Expand.gif');
        $expanderElement.attr('alt', '[+]');
    }
};

this.expanderChangedHandler = function () {
    initData.Expanded = !$('#' + detailContainerId).is(':visible');

    $('#' + initData.RenderTo).empty();
    $('#' + detailContainerId).hide();

    loadData(false);
};

this.init = function (info) {
    initData = info;

    tableContainerId = 'srm-blockcapacitysummary-table-' + initData.ResourceId;
    detailContainerId = 'srm-blockcapacitysummary-details-' + initData.ResourceId;
    legendContainerId = 'srm-blockcapacitysummary-legend-' + initData.ResourceId;
    expanderImageId = 'srm-blockcapacitysummary-img-' + initData.ResourceId;
    objectNameId = 'srm-blockcapacitysummary-objectname-' + initData.ResourceId;

    createChart();
};
}
