﻿SW = SW || {};
SW.SRM = SW.SRM || {};
SW.SRM.Charts = SW.SRM.Charts || {};

SW.SRM.Charts.PerformanceSummary = function () {
    var netObjectId,
        resId,
        chartId,
        chartWidth,
        dataBindMethod,
        sampleSizeInMinutes,
        daysOfDataToLoad,
        chartInitialZoom,
        blocksVisible,
        startRange,
        endRange,
        chartTitle,
        chartSubTitle;
        

    var getPoolChartConfig = function () {
        return {
            chart: {
                type: 'line',
                borderRadius: 0,
                borderWidth: 0,
                spacingBottom: 2,
                spacingLeft: 0,
                spacingRight: 0,
                spacingTop: 0,
                margin: [0, 10, 10, 1],
                width: chartWidth + 20,
                backgroundColor: 'rgba(255, 255, 255, 0)',
                plotBackgroundColor: 'rgba(255, 255, 255, 0)',
                plotBorderWidth: 0,
                style: {},
                events: {
                    load: function () {
                        SW.SRM.Common.ChartSelectCurrentZoomButton(this);
                    }
                }
            },
           labels: {
                style: {
                    fontSize: '0.8em'
                }
            },
            legend: {
                enabled: false
            },
            navigator: {
                enabled: true,
            },
            rangeSelector: {
                labelStyle: {
                    color: 'black',
                    fontSize: '8pt'
                },
                buttons: [{ tag: '1h' }, { tag: '12h' }, { tag: '24h' }], // can't use localized value, so add tags
                srmCustomRender: function (self, min, max) {
                    var leftPosition = self.chart.plotWidth - self.chart.plotLeft - 108;
                    self.zoomText.attr('x', leftPosition);
                    leftPosition += self.zoomText.getBBox().width + 5;
                    for (var i = 0; i < self.buttons.length; i++) {
                        self.buttons[i].attr('x', leftPosition);
                        leftPosition += self.buttons[i].width;
                    }
                }
            },
            loading: {
                labelStyle: {
                    display: 'none'
                }
            },
            plotOptions: {
                series: {
                    connectNulls: true,
                    marker: {
                        enabled: false
                    }
                },
                line: {
                    lineWidth: 2
                }
            },
            xAxis: {
                labels: {
                    enabled: true
                },
                gridLineWidth: 0,
                lineWidth: 0,
                tickWidth: 0,
                type: 'datetime',
                events: {
                    setExtremes: function (event) {
                        var lang = Highcharts.getOptions().lang;
                        var zoomRange = Highcharts.dateFormat(lang.zoomRangeDateTimeFormat, event.min) + ' - ' + Highcharts.dateFormat(lang.zoomRangeDateTimeFormat, event.max);
                        $('#subtitile' + resId).text(zoomRange);
                    }
                },
                minRange: 3600 * 1000, //one hour
                min: chartInitialZoom == null ? startRange:null,
                max: chartInitialZoom == null ? endRange : null
            },
            tooltip: {
                shared: true,
                formatter: SW.SRM.Formatters.ChartTooltipFormatter,
                positioner: function (boxWidth, boxHeight, point) {
                    var boxX = point.plotX;
                    var boxY = point.plotY;
                    if (this.chart.plotWidth > point.plotX + boxWidth - 60) {
                        boxX = boxX + 10;
                    } else {
                        boxX = boxX - 40 - boxWidth;
                    }

                    if (this.chart.plotHeight > point.plotY + boxHeight) {
                        boxY = boxY + 40;
                    } else {
                        boxY = boxY + 10 - boxHeight;
                    }

                    return { x: boxX, y: boxY };
                }
            }
        };
    };

    var loadChart = function (isLoad, item, requestedState) {
        var chartSettings = {
            'renderTo': chartId,
            'dataUrl': "/Orion/SRM/Services/PerformanceSummaryService.asmx/GetData",
            'netObjectIds': [netObjectId],

            'title': chartTitle == null || typeof chartTitle === 'undefined' ? '' : chartTitle,
            'showTitle': true,
            'subtitle': chartSubTitle != "" ? chartSubTitle : "",

            'initialZoom': chartInitialZoom,
            'resourceId': resId,
            'item': item,
            'state': requestedState,
            'isLoad': isLoad,
            'dataBindMethod': dataBindMethod,
            'sampleSizeInMinutes': sampleSizeInMinutes,
            'daysOfDataToLoad': daysOfDataToLoad,
            'blocksVisible': blocksVisible
        };
        
        SW.Core.Charts.initializeStandardChart(chartSettings, SW.SRM.Common.TooltipHighlighter(getPoolChartConfig()), function () {
            var chart = $('#' + chartId).data();
            if (chart.series == 0) {
                $('#' + chartId).attr('style', 'width:100%; left:0;');
                $('#' + chartId).parent().height(200);
            } else {
                //due to auto refresh
                $('#' + chartId).removeAttr('style');
                $('#' + chartId).parent().removeAttr('style');

                $("div[id^='parent" + resId + "_']").each(function (index, parent) {
                    setExpander($(parent).attr('id').replace('parent' + resId + '_', ''), $(parent).hasClass('srm-lunperformancesummary-expanded'));
                });
            }
        });
    };

    var createChart = function () {
        var orgHighchartsRangeSelectorPrototypeRender = Highcharts.RangeSelector.prototype.render;
        Highcharts.RangeSelector.prototype.render = function (min, max) {
            orgHighchartsRangeSelectorPrototypeRender.apply(this, [min, max]);
            if (this.chart && this.chart.options && this.chart.options.rangeSelector && this.chart.options.rangeSelector.srmCustomRender) {
                this.chart.options.rangeSelector.srmCustomRender(this, min, max);
            }
        };

        var refresh = function () {
            loadChart(true, null, false);
        };
        if (typeof (SW.Core.View) != 'undefined' && typeof (SW.Core.View.AddOnRefresh) != 'undefined') {
            SW.Core.View.AddOnRefresh(refresh, chartId);
        }
        refresh();
    };

    var setExpander = function (item, state) {
        var postfix = resId + '_' + item;
        var $containerElement = $('#children' + postfix);
        var $expanderElement = $('#expander' + postfix);
        var $parentElement = $('#parent' + postfix);
        if (state) {
            $parentElement.addClass('srm-lunperformancesummary-expanded');
            $containerElement.show();
            $expanderElement.attr('src', '/Orion/SRM/images/Button.Collapse.gif');
            $expanderElement.attr('alt', '[-]');
        }
        else {
            $parentElement.removeClass('srm-lunperformancesummary-expanded');
            $containerElement.hide();
            $expanderElement.attr('src', '/Orion/SRM/images/Button.Expand.gif');
            $expanderElement.attr('alt', '[+]');
        }
    };

    this.expanderChangedHandler = function (item) {
        var requestedState = !$('#children' + resId + '_' + item).is(':visible');
        var chart = $('#' + chartId).data();

        var extremes = chart.xAxis[0].getExtremes();
        startRange = extremes.min;
        endRange = extremes.max;
        
        chartInitialZoom = null;
        if (chart.rangeSelector.selected != undefined) {
            chartInitialZoom = chart.rangeSelector.buttonOptions[chart.rangeSelector.selected].tag;
        }
        $('#' + chartId).data(null);
        $('#' + chartId).empty();
        loadChart(false, item, requestedState);
        setExpander(item, requestedState);
    };

    this.init = function (info) {
        netObjectId = info.NetObjectId;
        resId = info.ResourceId;
        chartWidth = info.ChartWidth;
        chartId = info.RenderTo;
        dataBindMethod = info.DataBindMethod;
        sampleSizeInMinutes = info.SampleSizeInMinutes;
        daysOfDataToLoad = info.DaysOfDataToLoad;
        chartInitialZoom = info.InitialZoom;
        blocksVisible = info.BlocksVisible;
        chartTitle = info.ChartTitle;
        chartSubTitle = info.ChartSubTitle;

        createChart();

        if (info.Selected != null && info.Selected.length != 0) {
            $.each(info.Selected, function(index, item) {
                setExpander(item, true);
            });
        }
    };
}
