﻿SW = SW || {};
SW.SRM = SW.SRM || {};
SW.SRM.Charts = SW.SRM.Charts || {};

SW.SRM.Charts.ArraysCapacitySummary = function () {
    var chartsArea,
        topChartsLegendArea,
        resourceInitData,
        resourceSettings,
        pagingToolbarArea,
        toolbarArea,
        pageIndex = 0,
        pageSize = 5,
        remainingItemsCount = 0,
        searchTextBox,
        showMoreElement,
        pageableDataTable,
        legendContainerId,
        transparentColor = 'rgba(255, 255, 255, 0)',
        topLegendControlTemplate = '<div class="srm-arraycapacitysummary-legend-item" title="{tooltip}">\
                                        <div class="srm-arraycapacitysummary-legend-marker" style="background-color: {color};"></div>\
                                        {label}\
                                    </div>',
        legendControlTemplate = '<div style="float:left;">\
                                <span class="srm-arraycapacitysummary-legend-control">{separator}</span>\
                                <span class="SRM_BoldText" style="color: {color};">{label}</span>\
                                </div>',
        tooltipWrapperHeader = '<div id="highchartTooltip">',
        tooltipWrapperFooter = '</div>',
        tooltipPoint = '<tr style="line-height: 90%; font-weight: bold;">\
                            <td style="border: 0; font-size: 11px; color: {series.color}; padding-top: 4px;">\
                                {series.name}:&nbsp;\
                            </td>\
                            <td style="border: 0px; font-size: 11px; padding-top: 4px;">\
                                <b>{point.y}</b>\
                            </td>\
                        </tr>',
        chartContainer = "<table>\
                            <tr>\
                                <td style='border-bottom: 0px;'>\
                                    <a id='arrayLink_{0}_{1}'><span id='arrayName_{0}_{1}' class='SRM_BoldText srm_word_wrap_text'></span></a>\
                                </td>\
                                <td style='border-bottom: 0px;'>\
                                    <div style='float:right;'><span id='arrayLegend_{0}_{1}'></span></div>\
                                </td>\
                            </tr>\
					        <tr>\
                                <td colspan='2' class='srm-arraycapacitysummary-band' >\
                                    <div id='arrayChartArea_{0}_{1}'></div>\
                                </td>\
                            </tr>\
                        </table>";
    
    var getPointFormat = function () {
        return tooltipPoint;
    };

    var getChartConfig = function (id, dataSeries) {
        return {
            chart: {
                renderTo: id,
                type: 'bar',
                height: 20,
                margin: 0,
                spacing: [0, 0, 0, 0],
                plotBorderWidth: 0,
                backgroundColor: transparentColor,
                plotBackgroundColor: transparentColor
            },
            xAxis: {
                labels: {
                    enabled: false
                },
                gridLineWidth: 0,
                lineWidth: 0,
                tickWidth: 0
            },
            yAxis: [
                {
                    labels: {
                        enabled: false
                    },
                    gridLineWidth: 0,
                    lineWidth: 0,
                    tickWidth: 0,
                    tickInterval: null,
                    title: {
                        text: null
                    },
                    reversed: true
                }
            ],
            plotOptions: {
                bar: {
                    borderWidth: 0
                },
                series: {
                    stacking: "percent",
                    animation: true,
                    stickyTracking: false,
                    groupPadding: 0,
                    pointPadding: 0,
                    borderWidth: 0,
                    shadow: false,
                    pointWidth: 15
                },
                line: {
                    visible: false
                },
                candlestick: {
                    lineWidth: 0
                }
            },
            series: [
                { name: dataSeries[2].Label, data: [{ y: dataSeries[2].Data[0].y, id: dataSeries[2].Data[0].y }], color: '#93008B' },
                { name: dataSeries[1].Label, data: [{ y: dataSeries[1].Data[0].y, id: dataSeries[1].Data[0].y }], color: '#006CA9' },
                {
                    name: dataSeries[0].Label,
                    data: [{ y: dataSeries[0].Data[0].y, id: dataSeries[0].Data[0].y }],
                    color: {
                        linearGradient: { x1: 1, x2: 0, y1: 0, y2: 0 },
                        stops: [[0, '#E2E2E2'], [1, '#F6F6F6']]
                    }
                }
            ],
            tooltip: {
                shared: false,
                formatter: function() {
                    {
                        var series = this.point.series;
                        var s = [series.chart.options.tooltip.headerFormat];
                        s.push((series.tooltipFormatter && series.tooltipFormatter(this)) ||
                            this.point.tooltipFormatter(series.chart.options.tooltip.pointFormat.replace("{point.y}", SW.SRM.Formatters.FormatCapacity(this.point.y))));
                        s.push(series.chart.options.tooltip.footerFormat || '');
                        return s.join('');
                    }
                },
                positioner: SW.Core.Charts.Tooltip.positioner,
                positioningEnabled: true,
                useHTML: true,
                backgroundColor: 'rgba(0,0,0,0)',
                borderWidth: 0,
                shadow: false,
                wrapperHeader: tooltipWrapperHeader,
                wrapperFooter: tooltipWrapperFooter,
                footerFormat: '</table>' + tooltipWrapperFooter,
                headerFormat: tooltipWrapperHeader + '<table><tr><td columnspan="2" style="border: 0px;font-size: 11px;"></td></tr>',
                pointFormat: getPointFormat()
            },
            navigator: {
                enabled: false
            },
            scrollbar: {
                enabled: false
            },
            rangeSelector: {
                enabled: false
            }
        };
    };

    var addTopChartsLegend = function (chartSeries) {
        $('#' + topChartsLegendArea).empty();
        $.each(chartSeries, function (j, serie) {
            var tagName = serie.options.name;
            var tooltip = '';
            if (typeof (resourceInitData.LegendMapping) !== "undefined")
                if (typeof (resourceInitData.LegendMapping[tagName]) !== "undefined") {
                    if (typeof (resourceInitData.LegendMapping[tagName]) === "string") {
                        serie.name = resourceInitData.LegendMapping[tagName];
                    }
                    else {
                        serie.name = resourceInitData.LegendMapping[tagName][0];
                        tooltip = resourceInitData.LegendMapping[tagName][1];
                    }

                    $(topLegendControlTemplate.replace('{color}', serie.color.stops ? serie.color.stops[0][1] : serie.color)
                                              .replace('{label}', serie.name)
                                              .replace('{tooltip}', tooltip))

                    .appendTo('#' + topChartsLegendArea);
                }
        });
    };

    var createChart = function (renderTo, dataSeries) {
        var config = $.extend(null,
            SW.Core.Charts.getBasicChartConfig(), getChartConfig(renderTo, dataSeries));
        SW.Core.Charts.createChart(config);
    };

    function RefreshDataTable()
    {
        var startIndex = pageIndex * pageSize;
        ORION.callWebService("/Orion/SRM/Services/ArraysCapacitySummaryService.asmx",
            "GetData", { 'resourceId': resourceInitData.ResourceId, 'dataSourceMethod': resourceInitData.DataSourceMethod, 'fromIndex': startIndex, 'pageSize': pageSize, 'arrayNameFilter': searchTextBox.val() },
            function (result) {
                $('#' + chartsArea).empty();
                $('#' + legendContainerId).empty();
                if (pageableDataTable != null)
                {
                    for (var i = 0; i < result.DataTable.Rows.length; i++)
                    {
                        var row = result.DataTable.Rows[i];
                        pageableDataTable.DataTable.Rows.push(row);
                    }
                }
                else
                {
                    pageableDataTable = result;
                }
                for (var i = 0; i < pageableDataTable.DataTable.Rows.length; i++)
                {
                    var row = pageableDataTable.DataTable.Rows[i];
                    var dataSeries = row[0].DataSeries;
                    var chartOptionsOverride = row[0].ChartOptionsOverride;
                    var currentIndex = i;
                    var renderTo = SW.SRM.Common.StringFormat("arrayChartArea_{0}_{1}", resourceInitData.ResourceId, currentIndex);
                    var container = SW.SRM.Common.StringFormat(chartContainer, resourceInitData.ResourceId, currentIndex);

                    $('#' + chartsArea).append(container);
                    $(SW.SRM.Common.StringFormat("#arrayName_{0}_{1}", resourceInitData.ResourceId, currentIndex)).text(chartOptionsOverride.chart.title);
                    $(SW.SRM.Common.StringFormat("#arrayLink_{0}_{1}", resourceInitData.ResourceId, currentIndex)).attr("href", SW.SRM.Common.StringFormat("/Orion/View.aspx?NetObject={0}", dataSeries[0].NetObjectId));
                    createChart(renderTo, dataSeries);

                    legendContainerId = SW.SRM.Common.StringFormat("arrayLegend_{0}_{1}", resourceInitData.ResourceId, currentIndex);
                    var chart = $('#' + renderTo).data();
                    if (i == 0) {
                        addTopChartsLegend(chart.series);
                    }

                    var chartAmount = 0;
                    $.each(chart.series, function (j, serie) {
                        var tagName = serie.options.name;
                        if (typeof (resourceInitData.LegendMapping) !== "undefined")
                            if (typeof (resourceInitData.LegendMapping[tagName]) !== "undefined") {
                                $(legendControlTemplate.replace('{color}', serie.color.stops ? '#B4B4B4' : serie.color)
                                        .replace('{separator}', chartAmount == 0 ? "&nbsp;" : "/")
                                        .replace('{label}', SW.SRM.Formatters.FormatCapacity(serie.yData[0])))
                                    .appendTo('#' + legendContainerId);
                                chartAmount++;
                            }
                    });
                    $(legendControlTemplate.replace('{color}', "#000000")
                        .replace('{separator}', "/")
                        .replace('{label}', SW.SRM.Formatters.FormatCapacity(chartOptionsOverride.chart.capacitytotal) + "&nbsp;" + "@{R=SRM.Strings;K=ArrayCapacitySummary_TotalLabel;E=js}"))
                        .appendTo('#' + legendContainerId);
                }
                UpdatePagingButton();
            });
    }

    function UpdatePagingButton()
    {
        remainingItemsCount = pageableDataTable.TotalRows - pageableDataTable.DataTable.Rows.length;
        if (remainingItemsCount > 0) {
            var nextPageSize = remainingItemsCount > pageSize ? pageSize : remainingItemsCount;
            var showMoreText = remainingItemsCount == 1 ? "@{R=SRM.Strings;K=ArrayCapacitySummary_ShowMoreSingularMessage;E=js}" : "@{R=SRM.Strings;K=ArrayCapacitySummary_ShowMorePluralMessage;E=js}";
            showMoreElement.nodeValue = showMoreText.replace('{0}', remainingItemsCount).replace('{1}', nextPageSize);
        }
        else
            showMoreElement.nodeValue = '';
    }

    function LoadNextItems()
    {
        if (remainingItemsCount > 0) {
            pageIndex++;
            RefreshDataTable();
        }
    }

    var initializePaging = function (initialSettings)
    {
        // use value from settings if specified and >0 otherwise leave default value (5)
        pageSize = initialSettings.pageSize || pageSize;
    }

    var initializeSearch = function (initialSettings)
    {
        searchTextBox = null;
        if (typeof (initialSettings.searchTextBoxId) !== "undefined" && initialSettings.searchTextBoxId != '') {
            searchTextBox = $('#' + initialSettings.searchTextBoxId);
        }
        if (typeof (searchTextBox) === "undefined" || searchTextBox == null) {
            // no search box, no search
            return;
        }

        var searchButton = null;
        if (typeof (initialSettings.searchButtonId) !== "undefined" && initialSettings.searchButtonId != '') {
            searchButton = $('#' + initialSettings.searchButtonId);
        }

        var triggerSearchFunction = function () {
            var searchText = searchTextBox.val();
            var attrVal = searchButton.attr('src');
            if (attrVal == '/Orion/images/Button.SearchIcon.gif') {
                if (typeof (searchText) != "undefined" && searchText.length > 0) {
                    searchButton.attr('src', '/Orion/images/Button.SearchCancel.gif');
                }
            } else {
                searchButton.attr('src', '/Orion/images/Button.SearchIcon.gif');
                searchTextBox.val('');
            }

            SearchClick();
        };

        // search textbox handler
        searchTextBox.unbind();
        searchTextBox.keyup(function (e) {
            if (e.keyCode == 13 || e.keyCode == 27) {
                triggerSearchFunction();
                return false;
            }
            else {
                searchButton.attr('src', '/Orion/images/Button.SearchIcon.gif');
            }
            return true;
        });
        // search button handler

        if (typeof (searchButton) !== "undefined" && searchButton != null) {
            searchButton.unbind();
            searchButton.click(function () {
                triggerSearchFunction();
                return false;
            });
        }
    }

    function SearchClick()
    {
        pageIndex = 0;
        RefreshDataTable();
    }

    /*
    Sample settings:
    {
        searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
        searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
        pageSize: '20'
    }
    */
    this.init = function (initData, settings) {
        resourceInitData = initData;
        resourceSettings = settings;
        chartsArea = "srm-arrayscapacitysummary-chart-area_" + resourceInitData.ResourceId;
        topChartsLegendArea = "srm-arrayscapacitysummary_top_charts_legend_area_" + resourceInitData.ResourceId;
        pagingToolbarArea = "srm-arrayscapacitysummary-pagingToolbar-area_" + resourceInitData.ResourceId;
        toolbarArea = "srm-arrayscapacitysummary-toolbar-area_" + resourceInitData.ResourceId;

        $('#' + chartsArea).empty();
        $('#' + pagingToolbarArea).empty();
        
        var showMoreButton = document.createElement('a');
        showMoreElement = document.createTextNode('');
        showMoreButton.appendChild(showMoreElement);
        showMoreButton.addEventListener('click', LoadNextItems);
        showMoreButton.setAttribute('class', 'srm-arraycapacitysummary-pagingButton');
        $('#' + pagingToolbarArea).append(showMoreButton);

        initializeSearch(resourceSettings);
        initializePaging(resourceSettings);

        RefreshDataTable();
    };
};
