﻿SW = SW || {};
SW.SRM = SW.SRM || {};
SW.SRM.Charts = SW.SRM.Charts || {};

SW.SRM.Charts.CapacityRisk = function () {
    var controlTemplate = '<table><tr><td rowspan="2"><div id="{chartId}"></div></td><td><span>@{R=SRM.Strings;K=CapacityRisk_Used_Label;E=js}: <b>{used}</b>@{R=SRM.Strings;K=Value_Unit_Percentage;E=js}</span></td></tr><tr><td class="srm-capacityrisk-direction {direction}"><span>{diff}@{R=SRM.Strings;K=Value_Unit_Percentage;E=js}</span></td></tr></table>';

    var getConfig = function () {
        return {
            chart: {
                type: 'line',
                width: 70,
                height: 20,
                spacing: 0,
                margin: 0,
                backgroundColor: 'rgba(255, 255, 255, 0)',
                plotBackgroundColor: 'rgba(255, 255, 255, 0)',
                plotBorderWidth: 0,
                style: {}
            },
            xAxis: {
                labels: { enabled: false },
                gridLineWidth: 0,
                lineWidth: 0
            },
            yAxis: [{
                labels: { enabled: false },
                gridLineWidth: 0,
                lineWidth: 0,
                tickWidth: 0,
                tickInterval: null,
                title: { text: null },
                unit: 'bytes',
                startOnTick: false,
                endOnTick: false,
                minPadding: 0,
                maxPadding: 0
            }],
            legend: { enabled: false },
            scrollbar: { enabled: false },
            rangeSelector: { enabled: false },
            navigator: { enabled: false },
            loading: {
                labelStyle: {
                    display: 'none'
                }
            },
            plotOptions: {
                series: {
                    marker: {
                        enabled: false
                    },
                    connectNulls: true
                },
                line: {
                    lineWidth: 0.8,
                    states: {
                        hover: {
                            lineWidth: 1,
                            marker: {
                                radius: 1
                            }
                        }
                    }
                }
            },
            tooltip: {
                positioningEnabled: true
            }
        };
    };

    this.createChart = function (config) {
        var chartId = config.placeHolderId + '_chart';
        $(controlTemplate.replace('{chartId}', chartId)
                         .replace('{used}', Math.floor(config.usedInPercent * 10) / 10.0)
                         .replace('{diff}', Math.abs(config.slopeChartData).toFixed(1))
                         .replace('{direction}', config.slopeChartData > 0
                                                   ? 'srm-capacityrisk-direction-up'
                                                   : config.slopeChartData < 0
                                                        ? 'srm-capacityrisk-direction-down'
                                                        : 'srm-capacityrisk-direction-right'))
                         .appendTo('#' + config.placeHolderId);

        var config = $.extend(true, {}, SW.Core.Charts.getBasicChartConfig()
                                      , getConfig()
                                      , {
                                          chart: {
                                              renderTo: chartId
                                          },
                                          series: [{
                                              data: config.data,
                                              name: config.name
                                          }]
                                      });

        setTimeout(function () { SW.Core.Charts.createChart(config); }, 1); // do not wait, do it async
    };
}
