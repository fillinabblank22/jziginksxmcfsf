﻿SW = SW || {};
SW.SRM = SW.SRM || {};
SW.SRM.Charts = SW.SRM.Charts || {};
SW.SRM.Charts.Options = SW.SRM.Charts.Options || {};

(function (ns) {

	/*Aggregated IOPS Performance chart options*/
	ns.AggregatedIOPSPerformance = function () {
		return {
		    title: { text: "@{R=SRM.Strings;K=SRM_Chart_IOPS_Title;E=js}" },
		    subtitle: { text: "@{R=SRM.Strings;K=SRM_Chart_IOPS_Title;E=js}" },
			yAxis: [{
				title: {
					margin: 10,
					text: "IOPs"
				},
				unit: "",
				labels: { align: "right", x: -8 },
				min: 0,
				startOnTick: false,
				endOnTick: true
			}],
			plotOptions: {
				line: {
					pointPadding: 0,
					borderColor: "#000000",
					borderWidth: 1,
					groupPadding: 0
				}
			},
			seriesTemplates: {
				Total: {
					name: "Total",
					type: "line",
					zIndex: 2
				},
				Read: {
					name: "Read",
					type: "line",
					zIndex: 2
				},
				Write: {
					name: "Write",
					type: "line",
					zIndex: 2
				},
				Other: {
					name: "Other",
					type: "line",
					zIndex: 2
				},
				Data_Navigator: {
					name: "Navigator",
					id: "navigator",
					showInLegend: false,
					visible: false
				}
			}
		};
	}();

	/*Aggregate Throughput Performance chart options*/
	ns.AggregateThroughputPerformance = function () {
		return {
		    title: { text: "@{R=SRM.Strings;K=SRM_Chart_Throughput_Title;E=js}" },
		    subtitle: { text: "@{R=SRM.Strings;K=SRM_Chart_Throughput_Title;E=js}" },
			yAxis: [{
				title: {
					margin: 10,
					text: "MB/second"
				},
				unit: "",
				labels: { align: "right", x: -8 },
				min: 0,
				startOnTick: false,
				endOnTick: true
			}],
			plotOptions: {
				line: {
					pointPadding: 0,
					borderColor: "#000000",
					borderWidth: 1,
					groupPadding: 0
				}
			},
			seriesTemplates: {
				Total: {
					name: "Total",
					type: "line",
					zIndex: 2
				},
				Read: {
					name: "Read",
					type: "line",
					zIndex: 2
				},
				Write: {
					name: "Write",
					type: "line",
					zIndex: 2
				},
				Data_Navigator: {
					name: "Navigator",
					id: "navigator",
					showInLegend: false,
					visible: false
				}
			}
		};
	}();

	/*Aggregated IOPS Performance chart options*/
	ns.AggregateIOSize = function () {
		return {
		    title: { text: "@{R=SRM.Strings;K=SRM_Chart_IOLatency_Title;E=js}" },
		    subtitle: { text: "@{R=SRM.Strings;K=SRM_Chart_IOLatency_Title;E=js}" },
			yAxis: [{
				title: {
					margin: 10,
					text: "MB"
				},
				unit: "MB",
				labels: { align: "right", x: -8 },
				min: 0,
				startOnTick: false,
				endOnTick: true
			}],
			plotOptions: {
				line: {
					pointPadding: 0,
					borderColor: "#000000",
					borderWidth: 1,
					groupPadding: 0
				}
			},
			seriesTemplates: {
				Total: {
					name: "Total",
					type: "line",
					zIndex: 2
				},
				Read: {
					name: "Read",
					type: "line",
					zIndex: 2
				},
				Write: {
					name: "Write",
					type: "line",
					zIndex: 2
				},
				Data_Navigator: {
					name: "Navigator",
					id: "navigator",
					showInLegend: false,
					visible: false
				}
			}
		};
	}();

	ns.LatencyHistogram = function () {
	    return {
	        highchartsChartType: 'chart',
	        chart: {
	            type: 'column',
	            animation: false,
	            zoomType: null,
	            events: {
	                load: function () {
	                    var chart = this;

	                    if (!chart.series || chart.series.length != 4) {
	                        return; // data is not ready yet
	                    }

	                    $.each(chart.options.rangeSelector.buttons, function (index, item) {
	                        if (chart !== undefined && chart.chartSettings !== undefined && item.tagName === chart.chartSettings.ResourceProperties.initserie) {
	                            chart.rangeSelector.clickButton(index, item, true);
	                            return false;
	                        }
	                    });
	                }
	            }
	        },
	        chartPreInitializers: [
                function (config) {
                    var lang = Highcharts.getOptions().lang;
                    config.rangeSelector.buttons[1].text = lang.zoomOneHour;
                    config.rangeSelector.buttons[2].text = lang.zoomTwelveHours;
                    config.rangeSelector.buttons[3].text = lang.zoomTwentyfourHours;
                }
	        ],
	        plotOptions: {
	            series: {
	                point: {
	                    events: {
	                        click: function (e) {
	                            var args = {
	                                parameters: {
	                                    ResourceFile: '/Orion/SRM/Resources/DrillDowns/LatencyHistogramList.ascx',
	                                    Category: this.category,
	                                    Width: 700,
	                                    CurrentZoom: this.series.chart.selectedRangeTagName,
	                                    AsBlock: this.series.chart.chartSettings.ResourceProperties.asblock
	                                }
	                            };
	                            SW.SRM.Redirector.Redirect(args);
	                        }
	                    }
	                }
	            },
	            column: {
	                color: '#006DAD',
	                shadow: false,
	                animation: false
	            }
	        },
	        yAxis: [{
	            title: {
	                margin: 10,
	                style: 'text-transform:none;',
	                text: null
	            },
	            labels: { align: 'right', x: -8 },
	            min: 0,
                max: 1,
	            startOnTick: false,
	            endOnTick: true,
	            minTickInterval: 1,
	            unit: 'empty'
	        }],
	        xAxis: {
	            events: {
	                setExtremes: function (e, xAxis) {
	                    var axis = xAxis || this;
	                    
	                    if (e && e.rangeSelectorButton && e.rangeSelectorButton.tagName) {
	                        axis.chart.selectedRangeTagName = e.rangeSelectorButton.tagName;
	                        $.each(axis.chart.series, function (index, item) {
	                            if (item.options.uniqueId === (e.rangeSelectorButton.tagName + axis.chart.chartSettings.netObjectIds[0])) {
	                                item.show();

	                                e.min = item.data[0].min;
	                                e.max = item.data[0].max;
	                                SW.Core.Charts.getBasicChartConfig().xAxis.events.setExtremes(e, axis);
	                            }
	                            else {
	                                item.hide();
	                            }
	                        });
	                    }
	                }
	            },
	            ordinal: true,
	            title: {
	                text: '@{R=SRM.Strings;K=LatencyHistogram_xAxis_Title_Milliseconds;E=xml}',
	                style: 'text-transform:none;'
	            },
	            categories: [],
	            type: 'category',
	            tickInterval: 1,
	            min: 0,
	            max: 0,
	            labels: {
	                rotation: -90,
	                align: 'right'
	            }
	        },
	        rangeSelector: {
	            enabled: true,
	            buttons: [{
	                tagName: 'dataLastPoll',
	                text: '@{R=SRM.Strings;K=LatencyHistogram_Zoom_LatestPoll;E=xml}'
	            }, {
	                tagName: 'data1h'
	            }, {
	                tagName: 'data12h'
	            }, {
	                tagName: 'data24h'
	            }],
	            buttonTheme: {
	                width: null,
	                states: {
	                    select: {
	                        style: {
	                            fontWeight: 'normal',
	                            color: '#000'
	                        }
	                    }
	                }
	            }
	        },
	        navigator: { enabled: false },
	        scrollbar: { enabled: false },
	        seriesTemplates: {}
	    };
	}();

}(SW.SRM.Charts.Options));