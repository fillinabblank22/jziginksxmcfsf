﻿SW = SW || {};
SW.SRM = SW.SRM || {};
SW.SRM.Charts = SW.SRM.Charts || {};

SW.SRM.Charts.ArrayStatusResource = function () {
	var mData;
	var mResId, mNetObjectId;
	var mIopsChart, mThgoughputChart;
	var currentBtnIndex;
	var millisecondsInOneHour = 3600000; // 60 * 60 * 1000
	var sampleSizeInMinutes, daysOfDataToLoad, chartInitialZoom;
    var aggThroughput, aggIops;

	var StringFormat = function (format) {
		var args = [];
		for (var i = 1; i < arguments.length; i++) { args.push(arguments[i]); }
		return format.replace(/\{(\d+)\}/g, function (m, i) {
			return args[i];
		});
	};
    
	var srmArrayStatustooltipPositioner = function (boxWidth, boxHeight, point) {
	    return {
	        x: Math.max(0, point.plotX - boxWidth / 2),
	        y: 0 - boxHeight + 190
	    };
	};

    var tooltipTemplateIOPS =   '<tr>' +
                                    '<td>' +
                                        'Date: <b>{point.x}</b>' +
                                    '</td>' +
                                '</tr>' +
                                '<tr>' +
                                    '<td>' +
                                        'Value: <b>{point.y} @{R=SRM.Strings;K=ArrayStatus_AggregateIops_Chart_Unit;E=js} </b>' +
                                    '</td>' +
                                '</tr>';
    var tooltipTemplate =   '<tr>' +
                                '<td>' +
                                    'Date: <b>{point.x}</b>' +
                                '</td>' +
                            '</tr> ' +
                            '<tr>' +
                                '<td>' +
                                    'Value: <b>{point.y} @{R=SRM.Strings;K=ArrayStatus_AggregateThgoughput_Chart_Unit;E=js}</b>' +
                                '</td>' +
                            '</tr>';

	function getConfig(id, showRangeSelector) {
		return {
		    chart: {
		        zoomType: null,
			    borderWidth: 0,
			    plotBorderWidth: 0,
			    marginTop: 60,
                height:150
			    },			
			title: {
			    text: showRangeSelector ? "<div class='sub-title srm-array-status-iops-title'>" +
			        "@{R=SRM.Strings;K=ArrayStatus_AggregateIops_Chart_Title;E=js}" +
			        "</div>" :
			        "<div class='sub-title srm-array-status-thgoughput-title'>" +
			            "@{R=SRM.Strings;K=ArrayStatus_AggregateThgoughput_Chart_Title;E=js}" +
			        "</div>",
			    useHTML: true,
			    enable: true,
			    y: 30,
			    style: {
			        cursor: 'pointer'
			    }
			},
			subtitle: {
			    enable: true,
			    useHTML: true,
			    style: {
			        cursor: 'pointer'
			    }
			},
			yAxis: [{ gridLineWidth: 0, labels: { enabled: false }, lineWidth: 2, lineColor: "#cccccc", offset: 1, tickLength: 0 }],
			xAxis: {
			    minRange: millisecondsInOneHour,
			    tickInterval: millisecondsInOneHour,
                gridLineWidth: 0, labels: { enabled: false }, lineWidth: 2, lineColor: "#cccccc", offset: 1
			},
			plotOptions: {
			    column: { stacking: "normal", pointPadding: 0, borderColor: "#fff", borderWidth: 0, groupPadding: 0.15, pointWidth: 6 },
			},
			navigator: { enabled: false },
			scrollbar: { enabled: false },
			rangeSelector: {
			    enabled: true,
			    selected: currentBtnIndex,
				srmUseCustomRender: true,
				srmVisible: showRangeSelector,
				srmTopOffset: 60,
				srmButtonClickHandler: onRangeButtonClick,
			    inputEnabled: false
			},
			tooltip: {
			    positioner: srmArrayStatustooltipPositioner,
			    positioningEnabled: true,
			    formatter: function () {
			        {
			            var _tooltip = showRangeSelector ? tooltipTemplateIOPS : tooltipTemplate;
			            _tooltip = "<table class='srm_array_status_tooltip'>" + _tooltip + "</table>";
			            _tooltip = !showRangeSelector ? _tooltip.replace('{point.y}', SW.SRM.Formatters.FormatCapacity(this.y, 2)) : _tooltip.replace('{point.y}', this.y.toFixed(2));
			            _tooltip = _tooltip.replace('{point.x}', Highcharts.dateFormat(this.points[0].series.chart.tooltip.options.xDateFormat, this.x));
			            return _tooltip;
			        }
			    }
			},
			seriesTemplates: { Data: { name: "Data", type: "column", zIndex: 1, color: "#006ca9" } }
		};
	};

	function onRangeButtonClick(index) {
	    updateColumnChartData(index);
		updateBarChartData(index);
	}

	function updateColumnChartData(index) {
	    $.each([mIopsChart, mThgoughputChart], function (i, elem) {
	        var chart = $("#" + this).data();
            if (index === 0 && chart.series.length > 0) {
			    var yData = chart.series[index].yData;
			    var dataLength = yData.length - 2;
			    if (elem === mIopsChart) {
			        aggIops = yData[dataLength];
			    } else if (elem === mThgoughputChart) {
			        aggThroughput = yData[dataLength];
			    }
			} else {
			    aggIops = null;
			    aggThroughput = null;
			}
			chart.rangeSelector.clickButton(index, chart.rangeSelector.buttonOptions[index]);
			chart.rangeSelector.isActive = true;
		});
	}

	function updateBarChartData(index) {
	    currentBtnIndex = index;
	    var data = mData[StringFormat("I{0}", currentBtnIndex)];
    
	    setTimeout(function () {
	        var iopsValue = (aggIops == null) ? data.AggregatedIopsValue : aggIops;
	        var throughputValue = (aggThroughput == null) ? data.AverageThroughputValue : aggThroughput;
	        if (iopsValue == null)
	            data.AggregatedIopsStatus = "up";
	        if (throughputValue == null)
	            data.AverageThroughputStatus = "up";

            $("#" + mIopsChart).find("span.SRM_ChartSubTitleSpan")
                .removeClass("up").removeClass("warning").removeClass("critical")
                .addClass(data.AggregatedIopsStatus);

            $("#" + mThgoughputChart).find("span.SRM_ChartSubTitleSpan")
                .removeClass("up").removeClass("warning").removeClass("critical")
                .addClass(data.AverageThroughputStatus);
           
            var iops = SW.SRM.Formatters.NumberWithCommas(iopsValue);
            var throughput = SW.SRM.Formatters.FormatCapacity(throughputValue, 2).split(" ")[0];
            $("#AggregatedIopsValue" + mResId).text(iopsValue == null ? "@{R=SRM.Strings;K=ArrayStatus_Aggregate_Chart_Unknown;E=js}" : iops, 2);
            $("#supAverageIopsUnit").text(iopsValue == null ? "" : "@{R=SRM.Strings;K=ArrayStatus_AggregateIops_Chart_Unit;E=js}");
            $("#AverageThroughputValue" + mResId).text(throughputValue == null ? "@{R=SRM.Strings;K=ArrayStatus_Aggregate_Chart_Unknown;E=js}" : throughput);
            $("#supAverageThroughputUnit").text(throughputValue == null ? "" : SW.SRM.Formatters.FormatCapacity(throughputValue, 2).split(" ")[1] + "@{R=SRM.Strings;K=ArrayStatus_AggregateThgoughput_Chart_Unit;E=js}");
            addIopsClickEvent('srm-array-status-iops-subtitle');
            addThroughputClickEvent("srm-array-status-throughput-subtitle");
            
            $(".srm-array-status-iops-subtitle. img").remove();
            $(data.AggregatedIopsImageStatus).prependTo($(".srm-array-status-iops-subtitl-text"));

            $(".srm-array-status-throughput-subtitle img").remove();
            $(data.AverageThroughputImageStatus).prependTo($(".srm-array-status-throughput-subtitle-text"));
   
        }, 100);
    }

    function addIopsClickEvent(elClass) {
        $('.' + elClass).unbind("click").click(function() {
            redirectToDetachedResource('line', 'IOPS',
                '@{R=SRM.Strings;K=IOPSPerformance_ResourceTitle;E=js}',
                '@{R=SRM.Strings;K=IOPSPerformance_ChartTitle;E=js}',
                '@{R=SRM.Strings;K=SRM_Chart_yAxis_Title_Iops;E=js}');
        });
    }
    
    function addThroughputClickEvent(elClass) {
        $('.' + elClass).unbind("click").click(function () {
            redirectToDetachedResource('line', 'Throughput',
                '@{R=SRM.Strings;K=ThroughputPerformance_ResourceTitle;E=js}',
                '@{R=SRM.Strings;K=ThroughputPerformance_ChartTitle;E=js}',
                '@{R=SRM.Strings;K=SRM_Chart_yAxis_Title_Throughput;E=js}');
        });
    }

    function redirectToDetachedResource(chartType, category, resourceTitle, chartTitle, yAxisTitle) {
        var detachedResource = '/Orion/SRM/Resources/PerformanceDrillDown/PerformanceDrillDownChart.ascx';
        var performanceServiceMethod = '/Orion/SRM/Services/PerformanceDrillDownService.asmx/GetPerformance';
        var resourceWidth = 800;
        var args = {
            parameters: {
                ResourceFile: detachedResource,
                ResourceTitle: resourceTitle,
                Width: resourceWidth,
                ChartTitle: chartTitle,
                ChartType: chartType,
                YAxisTitle: yAxisTitle,
                DataBindMethod: performanceServiceMethod,
                RequestParameter: category
            }
        };
        SW.SRM.Redirector.Redirect(args);
    }

    function createAggregatedIops(renderTo) {
        var data = mData[StringFormat("I{0}", currentBtnIndex)];
        var refresh = function () {
            var isVserver = (mNetObjectId.substring(0, 4) == "SMVS");
            var iopsUnits = " <sup id='supAverageIopsUnit' class='unit'>@{R=SRM.Strings;K=ArrayStatus_AggregateIops_Chart_Unit;E=js}</sup>";
            var iops = data.AggregatedIopsValue;

            if (iops == null) {
                iops = "@{R=SRM.Strings;K=ArrayStatus_Aggregate_Chart_Unknown;E=js}";
                iopsUnits = "";
            } else {
                iops = SW.SRM.Formatters.NumberWithCommas(data.AggregatedIopsValue, 2);
            }
            var params = {
                "renderTo": renderTo,
                "dataUrl": (isVserver) ? "/Orion/SRM/Services/ChartData.asmx/GetVserverSizeAggregatedIOPS" : "/Orion/SRM/Services/ChartData.asmx/GetArraySizeAggregatedIOPS",
                "netObjectIds": [mNetObjectId],
                "title": "",
                "subtitle": StringFormat("<div srm-asr-type='title' class='sub-title srm-array-status-iops-subtitle value'>" +
                            "<text><span class='SRM_ChartSubTitleSpan {0}' id='AggregatedIopsValue{1}'>{2}</span></text>{3}</div>",
                            data.AggregatedIopsStatus, mResId, iops, iopsUnits),
                "subTitleWidth": 200,
                "sampleSizeInMinutes": sampleSizeInMinutes,
                "daysOfDataToLoad": daysOfDataToLoad,
                "initialZoom": chartInitialZoom
            };
	        SW.Core.Charts.initializeStandardChart(params, getConfig(renderTo, true),
	            function () {
	                //this is the very custom method to hide sub-data, do not use anywhere!
	                if ($('#' + renderTo + ' div').hasClass('chartDataNotAvailableArea')) {
	                    $('#aggregate-container-' + renderTo).hide();
	                    $('.table-row-' + renderTo).hide();
	                } else {
	                    $('#aggregate-container-' + renderTo).show();
	                    $('.table-row-' + renderTo).show();
	                }
	                $('#chartDataNotAvailable').width(170);
	                addIopsClickEvent('srm-array-status-iops-title');
	                addIopsClickEvent('srm-array-status-iops-subtitle');
	            });
	    };
		if (typeof (SW.Core.View) != "undefined" && typeof (SW.Core.View.AddOnRefresh) != "undefined") {
			SW.Core.View.AddOnRefresh(refresh, renderTo);
		}
		refresh();
	}
    function createAverageThroughput(renderTo) {
        var data = mData[StringFormat("I{0}", currentBtnIndex)];
		var refresh = function () {
		    var isVserver = (mNetObjectId.substring(0, 4) == "SMVS");
		    var throughputUnits = " <sup id='supAverageThroughputUnit' class='unit'>" +
               SW.SRM.Formatters.FormatCapacity(data.AverageThroughputValue, 2).split(" ")[1] +
               "@{R=SRM.Strings;K=ArrayStatus_AggregateThgoughput_Chart_Unit;E=js}</sup>";
		    var throughput = data.AverageThroughputValue;
		    if (throughput == null) {
		        throughput = "@{R=SRM.Strings;K=ArrayStatus_Aggregate_Chart_Unknown;E=js}";
		        throughputUnits = "";
		    } else {
		        throughput = SW.SRM.Formatters.FormatCapacity(data.AverageThroughputValue, 2).split(" ")[0];
		    }
		    var params = {
		        "renderTo": renderTo,
		        "dataUrl": (isVserver) ? "/Orion/SRM/Services/ChartData.asmx/GetVserverSizeAverageThroughput" : "/Orion/SRM/Services/ChartData.asmx/GetArraySizeAverageThroughput",
		        "netObjectIds": [mNetObjectId],
		        "title": "",
		        "subtitle": StringFormat("<div srm-asr-type='title' class='sub-title srm-array-status-throughput-subtitle value'>" +
                            "<text><span class='SRM_ChartSubTitleSpan {0}' id='AverageThroughputValue{1}'>{2}</span></text> {3}</div>",
                            data.AverageThroughputStatus, mResId, throughput, throughputUnits),
		        "sampleSizeInMinutes": sampleSizeInMinutes,
		        "daysOfDataToLoad": daysOfDataToLoad,
		        "initialZoom": chartInitialZoom
		    };
		    SW.Core.Charts.initializeStandardChart(params, getConfig(renderTo, false),
			    function () {
			        $('#chartDataNotAvailable').width(170);
			        addThroughputClickEvent("srm-array-status-thgoughput-title");
			        addThroughputClickEvent("srm-array-status-throughput-subtitle");
			    }
			);
		};
		if (typeof (SW.Core.View) != "undefined" && typeof (SW.Core.View.AddOnRefresh) != "undefined") {
			SW.Core.View.AddOnRefresh(refresh, renderTo);
		}
		refresh();
	}

    this.init = function (info) {
        mData = info.Data;
		mResId = info.ResourceId;
		mNetObjectId = info.NetObjectId;

	    sampleSizeInMinutes = info.SampleSizeInMinutes;
	    daysOfDataToLoad = info.DaysOfDataToLoad;
	    chartInitialZoom = info.ChartInitialZoom;
	    currentBtnIndex = Math.floor(parseInt(info.ChartInitialZoom) / 12);

		mIopsChart = StringFormat("srm-asr-c1{0}", mResId);
	    mThgoughputChart = StringFormat("srm-asr-c2{0}", mResId);

		if ($('#' + mIopsChart).length > 0)
		    createAggregatedIops(mIopsChart);

		if ($('#' + mThgoughputChart).length > 0)
		    createAverageThroughput(mThgoughputChart);
	};
}
SW.SRM.Charts.ArrayStatusResource.create = function (info) {
	var res = new SW.SRM.Charts.ArrayStatusResource();
	res.init(info);
}