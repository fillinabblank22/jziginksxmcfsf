﻿SW = SW || {};
SW.SRM = SW.SRM || {};
SW.SRM.Charts = SW.SRM.Charts || {};


SW.SRM.Charts.AggregatedNASVolumeCapacity = function () {
    var infoData,
        chartId,
        legendContainerId,

        tooltipTemplate = '<tr style="line-height: 90%; font-weight: bold;"><td style="border: 0px; font-size: 12px; padding-top: 4px;"><b><span>{point.y}</span></b></td></td></tr>',

        legendControlTemplate = '<tr><td><div style="background-color: {color};"></div></td><td><span>{label}</span></td><td class="small"><span>{amount}</span></td><td class="small"><span>{percentage}@{R=SRM.Strings;K=AggregatedNASVolumeCapacity_PercentSign;E=js}</span></td>',
        summaryLegendControlTemplate = '<tr><td></td><td class="total"><span>@{R=SRM.Strings;K=AggregatedNASVolumeCapacity_TotalSizeLabel;E=js}</span></td><td class="small"><span>{amount}</span></td><td></td>';

    var chartConfig = function () {
        return {
            highchartsChartType: "chart",
            chart: {
                type: 'pie',
                plotBorderWidth: null,
                width: 180,
                height: 180,
                spacing: 0,
                margin: 0,
                events: {
                    init: function () {
                        var localPointTooltipFormatter = Highcharts.Point.prototype.tooltipFormatter;
                        Highcharts.Point.prototype.tooltipFormatter = function (pointFormat) {
                            if (typeof (this.series.chart.options.tooltip.srmFormatter) === 'function') {
                                pointFormat = this.series.chart.options.tooltip.srmFormatter.apply(this, [pointFormat]);
                            }

                            return localPointTooltipFormatter.apply(this, [pointFormat]);
                        };
                    }
                }
            },
            navigator: {
                enabled: false
            },
            scrollbar: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    size: '100%',
                    cursor: 'pointer',
                    animation: true,
                    borderWidth: 0,
                    shadow: false,
                    dataLabels: {
                        enabled: false
                    }
                }
            },
            tooltip: {
                pointFormat: tooltipTemplate,
                srmFormatter: function (pointFormat) {
                    return pointFormat.replace('{point.y}', SW.SRM.Formatters.FormatCapacity(this.y));
                }
            },
            seriesTemplates: {}
        };
    };

    var createChart = function () {
        var refresh = function () {
            var params = {
                'renderTo': chartId,
                'dataUrl': infoData.DataUrl,
                'netObjectIds': [infoData.NetObjectId],
                'showTitle': false,
                'title': ''
            };
            
            SW.Core.Charts.initializeStandardChart(params, chartConfig(), function () {
                var chart = $('#' + chartId).data();
                
                if (chart.series.length == 0 || chart.series[0].data.length < 2) {
                    $('#' + chartId).removeClass('srm-aggregatedNASVolumeCapacity-chart');
                    $('#' + chartId).addClass('srm-aggregatedNASVolumeCapacity-chart-nodata');
                    SW.SRM.Common.SetVisible($("div[resourceid='" + infoData.ResourceId + "']"), false);
                    return;
                }
                var data = chart.series[0].data;
                var total = data[0].y + data[1].y;
                var $table = $('<table>');
                
                var percentData0 = ((data[0].y / total) * 100);
                var percentData1 = 100;
                if (percentData0 > 0 && percentData0 < 0.5) {
                    percentData0 = "< 1";
                    percentData1 = "> 99";
                } else {
                    percentData0 = percentData0.toFixed(0);
                    percentData1 = (100 - percentData0).toFixed(0);
                }

                $table.append(legendControlTemplate.replace('{color}', data[0].color)
                                                   .replace('{label}', data[0].name)
                                                   .replace('{amount}', SW.SRM.Formatters.FormatCapacity(data[0].y))
                                                   .replace('{percentage}', percentData0));
                $table.append(legendControlTemplate.replace('{color}', data[1].color)
                                                   .replace('{label}', data[1].name)
                                                   .replace('{amount}', SW.SRM.Formatters.FormatCapacity(data[1].y))
                                                   .replace('{percentage}', percentData1));
                $table.append(summaryLegendControlTemplate.replace('{amount}', SW.SRM.Formatters.FormatCapacity(total)));
                $table.append('</table>');

                $('#' + legendContainerId).empty();
                $table.appendTo('#' + legendContainerId)
            });
        };
        if (typeof (SW.Core.View) !== 'undefined' && typeof (SW.Core.View.AddOnRefresh) !== 'undefined') {
            SW.Core.View.AddOnRefresh(refresh, chartId);
        }
        refresh();
    };

    this.init = function (info) {
        infoData = info;
        chartId = 'srm-aggregatedNASVolumeCapacity-chart-' + infoData.ResourceId;
        legendContainerId = 'srm-aggregatedNASVolumeCapacity-legend-' + infoData.ResourceId;

        createChart();
    };
};
