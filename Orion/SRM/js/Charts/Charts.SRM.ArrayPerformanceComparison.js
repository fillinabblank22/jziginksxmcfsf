﻿SW = SW || {};
SW.SRM = SW.SRM || {};
SW.SRM.Charts = SW.SRM.Charts || {};

SW.SRM.Charts.ArrayPerformanceComparison = function() {

    var netObjectId,
        resId,
        theChartId,
        legendContainerId,
        summaryContainerId,
        daysOfDataToLoad,
        sampleSizeInMinutes,
        firstComparisonChoice,
        secondComparisonChoice,
        chartInitialZoom,
        chartTitle,
        chartSubTitle;
    var legendControlTemplate = '<div class="srm-performance-comparison-legend-item"><div class="srm-performance-comparison-legend-marker" style="background-color: {color};"></div>{label}<div>';
    var firstComparisonChoiceColor = "#A87000";
    var secondComparisonChoiceColor = "#515151";

    var tooltipFormatter = function () {
        var s = '<table class="srm-performance-comparison-tooltip"><tr><td><b>' + SW.SRM.Formatters.FormatDate(new Date(this.x)) + '</b></td><td/></tr>';
        $.each(this.points, function (i, point) {
            var isIops = (i == 0 ? firstComparisonChoice : secondComparisonChoice).indexOf("IOPS") > -1;
            s += '<tr class="' + point.series.swState + '"><td>'
                + point.series.name + ': </td><td><b>'
                  // IOPs must be rounded, while capacities should have 2 decimals.
                + SW.SRM.Formatters.FormatTinyNumber(point.y, !isIops)
                + '</b></td></td></tr>';
            });
        s += '</table>';
        return s;
    };

    var chartConfig = function () {
        return {
            title: { text: "@{R=SRM.Strings;K=Resource_Title_PerformanceComparison;E=js}" },
            subtitle: { text: "@{R=SRM.Strings;K=Resource_Subtitle_PerformanceComparison;E=js}" },
            yAxis: [{
                    title: {
                        margin: 10,
                        text: eval('firstComparisonChoiceLabel_' + resId),
                        style: "text-transform:none;"
                    },
                    labels: { align: "right", x: -8 },
                    min: 0.00,
                    startOnTick: true,
                    endOnTick: true
                },
                {
                    opposite: true,
                    title: {
                        margin: 15,
                        text: eval('secondComparisonChoiceLabel_' + resId),
                        style: "text-transform:none;"
                    }
                }],
            plotOptions: {
                line: {
                    pointPadding: 0,
                    borderColor: "#000000",
                    borderWidth: 1,
                    groupPadding: 0
                }
            },
            seriesTemplates: {
                firstComparisonChoice: {
                    type: "line",
                    zIndex: 2,
                    color: firstComparisonChoiceColor
                },
                secondComparisonChoice: {
                    type: "line",
                    zIndex: 2,
                    color: secondComparisonChoiceColor
                }
            },
            // 'chart' and 'rangeSelector' are needed to add empty space between plot area and zoom buttons.
            // Drop downs (second/secondComparisonChoice) to be positioned into that space.
            chart: {
                marginTop: 100,
                events: {
                    init: function() {
                        var orgHighchartsRangeSelectorPrototypeRender = Highcharts.RangeSelector.prototype.render;
                        Highcharts.RangeSelector.prototype.render = function(min, max) {
                            orgHighchartsRangeSelectorPrototypeRender.apply(this, [min, max]);

                            if (this.chart && this.chart.options && this.chart.options.rangeSelector && this.chart.options.rangeSelector.srmCustomRender) {
                                this.chart.options.rangeSelector.srmCustomRender(this, min, max);
                            }
                        };
                    },
                    load: function () {
                        SW.SRM.Common.ChartSelectCurrentZoomButton(this);
                    }
                }
            },
            rangeSelector: {
                buttons: [{ tag: '1h' }, { tag: '12h' }, { tag: '24h' }], // can't use localized value, so add tags
                srmCustomRender: function (self, min, max) {
                    var offset = 30;

                    if (self.zoomText.defaultY === undefined) {
                        self.zoomText.defaultY = self.zoomText.attr('y');
                    }
                    self.zoomText.attr('y', self.zoomText.defaultY - offset);

                    for (var i = 0; i < self.buttons.length; i++) {
                        if (self.buttons[i].defaultY === undefined) {
                            self.buttons[i].defaultY = self.buttons[i].attr('y');
                        }
                        self.buttons[i].attr('y', self.buttons[i].defaultY - offset);
                    }
                    // y-axis width change depending on length of values, so we dynamically align drop down above it
                    $('#srm-performance-comparison-choices-container-' + resId)
                        .css('margin-left', (self.chart.axisOffset[3] + 10) + 'px')
                        .css('width', self.chart.plotWidth + 'px');
                }
            },
             xAxis: {
                 minRange: 3600 * 1000 //one hour
            },
            tooltip: {
                formatter: tooltipFormatter
            }
        };
    };

    var createChart = function () {
        var refresh = function () {
            var params = {
                'renderTo': theChartId,
                'dataUrl': "/Orion/SRM/Services/ArrayPerformanceComparisonService.asmx/GetData",
                'netObjectIds': [netObjectId],
                'daysOfDataToLoad': daysOfDataToLoad,
                'sampleSizeInMinutes': sampleSizeInMinutes,
                'firstComparisonChoice': firstComparisonChoice,
                'secondComparisonChoice': secondComparisonChoice,
                'title': chartTitle != "" ? chartTitle : '@{R=SRM.Strings;K=Resource_Subtitle_PerformanceComparison;E=js}'
                    .replace('{0}', eval('firstComparisonChoiceLabel_' + resId))
                    .replace('{1}', eval('secondComparisonChoiceLabel_' + resId)),
                'subtitle': chartSubTitle != "" ? chartSubTitle : "$[ZoomRange]",
                'showTitle': true,
                'initialZoom': chartInitialZoom,
            };
            
            SW.Core.Charts.initializeStandardChart(params, SW.SRM.Common.TooltipHighlighter(chartConfig()), function () {
                var chart = $('#' + theChartId).data();
                $('#' + legendContainerId).empty();
                $.each(chart.series, function (i, serie) {
                    if (serie.name == 'Navigator') {
                        return;
                    }
                    $(legendControlTemplate.replace('{color}', serie.color)
                        .replace('{label}', serie.name))
                        .appendTo('#' + legendContainerId);
                });
                if (chart.series.length == 0) {
                    $('#srm-performance-comparison-choices-container-' + resId).hide();
                    $('#srm-performance-comparison-main-div-' + resId).attr('style', 'min-height: 0px !important; ');
                } else {
                    $('#srm-performance-comparison-choices-container-' + resId).show();
                }
            });
        };
        if (typeof (SW.Core.View) != 'undefined' && typeof (SW.Core.View.AddOnRefresh) != 'undefined') {
            SW.Core.View.AddOnRefresh(refresh, theChartId);
        }
        refresh();
    };

    this.init = function (info) {
        netObjectId = info.NetObjectId;
        resId = info.ResourceId;

        sampleSizeInMinutes = info.SampleSizeInMinutes;
        daysOfDataToLoad = info.DaysOfDataToLoad;
        chartInitialZoom = info.ChartInitialZoom;
        chartTitle = info.ChartTitle;
        chartSubTitle = info.ChartSubTitle;

        firstComparisonChoice = info.FirstComparisonChoice;
        secondComparisonChoice = info.SecondComparisonChoice;

        theChartId = 'srm-performance-comparison-summary-' + resId;
        legendContainerId = 'srm-performance-comparison-legend-' + resId;
        summaryContainerId = 'srm-performance-comparison-main-div-' + resId;

        createChart();
    };
};
