﻿(function (NS) {
	NS.wrap(NS.RangeSelector.prototype, "render", function (proceed, min, max) {

		var that = this, value = false;
		try {
		    value = that.chart.options.rangeSelector.srmUseCustomRender;
		}
		catch (ex) { }

		if (value === true) {
		    that.srmRender(min, max);
		} else {
			proceed.apply(that, [min, max]);
		}
	});

	NS.RangeSelector.prototype.srmRender = function (min, max) {
		var
			rangeSelector = this,
			chart = rangeSelector.chart,
			renderer = chart.renderer,
			container = chart.container,
			chartOptions = chart.options,
			navButtonOptions = chartOptions.exporting && chart.options.navigation.buttonOptions,
			options = chartOptions.rangeSelector,
			buttons = rangeSelector.buttons,
			lang = Highcharts.getOptions().lang,
			div = rangeSelector.div,
			inputGroup = rangeSelector.inputGroup,
			buttonTheme = options.buttonTheme,
			inputEnabled = options.inputEnabled !== false,
			states = buttonTheme && buttonTheme.states,
			plotLeft = chart.plotLeft,
			yAlign,
			buttonLeft;

		if (!rangeSelector.rendered) {

			rangeSelector.zoomText = renderer.text(lang.rangeSelectorZoom, plotLeft, chart.plotTop - (options.srmTopOffset + 10))
				.css(options.labelStyle)
				.add();
			if (!options.srmVisible) {
				rangeSelector.zoomText.hide();
			}

			buttonLeft = plotLeft + rangeSelector.zoomText.getBBox().width + 5;

			NS.each(rangeSelector.buttonOptions, function (rangeOptions, i) {
				buttons[i] = renderer.button(
						rangeOptions.text,
						buttonLeft,
						chart.plotTop - (options.srmTopOffset + 25),
						function () {
							options.srmButtonClickHandler(i);
						},
						buttonTheme,
						states && states.hover,
						states && states.select
					)
					.css({ textAlign: "center" })
					.add();

				if (!options.srmVisible) {
				    buttons[i].hide();
				}
				buttonLeft += buttons[i].width + (options.buttonSpacing || 0);

				if (rangeSelector.selected === i) {
					buttons[i].setState(2);
				}
			});
		}
		rangeSelector.rendered = true;
	};
}(Highcharts));