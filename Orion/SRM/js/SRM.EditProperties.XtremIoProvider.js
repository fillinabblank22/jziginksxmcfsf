﻿SW = SW || {};
SW.Core = SW.Core || {};

var xtremIoProvider = SW.Core.namespace("SW.SRM.EditProperties.XtremIoProvider");

var request;

xtremIoProvider.onCancelClick = function() {
    $("#testingConnectionMsg").hide();
    if (request) {
        request.abort();
    }
};

xtremIoProvider.hideError = function() {
    $("#testConnectionFailMsg").hide();
};

xtremIoProvider.hidePass = function () {
    $("#testConnectionPassMsg").hide();
};

xtremIoProvider.showPass = function(text) {
    $("#passMessageContent").empty();
    $("#passMessageContent").append(text);
    $("#testConnectionPassMsg").show();
};

xtremIoProvider.showLoading = function (visible) {
    if (visible) {
        $("#testingConnectionMsg").show();
        $('#testConnectionXtremIoButton').attr('disabled', 'disabled');
    } else {
        $("#testingConnectionMsg").hide();
        $('#testConnectionXtremIoButton').removeAttr('disabled');
    }
};

xtremIoProvider.ValidateConnection = function () {
    return SW.SRM.EditProperties.XtremIoProvider.TestConnection(false);
};

xtremIoProvider.ValidateDiscovery = function() {
    return SW.SRM.EditProperties.XtremIoProvider.TestConnection(true);
}

xtremIoProvider.TestConnection = function (runDiscovery) {
    var testCredentialDeferred = $.Deferred();
    var testGlobalCredentialDeferred = $.Deferred();

    var cfg = JSON.parse($('#hfEditPropertiesXtremIoCfg').val());
    var credentialId = cfg.CredentialId;
    var ip = cfg.IP;
    var engineid = $('#selPollingEngineId').length == 0 ? cfg.EngineId : $('#' + $('#selPollingEngineId').val()).val();

    var showFailMessageBox = function (message, errorMessage) {
        var title = "@{R=SRM.Strings;K=Provider_Step_Provider_Connecting_Error; E=js}".concat(' ').concat(ip);
        SW.SRM.Common.ShowErrorEditProperties(title, message, errorMessage);
    }

    SW.SRM.DiscoveryValidation.init(cfg.ArrayID, cfg.ArraySerialNumber, cfg.DeviceGroupId, request, testCredentialDeferred, '@{R=SRM.Strings;K=Provider_Step_Unexpected_Error; E=js}', showFailMessageBox);

    if (!pollingProperties.ValidateUserInput()) {
        return testGlobalCredentialDeferred.reject();
    }

    request = SW.SRM.Common.CallSrmWebService("/Orion/SRM/Services/EnrollmentWizardService.asmx",
                                'TestXtremIoCredentialsProvider',
                                {
                                    'credentialId': credentialId,
                                    'credentialName': null,
                                    'useHttp': true,
                                    'password': null,
                                    'username': null,
                                    'httpPort': 0,
                                    'httpsPort': 0,
                                    'ip': ip,
                                    'engineid': engineid,
                                    'saveProvider': runDiscovery
                                },

                              function (result) {
                                    if (result.IsSuccess) {                                        
                                        SW.SRM.EditProperties.XtremIoProvider.showLoading(false);
                                        SW.SRM.EditProperties.XtremIoProvider.showPass(result.Message);

                                        if (runDiscovery){
                                          SW.SRM.DiscoveryValidation.startDiscovery();
	                                    }
	                                    else {
	                                      testCredentialDeferred.resolve();
	                                    }
                                    }
                                    else {
                                        SW.SRM.EditProperties.XtremIoProvider.showLoading(false);

                                        showFailMessageBox(result.Message, result.ErrorMessage);

                                        SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
                                    }
                                },
                                function () {
                                    SW.SRM.Common.ShowError('Error during AJAX call');
                                    SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
                                });

    $.when(testCredentialDeferred).then(
    function (x) {
        SW.SRM.EditProperties.XtremIoProvider.showLoading(false);
        testGlobalCredentialDeferred.resolve();
    },
    function () {
        SW.SRM.EditProperties.XtremIoProvider.showLoading(false);
        testGlobalCredentialDeferred.reject();
    });

    SW.SRM.EditProperties.XtremIoProvider.hideError();
    SW.SRM.EditProperties.XtremIoProvider.hidePass();
    SW.SRM.EditProperties.XtremIoProvider.showLoading(true);

    return testGlobalCredentialDeferred.promise();
};

xtremIoProvider.MonitoredControlIds = ['pollEngineDescr'];
SW.SRM.EditProperties.ProviderChangeManager.InitProvider(xtremIoProvider, xtremIoProvider.ValidateDiscovery);