﻿
var pollingProperties = SW.Core.namespace("SW.SRM.EditProperties.PollingProperties");

$(function () {
    $("#pollingValidationFailMsg").hide();
});

pollingProperties.showError = function (message) {
    $("#pollingFailMessageContent").text(message);
    $("#pollingValidationFailMsg").show();
    $("#pollingValidationFailMsg").focus();
};

pollingProperties.ValidateUserInput = function () {
    $("#pollingValidationFailMsg").hide();
    
    return pollingProperties.ValidateTextBox('#editBoxPerf', '#labelEditBoxPerformance', '#cbSelectedPerformance', performancePollingMin)
        && ($('#CapacityPollingFrequencyRow').length == 0 || pollingProperties.ValidateTextBox('#editBoxCap', '#labelEditBoxCapacity', '#cbSelectedCapacity', performancePollingMin))
        && ($('#TopologyPollingFrequencyRow').length == 0 || pollingProperties.ValidateTextBox('#editBoxTopology', '#labelEditBoxTopology', '#cbSelectedTopology', topologyPollingMin));
}

pollingProperties.Validate = function () {
    var resultingDeferred = $.Deferred();
    if (pollingProperties.ValidateUserInput()) {
        return resultingDeferred.resolve();
    };
    return resultingDeferred.reject();
}

pollingProperties.MonitoredControlIds = ['pollEngineDescr'];
SW.SRM.EditProperties.ProviderChangeManager.InitProvider(pollingProperties, pollingProperties.Validate);

pollingProperties.validateKaminarioPerformancePollingRange = function (value, fieldName) {
    if (value <= 60 && (value % 5) != 0) {
        pollingProperties.showError(
            "@{R=SRM.Strings;K=PollingProperties_InvalidIntervalKaminario_ErrorMessage; E=js}"
                .replace('{0}', fieldName).replace('{1}', 1).replace('{2}', 60).replace('{3}', 5));
        return false;
    } else if (value > 60 && (value % 60) != 0) {
        pollingProperties.showError(
            "@{R=SRM.Strings;K=PollingProperties_InvalidIntervalKaminario_ErrorMessage; E=js}"
                .replace('{0}', fieldName).replace('{1}', 60).replace('{2}', 1440).replace('{3}', 60));
        return false;
    }
    return true;
}

pollingProperties.ValidateTextBox = function (textBoxId, labelId, checkboxId, minimum) {
    $("#validationFailMsg").hide();
    
    var textBoxLabel = $(labelId).text();
    var value = $(textBoxId).val();
    var checkbox = $(checkboxId).is(':checked');
    var checkboxVisible = $(checkboxId).is(':visible');
    var checkKaminario = $(textBoxId).attr('checkKaminario');
    
    if (!checkbox && checkboxVisible) {
        return true;
    }

    if (value == "") {
        pollingProperties.showError("@{R=SRM.Strings;K=PollingProperties_RequiredFieldValidator; E=js}".replace('{0}', textBoxLabel));
        return false;
    }
    if (isNaN(value)) {
        pollingProperties.showError("@{R=SRM.Strings;K=PollingProperties_InvalidNumber_ErrorMessage; E=js}".replace('{0}', textBoxLabel));
        return false;
    }
    var tbPerfInt = parseInt(value);
    if (tbPerfInt < minimum || tbPerfInt > performancePollingMax) {
        pollingProperties.showError("@{R=SRM.Strings;K=PollingProperties_InvalidInterval_ErrorMessage; E=js}".replace('{0}', textBoxLabel).replace('{1}', minimum).replace('{2}', performancePollingMax));
        return false;
    }
    if (checkKaminario === "true") {
        return pollingProperties.validateKaminarioPerformancePollingRange(tbPerfInt, textBoxLabel);
    }
    return true;
}