﻿function comboHasText(dropDownList, text) {
    return dropDownList.find('option').filter(function () {
        return $(this).text().toLowerCase() === text.toLowerCase();
    }).length > 0;
}