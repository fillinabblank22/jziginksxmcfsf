﻿
var pollingPropertiesAdvanced = SW.Core.namespace("SW.SRM.EditProperties.Advanced");

$(function () {
    pollingPropertiesAdvanced.init();
});

pollingPropertiesAdvanced.init = function() {
    pollingPropertiesAdvanced.collapsible($('img.collapsible'));
    if ($('#multipleAdvChange').length != 0) {
        pollingPropertiesAdvanced.disableable($('#multipleAdvChange'));
        pollingPropertiesAdvanced.setElmsDisabled($('#multipleAdvChange'));
    }
}

pollingPropertiesAdvanced.collapsible = function(el)
{
    el.click(function () {
        var img = $(this);
        var target = $('#' + img.attr('data-collapse-target'));
        var isExpanded = img.attr('src').toLowerCase().indexOf("button.expand") == -1;
        
        	if (isExpanded) {
        			target.hide();
        			img.attr('src', '/Orion/images/Button.Expand.gif');
        		} else {
        			target.show();
        			img.attr('src', '/Orion/images/Button.Collapse.gif');
        		}
        
        	return false;
        });
}

pollingPropertiesAdvanced.disableable = function (el) {
    el.click(function () {
        pollingPropertiesAdvanced.setElmsDisabled(el);
    });
}

pollingPropertiesAdvanced.setElmsDisabled = function (el) {
    var target = $('table.advancedMultipleSettings *');
    var checked = el.length == 0 || el.prop('checked');
    if (checked) {
        target.removeAttr("disabled");
    } else {
        target.attr('disabled', 'disabled');
    }
}