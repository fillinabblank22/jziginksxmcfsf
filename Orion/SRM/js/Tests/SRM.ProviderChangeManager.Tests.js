﻿/// <reference path="OrionRequirements.js" />
/// <reference path="../SRM.ProviderChangeManager.js" />

var span1;
var provider;

module("SRM.ProviderChangeManager.js related tests", {
    setup: function () {
        var input = document.createElement('input');
        input.id = 'ConnetionPropertyChangedField';
        document.documentElement.appendChild(input);

        var span1 = document.createElement('span');
        span1.id = 'span1';
        span1.innerHTML = 'span1Text';
        document.documentElement.appendChild(span1);

        provider = {};
    }
});

test("SW.SRM.EditProperties.ProviderChangeManager test positiveTest", function (assert) {
    var validationFinished = false;
    provider.MonitoredControlIds = ['span1'];
    SW.SRM.EditProperties.ProviderChangeManager.InitProvider(provider, function () { validationFinished = true; });

    assert.equal(provider.ConnectionProperties, 'span1Text');

    $('#span1').text('span1Text2');

    var validationFunction = provider.InitValidation();

    assert.ok(validationFunction != null);
    assert.ok($('#ConnetionPropertyChangedField').val() == 'true');

    validationFunction.call(this);
    assert.ok(validationFinished);
});

test("SW.SRM.EditProperties.ProviderChangeManager test noChangeTest", function (assert) {
    $('#span1').text('span1Text');
    $('#ConnetionPropertyChangedField').val(false);
    var validationFinished = false;
    provider.MonitoredControlIds = ['span1'];
    SW.SRM.EditProperties.ProviderChangeManager.InitProvider(provider, function () { validationFinished = true; });

    assert.equal(provider.ConnectionProperties, 'span1Text');

    var validationFunction = provider.InitValidation();
    assert.ok(validationFunction != null);
    assert.ok($('#ConnetionPropertyChangedField').val() == 'false');

    validationFunction.call(this);
    assert.ok(validationFinished);
});

test("SW.SRM.EditProperties.ProviderChangeManager test wrongElementTest", function (assert) {
    $('#ConnetionPropertyChangedField').val(false);
    var validationFinished = false;
    provider.MonitoredControlIds = ['span999'];
    SW.SRM.EditProperties.ProviderChangeManager.InitProvider(provider, function () { validationFinished = true; });

    assert.equal(provider.ConnectionProperties, '');
    var validationFunction = provider.InitValidation();

    assert.ok(validationFunction != null);
    assert.ok($('#ConnetionPropertyChangedField').val() == 'false');

    validationFunction.call(this);
    assert.ok(validationFinished);
});
