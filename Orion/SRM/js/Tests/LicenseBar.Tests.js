﻿/// <reference path="OrionRequirements.js" />
/// <reference path="../../Config/Controls/LicenseBar.js" />

module("ArrayStep.js related tests", {
	setup: function () {
		SW = SW || {};
		SW.SRM = SW.SRM || {};
		SW.SRM.ArraySelector = SW.SRM.ArraySelector || {};

		$('body').append('<span id="licenseContent" />');
		$('body').append('<div id="licenseBreakerLine" />');
		$('body').append('<span id="licenseExceeded" />');
		$('body').append('<input id="hiddenRequired" value="" />');
		$('body').append('<input id="hiddenInUse" value="" />');
		$('body').append('<input id="hiddenTotal" value="" />');
		$('body').append('<input id="hiddenLicViolation" value="" />');
	},
	teardown: function () {
		$('#licenseContent').remove();
		$('#licenseBreakerLine').remove();
		$('#licenseExceeded').remove();
		$('#hiddenRequired').remove();
		$('#hiddenInUse').remove();
		$('#hiddenTotal').remove();
		$('#hiddenLicViolation').remove();

		//Just in case.
		SW.SRM.ArraySelector = undefined;
	}
});

test("LicenseBar positive render - required + used disks equals to total", function (assert) {
	SW.SRM.ArraySelector = undefined;

	var licenseDetailsText = "Licenses required: {0} ({1} in use / {2} remaining / {3} total)",
        unlimitedText = "My unlimited text",
        isEval = 0,
        isExpired = 0;

	$('#hiddenTotal').val('20');
	$('#hiddenInUse').val('10');
	$('#hiddenRequired').val('10');

	SW.SRM.LicenseBar.init(licenseDetailsText, unlimitedText, isEval, isExpired);

	assert.equal($('#hiddenLicViolation').val(), '0', 'Expected no license violation');
	assert.equal($('#licenseExceeded').css('display'), 'none', 'Expected no error message');
	assert.equal($('#licenseContent').text(), 'Licenses required: 10 (10 in use / 10 remaining / 20 total)');
});

test("LicenseBar positive render - required + used disks is bigger than total - still remains available disks so add the array regardless of disk count on that array", function (assert) {
	SW.SRM.ArraySelector = undefined;

	var licenseDetailsText = "Licenses required: {0} ({1} in use / {2} remaining / {3} total)",
        unlimitedText = "My unlimited text",
        isEval = 0,
        isExpired = 0;

	$('#hiddenTotal').val('20');
	$('#hiddenInUse').val('19');
	$('#hiddenRequired').val('10');

	SW.SRM.LicenseBar.init(licenseDetailsText, unlimitedText, isEval, isExpired);

	assert.equal($('#hiddenLicViolation').val(), '0', 'Expected no license violation');
	assert.equal($('#licenseExceeded').css('display'), 'none', 'Expected no error message');
	assert.equal($('#licenseContent').text(), 'Licenses required: 10 (19 in use / 1 remaining / 20 total)');
});

test("LicenseBar negative render - no more disks available", function (assert) {
	SW.SRM.ArraySelector = undefined;

	var licenseDetailsText = "Licenses required: {0} ({1} in use / {2} remaining / {3} total)",
        unlimitedText = "My unlimited text",
        isEval = 0,
        isExpired = 0;

	$('#hiddenTotal').val('20');
	$('#hiddenInUse').val('50');
	$('#hiddenRequired').val('10');

	SW.SRM.LicenseBar.init(licenseDetailsText, unlimitedText, isEval, isExpired);

	assert.equal($('#hiddenLicViolation').val(), '1', 'Expected license violation');
	assert.equal($('#licenseExceeded').css('display'), 'inline', 'Expected error message');
	assert.equal($('#licenseContent').text(), 'Licenses required: 10 (50 in use / 0 remaining / 20 total)');
});

test("LicenseBar negative render - expired", function (assert) {
	SW.SRM.ArraySelector = undefined;

	var licenseDetailsText = "Licenses required: {0} ({1} in use / {2} remaining / {3} total)",
        unlimitedText = "My unlimited text",
        isEval = 0,
        isExpired = 1;

	$('#hiddenTotal').val('20');
	$('#hiddenInUse').val('50');
	$('#hiddenRequired').val('10');

	SW.SRM.LicenseBar.init(licenseDetailsText, unlimitedText, isEval, isExpired);

	assert.equal($('#hiddenLicViolation').val(), '1', 'Expected license violation');
	assert.equal($('#licenseExceeded').css('display'), 'inline', 'Expected error message');
	assert.equal($('#licenseContent').css('display'), 'none', 'Expected no content of license details');
});

test("LicenseBar positive render - unlimited", function (assert) {
	SW.SRM.ArraySelector = undefined;

	var licenseDetailsText = "Licenses required: {0} ({1} in use / {2} remaining / {3} total)",
        unlimitedText = "My unlimited text",
        isEval = 1,
        isExpired = 0;

	$('#hiddenTotal').val('0x7fffffff');
	$('#hiddenInUse').val('50');
	$('#hiddenRequired').val('10');

	SW.SRM.LicenseBar.init(licenseDetailsText, unlimitedText, isEval, isExpired);

	assert.equal($('#hiddenLicViolation').val(), '0', 'Expected no license violation');
	assert.equal($('#licenseExceeded').css('display'), 'none', 'Expected no error message');
	assert.equal($('#licenseContent').text(), unlimitedText);
});

var initArraySelector = function(data) {
	SW.SRM.ArraySelector = function () {
		return {
			getGrid: function () {
				return {
					getSelectionModel: function () {
						return {
							selections: {
								items: data
							}
						}
					}
				}
			}
		}
	}();
};

test("LicenseBar positive onLicenseUpdate - all selected items fits disk count limit", function (assert) {
	initArraySelector([{ data: { Disks: '4' } }, { data: { Disks: '8' } }, { data: { Disks: '6' } }]);

	var licenseDetailsText = "Licenses required: {0} ({1} in use / {2} remaining / {3} total)",
        unlimitedText = "My unlimited text",
        isEval = 0,
        isExpired = 0;

	$('#hiddenTotal').val('30');
	$('#hiddenInUse').val('10');

	SW.SRM.LicenseBar.init(licenseDetailsText, unlimitedText, isEval, isExpired);
	SW.SRM.ArraySelector.onLicenseUpdate();

	assert.equal($('#hiddenLicViolation').val(), '0', 'Expected no license violation');
	assert.equal($('#licenseExceeded').css('display'), 'none', 'Expected no error message');
	assert.equal($('#licenseContent').text(), 'Licenses required: 18 (10 in use / 20 remaining / 30 total)');
});

test("LicenseBar positive onLicenseUpdate - all selected items have more disks that total, but still are fine with license", function (assert) {
	initArraySelector([{ data: { Disks: '4' } }, { data: { Disks: '8' } }, { data: { Disks: '6' } }]);

	var licenseDetailsText = "Licenses required: {0} ({1} in use / {2} remaining / {3} total)",
        unlimitedText = "My unlimited text",
        isEval = 0,
        isExpired = 0;

	$('#hiddenTotal').val('25');
	$('#hiddenInUse').val('10');

	SW.SRM.LicenseBar.init(licenseDetailsText, unlimitedText, isEval, isExpired);
	SW.SRM.ArraySelector.onLicenseUpdate();

	assert.equal($('#hiddenLicViolation').val(), '0', 'Expected no license violation');
	assert.equal($('#licenseExceeded').css('display'), 'none', 'Expected no error message');
	assert.equal($('#licenseContent').text(), 'Licenses required: 18 (10 in use / 15 remaining / 25 total)');
});

test("LicenseBar negative onLicenseUpdate - one of selected item breaks allowed license count", function (assert) {
	initArraySelector([{ data: { Disks: '15' } }, { data: { Disks: '8' } }]);

	var licenseDetailsText = "Licenses required: {0} ({1} in use / {2} remaining / {3} total)",
        unlimitedText = "My unlimited text",
        isEval = 0,
        isExpired = 0;

	$('#hiddenTotal').val('20');
	$('#hiddenInUse').val('10');

	SW.SRM.LicenseBar.init(licenseDetailsText, unlimitedText, isEval, isExpired);
	SW.SRM.ArraySelector.onLicenseUpdate();

	assert.equal($('#hiddenLicViolation').val(), '1', 'Expected  license violation');
	assert.equal($('#licenseExceeded').css('display'), 'inline', 'Expected error message');
	assert.equal($('#licenseContent').text(), 'Licenses required: 23 (10 in use / 10 remaining / 20 total)');
});

test("LicenseBar negative onLicenseUpdate - one of selected item breaks allowed license count, reversed order", function (assert) {
	initArraySelector([{ data: { Disks: '8' } }, { data: { Disks: '15' } }]);

	var licenseDetailsText = "Licenses required: {0} ({1} in use / {2} remaining / {3} total)",
        unlimitedText = "My unlimited text",
        isEval = 0,
        isExpired = 0;

	$('#hiddenTotal').val('20');
	$('#hiddenInUse').val('10');

	SW.SRM.LicenseBar.init(licenseDetailsText, unlimitedText, isEval, isExpired);
	SW.SRM.ArraySelector.onLicenseUpdate();

	assert.equal($('#hiddenLicViolation').val(), '1', 'Expected license violation');
	assert.equal($('#licenseExceeded').css('display'), 'inline', 'Expected error message');
	assert.equal($('#licenseContent').text(), 'Licenses required: 23 (10 in use / 10 remaining / 20 total)');
});

test("LicenseBar negative onLicenseUpdate - expired", function (assert) {
	initArraySelector([{ data: { Disks: '8' } }, { data: { Disks: '15' } }]);

	var licenseDetailsText = "Licenses required: {0} ({1} in use / {2} remaining / {3} total)",
        unlimitedText = "My unlimited text",
        isEval = 0,
        isExpired = 1;

	$('#hiddenTotal').val('20');
	$('#hiddenInUse').val('10');

	SW.SRM.LicenseBar.init(licenseDetailsText, unlimitedText, isEval, isExpired);
	SW.SRM.ArraySelector.onLicenseUpdate();

	assert.equal($('#hiddenLicViolation').val(), '1', 'Expected license violation');
	assert.equal($('#licenseExceeded').css('display'), 'inline', 'Expected error message');
	assert.equal($('#licenseContent').css('display'), 'none', 'Expected no content of license details');
});

test("LicenseBar positive onLicenseUpdate - unlimited", function (assert) {
	initArraySelector([{ data: { Disks: '8' } }, { data: { Disks: '15' } }]);

	var licenseDetailsText = "Licenses required: {0} ({1} in use / {2} remaining / {3} total)",
        unlimitedText = "My unlimited text",
        isEval = 1,
        isExpired = 0;

	$('#hiddenTotal').val('0x7fffffff');
	$('#hiddenInUse').val('10');

	SW.SRM.LicenseBar.init(licenseDetailsText, unlimitedText, isEval, isExpired);
	SW.SRM.ArraySelector.onLicenseUpdate();

	assert.equal($('#hiddenLicViolation').val(), '0', 'Expected no license violation');
	assert.equal($('#licenseExceeded').css('display'), 'none', 'Expected no error message');
	assert.equal($('#licenseContent').text(), unlimitedText);
});
