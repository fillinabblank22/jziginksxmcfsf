﻿/// <reference path="OrionRequirements.js" />
/// <reference path="../ArrayStep.js" />

var originalCallWebService,
    originalPrefs,
    originalWebServiceStoreLoadPrototype,
    originalWebServiceStoreEachPrototype,
    originalSelectionModelSelectRecordsPrototype,
    originalAlert;

var globalServiceUri,
    globalEndpoint,
    globalParams,
    prefsLoadCalled,
    prefsSaveValue,
    webServiceStoreLoadCalled,
    webServiceStoreEachCalled,
    discoveryJobResult;

module("ArrayStep.js related tests", {
    setup: function () {
        globalServiceUri = undefined;
        globalEndpoint = undefined;
        globalParams = undefined;
        prefsLoadCalled = false;
        prefsSaveValue = 0;
        webServiceStoreLoadCalled = false;
        webServiceStoreEachCalled = false;
        discoveryJobResult = '';

        //Override the ORION.callWebService function to save data to global fields instead of going
        //to the server. Then we can verify the correctness of the params, an easy form of ajax mocking.
        //ORION.callWebService is the underlying function of the SUT.
        originalCallWebService = ORION.callWebService;
        ORION.callWebService = function (serviceUri, endpoint, params, successCallback, errorCallback) {
            globalServiceUri = serviceUri;
            globalEndpoint = endpoint;
            globalParams = params;

            if (discoveryJobResult != '') {
                successCallback(discoveryJobResult);
            }
        };

        originalPrefs = ORION.Prefs;
        ORION.Prefs = function () {
            return {
                load: function (a, b) {
                    prefsLoadCalled = true;
                    return b;
                },
                save: function (a, b) { prefsSaveValue = b; }
            };
        }();
        
        originalWebServiceStoreLoadPrototype = ORION.WebServiceStore.prototype.load;
        ORION.WebServiceStore.prototype.load = function (data) {
            webServiceStoreLoadCalled = true;
            if (data && data.callback) {
                data.callback();
            }
        };

        originalWebServiceStoreEachPrototype = ORION.WebServiceStore.prototype.each;
        ORION.WebServiceStore.prototype.each = function (callback) {
            webServiceStoreEachCalled = true;
            callback({ data: { UniqueArrayId: 'unique-1' } }, 0);
            callback({ data: { UniqueArrayId: 'unique-2' } }, 1);
            callback({ data: { UniqueArrayId: 'unique-3' } }, 2);
        };

        originalSelectionModelSelectRecordsPrototype = Ext.grid.CheckboxSelectionModel.prototype.selectRecords;
        Ext.grid.CheckboxSelectionModel.prototype.selectRecords = function(records, selected) { };

        originalAlert = Ext.Msg.alert;

        SW.SRM.ArraySelector.onLicenseUpdate = null;

        $('body').append('<div id="Grid" />');
        $('body').append('<input type="hidden" id="SelectedArrayIDs" value="" />');
    },
    teardown: function () {
        $('#Grid').remove();
        $('#SelectedArrayIDs').remove();

        //Just in case.
        ORION.callWebService = originalCallWebService;
        ORION.Prefs = originalPrefs;
        ORION.WebServiceStore.prototype.load = originalWebServiceStoreLoadPrototype;
        ORION.WebServiceStore.prototype.each = originalWebServiceStoreEachPrototype;
        Ext.grid.CheckboxSelectionModel.prototype.selectRecords = originalSelectionModelSelectRecordsPrototype;
        Ext.Msg.alert = originalAlert;
        SW.SRM.ArraySelector.onLicenseUpdate = null;
    }
});

test("SW.SRM.ArraySelector test GetDiscoveryJobResultMethod", function (assert) {
    SW.SRM.ArraySelector.init('Grid', 500, 1);
    assert.equal(globalServiceUri, "/Orion/SRM/Services/EnrollmentWizardService.asmx");
    assert.equal(globalEndpoint, "GetDiscoveryJobResult");
});

test("SW.SRM.ArraySelector test GetDiscoveryJobResult in error mode where alert is expected", function (assert) {
    var errorMessage = "my error message";
    discoveryJobResult = { isDone: true, errorMessage: errorMessage, selectedArray: "", discoveryResult: [] };

    var actualMessage = '';
    Ext.Msg.alert = function (t, m) { actualMessage = m; };
    SW.SRM.ArraySelector.init('Grid', 500, 1);

    assert.equal(actualMessage, errorMessage, "Expected error message");
});

test("SW.SRM.ArraySelector test GetDiscoveryJobResult in success mode where result should be in SelectedArrayIDs where error message is null", function (assert) {
    discoveryJobResult = { isDone: true, errorMessage: null, selectedArray: "{unique-1}{unique-3}", discoveryResult: null };

    assert.equal($('#SelectedArrayIDs').val(), "", "SelectedArrayIDs should be empty since it is just initialized");
    SW.SRM.ArraySelector.init('Grid', 500, 1);
    assert.equal($('#SelectedArrayIDs').val(), "{unique-1}{unique-3}", "SelectedArrayIDs should contain data from response");
});

test("SW.SRM.ArraySelector test GetDiscoveryJobResult in success mode where result should be in SelectedArrayIDs where error message is ''", function (assert) {
    discoveryJobResult = { isDone: true, errorMessage: "", selectedArray: "{unique-1}{unique-3}", discoveryResult: null };

    assert.equal($('#SelectedArrayIDs').val(), "", "SelectedArrayIDs should be empty since it is just initialized");
    SW.SRM.ArraySelector.init('Grid', 500, 1);
    assert.equal($('#SelectedArrayIDs').val(), "{unique-1}{unique-3}", "SelectedArrayIDs should contain data from response");
});

test("SW.SRM.ArraySelector test Get Page size during Init", function (assert) {
    SW.SRM.ArraySelector.init('Grid', 500, 1);
    assert.ok(prefsLoadCalled, "ORION.Prefs.load was Called");
});

test("SW.SRM.ArraySelector test Store server details setup and Load method calling", function (assert) {
    assert.equal(webServiceStoreLoadCalled, false, "ORION.WebServiceStore.load is initialized to false");
    SW.SRM.ArraySelector.init('Grid', 500, 1);
    assert.ok(webServiceStoreLoadCalled, "ORION.WebServiceStore.load was Called");
    assert.equal(SW.SRM.ArraySelector.getGrid().getStore().proxy.url, "/Orion/SRM/Services/EnrollmentWizardService.asmx/GetArrays");
    assert.equal(SW.SRM.ArraySelector.getGrid().getStore().sortInfo.field, "Name");
});

test("SW.SRM.ArraySelector fillGridColumns() test", function (assert) {
    SW.SRM.ArraySelector.init('Grid', 500, 0);
    var columns = SW.SRM.ArraySelector.getGrid().getColumns();
    assert.equal(columns[columns.length - 1].header, '@{R=SRM.Strings;K=SRMArrayStep_Monitored;E=xml}');

    SW.SRM.ArraySelector.init('Grid', 500, 1);
    columns = SW.SRM.ArraySelector.getGrid().getColumns();
    assert.equal(columns[columns.length - 1].header, '@{R=SRM.Strings;K=SRMArrayStep_Provider;E=xml}');
});

test("SW.SRM.ArraySelector isExternalProvider test", function (assert) {
    SW.SRM.ArraySelector.init('Grid', 500, 0);
    var columns = SW.SRM.ArraySelector.getGrid().getColumns();
    assert.equal(columns[1].sortable, false, "Sortable column is expected to be false since no input box exists");

    SW.SRM.ArraySelector.init('Grid', 500, 1);
    columns = SW.SRM.ArraySelector.getGrid().getColumns();
    assert.equal(columns[1].sortable, true, "Sortable column is expected to be true");

    SW.SRM.ArraySelector.init('Grid', 500);
    columns = SW.SRM.ArraySelector.getGrid().getColumns();
    assert.equal(columns[1].sortable, false, "Sortable column is expected to be false");
});

test("SW.SRM.ArraySelector reload() calling", function (assert) {
    SW.SRM.ArraySelector.init('Grid', 500, 1);
    webServiceStoreLoadCalled = false;

    SW.SRM.ArraySelector.reload();
    assert.ok(webServiceStoreLoadCalled, "ORION.WebServiceStore.load was Called");
});

test("SW.SRM.ArraySelector onAfterPageChange() empty SelectedArrayIDs and selectAll is false", function (assert) {
    SW.SRM.ArraySelector.init('Grid', 500, 1);

    var wasCalled = false;
    webServiceStoreEachCalled = false;
    Ext.grid.CheckboxSelectionModel.prototype.selectRecords = function (records, selected) { wasCalled = true; };
    SW.SRM.ArraySelector.reload();

    assert.ok(webServiceStoreEachCalled, "onAfterPageChange() should call grid.store.each loop that is mocked");
    assert.equal(wasCalled, false, "onAfterPageChange() should not call grid.getSelectionModel().selectRecords() in case nothing is selected in SelectedArrayIDs inputbox");
});

test("SW.SRM.ArraySelector onAfterPageChange() SelectedArrayIDs contains '{unique-2}' and selectAll is false", function (assert) {
    $('#SelectedArrayIDs').val('{unique-2}');
    SW.SRM.ArraySelector.init('Grid', 500, 1);

    var callCounter = 0;
    var wasCalled = false;
    webServiceStoreEachCalled = false;
    Ext.grid.CheckboxSelectionModel.prototype.selectRecords = function (records, selected) {
        callCounter++;
        wasCalled = records.length == 1 && records[0].data.UniqueArrayId === 'unique-2';
    };
    SW.SRM.ArraySelector.reload();

    assert.ok(webServiceStoreEachCalled, "onAfterPageChange() should call grid.store.each loop that is mocked");
    assert.equal(wasCalled, true, "onAfterPageChange() should call grid.getSelectionModel().selectRecords() with 'unique-2' record id since it is preselected in SelectedArrayIDs inputbox");
    assert.equal(callCounter, 1, "onAfterPageChange() should call grid.getSelectionModel().selectRecords() only once since only one ID is specified in SelectedArrayIDs inputbox");
});

test("SW.SRM.ArraySelector onAfterPageChange() SelectedArrayIDs contains '{unique-2},{unique-3}' and selectAll is false", function (assert) {
    $('#SelectedArrayIDs').val('{unique-2},{unique-3}');
    SW.SRM.ArraySelector.init('Grid', 500, 1);

    var callCounter = 0;
    var wasCalledUnique2 = false;
    var wasCalledUnique3 = false;
    webServiceStoreEachCalled = false;
    Ext.grid.CheckboxSelectionModel.prototype.selectRecords = function (records, selected) {
        callCounter++;
        if (wasCalledUnique2 === false) {
            wasCalledUnique2 = records.length == 1 && records[0].data.UniqueArrayId === 'unique-2';
        }
        if (wasCalledUnique3 === false) {
            wasCalledUnique3 = records.length == 1 && records[0].data.UniqueArrayId === 'unique-3';
        }
    };
    SW.SRM.ArraySelector.reload();

    assert.ok(webServiceStoreEachCalled, "onAfterPageChange() should call grid.store.each loop that is mocked");
    assert.equal(wasCalledUnique2, true, "onAfterPageChange() should call grid.getSelectionModel().selectRecords() with 'unique-2' record id since it is preselected in SelectedArrayIDs inputbox");
    assert.equal(wasCalledUnique3, true, "onAfterPageChange() should call grid.getSelectionModel().selectRecords() with 'unique-3' record id since it is preselected in SelectedArrayIDs inputbox");
    assert.equal(callCounter, 2, "onAfterPageChange() should call grid.getSelectionModel().selectRecords() twice since only one ID is specified in SelectedArrayIDs inputbox");
});

test("SW.SRM.ArraySelector onAfterPageChange() SelectedArrayIDs contains '{unique-2}' and isExternalProvider is false", function (assert) {
    $('#SelectedArrayIDs').val('{unique-2}');
    SW.SRM.ArraySelector.init('Grid', 500, 0);

    var callCounter = 0;
    var wasCalledUnique1 = false;
    var wasCalledUnique2 = false;
    var wasCalledUnique3 = false;
    webServiceStoreEachCalled = false;
    Ext.grid.CheckboxSelectionModel.prototype.selectRecords = function (records, selected) {
        callCounter++;
        if (wasCalledUnique1 === false) {
            wasCalledUnique1 = records.length == 1 && records[0].data.UniqueArrayId === 'unique-1';
        }
        if (wasCalledUnique2 === false) {
            wasCalledUnique2 = records.length == 1 && records[0].data.UniqueArrayId === 'unique-2';
        }
        if (wasCalledUnique3 === false) {
            wasCalledUnique3 = records.length == 1 && records[0].data.UniqueArrayId === 'unique-3';
        }
    };
    SW.SRM.ArraySelector.reload();

    assert.ok(webServiceStoreEachCalled, "onAfterPageChange() should call grid.store.each loop that is mocked");
    assert.equal(wasCalledUnique1, true, "onAfterPageChange() should call grid.getSelectionModel().selectRecords() with 'unique-1' record id since selectAll is true");
    assert.equal(wasCalledUnique2, true, "onAfterPageChange() should call grid.getSelectionModel().selectRecords() with 'unique-2' record id since selectAll is true");
    assert.equal(wasCalledUnique3, true, "onAfterPageChange() should call grid.getSelectionModel().selectRecords() with 'unique-3' record id since selectAll is true");
    assert.equal(callCounter, 3, "onAfterPageChange() should call grid.getSelectionModel().selectRecords() three times since selectAll is true so all instances should be selected");
});

test("SW.SRM.ArraySelector onLicenseUpdate() is null and not called", function (assert) {
    SW.SRM.ArraySelector.init('Grid', 500, 1);

    var wasCalled = false;
    SW.SRM.ArraySelector.onLicenseUpdate = null;
    SW.SRM.ArraySelector.reload();

    assert.equal(wasCalled, false, "onAfterPageChange() should not call SW.SRM.ArraySelector.onLicenseUpdate() since it is not defined");
});

test("SW.SRM.ArraySelector onLicenseUpdate() is called", function (assert) {
    SW.SRM.ArraySelector.init('Grid', 500, 1);

    var wasCalled = false;
    SW.SRM.ArraySelector.onLicenseUpdate = function () { wasCalled = true; };
    SW.SRM.ArraySelector.reload();

    assert.equal(wasCalled, true, "onAfterPageChange() should call SW.SRM.ArraySelector.onLicenseUpdate() since it is defined");
});

test("SW.SRM.ArraySelector onRowSelected() selection already contains selected id", function (assert) {
    $('#SelectedArrayIDs').val('{unique-2}');
    SW.SRM.ArraySelector.init('Grid', 500, 1);

    var wasCalled = false;
    SW.SRM.ArraySelector.onLicenseUpdate = function () { wasCalled = true; };

    var model = SW.SRM.ArraySelector.getGrid().getSelectionModel();
    model.events.rowselect.fire(null, null, { data: { UniqueArrayId: 'unique-2' } });

    assert.equal($('#SelectedArrayIDs').val(), '{unique-2}', "onRowSelected() should ensure the id is still filled in SelectedArrayIDs input");
    assert.equal(wasCalled, false, "onRowSelected() should not call SW.SRM.ArraySelector.onLicenseUpdate() because selected item was already in SelectedArrayIDs input");
});

test("SW.SRM.ArraySelector onRowSelected() no selected item in SelectedArrayIDs input", function (assert) {
    $('#SelectedArrayIDs').val('{unique-3}');
    SW.SRM.ArraySelector.init('Grid', 500, 1);

    var wasCalled = false;
    SW.SRM.ArraySelector.onLicenseUpdate = function () { wasCalled = true; };

    var model = SW.SRM.ArraySelector.getGrid().getSelectionModel();
    model.events.rowselect.fire(null, null, { data: { UniqueArrayId: 'unique-2' } });

    assert.equal($('#SelectedArrayIDs').val(), '{unique-3}{unique-2}', "onRowSelected() should fill SelectedArrayIDs input by 'unique-2' id since it was 'row selected'");
    assert.equal(wasCalled, true, "onRowSelected() should call SW.SRM.ArraySelector.onLicenseUpdate() because selection was updated");
});

test("SW.SRM.ArraySelector onRowDeselected() no such selected item in SelectedArrayIDs input", function (assert) {
    $('#SelectedArrayIDs').val('{unique-3}');
    SW.SRM.ArraySelector.init('Grid', 500, 1);

    var wasCalled = false;
    SW.SRM.ArraySelector.onLicenseUpdate = function () { wasCalled = true; };

    var model = SW.SRM.ArraySelector.getGrid().getSelectionModel();
    model.events.rowdeselect.fire(null, null, { data: { UniqueArrayId: 'unique-3' } });

    assert.equal($('#SelectedArrayIDs').val(), '', "onRowSelected() SelectedArrayIDs input should be empty since item has been un-selected, so it should get out from the input");
    assert.equal(wasCalled, true, "onRowSelected() should call SW.SRM.ArraySelector.onLicenseUpdate() because selection was updated");
});

test("SW.SRM.ArraySelector is checkbox hidded", function (assert) {
    SW.SRM.ArraySelector.init('Grid', 500, 0);
    assert.equal(SW.SRM.ArraySelector.getGrid().getColumnModel().isHidden(0), true, "isExternalProvider is setup to the checkbox in grid should not be visible");

    SW.SRM.ArraySelector.init('Grid', 500, 1);
    assert.equal(SW.SRM.ArraySelector.getGrid().getColumnModel().isHidden(0), false, "isExternalProvider is setup to the checkbox in grid should be visible");
});

test("SW.SRM.ArraySelector bottom toolbar is hidden", function (assert) {
    SW.SRM.ArraySelector.init('Grid', 500, 0);
    assert.equal(SW.SRM.ArraySelector.getGrid().bbar == null, true, "isExternalProvider is setup to bottom toolbar in grid should not be visible");

    SW.SRM.ArraySelector.init('Grid', 500, 1);
    assert.equal(SW.SRM.ArraySelector.getGrid().bbar != null, true, "isExternalProvider is setup to bottom toolbar in grid should be visible");
});

test("SW.SRM.ArraySelector page sizer is hidden", function (assert) {
    SW.SRM.ArraySelector.init('Grid', 500, 0);
    var pager = $('#pagerCombo')[0];
    assert.equal(pager == null, true, "isExternalProvider is setup to pager in grid should not be visible");

    SW.SRM.ArraySelector.init('Grid', 500, 1);
    pager = $('#pagerCombo')[0];
    assert.equal(pager != null, true, "isExternalProvider is setup to pager in grid should be visible");
});

test("SW.SRM.ArraySelector bottom toolbar is hidden", function (assert) {
    SW.SRM.ArraySelector.init('Grid', 500, 1);

    webServiceStoreLoadCalled = false;

    assert.equal(prefsSaveValue, 0, 'Initial value should be 0');
    SW.SRM.ArraySelector.getGrid().getBottomToolbar().findById('pagerCombo').events.select.fire(null, function() { return { get: function(a) { return 20; } } }());
    assert.equal(prefsSaveValue, 20, 'Initial value should be 20');

    assert.ok(webServiceStoreLoadCalled, "ORION.WebServiceStore.load was Called");
});
