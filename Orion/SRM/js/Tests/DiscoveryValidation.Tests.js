﻿/// <reference path="OrionRequirements.js" />
/// <reference path="../SRM.ProviderChangeManager.js" />
/// <reference path="../../Config/Controls/DiscoveryValidation.js" />

var originalCallWebService,
    originalPrefs,
    originalWebServiceStoreLoadPrototype,
    originalWebServiceStoreEachPrototype,
    originalSelectionModelSelectRecordsPrototype,
    originalAlert;

var globalServiceUri,
    globalEndpoint,
    globalParams,
    prefsLoadCalled,
    prefsSaveValue,
    webServiceStoreLoadCalled,
    webServiceStoreEachCalled,
    discoveryJobResult,
    defferedObject,
    resultingMessage,
    requestFailed,
    errorPrefix,
    successConst;

SW = SW || {};
SW.SRM = SW.SRM || {};
SW.SRM.Common = SW.SRM.Common || {};
SW.SRM.TestConnectionUI = SW.SRM.TestConnectionUI || {};

module("DiscoveryValidation.js related tests", {
    setup: function () {
        globalServiceUri = undefined;
        globalEndpoint = undefined;
        globalParams = undefined;
        prefsLoadCalled = false;
        prefsSaveValue = 0;
        webServiceStoreLoadCalled = false;
        webServiceStoreEachCalled = false;
        discoveryJobResult = '';
        defferedObject = $.Deferred();
        resultingMessage = '';
        errorPrefix = 'error_';
        successConst = 'success';
        requestFailed = 'requestFailed';

        //Override the ORION.callWebService function to save data to global fields instead of going
        //to the server. Then we can verify the correctness of the params, an easy form of ajax mocking.
        //ORION.callWebService is the underlying function of the SUT.
        originalCallWebService = SW.SRM.Common.CallSrmWebService;
        SW.SRM.Common.CallSrmWebService = function (serviceUri, endpoint, params, successCallback, errorCallback) {
            globalServiceUri = serviceUri;
            globalEndpoint = endpoint;
            globalParams = params;

            if (discoveryJobResult == '') {
                resultingMessage = requestFailed;
            }
            else {
                successCallback(discoveryJobResult);
            }
        };

        SW.SRM.TestConnectionUI.ShowDiscovery = function() {};
        SW.SRM.TestConnectionUI.HideDiscovery = function() {};

        var handleError = function (message) {
            resultingMessage = message;
        }

        SW.SRM.DiscoveryValidation.init("Array01","123", "ABC", '', defferedObject, "UnexpectedErrorMessage", handleError);

        $.when(defferedObject).then(
                function (x) {
                    resultingMessage = successConst;
                },
                function () {
                    resultingMessage = errorPrefix + resultingMessage;
                });
    },
    teardown: function () {
    }
});

test("SW.SRM.DiscoveryValidation test startDiscovery", function (assert) {
    SW.SRM.DiscoveryValidation.startDiscovery();
    assert.equal(globalServiceUri, "/Orion/SRM/Services/EnrollmentWizardService.asmx");
    assert.equal(globalEndpoint, "StartDiscoveryJob");
});

test("SW.SRM.DiscoveryValidation test startDiscovery in error mode where reject and error message is expected", function (assert) {    
    var errorMessage = "my error message";
    discoveryJobResult = { IsDone: true, ErrorMessage: errorMessage, IsSuccess: false };
    SW.SRM.DiscoveryValidation.startDiscovery();

    assert.equal(resultingMessage, errorPrefix + errorMessage, "Expected error message");
});

test("SW.SRM.DiscoveryValidation test startDiscovery in error mode where reject and unexpected error is expected", function (assert) {
    discoveryJobResult = '';
    SW.SRM.DiscoveryValidation.startDiscovery();

    assert.equal(resultingMessage, requestFailed, "Expected error message");
});

test("SW.SRM.DiscoveryValidation test startDiscovery in success mode where resolve is expected", function (assert) {
    discoveryJobResult = { IsDone: true, ErrorMessage: '', IsSuccess: true };
    SW.SRM.DiscoveryValidation.startDiscovery();

    assert.equal(resultingMessage, successConst, "Expected error message");
});




test("SW.SRM.DiscoveryValidation test validateDiscovery", function (assert) {
    SW.SRM.DiscoveryValidation.validateDiscovery();
    assert.equal(globalServiceUri, "/Orion/SRM/Services/EnrollmentWizardService.asmx");
    assert.equal(globalEndpoint, "CheckArrayIsInDiscoveryJobResults");
});

test("SW.SRM.DiscoveryValidation test validateDiscovery in error mode where reject and error message is expected", function (assert) {
    var errorMessage = "my error message";
    discoveryJobResult = { IsDone: true, ErrorMessage: errorMessage, IsSuccess: false };
    SW.SRM.DiscoveryValidation.validateDiscovery();

    assert.equal(resultingMessage, errorPrefix + errorMessage, "Expected error message");
});

test("SW.SRM.DiscoveryValidation test validateDiscovery in error mode where reject and unexpected error is expected", function (assert) {
    discoveryJobResult = '';
    SW.SRM.DiscoveryValidation.validateDiscovery();

    assert.equal(resultingMessage, requestFailed, "Expected error message");
});

test("SW.SRM.DiscoveryValidation test validateDiscovery in success mode where resolve is expected", function (assert) {
    discoveryJobResult = { IsDone: true, ErrorMessage: '', IsSuccess: true };
    SW.SRM.DiscoveryValidation.validateDiscovery();

    assert.equal(resultingMessage, successConst, "Expected error message");
});



test("SW.SRM.DiscoveryValidation test rejectDeferred rejects deffered when runDiscovery should be run", function (assert) {
    SW.SRM.DiscoveryValidation.rejectDeferred(true);

    assert.equal(resultingMessage, errorPrefix, "Expected error message");
});

test("SW.SRM.DiscoveryValidation test rejectDeferred does nothing when runDiscovery shouldn't be run", function (assert) {
    SW.SRM.DiscoveryValidation.rejectDeferred(false);

    assert.equal(resultingMessage, '', "Expected error message");
});