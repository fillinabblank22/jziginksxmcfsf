﻿/// <reference path="OrionRequirements.js" />
/// <reference path="../SRM.Formatters.js" />

module("SRM.Formatters.js related tests", {
    setup: function () { },
    teardown: function () { }
});

test("SW.SRM.Formatters.FormatCapacity value parameter", function (assert) {
    assert.equal(SW.SRM.Formatters.FormatCapacity(1), '0.00 @{R=SRM.Strings;K=Value_Unit_KB;E=js}');
    assert.equal(SW.SRM.Formatters.FormatCapacity(100), '0.10 @{R=SRM.Strings;K=Value_Unit_KB;E=js}');
    assert.equal(SW.SRM.Formatters.FormatCapacity(1000), '0.98 @{R=SRM.Strings;K=Value_Unit_KB;E=js}');

    assert.equal(SW.SRM.Formatters.FormatCapacity(1024), '1.00 @{R=SRM.Strings;K=Value_Unit_KB;E=js}');
    assert.equal(SW.SRM.Formatters.FormatCapacity(1024 * 1024), '1.00 @{R=SRM.Strings;K=Value_Unit_MB;E=js}');
    assert.equal(SW.SRM.Formatters.FormatCapacity(1024 * 1024 * 1024), '1.00 @{R=SRM.Strings;K=Value_Unit_GB;E=js}');
    assert.equal(SW.SRM.Formatters.FormatCapacity(1024 * 1024 * 1024 * 1024), '1.00 @{R=SRM.Strings;K=Value_Unit_TB;E=js}');
    assert.equal(SW.SRM.Formatters.FormatCapacity(1024 * 1024 * 1024 * 1024 * 1024), '1.00 @{R=SRM.Strings;K=Value_Unit_PB;E=js}');
    assert.equal(SW.SRM.Formatters.FormatCapacity(1024 * 1024 * 1024 * 1024 * 1024 * 1024 * 2), '2048.00 @{R=SRM.Strings;K=Value_Unit_PB;E=js}');

    assert.equal(SW.SRM.Formatters.FormatCapacity(-1024), '-1.00 @{R=SRM.Strings;K=Value_Unit_KB;E=js}');
});

test("SW.SRM.Formatters.FormatCapacity value parameter in string representation", function (assert) {
    assert.equal(SW.SRM.Formatters.FormatCapacity("100"), '0.10 @{R=SRM.Strings;K=Value_Unit_KB;E=js}');
    assert.equal(SW.SRM.Formatters.FormatCapacity("-100"), '-0.10 @{R=SRM.Strings;K=Value_Unit_KB;E=js}');
});

test("SW.SRM.Formatters.FormatCapacity fixed parameter", function (assert) {
    assert.equal(SW.SRM.Formatters.FormatCapacity(100), '0.10 @{R=SRM.Strings;K=Value_Unit_KB;E=js}');
    assert.equal(SW.SRM.Formatters.FormatCapacity(100, 1), '0.1 @{R=SRM.Strings;K=Value_Unit_KB;E=js}');
    assert.equal(SW.SRM.Formatters.FormatCapacity(100, 0), '0 @{R=SRM.Strings;K=Value_Unit_KB;E=js}');
});

test("SW.SRM.Formatters.FormatCapacity valueFormatter parameter", function (assert) {
    assert.equal(SW.SRM.Formatters.FormatCapacity(100, 1, function (v) { return v + 's per'; }), '0.1s per @{R=SRM.Strings;K=Value_Unit_KB;E=js}');
});

test("SW.SRM.Formatters.FormatCapacity unknown expectation", function (assert) {
    assert.equal(SW.SRM.Formatters.FormatCapacity(''), '@{R=SRM.Strings;K=Value_Unknown;E=js}', "test for value is ''");
    assert.equal(SW.SRM.Formatters.FormatCapacity(null), '@{R=SRM.Strings;K=Value_Unknown;E=js}', 'test for value is null');
    assert.equal(SW.SRM.Formatters.FormatCapacity(-1), '@{R=SRM.Strings;K=Value_Unknown;E=js}', 'test for value is -1');
    assert.equal(SW.SRM.Formatters.FormatCapacity('>3'), '@{R=SRM.Strings;K=Value_Unknown;E=js}', 'test for value is ">3"');
    assert.equal(SW.SRM.Formatters.FormatCapacity('aaa'), '@{R=SRM.Strings;K=Value_Unknown;E=js}', 'test for value is "aaa"');
});

test("SW.SRM.Formatters.ParseIntOrUnknown unknown expectation", function (assert) {
    assert.equal(SW.SRM.Formatters.ParseIntOrUnknown(''), '@{R=SRM.Strings;K=Value_Unknown;E=js}', "test for value is ''");
    assert.equal(SW.SRM.Formatters.ParseIntOrUnknown('', 'ms'), '@{R=SRM.Strings;K=Value_Unknown;E=js}', "test for value is '' with unit 'ms'");

    assert.equal(SW.SRM.Formatters.ParseIntOrUnknown(null), '@{R=SRM.Strings;K=Value_Unknown;E=js}', "test for value is null");
    assert.equal(SW.SRM.Formatters.ParseIntOrUnknown(null, 'ms'), '@{R=SRM.Strings;K=Value_Unknown;E=js}', "test for value is null with unit 'ms'");

    assert.equal(SW.SRM.Formatters.ParseIntOrUnknown(), '@{R=SRM.Strings;K=Value_Unknown;E=js}', "test for value is unknown");

    assert.equal(SW.SRM.Formatters.ParseIntOrUnknown('a'), '@{R=SRM.Strings;K=Value_Unknown;E=js}', "test for value is 'a'");
});

test("SW.SRM.Formatters.ParseIntOrUnknown int is expected", function (assert) {
    assert.equal(SW.SRM.Formatters.ParseIntOrUnknown('5'), '5', "test for value is '5'");
    assert.equal(SW.SRM.Formatters.ParseIntOrUnknown('5', 'ms'), '5 ms', "test for value is '5' with unit 'ms'");

    assert.equal(SW.SRM.Formatters.ParseIntOrUnknown(5), '5', "test for value is 5");
    assert.equal(SW.SRM.Formatters.ParseIntOrUnknown(5, 'ms'), '5 ms', "test for value is 5 with unit 'ms'");

    assert.equal(SW.SRM.Formatters.ParseIntOrUnknown('5.4'), '5', "test for value is '5.4'");
    assert.equal(SW.SRM.Formatters.ParseIntOrUnknown('5.4', 'ms'), '5 ms', "test for value is '5.4' with unit 'ms'");

    assert.equal(SW.SRM.Formatters.ParseIntOrUnknown(5.8), '5', "test for value is 5.8");
    assert.equal(SW.SRM.Formatters.ParseIntOrUnknown(5.8, 'ms'), '5 ms', "test for value is 5.8 with unit 'ms'");
});
