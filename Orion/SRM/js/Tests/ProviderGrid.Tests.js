/// <reference path="OrionRequirements.js" />
/// <reference path="../../Admin/ProviderGrid.js" />

var 
    originalWebServiceStoreLoadPrototype,
    originalAlert;

var webServiceStoreLoadCalled;

module("ProviderGrid.js related tests", {
    setup: function () {
        webServiceStoreLoadCalled = false;
        
        originalWebServiceStoreLoadPrototype = ORION.WebServiceStore.prototype.load;
        ORION.WebServiceStore.prototype.load = function (data) {
            webServiceStoreLoadCalled = true;
            if (data && data.callback) {
                data.callback();
            }
        };

        originalAlert = Ext.Msg.alert;

        $('body').append('<div id="Grid" />');
    },
    teardown: function () {
        $('#Grid').remove();

        //Just in case.
        ORION.WebServiceStore.prototype.load = originalWebServiceStoreLoadPrototype;
        Ext.Msg.alert = originalAlert;
    }
});

test("SW.SRM.ProviderGrid test Store server details setup and Load method calling", function (assert) {
    assert.equal(webServiceStoreLoadCalled, false, "ORION.WebServiceStore.load is initialized to false");
    SW.SRM.ProviderGrid.init();
    assert.ok(webServiceStoreLoadCalled, "ORION.WebServiceStore.load was Called");
    assert.equal(SW.SRM.ProviderGrid.GetGrid().getStore().proxy.url, "/Orion/SRM/Services/StorageManagementService.asmx/GetAllProviders");
    assert.equal(SW.SRM.ProviderGrid.GetGrid().getStore().sortInfo.field, "ID");
});

test("SW.SRM.ProviderGrid RefreshGrid() calling", function (assert) {
    SW.SRM.ProviderGrid.init('Grid');
    webServiceStoreLoadCalled = false;

    SW.SRM.ProviderGrid.RefreshGrid();
    assert.ok(webServiceStoreLoadCalled, "ORION.WebServiceStore.load was Called");
});

test("SW.SRM.ProviderGrid button state change on row selection", function (assert) {
    SW.SRM.ProviderGrid.init('Grid');
    var grid = SW.SRM.ProviderGrid.GetGrid();

    grid.getSelectionModel().selections.items.push({ ID: 'id1', Name: 'name1', IPAddress: 'ipaddress1', Status: 'status1', StatusDescription: 'statusdescription1' });
    grid.getSelectionModel().selections.items.push({ ID: 'id2', Name: 'name2', IPAddress: 'ipaddress2', Status: 'status2', StatusDescription: 'statusdescription2' });

    var map = grid.getTopToolbar().items.map;
    assert.equal(map.DeleteButton.disabled, true);
    assert.equal(map.EditButton.disabled, true);

    var model = grid.getSelectionModel();
    model.events.selectionchange.fire(null, null, { data: { ID: 'id1' } });

    assert.equal(map.DeleteButton.disabled, false);
    assert.equal(map.EditButton.disabled, false);
});

test("SW.SRM.ProviderGrid button state change on row selection - clear selection - disable buttons", function (assert) {
    SW.SRM.ProviderGrid.init('Grid');
    var grid = SW.SRM.ProviderGrid.GetGrid();

    grid.getSelectionModel().selections.items.push({ ID: 'id1', Name: 'name1', IPAddress: 'ipaddress1', Status: 'status1', StatusDescription: 'statusdescription1' });
    grid.getSelectionModel().selections.items.push({ ID: 'id2', Name: 'name2', IPAddress: 'ipaddress2', Status: 'status2', StatusDescription: 'statusdescription2' });

    var map = grid.getTopToolbar().items.map;
    assert.equal(map.DeleteButton.disabled, true);
    assert.equal(map.EditButton.disabled, true);

    var model = grid.getSelectionModel();
    model.events.selectionchange.fire(null, null, { data: { ID: 'id1' } });

    assert.equal(map.DeleteButton.disabled, false);
    assert.equal(map.EditButton.disabled, false);

    model.clearSelections();
    model.events.selectionchange.fire(null, null, null);

    assert.equal(map.DeleteButton.disabled, true);
    assert.equal(map.EditButton.disabled, true);
});