/// <reference path="OrionRequirements.js" />
/// <reference path="../../../js/OrionMinReqs.js" />
/// <reference path="../SRM.EditProperties.Advanced.js" />

module("SRM.EdtiProperties.Advanced.js - Single selection tests", {
    setup: function () {
        $('body').append('<div id=\'fixtureDiv\' />');;
        var fixtureDiv = $('#fixtureDiv');
        fixtureDiv.append(SW.Core.String.Format('<img src=\'{0}\' class=\'collapsible\' data-collapse-target=\'{1}\' id=\'imageTrigger\'/>', '../../../../Orion/images/Button.Expand.gif', 'advancedSettings'));
        var tableTarget = SW.Core.String.Format('<table class=\'{0}\'> <tr><td>Inside</td></tr> </table>', 'advancedMultipleSettings');
        fixtureDiv.append(SW.Core.String.Format('<div id=\'{0}\'> {1} </div>', 'advancedSettings', tableTarget));

        SW.SRM.EditProperties.Advanced.init();
    },
    teardown: function () {
        $('#fixtureDiv').remove();
    }
});

test("Advanced properties are initialized on load ", function (assert) {
    assert.equal($('advancedSettings').is(':visible'), false);
    assert.equal($('#imageTrigger').attr('src').indexOf('Button.Expand') > -1, true);
});

test("Expand button section click ", function (assert) {
    assert.equal($('advancedSettings').is(':visible'), false);

    $('#imageTrigger').click();

    assert.equal($('#advancedSettings').is(':visible'), true);
    assert.equal($('table.advancedMultipleSettings *').first().attr('disabled'), undefined);
});


module("SRM.EdtiProperties.Advanced.js - MultipleSelection tests", {
    setup: function () {
        $('body').append('<div id=\'fixtureDiv\' />');;
        var fixtureDiv = $('#fixtureDiv');
        fixtureDiv.append(SW.Core.String.Format('<img src=\'{0}\' class=\'collapsible\' data-collapse-target=\'{1}\' id=\'imageTrigger\'/>', '../../../../Orion/images/Button.Expand.gif', 'advancedSettings'));
        fixtureDiv.append('<input type=\'checkbox\' id=\'multipleAdvChange\' >');
        var tableTarget = SW.Core.String.Format('<table class=\'{0}\'> <tr><td>Inside</td></tr> </table>', 'advancedMultipleSettings');
        fixtureDiv.append(SW.Core.String.Format('<div id=\'{0}\'> {1} </div>', 'advancedSettings', tableTarget));

        SW.SRM.EditProperties.Advanced.init();
    },
    teardown: function () {
        $('#fixtureDiv').remove();
    }
});

test("Advanced properties are initialized on load ", function (assert) {
    assert.equal($('advancedSettings').is(':visible'), false);
    assert.equal($('#imageTrigger').attr('src').indexOf('Button.Expand') > -1, true);
    assert.equal($('#multipleAdvChange').prop('checked'), false);
    assert.equal($('#multipleAdvChange').prop('disabled'), false);
});

test("Expand button section click & Checkbox tests", function (assert) {
    assert.equal($('advancedSettings').is(':visible'), false);

    $('#imageTrigger').click();

    assert.equal($('#advancedSettings').is(':visible'), true);

    assert.equal($('#multipleAdvChange').prop('checked'), false);
    assert.equal($('table.advancedMultipleSettings *').first().attr('disabled'), 'disabled');

    $('#multipleAdvChange').prop('checked', true);
    $('#multipleAdvChange').click();
    assert.equal($('table.advancedMultipleSettings *').first().attr('disabled'), undefined);
});