﻿/// <reference path="OrionRequirements.js" />
/// <reference path="../SRM.Common.js" />
/// <reference path="../SRM.Formatters.js" />

var originalCallWebService;


var globalServiceUri,
    globalEndpoint,
    globalParams,
    orionVolumeResult;

var params = {
    resourceID: 1, netObjectFieldName: "_NetObjectID",
    rows: [["tex-2k8-2luns-lun02", "/Orion/StatusIcon.ashx?entity=Orion.SRM.LUNs&status=1&size=small", "/Orion/View.aspx?NetObject=SML:4", "%SML:4%", "%SML:4%", 21474836480, "%SML:4%", "%SML:4%", "SML:4"]],
    columnsInfo: [
    { header: "LUN", formatter: null, name: "Name", iconColumn: 1, linkColumn: 2 },
    { header: null, formatter: null, name: "_IconFor_Name" },
    { header: null, formatter: null, name: "_LinkFor_Name" },
    { header: "Storage Pool", formatter: null, name: "PoolName" },
    { header: "Associated Endpoint", formatter: null, name: "AssociatedEndpoint" },
    { header: "Total Size", formatter: (function (value) { return SW.SRM.Formatters.FormatCapacity(value); }), name: "TotalSize" },
    { header: "File System Used Capacity", formatter: null, name: "FileSystemUsedCapacity" },
    { header: "File System Used %", formatter: null, name: "FileSystemUsedPercentage" },
    { header: null, formatter: null, name: "_NetObjectID" }],
    volumeSpacePercentWarning: 90,
    volumeSpacePercentCritical: 97,
    classesName: {
        associatedPools: "AssociatedPools",
        associatedVolumes: "AssociatedVolumes",
        associatedEndpoint: "AssociatedEndpoint",
        volumeSpaceUsed: "FileSystemUsedCapacity",
        volumeSpacePercent: "FileSystemUsedPercentage"
    }
}

var orionVolume = [
    { NetObject: "SML:1", Caption: "AssociatedEndpoint1", VolumeSize: 100, VolumeSpaceUsed: 30, Pools: [{ PoolID: 1, Name: 'Pool1', Status: 1 }], Volumes: [] },
    {
        NetObject: "SMV:1",
        Caption: "AssociatedEndpoint2",
        VolumeSize: null,
        VolumeSpaceUsed: null,
        Pools: [],
        Volumes: [{ Key: 2, Value: "Volume1" }]
    }
];

var testNetObjectLink = function (assert, actual, netObjectType, netObjectId, entity, status, name) {
    assert.ok(actual.indexOf('?NetObject=' + netObjectType + ':' + netObjectId) >= 0);
    assert.ok(actual.indexOf('entity=' + entity + '&status=' + status + '&size=small') >= 0);
    assert.ok(actual.indexOf('> ' + name + '</a>') >= 0);
}

module("SRM.Common.js related tests", {
    setup: function () {
        globalServiceUri = undefined;
        globalEndpoint = undefined;
        globalParams = undefined;
        orionVolumeResult = '';

        originalCallWebService = SW.Core.Services.callWebService;
        SW.Core.Services.callWebService = function (serviceUri, endpoint, params, successCallback, errorCallback) {
            globalServiceUri = serviceUri;
            globalEndpoint = endpoint;
            globalParams = params;

            if (orionVolumeResult != undefined) {
                successCallback(orionVolumeResult);
            }
        };

        $('body').append('<div id="AssociatedPools" class="AssociatedPools1">%SML:1%<div>');
        $('body').append('<div id="AssociatedVolumes" class="AssociatedVolumes1">%SMV:1%<div>');
        $('body').append('<div id="AssociatedEndpoint" class="AssociatedEndpoint1">%SML:1%<div>');
        $('body').append('<div id="FileSystemUsedCapacity" class="FileSystemUsedCapacity1">%SML:1%<div>');
        $('body').append('<div id="FileSystemUsedPercentage" class="FileSystemUsedPercentage1">%SML:1%<div>');
        
    },
    teardown: function () {
        $('#AssociatedPools').remove();
        $('#AssociatedVolumes').remove();
        $('#AssociatedEndpoint').remove();
        $('#FileSystemUsedCapacity').remove();
        $('#FileSystemUsedPercentage').remove();
        SW.Core.Services.callWebService = originalCallWebService;
    }
});

test("SW.SRM.Common test fill Associated Pools placeholders", function (assert) {
    orionVolumeResult = orionVolume;
    SW.SRM.Common.FillAdditionColumns(params);
    assert.equal($('#AssociatedPools a')[0].innerHTML, "Pool1", "Value must be Pool1");
});

test("SW.SRM.Common test fill Associated Volumes placeholders", function (assert) {
    orionVolumeResult = orionVolume;
    SW.SRM.Common.FillAdditionColumns(params);
    assert.equal($('#AssociatedVolumes a')[0].innerHTML, "Volume1", "Value must be Volume1");
});

test("SW.SRM.Common test fill Associated Endpoint placeholders", function (assert) {
    orionVolumeResult = orionVolume;
    SW.SRM.Common.FillAdditionColumns(params);
    assert.equal($('#AssociatedEndpoint')[0].innerHTML, "AssociatedEndpoint1", "Value must be AssociatedEndpoint1");
});

test("SW.SRM.Common test fill SpaceUsed placeholders", function (assert) {
    orionVolumeResult = orionVolume;
    SW.SRM.Common.FillAdditionColumns(params);
    assert.equal($('#FileSystemUsedCapacity')[0].innerHTML, "0.03 @{R=SRM.Strings;K=Value_Unit_KB;E=js}", "Value must be 0.03KB");
});

test("SW.SRM.Common test fill Used Percentage placeholders", function (assert) {
    orionVolumeResult = orionVolume;
    SW.SRM.Common.FillAdditionColumns(params);
    assert.equal($('#FileSystemUsedPercentage')[0].innerHTML, "30@{R=SRM.Strings;K=Value_Unit_Percentage;E=js}", "Value must be 30%");
});

test("SW.SRM.Common test NetObject link created for array", function (assert) {
    var netObjectType = 'SMSA';
    var netObjectId = 27;
    var entity = 'Orion.SRM.StorageArrays';
    var name = 'Array Name';
    var status = 1;
    var link = SW.SRM.Common.GetNetObjectLink(netObjectType, netObjectId, status, false, name);
    testNetObjectLink(assert, link, netObjectType, netObjectId, entity, status, name);
});

test("SW.SRM.Common test NetObject link created for cluster", function (assert) {
    var netObjectType = 'SMSA';
    var netObjectId = 26;
    var entity = 'Orion.SRM.Clusters';
    var name = 'The Cluster';
    var status = 2;
    var link = SW.SRM.Common.GetNetObjectLink(netObjectType, netObjectId, status, true, name);
    testNetObjectLink(assert, link, netObjectType, netObjectId, entity, status, name);
});

test("SW.SRM.Common test NetObject link created for pool", function (assert) {
    var netObjectType = 'SMSP';
    var netObjectId = 25;
    var entity = 'Orion.SRM.Pools';
    var name = 'De poool';
    var status = 3;
    var link = SW.SRM.Common.GetNetObjectLink(netObjectType, netObjectId, status, false, name);
    testNetObjectLink(assert, link, netObjectType, netObjectId, entity, status, name);
});

test("SW.SRM.Common test NetObject link created for lun", function (assert) {
    var netObjectType = 'SML';
    var netObjectId = 24;
    var entity = 'Orion.SRM.LUNs';
    var name = 'some lun name';
    var status = 4;
    var link = SW.SRM.Common.GetNetObjectLink(netObjectType, netObjectId, status, false, name);
    testNetObjectLink(assert, link, netObjectType, netObjectId, entity, status, name);
});

test("SW.SRM.Common test NetObject link created for volume", function (assert) {
    var netObjectType = 'SMV';
    var netObjectId = 23;
    var entity = 'Orion.SRM.Volumes';
    var name = 'some lun name';
    var status = 5;
    var link = SW.SRM.Common.GetNetObjectLink(netObjectType, netObjectId, status, false, name);
    testNetObjectLink(assert, link, netObjectType, netObjectId, entity, status, name);
});

test("SW.SRM.Common test NetObject link created for vserver", function (assert) {
    var netObjectType = 'SMVS';
    var netObjectId = 22;
    var entity = 'Orion.SRM.VServers';
    var name = 'VServer';
    var status = 6;
    var link = SW.SRM.Common.GetNetObjectLink(netObjectType, netObjectId, status, false, name);
    testNetObjectLink(assert, link, netObjectType, netObjectId, entity, status, name);
});

test("SW.SRM.Common test NetObject link created with status Unknown if status is null", function (assert) {
    var netObjectType = 'SMVS';
    var netObjectId = 22;
    var entity = 'Orion.SRM.VServers';
    var name = 'VServer';
    var link = SW.SRM.Common.GetNetObjectLink(netObjectType, netObjectId, null, false, name);
    testNetObjectLink(assert, link, netObjectType, netObjectId, entity, 0, name);
});

test("SW.SRM.Common test NetObject link created with status Unknown if status is empty string", function (assert) {
    var netObjectType = 'SMVS';
    var netObjectId = 22;
    var entity = 'Orion.SRM.VServers';
    var name = 'VServer';
    var link = SW.SRM.Common.GetNetObjectLink(netObjectType, netObjectId, '', false, name);
    testNetObjectLink(assert, link, netObjectType, netObjectId, entity, 0, name);
});


    
