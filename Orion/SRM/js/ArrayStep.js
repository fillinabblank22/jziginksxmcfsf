﻿Ext.namespace('SW');
Ext.namespace('SW.Core');
Ext.namespace('SW.SRM');

SW.SRM.ArraySelector = function () {

    var selectorModel;
    var dataStore;
    var grid;
    var pageSizeNum;
    var discoveryResultCheckInterval = 1000;
    var gridColumns = [];
    var isExternalProvider;
    var discoveryResult;

    ORION.prefix = "SRM.ArraySelector_";

    function getDiscoveryJobResult() {
        SW.SRM.ArraySelector.getGrid().loadMask.show();
        ORION.callWebService("/Orion/SRM/Services/EnrollmentWizardService.asmx", "GetDiscoveryJobResult", {}, function (result) {
            if (result.isDone == true) {
                SW.SRM.ArraySelector.getGrid().loadMask.hide();
                if (result.errorMessage == null || result.errorMessage.length == 0) {
                    $('.srm-enrollment-wizard-discovery-result-link').hide();
                    $("#SelectedArrayIDs").val(result.selectedArray);
                    SW.SRM.ArraySelector.reload();
                } else {
                    discoveryResult = result.discoveryResult;
                    $('.srm-enrollment-wizard-discovery-result-link').show();

                    Ext.Msg.alert('@{R=SRM.Strings;K=AddStorage_ArraysStep_MessageBoxTitle;E=xml}', result.errorMessage);
                }
            }
            else {
                setTimeout(function () { getDiscoveryJobResult(); }, discoveryResultCheckInterval);
            }
        });
    };

    function fillGridColumns(sModel) {
        gridColumns = [
                sModel,
                { header: '@{R=SRM.Strings;K=SRMArrayStep_ID;E=xml}', width: 80, hidden: true, hideable: false, sortable: isExternalProvider, dataIndex: 'UniqueArrayId', menuDisabled: true },
                { header: '@{R=SRM.Strings;K=SRMArrayStep_Array;E=xml}', hideable: false, width: 200, sortable: isExternalProvider, dataIndex: 'Name', menuDisabled: true },
                { header: '@{R=SRM.Strings;K=SRMArrayStep_Type;E=xml}', hideable: false, width: 200, sortable: isExternalProvider, dataIndex: 'Type', menuDisabled: true },
                { header: '@{R=SRM.Strings;K=SRMArrayStep_Disks;E=xml}', hideable: false, sortable: isExternalProvider, dataIndex: 'Disks', menuDisabled: true },
                { header: '@{R=SRM.Strings;K=SRMArrayStep_Monitored;E=xml}', hideable: false, sortable: isExternalProvider, dataIndex: 'AlreadyManaged', menuDisabled: true, renderer: function (value, record) { return value ? '@{R=SRM.Strings;K=SRMArrayStep_MonitoredYes;E=xml}' : '@{R=SRM.Strings;K=SRMArrayStep_MonitoredNo;E=xml}'; } }
        ];
        if (isExternalProvider == 1) {
            gridColumns.push({ header: '@{R=SRM.Strings;K=SRMArrayStep_Provider;E=xml}', width: 200, hideable: false, sortable: isExternalProvider, dataIndex: 'Provider', menuDisabled: true });
        }
    };

    function onAfterPageChange() {
        var value = $("#SelectedArrayIDs").val();
        if (typeof (value) === 'undefined') {
            value = '';
        }
        var selectAll = !isExternalProvider;

        grid.store.each(function (record, id) {
            if (selectAll || value.indexOf('{' + record.data.UniqueArrayId + '}') != -1) {
                grid.getSelectionModel().selectRecords([record], true);
            }
        });
        if (SW.SRM.ArraySelector.onLicenseUpdate != null) {
            SW.SRM.ArraySelector.onLicenseUpdate();
        }
    };

    function onRowSelected(sm, rowIdx, r) {
        if (r.data.AlreadyManaged) {
            sm.deselectRow(rowIdx);
            return;
        }
        var value = $("#SelectedArrayIDs").val();
        if (value.indexOf('{' + r.data.UniqueArrayId + '}') != -1) {
            return;
        }
        value = value + '{' + r.data.UniqueArrayId + '}';

        $("#SelectedArrayIDs").val(value);

        if (SW.SRM.ArraySelector.onLicenseUpdate != null) {
            SW.SRM.ArraySelector.onLicenseUpdate();
        }
    };

    function onRowDeselected(sm, rowIdx, r) {
        var value = $("#SelectedArrayIDs").val();
        value = value.replace('{' + r.data.UniqueArrayId + '}', '');
        $("#SelectedArrayIDs").val(value);

        if (SW.SRM.ArraySelector.onLicenseUpdate != null) {
            SW.SRM.ArraySelector.onLicenseUpdate();
        }
    };

    return {
        onLicenseUpdate: null,
        showDiscoveryResult: function () {
            if (typeof(discoveryResult) !== "object" ) {
                return;
            }

            var $table = $('<table class="srm-enrollment-wizard-discovery-result" />');

            var $row = $('<tr></tr>');
            $row.append($('<th></th>').text('@{R=SRM.Strings;K=AddStorage_ArraysStep_DiscoveryResult_TestedTemplate;E=xml}'));
            $row.append($('<th></th>').text('@{R=SRM.Strings;K=AddStorage_ArraysStep_DiscoveryResult_Result;E=xml}'));
            $table.append($row);

            $.each(discoveryResult, function (i, row) {
                $row = $('<tr></tr>');
                
                $row.append($('<td></td>').text(row.Item1));
                $row.append($('<td></td>').text(row.Item2));

                $table.append($row);
            });

            var $div = $('<div />');
            $div.append($table);

            var myWindow = new Ext.Window({
                title: '@{R=SRM.Strings;K=AddStorage_ArraysStep_DiscoveryResult_Title;E=xml}',
                resizable: false, closable: true, closeAction: 'hide', width: 600, minHeight: 200, plain: true, modal: true, layout: 'fit',
                html: $div.html(),
                buttons: [{
                    text: 'OK',
                    handler: function () { myWindow.hide(); }
                }]
            });

            myWindow.show();
        },
        getGrid: function () {
            return grid;
        },
        reload: function () {
            grid.store.reload({
                scope: this,
                callback: function () {
                    onAfterPageChange();
                }
            });
        },
        init: function (gridElementId, gridPanelWidth, externalProvider) {
            isExternalProvider = externalProvider;

            // load default page size from Orion settings
            pageSizeNum = parseInt(ORION.Prefs.load('PageSize', '10'));
            Ext.override(Ext.PagingToolbar, {
                addPageSizer: function () {
                    // add a combobox to the toolbar
                    var store = new Ext.data.SimpleStore({
                        fields: ['pageSize'],
                        data: [[10], [20], [30], [40], [50]]
                    });
                    var combo = new Ext.form.ComboBox({
                        regex: /^\d*$/,
                        store: store,
                        displayField: 'pageSize',
                        mode: 'local',
                        triggerAction: 'all',
                        selectOnFocus: true,
                        width: 50,
                        editable: false,
                        value: pageSizeNum,
                        id: 'pagerCombo'
                    });
                    this.addField(combo);

                    combo.on("select", function (c, record) {
                        this.pageSize = record.get("pageSize");
                        this.cursor = 0;
                        ORION.Prefs.save('PageSize', this.pageSize);
                        this.doRefresh();
                    }, this);
                }
            });

            // set selection model to checkbox
            selectorModel = new Ext.grid.CheckboxSelectionModel();
            fillGridColumns(selectorModel);
            // define data store for credential grid - use web service (method) for load of data
            dataStore = new ORION.WebServiceStore(
                "/Orion/SRM/Services/EnrollmentWizardService.asmx/GetArrays",
                [
                    { name: 'UniqueArrayId', mapping: 0 },
                    { name: 'Name', mapping: 1 },
                    { name: 'Type', mapping: 2 },
                    { name: 'Disks', mapping: 3 },
                    { name: 'Provider', mapping: 4 },
                    { name: 'AlreadyManaged', mapping: 5 },
                    { name: 'IsLicensed', mapping: 6 }
                ],
                "Name");

            // define grid panel
            grid = new Ext.grid.GridPanel({
                store: dataStore,
                columns: gridColumns,
                getColumns: function () { return gridColumns; },
                sm: selectorModel,
                viewConfig: {
                    forceFit: true
                },
                listeners: {
                    rowclick: function (in_this, rowIndex, e) {
                        var record = in_this.getStore().getAt(rowIndex);
                        if (record.data.AlreadyManaged) {
                            Ext.Msg.alert('@{R=SRM.Strings;K=AddStorage_ArraysStep_MessageBoxAlreadyMangadedTitle;E=xml}', '@{R=SRM.Strings;K=AddStorage_ArraysStep_MessageBoxAlreadyMangadedInfo;E=xml}');
                            in_this.getSelectionModel().deselectRow(rowIndex);
                        }
                    }
                },
                loadMask: { msg: '@{R=SRM.Strings;K=AddStorage_ArrayStep_DiscoveryRunning;E=xml}' },
                autoHeight: true,
                stripeRows: true,
                bbar: isExternalProvider == 1 ? new Ext.PagingToolbar({
                    store: dataStore,
                    pageSize: pageSizeNum,
                    displayInfo: true,
                    listeners:
                    {
                        change: onAfterPageChange
                    }
                }) : null
            });

            if (isExternalProvider == 1) {
                grid.bottomToolbar.addPageSizer();
            } else {
                grid.getColumnModel().setHidden(0, true);
            }

            grid.getView().on('refresh', function () {
                var obj = $('.x-grid3-hd-checker');
                if (obj && obj.hasClass('x-grid3-hd-checker-on'))
                    obj.removeClass('x-grid3-hd-checker-on');
            });

            grid.getSelectionModel().on('rowselect', onRowSelected);
            grid.getSelectionModel().on('rowdeselect', onRowDeselected);

            grid.render(gridElementId);

            grid.setWidth(gridPanelWidth);
            grid.store.proxy.conn.jsonData = {};
            grid.store.load();

            getDiscoveryJobResult();
        }
    };

}();
