﻿SW = SW || {};
SW.SRM = SW.SRM || {};
SW.SRM.Common = SW.SRM.Common || {};

Ext.QuickTips.init();

/* 
String format like in C#
*/
String.prototype.format = function (params) {
    var pattern = /{([^}]+)}/g;
    return this.replace(pattern, function ($0, $1) { return params[$1]; });
};

SW.SRM.AssignPollingEngine = function () {
    var horizon = 2; //2 minutes

    function GetStatusImage(dbTimeDiff) {

        if ((null != dbTimeDiff) && (undefined !== dbTimeDiff)) {
            if (dbTimeDiff < horizon) {
                return "Small-Up.gif";
            }
            else if (dbTimeDiff > horizon * 1.5) {
                return "Small-Down.gif";
            }
            else {
                return "Small-Warning.gif";
            }
        }
        return "";
    }

    function renderStatus(keepAlive, meta, record) {

        if ((null != record.data.DatabaseTimeDiff) && (undefined != record.data.DatabaseTimeDiff)) {
            if (record.data.DatabaseTimeDiff < horizon) {
                return String.format('<span style="vertical-align: top;">{0}</span>', '@{R=Core.Strings;K=StatusDesc_Active;E=js}');
            }
            else if (record.data.DatabaseTimeDiff > horizon * 1.5) {
                return String.format('<span style="vertical-align: top;">{0}</span>', '@{R=Core.Strings;K=StatusDesc_Down;E=js}');
            }
            else {
                return String.format('<span style="vertical-align: top;">{0}</span>', '@{R=Core.Strings;K=StatusDesc_Warning;E=js}');
            }
        }
        return "";
    }

    function renderEngineLink(record) {
        return String.format('<a href="/Orion/Admin/Details/Engines.aspx"><img src="/NetPerfMon/images/{0}"/>&nbsp;<span style="vertical-align: top;">{1} ({2})</span></a>',
            GetStatusImage(record.data.DatabaseTimeDiff), record.data.ServerName, getServerType(record.data.ServerType));
    }

    function renderText(value, meta, record) {
        return String.format('<span style="vertical-align: top;">{0}</span>', Ext.util.Format.htmlEncode(value));
    }

    function renderName(value, meta, record) {
        return String.format('<img src="/NetPerfMon/images/{0}"/>&nbsp;<span style="vertical-align: top;">{1}</span>',
                    GetStatusImage(record.data.DatabaseTimeDiff), Ext.util.Format.htmlEncode(value));
    }

    function renderServerType(value, meta, record) {
        return String.format('<span style="vertical-align: top;">{0}</span>', Ext.util.Format.htmlEncode(getServerType(value)));
    }

    function getServerType(serverType) {
        switch (serverType) {
            case "Primary": return "@{R=Core.Strings;K=ServerType_primary;E=js}";
            case "Additional": return "@{R=Core.Strings;K=ServerType_additional;E=js}";
            default: return serverType;
        }
    }

    function refreshObjects(callback) {
        grid.store.removeAll();
        grid.store.proxy.conn.jsonData = { search: "" };
        grid.store.load({ callback: callback });
    };

    //Ext grid
    ORION.prefix = "SRM_AssignPollingEngine_";
    var selectorModel;
    var dataStore;
    var grid;
    var win;
    return {
        showDialog: function () {
            grid.store.removeAll();
            refreshObjects(function () {
                win.show();
            });
        },
        init: function (onSubmit) {
            selectorModel = new Ext.sw.grid.RadioSelectionModel();
            dataStore = new ORION.WebServiceStore(
                                "/Orion/SRM/Services/StorageManagementService.asmx/GetEnginesTable",
                                [
                                 { name: 'EngineID', mapping: 0 },
                                 { name: 'ServerName', mapping: 1 },
                                 { name: 'ServerType', mapping: 2 },
                                 { name: 'KeepAlive', mapping: 3 },
                                 { name: 'Elements', mapping: 4 },
								 { name: 'DatabaseTimeDiff', mapping: 5 }
                                ],
                                "ServerName");

            var pagingToolbar = new Ext.PagingToolbar(
                { store: dataStore, pageSize: 15, displayInfo: true, split: true, displayMsg: '@{R=Core.Strings;K=WEBJS_AK0_4;E=js}', emptyMsg: "@{R=Core.Strings;K=WEBJS_AK0_3;E=js}" }
            );

            grid = new Ext.grid.GridPanel({
                region: 'center',
                store: dataStore,
                columns: [selectorModel,
                    { header: '@{R=Core.Strings;K=WEBJS_AK0_5;E=js}', width: 10, hidden: true, hideable: false, sortable: false, dataIndex: 'EngineID' },
                    { header: '@{R=Core.Strings;K=WEBJS_AK0_9;E=js}', width: 200, sortable: true, dataIndex: 'ServerName', renderer: renderName },
                    { header: '@{R=Core.Strings;K=WEBJS_AK0_6;E=js}', width: 90, sortable: true, dataIndex: 'KeepAlive', renderer: renderStatus },
                    { header: '@{R=Core.Strings;K=WEBJS_AK0_7;E=js}', width: 90, sortable: true, dataIndex: 'ServerType', renderer: renderServerType },
                    { header: '@{R=Core.Strings;K=WEBJS_AK0_8;E=js}', width: 180, sortable: true, dataIndex: 'Elements', renderer: renderText }
                ],
                sm: selectorModel, layout: 'fit', autoScroll: 'true', loadMask: true, width: 560, height: 400, stripeRows: true,
                bbar: pagingToolbar
            });
            
            win = new Ext.Window({
                title: '@{R=Core.Strings;K=WEBJS_AK0_10;E=js}', resizable: false, closable: true, closeAction: 'hide', width: 600, height: 500, plain: true, modal: true, layout: 'border', items: [grid],
                buttons: [{
                    cls: "change-poll-engine-btn",
                    text: '@{R=Core.Strings;K=WEBJS_AK0_11;E=js}',
                    handler: function () {
                        if ($("#isDemoMode").length != 0) {
                            demoAction("Core_NodeManagement_ChangePollingEngine", this);
                            return;
                        }

                        var item = grid.getSelectionModel().selections.items[0];

                        if ($("#selPollingEngineId").length != 0) {
                            $("#" + $("#selPollingEngineId")[0].value)[0].value = item.data.EngineID;
                            $("span[id*='pollEngineDescr']")[0].innerHTML = renderEngineLink(item);
                        }

                        if (typeof (onSubmit) === "function") {
                            onSubmit(item.data.EngineID, renderEngineLink(item));
                        }
                        win.hide();
                    }
                },
                {
                    cls: "change-poll-engine-btn", text: '@{R=Core.Strings;K=CommonButtonType_Cancel;E=js}', handler: function () { win.hide(); }
                }]
            });

            $("a[id*='changePollEngineLink']").click(function () {
                if (!$(this).parent().hasClass("Disabled")) {
                    SW.SRM.AssignPollingEngine.showDialog();
                }
                return false;
            });
        }
    };
}();