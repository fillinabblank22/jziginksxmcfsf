﻿SW = SW || {};
SW.Core = SW.Core || {};

var dfmProvider = SW.Core.namespace("SW.SRM.EditProperties.DfmProvider");
var request;

dfmProvider.onCancelClick = function () {
    $("#testingConnectionMsg").hide();
    if (request) {
        request.abort();
    }
};

dfmProvider.hideError = function () {
    $("#failMessageHeader").hide();
    $("#testConnectionPassMsg").hide();
    $("#testConnectionFailMsg").hide();
};

dfmProvider.hidePass = function () {
    $("#testConnectionPassMsg").hide();
};

dfmProvider.showPass = function (text) {
    $("#passMessageContent").empty();
    $("#passMessageContent").append(text);
    $("#testConnectionPassMsg").show();
};

dfmProvider.showLoading = function (visible) {
    if (visible) {
        $('#testingConnectionMsg').show();
        $('#testConnectionSmisButton').attr('disabled', 'disabled');
    } else {
        $('#testingConnectionMsg').hide();
        $('#testConnectionSmisButton').removeAttr('disabled');
    }
};

dfmProvider.ValidateConnection = function () {
    return SW.SRM.EditProperties.DfmProvider.TestConnection(false);
};

dfmProvider.ValidateDiscovery = function() {
    return SW.SRM.EditProperties.DfmProvider.TestConnection(true);
}

dfmProvider.TestConnection = function (runDiscovery) {
    var testCredentialDeferred = $.Deferred();
    var testGlobalCredentialDeferred = $.Deferred();

    var cfg = JSON.parse($('#hfEditPropertiesDfmCfg').val());
    var credentialId = cfg.CredentialId;
    var ip = cfg.IP;
    var engineid = $('#selPollingEngineId').length == 0 ? cfg.EngineId : $('#' + $('#selPollingEngineId').val()).val();
	var useSsl = true;

	var showFailMessageBox = function (message, errorMessage) {
	    var title = "@{R=SRM.Strings;K=Provider_Step_Provider_Connecting_Error; E=js}".concat(' ').concat(ip);
	    SW.SRM.Common.ShowErrorEditProperties(title, message, errorMessage);
	}

	SW.SRM.DiscoveryValidation.init(cfg.ArrayID, cfg.ArraySerialNumber, cfg.DeviceGroupId, request, testCredentialDeferred, '@{R=SRM.Strings;K=Provider_Step_Unexpected_Error; E=js}', showFailMessageBox);
    
    if (!pollingProperties.ValidateUserInput()) {
        return testGlobalCredentialDeferred.reject();
    }

    request = SW.SRM.Common.CallSrmWebService("/Orion/SRM/Services/EnrollmentWizardService.asmx",
                                'TestNetAppDfmCredentialsProvider',
                                {
                                    'credentialId': credentialId,
                                    'credentialName': null,
                                    'password': null,
                                    'username': null,
                                    'httpPort': 0,
                                    'httpsPort': 0,
                                    'useSsl': useSsl,
                                    'ip': ip,
                                    'engineid': engineid,
                                    'saveProvider': runDiscovery
                                },

                              function (result) {
                                  if (result.IsSuccess) {
                                      SW.SRM.EditProperties.DfmProvider.showLoading(false);
                                      SW.SRM.EditProperties.DfmProvider.showPass(result.Message);

                                      if (runDiscovery){
                                          SW.SRM.DiscoveryValidation.startDiscovery();
                                      }
                                      else {
                                          testCredentialDeferred.resolve();
                                      }
                                  }
                                  else {
                                      SW.SRM.EditProperties.DfmProvider.showLoading(false);

                                      showFailMessageBox(result.Message, result.ErrorMessage);
                                      SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
                                  }
                              },
                                function () {
                                    SW.SRM.Common.ShowErrorEditProperties('Error during AJAX call');
                                    SW.SRM.DiscoveryValidation.rejectDeferred(runDiscovery);
                                });

    $.when(testCredentialDeferred).then(
    function (x) {
        SW.SRM.EditProperties.DfmProvider.showLoading(false);
        testGlobalCredentialDeferred.resolve();
    },
    function () {
        SW.SRM.EditProperties.DfmProvider.showLoading(false);
        testGlobalCredentialDeferred.reject();
    });

    SW.SRM.EditProperties.DfmProvider.hideError();
    SW.SRM.EditProperties.DfmProvider.hidePass();
    SW.SRM.EditProperties.DfmProvider.showLoading(true);

    return testGlobalCredentialDeferred.promise();
};

dfmProvider.MonitoredControlIds = ['pollEngineDescr'];
SW.SRM.EditProperties.ProviderChangeManager.InitProvider(dfmProvider, dfmProvider.ValidateDiscovery);
