﻿SW = SW || {};
SW.SRM = SW.SRM || {};
SW.SRM.Common = SW.SRM.Common || {};

SW.SRM.StorageObjects = function () {
    var selectorModel;
    var dataStore;
    var grid;
    var loadingMask = null;
    var toolBarHidden = false;
    var gridHeight = 610;
    var gridWidth = 1290;
    var selectedGroupElement;
    var filterField = "1=1";
    var userPageSize;
    var netObject = "SMSA:";
    var moreActionDataStore;
    var comboAction;

    var RefreshObjects = function () {
        grid.store.removeAll();
        if (loadingMask == null) {
            loadingMask = new Ext.LoadMask(grid.el, {
                msg: "@{R=Core.Strings;K=WEBJS_TM0_1;E=js}"
            });
        }
        grid.store.on('beforeload', function () { loadingMask.show(); });
        grid.store.proxy.conn.jsonData = { property: filterField, type: "", value: "", search: filterText };
        grid.store.on('load', function () {
            loadingMask.hide();
        });
        grid.store.load({ params: { start: 0, limit: userPageSize } });
    };

    function postRequest(url, params) {
        if (Ext.isEmpty(url)) return;
        if (Ext.isEmpty(params) || params.length == 0) {
            location.href = url;
            return;
        }
        var stringForm = String.format("<form id='importForm' action='{0}' method='POST'>", url);
        for (var i = 0; i < params.length; i++) {
            stringForm += String.format("<input type='hidden' name='{0}' value='{1}'/>", params[i].name, params[i].value);
        }
        stringForm += "</form>";

        $('body').append(stringForm);
        $('#importForm').submit();
    }

    function encodeHTML(value) {
        return Ext.util.Format.htmlEncode(value);
    }

    function renderName(value, meta, record) {
        var id = record.data.ID;
        var status = record.data.Status;

        var result = '';
        result += '<img src="/Orion/StatusIcon.ashx?size=small\&entity=Orion.SRM.StorageArrays\&id=' + id + '\&status=' + status + '" class="SRM-vertical-align-middle"/> ';
        result += '<span class="SRM-vertical-align-middle">';
        result += '<a href="' + record.data.DetailsUrl + '">' + renderString(value, meta, record) + '</a>';
        result += '</span>';

        return result;
    }

    function renderString(value, meta, record) {
        if (!value || value.length === 0)
            return value;

        if (!filterText || filterText.length === 0)
            return encodeHTML(value);

        var patternText = filterText;

        // replace any %'s with a *
        patternText = patternText.replace(/\%/g, "*");

        // replace multiple *'s with a single instance
        patternText = patternText.replace(/\*{2,}/g, "*");

        // check if the search string is now just down to a single *, and if so return without any further regex
        if (patternText == '*') {
            return '<span style=\"background-color: #FFE89E\">' + encodeHTML(value) + '</span>';
        }

        // replace \ with \\
        patternText = patternText.replace(/\\/g, '\\\\');
        // replace . with \.
        patternText = patternText.replace(/\./g, '\\.');
        // replace * with .*
        patternText = patternText.replace(/\*/g, '.*');

        // set regex pattern
        var x = '((' + patternText + ')+)\*';
        var content = value,
            pattern = new RegExp(x, "gi"),
            replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

        // do regex replace + content gets encoded
        var fieldValue = encodeHTML(content.replace(pattern, replaceWith));

        // now replace the literal SPAN markers with the HTML span tags
        fieldValue = fieldValue.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
        fieldValue = fieldValue.replace(/{SPAN-END-MARKER}/g, '</span>');

        return fieldValue;
    };

    function renderStatus(value, meta, record) {
        var status = record.data.Status;
        return SW.SRM.Common.RenderArrayStatus(status);
    }

    var DeleteSelectedItems = function (items) {
        var waitMsg = Ext.Msg.wait("@{R=Core.Strings;K=WEBJS_TM0_2;E=js}");

        var ids = new Array();
        Ext.each(items, function (item) {
            ids.push(item.data.ID);
        });

        ORION.callWebService("/Orion/SRM/Services/StorageManagementService.asmx",
                 "DeleteStorageArray", { storageArrayIds: ids },
                 function () {
                     waitMsg.hide();
                     SW.SRM.StorageObjects.RefreshGrid();
                     var combo = $('#groupBySelect')[0];
                     SW.SRM.StorageObjects.GenerateGroups(combo);
                 },
                function (errorMessage) {
                    Ext.Msg.show({
                        title: '@{R=SRM.Strings;K=StorageObjects_ErrorDeleteTitle; E=js}',
                        msg: '@{R=SRM.Strings;K=StorageObjects_ErrorDeleteTitle; E=js}',
                        icon: Ext.Msg.ERROR,
                        buttons: Ext.Msg.OK
                    });
                });
    };

    function ItemNotSelectedMsg() {
        Ext.Msg.show({
            title: '@{R=Core.Strings;K=WEBJS_YK0_2; E=js}',
            msg: '@{R=SRM.Strings;K=StorageObjects_NoItemsSelected; E=js}',
            buttons: { cancel: '@{R=Core.Strings;K=CommonButtonType_Ok; E=js}' },
            icon: Ext.Msg.WARNING
        });
    }

    function MoreActions(value, items) {
        switch (value) {
            case 'StatisticsNow':
                PollNow(items, false);
                break;
            case 'TopologyNow':
                PollNow(items, true);
                break;
            case 'Import':
                ImportProperties();
                break;
            case 'Export':
                ExportProperties(items);
                break;
            case 'AssignEngine':
                assignPollingEngine(items);
                break;
            default:

                break;
        }
    }

    function PollNow(items, isTopology) {
        var ids = new Array();
        if (grid.getSelectionModel().getCount() < 1) {
            ItemNotSelectedMsg();
            return;
        }
        Ext.each(items, function (item) {
            ids.push(netObject + item.data.ID);
        });

        return isTopology ? showDialogPollForJobNow(ids, 'Topology') : showDialogPollNow(ids);
    }

    function assignPollingEngine(items) {
        if (grid.getSelectionModel().getCount() < 1) {
            ItemNotSelectedMsg();
            return;
        }
        var ids = new Array();
        Ext.each(items, function (item) {
            ids.push(item.data.ID);
        });
        var polingEngine = SW.SRM.AssignPollingEngine;
        polingEngine.init(function (engId) {
            ORION.callWebService("/Orion/SRM/Services/StorageManagementService.asmx",
                                  "UpdatePollingEngine", { engineId: engId, ids: ids },
                                  function () {
                                      window.location.reload();
                                  },
                                  function (errorMessage) {
                                      Ext.Msg.show({
                                          msg: errorMessage,
                                          icon: Ext.Msg.ERROR,
                                          buttons: Ext.Msg.OK
                                      });
                                  });
        });
        SW.SRM.AssignPollingEngine.showDialog();
    }

    function ImportProperties() {
        var importParams = new Array();
        importParams.push({ name: 'EntityType', value: "Orion.SRM.StorageArrays" });
        postRequest('/Orion/Admin/CPE/ImportCP.aspx', importParams);
    }

    function ExportProperties(items) {
        //check if something selected
        if (grid.getSelectionModel().getCount() < 1) {
            ItemNotSelectedMsg();
            return;
        }
        var ids = new Array();
        Ext.each(items, function (item) {
            ids.push(item.data.ID);
        });
        $('body').append(PrepareExportPostForm("Orion.SRM.StorageArrays", ids, $('#ReturnToUrl').val()));
        $('#selectedNetObjects').submit();
    }

    function PrepareExportPostForm(netObjectType, ids, returnUrl) {
        var stringForm = "<form id='selectedNetObjects' action='{0}' method='POST'> \
                            <input type='hidden' name='EntityType' value='{1}'/> \
                            <input type='hidden' name='NetObjectsIds' value='{2}'/> \
                         </form>";
        var params = (ids.length) ? ids.join(",") : '';
        var actionUrl = "/Orion/Admin/CPE/ExportCP.aspx?ReturnTo=" + returnUrl;
        return $(stringForm.format([actionUrl, netObjectType, params]));
    }

    var updateToolbarButtons = function () {
        var map = grid.getTopToolbar().items.map;
        var selCount = grid.getSelectionModel().getSelections().length;

        map.DeleteButton.setDisabled(selCount == 0);
        map.RemanageButton.setDisabled(selCount == 0);
        map.UnmanageButton.setDisabled(selCount == 0);
        map.EditPropertiesButton.setDisabled(selCount == 0);
        Ext.each(grid.getSelectionModel().selections.items, function (item) {
            if (!item.data.Unmanaged) {
                map.RemanageButton.setDisabled(true);
            }
            if (item.data.Unmanaged) {
                map.UnmanageButton.setDisabled(true);
            }
        });
        if (selCount == 0)
            map.moreActionCombo.setValue('');
    };


    var result = {
        FieldFilter: function () {
            return filterField;
        },
        RefreshGrid: function () {
            RefreshObjects();
        },
        GenerateGroups: function (select) {
            var groupByResults = $('#groupByResults');
            var propertyName = 'A.' + select.value;
            if (propertyName == 'A.None') {
                filterField = "1=1";
                SW.SRM.StorageObjects.RefreshGrid();
                groupByResults.children().remove();
                return;
            }
            ORION.callWebService("/Orion/SRM/Services/StorageManagementService.asmx",
                "GetArraysFilter", { fieldName: propertyName },
                function (result) {
                    groupByResults.children().remove();
                    var innerHtml = groupByResults.html();
                    for (var i = 0; i < result.Rows.length; i++) {
                        var row = result.Rows[i];
                        var propValue = "";

                        if (row[0] != null) {
                            propValue = encodeHTML(row[0].replace(/"/g, "\'"));
                        }
                        innerHtml += '<tr style="width: 100%"><td class="SRM-StorageObjects-ClickableResult"';
                        innerHtml += 'onClick="SW.SRM.StorageObjects.ApplyGrouping(this, &quot;' + propertyName + '&quot;,&quot;' + propValue + '&quot;)" >';

                        if (propertyName == "A.Status") {
                            propValue = SW.SRM.Common.RenderArrayStatus(parseInt(propValue));
                        }

                        innerHtml += '<div>' + (propValue || 'Other') + ' (' + row[1] + ')</div></td></tr>';
                    }
                    groupByResults.html(innerHtml);
                });
        },
        ApplyGrouping: function (tdElement, fieldName, fieldValue) {
            fieldValue = fieldValue.replace(/'/g, "''");
            var fieldCondition = " = '" + fieldValue + "'";
            if (selectedGroupElement) {
                selectedGroupElement.className = 'SRM-StorageObjects-ClickableResult';
            }

            selectedGroupElement = tdElement;
            selectedGroupElement.className += ' SelectedGroupItem';
            if ((fieldValue === 'null') || (!fieldValue)) {
                fieldCondition = ' is null OR ' + fieldName + " = ''";
            }

            filterField = fieldValue.indexOf('?') < 0 ? fieldName + fieldCondition : "";
            SW.SRM.StorageObjects.RefreshGrid();
        },

        setProviderData: function (data) {
            if (data != null && data != '') {
                var fields = data.split(':');
                filterField = "[A].Providers.IPAddress LIKE '" + fields[1] + "'OR [A].Providers.DisplayName LIKE '" + fields[0] + "'";
                window.filterPropertyParam = filterField;
            }
        },

        init: function () {

            userPageSize = parseInt(ORION.Prefs.load('PageSize', '20'));
            selectorModel = new Ext.grid.CheckboxSelectionModel({ hidden: false });
            var gridStoreColumns = [
                { name: 'ID', mapping: 0 },
                { name: 'Name', mapping: 1 },
                { name: 'IPAddress', mapping: 6 },
                { name: 'DataProvider', mapping: 7 },
                { name: 'Status', mapping: 2 },
                { name: 'PollingEngine', mapping: 3 },
                { name: 'Unmanaged', mapping: 4 },
                { name: 'DetailsUrl', mapping: 5 }
            ];
            dataStore = new ORION.WebServiceStore("/Orion/SRM/Services/StorageManagementService.asmx/GetAllArrays", gridStoreColumns, "ID");
            pageSizeBox = new Ext.form.NumberField({
                id: 'PageSizeField',
                enableKeyEvents: true,
                allowNegative: false,
                width: 40,
                allowBlank: false,
                minValue: 1,
                maxValue: 100,
                value: userPageSize,
                listeners: {
                    scope: this,
                    'keydown': function (f, e) {
                        var k = e.getKey();
                        if (k == e.RETURN) {
                            e.stopEvent();
                            var v = parseInt(f.getValue());

                            // Perform Extended Validation
                            if (isNaN(v)) {
                                Ext.Msg.show({
                                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                    msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                                });
                                return;
                            }
                            else if (v <= 0) {
                                Ext.Msg.show({
                                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                    msg: "@{R=SRM.Strings;K=SRM_StorageObjects_MinimumWarning; E=js}",
                                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                                });
                                // Fix user input by establishing Minimum value
                                v = 1;
                                f.setValue('1');
                            }
                            else if (v > 100) {
                                Ext.Msg.show({
                                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                    msg: "@{R=SRM.Strings;K=SRM_StorageObjects_MaximumWarning; E=js}",
                                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                                });
                                // Fix user input by establishing Maximum value
                                v = 100;
                                f.setValue('100');
                            }

                            if (userPageSize != v) {
                                userPageSize = v;
                                pagingToolbar.pageSize = userPageSize;
                                ORION.Prefs.save('PageSize', userPageSize);
                                pagingToolbar.doLoad(0);
                            }

                        }
                    },
                    'focus': function (field) {
                        field.el.dom.select();
                    },
                    'change': function (f, numbox, o) {
                        var pSize = parseInt(f.getValue());

                        // Perform Extended Validation
                        if (isNaN(pSize)) {
                            Ext.Msg.show({
                                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                            });
                            return;
                        }
                        else if (pSize <= 0) {
                            Ext.Msg.show({
                                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                msg: "@{R=SRM.Strings;K=SRM_StorageObjects_MinimumWarning; E=js}",
                                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                            });
                            // Fix user input by establishing Minimum value
                            pSize = 1;
                            f.setValue('1');
                        }
                        else if (pSize > 100) {
                            Ext.Msg.show({
                                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                msg: "@{R=SRM.Strings;K=SRM_StorageObjects_MaximumWarning; E=js}",
                                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                            });
                            // Fix user input by establishing Maximum value
                            pSize = 100;
                            f.setValue('100');
                        }

                        if (pagingToolbar.pageSize != pSize) { // update page size only if it is different
                            pagingToolbar.pageSize = pSize;
                            userPageSize = pSize;

                            ORION.Prefs.save('PageSize', userPageSize);
                            pagingToolbar.doLoad(0);
                        }
                    }
                }
            });
            selectorModel.on("selectionchange", function () {
                updateToolbarButtons();
            });
            var pagingToolbar = new Ext.PagingToolbar(
            {
                store: dataStore,
                pageSize: userPageSize,
                displayInfo: true,
                displayMsg: "@{R=Core.Strings;K=WEBJS_SO0_2; E=js}",
                emptyMsg: "@{R=Core.Strings;K=WEBJS_SO0_3; E=js}",
                beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
                items: [
                '-',
                new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }),
                pageSizeBox
                ]
            });
            moreActionDataStore = new Ext.data.SimpleStore({
                fields: ['Name', 'Value'],
                data: [
                    ['@{R=SRM.Strings;K=StorageObjects_MoreActionsLabel; E=js}', ''],
                    ['@{R=SRM.Strings;K=StorageObjects_StatisticsNowLabel; E=js}', 'StatisticsNow'],
                    ['@{R=SRM.Strings;K=StorageObjects_TopologyNowLabel; E=js}', 'TopologyNow'],
                    ['@{R=SRM.Strings;K=StorageObjects_ImportCustomPropertyLabel; E=js}', 'Import'],
                    ['@{R=SRM.Strings;K=StorageObjects_ExportCustomPropertyLabel; E=js}', 'Export'],
                    ['@{R=SRM.Strings;K=StorageObjects_AssignPollingEngineLabel; E=js}', 'AssignEngine']
                ],
                autoLoad: true
            });

            var tpl = new Ext.XTemplate(
                '<tpl for=".">',
                '<tpl if="(Value ==\'\')">',
                '<div class="x-combo-list-item">{Name}</div>',
                '</tpl>',
                '<tpl if="(Value ==\'Export\') || (Value ==\'StatisticsNow\') || (Value ==\'TopologyNow\') || (Value ==\'AssignEngine\')">',
                '<div class="itemDisabled x-combo-list-item">{Name}</div>',
                '</tpl>',
                '<tpl if="(Value ==\'Import\')">',
                '<div class="x-combo-list-item">{Name}</div>',
                '</tpl>',
                '</tpl>');

            comboAction = new Ext.form.ComboBox({
                renderTo: document.body,
                tpl: tpl,
                name: 'moreAction',
                value: '',
                id: 'moreActionCombo',
                typeAhead: false,
                width: 200,
                store: moreActionDataStore,
                displayField: 'Name',
                valueField: 'Value',
                mode: 'local',
                triggerAction: 'all',
                selectOnFocus: false,
                editable: false,
                listeners: {
                    expand: function () {
                        var selCount = grid.getSelectionModel().getSelections().length;
                        if (selCount == 0) {
                            $(".itemDisabled").removeClass("x-combo-list-item").css("color", "gray");
                        } else {
                            $(".itemDisabled").addClass("x-combo-list-item").css("color", "black");
                        }
                    },
                    select: function (value) {
                        MoreActions(value.value, grid.getSelectionModel().selections.items);
                    }
                },
                lastQuery: ''
            });
            var toolbar = new Ext.Toolbar({
                id: 'myGridToolbar',
                style: { overflow: 'visible', width: '100%' },
                hidden: toolBarHidden,
                items: [
                {
                    id: 'AddButtonButton',
                    text: '@{R=SRM.Strings;K=StorageObjects_AddArrayLabel;E=js}',
                    tooltip: '@{R=SRM.Strings;K=StorageObjects_AddArrayLabel;E=js}',
                    icon: '/Orion/SRM/images/u412.png',
                    cls: 'x-btn-text-icon',
                    handler: function() { location.href = '/Orion/SRM/Config/DeviceTypeStep.aspx'; }
                }, '-', {
                    id: 'CustomPropertyButton',
                    text: '@{R=SRM.Strings;K=StorageObjects_CustomPropertyLabel;E=js}',
                    tooltip: '@{R=SRM.Strings;K=StorageObjects_CustomPropertyLabel;E=js}',
                    icon: '/Orion/images/CPE/custom_properties_inline_editor.png',
                    cls: 'x-btn-text-icon',
                    handler: function() { location.href = '/Orion/Admin/CPE/Default.aspx'; }
                }, '-', {
                    id: 'EditPropertiesButton',
                    text: '@{R=SRM.Strings;K=StorageObjects_EditPropertiesLabel;E=js}',
                    tooltip: '@{R=SRM.Strings;K=StorageObjects_EditPropertiesLabel;E=js}',
                    icon: '/Orion/SRM/images/bg_button_edit_icon.png',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        var ids = new Array();
                        Ext.each(grid.getSelectionModel().selections.items, function (item) {
                            ids.push(netObject + item.data.ID);
                        });
                        var netobj = ids.join(",");
                        location.href = '/Orion/SRM/Admin/EditProperties.aspx?NetObject=' + netobj + "&ReturnTo=" + $('#ReturnToUrl').val();
                    }
                }, '-', {
                    id: 'UnmanageButton',
                    text: '@{R=SRM.Strings;K=StorageObjects_UnmanageLabel;E=js}',
                    tooltip: '@{R=SRM.Strings;K=StorageObjects_UnmanageLabel;E=js}',
                    icon: '/Orion/SRM/images/bg_button_pause_icon.png',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        var ids = new Array();
                        Ext.each(grid.getSelectionModel().selections.items, function (item) {
                            ids.push(item.data.ID);
                        });
                        showUnmanageDialog(ids);

                    }
                }, '-', {
                    id: 'RemanageButton',
                    text: '@{R=SRM.Strings;K=StorageObjects_RemanageLabel;E=js}',
                    tooltip: '@{R=SRM.Strings;K=StorageObjects_RemanageLabel;E=js}',
                    icon: '/Orion/Nodes/images/icons/icon_remanage.gif',
                    handler: function () {
                        var ids = new Array();
                        Ext.each(grid.getSelectionModel().selections.items, function (item) {
                            ids.push(item.data.ID);
                        });
                        remanageStorageArrays(ids);
                    }
                }, '-', comboAction
                        , '-', {
                            id: 'DeleteButton',
                            text: '@{R=SRM.Strings;K=StorageObjects_DeleteLabel;E=js}',
                            tooltip: '@{R=SRM.Strings;K=StorageObjects_DeleteLabel;E=js}',
                            iconCls: 'deleteContainer',
                            handler: function () {
                                Ext.Msg.confirm("@{R=SRM.Strings;K=StorageObjects_ConfirmWindowTitle;E=js}",
                                    "@{R=SRM.Strings;K=StorageObjects_ConfirmWindowText;E=js}",
                                    function (btn, text) {
                                        if (btn == 'yes') {
                                            DeleteSelectedItems(grid.getSelectionModel().selections.items);
                                        }
                                    });
                            }
                        },
                        '->',
                         new Ext.ux.form.SearchField({
                             store: dataStore,
                             width: 200
                         })
                ]
            });

            var gridColumnsModel = [
                            selectorModel,
                            { id: 'Name', header: '@{R=SRM.Strings;K=StorageObjects_NameColumnLabel;E=js}', width: 200, hideable: false, dataIndex: 'Name', sortable: true, renderer: renderName },
                            { id: 'IPAddress', header: '@{R=SRM.Strings;K=StorageObjects_PollingIPaddress;E=js}', width: 200, sortable: true, dataIndex: 'IPAddress', hidden: false, renderer: renderString },
                            { id: 'DataProvider', header: '@{R=SRM.Strings;K=StorageObjects_DataProvider;E=js}', width: 200, sortable: true, dataIndex: 'DataProvider', hidden: false, renderer: renderString },
                            { id: 'Status', header: '@{R=SRM.Strings;K=StorageObjects_Status;E=js}', width: 200, hidden: false, sortable: false, dataIndex: 'Status', renderer: renderStatus },
                            { id: 'PollingEngine', header: '@{R=SRM.Strings;K=StorageObjects_PollingEngine;E=js}', width: 200, sortable: true, dataIndex: 'PollingEngine', hidden: false, renderer: renderString }
            ];

            grid = new Ext.grid.GridPanel({
                id: 'myGridPanel',
                columns: gridColumnsModel,
                store: dataStore,
                baseParams: { start: 0, limit: userPageSize },
                sm: selectorModel,
                height: gridHeight,
                layout: 'fit',
                width: gridWidth,
                autoscroll: true,
                stripeRows: true,
                style: { overflow: 'visible' },
                tbar: toolbar,
                bbar: pagingToolbar,
                viewConfig: {
                    autoFill: true,
                    forceFit: true
                }
            });

            grid.render('Grid');
            RefreshObjects();
            updateToolbarButtons();
        }
    };
    return result;
}();
