﻿SW = SW || {};
SW.SRM = SW.SRM || {};
SW.SRM.Common = SW.SRM.Common || {};

SW.SRM.StorageObjects.Credentials = function () {
    var gridCredentials;
    var gridCredentialWidth = 1290;
    var gridCredentialHeight = 610;
    var userCredentialPageSize;
    var selectorCredentialModel;
    var dataStoreCredentials;
    var pageCredentialSizeBox;
    var filterCredentialField = "1=1";
    var loadingMaskCredential = null;
    var defaultHttpPort = 80;
    var defaultHttpsPort = 443;
    var defaultNimbleGenericHttpPort = 5392;
    var defaultIsilonAPIPort = 8080;

    var refreshCredentialGrid = function () {
        gridCredentials.store.removeAll();
        if (loadingMaskCredential == null) {
            loadingMaskCredential = new Ext.LoadMask(gridCredentials.el, {
                msg: "@{R=Core.Strings;K=WEBJS_TM0_1;E=js}"
            });
        }

        gridCredentials.store.on('beforeload', function () { loadingMaskCredential.show(); });

        var tmpfilterText = filterText.replace(/\*/g, "%");

        if (tmpfilterText.length > 0) {
            if (!tmpfilterText.match("^%"))
                tmpfilterText = "%" + tmpfilterText;

            if (!tmpfilterText.match("%$"))
                tmpfilterText = tmpfilterText + "%";
        }

        gridCredentials.store.proxy.conn.jsonData = { property: filterCredentialField, type: "", value: "", search: tmpfilterText };
        gridCredentials.store.on('load', function () {
            loadingMaskCredential.hide();
        });
        gridCredentials.store.load({ params: { start: 0, limit: userCredentialPageSize } });
    };

    var updateToolbarCredentialButtons = function (value) {

        var map = gridCredentials.getTopToolbar().items.map;
        var selCount = gridCredentials.getSelectionModel().getSelections().length;
        map.DeleteCredential.setDisabled(selCount == 0);
        map.EditCredential.setDisabled(selCount == 0 || selCount > 1); //credentials can be edited only one by time
        var items = gridCredentials.getSelectionModel().selections.items;
        for (var i = 0; i < items.length; i++) {
            if (items[i].data.UsageCount != 0) {
                map.DeleteCredential.setDisabled(true);
                break;
            }
        }
    };

    function encodeHTML(value) {
        return Ext.util.Format.htmlEncode(value);
    }
    function renderColumns(value, meta, record) {

        if (!value || value.length === 0)
            return value;

        if (!filterText || filterText.length === 0)
            return encodeHTML(value);

        var patternText = filterText;

        // replace any %'s with a *
        patternText = patternText.replace(/\%/g, "*");

        // replace multiple *'s with a single instance
        patternText = patternText.replace(/\*{2,}/g, "*");

        // check if the search string is now just down to a single *, and if so return without any further regex
        if (patternText == '*')
        { return '<span style=\"background-color: #FFE89E\">' + encodeHTML(value) + '</span>'; }

        // replace \ with \\
        patternText = patternText.replace(/\\/g, '\\\\');
        // replace . with \.
        patternText = patternText.replace(/\./g, '\\.');
        // replace * with .*
        patternText = patternText.replace(/\*/g, '.*');

        // set regex pattern
        var x = '((' + patternText + ')+)\*';
        var content = value, pattern = new RegExp(x, "gi"), replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

        // do regex replace + content gets encoded
        var fieldValue = encodeHTML(content.replace(pattern, replaceWith));

        // now replace the literal SPAN markers with the HTML span tags
        fieldValue = fieldValue.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
        fieldValue = fieldValue.replace(/{SPAN-END-MARKER}/g, '</span>');

        return fieldValue;
    };
    function deleteCredentials(items) {
        var waitMsg = Ext.Msg.wait("@{R=Core.Strings;K=WEBJS_TM0_2;E=js}");

        var ids = new Array();
        Ext.each(items, function (item) {
            ids.push(item.data.ID);
        });

        ORION.callWebService("/Orion/SRM/Services/StorageManagementService.asmx",
                 "DeleteCredentials", { credentialIds: ids },
                 function () {
                     waitMsg.hide();
                     refreshCredentialGrid();
                 },
                function (errorMessage) {
                    Ext.Msg.show({
                        title: '@{R=SRM.Strings;K=StorageObjects_ErrorDeleteCredentialTitle; E=js}',
                        msg: '@{R=SRM.Strings;K=StorageObjects_ErrorDeleteCredentialTitle; E=js}' + '. ' + errorMessage,
                        icon: Ext.Msg.ERROR,
                        buttons: Ext.Msg.OK
                    });

                    refreshCredentialGrid();
                    updateToolbarCredentialButtons();
                });

    }
    function areCredentialsValid(password, confirm) {
        if (!(password === confirm) || $.trim(String(password)) == '') {
            return false;
        }
        return true;
    };
    function isFieldValid(value) {
        if ($.trim(String(value)) == '') {
            return false;
        }
        return true;
    };
    function isPortNumberValid(portNumber) {
        var httpsPortInt = parseInt(portNumber);
        if (!$.isNumeric(httpsPortInt) || Math.floor(httpsPortInt) !== httpsPortInt || httpsPortInt < 1 || httpsPortInt > 65535) {
            return false;
        }
        return true;
    }
    function validateAll(port, port2, fields, name, id, password, confPassword, credType) {
        var error = "";
        if (port != "") {
            if (!isPortNumberValid(port))
                error = "@{R=SRM.Strings;K=StorageObjects_NotValidPort_Validator; E=js}";
        }
        if (port2 != "") {
            if (!isPortNumberValid(port2))
                error = "@{R=SRM.Strings;K=StorageObjects_NotValidPort_Validator; E=js}";
        }
        for (var i = 0; i < fields.length; i++) {
            if (!isFieldValid(fields[i].value)) {
                error = "@{R=SRM.Strings;K=StorageObjects_AllFieldsWithStar; E=js}";
            }
        }

        dataStoreCredentials.each(function (rec) {
            if ((rec.get("CredentialName")) == name && rec.get("CredentialType") == credType && (rec.get("ID") != id)) {
                error = "@{R=SRM.Strings;K=StorageObjects_DisplayNameExists; E=js}";
            }
        });

        if (password != "") {
            if (!areCredentialsValid(password, confPassword))
                error = "@{R=SRM.Strings;K=StorageObjects_CredentialsPasswordDoNotMatch;E=js}";
        }
        return error;
    }

    var addDialogSnmp;
    function addSnmpCredentials() {
        $("#addDialogSnmpVerOneBody .Error").text('');
        if (!addDialogSnmp) {
            addDialogSnmp = new Ext.Window({
                applyTo: 'addDialogSnmpVerOne',
                autoWidth: true,
                autoHeight: true,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'addDialogSnmpVerOneBody'
                }),
                buttons: [
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialOkButton; E=js}',
                    handler: function () {
                        var displayName = $("#CredentialNameTextBox").val();
                        var community = $("#SnmpCommunityTextBox").val();
                        var port = $("#SnmpPortTextBoxV2").val();
                        var credId = $("#snmpCredId").val();
                        $("#addDialogSnmpVerOneBody .Error").text("");
                        var error = "";
                        var fields = $(".checkEmpty");

                        error = validateAll(port, "", fields, displayName, credId, "", "", "SolarWinds.SRM.Common.Credentials.SrmSnmpCredentialsV2");

                        if (error == "") {
                            ORION.callWebService("/Orion/SRM/Services/StorageManagementService.asmx", "CreateOrUpdateSnmpCredentials", {
                                credId: credId,
                                name: displayName,
                                community: community,
                                port: parseInt(port)
                            }, function () {
                                refreshCredentialGrid();
                                addDialogSnmp.hide();
                            });
                        } else {
                            $("#addDialogSnmpVerOneBody .Error").text(error);
                            addDialogSnmp.setHeight();
                            addDialogSnmp.doLayout();
                            addDialogSnmp.syncShadow();
                        }
                    }
                },
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialCancelButton; E=js}',
                    style: 'margin-left: 5px;',
                    handler: function () {
                        addDialogSnmp.hide();
                    }
                }]

            });
        }
        addDialogSnmp.alignTo(document.body, "c-c");
        addDialogSnmp.show();

        return false;
    }

    var addDialogSnmpv3;
    function addSnmpV3Credentials() {
        $("#addDialogSnmpVerThreeBody .Error").text('');

        if (!addDialogSnmpv3) {
            addDialogSnmpv3 = new Ext.Window({
                applyTo: 'addDialogSnmpVerThree',
                autoWidth: true,
                autoHeight: true,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'addDialogSnmpVerThreeBody'
                }),
                buttons: [
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialOkButton; E=js}',
                    handler: function () {
                        $("#addDialogSnmpVerThreeBody .Error").text("");
                        var credId = $("#snmpV3credId").val();
                        var port = $("#SnmpPortTextBoxV3").val();
                        var name = $("#SnmpV3CredentialNameTextBox").val();
                        var userName = $("#SnmpUserNameTextBox").val();
                        var context = $("#SnmpContextTextBox").val();
                        var fields = $(".checkEmptySnmpV3");
                        var authenticationType = $("#AuthenticationMethodDropDown  option:selected").val();
                        var privacyType = $("#EncryptionMethodDropDown  option:selected").val();
                        var authenticationPassword = $("#PasswordKey1TextBox").val();
                        var authenticationKeyIsPassword = $("#PasswordKey1CheckBox").is(':checked');
                        var privacyPassword = $("#PasswordKey2TextBox").val();
                        var privacyKeyIsPassword = $("#PasswordKey2CheckBox").is(':checked');

                        var error = validateAll(port, "", fields, name, credId, "", "", "SolarWinds.SRM.Common.Credentials.SrmSnmpCredentialsV3");

                        if (error == "") {
                            ORION.callWebService("/Orion/SRM/Services/StorageManagementService.asmx", "CreateOrUpdateSnmpV3Credentials", {
                                credId: credId,
                                name: name,
                                userName: userName,
                                context: context,
                                authenticationType: authenticationType,
                                privacyType: privacyType,
                                authenticationPassword: authenticationPassword,
                                authenticationKeyIsPassword: authenticationKeyIsPassword,
                                privacyPassword: privacyPassword,
                                privacyKeyIsPassword: privacyKeyIsPassword,
                                port: parseInt(port)
                            }, function () {
                                refreshCredentialGrid();
                                addDialogSnmpv3.hide();
                            });
                        } else {
                            $("#addDialogSnmpVerThreeBody .Error").text(error);
                            addDialogSnmpv3.setHeight();
                            addDialogSnmpv3.doLayout();
                            addDialogSnmpv3.syncShadow();
                        }

                    }
                },
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialCancelButton; E=js}',
                    style: 'margin-left: 5px;',
                    handler: function () {
                        addDialogSnmpv3.hide();
                    }
                }]

            });
        }
        addDialogSnmpv3.alignTo(document.body, "c-c");
        addDialogSnmpv3.show();

        return false;
    }

    var addDialogSmis;
    function addSmisCredentials() {
        $("#addDialogSmisBody .Error").text('');

        if (!addDialogSmis) {
            addDialogSmis = new Ext.Window({
                applyTo: 'addDialogSmis',
                autoWidth: true,
                autoHeight: true,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'addDialogSmisBody'
                }),
                buttons: [
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialOkButton; E=js}',
                    handler: function () {
                        var credId = $("#smisCredId").val();
                        var name = $("#DisplayNameSmisTextBox").val();
                        var userName = $("#ProviderUsernameSmisTextBox").val();
                        var password = $("#ProviderPasswordSmisTextBox").val();
                        var confPassword = $("#ProviderConfirmPasswordSmisTextBox").val();
                        var useSsl = !$("#ProtocolSmisCheckBox").is(':checked');
                        var httpsPort = $("#HttpsPortSmisTextBox").val();
                        var httpPort = $("#HttpPortSmisTextBox").val();
                        var interopNamespace = $("#InteropNamespaceSmisTextBox").val();
                        var arrayNamespace = $("#ArrayNamespaceSmisTextBox").val();
                        var fields = $(".checkEmptySmsi");
                        var changePassword = $("#ChangePasswordCheckBox").is(':checked');

                        $("#addDialogSmisBody .Error").text("");
                        var error = validateAll(httpsPort, httpPort, fields, name, credId, password, confPassword, "SolarWinds.SRM.Common.Credentials.SmisCredentials");

                        if (error == "") {
                            ORION.callWebService("/Orion/SRM/Services/StorageManagementService.asmx", "CreateOrUpdateSmsiCredentials", {
                                credId: credId,
                                name: name,
                                userName: userName,
                                password: password,
                                useSsl: useSsl,
                                httpsPort: parseInt(httpsPort),
                                httpPort: parseInt(httpPort),
                                interopNamespace: interopNamespace,
                                arrayNamespace: arrayNamespace,
                                changePassword: changePassword
                            }, function () {
                                refreshCredentialGrid();
                                addDialogSmis.hide();
                            });
                        } else {
                            $("#addDialogSmisBody .Error").text(error);
                            addDialogSmis.setHeight();
                            addDialogSmis.doLayout();
                            addDialogSmis.syncShadow();
                        }
                    }
                },
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialCancelButton; E=js}',
                    style: 'margin-left: 5px;',
                    handler: function () {
                        addDialogSmis.hide();
                    }
                }]

            });
        }
        addDialogSmis.alignTo(document.body, "c-c");
        addDialogSmis.show();

        return false;
    }

    var addDialogNetAppFiler;
    function addNetAppFilerCredentials() {
        $("#addDialogNetAppFilerBody .Error").text('');

        if (!addDialogNetAppFiler) {
            addDialogNetAppFiler = new Ext.Window({
                applyTo: 'addDialogNetAppFiler',
                autoWidth: true,
                autoHeight: true,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'addDialogNetAppFilerBody'
                }),
                buttons: [
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialOkButton; E=js}',
                    handler: function () {
                        var divName = $("#addDialogNetAppFilerBody");
                        var credId = divName.find("#netAppFilerCredId").val();
                        var displayName = divName.find("#FilerCredentialNameTextBox").val();
                        var userName = divName.find("#FilerUserNameTextBox").val();
                        var password = divName.find("#FilerPasswordTextBox").val();
                        var confPassword = divName.find("#FilerConfirmPasswordTextBox").val();
                        var useSsl = divName.find("#FilerProtocolCheckBox").is(':checked');

                        divName.find(".Error").text("");
                        var error = "";
                        var fields = divName.find(".checkEmptyFiler");

                        error = validateAll("", "", fields, displayName, credId, password, confPassword, "SolarWinds.SRM.Common.Credentials.NetAppOntapCredentials");
                        var changePassword = divName.find("#ChangePasswordFilerCheckBox").is(':checked');
                        if (error == "") {
                            ORION.callWebService("/Orion/SRM/Services/StorageManagementService.asmx", "CreateOrUpdateNetAppFilerCredentials",
                                {
                                    credId: credId,
                                    name: displayName,
                                    userName: userName,
                                    password: password,
                                    useSsl: useSsl,
                                    changePassword: changePassword
                                }, function () {
                                    refreshCredentialGrid();
                                    addDialogNetAppFiler.hide();
                                });
                        } else {
                            $("#addDialogNetAppFilerBody .Error").text(error);
                            addDialogNetAppFiler.setHeight();
                            addDialogNetAppFiler.doLayout();
                            addDialogNetAppFiler.syncShadow();
                        }

                    }
                },
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialCancelButton; E=js}',
                    style: 'margin-left: 5px;',
                    handler: function () {
                        addDialogNetAppFiler.hide();
                    }
                }]

            });
        }
        addDialogNetAppFiler.alignTo(document.body, "c-c");
        addDialogNetAppFiler.show();

        return false;
    }

    var addDialogEmcUnity;
    function addEmcUnityCredentials() {
        $("#addDialogEmcUnityBody .Error").text('');

        if (!addDialogEmcUnity) {
            addDialogEmcUnity = new Ext.Window({
                applyTo: 'addDialogEmcUnity',
                autoWidth: true,
                autoHeight: true,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'addDialogEmcUnityBody'
                }),
                buttons: [
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialOkButton; E=js}',
                    handler: function () {
                        var divName = $("#addDialogEmcUnityBody");
                        var credId = divName.find("#netAppFilerCredId").val();
                        var displayName = divName.find("#FilerCredentialNameTextBox").val();
                        var userName = divName.find("#FilerUserNameTextBox").val();
                        var password = divName.find("#FilerPasswordTextBox").val();
                        var confPassword = divName.find("#FilerConfirmPasswordTextBox").val();
                        var useSsl = divName.find("#FilerProtocolCheckBox").is(':checked');
                        divName.find(".Error").text("");
                        var error = "";
                        var fields = divName.find(".checkEmptyFiler");

                        error = validateAll("", "", fields, displayName, credId, password, confPassword, "SolarWinds.SRM.Common.Credentials.EmcUnityHttpCredential");
                        var changePassword = divName.find("#ChangePasswordFilerCheckBox").is(':checked');
                        if (error == "") {
                            ORION.callWebService("/Orion/SRM/Services/StorageManagementService.asmx", "CreateOrUpdateEmcUnityCredentials",
                                {
                                    credId: credId,
                                    name: displayName,
                                    userName: userName,
                                    password: password,
                                    useSsl: useSsl,
                                    changePassword: changePassword
                                }, function () {
                                    refreshCredentialGrid();
                                    addDialogEmcUnity.hide();
                                });
                        } else {
                            $("#addDialogEmcUnityBody .Error").text(error);
                            addDialogEmcUnity.setHeight();
                            addDialogEmcUnity.doLayout();
                            addDialogEmcUnity.syncShadow();
                        }

                    }
                },
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialCancelButton; E=js}',
                    style: 'margin-left: 5px;',
                    handler: function () {
                        addDialogEmcUnity.hide();
                    }
                }]

            });
        }
        addDialogEmcUnity.alignTo(document.body, "c-c");
        addDialogEmcUnity.show();

        return false;
    }

    var addDialogGenericHttp;
    function addGenericHttpCredentials() {
        $("#addDialogGenericHttpBody .Error").text('');

        if (!addDialogGenericHttp) {
            addDialogGenericHttp = new Ext.Window({
                applyTo: 'addDialogGenericHttp',
                autoWidth: true,
                autoHeight: true,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'addDialogGenericHttpBody'
                }),
                buttons: [
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialOkButton; E=js}',
                    handler: function () {
                        var divName = $("#addDialogGenericHttpBody");

                        var credId = divName.find("#genericHttpCredId").val();
                        var displayName = divName.find("#DisplayNameApiTextBox").val();
                        var userName = divName.find("#ArrayApiUsernameTextBox").val();
                        var password = divName.find("#ArrayApiPasswordTextBox").val();
                        var confPassword = divName.find("#ArrayApiConfirmPasswordTextBox").val();
                        var useSsl = !divName.find("#ArrayApiIsHttpCheckbox").is(':checked');
                        var portApi = divName.find("#ArrayApiPortTextBox").val();

                        divName.find(".Error").text("");
                        var error = "";
                        var fields = divName.find(".checkEmptyGenericHttp");

                        error = validateAll(portApi, "", fields, displayName, credId, password, confPassword, "SolarWinds.SRM.Common.Credentials.GenericHttpCredential");
                        var changePassword = divName.find("#ChangePasswordCheckBox").is(':checked');
                        if (error == "") {
                            ORION.callWebService("/Orion/SRM/Services/StorageManagementService.asmx", "CreateOrUpdateGenericCredentials",
                                {
                                    credId: credId,
                                    name: displayName,
                                    userName: userName,
                                    password: password,
                                    useSsl: useSsl,
                                    port: parseInt(portApi),
                                    changePassword: changePassword
                                }, function () {
                                    refreshCredentialGrid();
                                    addDialogGenericHttp.hide();
                                });
                        } else {
                            $("#addDialogGenericHttpBody .Error").text(error);
                            addDialogGenericHttp.setHeight();
                            addDialogGenericHttp.doLayout();
                            addDialogGenericHttp.syncShadow();
                        }

                    }
                },
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialCancelButton; E=js}',
                    style: 'margin-left: 5px;',
                    handler: function () {
                        addDialogGenericHttp.hide();
                    }
                }]

            });
        }
        addDialogGenericHttp.alignTo(document.body, "c-c");
        addDialogGenericHttp.show();

        return false;
    }

    var addDialogInfiniBox;
    function addInfiniBoxCredentials() {
        $("#addDialogInfiniBoxBody .Error").text('');

        if (!addDialogInfiniBox) {
            addDialogInfiniBox = new Ext.Window({
                applyTo: 'addDialogInfiniBox',
                autoWidth: true,
                autoHeight: true,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'addDialogInfiniBoxBody'
                }),
                buttons: [
                    {
                        text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialOkButton; E=js}',
                        handler: function () {
                            var divName = $("#addDialogInfiniBoxBody");
                            var credId = divName.find("#netAppFilerCredId").val();
                            var displayName = divName.find("#FilerCredentialNameTextBox").val();
                            var userName = divName.find("#FilerUserNameTextBox").val();
                            var password = divName.find("#FilerPasswordTextBox").val();
                            var confPassword = divName.find("#FilerConfirmPasswordTextBox").val();
                            var useSsl = divName.find("#FilerProtocolCheckBox").is(':checked');
                            divName.find(".Error").text("");
                            var error = "";
                            var fields = divName.find(".checkEmptyFiler");

                            error = validateAll("", "", fields, displayName, credId, password, confPassword, "SolarWinds.SRM.Common.Credentials.InfinidatHttpCredential");
                            var changePassword = divName.find("#ChangePasswordFilerCheckBox").is(':checked');
                            if (error == "") {
                                ORION.callWebService("/Orion/SRM/Services/StorageManagementService.asmx", "CreateOrUpdateInfiniBoxCredentials",
                                    {
                                        credId: credId,
                                        name: displayName,
                                        userName: userName,
                                        password: password,
                                        useSsl: useSsl,
                                        changePassword: changePassword
                                    }, function () {
                                        refreshCredentialGrid();
                                        addDialogInfiniBox.hide();
                                    });
                            } else {
                                $("#addDialogInfiniBoxBody .Error").text(error);
                                addDialogInfiniBox.setHeight();
                                addDialogInfiniBox.doLayout();
                                addDialogInfiniBox.syncShadow();
                            }

                        }
                    },
                    {
                        text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialCancelButton; E=js}',
                        style: 'margin-left: 5px;',
                        handler: function () {
                            addDialogInfiniBox.hide();
                        }
                    }]

            });
        }
        addDialogInfiniBox.alignTo(document.body, "c-c");
        addDialogInfiniBox.show();

        return false;
    }

    var addDialogDfm;
    function addDfmCredentials() {
        $("#addDialogDfmBody .Error").text('');

        if (!addDialogDfm) {
            addDialogDfm = new Ext.Window({
                applyTo: 'addDialogDfm',
                autoWidth: true,
                autoHeight: true,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'addDialogDfmBody'
                }),
                buttons: [
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialOkButton; E=js}',
                    handler: function () {
                        var credId = $("#dfmCredId").val();
                        var displayName = $("#DisplayNameDfmTextBox").val();
                        var userName = $("#ProviderUsernameDfmTextBox").val();
                        var password = $("#ProviderPasswordDfmTextBox").val();
                        var confPassword = $("#ProviderConfirmPasswordDfmTextBox").val();
                        var useSsl = !$("#ProtocolDfmCheckBox").is(':checked');
                        var httpsPort = $("#HttpsPortDfmTextBox").val();
                        var httpPort = $("#HttpPortDfmTextBox").val();

                        $("#addDialogDfmBody .Error").text("");
                        var error = "";
                        var fields = $(".checkEmptyDfm");
                        error = validateAll(httpsPort, httpPort, fields, displayName, credId, password, confPassword, "SolarWinds.SRM.Common.Credentials.NetAppDfmCredentials");
                        var changePassword = $("#ChangePasswordDfmCheckBox").is(':checked');
                        if (error == "") {
                            ORION.callWebService("/Orion/SRM/Services/StorageManagementService.asmx", "CreateOrUpdateDfmCredentials",
                                {
                                    credId: credId,
                                    name: displayName,
                                    userName: userName,
                                    password: password,
                                    useSsl: useSsl,
                                    httpsPort: parseInt(httpsPort),
                                    httpPort: parseInt(httpPort),
                                    changePassword: changePassword
                                }, function () {
                                    refreshCredentialGrid();
                                    addDialogDfm.hide();
                                });
                        } else {
                            $("#addDialogDfmBody .Error").text(error);
                            addDialogDfm.setHeight();
                            addDialogDfm.doLayout();
                            addDialogDfm.syncShadow();
                        }

                    }
                },
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialCancelButton; E=js}',
                    style: 'margin-left: 5px;',
                    handler: function () {
                        addDialogDfm.hide();
                    }
                }]

            });
        }
        addDialogDfm.alignTo(document.body, "c-c");
        addDialogDfm.show();

        return false;
    }

    var addDialogVnx;
    function addVnxCredentials() {
        $("#addDialogVnxBody .Error").text('');

        if (!addDialogVnx) {
            addDialogVnx = new Ext.Window({
                applyTo: 'addDialogVnx',
                autoWidth: true,
                autoHeight: true,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'addDialogVnxBody'
                }),
                buttons: [
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialOkButton; E=js}',
                    handler: function () {
                        var fields = $(".checkEmptyVnx");
                        var error = "";
                        var credId = $("#smsiVnxCredId").val();
                        var nameApi = $("#DisplayNameApiTextBox").val();
                        var userNameApi = $("#ArrayApiUsernameTextBox").val();
                        var passwordApi = $("#ArrayApiPasswordTextBox").val();
                        var confPasswordApi = $("#ArrayApiConfirmPasswordTextBox").val();
                        var pathApi = $("#ArrayApiManagementServicesPathTextBox").val();
                        var portApi = $("#ArrayApiVnxPortTextBox").val();

                        var changePassword = $("#ChangePasswordVnxCheckBox").is(':checked');
                        error = validateAll(portApi, "", fields, nameApi, credId, passwordApi, confPasswordApi, "SolarWinds.SRM.Common.Credentials.VnxCredential");
                        if (error == "") {
                            ORION.callWebService("/Orion/SRM/Services/StorageManagementService.asmx", "CreateOrUpdateVnxCredentials", {
                                credId: credId,
                                name: nameApi,
                                userName: userNameApi,
                                password: passwordApi,
                                path: pathApi,
                                port: parseInt(portApi),
                                changePassword: changePassword
                            }, function () {
                                refreshCredentialGrid();
                                addDialogVnx.hide();
                            });
                        } else {
                            $("#addDialogVnxBody .Error").text(error);
                            addDialogVnx.setHeight(340);
                            addDialogVnx.doLayout();
                            addDialogVnx.syncShadow();
                        }
                    }
                },
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialCancelButton; E=js}',
                    style: 'margin-left: 5px;',
                    handler: function () {
                        addDialogVnx.hide();
                    }
                }]

            });
        }
        addDialogVnx.alignTo(document.body, "c-c");
        addDialogVnx.show();

        return false;
    }

    var addDialogMsa;
    function addMsaCredentials() {
        $("#addDialogMsaBody .Error").text('');

        if (!addDialogMsa) {
            addDialogMsa = new Ext.Window({
                applyTo: 'addDialogMsa',
                autoWidth: true,
                autoHeight: true,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'addDialogMsaBody'
                }),
                buttons: [
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialOkButton; E=js}',
                    handler: function () {
                        var divName = $("#addDialogMsaBody");
                        var fields = divName.find(".checkEmptyMsa");
                        var error = "";
                        var credId = divName.find("#smsiMsaCredId").val();
                        var nameApi = divName.find("#MsaDisplayNameApiTextBox").val();
                        var userNameApi = divName.find("#MsaArrayApiUsernameTextBox").val();
                        var passwordApi = divName.find("#MsaArrayApiPasswordTextBox").val();
                        var confPasswordApi = divName.find("#MsaArrayApiConfirmPasswordTextBox").val();
                        var isHttp = divName.find("#ArrayApiIsHttpCheckbox").is(':checked');
                        var portApi = divName.find("#ArrayApiMsaPortTextBox").val();
                        var httpsPortApi = divName.find("#ArrayApiMsaHttpsPortTextBox").val();
                        var changePassword = divName.find("#ChangePasswordMsaCheckBox").is(':checked');
                        error = validateAll(portApi, "", fields, nameApi, credId, passwordApi, confPasswordApi, "SolarWinds.SRM.Common.Credentials.MsaCredential");
                        if (error == "") {
                            ORION.callWebService("/Orion/SRM/Services/StorageManagementService.asmx", "CreateOrUpdateMsaCredentials", {
                                credId: credId,
                                name: nameApi,
                                userName: userNameApi,
                                password: passwordApi,
                                isHttp: isHttp,
                                httpPort: parseInt(portApi),
                                httpsPort: parseInt(httpsPortApi),
                                changePassword: changePassword
                            }, function () {
                                refreshCredentialGrid();
                                addDialogMsa.hide();
                            });
                        } else {
                            $("#addDialogMsaBody .Error").text(error);
                            addDialogMsa.setHeight();
                            addDialogMsa.doLayout();
                            addDialogMsa.syncShadow();
                        }
                    }
                },
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialCancelButton; E=js}',
                    style: 'margin-left: 5px;',
                    handler: function () {
                        addDialogMsa.hide();
                    }
                }]

            });
        }
        addDialogMsa.alignTo(document.body, "c-c");
        addDialogMsa.show();

        return false;

    }

    var addDialogIsilon;
    function addIsilonCredentials() {
        $("#addDialogIsilonBody .Error").text('');

        if (!addDialogIsilon) {
            addDialogIsilon = new Ext.Window({
                applyTo: 'addDialogIsilon',
                autoWidth: true,
                autoHeight: true,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'addDialogIsilonBody'
                }),
                buttons: [
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialOkButton; E=js}',
                    handler: function () {
                        var divName = $("#addDialogIsilonBody");
                        var fields = divName.find(".checkEmptyMsa");
                        var error = "";
                        var credId = divName.find("#smsiMsaCredId").val();
                        var nameApi = divName.find("#MsaDisplayNameApiTextBox").val();
                        var userNameApi = divName.find("#MsaArrayApiUsernameTextBox").val();
                        var passwordApi = divName.find("#MsaArrayApiPasswordTextBox").val();
                        var confPasswordApi = divName.find("#MsaArrayApiConfirmPasswordTextBox").val();
                        var isHttp = divName.find("#ArrayApiIsHttpCheckbox").is(':checked');
                        var portApi = divName.find("#ArrayApiMsaPortTextBox").val();
                        var changePassword = divName.find("#ChangePasswordMsaCheckBox").is(':checked');
                        error = validateAll(portApi, "", fields, nameApi, credId, passwordApi, confPasswordApi, "SolarWinds.SRM.Common.Credentials.IsilonHttpCredential");
                        if (error == "") {
                            ORION.callWebService("/Orion/SRM/Services/StorageManagementService.asmx", "CreateOrUpdateIsilonCredentials", {
                                credId: credId,
                                name: nameApi,
                                userName: userNameApi,
                                password: passwordApi,
                                isHttp: isHttp,
                                port: parseInt(portApi),
                                changePassword: changePassword
                            }, function () {
                                refreshCredentialGrid();
                                addDialogIsilon.hide();
                            });
                        } else {
                            $("#addDialogIsilonBody .Error").text(error);
                            addDialogIsilon.setHeight();
                            addDialogIsilon.doLayout();
                            addDialogIsilon.syncShadow();
                        }
                    }
                },
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialCancelButton; E=js}',
                    style: 'margin-left: 5px;',
                    handler: function () {
                        addDialogIsilon.hide();
                    }
                }]

            });
        }
        addDialogIsilon.alignTo(document.body, "c-c");
        addDialogIsilon.show();

        return false;

    }

    var addDialogPureStorage;
    function addPureStorageCredentials() {
        $("#addDialogPureStorageBody .Error").text('');

        if (!addDialogPureStorage) {
            addDialogPureStorage = new Ext.Window({
                applyTo: 'addDialogPureStorage',
                autoWidth: true,
                autoHeight: true,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'addDialogPureStorageBody'
                }),
                buttons: [
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialOkButton; E=js}',
                    handler: function () {
                        var divName = $("#addDialogPureStorageBody");
                        var fields = divName.find(".checkEmptyMsa");
                        var error = "";
                        var credId = divName.find("#smsiMsaCredId").val();
                        var nameApi = divName.find("#MsaDisplayNameApiTextBox").val();
                        var userNameApi = divName.find("#MsaArrayApiUsernameTextBox").val();
                        var passwordApi = divName.find("#MsaArrayApiPasswordTextBox").val();
                        var confPasswordApi = divName.find("#MsaArrayApiConfirmPasswordTextBox").val();
                        var isHttp = divName.find("#ArrayApiIsHttpCheckbox").is(':checked');
                        var portApi = divName.find("#ArrayApiMsaPortTextBox").val();
                        var httpsPortApi = divName.find("#ArrayApiMsaHttpsPortTextBox").val();
                        
                        var changePassword = divName.find("#ChangePasswordMsaCheckBox").is(':checked');
                        error = validateAll(portApi, "", fields, nameApi, credId, passwordApi, confPasswordApi, "SolarWinds.SRM.Common.Credentials.PureStorageHttpCredential");
                        if (error == "") {
                            ORION.callWebService("/Orion/SRM/Services/StorageManagementService.asmx", "CreateOrUpdatePureStorageCredentials", {
                                credId: credId,
                                name: nameApi,
                                userName: userNameApi,
                                password: passwordApi,
                                isHttp: isHttp,
                                httpPort: parseInt(portApi),
                                httpsPort: parseInt(httpsPortApi),
                                changePassword: changePassword
                            }, function () {
                                refreshCredentialGrid();
                                addDialogPureStorage.hide();
                            });
                        } else {
                            $("#addDialogPureStorageBody .Error").text(error);
                            addDialogPureStorage.setHeight();
                            addDialogPureStorage.doLayout();
                            addDialogPureStorage.syncShadow();
                        }
                    }
                },
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialCancelButton; E=js}',
                    style: 'margin-left: 5px;',
                    handler: function () {
                        addDialogPureStorage.hide();
                    }
                }]

            });
        }
        addDialogPureStorage.alignTo(document.body, "c-c");
        addDialogPureStorage.show();

        return false;

    }

    var addDialogXtremIo;
    function addXtremIoCredentials() {
        $("#addDialogXtremIOBody").text('');
        $(".Error").text('');

        if (!addDialogXtremIo) {
            addDialogXtremIo = new Ext.Window({
                applyTo: 'addDialogXtremIo',
                autoWidth: true,
                autoHeight: true,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'addDialogXtremIoBody'
                }),
                buttons: [
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialOkButton; E=js}',
                    handler: function () {
                        var divName = $("#addDialogXtremIoBody");
                        var fields = divName.find(".checkEmptyMsa");
                        var error = "";
                        var credId = divName.find("#smsiMsaCredId").val();
                        var nameApi = divName.find("#MsaDisplayNameApiTextBox").val();
                        var userNameApi = divName.find("#MsaArrayApiUsernameTextBox").val();
                        var passwordApi = divName.find("#MsaArrayApiPasswordTextBox").val();
                        var confPasswordApi = divName.find("#MsaArrayApiConfirmPasswordTextBox").val();
                        var isHttp = divName.find("#ArrayApiIsHttpCheckbox").is(':checked');
                        var portApi = divName.find("#ArrayApiMsaPortTextBox").val();
                        var httpsPortApi = divName.find("#ArrayApiMsaHttpsPortTextBox").val();
                        
                        var changePassword = divName.find("#ChangePasswordMsaCheckBox").is(':checked');
                        error = validateAll(portApi, "", fields, nameApi, credId, passwordApi, confPasswordApi, "SolarWinds.SRM.Common.Credentials.XtremIoHttpCredential");
                        if (error == "") {
                            ORION.callWebService("/Orion/SRM/Services/StorageManagementService.asmx", "CreateOrUpdateXtremIoCredentials", {
                                credId: credId,
                                name: nameApi,
                                userName: userNameApi,
                                password: passwordApi,
                                isHttp: isHttp,
                                httpPort: parseInt(portApi),
                                httpsPort: parseInt(httpsPortApi),
                                changePassword: changePassword
                            }, function () {
                                refreshCredentialGrid();
                                addDialogXtremIo.hide();
                            });
                        } else {
                            $("#addDialogXtremIoBody .Error").text(error);
                            addDialogXtremIo.setHeight();
                            addDialogXtremIo.doLayout();
                            addDialogXtremIo.syncShadow();
                        }
                    }
                },
                {
                    text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialCancelButton; E=js}',
                    style: 'margin-left: 5px;',
                    handler: function () {
                        addDialogXtremIo.hide();
                    }
                }]

            });
        }
        addDialogXtremIo.alignTo(document.body, "c-c");
        addDialogXtremIo.show();

        return false;

    }

    var addDialogKaminario;
    function addKaminarioCredentials() {
        $("#addDialogKaminarioBody .Error").text('');

        if (!addDialogKaminario) {
            addDialogKaminario = new Ext.Window({
                applyTo: 'addDialogKaminario',
                autoWidth: true,
                autoHeight: true,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'addDialogKaminarioBody'
                }),
                buttons: [
                    {
                        text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialOkButton; E=js}',
                        handler: function () {
                            var divName = $("#addDialogKaminarioBody");
                            var credId = divName.find("#genericHttpCredId").val();
                            var displayName = divName.find("#DisplayNameApiTextBox").val();
                            var userName = divName.find("#ArrayApiUsernameTextBox").val();
                            var password = divName.find("#ArrayApiPasswordTextBox").val();
                            var confPassword = divName.find("#ArrayApiConfirmPasswordTextBox").val();
                            var useSsl = !divName.find("#ArrayApiIsHttpCheckbox").is(':checked');
                            var portApi = divName.find("#ArrayApiPortTextBox").val();

                            divName.find(".Error").text("");
                            var error = "";
                            var fields = divName.find(".checkEmptyGenericHttp");

                            error = validateAll(portApi, "", fields, displayName, credId, password, confPassword, "SolarWinds.SRM.Common.Credentials.GenericHttpCredential");
                            var changePassword = divName.find("#ChangePasswordCheckBox").is(':checked');
                            if (error == "") {
                                ORION.callWebService("/Orion/SRM/Services/StorageManagementService.asmx", "CreateOrUpdateKaminarioCredentials", {
                                    credId: credId,
                                    name: displayName,
                                    userName: userName,
                                    password: password,
                                    useSsl: useSsl,
                                    port: parseInt(portApi),
                                    changePassword: changePassword
                                }, function () {
                                    refreshCredentialGrid();
                                    addDialogKaminario.hide();
                                });
                            } else {
                                $("#addDialogKaminarioBody .Error").text(error);
                                addDialogKaminario.setHeight();
                                addDialogKaminario.doLayout();
                                addDialogKaminario.syncShadow();
                            }
                        }
                    },
                    {
                        text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialCancelButton; E=js}',
                        style: 'margin-left: 5px;',
                        handler: function () {
                            addDialogKaminario.hide();
                        }
                    }]

            });
        }
        addDialogKaminario.alignTo(document.body, "c-c");
        addDialogKaminario.show();

        return false;

    }


    function editCredential() {
        var rec;
        gridCredentials.getSelectionModel().each(function (record) {
            rec = record;
        });

        ORION.callWebService("/Orion/SRM/Services/StorageManagementService.asmx",
                "GetCredentialsProperty", { credId: rec.data["ID"] },
                function (respond) {
                    switch (rec.data["CredentialType"]) {
                        case ("SolarWinds.SRM.Common.Credentials.SrmSnmpCredentialsV2"):
                            {
                                fillSnmp(respond, rec.data["CredentialName"]);
                                break;
                            }
                        case ("SolarWinds.SRM.Common.Credentials.SrmSnmpCredentialsV3"):
                            {
                                fillSnmpV3(respond, rec.data["CredentialName"]);
                                break;
                            }
                        case ("SolarWinds.SRM.Common.Credentials.SmisCredentials"):
                            {
                                fillSmis(respond, rec.data["CredentialName"]);
                                break;
                            }
                        case ("SolarWinds.SRM.Common.Credentials.VnxCredential"):
                            {
                                fillVnx(respond, rec.data["CredentialName"]);
                                break;
                            }
                        case ("SolarWinds.SRM.Common.Credentials.MsaCredential"):
                            {
                                fillMsa(respond, rec.data["CredentialName"]);
                                break;
                            }
                        case ("SolarWinds.SRM.Common.Credentials.NetAppOntapCredentials"):
                            {
                                fillNetAppFiler(respond, rec.data["CredentialName"]);
                                break;
                            }
                        case ("SolarWinds.SRM.Common.Credentials.NetAppDfmCredentials"):
                            {
                                fillNetAppDfm(respond, rec.data["CredentialName"]);
                                break;
                            }
                        case ("SolarWinds.SRM.Common.Credentials.IsilonHttpCredential"):
                            {
                                fillIsilon(respond, rec.data["CredentialName"]);
                                break;
                            }
                        case ("SolarWinds.SRM.Common.Credentials.PureStorageHttpCredential"):
                            {
                                fillPureStorage(respond, rec.data["CredentialName"]);
                                break;
                            }
                        case ("SolarWinds.SRM.Common.Credentials.XtremIoHttpCredential"):
                            {
                                fillXtremIo(respond, rec.data["CredentialName"]);
                                break;
                            }
                        case ("SolarWinds.SRM.Common.Credentials.EmcUnityHttpCredential"):
                            {
                                fillEmcUnity(respond, rec.data["CredentialName"]);
                                break;
                            }
                        case ("SolarWinds.SRM.Common.Credentials.InfinidatHttpCredential"):
                            {
                                fillInfiniBox(respond, rec.data["CredentialName"]);
                                break;
                            }
                        case ("SolarWinds.SRM.Common.Credentials.GenericHttpCredential"):
                            {
                                fillGenericHttp(respond, rec.data["CredentialName"]);
                                break;
                            }
                        case ("SolarWinds.SRM.Common.Credentials.GenericHttpCredential"):
                            {
                            fillKaminario(respond, rec.data["CredentialName"]);
                            break;
                            }
                    }
                },
               function (errorMessage) {
                   Ext.Msg.show({
                       title: '@{R=SRM.Strings;K=StorageObjects_ErrorWhileDownloadingData;E=js}',
                       msg: '@{R=SRM.Strings;K=StorageObjects_ErrorWhileDownloadingData;E=js}',
                       icon: Ext.Msg.ERROR,
                       buttons: Ext.Msg.OK
                   });
               });
        refreshCredentialGrid();
    }

    function fillSnmp(respond, name) {
        $("#CredentialNameTextBox").val(name);
        $("#SnmpCommunityTextBox").val(respond.Community);
        $("#SnmpPortTextBoxV2").val(respond.Port);
        $("#snmpCredId").val(respond.Id);
        addSnmpCredentials();
    }
    function fillSnmpV3(respond, name) {
        if (respond == null) {
            Ext.Msg.show({
                title: '@{R=SRM.Strings;K=ManageStorageObjects_Credentials_UnableToEditCredentials_Title;E=js}',
                msg: '@{R=SRM.Strings;K=ManageStorageObjects_Credentials_CredentialCannotBeEditedDueToFIPS;E=js}',
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK
            });
        }

        $("#snmpV3credId").val(respond.Id);
        $("#SnmpPortTextBoxV3").val(respond.Port);
        $("#SnmpV3CredentialNameTextBox").val(name);
        $("#SnmpUserNameTextBox").val(respond.Username);
        $("#SnmpContextTextBox").val(respond.Context);
        $("#AuthenticationMethodDropDown option").each(function () {
            if ($(this).html() == respond.AuthenticationType) {
                $(this).attr('selected', 'selected');
            }
        });
        $("#EncryptionMethodDropDown option").each(function () {
            if ($(this).html() == respond.PrivacyType) {
                $(this).attr('selected', 'selected');
            }
        });

        $("#PasswordKey1TextBox").val(respond.AuthenticationPassword);
        $("#PasswordKey1CheckBox").attr("checked", String(respond.AuthenticationKeyIsPassword) === 'true');
        $("#PasswordKey2TextBox").val(respond.PrivacyPassword);
        $("#PasswordKey2CheckBox").attr("checked", String(respond.PrivacyKeyIsPassword) === 'true');

        OnDropDownListChanged();
        addSnmpV3Credentials();
    }

    function fillSmis(respond, name) {
        $("#smisCredId").val(respond.Id);
        $("#DisplayNameSmisTextBox").val(name);
        $("#ProviderUsernameSmisTextBox").val(respond.Username);
        $("#ProtocolSmisCheckBox").prop("checked", String(respond.UseSSL).toLowerCase() == 'false');
        $("#HttpsPortSmisTextBox").val(respond.HttpsPort);
        $("#HttpPortSmisTextBox").val(respond.HttpPort);
        $("#InteropNamespaceSmisTextBox").val(respond.InteropNamespace);
        $("#ArrayNamespaceSmisTextBox").val(respond.Namespace);

        $("#ProviderPasswordSmisTextBox").removeClass("checkEmptySmsi");
        $("#ProviderConfirmPasswordSmisTextBox").removeClass("checkEmptySmsi");
        $("#ProviderPasswordSmisTextBox").attr("disabled", "disabled").val("");
        $("#ProviderConfirmPasswordSmisTextBox").attr("disabled", "disabled").val("");
        $("#ChangePasswordCheckBox").prop("checked", false);
        $("#checkboxPassword").show();

        addSmisCredentials();
    }

    function fillVnx(respond, name) {
        $("#smsiVnxCredId").val(respond.Id);
        $("#DisplayNameApiTextBox").val(name);
        $("#ArrayApiUsernameTextBox").val(respond.Username);
        $("#ArrayApiManagementServicesPathTextBox").val(respond.PathToManagementServices);
        $("#ArrayApiVnxPortTextBox").val(respond.HttpsPort);

        $("#ArrayApiPasswordTextBox").removeClass("checkEmptyVnx");
        $("#ArrayApiConfirmPasswordTextBox").removeClass("checkEmptyVnx");
        $("#ArrayApiPasswordTextBox").attr("disabled", "disabled").val("");
        $("#ArrayApiConfirmPasswordTextBox").attr("disabled", "disabled").val("");
        $("#ChangePasswordVnxCheckBox").prop("checked", false);
        $("#checkboxPasswordVnx").show();

        addVnxCredentials();
    }

    function fillMsa(respond, name) {
        var divName = $("#addDialogMsaBody");

        divName.find("#smsiMsaCredId").val(respond.Id);
        divName.find("#MsaDisplayNameApiTextBox").val(name);
        divName.find("#MsaArrayApiUsernameTextBox").val(respond.Username);
        divName.find("#ArrayApiIsHttpCheckbox").prop("checked", String(respond.UseSSL).toLowerCase() == 'false');
        divName.find("#ArrayApiMsaPortTextBox").val(respond.HttpPort);
        divName.find("#ArrayApiMsaHttpsPortTextBox").val(respond.HttpsPort);

        divName.find("#MsaArrayApiPasswordTextBox").removeClass("checkEmptyMsa");
        divName.find("#MsaArrayApiConfirmPasswordTextBox").removeClass("checkEmptyMsa");
        divName.find("#MsaArrayApiPasswordTextBox").attr("disabled", "disabled").val("");
        divName.find("#MsaArrayApiConfirmPasswordTextBox").attr("disabled", "disabled").val("");
        divName.find("#ChangePasswordMsaCheckBox").prop("checked", false);
        divName.find("#checkboxPasswordMsa").show();
        addMsaCredentials();
    }

    function fillIsilon(respond, name) {
        var divName = $("#addDialogIsilonBody");
        divName.find("#apiHttpsPortRow").remove();
        divName.find("#smsiMsaCredId").val(respond.Id);
        divName.find("#MsaDisplayNameApiTextBox").val(name);
        divName.find("#MsaArrayApiUsernameTextBox").val(respond.Username);
        divName.find("#ArrayApiIsHttpCheckbox").prop("checked", String(respond.UseSSL).toLowerCase() == 'false');
        divName.find("#ArrayApiMsaPortTextBox").val(respond.Port);

        divName.find("#MsaArrayApiPasswordTextBox").removeClass("checkEmptyMsa");
        divName.find("#MsaArrayApiConfirmPasswordTextBox").removeClass("checkEmptyMsa");
        divName.find("#MsaArrayApiPasswordTextBox").attr("disabled", "disabled").val("");
        divName.find("#MsaArrayApiConfirmPasswordTextBox").attr("disabled", "disabled").val("");
        divName.find("#ChangePasswordMsaCheckBox").prop("checked", false);
        divName.find("#checkboxPasswordMsa").show();
        addIsilonCredentials();
    }

    function fillPureStorage(respond, name) {
        var divName = $("#addDialogPureStorageBody");
        divName.find("#smsiMsaCredId").val(respond.Id);
        divName.find("#MsaDisplayNameApiTextBox").val(name);
        divName.find("#MsaArrayApiUsernameTextBox").val(respond.Username);
        divName.find("#ArrayApiIsHttpCheckbox").prop("checked", String(respond.UseSSL).toLowerCase() == 'false');
        divName.find("#ArrayApiMsaPortTextBox").val(respond.HttpPort);
        divName.find("#ArrayApiMsaHttpsPortTextBox").val(respond.HttpsPort);

        divName.find("#MsaArrayApiPasswordTextBox").removeClass("checkEmptyMsa");
        divName.find("#MsaArrayApiConfirmPasswordTextBox").removeClass("checkEmptyMsa");
        divName.find("#MsaArrayApiPasswordTextBox").attr("disabled", "disabled").val("");
        divName.find("#MsaArrayApiConfirmPasswordTextBox").attr("disabled", "disabled").val("");
        divName.find("#ChangePasswordMsaCheckBox").prop("checked", false);
        divName.find("#checkboxPasswordMsa").show();
        addPureStorageCredentials();
    }

    function fillXtremIo(respond, name) {
        var divName = $("#addDialogXtremIoBody");
        divName.find("#smsiMsaCredId").val(respond.Id);
        divName.find("#MsaDisplayNameApiTextBox").val(name);
        divName.find("#MsaArrayApiUsernameTextBox").val(respond.Username);
        divName.find("#ArrayApiIsHttpCheckbox").prop("checked", String(respond.UseSSL).toLowerCase() == 'false');
        divName.find("#ArrayApiMsaPortTextBox").val(respond.HttpPort);
        divName.find("#ArrayApiMsaHttpsPortTextBox").val(respond.HttpsPort);

        divName.find("#MsaArrayApiPasswordTextBox").removeClass("checkEmptyMsa");
        divName.find("#MsaArrayApiConfirmPasswordTextBox").removeClass("checkEmptyMsa");
        divName.find("#MsaArrayApiPasswordTextBox").attr("disabled", "disabled").val("");
        divName.find("#MsaArrayApiConfirmPasswordTextBox").attr("disabled", "disabled").val("");
        divName.find("#ChangePasswordMsaCheckBox").prop("checked", false);
        divName.find("#checkboxPasswordMsa").show();
        addXtremIoCredentials();
    }

    function fillNetAppFiler(respond, name) {
        var divName = $("#addDialogNetAppFilerBody");
        divName.find("#netAppFilerCredId").val(respond.Id);
        divName.find("#FilerCredentialNameTextBox").val(name);
        divName.find("#FilerUserNameTextBox").val(respond.Username);
        divName.find("#FilerProtocolCheckBox").prop("checked", String(respond.UseSSL).toLowerCase() == 'true');

        divName.find("#FilerPasswordTextBox").removeClass("checkEmptyFiler");
        divName.find("#FilerConfirmPasswordTextBox").removeClass("checkEmptyFiler");
        divName.find("#FilerPasswordTextBox").attr("disabled", "disabled").val("");
        divName.find("#FilerConfirmPasswordTextBox").attr("disabled", "disabled").val("");
        divName.find("#ChangePasswordFilerCheckBox").prop("checked", false);
        divName.find("#checkboxPasswordFiler").show();

        addNetAppFilerCredentials();
    }

    function fillEmcUnity(respond, name) {
        var divName = $("#addDialogEmcUnityBody");
        divName.find("#netAppFilerCredId").val(respond.Id);
        divName.find("#FilerCredentialNameTextBox").val(name);
        divName.find("#FilerUserNameTextBox").val(respond.Username);
        divName.find("#FilerProtocolCheckBox").prop("checked", String(respond.UseSSL).toLowerCase() == 'true');

        divName.find("#FilerPasswordTextBox").removeClass("checkEmptyFiler");
        divName.find("#FilerConfirmPasswordTextBox").removeClass("checkEmptyFiler");
        divName.find("#FilerPasswordTextBox").attr("disabled", "disabled").val("");
        divName.find("#FilerConfirmPasswordTextBox").attr("disabled", "disabled").val("");
        divName.find("#ChangePasswordFilerCheckBox").prop("checked", false);
        divName.find("#checkboxPasswordFiler").show();

        addEmcUnityCredentials();
    }


    function fillGenericHttp(respond, name) {
        var divName = $("#addDialogGenericHttpBody");

        divName.find("#genericHttpCredId").val(respond.Id);
        divName.find("#DisplayNameApiTextBox").val(name);
        divName.find("#ArrayApiUsernameTextBox").val(respond.Username);
        var isHttp = String(respond.UseSSL).toLowerCase() == 'false';
        divName.find("#ArrayApiIsHttpCheckbox").prop("checked", isHttp);
        divName.find("#ArrayApiPortTextBox").val(isHttp ? respond.HttpPort : respond.HttpsPort);

        divName.find("#ArrayApiPasswordTextBox").removeClass("checkEmptyGenericHttp");
        divName.find("#ArrayApiConfirmPasswordTextBox").removeClass("checkEmptyGenericHttp");
        divName.find("#ArrayApiPasswordTextBox").attr("disabled", "disabled").val("");
        divName.find("#ArrayApiConfirmPasswordTextBox").attr("disabled", "disabled").val("");
        divName.find("#ChangePasswordCheckBox").prop("checked", false);
        divName.find("#checkboxPassword").show();

        addGenericHttpCredentials();
    }

    function fillInfiniBox(respond, name) {
        var divName = $("#addDialogInfiniBoxBody");
        divName.find("#netAppFilerCredId").val(respond.Id);
        divName.find("#FilerCredentialNameTextBox").val(name);
        divName.find("#FilerUserNameTextBox").val(respond.Username);
        divName.find("#FilerProtocolCheckBox").prop("checked", String(respond.UseSSL).toLowerCase() == 'true');

        divName.find("#FilerPasswordTextBox").removeClass("checkEmptyFiler");
        divName.find("#FilerConfirmPasswordTextBox").removeClass("checkEmptyFiler");
        divName.find("#FilerPasswordTextBox").attr("disabled", "disabled").val("");
        divName.find("#FilerConfirmPasswordTextBox").attr("disabled", "disabled").val("");
        divName.find("#ChangePasswordFilerCheckBox").prop("checked", false);
        divName.find("#checkboxPasswordFiler").show();

        addInfiniBoxCredentials();
    }

    function fillNetAppDfm(respond, name) {
        $("#dfmCredId").val(respond.Id);
        $("#DisplayNameDfmTextBox").val(name);
        $("#ProviderUsernameDfmTextBox").val(respond.Username);
        $("#ProtocolDfmCheckBox").prop("checked", String(respond.UseSSL).toLowerCase() == 'false');
        $("#HttpsPortDfmTextBox").val(respond.HttpsPort);
        $("#HttpPortDfmTextBox").val(respond.HttpPort);

        $("#ProviderPasswordDfmTextBox").removeClass("checkEmptyDfm");
        $("#ProviderConfirmPasswordDfmTextBox").removeClass("checkEmptyDfm");
        $("#ProviderPasswordDfmTextBox").attr("disabled", "disabled").val("");
        $("#ProviderConfirmPasswordDfmTextBox").attr("disabled", "disabled").val("");
        $("#ChangePasswordDfmCheckBox").prop("checked", false);
        $("#checkboxPasswordDfm").show();

        addDfmCredentials();
    }

    function fillKaminario(respond, name) {
        var divName = $("#addDialogKaminarioBody");
        divName.find("#genericHttpCredId").val(respond.Id);
        divName.find("#DisplayNameApiTextBox").val(name);
        divName.find("#ArrayApiUsernameTextBox").val(respond.Username);
        var isHttp = String(respond.UseSSL).toLowerCase() == 'false';
        divName.find("#ArrayApiIsHttpCheckbox").prop("checked", isHttp);
        divName.find("#ArrayApiPortTextBox").val(isHttp ? respond.HttpPort : respond.HttpsPort);

        divName.find("#ArrayApiPasswordTextBox").removeClass("checkEmptyGenericHttp");
        divName.find("#ArrayApiConfirmPasswordTextBox").removeClass("checkEmptyGenericHttp");
        divName.find("#ArrayApiPasswordTextBox").attr("disabled", "disabled").val("");
        divName.find("#ArrayApiConfirmPasswordTextBox").attr("disabled", "disabled").val("");
        divName.find("#ChangePasswordCheckBox").prop("checked", false);
        divName.find("#checkboxPassword").show();

        addKaminarioCredentials();
    }

    var result = {
        FieldFilter: function () {
            return filterCredentialField;
        },
        RefreshGrid: function () {
            refreshCredentialGrid();
        },
        SetDefaultPorts: function (httpPort, httpsPort) {
            defaultHttpPort = httpPort;
            defaultHttpsPort = httpsPort;
        },
        init: function () {
            selectorCredentialModel = new Ext.grid.CheckboxSelectionModel({ hidden: false });
            userCredentialPageSize = parseInt(ORION.Prefs.load('PageSize', '20'));
            var gridCredentialColumnsModel = [
                selectorCredentialModel,
                { id: 'CredentialName', header: '@{R=SRM.Strings;K=StorageObjects_CredentialNameColumnLabel;E=js}', resizable: false, flex: 0, hideable: false, dataIndex: 'CredentialName', sortable: true, renderer: renderColumns },
                { id: 'UserName', header: '@{R=SRM.Strings;K=StorageObjects_UserNameColumnLabel;E=js}', resizable: false, flex: 0, sortable: true, dataIndex: 'UserName', hidden: false },
                { id: 'UsageCount', header: '@{R=SRM.Strings;K=StorageObjects_UsageCountColumnLabel;E=js}', resizable: false, width: 100, flex: 0, sortable: true, dataIndex: 'UsageCount', hidden: false },
                { id: 'CredentialType', header: '@{R=SRM.Strings;K=StorageObjects_CredentialType;E=js}', resizable: false, width: 100, flex: 0, sortable: false, dataIndex: 'CredentialTypeName', hidden: false }
            ];
            var gridCredentialStoreColumns = [
                { name: 'Row', mapping: 0 },
                { name: 'ID', mapping: 1 },
                { name: 'CredentialName', mapping: 2 },
                { name: 'UserName', mapping: 3 },
                { name: 'UsageCount', mapping: 4 },
                { name: 'TotalRows', mapping: 5 },
                { name: 'CredentialType', mapping: 6 },
                { name: 'CredentialTypeName', mapping: 7 }
            ];
            dataStoreCredentials = new ORION.WebServiceStore("/Orion/SRM/Services/StorageManagementService.asmx/GetAllCredentials", gridCredentialStoreColumns, "ID");
            pageCredentialSizeBox = new Ext.form.NumberField({
                id: 'PageSizeFieldCredentials',
                enableKeyEvents: true,
                allowNegative: false,
                width: 40,
                allowBlank: false,
                minValue: 1,
                maxValue: 100,
                value: userCredentialPageSize,
                listeners: {
                    scope: this,
                    'keydown': function (f, e) {
                        var k = e.getKey();
                        if (k == e.RETURN) {
                            e.stopEvent();
                            var v = parseInt(f.getValue());

                            // Perform Extended Validation
                            if (isNaN(v)) {
                                Ext.Msg.show({
                                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                    msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                                });
                                return;
                            }
                            else if (v <= 0) {
                                Ext.Msg.show({
                                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                    msg: "@{R=SRM.Strings;K=SRM_StorageObjects_MinimumWarning; E=js}",
                                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                                });
                                // Fix user input by establishing Minimum value
                                v = 1;
                                f.setValue('1');
                            }
                            else if (v > 100) {
                                Ext.Msg.show({
                                    title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                    msg: "@{R=SRM.Strings;K=SRM_StorageObjects_MaximumWarning; E=js}",
                                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                                });
                                // Fix user input by establishing Maximum value
                                v = 100;
                                f.setValue('100');
                            }

                            if (userCredentialPageSize != v) { // Validation passed
                                userCredentialPageSize = v;
                                pagingCredentialToolbar.pageSize = userCredentialPageSize;
                                ORION.Prefs.save('f', userCredentialPageSize);
                                pagingCredentialToolbar.doLoad(0);
                            }
                        }
                    },
                    'focus': function (field) {
                        field.el.dom.select();
                    },
                    'change': function (f, numbox, o) {
                        var pSize = parseInt(f.getValue());

                        // Perform Extended Validation
                        if (isNaN(pSize)) {
                            Ext.Msg.show({
                                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                msg: "@{R=Core.Strings;K=WEBJS_SO0_25; E=js}",
                                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                            });
                            return;
                        }
                        else if (pSize <= 0) {
                            Ext.Msg.show({
                                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                msg: "@{R=SRM.Strings;K=SRM_StorageObjects_MinimumWarning; E=js}",
                                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                            });
                            // Fix user input by establishing Minimum value
                            pSize = 1;
                            f.setValue('1');
                        }
                        else if (pSize > 100) {
                            Ext.Msg.show({
                                title: "@{R=Core.Strings;K=WEBJS_SO0_24; E=js}",
                                msg: "@{R=SRM.Strings;K=SRM_StorageObjects_MaximumWarning; E=js}",
                                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                            });
                            // Fix user input by establishing Maximum value
                            pSize = 100;
                            f.setValue('100');
                        }

                        if (pagingCredentialToolbar.pageSize != pSize) { // update page size only if it is different
                            pagingCredentialToolbar.pageSize = pSize;
                            userCredentialPageSize = pSize;

                            ORION.Prefs.save('PageSize', userCredentialPageSize);
                            pagingCredentialToolbar.doLoad(0);
                        }
                    }
                }
            });
            selectorCredentialModel.on("selectionchange", function () {
                updateToolbarCredentialButtons();
            });

            var pagingCredentialToolbar = new Ext.PagingToolbar(
            {
                store: dataStoreCredentials,
                pageSize: userCredentialPageSize,
                height: '25px',
                displayInfo: true,
                displayMsg: "@{R=Core.Strings;K=WEBJS_SO0_2; E=js}",
                emptyMsg: "@{R=Core.Strings;K=WEBJS_SO0_3; E=js}",
                beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
                items: [
                    '-',
                    new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46; E=js}', style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;' }),
                    pageCredentialSizeBox
                ]
            });

            var splitButtonAdd = new Ext.SplitButton({
                id: 'splitButtonAdd',
                text: '@{R=SRM.Strings;K=StorageObjects_AddCredentialLabel;E=js}',
                tooltip: '@{R=SRM.Strings;K=StorageObjects_AddCredentialLabel;E=js}',
                icon: '/Orion/SRM/images/u412.png',
                cls: 'x-btn-text-icon',
                disabled: false,
                handler: function () { this.showMenu(); },
                menu: new Ext.menu.Menu({
                    items: [
                        {
                            text: '@{R=SRM.Strings;K=StorageObjects_AddSNMP;E=js}',
                            icon: '/Orion/SRM/images/u412.png',
                            cls: 'x-btn-text-icon',
                            handler: function () {
                                $("#addDialogSnmpVerOneBody input").val('');
                                addSnmpCredentials();
                            }
                        },
                        {
                            text: '@{R=SRM.Strings;K=StorageObjects_AddSNMPV3;E=js}',
                            icon: '/Orion/SRM/images/u412.png',
                            cls: 'x-btn-text-icon',
                            handler: function () {
                                $("#addDialogSnmpVerThreeBody input").val('');
                                $('#addDialogSnmpVerThreeBody input:checkbox').removeAttr('checked');
                                $('#addDialogSnmpVerThreeBody option').removeAttr('selected');
                                addSnmpV3Credentials();
                            }
                        },
                        {
                            text: '@{R=SRM.Strings;K=StorageObjects_AddSMIS;E=js}',
                            icon: '/Orion/SRM/images/u412.png',
                            cls: 'x-btn-text-icon',
                            handler: function () {
                                $("#addDialogSmisBody input").val('');
                                $('#addDialogSmisBody input:checkbox').removeAttr('checked');
                                $("#ProviderPasswordSmisTextBox").removeAttr("disabled");
                                $("#ProviderConfirmPasswordSmisTextBox").removeAttr("disabled");
                                if (!$("#ProviderPasswordSmisTextBox").hasClass('checkEmptySmsi')) {
                                    $("#ProviderPasswordSmisTextBox").addClass("checkEmptySmsi");
                                    $("#ProviderConfirmPasswordSmisTextBox").addClass("checkEmptySmsi");
                                }
                                $("#checkboxPassword").hide();
                                addSmisCredentials();

                            }
                        },
                        {
                            text: '@{R=SRM.Strings;K=StorageObjects_AddMSA;E=js}',
                            icon: '/Orion/SRM/images/u412.png',
                            cls: 'x-btn-text-icon',
                            handler: function () {
                                var divName = $("#addDialogMsaBody");
                                divName.find("input").val('');
                                divName.find('input:checkbox').removeAttr('checked');
                                divName.find("#MsaArrayApiPasswordTextBox").removeAttr("disabled");
                                divName.find("#MsaArrayApiConfirmPasswordTextBox").removeAttr("disabled");
                                if (!divName.find("#MsaArrayApiPasswordTextBox").hasClass('checkEmptyVnx')) {
                                    divName.find("#MsaArrayApiPasswordTextBox").addClass("checkEmptyVnx");
                                    divName.find("#MsaArrayApiConfirmPasswordTextBox").addClass("checkEmptyVnx");
                                }
                                divName.find("#checkboxPasswordMsa").hide();
                                divName.find("#ArrayApiIsHttpCheckbox").prop("checked", true);
                                divName.find('#ArrayApiMsaPortTextBox').val(defaultHttpPort);
                                divName.find('#ArrayApiMsaHttpsPortTextBox').val(defaultHttpsPort);
                                addMsaCredentials();
                            }
                        },
                        {
                            text: '@{R=SRM.Strings;K=StorageObjects_AddIsilonHttp;E=js}',
                            icon: '/Orion/SRM/images/u412.png',
                            cls: 'x-btn-text-icon',
                            handler: function () {
                                var divName = $("#addDialogIsilonBody");
                                divName.find("#apiHttpsPortRow").remove();
                                divName.find("input").val('');
                                divName.find('input:checkbox').removeAttr('checked');
                                divName.find("#MsaArrayApiPasswordTextBox").removeAttr("disabled");
                                divName.find("#MsaArrayApiConfirmPasswordTextBox").removeAttr("disabled");
                                if (!divName.find("#MsaArrayApiPasswordTextBox").hasClass('checkEmptyVnx')) {
                                    divName.find("#MsaArrayApiPasswordTextBox").addClass("checkEmptyVnx");
                                    divName.find("#MsaArrayApiConfirmPasswordTextBox").addClass("checkEmptyVnx");
                                }
                                divName.find("#checkboxPasswordMsa").hide();
                                divName.find("#ArrayApiIsHttpCheckbox").prop("checked", false);
                                divName.find('#ArrayApiMsaPortTextBox').val(defaultIsilonAPIPort);
                                addIsilonCredentials();
                            }
                        },
                        {
                            text: '@{R=SRM.Strings;K=StorageObjects_AddPureStorageHttp;E=js}',
                            icon: '/Orion/SRM/images/u412.png',
                            cls: 'x-btn-text-icon',
                            handler: function () {
                                var divName = $("#addDialogPureStorageBody");
                                divName.find("input").val('');
                                divName.find('input:checkbox').removeAttr('checked');
                                divName.find("#MsaArrayApiPasswordTextBox").removeAttr("disabled");
                                divName.find("#MsaArrayApiConfirmPasswordTextBox").removeAttr("disabled");
                                if (!divName.find("#MsaArrayApiPasswordTextBox").hasClass('checkEmptyVnx')) {
                                    divName.find("#MsaArrayApiPasswordTextBox").addClass("checkEmptyVnx");
                                    divName.find("#MsaArrayApiConfirmPasswordTextBox").addClass("checkEmptyVnx");
                                }
                                divName.find("#checkboxPasswordMsa").hide();
                                divName.find("#ArrayApiIsHttpCheckbox").prop("checked", false);
                                divName.find('#ArrayApiMsaPortTextBox').val(defaultHttpPort);
                                divName.find('#ArrayApiMsaHttpsPortTextBox').val(defaultHttpsPort);
                                addPureStorageCredentials();
                            }
                        },
                        {
                            text: '@{R=SRM.Strings;K=StorageObjects_AddXtremIoHttp;E=js}',
                            icon: '/Orion/SRM/images/u412.png',
                            cls: 'x-btn-text-icon',
                            handler: function () {
                                var divName = $("#addDialogXtremIoBody");
                                divName.find("input").val('');
                                divName.find('input:checkbox').removeAttr('checked');
                                divName.find("#MsaArrayApiPasswordTextBox").removeAttr("disabled");
                                divName.find("#MsaArrayApiConfirmPasswordTextBox").removeAttr("disabled");
                                if (!divName.find("#MsaArrayApiPasswordTextBox").hasClass('checkEmptyVnx')) {
                                    divName.find("#MsaArrayApiPasswordTextBox").addClass("checkEmptyVnx");
                                    divName.find("#MsaArrayApiConfirmPasswordTextBox").addClass("checkEmptyVnx");
                                }
                                divName.find("#checkboxPasswordMsa").hide();
                                divName.find("#ArrayApiIsHttpCheckbox").prop("checked", false);
                                divName.find('#ArrayApiMsaPortTextBox').val(defaultHttpPort);
                                divName.find('#ArrayApiMsaHttpsPortTextBox').val(defaultHttpsPort);
                                addXtremIoCredentials();
                            }
                        },
                        {
                            text: '@{R=SRM.Strings;K=StorageObjects_AddVNX;E=js}',
                            icon: '/Orion/SRM/images/u412.png',
                            cls: 'x-btn-text-icon',
                            handler: function () {
                                $("#addDialogVnxBody input").val('');
                                $('#addDialogVnxBody input:checkbox').removeAttr('checked');
                                $("#ArrayApiPasswordTextBox").removeAttr("disabled");
                                $("#ArrayApiConfirmPasswordTextBox").removeAttr("disabled");
                                if (!$("#ArrayApiPasswordTextBox").hasClass('checkEmptyVnx')) {
                                    $("#ArrayApiPasswordTextBox").addClass("checkEmptyVnx");
                                    $("#ArrayApiConfirmPasswordTextBox").addClass("checkEmptyVnx");
                                }
                                $("#checkboxPasswordVnx").hide();
                                addVnxCredentials();
                            }
                        },
                        {
                            text: '@{R=SRM.Strings;K=StorageObjects_AddNetAppFiler;E=js}',
                            icon: '/Orion/SRM/images/u412.png',
                            cls: 'x-btn-text-icon',
                            handler: function () {
                                var divName = $("#addDialogNetAppFilerBody");
                                divName.find("input").val('');
                                divName.find('input:checkbox').removeAttr('checked');
                                divName.find("#FilerPasswordTextBox").removeAttr("disabled");
                                divName.find("#FilerConfirmPasswordTextBox").removeAttr("disabled");
                                if (!divName.find("#FilerPasswordTextBox").hasClass('checkEmptyFiler')) {
                                    divName.find("#FilerPasswordTextBox").addClass("checkEmptyFiler");
                                    divName.find("#FilerConfirmPasswordTextBox").addClass("checkEmptyFiler");
                                }
                                divName.find("#checkboxPasswordFiler").hide();
                                addNetAppFilerCredentials();
                            }
                        },
                        {
                            text: '@{R=SRM.Strings;K=StorageObjects_AddNetAppDfm;E=js}',
                            icon: '/Orion/SRM/images/u412.png',
                            cls: 'x-btn-text-icon',
                            handler: function () {
                                $("#addDialogDfmBody input").val('');
                                $('#addDialogDfmBody input:checkbox').removeAttr('checked');
                                $("#ProviderPasswordDfmTextBox").removeAttr("disabled");
                                $("#ProviderConfirmPasswordDfmTextBox").removeAttr("disabled");
                                if (!$("#ProviderPasswordDfmTextBox").hasClass('checkEmptyDfm')) {
                                    $("#ProviderPasswordDfmTextBox").addClass("checkEmptyDfm");
                                    $("#ProviderConfirmPasswordDfmTextBox").addClass("checkEmptyDfm");
                                }
                                $("#checkboxPasswordDfm").hide();
                                addDfmCredentials();
                            }
                        },
                        {
                            text: '@{R=SRM.Strings;K=StorageObjects_AddEmcUnity;E=js}',
                            icon: '/Orion/SRM/images/u412.png',
                            cls: 'x-btn-text-icon',
                            handler: function () {
                                var divName = $("#addDialogEmcUnityBody");
                                divName.find("input").val('');
                                divName.find('input:checkbox').removeAttr('checked');
                                divName.find("#FilerPasswordTextBox").removeAttr("disabled");
                                divName.find("#FilerConfirmPasswordTextBox").removeAttr("disabled");
                                if (!divName.find("#FilerPasswordTextBox").hasClass('checkEmptyFiler')) {
                                    divName.find("#FilerPasswordTextBox").addClass("checkEmptyFiler");
                                    divName.find("#FilerConfirmPasswordTextBox").addClass("checkEmptyFiler");
                                }
                                divName.find("#checkboxPasswordFiler").hide();
                                addEmcUnityCredentials();
                            }
                        },
                        {
                            text: '@{R=SRM.Strings;K=StorageObjects_AddInfiniBox;E=js}',
                            icon: '/Orion/SRM/images/u412.png',
                            cls: 'x-btn-text-icon',
                            handler: function () {
                                var divName = $("#addDialogInfiniBoxBody");
                                divName.find("input").val('');
                                divName.find('input:checkbox').removeAttr('checked');
                                divName.find("#FilerPasswordTextBox").removeAttr("disabled");
                                divName.find("#FilerConfirmPasswordTextBox").removeAttr("disabled");
                                if (!divName.find("#FilerPasswordTextBox").hasClass('checkEmptyFiler')) {
                                    divName.find("#FilerPasswordTextBox").addClass("checkEmptyFiler");
                                    divName.find("#FilerConfirmPasswordTextBox").addClass("checkEmptyFiler");
                                }
                                divName.find("#checkboxPasswordFiler").hide();
                                addInfiniBoxCredentials();
                            }
                        },
                        {
                            text: '@{R=SRM.Strings;K=StorageObjects_AddNimble;E=js}',
                            icon: '/Orion/SRM/images/u412.png',
                            cls: 'x-btn-text-icon',
                            handler: function () {

                                var divName = $("#addDialogGenericHttpBody");

                                divName.find("input").val('');
                                divName.find('input:checkbox').removeAttr('checked');
                                divName.find("#ArrayApiPasswordTextBox").removeAttr("disabled");
                                divName.find("#ArrayApiConfirmPasswordTextBox").removeAttr("disabled");
                                if (!divName.find("#ArrayApiPasswordTextBox").hasClass('checkEmptyGenericHttp')) {
                                    divName.find("#ArrayApiPasswordTextBox").addClass("checkEmptyGenericHttp");
                                    divName.find("#ArrayApiConfirmPasswordTextBox").addClass("checkEmptyGenericHttp");
                                }
                                divName.find("#checkboxPassword").hide();
                                divName.find("#ArrayApiIsHttpCheckbox").prop("checked", false);
                                divName.find('#ArrayApiPortTextBox').val(defaultNimbleGenericHttpPort);

                                addGenericHttpCredentials();
                            }
                        },
                        {
                            text: '@{R=SRM.Strings;K=StorageObjects_AddKaminarioHttp;E=js}',
                            icon: '/Orion/SRM/images/u412.png',
                            cls: 'x-btn-text-icon',
                            handler: function () {
                                var divName = $("#addDialogKaminarioBody");
                                divName.find("input").val('');
                                divName.find('input:checkbox').removeAttr('checked');
                                divName.find("#ArrayApiPasswordTextBox").removeAttr("disabled");
                                divName.find("#ArrayApiConfirmPasswordTextBox").removeAttr("disabled");
                                if (!divName.find("#ArrayApiPasswordTextBox").hasClass('checkEmptyGenericHttp')) {
                                    divName.find("#ArrayApiPasswordTextBox").addClass("checkEmptyGenericHttp");
                                    divName.find("#ArrayApiConfirmPasswordTextBox").addClass("checkEmptyGenericHttp");
                                }
                                divName.find("#checkboxPassword").hide();
                                divName.find("#ArrayApiIsHttpCheckbox").prop("checked", false);
                                divName.find('#ArrayApiPortTextBox').val(defaultHttpsPort);
                                addKaminarioCredentials();
                            }
                        },
                    ]
                })
            });

            var toolbarCredential = new Ext.Toolbar({
                id: 'toolbarCredentials',
                style: { overflow: 'visible', height: '23px' },
                hidden: false,
                items: [
                    splitButtonAdd,
                    '-', {
                        id: 'EditCredential',
                        text: '@{R=SRM.Strings;K=StorageObjects_EditCredentialLabel;E=js}',
                        tooltip: '@{R=SRM.Strings;K=StorageObjects_EditCredentialLabel;E=js}',
                        icon: '/Orion/SRM/images/bg_button_edit_icon.png',
                        cls: 'x-btn-text-icon',
                        disabled: true,
                        handler: function () {
                            editCredential();
                        }
                    }, '-', {
                        id: 'DeleteCredential',
                        text: '@{R=SRM.Strings;K=StorageObjects_DeleteCredentialLabel;E=js}',
                        tooltip: '@{R=SRM.Strings;K=StorageObjects_DeleteCredentialLabel;E=js}',
                        iconCls: 'deleteContainer',
                        handler: function () {
                            Ext.Msg.confirm("@{R=SRM.Strings;K=StorageObjects_ConfirmWindowDeleteCredentialsTitle;E=js}",
                                "@{R=SRM.Strings;K=StorageObjects_ConfirmWindowDeleteCredentialsText;E=js}",
                                function (btn, text) {
                                    if (btn == 'yes') {
                                        deleteCredentials(gridCredentials.getSelectionModel().selections.items);
                                    }
                                });
                        }
                    },
                    '->',
                     new Ext.ux.form.SearchField({
                         id: 'searchCredentials',
                         store: dataStoreCredentials,
                         width: 200
                     })
                ]
            });
            gridCredentials = new Ext.grid.GridPanel({
                id: 'myGridCredentials',
                columns: gridCredentialColumnsModel,
                store: dataStoreCredentials,
                baseParams: { start: 0, limit: userCredentialPageSize },
                //layout: 'fit',
                sm: selectorCredentialModel,
                height: gridCredentialHeight,
                width: gridCredentialWidth,
                autoscroll: true,
                stripeRows: true,
                tbar: toolbarCredential,
                bbar: pagingCredentialToolbar,
                viewConfig: {
                    autoFill: true,
                    forceFit: true
                }
            });

            gridCredentials.render('Grid');
            refreshCredentialGrid();
            updateToolbarCredentialButtons();
        }
    };
    return result;
}();