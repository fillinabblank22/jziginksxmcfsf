﻿var redirector = SW.Core.namespace('SW.SRM.Redirector');

// Page which is handling the Post-Redirect-Get pattern  
redirector.redirectPostUrl = '/Orion/SRM/RedirectGet.aspx';

// To be initialized in SW.SRM.Redirector.init()
redirector.redirectButtonId = '';
redirector.redirectParamsHiddenFieldId = '';
redirector.redirectFormName = '';

redirector.localChartsInitialized = false;

redirector.init = function (redirectButtonId, redirectParamsHiddenFieldId, redirectFormName) {
    this.redirectButtonId = redirectButtonId;
    this.redirectParamsHiddenFieldId = redirectParamsHiddenFieldId;
    this.redirectFormName = redirectFormName;
}

redirector.Redirect = function (arg) {    
    // Automatically add NetObject if exists on source page
    $.each(SW.Core.UriHelper.decodeURIParams(), function (key, value) {
        if (key.toLowerCase() == 'netobject') {
            if (arg.queryString === undefined)
                arg.queryString = {};
            if (arg.queryString.NetObject === undefined)
                arg.queryString['NetObject'] = value;
        }
    })
    // if no target passed, use default
    if (arg.targetUrl === undefined)
        arg.targetUrl = '/Orion/SRM/DisplayResource.aspx';

    $('#' + this.redirectParamsHiddenFieldId).val(JSON.stringify(arg));
    var form = $('#' + this.redirectFormName + '')[0];
    var originalAction = form.action;
    var originalTarget = form.target;
    form.target = "_blank";
    // Set the Post-Redirect-Get page url for the button
    WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(this.redirectButtonId, '', false, '', this.redirectPostUrl, false, true));
    form.action = originalAction;
    form.target = originalTarget;
}

redirector.bindRedirectClick = function (arg, fn) {
    if (arg.length !== undefined) {
        $.each(arg, function(index, value){
            redirector.bindRedirectClickEl(value, fn);
        });        
    } else {
        redirector.bindRedirectClickEl(arg, fn);
    }
}


redirector.bindRedirectClickEl = function (el, fn) {
    var hasChartClass = $(el).hasClass('hasChart');
    var innerEl = el;
    if (hasChartClass) {
        // delayed initialization of event for all charts on page
        if (!redirector.localChartsInitialized) {
            $(function () {
                (function (H) {
                    Highcharts.Chart.prototype.callbacks.push(function (chart) {
                        H.addEvent(chart.container, 'click', function (e) {
                            var el = $(e.currentTarget).parent();
                            var clickFn = chart.renderTo.click;
                            if (clickFn !== undefined && $(el).data('drilldown-link') !== undefined) {
                                chart.renderTo.click();
                            }
                        });

                        H.addEvent(chart.container, 'mouseover', function (e) {
                            var el = $(e.currentTarget).parent();
                            if ($(el).data('drilldown-link') !== undefined)
                                $(chart.container).css('cursor', 'pointer');
                        });
                    });
                }(Highcharts));

                var chartId = $(el).data('highcharts-chart');
                var chart = Highcharts.charts[chartId];

                $(el).data('drilldown-link', 'drilldown-link');
                // bind click event to host
                $(chart.renderTo).click(fn);
                
            });
            redirector.localChartsInitialized = true;
        }        
    }
    else {
        $(el).css('cursor', 'pointer');
        $(el).click(fn);
    }
}

