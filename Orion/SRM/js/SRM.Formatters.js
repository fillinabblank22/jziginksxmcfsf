﻿SW = SW || {};
SW.SRM = SW.SRM || {};
SW.SRM.Formatters = SW.SRM.Formatters || {};

var bytesInKB = 1024,
    bytesInMB = 1048576,
    bytesInGB = 1073741824,
    bytesInTB = 1099511627776,
    bytesInPB = 1125899906842624;

SW.SRM.Formatters.FormatCapacity = function (value, fixed, valueFormatter) {
    var fixedValue = fixed != undefined ? fixed : 2,
        formatter = valueFormatter || function (v) { return v; };

    if (value == -1 || value === '' || isNaN(value) || value == null) {
        return "@{R=SRM.Strings;K=Value_Unknown;E=js}";
    }

    if (Math.abs((value / bytesInKB).toFixed(fixedValue)) < bytesInKB) {
        return formatter((value / bytesInKB).toFixed(fixedValue)) + " @{R=SRM.Strings;K=Value_Unit_KB;E=js}";
    }
    if (Math.abs((value / bytesInMB).toFixed(fixedValue)) < bytesInKB) {
        return formatter((value / bytesInMB).toFixed(fixedValue)) + " @{R=SRM.Strings;K=Value_Unit_MB;E=js}";
    }
    if (Math.abs((value / bytesInGB).toFixed(fixedValue)) < bytesInKB) {
        return formatter((value / bytesInGB).toFixed(fixedValue)) + " @{R=SRM.Strings;K=Value_Unit_GB;E=js}";
    }
    if (Math.abs((value / bytesInTB).toFixed(fixedValue)) < bytesInKB) {
        return formatter((value / bytesInTB).toFixed(fixedValue)) + " @{R=SRM.Strings;K=Value_Unit_TB;E=js}";
    }
    return formatter((value / bytesInPB).toFixed(fixedValue)) + " @{R=SRM.Strings;K=Value_Unit_PB;E=js}";
};

SW.SRM.Formatters.FormatCapacityAbsolute = function (value) {
    return SW.SRM.Formatters.FormatCapacity(value, 2, Math.abs);
};

SW.SRM.Formatters.FormatLatencyNumber = function (value) {
    return (value > 0 && value < 10) ? value.toFixed(2) : Math.round(value);
};

SW.SRM.Formatters.FormatTinyNumber = function (value, isCapacity) {
    var formatted;
    if (isCapacity) {
        formatted = (value > 0 && value < 0.01) ? String.format('@{R=SRM.Strings;K=SRM_NumberFormat_IsLessThan;E=js}', 0.01) : value.toFixed(2);
    } else {
        formatted = (value > 0 && value < 1) ? String.format('@{R=SRM.Strings;K=SRM_NumberFormat_IsLessThan;E=js}', 1) : Math.round(value);
    }
    return formatted;
};

SW.SRM.Formatters.FormatLatencyNumberOrUnknown = function (value, unit) {
    if (typeof value === "undefined" || value === '' || isNaN(value) || value == null) {
        return "@{R=SRM.Strings;K=Value_Unknown;E=js}";
    }

    return SW.SRM.Formatters.FormatLatencyNumber(value) + (typeof unit === "undefined" ? '' : ' ' + unit);
};

SW.SRM.Formatters.ParseIntOrUnknown = function (value, unit) {
    if (typeof value === "undefined" || value === '' || isNaN(value) || value == null) {
        return "@{R=SRM.Strings;K=Value_Unknown;E=js}";
    }

    return parseInt(value) + (typeof unit === "undefined" ? '' : ' ' + unit);
};

SW.SRM.Formatters.FormatArrayType = function (value) {
    var strType = "@{R=SRM.Strings;K=Arrays_Type_Unknown;E=js}";
    switch (value) {
        case 0:
            strType = "@{R=SRM.Strings;K=Arrays_Type_Block;E=js}";
            break;
        case 1:
            strType = "@{R=SRM.Strings;K=Arrays_Type_File;E=js}";
            break;
        case 2:
            strType = "@{R=SRM.Strings;K=Arrays_Type_Unified;E=js}";
            break;
    }
    return strType;
};

SW.SRM.Formatters.FormatColumnTitle = function (value) {
    return value.replace(/\|/g, "<br/>");
};

SW.SRM.Formatters.FormatCellStyle = function (value, warning, critical) {
    var cssclass = "";
    if (warning && value > warning)
        cssclass = "SRM_Grid_Warning_Cell SRM_NoWrap";
    if (critical && value > critical)
        cssclass = "SRM_Grid_Critical_Cell SRM_NoWrap";
    return cssclass;
};

SW.SRM.Formatters.ApplyThresholds = function (isWarning, isCritical) {
    var cssclass = "";
    if (isWarning)
        cssclass = "SRM_Grid_Warning_Cell SRM_NoWrap";
    if (isCritical)
        cssclass = "SRM_Grid_Critical_Cell SRM_NoWrap";
    return cssclass;
};

SW.SRM.Formatters.FormatCapacityRunoutCellStyle = function (value, warning, critical) {
    if (SW.SRM.Formatters.IsRunoutCollectingData(value)) {
        return "SRM_Grid_CollectingData_Cell";
    }
    if (SW.SRM.Formatters.IsRunoutAlreadyPassed(value)) {
        return "SRM_Grid_AlreadyPassed_Cell";
    }
    if (critical && value <= critical)
        return "SRM_Grid_Critical_Cell";
    if (warning && value <= warning)
        return "SRM_Grid_Warning_Cell";
    return "";
};

SW.SRM.Formatters.FormatPercentageColumnValue = function (value, fixed) {
    if (value == "@{R=SRM.Strings;K=Value_Unknown;E=js}" || isNaN(value) || value === '') {
        return "@{R=SRM.Strings;K=Value_Unknown;E=js}";
    }
    if (value > 0 && value < 1) {
        return "< 1" + "@{R=SRM.Strings;K=Value_Unit_Percentage;E=js}";
    }
    if (value > 99 && value < 100) {
        return "> 99" + "@{R=SRM.Strings;K=Value_Unit_Percentage;E=js}";
    }
    return value.toFixed(fixed) + "@{R=SRM.Strings;K=Value_Unit_Percentage;E=js}";
};

SW.SRM.Formatters.FormatPoolType = function (value) {
    var strType = value;
    switch (value) {
        case 0:
            strType = "@{R=SRM.Strings;K=Pools_List_StoragePool;E=js}";
            break;
        case 1:
            strType = "@{R=SRM.Strings;K=Pools_List_RaidGroup;E=js}";
            break;
        case 2:
            strType = "@{R=SRM.Strings;K=Pools_List_AggregatePool;E=js}";
            break;
    }
    return strType;
};

var daysIn100Years = 36500;

SW.SRM.Formatters.IsRunoutCollectingData = function (value) {
    // Changes made to that function should be made also in SolarWinds.SRM.Web.WebHelper.IsRunoutCollectingData
    return value === "" || value == -daysIn100Years;
};

SW.SRM.Formatters.IsRunoutAlreadyPassed = function (value) {
    // Changes made to that function should be made also in SolarWinds.SRM.Web.WebHelper.IsRunoutAlreadyPassed
    return value < 1;

};

SW.SRM.Formatters.DateDiffInDays = function (d1, d2) {
    var msecInDay = 24 * 3600 * 1000;
    var t2 = d2.getTime();
    var t1 = d1.getTime();
    return Math.round((t2 - t1) / msecInDay);
};

SW.SRM.Formatters.FormatDaysDiff = function (value, warningThreshold, criticalThreshold) {
    var formatValue = function (value) {
        return Math.floor(value);
    };

    var result;

    if (SW.SRM.Formatters.IsRunoutCollectingData(value)) {
        return "@{R=SRM.Strings;K=SRM_Collecting_Data;E=js}";
    }
    if (SW.SRM.Formatters.IsRunoutAlreadyPassed(value)) {
        result = "@{R=SRM.Strings;K=SRM_Already_Passed;E=js}";
    } else {
        if (value < 2) {
            result = '1 ' + '@{R=SRM.Strings;K=SRM_Day;E=js}';
        } else {
            if (value < 14) {
                result = value + ' ' + '@{R=SRM.Strings;K=SRM_Days;E=js}';
            } else {
                if (value < 60) {
                    result = formatValue(value / 7) + ' ' + '@{R=SRM.Strings;K=SRM_Weeks;E=js}';
                } else {
                    if (value < 5*365) {
                        result = formatValue(value / 30) + ' ' + '@{R=SRM.Strings;K=SRM_Months;E=js}';
                    } else {
                        result = '> 5 ' + '@{R=SRM.Strings;K=SRM_Years;E=js}';
                    }
                }
            }
        }
    }

    if (criticalThreshold && value <= criticalThreshold) {
        var span = $("<span />").addClass('SRM_CriticalText').text(result);
        return $("<p />").append(span).html();
    }
    if (warningThreshold && value <= warningThreshold) {
        var span = $("<span />").addClass('SRM_WarningText').text(result);
        return $("<p />").append(span).html();
    }
    return result;
};

SW.SRM.Formatters.FormatDate = function (date) {
    var lang = Highcharts.getOptions().lang;
    return Highcharts.dateFormat(lang.tooltipDateFormat, date);
};

var seriesThroughput = new Array('@{R=SRM.Strings;K=Lun_Performance_Summary_Throughput;E=js}',
    '@{R=SRM.Strings;K=Lun_Performance_Summary_Throughput_Read;E=js}',
    '@{R=SRM.Strings;K=Lun_Performance_Summary_Throughput_Write;E=js}');

var seriesIOSize = new Array('@{R=SRM.Strings;K=Lun_Performance_Summary_IOSize;E=js}',
    '@{R=SRM.Strings;K=Lun_Performance_Summary_IOSize_Read;E=js}',
    '@{R=SRM.Strings;K=Lun_Performance_Summary_IOSize_Write;E=js}');

var seriesLatency = new Array('@{R=SRM.Strings;K=Lun_Performance_Summary_Latency;E=js}',
    '@{R=SRM.Strings;K=Lun_Performance_Summary_Latency_Read;E=js}',
    '@{R=SRM.Strings;K=Lun_Performance_Summary_Latency_Write;E=js}',
    '@{R=SRM.Strings;K=Lun_Performance_Summary_Latency_Other;E=js}');

var SeriesNameToUnits = {
    '@{R=SRM.Strings;K=Lun_Performance_Summary_IOPS;E=js}': "@{R=SRM.Strings;K=Iops_Unit;E=js}",
    '@{R=SRM.Strings;K=Lun_Performance_Summary_IOPS_Read;E=js}': "@{R=SRM.Strings;K=Iops_Unit;E=js}",
    '@{R=SRM.Strings;K=Lun_Performance_Summary_IOPS_Write;E=js}': "@{R=SRM.Strings;K=Iops_Unit;E=js}",
    '@{R=SRM.Strings;K=Lun_Performance_Summary_IOPS_Other;E=js}': "@{R=SRM.Strings;K=Iops_Unit;E=js}",
    '@{R=SRM.Strings;K=Lun_Performance_Summary_CacheHitRatio;E=js}': "@{R=SRM.Strings;K=CacheHitRatioy_Unit;E=js}",
    '@{R=SRM.Strings;K=Lun_Performance_Summary_Ratio_Read;E=js}': "@{R=SRM.Strings;K=CacheHitRatioy_Unit;E=js}",
    '@{R=SRM.Strings;K=Lun_Performance_Summary_Ratio_Write;E=js}': "@{R=SRM.Strings;K=CacheHitRatioy_Unit;E=js}",
    '@{R=SRM.Strings;K=Lun_Performance_Summary_Latency;E=js}': "@{R=SRM.Strings;K=Latency_Unit;E=js}",
    '@{R=SRM.Strings;K=Lun_Performance_Summary_Latency_Read;E=js}': "@{R=SRM.Strings;K=Latency_Unit;E=js}",
    '@{R=SRM.Strings;K=Lun_Performance_Summary_Latency_Write;E=js}': "@{R=SRM.Strings;K=Latency_Unit;E=js}",
    '@{R=SRM.Strings;K=Lun_Performance_Summary_Latency_Other;E=js}': "@{R=SRM.Strings;K=Latency_Unit;E=js}",
    '@{R=SRM.Strings;K=Lun_Performance_Summary_IOPSRatio;E=js}': "@{R=SRM.Strings;K=IOPSRatio_Unit;E=js}",
    '@{R=SRM.Strings;K=Lun_Performance_Summary_QueueLength;E=js}': "@{R=SRM.Strings;K=QueueLength_Unit;E=js}",
    '@{R=SRM.Strings;K=Lun_Performance_Summary_QueueLength_Read;E=js}': "@{R=SRM.Strings;K=QueueLength_Unit;E=js}",
    '@{R=SRM.Strings;K=Lun_Performance_Summary_QueueLength_Write;E=js}': "@{R=SRM.Strings;K=QueueLength_Unit;E=js}",
    '@{R=SRM.Strings;K=Lun_Performance_Summary_DiskBusy;E=js}': "@{R=SRM.Strings;K=DiskBusy_Unit;E=js}",
    '@{R=SRM.Strings;K=Lun_Performance_Summary_IOSize;E=js}': "@{R=SRM.Strings;K=IOSize_Unit;E=js}",
    '@{R=SRM.Strings;K=Lun_Performance_Summary_IOSize_Read;E=js}': "@{R=SRM.Strings;K=IOSize_Unit;E=js}",
    '@{R=SRM.Strings;K=Lun_Performance_Summary_IOSize_Write;E=js}': "@{R=SRM.Strings;K=IOSize_Unit;E=js}",
    '@{R=SRM.Strings;K=Lun_Performance_Summary_Throughput;E=js}': "@{R=SRM.Strings;K=Throughput_Unit;E=js}",
    '@{R=SRM.Strings;K=Lun_Performance_Summary_Throughput_Read;E=js}': "@{R=SRM.Strings;K=Throughput_Unit;E=js}",
    '@{R=SRM.Strings;K=Lun_Performance_Summary_Throughput_Write;E=js}': "@{R=SRM.Strings;K=Throughput_Unit;E=js}",
    '@{R=SRM.Strings;K=Lun_Performance_Summary_Utilization;E=js}': "@{R=SRM.Strings;K=Utilization_Unit;E=js}"
};

SW.SRM.Formatters.FormatYChartValue = function (value, seriesName) {
    var unit = SeriesNameToUnits[seriesName] ? SeriesNameToUnits[seriesName] : "";
    if (value) {
        if ($.inArray(seriesName, seriesThroughput) > -1) {
            return SW.SRM.Formatters.FormatTinyNumber(value / bytesInMB, true) + " " + unit;
        }
        if ($.inArray(seriesName, seriesIOSize) > -1) {
            return SW.SRM.Formatters.FormatTinyNumber(value / bytesInKB, true) + " " + unit;
        }
        if ($.inArray(seriesName, seriesLatency) > -1) {
            return SW.SRM.Formatters.FormatLatencyNumber(value) + " " + unit;
        }
        return SW.SRM.Formatters.FormatTinyNumber(value, false) + " " + unit;
    } else {
        return "0 " + unit;
    }
};

SW.SRM.Formatters.ChartTooltipFormatter = function () {
    var s = '<table><tr><td><b>' + SW.SRM.Formatters.FormatDate(new Date(this.x)) + '</b></td></tr>';
    $.each(this.points, function (i, point) {
        s += '<tr class="' + point.series.swState + '"><td>' + point.series.name + ': </td><td><b>' + SW.SRM.Formatters.FormatYChartValue(point.y, point.series.name) + '</b></td></td></tr>';
    });
    s += '</table>';
    return s;
};

SW.SRM.Formatters.NumberWithCommas = function (number, index) {
    var lang = Highcharts.getOptions().lang;
    return Highcharts.numberFormat(number, index, lang.decimalPoint, lang.thousandsSep);
}

SW.SRM.Formatters.ShortenText = function (text, maxCharacters) {
    return text.substring(0,maxCharacters);
}

SW.SRM.Formatters.FormatSpeedValueUnit = function (value) {

    var unitString;

    if (value < 1e+3) {
        unitString = "@{R=SRM.Strings;K=Value_Unit_Bit_Per_Second;E=js}"
    }
    else if (value < 1e+6) {
        value /= 1e+3;
        unitString = "@{R=SRM.Strings;K=Value_Unit_Kbit_Per_Second;E=js}";
    }
    else if (value < 1e+9) {
        value /= 1e+6;
        unitString = "@{R=SRM.Strings;K=Value_Unit_Mbit_Per_Second;E=js}";
    }
    else if (value < 1e+12) {
        value /= 1e+9;
        unitString = "@{R=SRM.Strings;K=Value_Unit_Gbit_Per_Second;E=js}";
    }
    else {
        value /= 1e+12;
        unitString = "@{R=SRM.Strings;K=Value_Unit_Tbit_Per_Second;E=js}";
    }

    return Math.round(value) + ' ' + unitString;
}