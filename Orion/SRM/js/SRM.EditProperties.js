﻿/// <reference path="~/Orion/js/jquery-1.7.1/jquery.js" />

var editProperties = SW.Core.namespace("SW.SRM.EditProperties");

editProperties.redirect = function (url) {
    window.location = url;
}

function isPremise(obj) {
    if (typeof (obj) == 'undefined')
        return false;

    if (typeof obj.then === 'function') {
        return true;
    } else {
        return false;
    }
}

editProperties.validate = function (fnArray, fnPostBack) {
    var fnPremises = new Array();

    function handleDone(succ, fail, all, fn) {
        if (succ + fail == all) {
            if (fail == 0) {
                // Submits for server side validation & saving results            
                fn();
            }
            // Else stop progress
            showLoading(false);
            return false;
        }
    }

    var emptyAction = 'javascript: return false;';
    function showLoading(isVisible) {
        if (isVisible) {
            $('#loadingIcon').show();
            // block onclick if not blocked
            $('#BottomSubmitButton').addClass('sw-btn-disabled');
            var bottomSubmitButtonClickAction = $('#BottomSubmitButton').attr('onclick');
            if (bottomSubmitButtonClickAction.indexOf(emptyAction) != 0)
                $('#BottomSubmitButton').attr('onclick', emptyAction + bottomSubmitButtonClickAction);
        } else {
            $('#loadingIcon').hide();
            // remove blocking if blocked
            $('#BottomSubmitButton').removeClass('sw-btn-disabled');
            var bottomSubmitButtonClickAction = $('#BottomSubmitButton').attr('onclick');
            if (bottomSubmitButtonClickAction.indexOf(emptyAction) == 0)
                $('#BottomSubmitButton').attr('onclick', bottomSubmitButtonClickAction.substring(emptyAction.length));
            
        }
    }
    if (!fnArray || fnArray.length == 0)
        return true;
    var validationFuncArray = new Array();
    $.each(fnArray, function (i, fn) {
        try {
            validationFuncArray.push(fn.call(this));
        } catch (e) {
            console.error(e);
            return false;
        }
    });

    if($('#ConnetionPropertyChangedField').val() != 'true')
        return true;

    showLoading(true);
    var isValid = true;
    $.each(validationFuncArray, function (i, fn) {
        try {
            valRes = fn.call(this);
        } catch (e) {
            console.error(e);
            showLoading(false);
            return false;
        }

        var prem = isPremise(valRes);
        if (prem)
            fnPremises.push(valRes);
        if (!prem && !valRes)
        {
            isValid = false;
            showLoading(false);
            return false;
        }
    });
    var succ = fail = 0;
    var all = fnPremises.length;
    if (all == 0)
    {
        // No premises found, just submit
        if (isValid)
            fn();
        else {
            showLoading(false);
        }
    } 
    else 
    {
        $.each(fnPremises, function (i, fn) {
            $.when(fn).then(function (x) {
                succ++;
                handleDone(succ, fail, all, fnPostBack);
            },
            function (x) {
                fail++;
                handleDone(succ, fail, all, fnPostBack);
            });
        });
    }
    // Will never submit without validation
    return false;
}

