﻿this.SRM = {};

SRM.AjaxTree = function () {
    var nodeTemplateId = [];
    var noDataTemplateIds = [];
    var treeNodeIdPrefixes = [];
    var treeRoot = [];
    var rememberExpandedGroups = [];
    var maxBatchSize = [];
    var showDescription = [];
    var stripeRows = [];
    var filters = [];
    var nodeIdCounter = 0;
    var showMoreSingularMessages = [];
    var showMorePluralMessages = [];
    var treeServices = [];
    var expandRootLevelFlags = [];
    var isDemoFlags = [];
    var groupings = [];
    var initialValues = [];
    var visibilityChangedHandler;
    var expandAllItems = [];

    Load = function (treeService, elementId, resId, treeNodeIdPrefix, treeNodeTemplateId, noDataTemplateId, rememberExpanded, batchSize, resourceFilters, showMoreSingularMessage, showMorePluralMessage, showDescriptions, xstripeRows, expandRootLevel, isDemo, grouping, initialValue, visibilityChangedHandlerFunction, expandAll) {
        nodeTemplateId[resId] = treeNodeTemplateId;
        noDataTemplateIds[resId] = noDataTemplateId;
        treeNodeIdPrefixes[resId] = treeNodeIdPrefix;
        rememberExpandedGroups[resId] = rememberExpanded;
        maxBatchSize[resId] = batchSize;
        filters[resId] = resourceFilters;
        showMoreSingularMessages[resId] = showMoreSingularMessage;
        showMorePluralMessages[resId] = showMorePluralMessage;
        treeRoot[resId] = $('#' + elementId);   
        treeServices[resId] = treeService;
        showDescription[resId] = showDescriptions;
        stripeRows[resId] = xstripeRows;
        expandRootLevelFlags[resId] = expandRootLevel;
        isDemoFlags[resId] = isDemo;
        groupings[resId] = grouping;
        initialValues[resId] = initialValue ? initialValue : 0;
        visibilityChangedHandler = visibilityChangedHandlerFunction;
        expandAllItems[resId] = expandAll != 'false';

        SRM_CreateRootLevelBatch(resId, 0, expandAllItems[resId]);
    }

    SRM_CreateTreeLevelBatch = function (resId, nodeId, fromIndex, expandAll) {
        var moreNode = $("#" + SRM_GetUniqueNodeID(resId, nodeId + '_' + 'showMore'));

        if (moreNode) {
            moreNode.remove();
        }

        var node = new SRM_Node(resId, nodeId);

        treeServices[resId].GetTreeNodes(resId, node.itemType(), node.itemId(), fromIndex, maxBatchSize[resId], filters[resId], node.getGroupingParents(), function (items, e) {
            var loadingNode = $("#" + SRM_GetUniqueNodeID(resId, 'loadingNode'));
            if (loadingNode) {
                loadingNode.remove();
            }

            if ((items.length > 0) && (node.expanded())) {
                SRM_CreateTreeLevel(resId, nodeId, items, true);

                if (items[items.length - 1].Id == 'EmptyNode') {
                    if (expandAll) {
                        node.addChild(SRM_CreateLoadingNode(resId).getNode());
                        setTimeout(function () { SRM_CreateTreeLevelBatch(resId, nodeId, fromIndex + items.length - 1, true); }, 100);
                    }
                }
            }
        });
    }

    SRM_CreateRootLevelBatch = function (resId, fromIndex, expandAll) {
        var moreNode = $("#" + SRM_GetUniqueNodeID(resId, treeRoot[resId] + '_' + 'showMore'));

        if (moreNode) {
            moreNode.remove();
        }

        treeServices[resId].GetTreeNodes(resId, groupings[resId][0] || "", initialValues[resId], fromIndex, maxBatchSize[resId], filters[resId], [], function (items, e) {
            var loadingNode = $("#" + SRM_GetUniqueNodeID(resId, 'loadingNode'));
            if (loadingNode) {
                loadingNode.remove();
            }
            if (fromIndex == 0)
                treeRoot[resId].find("*").remove();

            if (items.length > 0) {
                SRM_CreateRootLevel(resId, items, fromIndex);
                if (items[items.length - 1].Id == 'EmptyNode') {
                    if (expandAll) {
                        treeRoot[resId].append(SRM_CreateLoadingNode(resId).getNode());
                        setTimeout(function () { SRM_CreateRootLevelBatch(resId, fromIndex + items.length - 1, true); }, 100);
                    }
                }
            }
            else if (noDataTemplateIds[resId]) {
                treeServices[resId].GetShowGettingStarted(resId, filters[resId], function (show, e) {
                    if (!show)
                        $('#' + SRM_GetNoDataElementID(resId) + ' .SRM_GettingStartedBox').hide();
                    $('#' + SRM_GetNoDataElementID(resId)).show();
                });

                var currentFilters = filters[resId];
                if (currentFilters && currentFilters.replace(/(?:[{}\s])*(?:1=1)*/g, "").length > 1) {
                    treeRoot[resId].append(SRM_CreateTextNode(resId, "@{R=SRM.Strings;K=FilterableResources_FilterSupressAllData;E=xml}").getNode());
                } else {
                    //treeRoot[resId].append(SRM_CreateTextNode(resId, "@{R=SRM.Strings;K=AllStorageObjects_NoData;E=xml}").getNode());
                    SetResourceVisibility(items.length > 0);
                }
            }
        }, function (error) {

            // catch SWQL query exceptions
            var loadingNode = $("#" + SRM_GetUniqueNodeID(resId, 'loadingNode'));
            if (loadingNode) {
                loadingNode.remove();
            }
            if (fromIndex == 0)
                treeRoot[resId].find("*").remove();

            treeRoot[resId].append(SRM_CreateErrorNode(resId, error.get_message()).getNode());
        });
    }

    function SetResourceVisibility(visibility)
    {
        if (visibilityChangedHandler) {
            visibilityChangedHandler(visibility);
        }
    }

    SRM_CreateRootLevel = function (resId, items, fromIndex) {

        try
        {
            for (var i = 0; i < items.length; i++) {
                var node;
                if (items[i].Id != '' && items[i].Id != "EmptyNode") {
                    node = SRM_CreateTreeNode(
                        resId,
                        items[i].Id,
                        0,
                        items[i].Name,
                        items[i].Entity,
                        items[i].Status,
                        items[i].IsExpandable,
                        items[i].Description,
                        items[i].Id,
                        items[i].ItemType,
                        items[i].BaseType,
                        items[i].ClassName,
                        items[i].DetailsLink,
                        items[i].Used,
                        items[i].Size,
                        items[i].Type,
                        items[i].NetObjectName,
                        items[i].NetObjectLink);
                        node.setEven(i % 2 != 0);
                } else {
                    var childNode = SRM_CreateShowMoreNode(resId, treeRoot[resId], parseInt(items[items.length - 1].Name),
                    function () {
                        SRM_CreateRootLevelBatch(resId, fromIndex + items.length - 1, expandAllItems[resId]);
                    }).getNode();
                    treeRoot[resId].append(childNode);
                    childNode.show();
                    continue;
                }

                treeRoot[resId].append(node.getNode());

                if (expandRootLevelFlags[resId] == 'true'
                    || (isDemoFlags[resId] == 'true' && i == 0)) // demo should have first node expanded
                {
                    SRM_ExpandNode(resId, node.id(), true);
                } else if (rememberExpandedGroups[resId] == 'true') {
                    treeServices[resId].IsExpanded(resId, node.id(), function (nodeId, eventArgs) {
                        if (nodeId != '') {
                            SRM_ExpandNode(resId, nodeId, true);
                        }
                    });
                }
            }
        }
        catch (ex) {
            alert(ex.message);
        }
    }

    SRM_CreateTreeLevel = function (resId, parentNodeId, items, checkForExpansion) {
        var node = new SRM_Node(resId, parentNodeId);
        for (var i = 0; i < items.length; i++) {
            var newNodeID;
            var newNode;
            if (items[i].Id != "EmptyNode") {
                newNode = SRM_CreateTreeNode(
                    resId,
                    newNodeID = parentNodeId + '-' + items[i].Id,
                    parseInt(node.level()),
                    items[i].Name,
                    items[i].Entity,
                    items[i].Status,
                    items[i].IsExpandable,
                    items[i].Description,
                    items[i].Id,
                    items[i].ItemType,
                    items[i].BaseType,
                    items[i].ClassName,
                    items[i].DetailsLink,
                    items[i].Used,
                    items[i].Size,
                    items[i].Type,
                    items[i].NetObjectName,
                    items[i].NetObjectLink);

                node.addChild(newNode.getNode());

            } else {
                var childNode = SRM_CreateShowMoreNode(resId, parentNodeId, parseInt(items[items.length - 1].Name),
                    function() {
                        SRM_CreateTreeLevelBatch(resId, parentNodeId, node.getCurrentCount(), expandAllItems[resId]);
                    }).getNode();
                node.addChild(childNode);
                childNode.show();
                continue;
            }

            if (checkForExpansion && rememberExpandedGroups[resId] == 'true' && items[i].IsExpandable) {
                treeServices[resId].IsExpanded(resId, newNode.id(), function (nodeId, eventArgs) {
                    if (nodeId != '') {
                        SRM_ExpandNode(resId, nodeId, checkForExpansion);
                    }
                });
            }
        }
    }

    SRM_CreateTreeNode = function (resId, nodeId, parentLevel, name, entity, status, isExpandable, description, itemId, itemType, baseType, className, detailsLink, used, size, type, netObjectName, netObjectLink) {
        var node = new SRM_Node(resId, nodeTemplateId[resId]);
        node.id(nodeId); // this clones node
        node.level(parentLevel + 1);
        node.itemId(itemId);
        node.name(baseType === 'System.Boolean' ? name == "1" ? "True" : "False" : name);
        node.className(className);
        node.detailsLink(detailsLink);
        node.used(used);
        node.size(size);
        node.type(type, netObjectName, netObjectLink);

        if (showDescription[resId]) {
            node.description(description);
        }

        node.isExpandable(isExpandable);
        node.itemType(itemType);
        node.statusIcon('/Orion/StatusIcon.ashx?size=small&entity=' + entity + '&id=' + itemId + '&status=' + status);
        node.onExpand(function () { SRM_Toggle(resId, nodeId); });

        if (itemId.startsWith("group-") && !name.startsWith("group-"))
            node.onClick(function () { SRM_Toggle(resId, nodeId); });
        node.show();

        return node;
    }

    SRM_CreateTextNode = function (resId, message) {
        var node = new SRM_Node(resId, nodeTemplateId[resId]);
        node.id('textNode');
        node.level(0);
        node.name(message);
        node.description('');
        node.isExpandable(false);
        node.className('SRM-TreeNode-Text');
        node.show();

        return node;
    }

    SRM_CreateLoadingNode = function (resId) {
        var node = new SRM_Node(resId, nodeTemplateId[resId]);
        node.id('loadingNode'); // this clones node
        node.level(0);
        node.name('Loading...');
        node.description('');
        node.isExpandable(false);
        node.statusIcon('/Orion/images/AJAX-Loader.gif');
        node.className('');
        node.show();

        return node;
    }

    SRM_CreateErrorNode = function (resId, message) {
        var node = new SRM_Node(resId, nodeTemplateId[resId]);
        node.id('errorNode');
        node.level(0);
        node.name(message);
        node.description('');
        node.isExpandable(false);
        node.className('SRM-TreeNode-Error');
        node.show();

        return node;
    }

    SRM_CreateShowMoreNode = function (resId, parentId, numMore, callback) {
        var nextBatchSize = Math.min(numMore, maxBatchSize[resId]);
        var node = new SRM_Node(resId, nodeTemplateId[resId]);
        node.id(parentId + '_' + 'showMore'); // this clones node
        node.className('ShowMoreItem');
        node.level(0);
        if (numMore == 1)
            node.name(String.format(showMoreSingularMessages[resId], numMore, nextBatchSize));
        else
            node.name(String.format(showMorePluralMessages[resId], numMore, nextBatchSize));
        node.description('');
        node.isExpandable(false);
        node.statusIcon(null);
        node.onClick(callback);
        node.show();
        return node;
    }

    SRM_Toggle = function (resId, nodeId) {
        var node = new SRM_Node(resId, nodeId);
        if (node.expanded()) {
            node.collapse();
            if (rememberExpandedGroups[resId] == 'true') {
                treeServices[resId].ChangeTree(resId, node.id(), false);
            }
        } else {
            node.addChild(SRM_CreateLoadingNode(resId).getNode());
            node.expand();

            SRM_CreateTreeLevelBatch(resId, nodeId, 0, false);
            if (rememberExpandedGroups[resId] == 'true') {
                treeServices[resId].ChangeTree(resId, node.id(), true);
            }
        }
    }

    SRM_GetResourceErrorID = function (resId) {
        return treeNodeIdPrefixes[resId] + resId + '-ResourceError';
    }

    SRM_GetUniqueNodeID = function (resId, nodeId) {
        return treeNodeIdPrefixes[resId] + resId + '-' + SRM_GetSafeID(nodeId);
    }

    SRM_GetNoDataElementID = function (resId) {
        return treeNodeIdPrefixes[resId] + resId + '-' + noDataTemplateIds[resId];
    }

    SRM_GetSafeID = function (id) {
        return id.replace(/[^\w-]/g, ''); // element id can't non-word characters
    }

    SRM_ExpandNode = function (resId, nodeId, checkForExpansion) {
        var n = new SRM_Node(resId, nodeId);

        n.addChild(SRM_CreateLoadingNode(resId).getNode());

        n.expand();

        treeServices[resId].GetTreeNodes(resId, n.itemType(), n.itemId(), 0, maxBatchSize[resId], filters[resId], n.getGroupingParents(), function (items, e) {
            n.childrenDiv.children().remove();
            SRM_CreateTreeLevel(resId, n.id(), items, checkForExpansion);
        });

        n.expand();
    }

    function UpdateNodeProgressBar(srmNode)
    {
        var usedPercentage = srmNode.size() > 0 ? srmNode.used() * 100 / srmNode.size() : 0;
        var availablePercentage = 100 - usedPercentage;

        var foregroundRect = srmNode.node.find('.ForegroundRect');
        if (foregroundRect)
            foregroundRect.attr('width', availablePercentage + '%')

        var backgroundRect = srmNode.node.find('.BackgroundRect');
        if (backgroundRect)
            backgroundRect.attr('width', usedPercentage + '%')
    }

    function SRM_Node(resId, nodeId) {
        this.resId = resId;
        this.elementId = SRM_GetUniqueNodeID(resId, nodeId);

        this.node = $('#' + this.elementId);
        this.node.nodeId = nodeId;

        this.expandButton = this.node.find('.SRM-TreeNode-expandButton');
        this.childrenDiv = $('#' + this.elementId + '-children');
        this.expandable = true;

        var progressMax = 0;
        var progressValue = 0;

        this.id = function (id) {
            if (typeof id == 'undefined') {
                return this.node.nodeId;
            } else {
                this.elementId = SRM_GetUniqueNodeID(this.resId, id);

                this.node = this.node.clone();
                this.expandButton = this.node.find('.SRM-TreeNode-expandButton');
                this.node.attr('id', this.elementId);
                this.node.nodeId = id;

                this.childrenDiv = this.node.children('div.SRM-TreeNode-children');
                this.childrenDiv.attr('id', this.elementId + '-children');
            }
        },
        this.elementId = function () {
            return this.elementId;
        },
        this.expanded = function () {
            return this.node.hasClass('expanded');
        },
        this.isExpandable = function (isExpandable) {
            if (typeof isExpandable == 'undefined') {
                return this.expandable;
            } else {
                this.expandable = isExpandable;
                if (!isExpandable) {
                    this.expandButton.css('display', 'none');
                } else {
                    this.expandButton.css('display', 'inline');
                }

            }
        },
        this.itemId = function (itemId) {
            if (typeof itemId == 'undefined') {
                return this.node.attr("itemId");
            } else {
                this.node.attr('itemId', itemId);
            }
        },
        this.level = function (level) {
            if (typeof level == 'undefined') {
                return this.node.attr("level");
            } else {
                this.node.attr('level', level);
                this.node.addClass('SRM-Level' + level);
            }
        },
        this.itemType = function (type) {
            if (typeof type == 'undefined') {
                var itemType;
                var level = parseInt(this.level());
                var currentGroupings = groupings[this.resId];

                if (level < currentGroupings.length) {
                    itemType = currentGroupings[level];
                } else if (level == currentGroupings.length) {
                    itemType = 'StorageArrayDirectory';
                } else {
                    itemType = this.node.attr("itemType")
                }

                return itemType;
            } else {
                this.node.attr('itemType', type);
            }
        },
        this.expand = function () {
            this.node.addClass('expanded');
            this.expandButton.attr('src', '/Orion/images/Button.Collapse.gif');
            this.childrenDiv.slideDown('fast');
        },
        this.collapse = function () {
            this.childrenDiv.slideUp('fast');
            this.node.removeClass('expanded');
            this.expandButton.attr('src', '/Orion/images/Button.Expand.gif');
            this.childrenDiv.children().remove();
        },
        this.name = function (name) {
            if (typeof name == 'undefined') {
                return this.node.find('.SRM-TreeNode-itemName').html();
            } else {
                this.node.find('.SRM-TreeNode-itemName').html(name);
            }
        },
        this.description = function (description) {
            if (typeof description == 'undefined') {
                return this.node.find('.SRM-TreeNode-itemDescription').html();
            } else {
                this.node.find('.SRM-TreeNode-itemDescription').html(description);
            }
        },
        this.statusIcon = function (src) {
            if (typeof src == 'undefined') {
                return this.node.find('#SRM-TreeNode-statusIcon').attr('src');
            } else if (src === null) {
                this.node.find('#SRM-TreeNode-statusIcon').hide();
            } else {
                this.node.find('#SRM-TreeNode-statusIcon').attr('src', src);
            }
        },
        this.className = function (className) {
            if (typeof className == 'undefined') {
                return this.node.attr('class');
            } else {
                this.node.removeAttr('class');
                if (className && className.length > 0)
                    this.node.find('.SRM-TreeNode-itemHeader').attr('class', className);
            }
        },
        this.detailsLink = function (detailsLink) {
            var linkControl = this.node.find('.SRM-TreeNode-detailsLink');
            if (typeof detailsLink == 'undefined') {
                return linkControl.attr('href');
            } else {
                if (linkControl && detailsLink && detailsLink.length > 0)
                    linkControl.attr('href', detailsLink);
            }
        },
        this.used = function (used) {
            if (typeof used == 'undefined') {
                return progressValue;
            } else {
                progressValue = used;
                UpdateNodeProgressBar(this);
            }
        },
        this.size = function (size) {
            var sizeControl = this.node.find('#SRM-TreeNode-size');
            if (typeof size == 'undefined') {
                return progressMax;
            } else if (size) {
                progressMax = size;
                if (sizeControl) {
                    sizeControl.html(SW.SRM.Formatters.FormatCapacity(size,0));
                }
                UpdateNodeProgressBar(this);
            }
        },
        this.type = function (type, netObjectName, netObjectLink) {
            var typeControl = this.node.find('#SRM-TreeNode-type');
            if (typeof type == 'undefined' && typeof netObjectName == 'undefined') {
                return typeControl.html();
            } else {
                if (typeControl) {
                    if (netObjectName) {
                        var html = '<a href="' + netObjectLink + '">' + netObjectName + '</a>';
                        typeControl.html(html);
                    } else {
                        typeControl.html(type);
                    }
                }
            }
        },
        this.show = function () {
            this.node.show();
        },
        this.getNode = function () {
            return this.node;
        },
        this.onClick = function (handler) {
            var self = this;

            this.node.find('.SRM-TreeNode-itemName').wrap('<a href="javascript:void(0);" class="SRM-TreeNode-clickWrapper"></a>');

            this.node.find('a.SRM-TreeNode-clickWrapper').click(
            function () {
                handler();
            });
        },
        this.onExpand = function (handler) {
            var self = this;
            if (self.isExpandable()) {
                this.node.find('a.SRM-TreeNode-expandLink').click(
                function () {
                    if (self.isExpandable())
                        handler();
                });
            }
        },
        this.addChild = function (child) {
            this.childrenDiv.append(child);
        },
        this.getCurrentCount = function (child) {
            var emptyNodesCount = this.childrenDiv.find('.ShowMoreItem').length;
            return this.childrenDiv.children().length - emptyNodesCount;
        },
        this.setEven = function (even) {
            if (even)
                this.node.find('.SRM-AjaxTree-Header').addClass('ZebraStripe');
            else
                this.node.find('.SRM-AjaxTree-Header').removeClass('ZebraStripe');
        },
        this.getGroupingParents = function () {
            var parents = [];
            var current = this.node;

            // retrieve parents only if item is within grouping scope
            if (parseInt(this.level()) <= groupings[this.resId].length) {
                parents.push({ itemType: current.attr('itemtype'), name: current.attr('itemid') });
                try {
                    for (var i = 1; i < this.level(); i++) {
                        current = current.parent().parent();
                        parents.push({ itemType: current.attr('itemtype'), name: current.attr('itemid') });
                    }
                } catch (ex) {
                    console.log(ex)
                }
            }

            return parents.reverse();
        }
    }

    return {
        Load: Load
    };
} ();
