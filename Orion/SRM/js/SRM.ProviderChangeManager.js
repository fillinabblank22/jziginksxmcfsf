﻿var providerChangeManager = SW.Core.namespace("SW.SRM.EditProperties.ProviderChangeManager");

providerChangeManager.InitProvider = function(provider, validationFunction)
{
    provider.ConnectionProperties = '';

    provider.GetConnectionProperties = function () {
        var result = new Array();
        for (var i = 0; i < provider.MonitoredControlIds.length; i++)
        {
            var control = $('#' + provider.MonitoredControlIds[i]);
            result.push(control.val() || control.text());
        }
        return result.join(',');
    }

    provider.InitValidation = function ()
    {
        if (provider.GetConnectionProperties() != provider.ConnectionProperties)
            $('#ConnetionPropertyChangedField').val(true);

        return validationFunction;
    }

    $(document).ready(function () {
        provider.ConnectionProperties = provider.GetConnectionProperties();
    });
}
