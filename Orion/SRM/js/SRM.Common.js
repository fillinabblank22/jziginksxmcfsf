﻿SW = SW || {};
SW.SRM = SW.SRM || {};
SW.SRM.Common = SW.SRM.Common || {};

// return the .ajax call result to be able to abort it later
SW.SRM.Common.CallSrmWebService = function(serviceName, methodName, param, onSuccess, onError) {
    var paramString = JSON.stringify(param);

    if (paramString.length === 0) {
        paramString = "{}";
    }

    var callback = $.ajax({
        type: "POST",
        url: serviceName + "/" + methodName,
        data: paramString,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            onSuccess(msg.d);
        },
        error: function (xhr, status, errorThrown) {
            SW.Core.Services.handleError(xhr, onError);
        }
    });
    return callback;
};

SW.SRM.Common.StringFormat = function (format) {
    var args = [];
    for (var i = 1; i < arguments.length; i++) { args.push(arguments[i]); }
    return format.replace(/\{(\d+)\}/g, function (m, i) {
        return args[i];
    });
};

SW.SRM.Common.FillAdditionColumns = function (params) {
    var netObjects = [],
        valueUnknownConst = "@{R=SRM.Strings;K=Value_Unknown;E=js}";
    $.each(params.columnsInfo, function(index, columnInfo) {
        if (columnInfo.name == params.netObjectFieldName) {
            $.each(params.rows, function(rowIndex, row) {
                netObjects.push(row[index]);
            });
        }
    });
    if (typeof params.classesName != 'undefined') {
        SW.Core.Services.callWebService("/Orion/SRM/Services/OrionVolumeService.asmx",
            "GetOrionVolumeInfo", { 'ids': netObjects },
            function (result) {
                for (var i = 0; i < result.length; i++) {
                    if (result[i].NetObject.length > 0) {
                        if (params.classesName.associatedPools) {
                            var poolNames = "";

                            $.each(result[i].Pools, function (index, entry) {
                                if (entry.PoolID > 0) {
                                    var img = '<img src="/Orion/StatusIcon.ashx?entity=Orion.SRM.Pools&amp;status=' + entry.Status + '&amp;size=small">';
                                    var poolName = img + "<a href=\"/Orion/View.aspx?NetObject=@{R=SRM.Strings;K=PoolNetObjectPrefix;E=js}:" + entry.PoolID + "\">" + entry.Name + "</a>";
                                    poolNames = poolNames + (poolNames.length > 0 ? ", " : "") + poolName;
                                }
                            });
                            
                            $("." + params.classesName.associatedPools + params.resourceID + ":contains('%" + result[i].NetObject + "%')").empty().append(poolNames);
                        }
                        
                        if (params.classesName.associatedVolumes) {
                            var volumeNames = "";

                            $.each(result[i].Volumes, function (index, entry) {
                                if (entry.Key > 0) {
                                    var volumeName = "<a href=\"/Orion/View.aspx?NetObject=@{R=SRM.Strings;K=VolumeNetObjectPrefix;E=js}:" + entry.Key + "\">" + entry.Value + "</a>";
                                    volumeNames = volumeNames + (volumeNames.length > 0 ? ", " : "") + volumeName;
                                }
                            });

                            $("." + params.classesName.associatedVolumes + params.resourceID + ":contains('%" + result[i].NetObject + "%')").empty().append(volumeNames);
                        }
                        if (params.classesName.associatedEndpoint)
                            $("." + params.classesName.associatedEndpoint + params.resourceID + ":contains('%" + result[i].NetObject + "%')").text(result[i].Caption);

                        if (params.classesName.volumeSpacePercent) {
                            var volumeSpaceUsedPercent = result[i].VolumeSize != 'null' ? result[i].VolumeSpaceUsed * 100.0 / result[i].VolumeSize : valueUnknownConst;
                            var netObjectElem = $("." + params.classesName.volumeSpacePercent + params.resourceID + ":contains('%" + result[i].NetObject + "%')");
                            netObjectElem.addClass(SW.SRM.Formatters.FormatCellStyle(volumeSpaceUsedPercent, params.volumeSpacePercentWarning, params.volumeSpacePercentCritical));
                            netObjectElem.text(SW.SRM.Formatters.FormatPercentageColumnValue(volumeSpaceUsedPercent, 0));

                            $("." + params.classesName.volumeSpaceUsed + params.resourceID + ":contains('%" + result[i].NetObject + "%')").addClass(SW.SRM.Formatters.FormatCellStyle(volumeSpaceUsedPercent,
                                params.volumeSpacePercentWarning, params.volumeSpacePercentCritical));
                        }

                        if (params.classesName.volumeSpaceUsed) {
                            $("." + params.classesName.volumeSpaceUsed + params.resourceID + ":contains('%" + result[i].NetObject + "%')").text(SW.SRM.Formatters.FormatCapacity(result[i].VolumeSpaceUsed));
                        }
                    }
                }
                $('.SRM_HiddenGridCell', $("#Grid-" + params.resourceID)).removeClass("SRM_HiddenGridCell");
            });
    } else {
        $('.SRM_HiddenGridCell', $("#Grid-" + params.resourceID)).removeClass("SRM_HiddenGridCell");
    }
};

SW.SRM.Common.SetVisible = function (element, isVisible) {
    if (isVisible) {
        element.removeClass('SRM_HiddenField');
    } else {
        element.addClass('SRM_HiddenField');
    }
};

SW.SRM.Common.TooltipHighlighter = function (config) {
    var chartOptions = config;
    var chartHilite = {};
    chartOptions = SW.SRM.Common.EnsureObject(chartOptions, 'chart.events');
    // init series tooltip hilite state
    chartOptions.chart.events.addSeries = function () {
        this.swState = '';
    };
    SW.SRM.Common.EnsureObject(chartOptions, 'plotOptions.series.events');
    // series tooltip hilite state update
    chartOptions.plotOptions.series.events.mouseOver = function () {
        var series = this, update = false;
        if (chartHilite.lastActiveSeries) {
            if (chartHilite.lastActiveSeries !== series) {
                chartHilite.lastActiveSeries.swState = '';
                update = true;
            }
        } else {
            update = true;
        }
        if (update) {
            chartHilite.lastActiveSeries = series;
            series.swState = 'hover';
            series.chart.redraw();
        }
    };

    return chartOptions;
};

SW.SRM.Common.EnsureObject = function (source, path) {
    source = source || {};
    var target = source, modules = path.split('.');
    $.each(modules, function () {
        if (!target.hasOwnProperty(this)) {
            target[this] = {};
        }
        target = target[this];
    });
    return source;
};

SW.SRM.Common.ChartSelectCurrentZoomButton = function (chart) {
    // must be at least 2 (navigator + at least 1 serie)
    if (!chart.series || chart.series.length < 2) {
        return; // data is not ready yet
    }
    if (chart.chartSettings == null || chart.chartSettings == undefined) {
        return;
    }

    $.each(chart.options.rangeSelector.buttons, function (index, item) {
        if (item.tag === chart.chartSettings.initialZoom) {
            chart.rangeSelector.clickButton(index, item, true);
            return false;
        }
    });
};

SW.SRM.Common.RenderArrayStatus = function (status) {
    var result = '';
    switch (status) {
        case 0:
            result = '@{R=SRM.Strings;K=ManageStorageObjects_ArrayStatus;E=js} @{R=SRM.Strings;K=StatusLabel_Unknown;E=js}.';
            break;
        case 1:
            result = '@{R=SRM.Strings;K=ManageStorageObjects_ArrayStatus;E=js} @{R=SRM.Strings;K=StatusLabel_Up;E=js}.';
            break;
        case 2:
            result = '@{R=SRM.Strings;K=ManageStorageObjects_ArrayStatus;E=js} @{R=SRM.Strings;K=StatusLabel_Down;E=js}.';
            break;
        case 3:
            result = '@{R=SRM.Strings;K=ManageStorageObjects_ArrayStatus;E=js} @{R=SRM.Strings;K=StatusLabel_Warning;E=js}.';
            break;
        case 9:
            result = '@{R=SRM.Strings;K=ManageStorageObjects_ArrayStatus;E=js} @{R=SRM.Strings;K=StatusLabel_Unmanaged;E=js}.';
            break;
        case 12:
            result = '@{R=SRM.Strings;K=ManageStorageObjects_ArrayStatus;E=js} @{R=SRM.Strings;K=StatusLabel_Unreachable;E=js}.';
            break;
        case 14:
            result = '@{R=SRM.Strings;K=ManageStorageObjects_ArrayStatus;E=js} @{R=SRM.Strings;K=StatusLabel_Critical;E=js}.';
            break;
        case null:
            result = '@{R=SRM.Strings;K=ManageStorageObjects_ArrayStatus;E=js} @{R=SRM.Strings;K=StatusLabel_Unknown;E=js}.';
            break;
        default:
            result = '@{R=SRM.Strings;K=ManageStorageObjects_Other;E=js}' + '(' + status + ')';
            break;
    }
    return result;
};

SW.SRM.Common.GetEntityNameForStatus = function (netObjectType, isCluster) {
    var entityNameByNetObjectType = {
        "SMSA": "Orion.SRM.StorageArrays",
        "SMSP": "Orion.SRM.Pools",
        "SML": "Orion.SRM.LUNs",
        "SMV": "Orion.SRM.Volumes",
        "SMVS": "Orion.SRM.VServers",
        "SMSAC": "Orion.SRM.Clusters"
    };
    return entityNameByNetObjectType[netObjectType + (isCluster == true ? 'C' : '')];
}

SW.SRM.Common.GetNetObjectLink = function (netObjectType, netObjectId, status, isCluster, name) {
    var result = '';

    if (netObjectId != null) {
        result += '<a href="/Orion/View.aspx?NetObject=' + netObjectType + ':' + netObjectId + '">';
        result += '<img class="StatusIcon" src="/Orion/StatusIcon.ashx?entity=' + SW.SRM.Common.GetEntityNameForStatus(netObjectType, isCluster) + '&status=' + ($.isNumeric(status) ? status : 0) + '&size=small" /> ';
        result += name;
        result += '</a>';
    }

    return result;
}

SW.SRM.Common.GetSeeLogsMessage = function () {
    return '@{R=SRM.Strings;K=ProviderStep_TestConnectionFailed_GeneralMessage; E=js}' + 
            ' <a id="providerStepTestConnectionFailedSeeLogs" href="#">' + 
            '@{R=SRM.Strings;K=ProviderStep_SeeLogs; E=js}' +
            "</a>";
}

SW.SRM.Common.ShowError = function (title, message, errorMessage) {
    var $failMessageHeaderContent = $("#failMessageHeaderContent");
    $failMessageHeaderContent.empty();
    $failMessageHeaderContent.append(title);

    var $failMessageContent = $("#failMessageContent");
    $failMessageContent.empty();
    $failMessageContent.append(message);

    if (errorMessage && errorMessage.length < 50) {
        $failMessageContent.append(" ").append(errorMessage);
        $("#providerStepTestConnectionFailedSeeLogs").remove();
    }
    else if (errorMessage) {
        $("#providerStepTestConnectionFailedSeeLogs").before("<span>" + SW.SRM.Formatters.ShortenText(errorMessage, 40) + "... </span><br/>");
        $("#providerStepTestConnectionFailedSeeLogs").click(function () {
            Ext.Msg.show({
                title: title,
                msg: errorMessage,
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK
            });
        });
    }

    $("#failMessageHeader").show();
    $("#testConnectionFailMsg").show();
}

SW.SRM.Common.ShowErrorEditProperties = function (title, message, errorMessage) {
    var $failMessageHeaderContent = $("#failMessageHeaderContent");
    $failMessageHeaderContent.empty();
    $failMessageHeaderContent.append(title);

    var $failMessageContent = $("#failMessageContent");
    $failMessageContent.empty();
    $failMessageContent.append(message);
    $("#testConnectionPassMsg").hide();
    $("#failMessageHeader").show();
    $("#testConnectionFailMsg").show();

    if (errorMessage && errorMessage.length < 50) {
        $failMessageContent.append(" ").append(errorMessage);
        $("#providerStepTestConnectionFailedSeeLogs").remove();
    }
    else if (errorMessage) {
        $("#providerStepTestConnectionFailedSeeLogs").before("<span>" + SW.SRM.Formatters.ShortenText(errorMessage, 40) + "... </span><br/>");
        $("#providerStepTestConnectionFailedSeeLogs").click(function () {
            Ext.Msg.show({
                title: title,
                msg: errorMessage,
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK
            });
        });
    }
}