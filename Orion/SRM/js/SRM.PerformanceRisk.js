﻿SW = SW || {};
SW.SRM = SW.SRM || {};

SW.SRM.PerformanceRisk = function() {
    var iopsCategory = 'IOPS';
    var latencyCategory = 'Latency';
    var throughputCategory = 'Throughput';
    var errorWeight = 10;
    var detachedResource = '/Orion/SRM/Resources/PerformanceDrillDown/PerformanceDrillDownChart.ascx';
    var performanceServiceMethod = '/Orion/SRM/Services/PerformanceDrillDownService.asmx/GetPerformance';
    var resourceWidth = 800;

    var resourceUniqueId;
    var swqlFilter;
    var getContentTablePointer = function() {
        return $('#Grid-' + resourceUniqueId);
    };
    var getErrorMessagePointer = function() {
        return $('#ErrorMsg-' + resourceUniqueId);
    };
    
    this.initialize = function (_uniqueId, _searchTextBoxId, _searchButtonId, _typeMap, _rowsPerPage, _swqlFilter) {
        resourceUniqueId = _uniqueId;
        swqlFilter = _swqlFilter.replace(/(?:[{}\s])*(?:1=1)*/g, '');

        SW.Core.Resources.CustomQuery.initialize(
            {
                uniqueId: _uniqueId,
                initialPage: 0,
                rowsPerPage: _rowsPerPage,
                allowSort: false,
                columnSettings: {
                    "Resource": {
                        header: SW.SRM.Formatters.FormatColumnTitle('@{R=SRM.Strings;K=PerformanceRisk_Resource_Column;E=js}'),
                        cellCssClassProvider: function (value, row, cellInfo) { return "SRM_Icon_Cell srm_word_wrap"; }
                    },
                    "Type": {
                        header: SW.SRM.Formatters.FormatColumnTitle('@{R=SRM.Strings;K=PerformanceRisk_Type_Column;E=js}'),
                        formatter: function (cellValue, row) {
                            row[4] = _typeMap[row[4]];

                            return cellValue;
                        }
                    },
                    "Array": {
                        header: SW.SRM.Formatters.FormatColumnTitle('@{R=SRM.Strings;K=PerformanceRisk_Array_Column;E=js}'),
                        cellCssClassProvider: function (value, row, cellInfo) { return "srm_word_wrap"; }
                    },
                    "IOPS":
                        {
                            header: SW.SRM.Formatters.FormatColumnTitle('@{R=SRM.Strings;K=PerformanceRisk_IOPS_Column;E=js}'),
                            isHtml: true,
                            formatter: function(cellValue, row, cellInfo) {
                                return formatCellValue(cellValue, row, cellInfo, iopsCategory);
                            },
                            cellCssClassProvider: function(value, row, cellInfo) {
                                return setFormatCellStyle(value, row, cellInfo, iopsCategory) + " IOPS_Drill_Down_" + getNetObject(row, "_") + "_" + _uniqueId;
                            }
                        },
                    "Latency": {
                        header: SW.SRM.Formatters.FormatColumnTitle('@{R=SRM.Strings;K=PerformanceRisk_Latency_Column;E=js}'),
                        isHtml: true,
                        formatter: function(cellValue, row, cellInfo) {
                            return formatCellValue(cellValue, row, cellInfo, latencyCategory);
                        },
                        cellCssClassProvider: function(value, row, cellInfo) {
                            return setFormatCellStyle(value, row, cellInfo, latencyCategory) + " Latency_Drill_Down_" + getNetObject(row, "_") + "_" + _uniqueId;
                        }
                    },
                    "Throughput": {
                        header: SW.SRM.Formatters.FormatColumnTitle('@{R=SRM.Strings;K=PerformanceRisk_Throughput_Column;E=js}'),
                        isHtml: true,
                        formatter: function (cellValue, row, cellInfo) {
                            return formatCellValue(cellValue, row, cellInfo, throughputCategory);
                        },
                        cellCssClassProvider: function(value, row, cellInfo) {
                            return setFormatCellStyle(value, row, cellInfo, throughputCategory) + " Throughput_Drill_Down_" + getNetObject(row, "_") + "_" + _uniqueId;
                        }
                    },
                },
                onLoad: function (rows, columnsInfo) {
                    if ((swqlFilter && swqlFilter.length > 0) && (!rows || rows.length <= 0)) {
                        showCustomErrorText("@{R=SRM.Strings;K=FilterableResources_FilterSupressAllData;E=xml}");
                    }

                    $.each(rows, function (rowIndex, row) {
                        var cssNetObject = getNetObject(row, "_");
                        SW.SRM.Redirector.bindRedirectClick($('.IOPS_Drill_Down_' + cssNetObject + "_" + _uniqueId), function () { clickOnIOPS(getNetObject(row)); });
                        if (cssNetObject.lastIndexOf("SMSA") == -1 && cssNetObject.lastIndexOf("SMSP") == -1 && cssNetObject.lastIndexOf("SMVS") == -1) {
                            SW.SRM.Redirector.bindRedirectClick($('.Latency_Drill_Down_' + cssNetObject + "_" + _uniqueId), function () { clickOnLatency(getNetObject(row)); });
                        }
                        SW.SRM.Redirector.bindRedirectClick($('.Throughput_Drill_Down_' + cssNetObject + "_" + _uniqueId), function () { clickOnThroughput(getNetObject(row)); });
                    });
                },
                searchTextBoxId: _searchTextBoxId,
                searchButtonId: _searchButtonId
            });

        // Order by number of errors, most critical items on top
        // Note: default ordering is not supported by CustomQuery component FB350425
        $('#OrderBy-' + _uniqueId).val('[_TotalErrors] DESC, [Latency] DESC, [IOPS] DESC, [Throughput] DESC');

        SW.Core.Resources.CustomQuery.refresh(_uniqueId);
    };

    var showCustomErrorText = function (text) {
        getContentTablePointer().hide();
        getErrorMessagePointer().text(text).show();
    };

    var formatCellValue = function(cellValue, row, cellInfo, category) {
        var result = '';
        var arrayOrPoolOrVServer = row[4] == 3 || row[4] == 1 || row[4] == 4;
        var arrayOrVServer = row[4] == 3 || row[4] == 4;
        var totalValue = cellValue;

        if (totalValue == null || totalValue === '' || typeof (cellValue) === "undefined") {
            //if netObject is array or pool or vserver and category is latencyCategory then return N/A value
            if (arrayOrPoolOrVServer && category == latencyCategory)
                return '@{R=SRM.Strings;K=Value_NA_Word;E=js}';
            else
                return '@{R=SRM.Strings;K=Value_Unknown;E=js}';
        }

        if (category == iopsCategory) {
            result = totalValue.toFixed();
        } else if (category == latencyCategory) {
            if (arrayOrVServer) {
                result = '@{R=SRM.Strings;K=Value_NA_Word;E=js}';
            } else {
                result = SW.SRM.Formatters.FormatLatencyNumber(totalValue) + ' @{R=SRM.Strings;K=PerformanceRisk_ms_Cell;E=js}';
            }
        } else if (category == throughputCategory) {
            var value = totalValue / bytesInMB;
            result = value.toFixed(2) + ' @{R=SRM.Strings;K=Throughput_Unit;E=js}';
        }
        return result;
    };

    var setFormatCellStyle = function (value, row, cellInfo, category) {
        var totalValue = value;
        if (totalValue == null) {
            return cssclass;
        }
        var cssclass = "";
        var errorCount = row[getCategoryErrorIndex(category)];
        if (errorCount >= 1 && errorCount < errorWeight) {
            cssclass = "SRM_Grid_Warning_Cell";
        } else if (errorCount >= errorWeight) {
            cssclass = "SRM_Grid_Critical_Cell";
        }
        return cssclass;
    };

    var getCategoryErrorIndex = function(category) {
        var errorIndex;
        switch (category) {
        case iopsCategory:
            errorIndex = 12;
            break;
        case latencyCategory:
            errorIndex = 13;
            break;
        case throughputCategory:
            errorIndex = 14;
            break;
        default:
            errorIndex = 0;
        }
        return errorIndex;
    };
    
    var getNetObject = function (row, separator) {
        //Position of _NetObjectPrefix field position in Query
        var NetObjectPosition = 0;
        var NetObjectId = 1;
        if (separator) {
            return row[NetObjectPosition] + separator + row[NetObjectId];
        }
        return row[NetObjectPosition] + ":" + row[NetObjectId];
    }

    var clickOnIOPS = function (netObject) {
        var args = {
            parameters: {
                ResourceFile: detachedResource,
                Width: resourceWidth,
                DataBindMethod: performanceServiceMethod,
                RequestParameter: 'IOPS',
                NetObject: netObject
            }
        };
        SW.SRM.Redirector.Redirect(args);
    };

    var clickOnLatency = function (netObject) {
        var args = {
            parameters: {
                ResourceFile: detachedResource,
                Width: resourceWidth,
                DataBindMethod: performanceServiceMethod,
                RequestParameter: 'Latency',
                NetObject: netObject
            }
        };
        SW.SRM.Redirector.Redirect(args);
    };

    var clickOnThroughput = function (netObject) {
        var args = {
            parameters: {
                ResourceFile: detachedResource,
                Width: resourceWidth,
                DataBindMethod: performanceServiceMethod,
                RequestParameter: 'Throughput',
                NetObject: netObject
            }
        };
        SW.SRM.Redirector.Redirect(args);
    };
};