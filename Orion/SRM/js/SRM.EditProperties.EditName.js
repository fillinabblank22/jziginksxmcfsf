﻿
var editName = SW.Core.namespace("SW.SRM.EditProperties.EditName");


editName.ValidateEditEl = function (el) {
    if ($(el).val() == "")
    {
        $(el).focus();
        $('#editBoxValidation').text('@{R=SRM.Strings;K=EditName_EditShouldNotBeEmpty;E=xml}');
        return false;
    } else {
        $('#editBoxValidation').text('');
    }
    return true;
}

