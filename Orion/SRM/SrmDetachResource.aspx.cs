using System.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI.Models;
using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Orion_SRM_SrmDetachResource : Page, IResourceContainer, IModelProvider
{
    private NetObject _netObject;
    private ResourceInfo _resource;
    private bool _showResourceOnly;

    public NetObject NetObject
    {
        get { return _netObject; }
    }

    public ResourceInfo Resource
    {
        get { return _resource; }
    }

    protected override void OnPreInit(EventArgs e)
    {
        var resourceOnlyParam = Request.QueryString["ResourceOnly"] ?? "0";
        _showResourceOnly = resourceOnlyParam.Equals("1", StringComparison.OrdinalIgnoreCase);

        GetNetObjectAndResourceFromRequest(Request, this.Page, out _netObject, out _resource);

        this.Context.Items[typeof(ViewInfo).Name] = this.Resource.View;

        base.OnPreInit(e);
    }

    protected override void OnInit(EventArgs e)
    {
        if (null == this.NetObject)
        {
            this.Title = Macros.ParseMacros(this.Resource.Title, string.Empty);
        }
        else
        {
            this.Title = string.Format("{0} - {1}", this.NetObject.Name,
                Macros.ParseMacros(this.Resource.Title, this.NetObject.NetObjectID));
        }

        this.divContainer.Style[HtmlTextWriterStyle.Width] = string.Format("{0}px", Resource.Width);

        if (!_showResourceOnly)
            this.divContainer.Style[HtmlTextWriterStyle.MarginLeft] = "12px";

        if (_showResourceOnly)
        {
            ResourceTitle.Visible = false;
        }

		if (Resource.IsAspNetControl)
		{
			BaseResourceControl ctrl = (BaseResourceControl)LoadControl(Resource.File);
			ctrl.Resource = Resource;
			ResourceHostControl host = ResourceHostManager.GetSupportingControl(ctrl, Resource.View.ViewType);

			this.divContainer.Controls.Add(host);
			host.LoadFromRequest();

			host.Controls.Add(ctrl);
		}
		else
		{
			this.divContainer.Controls.Add(new Label { Text = string.Format("{0} is an unsupported legacy resource.", Resource.File) });
		}

        HtmlMeta metaTag = new HtmlMeta();
        metaTag.HttpEquiv = "REFRESH";
        metaTag.Content = string.Format("{0}; URL='{1}'", WebSettingsDAL.AutoRefreshSeconds, this.Request.Url);
        this.Page.Header.Controls.Add(metaTag);

        OrionInclude.CoreThemeStylesheets();
        
        base.OnInit(e);
    }

    private static void GetNetObjectAndResourceFromRequest(HttpRequest request, Page page, out NetObject netObject, out ResourceInfo resource)
    {
        if (string.IsNullOrEmpty(request.QueryString["ResourceID"]))
            throw new InvalidOperationException("ResourceID missing from Query String.");

        netObject = null;
        resource = ResourceManager.GetResourceByID(Convert.ToInt32(request.QueryString["ResourceID"]));

        String netObjectID = request.QueryString["NetObject"];

        if (String.IsNullOrWhiteSpace(netObjectID))
        {
            netObjectID = resource.Properties["NetObjectID"];
        }

        if (!String.IsNullOrWhiteSpace(netObjectID))
        {
            try
            {
                netObject = NetObjectFactory.Create(netObjectID.Trim());
            }
            catch (AccountLimitationException)
            {
                page.Server.Transfer(
                    string.Format("/Orion/AccountLimitationError.aspx?NetObject={0}", netObjectID.Trim())
                    );
            }
        }
    }

    public IPageModelProvider PageModelProvider
    {
        get
        {
            if (_netObject == null)
                return null;
            if (_netObject.GetType() == typeof(StorageArray))
                return new ArrayPageModelProvider((StorageArray)_netObject);
            if (_netObject.GetType() == typeof(VServer))
                return new VServerPageModelProvider((VServer)_netObject);
            return null;
        }
    }
}
