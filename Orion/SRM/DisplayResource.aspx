﻿<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMinReqs.master" AutoEventWireup="true" CodeFile="DisplayResource.aspx.cs" Inherits="Orion_SRM_DisplayResource" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="orion" TagName="NetObjectTips" Src="~/Orion/Controls/NetObjectTips.ascx" %>

<asp:Content ID="Content7" ContentPlaceHolderID="HeadContent" runat="server">
    <orion:Include File="Resources.css" runat="server" />
    <orion:Include ID="Include1" runat="server" File="AsyncView.js" />
    <orion:Include runat="server" File="OrionCore.js" />
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="BodyContent" Runat="Server">
    <orion:NetObjectTips ID="NetObjectTips1" runat="server" />
    <form id="form1" runat="server" class="sw-app-region">
        <asp:ScriptManager ID="scriptManager" runat="server" EnablePageMethods="true" />

        <h1 id="ResourceTitle" runat="server"><%= CommonWebHelper.EncodeHTMLTags(Page.Title)%></h1>

        <div runat="server" id="divContainer">
        </div>
    </form>
</asp:Content>
