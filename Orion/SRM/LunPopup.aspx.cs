﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.NetObjects;
using System.Globalization;
using System.Linq;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Web.Models;

public partial class Orion_SRM_LunPopup : Page
{
    protected Lun Lun { get; set; }
    protected int WarningThresholdDays { get { return (int)SettingsDAL.GetSetting(SRMConstants.WarningThresholdDays).SettingValue; } }
    protected int CriticalThresholdDays { get { return (int)SettingsDAL.GetSetting(SRMConstants.CriticalThresholdDays).SettingValue; } }
    public int CapacityRunout { get; set; }

    protected override void OnPreInit(EventArgs e)
    {
        string netObjectID = Request.QueryString["NetObject"];
        try
        {
            this.Lun = NetObjectFactory.Create(netObjectID) as Lun;
        }
        catch (AccountLimitationException)
        {
            Server.Transfer(
                string.Format(CultureInfo.InvariantCulture, "/Orion/SRM/AccountLimitationPopup.aspx?NetObject={0}", netObjectID));
        }

        base.OnPreInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Lun != null)
        {
            this.StatusLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", SrmWebContent.LUN_Popup_LUN_Name, SrmStatusLabelHelper.GetStatusLabel(Lun.LUN.Status));
            WebHelper.SetValueOrHide(VendorStatusLabel, VendorStatusRow, Lun.LUN.VendorStatusDescription);
            this.UUIDLabel.Text = Lun.LUN.UUID;
            UUIDRow.Visible = !String.IsNullOrEmpty(UUIDLabel.Text);
            this.DefaultControllerLabel.Text = Lun.LUN.DefaultControllerID.ToString(CultureInfo.CurrentUICulture);
            this.CurrentControllerLabel.Text = Lun.LUN.CurrentControllerID.ToString(CultureInfo.CurrentUICulture);
            if (Lun.LUN.VolumeId.HasValue)
            {
                this.ParentVolumeLabel.Text =
                    ServiceLocator.GetInstance<IVolumeDAL>().GetVolumeDetails(Lun.LUN.VolumeId.Value).DisplayName;
            }
            else
            {
                this.ParentVolumeRow.Visible = false;
            }
            this.StoragePoolLabel.Text = String.Join(", ", ServiceLocator.GetInstance<IPoolDAL>().GetPoolsByLunId(Lun.LUN.LUNID).Select(x => x.DisplayName));
            StoragePoolRow.Visible = !String.IsNullOrEmpty(StoragePoolLabel.Text);
            this.ArrayLabel.Text = Lun.StorageArray.Array.DisplayName;
            var vServer = ServiceLocator.GetInstance<IVServerDAL>().GetVServersByLunId(Lun.LUN.LUNID);
            if (vServer != null)
            {
                this.VServerLabel.Text = String.Join(", ", vServer.Select(x => x.DisplayName));
            }
            this.ClusterLabel.Text = Lun.StorageArray.Array.DisplayName;

            if (Lun.StorageArray.Array.IsCluster)
            {
                this.ArrayRow.Visible = false;
            }
            else
            {
                this.VserverRow.Visible = this.ClusterRow.Visible = false;
            }
            this.CurrentControllerRow.Visible = this.DefaultControllerRow.Visible = false;

            if (!String.IsNullOrEmpty(Lun.LUN.RaidType))
            {
                this.RaidConfigLabel.Text = Lun.LUN.RaidType;
                this.trRAIDConfig.Visible = true;
            }

            if (!String.IsNullOrEmpty(Lun.LUN.Protocol))
            {
                this.ProtocolLabel.Text = Lun.LUN.Protocol;
                this.trProtocol.Visible = true;
            }

            this.TotalCapacityLabel.Text = WebHelper.FormatCapacityTotal(Lun.LUN.CapacityTotal);
            var volumes =
                ServiceLocator.GetInstance<ITopologyDAL>().GetOrionVolumeBySRMNetObject(new List<string>
                {
                    Lun.NetObjectID
                });

            var dal = ServiceLocator.GetInstance<IThresholdDAL>();

            IResourceVisibilityService visibilityService = ServiceLocator.GetInstance<IResourceVisibilityService>();
            performanceTable.Visible = visibilityService.IsPerformanceAvailableForArray(this.Lun.StorageArray);

            if (performanceTable.Visible)
            {
                SetPerformanceData(this.Lun, dal);
            }


            this.FileSystemUsedLabel.Text = WebHelper.FormatCapacityTotal(Lun.LUN.CapacityFileSystem);
            this.ConsumedCapacityLabel.Text = WebHelper.FormatCapacityTotal(Lun.LUN.CapacityAllocated);

            CapacityRunout = WebHelper.GetDayDifference(Lun.LUN.CapacityRunout);

            //WebHelper.SetValueOrHide(lblTotalReduction, TotalReductionRow, WebHelper.FormatDataReduction(Lun.LUN.TotalReduction));
            
            ProjectedRunOutLabel.Text = WebHelper.FormatDaysDiff(CapacityRunout);
            WebHelper.MarkProjectedRunOut(this.ProjectedRunOutLabel, CapacityRunout, WarningThresholdDays, CriticalThresholdDays);

            popupChart.ChartColors = new[] { PopupChartHelper.LightBlueChartColor, PopupChartHelper.BlueChartColor, PopupChartHelper.GradientChartColor };
            popupChart.ChartLabelColors = new[] { PopupChartHelper.LightBlueFontColor, PopupChartHelper.BluetFontColor, PopupChartHelper.GradientFontColor };
            popupChart.Values = new[] { (long)volumes.Sum(i => i.VolumeSpaceUsed ?? 0), Lun.LUN.CapacityAllocated ?? 0, Lun.LUN.CapacityFree ?? 0 };
            popupChart.Total = Lun.LUN.CapacityTotal ?? 0;

            ProjectedRunOutRow.Visible = Lun.LUN.Thin;

            #region Other potential issues - Marked by thresholds


            IOPSReadLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(Lun.LUN.IOPSRead ?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.PopUp_IOPSUnit);
            WebHelper.MarkThresholdAndShowRow(IOPSReadLabel, IOPSReadRow, Lun.LUN.IOPSRead,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.LunIOPSRead, Lun.LUN.LUNID));

            IOPSWriteLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(Lun.LUN.IOPSWrite ?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.PopUp_IOPSUnit);
            WebHelper.MarkThresholdAndShowRow(IOPSWriteLabel, IOPSWriteRow, Lun.LUN.IOPSWrite,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.LunIOPSWrite, Lun.LUN.LUNID));

            ThroughputReadLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", WebHelper.FormatCapacityTotal(Lun.LUN.BytesPSRead ?? 0), SrmWebContent.PopUp_secUnit);
            WebHelper.MarkThresholdAndShowRow(ThroughputReadLabel, ThroughputReadRow, Lun.LUN.BytesPSRead,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.LunThroughputRead, Lun.LUN.LUNID));

            ThroughputWriteLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", WebHelper.FormatCapacityTotal(Lun.LUN.BytesPSWrite ?? 0), SrmWebContent.PopUp_secUnit);
            WebHelper.MarkThresholdAndShowRow(ThroughputWriteLabel, ThroughputWriteRow, Lun.LUN.BytesPSWrite,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.LunThroughputWrite, Lun.LUN.LUNID));

            LatencyReadLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(Lun.LUN.IOLatencyRead ?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.PopUp_msUnit);
            WebHelper.MarkThresholdAndShowRow(LatencyReadLabel, LatencyReadRow, Lun.LUN.IOLatencyRead,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.LunLatencyRead, Lun.LUN.LUNID));

            LatencyWriteLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(Lun.LUN.IOLatencyWrite ?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.PopUp_msUnit);
            WebHelper.MarkThresholdAndShowRow(LatencyWriteLabel, LatencyWriteRow, Lun.LUN.IOLatencyWrite,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.LunLatencyWrite, Lun.LUN.LUNID));

            LatencyOtherLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round(Lun.LUN.IOLatencyOther ?? 0, 2, MidpointRounding.AwayFromZero), SrmWebContent.PopUp_msUnit);
            WebHelper.MarkThresholdAndShowRow(LatencyOtherLabel, LatencyOtherRow, Lun.LUN.IOLatencyOther,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.LunLatencyOther, Lun.LUN.LUNID));

            IOSizeTotalLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0}", WebHelper.FormatCapacityTotal(Lun.LUN.IOSizeTotal));
            WebHelper.MarkThresholdAndShowRow(IOSizeTotalLabel, IOSizeTotalRow, Lun.LUN.IOSizeTotal,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.LunIOSizeTotal, Lun.LUN.LUNID));

            IOSizeReadLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0}", WebHelper.FormatCapacityTotal(Lun.LUN.IOSizeRead));
            WebHelper.MarkThresholdAndShowRow(IOSizeReadLabel, IOSizeReadRow, Lun.LUN.IOSizeRead,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.LunIOSizeRead, Lun.LUN.LUNID));

            IOSizeWriteLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0}", WebHelper.FormatCapacityTotal(Lun.LUN.IOSizeWrite));
            WebHelper.MarkThresholdAndShowRow(IOSizeWriteLabel, IOSizeWriteRow, Lun.LUN.IOSizeWrite,
            dal.GetThresholdDynamicOrGlobal(ThresholdDataType.LunIOSizeWrite, Lun.LUN.LUNID));

            if (!IOSizeTotalRow.Visible && !IOSizeReadRow.Visible && !IOSizeWriteRow.Visible && !IOPSReadRow.Visible && !IOPSWriteRow.Visible && !LatencyReadRow.Visible && !LatencyWriteRow.Visible && !LatencyOtherRow.Visible && !ThroughputWriteRow.Visible && !ThroughputReadRow.Visible)
                OtherPotentialIssuesSection.Visible = false;
            #endregion
        }

        popupChart.RenderTotalLabel = false;
        popupChart.Height = PopupChartHelper.Height;
    }

    private void SetPerformanceData(Lun lun, IThresholdDAL thresholdDal)
    {
        NetObjectPerformance lunPerformance = ServiceLocator.GetInstance<ILunDAL>().GetLunPerformance(Lun.LUN.LUNID);

        if (lunPerformance == null)
        {
            return;
        }

        this.TotalIopsLabel.Text = lunPerformance.TotalIOPS.HasValue
            ? string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round((Single)lunPerformance.TotalIOPS, 0, MidpointRounding.AwayFromZero), Resources.SrmWebContent.PopUp_IOPSUnit)
            : SrmWebContent.Value_Unknown;
        WebHelper.MarkThresholds(TotalIopsLabel, Lun.LUN.IOPSTotal, thresholdDal.GetThresholdDynamicOrGlobal(ThresholdDataType.LunIOPSTotal, Lun.LUN.LUNID));

        this.TotalThroughputLabel.Text = lunPerformance.TotalThroughput.HasValue
            ? string.Format(CultureInfo.CurrentCulture, "{0} {1}", WebHelper.FormatCapacityTotal((Single)lunPerformance.TotalThroughput), Resources.SrmWebContent.PopUp_secUnit)
            : SrmWebContent.Value_Unknown;
        WebHelper.MarkThresholds(TotalThroughputLabel, Lun.LUN.BytesPSTotal, thresholdDal.GetThresholdDynamicOrGlobal(ThresholdDataType.LunThroughputTotal, Lun.LUN.LUNID));

        this.TotalLatencyLabel.Text = lunPerformance.Latency.HasValue
            ? string.Format(CultureInfo.CurrentCulture, "{0} {1}", Math.Round((Single)lunPerformance.Latency, 2, MidpointRounding.AwayFromZero), Resources.SrmWebContent.PopUp_msUnit)
            : SrmWebContent.Value_Unknown;
        WebHelper.MarkThresholds(TotalLatencyLabel, Lun.LUN.IOLatencyTotal, thresholdDal.GetThresholdDynamicOrGlobal(ThresholdDataType.LunLatencyTotal, Lun.LUN.LUNID));
    }
}