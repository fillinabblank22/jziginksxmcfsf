﻿using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_SRM_VServerBlockStorageDetailsView : OrionView, IVServerProvider, IModelProvider
{
    protected override void OnInit(EventArgs e)
    {
        this.Title = SolarWinds.Orion.Web.Helpers.UIHelper.NormalizeSpaces(this.ViewInfo.ViewTitle);
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {       

    }
    public override string ViewType
    {
        get { return "SRM VServer Block Storage Details"; }
    }
    public VServer VServer
    {
        get { return new VServer(NetObject.NetObjectID); }
    }

    public IPageModelProvider PageModelProvider
    {
        get { return new VServerPageModelProvider(VServer); }
    }
}