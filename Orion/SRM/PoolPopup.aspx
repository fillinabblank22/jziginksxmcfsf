﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PoolPopup.aspx.cs" Inherits="Orion_SRM_PoolPopup" %>

<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Register TagPrefix="srm" TagName="PopupChart" Src="~/Orion/SRM/Controls/PopupChart.ascx" %>

<h3 class="Status<%=Enum.IsDefined(typeof (PoolStatus), Pool.PoolEntity.Status)?(Pool.PoolEntity.Status).ToString():PoolStatus.Unknown.ToString()%>">
    <%=SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(Pool.Name)%></h3>
<div class="NetObjectTipBody">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2">
                <asp:label runat="server" id="StatusLabel" />
            </td>
        </tr>
        <tr runat="server" id="VendorStatusRow">
            <th style="width: 110px; text-align: left; min-width: 110px;">
                <%= SrmWebContent.StoragePoolDetails_VendorStatusCode%>:
            </th>
            <td>
                <asp:literal runat="server" id="VendorStatusLabel" />
            </td>
        </tr>
        <tr id="ArrayRow" runat="server">
            <th style="width: 110px; text-align: left;"><%= SrmWebContent.BlockStoragePoolDetails_Array %>:</th>
            <td>
                <asp:label runat="server" id="ArrayLabel" />
            </td>
        </tr>
        <tr id="ClusterRow" runat="server">
            <th style="width: 110px; text-align: left;"><%= SrmWebContent.BlockStoragePoolDetails_Cluster %>:</th>
            <td>
                <asp:label runat="server" id="ClusterLabel" />
            </td>
        </tr>
        <tr id="TypeRow" runat="server">
            <th style="width: 110px; text-align: left;"><%= SrmWebContent.BlockStoragePoolDetails_Type %>:</th>
            <td>
                <asp:label runat="server" id="TypeLabel" />
            </td>
        </tr>
        <tr runat="server" id="trRaidConfig" visible="false">
            <th style="width: 110px; text-align: left;"><%= SrmWebContent.ToolTipPoolRaidConfig %>:</th>
            <td>
                <asp:label runat="server" id="lblRAIDConfig" />
            </td>
        </tr>
    </table>

    <div runat="server" id="performanceTable">
        <hr />
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th style="width: 110px; text-align: left;"><%= SrmWebContent.ToolTipPoolTotalIops %>:</th>
                <td>
                    <asp:label runat="server" id="lblTotalIOPS" />
                </td>
            </tr>
            <tr>
                <th style="width: 110px; text-align: left;"><%= SrmWebContent.ToolTipPoolTotalThroughput %>:</th>
                <td>
                    <asp:label runat="server" id="lblTotalThroughput" />
                </td>
            </tr>
            <tr runat="server" id="totalLatencyRow">
                <th style="width: 110px; text-align: left;"><%= SrmWebContent.ToolTipPoolTotalLatency %>:</th>
                <td>
                    <asp:label runat="server" id="lblTotalLatency" />
                </td>
            </tr>
        </table>
    </div>

    <hr />
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th style="width: 110px; text-align: left;"><%= SrmWebContent.ToolTipPoolUserCapacity %>:</th>
            <td>
                <asp:label runat="server" id="lblUserCapcity" />
            </td>
        </tr>
        <tr>
            <th style="width: 110px; text-align: left;"><%= SrmWebContent.ToolTipPoolTUsed %>:</th>
            <td>
                <asp:label runat="server" id="lblUsed" />
            </td>
        </tr>
        <tr runat="server" id="DataReductionRow">
            <th style="width: 110px; text-align: left;"><%= SrmWebContent.UsableCapacityDataReduction%>:</th>
            <td>
                <asp:label runat="server" id="lblDataReduction" />
            </td>
        </tr>
        <!--
        <tr runat="server" id="TotalReductionRow">
            <th style="width: 110px; text-align: left;"><%= SrmWebContent.UsableCapacityTotalReduction %>:</th>
            <td>
                <asp:label runat="server" id="lblTotalReduction" />
            </td>
        </tr>
        -->
        <tr>
            <th style="width: 110px; text-align: left;"><%= SrmWebContent.ToolTipPoolProjectedRunOut %>:</th>
            <td>
                <asp:label runat="server" id="lblProjectedRunOut" />
            </td>
        </tr>
    </table>
    <hr style="margin-bottom: 2px; margin-top: 2px;" />
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><%= SrmWebContent.UserCapacity_Title%>:</th>
            <td></td>
        </tr>
        <tr>
            <td colspan="2">
                <srm:PopupChart ID="popupChartUserSummary" runat="server" />
            </td>
        </tr>
    </table>



    <div id="OtherPotentialIssuesSection" runat="server">
        <hr style="margin-bottom: 2px; margin-top: 2px;" />
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th style="width: 110px; text-align: left; min-width: 110px;"><%= SrmWebContent.Popups_OtherIssues%>:</th>
                <td></td>
            </tr>
            <tr id="IOPSReadRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_ReadIOPS%>:</th>
                <td>
                    <asp:label runat="server" id="IOPSReadLabel" />
                </td>
            </tr>
            <tr id="IOPSWriteRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_WriteIOPS%>: </th>
                <td>
                    <asp:label runat="server" id="IOPSWriteLabel" />
                </td>
            </tr>
            <tr id="ThroughputReadRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_ReadThroughput%>: </th>
                <td>
                    <asp:label runat="server" id="ThroughputReadLabel" />
                </td>
            </tr>
            <tr id="ThroughputWriteRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_WriteThroughput%>: </th>
                <td>
                    <asp:label runat="server" id="ThroughputWriteLabel" />
                </td>
            </tr>
            <tr id="IOSizeTotalRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_TotalIOSize%>: </th>
                <td>
                    <asp:label runat="server" id="IOSizeTotalLabel" />
                </td>
            </tr>
            <tr id="IOSizeReadRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_ReadIOSize%>: </th>
                <td>
                    <asp:label runat="server" id="IOSizeReadLabel" />
                </td>
            </tr>
            <tr id="IOSizeWriteRow" runat="server">
                <th style="width: 110px; text-align: left; min-width: 110px; padding-left: 10px;"><%= SrmWebContent.Popups_WriteIOSize%>: </th>
                <td>
                    <asp:label runat="server" id="IOSizeWriteLabel" />
                </td>
            </tr>
        </table>
    </div>
</div>
