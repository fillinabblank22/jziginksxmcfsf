<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SrmDetachResource.aspx.cs" Inherits="Orion_SRM_SrmDetachResource" MasterPageFile="~/Orion/OrionMinReqs.master"  %>
<%@ Register TagPrefix="orion" TagName="NetObjectTips" Src="~/Orion/Controls/NetObjectTips.ascx" %>

<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
    <orion:Include File="Resources.css" runat="server" />
    <orion:Include ID="Include1" runat="server" File="AsyncView.js" />
    <orion:Include runat="server" File="OrionCore.js" />
    <Orion:Include runat="server" File="visibilityObserver.js"/>
</asp:Content>

<asp:Content ContentPlaceHolderID="BodyContent" runat="server">
	<orion:NetObjectTips ID="NetObjectTips1" runat="server" />
    <form id="form1" runat="server" class="sw-app-region">
        <asp:ScriptManager id="ScriptManager1" runat="server">
		</asp:ScriptManager>

		<h1 id="ResourceTitle" runat="server"><%= CommonWebHelper.EncodeHTMLTags(Page.Title)%></h1>
        
        <div runat="server" id="divContainer">
        
        </div>
    </form>
</asp:Content>
