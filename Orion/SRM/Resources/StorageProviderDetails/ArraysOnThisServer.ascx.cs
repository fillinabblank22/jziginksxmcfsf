﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Resources;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;


public partial class Orion_SRM_Resources_Summary_ArraysOnThisServer : SrmBaseResourceControl
{
    #region Fields

    private string query;

    #endregion

    #region Properties

    /// <summary>
    /// Override default title property.
    /// </summary>
    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.ArrayOnServer_Title;
        }
    }

    /// <summary>
    /// Override help link fragment property.
    /// </summary>
    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceProviderArrays";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IStorageProviderProvider) }; }
    }
   
    #endregion

    private ProviderEntity Provider { get; set; }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        var spp = GetInterfaceInstance<IStorageProviderProvider>();


        if (spp == null)
            return;
        Provider = spp.Provider.Provider;
        query = String.Format(CultureInfo.InvariantCulture,
            @"SELECT p.StorageArrays.DisplayName AS [Array],
                                concat('/Orion/StatusIcon.ashx?entity=',
                                    CASE WHEN p.StorageArrays.IsCluster = true THEN '{3}' ELSE '{2}' END
                                    ,'&status=', ISNULL(p.StorageArrays.Status, 0)) AS [_IconFor_Array],
                                '/Orion/View.aspx?NetObject={1}:' + ToString(p.StorageArrays.[StorageArrayID]) AS [_LinkFor_Array],
		                                p.StorageArrays.Manufacturer AS [Mfr],
		                                p.StorageArrays.Model AS [Model],
		                                p.StorageArrays.SerialNumber AS [SerialNumber],
		                                p.Engine.IP AS [Poller]
                                FROM Orion.SRM.Providers (nolock=true) p
                                WHERE p.ProviderID={0}"
            , spp.Provider.Provider.ID
            , StorageArray.NetObjectPrefix
            , SwisEntities.StorageArrays
            , SwisEntities.Clusters
            );

        CustomTable.UniqueClientID = ScriptFriendlyResourceId;
        CustomTable.SWQL = query;
    }

    protected void ManageClick(object sender, EventArgs e)
    {
        Page.Response.Redirect(string.Format("/Orion/SRM/Admin/StorageObjects.aspx?Provider={0}:{1}", Provider.Name, Provider.IPAddress));
    }
}