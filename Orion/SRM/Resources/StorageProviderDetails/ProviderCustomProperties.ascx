<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProviderCustomProperties.ascx.cs" Inherits="Orion_SRM_Resources_StorageProviderDetails_ProviderCustomProperties" %>
<%@ Register TagPrefix="orion" TagName="CustomPropertyList" Src="~/Orion/SRM/Controls/CustomPropertyList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:CustomPropertyList runat="server" ID="customPropertyList" EntityName="Orion.SRM.ProviderCustomProperties"/>
    </Content>
</orion:resourceWrapper>