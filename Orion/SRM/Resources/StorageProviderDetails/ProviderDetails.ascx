﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProviderDetails.ascx.cs" Inherits="Orion_SRM_Resources_DetailsView_ProviderDetails" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.css" />

<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <table cellspacing="0" id="details_table">
             <tr>
                <td class="PropertyHeader">
                    <div class="SRM_FirstRowDetailsTable">
                        <%= SrmWebContent.ProviderDetail_ProviderStatus%>:
                    </div>
                </td>
                <td class="Property SRM_StatusImage">
                    <img  class="StatusIcon" src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.Providers%>&status=<%=(int)Provider.Status%>&size=small" />
                </td>
                <td class="Property">  
                    <asp:Label runat="server" ID="StatusLabel" />  
                </td>
            </tr>
            <tr runat="server" ID="NameRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.Provider_Name%>:
                </td><td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="NameLabel" />
                </td>
            </tr>
            <tr runat="server" ID="IpRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.Provider_Polling_IPAddress%>:
                </td><td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="IpLabel" />
                </td>
            </tr>
            <tr runat="server" ID="ProviderVersionRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.Provider_Version%>:
                </td><td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="ProviderVersionLabel" />
                </td>
            </tr>
            <tr runat="server" ID="ProviderLastSyncRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.Provider_Last_SyncTime%>:
                </td><td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="ProviderLastSyncLabel" />
                </td>
            </tr>
            <tr runat="server" ID="MachineTypeRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.Provider_MachineType%>:
                </td><td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="MachineTypeLabel" />
                </td>
            </tr>
            <tr runat="server" ID="DNSRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.Provider_DNS%>:
                </td><td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="DNSRowLabel" />
                </td>
            </tr>
            <tr runat="server" ID="SystemNameRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.Provider_SystemName%>:
                </td><td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="SystemNameLabel" />
                </td>
            </tr>
            <tr runat="server" ID="DescriptionRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.Provider_Description%>:
                </td><td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="DescriptionLabel" />
                </td>
            </tr>
            <tr runat="server" ID="LocationRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.Provider_Location%>:
                </td><td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="LocationLabel" />
                </td>
            </tr>
            <tr runat="server" ID="OperationSystemRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.Provider_OperationSystem%>:
                </td><td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="OperationSystemLabel" />
                </td>
            </tr>
            <tr runat="server" ID="TelnetRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.Provider_Telnet%>:
                </td><td></td>
                <td class="Property">
                    <asp:HyperLink runat="server" ID="TelnetLabel" />
                </td>
            </tr>
            <tr runat="server" ID="WebBrowseRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.Provider_WebBrowse%>:
                </td><td></td>
                <td class="Property">
                    <asp:HyperLink runat="server" ID="WebBrowseLabel" />
                </td>
            </tr>
        </table>
        <script type="text/javascript">
            $('#details_table tr:even').addClass('SRM_AltRow1').removeClass('SRM_AltRow2');
            $('#details_table tr:odd').addClass('SRM_AltRow2').removeClass('SRM_AltRow1');
        </script>
    </Content>
</orion:ResourceWrapper>
