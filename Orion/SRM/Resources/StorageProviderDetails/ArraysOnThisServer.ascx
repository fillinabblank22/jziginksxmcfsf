﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ArraysOnThisServer.ascx.cs" Inherits="Orion_SRM_Resources_Summary_ArraysOnThisServer" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Resources" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx"%>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<orion:Include ID="Include1" runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.css" />


<orion:resourceWrapper ID="ResourceWrapper" runat="server">
    <HeaderButtons>
        <a class="EditResourceButton sw-btn-resource sw-btn" id="manage" OnServerClick="ManageClick" runat="server">
            <span class="sw-btn-c">
                <span class="sw-btn-t"><%=SrmWebContent.ArrayOnServer_Manage%></span>
            </span>
        </a>
    </HeaderButtons>
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: '<%= CustomTable.UniqueClientID %>',
                        initialPage: 0,
                        rowsPerPage: <%= this.GetPageSize() %>,
                        allowSort: true,

                        columnSettings: {
                            "Array": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.ArrayOnServer_Array%>'),
                                cellCssClassProvider: function (value, row, cellInfo) { return "SRM_Icon_Cell srm_word_wrap"; }
                            },
                            "Mfr": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.ArrayOnServer_Manufacturer%>'),
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "srm_word_wrap"; }
                            },
                            "Model": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.ArrayOnServer_Model%>'),
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "srm_word_wrap"; }
                            },
                            "SerialNumber": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.ArrayOnServer_SerialNumber%>'),
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "srm_word_wrap"; }
                            },
                            "Poller": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.ArrayOnServer_Poller%>'),
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "srm_word_wrap"; }
                            }
                        }
                    });
                SW.Core.Resources.CustomQuery.refresh('<%=CustomTable.UniqueClientID %>');
            });
        </script>
    </Content>
</orion:resourceWrapper>