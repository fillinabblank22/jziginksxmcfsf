﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.HtmlControls;
using Resources;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Common.Enums;
using Node = SolarWinds.Orion.NPM.Web.Node;
using Label = System.Web.UI.WebControls.Label;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SRM_Resources_DetailsView_ProviderDetails : SrmBaseResourceControl
{
    private string webBrowseTemplate;
    private string telnetTemplate;

    protected override string DefaultTitle
    {
        get
        {
            return Resources.SrmWebContent.Test_Resource;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            // TODO
            return "SRMPHResourceProviderDetails";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.RenderControl;
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IStorageProviderProvider) }; }
    }

    public ProviderEntity Provider;

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            ResourceWrapper.HeaderButtons.Controls.Add(LoadControl("~/Orion/SRM/Controls/ManageNetObjectButton.ascx"));
        var spp = GetInterfaceInstance<IStorageProviderProvider>();
        if (spp == null)
            return;
        Provider = spp.Provider.Provider;

        var nodesId = ServiceLocator.GetInstance<ITopologyDAL>().GetNodeIdsByProviderId(Provider.ID.Value);
        StatusLabel.Text = String.Format(CultureInfo.CurrentCulture, "{0} {1}", Provider.Caption,
            SrmStatusLabelHelper.GetStatusLabel(Provider.Status));
        SetLabelValue(IpLabel, IpRow, Provider.IPAddress);
        SetLabelValue(NameLabel, NameRow, Provider.Caption);
        SetLabelValue(ProviderVersionLabel, ProviderVersionRow, Provider.Version);
        SetLabelValue(ProviderLastSyncLabel, ProviderLastSyncRow, Provider.LastSync.HasValue ? Utils.FormatDateTimeForDisplayingOnWebsite(Provider.LastSync.Value.ToLocalTime(), true) : String.Empty);

        if (nodesId.Any())
        {
            Node node = new Node(NetObjectHelper.GetNetObjectForNode(nodesId.First()));
            SetLabelValue(MachineTypeLabel, MachineTypeRow, node.MachineType);
            SetLabelValue(DNSRowLabel, DNSRow, node.DNSName);
            SetLabelValue(SystemNameLabel, SystemNameRow, node.SysName);
            SetLabelValue(DescriptionLabel, DescriptionRow, node.Description);
            SetLabelValue(LocationLabel, LocationRow, node.Location);
            SetLabelValue(OperationSystemLabel, OperationSystemRow, node.IOSVersion);

            telnetTemplate = "telnet://" + node.HrefIPAddress;
            TelnetLabel.Text = telnetTemplate;
            TelnetLabel.NavigateUrl = telnetTemplate;

            webBrowseTemplate = "http://" + node.HrefIPAddress;
            WebBrowseLabel.Text = webBrowseTemplate;
            WebBrowseLabel.NavigateUrl = webBrowseTemplate;
        }
        else
        {
            MachineTypeRow.Visible = false;
            DNSRow.Visible = false;
            SystemNameRow.Visible = false;
            DescriptionRow.Visible = false;
            LocationRow.Visible = false;
            OperationSystemRow.Visible = false;
            TelnetRow.Visible = false;
            WebBrowseRow.Visible = false;
        }
    }

    private void SetLabelValue(Label label, HtmlTableRow row, string value)
    {
        if (string.IsNullOrEmpty(value))
        {
            row.Visible = false;
            return;
        }
        label.Text = value;
    }

    private string TranslateCredentialType(CredentialType type)
    {
        switch (type)
        {
            case CredentialType.SNMP:
                return SrmWebContent.Provider_Credential_Type_Snmp;
            case CredentialType.SnmpV3:
                return SrmWebContent.Provider_Credential_Type_Snmpv2;
            case CredentialType.SMIS:
                return SrmWebContent.Provider_Credential_Type_Smis;
            case CredentialType.WMI:
                return SrmWebContent.Provider_Credential_Type_Wmi;
            case CredentialType.Ssh:
                return SrmWebContent.Provider_Credential_Type_Ssh;
            default:
                return String.Empty;
        }
    }
}