using System;
using System.Data;
using SolarWinds.Orion.Web.UI;
using System.Text;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;

public partial class Orion_SRM_Resources_Summary_LastEvents : BaseResourceControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // if property Custom has any value then edit mode should be active
            if (!String.IsNullOrEmpty(Resource.Properties["Custom"])) ResourceWrapper.ShowEditButton = true;
            DataTable eventsTable = new DataTable();

            // get period from resources
            string periodName = Resource.Properties["Period"];

            // if there is no valid period set "Today" as default
            if (String.IsNullOrEmpty(periodName))
                periodName = "Today";
 
            DateTime periodBegin = new DateTime();
            DateTime periodEnd = new DateTime();

            string periodTemp = periodName;
            Periods.Parse(ref periodTemp, ref periodBegin, ref periodEnd);
            eventsTable = ServiceLocator.GetInstance<IEventDAL>().GetEvents(periodBegin, periodEnd);

            // adds new column to the table for URL resolving
            eventsTable.Columns.Add("URL");

            foreach (DataRow r in eventsTable.Rows)
            {
                StringBuilder url = new StringBuilder("/Orion/NetPerfMon/Events.aspx?");

                url.AppendFormat("&Period={0}", periodName);
                url.AppendFormat("&EventType={0}", r["EventType"]);

                r["URL"] = url.ToString();
                int color = (int) r["BackColor"];
                r["BackColor"] = ((color & 0xFF0000) >> 16) + (color & 0x00FF00) + ((color & 0x0000FF) << 16);
            }

            this.EventList.LoadData(eventsTable);
        }

    }

    // overriden dafault title
    protected override string DefaultTitle
    {
        get { return Resources.SrmWebContent.Summary_LastEvents_EventSummary; }
    }

    // overriden subtitle of resource
    public override string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Resource.SubTitle))
                return Resource.SubTitle;

            // if subtitle is not specified subtitle is a period
            string subTitle = Periods.GetLocalizedPeriod(Resource.Properties["Period"]);

            if (String.IsNullOrEmpty(subTitle))
            {
                subTitle = Resources.SrmWebContent.Summary_LastEvents_Today;
                Resource.Properties["Period"] = "Today";
            }

            return subTitle;
        }
    }

    // overriden help fragment
    public override string HelpLinkFragment
    {
        get
        {
            string helpLink = Resource.Properties["HelpLink"];
            if (String.IsNullOrEmpty(helpLink))
            {
                helpLink = "OrionPHResourceEventSummary";
                Resource.Properties["HelpLink"] = helpLink;
            }
            return helpLink;
        }
    }

    // overriden location of edit resource
    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditEventSummary.ascx"; }
    }


}