﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LastEvents.ascx.cs" Inherits="Orion_SRM_Resources_Summary_LastEvents" %>
<%@ Register TagPrefix="orion" TagName="EventsSummaryList" Src="~/Orion/Controls/EventSummaryReportControl.ascx" %>

<orion:resourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <orion:EventsSummaryList runat="server" ID="EventList" />
    </Content>
</orion:resourceWrapper>



