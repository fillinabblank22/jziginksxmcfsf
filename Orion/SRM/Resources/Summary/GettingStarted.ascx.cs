﻿using System;
using System.Globalization;
using System.Web.UI;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Common;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Help)]
public partial class Orion_SRM_Summary_GettingStarted : BaseResourceControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Wrapper.ShowEditButton = false;
        Wrapper.Visible = Wrapper.Visible && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
        btnLearnMore.Attributes.Add("onclick", InvariantString.Format("window.open('{0}')", HelpHelper.GetHelpUrl("SRMPHResourceEnrollWizard")));
    }

    protected override string DefaultTitle
    {
        get { return SrmWebContent.GettingStarted_ResourceTitle; }
    }

    protected void BtnRemoveResourceClick(object sender, EventArgs e)
    {
        ResourceManager.DeleteById(Resource.ID);
        Page.Response.Redirect(Request.Url.ToString());
    }

    protected void BtnAddStorageDeviceClick(object sender, EventArgs e)
    {
        Page.Response.Redirect("/Orion/SRM/Config/DeviceTypeStep.aspx");
    }
}