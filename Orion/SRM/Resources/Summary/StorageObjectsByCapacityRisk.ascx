﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StorageObjectsByCapacityRisk.ascx.cs" Inherits="Orion_SRM_Resources_Summary_StorageObjectsByCapacityRisk" %>

<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Resources" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include runat="server" Module="SRM" File="Charts/Charts.SRM.CapacityRisk.js" />
<orion:Include runat="server" Module="SRM" File="SRM.CapacityRisk.js" />
<orion:Include runat="server" Module="SRM" File="SRM.css" />
<orion:Include runat="server" Module="SRM" File="CapacityRisk.css" />
<orion:ResourceWrapper CssClass="srm_overflow_hidden" runat="server" ID="Wrapper">
    <Content>
        <div class="srm-capacityrisk">
            <orion:CustomQueryTable runat="server" ID="CustomTable" />
        </div>
        <script type="text/javascript">
            var typeMap = {
                '<%= SrmWebContent.CapacityRisk_Lun_Title %>' :             <%= (int)StorageObject.Lun %>,
                '<%= SrmWebContent.CapacityRisk_Pool_Title %>' :            <%= (int)StorageObject.Pool %>,
                '<%= SrmWebContent.CapacityRisk_Volume_Title %>' :          <%= (int)StorageObject.Volume %>,
                '<%= SrmWebContent.CapacityRisk_StorageArray_Title %>' :    <%= (int)StorageObject.StorageArray %>,
                '<%= SrmWebContent.CapacityRisk_Vserver_Title %>' :         <%= (int)StorageObject.Vserver %>,
            };
            
            $(function () {
                new SW.SRM.CapacityRisk().initialize('<%= CustomTable.UniqueClientID %>', 
                    '<%= SearchControl.SearchBoxClientID %>', 
                    '<%= SearchControl.SearchButtonClientID %>', 
                    typeMap,
                    '<%= this.GetPageSize() %>',
                    "<%= SwqlFilter %>");
            });
        </script>
    </Content>
</orion:ResourceWrapper>
