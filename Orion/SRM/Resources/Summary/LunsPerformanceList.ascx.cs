using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.Resources;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Web.UI.Models;
using SolarWinds.SRM.Web.UI.Models.LunsByPerformance;
using System;
using System.Collections.Generic;
using SolarWinds.SRM.Common.ServiceProvider;


[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_SRM_Resources_Summary_LunsPerformanceList : SrmBaseResourceControl, IResourceIsInternal
{
    public ResourceSearchControl SearchControl { get; set; }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new Type[] { typeof(IStorageArrayProvider), typeof(IVServerProvider) };
        }
    }

    public bool IsInternal
    {
        get
        {
            return true;
        }
    }

    public override string SubTitle
    {
        get
        {
            if (String.IsNullOrEmpty(base.SubTitle))
            {
                StringComparison comparison = StringComparison.InvariantCultureIgnoreCase;

                var selectedZoom = Resource.Properties[EditXXByPerformance.TimePeriodPropertyKey];

                if (EditXXByPerformance.LastPoll.Equals(selectedZoom, comparison))
                {
                    return SrmWebContent.Luns_PerformanceList_Latest_Poll;
                }
                else if (EditXXByPerformance.LastHour.Equals(selectedZoom, comparison))
                {
                    return SrmWebContent.Luns_PerformanceList_Last_Hour;
                }
            }
            return base.SubTitle;
        }
    }

    protected override string DefaultTitle
    {
        get { return SrmWebContent.LunsPerformanceListResourceTitle; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceLUNsPefromanceSummary";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
    }

    protected override void OnLoad(EventArgs e)
    {
        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        Wrapper.HeaderButtons.Controls.Add(SearchControl);

        base.OnLoad(e);
        var pageModel = this.Page as IModelProvider;
        if (pageModel != null)
        {
            ILunsByPerformanceModel model = (pageModel).PageModelProvider.LunsByPerformance;
            var query = model.GetQuery();

            CustomTable.UniqueClientID = ScriptFriendlyResourceId;
            CustomTable.SWQL = query;
            CustomTable.SearchSWQL =
                model.GetSearchQuery();
        }
        else
            CustomTable.Visible = false;
    }

    protected override bool IsResourceVisible()
    {
        IStorageArrayProvider arrayProvider = GetInterfaceInstance<IStorageArrayProvider>();
        if (arrayProvider != null)
        {
            return ServiceLocator.GetInstance<IResourceVisibilityService>()
                .IsResourceVisibleForArray(arrayProvider.Array, this.AppRelativeVirtualPath);
        }

        IVServerProvider vserverProvider = GetInterfaceInstance<IVServerProvider>();
        if (vserverProvider != null)
        {
            return ServiceLocator.GetInstance<IResourceVisibilityService>()
                .IsResourceVisibleForArray(vserverProvider.VServer.StorageArray, this.AppRelativeVirtualPath);
        }

        return true;
    }
}
