﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StorageObjectsByPerformanceRisk.ascx.cs" Inherits="Orion_SRM_Resources_Summary_StorageObjectsByPerformanceRisk" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Resources" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Register Src="~/Orion/SRM/Controls/Redirector.ascx" TagPrefix="srm" TagName="Redirector" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>

<orion:Include ID="Include1" runat="server" File="OrionCore.js" />
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include ID="Include4" runat="server" Module="SRM" File="SRM.PerformanceRisk.js" />
<orion:Include ID="Include5" runat="server" Module="SRM" File="SRM.css" />
<orion:Include ID="Include6" runat="server" Module="SRM" File="CapacityRisk.css" />

<srm:Redirector ID="Redirector1" runat="server" />
<orion:ResourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div class="srm-capacityrisk">
            <orion:CustomQueryTable runat="server" ID="CustomTable" />
        </div>
        <script type="text/javascript">
            var typeMap = {
                '<%= SrmWebContent.CapacityRisk_Lun_Title %>' :             <%= (int)StorageObject.Lun %>,
                '<%= SrmWebContent.CapacityRisk_Pool_Title %>' :            <%= (int)StorageObject.Pool %>,
                '<%= SrmWebContent.CapacityRisk_Volume_Title %>' :          <%= (int)StorageObject.Volume %>,
                '<%= SrmWebContent.CapacityRisk_StorageArray_Title %>' :    <%= (int)StorageObject.StorageArray %>,
                '<%= SrmWebContent.CapacityRisk_Vserver_Title %>' :         <%= (int)StorageObject.Vserver %>,
            };
            
            $(function () {
                new SW.SRM.PerformanceRisk().initialize('<%= CustomTable.UniqueClientID %>', 
                                                        '<%= SearchControl.SearchBoxClientID %>', 
                                                        '<%= SearchControl.SearchButtonClientID %>', 
                                                        typeMap,
                                                        '<%= this.GetPageSize() %>',
                                                        "<%= SwqlFilter %>");
            });
        </script>
    </Content>
</orion:ResourceWrapper>