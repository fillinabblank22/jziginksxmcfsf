﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllStorageObjects.ascx.cs" Inherits="Orion_SRM_Resources_Summary_AllStorageObjects" %>
<%@ Register src="../../Controls/AjaxTree.ascx" TagName="AjaxTree" TagPrefix="srm" %>

<srm:AjaxTree ID="AjaxTree" runat="server" ServicePath="/Orion/SRM/Services/AllStorageObjectsTree.asmx" />