﻿using System;
using System.Globalization;
using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;
using System.Text;
using System.Linq;
using SolarWinds.SRM.Common.Helpers;
using SolarWinds.SRM.Web.DAL;
using System.Text.RegularExpressions;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Common.ServiceProvider;
using System.Collections.Generic;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_SRM_Resources_Summary_StorageObjectsByPerformanceRisk : SrmBaseResourceControl
{
    #region Properties

    private string query;

    /// <summary>
    /// Override default title property.
    /// </summary>
    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.PerformanceRisk_StorageObjectsByPerformanceRisk_ResourceTitle;
        }
    }

    /// <summary>
    /// Gets help link property.
    /// </summary>
    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceStorageObjectsPerformanceRisks";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditFilteringGridResourceControl.ascx"; }
    }

    protected override string SwqlFilter
    {
        get { return FormatThroughputInFilter(base.SwqlFilter); }
    }

    /// <summary>
    /// Gets or sets search control on resource.
    /// </summary>
    public ResourceSearchControl SearchControl
    {
        get;
        set;
    }
    
    #endregion

    /// <summary>
    /// Page load event handler.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        IResourceVisibilityService visibilityService = ServiceLocator.GetInstance<IResourceVisibilityService>();
        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        Wrapper.HeaderButtons.Controls.Add(SearchControl);
        SearchControl = SearchControl;

        // !!! IMPORTANT !!!  _NetObjectPrefix and _ObjectId  must be the first in the list!
        var sb = new StringBuilder();

        sb.AppendInvariantFormattedLine(@"SELECT [_NetObjectPrefix], [_ObjectId], [Resource], [_IconFor_Resource], 
	                                    [Type], [_LinkFor_Resource], [Array], [_LinkFor_Array], [_IconFor_Array], [Latency], [IOPS], [Throughput], 
		                                [_IOPSError], [_LatencyError], [_ThroughputError], [_TotalErrors]
                                        FROM (");

        /*
         [_NetObjectPrefix], 
         [_ObjectId], 
         [Resource], 
         [_IconFor_Resource], 
         [Type],
         [_LinkFor_Resource],          
         [Array], 
         [_LinkFor_Array], 
         [_IconFor_Array],
         [Latency], 
         [IOPS], 
         [Throughput],
         [_IOPSError], 
         [_LatencyError], 
         [_ThroughputError], 
         [_IOPSError] + [_LatencyError] + [_ThroughputError] AS _TotalErrors
        */

        // --- Pools
        sb.AppendFormat(CultureInfo.InvariantCulture,
@"SELECT '{1}' AS [_NetObjectPrefix], [_ObjectId], [Resource], [_IconFor_Resource], 
	    '{2}' AS [Type], 
	    [_LinkFor_Resource], 
		[Array], [_LinkFor_Array], [_IconFor_Array], 
		[Latency], [IOPS], [Throughput], 
        ISNULL(_IOPSError, 0) AS [_IOPSError], ISNULL(_LatencyError, 0) AS [_LatencyError], ISNULL(_ThroughputError, 0) AS [_ThroughputError],  
        ISNULL(([_IOPSError] + [_LatencyError] + [_ThroughputError]), 0) AS _TotalErrors
FROM  (
	SELECT PoolID as [_ObjectId], [StorageArrayId], [Resource], [_IconFor_Resource], [_LinkFor_Resource], [Throughput], [IOPS], [Latency], 
		SUM([_IOPSError]) AS [_IOPSError], SUM([_LatencyError]) AS [_LatencyError], SUM([_ThroughputError]) AS [_ThroughputError]
	FROM (
	  SELECT PoolID,
		 StorageArrayID as [StorageArrayId],
		 DisplayName as [Resource],
		 concat('/Orion/StatusIcon.ashx?entity={0}&status=', ISNULL([Status], 0)) AS [_IconFor_Resource],
		 DetailsUrl AS [_LinkFor_Resource],
		 BytesPSTotal AS [Throughput],
		 IOPSTotal AS [IOPS], 
		 IOLatencyTotal AS [Latency],
		 {6} AS [_IOPSError],
		 0 AS [_LatencyError],
		 0 AS [_ThroughputError]
	  FROM Orion.SRM.Pools
	  UNION ALL (
		SELECT PoolID,
			 StorageArrayID as [StorageArrayId],
			 DisplayName as [Resource],
			 concat('/Orion/StatusIcon.ashx?entity={0}&status=', ISNULL([Status], 0)) AS [_IconFor_Resource],
			 DetailsUrl AS [_LinkFor_Resource],
			 BytesPSTotal AS [Throughput],
			 IOPSTotal AS [IOPS], 
			 IOLatencyTotal AS [Latency],
			 0 AS [_IOPSError],
			 0 AS [_LatencyError],
			 {8} AS [_ThroughputError]
		FROM Orion.SRM.Pools
	   ))
	GROUP BY [PoolID], [StorageArrayId], [Resource], [_IconFor_Resource], [_LinkFor_Resource], [Throughput], [IOPS], [Latency] 
) PoolsThresholds
LEFT JOIN (
SELECT StorageArrayID, 
       DisplayName as [Array],  
       concat('/Orion/View.aspx?NetObject={3}:', StorageArrayID) AS [_LinkFor_Array],
       concat('/Orion/StatusIcon.ashx?entity=',
                CASE WHEN IsCluster = true THEN '{4}' ELSE '{5}' END
                ,'&status=', ISNULL(Status, 0)) AS [_IconFor_Array],
       TemplateID
FROM Orion.SRM.StorageArrays
WHERE Unmanaged = FALSE 
) SA ON SA.StorageArrayID = PoolsThresholds.StorageArrayId {9}",
            /*0*/SwisEntities.Pools,
            /*1*/Pool.NetObjectPrefix,
            /*2*/SrmWebContent.PerformanceRisk_Pool_Title,
            /*3*/StorageArray.NetObjectPrefix,
            /*4*/SwisEntities.Clusters,
            /*5*/SwisEntities.StorageArrays,
            /*6*/FormatThreshold("Pools.IOPSTotalThreshold"),
            /*7*/"0", // Pools do not have latency threshold
            /*8*/FormatThreshold("Pools.BytesPSTotalThreshold"),
            /*9*/this.FilterOutEntitiesWithoutPerformance(visibilityService, "WHERE"));

        sb.Append(" UNION ALL ( ");

        // --- LUNs
        sb.AppendFormat(CultureInfo.InvariantCulture,
@"SELECT '{1}' AS [_NetObjectPrefix], 
        L.LUNID AS [_ObjectId],
        L.DisplayName as [Resource],
        concat('/Orion/StatusIcon.ashx?entity=Orion.SRM.LUNs&status=', ISNULL(L.Status, 0)) AS [_IconFor_Resource],
        '{2}' AS [Type], 
        L.DetailsUrl AS [_LinkFor_Resource],
	    [Array], [_LinkFor_Array], [_IconFor_Array], 
        L.IOLatencyTotal AS [Latency],
        L.IOPSTotal AS [IOPS],
        L.BytesPSTotal AS [Throughput],	                    
        ISNULL(_IOPSError, 0) AS [_IOPSError], ISNULL(_LatencyError, 0) AS [_LatencyError], ISNULL(_ThroughputError, 0) AS [_ThroughputError],  
        ISNULL(([_IOPSError] + [_LatencyError] + [_ThroughputError]), 0) AS _TotalErrors
FROM Orion.SRM.LUNs L
    LEFT JOIN (
        SELECT LUNID as [_ObjectId], SUM([_IOPSError]) AS [_IOPSError], SUM([_LatencyError]) AS [_LatencyError], SUM([_ThroughputError]) AS [_ThroughputError]
        FROM (
            SELECT LUNID,
			     {6} AS [_IOPSError],
			     0 AS [_LatencyError],
			     0 AS [_ThroughputError]
            FROM Orion.SRM.LUNs (nolock=true)
            WHERE {9}
            UNION ALL (
                SELECT LUNID,
				    0 AS [_IOPSError],
				    {7} AS [_LatencyError],
				    0 AS [_ThroughputError]
				FROM Orion.SRM.LUNs (nolock=true)
	            WHERE {10}
            ) UNION ALL (
                SELECT LUNID,
					0 AS [_IOPSError],
					0 AS [_LatencyError],
					{8} AS [_ThroughputError]
				FROM Orion.SRM.LUNs (nolock=true)
	            WHERE {11}
			)
        ) GROUP BY [LUNID]
    ) LUNsThresholds ON L.LUNID = LUNsThresholds._ObjectId
LEFT JOIN (
SELECT StorageArrayID, 
       DisplayName as [Array],  
       concat('/Orion/View.aspx?NetObject={3}:', StorageArrayID) AS [_LinkFor_Array],
       concat('/Orion/StatusIcon.ashx?entity=',
                CASE WHEN IsCluster = true THEN '{4}' ELSE '{5}' END
                ,'&status=', ISNULL(Status, 0)) AS [_IconFor_Array],
       TemplateID
FROM Orion.SRM.StorageArrays
WHERE Unmanaged = FALSE 
) SA ON SA.StorageArrayID = L.StorageArrayId {12}",
            /*0*/SwisEntities.Lun,
            /*1*/Lun.NetObjectPrefix,
            /*2*/SrmWebContent.PerformanceRisk_Lun_Title,
            /*3*/StorageArray.NetObjectPrefix,
            /*4*/SwisEntities.Clusters,
            /*5*/SwisEntities.StorageArrays,
            /*6*/FormatThreshold("LUNs.IOPSTotalThreshold"),
            /*7*/FormatThreshold("LUNs.IOLatencyTotalThreshold"),
            /*8*/FormatThreshold("LUNs.BytesPSTotalThreshold"),
            /*9*/FormatIfThresholdStateEnabled("LUNs.IOPSTotalThreshold"),
            /*10*/FormatIfThresholdStateEnabled("LUNs.IOLatencyTotalThreshold"),
            /*11*/FormatIfThresholdStateEnabled("LUNs.BytesPSTotalThreshold"),
            /*12*/this.FilterOutEntitiesWithoutPerformance(visibilityService, "WHERE")
        );

        sb.Append(" ) UNION ALL ( ");

        // --- Arrays
        sb.AppendFormat(CultureInfo.InvariantCulture,
@"SELECT [_NetObjectPrefix], 
         [_ObjectId], 
         [Resource], 
         [_IconFor_Resource], 
         [Type],
         [_LinkFor_Resource],          
         [Array], 
         [_LinkFor_Array], 
         [_IconFor_Array],
         [Latency], 
         [IOPS], 
         [Throughput], 
         ISNULL(_IOPSError, 0) AS [_IOPSError], ISNULL(_LatencyError, 0) AS [_LatencyError], ISNULL(_ThroughputError, 0) AS [_ThroughputError],  
         ISNULL(([_IOPSError] + [_LatencyError] + [_ThroughputError]), 0) AS _TotalErrors
 FROM (
        SELECT StorageArrayID AS [_ObjectId], 
                 DisplayName as [Resource], 
                 concat('/Orion/StatusIcon.ashx?entity=',
                        CASE WHEN sa.IsCluster = true THEN '{0}' ELSE '{1}' END
                        ,'&status=', ISNULL([Status], 0)) AS [_IconFor_Resource],
                 DetailsUrl AS [_LinkFor_Resource],
                 '{3}' as [Type], 
                 DisplayName as [Array], 
                 concat('/Orion/View.aspx?NetObject={2}:', StorageArrayID) AS [_LinkFor_Array],
                 concat('/Orion/StatusIcon.ashx?entity=',
                        CASE WHEN sa.IsCluster = true THEN '{0}' ELSE '{1}' END
                        ,'&status=', ISNULL([Status], 0)) AS [_IconFor_Array],
                 BytesPSTotal AS [Throughput],
                 IOPSTotal AS [IOPS], 
                 0 AS [Latency],
                 {4} AS [_IOPSError],
                 {5} AS [_LatencyError],
                 {6} AS [_ThroughputError],
                 '{2}' AS [_NetObjectPrefix],
                 TemplateID
        FROM Orion.SRM.StorageArrays (nolock=true) sa
        WHERE sa.Unmanaged = FALSE {7}
)",
            /*0*/SwisEntities.Clusters,
            /*1*/SwisEntities.StorageArrays,
            /*2*/StorageArray.NetObjectPrefix,
            /*3*/SrmWebContent.PerformanceRisk_StorageArray_Title,
            /*4*/FormatThreshold("sa.IOPSTotalThreshold"),
            /*5*/"0", // Arrays do not have latency threshold
            /*6*/FormatThreshold("sa.BytesPSTotalThreshold"),
            /*7*/this.FilterOutEntitiesWithoutPerformance(visibilityService, "AND")
        );

        sb.Append(" ) UNION ALL ( ");

        // --- Vservers
        sb.AppendFormat(CultureInfo.InvariantCulture,
@"SELECT '{1}' AS [_NetObjectPrefix], [_ObjectId], [Resource], [_IconFor_Resource], 
	    '{2}' AS [Type], 
	    [_LinkFor_Resource], 
	    [Array], [_LinkFor_Array], [_IconFor_Array], 
	    [Latency], [IOPS], [Throughput],
        ISNULL(_IOPSError, 0) AS [_IOPSError], ISNULL(_LatencyError, 0) AS [_LatencyError], ISNULL(_ThroughputError, 0) AS [_ThroughputError],  
        ISNULL(([_IOPSError] + [_LatencyError] + [_ThroughputError]), 0) AS _TotalErrors
FROM  (
SELECT VServerID as [_ObjectId], [StorageArrayId], [Resource], [_IconFor_Resource], [_LinkFor_Resource], [Throughput], [IOPS], [Latency], 
		SUM([_IOPSError]) AS [_IOPSError], SUM([_LatencyError]) AS [_LatencyError], SUM([_ThroughputError]) AS [_ThroughputError]
	FROM (
	  SELECT VServerID,
		 StorageArrayID as [StorageArrayId],
		 DisplayName as [Resource],
		 concat('/Orion/StatusIcon.ashx?entity={0}&status=', ISNULL([Status], 0)) AS [_IconFor_Resource],
		 DetailsUrl AS [_LinkFor_Resource],
		 BytesPSTotal AS [Throughput],	                         
		 IOPSTotal AS [IOPS], 	                
		 0 AS [Latency],
		 {5} AS [_IOPSError],
		 0 AS [_LatencyError],
		 0 AS [_ThroughputError]
	  FROM Orion.SRM.VServers
	  UNION ALL (
		SELECT VServerID,
			 StorageArrayID as [StorageArrayId],
			 DisplayName as [Resource],
			 concat('/Orion/StatusIcon.ashx?entity={0}&status=', ISNULL([Status], 0)) AS [_IconFor_Resource],
			 DetailsUrl AS [_LinkFor_Resource],
			 BytesPSTotal AS [Throughput],	                         
			 IOPSTotal AS [IOPS], 	                
			 0 AS [Latency],
			 0 AS [_IOPSError],
			 0 AS [_LatencyError],
			 {7} AS [_ThroughputError]
		FROM Orion.SRM.VServers
	   ))
	GROUP BY [VServerID], [StorageArrayId], [Resource], [_IconFor_Resource], [_LinkFor_Resource], [Throughput], [IOPS], [Latency] 
) VServersThresholds
LEFT JOIN (
SELECT StorageArrayID, 
       DisplayName as [Array],  
       concat('/Orion/View.aspx?NetObject={3}:', StorageArrayID) AS [_LinkFor_Array],
       concat('/Orion/StatusIcon.ashx?entity={4}&status=', ISNULL(Status, 0)) AS [_IconFor_Array],
       TemplateID
FROM Orion.SRM.StorageArrays
WHERE Unmanaged = FALSE 
) SA ON SA.StorageArrayID = VServersThresholds.StorageArrayId {8}",
            /*0*/SwisEntities.VServers,
            /*1*/VServer.NetObjectPrefix,
            /*2*/SrmWebContent.PerformanceRisk_Vserver_Title,
            /*3*/StorageArray.NetObjectPrefix,
            /*4*/SwisEntities.Clusters,
            /*5*/FormatThreshold("VServers.IOPSTotalThreshold"),
            /*6*/"0", // Vservers do not have latency threshold
            /*7*/FormatThreshold("VServers.BytesPSTotalThreshold"),
            /*8*/ this.FilterOutEntitiesWithoutPerformance(visibilityService, "WHERE")
        );

        sb.Append(" ) UNION ALL ( ");

        // --- volumes
        sb.AppendFormat(CultureInfo.InvariantCulture,
@"SELECT '{1}' AS [_NetObjectPrefix], [_ObjectId], [Resource], [_IconFor_Resource], 
	    '{2}' AS [Type], 
	    [_LinkFor_Resource], 
		[Array], [_LinkFor_Array], [_IconFor_Array], 
		[Latency], [IOPS], [Throughput], 
        ISNULL(_IOPSError, 0) AS [_IOPSError], ISNULL(_LatencyError, 0) AS [_LatencyError], ISNULL(_ThroughputError, 0) AS [_ThroughputError],  
        ISNULL(([_IOPSError] + [_LatencyError] + [_ThroughputError]), 0) AS _TotalErrors
FROM  (
	SELECT VolumeID as [_ObjectId], [StorageArrayId], [Resource], [_IconFor_Resource], [_LinkFor_Resource], [Throughput], [IOPS], [Latency], 
		SUM([_IOPSError]) AS [_IOPSError], SUM([_LatencyError]) AS [_LatencyError], SUM([_ThroughputError]) AS [_ThroughputError]
	FROM (
	  SELECT VolumeID,
		 StorageArrayID as [StorageArrayId],
		 DisplayName as [Resource],
		 concat('/Orion/StatusIcon.ashx?entity={0}&status=', ISNULL([Status], 0)) AS [_IconFor_Resource],
		 DetailsUrl AS [_LinkFor_Resource],
		 BytesPSTotal AS [Throughput],	                    
		 IOPSTotal AS [IOPS], 
		 IOLatencyTotal AS [Latency],
	     {6} AS [_IOPSError],
	     0 AS [_LatencyError],
	     0 AS [_ThroughputError]
	  FROM Orion.SRM.Volumes (nolock=true)
	  UNION ALL (
		SELECT VolumeID,
			 StorageArrayID as [StorageArrayId],
			 DisplayName as [Resource],
			 concat('/Orion/StatusIcon.ashx?entity={0}&status=', ISNULL([Status], 0)) AS [_IconFor_Resource],
			 DetailsUrl AS [_LinkFor_Resource],
			 BytesPSTotal AS [Throughput],	                    
			 IOPSTotal AS [IOPS], 
			 IOLatencyTotal AS [Latency],
			 0 AS [_IOPSError],
			 {7} AS [_LatencyError],
			 0 AS [_ThroughputError]
		FROM Orion.SRM.Volumes (nolock=true)
	  ) UNION ALL (
		SELECT VolumeID,
			 StorageArrayID as [StorageArrayId],
			 DisplayName as [Resource],
			 concat('/Orion/StatusIcon.ashx?entity={0}&status=', ISNULL([Status], 0)) AS [_IconFor_Resource],
			 DetailsUrl AS [_LinkFor_Resource],
			 BytesPSTotal AS [Throughput],	                    
			 IOPSTotal AS [IOPS], 
			 IOLatencyTotal AS [Latency],
			 0 AS [_IOPSError],
			 0 AS [_LatencyError],
			 {8} AS [_ThroughputError]
		FROM Orion.SRM.Volumes (nolock=true)
	   ))
	GROUP BY [VolumeID], [StorageArrayId], [Resource], [_IconFor_Resource], [_LinkFor_Resource], [Throughput], [IOPS], [Latency] 
) VolumesThresholds
LEFT JOIN (
SELECT StorageArrayID, 
       DisplayName as [Array],  
       concat('/Orion/View.aspx?NetObject={3}:', StorageArrayID) AS [_LinkFor_Array],
       concat('/Orion/StatusIcon.ashx?entity=',
                CASE WHEN IsCluster = true THEN '{4}' ELSE '{5}' END
                ,'&status=', ISNULL(Status, 0)) AS [_IconFor_Array],
       TemplateID
FROM Orion.SRM.StorageArrays
WHERE Unmanaged = FALSE 
) SA ON SA.StorageArrayID = VolumesThresholds.StorageArrayId {9}
)
)",
            /*0*/SwisEntities.Volume,
            /*1*/Volume.NetObjectPrefix,
            /*2*/SrmWebContent.PerformanceRisk_Volume_Title,
            /*3*/StorageArray.NetObjectPrefix,
            /*4*/SwisEntities.Clusters,
            /*5*/SwisEntities.StorageArrays,
            /*6*/FormatThreshold("Volumes.IOPSTotalThreshold"),
            /*7*/FormatThreshold("Volumes.IOLatencyTotalThreshold"),
            /*8*/FormatThreshold("Volumes.BytesPSTotalThreshold"),
            /*9*/ this.FilterOutEntitiesWithoutPerformance(visibilityService, "WHERE")
            );

        query = sb.ToString();
        CustomTable.UniqueClientID = ScriptFriendlyResourceId;

        CustomTable.SWQL = new ExpressionBuilder(query).Where(SwqlFilter).ToString();
        CustomTable.SearchSWQL = new ExpressionBuilder(query).Where(SwqlFilter, SearchSWQL).ToString();
    }

    private string FormatThreshold(string thresholdEntity)
    {
        return String.Format(CultureInfo.InvariantCulture,
@"CASE WHEN {0}.IsLevel2State = true THEN 10 ELSE 0 END + CASE WHEN {0}.IsLevel1State = true THEN 1  ELSE 0 END",
                             thresholdEntity);
    }

    private string FormatIfThresholdStateEnabled(string thresholdEntity)
    {
        return String.Format(CultureInfo.InvariantCulture, @"{0}.IsLevel2State = true OR {0}.IsLevel1State = true",thresholdEntity);
    }

    private string FormatThroughputInFilter(string filter)
    {
        return Regex.Replace(filter, 
                            "(throughput)(\\D*)([\\d.]+)",
                            m => m.Groups[1].Value + m.Groups[2].Value + StringHelper.ConvertToBytes(m.Groups[3].Value), 
                            RegexOptions.IgnoreCase | RegexOptions.Multiline);
    }

    private string FilterOutEntitiesWithoutPerformance(IResourceVisibilityService visibilityService, string keyword)
    {
        List<Guid> guids = visibilityService.GetAllTemplateGuidsWithDisabledPerformance().ToList();
        return !guids.Any() ? string.Empty : $"{keyword} SA.TemplateID NOT IN ('{String.Join("','", guids)}')";
    }
}
