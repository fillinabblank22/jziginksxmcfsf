﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GettingStarted.ascx.cs" Inherits="Orion_SRM_Summary_GettingStarted" %>
<%@ Import Namespace="Resources" %>
<orion:Include ID="Include2" runat="server" Module="SRM" File="GettingStarted.css" />

<orion:resourceWrapper runat="server" ID="Wrapper" >
    <Content>
        <div class="SRM-GettingStarted">
            <div class="SRM-GettingStarted-InnerTable">
                <table cellspacing="0">
                    <tr>
                        <td>
                            <span><%=SrmWebContent.GettingStarted_AddStorageDevice_Title %></span>
                        </td>
                        <td>
                            <div class="SRM-GettingStarted-Button-Width">
                                <button id="btnAddStorageDevice" runat="server" onserverclick="BtnAddStorageDeviceClick" type="button"
                                    class="SRM-GettingStarted-Button sw-btn-primary sw-btn" tooltip="<%$ Resources: SrmWebContent,  GettingStarted_AddStorageDevice_Title%>">
                                    <%=SrmWebContent.GettingStarted_AddStorageDevice_Button %>
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span><%=SrmWebContent.GettingStarted_LearnMore_Title %></span>
                        </td>
                        <td>
                            <div class="SRM-GettingStarted-Button-Width">
                                <button id="btnLearnMore" runat="server" type="button"
                                    class="SRM-GettingStarted-Button sw-btn-primary sw-btn" tooltip="<%$ Resources: SrmWebContent,  GettingStarted_LearnMore_Title%>">
                                    <%=SrmWebContent.GettingStarted_LearnMore_Button %>
                                </button>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                <span class="SRM-GettingStarted-RemoveButton">
                    <button class="srm-uppercase sw-btn-secondary sw-btn sw-btn-c sw-btn-t" type="button" id="btnRemove" runat="server" tooltip="<%$ Resources: SrmWebContent, GettingStarted_Remove_Button %>"
                        onserverclick="BtnRemoveResourceClick">
                        <%= SrmWebContent.GettingStarted_Remove_Button %>
                    </button>
                </span>
            </div>
        </div>
    </Content>
</orion:resourceWrapper>


