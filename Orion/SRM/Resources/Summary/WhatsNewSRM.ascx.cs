﻿using System;
using System.Web.UI;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.ResourcesMetadata;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_SRM_Resources_Summary_WhatsNewSrm : BaseResourceControl
{
    public string ProductVersion { get; set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ProductVersion = SolarWinds.SRM.Web.WebHelper.GetProductVersion();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Wrapper.ShowEditButton = false;
        btnNewFeature.Text = $"{ SrmWebContent.WhatsNew_LearnMore_Button} {ProductVersion}";
    }

    protected override string DefaultTitle
    {
        get { return SrmWebContent.WhatsNew_Resource_Title; }
    }

    public sealed override string DisplayTitle
    {
        get { return $"{SrmWebContent.WhatsNew_Resource_Title} {ProductVersion}"; }
    }

    protected void btnRemoveResource_Click(object sender, EventArgs e)
    {
        ResourceManager.DeleteById(Resource.ID);
        Page.Response.Redirect(Request.Url.ToString());
    }
}