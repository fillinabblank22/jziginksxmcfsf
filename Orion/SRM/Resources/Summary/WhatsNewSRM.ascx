﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WhatsNewSRM.ascx.cs" Inherits="Orion_SRM_Resources_Summary_WhatsNewSrm" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Resources" %>

<orion:resourceWrapper ID="Wrapper" runat="server" style="display:none"> 
    <Content>
        <div id="whatsnew">
        <table>
            <colGroup>
                <col width="auto" />
                <col width="100%" />
            </colGroup>
            <tr>
                <td valign="top">
                    <img alt="" src="<%= SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("SRM","WhatsNew/icon_new.gif") %>" border="0" />
                </td>
                <td style="padding-left: 10px;">
                    <%=SrmWebContent.WhatsNew_InnerHtml%>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <div class="sw-btn-bar">
                        <orion:LocalizableButtonLink NavigateUrl='https://support.solarwinds.com/Success_Center/Storage_Resource_Monitor_(SRM)/release_notes' runat="server" ID="btnNewFeature" LocalizedText="CustomText" DisplayType="Primary" rel="noopener noreferrer" Target="_blank" />
                        <orion:LocalizableButton runat="server" ID="btnRemoveResource" LocalizedText="CustomText" Text="<%$ Resources: SrmWebContent, WhatsNew_Remove_Button %>" DisplayType="Secondary" OnClick="btnRemoveResource_Click" />
                    </div>
                </td>
            </tr>
        </table>
        </div>
    </Content>
</orion:resourceWrapper>
