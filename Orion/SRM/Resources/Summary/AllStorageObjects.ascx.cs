﻿using System;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Web.Resources;
using SolarWinds.SRM.Web;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Linq;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common.Helpers;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SRM_Resources_Summary_AllStorageObjects : SRMBaseResource
{
    private static Log log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceAllStorageObjects";
        }
    }

    #region Resource Properties

    protected override string DefaultTitle
    {
        get { return Resources.SrmWebContent.SummaryDetails_Storages_defaultTitle; }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/SRM/Controls/EditResourceControls/EditAllStorageObjects.ascx";
        }
    }

    public override string SubTitle
    {
        get
        {
            if (Resource.GetSubProperties(ResourcePropertiesKeys.Grouping).Length == 0)
            {
                return Resources.SrmWebContent.SummaryDetails_Storages_noGroupingLabel;
            }

            string subtitle = string.Join(", ", Resource.GetSubProperties(ResourcePropertiesKeys.Grouping)
                                                        .Select(p => p.StartsWith("CustomProperty") 
                                                                        ? p.Replace("CustomProperty.", "") 
                                                                        : p));

            return StringHelper.FormatInvariant(Resources.SrmWebContent.SummaryDetails_Storages_groupedByVendorLabel, subtitle);
        }
    }

    #endregion

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        AjaxTree.ResourceId = this.Resource.ID.ToString();
        AjaxTree.Filters = Resource.Properties[ResourcePropertiesKeys.SwqlFilter] ?? string.Empty;
        AjaxTree.Grouping = Resource.GetSubProperties(ResourcePropertiesKeys.Grouping, "Vendor");
        AjaxTree.RememberExpandedGroups = this.Resource.Properties[ResourcePropertiesKeys.RememberCollapseState] ?? "true";
        AjaxTree.ExpandRootLevel = this.Resource.Properties[ResourcePropertiesKeys.ExpandRootLevel] ?? "false";
        AjaxTree.ShowMoreSingularMessage = Resources.SrmWebContent.SummaryDetails_Storages_showNextSingularMessage;
        AjaxTree.ShowMorePluralMessage = Resources.SrmWebContent.SummaryDetails_Storages_showNextPluralMessage;
        AjaxTree.ExpandAll = "false";

        if (this.Resource.Properties[SolarWinds.SRM.Web.Resources.PageSizeHelper.PropertyKey] != null)
            AjaxTree.BatchSize = this.Resource.Properties[SolarWinds.SRM.Web.Resources.PageSizeHelper.PropertyKey];
    }
}