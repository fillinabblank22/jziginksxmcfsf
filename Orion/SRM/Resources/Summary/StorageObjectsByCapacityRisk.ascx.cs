﻿using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.DAL;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.Resources;
using SolarWinds.SRM.Web.UI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_SRM_Resources_Summary_StorageObjectsByCapacityRisk : SrmBaseResourceControl
{
    public ResourceSearchControl SearchControl { get; set; }
    private string query;
    protected void Page_Load(object sender, EventArgs e)
    {
        WebHelper.IncludeCharts();
        
        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        Wrapper.HeaderButtons.Controls.Add(SearchControl);
        SearchControl = SearchControl;

        query = String.Format(CultureInfo.InvariantCulture, @"
SELECT [_ObjectId], 
       [Resource],
       [_IconFor_Resource], 
       [_LinkFor_Resource], 
       [Type], 
       [Array], 
       [_LinkFor_Array],        
       '' AS [Last7Days],
       {4} AS [Warning], 
       {5} AS [Critical], 
       100 AS [ATCapacity], 
       [_TotalCapacity], 
       CASE WHEN [_UsedCapacityPercentage] > 100 THEN 100 ELSE [_UsedCapacityPercentage] END AS _UsedCapacityPercentage,
       [_CapacityRunoutOffset],
       [_CapacityRunoutSlope],
       [_IconFor_Array]
FROM (
	    SELECT LUNID AS [_ObjectId],
               DisplayName as [Resource], 
               concat('/Orion/StatusIcon.ashx?entity={0}&status=', ISNULL(Status, 0)) AS [_IconFor_Resource],
               [DetailsUrl] AS [_LinkFor_Resource],
               '{6}' AS [Type], 
               LUNs.StorageArray.DisplayName as [Array],  
               LUNs.StorageArray.DetailsUrl AS [_LinkFor_Array],
               concat('/Orion/StatusIcon.ashx?entity=',
                                    CASE WHEN LUNs.StorageArray.IsCluster = true THEN '{10}' ELSE '{3}' END
                                    ,'&status=', ISNULL(LUNs.StorageArray.Status, 0)) AS [_IconFor_Array],
               CapacityTotal AS [_TotalCapacity], 
	           ((CapacityAllocated * 1.0 / CapacityTotal) * 100) AS [_UsedCapacityPercentage],
               CapacityRunoutSlope AS [_CapacityRunoutSlope],
               CapacityRunoutOffset AS [_CapacityRunoutOffset]
        FROM Orion.SRM.LUNs (nolock=true)
        WHERE [Thin] = true
          AND [CapacityTotal] > 0
          AND LUNs.StorageArray.Unmanaged = FALSE

        UNION ALL (

        SELECT PoolID AS [_ObjectId], 
               DisplayName as [Resource], 
               concat('/Orion/StatusIcon.ashx?entity={1}&status=', ISNULL(Status, 0)) AS [_IconFor_Resource],
               [DetailsUrl] AS [_LinkFor_Resource],
               '{7}' as [Type], 
               Pools.StorageArray.DisplayName as [Array], 
               Pools.StorageArray.DetailsUrl AS [_LinkFor_Array], 
               concat('/Orion/StatusIcon.ashx?entity=',
                                    CASE WHEN Pools.StorageArray.IsCluster = true THEN '{10}' ELSE '{3}' END
                                    ,'&status=', ISNULL(Pools.StorageArray.Status, 0)) AS [_IconFor_Array],
               CapacityUserTotal AS [_TotalCapacity], 
               ((CapacityUserUsed * 1.0 / CapacityUserTotal) * 100) AS [_UsedCapacityPercentage],
               CapacityRunoutSlope AS [_CapacityRunoutSlope],
               CapacityRunoutOffset AS [_CapacityRunoutOffset]
        FROM Orion.SRM.Pools (nolock=true)
        WHERE [Thin] = true
          AND [CapacityUserTotal] > 0
          AND Pools.StorageArray.Unmanaged = FALSE)

        UNION ALL (

        SELECT VolumeID AS [_ObjectId],
               DisplayName as [Resource],
               concat('/Orion/StatusIcon.ashx?entity={2}&status=', ISNULL(Status, 0)) AS [_IconFor_Resource],
               [DetailsUrl] AS [_LinkFor_Resource],
               '{8}' as [Type], 
               Volumes.StorageArray.DisplayName as [Array],  
               Volumes.StorageArray.DetailsUrl AS [_LinkFor_Array],
               concat('/Orion/StatusIcon.ashx?entity=',
                                    CASE WHEN Volumes.StorageArray.IsCluster = true THEN '{10}' ELSE '{3}' END
                                    ,'&status=', ISNULL(Volumes.StorageArray.Status, 0)) AS [_IconFor_Array],
               CapacityTotal AS [_TotalCapacity],
               ((ISNULL(CapacityFileSystem, 0) * 1.0 / CapacityTotal) * 100) AS [_UsedCapacityPercentage],
               CapacityRunoutSlope AS [_CapacityRunoutSlope],
               CapacityRunoutOffset AS [_CapacityRunoutOffset]
        FROM Orion.SRM.Volumes (nolock=true)
        WHERE [CapacityTotal] > 0
          AND Volumes.StorageArray.Unmanaged = FALSE)

        UNION ALL (

        SELECT StorageArrayID AS [_ObjectId], 
               DisplayName as [Resource], 
               concat('/Orion/StatusIcon.ashx?entity=',
                                    CASE WHEN sa.IsCluster = true THEN '{10}' ELSE '{3}' END
                                    ,'&status=', ISNULL([Status], 0)) AS [_IconFor_Resource],
               [DetailsUrl] AS [_LinkFor_Resource],
               '{9}' as [Type], 
               DisplayName as [Array], 
               [DetailsUrl] AS [_LinkFor_Array],
               concat('/Orion/StatusIcon.ashx?entity=',
                                    CASE WHEN sa.IsCluster = true THEN '{10}' ELSE '{3}' END
                                    ,'&status=', ISNULL([Status], 0)) AS [_IconFor_Array],
               CapacityUserTotal AS [_TotalCapacity], 
               ((CapacityUserUsed * 1.0 / CapacityUserTotal) * 100) AS [_UsedCapacityPercentage],
               CapacityRunoutSlope AS [_CapacityRunoutSlope],
               CapacityRunoutOffset AS [_CapacityRunoutOffset]
        FROM Orion.SRM.StorageArrays (nolock=true) sa
        WHERE [CapacityUserTotal] > 0
          AND sa.Unmanaged = FALSE)

        UNION ALL (
  SELECT VServerID AS [_ObjectId], 
               DisplayName as [Resource], 
               concat('/Orion/StatusIcon.ashx?entity={11}&status=', ISNULL(Status, 0)) AS [_IconFor_Resource],
               [DetailsUrl] AS [_LinkFor_Resource],
               '{12}' as [Type], 
               VServers.StorageArray.DisplayName as [Array], 
               VServers.StorageArray.DetailsUrl AS [_LinkFor_Array], 
               concat('/Orion/StatusIcon.ashx?entity={10}&status=', ISNULL(VServers.StorageArray.Status, 0)) AS [_IconFor_Array],
               CapacitySubscribed AS [_TotalCapacity], 
               ((CapacityAllocated * 1.0 / CapacitySubscribed) * 100) AS [_UsedCapacityPercentage],
               CapacityRunoutSlope AS [_CapacityRunoutSlope],
               CapacityRunoutOffset AS [_CapacityRunoutOffset]
        FROM Orion.SRM.VServers (nolock=true)
        WHERE [CapacitySubscribed] > 0
          AND VServers.StorageArray.Unmanaged = FALSE
)
)",
    SwisEntities.Lun,
    SwisEntities.Pools,
    SwisEntities.Volume,
    SwisEntities.StorageArrays,
    (int)SettingsDAL.GetSetting(SRMConstants.WarningThresholdCapacityRunOut).SettingValue,
    (int)SettingsDAL.GetSetting(SRMConstants.CriticalThresholdCapacityRunOut).SettingValue,
    SrmWebContent.CapacityRisk_Lun_Title,
    SrmWebContent.CapacityRisk_Pool_Title,
    SrmWebContent.CapacityRisk_Volume_Title,
    SrmWebContent.CapacityRisk_StorageArray_Title,
    SwisEntities.Clusters,
    SwisEntities.VServers,
    SrmWebContent.CapacityRisk_Vserver_Title
);

        CustomTable.UniqueClientID = ScriptFriendlyResourceId;
        CustomTable.SWQL = new ExpressionBuilder(query).Where(SwqlFilter).ToString();
        CustomTable.SearchSWQL = new ExpressionBuilder(query).Where(SwqlFilter, SearchSWQL).ToString();
        //CustomTable.
    }

    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.CapacityRisk_StorageObjectsByCapacityRisk_ResourceTitle;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceStorageObjectsCapacityRisks";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditFilteringGridResourceControl.ascx"; }
    }
}
