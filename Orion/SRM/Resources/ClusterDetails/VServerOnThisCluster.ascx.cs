﻿using System;
using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;
using System.Globalization;
using SolarWinds.SRM.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SRM_Resources_ClusterDetails_VServersOnThisCluster : ArrayBaseResourceControl
{
    #region Fields

    private string query;

    #endregion

    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.Resource_Title_VserversOnThisCluster;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceVservers";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
    }

    /// <summary>
    /// OnLoad override method.
    /// </summary>
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        query = String.Format(CultureInfo.InvariantCulture,
                              @"SELECT v.[Caption] AS Name,
                                       '/Orion/StatusIcon.ashx?entity={2}&status=' + ToString(ISNULL([Status], 0)) +'&size=small' AS [_IconFor_Name],  
                                       '/Orion/View.aspx?NetObject={1}:' + ToString(v.[VServerID]) AS [_LinkFor_Name]  ,
                                       v.[CapacitySubscribed] as [TotalCapacity],
                                       v.[CapacityAllocated] as [Used],                                                                
                                       case when v.[CapacitySubscribed] = 0 then 0 else (((v.[CapacityAllocated] * 1.0 ) / v.[CapacitySubscribed]) * 100) end as [UsedPercentage]
                                FROM Orion.SRM.vServers (nolock=true) v
                                WHERE v.[StorageArrayID]={0}",
                              Array.Array.StorageArrayId,
                              VServer.NetObjectPrefix,                              
                              SwisEntities.VServers);
        CustomTable.UniqueClientID = ScriptFriendlyResourceId;
        CustomTable.SWQL = query;        
    }
}