﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VServerOnThisCluster.ascx.cs" Inherits="Orion_SRM_Resources_ClusterDetails_VServersOnThisCluster" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>


<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Resources" %>

<orion:Include ID="Include1" runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include ID="Include3" runat="server" Module="SRM" File="SRM.Common.js" />
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.css" />


<orion:ResourceWrapper ID="Wrapper" runat="server" >
    <Content>
      <orion:CustomQueryTable runat="server" ID="CustomTable" />
      <script type="text/javascript">
          $(function () {
              SW.Core.Resources.CustomQuery.initialize(
                  {
                      uniqueId: '<%= CustomTable.UniqueClientID %>',
                        initialPage: 0,
                        rowsPerPage: <%= this.GetPageSize() %>,
                        allowSort: true,
                        columnSettings: {
                            "Name": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.VServerOnThisCluster_Name%>'),
                                cellCssClassProvider: function (value, row, cellInfo) { return "SRM_Icon_Cell srm_word_wrap"; }
                            },                          
                            "TotalCapacity": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.VServerOnThisCluster_TotalCapacity%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "SRM_NoWrap"; }
                            },                          
                            "Used": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.VServerOnThisCluster_Used%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "SRM_NoWrap"; }
                            },                          
                            "UsedPercentage": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.VServerOnThisCluster_UsedPercentage%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatPercentageColumnValue(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "SRM_NoWrap"; }
                            }
                        },                      
                      onLoad: function(rows, columnsInfo) {
                          var params = {
                              resourceID: <%=this.Resource.ID%>,
                                netObjectFieldName:"_NetObjectID",
                                rows:  rows, 
                                columnsInfo: columnsInfo
                            };
                            SW.SRM.Common.FillAdditionColumns(params);
                        }
                    });               
               SW.Core.Resources.CustomQuery.refresh('<%=CustomTable.UniqueClientID %>');
           });
        </script>
    </Content>
</orion:ResourceWrapper>
