﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PoolCapacitySummary.ascx.cs" Inherits="Orion_SRM_Resources_Charting_PoolCapacitySummary" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" Module="SRM" File="Charts/Charts.SRM.BlockCapacitySummary.js" />
<orion:Include runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include runat="server" Module="SRM" File="BlockCapacitySummary.css" />
<orion:Include runat="server" Module="SRM" File="SRM.css" />

<orion:resourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <div class="srm-blockcapacitysummary-wrapper">
            <div id="srm-blockcapacitysummary-main-div-<%=Resource.ID%>" class="srm-blockcapacitysummary-main">
                <div class="srm-blockcapacitysummary-table">
                    <table id="srm-blockcapacitysummary-table-<%=Resource.ID%>">
                        <tr>
                            <td class="srm-blockcapacitysummary-td-big">
                                <div><span Title="<%= Resources.SrmWebContent.StoragePoolDetails_CapacitySummary_TotalUsableCapacity_Tooltip %>"><%= Resources.SrmWebContent.BlockCapacitySummary_TotalCapacity %>:</span><asp:Label runat="server" CssClass="SRM_BoldText" ID="TotalSizeLabel"></asp:Label></div>
                                <div><span Title="<%= Resources.SrmWebContent.StoragePoolDetails_CapacitySummary_TotalSubscribed_Tooltip %>"><%= Resources.SrmWebContent.BlockCapacitySummary_TotalSubscribed %>:</span><asp:Label runat="server" CssClass="SRM_BoldText" ID="TotalSubscribedLabel"></asp:Label></div>
                                <div><span Title="<%= Resources.SrmWebContent.StoragePoolDetails_CapacitySummary_ProjectedRunOut_Tooltip %>"><%= Resources.SrmWebContent.BlockCapacitySummary_ProjectedRunOut %>:</span>
                                    <span id="CapacityRunOutSpan_<%=Resource.ID%>" class="SRM_BoldText"></span>
                                </div>
                                <!--
                                <div runat="server" id="divTotalReduction"><span><%= Resources.SrmWebContent.UsableCapacityTotalReduction %>: </span><asp:Label runat="server" CssClass="SRM_BoldText" ID="TotalReductionLabel"></asp:Label></div>
                                -->
                                <div runat="server" id="divDataReduction"><span><%= Resources.SrmWebContent.UsableCapacityDataReduction %>: </span><asp:Label runat="server" CssClass="SRM_BoldText" ID="DataReductionLabel"></asp:Label></div>
                            </td>
                            <td class="srm-blockcapacitysummary-td-small">
                                <span id="srm-blockcapacitysummary-legend-<%=Resource.ID%>"></span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="srm-blockcapacitysummary-chart-area">
                    <div>
                        <a href="javascript:summaryChart_<%=Resource.ID%>.expanderChangedHandler();"><img id="srm-blockcapacitysummary-img-<%=Resource.ID%>" src="/Orion/SRM/images/Button.Expand.gif" alt="[+]" /></a>
                        <span><a href="/Orion/View.aspx?NetObject=<%= Pool.NetObjectID %>" id="srm-blockcapacitysummary-objectname-<%=Resource.ID%>"><%= Pool.PoolEntity.DisplayName %></a></span>
                    </div>
                    <asp:Panel ID="chart" runat="server" CssClass="srm-blockcapacitysummary-chart"></asp:Panel>
                </div>
            </div>
            <div id="srm-blockcapacitysummary-details-<%=Resource.ID%>" class="srm-blockcapacitysummary-detail-main"></div>
        </div>
        <script type="text/javascript">
            var summaryChart_<%=Resource.ID%>;

            $(function () {
                $('#CapacityRunOutSpan_<%=Resource.ID%>').html(SW.SRM.Formatters.FormatDaysDiff(<%= CapacityProjectedRunout %>, <%= WarningThresholdDays %>, <%= CriticalThresholdDays %>));

                summaryChart_<%=Resource.ID%> = new SW.SRM.Charts.BlockCapacitySummary();
                summaryChart_<%=Resource.ID%>.init(<%=Data%>);
            });
        </script>
    </Content>
</orion:resourceWrapper>
