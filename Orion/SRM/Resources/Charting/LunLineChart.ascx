﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LunLineChart.ascx.cs" Inherits="Orion_SRM_Resources_Charting_LunLineChart" %>
<orion:Include runat="server" Module="SRM" File="SRM.Common.js" />
<orion:Include runat="server" Module="SRM" File="SRM.Formatters.js" />

<orion:resourceWrapper ID="Wrapper" runat="server">
    <Content>
        <asp:PlaceHolder ID="WrapperContents" runat="server" />
         <script type="text/javascript">
             $(function () {
                 $(".chartLegendLogo").hide();
             })
          </script>
    </Content>
</orion:resourceWrapper> 