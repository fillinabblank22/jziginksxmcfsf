﻿using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Web.UI.Models.ObjectStatus;
using SolarWinds.SRM.Web.UI.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_SRM_Resources_Charting_ObjectStatus : SrmBaseResourceControl, IResourceIsInternal
{
    protected IObjectStatus model;

    protected override string DefaultTitle
    {
        get
        {
            // Internal resource, titles will defined in parents.
            return null;
        }
    }

    /// <summary>
    /// Internal resources do not show up in Customize page list.
    /// </summary>
    public bool IsInternal
    {
        get
        {
            return true;
        }
    }

    public bool IsPerformanceVisible { get; set; }

    public override string HelpLinkFragment
    {
        get { return "SRMPHResourceArrayStatus"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditArrayStatusHistogram.ascx"; }
    }

    public override string EditURL
    {
        get
        {
            return ExtendCustomEditUrl("/Orion/NetPerfMon/Resources/EditResource.aspx");
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        WebHelper.IncludeCharts();

        OrionInclude.ModuleFile("SRM", "Charts/Charts.SRM.Extension.js", OrionInclude.Section.Bottom);
        OrionInclude.ModuleFile("SRM", "Charts/Charts.SRM.ArrayStatusResource.js", OrionInclude.Section.Bottom);
    }

    protected override void OnLoad(EventArgs e)
    {
        var modelProvider = HierarchyHelper.FindParent<IModelProvider>(this.Parent);
        if (modelProvider != null && modelProvider.PageModelProvider != null)
        {
            model = modelProvider.PageModelProvider.ObjectStatusModel;
            model.InitializeModel(this.Resource);
        }

        IStorageArrayProvider arrayProvider = GetInterfaceInstance<IStorageArrayProvider>();

        this.IsPerformanceVisible = arrayProvider != null
            ? ServiceLocator.GetInstance<IResourceVisibilityService>()
                .IsPerformanceAvailableForArray(arrayProvider.Array)
            : true;
         
 
    }
}