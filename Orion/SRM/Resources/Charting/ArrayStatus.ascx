﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ArrayStatus.ascx.cs" Inherits="Orion_SRM_Resources_Charting_ArrayStatus" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="srm" TagName="ObjectStatus" Src="~/Orion/SRM/Resources/Charting/ObjectStatus.ascx" %>
<%@ Register Src="~/Orion/SRM/Controls/Redirector.ascx" TagPrefix="srm" TagName="Redirector" %>

<srm:Redirector ID="Redirector1" runat="server" />
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <srm:ObjectStatus runat="server" ID="ObjectStatus" />
    </Content>
</orion:resourceWrapper>