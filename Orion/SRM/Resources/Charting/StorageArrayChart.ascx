﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StorageArrayChart.ascx.cs" Inherits="Orion_SRM_Resources_Charting_StorageArrayChart" %>
<%@ Import Namespace="Resources" %>

<orion:Include runat="server" Module="SRM" File="SRM.Common.js" />

<orion:resourceWrapper ID="Wrapper" CssClass="srm_resource_wrapper" runat="server">
    <Content>
        <asp:PlaceHolder ID="WrapperContents" runat="server" />
    </Content>
</orion:resourceWrapper> 