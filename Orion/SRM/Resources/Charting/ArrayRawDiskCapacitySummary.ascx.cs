﻿using System;
using System.Globalization;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;
using SolarWinds.Orion.Web.Charting.v2;
using System.Collections.Generic;
using SolarWinds.SRM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_SRM_Resources_Charting_ArrayRawDiskCapacitySummary : CapacitySummaryBase
{
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IStorageArrayProvider) }; }
    }

    protected override string DefaultTitle
    {
        get { return SrmWebContent.ArrayDetails_RawDiskCapacitySummary_Title; }
    }

    public override string EditControlLocation
    {
        get { return null; }
    }

    public override string HelpLinkFragment
    {
        get { return "SRMPHResourceRawCapacitySummary"; }
    }

    public StorageArray Array { get; set; }

    protected string Data
    {
        get
        {
            return Serializer.Serialize(new
            {
                ResourceId = this.Resource.ID,
                NetObjectId = this.Request["NetObject"],
                PageSize = this.PageSize,
                Expanded = SrmSessionManager.GetBlockCapacitySummaryExpanderState(Resource.ID),
                RenderTo = this.chart.ClientID,
                ResourceWidth = Resource.Width,
                DataSourceMethod = "GetStorageArrayRawData",
                LegendMapping = new Dictionary<String, String[]>() 
                { 
                    { "columnDarkBlue", new String[] { Resources.SrmWebContent.ArrayDetails_SummaryCapacity_Used, Resources.SrmWebContent.RawDiskCapacitySummary_UsedCapacity_Tooltip  }},
                    { "columnPurple", new String[] { Resources.SrmWebContent.ArrayDetails_SummaryCapacity_Spare, Resources.SrmWebContent.RawDiskCapacitySummary_SpareCapacity_Tooltip } },
                    { "columnGrey", new String[] { Resources.SrmWebContent.ArrayDetails_SummaryCapacity_Free, Resources.SrmWebContent.RawDiskCapacitySummary_FreeCapacity_Tooltip } }
                }
            });
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var arrayProvider = GetInterfaceInstance<IStorageArrayProvider>();
        this.Array = arrayProvider.Array;

        if (!this.IsPostBack)
        {
            this.TotalSizeLabel.Text = WebHelper.FormatCapacityTotal(this.Array.Array.CapacityTotal);
            this.UsedLabel.Text = WebHelper.FormatCapacityTotal(this.Array.Array.CapacityRawUsed);
            this.FreeLabel.Text = WebHelper.FormatCapacityTotal(this.Array.Array.CapacityRawFree);
        }
    }
}