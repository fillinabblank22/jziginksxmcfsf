﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ArrayLunLineChart.ascx.cs" Inherits="Orion_SRM_Resources_Charting_ArrayLunLineChart" %>
<%@ Import Namespace="Resources" %>

<orion:Include runat="server" Module="SRM" File="SRM.Common.js" />
<orion:Include runat="server" Module="SRM" File="SRM.Formatters.js" />

<orion:resourceWrapper ID="Wrapper" CssClass="srm_resource_wrapper" runat="server">
    <Content>
        <asp:PlaceHolder ID="WrapperContents" runat="server" />
         <script type="text/javascript">
             $(function () {
                 $(".chartLegendLogo").hide();
             })
          </script>
    </Content>
</orion:resourceWrapper> 