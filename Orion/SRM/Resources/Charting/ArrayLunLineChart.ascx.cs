﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_SRM_Resources_Charting_ArrayLunLineChart : SrmLineChart
{
    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get { return StorageArray.NetObjectPrefix; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IStorageArrayProvider) }; }
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get { return new[] { typeof(IMultiStorageArrayProvider) }; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!this.IsResourceVisible())
        {
            this.Visible = false;
            return;
        }
        HandleInit(WrapperContents);
        Resource.Title = WebHelper.GetTopXxResourceTitle(Resource, 10);
    }

    public override string EditURL
    {
        get
        {
            return ExtendCustomEditUrl("/Orion/SRM/EditTopXXChartResource.aspx");
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceLUNsByIOPS";
        }
    }

    private bool IsResourceVisible()
    {
        IStorageArrayProvider arrayProvider = GetInterfaceInstance<IStorageArrayProvider>();
        if (arrayProvider != null)
        {
            return ServiceLocator.GetInstance<IResourceVisibilityService>()
                .IsResourceVisibleForArray(arrayProvider.Array, this.AppRelativeVirtualPath);
        }
        return true;
    }

}