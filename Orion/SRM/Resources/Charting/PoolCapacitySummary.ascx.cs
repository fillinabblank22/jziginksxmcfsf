﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_SRM_Resources_Charting_PoolCapacitySummary : CapacitySummaryBase
{
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IPoolProvider) }; }
    }

    private IPoolProvider poolProvider;
    /// <summary>
    /// Don't create several instances of IPoolProvider. Use the single one.
    /// </summary>
    protected IPoolProvider GetPoolProvider()
    {
        if (poolProvider == null)
        {
            poolProvider = GetInterfaceInstance<IPoolProvider>();
        }
        return poolProvider;
    }

    protected override string DefaultTitle
    {
        get { return Resources.SrmWebContent.PoolCapacitySummary_Title; }
    }

    public override string HelpLinkFragment
    {
        get { return "SRMPHResourcePoolCapacitySummary"; }
    }

    protected string Data
    {
        get
        {
            return Serializer.Serialize(new
            {
                ResourceId = this.Resource.ID,
                NetObjectId = GetPoolProvider().Pool.NetObjectID,
                PageSize = this.PageSize,
                Expanded = SrmSessionManager.GetBlockCapacitySummaryExpanderState(Resource.ID),
                RenderTo = chart.ClientID,
                ResourceWidth = Resource.Width,
                DataSourceMethod = GetPoolProvider().Pool.PoolEntity.Category == PoolCategory.Block ? "GetStoragePoolBlockData" : "GetStoragePoolFileData",
                LegendMapping = new Dictionary<string, string[]>()
                                {
                                    { "columnDarkBlue", new string[] { Resources.SrmWebContent.BlockCapacitySummary_Provisioned, Resources.SrmWebContent.StoragePoolDetails_CapacitySummary_ProvisionedCapacity_Tooltip } },
                                    { "columnGrey", new string[] { Resources.SrmWebContent.BlockCapacitySummary_Remaining, Resources.SrmWebContent.StoragePoolDetails_CapacitySummary_Remaining_Tooltip } },
                                    { "columnPink", new string[] { Resources.SrmWebContent.BlockCapacitySummary_OverSubscribed, Resources.SrmWebContent.StoragePoolDetails_CapacitySummary_OverSubscribedCapacity_Tooltip } }
                                }
            });
        }
    }

    public Pool Pool
    {
        get;
        set;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Pool = GetPoolProvider().Pool;

        if (Pool.PoolEntity.CapacityRunOut.HasValue)
        {
            CapacityProjectedRunout = WebHelper.GetDayDifference(Pool.PoolEntity.CapacityRunOut.Value);
        }

        if (!IsPostBack)
        {
            TotalSubscribedLabel.Text = WebHelper.FormatCapacityTotal(Pool.PoolEntity.CapacitySubscribed);
            TotalSizeLabel.Text = WebHelper.FormatCapacityTotal(Pool.PoolEntity.CapacityUserTotal);
            //WebHelper.SetValueOrHide(TotalReductionLabel, divTotalReduction, WebHelper.FormatDataReduction(Pool.PoolEntity.TotalReduction));
            WebHelper.SetValueOrHide(DataReductionLabel, divDataReduction, WebHelper.FormatDataReduction(Pool.PoolEntity.DataReduction));
        }
    }
}