﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VServerStatus.ascx.cs" Inherits="Orion_SRM_Resources_Charting_VServerStatus" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="srm" TagName="ObjectStatus" Src="~/Orion/SRM/Resources/Charting/ObjectStatus.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <srm:ObjectStatus runat="server" ID="ObjectStatus" />
    </Content>
</orion:resourceWrapper>