﻿using System;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.Resources;
using SolarWinds.SRM.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_SRM_Resources_Charting_BlockCapacitySummary : CapacitySummaryBase
{
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IStorageArrayProvider) }; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.SrmWebContent.BlockCapacitySummary_Title; }
    }

    public override string HelpLinkFragment
    {
        get { return "SRMPHResourceArrayCapacitySummary"; }
    }

    public StorageArray Array
    {
        get;
        set;
    }

    protected string Data
    {
        get
        {
            return Serializer.Serialize(new
            {
                ResourceId = this.Resource.ID,
                NetObjectId = this.Request["NetObject"],
                PageSize = this.PageSize,
                Expanded = SrmSessionManager.GetBlockCapacitySummaryExpanderState(Resource.ID),
                RenderTo = this.chart.ClientID,
                ResourceWidth = Resource.Width,
                DataSourceMethod = "GetStorageArrayData",
                LegendMapping = new Dictionary<String, String[]>()
                                {
                                    { "columnDarkBlue", new String[] { Resources.SrmWebContent.BlockCapacitySummary_Used,  Resources.SrmWebContent.BlockCapacitySummary_Used_Tooltip }},
                                    { "columnGrey", new String[] { Resources.SrmWebContent.BlockCapacitySummary_Remaining, Resources.SrmWebContent.BlockCapacitySummary_Remaining_Tooltip}},
                                }
            });
        }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        var arrayProvider = GetInterfaceInstance<IStorageArrayProvider>();
        this.Array = arrayProvider.Array;
        CapacityProjectedRunout = WebHelper.GetDayDifference(this.Array.Array.CapacityRunout);
        if (!this.IsPostBack)
        {
            this.TotalSizeLabel.Text = WebHelper.FormatCapacityTotal(this.Array.Array.CapacityUserTotal);
            WebHelper.SetValueOrHide(TotalReductionLabel, divTotalReduction, WebHelper.FormatDataReduction(Array.Array.TotalReduction));
            WebHelper.SetValueOrHide(DataReductionLabel, divDataReduction, WebHelper.FormatDataReduction(Array.Array.DataReduction));
        }
    }
}