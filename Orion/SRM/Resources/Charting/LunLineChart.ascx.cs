﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_SRM_Resources_Charting_LunLineChart : SrmLineChart
{
    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get { return Lun.NetObjectPrefix; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(ILunProvider) }; }
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get { return new[] { typeof(IMultiLunProvider) }; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
        OrionInclude.ModuleFile("SRM", "Charts/Lun.Charts.Options.js", OrionInclude.Section.Middle);
    }
}