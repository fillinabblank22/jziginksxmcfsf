﻿using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.Resources;
using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_SRM_Resources_Charting_ArrayPerformanceComparison : ArrayBaseResourceControl
{
    private static readonly JavaScriptSerializer serializer = new JavaScriptSerializer { MaxJsonLength = 0x400000 };
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IStorageArrayProvider), typeof(IVServerProvider) }; }
    }

    protected override string DefaultTitle
    {
        get { return SrmWebContent.Resource_Title_PerformanceComparison; }
    }

    public override string HelpLinkFragment
    {
        get { return "SRMPHResourceIopsComparison"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditArrayPerformanceComparison.ascx"; }
    }

    public override string EditURL
    {
        get
        {
            return this.ExtendCustomEditUrl("/Orion/SRM/EditTopXXChartResource.aspx?EditXX=False");
        }
    }

    protected int DaysOfDataToLoadProperty
    {
        get
        {
            Int32 daysOfDataToLoad;
            if (!Int32.TryParse(this.Resource.Properties[ArrayPerformanceComparison.DaysOfDataToLoadPropertyKey], out daysOfDataToLoad))
                daysOfDataToLoad = ArrayPerformanceComparison.DaysOfDataToLoadDefault;
            return daysOfDataToLoad;
        }
    }

    protected int SampleSizeInMinutesProperty
    {
        get
        {
            Int32 sampleSizeInMinutes;
            if (!Int32.TryParse(this.Resource.Properties[ArrayPerformanceComparison.SampleSizeInMinutesPropertyKey], out sampleSizeInMinutes))
                sampleSizeInMinutes = ArrayPerformanceComparison.SampleSizeInMinutesDefault;
            return sampleSizeInMinutes;
        }
    }

    protected string ChartInitialZoomProperty
    {
        get
        {
            string sampleSizeInMinutes = this.Resource.Properties[ArrayPerformanceComparison.ZoomValuePropertyKey];
            if(string.IsNullOrEmpty(sampleSizeInMinutes))
                sampleSizeInMinutes = ArrayPerformanceComparison.ZoomDefault;
            return sampleSizeInMinutes;
        }
    }

    protected string ChartTitleProperty
    {
        get
        {
            return string.IsNullOrEmpty(this.Resource.Properties["ChartTitle"])
                ? String.Empty
                : this.Resource.Properties["ChartTitle"];
        }
    }

    protected string ChartSubTitleProperty
    {
        get
        {
            return string.IsNullOrEmpty(this.Resource.Properties["ChartSubTitle"])
                ? String.Empty
                : this.Resource.Properties["ChartSubTitle"];
        }
    }

    protected string Data
    {
        get
        {
            return serializer.Serialize(new
            {
                ResourceId = this.Resource.ID,
                NetObjectId = this.Request["NetObject"],
	            SampleSizeInMinutes = this.SampleSizeInMinutesProperty,
                DaysOfDataToLoad = this.DaysOfDataToLoadProperty,
                FirstComparisonChoice = this.ComparisonValue1DropDownList.SelectedValue,
                SecondComparisonChoice = this.ComparisonValue2DropDownList.SelectedValue,
                ChartInitialZoom = this.ChartInitialZoomProperty,
                ChartTitle = this.ChartTitleProperty,
                ChartSubTitle = this.ChartSubTitleProperty
            });
        }
    }

    protected string InitialFirstComparisonChoiceLabel
    {
        get
        {
            return ArrayPerformanceComparison.ComparisonChoices[ArrayPerformanceComparison.ComparisonChoicesIOPSRead];
        }
    }

    protected string InitialSecondComparisonChoiceLabel
    {
        get
        {
            return ArrayPerformanceComparison.ComparisonChoices[ArrayPerformanceComparison.ComparisonChoicesIOPSTotal];
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        WebHelper.IncludeCharts();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            ComparisonValue1DropDownList.Items.Clear();
            foreach (KeyValuePair<string, string> comparisonChoice in ArrayPerformanceComparison.ComparisonChoices)
            {
                ComparisonValue1DropDownList.Items.Add(new ListItem(comparisonChoice.Value, comparisonChoice.Key));
                ComparisonValue2DropDownList.Items.Add(new ListItem(comparisonChoice.Value, comparisonChoice.Key));
            }
            // set default values
            ComparisonValue1DropDownList.Items.FindByValue(ArrayPerformanceComparison.ComparisonChoicesIOPSRead).Selected = true;
            ComparisonValue2DropDownList.Items.FindByValue(ArrayPerformanceComparison.ComparisonChoicesIOPSTotal).Selected = true;
        }
    }

    protected override bool IsResourceVisible()
    {
        if (Array != null)
        {
            return ServiceLocator.GetInstance<IResourceVisibilityService>()
                .IsResourceVisibleForArray(Array, this.AppRelativeVirtualPath);
        }

        IVServerProvider vserverProvider = GetInterfaceInstance<IVServerProvider>();
        if (vserverProvider != null)
        {
            return ServiceLocator.GetInstance<IResourceVisibilityService>()
                .IsResourceVisibleForArray(vserverProvider.VServer.StorageArray, this.AppRelativeVirtualPath);
        }

        return true;
    }
}