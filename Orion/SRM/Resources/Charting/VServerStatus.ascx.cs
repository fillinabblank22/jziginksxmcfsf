﻿using System;

using Resources;

using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Web.UI.Models;

public partial class Orion_SRM_Resources_Charting_VServerStatus : VServerBaseResourceControl, IModelProvider
{
    #region Properties

    /// <summary>
    /// Override default title property.
    /// </summary>
    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.VServer_Status_Resource_Title;
        }
    }

    /// <summary>
    /// Override help link fragment property.
    /// </summary>
    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceArrayStatus";
        }
    }

    public IPageModelProvider PageModelProvider
    {
        get
        {
            return new VServerPageModelProvider(this.VServer);
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        ObjectStatus.Resource = this.Resource;
    }

    #endregion
}