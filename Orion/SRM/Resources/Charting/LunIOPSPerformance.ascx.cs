﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_SRM_Resources_Charting_LunIOPSPerformance : PerformanceChartBase
{
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(ILunProvider) }; }
    }

    protected override string DefaultTitle
    {
        get { return SrmWebContent.LunIopsPerformance_Resource_Title; }
    }

    /// <summary>
    /// Override help link fragment property.
    /// </summary>
    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourcePoolIOPS";
        }
    }

    protected string Data
    {
        get
        {
            int lunID = int.Parse(this.Request["NetObject"].Split(':')[1]);
            List<PoolEntity> pools = ServiceLocator.GetInstance<IPoolDAL>().GetPoolsByLunId(lunID);
            List<object> listPools = new List<object>(); 
            foreach (PoolEntity pool in pools)
            {
                listPools.Add(new {id = pool.PoolID, name = pool.DisplayName});
            }
            return Serializer.Serialize(new
            {
                ResourceId = this.Resource.ID,
                NetObjectId = this.Request["NetObject"],
                SampleSizeInMinutes = this.SampleSizeInMinutesProperty,
                DaysOfDataToLoad = this.DaysOfDataToLoadProperty,
                YAxisTitle = SrmWebContent.LunIopsPerformance_Chart_yAxis_Title,
                ChartTitle =
                    !String.IsNullOrEmpty(this.ChartTitleProperty)
                        ? this.ChartTitleProperty
                        : SrmWebContent.LunIopsPerformance_Chart_Title,
                ChartSubTitle = this.ChartSubTitleProperty,
                Pools = listPools,
                DataUrl = "/Orion/SRM/Services/ChartData.asmx/GetLunPerformanceByField",
                FilterField = StatisticFields.IOPSTotal,
                ChartInitialZoom = this.ChartInitialZoomProperty,
                NumberOfSeriesToShow = this.NumberOfSeriesToShowProperty
            });
        }
    }

    public override string EditURL
    {
        get
        {
            return this.ExtendCustomEditUrl("/Orion/SRM/EditTopXXChartResource.aspx?EditXX=True");
        }
    }

    protected override bool IsResourceVisible()
    {
        ILunProvider lunProvider = GetInterfaceInstance<ILunProvider>();
        if (lunProvider != null)
        {
            return ServiceLocator.GetInstance<IResourceVisibilityService>()
                .IsResourceVisibleForArray(lunProvider.Lun.StorageArray, this.AppRelativeVirtualPath);
        }
        return true;
    }
}