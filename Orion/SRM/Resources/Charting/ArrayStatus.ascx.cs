﻿using System;

using Resources;

using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Web.UI.Models;

public partial class Orion_SRM_Resources_Charting_ArrayStatus :  ArrayBaseResourceControl, IModelProvider
{
    #region Properties

    /// <summary>
    /// Override default title property.
    /// </summary>
    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.Array_Status_Resource_Title;
        }
    }

    /// <summary>
    /// Override help link fragment property.
    /// </summary>
    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceArrayStatus";
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        this.Visible = !this.Array.Array.IsCluster;

        if (this.Visible)
        {
            base.OnLoad(e);
        }

        ObjectStatus.Resource = this.Resource;
    }

    #endregion

    public IPageModelProvider PageModelProvider
    {
        get
        {
            return new ArrayPageModelProvider(this.Array);
        }
    }
}