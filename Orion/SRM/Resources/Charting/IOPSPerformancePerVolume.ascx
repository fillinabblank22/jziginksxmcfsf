﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IOPSPerformancePerVolume.ascx.cs" Inherits="Orion_SRM_Resources_Charting_IOPSPerformancePerVolume" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" Module="SRM" File="SRM.Common.js" />
<orion:Include runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include runat="server" Module="SRM" File="Charts/Charts.SRM.Performance.js" />
<orion:Include runat="server" Module="SRM" File="SRM.css" />
<orion:Include runat="server" Module="SRM" File="Performance.css" />

<orion:resourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <div id="srm-performance-chart-buttons-<%=Resource.ID%>" class="srm-performance-buttons">
            <span><%= Resources.SrmWebContent.IopsPerformancePerVolume_DataSet %></span>
            <input id ="btnRead<%=Resource.ID%>" class="srm-performance-button" type="button" value="<%= Resources.SrmWebContent.IopsPerformancePerVolume_ButtonRead %>" >
            <input id ="btnWrite<%=Resource.ID%>" class="srm-performance-button" type="button" value="<%= Resources.SrmWebContent.IopsPerformancePerVolume_ButtonWrite %>">
            <input id ="btnOther<%=Resource.ID%>" class="srm-performance-button" type="button" value="<%= Resources.SrmWebContent.IopsPerformancePerVolume_ButtonOther %>">
            <input id ="btnTotal<%=Resource.ID%>" class="srm-performance-button srm-performance-button-selected" type="button" value="<%= Resources.SrmWebContent.IopsPerformancePerVolume_ButtonTotal %>">
        </div>
        <div id="srm-performance-chart-<%=Resource.ID%>"></div>    
        <div id="srm-performance-legend-<%=Resource.ID%>" class="srm-legend"></div>
        <script type="text/javascript">
            var performanceChart<%=Resource.ID%>;
            $(function () {
                performanceChart<%=Resource.ID%> = new SW.SRM.Charts.Performance();
                performanceChart<%=Resource.ID%>.init(false, $.extend(true, <%=Data%>, { DataUrl: "/Orion/SRM/Services/ChartData.asmx/GetIOPSPerformancePerVolumeTotal" }));
            });
            $("#btnRead<%=Resource.ID%>").click(function () {
                performanceChart<%=Resource.ID%>.init(false, $.extend(true, <%=Data%>, { DataUrl: '/Orion/SRM/Services/ChartData.asmx/GetIOPSPerformancePerVolumeRead' }));
                setSelected("#btnRead<%=Resource.ID%>");
            });
            $("#btnWrite<%=Resource.ID%>").click(function () {
                performanceChart<%=Resource.ID%>.init(false, $.extend(true, <%=Data%>, { DataUrl: '/Orion/SRM/Services/ChartData.asmx/GetIOPSPerformancePerVolumeWrite' }));
                setSelected("#btnWrite<%=Resource.ID%>");
            });
            $("#btnOther<%=Resource.ID%>").click(function () {
                performanceChart<%=Resource.ID%>.init(false, $.extend(true, <%=Data%>, { DataUrl: '/Orion/SRM/Services/ChartData.asmx/GetIOPSPerformancePerVolumeOther' }));
                setSelected("#btnOther<%=Resource.ID%>");
            });
            $("#btnTotal<%=Resource.ID%>").click(function () {
                performanceChart<%=Resource.ID%>.init(false, $.extend(true, <%=Data%>, { DataUrl: '/Orion/SRM/Services/ChartData.asmx/GetIOPSPerformancePerVolumeTotal' }));
                setSelected("#btnTotal<%=Resource.ID%>");
            });

            function setSelected(buttonId) {
                var myObjects = ["#btnRead<%=Resource.ID%>", "#btnWrite<%=Resource.ID%>", "#btnOther<%=Resource.ID%>", "#btnTotal<%=Resource.ID%>"];
                $.each(myObjects, function(index, value) {
                    if (value == buttonId) {
                        $(value).addClass("srm-performance-button-selected");
                    } else {
                        $(value).removeClass("srm-performance-button-selected");
                    }
                });
            }

        </script>
    </Content>
</orion:resourceWrapper>

