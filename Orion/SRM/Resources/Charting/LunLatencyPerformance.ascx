﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LunLatencyPerformance.ascx.cs" Inherits="Orion_SRM_Resources_Charting_LunLatencyPerformance" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" Module="SRM" File="SRM.Common.js" />
<orion:Include runat="server" Module="SRM" File="Charts/Charts.SRM.Performance.js" />
<orion:Include runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include runat="server" Module="SRM" File="Performance.css" />
<orion:Include runat="server" Module="SRM" File="SRM.css" />


<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <div id="srm-performance-chart-buttons-<%=Resource.ID%>" class="srm-performance-buttons-with-dropdown">
            <span><%= SrmWebContent.IopsPerformancePerVolume_DataSet %></span>
            <input id="btnRead<%=Resource.ID%>" onclick="onBtnClick<%=Resource.ID%>($(this))" filterfield="<%=StatisticFields.IOLatencyRead%>" class="srm-performance-button" type="button" value="<%= SrmWebContent.LunIOPSPerformance_ButtonRead %>">
            <input id="btnWrite<%=Resource.ID%>" onclick="onBtnClick<%=Resource.ID%>($(this))" filterfield="<%=StatisticFields.IOLatencyWrite%>" class="srm-performance-button" type="button" value="<%= SrmWebContent.LunIOPSPerformance_ButtonWrite %>">
            <input id="btnOther<%=Resource.ID%>" onclick="onBtnClick<%=Resource.ID%>($(this))" filterfield="<%=StatisticFields.IOLatencyOther%>" class="srm-performance-button" type="button" value="<%= SrmWebContent.LunIOPSPerformance_ButtonOther %>">
            <input id="btnTotal<%=Resource.ID%>" onclick="onBtnClick<%=Resource.ID%>($(this))" filterfield="<%=StatisticFields.IOLatencyTotal%>" class="srm-performance-button" type="button" value="<%= SrmWebContent.LunIOPSPerformance_ButtonTotal %>">
        </div>
        <div class="srm-custom-header-content">
            <span class="srm-performance-uppercase srm-performance-label"><%= SrmWebContent.LunIopsPerformance_CurrentPool_Label %>:</span>
            <select id="PoolsDropDownList<%=Resource.ID%>" onChange="poolsListChange<%=Resource.ID%>();"></select>
        </div>
        <div id="srm-performance-chart-<%=Resource.ID%>"></div>
        <div id="srm-performance-legend-<%=Resource.ID%>" class="srm-legend"></div>
        <script type="text/javascript">
            var data<%=Resource.ID%> = <%=Data%>;
            var performanceChart<%=Resource.ID%>;
            $(function () {                                
                jQuery.each(<%=Data%>.Pools, function(index, item) {
                    $('#PoolsDropDownList<%=Resource.ID%>').append('<option value="' + item.id + '">' + item.name + '</option>');
                });                
                data<%=Resource.ID%>.PoolID = parseInt($( '#PoolsDropDownList<%=Resource.ID%>' ).val());                
                onBtnClick<%=Resource.ID%>($("#btnTotal<%=Resource.ID%>"));                
                setDdlDisabled<%=Resource.ID%>();
                $("#srm-performance-legend-<%=Resource.ID%>").addClass("srm-performance-legend-item_wo_parent");
            });
            
            function onBtnClick<%=Resource.ID%>(btn) {
                $("#srm-performance-chart-buttons-<%=Resource.ID%> :input").removeClass("srm-performance-button-selected");
                btn.addClass("srm-performance-button-selected");
                data<%=Resource.ID%>.PoolID = parseInt($( '#PoolsDropDownList<%=Resource.ID%>' ).val());
                if (String('<%= this.ChartTitleProperty%>').length == 0) {
                    data<%=Resource.ID%>.ChartTitle = "<%=SrmWebContent.LunLatencyPerformance_Chart_Title%>: " + $('#PoolsDropDownList<%=Resource.ID%> option:selected').text();
                }
                data<%=Resource.ID%>.FilterField = btn.attr('filterfield');
                performanceChart<%=Resource.ID%> = new SW.SRM.Charts.Performance();
                performanceChart<%=Resource.ID%>.init(true, data<%=Resource.ID%>);
            }
            
            function setDdlDisabled<%=Resource.ID%>() {
                if(data<%=Resource.ID%>.Pools.length < 2)
                    $('#PoolsDropDownList<%=Resource.ID%>').attr('disabled','disabled');
                else
                    $('#PoolsDropDownList<%=Resource.ID%>').removeAttr('disabled');
            }

            function poolsListChange<%=Resource.ID%>() {
                var selectedBtn = $("#srm-performance-chart-buttons-<%=Resource.ID%> ").find(".srm-performance-button-selected");
                onBtnClick<%=Resource.ID%>(selectedBtn);
            }            

        </script>
    </Content>
</orion:ResourceWrapper>
