﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Common.ServiceProvider;


[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_SRM_Resources_PoolDetails_IOPSPerformancePerLun : PerformanceChartBase
{

    #region Properties

    /// <summary>
    /// Override default title property.
    /// </summary>
    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.BlockIOPSPerformancePerLun_ResourceTitle;
        }
    }

    /// <summary>
    /// Gets or sets required interfaces.
    /// </summary>
    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new[] {typeof (IPoolProvider)};
        }
    }

    /// <summary>
    /// Gets data for current resource.
    /// </summary>
    protected string Data
    {
        get
        {
            return
                Serializer.Serialize(
                    new
                        {
                            ResourceId = Resource.ID,
                            NetObjectId = Request["NetObject"],
                            SampleSizeInMinutes = SampleSizeInMinutesProperty,
                            DaysOfDataToLoad = DaysOfDataToLoadProperty,
                            YAxisTitle = SrmWebContent.IopsPerformancePerLun_Chart_yAxis_Title,
                            ChartTitle = String.IsNullOrEmpty(this.ChartTitleProperty) ? SrmWebContent.SRM_IOPSPerformancePerLun_Chart_Title : this.ChartTitleProperty,
                            ChartSubTitle = this.ChartSubTitleProperty,
                            ChartInitialZoom = this.ChartInitialZoomProperty,
                            NumberOfSeriesToShow = this.NumberOfSeriesToShowProperty
                        });
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Visible = GetInterfaceInstance<IPoolProvider>().Pool.PoolEntity.Category == PoolCategory.Block;
    }

    public override string EditURL
    {
        get
        {
            return this.ExtendCustomEditUrl("/Orion/SRM/EditTopXXChartResource.aspx?EditXX=True");
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourcePoolIOPS";
        }
    }

    protected override bool IsResourceVisible()
    {
        IPoolProvider poolProvider = GetInterfaceInstance<IPoolProvider>();
        if (poolProvider != null)
        {
            return ServiceLocator.GetInstance<IResourceVisibilityService>()
                .IsResourceVisibleForArray(poolProvider.Pool.StorageArray, this.AppRelativeVirtualPath);
        }
        return true;
    }

    #endregion
}
