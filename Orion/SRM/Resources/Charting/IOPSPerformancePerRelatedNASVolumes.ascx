﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IOPSPerformancePerRelatedNASVolumes.ascx.cs" Inherits="Orion_SRM_Resources_Charting_IOPSPerformancePerRelatedNASVolumes" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Import Namespace="Resources" %>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" Module="SRM" File="SRM.Common.js" />
<orion:Include runat="server" Module="SRM" File="Charts/Charts.SRM.Performance.js" />
<orion:Include runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include runat="server" Module="SRM" File="Performance.css" />
<orion:Include runat="server" Module="SRM" File="SRM.css" />

<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <div id="srm-performance-chart-buttons-<%=Resource.ID%>" class="srm-performance-buttons-with-dropdown">
            <span><%= SrmWebContent.IOPSPerformancePerRelatedNASVolumes_DataSet %></span>
            <input id="btnRead<%=Resource.ID%>" class="srm-performance-button" type="button" value="<%= Resources.SrmWebContent.IOPSPerformancePerRelatedNASVolumes_ButtonRead %>">
            <input id="btnWrite<%=Resource.ID%>" class="srm-performance-button" type="button" value="<%= Resources.SrmWebContent.IOPSPerformancePerRelatedNASVolumes_ButtonWrite %>">
            <input id="btnOther<%=Resource.ID%>" class="srm-performance-button" type="button" value="<%= Resources.SrmWebContent.IOPSPerformancePerRelatedNASVolumes_ButtonOther %>">
            <input id="btnTotal<%=Resource.ID%>" class="srm-performance-button srm-performance-button-selected" type="button" value="<%= Resources.SrmWebContent.IOPSPerformancePerRelatedNASVolumes_ButtonTotal %>">
        </div>
        <div class="srm-custom-header-content">
            <span class="srm-performance-uppercase srm-performance-label"><%= SrmWebContent.IOPSPerformancePerRelatedNASVolumes_CurrentPool_Label %>:</span>
            <select id="PoolsDropDownList<%=Resource.ID%>" onchange="poolsListChange<%=Resource.ID%>();"></select>
        </div>
        <div id="srm-performance-chart-<%=Resource.ID%>"></div>
        <div id="srm-performance-legend-<%=Resource.ID%>" class="srm-legend"></div>
        <script type="text/javascript">
            var data<%=Resource.ID%> = <%=Data%>;
            var performanceChart<%=Resource.ID%>;
            
            $(function () {
                var data = data<%=Resource.ID%>;

                performanceChart<%=Resource.ID%> = new SW.SRM.Charts.Performance();
                performanceChart<%=Resource.ID%>.init(false, $.extend(true, data, { FilterField: "<%=StatisticFields.IOPSTotal %>" }));
                
                if (data.Pools.length != 0) {
                    $.each(<%=Data%>.Pools, function(index, item) {
                        $('#PoolsDropDownList<%=Resource.ID%>').append('<option value="' + item.id + '">' + item.name + '</option>');
                    });
                    data.PoolID = parseInt($('#PoolsDropDownList<%=Resource.ID%>').val());

                    setDdlDisabled<%=Resource.ID%>();
                }
            });
            $("#btnRead<%=Resource.ID%>").click(function () {
                var data = data<%=Resource.ID%>;
                performanceChart<%=Resource.ID%>.init(false, $.extend(true, data, { FilterField: "<%=StatisticFields.IOPSRead %>" }));
                setSelectedBtn("#btnRead<%=Resource.ID%>");
            });
            $("#btnWrite<%=Resource.ID%>").click(function () {
                var data = data<%=Resource.ID%>;
                performanceChart<%=Resource.ID%>.init(false, $.extend(true, data, { FilterField: "<%=StatisticFields.IOPSWrite %>" }));
                setSelectedBtn("#btnWrite<%=Resource.ID%>");
            });
            $("#btnOther<%=Resource.ID%>").click(function () {
                var data = data<%=Resource.ID%>;
                performanceChart<%=Resource.ID%>.init(false, $.extend(true, data, { FilterField: "<%=StatisticFields.IOPSOther %>" }));
                setSelectedBtn("#btnOther<%=Resource.ID%>");
            });
            $("#btnTotal<%=Resource.ID%>").click(function () {
                var data = data<%=Resource.ID%>;
                performanceChart<%=Resource.ID%>.init(false, $.extend(true, data, { FilterField: "<%=StatisticFields.IOPSTotal %>" }));
                setSelectedBtn("#btnTotal<%=Resource.ID%>");
            });

            function setSelectedBtn(buttonId) {
                var data = data<%=Resource.ID%>;
                var myObjects = ["#btnRead<%=Resource.ID%>", "#btnWrite<%=Resource.ID%>", "#btnOther<%=Resource.ID%>", "#btnTotal<%=Resource.ID%>"];
                
                $.each(myObjects, function(index, value) {
                    if (value == buttonId) {
                        $(value).addClass("srm-performance-button-selected");
                    } else {
                        $(value).removeClass("srm-performance-button-selected");
                    }
                });
                           
                data.PoolID = parseInt($( '#PoolsDropDownList<%=Resource.ID%>' ).val());
                if (String('<%= this.ChartTitleProperty%>').length == 0) {
                    data.ChartTitle = "<%=SrmWebContent.IOPSPerformancePerRelatedNASVolumes_Chart_Title%>: " + $('#PoolsDropDownList<%=Resource.ID%> option:selected').text();
                }
            }
   
            function setDdlDisabled<%=Resource.ID%>() {
                var data = data<%=Resource.ID%>;
                if(data.Pools.length < 2)
                    $('#PoolsDropDownList<%=Resource.ID%>').attr('disabled','disabled');
                else
                    $('#PoolsDropDownList<%=Resource.ID%>').removeAttr('disabled');
            }
	  	 	 
            function poolsListChange<%=Resource.ID%>() {    
                var data = data<%=Resource.ID%>;
                setSelectedBtn("#btnTotal<%=Resource.ID%>");
                performanceChart<%=Resource.ID%> = new SW.SRM.Charts.Performance();
                performanceChart<%=Resource.ID%>.init(false, $.extend(true, data, { FilterField: "<%=StatisticFields.IOPSTotal %>" }));
            }   
        </script>
    </Content>
</orion:ResourceWrapper>

