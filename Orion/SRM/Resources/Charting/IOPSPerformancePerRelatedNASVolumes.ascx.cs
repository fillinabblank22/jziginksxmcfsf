﻿using System;
using System.Collections.Generic;
using System.ServiceModel.Configuration;
using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;
using System.Globalization;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_SRM_Resources_Charting_IOPSPerformancePerRelatedNASVolumes : PerformanceChartBase
{
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IVolumeProvider) }; }
    }

    protected override string DefaultTitle
    {
        get { return SrmWebContent.IOPSPerformancePerRelatedNASVolumes_Resource_Title; }
    }

    protected string Data
    {
        get
        {
            int volumeID = int.Parse(this.Request["NetObject"].Split(':')[1]);
            List<PoolEntity> pools = ServiceLocator.GetInstance<IPoolDAL>().GetPoolsByVolumeId(volumeID);
            List<object> listPools = new List<object>();
            foreach (PoolEntity pool in pools)
            {
                listPools.Add(new { id = pool.PoolID, name = pool.DisplayName });
            }
            return Serializer.Serialize(new
            {
                ResourceId = this.Resource.ID,
                NetObjectId = this.Request["NetObject"],
                SampleSizeInMinutes = this.SampleSizeInMinutesProperty,
                DaysOfDataToLoad = this.DaysOfDataToLoadProperty,
                YAxisTitle = SrmWebContent.IOPSPerformancePerRelatedNASVolumes_Chart_yAxis_Title,
                ChartTitle =
                    !String.IsNullOrEmpty(this.ChartTitleProperty)
                        ? this.ChartTitleProperty
                        : pools != null && pools.Count > 0
                            ? string.Format(CultureInfo.CurrentCulture, "{0}: {1}",
                                SrmWebContent.IOPSPerformancePerRelatedNASVolumes_Chart_Title, pools[0].DisplayName)
                            : SrmWebContent.IOPSPerformancePerRelatedNASVolumes_Chart_Title,
                ChartSubTitle = this.ChartSubTitleProperty,
                Pools = listPools,
                PoolID = pools != null && pools.Count > 0 ? pools[0].PoolID : -1,
                DataUrl = "/Orion/SRM/Services/ChartData.asmx/GetPerformancePerRelatedNASVolumes",
                HasParent = true,
                ChartInitialZoom = this.ChartInitialZoomProperty,
                NumberOfSeriesToShow = this.NumberOfSeriesToShowProperty
            });
        }
    }

    public override string EditURL
    {
        get
        {
            return this.ExtendCustomEditUrl("/Orion/SRM/EditTopXXChartResource.aspx?EditXX=True");
        }
    }
    public override string HelpLinkFragment
    {
        get { return "SRMPHResourcePoolIOPS"; }
    }

    protected override bool IsResourceVisible()
    {
        IVolumeProvider volumeProvider = GetInterfaceInstance<IVolumeProvider>();
        if (volumeProvider != null)
            return ServiceLocator.GetInstance<IResourceVisibilityService>()
                .IsResourceVisibleForArray(volumeProvider.Volume.StorageArray, this.AppRelativeVirtualPath);
        return true;
    }
}