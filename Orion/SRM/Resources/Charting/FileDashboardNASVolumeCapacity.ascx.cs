﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.SRM.Web;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_SRM_Resources_Charting_FileDashboardNASVolumeCapacity : BaseResourceControl
{
   

    protected override string DefaultTitle
    {
        get { return SrmWebContent.AggregatedNASVolumeCapacity_Resource_Title; }
    }

    public override string HelpLinkFragment
    {
        get { return "SRMPHResourceAggregatedNasCapacity"; }
    }

    
    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ResourceWrapper.ShowEditButton = ResourceWrapper.ShowEditButton && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        WebHelper.IncludeCharts();
    }

    protected string Data
    {
        get
        {
            return new JavaScriptSerializer().Serialize(new
            {
                ResourceId = this.Resource.ID,
                DataUrl = "/Orion/SRM/Services/ChartData.asmx/GetAllAggregatedNASVolumeCapacity"
            });
        }
    }
}