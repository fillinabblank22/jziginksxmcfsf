﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LatencyPerformancePerRelatedNASVolumes.ascx.cs" Inherits="Orion_SRM_Resources_Charting_LatencyPerformancePerRelatedNASVolumes" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" Module="SRM" File="SRM.Common.js" />
<orion:Include runat="server" Module="SRM" File="Charts/Charts.SRM.Performance.js" />
<orion:Include runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include runat="server" Module="SRM" File="Performance.css" />
<orion:Include runat="server" Module="SRM" File="SRM.css" />

<orion:resourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <div id="srm-performance-chart-buttons-<%=Resource.ID%>" class="srm-performance-buttons-with-dropdown">
            <span><%= Resources.SrmWebContent.LatencyPerformancePerRelatedNASVolumes_DataSet %></span>
            <input id ="btnRead<%=Resource.ID%>" class="srm-performance-button" type="button" value="<%= Resources.SrmWebContent.LatencyPerformancePerRelatedNASVolumes_ButtonRead %>" >
            <input id ="btnWrite<%=Resource.ID%>" class="srm-performance-button" type="button" value="<%= Resources.SrmWebContent.LatencyPerformancePerRelatedNASVolumes_ButtonWrite %>">
            <input id ="btnOther<%=Resource.ID%>" class="srm-performance-button" type="button" value="<%= Resources.SrmWebContent.LatencyPerformancePerRelatedNASVolumes_ButtonOther %>">
            <input id ="btnTotal<%=Resource.ID%>" class="srm-performance-button srm-performance-button-selected" type="button" value="<%= Resources.SrmWebContent.LatencyPerformancePerRelatedNASVolumes_ButtonTotal %>">
        </div>        
        <div class="srm-custom-header-content">
            <span class="srm-performance-uppercase srm-performance-label"><%= Resources.SrmWebContent.LunIopsPerformance_CurrentPool_Label %>:</span>
            <select  id="PoolsDropDownList<%=Resource.ID%>" onChange="poolsListChange<%=Resource.ID%>();"></select>
        </div>        
        <div id="srm-performance-chart-<%=Resource.ID%>"></div>                    
        <div id="srm-performance-legend-<%=Resource.ID%>" class="srm-legend"></div>
        <script type="text/javascript">
            var data<%=Resource.ID%> = <%=Data%>;
            var performanceChart<%=Resource.ID%>;    
            
            $(function () {
                var data = data<%=Resource.ID%>;
                
                performanceChart<%=Resource.ID%> = new SW.SRM.Charts.Performance();

                if (data.Pools.length !== 0) {
                    $.each(<%=Data%>.Pools, function(index, item) {
                        $('#PoolsDropDownList<%=Resource.ID%>').append('<option value="' + item.id + '">' + item.name + '</option>');
                    });
                    
                    setDdlDisabled<%=Resource.ID%>();
                } 
                onBtnClick<%=Resource.ID%>($("#btnTotal<%=Resource.ID%>"));
            });
            $("#btnRead<%=Resource.ID%>").click(function () {
                onBtnClick<%=Resource.ID%>($("#btnRead<%=Resource.ID%>"), "<%=StatisticFields.IOLatencyRead %>");
            });
            $("#btnWrite<%=Resource.ID%>").click(function () {
                onBtnClick<%=Resource.ID%>($("#btnWrite<%=Resource.ID%>"), "<%=StatisticFields.IOLatencyWrite %>");
            });
            $("#btnOther<%=Resource.ID%>").click(function () {
                onBtnClick<%=Resource.ID%>($("#btnOther<%=Resource.ID%>"), "<%=StatisticFields.IOLatencyOther %>");
            });
            $("#btnTotal<%=Resource.ID%>").click(function () {
                onBtnClick<%=Resource.ID%>($("#btnTotal<%=Resource.ID%>"), "<%=StatisticFields.IOLatencyTotal %>");
            });
           
            function onBtnClick<%=Resource.ID%>(btn, filterField) {
                $("#srm-performance-chart-buttons-<%=Resource.ID%> :input").removeClass("srm-performance-button-selected");
                btn.addClass("srm-performance-button-selected");
                
                var data = data<%=Resource.ID%>;
                if (data.Pools.length !== 0) {
                    data.PoolID = parseInt($('#PoolsDropDownList<%=Resource.ID%>').val());

                    if (String('<%= this.ChartTitleProperty%>').length === 0) {
                        data.ChartTitle = "<%=Resources.SrmWebContent.LatencyPerformancePerRelatedNASVolumes_Chart_Title%>: " + $('#PoolsDropDownList<%=Resource.ID%> option:selected').text();
                    }
                }

                if (!filterField) {
                    filterField = "<%=StatisticFields.IOLatencyTotal %>";
                }
                
                performanceChart<%=Resource.ID%>.init(true, $.extend(true, data, { FilterField: filterField }));
            }
            
            function setDdlDisabled<%=Resource.ID%>() {
                var data = data<%=Resource.ID%>;
                if(data.Pools.length < 2)
                    $('#PoolsDropDownList<%=Resource.ID%>').attr('disabled','disabled');
                else
                    $('#PoolsDropDownList<%=Resource.ID%>').removeAttr('disabled');
            }

            function poolsListChange<%=Resource.ID%>() {                
                var selectedBtn = $("#srm-performance-chart-buttons-<%=Resource.ID%> ").find(".srm-performance-button-selected");
                performanceChart<%=Resource.ID%> = new SW.SRM.Charts.Performance();
                onBtnClick<%=Resource.ID%>(selectedBtn);
            }

        </script>
    </Content>
</orion:resourceWrapper>

