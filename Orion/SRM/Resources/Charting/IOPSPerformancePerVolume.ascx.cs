﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_SRM_Resources_Charting_IOPSPerformancePerVolume : PerformanceChartBase
{
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IPoolProvider) }; }
    }

    protected override string DefaultTitle
    {
        get { return SrmWebContent.IopsPerformancePerVolume_Resource_Title; }
    }

    public override string EditURL
    {
        get
        {
            return this.ExtendCustomEditUrl("/Orion/SRM/EditTopXXChartResource.aspx?EditXX=True");
        }
    }

    public override string HelpLinkFragment
    {
        get { return "SRMPHResourcePoolIOPS"; }
    }

    protected string Data
    {
        get
        {
            return Serializer.Serialize(new
            {
                ResourceId = this.Resource.ID,
                NetObjectId = this.Request["NetObject"],
                SampleSizeInMinutes = this.SampleSizeInMinutesProperty,
                DaysOfDataToLoad = this.DaysOfDataToLoadProperty,
                ChartInitialZoom = this.ChartInitialZoomProperty,
                YAxisTitle = SrmWebContent.IopsPerformancePerVolume_Chart_yAxis_Title,
                ChartTitle =
                    String.IsNullOrEmpty(this.ChartTitleProperty)
                        ? SrmWebContent.SRM_IOPSPerformancePerVolume_Chart_Title
                        : this.ChartTitleProperty,
                ChartSubTitle = this.ChartSubTitleProperty,
                NumberOfSeriesToShow = this.NumberOfSeriesToShowProperty
            });
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Visible = GetInterfaceInstance<IPoolProvider>().Pool.PoolEntity.Category == PoolCategory.File;
    }

}