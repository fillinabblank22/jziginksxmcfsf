﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IOPSPerformancePerLun.ascx.cs" Inherits="Orion_SRM_Resources_PoolDetails_IOPSPerformancePerLun" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" Module="SRM" File="SRM.Common.js" />
<orion:Include runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include runat="server" Module="SRM" File="Charts/Charts.SRM.Performance.js" />
<orion:Include runat="server" Module="SRM" File="SRM.css" />
<orion:Include runat="server" Module="SRM" File="Performance.css" />

<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <div id="srm-performance-chart-buttons-<%=Resource.ID%>" class="srm-performance-buttons">
            <span><%= Resources.SrmWebContent.BlockIOPSPerformancePerLun_DataSet %></span>
            <input id="btnRead" class="srm-performance-button" type="button" value="<%= Resources.SrmWebContent.BlockIOPSPerformancePerLun_ButtonRead %>">
            <input id="btnWrite" class="srm-performance-button" type="button" value="<%= Resources.SrmWebContent.BlockIOPSPerformancePerLun_ButtonWrite %>">
            <input id="btnOther" class="srm-performance-button" type="button" value="<%= Resources.SrmWebContent.BlockIOPSPerformancePerLun_ButtonOther %>">
            <input id="btnTotal" class="srm-performance-button srm-performance-button-selected" type="button" value="<%= Resources.SrmWebContent.BlockIOPSPerformancePerLun_ButtonTotal %>">
        </div>
        <div id="srm-performance-chart-<%=Resource.ID%>"></div>
        <div id="srm-performance-legend-<%=Resource.ID%>" class="srm-legend"></div>
        
        <script type="text/javascript">
            var performanceChart<%=Resource.ID%>;
            $(function () {
                performanceChart<%=Resource.ID%> = new SW.SRM.Charts.Performance();
                performanceChart<%=Resource.ID%>.init(false, $.extend(true, <%=Data%>, { DataUrl: "/Orion/SRM/Services/ChartData.asmx/GetIOPSPerformancePerLunTotal" }));
            });
            $("#btnRead").click(function () {
                performanceChart<%=Resource.ID%>.init(false, $.extend(true, <%=Data%>, { DataUrl: "/Orion/SRM/Services/ChartData.asmx/GetIOPSPerformancePerLunRead" }));
                setSelected("#btnRead");
            });
            $("#btnWrite").click(function () {
                performanceChart<%=Resource.ID%>.init(false, $.extend(true, <%=Data%>, { DataUrl: "/Orion/SRM/Services/ChartData.asmx/GetIOPSPerformancePerLunWrite" }));
                setSelected("#btnWrite");
            });
            $("#btnOther").click(function () {
                performanceChart<%=Resource.ID%>.init(false, $.extend(true, <%=Data%>, { DataUrl: "/Orion/SRM/Services/ChartData.asmx/GetIOPSPerformancePerLunOther" }));
                setSelected("#btnOther");
            });
            $("#btnTotal").click(function () {
                performanceChart<%=Resource.ID%>.init(false, $.extend(true, <%=Data%>, { DataUrl: "/Orion/SRM/Services/ChartData.asmx/GetIOPSPerformancePerLunTotal" }));
                setSelected("#btnTotal");
            });

            function setSelected(buttonId) {
                var myObjects = ["#btnRead", "#btnWrite", "#btnOther", "#btnTotal"];

                $.each(myObjects, function(index, value) {
                    if (value == buttonId) {
                        $(value).addClass("srm-performance-button-selected");
                    } else {
                        $(value).removeClass("srm-performance-button-selected");
                    }
                });
            }

        </script>
    </Content>
</orion:ResourceWrapper>
