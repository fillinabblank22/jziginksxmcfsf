﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ArrayPerformanceComparison.ascx.cs" Inherits="Orion_SRM_Resources_Charting_ArrayPerformanceComparison" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" Module="SRM" File="SRM.Common.js" />
<orion:Include runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include runat="server" Module="SRM" File="Charts/Charts.SRM.ArrayPerformanceComparison.js" />
<orion:Include runat="server" Module="SRM" File="ArrayPerformanceComparison.css" />
<orion:Include runat="server" Module="SRM" File="SRM.css" />
<orion:Include runat="server" Module="SRM" File="SRM.Common.js" />

<orion:resourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <div id="srm-performance-comparison-main-div-<%=Resource.ID%>" class="srm-performance-comparison-main-div">
            <table>
                <tr>
                    <td class="srm-performance-comparison-td-no-border">
                        <div class="srm-performance-comparison-choices-container" id="srm-performance-comparison-choices-container-<%=Resource.ID%>">
                            <asp:DropDownList runat="server"
                                AutoPostBack="false"
                                ID="ComparisonValue1DropDownList"
                                onChange="comparison1Change(this);" 
                                class="srm-performance-comparison-combobox-left"/>
                            <asp:DropDownList runat="server"
                                AutoPostBack="false"
                                ID="ComparisonValue2DropDownList"
                                onChange="comparison2Change(this);"
                                class="srm-performance-comparison-combobox-right" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="srm-performance-comparison-td-big">
                        <div id="srm-performance-comparison-summary-<%=Resource.ID%>"></div>    
                    </td>
                </tr>
                <tr>
                    <td class="srm-performance-comparison-td-small">
                        <span id="srm-performance-comparison-legend-<%=Resource.ID%>"></span>
                    </td>
                </tr>
            </table>
        </div>
        <script type="text/javascript">
            var theChart_<%=Resource.ID%>;
            
            // template for chart initialization
            var initData_<%=Resource.ID%> = <%=Data%>;
            
            var firstComparisonChoiceLabel_<%=Resource.ID%> = '<%=InitialFirstComparisonChoiceLabel%>';
            var secondComparisonChoiceLabel_<%=Resource.ID%> = '<%=InitialSecondComparisonChoiceLabel%>';

            $(function () {
                theChart_<%=Resource.ID%> = new SW.SRM.Charts.ArrayPerformanceComparison();
                comparisonInit();
            });

            function comparison1Change(combobox) {
                initData_<%=Resource.ID%>.FirstComparisonChoice = combobox.options[combobox.selectedIndex].value;
                firstComparisonChoiceLabel_<%=Resource.ID%> = combobox.options[combobox.selectedIndex].text;
                comparisonInit();
            }
            
            function comparison2Change(combobox) {
                initData_<%=Resource.ID%>.SecondComparisonChoice = combobox.options[combobox.selectedIndex].value;
                secondComparisonChoiceLabel_<%=Resource.ID%> = combobox.options[combobox.selectedIndex].text;
                comparisonInit();
            }

            function comparisonInit() {
                theChart_<%=Resource.ID%>.init(initData_<%=Resource.ID%>);
            }
        </script>
    </Content>
</orion:resourceWrapper>
