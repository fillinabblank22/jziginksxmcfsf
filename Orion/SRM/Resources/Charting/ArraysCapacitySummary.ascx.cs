﻿using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Resources;
using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_SRM_Resources_Charting_ArraysCapacitySummary : BaseResourceControl
{
    protected static readonly JavaScriptSerializer Serializer = new JavaScriptSerializer { MaxJsonLength = 0x400000 };
    
    protected override string DefaultTitle
    {
        get { return Resources.SrmWebContent.ArrayRawDiskCapacitySummary_Title; }
    }

    public override string HelpLinkFragment
    {
        get { return "SRMPHResourceRawCapacitySummary"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
    }

    protected string Data
    {
        get
        {
            return Serializer.Serialize(new
            {
                ResourceId = this.Resource.ID,
                DataSourceMethod = "GetRawStorageArrayData",
                LegendMapping = new Dictionary<String, String>() 
                                { 
                                    {"Spare", Resources.SrmWebContent.ArrayCapacitySummary_Spare},
                                    {"Used", Resources.SrmWebContent.ArrayCapacitySummary_Used },
                                    {"Remaining", Resources.SrmWebContent.ArrayCapacitySummary_Remaining },
                                }
            });
        }
    }

    protected ResourceSearchControl SearchControl { get; private set; }

    protected string PageSize
    {
        get { return Resource.Properties.ContainsKey(PageSizeHelper.PropertyKey) ? Resource.Properties[PageSizeHelper.PropertyKey] : null; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        WebHelper.IncludeCharts();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ResourceWrapper.ShowEditButton = ResourceWrapper.ShowEditButton && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;

        // add search controll to resource header
        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        ResourceWrapper.HeaderButtons.Controls.Add(SearchControl);
    }
}