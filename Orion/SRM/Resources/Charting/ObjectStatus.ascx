﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ObjectStatus.ascx.cs" Inherits="Orion_SRM_Resources_Charting_ObjectStatus" %>
<%@ Import Namespace="Resources" %>
<orion:Include ID="Include1" runat="server" Module="SRM" File="ArrayStatus.css" />
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.css" />
<orion:Include ID="Include3" runat="server" Module="SRM" File="SRM.Formatters.js" />

<asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
<table class="srm-array-status-resource">
    <%if(IsPerformanceVisible) {%>
	<tr>
		<td>
			<div id="srm-asr-c1<%=Resource.ID%>" class="hasChart"></div>
		</td>
		<td id="aggregate-container-srm-asr-c1<%=Resource.ID%>">
			<div id="srm-asr-c2<%=Resource.ID%>" class="hasChart"></div>
		</td>
	</tr>
    <% } %>
	<tr class="table-row-srm-asr-c1<%=Resource.ID %>">
            <%if(model.IsArrayModel) { %>
		<td class="title">
			<span class="srm-uppercase"><%=SrmWebContent.User_Capacity_Chart_Title %></span>
			<div><div class="value <%=model.UserCapacityStatus%>"><span id="UserCapacityValue"><%=model.UserCapacity%></span><%=SrmWebContent.Array_Status_Percent_Symbol%></div>
            </div>
		</td>
            <% } %>
		<td class="title">
			<span class="srm-uppercase"><%=SrmWebContent.Nas_Capacity_Chart_Title %></span>
			<div class="value <%=model.NasFileCapacityStatus%>"> <span id="NasFileCapacityValue"><%=model.NasFileCapacity%></span><%=SrmWebContent.Array_Status_Percent_Symbol%></div>
		</td>
	</tr>
	<tr class="table-row-srm-asr-c1<%=Resource.ID %>">
        <%if (model.IsArrayModel)
            { %>
		<td>
			<div class="status-bar-background">
				<div id="UserCapacityStatusBar<%=Resource.ID %>" class="status-bar"></div>
			</div>
		</td>
            <% } %>
		<td>
			<div class="status-bar-background">
				<div id="NasFileCapacityStatusBar<%=Resource.ID %>" class="status-bar"></div>
			</div>
		</td>
	</tr>
	<tr class="table-row-srm-asr-c1<%=Resource.ID %>">
        <%if (model.IsArrayModel)
            { %>
		<td class="value-info"> <span id="UserCapacityCurrent<%=Resource.ID %>"></span>&nbsp;/ <span id="UserCapacityTotal<%=Resource.ID %>"></span></td>
        <% } %>
		<td class="value-info"> <span id="NasFileCapacityCurrent<%=Resource.ID %>"></span>&nbsp;/ <span id="NasFileCapacityTotal<%=Resource.ID %>"></span></td>
	</tr>
</table>
<script type="text/javascript">
	$(function () {
		SW.SRM.Charts.ArrayStatusResource.create(<%=model.Data%>);
		$("#UserCapacityStatusBar<%=Resource.ID %>").css("width", "<%=model.UserCapacity%>%");
		$("#NasFileCapacityStatusBar<%=Resource.ID %>").css("width", "<%=model.NasFileCapacity%>%");
			    
		$("#NasFileCapacityCurrent<%=Resource.ID %>").text(SW.SRM.Formatters.FormatCapacity(<%=model.NasFileCapacityCurrent%>,2));
		$("#NasFileCapacityTotal<%=Resource.ID %>").text(SW.SRM.Formatters.FormatCapacity(<%=model.NasFileCapacityTotal%>,2));
		$("#UserCapacityCurrent<%=Resource.ID %>").text(SW.SRM.Formatters.FormatCapacity(<%=model.UserCapacityCurrent%>,2));
		$("#UserCapacityTotal<%=Resource.ID %>").text(SW.SRM.Formatters.FormatCapacity(<%=model.UserCapacityTotal%>,2));
	});
		   
</script>
