﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AggregatedNASVolumeCapacity.ascx.cs" Inherits="Orion_SRM_Resources_Charting_AggregatedNASVolumeCapacity" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" Module="SRM" File="Charts/Charts.SRM.AggregatedNASVolumeCapacity.js" />
<orion:Include runat="server" Module="SRM" File="AggregatedNASVolumeCapacity.css" />
<orion:Include runat="server" Module="SRM" File="SRM.css" />
<orion:Include runat="server" Module="SRM" File="SRM.Formatters.js" />

<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <div class="srm-aggregatedNASVolumeCapacity-main">
            <div id="srm-aggregatedNASVolumeCapacity-chart-<%=Resource.ID%>" class="srm-aggregatedNASVolumeCapacity-chart"></div>
            <div id="srm-aggregatedNASVolumeCapacity-legend-<%=Resource.ID%>" class="srm-aggregatedNASVolumeCapacity-legend"></div>
        </div>
        <script type="text/javascript">
            $(function () {
                new SW.SRM.Charts.AggregatedNASVolumeCapacity().init(<%=Data%>);
            });
        </script>
    </Content>
</orion:ResourceWrapper>

