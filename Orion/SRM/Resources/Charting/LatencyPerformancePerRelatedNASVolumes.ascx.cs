﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Common.Enums;
using System.Globalization;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_SRM_Resources_Charting_LatencyPerformancePerRelatedNASVolumes: PerformanceChartBase
{
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IVolumeProvider) }; }
    }

    protected override string DefaultTitle
    {
        get { return SrmWebContent.LatencyPerformancePerRelatedNASVolumes_Resource_Title; }
    }

    protected string Data
    {
        get
        {
            int volumeID = int.Parse(this.Request["NetObject"].Split(':')[1]);            
            List<PoolEntity> pools = ServiceLocator.GetInstance<IPoolDAL>().GetPoolsByVolumeId(volumeID);
            List<object> listPools = new List<object>();
            foreach (PoolEntity pool in pools)
            {
                listPools.Add(new { id = pool.PoolID, name = pool.DisplayName });
            }

            return Serializer.Serialize(new
            {
                ResourceId = this.Resource.ID,
                NetObjectId = this.Request["NetObject"],
                SampleSizeInMinutes = this.SampleSizeInMinutesProperty,
                DaysOfDataToLoad = this.DaysOfDataToLoadProperty,
                YAxisTitle = SrmWebContent.LatencyPerformancePerRelatedNASVolumes_Chart_yAxis_Title,
                ChartTitle = !String.IsNullOrEmpty(this.ChartTitleProperty)
                    ? this.ChartTitleProperty
                    : pools != null && pools.Count > 0
                        ? string.Format(CultureInfo.CurrentCulture, "{0}: {1}",
                            SrmWebContent.IOPSPerformancePerRelatedNASVolumes_Chart_Title,
                            pools[0].DisplayName)
                        : SrmWebContent.IOPSPerformancePerRelatedNASVolumes_Chart_Title,
                ChartSubTitle = this.ChartSubTitleProperty,
                PoolID = pools != null && pools.Count > 0 ? pools[0].PoolID : -1,
                Pools = listPools,
                DataUrl = "/Orion/SRM/Services/ChartData.asmx/GetPerformancePerRelatedNASVolumes",
                ShowParent = false,
                HasParent = false,
                ChartInitialZoom = this.ChartInitialZoomProperty,
                NumberOfSeriesToShow = this.NumberOfSeriesToShowProperty
            });
        }
    }
    public override string EditURL
    {
        get
        {
            return this.ExtendCustomEditUrl("/Orion/SRM/EditTopXXChartResource.aspx?EditXX=True");
        }
    }

    public override string HelpLinkFragment
    {
        get { return "SRMPHResourcePoolLatency"; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        base.Page_Init(sender, e);
        Volume volume = GetInterfaceInstance<IVolumeProvider>().Volume;

        if (volume.StorageArray.Array.StorageArrayId != null)
        {
            var deviceGroup =
                ServiceLocator.GetInstance<IDeviceGroupDAL>()
                    .GetDeviceGroupDetailsByArrayId(volume.StorageArray.Array.StorageArrayId.Value);

            string hideLatencyHistogram;
            if (deviceGroup.Properties.TryGetValue("HideLatencyHistogramFile", out hideLatencyHistogram))
            {
                bool isHidden;
                if (!bool.TryParse(hideLatencyHistogram, out isHidden))
                {
                    isHidden = false;
                }
                this.Visible = !isHidden;
            }
        }
    }

    protected override bool IsResourceVisible()
    {
        IVolumeProvider volumeProvider = GetInterfaceInstance<IVolumeProvider>();
        if (volumeProvider != null)
        {
            return ServiceLocator.GetInstance<IResourceVisibilityService>()
                .IsResourceVisibleForArray(volumeProvider.Volume.StorageArray, this.AppRelativeVirtualPath);
        }
        return true;
    }
}