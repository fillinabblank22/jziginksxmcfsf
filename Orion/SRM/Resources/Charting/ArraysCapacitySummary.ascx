﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ArraysCapacitySummary.ascx.cs" Inherits="Orion_SRM_Resources_Charting_ArraysCapacitySummary" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" Module="SRM" File="Charts/Charts.SRM.ArraysCapacitySummary.js" />
<orion:Include runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include ID="Include1" runat="server" Module="SRM" File="SRM.Common.js" />
<orion:Include runat="server" Module="SRM" File="ArrayCapacitySummary.css" />
<orion:Include runat="server" Module="SRM" File="SRM.css" />

<orion:resourceWrapper CssClass="srm_overflow_hidden" ID="ResourceWrapper" runat="server">
    <Content>
       <div id="srm-arrayscapacitysummary-toolbar-area_<%=Resource.ID%>" class="srm-arraycapacitysummary-toolbar"></div>
       <table>
           <tr>
               <td>
                    <div id="srm-arrayscapacitysummary_top_charts_legend_area_<%=Resource.ID%>" ></div>  
               </td>
           </tr>
       </table>
         
       <div id="srm-arrayscapacitysummary-chart-area_<%=Resource.ID%>"></div>
       <div id="srm-arrayscapacitysummary-pagingToolbar-area_<%=Resource.ID%>" class="srm-arraycapacitysummary-pagingToolbar"></div>
        <script type="text/javascript">
            $(function () {
                new SW.SRM.Charts.ArraysCapacitySummary().init(<%=Data%>, 
                    { 
                        searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                        searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                        pageSize: '<%= PageSize %>'
                    });
            });
        </script>
    </Content>
</orion:resourceWrapper>
