﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LatencyHistogramList.ascx.cs" Inherits="Orion.SRM.Resources.DrillDowns.Orion_SRM_Resources_DrillDowns_LatencyHistogramList" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Resources" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<%@ Register Src="~/Orion/SRM/Controls/Redirector.ascx" TagPrefix="srm" TagName="Redirector" %>
<script type="text/javascript">
    $(function () {
        var topTitle = $('*[id*=ResourceTitle]');
        topTitle.html("<%= TopTitle%>");
        topTitle.addClass("SRM_DrillDown_Top_Title");
        $('.srm-latency-drilldown-button').removeClass("srm-latency-drilldown-button-selected");
        $('*[range*=<%=this.Resource.Properties["category"].Replace("+","plus")%>]').addClass( "srm-latency-drilldown-button-selected" );
    });
</script>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <srm:Redirector ID="Redirector1" runat="server" />
        <orion:Include ID="Include5" runat="server" Module="SRM" File="SRM.css" />
        <orion:Include ID="Include2" runat="server" Module="SRM" File="SRMLatencyDrillDown.css" />
        <orion:Include ID="Include1" runat="server" Module="SRM" File="SRM.Formatters.js" />
        <orion:Include ID="Include3" runat="server" Module="SRM" File="SRM.Common.js" />
        <orion:Include ID="Include6" runat="server" File="visibilityObserver.js" />
        
        <div class="srm-latency-drilldown-buttons">
            <span><%= SrmWebContent.LatencyHistogram_DrillDown_Grouping_Label %>:</span>
            <input range="All" class="srm-latency-drilldown-button" onclick="applyFilter($(this))" type="button" value="<%= SrmWebContent.LatencyHistogram_DrillDown_Button_All %>">
            <input range="0-5" class="srm-latency-drilldown-button" onclick="applyFilter($(this))" type="button" value="0-5<%= SrmWebContent.LatencyHistogram_DrillDown_Latency_Unit %>">
            <input range="6-10" class="srm-latency-drilldown-button" onclick="applyFilter($(this))" type="button" value="6-10<%= SrmWebContent.LatencyHistogram_DrillDown_Latency_Unit %>">
            <input range="11-15" class="srm-latency-drilldown-button" onclick="applyFilter($(this))" type="button" value="11-15<%= SrmWebContent.LatencyHistogram_DrillDown_Latency_Unit %>">
            <input range="16-20" class="srm-latency-drilldown-button" onclick="applyFilter($(this))" type="button" value="16-20<%= SrmWebContent.LatencyHistogram_DrillDown_Latency_Unit %>">
            <input range="21-30" class="srm-latency-drilldown-button" onclick="applyFilter($(this))" type="button" value="21-30<%= SrmWebContent.LatencyHistogram_DrillDown_Latency_Unit %>">
            <input range="31-50" class="srm-latency-drilldown-button" onclick="applyFilter($(this))" type="button" value="31-50<%= SrmWebContent.LatencyHistogram_DrillDown_Latency_Unit %>">
            <input range="51-100" class="srm-latency-drilldown-button" onclick="applyFilter($(this))" type="button" value="51-100<%= SrmWebContent.LatencyHistogram_DrillDown_Latency_Unit %>">
            <input range="101-500" class="srm-latency-drilldown-button" onclick="applyFilter($(this))" type="button" value="101-500<%= SrmWebContent.LatencyHistogram_DrillDown_Latency_Unit %>">
            <input range="501-1000" class="srm-latency-drilldown-button" onclick="applyFilter($(this))" type="button" value="501-1000<%= SrmWebContent.LatencyHistogram_DrillDown_Latency_Unit %>">
            <input range="1000plus" class="srm-latency-drilldown-button" onclick="applyFilter($(this))" type="button" value="1001+<%= SrmWebContent.LatencyHistogram_DrillDown_Latency_Unit %>">
        </div>
        
        <orion:CustomQueryTable runat="server" ID="CustomTable" />
        <script type="text/javascript">
            $(function() {
                SW.Core.Resources.CustomQuery.initialize(
                {
                    uniqueId: '<%= CustomTable.UniqueClientID %>',
                    initialPage: 0,
                    rowsPerPage: <%= this.GetPageSize() %>,
                    allowSort: true,
                    columnSettings: {
                        "Name": {
                            header: SW.SRM.Formatters.FormatColumnTitle('<%=ColumnNameHeader%>'),
                            cellCssClassProvider: function(value, row, cellInfo) { return "SRM_Icon_Cell srm_word_wrap"; }
                        },
                        "PoolName": {
                            header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.LatencyHistogram_DrillDown_Pool_Name%>'),
                            cellCssClassProvider: function(cellValue, rowArray, cellInfo) {
                                return "LatencyHistogramListAssociatedPools<%=this.Resource.ID%> SRM_HiddenGridCell srm_word_wrap";
                            }
                        },
                        "AssociatedEndpoint": {
                            header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.LatencyHistogram_DrillDown_Associated_Endpoint%>'),
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "LatencyHistogramListAssociatedEndpoint<%=this.Resource.ID%> SRM_HiddenGridCell srm_word_wrap";
                                }
                            },
                        "IOLatencyTotal": {
                            header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.LatencyHistogram_DrillDown_LatencyTotal%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatLatencyNumberOrUnknown(value, "<%=SrmWebContent.LatencyHistogram_DrillDown_Latency_Unit%>");
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_NoWrap " + SW.SRM.Formatters.ApplyThresholds(rowArray[11], rowArray[12]);
                                }
                            },
                        "IOLatencyWrite": {
                            header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.LatencyHistogram_DrillDown_LatencyWrite%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatLatencyNumberOrUnknown(value, "<%=SrmWebContent.LatencyHistogram_DrillDown_Latency_Unit%>");
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_NoWrap " + SW.SRM.Formatters.ApplyThresholds(rowArray[13], rowArray[14]);
                                }
                            },
                        "IOLatencyRead": {
                            header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.LatencyHistogram_DrillDown_LatencyRead%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatLatencyNumberOrUnknown(value, "<%=SrmWebContent.LatencyHistogram_DrillDown_Latency_Unit%>");
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_NoWrap " + SW.SRM.Formatters.ApplyThresholds(rowArray[15], rowArray[16]);
                                }
                            },
                        "IOLatencyOther": {
                            header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.LatencyHistogram_DrillDown_LatencyOther%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatLatencyNumberOrUnknown(value, "<%=SrmWebContent.LatencyHistogram_DrillDown_Latency_Unit%>");
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_NoWrap " + SW.SRM.Formatters.ApplyThresholds(rowArray[17], rowArray[18]);
                                }
                            }
                            
                    },
                    searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                    searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                    onLoad: function(rows, columnsInfo) {
                        var params = {
                            resourceID: <%=this.Resource.ID%>,
                                netObjectFieldName:"_NetObjectID",
                                rows:  rows, 
                                columnsInfo: columnsInfo,
                                classesName: {
                                    associatedPools:"LatencyHistogramListAssociatedPools",
                                    associatedEndpoint: "LatencyHistogramListAssociatedEndpoint",
                                    volumeSpaceUsed: null,
                                    volumeSpacePercent: null
                                }
                            };
                            SW.SRM.Common.FillAdditionColumns(params);
                        }
                });
                SW.Core.View.AddOnRefresh(applyFilter, '<%= CustomTable.ClientID %>');
                applyFilter($('.srm-latency-drilldown-button-selected'));
            });

            function applyFilter(btn) {
                $('.srm-latency-drilldown-button').removeClass("srm-latency-drilldown-button-selected");
                btn.addClass( "srm-latency-drilldown-button-selected" );
                var swql = "<%=this.SWQL %>";
                swql = swql.replace('<%=this.FilterPattern %>', getFilter(btn.attr('range')));
                var searchSWQL = "<%=this.SearchSWQL %>";
                searchSWQL = searchSWQL.replace('<%=this.FilterPattern %>', getFilter(btn.attr('range')));

                SW.Core.Resources.CustomQuery.setQuery("<%=CustomTable.UniqueClientID %>", swql);
                SW.Core.Resources.CustomQuery.setSearchQuery("<%=CustomTable.UniqueClientID %>", searchSWQL);
                SW.Core.Resources.CustomQuery.refresh("<%=CustomTable.UniqueClientID %>");
            }
            
            function getFilter(currentFilter) {
                var filters = currentFilter.split('-');
                if (filters.length == 2)
                    return "HAVING Round(SUM(IOLatencyTotal * Weight) / SUM(Weight), 0) >= " + filters[0] + " AND Round(SUM(IOLatencyTotal * Weight) / SUM(Weight), 0)  <= " + filters[1];
                if (currentFilter == "1000plus")
                    return " HAVING Round(SUM(IOLatencyTotal * Weight) / SUM(Weight), 0) > 1000 ";
                return "";
            }
        </script>
    </Content>
</orion:resourceWrapper>