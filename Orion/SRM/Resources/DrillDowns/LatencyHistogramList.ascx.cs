﻿using System;
using System.Collections.Generic;
using System.Linq;
using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Enums;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;

namespace Orion.SRM.Resources.DrillDowns
{
    [ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
    public partial class Orion_SRM_Resources_DrillDowns_LatencyHistogramList : SrmBaseResourceControl
    {
        private const string arrayLunQueryTemplate = @"SELECT 
                                        LUNsThresholds.DisplayName AS Name, 
                                        Concat('%{1}:', LUNsThresholds._ObjectId,'%') AS [PoolName],
                                        '/Orion/View.aspx?NetObject={1}:' + ToString(LUNsThresholds._ObjectId) AS [_LinkFor_Name],
                                        '/Orion/StatusIcon.ashx?entity={3}&status=' + ToString(LUNsThresholds.Status) +'&size=small' AS [_IconFor_Name],
									    Concat('%{1}:', LUNsThresholds._ObjectId,'%') AS AssociatedEndpoint,
                                        Concat('{1}:', LUNsThresholds._ObjectId) AS _NetObjectID,
                                        {4}
                                        {5}
                                        FROM Orion.SRM.LUNStatistics (nolock=true)
                                        INNER JOIN ( " + LUNsThresholds + @" ) LUNsThresholds ON LUNStatistics.LunID = LUNsThresholds._ObjectId
                                        WHERE {0}";

        private const string LUNsThresholds = @" SELECT LUNID as [_ObjectId],
		                                        [StorageArrayId], 
		                                        [DisplayName], 
		                                        [Status],
		                                        SUM([IOLatencyTotal]) AS [IOLatencyTotal], 
		                                        SUM([IOLatencyWrite]) AS [IOLatencyWrite], 
		                                        SUM([IOLatencyRead])  AS [IOLatencyRead], 
		                                        SUM([IOLatencyOther]) AS [IOLatencyOther],
		                                        SUM([_WarningTotal]) AS [_WarningTotal], SUM([_CriticalTotal]) AS [_CriticalTotal],
		                                        SUM([_WarningRead])  AS [_WarningRead],  SUM([_CriticalRead])  AS [_CriticalRead],
		                                        SUM([_WarningWrite]) AS [_WarningWrite], SUM([_CriticalWrite]) AS [_CriticalWrite],
		                                        SUM([_WarningOther]) AS [_WarningOther], SUM([_CriticalOther]) AS [_CriticalOther]
	                                        FROM (
	                                          SELECT LUNID,
		                                         StorageArrayID as [StorageArrayId],
		                                         DisplayName as [DisplayName],
		                                         Status as [Status],
			
		                                        IOLatencyTotal AS [IOLatencyTotal],
		                                        0 AS [IOLatencyRead],
		                                        0 AS [IOLatencyWrite],
		                                        0 AS [IOLatencyOther],

		                                        CASE WHEN LUNs.IOLatencyTotalThreshold.IsLevel1State = true THEN 1 ELSE 0 END AS [_WarningTotal],
		                                        CASE WHEN LUNs.IOLatencyTotalThreshold.IsLevel2State = true THEN 1 ELSE 0 END AS [_CriticalTotal],
		                                        0 AS [_WarningRead],
		                                        0 AS [_CriticalRead],
		                                        0 AS [_WarningWrite],
		                                        0 AS [_CriticalWrite],
		                                        0 AS [_WarningOther],
		                                        0 AS [_CriticalOther]
 	                                          FROM Orion.SRM.LUNs (nolock=true)
	                                          UNION ALL (
		                                        SELECT	LUNID,
			                                        StorageArrayID as [StorageArrayId],
			                                        DisplayName as [DisplayName],
			                                        Status as [Status],
			
			                                        0 AS [IOLatencyTotal],
			                                        IOLatencyRead AS [IOLatencyRead],
			                                        0 AS [IOLatencyWrite],
			                                        0 AS [IOLatencyOther],

			                                        0 AS [_WarningTotal],
			                                        0 AS [_CriticalTotal],
			                                        CASE WHEN LUNs.IOLatencyReadThreshold.IsLevel1State = true THEN 1 ELSE 0 END AS [_WarningRead],
			                                        CASE WHEN LUNs.IOLatencyReadThreshold.IsLevel2State = true THEN 1 ELSE 0 END AS [_CriticalRead],
			                                        0 AS [_WarningWrite],
			                                        0 AS [_CriticalWrite],
			                                        0 AS [_WarningOther],
			                                        0 AS [_CriticalOther]
		                                        FROM Orion.SRM.LUNs (nolock=true)
	                                          ) UNION ALL (
		                                        SELECT	LUNID,
			                                        StorageArrayID as [StorageArrayId],
			                                        DisplayName as [DisplayName],
			                                        Status as [Status],

			                                        0 AS [IOLatencyTotal],
			                                        0 AS [IOLatencyRead],
			                                        IOLatencyWrite AS [IOLatencyWrite],
			                                        0 AS [IOLatencyOther],

			                                        0 AS [_WarningTotal],
			                                        0 AS [_CriticalTotal],
			                                        0 AS [_WarningRead],
			                                        0 AS [_CriticalRead],
			                                        CASE WHEN LUNs.IOLatencyWriteThreshold.IsLevel1State = true THEN 1 ELSE 0 END AS [_WarningWrite],
			                                        CASE WHEN LUNs.IOLatencyWriteThreshold.IsLevel2State = true THEN 1 ELSE 0 END AS [_CriticalWrite],
			                                        0 AS [_WarningOther],
			                                        0 AS [_CriticalOther]
		                                        FROM Orion.SRM.LUNs (nolock=true)
	                                           ) UNION ALL (
		                                        SELECT	LUNID,
			                                        StorageArrayID as [StorageArrayId],
			                                        DisplayName as [DisplayName],
			                                        Status as [Status],

			                                        0 AS [IOLatencyTotal],
			                                        0 AS [IOLatencyRead],
			                                        0 AS [IOLatencyWrite],
			                                        IOLatencyOther AS [IOLatencyOther],

			                                        0 AS [_WarningTotal],
			                                        0 AS [_CriticalTotal],
			                                        0 AS [_WarningRead],
			                                        0 AS [_CriticalRead],
			                                        0 AS [_WarningWrite],
			                                        0 AS [_CriticalWrite],
			                                        CASE WHEN LUNs.IOLatencyOtherThreshold.IsLevel1State = true THEN 1 ELSE 0 END AS [_WarningOther],
			                                        CASE WHEN LUNs.IOLatencyOtherThreshold.IsLevel2State = true THEN 1 ELSE 0 END AS [_CriticalOther]
		                                        FROM Orion.SRM.LUNs (nolock=true)
	                                           )
	                                        )
	                                        GROUP BY [LUNID], [StorageArrayId], [DisplayName], [Status] ";

        private const string arrayLunWhereLastHours = @"LUNsThresholds.StorageArrayID = {0} {1} ";
        private const string arrayLunWhereLastPoll = @"LUNsThresholds.StorageArrayID = {0} AND ObservationTimestamp = (
                                                            SELECT MAX(ObservationTimestamp) AS LastPollDateTime 
                                                            FROM Orion.SRM.StorageArrayStatistics (nolock=true) 
                                                            WHERE StorageArrayStatistics.StorageArrayID = {0}
                                                        ) ";

        private const string arrayLunGrouping = @" GROUP BY LUNsThresholds.DisplayName, 
                                                            LUNsThresholds._ObjectId, 
                                                            LUNsThresholds.Status,
                                                            LUNsThresholds._WarningTotal,
                                                            LUNsThresholds._CriticalTotal,
                                                            LUNsThresholds._WarningRead,
                                                            LUNsThresholds._CriticalRead,
                                                            LUNsThresholds._WarningWrite,
                                                            LUNsThresholds._CriticalWrite,
                                                            LUNsThresholds._WarningOther,
                                                            LUNsThresholds._CriticalOther  {0}";

        private const string poolLunQueryTemplate = @"SELECT LUNsThresholds.DisplayName AS Name, 
                                        '/Orion/View.aspx?NetObject={1}:' + ToString(LUNsThresholds._ObjectId) AS [_LinkFor_Name],
                                        '/Orion/StatusIcon.ashx?entity={2}&status=' + ToString(LUNsThresholds.Status) +'&size=small' AS [_IconFor_Name],
									    Concat('%{1}:', LUNsThresholds._ObjectId,'%') AS AssociatedEndpoint,
                                        Concat('{1}:', LUNsThresholds._ObjectId) AS _NetObjectID,
                                        {3}
                                        {4}
                                        FROM Orion.SRM.LUNStatistics (nolock=true)
                                        INNER JOIN ( " + LUNsThresholds + @" ) LUNsThresholds ON LUNStatistics.LunID = LUNsThresholds._ObjectId
                                        WHERE {0}";

        private const string poolLunWhereLastHours = @"LUNStatistics.LUN.Pools.PoolID = {0} {1} ";
        private const string poolLunWhereLastPoll = @"LUNStatistics.LUN.Pools.PoolID = {0} AND ObservationTimestamp = (
                                                            SELECT MAX(ObservationTimestamp) AS LastPollDateTime 
                                                            FROM Orion.SRM.PoolStatistics (nolock=true)
                                                            WHERE PoolStatistics.PoolID = {0}
                                                        ) ";

        private const string poolLunGrouping = @" GROUP BY LUNsThresholds.DisplayName, 
                                                           LUNsThresholds._ObjectId, 
                                                           LUNsThresholds.Status,
                                                           LUNsThresholds._WarningTotal,
                                                           LUNsThresholds._CriticalTotal,
                                                           LUNsThresholds._WarningRead,
                                                           LUNsThresholds._CriticalRead,
                                                           LUNsThresholds._WarningWrite,
                                                           LUNsThresholds._CriticalWrite,
                                                           LUNsThresholds._WarningOther,
                                                           LUNsThresholds._CriticalOther {0}";


        private const string VolumeThresholds = @"
                                            SELECT VolumeID as [_ObjectId],
		                                        [StorageArrayId], 
		                                        [DisplayName], 
		                                        [Status],
		                                        SUM([IOLatencyTotal]) AS [IOLatencyTotal], 
		                                        SUM([IOLatencyWrite]) AS [IOLatencyWrite], 
		                                        SUM([IOLatencyRead])  AS [IOLatencyRead], 
		                                        SUM([IOLatencyOther]) AS [IOLatencyOther],
		                                        SUM([_WarningTotal]) AS [_WarningTotal], SUM([_CriticalTotal]) AS [_CriticalTotal],
		                                        SUM([_WarningRead])  AS [_WarningRead],  SUM([_CriticalRead])  AS [_CriticalRead],
		                                        SUM([_WarningWrite]) AS [_WarningWrite], SUM([_CriticalWrite]) AS [_CriticalWrite],
		                                        SUM([_WarningOther]) AS [_WarningOther], SUM([_CriticalOther]) AS [_CriticalOther]
	                                        FROM (
	                                          SELECT VolumeID,
		                                         StorageArrayID as [StorageArrayId],
		                                         DisplayName as [DisplayName],
		                                         Status as [Status],
			
		                                        IOLatencyTotal AS [IOLatencyTotal],
		                                        0 AS [IOLatencyRead],
		                                        0 AS [IOLatencyWrite],
		                                        0 AS [IOLatencyOther],

		                                        CASE WHEN Volumes.IOLatencyTotalThreshold.IsLevel1State = true THEN 1 ELSE 0 END AS [_WarningTotal],
		                                        CASE WHEN Volumes.IOLatencyTotalThreshold.IsLevel2State = true THEN 1 ELSE 0 END AS [_CriticalTotal],
		                                        0 AS [_WarningRead],
		                                        0 AS [_CriticalRead],
		                                        0 AS [_WarningWrite],
		                                        0 AS [_CriticalWrite],
		                                        0 AS [_WarningOther],
		                                        0 AS [_CriticalOther]
 	                                          FROM Orion.SRM.Volumes (nolock=true)
	                                          UNION ALL (
		                                        SELECT	VolumeID,
			                                        StorageArrayID as [StorageArrayId],
			                                        DisplayName as [DisplayName],
			                                        Status as [Status],
			
			                                        0 AS [IOLatencyTotal],
			                                        IOLatencyRead AS [IOLatencyRead],
			                                        0 AS [IOLatencyWrite],
			                                        0 AS [IOLatencyOther],

			                                        0 AS [_WarningTotal],
			                                        0 AS [_CriticalTotal],
			                                        CASE WHEN Volumes.IOLatencyReadThreshold.IsLevel1State = true THEN 1 ELSE 0 END AS [_WarningRead],
			                                        CASE WHEN Volumes.IOLatencyReadThreshold.IsLevel2State = true THEN 1 ELSE 0 END AS [_CriticalRead],
			                                        0 AS [_WarningWrite],
			                                        0 AS [_CriticalWrite],
			                                        0 AS [_WarningOther],
			                                        0 AS [_CriticalOther]
		                                        FROM Orion.SRM.Volumes (nolock=true)
	                                          ) UNION ALL (
		                                        SELECT	VolumeID,
			                                        StorageArrayID as [StorageArrayId],
			                                        DisplayName as [DisplayName],
			                                        Status as [Status],

			                                        0 AS [IOLatencyTotal],
			                                        0 AS [IOLatencyRead],
			                                        IOLatencyWrite AS [IOLatencyWrite],
			                                        0 AS [IOLatencyOther],

			                                        0 AS [_WarningTotal],
			                                        0 AS [_CriticalTotal],
			                                        0 AS [_WarningRead],
			                                        0 AS [_CriticalRead],
			                                        CASE WHEN Volumes.IOLatencyWriteThreshold.IsLevel1State = true THEN 1 ELSE 0 END AS [_WarningWrite],
			                                        CASE WHEN Volumes.IOLatencyWriteThreshold.IsLevel2State = true THEN 1 ELSE 0 END AS [_CriticalWrite],
			                                        0 AS [_WarningOther],
			                                        0 AS [_CriticalOther]
		                                        FROM Orion.SRM.Volumes (nolock=true)
	                                           ) UNION ALL (
		                                        SELECT	VolumeID,
			                                        StorageArrayID as [StorageArrayId],
			                                        DisplayName as [DisplayName],
			                                        Status as [Status],

			                                        0 AS [IOLatencyTotal],
			                                        0 AS [IOLatencyRead],
			                                        0 AS [IOLatencyWrite],
			                                        IOLatencyOther AS [IOLatencyOther],

			                                        0 AS [_WarningTotal],
			                                        0 AS [_CriticalTotal],
			                                        0 AS [_WarningRead],
			                                        0 AS [_CriticalRead],
			                                        0 AS [_WarningWrite],
			                                        0 AS [_CriticalWrite],
			                                        CASE WHEN Volumes.IOLatencyOtherThreshold.IsLevel1State = true THEN 1 ELSE 0 END AS [_WarningOther],
			                                        CASE WHEN Volumes.IOLatencyOtherThreshold.IsLevel2State = true THEN 1 ELSE 0 END AS [_CriticalOther]
		                                        FROM Orion.SRM.Volumes (nolock=true)
	                                           )
	                                        )
	                                        GROUP BY [VolumeID], [StorageArrayId], [DisplayName], [Status]
        ";


        private const string arrayVolumeQueryTemplate = @"SELECT 
                                        VolumeThresholds.DisplayName AS Name, 
                                        Concat('%{1}:', VolumeThresholds._ObjectId,'%') AS PoolName,
                                        '/Orion/View.aspx?NetObject={1}:' + ToString(VolumeThresholds._ObjectId) AS [_LinkFor_Name],
										'/Orion/StatusIcon.ashx?entity={3}&status=' + ToString(ISNULL(VolumeThresholds.Status, 0)) +'&size=small' AS [_IconFor_Name],
									    '' AS _AssociatedEndpoint,
                                        Concat('{1}:', VolumeThresholds._ObjectId) AS _NetObjectID,
                                        {4}
                                        {5}
                                        FROM Orion.SRM.VolumeStatistics (nolock=true)
                                        INNER JOIN ( " + VolumeThresholds + @" ) VolumeThresholds ON VolumeStatistics.VolumeID = VolumeThresholds._ObjectId
                                        WHERE {0}";
        private const string arrayVolumeWhereLastHours = @"VolumeThresholds.StorageArrayID = {0} {1} ";

        private const string arrayVolumeWhereLastPoll = @"VolumeThresholds.StorageArrayID = {0} AND ObservationTimestamp = (
                                                            SELECT MAX(ObservationTimestamp) AS LastPollDateTime 
                                                            FROM Orion.SRM.StorageArrayStatistics (nolock=true) 
                                                            WHERE StorageArrayStatistics.StorageArrayID = {0}
                                                        ) ";

        private const string arrayVolumeGrouping =  @" GROUP BY VolumeThresholds.DisplayName, 
                                                                VolumeThresholds._ObjectId, 
                                                                VolumeThresholds.Status,
                                                                VolumeThresholds._WarningTotal,
                                                                VolumeThresholds._CriticalTotal,
                                                                VolumeThresholds._WarningRead,
                                                                VolumeThresholds._CriticalRead,
                                                                VolumeThresholds._WarningWrite,
                                                                VolumeThresholds._CriticalWrite,
                                                                VolumeThresholds._WarningOther,
                                                                VolumeThresholds._CriticalOther  {0}";


        private const string poolVolumeQueryTemplate = @"SELECT 
                                        VolumeThresholds.DisplayName AS Name, 
                                        '/Orion/View.aspx?NetObject={1}:' + ToString(VolumeThresholds._ObjectId) AS [_LinkFor_Name],
										'/Orion/StatusIcon.ashx?entity={2}&status=' + ToString(ISNULL(VolumeThresholds.Status, 0)) +'&size=small' AS [_IconFor_Name],
                                        {3}
                                        {4}
                                        FROM Orion.SRM.VolumeStatistics (nolock=true)
                                        INNER JOIN ( " + VolumeThresholds + @" ) VolumeThresholds ON VolumeStatistics.VolumeID = VolumeThresholds._ObjectId
                                        WHERE {0}";
        private const string poolVolumeWhereLastHours = @"VolumeStatistics.Volume.Pools.PoolID = {0} {1} ";
        private const string poolVolumeWhereLastPoll = @"VolumeStatistics.Volume.Pools.PoolID = {0} AND ObservationTimestamp = (
                                                            SELECT MAX(ObservationTimestamp) AS LastPollDateTime 
                                                            FROM Orion.SRM.PoolStatistics (nolock=true)
                                                            WHERE PoolStatistics.PoolID = {0}
                                                        ) ";

        private const string poolVolumeGrouping = @" GROUP BY VolumeThresholds.DisplayName, 
                                                           VolumeThresholds._ObjectId, 
                                                           VolumeThresholds.Status,
                                                           VolumeThresholds._WarningTotal,
                                                           VolumeThresholds._CriticalTotal,
                                                           VolumeThresholds._WarningRead,
                                                           VolumeThresholds._CriticalRead,
                                                           VolumeThresholds._WarningWrite,
                                                           VolumeThresholds._CriticalWrite,
                                                           VolumeThresholds._WarningOther,
                                                           VolumeThresholds._CriticalOther {0}";

        private const string latencyFields = @"SUM(LUNStatistics.IOLatencyTotal * Weight) / SUM(Weight) AS IOLatencyTotal,
                                        SUM(LUNStatistics.IOLatencyWrite * Weight) / SUM(Weight) AS IOLatencyWrite,
                                        SUM(LUNStatistics.IOLatencyRead * Weight) / SUM(Weight) AS IOLatencyRead,
                                        SUM(LUNStatistics.IOLatencyOther * Weight) / SUM(Weight) AS IOLatencyOther";

        private const string volumeFields = @"SUM(VolumeStatistics.IOLatencyTotal * Weight) / SUM(Weight) AS IOLatencyTotal,
                                        SUM(VolumeStatistics.IOLatencyWrite * Weight) / SUM(Weight) AS IOLatencyWrite,
                                        SUM(VolumeStatistics.IOLatencyRead * Weight) / SUM(Weight) AS IOLatencyRead,
                                        SUM(VolumeStatistics.IOLatencyOther * Weight) / SUM(Weight) AS IOLatencyOther";

        private const string lunThresholdFields = @", LUNsThresholds._WarningTotal
                                                    , LUNsThresholds._CriticalTotal
                                                    , LUNsThresholds._WarningRead 
                                                    , LUNsThresholds._CriticalRead
                                                    , LUNsThresholds._WarningWrite 
                                                    , LUNsThresholds._CriticalWrite
                                                    , LUNsThresholds._WarningOther
                                                    , LUNsThresholds._CriticalOther";

        private const string volumeThresholdFields =  @", VolumeThresholds._WarningTotal
                                                        , VolumeThresholds._CriticalTotal
                                                        , VolumeThresholds._WarningRead 
                                                        , VolumeThresholds._CriticalRead
                                                        , VolumeThresholds._WarningWrite 
                                                        , VolumeThresholds._CriticalWrite
                                                        , VolumeThresholds._WarningOther
                                                        , VolumeThresholds._CriticalOther";

        private const string thresholdFieldsEmpty = @", false AS _WarningTotal
                                                      , false AS _CriticalTotal
                                                      , false AS _WarningRead
                                                      , false AS _CriticalRead
                                                      , false AS _WarningWrite
                                                      , false AS _CriticalWrite
                                                      , false AS _WarningOther
                                                      , false AS _CriticalOther";

        private const string vserverLunQueryTemplate = @"SELECT 
                                        LUNStatistics.LUN.Caption AS Name, 
                                        Concat('%{1}:', LUNStatistics.LUN.LUNID,'%') AS PoolName,
                                        '/Orion/View.aspx?NetObject={1}:' + ToString(LUNStatistics.LUN.LUNID) AS [_LinkFor_Name],
                                        '/Orion/StatusIcon.ashx?entity={3}&status=' + ToString(LUNStatistics.LUN.Status) +'&size=small' AS [_IconFor_Name],
									    Concat('%{1}:', LUNStatistics.LUN.LUNID,'%') AS AssociatedEndpoint,
                                        Concat('{1}:', LUNStatistics.LUN.LUNID) AS _NetObjectID,
                                        {4}
                                        {5}
                                        FROM Orion.SRM.LUNStatistics (nolock=true)
                                        INNER JOIN ( " + LUNsThresholds + @" ) LUNsThresholds ON LUNStatistics.LUN.LUNID = LUNsThresholds._ObjectId
                                        WHERE {0}";
        private const string vserverLunWhereLastHours = @"LUNStatistics.LUN.RelyVolume.VServers.VServerID = {0} {1} ";
        private const string vserverLunWhereLastPoll = @"LUNStatistics.LUN.RelyVolume.VServers.VServerID = {0} AND ObservationTimestamp = (
                                                            SELECT MAX(ObservationTimestamp) AS LastPollDateTime 
                                                            FROM Orion.SRM.VServerStatistics (nolock=true)
                                                            WHERE VServerStatistics.VServerID = {0}
                                                        ) ";

        private const string vserverLunGrouping = @" GROUP BY LUNStatistics.LUN.Caption, 
                                                            LUNStatistics.LUN.LUNID, 
                                                            LUNStatistics.LUN.Status,
                                                            LUNsThresholds._WarningTotal,
                                                            LUNsThresholds._CriticalTotal,
                                                            LUNsThresholds._WarningRead,
                                                            LUNsThresholds._CriticalRead,
                                                            LUNsThresholds._WarningWrite,
                                                            LUNsThresholds._CriticalWrite,
                                                            LUNsThresholds._WarningOther,
                                                            LUNsThresholds._CriticalOther {0}";

        private const string vserverVolumeQueryTemplate = @"SELECT 
                                        VolumeThresholds.DisplayName AS Name, 
                                        Concat('%{1}:', VolumeThresholds._ObjectId,'%') AS PoolName,
                                        '/Orion/View.aspx?NetObject={1}:' + ToString(VolumeThresholds._ObjectId) AS [_LinkFor_Name],
										'/Orion/StatusIcon.ashx?entity={3}&status=' + ToString(ISNULL(VolumeThresholds.Status, 0)) +'&size=small' AS [_IconFor_Name],
									    '' AS _AssociatedEndpoint,
                                        Concat('{1}:', VolumeThresholds._ObjectId) AS _NetObjectID,
                                        {4}
                                        {5}
                                        FROM Orion.SRM.VolumeStatistics (nolock=true)
                                        INNER JOIN ( " + VolumeThresholds + @" ) VolumeThresholds ON VolumeStatistics.VolumeID = VolumeThresholds._ObjectId
                                        WHERE {0}";
        private const string vserverVolumeWhereLastHours = @"VolumeStatistics.Volume.VServers.VServerID = {0} {1} ";

        private const string vserverVolumeWhereLastPoll = @"VolumeStatistics.Volume.VServers.VServerID = {0}
                                        AND ObservationTimestamp = (SELECT MAX(ObservationTimestamp) AS LastPollDateTime 
                                        FROM Orion.SRM.VolumeStatistics (nolock=true)
                                        WHERE VolumeStatistics.Volume.VServers.VServerID = {0}) ";

        private const string vserverVolumeGrouping = @" GROUP BY 
                                                           VolumeThresholds._ObjectId, 
                                                           VolumeThresholds.DisplayName, 
                                                           VolumeThresholds.Status,
                                                           VolumeThresholds._WarningTotal,
                                                           VolumeThresholds._CriticalTotal,
                                                           VolumeThresholds._WarningRead,
                                                           VolumeThresholds._CriticalRead,
                                                           VolumeThresholds._WarningWrite,
                                                           VolumeThresholds._CriticalWrite,
                                                           VolumeThresholds._WarningOther,
                                                           VolumeThresholds._CriticalOther {0}";


        private static readonly Dictionary<string, string> zoomDictionary = new[] 
        {
            DataTimeRangeEnum.DataLastPoll, 
            DataTimeRangeEnum.DataOneHour, 
            DataTimeRangeEnum.DataTwelveHours, 
            DataTimeRangeEnum.DataTwentyFourHours
        }.ToDictionary(x => x.GetDescription(), x => x.GetLocalizedDescription()); 

        protected override string DefaultTitle
        {
            get { return String.Empty; }
        }

        private T GetValueForCurrentNetObject<T>(Func<T> poolBlock, Func<T> poolFile, Func<T> arrayBlock, Func<T> arrayFile, Func<T> vserverBlock, Func<T> vserverFile)
        {
            T result = default(T);
            
            if (Pool != null)
            {
                result = IsBlock ? poolBlock() : poolFile();
            }
            else if (Array != null)
            {
                result = IsBlock ? arrayBlock() : arrayFile();
            }

            if (Vserver != null)
            {
                result = IsBlock ? vserverBlock() : vserverFile();
            }
            
            return result;
        }

        private T GetValueForCurrentNetObject<T>(Func<T> pool, Func<T> array, Func<T> cluster, Func<T> vserver)
        {
            T result = default(T);
            
            if (Pool != null)
            {
                result = pool();
            }
            else if (Array != null)
            {
                result = Array.Array.IsCluster ? cluster() : array();
            }

            if (Vserver != null)
            {
                result = vserver();
            }
            
            return result;
        }

        public override string DisplayTitle
        {
            get
            {
                return InvariantString.Format(
                    "{0} - {1}",
                    this.GetValueForCurrentNetObject(
                        () => SrmWebContent.LatencyHistogram_DrillDown_Resource_Pool_Block_Title,
                        () => SrmWebContent.LatencyHistogram_DrillDown_Resource_Pool_File_Title,
                        () => SrmWebContent.LatencyHistogram_DrillDown_Resource_Array_Block_Title,
                        () => SrmWebContent.LatencyHistogram_DrillDown_Resource_Array_File_Title,
                        () => SrmWebContent.LatencyHistogram_DrillDown_Resource_Vserver_Block_Title,
                        () => SrmWebContent.LatencyHistogram_DrillDown_Resource_Vserver_File_Title
                    ),
                    zoomDictionary[CurrentZoom]);
            }
        }

        public override string HelpLinkFragment
        {
            get
            {
                return "SRMPHResourceLatencyHistogram";
            }
        }

        public override string EditControlLocation
        {
            get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
        }

        public ResourceSearchControl SearchControl { get; set; }

        public Pool Pool
        {
            get
            {
                IPoolProvider tmp = GetInterfaceInstance<IPoolProvider>();
                return tmp == null ? null : tmp.Pool;
            }
        }

        public StorageArray Array
        {
            get
            {
                IStorageArrayProvider tmp = GetInterfaceInstance<IStorageArrayProvider>();
                return tmp == null ? null : tmp.Array;
            }
        }

        public VServer Vserver
        {
            get
            {
                IVServerProvider tmp = GetInterfaceInstance<IVServerProvider>();
                return tmp == null ? null : tmp.VServer;
            }
        }

        protected string TopTitle
        {
            get
            {
                string statusIconUrl = InvariantString.Format(
                    "/Orion/StatusIcon.ashx?entity={0}&status={1}&size=large",
                    this.GetValueForCurrentNetObject(() => SwisEntities.Pools, () => SwisEntities.StorageArrays, () => SwisEntities.Clusters, () => SwisEntities.VServers),
                    this.GetValueForCurrentNetObject(() => (int)Pool.PoolEntity.Status, () => (int)Array.Array.Status, () => (int)Array.Array.Status, () => (int)Vserver.VServerEntity.Status));
                
                return InvariantString.Format("<img src='{0}'/> <span>{1} - {2}</span>",
                    statusIconUrl,
                    this.GetValueForCurrentNetObject(() => Pool.PoolEntity.DisplayName, () => Array.Array.DisplayName, () => Array.Array.DisplayName, () => Vserver.VServerEntity.DisplayName),
                    DisplayTitle);
            }
        }

        protected string ColumnNameHeader
        {
            get
            {
                return IsBlock
                    ? SrmWebContent.LatencyHistogram_DrillDown_Lun
                    : SrmWebContent.LatencyHistogram_DrillDown_Volume;
            }
        }

        private string GetWhereLastHours()
        {
            int seconds = 60 * 60;
            if (CurrentZoom.Equals(DataTimeRangeEnum.DataTwelveHours.GetDescription()))
                seconds *= 12; 
            if (CurrentZoom.Equals(DataTimeRangeEnum.DataTwentyFourHours.GetDescription()))
                seconds *= 24; 
            return InvariantString.Format(
               "AND ObservationTimestamp <=  AddSecond(Floor(Second(GetUtcDate()) / 1) * 1, DateTrunc('minute', GetUtcDate())) AND ObservationTimestamp >= AddSecond(Floor(Second(GetUtcDate()) / 1) * 1 - {0}, DateTrunc('minute', GetUtcDate())) ", seconds);
        }

        public string SearchQuery
        {
            get
            {
                return IsBlock
                    ? string.Concat(Query, " AND LUNStatistics.LUN.[Caption] LIKE '%${SEARCH_STRING}%'")
                    : string.Concat(Query, " AND VolumeStatistics.Volume.[Caption] LIKE '%${SEARCH_STRING}%'");
            }
        }

        protected string Query
        {
            get
            {
                return GetValueForCurrentNetObject(
                    () => InvariantString.Format(poolLunQueryTemplate, Where, Lun.NetObjectPrefix, SwisEntities.Lun, latencyFields, Thresholds),
                    () => InvariantString.Format(poolVolumeQueryTemplate, Where, Volume.NetObjectPrefix, SwisEntities.Volume, volumeFields, Thresholds),
                    () => InvariantString.Format(arrayLunQueryTemplate, Where, Lun.NetObjectPrefix, Pool.NetObjectPrefix, SwisEntities.Lun, latencyFields, Thresholds),
                    () => InvariantString.Format(arrayVolumeQueryTemplate, Where, Volume.NetObjectPrefix, Pool.NetObjectPrefix, SwisEntities.Volume, volumeFields, Thresholds),
                    () => InvariantString.Format(vserverLunQueryTemplate, Where, Lun.NetObjectPrefix, Pool.NetObjectPrefix, SwisEntities.Lun, latencyFields, Thresholds),
                    () => InvariantString.Format(vserverVolumeQueryTemplate, Where, Volume.NetObjectPrefix, Pool.NetObjectPrefix, SwisEntities.Volume, volumeFields, Thresholds)
                );
            }
        }

        private string Grouping
        {
            get
            {
                return InvariantString.Format(
                    this.GetValueForCurrentNetObject(() => poolLunGrouping, () => poolVolumeGrouping, () => arrayLunGrouping, () => arrayVolumeGrouping, () => vserverLunGrouping, () => vserverVolumeGrouping),
                    FilterPattern);
            }
        }

        private string Thresholds
        {
            get
            {
                if (CurrentZoom.Equals(DataTimeRangeEnum.DataLastPoll.GetDescription()))
                {
                    return IsBlock
                        ? lunThresholdFields
                        : volumeThresholdFields;
                }

                return thresholdFieldsEmpty;
            }
        }

        private string Where
        {
            get
            {
                if (Pool != null)
                {
                    if (CurrentZoom.Equals(DataTimeRangeEnum.DataLastPoll.GetDescription()))
                    {
                        return IsBlock
                            ? InvariantString.Format(poolLunWhereLastPoll, Pool.PoolEntity.PoolID)
                            : InvariantString.Format(poolVolumeWhereLastPoll, Pool.PoolEntity.PoolID);
                    }

                    return IsBlock
                        ? InvariantString.Format(poolLunWhereLastHours, Pool.PoolEntity.PoolID, GetWhereLastHours())
                        : InvariantString.Format(poolVolumeWhereLastHours, Pool.PoolEntity.PoolID, GetWhereLastHours());
                }

                if (Array != null)
                {
                    if (CurrentZoom.Equals(DataTimeRangeEnum.DataLastPoll.GetDescription()))
                    {
                        return IsBlock
                            ? InvariantString.Format(arrayLunWhereLastPoll, Array.Array.StorageArrayId)
                            : InvariantString.Format(arrayVolumeWhereLastPoll, Array.Array.StorageArrayId);
                    }

                    return IsBlock
                        ? InvariantString.Format(arrayLunWhereLastHours, Array.Array.StorageArrayId, GetWhereLastHours())
                        : InvariantString.Format(arrayVolumeWhereLastHours, Array.Array.StorageArrayId, GetWhereLastHours());
                }

                // otherwise Vserver
                if (CurrentZoom.Equals(DataTimeRangeEnum.DataLastPoll.GetDescription()))
                {
                    return IsBlock
                        ? InvariantString.Format(vserverLunWhereLastPoll, Vserver.VServerEntity.VServerID)
                        : InvariantString.Format(vserverVolumeWhereLastPoll, Vserver.VServerEntity.VServerID);
                }

                return IsBlock
                    ? InvariantString.Format(vserverLunWhereLastHours, Vserver.VServerEntity.VServerID, GetWhereLastHours())
                    : InvariantString.Format(vserverVolumeWhereLastHours, Vserver.VServerEntity.VServerID, GetWhereLastHours());

            }
        }

        protected string FilterPattern
        {
            get { return "[FILTER_PATTERN]"; }
        }

        protected string SWQL
        {
            get { return CustomTable.SWQL.Replace(Environment.NewLine, " ").Trim(); }
        }

        protected string SearchSWQL
        {
            get { return CustomTable.SearchSWQL.Replace(Environment.NewLine, " ").Trim(); }
        }

        protected string CurrentZoom
        {
            get
            {
                return this.Resource.Properties["currentzoom"];
            }
        }

        protected bool IsBlock
        {
            get
            {
                return InvariantString.Equals("true", this.Resource.Properties["asblock"]);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            this.Page.Title += DisplayTitle;
            SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
            Wrapper.HeaderButtons.Controls.Add(SearchControl);

            base.OnLoad(e);
            CustomTable.UniqueClientID = ScriptFriendlyResourceId;
            CustomTable.SWQL = Query + Grouping;
            CustomTable.SearchSWQL = SearchQuery + Grouping;
        }
    }
}
