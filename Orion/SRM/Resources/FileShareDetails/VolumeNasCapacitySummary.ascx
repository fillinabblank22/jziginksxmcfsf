﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VolumeNasCapacitySummary.ascx.cs" Inherits="Orion_SRM_Resources_FileShareDetails_VolumeNasCapacitySummary" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.UnifiedResult" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<orion:Include ID="Include1" runat="server" File="OrionCore.js" />
<orion:Include ID="Include2" runat="server" Module="SRM" File="Charts/Charts.SRM.BlockCapacitySummary.js" />
<orion:Include ID="Include3" runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include ID="Include4" runat="server" Module="SRM" File="BlockCapacitySummary.css" />
<orion:Include ID="Include6" runat="server" Module="SRM" File="SRM.css" />

<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <div class="srm-blockcapacitysummary-wrapper">
            <div id="srm-blockcapacitysummary-main-div-<%=Resource.ID%>" class="srm-blockcapacitysummary-main">
                <table id="srm-blockcapacitysummary-table-<%=Resource.ID%>">
                    <tr>
                        <td class="srm-blockcapacitysummary-td-big">
                            <div><span><%= SrmWebContent.FileSharesDetails_VolumeCapacity %>:</span><asp:Label runat="server" CssClass="SRM_BoldText" ID="TotalCapacityLabel"></asp:Label></div>
                            <div><span><%= SrmWebContent.FileSharesDetails_QuotaCapacity %>:</span><asp:Label runat="server" CssClass="SRM_BoldText" ID="QuotaCapacityLabel"></asp:Label></div>
                            <div><span><%= SrmWebContent.FileShare_CapacitySummary_Used %>:</span><asp:Label runat="server" CssClass="SRM_BoldText" ID="FileSystemUsedLabel"></asp:Label></div>
                            <div><span><%= SrmWebContent.NasCapacitySummary_ProjectedRunOut %>:</span>
                                <span id="CapacityRunOutSpan_<%=Resource.ID%>" class="SRM_BoldText"></span>
                            </div>
                        </td>
                        <td class="srm-FileShareCapacitysummary-td-small">
                            <span id="srm-blockcapacitysummary-legend-<%=Resource.ID%>"></span>
                        </td>
                    </tr>
                </table>
                <div class="srm-blockcapacitysummary-chart-area srm-filesharefix">
                    <asp:Panel ID="chart" runat="server" CssClass="srm-blockcapacitysummary-chart srm-filesharefix"></asp:Panel>
                </div>
            </div>
            <div id="srm-blockcapacitysummary-details-<%=Resource.ID%>" class="srm-blockcapacitysummary-detail-main"></div>
        </div>
        <script type="text/javascript">
            var summaryChart_<%=Resource.ID%>;

            $(function () {
                $('#CapacityRunOutSpan_<%=Resource.ID%>').html(SW.SRM.Formatters.FormatDaysDiff(<%= CapacityProjectedRunout %>, <%= WarningThresholdDays %>, <%= CriticalThresholdDays %>));

                summaryChart_<%=Resource.ID%> = new SW.SRM.Charts.BlockCapacitySummary();
                summaryChart_<%=Resource.ID%>.init(<%=Data%>);
            });
        </script>
    </Content>
</orion:ResourceWrapper>
