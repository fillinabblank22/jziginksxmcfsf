﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.Resources;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Common.ServiceProvider;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_SRM_Resources_FileShareDetails_VolumeNasCapacitySummary : CapacitySummaryBase
{
    #region Properties

    /// <summary>
    /// Override resource property.
    /// </summary>
    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.VolumeNasCapacitySummary_Title;
        }
    }

    /// <summary>
    /// Override help link fragment property.
    /// </summary>
    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceLUNCapacitySummary";
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new[] { typeof(IVolumeProvider) };
        }
    }

    public FileShares FileShare
    {
        get;
        set;
    }

    public override string EditControlLocation { get { return null; } }

    /// <summary>
    /// Gets data for current resource.
    /// </summary>
    protected string Data
    {
        get
        {
            return
                Serializer.Serialize(
                    new
                    {
                        ResourceId = Resource.ID,
                        NetObjectId = Request["NetObject"],
                        PageSize = PageSize,
                        Expanded = SrmSessionManager.GetBlockCapacitySummaryExpanderState(Resource.ID),
                        RenderTo = chart.ClientID,
                        ResourceWidth = Resource.Width,
                        DataSourceMethod = "GetFileShareCapacitySummaryData",
                        LegendMapping = new Dictionary<String, String>() 
                                { 
                                    { "columnDarkBlue", SrmWebContent.FileShare_CapacitySummary_Used },
                                    { "columnGrey", SrmWebContent.FileShare_CapacitySummary_Remaining }
                                }
                    });
        }
    }

    #endregion


    #region Methods

    /// <summary>
    /// Page load event handler.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        var fileShareProvider = GetInterfaceInstance<IFileSharesProvider>();
        this.FileShare = fileShareProvider.FileShares;

        if (this.FileShare.Volume.VolumeEntity.CapacityRunout.HasValue)
        {
            CapacityProjectedRunout = WebHelper.GetDayDifference(this.FileShare.Volume.VolumeEntity.CapacityRunout.Value);
        }

        if (!IsPostBack)
        {
            TotalCapacityLabel.Text = WebHelper.FormatCapacityTotal(this.FileShare.FileSharesEntity.QuotaOrVolumeTotalCapacity);

            QuotaCapacityLabel.Text = WebHelper.FormatCapacityTotal(this.FileShare.FileSharesEntity.Quota);

            FileSystemUsedLabel.Text = WebHelper.FormatCapacityTotal(this.FileShare.FileSharesEntity.ShareOrVolumeUsedCapacity);

            var dal = ServiceLocator.GetInstance<IThresholdDAL>();
            WebHelper.MarkThresholds(FileSystemUsedLabel, (float)FileShare.FileSharesEntity.UsedCapacityPercent, dal.GetThresholdDynamicOrGlobal(ThresholdDataType.FileShareFileSystemUsedCapacityPercent, FileShare.FileSharesEntity.FileShareID));
        }
    }

    #endregion
}
