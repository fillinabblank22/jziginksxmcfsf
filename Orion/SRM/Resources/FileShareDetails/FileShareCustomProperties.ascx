<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FileShareCustomProperties.ascx.cs" Inherits="Orion_SRM_Resources_FileShareDetails_FileShareCustomProperties" %>
<%@ Register TagPrefix="orion" TagName="CustomPropertyList" Src="~/Orion/SRM/Controls/CustomPropertyList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:CustomPropertyList runat="server" ID="customPropertyList" EntityName="Orion.SRM.FileShareCustomProperties"/>
    </Content>
</orion:resourceWrapper>