using System;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SRM_Resources_FileShareDetails_FileShareCustomProperties : SrmBaseResourceControl
{
    private const string SrmEntityName = SwisEntities.FileShare;
    private const string SrmEntityIDFieldName = "FileShareID";
    private NetObject Netobject { set; get; }
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.customPropertyList.Resource = this.Resource;

        var fileSharesProvider = GetInterfaceInstance<IFileSharesProvider>();
        var fileShares = fileSharesProvider.FileShares;

        if (fileShares != null)
        {
            this.customPropertyList.NetObjectId = fileShares.FileSharesEntity.FileShareID;
            this.Netobject = fileShares;
        }

        customPropertyList.CustomPropertyLoader = (netObjectId, displayProperties) =>
        {
            return CustomPropertyHelper.GetCustomPropertiesForObject(netObjectId, displayProperties, SrmEntityName, SrmEntityIDFieldName, Netobject);
        };

        customPropertyList.EditCustomPropertiesLinkGenerator = (netObjectId, redirectUrl) =>
        {
            if (Request.UrlReferrer != null)
            {
                redirectUrl = UrlHelper.ToSafeUrlParameter(Request.UrlReferrer.PathAndQuery);
            }
            return String.Format("/Orion/SRM/Admin/EditProperties.aspx?NetObject={0}&ReturnTo={1}", Netobject.NetObjectID, redirectUrl);
        };
    }
    protected override string DefaultTitle => Resources.SrmWebContent.CustomProperties_FileShares;

    public override string EditControlLocation => "/Orion/SRM/Controls/EditResourceControls/EditCustomPropertyList.ascx";

    public override string HelpLinkFragment => "SRMPHResourceCustomPropertiesFileShares";

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces => new [] { typeof(IFileSharesProvider) };

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl;
}
