﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FileShareDetails.ascx.cs" Inherits="Orion_SRM_Resources_FileShareDetails_FileShareDetails" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web" %>
<%@ Import Namespace="SolarWinds.SRM.Web.NetObjects" %>

<orion:Include runat="server" Module="SRM" File="SRM.css" />
<orion:Include runat="server" Module="SRM" File="SRM.Formatters.js" />

<orion:resourceWrapper ID="Wrapper" runat="server">
    <Content>
        <table cellspacing="0" class="srm_details_table">
            <tr>
                <td class="PropertyHeader">
                    <div class="SRM_FirstRowDetailsTable">
                        <asp:Literal runat="server" ID="StatusHeaderLabel" />
                    </div>
                </td>
                <td class="Property SRM_StatusImage">
                    <img class="StatusIcon" src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.FileShare%>&status=<%=(int)FileShareStatus%>&size=large" />
                </td>
                <td class="Property">
                    <asp:Label runat="server" ID="StatusLabel" />
                </td>
            </tr>
            <tr runat="server" id ="NameRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.FileSharesDetails_Name%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="NameLabel" />
                </td>
            </tr>
            <tr runat="server" id ="PathRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.FileSharesDetails_Path%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="PathLabel" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= SrmWebContent.FileSharesDetails_Volume%>:
                </td>
                <td></td>
                <td class="SRM_Icon_Cell">
                    <img src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.Volume%>&status=<%=(int)VolumeDetailsStatus%>" />
                    <asp:HyperLink runat="server" ID="VolumeHyperLink" />
                </td>
            </tr>
            <tr runat="server" ID="ArrayRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.FileSharesDetails_Array%>:
                </td>
                <td></td>
                <td class="SRM_Icon_Cell">
                    <img src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.StorageArrays%>&status=<%=(int)StorageArrayStatus%>" />
                    <asp:HyperLink runat="server" ID="ArrayHyperlink" />
                </td>
            </tr>
            <tr runat="server" ID="VServerRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.FileSharesDetails_vServer%>:
                </td>
                <td></td>
                <td>
                    <asp:Repeater id="VServersList" runat="server">
                        <ItemTemplate>
                            <span class="SRM_Icon_Cell">
                                <img src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.VServers%>&status=<%# (int)Eval("Status") %>" />
                                <asp:HyperLink 
                                    Text='<%# Eval("DisplayName") %>' 
                                    NavigateUrl='<%# String.Format(CultureInfo.CurrentCulture, "/Orion/View.aspx?NetObject={0}:{1}", VServer.NetObjectPrefix, Eval("VServerId")) %>' runat="server" ID="VServerHyperLink" />
                                <br />
                            </span>
                        </ItemTemplate>
                    </asp:Repeater>                    
                </td>
            </tr>
            <tr runat="server" ID="ClusterRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.FileSharesDetails_Cluster %>:
                </td>
                <td></td>
                <td class="SRM_Icon_Cell">
                    <img src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.Clusters%>&status=<%=(int)StorageArrayStatus%>" />
                    <asp:HyperLink runat="server" ID="ClusterHyperLink" />
                </td>
            </tr>                    
            <tr runat="server" id ="ServerAddressesRow">
                <td class="PropertyHeader">
                    <%= ServerAddressesTitle%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="ServerAddressesLabel" />
                </td>
            </tr>
            <tr runat="server" id ="VendorStatusRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.FileSharesDetails_VendorStatus%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="VendorStatusLabel" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= SrmWebContent.FileSharesDetails_Type%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="TypeLabel" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= SrmWebContent.FileSharesDetails_VolumeCapacity%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="VolumeCapacityLabel" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= SrmWebContent.FileSharesDetails_QuotaCapacity%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="QuotaCapacityLabel" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= SrmWebContent.FileSharesDetails_UsedCapacity%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="UsedCapacityLabel" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= SrmWebContent.FileSharesDetails_FreeCapacity%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="FreeCapacityLabel" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= SrmWebContent.FileSharesDetails_LastSync%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="LastSync" /></td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= SrmWebContent.FileSharesDetails_ProjectedRunOut%>:
                </td>
                <td></td>
                <td class="Property">
                    <span id="RunSpan_<%=Resource.ID%>" class="SRM_BoldText">
                        <script type="text/javascript">$('#RunSpan_<%=Resource.ID%>').html(SW.SRM.Formatters.FormatDaysDiff(<%= CapacityRunout %>, <%= WarningThresholdDays %>, <%= CriticalThresholdDays %>))</script>
                    </span>
                </td>
            </tr>
        </table>
    </Content>
</orion:resourceWrapper>
