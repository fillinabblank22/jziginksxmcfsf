﻿using Resources;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;
using System;
using System.Globalization;
using System.Linq;
using System.Text;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SRM_Resources_FileShareDetails_FileShareDetails : FileSharesBaseResourceControl, IResourceIsInternal
{
    protected override string DefaultTitle
    {
        get { return SrmWebContent.FileSharesDetails_NFSShareTitle;}
    }

    public bool IsInternal
    {
        get
        {
            return true;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "SRMPHResourceShareDetails"; }
    }

    protected VolumeStatus VolumeDetailsStatus { get; set; }
    protected StorageArrayStatus StorageArrayStatus { get; set; }
    protected FileShareStatus FileShareStatus { get; set; }
    protected int CapacityRunout { get; set; }
    protected string ServerAddressesTitle { get; set; }

    protected int WarningThresholdDays
    {
        get { return (int) SettingsDAL.GetSetting(SRMConstants.WarningThresholdDays).SettingValue; }
    }

    protected int CriticalThresholdDays
    {
        get { return (int) SettingsDAL.GetSetting(SRMConstants.CriticalThresholdDays).SettingValue; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            Wrapper.HeaderButtons.Controls.Add(LoadControl("~/Orion/SRM/Controls/ManageNetObjectButton.ascx"));

        FileSharesType resourceType = (FileSharesType) (Convert.ToInt16(Resource.Properties["Type"]));

        var fs = FileShares;
        if (fs != null)
        {
            var fileShares = fs.FileSharesEntity;
            if (resourceType != fileShares.Type)
            {
                this.Visible = false;
                return;
            }
            var volume = ServiceLocator.GetInstance<IVolumeDAL>().GetVolumeDetails(fileShares.VolumeID);
            var array = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayDetails(volume.StorageArrayID);
            
            FileShareStatus = fileShares.Status;
            StorageArrayStatus = array.Status;
            VolumeDetailsStatus = volume.Status;

            if (fileShares.Type == FileSharesType.NFS)
            {
                //NFS
                StatusHeaderLabel.Text = SrmWebContent.FileSharesDetails_NFSShareStatus;
                TypeLabel.Text = SrmWebContent.FileSharesDetails_Type_NFS;
                ServerAddressesTitle = SrmWebContent.FileSharesDetails_ServerAddresses_NFS;
            }
            else
            {
                //CIFS
                StatusHeaderLabel.Text = SrmWebContent.FileSharesDetails_CIFSShareStatus;
                TypeLabel.Text = SrmWebContent.FileSharesDetails_Type_CIFS;
                ServerAddressesTitle = SrmWebContent.FileSharesDetails_ServerAddresses_CIFS;
            }

            var fileServerIdentification =
                ServiceLocator.GetInstance<IFileServerIdentificationDAL>()
                    .GetFileServerIdentificationByFileServerId(fileShares.FileServerID);

            var ipAddressStringBuilder = new StringBuilder();
            if (fileServerIdentification.Any())
            {
                foreach (var fileServer in fileServerIdentification)
                {
                    ipAddressStringBuilder.Append(InvariantString.Format("{0}, ", fileServer.IpAddress));
                }
                ipAddressStringBuilder.Remove(ipAddressStringBuilder.Length - 2, 2);
            }
            WebHelper.SetValueOrHide(ServerAddressesLabel, ServerAddressesRow, ipAddressStringBuilder.ToString());
            StatusLabel.Text = InvariantString.Format("{0} {1}", fileShares.DisplayName,
                SrmStatusLabelHelper.GetStatusLabel(volume.Status));
            WebHelper.SetupHyperLink(VolumeHyperLink, volume.DisplayName, SolarWinds.SRM.Web.NetObjects.Volume.NetObjectPrefix,
                volume.VolumeID);

            if (array.IsCluster)
            {
                ArrayRow.Visible = false;
                // Assign data source to the repeater and rebind
                var data = ServiceLocator.GetInstance<IVServerDAL>().GetVServersByVolumeId(volume.VolumeID);
                if (data.Any())
                {
                    VServersList.DataSource = data;
                    VServersList.DataBind();
                }
                else
                {
                    VServerRow.Visible = false;
                }
                WebHelper.SetupHyperLink(ClusterHyperLink, array.DisplayName, StorageArray.NetObjectPrefix,
                    array.StorageArrayId.Value);
            }
            else
            {
                // Regular array
                VServerRow.Visible = false;
                ClusterRow.Visible = false;
                WebHelper.SetupHyperLink(ArrayHyperlink, array.DisplayName, StorageArray.NetObjectPrefix,
                    array.StorageArrayId.Value);
            }

            WebHelper.SetValueOrHide(NameLabel, NameRow, fileShares.DisplayName);
            WebHelper.SetValueOrHide(PathLabel, PathRow, fileShares.Path);
            WebHelper.SetValueOrHide(VendorStatusLabel, VendorStatusRow, volume.VendorStatusDescription);
            VolumeCapacityLabel.Text = WebHelper.FormatCapacityTotal(volume.CapacityTotal);
            QuotaCapacityLabel.Text = WebHelper.FormatCapacityTotal(fileShares.Quota);
            UsedCapacityLabel.Text = FormatValueWithPercent(fileShares.ShareOrVolumeUsedCapacity, fileShares.QuotaOrVolumeTotalCapacity);

            var dal = ServiceLocator.GetInstance<IThresholdDAL>();
            WebHelper.MarkThresholds(UsedCapacityLabel, (float)fileShares.UsedCapacityPercent, dal.GetThresholdDynamicOrGlobal(ThresholdDataType.FileShareFileSystemUsedCapacityPercent, fileShares.FileShareID));

            FreeCapacityLabel.Text = FormatValueWithPercent(fileShares.ShareOrVolumeFreeCapacity, fileShares.QuotaOrVolumeTotalCapacity);
            CapacityRunout = WebHelper.GetDayDifference(volume.CapacityRunout);

            LastSync.Text = fileShares.LastSync.HasValue ? Utils.FormatDateTimeForDisplayingOnWebsite(fileShares.LastSync.Value.ToLocalTime(), true) : String.Empty;
        }
    }

    private string FormatValueWithPercent(long? value, long total)
    {
        // hide percents if value is unknown
        return value == null
            ? WebHelper.FormatCapacityTotal(value) // could return Unknown right away, but lets delegate this to formatting method to be consistent
            : string.Format(CultureInfo.CurrentCulture, "{0} ({1}%)", WebHelper.FormatCapacityTotal(value), GetPercent(value.Value, total));
    }

    private static string GetPercent(long value, long total)
    {
        if (total == 0)
        {
            return "0";
        }
        double result = value*100.0/total;
        if (result > 0 && result < 1)
        {
            return "< 1";
        }
        if (result > 99 && result < 100)
        {
            return "> 99";
        }
        return ((int) Math.Round(result, MidpointRounding.AwayFromZero)).ToString();
    }
}
