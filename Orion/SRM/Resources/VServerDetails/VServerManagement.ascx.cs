﻿using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;
using System;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_SRM_Resources_VServerDetails_VServerManagement : SrmBaseResourceControl
{
    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.VServerManagement_DefaultPageTitle;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceVserverManagement";
        }
    }

    protected VServer VServer { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.VServer = NetObjectFactory.Create(Request["NetObject"], true) as VServer;
    }
}