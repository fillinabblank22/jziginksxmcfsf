﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VServerManagement.ascx.cs" Inherits="Orion_SRM_Resources_VServerDetails_VServerManagement" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="PollRediscoverDialog" Src="~/Orion/Controls/PollRediscoverDialog.ascx" %>
<%@ Register TagPrefix="orion" TagName="UnmanageDialog" Src="~/Orion/SRM/Controls/UnmanageDialog.ascx" %>

<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.css" />
<orion:Include ID="Include1" runat="server" Module="SRM" File="ArrayManagement.css" />

<orion:ResourceWrapper ID="Wrapper" runat="server">
    <Content>
        <div class="srm-array-management-wrapper">
            <div class="srm-array-management-header srm-uppercase">
                <%= SrmWebContent.VServerManagement_VServer %>
            </div>
            <a href="#" onclick="return redirectToEditPage(['<%= VServer.NetObjectID %>']);" class="srm-array-management-button-edit-icon">
                <%= SrmWebContent.VServerManagement_Edit_VServer %>
            </a>
            

            <a href="/Orion/SRM/Admin/ManualE2EMapping/" class="srm-array-management-button-edit-icon">
                <%= SrmWebContent.VServerManagement_Map_Server_Volumes %>
            </a>
        </div>
        
        <script type="text/javascript">
            function redirectToEditPage(ids) {
                window.location = '/Orion/SRM/Admin/EditProperties.aspx?NetObject=' + ids + '&ReturnTo=' +  '<%= Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(Request.Url.AbsoluteUri)) %>';
            }
        </script>

    </Content>
</orion:ResourceWrapper>

