<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VServerCustomProperties.ascx.cs" Inherits="Orion_SRM_Resources_VServerDetails_VServerCustomProperties" %>
<%@ Register TagPrefix="orion" TagName="CustomPropertyList" Src="~/Orion/SRM/Controls/CustomPropertyList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:CustomPropertyList runat="server" ID="customPropertyList" EntityName="Orion.SRM.VServersCustomProperties"/>
    </Content>
</orion:resourceWrapper>