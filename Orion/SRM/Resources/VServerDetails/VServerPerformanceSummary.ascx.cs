﻿using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web.UI;
using System;

namespace Orion.SRM.Resources.VServerDetails
{
    [ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
    [ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
    public partial class Orion_SRM_Resources_VServerDetails_VServerPerformanceSummary : SrmBaseResourceControl
    {
        protected override string DefaultTitle
        {
            get { return SrmWebContent.VServer_Performance_Summary_Title; }
        }

        public override string HelpLinkFragment
        {
            get { return "SRMPHResourcePerformanceSummary"; }
        }

        public override string EditURL
        {
            get
            {
                return ExtendCustomEditUrl("/Orion/SRM/EditTopXXChartResource.aspx?EditXX=False");
            }
        }

        public override ResourceLoadingMode ResourceLoadingMode
        {
            get { return ResourceLoadingMode.Ajax; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            PerformanceSummary.DataBindMethod = "GetVServerPerformanceSummaryData"; 
            PerformanceSummary.ResourceID = Resource.ID;
            PerformanceSummary.ResourceWidth = Resource.Width;

            // panels of chart
            PerformanceSummary.IOPSEnable = true;
            PerformanceSummary.ThroughputEnable = true;
            PerformanceSummary.IOSizeEnable = true;
            PerformanceSummary.CacheHitRatioEnable = true;

            PerformanceSummary.CustomIOPSResourceTitle = SrmWebContent.PerformanceDrillDown_ArrayIOPSResourceTitle;
            PerformanceSummary.CustomIOPSChartTitle = SrmWebContent.PerformanceDrillDown_ArrayIOPSChartTitle;
            PerformanceSummary.CustomThroughputResourceTitle = SrmWebContent.PerformanceDrillDown_ArrayThroughputResourceTitle;
            PerformanceSummary.CustomThroughputChartTitle = SrmWebContent.PerformanceDrillDown_ArrayThroughputChartTitle;
            PerformanceSummary.CustomIOSizeResourceTitle = SrmWebContent.PerformanceDrillDown_ArrayIOSizeResourceTitle;
            PerformanceSummary.CustomIOSizeChartTitle = SrmWebContent.PerformanceDrillDown_ArrayIOSizeChartTitle;
            PerformanceSummary.CustomCacheHitRatioResourceTitle = SrmWebContent.PerformanceDrillDown_ArrayCacheHitRatioResourceTitle;
            PerformanceSummary.CustomCacheHitRatioChartTitle = SrmWebContent.PerformanceDrillDown_ArrayCacheHitRatioChartTitle;

            PerformanceSummary.SampleSizeInMinutes = SampleSizeInMinutes;
            PerformanceSummary.DaysOfDataToLoad = DaysOfDataToLoad;
            PerformanceSummary.InitialZoom = InitialZoom;

            PerformanceSummary.ChartTitleProperty = Resource.Properties["ChartTitle"];
            PerformanceSummary.ChartSubTitleProperty = Resource.Properties["ChartSubTitle"];
        }
    }
}