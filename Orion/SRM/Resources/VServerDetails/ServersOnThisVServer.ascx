﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ServersOnThisVServer.ascx.cs" Inherits="Orion_SRM_Resources_VServerDetails_ServersOnThisVServer" %>
<%@ Register src="../../Controls/AjaxTree.ascx" TagName="AjaxTree" TagPrefix="srm" %>

<srm:AjaxTree ID="AjaxTree" runat="server" ServicePath="/Orion/SRM/Services/ServersOnThisVServerTree.asmx" />
