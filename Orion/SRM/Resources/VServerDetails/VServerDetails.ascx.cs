﻿using System;
using System.Globalization;
using System.Web.UI.WebControls;
using Resources;

using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Web.Plugins;


[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SRM_Resources_VServerDetails_VServerDetails : VServerBaseResourceControl
{

    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.VServerDetails_DefaultPageTitle;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceVserverDetails";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.RenderControl;
        }
    }

    protected VServerStatus VServerStatus { get; set; }
    protected ArrayEntity ParentArray { get; set; }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            Wrapper.HeaderButtons.Controls.Add(LoadControl("~/Orion/SRM/Controls/ManageNetObjectButton.ascx"));
        if (VServer != null)
        {
            VServerEntity vs = VServer.VServerEntity;
            VServerStatus = vs.Status;
            StatusLabel.Text = String.Format(CultureInfo.CurrentCulture, "{0} {1}", vs.DisplayName, SrmStatusLabelHelper.GetStatusLabel(vs.Status));
            NameLabel.Text = vs.DisplayName;
            WebHelper.SetValueOrHide(VendorStatusLabel, VendorStatusRow, vs.VendorStatusDescription);
            IpAddressLabel.Text = vs.IPAddress;

            DescriptionLabel.Text = vs.Description;
            DescriptionRow.Visible = !String.IsNullOrEmpty(DescriptionLabel.Text);

            ParentArray = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayDetails(vs.StorageArrayID);

            ClusterHyperLink.Text = ParentArray.DisplayName;

            ClusterHyperLink.NavigateUrl = InvariantString.Format("/Orion/View.aspx?NetObject={0}:{1}", SolarWinds.SRM.Web.NetObjects.StorageArray.NetObjectPrefix, ParentArray.StorageArrayId);

            if (!vs.RootVolumeID.HasValue)
            {
                RootVolumeLabel.Text = SrmWebContent.EntityDetail_DataNotDefined;
                RootVolumeAggregateLabel.Text = SrmWebContent.EntityDetail_DataNotDefined;
            }
            else
            {
                var rootVolume = ServiceLocator.GetInstance<IVolumeDAL>().GetVolumeDetails(vs.RootVolumeID.Value);
                RootVolumeLabel.Text = rootVolume != null ? rootVolume.DisplayName : SrmWebContent.EntityDetail_DataNotDefined;

                var rootVolumeAggregatePool = ServiceLocator.GetInstance<IPoolDAL>().GetSinglePoolByVolumeId(vs.RootVolumeID.Value);
                RootVolumeAggregateLabel.Text = rootVolumeAggregatePool != null ? rootVolumeAggregatePool.DisplayName : SrmWebContent.EntityDetail_DataNotDefined;
            }

            LastSync.Text = vs.LastSync.HasValue ? Utils.FormatDateTimeForDisplayingOnWebsite(vs.LastSync.Value.ToLocalTime(), true) : String.Empty;

            this.PerformanceAnalyzerImage.ImageUrl = PerformanceStackHelper.PerformanceAnalyzerImage;
            SetupPerformanceHyperLink(this.PerformanceHyperLink, vs.VServerID);
        }
        else
        {
            StatusLabel.Text = SrmWebContent.ArrayDetails_ArrayStatusFail;
        }
    }

    private void SetupPerformanceHyperLink(HyperLink link, int vserverID)
    {
        link.Text = SrmWebContent.PerformanceAnalizer_TextLink;
        link.NavigateUrl = PerformanceStackHelper.LinkToVServer(vserverID);
    }


}