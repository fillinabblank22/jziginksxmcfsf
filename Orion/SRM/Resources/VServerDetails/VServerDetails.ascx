﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VServerDetails.ascx.cs" Inherits="Orion_SRM_Resources_VServerDetails_VServerDetails" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.css" />

<orion:ResourceWrapper ID="Wrapper" runat="server">
    <Content>
        <table cellspacing="0" class="srm_details_table">
            <tr>
                <td class="PropertyHeader">
                    <div class="SRM_FirstRowDetailsTable">
                        <%= Resources.SrmWebContent.LinkToPerfStack_Title %>:
                    </div>
                </td>
                <td class="Property SRM_StatusImage">
                    <asp:Image ID="PerformanceAnalyzerImage" CssClass="StatusIcon" runat="server" />
                </td>
                <td class="Property">
                    <asp:HyperLink runat="server" ID="PerformanceHyperLink" Text="Performance Analyzer"/>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <div>
                        <%= SrmWebContent.VserverDetails_VserverStatus%>:
                    </div>
                </td>
                <td class="Property SRM_StatusImage">
                    <img class="StatusIcon SRM_ArrayStatusImageTd" src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.VServers%>&status=<%=(int)VServerStatus%>&size=large" />
                </td>
                <td class="Property">
                    <asp:Label runat="server" ID="StatusLabel" />
                </td>
            </tr>
            <tr runat="server" id="IpAddressRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.ArrayDetails_IP_Address%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="IpAddressLabel" /></td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= SrmWebContent.VServerDetails_Name%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="NameLabel" /></td>
            </tr>
             <tr runat="server" id ="VendorStatusRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.VserverDetails_VendorStatusCode%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="VendorStatusLabel" />
                </td>
            </tr>
            <tr runat="server" ID="ClusterTableRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.StoragePoolDetails_Cluster %>:
                </td>
                <td></td>
                <td class="SRM_Icon_Cell">
                    <img src="/Orion/StatusIcon.ashx?entity=<%=ParentArray.IsCluster?SwisEntities.Clusters:SwisEntities.StorageArrays%>&status=<%=(int)ParentArray.Status%>" />
                    <asp:HyperLink runat="server" ID="ClusterHyperLink" />
                </td>
            </tr>
            <tr runat="server" id="DescriptionRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.ArrayDetails_Description%>:
                </td>
                <td></td>
                <td class="Property statusAndText">
                    <asp:Label runat="server" ID="DescriptionLabel" /></td>
            </tr>
            <tr runat="server" id="RootVolumeRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.VServerDetails_RootVolume%>:
                </td>
                <td></td>
                <td class="Property statusAndText">
                    <asp:Label runat="server" ID="RootVolumeLabel" /></td>
            </tr>
            <tr runat="server" id="RootVolumeAggregateRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.VServerDetails_RootVolumeAggregate%>:
                </td>
                <td></td>
                <td class="Property statusAndText">
                    <asp:Label runat="server" ID="RootVolumeAggregateLabel" /></td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= SrmWebContent.VServerDetails_LastSync%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="LastSync" /></td>
            </tr>
        </table>
        <script type="text/javascript">
            $('.srm_details_table tr:even').addClass('SRM_AltRow1').removeClass('SRM_AltRow2');
            $('.srm_details_table tr:odd').addClass('SRM_AltRow2').removeClass('SRM_AltRow1');
        </script>
    </Content>
</orion:ResourceWrapper>
