﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VServerPerformanceSummary.ascx.cs" Inherits="Orion.SRM.Resources.VServerDetails.Orion_SRM_Resources_VServerDetails_VServerPerformanceSummary" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="srm" TagName="PerformanceSummary" Src="~/Orion/SRM/Controls/PerformanceSummary.ascx" %>

<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <srm:PerformanceSummary runat="server" id="PerformanceSummary" />
    </Content>
</orion:ResourceWrapper>
