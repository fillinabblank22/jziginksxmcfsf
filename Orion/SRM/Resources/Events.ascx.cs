﻿using System;
using SolarWinds.SRM.Web.UI;

public partial class Orion_SRM_Resources_Events : AlertsAndEventsBaseResourceControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        EventSummary.Resource = base.Resource;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Visible = base.AppropriateNetObjectType();
    }

    protected override string DefaultTitle
    {
        get { return base.Resource.Title; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return EventSummary.HelpLinkFragment;
        }
    }

    public override string EditControlLocation
    {
        get
        {
            return EventSummary.EditControlLocation;
        }
    }
}