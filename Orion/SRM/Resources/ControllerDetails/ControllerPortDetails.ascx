﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ControllerPortDetails.ascx.cs" Inherits="Orion_SRM_Resources_ControllerDetails_ControllerPortDetails" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Resources" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="ResourceSearchControl" Src="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<orion:Include ID="Include1" runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.css" />

<orion:ResourceWrapper runat="server" ID="StorageControllerPortDetailsResourceWrapper">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTableStorageControllerPortDetails" />
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= CustomTableStorageControllerPortDetails.UniqueClientID %>,
                        initialPage: 0,
                        rowsPerPage: <%= this.GetPageSize() %>,
                        allowSort: true,
                        columnSettings: {
                            "Port_Name": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.StorageControllerPortDetails_Name%>'),
                                cellCssClassProvider: function (value, row, cellInfo) { return "SRM_Icon_Cell srm_word_wrap"; }
                            },
                            "Port_Type": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.StorageControllerPortDetails_Type%>'),
                                cellCssClassProvider: function (value, row, cellInfo) { return "SRM_Icon_Cell srm_word_wrap"; }
                            },
                            "Link_Type": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.StorageControllerPortDetails_Link%>'),
                                cellCssClassProvider: function (value, row, cellInfo) { return "SRM_Icon_Cell srm_word_wrap"; }
                            },
                            "Port_Number": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.StorageControllerPortDetails_Number%>'),
                                cellCssClassProvider: function (value, row, cellInfo) { return "SRM_Icon_Cell srm_word_wrap"; }
                            },
                            "Port_Identifier": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.StorageControllerPortDetails_Identifier%>'),
                                cellCssClassProvider: function (value, row, cellInfo) { return "SRM_Icon_Cell srm_word_wrap"; }
                            },
                            "Port_Speed": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.StorageControllerPortDetails_Speed%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatSpeedValueUnit(value);
                                },
                                cellCssClassProvider: function (value, row, cellInfo) { return "SRM_Icon_Cell srm_word_wrap"; }
                            }
                        },
                        searchTextBoxId: '<%= StorageControllerPortDetailsSearchControl.SearchBoxClientID %>',
                        searchButtonId: '<%= StorageControllerPortDetailsSearchControl.SearchButtonClientID %>'
                    });
                SW.Core.Resources.CustomQuery.refresh('<%=CustomTableStorageControllerPortDetails.UniqueClientID %>');
            });
        </script>
    </Content>
    <HeaderButtons>
        <orion:ResourceSearchControl runat="server" ID="StorageControllerPortDetailsSearchControl"/>
    </HeaderButtons>
</orion:ResourceWrapper>