﻿using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web.UI;
using System;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_SRM_Resources_ControllerDetails_ControllerPerformanceSummary : SrmBaseResourceControl
{
    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.StorageController_Performance_Summary_Title;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceStorageControllerPerformanceSummary";
        }
    }

    public override string EditURL
    {
        get
        {
            return ExtendCustomEditUrl("/Orion/SRM/EditTopXXChartResource.aspx?EditXX=False");
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.Ajax;
        }
    }

    /// <summary>
    /// Page load event handler.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        StorageControllerPerformanceSummary.ResourceID = Resource.ID;
        StorageControllerPerformanceSummary.ResourceWidth = Resource.Width;

        // panels of chart
        StorageControllerPerformanceSummary.IOPSEnable = true;
        StorageControllerPerformanceSummary.LatencyEnable = true;
        StorageControllerPerformanceSummary.ThroughputEnable = true;
        StorageControllerPerformanceSummary.IOSizeEnable = true;
        StorageControllerPerformanceSummary.UtilizationEnable = true;

        StorageControllerPerformanceSummary.CustomIOPSResourceTitle = SrmWebContent.PerformanceDrillDown_LunIOPSResourceTitle;
        StorageControllerPerformanceSummary.CustomIOPSChartTitle = SrmWebContent.PerformanceDrillDown_LunIOPSChartTitle;
        StorageControllerPerformanceSummary.CustomLatencyResourceTitle = SrmWebContent.PerformanceDrillDown_LunLatencyResourceTitle;
        StorageControllerPerformanceSummary.CustomLatencyChartTitle = SrmWebContent.PerformanceDrillDown_LunLatencyChartTitle;
        StorageControllerPerformanceSummary.CustomThroughputResourceTitle = SrmWebContent.PerformanceDrillDown_LunThroughputResourceTitle;
        StorageControllerPerformanceSummary.CustomThroughputChartTitle = SrmWebContent.PerformanceDrillDown_LunThroughputChartTitle;
        StorageControllerPerformanceSummary.CustomIOSizeResourceTitle = SrmWebContent.PerformanceDrillDown_LunIOSizeResourceTitle;
        StorageControllerPerformanceSummary.CustomIOSizeChartTitle = SrmWebContent.PerformanceDrillDown_LunIOSizeChartTitle;
        StorageControllerPerformanceSummary.CustomIOPSRatioResourceTitle = SrmWebContent.PerformanceDrillDown_LunIOPSRatioResourceTitle;
        StorageControllerPerformanceSummary.CustomIOPSRatioChartTitle = SrmWebContent.PerformanceDrillDown_LunIOPSRatioChartTitle;
        StorageControllerPerformanceSummary.CustomUtilizationResourceTitle = SrmWebContent.PerformanceDrillDown_ControllerUtilizationResourceTitle;
        StorageControllerPerformanceSummary.CustomUtilizationChartTitle = SrmWebContent.PerformanceDrillDown_ControllerUtilizationChartTitle;

        StorageControllerPerformanceSummary.SampleSizeInMinutes = SampleSizeInMinutes;
        StorageControllerPerformanceSummary.DaysOfDataToLoad = DaysOfDataToLoad;
        StorageControllerPerformanceSummary.InitialZoom = InitialZoom;

        StorageControllerPerformanceSummary.ChartTitleProperty = Resource.Properties["ChartTitle"];
        StorageControllerPerformanceSummary.ChartSubTitleProperty = Resource.Properties["ChartSubTitle"];
    }
}