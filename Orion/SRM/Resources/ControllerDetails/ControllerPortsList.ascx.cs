﻿using System;
using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_SRM_Resources_ControllerDetails_ControllerPortsList : StorageControllerBaseResourceControl
{

    protected override string DefaultTitle => SrmWebContent.StorageControllerPortsList_Title;

    public override string HelpLinkFragment => "SRMPHResourceStorageControllerPortsListResource";

    public override string EditControlLocation => "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx";

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        this.StorageControllerPortsListResourceWrapper.Visible = true;

        string query = this.GetStorageControllerPortsListQuery();

        this.CustomTableStorageControllerPortsList.UniqueClientID = this.ScriptFriendlyResourceId;
        this.CustomTableStorageControllerPortsList.SWQL = query;
        this.CustomTableStorageControllerPortsList.SearchSWQL = $"{query} AND ([Name] LIKE '%${{SEARCH_STRING}}%')";
    }

    private string GetStorageControllerPortsListQuery()
    {
        string storageControllerPortsListQuery = InvariantString.Format(@"SELECT 
                                                Name AS [Name],
                                                '/Orion/StatusIcon.ashx?entity={1}&status=' + ToString(ISNULL(Status, 0)) +'&size=small' AS [_IconFor_Name],
                                                cp0.IOPSDistribution AS [IOPSDistribution],
                                                cp0.BytesPSDistribution AS [BytesPSDistribution],
                                                cp0.IOPSTotal AS [IOPSTotal],
                                                cp0.BytesPSTotal AS [BytesPSTotal],
                                                cp0.IOPSDistributionThreshold.IsLevel1State AS [_IOPSDistributionWariningFlag],
	                                            cp0.IOPSDistributionThreshold.IsLevel2State AS [_IOPSDistributionCriticalFlag],
	                                            cp0.BytesPSDistributionThreshold.IsLevel1State AS [_ThroughputDistributionWariningFlag],
	                                            cp0.BytesPSDistributionThreshold.IsLevel2State AS [_ThroughputDistributionCriticalFlag],
                                                cp0.IOPSTotalThreshold.IsLevel1State AS [_IOPSTotalWariningFlag],
	                                            cp0.IOPSTotalThreshold.IsLevel2State AS [_IOPSTotalCriticalFlag],
	                                            cp0.BytesPSTotalThreshold.IsLevel1State AS [_ThroughputTotalWariningFlag],
	                                            cp0.BytesPSTotalThreshold.IsLevel2State AS [_ThroughputTotalCriticalFlag]
                                             FROM Orion.SRM.StorageControllerPorts (nolock=true) cp0
                                             WHERE StorageControllerID = {0}",
                                             this.StorageController.StorageControllerEntity.StorageControllerID.Value,
                                             SwisEntities.StorageControllerPorts);

        return storageControllerPortsListQuery;
    }
}

