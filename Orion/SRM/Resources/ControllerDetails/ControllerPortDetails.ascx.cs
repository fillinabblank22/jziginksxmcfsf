﻿using System;
using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_SRM_Resources_ControllerDetails_ControllerPortDetails : StorageControllerBaseResourceControl
{
    protected override string DefaultTitle => SrmWebContent.StorageControllerPortDetails_Title;

    public override string HelpLinkFragment => "SRMPHResourceStorageControllerPortDetailsResource";

    public override string EditControlLocation => "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx";

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        this.StorageControllerPortDetailsResourceWrapper.Visible = true;

        string query = this.GetStorageControllerPortsDetailsQuery();

        this.CustomTableStorageControllerPortDetails.UniqueClientID = this.ScriptFriendlyResourceId;
        this.CustomTableStorageControllerPortDetails.SWQL = query;
        this.CustomTableStorageControllerPortDetails.SearchSWQL = $"{query} AND ([Name] LIKE '%${{SEARCH_STRING}}%')";
    }

    private string GetStorageControllerPortsDetailsQuery()
    {
        string storageControllerPortsDetailsQuery = InvariantString.Format(@"SELECT 
                                                Name AS [Port_Name], 
                                                PortType AS [Port_Type],
                                                LinkType AS [Link_Type],
                                                PortNumber AS [Port_Number],
                                                PortIdentifier AS [Port_Identifier],
                                                PortSpeed AS [Port_Speed]
                                             FROM Orion.SRM.StorageControllerPorts (nolock=true)
                                             WHERE StorageControllerID = {0}", this.StorageController.StorageControllerEntity.StorageControllerID.Value);

        return storageControllerPortsDetailsQuery;
    }
}