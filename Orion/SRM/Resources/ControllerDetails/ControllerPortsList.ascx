﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ControllerPortsList.ascx.cs" Inherits="Orion_SRM_Resources_ControllerDetails_ControllerPortsList" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Resources" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="ResourceSearchControl" Src="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<orion:Include ID="Include1" runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.css" />

<orion:ResourceWrapper runat="server" ID="StorageControllerPortsListResourceWrapper">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTableStorageControllerPortsList" />
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= CustomTableStorageControllerPortsList.UniqueClientID %>,
                        initialPage: 0,
                        rowsPerPage: <%= this.GetPageSize() %>,
                        allowSort: true,
                        columnSettings: {
                            "Name": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.StorageControllerPortsList_Name%>'),
                                cellCssClassProvider: function (value, row, cellInfo) { return "SRM_Icon_Cell srm_word_wrap"; }
                            },
                            "IOPSDistribution": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.StorageControllerPortsList_IOPSDistribution%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatPercentageColumnValue(value);
                                },
                                cellCssClassProvider: function(cellValue, rowArray, cellInfo) {
                                    return SW.SRM.Formatters.ApplyThresholds(rowArray[6], rowArray[7]);
                                }
                            },
                            "BytesPSDistribution": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.StorageControllerPortsList_BytesPSDistribution%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatPercentageColumnValue(value);
                                },
                                cellCssClassProvider: function(cellValue, rowArray, cellInfo) {
                                    return SW.SRM.Formatters.ApplyThresholds(rowArray[8], rowArray[9]);
                                }
                            },
                            "IOPSTotal": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.StorageControllerPortsList_IOPSTotal%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.ParseIntOrUnknown(value);
                                },
                                cellCssClassProvider: function(cellValue, rowArray, cellInfo) {
                                    return SW.SRM.Formatters.ApplyThresholds(rowArray[10], rowArray[11]);
                                }
                            },
                            "BytesPSTotal": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.StorageControllerPortsList_BytesPSTotal%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function(cellValue, rowArray, cellInfo) {
                                    return SW.SRM.Formatters.ApplyThresholds(rowArray[12], rowArray[13]);
                                }
                            },
                        },
                        searchTextBoxId: '<%= StorageControllerPortsListSearchControl.SearchBoxClientID %>',
                        searchButtonId: '<%= StorageControllerPortsListSearchControl.SearchButtonClientID %>'
                    });
                SW.Core.Resources.CustomQuery.refresh('<%=CustomTableStorageControllerPortsList.UniqueClientID %>');
            });
        </script>
    </Content>
    <HeaderButtons>
        <orion:ResourceSearchControl runat="server" ID="StorageControllerPortsListSearchControl"/>
    </HeaderButtons>
</orion:ResourceWrapper>