﻿using System;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.Plugins;
using SolarWinds.SRM.Web.UI;
using System;
using System.Collections.Generic;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Common;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]

public partial class Orion_SRM_Resources_DetailsView_ControllerDetails : StorageControllerBaseResourceControl
{
    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceStorageControllerDetails";
        }
    }

    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.StorageControllerDetailsResourceTitle;
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            Wrapper.HeaderButtons.Controls.Add(LoadControl("~/Orion/SRM/Controls/ManageNetObjectButton.ascx"));
        }
            
        if (StorageController != null)
        {
            SetWebControls(StorageController);
        }
    }

    private void SetWebControls(StorageController storageController)
    {
        var status = storageController.StorageControllerEntity.Status;

        StorageControllerStatus.Text = InvariantString.Format("{0} {1}", storageController.Name,
            SrmStatusLabelHelper.GetStatusLabel(Enum.IsDefined(typeof(StorageControllerStatus), status) ? (StorageControllerStatus)status : SolarWinds.SRM.Common.Enums.StorageControllerStatus.Unknown));
        statusImg.Src = InvariantString.Format("/Orion/StatusIcon.ashx?entity={0}&status={1}&size=large",
            SwisEntities.StorageArrays, (int)status);
        Type.Text = storageController.StorageControllerEntity.Type;
        Manufacturer.Text = storageController.StorageControllerEntity.Manufacturer;
        Model.Text = storageController.StorageControllerEntity.Model;
        Firmware.Text = storageController.StorageControllerEntity.Firmware;
        TotalMemory.Text = WebHelper.FormatCapacityTotal(storageController.StorageControllerEntity.TotalMemory);
        TotalCacheSize.Text = WebHelper.FormatCapacityTotal(storageController.StorageControllerEntity.TotalCacheSize);

        WebHelper.SetValueOrHide(this.FCPortsCount, this.FCPortsCountRow, storageController.StorageControllerEntity.FcPortCount);
        WebHelper.SetValueOrHide(this.ISCSIPortsCount, this.ISCSIPortsCountRow, storageController.StorageControllerEntity.ISCSIPortCount);
        WebHelper.SetValueOrHide(this.EthernetPortsCount, this.EthernetPortsCountRow, storageController.StorageControllerEntity.EthernetPortCount);
        WebHelper.SetValueOrHide(this.CPUCount, this.CPUCountRow, storageController.StorageControllerEntity.CPUCount);
        WebHelper.SetValueOrHide(this.CPUFrequency, this.CPUFrequencyRow,
            storageController.StorageControllerEntity.CPUFrequency);

        this.PerformanceAnalyzerImage.ImageUrl = PerformanceStackHelper.PerformanceAnalyzerImage;
        this.SetupPerformanceHyperLink(this.PerformanceHyperLink, (int)storageController.StorageControllerEntity.StorageControllerID);

    }

    private void SetupPerformanceHyperLink(HyperLink link, int ControlerID)
    {
        link.Text = SrmWebContent.PerformanceAnalizer_TextLink;
        link.NavigateUrl = PerformanceStackHelper.LinkToStorageController(ControlerID);
    }
}
