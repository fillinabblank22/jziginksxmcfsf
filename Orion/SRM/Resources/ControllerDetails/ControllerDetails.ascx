﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ControllerDetails.ascx.cs" Inherits="Orion_SRM_Resources_DetailsView_ControllerDetails" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.css" />

<orion:ResourceWrapper ID="Wrapper" runat="server">
<Content>
    <table cellspacing="0" class="srm_details_table">
        <tr>
            <td class="PropertyHeader">
                <%= SrmWebContent.StorageControllerDetails_StorageControllerStatus%>:
            </td>
            <td class="Property SRM_StatusImage">
                <img class="StatusIcon" id="statusImg" runat="server" alt="status" />
            </td>
            <td class="Property statusAndText">
                <asp:Literal runat="server" ID="StorageControllerStatus" />
            </td>
        </tr>
        <tr>
            <td class="PropertyHeader">
                <%= Resources.SrmWebContent.LinkToPerfStack_Title %>:
            </td>
            <td class="Property SRM_StatusImage">
                <asp:Image ID="PerformanceAnalyzerImage" CssClass="StatusIcon" runat="server" />
            </td>
            <td class="Property">
                <asp:HyperLink runat="server" ID="PerformanceHyperLink" Text="Performance Analyzer"/>
            </td>
        </tr>
        <tr>
            <td class="PropertyHeader">
                 <%= SrmWebContent.StorageControllerDetails_Type%>:
            </td>
            <td></td>
            <td class="Property statusAndText">
                <asp:Literal runat="server" ID="Type" />
            </td>
        </tr>
        <tr>
            <td class="PropertyHeader">
                 <%= SrmWebContent.StorageControllerDetails_Manufacturer%>:
            </td>
            <td></td>
            <td class="Property statusAndText">
                <asp:Literal runat="server" ID="Manufacturer" />
            </td>
        </tr>
        <tr>
            <td class="PropertyHeader">
                 <%= SrmWebContent.StorageControllerDetails_Model%>:
            </td>
            <td></td>
            <td class="Property statusAndText">
                <asp:Literal runat="server" ID="Model" />
            </td>
        </tr>
        <tr>
            <td class="PropertyHeader">
                 <%= SrmWebContent.StorageControllerDetails_Firmware%>:
            </td>
            <td></td>
            <td class="Property statusAndText">
                <asp:Literal runat="server" ID="Firmware" />
            </td>
        </tr>
        <tr>
            <td class="PropertyHeader">
                 <%= SrmWebContent.StorageControllerDetails_TotalMemory%>:
            </td>
            <td></td>
            <td class="Property statusAndText">
                <asp:Literal runat="server" ID="TotalMemory" />
            </td>
        </tr>
        <tr runat="server" id="CPUCountRow">
            <td class="PropertyHeader">
                 <%= SrmWebContent.StorageControllerDetails_CPUCount%>:
            </td>
            <td></td>
            <td class="Property statusAndText">
                <asp:Literal runat="server" ID="CPUCount" />
            </td>
        </tr>
        <tr runat="server" id="CPUFrequencyRow">
            <td class="PropertyHeader">
                 <%= SrmWebContent.StorageControllerDetails_CPUFrequency%>:
            </td>
            <td></td>
            <td class="Property statusAndText">
                <asp:Literal runat="server" ID="CPUFrequency" />
            </td>
        </tr>
        <tr>
            <td class="PropertyHeader">
                 <%= SrmWebContent.StorageControllerDetails_TotalCacheSize%>:
            </td>
            <td></td>
            <td class="Property statusAndText">
                <asp:Literal runat="server" ID="TotalCacheSize" />
            </td>
        </tr>
        <tr runat="server" id="FCPortsCountRow">
            <td class="PropertyHeader">
                 <%= SrmWebContent.StorageControllerDetails_FCPortsCount%>:
            </td>
            <td></td>
            <td class="Property statusAndText">
                <asp:Literal runat="server" ID="FCPortsCount" />
            </td>
        </tr>
        <tr runat="server" id="ISCSIPortsCountRow">
            <td class="PropertyHeader">
                 <%= SrmWebContent.StorageControllerDetails_ISCSIPortsCount%>:
            </td>
            <td></td>
            <td class="Property statusAndText">
                <asp:Literal runat="server" ID="ISCSIPortsCount" />
            </td>
        </tr>
        <tr runat="server" id="EthernetPortsCountRow">
            <td class="PropertyHeader">
                 <%= SrmWebContent.StorageControllerDetails_EthernetPortsCount%>:
            </td>
            <td></td>
            <td class="Property statusAndText">
                <asp:Literal runat="server" ID="EthernetPortsCount" />
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        $('.srm_details_table tr:even').addClass('SRM_AltRow1').removeClass('SRM_AltRow2');
        $('.srm_details_table tr:odd').addClass('SRM_AltRow2').removeClass('SRM_AltRow1');
    </script>
</Content>
</orion:ResourceWrapper>
