﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LunDetails.ascx.cs" Inherits="Orion_SRM_Resources_LunDetails_LunDetails" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web" %>
<%@ Import Namespace="SolarWinds.SRM.Web.NetObjects" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.css" />
<orion:Include ID="Include3" runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <table cellspacing="0" class="srm_details_table">
            <tr>
                <td class="PropertyHeader">
                    <div class="SRM_FirstRowDetailsTable">
                        <%= Resources.SrmWebContent.LinkToPerfStack_Title %>:
                    </div>
                </td>
                <td class="Property SRM_StatusImage">
                    <asp:Image ID="PerformanceAnalyzerImage" CssClass="StatusIcon" runat="server" />
                </td>
                <td class="Property">
                    <asp:HyperLink runat="server" ID="PerformanceHyperLink" Text="Performance Analyzer"/>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.LunDetails_Status_Tooltip %>">
                    <%= SrmWebContent.Lun_Status%>:</span>
                </td>
                <td class="Property SRM_StatusImage">
                    <img class="StatusIcon" src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.Lun%>&status=<%=(int)LunStatus%>&size=large" />
                </td>
                <td class="Property">
                    <asp:Label runat="server" ID="StatusLabel" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.LunDetails_Name_Tooltip %>">
                    <%= SrmWebContent.Lun_Name%>:</span>
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="NameLabel" />
                </td>
            </tr>
             <tr runat="server" id="UUIDRow">
                <td class="PropertyHeader">
                   <span Title="<%= Resources.SrmWebContent.LunDetails_UUID_Tooltip %>">
                    <%= SrmWebContent.Lun_UUID%>:</span>
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="UUIDLabel" />
                </td>
            </tr> 
            <tr class="SRM_HiddenField">
                <td class="PropertyHeader">
                    <%= SrmWebContent.Lun_DefaultController%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="DefaultControllerLabel" />
                </td>
            </tr>
            <tr class="SRM_HiddenField">
                <td class="PropertyHeader">
                    <%= SrmWebContent.Lun_CurrentController%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="CurrentControllerLabel" />
                </td>
            </tr>
            <tr runat="server" id="CacheEnabledRow">
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.LunDetails_CacheEnabled_Tooltip %>">
                    <%= SrmWebContent.Lun_CacheEnabled%>:</span>
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="CacheEnabledLabel" />
                </td>
            </tr>
            <tr runat="server" id="ParenVolumeRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.Lun_ParentVolume%>:
                </td>
                <td></td>
                <td class="SRM_Icon_Cell">
                    <img src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.Volume%>&status=<%=(int)ParentVolume.Status%>" />
                    <asp:HyperLink runat="server" ID="ParentVolumeHyperLink" />
                </td>
            </tr>
            <tr id="StoragePoolRow" runat="server">
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.LunDetails_StoragePool_Tooltip %>">
                    <%= SrmWebContent.Lun_Storage_Pool%>:
                    </span>
                </td>
                <td></td>
                <td>
                    <asp:Repeater id="StoragePoolList" runat="server">
                        <ItemTemplate>
                            <span class="SRM_Icon_Cell">
                                <img src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.Pools%>&status=<%# (int)Eval("Status") %>" />
                                <asp:HyperLink 
                                    Text='<%# Eval("Caption") %>' 
                                    NavigateUrl='<%# String.Format(CultureInfo.CurrentCulture, "/Orion/View.aspx?NetObject={0}:{1}", Pool.NetObjectPrefix, Eval("PoolId")) %>' runat="server" ID="PoolHyperLink" />
                                <br />
                            </span>
                        </ItemTemplate>
                    </asp:Repeater>                    
                </td>
            </tr>
            <tr runat="server" ID="ArrayRow">
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.LunDetails_Array_Tooltip %>">
                    <%= SrmWebContent.Lun_Array%>:</span>
                </td>
                <td></td>
                <td class="SRM_Icon_Cell">
                    <img src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.StorageArrays%>&status=<%=(int)Array.Status%>" />
                    <asp:HyperLink runat="server" ID="ArrayHyperlink" />
                </td>
            </tr>
            <tr runat="server" ID="ClusterRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.Lun_Cluster %>:
                </td>
                <td></td>
                <td class="SRM_Icon_Cell">
                    <img src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.Clusters%>&status=<%=(int)Array.Status%>" />
                    <asp:HyperLink runat="server" ID="ClusterHyperLink" />
                </td>
            </tr>
            <tr runat="server" ID="VServerRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.Lun_vServer%>:
                </td>
                <td></td>
                <td>
                    <asp:Repeater id="VServersList" runat="server">
                        <ItemTemplate>
                            <span class="SRM_Icon_Cell">
                                <img src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.VServers%>&status=<%# (int)Eval("Status") %>" />
                                <asp:HyperLink 
                                    Text='<%# Eval("DisplayName") %>' 
                                    NavigateUrl='<%# String.Format(CultureInfo.CurrentCulture, "/Orion/View.aspx?NetObject={0}:{1}", VServer.NetObjectPrefix, Eval("VServerId")) %>' runat="server" ID="VServerHyperLink" />
                                <br />
                            </span>
                        </ItemTemplate>
                    </asp:Repeater>                    
                </td>
            </tr>
             <tr runat="server" id="VendorStatusCodeRow">
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.LunDetails_VendorStatusCode_Tooltip %>">
                    <%= SrmWebContent.LunDetails_VendorStatusCode%>:</span>
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="VendorStatusCodeLabel" /></td>
            </tr>
            <tr runat="server" id="RAIDTypeRow">
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.LunDetails_RaidConfigurationType_Tooltip %>">
                    <%= SrmWebContent.Lun_RAIDType%>:</span>
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="RAIDTypeLabel" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.LunDetails_Type_Tooltip %>">
                    <%= SrmWebContent.Lun_Type%>:</span>
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="TypeLabel" />
                </td>
            </tr>
            <tr runat="server" id="AssociatedEndpointsRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.Lun_Associated_DatastoreVolume%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="AssociatedEndpointsLabel" />
                </td>
            </tr>
            <tr runat="server" id="HostMasksRow">
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.LunDetails_HostMasks_Tooltip %>">
                    <%= SrmWebContent.Lun_Associated_Server_Storage_Host%>:</span>
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="HostMasksLabel" />
                </td>
            </tr>
            <tr runat="server" id="ProtocolRow">
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.LunDetails_Protocol_Tooltip %>">
                    <%= SrmWebContent.Lun_Protocol%>:</span>
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="ProtocolLabel" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.LunDetails_ReadOnly_Tooltip %>">
                    <%= SrmWebContent.Lun_ReadOnly%>:</span>
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="ReadOnlyLabel" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.LunDetails_Mapped_Tooltip %>">
                    <%= SrmWebContent.Lun_Mapped%>:</span>
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="MappedLabel" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.LunDetails_TotalSize_Tooltip %>">
                    <%= SrmWebContent.LUN_Total_User_Capacity%>:</span>
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="TotalUserCapacityLabel" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.LunDetails_FileSystemUsedCapacity_Tooltip %>">
                    <%= SrmWebContent.LUN_File_System_Used_Capacity%>:</span>
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="FileSystemUsedCapacityLabel" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                   <span Title="<%= Resources.SrmWebContent.LunDetails_ConsumedCapacity_Tooltip %>">
                    <%= SrmWebContent.LUN_Consumed_Capacity%>:</span>
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="ConsumedCapacityLabel" />
                </td>
            </tr>
            <!--
            <tr runat="server" ID="rowTotalReduction">
                <td class="PropertyHeader">
                    <%= SrmWebContent.UsableCapacityTotalReduction %>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="TotalReductionLabel" />
                </td>
            </tr>
            -->
            <tr>
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.LunDetails_LastDatabaseUpdate_Tooltip %>">
                    <%= SrmWebContent.Lun_LastSync%>:</span>
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="LastSync" /></td>
            </tr>
            <tr runat="server" ID="ProjectedRunOutRow">
                <td class="PropertyHeader">
                   <span Title="<%= Resources.SrmWebContent.LunDetails_ProjectedRunOut_Tooltip %>">
                    <%= SrmWebContent.LUN_Projected_RunOut%>:</span>
                </td>
                <td></td>
                <td>
                    <span id="RunSpan_<%=Resource.ID%>" class="SRM_BoldText">
                        <script type="text/javascript">$('#RunSpan_<%=Resource.ID%>').html(SW.SRM.Formatters.FormatDaysDiff(<%= CapacityRunout %>, <%= WarningThresholdDays %>, <%= CriticalThresholdDays %>))</script>
                    </span>
                </td>
            </tr>
        </table>
        <script type="text/javascript">
            $('.srm_details_table tr:even').addClass('SRM_AltRow1').removeClass('SRM_AltRow2');
            $('.srm_details_table tr:odd').addClass('SRM_AltRow2').removeClass('SRM_AltRow1');
        </script>
    </Content>
</orion:ResourceWrapper>
