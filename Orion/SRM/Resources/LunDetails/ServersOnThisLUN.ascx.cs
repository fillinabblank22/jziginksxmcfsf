﻿using System;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Web.Resources;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Web.UI;
using System.Globalization;
using SolarWinds.SRM.Web.Models;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_SRM_Resources_LUNDetails_ServersOnThisLUN : LunBaseResourceControl
{
    private static Log log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceServersUsingStorage";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }


    #region Resource Properties

    protected override string DefaultTitle
    {
        get { return Resources.SrmWebContent.Resource_Title_ServersOnThisLUN; }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/SRM/Controls/EditResourceControls/EditServersOnThisArray.ascx";
        }
    }

    public override string SubTitle
    {
        get
        {
            if (!String.IsNullOrEmpty(base.SubTitle))
            {
                return base.SubTitle;
            }

            return String.Empty;
        }
    }

    #endregion

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (this.Lun != null)
        {
            LunEntity lunEntity = this.Lun.LUN;

            AjaxTree.ResourceId = this.Resource.ID.ToString();
            AjaxTree.Filters = String.Format(CultureInfo.InvariantCulture, "{{ {0} }}", Resource.Properties[ResourcePropertiesKeys.Filter] ?? string.Empty);
            AjaxTree.Grouping = new string[0];
            AjaxTree.InitialValue = lunEntity.LUNID.ToString();
            AjaxTree.RememberExpandedGroups = this.Resource.Properties[ResourcePropertiesKeys.RememberCollapseState] ?? "True";
            AjaxTree.ExpandRootLevel = this.Resource.Properties[ResourcePropertiesKeys.ExpandRootLevel] ?? "False";
            AjaxTree.ShowMoreSingularMessage = Resources.SrmWebContent.SummaryDetails_Storages_showMoreSingularMessage;
            AjaxTree.ShowMorePluralMessage = Resources.SrmWebContent.SummaryDetails_Storages_showMorePluralMessage;
        }
    }
}