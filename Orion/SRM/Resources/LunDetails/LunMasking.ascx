﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LunMasking.ascx.cs" Inherits="Orion.SRM.Resources.LunDetails.Orion_SRM_Resources_LunDetails_LunMasking" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Resources" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<orion:Include ID="Include1" runat="server" Module="SRM" File="SRM.Formatters.js" />

<orion:ResourceWrapper runat="server" ID="ResourceWrapper">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: '<%= CustomTable.UniqueClientID %>',
                        initialPage: 0,
                        rowsPerPage: '<%= this.GetPageSize() %>',
                        allowSort: true,
                        columnSettings: {
                            "HostGroup": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.Lun_LunMasking_HostGroup%>')
                            },
                            "HostAlias": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.Lun_LunMasking_HostAlias%>')
                            },
                            "InitiatorLunID": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.Lun_LunMasking_InitiatorLunID%>')
                            },
                            "InitiatorEndpoint": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.Lun_LunMasking_InitiatorEndpoint%>'),
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "srm_word_wrap";
                                }
                            },
                            "TargetEndpoint": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.Lun_LunMasking_TargetEndpoint%>'),
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "srm_word_wrap";
                                }
                            },
                            "UUID": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.Lun_LunMasking_UUID%>'),
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "srm_word_wrap";
                                }
                            }
                        },
                        searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                        searchButtonId: '<%= SearchControl.SearchButtonClientID %>'
                    });
                SW.Core.Resources.CustomQuery.refresh('<%=CustomTable.UniqueClientID %>');
            });
        </script>
    </Content>
</orion:ResourceWrapper>