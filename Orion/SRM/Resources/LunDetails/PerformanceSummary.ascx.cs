﻿using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web.NetObjects;
using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_SRM_Resources_LunDetails_PerformanceSummary : LunBaseResourceControl
{
    #region Fields

    private static readonly JavaScriptSerializer serializer = new JavaScriptSerializer {MaxJsonLength = 0x400000};

    #endregion

#region Properties

    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.Lun_Performance_Summary_Title;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourcePerformanceSummary";
        }
    }

    public override string EditURL
    {
        get
        {
            return ExtendCustomEditUrl("/Orion/SRM/EditTopXXChartResource.aspx?EditXX=False");
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.Ajax;
        }
    }

    #endregion
    
    #region Methods

    /// <summary>
    /// Page load event handler.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        LunPerformanceSummary.ResourceID = Resource.ID;
        LunPerformanceSummary.ResourceWidth = Resource.Width;

        // panels of chart
        LunPerformanceSummary.IOPSEnable = true;
        LunPerformanceSummary.LatencyEnable = true;
        LunPerformanceSummary.ThroughputEnable = true;
        LunPerformanceSummary.IOSizeEnable = true;
        LunPerformanceSummary.QueueLengthEnable = true;
        LunPerformanceSummary.IOPSRatioEnable = true;
        LunPerformanceSummary.DiskBusyEnable = false;
        LunPerformanceSummary.CacheHitRatioEnable = true;

        LunPerformanceSummary.CustomIOPSResourceTitle = SrmWebContent.PerformanceDrillDown_LunIOPSResourceTitle;
        LunPerformanceSummary.CustomIOPSChartTitle = SrmWebContent.PerformanceDrillDown_LunIOPSChartTitle;
        LunPerformanceSummary.CustomLatencyResourceTitle = SrmWebContent.PerformanceDrillDown_LunLatencyResourceTitle;
        LunPerformanceSummary.CustomLatencyChartTitle = SrmWebContent.PerformanceDrillDown_LunLatencyChartTitle;
        LunPerformanceSummary.CustomThroughputResourceTitle = SrmWebContent.PerformanceDrillDown_LunThroughputResourceTitle;
        LunPerformanceSummary.CustomThroughputChartTitle = SrmWebContent.PerformanceDrillDown_LunThroughputChartTitle;
        LunPerformanceSummary.CustomIOSizeResourceTitle = SrmWebContent.PerformanceDrillDown_LunIOSizeResourceTitle;
        LunPerformanceSummary.CustomIOSizeChartTitle = SrmWebContent.PerformanceDrillDown_LunIOSizeChartTitle;
        LunPerformanceSummary.CustomQueueLengthResourceTitle = SrmWebContent.PerformanceDrillDown_LunQueueLengthResourceTitle;
        LunPerformanceSummary.CustomQueueLengthChartTitle = SrmWebContent.PerformanceDrillDown_LunQueueLengthChartTitle;
        LunPerformanceSummary.CustomIOPSRatioResourceTitle = SrmWebContent.PerformanceDrillDown_LunIOPSRatioResourceTitle;
        LunPerformanceSummary.CustomIOPSRatioChartTitle = SrmWebContent.PerformanceDrillDown_LunIOPSRatioChartTitle;
        LunPerformanceSummary.CustomDiskBusyResourceTitle = SrmWebContent.PerformanceDrillDown_LunDiskBusyResourceTitle;
        LunPerformanceSummary.CustomDiskBusyChartTitle = SrmWebContent.PerformanceDrillDown_LunDiskBusyChartTitle;
        LunPerformanceSummary.CustomCacheHitRatioResourceTitle = SrmWebContent.PerformanceDrillDown_LunCacheHitRatioResourceTitle;
        LunPerformanceSummary.CustomCacheHitRatioChartTitle = SrmWebContent.PerformanceDrillDown_LunCacheHitRatioChartTitle;

        LunPerformanceSummary.SampleSizeInMinutes = SampleSizeInMinutes;
        LunPerformanceSummary.DaysOfDataToLoad = DaysOfDataToLoad;
        LunPerformanceSummary.InitialZoom = InitialZoom;

        LunPerformanceSummary.ChartTitleProperty = Resource.Properties["ChartTitle"];
        LunPerformanceSummary.ChartSubTitleProperty = Resource.Properties["ChartSubTitle"];
    }

    protected override bool IsResourceVisible()
    {
        return ServiceLocator.GetInstance<IResourceVisibilityService>()
            .IsResourceVisibleForArray(Lun.StorageArray, this.AppRelativeVirtualPath);
    }

    #endregion
}