﻿using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Web.UI;
using System;

namespace Orion.SRM.Resources.LunDetails
{
    [ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
    public partial class Orion_SRM_Resources_LunDetails_LunMasking : LunBaseResourceControl
    {
        private string query;

        /// <summary>
        /// OnLoad override method.
        /// </summary>
        protected override void OnLoad(EventArgs e)
        {
                base.OnLoad(e);

                SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
                ResourceWrapper.HeaderButtons.Controls.Add(SearchControl);

                query = InvariantString.Format(@"SELECT HostGroup, HostAlias, InitiatorLunID, InitiatorEndpoint, TargetEndpoint, UUID
                                                 FROM Orion.SRM.LunMasking (nolock=true)
                                                 WHERE LunID = {0}",
                                                 Lun.LUN.LUNID);

                CustomTable.UniqueClientID = ScriptFriendlyResourceId;
                CustomTable.SWQL = query;
                CustomTable.SearchSWQL = SearchSwql;
        }

        /// <summary>
        /// Gets or sets Search Control property.
        /// </summary>
        protected ResourceSearchControl SearchControl { get; set; }

        /// <summary>
        /// Gets additional query for search control.
        /// </summary>
        protected string SearchSwql
        {
            get { return string.Concat(query, " AND HostAlias LIKE '%${SEARCH_STRING}%'"); }
        }

        protected string SearchQuery
        {
            get { return CustomTable.SearchSWQL.Replace(Environment.NewLine, " ").Trim(); }
        }

        protected string Query
        {
            get { return CustomTable.SWQL.Replace(Environment.NewLine, " ").Trim(); }
        }

        protected override string DefaultTitle
        {
            get { return SrmWebContent.LunMasking_Resource_Title; }
        }

        public override string HelpLinkFragment
        {
            get { return "SRMPHResourceLunMaskingForThisLun"; }
        }

        public override string EditControlLocation
        {
            get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
        }
    }
}