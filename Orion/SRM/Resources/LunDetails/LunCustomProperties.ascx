<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LunCustomProperties.ascx.cs" Inherits="Orion_SRM_Resources_LunDetails_LunCustomProperties" %>
<%@ Register TagPrefix="orion" TagName="CustomPropertyList" Src="~/Orion/SRM/Controls/CustomPropertyList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:CustomPropertyList runat="server" ID="customPropertyList" EntityName="Orion.SRM.LUNCustomProperties"/>
    </Content>
</orion:resourceWrapper>