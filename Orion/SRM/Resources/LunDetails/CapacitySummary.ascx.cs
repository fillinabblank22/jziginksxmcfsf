﻿using System;
using System.Globalization;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;
using SolarWinds.Orion.Web.Charting.v2;
using System.Collections.Generic;
using SolarWinds.SRM.Web.Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_SRM_Resources_LunDetails_CapacitySummary : CapacitySummaryBase
{
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(ILunProvider) }; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.SrmWebContent.LunDetails_SummaryCapacity_Title; }
    }

    public override string HelpLinkFragment
    {
        get { return "SRMPHResourceLUNCapacitySummary"; }
    }

    public override string EditControlLocation
    {
        get { return null; }
    }


    public Lun Lun { get; set; }

    protected string Data
    {
        get
        {
            return Serializer.Serialize(new
            {
                ResourceId = this.Resource.ID,
                NetObjectId = this.Request["NetObject"],
                PageSize = this.PageSize,
                Expanded = SrmSessionManager.GetBlockCapacitySummaryExpanderState(Resource.ID),
                RenderTo = this.chart.ClientID,
                ResourceWidth = Resource.Width,
                DataSourceMethod = "GetLunData",
                LegendMapping = new Dictionary<String, String[]>()
                                { 
                                    { "columnLightBlue",new String[] { Resources.SrmWebContent.LunDetails_SummaryCapacity_FileSystemUsed, Resources.SrmWebContent.LUNCapacitySummary_FileSystemUsed_Tooltip } },
                                    { "columnDarkBlue",new String[] { Resources.SrmWebContent.LunDetails_SummaryCapacity_Consumed, Resources.SrmWebContent.LUNCapacitySummary_Consumed_Tooltip }},
                                    { "columnGrey",new String[] { Resources.SrmWebContent.LunDetails_SummaryCapacity_Remaining, Resources.SrmWebContent.LUNCapacitySummary_Remaining_Tooltip }},
                                }
            });
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var lunProvider = GetInterfaceInstance<ILunProvider>();
        this.Lun = lunProvider.Lun;

        if (this.Lun.LUN.CapacityRunout.HasValue)
        {
            CapacityProjectedRunout = WebHelper.GetDayDifference(this.Lun.LUN.CapacityRunout.Value);
        }

        if (!this.IsPostBack)
        {
            this.TotalSizeLabel.Text = WebHelper.FormatCapacityTotal(this.Lun.LUN.CapacityTotal);
            this.ConsumedLabel.Text = WebHelper.FormatCapacityTotal(this.Lun.LUN.CapacityAllocated);
            this.AllocatedLabel.Text = WebHelper.FormatCapacityTotal(this.Lun.LUN.CapacityFileSystem);
            //WebHelper.SetValueOrHide(TotalReductionLabel, divTotalReduction, WebHelper.FormatDataReduction(Lun.LUN.TotalReduction));
        }
        ProjectedRunOutRow.Visible = Lun.LUN.Thin;
    }
}