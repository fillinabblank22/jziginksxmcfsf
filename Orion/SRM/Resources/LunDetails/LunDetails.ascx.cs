﻿using Resources;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Web.Plugins;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using SettingsDAL = SolarWinds.Orion.Web.DAL.SettingsDAL;


[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SRM_Resources_LunDetails_LunDetails : SrmBaseResourceControl
{
    protected override string DefaultTitle
    {
        get { return SrmWebContent.LunDetails_Title; }
    }

    /// <summary>
    /// Gets or sets warning threshold days property.
    /// </summary>
    protected int WarningThresholdDays { get; set; }

    /// <summary>
    /// Gets or sets critical threshold days property.
    /// </summary>
    protected int CriticalThresholdDays { get; set; }

    /// <summary>
    /// Gets or sets capacity projected runout property.
    /// </summary>
    public int CapacityRunout { get; set; }

    public override string HelpLinkFragment
    {
        get { return "SRMPHResourceLUNDetails"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(ILunProvider) }; }
    }

    public LunStatus LunStatus { get; set; }
    public VolumeEntity ParentVolume { get; set; }
    public ArrayEntity Array { get; set; }
    public string AssociatedServerHeader { get; set; }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            ResourceWrapper.HeaderButtons.Controls.Add(LoadControl("~/Orion/SRM/Controls/ManageNetObjectButton.ascx"));
        WarningThresholdDays = (int)SettingsDAL.GetSetting(SRMConstants.WarningThresholdDays).SettingValue;
        CriticalThresholdDays = (int)SettingsDAL.GetSetting(SRMConstants.CriticalThresholdDays).SettingValue;

        var lp = GetInterfaceInstance<ILunProvider>();
        var volumes =
            ServiceLocator.GetInstance<ITopologyDAL>().GetOrionVolumeBySRMNetObject(new List<string>
            {
                lp.Lun.NetObjectID
            });
        LunMaskingHosts agregateHostsData =
              ServiceLocator.GetInstance<ILunMaskingDAL>().GetAggregateHostsDataByLunID(lp.Lun.LUN.LUNID);
        SetAssociatedEndpointsRowSate(volumes, lp);
        SetParentVolumeRowState(lp);
        SetHostMasksRowState(agregateHostsData);

        this.PerformanceAnalyzerImage.ImageUrl = PerformanceStackHelper.PerformanceAnalyzerImage;
        SetupPerformanceHyperLink(this.PerformanceHyperLink, lp.Lun.LUN.LUNID);

        DataTable poolsDataByLunId = ServiceLocator.GetInstance<IPoolDAL>().GetPoolsDataByLunId(lp.Lun.LUN.LUNID);
        if (poolsDataByLunId != null && poolsDataByLunId.Rows.Count > 0)
        {
            StoragePoolList.DataSource = poolsDataByLunId;
            StoragePoolList.DataBind();
            StoragePoolRow.Visible = true;
        }
        else
            StoragePoolRow.Visible = false;

        this.Array = lp.Lun.StorageArray.Array;
        if (Array.IsCluster)
        {
            ArrayRow.Visible = false;
            // Assign data source to the repeater and rebind
            var data = ServiceLocator.GetInstance<IVServerDAL>().GetVServersByLunId(lp.Lun.LUN.LUNID);
            if (data.Any())
            {
                VServersList.DataSource = data;
                VServersList.DataBind();
            }
            else
            {
                VServerRow.Visible = false;
            }
            WebHelper.SetupHyperLink(ClusterHyperLink, Array.DisplayName, StorageArray.NetObjectPrefix,
                    Array.StorageArrayId.Value);
        }
        else
        {
            // Regular array
            VServerRow.Visible = false;
            ClusterRow.Visible = false;

            WebHelper.SetupHyperLink(ArrayHyperlink, Array.DisplayName, StorageArray.NetObjectPrefix, Array.StorageArrayId.Value);
        }

        LunStatus = lp.Lun.LUN.Status;
        this.StatusLabel.Text = InvariantString.Format("{0} {1}", lp.Lun.Name,
                                                       SrmStatusLabelHelper.GetStatusLabel(LunStatus));
        this.NameLabel.Text = lp.Lun.Name;
        WebHelper.SetValueOrHide(UUIDLabel, UUIDRow, lp.Lun.LUN.UUID);
        WebHelper.SetValueOrHide(ProtocolLabel, ProtocolRow, lp.Lun.LUN.Protocol);
        this.MappedLabel.Text = WebHelper.FormatNullableValue(lp.Lun.LUN.Mapped);
        this.ReadOnlyLabel.Text = lp.Lun.LUN.ReadOnly.ToString(CultureInfo.CurrentUICulture);
        this.DefaultControllerLabel.Text = lp.Lun.LUN.DefaultControllerID.ToString(CultureInfo.CurrentUICulture);
        this.CurrentControllerLabel.Text = lp.Lun.LUN.CurrentControllerID.ToString(CultureInfo.CurrentUICulture);
        this.SetCacheEnabledRowState(lp);
        WebHelper.SetValueOrHide(RAIDTypeLabel, RAIDTypeRow, lp.Lun.LUN.RaidType);
        WebHelper.SetValueOrHide(VendorStatusCodeLabel, VendorStatusCodeRow, lp.Lun.LUN.VendorStatusDescription);
        this.TypeLabel.Text = lp.Lun.LUN.Thin ? SrmWebContent.Lun_Type_Thin : SrmWebContent.Lun_Type_Thick;
        
        this.FileSystemUsedCapacityLabel.Text = WebHelper.FormatCapacityTotal(lp.Lun.LUN.CapacityFileSystem);
        this.ConsumedCapacityLabel.Text = WebHelper.FormatCapacityTotal(lp.Lun.LUN.CapacityAllocated);
        this.TotalUserCapacityLabel.Text = WebHelper.FormatCapacityTotal(lp.Lun.LUN.CapacityTotal);
        CapacityRunout = WebHelper.GetDayDifference(lp.Lun.LUN.CapacityRunout);

        //WebHelper.SetValueOrHide(TotalReductionLabel, rowTotalReduction, WebHelper.FormatDataReduction(lp.Lun.LUN.TotalReduction));

        LastSync.Text = lp.Lun.LUN.LastSync.HasValue ? Utils.FormatDateTimeForDisplayingOnWebsite(lp.Lun.LUN.LastSync.Value.ToLocalTime(), true) : String.Empty;

        ProjectedRunOutRow.Visible = lp.Lun.LUN.Thin;
    }

    private void SetParentVolumeRowState(ILunProvider lp)
    {
        if (lp.Lun.LUN.VolumeId.HasValue)
        {
            this.ParentVolume =
                ServiceLocator.GetInstance<IVolumeDAL>().GetVolumeDetails(lp.Lun.LUN.VolumeId.Value);
            WebHelper.SetupHyperLink(ParentVolumeHyperLink, ParentVolume.DisplayName, SolarWinds.SRM.Web.NetObjects.Volume.NetObjectPrefix, ParentVolume.VolumeID);

        }
        else
        {
            this.ParenVolumeRow.Visible = false;
        }
    }

    private void SetCacheEnabledRowState(ILunProvider lp)
    {
        if (lp.Lun.LUN.CacheEnabled == null)
        {
            CacheEnabledRow.Visible = false;
        }
        else
        {
            this.CacheEnabledLabel.Text = string.Format(CultureInfo.CurrentUICulture, lp.Lun.LUN.CacheEnabled.ToString());
        }
    }

    private void SetAssociatedEndpointsRowSate(IEnumerable<OrionVolume> volumes, ILunProvider lp)
    {
        if (!String.IsNullOrEmpty(volumes.First().Caption))
            this.AssociatedEndpointsLabel.Text = volumes.First().Caption;

        this.AssociatedEndpointsRow.Visible = !String.IsNullOrEmpty(this.AssociatedEndpointsLabel.Text);
    }

    private void SetHostMasksRowState(LunMaskingHosts agregateHostsData)
    {
        if (!String.IsNullOrEmpty(agregateHostsData.HostAlias))
        {
            this.HostMasksLabel.Text = agregateHostsData.HostAlias;
        }
        this.HostMasksRow.Visible = !String.IsNullOrEmpty(this.HostMasksLabel.Text);
    }

    private void SetupPerformanceHyperLink(HyperLink link, int lunID)
    {
        link.Text = SrmWebContent.PerformanceAnalizer_TextLink;
        link.NavigateUrl = PerformanceStackHelper.LinkToLUN(lunID);
    }

}
