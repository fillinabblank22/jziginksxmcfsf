using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SRM_Resources_LunDetails_LunCustomProperties : SrmBaseResourceControl
{
    private const string SrmEntityName = SwisEntities.Lun;
    private const string SrmEntityIDFieldName = "LUNID";
    private NetObject Netobject { set; get; }
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.customPropertyList.Resource = this.Resource;

        var lunProvider = GetInterfaceInstance<ILunProvider>();
        var lun = lunProvider.Lun;

        if (lun != null)
        {
            this.customPropertyList.NetObjectId = lun.LUN.LUNID;
            this.Netobject = lun;
        }

        customPropertyList.CustomPropertyLoader = (netObjectId, displayProperties) =>
        {
            return CustomPropertyHelper.GetCustomPropertiesForObject(netObjectId, displayProperties, SrmEntityName, SrmEntityIDFieldName, Netobject);
        };

        customPropertyList.EditCustomPropertiesLinkGenerator = (netObjectId, redirectUrl) =>
        {
            if (Request.UrlReferrer != null)
            {
                redirectUrl = UrlHelper.ToSafeUrlParameter(Request.UrlReferrer.PathAndQuery);
            }
            return String.Format("/Orion/SRM/Admin/EditProperties.aspx?NetObject={0}&ReturnTo={1}", Netobject.NetObjectID, redirectUrl);
        };
    }

    protected override string DefaultTitle => global::Resources.SrmWebContent.CustomProperties_LUNs;

    public override string EditControlLocation => "/Orion/SRM/Controls/EditResourceControls/EditCustomPropertyList.ascx";

    public override string HelpLinkFragment => "SRMPHResourceCustomPropertiesLUNs";

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces => new [] { typeof(ILunProvider) };

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl;
}

