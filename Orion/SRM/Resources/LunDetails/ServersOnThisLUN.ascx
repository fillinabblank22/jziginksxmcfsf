﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ServersOnThisLUN.ascx.cs" Inherits="Orion_SRM_Resources_LUNDetails_ServersOnThisLUN" %>
<%@ Register src="../../Controls/AjaxTree.ascx" TagName="AjaxTree" TagPrefix="srm" %>

<srm:AjaxTree ID="AjaxTree" runat="server" ServicePath="/Orion/SRM/Services/ServersOnThisLUNTree.asmx" />
