﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Placeholder.ascx.cs" Inherits="Orion_SRM_Resources_Placeholder" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.css" />

<orion:ResourceWrapper ID="Wrapper" runat="server">
    <Content>
        <!-- When current resource is no more needed, don't forget to remove also resource_coming_soon.png -->
        <table style="height: 170px; border:0px; background-image: url('/Orion/SRM/images/resource_coming_soon.png'); background-position: center; background-repeat: no-repeat;">
            <tr><td style="border:0px;"></td></tr>
        </table>
    </Content>
</orion:ResourceWrapper>
