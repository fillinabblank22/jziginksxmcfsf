﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PerformanceSummary.ascx.cs" Inherits="Orion_SRM_Resources_PoolDetails_PerformanceSummary" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="srm" TagName="PerformanceSummary" Src="~/Orion/SRM/Controls/PerformanceSummary.ascx" %>

<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <srm:PerformanceSummary runat="server" id="PerformanceSummary" />
    </Content>
</orion:ResourceWrapper>
