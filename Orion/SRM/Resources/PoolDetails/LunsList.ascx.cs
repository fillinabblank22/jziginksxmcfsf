﻿using System;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;

public partial class Orion_SRM_Resources_PoolDetails1_LunsList : PoolBaseResourceControl, IResourceIsInternal
{
    #region Fields

    private string query;
    private const string provisionedQueryTemplate = @"l.CapacityAllocated AS [CapacityAllocated],
                                            CASE WHEN 
                                                    l.CapacityTotal is Null 
                                                 OR l.CapacityTotal = 0 
                                                 OR l.CapacityAllocated is Null
                                                 THEN NULL
                                                 ELSE ((l.CapacityAllocated * 1.0 / l.CapacityTotal) * 100) END AS [CapacityAllocatedPercentage],";

    private const string queryTemplate = @"SELECT l.Caption as [Name],
                                            '/Orion/StatusIcon.ashx?entity={0}&status=' + ToString(ISNULL(l.Status, 0)) +'&size=small' AS [_IconFor_Name],
                                            '/Orion/View.aspx?NetObject={1}:' + ToString(l.LunID) AS [_LinkFor_Name],
                                            Concat('%{1}:', l.LunID,'%') AS [AssociatedEndpoint],                               
                                            l.CapacityTotal AS [TotalSize],
                                            {5}
                                            Concat('%{1}:', l.LunID,'%') AS [FileSystemUsedCapacity],
                                            Concat('%{1}:', l.LunID,'%') AS [FileSystemUsedPercentage],
                                            Concat('{1}:', l.LunID) AS [_NetObjectID]
                                            FROM Orion.SRM.Luns (nolock=true) l
                                            WHERE l.Thin = {3} AND l.Pools.PoolID={4}";

    #endregion


    #region Properties

    /// <summary>
    /// Override default title property.
    /// </summary>
    protected override string DefaultTitle
    {
        get
        {
            return IsThick ? SrmWebContent.ThickLunsListResourceTitle : SrmWebContent.ThinLunsListResourceTitle;
        }
    }

    /// <summary>
    /// Override help link fragment property.
    /// </summary>
    public override string HelpLinkFragment
    {
        get
        {
            return IsThick ? "SRMPHResourceThickLUNs" : "SRMPHResourceThinsLUNs"; ;
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
    }

    /// <summary>
    /// Determinate if resource is internal.
    /// </summary>
    public bool IsInternal
    {
        get
        {
            return true;
        }
    }

    /// <summary>
    /// Gets or sets Search Control property.
    /// </summary>
    public ResourceSearchControl SearchControl
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets is thick property.
    /// </summary>
    public bool IsThick
    {
        get;
        set;
    }

    /// <summary>
    /// Gets additional query for search control.
    /// </summary>
    public string SearchSwql
    {
        get
        {
            return string.Concat(query, " AND l.Caption LIKE '%${SEARCH_STRING}%'");
        }
    }

    #endregion


    #region Methods

    /// <summary>
    /// OnLoad override method.
    /// </summary>
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (Pool == null || Pool.PoolEntity == null)
            throw new NullReferenceException("Pool can't be null");
        this.Visible = Pool.PoolEntity.Category == PoolCategory.Block;
        if (this.Visible)
        {

            SearchControl =
                (ResourceSearchControl) LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
            Wrapper.HeaderButtons.Controls.Add(SearchControl);

            IsThick = Convert.ToBoolean(Resource.Properties["IsThick"]);

            query = InvariantString.Format(queryTemplate, SwisEntities.Lun, Lun.NetObjectPrefix, Pool.NetObjectPrefix,
                                           IsThick ? 0 : 1, Pool.PoolEntity.PoolID, !IsThick ? provisionedQueryTemplate : String.Empty);

            CustomTable.UniqueClientID = ScriptFriendlyResourceId;
            CustomTable.SWQL = query;
            CustomTable.SearchSWQL = SearchSwql;
        }
    }

    #endregion

}