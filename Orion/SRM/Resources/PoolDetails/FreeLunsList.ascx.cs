﻿using System;
using System.Globalization;
using Resources;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.UI;

namespace Orion.SRM.Resources.PoolDetails
{
    public partial class Orion_SRM_Resources_PoolDetails_FreeLunsList : PoolBaseResourceControl
    {
        #region Fields

        private string query;

        #endregion

        #region Properties

        /// <summary>
        /// Override default title property.
        /// </summary>
        protected override string DefaultTitle
        {
            get { return SrmWebContent.Free_Luns_Title; }
        }

        /// <summary>
        /// Override help link fragment property.
        /// </summary>
        public override string HelpLinkFragment
        {
            get { return "SRMPHResourceFreeLUNs"; }
        }

        public override string EditControlLocation
        {
            get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
        }

        /// <summary>
        /// Gets or sets Search Control property.
        /// </summary>
        public ResourceSearchControl SearchControl { get; set; }

        /// <summary>
        /// Gets additional query for search control.
        /// </summary>
        public string SearchSwql
        {
            get { return string.Concat(query, " AND l.[Caption] LIKE '%${SEARCH_STRING}%'"); }
        }

        #endregion

        /// <summary>
        /// OnLoad override method.
        /// </summary>
        protected override void OnLoad(EventArgs e)
        {
            this.Visible = Pool.PoolEntity.Category == PoolCategory.Block;
            if (this.Visible)
            {
                base.OnLoad(e);

                SearchControl =
                    (ResourceSearchControl) LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
                ResourceWrapper.HeaderButtons.Controls.Add(SearchControl);

                query = String.Format(CultureInfo.InvariantCulture,
                                      @"SELECT l.[Caption] AS Name,
                    '/Orion/View.aspx?NetObject={0}:' + ToString(LunID) AS [_LinkFor_Name],
                    '/Orion/StatusIcon.ashx?entity={3}&status=' + ToString(ISNULL([Status], 0)) +'&size=small' AS [_IconFor_Name],                     
                    l.Pools.Caption AS [PoolName],
                    '/Orion/View.aspx?NetObject={1}:' + ToString(l.Pools.PoolID) AS [_LinkFor_PoolName],
                    l.CapacityTotal AS [TotalUserCapacity]
                    FROM Orion.SRM.Luns (nolock=true) l
                    WHERE l.Pools.PoolID = {2}  AND l.Free = 1",
                                      Lun.NetObjectPrefix, Pool.NetObjectPrefix, Pool.PoolEntity.PoolID,
                                      SwisEntities.Lun);

                CustomTable.UniqueClientID = ScriptFriendlyResourceId;
                CustomTable.SWQL = query;
                CustomTable.SearchSWQL = SearchSwql;
            }
        }
    }
}