﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PoolNasCifsSmbShares.ascx.cs" Inherits="Orion.SRM.Resources.PoolDetails.Orion_SRM_Resources_PoolDetails_PoolNasCifsSmbShares" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register Src="~/Orion/SRM/Controls/FileSharesList.ascx" TagPrefix="orion" TagName="FileSharesList" %>

<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:FileSharesList runat="server" ID="FileSharesList" />
    </Content>
</orion:resourceWrapper>
