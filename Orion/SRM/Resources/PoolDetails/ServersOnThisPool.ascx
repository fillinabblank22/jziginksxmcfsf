﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ServersOnThisPool.ascx.cs" Inherits="Orion_SRM_Resources_PoolDetails_ServersOnThisPool" %>
<%@ Register src="../../Controls/AjaxTree.ascx" TagName="AjaxTree" TagPrefix="srm" %>

<srm:AjaxTree ID="AjaxTree" runat="server" ServicePath="/Orion/SRM/Services/ServersOnThisPoolTree.asmx" />
