﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StoragePoolDetails.ascx.cs" Inherits="Orion_SRM_Resources_PoolDetails_StoragePoolDetails" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web" %>
<%@ Import Namespace="SolarWinds.SRM.Web.NetObjects" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.css" />
<orion:Include ID="Include3" runat="server" Module="SRM" File="SRM.Formatters.js" />

<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <table cellspacing="0" class="srm_details_table">
            <tr>
                <td class="PropertyHeader">
                    <div class="SRM_FirstRowDetailsTable">
                        <%= Resources.SrmWebContent.LinkToPerfStack_Title %>:
                    </div>
                </td>
                <td class="Property SRM_StatusImage">
                    <asp:Image ID="PerformanceAnalyzerImage" CssClass="StatusIcon" runat="server" />
                </td>
                <td class="Property">
                    <asp:HyperLink runat="server" ID="PerformanceHyperLink" Text="Performance Analyzer"/>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.StoragePoolDetails_Status_Tooltip %>">
                    <%= SrmWebContent.BlockStoragePoolDetails_PoolStatus  %>:</span>
                </td>
                <td class="Property SRM_StatusImage">
                    <asp:Image ID="StatusIconImage" CssClass="StatusIcon" runat="server" />
                </td>
                <td class="Property">
                <asp:Label runat="server" ID="StatusLabel" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= SrmWebContent.BlockStoragePoolDetails_Name %>:
                </td>
                <td></td>
                <td>
                    <asp:Label runat="server" ID="NameLabel" />
                </td>
            </tr>
            <tr runat="server" ID="ArrayTableRow">
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.StoragePoolDetails_Array_Tooltip %>">
                    <%= SrmWebContent.BlockStoragePoolDetails_Array %>:</span>
                </td>
                <td></td>
                <td class="SRM_Icon_Cell">
                    <img src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.StorageArrays%>&status=<%=(int)Pool.StorageArray.Array.Status%>" />
                    <asp:HyperLink runat="server" ID="ArrayHyperlink" />
                </td>
            </tr>
            <tr runat="server" ID="ParentPoolRow">
                <td class="PropertyHeader">
                      <%= SrmWebContent.BlockStoragePoolDetails_ParentPool %>:
                </td>
                <td></td>
                <td>
                    <asp:Repeater id="PoolsList" runat="server">
                        <ItemTemplate>
                            <span class="SRM_Icon_Cell">
                                <img src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.Pools%>&status=<%# (int)Eval("Status") %>" />
                                <asp:HyperLink 
                                    Text='<%# Eval("DisplayName") %>' 
                                    NavigateUrl='<%# String.Format(CultureInfo.CurrentCulture, "/Orion/View.aspx?NetObject={0}:{1}", Pool.NetObjectPrefix, Eval("PoolID")) %>' runat="server" ID="PoolHyperLink" />
                                <br />
                            </span>
                        </ItemTemplate>
                    </asp:Repeater>                    
                </td>
            </tr>
            <tr runat="server" ID="VServerTableRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.StoragePoolDetails_vServer %>:
                </td>
                <td></td>
                <td>
                    <asp:Repeater id="VServersList" runat="server">
                        <ItemTemplate>
                            <span class="SRM_Icon_Cell">
                                <img src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.VServers%>&status=<%# (int)Eval("Status") %>" />
                                <asp:HyperLink 
                                    Text='<%# Eval("DisplayName") %>' 
                                    NavigateUrl='<%# String.Format(CultureInfo.CurrentCulture, "/Orion/View.aspx?NetObject={0}:{1}", VServer.NetObjectPrefix, Eval("VServerId")) %>' runat="server" ID="VServerHyperLink" />
                                <br />
                            </span>
                        </ItemTemplate>
                    </asp:Repeater>                    
                </td>
            </tr>
            <tr runat="server" ID="ClusterTableRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.StoragePoolDetails_Cluster %>:
                </td>
                <td></td>
                <td class="SRM_Icon_Cell">
                    <img src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.Clusters%>&status=<%=(int)Pool.StorageArray.Array.Status%>" />
                    <asp:HyperLink runat="server" ID="ClusterHyperLink" />
                </td>
            </tr>
              <tr runat="server" id="VendorStatusCodeRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.StoragePoolDetails_VendorStatusCode%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="VendorStatusCodeLabel" /></td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.StoragePoolDetails_Type_Tooltip %>">
                    <%= SrmWebContent.BlockStoragePoolDetails_Type %>:</span>
                </td>
                <td></td>
                <td>
                    <asp:Label runat="server" ID="TypeLabel" />
                </td>
            </tr>
            <tr  runat="server" id="RAIDTypeRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.BlockStoragePoolDetails_RAIDType %>:
                </td>
                <td></td>
                <td>
                    <asp:Label runat="server" ID="RAIDTypeLabel" />
                </td>
            </tr>
            <tr runat="server" ID="LunCountTableRow">
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.StoragePoolDetails_LunCount_Tooltip %>">
                    <%= SrmWebContent.BlockStoragePoolDetails_LUNCount %>:</span>
                </td>
                <td></td>
                <td>
                    <asp:Label runat="server" ID="LUNCountLabel" />
                </td>
            </tr>
            <tr runat="server" ID="VolumeCountTableRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.BlockStoragePoolDetails_NASVolumeCount %>:
                </td>
                <td></td>
                <td>
                    <asp:Label runat="server" ID="NASVolumeCountLabel" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.StoragePoolDetails_TotalUsableCapacity_Tooltip %>">
                    <%= SrmWebContent.BlockStoragePoolDetails_TotalSize %>:</span>
                </td>
                <td></td>
                <td>
                    <asp:Label runat="server" ID="TotalSizeLabel" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.StoragePoolDetails_TotalSubscribedCapacity_Tooltip %>">
                    <%= SrmWebContent.BlockStoragePoolDetails_TotalSubscribedCapacity %>:</span>
                </td>
                <td></td>
                <td>
                    <asp:Label runat="server" ID="TotalSubscribedCapacityLabel" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.StoragePoolDetails_OverSubscribedCapacity_Tooltip %>">
                    <%= SrmWebContent.BlockStoragePoolDetails_OverSubscribedCapacity %>:</span>
                </td>
                <td></td>
                <td>
                    <asp:Label runat="server" ID="OverSubscribedCapacityLabel" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.StoragePoolDetails_ProvisionedCapacity_Tooltip %>">
                    <%= SrmWebContent.BlockStoragePoolDetails_ProvisionedCapacity %>:</span>
                </td>
                <td></td>
                <td>
                    <asp:Label runat="server" ID="ProvisionedCapacityLabel" />
                </td>
            </tr>
            <!--
            <tr runat="server" ID="rowTotalReduction">
                <td class="PropertyHeader">
                    <%= SrmWebContent.UsableCapacityTotalReduction %>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="TotalReductionLabel" />
                </td>
            </tr>
            -->
            <tr runat="server" ID="rowDataReduction">
                <td class="PropertyHeader">
                    <%= SrmWebContent.UsableCapacityDataReduction %>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="DataReductionLabel" />
                </td>
            </tr>
            <tr runat="server" id="TotalLatencyRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.BlockStoragePoolDetails_TotalLatency %>:
                </td>
                <td></td>
                <td>
                    <asp:Label runat="server" ID="TotalLatencyLabel" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.StoragePoolDetails_LastDatabaseUpdate_Tooltip %>">
                    <%= SrmWebContent.BlockStoragePoolDetails_LastSync%>:</span>
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="LastSync" /></td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <span Title="<%= Resources.SrmWebContent.StoragePoolDetails_ProjectedRunOut_Tooltip %>">
                    <%= SrmWebContent.BlockStoragePoolDetails_ProjectedRunOut %>:</span>
                </td>
                <td></td>
                <td>
                    <span id="RunSpan_<%=Resource.ID%>" class="SRM_BoldText">
                        <script type="text/javascript">$('#RunSpan_<%=Resource.ID%>').html(SW.SRM.Formatters.FormatDaysDiff(<%= CapacityProjectedRunout %>, <%= WarningThresholdDays %>, <%= CriticalThresholdDays %>))</script>
                    </span>
                </td>
            </tr>
        </table>
        <script type="text/javascript">
            $('.srm_details_table tr:even').addClass('SRM_AltRow1').removeClass('SRM_AltRow2');
            $('.srm_details_table tr:odd').addClass('SRM_AltRow2').removeClass('SRM_AltRow1');
        </script>
    </Content>
</orion:ResourceWrapper>
