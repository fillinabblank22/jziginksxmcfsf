﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.Orion.NPM.Web;
using System.Globalization;
using System.Diagnostics;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_SRM_Resources_PoolDetails_LatencyHistogramChart : SrmLineChart
{
    private IEnumerable<string> elementIdsCache;

    protected override string NetObjectPrefix
    {
        get { return Pool.NetObjectPrefix; }
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IPoolProvider) }; }
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get { return new[] { typeof(IMultiPoolProvider) }; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditLatencyHistogram.ascx"; }
    }
    
    public override string EditURL
    {
        get
        {
            return ExtendCustomEditUrl("/Orion/NetPerfMon/Resources/EditResource.aspx");
        }
    }

    public override string HelpLinkFragment
    {
        get { return Visible ? "SRMPHResourceLatencyHistogram" : String.Empty; }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        if (elementIdsCache != null)
        {
            return elementIdsCache;
        }

        List<string> ids = new List<string>();

        var multiplePoolProvider = GetInterfaceInstance<IMultiObjectProvider>();
        if (multiplePoolProvider != null && multiplePoolProvider.SelectedNetObjects.Length > 0)
        {
            ids.AddRange(multiplePoolProvider.SelectedNetObjects);
        }
        else
        {
            var poolProvider = GetInterfaceInstance<IPoolProvider>();
            if (poolProvider != null)
            {
                ids.Add(poolProvider.Pool.NetObjectID);
            }
        }

        elementIdsCache = ids;

        return elementIdsCache;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Pool pool = GetInterfaceInstance<IPoolProvider>().Pool;

        var deviceGroup = ServiceLocator.GetInstance<IDeviceGroupDAL>().GetDeviceGroupDetailsByArrayId(pool.StorageArray.Array.StorageArrayId.Value);
        
        string hideLatencyHistogram;
        PoolCategory poolType = pool.PoolEntity.Category;
        if (deviceGroup.Properties.TryGetValue(poolType == PoolCategory.Block ? "HideLatencyHistogramBlock" : "HideLatencyHistogramFile", out hideLatencyHistogram))
        {
            bool isHidden;
            if (!bool.TryParse(hideLatencyHistogram, out isHidden))
            {
                isHidden = false;
            }
            Visible = !isHidden;
        }

        var asBlockString = Resource.Properties["AsBlock"];

        bool asBlock = String.IsNullOrEmpty(asBlockString) ? true : Boolean.Parse(asBlockString);
        Visible &= ((poolType == PoolCategory.Block && asBlock) || (poolType == PoolCategory.File && !asBlock)) && IsResourceVisible();
        if (Visible)
        {
            HandleInit(WrapperContents);
            OrionInclude.ModuleFile("SRM", "Charts/Charts.SRM.Options.js", OrionInclude.Section.Middle);

            Resource.Properties["DataSourceMethod"] = poolType == PoolCategory.File
                ? "GetData_FileStoragePool"
                : "GetData_BlockStoragePool";
        }
    }

    private bool IsResourceVisible()
    {
        IPoolProvider poolProvider = GetInterfaceInstance<IPoolProvider>();
        if (poolProvider != null)
        {
            return ServiceLocator.GetInstance<IResourceVisibilityService>()
                .IsResourceVisibleForArray(poolProvider.Pool.StorageArray, this.AppRelativeVirtualPath);
        }
        return true;
    }
}