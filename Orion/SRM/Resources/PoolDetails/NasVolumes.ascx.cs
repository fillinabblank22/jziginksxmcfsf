using System;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;

public partial class Orion_SRM_Resources_PoolDetails_NasVolumes : PoolBaseResourceControl, IResourceIsInternal
{
    #region Fields

    private string query;

    private const string queryTemplateThin = @"SELECT v.Caption AS [Name],
                                                '/Orion/StatusIcon.ashx?entity={0}&status=' + ToString(ISNULL(v.Status, 0)) +'&size=small' AS [_IconFor_Name],
                                                '/Orion/View.aspx?NetObject={1}:' + ToString(v.VolumeID) AS [_LinkFor_Name],
                                                ISNULL(v.CapacityTotal, '') AS [TotalSize],
                                                ISNULL(v.CapacityAllocated, '') AS [AllocatedTotal],
                                                CASE WHEN 
                                                        v.CapacityTotal is Null 
                                                     OR v.CapacityTotal = 0 
                                                     OR v.CapacityAllocated is Null
                                                     THEN NULL
                                                     ELSE ((v.CapacityAllocated * 1.0 / v.CapacityTotal) * 100) END AS [AllocatedPercentage],
                                                v.CapacityFileSystem AS [FileSystemUsed],
                                                CASE WHEN 
                                                        v.CapacityTotal is Null 
                                                     OR v.CapacityTotal = 0 
                                                     OR v.CapacityFileSystem is Null
                                                     THEN NULL
                                                     ELSE ((v.CapacityFileSystem * 1.0 / v.CapacityTotal) * 100) END AS [FileSystemUsedPercentage],
                                                DayDiff(getUtcDate(), v.CapacityRunout) AS [CapacityRunOut],
                                                Concat('{1}:', v.VolumeID) AS [_NetObjectID]
                                                FROM Orion.SRM.Volumes (nolock=true) v
                                                WHERE v.Thin = 1 AND v.Pools.PoolID={2}";

    private const string queryTemplateThick = @"SELECT v.Caption AS [Name],
                                                '/Orion/StatusIcon.ashx?entity={0}&status=' + ToString(ISNULL(v.Status, 0)) +'&size=small' AS [_IconFor_Name],
                                                '/Orion/View.aspx?NetObject={1}:' + ToString(v.VolumeID) AS [_LinkFor_Name],
                                                ISNULL(v.CapacityTotal, '') AS [TotalSize],
                                                v.CapacityFileSystem AS [FileSystemUsed],
                                                CASE WHEN 
                                                        v.CapacityTotal is Null 
                                                     OR v.CapacityTotal = 0 
                                                     OR v.CapacityFileSystem is Null
                                                     THEN NULL
                                                     ELSE ((v.CapacityFileSystem * 1.0 / v.CapacityTotal) * 100) END AS [FileSystemUsedPercentage],
                                                DayDiff(getUtcDate(), v.CapacityRunout) AS [CapacityRunOut],
                                                Concat('{1}:', v.VolumeID) AS [_NetObjectID]
                                                FROM Orion.SRM.Volumes (nolock=true) v
                                                WHERE v.Thin = 0 AND v.Pools.PoolID={2}";

    #endregion


    #region Properties

    /// <summary>
    /// Gets or sets default title.
    /// </summary>
    protected override string DefaultTitle
    {
        get
        {
            return IsThick ? SrmWebContent.SRM_StoragePool_ThickNASVolumes : SrmWebContent.SRM_StoragePool_ThinNASVolumes;
        }
    }

    /// <summary>
    /// Override help link fragment property.
    /// </summary>
    public override string HelpLinkFragment
    {
        get
        {
            return IsThick ? "SRMPHResourceThickVolumes" : "SRMPHResourceThinVolumes";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
    }

    /// <summary>
    /// Determinate if resource is internal.
    /// </summary>
    public bool IsInternal
    {
        get
        {
            return true;
        }
    }

    /// <summary>
    /// Gets or sets search control resource property.
    /// </summary>
    public ResourceSearchControl SearchControl
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets warning threshold days for projected runout.
    /// </summary>
    public int WarningThresholdDays
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets critical threshold days for projected runout.
    /// </summary>
    public int CriticalThresholdDays
    {
        get;
        set;
    }

    /// <summary>
    /// Additional query for swql query.
    /// </summary>
    public string SearchSWQL
    {
        get
        {
            return string.Concat(query, " AND v.Name LIKE '%${SEARCH_STRING}%'");
        }
    }

    /// <summary>
    /// Determinate is pool is thick.
    /// </summary>
    public bool IsThick
    {
        get;
        set;
    }

    #endregion


    #region Methods

    /// <summary>
    /// OnLoad resource event handler.
    /// </summary>
    protected override void OnLoad(EventArgs e)
    {
        if (Pool == null || Pool.PoolEntity == null)
            throw new NullReferenceException("Pool can't be null");
        this.Visible = Pool.PoolEntity.Category == PoolCategory.File;
        if (this.Visible)
        {
            base.OnLoad(e);

            SearchControl =
                (ResourceSearchControl) LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
            Wrapper.HeaderButtons.Controls.Add(SearchControl);

            IsThick =  Convert.ToBoolean(Resource.Properties["IsThick"]);

            query = InvariantString.Format(IsThick ? queryTemplateThick : queryTemplateThin, SwisEntities.Volume,
                                           Volume.NetObjectPrefix, Pool.PoolEntity.PoolID);

            WarningThresholdDays = (int) SettingsDAL.GetSetting(SRMConstants.WarningThresholdDays).SettingValue;
            CriticalThresholdDays = (int) SettingsDAL.GetSetting(SRMConstants.CriticalThresholdDays).SettingValue;

            CustomTable.UniqueClientID = ScriptFriendlyResourceId;
            CustomTable.SWQL = query;
            CustomTable.SearchSWQL = SearchSWQL;
        }
    }

    #endregion
}