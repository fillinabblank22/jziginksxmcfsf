﻿using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Web.UI;
using System;
using System.Web.Script.Serialization;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_SRM_Resources_PoolDetails_PerformanceSummary : PoolBaseResourceControl
{
    private static readonly JavaScriptSerializer serializer = new JavaScriptSerializer { MaxJsonLength = 0x400000 };
    protected override string DefaultTitle
    {
        get { return SrmWebContent.Pool_Performance_Summary_Title; }
    }

    public override string HelpLinkFragment
    {
        get { return "SRMPHResourcePerformanceSummary"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override string EditURL
    {
        get
        {
            return ExtendCustomEditUrl("/Orion/SRM/EditTopXXChartResource.aspx?EditXX=False");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        PerformanceSummary.DataBindMethod = Pool.PoolEntity.Category == PoolCategory.Block
            ? "GetDataStoragePoolBlock"
            : "GetDataStoragePoolFile";
        PerformanceSummary.ResourceID = Resource.ID;
        PerformanceSummary.ResourceWidth = Resource.Width;

        // panels of chart
        PerformanceSummary.IOPSEnable = true;
        PerformanceSummary.ThroughputEnable = true;
        PerformanceSummary.IOSizeEnable = true;
        PerformanceSummary.CacheHitRatioEnable = true;
        PerformanceSummary.LatencyEnable = true;
        PerformanceSummary.IOPSRatioEnable = true;

        PerformanceSummary.CustomIOPSResourceTitle = SrmWebContent.PerformanceDrillDown_PoolIOPSResourceTitle;
        PerformanceSummary.CustomIOPSChartTitle = SrmWebContent.PerformanceDrillDown_PoolIOPSChartTitle;
        PerformanceSummary.CustomThroughputResourceTitle = SrmWebContent.PerformanceDrillDown_PoolThroughputResourceTitle;
        PerformanceSummary.CustomThroughputChartTitle = SrmWebContent.PerformanceDrillDown_PoolThroughputChartTitle;
        PerformanceSummary.CustomIOSizeResourceTitle = SrmWebContent.PerformanceDrillDown_PoolIOSizeResourceTitle;
        PerformanceSummary.CustomIOSizeChartTitle = SrmWebContent.PerformanceDrillDown_PoolIOSizeChartTitle;
        PerformanceSummary.CustomCacheHitRatioResourceTitle = SrmWebContent.PerformanceDrillDown_PoolCacheHitRatioResourceTitle;
        PerformanceSummary.CustomCacheHitRatioChartTitle = SrmWebContent.PerformanceDrillDown_PoolCacheHitRatioChartTitle;
        PerformanceSummary.CustomLatencyResourceTitle = SrmWebContent.PerformanceDrillDown_PoolLatencyResourceTitle;
        PerformanceSummary.CustomLatencyChartTitle = SrmWebContent.PerformanceDrillDown_PoolLatencyChartTitle;
        PerformanceSummary.CustomIOPSRatioResourceTitle = SrmWebContent.PerformanceDrillDown_PoolIOPSRatioResourceTitle;
        PerformanceSummary.CustomIOPSRatioChartTitle = SrmWebContent.PerformanceDrillDown_PoolIOPSRatioChartTitle;

        PerformanceSummary.SampleSizeInMinutes = SampleSizeInMinutes;
        PerformanceSummary.DaysOfDataToLoad = DaysOfDataToLoad;
        PerformanceSummary.InitialZoom = InitialZoom;

        PerformanceSummary.ChartTitleProperty = Resource.Properties["ChartTitle"];
        PerformanceSummary.ChartSubTitleProperty = Resource.Properties["ChartSubTitle"];
    }

    protected override bool IsResourceVisible()
    {
        return ServiceLocator.GetInstance<IResourceVisibilityService>()
            .IsResourceVisibleForArray(Pool.StorageArray, this.AppRelativeVirtualPath);
    }
}
