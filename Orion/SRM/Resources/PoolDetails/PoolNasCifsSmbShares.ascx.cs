﻿using System;
using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Web.UI;

namespace Orion.SRM.Resources.PoolDetails
{
    [ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
    public partial class Orion_SRM_Resources_PoolDetails_PoolNasCifsSmbShares : PoolBaseResourceControl
    {
        protected override string DefaultTitle
        {
            get { return SrmWebContent.PoolNasCifsSmbShares_Resource_Title; }
        }

        public override string HelpLinkFragment
        {
            get
            {
                return "SRMPHResourceShares";
            }
        }

        public override string EditControlLocation
        {
            get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
        }

        protected override void OnLoad(EventArgs e)
        {
            this.Visible = Pool.PoolEntity.Category == PoolCategory.File;
            if (this.Visible)
            {
                base.OnLoad(e);
                FileSharesList.ParentResourceId = ScriptFriendlyResourceId;
                FileSharesList.Where =
                    InvariantString.Format(
                        @"WHERE fs.Type = {0} AND fs.Volume.Pools.PoolID = {1}",
                        (int)FileSharesType.CIFS, Pool.PoolEntity.PoolID);

                ResourceSearchControl searchControl =
                    (ResourceSearchControl) LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
                Wrapper.HeaderButtons.Controls.Add(searchControl);
                FileSharesList.SearchControl = searchControl;
            }
        }
    }
}
