﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Common;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Common.Enums;

namespace Orion.SRM.Resources.PoolDetails
{
    public partial class Orion_SRM_Resources_PoolDetails_StoragePoolsList : SrmBaseResourceControl, IResourceIsInternal
    {
        #region Fields

        private string query;

        #endregion


        #region Properties

        /// <summary>
        /// Gets or sets default title.
        /// </summary>
        protected override string DefaultTitle
        {
            get
            {
                return SrmWebContent.StoragePoolDetails_ResourceTitle;
            }
        }

        /// <summary>
        /// Override help link fragment property.
        /// </summary>
        public override string HelpLinkFragment
        {
            get
            {
                return IsThick ? "SRMPHResourceThickPools" : "SRMPHResourceThinPools";
            }
        }

        public override string EditControlLocation
        {
            get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
        }

        /// <summary>
        /// Determinate if resource is internal.
        /// </summary>
        public bool IsInternal
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets required interface for resource.
        /// </summary>
        public override IEnumerable<Type> RequiredInterfaces
        {
            get
            {
                return new Type[] { typeof(IPoolProvider) };
            }
        }

        /// <summary>
        /// Gets storage Pool property.
        /// </summary>
        public Pool Pool
        {
            get
            {
                IPoolProvider tmp = GetInterfaceInstance<IPoolProvider>();
                if (tmp != null)
                {
                    return tmp.Pool;
                }
                return null;
            }
        }

        /// <summary>
        /// Gets or sets search control resource property.
        /// </summary>
        public ResourceSearchControl SearchControl
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets warning threshold days for projected runout.
        /// </summary>
        public int WarningThresholdDays
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets critical threshold days for projected runout.
        /// </summary>
        public int CriticalThresholdDays
        {
            get;
            set;
        }

        /// <summary>
        /// Additional query for swql query.
        /// </summary>
        public string SearchSWQL
        {
            get
            {
                return string.Concat(query, " AND p.[Caption] LIKE '%${SEARCH_STRING}%'");
            }
        }

        /// <summary>
        /// Determinate is pool is thick.
        /// </summary>
        public bool IsThick
        {
            get;
            set;
        }

        #endregion


        #region Methods

        /// <summary>
        /// OnLoad resource event handler.
        /// </summary>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);            
            SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
            Wrapper.HeaderButtons.Controls.Add(SearchControl);

            List<int> poolCategories = Enum.GetValues(typeof(PoolCategory)).Cast<int>().ToList();

            IsThick = Convert.ToBoolean(Resource.Properties["IsThick"]);
            if (IsThick)
            {
                query = String.Format(CultureInfo.InvariantCulture, @"SELECT p.[Caption] AS Name   
                        ,'/Orion/StatusIcon.ashx?entity={0}&status=' + ToString(ISNULL(p.[Status], 0)) +'&size=small' AS [_IconFor_Name]
                        ,'/Orion/View.aspx?NetObject={1}:' + ToString(p.[PoolID]) AS [_LinkFor_Name]
                        ,p.[Type] as [Type]
                        ,ISNULL(p.[CapacityUserTotal], '') AS [CapacityUserTotal]
                        ,ISNULL(p.[CapacityUserUsed], '') AS [CapacityAllocated]
                        FROM Orion.SRM.Pools (nolock=true) p INNER JOIN 
                        Orion.SRM.PoolToPoolsMapping (nolock=true) m 
                        ON p.[PoolID]=m.[PoolID]
                        WHERE p.[Thin] = 0 AND m.[ParentPoolID] = {2}",
                        SwisEntities.Pools, Pool.NetObjectPrefix, Pool.PoolEntity.PoolID);
            }
            else
            {
                query = String.Format(CultureInfo.InvariantCulture, @"SELECT p.[Caption] AS Name
                        ,'/Orion/StatusIcon.ashx?entity={0}&status=' + ToString(ISNULL(p.[Status], 0)) +'&size=small' AS [_IconFor_Name]
                        ,'/Orion/View.aspx?NetObject={1}:' + ToString(p.[PoolID]) AS [_LinkFor_Name]
                        ,p.[Type] as [Type]
                        ,ISNULL(p.[CapacityUserTotal], '') AS [CapacityUserTotal]
                        ,ISNULL(p.[CapacityUserUsed], '') AS [CapacityAllocated]
                        ,ISNULL(p.[CapacitySubscribed], '') AS [SubscribedCapacity]
                        ,ISNULL(p.[CapacityOversubscribed], '') AS [CapacityOverSubscribed]
                        ,DayDiff(GetUtcDate(), p.CapacityRunout) AS [ProjectedRunOut]
                        FROM Orion.SRM.Pools (nolock=true) p INNER JOIN 
                        Orion.SRM.PoolToPoolsMapping (nolock=true) m 
                        ON p.[PoolID]=m.[PoolID]
                        WHERE p.[Thin] = 1 AND m.[ParentPoolID] = {2}",
                        SwisEntities.Pools, Pool.NetObjectPrefix, Pool.PoolEntity.PoolID);
            }

            WarningThresholdDays = (int)SettingsDAL.GetSetting(SRMConstants.WarningThresholdDays).SettingValue;
            CriticalThresholdDays = (int)SettingsDAL.GetSetting(SRMConstants.CriticalThresholdDays).SettingValue;

            CustomTable.UniqueClientID = ScriptFriendlyResourceId;
            CustomTable.SWQL = query;
            CustomTable.SearchSWQL = SearchSWQL;
        }

        #endregion
    }
}