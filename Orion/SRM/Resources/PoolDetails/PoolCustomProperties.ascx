<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PoolCustomProperties.ascx.cs" Inherits="Orion_SRM_Resources_PoolDetails_PoolCustomProperties" %>
<%@ Register TagPrefix="orion" TagName="CustomPropertyList" Src="~/Orion/SRM/Controls/CustomPropertyList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:CustomPropertyList runat="server" ID="customPropertyList" EntityName="Orion.SRM.PoolCustomProperties"/>
    </Content>
</orion:resourceWrapper>