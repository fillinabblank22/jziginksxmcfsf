﻿using System;
using System.Globalization;
using System.Web.UI.WebControls;

using Resources;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Plugins;

public partial class Orion_SRM_Resources_PoolDetails_StoragePoolDetails : PoolBaseResourceControl
{
    private const int NumberOfFractDigits = 2;

    #region Properties

    /// <summary>
    /// Override default title property.
    /// </summary>
    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.StoragePoolDetails_ResourceTitle;
        }
    }

    /// <summary>
    /// Override help link fragment property.
    /// </summary>
    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourcePoolDetails";
        }
    }

    /// <summary>
    /// Gets or sets warning threshold days property.
    /// </summary>
    protected int WarningThresholdDays
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets critical threshold days property.
    /// </summary>
    protected int CriticalThresholdDays
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets capacity projected runout property.
    /// </summary>
    public int CapacityProjectedRunout
    {
        get;
        set;
    }

    #endregion


    #region Methods

    /// <summary>
    /// OnLoad override method.
    /// </summary>
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (!OrionConfiguration.IsDemoServer)
            ResourceWrapper.HeaderButtons.Controls.Add(LoadControl("~/Orion/SRM/Controls/ManageNetObjectButton.ascx"));
        WarningThresholdDays = (int) SettingsDAL.GetSetting(SRMConstants.WarningThresholdDays).SettingValue;
        CriticalThresholdDays = (int) SettingsDAL.GetSetting(SRMConstants.CriticalThresholdDays).SettingValue;

        if (Pool != null)
        {
            PoolEntity poolEntity = Pool.PoolEntity;
            int poolId = poolEntity.PoolID;
            var storageArray = Pool.StorageArray;

            this.PerformanceAnalyzerImage.ImageUrl = PerformanceStackHelper.PerformanceAnalyzerImage;
            SetupPerformanceHyperLink(this.PerformanceHyperLink);

            StatusIconImage.ImageUrl = InvariantString.Format("/Orion/StatusIcon.ashx?entity={0}&status={1}&size=large", SwisEntities.Pools, (int)poolEntity.Status);
            StatusLabel.Text = String.Format(CultureInfo.CurrentUICulture, "{0} {1}", Pool.Name, SrmStatusLabelHelper.GetStatusLabel(poolEntity.Status));

            if (storageArray.Array.IsCluster)
            {
                ArrayTableRow.Visible = false;

                // Assign data source to the repeater and rebind
                VServersList.DataSource = ServiceLocator.GetInstance<IVServerDAL>().GetVServersByPoolId(poolId);
                VServersList.DataBind();
                
                SetupHyperLink(ClusterHyperLink, storageArray);
            }
            else
            {
                // Regular array
                VServerTableRow.Visible = false;
                ClusterTableRow.Visible = false;

                SetupHyperLink(ArrayHyperlink, storageArray);
            }
            WebHelper.SetValueOrHide(VendorStatusCodeLabel, VendorStatusCodeRow, poolEntity.VendorStatusDescription);
            NameLabel.Text = Pool.Name;
            TypeLabel.Text = GetPoolType(poolEntity.Type);

            var lunCount = ServiceLocator.GetInstance<IPoolDAL>().GetLunCountByPoolId(poolId);
            var volumeCount = ServiceLocator.GetInstance<IPoolDAL>().GetNasVolumeCountByPoolId(poolId);
            var poolPerformance = ServiceLocator.GetInstance<IPoolDAL>().GetPoolPerformance(poolId);
            var poolParent = ServiceLocator.GetInstance<IPoolDAL>().GetParentPoolByChildPoolId(poolId);
            LunCountTableRow.Visible = lunCount > 0;
            VolumeCountTableRow.Visible = volumeCount > 0;
            TotalLatencyRow.Visible = poolPerformance.Latency.HasValue;
            ParentPoolRow.Visible = poolParent != null;

            LUNCountLabel.Text = lunCount.ToString(CultureInfo.CurrentUICulture);
            NASVolumeCountLabel.Text = volumeCount.ToString(CultureInfo.CurrentUICulture);

            TotalSizeLabel.Text = WebHelper.FormatCapacityTotal(poolEntity.CapacityUserTotal);
            TotalSubscribedCapacityLabel.Text = WebHelper.FormatCapacityTotal(poolEntity.CapacitySubscribed);
            OverSubscribedCapacityLabel.Text = WebHelper.FormatCapacityTotal(poolEntity.CapacityOversubscribed);
            ProvisionedCapacityLabel.Text = WebHelper.FormatCapacityTotal(poolEntity.CapacityUserUsed);
            //WebHelper.SetValueOrHide(TotalReductionLabel, rowTotalReduction, WebHelper.FormatDataReduction(poolEntity.TotalReduction));
            WebHelper.SetValueOrHide(DataReductionLabel, rowDataReduction, WebHelper.FormatDataReduction(poolEntity.DataReduction));
            TotalLatencyLabel.Text = WebHelper.FormatLatencyTotal(poolPerformance.Latency, NumberOfFractDigits);

            if(poolParent != null)
            {
                PoolsList.DataSource = poolParent;
                PoolsList.DataBind();
            }

            var dal = ServiceLocator.GetInstance<IThresholdDAL>();
            WebHelper.MarkThresholds(ProvisionedCapacityLabel, (float)poolEntity.ProvisionedCapacityPercent, dal.GetThresholdDynamicOrGlobal(ThresholdDataType.PoolProvisionedPercent, poolEntity.PoolID));
            
            WebHelper.SetValueOrHide(RAIDTypeLabel, RAIDTypeRow, poolEntity.RAIDType);

            if (poolEntity.CapacityRunOut.HasValue)
            {
                CapacityProjectedRunout = WebHelper.GetDayDifference(poolEntity.CapacityRunOut.Value);
            }

            LastSync.Text = poolEntity.LastSync.HasValue ? Utils.FormatDateTimeForDisplayingOnWebsite(poolEntity.LastSync.Value.ToLocalTime(), true) : String.Empty;
        }
    }

    private void SetupHyperLink(HyperLink link, StorageArray storageArray)
    {
        link.Text = storageArray.Name;
        link.NavigateUrl = InvariantString.Format("/Orion/View.aspx?NetObject={0}", storageArray.NetObjectID);
    }

    private void SetupPerformanceHyperLink(HyperLink link)
    {
        link.Text = SrmWebContent.PerformanceAnalizer_TextLink;
        link.NavigateUrl = PerformanceStackHelper.LinkToStoragePool(this.Pool.GetIdentifier());
    }

    private string GetPoolType(PoolType storageGroupType)
    {
        string resultType = string.Empty;
        switch (storageGroupType)
        {
            case PoolType.RaidGroup:
                resultType = SrmWebContent.Pools_List_RaidGroup;
                break;
            case PoolType.StoragePool:
                resultType = SrmWebContent.Pools_List_StoragePool;
                break;
            case PoolType.Aggregate:
                resultType = SrmWebContent.Pools_List_AggregatePool;
                break;
        }
        return resultType;
    }

    #endregion
}
