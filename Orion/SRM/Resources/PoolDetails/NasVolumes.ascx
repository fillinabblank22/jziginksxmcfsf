<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NasVolumes.ascx.cs" Inherits="Orion_SRM_Resources_PoolDetails_NasVolumes" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Resources" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include ID="Include3" runat="server" Module="SRM" File="SRM.Common.js" />
<orion:Include ID="include8" runat="server" File="OrionMinReqs.js" />
<orion:Include ID="include9" runat="server" File="OrionCore.js" />
<orion:Include ID="Include10" runat="server" Module="SRM" File="SRM.css" />

<orion:ResourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: '<%= CustomTable.UniqueClientID %>',
                        initialPage: 0,
                        rowsPerPage: <%= this.GetPageSize() %>,
                        allowSort: true,
                        columnSettings: {
                            "Name": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.SRM_StoragePool_NASVolumes_Volume %>'),
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_Icon_Cell srm_word_wrap";
                                }
                            },
                            "TotalSize": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.SRM_StoragePool_NASVolumes_TotalCapacity%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "SRM_NoWrap"; }
                            },
                            "AllocatedTotal": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.SRM_StoragePool_NASVolumes_AllocatedCapacity%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_NoWrap " + 
                                        SW.SRM.Formatters.FormatCellStyle(rowArray[5],
                                        <%=ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.VolumeProvisionedPercent)%>,
                                        <%=ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.VolumeProvisionedPercent)%>);
                                }
                            },
                            "AllocatedPercentage": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.SRM_StoragePool_NASVolumes_ProvisionedCapacityPercentage%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatPercentageColumnValue(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_NoWrap " + 
                                        SW.SRM.Formatters.FormatCellStyle(cellValue,
                                        <%=ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.VolumeProvisionedPercent)%>,
                                        <%=ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.VolumeProvisionedPercent)%>);
                                }
                            },
                            "FileSystemUsed": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.SRM_StoragePool_NASVolumes_FileSystemUsed%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_NoWrap " + SW.SRM.Formatters.FormatCellStyle(rowArray[7],
                                        <%=ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.VolumeFileSystemUsedPercent)%>,
                                        <%=ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.VolumeFileSystemUsedPercent)%>);
                                }
                            },
                            "CapacityRunOut": {
                                header: SW.SRM.Formatters.FormatColumnTitle("<%= SrmWebContent.SRM_StoragePool_NASVolumes_ProjectedFileRunOut %>"),
                                isHtml: true,
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatDaysDiff(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return SW.SRM.Formatters.FormatCapacityRunoutCellStyle(cellValue, <%= WarningThresholdDays %>, <%= CriticalThresholdDays %>);
                                }
                            },        
                            "FileSystemUsedPercentage": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.SRM_StoragePool_NASVolumes_FileSystemUsedPercentage%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatPercentageColumnValue(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_NoWrap " + 
                                        SW.SRM.Formatters.FormatCellStyle(cellValue,
                                        <%=ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.VolumeFileSystemUsedPercent)%>,
                                        <%=ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.VolumeFileSystemUsedPercent)%>);
                                }
                            }
                        },
                        searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                        searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                        onLoad: function(rows, columnsInfo) {
                            var params = {
                                resourceID: <%=this.Resource.ID%>,
                                netObjectFieldName: "_NetObjectID",
                                rows: rows,
                                columnsInfo: columnsInfo,
                                volumeSpaceUsedWarning: <%=ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.VolumeFileSystemUsedPercent)%>,
                                volumeSpaceUsedCritical: <%=ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.VolumeFileSystemUsedPercent)%>,
                                volumeSpacePercentWarning: <%=ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.VolumeFileSystemUsedPercent)%>,
                                volumeSpacePercentCritical: <%=ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.VolumeFileSystemUsedPercent)%>,
                                volumeSpaceUsed: "NasVolumesListFileSystemUsed",
                                volumeSpacePercent: "NasVolumesListFileSystemUsedPercent"
                            }
                            SW.SRM.Common.FillAdditionColumns(params);
                        }
                    });

                $('#OrderBy-<%= CustomTable.UniqueClientID %>').val('[TotalSize] DESC');
                SW.Core.Resources.CustomQuery.refresh("<%=CustomTable.UniqueClientID %>");
            });
            
        </script>
    </Content>
</orion:ResourceWrapper>
