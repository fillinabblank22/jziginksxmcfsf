using System;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SRM_Resources_PoolDetails_PoolCustomProperties : SrmBaseResourceControl
{
    private const string SrmEntityName = SwisEntities.Pools;
    private const string SrmEntityIDFieldName = "PoolID";
    private NetObject Netobject { set; get; }
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.customPropertyList.Resource = this.Resource;

        var poolProvider = GetInterfaceInstance<IPoolProvider>();
        var pool = poolProvider.Pool;

        if (pool != null)
        {
            this.customPropertyList.NetObjectId = pool.PoolEntity.PoolID;
            this.Netobject = pool;
        }

        customPropertyList.CustomPropertyLoader = (netObjectId, displayProperties) =>
        {
            return CustomPropertyHelper.GetCustomPropertiesForObject(netObjectId, displayProperties, SrmEntityName, SrmEntityIDFieldName, Netobject);
        };

        customPropertyList.EditCustomPropertiesLinkGenerator = (netObjectId, redirectUrl) =>
        {
            if (Request.UrlReferrer != null)
            {
                redirectUrl = UrlHelper.ToSafeUrlParameter(Request.UrlReferrer.PathAndQuery);
            }
            return String.Format("/Orion/SRM/Admin/EditProperties.aspx?NetObject={0}&ReturnTo={1}", Netobject.NetObjectID, redirectUrl);
        };
    }

    protected override string DefaultTitle => Resources.SrmWebContent.CustomProperties_Pools;

    public override string EditControlLocation => "/Orion/SRM/Controls/EditResourceControls/EditCustomPropertyList.ascx";

    public override string HelpLinkFragment => "SRMPHResourceCustomPropertiesPools";

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces => new [] { typeof(IPoolProvider) };

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl;
}
