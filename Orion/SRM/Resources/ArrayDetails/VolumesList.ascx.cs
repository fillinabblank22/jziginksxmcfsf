﻿using System;
using System.Collections.Generic;

using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Web.UI.Models.Volumes;
using SolarWinds.SRM.Web.UI.Models;

public partial class Orion_SRM_Resources_ArrayDetails_VolumesList : SrmBaseResourceControl, IResourceIsInternal
{
    #region Properties

    /// <summary>
    /// Gets or sets default title.
    /// </summary>
    protected override string DefaultTitle
    {
        get { return IsThick ? SrmWebContent.ThickVolumesListResourceTitle : SrmWebContent.ThinVolumesListResourceTitle; }        
    }

    /// <summary>
    /// Determinate is pool is thick.
    /// </summary>
    public bool IsThick
    {
        get;
        set;
    }


    /// <summary>
    /// Override help link fragment property.
    /// </summary>
    public override string HelpLinkFragment
    {
        get
        {
            return IsThick ? "SRMPHResourceThickVolumes" : "SRMPHResourceThinVolumes";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
    }

    /// <summary>
    /// Determinate if resource is internal.
    /// </summary>
    public bool IsInternal
    {
        get
        {
            return true;
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new Type[] { typeof(IStorageArrayProvider), typeof(IVServerProvider) };
        }
    }

    /// <summary>
    /// Gets or sets search control resource property.
    /// </summary>
    public ResourceSearchControl SearchControl
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets warning threshold days for projected runout.
    /// </summary>
    public int WarningThresholdDays
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets critical threshold days for projected runout.
    /// </summary>
    public int CriticalThresholdDays
    {
        get;
        set;
    }

    #endregion


    #region Methods

    /// <summary>
    /// OnLoad resource event handler.
    /// </summary>
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        SearchControl = (ResourceSearchControl) LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        Wrapper.HeaderButtons.Controls.Add(SearchControl);

        IsThick = Convert.ToBoolean(Resource.Properties["IsThick"]);

        var pageModel = this.Page as IModelProvider;
        if (pageModel != null)
        {

            IVolumesModel model = (pageModel).PageModelProvider.VolumesModel;
            var query = model.GetQuery(IsThick);

            WarningThresholdDays = (int) SettingsDAL.GetSetting(SRMConstants.WarningThresholdDays).SettingValue;
            CriticalThresholdDays = (int) SettingsDAL.GetSetting(SRMConstants.CriticalThresholdDays).SettingValue;

            CustomTable.UniqueClientID = ScriptFriendlyResourceId;
            CustomTable.SWQL = query;
            CustomTable.SearchSWQL = string.Concat(query, " AND v.Caption LIKE '%${SEARCH_STRING}%'");
        }
        else
            CustomTable.Visible = false;
    }

    #endregion
}
