﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LatencyHistogramChart.ascx.cs" Inherits="Orion_SRM_Resources_ArrayDetails_LatencyHistogramChart" %>
<%@ Register Src="~/Orion/SRM/Controls/Redirector.ascx" TagPrefix="srm" TagName="Redirector" %>

<srm:Redirector ID="Redirector1" runat="server" />
<orion:resourceWrapper ID="Wrapper" runat="server">
    <Content>
        <asp:PlaceHolder ID="WrapperContents" runat="server"/>
    </Content>
</orion:resourceWrapper> 