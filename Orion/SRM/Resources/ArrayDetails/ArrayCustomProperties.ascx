<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ArrayCustomProperties.ascx.cs" Inherits="Orion_SRM_Resources_ArrayDetails_ArrayCustomProperties" %>
<%@ Register TagPrefix="orion" TagName="CustomPropertyList" Src="~/Orion/SRM/Controls/CustomPropertyList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:CustomPropertyList runat="server" ID="customPropertyList" EntityName="Orion.SRM.StorageArrayCustomProperties"/>
    </Content>
</orion:resourceWrapper>