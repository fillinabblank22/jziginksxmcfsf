﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ArrayDetails.ascx.cs" Inherits="Orion_SRM_Resources_DetailsView_ArrayDetails" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.css" />

<orion:ResourceWrapper ID="Wrapper" runat="server">
    <Content>
        <table cellspacing="0" class="srm_details_table">
            <tr>
                <td class="PropertyHeader">
                    <div class="SRM_FirstRowDetailsTable">
                        <%= Resources.SrmWebContent.LinkToPerfStack_Title %>:
                    </div>
                </td>
                <td class="Property SRM_StatusImage">
                    <asp:Image ID="PerformanceAnalyzerImage" CssClass="StatusIcon" runat="server" />
                </td>
                <td class="SRM_Icon_Cell">
                    <asp:HyperLink runat="server" ID="PerformanceHyperLink" Text="Performance Analyzer"/>
                    <br/>  
                    <asp:HyperLink runat="server" ID="ControllersPerfomanceHyperLink" Text="Controllers Analyzer"/>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <div id="statusRow" runat="server"></div>
                </td>
                <td class="Property SRM_StatusImage">
                    <img class="StatusIcon" id="statusImg" runat="server" alt="status" />
                </td>
                <td class="Property">  
                    <asp:Label runat="server" ID="StatusLabel" />  
                </td>
            </tr>
            <asp:Repeater ID="IssuesRepeater" runat="server">
                <HeaderTemplate>
                    <tr>
                        <td class="PropertyHeader">
                            <%= SrmWebContent.ArrayDetails_Issues%>:
                        </td>
                        <td></td>
                        <td>
                </HeaderTemplate>
                <ItemTemplate>
                    <span class="srm-issues-item">
                        <img src="<%# Eval("StatusImg")%>" class="StatusIcon srm-issue-status-icon" alt="status" />
                        <%# Eval("Description")%>
                        <a href="<%# Eval("LinkToDocumentation")%>" class="srm-learn-more-link" rel="noopener noreferrer" target="_blank">» <%= this.issuesRepreaterLinkDescription %></a>
                    </span>
                    <br/>
                </ItemTemplate>
                <FooterTemplate>
                     </td>
                    </tr>
                </FooterTemplate>
            </asp:Repeater>
            <tr>
                <td class="PropertyHeader">
                    <%= SrmWebContent.ArrayDetails_Manufacturer%>:
                </td>
                <td></td>
                <td class="Property statusAndText">
                    <asp:Literal runat="server" ID="ManufacturerLabel" /></td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= SrmWebContent.ArrayDetails_Model%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="ModelLabel" /></td>
            </tr>
            <tr runat="server" id="IpAddressRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.ArrayDetails_IP_Address%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="IpAddressLabel" /></td>
            </tr>
            <tr runat="server" id="DNSRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.ArrayDetails_Dns%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="DNSLabel" /></td>
            </tr>
             <tr runat="server" id="VendorStatusCodeRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.ArrayDetails_VendorStatusCode%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="VendorStatusCodeLabel" /></td>
            </tr>
           <tr runat="server" id="SystemNameRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.ArrayDetails_Name%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="SystemNameLabel" /></td>
            </tr>
            
            <tr runat="server" id="DescriptionRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.ArrayDetails_Description%>:
                </td>
                <td></td>
                <td class="Property statusAndText">
                    <asp:Literal runat="server" ID="DescriptionLabel" /></td>
            </tr>
            <tr runat="server" id="MonitoringTypeRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.ArrayDetails_Monitoring_Type%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="MonitoringTypeLabel" /></td>
            </tr>
            <tr runat="server" id="MonitoringProviderRow">
                <td class="PropertyHeader">
                    <%= SrmWebContent.ArrayDetails_Monitoring_Providers%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="MonitoringProvidersLabel" />
                   
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= SrmWebContent.ArrayDetails_SerialNumber%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="SerialNumberLabel" /></td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= SrmWebContent.ArrayDetails_Firmware%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="FirmwareLabel" /></td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%= SrmWebContent.ArrayDetails_LastSync%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="LastSync" /></td>
            </tr>

            <asp:Repeater ID="ArrayPropertiesRepeater" runat="server" Visible="false">
                <ItemTemplate>
                    <tr>
                        <td class="PropertyHeader">
                            <%# Eval("Property")%>:
                        </td>
                        <td></td>
                        <td class="Property">
                            <%# Eval("Value")%>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>

        </table>
        <script type="text/javascript">
            $('.srm_details_table tr:even').addClass('SRM_AltRow1').removeClass('SRM_AltRow2');
            $('.srm_details_table tr:odd').addClass('SRM_AltRow2').removeClass('SRM_AltRow1');
        </script>
    </Content>
</orion:ResourceWrapper>
