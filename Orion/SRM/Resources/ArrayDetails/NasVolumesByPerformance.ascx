﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NasVolumesByPerformance.ascx.cs" Inherits="Orion_SRM_Resources_NAS_NasVolumesByPerformance" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Resources" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<orion:Include ID="Include1" runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.Common.js" />
<orion:Include ID="Include3" runat="server" Module="SRM" File="SRM.css" />

<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: '<%= CustomTable.UniqueClientID %>',
                        initialPage: 0,
                        rowsPerPage: <%= this.GetPageSize() %>,
                        allowSort: true,
                        columnSettings: {
                            "Name": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.Nas_Volumes_Performance_VolumeName%>'),
                                cellCssClassProvider: function (value, row, cellInfo) { return "SRM_Icon_Cell srm_word_wrap"; }
                            },
                            "PoolName": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.Nas_Volumes_Performance_StoragePool%>'),
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "VolumeListAssociatedPools<%=this.Resource.ID%> SRM_HiddenGridCell srm_word_wrap";
                                }
                            },
                            "IOPSTotal": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.Nas_Volumes_Performance_IopsTotal%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.ParseIntOrUnknown(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return SW.SRM.Formatters.ApplyThresholds(rowArray[8], rowArray[9]);
                                }
                            },
                            "IOLatencyTotal": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.Nas_Volumes_Performance_LatencyTotal%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatLatencyNumberOrUnknown(value, "<%=SrmWebContent.Nas_Volumes_Performance_LatencyTotal_Unit%>");
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_NoWrap " + 
                                        SW.SRM.Formatters.ApplyThresholds(rowArray[10], rowArray[11]);
                                }
                            },
                            "BytesPSTotal": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.Nas_Volumes_Performance_ThroughputTotal%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_NoWrap " + 
                                        SW.SRM.Formatters.ApplyThresholds(rowArray[12], rowArray[13]);
                                }
                            }
                        },
                        searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                        searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                        onLoad: function(rows, columnsInfo) {
                            var params = {
                                resourceID: <%=this.Resource.ID%>,
                                netObjectFieldName:"_NetObjectID",
                                rows:  rows, 
                                columnsInfo: columnsInfo,
                                classesName: {
                                    associatedPools:"VolumeListAssociatedPools"
                                },
                            };
                            SW.SRM.Common.FillAdditionColumns(params);
                        }
                    });
                $('#OrderBy-<%= CustomTable.UniqueClientID %>').val('[IOPSTotal] DESC');
                SW.Core.Resources.CustomQuery.refresh('<%=CustomTable.UniqueClientID %>');
            });
        </script>
    </Content>
</orion:ResourceWrapper>
