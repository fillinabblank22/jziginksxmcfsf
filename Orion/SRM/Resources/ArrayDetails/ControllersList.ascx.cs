using System;
using System.Globalization;
using System.Text;
using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_SRM_Resources_ArrayDetails_ControllersList : ArrayBaseResourceControl
{
    protected override string DefaultTitle => SrmWebContent.ControllersList_Title;

    public override string HelpLinkFragment => "SRMPHResourceControllersOnThisArray";

    public override string EditControlLocation => "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx";

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        this.SetResourceVisibility();

        string query = this.GetControllersListQuery();

        this.CustomTableControllersList.UniqueClientID = this.ScriptFriendlyResourceId;
        this.CustomTableControllersList.SWQL = query;
        this.CustomTableControllersList.SearchSWQL = $"{query} AND ([Name] LIKE '%${{SEARCH_STRING}}%')";

    }

    private string GetControllersListQuery()
    {
        string controllersListQuery = InvariantString.Format(@"SELECT 
                                                Name AS [Name], 
                                                '/Orion/View.aspx?NetObject={0}:' + ToString(StorageControllerID) AS [_LinkFor_Name],
                                                '/Orion/StatusIcon.ashx?entity={2}&status=' + ToString(ISNULL([Status], 0)) +'&size=small' AS [_IconFor_Name],
                                                IOPSDistribution AS [IOPSDistribution],
                                                BytesPSDistribution AS [BytesPSDistribution],
                                                IOLatencyTotal AS [IOLatencyTotal],
                                                IOSizeTotal AS [IOSizeTotal],
                                                c0.IOPSDistributionThreshold.IsLevel1State AS [_IOPSDistributionWariningFlag],
		                                        c0.IOPSDistributionThreshold.IsLevel2State AS [_IOPSDistributionCriticalFlag],
			                                    c0.BytesPSDistributionThreshold.IsLevel1State AS [_ThroughputTotalWariningFlag],
			                                    c0.BytesPSDistributionThreshold.IsLevel2State AS [_ThroughputTotalCriticalFlag],
			                                    c0.IOLatencyTotalThreshold.IsLevel1State AS [_IOLatencyTotalWariningFlag],
			                                    c0.IOLatencyTotalThreshold.IsLevel2State AS [_IOLatencyTotalCriticalFlag],
                                                c0.IOSizeTotalThreshold.IsLevel1State AS [_IOSizeTotalWariningFlag],
			                                    c0.IOSizeTotalThreshold.IsLevel2State AS [_IOSizeTotalCriticalFlag]
	                                            FROM Orion.SRM.StorageControllers (nolock=true) c0
                                                WHERE c0.StorageArrayID = {1}",
                                                StorageController.NetObjectPrefix,
                                                this.Array.Array.StorageArrayId.Value,
                                                SwisEntities.StorageControllers);

        return controllersListQuery;
    }

    private void SetResourceVisibility()
    {
        DeviceGroup deviceGroup = ServiceLocator.GetInstance<IDeviceGroupDAL>().GetDeviceGroupDetailsByArrayId(this.Array.Array.StorageArrayId.Value);
        this.ControllersListResourceWrapper.Visible = this.Array.Array.ControllersPollingFeature == StorageControllerPollingFeature.Enabled;
    }
}