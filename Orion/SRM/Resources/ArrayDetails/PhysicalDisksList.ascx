<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PhysicalDisksList.ascx.cs" Inherits="Orion_SRM_Resources_Summary_PhysicalDisksList" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Resources" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<orion:Include ID="Include1" runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.css" />
<style type="text/css">
    #physicalDisksFilter {
        width: 100%;
        text-align: right;
        padding: 10px 0px 10px 0px;
    }

        #physicalDisksFilter span {
            padding-right: 10px;
            text-transform: capitalize;
        }
</style>

<orion:ResourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div id="physicalDisksFilter" class="allignToRight">
            <span><%= SrmWebContent.PhysicalDisksList_FilterTitle %>:</span>
            <asp:DropDownList runat="server"
                ClientIDMode="Static"
                Width="150"
                AutoPostBack="false"
                ID="filterByStatusDropDownList"
                onChange="applyFilter();" />
        </div>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= CustomTable.UniqueClientID %>,
                        initialPage: 0,
                        rowsPerPage: <%= this.GetPageSize() %>,
                        allowSort: true,
                        columnSettings: {
                            "Name": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.PhysicalDisksList_Disk%>'),
                                cellCssClassProvider: function (value, row, cellInfo) { return "SRM_Icon_Cell srm_word_wrap"; }
                            },
                            "Size": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.PhysicalDisksList_Size%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "SRM_NoWrap"; }
                            },
                            "Type": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.PhysicalDisksList_Type%>')
                            },
                            "StatusDesc": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.PhysicalDisksList_Status%>')
                            }
                        },
                        searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                        searchButtonId: '<%= SearchControl.SearchButtonClientID %>'
                    });
                SW.Core.View.AddOnRefresh(applyFilter, '<%= CustomTable.ClientID %>');
                applyFilter();
            });

            function applyFilter() {
                var swql = "<%= this.Query %>";
                swql = swql.replace('<%=this.FilterPattern %>', $("#filterByStatusDropDownList").val());

                var swqlSearch = "<%= this.SearchQuery %>";
                swqlSearch = swqlSearch.replace('<%=this.FilterPattern %>', $("#filterByStatusDropDownList").val());

                SW.Core.Resources.CustomQuery.setSearchQuery('<%=CustomTable.UniqueClientID %>', swqlSearch);
                SW.Core.Resources.CustomQuery.setQuery('<%=CustomTable.UniqueClientID %>', swql);
                SW.Core.Resources.CustomQuery.refresh('<%=CustomTable.UniqueClientID %>');
            }
        </script>
    </Content>
</orion:ResourceWrapper>
