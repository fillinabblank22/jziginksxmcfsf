﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GettingStartedMonitoring.ascx.cs" Inherits="Orion_SRM_Resources_DetailsView_GettingStartedMonitoring" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Import Namespace="Resources" %>
<orion:Include runat="server" Module="SRM" File="GettingStarted.css" />

<orion:ResourceWrapper ID="Wrapper" runat="server">
 <Content>
        <div class="SRM-GettingStarted">
            <div class="SRM-GettingStarted-InnerTable">
                <table  cellspacing="0">
                    <tr>
                         <td>
                            <span><%=SrmWebContent.ManageUnifiedControl_ConfigureLabel %></span>
                        </td>
                        <td>
                            <div class="SRM-GettingStarted-Button-Width">
                                <button id="btnAddStorageDevice" runat="server" OnServerClick="BtnEditDeviceClick" 
                                class="SRM-GettingStarted-Button sw-btn-primary sw-btn" ToolTip="<%$ Resources: SrmWebContent,  ManageUnifiedControl_ConfigureLabel%>">
                                        <%=SrmWebContent.ManageUnifiedControl_ConfigureButton %>
                                </button>
                             </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span><%=SrmWebContent.ManageUnifiedControl_HintLabel %></span>
                        </td>
                        <td>
                            <div class="SRM-GettingStarted-Button-Width">
                              <button id="btnLearnMore" runat="server" type="button"
                              class="SRM-GettingStarted-Button sw-btn-primary sw-btn" tooltip="<%$ Resources: SrmWebContent, ManageUnifiedControl_HintLabel%>">
                                      <%=SrmWebContent.ManageUnifiedControl_HintButton %>
                              </button>
                            </div>
                        </td>
                    </tr>
               </table>
            </div>
            <div >
                <span class="SRM-GettingStarted-RemoveButton">
                   <asp:Button class="srm-uppercase sw-btn-secondary sw-btn sw-btn-c sw-btn-t" Text="<%$ Resources: SrmWebContent, GettingStarted_Remove_Button%>"  ID="btnRemove" runat="server" ToolTip="<%$ Resources: SrmWebContent, GettingStarted_Remove_Button %>" OnClick="BtnRemoveResourceClick"/>
                </span>
            </div>
        </div>
    </Content>
</orion:ResourceWrapper>
