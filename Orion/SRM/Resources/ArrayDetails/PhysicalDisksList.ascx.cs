using System;
using Resources;

using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using System.Web.UI.WebControls;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using System.Globalization;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_SRM_Resources_Summary_PhysicalDisksList : ArrayBaseResourceControl
{
    private string query;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // Fill Disk-Statuses Dropdown List
            filterByStatusDropDownList.Items.Clear();
            filterByStatusDropDownList.Items.Add(new ListItem(SrmWebContent.PhysicalDisksList_FilterDefault, "StatusDesc OR StatusDesc IS NULL"));
            foreach (object item in ServiceLocator.GetInstance<IPhysicalDisksDAL>().GetStatuses(Array.Array.StorageArrayId.Value))
            {
                filterByStatusDropDownList.Items.Add(new ListItem(item.ToString(), String.Concat("'", GetDiskStatusText((PhysicalDiskStatus)item), "'")));
            }
        }

        string strStatusQuery = "CASE Status ";
        foreach (object item in Enum.GetValues(typeof(PhysicalDiskStatus)))
        {
            strStatusQuery += String.Format(CultureInfo.InvariantCulture, "WHEN {0} THEN '{1}' ", (int)(PhysicalDiskStatus)item, GetDiskStatusText((PhysicalDiskStatus)item) );
        }
        strStatusQuery += String.Format(CultureInfo.InvariantCulture, "ELSE '{0}' END", GetDiskStatusText(PhysicalDiskStatus.Unknown));

        this.query = CreateQuery(strStatusQuery);

        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        Wrapper.HeaderButtons.Controls.Add(SearchControl);
        
        CustomTable.UniqueClientID = ScriptFriendlyResourceId;
        CustomTable.SWQL = query;
        CustomTable.SearchSWQL = SearchSwql;
    }

    /// <summary>
    /// Gets or sets Search Control property.
    /// </summary>
    protected ResourceSearchControl SearchControl { get; set; }

    /// <summary>
    /// Gets additional query for search control.
    /// </summary>
    protected string SearchSwql { get { return string.Concat(query, " AND ([Name] LIKE '%${SEARCH_STRING}%' OR [Type] LIKE '%${SEARCH_STRING}%')"); } }

    protected string FilterPattern
    {
        get { return "[FILTER_PATTERN]"; }
    }

    protected string SearchQuery
    {
        get { return CustomTable.SearchSWQL.Replace(Environment.NewLine, " ").Trim(); }
    }

    protected string Query
    {
        get { return CustomTable.SWQL.Replace(Environment.NewLine, " ").Trim(); }
    }

    protected override string DefaultTitle
    {
        get { return SrmWebContent.PhysicalDisksList_Title; }
    }

    public override string HelpLinkFragment
    {
        get { return "SRMPHResourceDisksOnThisArray"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
    }

    private static string GetDiskStatusText(PhysicalDiskStatus status)
    {
        return SrmStatusLabelHelper.GetStatusText(status);
    }

    private bool IsDiskTypeColumnVisible()
    {
        return ServiceLocator.GetInstance<IResourceVisibilityService>()
            .IsDiskTypeColumnVisibleForArray(Array);
    }

    private string CreateQuery(string strStatusQuery)
    {
        string caseType = String.Format(CultureInfo.InvariantCulture, "(CASE WHEN Type IS NULL OR Type='' THEN '{0}' ELSE Type END) AS [Type],", SrmWebContent.Value_Unknown);
        string queryTemplate = String.Format(CultureInfo.InvariantCulture, @"SELECT [Name], [_IconFor_Name], [Size], {5} [StatusDesc] FROM (
                                            SELECT Name AS [Name], 
                                                '/Orion/StatusIcon.ashx?entity={2}&status=' + ToString(ISNULL(Status, 0)) +'&size=small' AS [_IconFor_Name],
                                                Capacity AS [Size], 
                                                {3}
                                                ({4}) as [StatusDesc]
                                            FROM Orion.SRM.PhysicalDisks (nolock=true)
                                            WHERE StorageArrayID = {0}
                                         )
                                         WHERE (StatusDesc = {1})",
            Array.Array.StorageArrayId.Value,
            FilterPattern,
            SwisEntities.PhysicalDisks,
            IsDiskTypeColumnVisible() ? caseType : String.Empty,
            strStatusQuery,
            IsDiskTypeColumnVisible() ? "[Type]," : String.Empty);

        return queryTemplate;
    }
}