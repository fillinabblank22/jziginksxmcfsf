﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ArrayPollingDetails.ascx.cs" Inherits="Orion_SRM_Resources_ArrayDetails_ArrayPollingDetails" %>
<%@ Import Namespace="Resources" %>
<orion:Include ID="Include1" runat="server" Module="SRM" File="SRM.css" />

<orion:ResourceWrapper ID="Wrapper" runat="server">
    <Content>
        <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%" runat="server" id="pollingDetailsTable">
        	<tr>
	          <td class="PropertyHeader"><%= SrmWebContent.ArrayPollingDetails_CapacityInterval %></td>
	          <td class="Property"><asp:Label runat="server" ID="capacityIntervalLbl" />&nbsp;</td>
	        </tr>
            <tr>
	          <td class="PropertyHeader"><%= SrmWebContent.ArrayPollingDetails_LastCapacityPollTime %></td>
	          <td class="Property"><asp:Label runat="server" ID="lastCapacityPollTimeLbl" />&nbsp;</td>
	        </tr>
            <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
            <tr>
	          <td class="PropertyHeader"><%= SrmWebContent.ArrayPollingDetails_PerformanceInterval %></td>
	          <td class="Property"><asp:Label runat="server" ID="performanceIntervalLbl" />&nbsp;</td>
	        </tr>
            <tr>
	          <td class="PropertyHeader"><%= SrmWebContent.ArrayPollingDetails_LastPerformancePollTime %></td>
	          <td class="Property"><asp:Label runat="server" ID="lastPerformancePollTimeLbl" />&nbsp;</td>
	        </tr>
            <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
            <tr>
	          <td class="PropertyHeader"><%= SrmWebContent.ArrayPollingDetails_TopologyInterval %></td>
	          <td class="Property"><asp:Label runat="server" ID="topologyIntervalLbl" />&nbsp;</td>
	        </tr>
            <tr>
	          <td class="PropertyHeader"><%= SrmWebContent.ArrayPollingDetails_LastTopologyPollTime %></td>
	          <td class="Property"><asp:Label runat="server" ID="lastTopologyPollTimeLbl" />&nbsp;</td>
	        </tr>
            <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
            <tr runat="server" ID="ControllerIntervalRow">
                <td class="PropertyHeader"><%= SrmWebContent.ArrayPollingDetails_ControllerInterval %></td>
                <td class="Property"><asp:Label runat="server" ID="controllerIntervalLbl" />&nbsp;</td>
            </tr>
            <tr runat="server" ID="LastControllerPollTimeRow">
                <td class="PropertyHeader"><%= SrmWebContent.ArrayPollingDetails_LastControllerPollTime %></td>
                <td class="Property"><asp:Label runat="server" ID="lastControllerPollTimeLbl" />&nbsp;</td>
            </tr>
            <tr runat="server" ID="ControllerEmptyRow"><td>&nbsp;</td><td>&nbsp;</td></tr>
            <tr>
	          <td class="PropertyHeader"><%= SrmWebContent.ArrayPollingDetails_IsArrayLicensed%></td>
	          <td class="Property"><asp:Label runat="server" ID="isArrayLicensedLbl" />&nbsp;</td>
	        </tr>
        </table>
    </Content>
</orion:ResourceWrapper>