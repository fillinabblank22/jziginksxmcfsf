<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ControllersList.ascx.cs" Inherits="Orion_SRM_Resources_ArrayDetails_ControllersList" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Resources" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="ResourceSearchControl" Src="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<orion:Include ID="Include1" runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.css" />

<orion:ResourceWrapper runat="server" ID="ControllersListResourceWrapper">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTableControllersList" />
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= CustomTableControllersList.UniqueClientID %>,
                        initialPage: 0,
                        rowsPerPage: <%= this.GetPageSize() %>,
                        allowSort: true,
                        columnSettings: {
                            "Name": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.ControllersList_Controller%>'),
                                cellCssClassProvider: function (value, row, cellInfo) { return "SRM_Icon_Cell srm_word_wrap"; }
                            },
                            "IOPSDistribution": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.ControllersList_IOPSDistribution%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatPercentageColumnValue(value);
                                },
                                cellCssClassProvider: function(cellValue, rowArray, cellInfo) {
                                     return SW.SRM.Formatters.ApplyThresholds(rowArray[7], rowArray[8]);
                                }
                            },
                            "BytesPSDistribution": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.ControllersList_BytesPSDistribution%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatPercentageColumnValue(value);
                                },
                                cellCssClassProvider: function(cellValue, rowArray, cellInfo) {
                                     return SW.SRM.Formatters.ApplyThresholds(rowArray[9], rowArray[10]);
                                }
                            },
                            "IOLatencyTotal": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.ControllersList_IOLatencyTotal%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatLatencyNumberOrUnknown(value, "<%=SrmWebContent.ControllersList_LatencyTotal_Unit%>");
                                },
                                cellCssClassProvider: function(cellValue, rowArray, cellInfo) {
                                     return SW.SRM.Formatters.ApplyThresholds(rowArray[11], rowArray[12]);
                                }
                            },
                            "IOSizeTotal": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.ControllersList_IOSizeTotal%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function(cellValue, rowArray, cellInfo) {
                                     return SW.SRM.Formatters.ApplyThresholds(rowArray[13], rowArray[14]);
                                }
                            },
                        },
                        searchTextBoxId: '<%= ControllersListSearchControl.SearchBoxClientID %>',
                        searchButtonId: '<%= ControllersListSearchControl.SearchButtonClientID %>'
                    });
                SW.Core.Resources.CustomQuery.refresh('<%=CustomTableControllersList.UniqueClientID %>');
            });
        </script>
    </Content>
    <HeaderButtons>
        <orion:ResourceSearchControl runat="server" ID="ControllersListSearchControl"/>
    </HeaderButtons>
</orion:ResourceWrapper>