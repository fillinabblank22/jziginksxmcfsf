﻿using System;
using System.Net.Http.Formatting;
using System.Text;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;


[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_SRM_Resources_DetailsView_GettingStartedMonitoring : ArrayBaseResourceControl
{

    protected override string DefaultTitle
    {
        get { return SrmWebContent.ManageUnifiedControl_Title; }
    }

    protected void BtnEditDeviceClick(object sender, EventArgs e)
    {
        Page.Response.Redirect(InvariantString.Format("/Orion/SRM/Admin/EditProperties.aspx?NetObject={0}&ReturnTo={1}", Array.NetObjectID, Convert.ToBase64String(Encoding.ASCII.GetBytes(Request.Url.AbsoluteUri))));
    }
   
    protected override void OnLoad(EventArgs e)
    {
        Wrapper.ShowEditButton = false;
        Wrapper.Visible = false;
        DeviceGroup deviceGroup = ServiceLocator.GetInstance<IDeviceGroupDAL>().GetDeviceGroupDetailsByArrayId(Array.Array.StorageArrayId.Value);
        btnLearnMore.Attributes.Add("onclick", InvariantString.Format("window.open('{0}')", HelpHelper.GetHelpUrl("SRMPHResourceUnifiedEnroll")));
        if (Array.Array.Type == ArrayType.Block && deviceGroup.GroupId == new Guid("A7CFE11E-CA36-4F38-BAB4-D72248A8AE66"))
        {
            Wrapper.Visible = true;
        }
    }

    protected void BtnRemoveResourceClick(object sender, EventArgs e)
    {
        ResourceManager.DeleteById(Resource.ID);
        Page.Response.Redirect(Request.Url.ToString());
    }
}