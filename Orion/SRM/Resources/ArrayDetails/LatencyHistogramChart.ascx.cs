﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Common.Helpers;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Common.ServiceProvider;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_SRM_Resources_ArrayDetails_LatencyHistogramChart : SrmLineChart
{
    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get { return StorageArray.NetObjectPrefix; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IStorageArrayProvider), typeof(IVServerProvider) }; }
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get { return new[] { typeof(IMultiStorageArrayProvider) }; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditLatencyHistogram.ascx"; }
    }

    public override string EditURL
    {
        get
        {
            return ExtendCustomEditUrl("/Orion/NetPerfMon/Resources/EditResource.aspx");
        }
    }

    public override string HelpLinkFragment
    {
        get { return Visible ? "SRMPHResourceLatencyHistogram" : String.Empty; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Visible = IsResourceVisible();
        if (NetObjectPrefix == StorageArray.NetObjectPrefix)
        {
            int? storageArrayId = GetParrentArrayId();
            if (!storageArrayId.HasValue)
            {
                throw new InvalidOperationException("Cannot retrieve parent Storage Array");
            }

            var deviceGroup = ServiceLocator.GetInstance<IDeviceGroupDAL>().GetDeviceGroupDetailsByArrayId(storageArrayId.Value);

            string hideLatencyHistogram;

            var asBlockString = Resource.Properties["AsBlock"];
            bool asBlock = String.IsNullOrEmpty(asBlockString) ? true : Boolean.Parse(asBlockString);

            if (deviceGroup.Properties.TryGetValue(asBlock ? "HideLatencyHistogramBlock" : "HideLatencyHistogramFile", out hideLatencyHistogram))
            {
                bool isHidden;
                if (!bool.TryParse(hideLatencyHistogram, out isHidden))
                {
                    isHidden = false;
                }
                Visible &= !isHidden;
            }
        }
        if (Visible)
        {
            HandleInit(WrapperContents);
            OrionInclude.ModuleFile("SRM", "Charts/Charts.SRM.Options.js", OrionInclude.Section.Middle);
        }
    }

    private int? GetParrentArrayId()
    {
        var storageArrayProvider = GetInterfaceInstance<IStorageArrayProvider>();
        if (storageArrayProvider != null)
        {
            return storageArrayProvider.Array.Array.StorageArrayId.Value;
        }

        var virtualServer = GetInterfaceInstance<IVServerProvider>();
        if (virtualServer != null)
        {
            return virtualServer.VServer.StorageArray.Array.StorageArrayId.Value;
        }

        throw new InvalidOperationException(StringHelper.FormatInvariant("Parrent container for the resouce '{0}' not found.", GetType().Name));
    }

    private bool IsResourceVisible()
    {
        IStorageArrayProvider storageArrayProvider = GetInterfaceInstance<IStorageArrayProvider>();
        if (storageArrayProvider != null)
        {
            return ServiceLocator.GetInstance<IResourceVisibilityService>()
                .IsResourceVisibleForArray(storageArrayProvider.Array, this.AppRelativeVirtualPath);
        }

        IVServerProvider vserverProvider = GetInterfaceInstance<IVServerProvider>();
        if (vserverProvider != null)
        {
            return ServiceLocator.GetInstance<IResourceVisibilityService>()
                .IsResourceVisibleForArray(vserverProvider.VServer.StorageArray, this.AppRelativeVirtualPath);
        }

        return true;
    }

}