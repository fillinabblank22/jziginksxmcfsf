﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ArrayNasNfsExport.ascx.cs" Inherits="Orion.SRM.Resources.ArrayDetails.Orion_SRM_Resources_ArrayDetails_ArrayNasNfsExport" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register Src="~/Orion/SRM/Controls/FileSharesList.ascx" TagPrefix="orion" TagName="FileSharesList" %>

<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:FileSharesList runat="server" ID="FileSharesList" />
    </Content>
</orion:resourceWrapper>
