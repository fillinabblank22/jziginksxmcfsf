﻿using System;
using System.Collections.Generic;

using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Web.UI.Models.Luns;
using SolarWinds.SRM.Web.UI.Models;

public partial class Orion_SRM_Resources_ArrayDetails_LunsList : SrmBaseResourceControl, IResourceIsInternal
{
    #region Properties

    /// <summary>
    /// Override default title property.
    /// </summary>
    protected override string DefaultTitle
    {
        get
        {
            return IsThick ? SrmWebContent.ArrayDetails_Thick_Luns_Title : SrmWebContent.ArrayDetails_Thin_Luns_Title;
        }
    }

    /// <summary>
    /// Override help link fragment property.
    /// </summary>
    public override string HelpLinkFragment
    {
        get
        {
            return IsThick ? "SRMPHResourceThickLUNs" : "SRMPHResourceThinsLUNs";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
    }

    /// <summary>
    /// Determinate if resource is internal.
    /// </summary>
    public bool IsInternal
    {
        get
        {
            return true;
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new Type[] { typeof(IStorageArrayProvider), typeof(IVServerProvider) };
        }
    }

    /// <summary>
    /// Gets or sets Search Control property.
    /// </summary>
    public ResourceSearchControl SearchControl
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets is thick property.
    /// </summary>
    public bool IsThick
    {
        get;
        set;
    }

    public string ParentColumnName
    {
        get
        {
            return Resource.Properties["ParentColumnName"] ?? "PoolName";
        }
    }

    #endregion


    #region Methods

    /// <summary>
    /// OnLoad override method.
    /// </summary>
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        SearchControl = (ResourceSearchControl) LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        Wrapper.HeaderButtons.Controls.Add(SearchControl);

        IsThick = Convert.ToBoolean(Resource.Properties["IsThick"]);
        
        var pageModel = this.Page as IModelProvider;
        if (pageModel != null)
        {
            ILunsModel model = (pageModel).PageModelProvider.LunsModel;
            var query = model.GetQuery(IsThick)
                .Replace(ArrayLunsModel.PoolVolumeColumnNamePlaceholder, ParentColumnName);

            CustomTable.UniqueClientID = ScriptFriendlyResourceId;
            CustomTable.SWQL = query;
            CustomTable.SearchSWQL = string.Concat(query, " AND l.Caption LIKE '%${SEARCH_STRING}%'");
        }
        else
            CustomTable.Visible = false;
    }

    #endregion

}