﻿using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.UI;
using System;

namespace Orion.SRM.Resources.ArrayDetails
{
    [ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
    [ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
    public partial class Orion_SRM_Resources_ArrayDetails_ArrayPerformanceSummary : ArrayBaseResourceControl
    {
        protected override string DefaultTitle
        {
            get { return SrmWebContent.Array_Performance_Summary_Title; }
        }

        public override string HelpLinkFragment
        {
            get { return "SRMPHResourcePerformanceSummary"; }
        }

        public override string EditURL
        {
            get
            {
                return ExtendCustomEditUrl("/Orion/SRM/EditTopXXChartResource.aspx?EditXX=False");
            }
        }

        public override ResourceLoadingMode ResourceLoadingMode
        {
            get { return ResourceLoadingMode.Ajax; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            PerformanceSummary.DataBindMethod = "GetArrayPerformanceSummaryData";
            PerformanceSummary.ResourceID = Resource.ID;
            PerformanceSummary.ResourceWidth = Resource.Width;

            // panels of chart
            PerformanceSummary.IOPSEnable = true;
            PerformanceSummary.ThroughputEnable = true;
            PerformanceSummary.IOSizeEnable = true;
            PerformanceSummary.CacheHitRatioEnable = true;
            PerformanceSummary.IOPSRatioEnable = true;

            PerformanceSummary.CustomIOPSResourceTitle = SrmWebContent.PerformanceDrillDown_ArrayIOPSResourceTitle;
            PerformanceSummary.CustomIOPSChartTitle = SrmWebContent.PerformanceDrillDown_ArrayIOPSChartTitle;
            PerformanceSummary.CustomThroughputResourceTitle = SrmWebContent.PerformanceDrillDown_ArrayThroughputResourceTitle;
            PerformanceSummary.CustomThroughputChartTitle = SrmWebContent.PerformanceDrillDown_ArrayThroughputChartTitle;
            PerformanceSummary.CustomIOSizeResourceTitle = SrmWebContent.PerformanceDrillDown_ArrayIOSizeResourceTitle;
            PerformanceSummary.CustomIOSizeChartTitle = SrmWebContent.PerformanceDrillDown_ArrayIOSizeChartTitle;
            PerformanceSummary.CustomCacheHitRatioResourceTitle = SrmWebContent.PerformanceDrillDown_ArrayCacheHitRatioResourceTitle;
            PerformanceSummary.CustomCacheHitRatioChartTitle = SrmWebContent.PerformanceDrillDown_ArrayCacheHitRatioChartTitle;
            PerformanceSummary.CustomIOPSRatioResourceTitle = SrmWebContent.PerformanceDrillDown_ArrayIOPSRatioResourceTitle;
            PerformanceSummary.CustomIOPSRatioChartTitle = SrmWebContent.PerformanceDrillDown_ArrayIOPSRatioChartTitle;

            PerformanceSummary.SampleSizeInMinutes = SampleSizeInMinutes;
            PerformanceSummary.DaysOfDataToLoad = DaysOfDataToLoad;
            PerformanceSummary.InitialZoom = InitialZoom;

            PerformanceSummary.ChartTitleProperty = Resource.Properties["ChartTitle"];
            PerformanceSummary.ChartSubTitleProperty = Resource.Properties["ChartSubTitle"];
        }

        protected override bool IsResourceVisible()
        {
            return ServiceLocator.GetInstance<IResourceVisibilityService>()
                .IsResourceVisibleForArray(Array, this.AppRelativeVirtualPath);
        }
    }
}