﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Resources;

using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Constant;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Web.Resources;
using SolarWinds.SRM.Web.Plugins;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SRM_Resources_DetailsView_ArrayDetails : ArrayBaseResourceControl
{
    public string issuesRepreaterLinkDescription = SrmWebContent.ArrayDetails_LearnMoreLink;
    protected override string DefaultTitle
    {
        get
        {
            return IsCluster 
                ? SrmWebContent.ClusterDetails_DefaultPageTitle
                : SrmWebContent.ArrayDetails_DefaultPageTitle;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceArrayDetails";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.RenderControl;
        }
    }

    protected StorageArrayStatus ArrayStatus { get; set; }
    
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            Wrapper.HeaderButtons.Controls.Add(LoadControl("~/Orion/SRM/Controls/ManageNetObjectButton.ascx"));
        if (this.Array == null)
        {
            this.StatusLabel.Text = SrmWebContent.ArrayDetails_ArrayStatusFail;
        }
        else
        {
            ArrayEntity arrayEntity = this.Array.Array;
            List<ProviderEntity> providers = ServiceLocator.GetInstance<IStorageProviderDAL>()
                .GetAllProvidersByArrayId(arrayEntity.StorageArrayId);

            this.ArrayStatus = arrayEntity.Status;

            // update status text and icon
            this.UpdateStatusLabelAndIcon();

            this.UpdateUIElements(arrayEntity, providers);

            // Array Issues row
            List<ArrayIssueDescription> issues = ArrayIssuesSolver.Get(arrayEntity.StorageArrayId.Value);
            if (arrayEntity.TemplateId == new Guid(SRMConstants.DellCompelentSmisTemplateID))
            {
                string returnLink =
                    Convert.ToBase64String(Encoding.ASCII.GetBytes(this.Request.UrlReferrer.AbsoluteUri));
                issues.Add(DellCompelentHwhIsNotEnabled(providers, returnLink));
                this.issuesRepreaterLinkDescription = "Edit provider";
            }

            if (issues.Count > 0)
            {
                this.IssuesRepeater.DataSource = issues;
                this.IssuesRepeater.DataBind();
            }
            else
            {
                this.IssuesRepeater.Visible = false;
            }

            // show array properties
            var arrayProperties = ServiceLocator.GetInstance<IStorageArrayPropertiesDAL>()
                .GetProperties(arrayEntity.StorageArrayId.Value)
                .Where(p => p.Item1.StartsWith(ArrayProperties.ArrayDetailsPreffix)).ToList();
            if (arrayProperties.Count > 0)
            {
                // removes array details preffix and replaces underscores in property name with spaces, before showing in resource
                this.ArrayPropertiesRepeater.DataSource = arrayProperties.Select(p => new
                {
                    Property = p.Item1.Replace(ArrayProperties.ArrayDetailsPreffix, String.Empty).Replace("_", " "),
                    Value = p.Item2
                });
                this.ArrayPropertiesRepeater.DataBind();
                this.ArrayPropertiesRepeater.Visible = true;
            }

            this.LastSync.Text = arrayEntity.LastSync.HasValue
                ? Utils.FormatDateTimeForDisplayingOnWebsite(arrayEntity.LastSync.Value.ToLocalTime(), true)
                : String.Empty;

            this.PerformanceAnalyzerImage.ImageUrl = PerformanceStackHelper.PerformanceAnalyzerImage;
            this.SetupPerformanceHyperLink(this.PerformanceHyperLink);

            if (arrayEntity.ControllersPollingFeature == StorageControllerPollingFeature.Enabled)
            {
                this.EnableStorageControllerPerfStackLink(arrayEntity.StorageArrayId.Value);
            }
            else
            {
                this.ControllersPerfomanceHyperLink.Visible = false;
            }
        }
    }

    private void UpdateUIElements(ArrayEntity arrayEntity, List<ProviderEntity> providers)
    {
        IpAddressLabel.Text =
            ServiceLocator.GetInstance<IStorageArrayIPDAL>()
                .GetAggregateArrayIPDataByArrayID(arrayEntity.StorageArrayId.Value)
                .IPAddress;
        IpAddressRow.Visible = !String.IsNullOrEmpty(IpAddressLabel.Text);

        this.StatusLabel.Text = String.Format(CultureInfo.CurrentCulture, "{0} {1}", arrayEntity.DisplayName,
            SrmStatusLabelHelper.GetStatusLabel(arrayEntity.Status));
        this.ManufacturerLabel.Text = arrayEntity.Manufacturer;
        this.ModelLabel.Text = arrayEntity.Model;
        WebHelper.SetValueOrHide(this.VendorStatusCodeLabel, this.VendorStatusCodeRow, arrayEntity.VendorStatusDescription);
        StringBuilder stringSystemName = new StringBuilder();
        StringBuilder stringDescription = new StringBuilder();
        StringBuilder stringDNS = new StringBuilder();
        var nodesID =
            ServiceLocator.GetInstance<ITopologyDAL>().GetNodeIdByStorageArrayIDs(arrayEntity.StorageArrayId.Value);
        if (nodesID.Any())
        {
            foreach (var nodeID in nodesID)
            {
                Node node = new Node(NetObjectHelper.GetNetObjectForNode(nodeID));
                stringSystemName.Append(node.SysName);
                stringDescription.Append(node.Description);
                stringDNS.Append(node.DNSName);
                stringSystemName.Append(", ");
                stringDescription.Append(", ");
                stringDNS.Append(", ");
            }

            stringSystemName.Remove(stringSystemName.Length - 2, 2);
            stringDescription.Remove(stringDescription.Length - 2, 2);
            stringDNS.Remove(stringDNS.Length - 2, 2);
            this.SystemNameLabel.Text = stringSystemName.ToString();
            this.DescriptionLabel.Text = stringDescription.ToString();
            this.DNSLabel.Text = stringDNS.ToString();
        }
        else
        {
            this.SystemNameRow.Visible = false;
            this.DescriptionRow.Visible = false;
            this.DNSRow.Visible = false;
        }

        this.MonitoringTypeLabel.Text = this.GetMonitoringType(arrayEntity.FlowType, providers);
        this.MonitoringTypeRow.Visible = !String.IsNullOrWhiteSpace(this.MonitoringTypeLabel.Text);
        this.MonitoringProvidersLabel.Text = ProviderHelper.GetProvidersRepresentation(providers);
        this.SerialNumberLabel.Text = arrayEntity.SerialNumber;
        this.FirmwareLabel.Text = arrayEntity.Firmware;
    }

    private void EnableStorageControllerPerfStackLink(int arrayID)
    {
            var listOfControllerIDs = ServiceLocator.GetInstance<IStorageControllerDAL>().GetStorageControllerIdsForArray(arrayID).ToList();

            this.ControllersPerfomanceHyperLink.NavigateUrl =
                PerformanceStackHelper.LinkToStorageControllerComparison(arrayID, listOfControllerIDs);
    }

    private static ArrayIssueDescription DellCompelentHwhIsNotEnabled(List<ProviderEntity> providers, string returnLink)
   {
       string netObjId = (providers.FirstOrDefault(x => x.CredentialType == CredentialType.SMIS)).ID.ToString();
       string helpLink = "/Orion/SRM/Admin/EditProperties.aspx?NetObject=SMP:" + netObjId+"&ReturnTo="+returnLink;
        
        return new ArrayIssueDescription()
        {
            Description = SrmWebContent.ArrayDetail_DellCompelentHwhIsNotenabled,
            LinkToDocumentation = helpLink,
            StatusImg = "/Orion/images/warning_16x16.gif"
        };
    }

    private string GetMonitoringType(FlowType flowType, IEnumerable<ProviderEntity> providers)
    {
        var localizedDescription = flowType.GetLocalizedDescription();

        if (flowType != FlowType.Smis)
        {
            return localizedDescription;
        }

        return string.Format(CultureInfo.CurrentCulture, 
            localizedDescription, 
            providers == null || !providers.Any() || providers.Any(p => p.ProviderLocation == ProviderLocation.External)
                    ? ProviderLocation.External.GetLocalizedDescription()
                    : providers.First().ProviderLocation.GetLocalizedDescription());
    }

    private void UpdateStatusLabelAndIcon()
    {
        statusRow.InnerHtml = IsCluster
                                  ? SrmWebContent.ClusterDetails_ArrayStatus
                                  : SrmWebContent.ArrayDetails_ArrayStatus;
        
        statusImg.Src = InvariantString.Format("/Orion/StatusIcon.ashx?entity={0}&status={1}&size=large",
                                        IsCluster ? SwisEntities.Clusters : SwisEntities.StorageArrays,
                                        (int)ArrayStatus);
    }

    private void SetupPerformanceHyperLink(HyperLink link)
    {
        link.Text = SrmWebContent.PerformanceAnalizer_TextLink;
        link.NavigateUrl = PerformanceStackHelper.LinkToStorageArray(this.Array.GetIdentifier());
    }

}