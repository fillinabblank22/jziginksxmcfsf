﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FreeLunsList.ascx.cs" Inherits="Orion_SRM_Resources_Summary_FreeLunsList" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Resources" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<orion:Include ID="Include1" runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include ID="Include3" runat="server" Module="SRM" File="SRM.Common.js" />
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.css" />

<orion:resourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: '<%= CustomTable.UniqueClientID %>',
                        initialPage: 0,
                        rowsPerPage: <%= this.GetPageSize() %>,
                        allowSort: true,
                        columnSettings: {
                            "Name": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.Free_Luns_LunName%>'),
                                cellCssClassProvider: function (value, row, cellInfo) { return "SRM_Icon_Cell srm_word_wrap"; }
                            },
                            "PoolName": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.Free_Luns_StoragePool%>'),
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "LunListAssociatedPools<%=this.Resource.ID%> SRM_HiddenGridCell srm_word_wrap";
                                }
                            },
                            "VolumeName" : {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.VServerDetails_Luns_NasVolumes%>'),
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "LunListAssociatedVolumes<%=this.Resource.ID%> SRM_HiddenGridCell srm_word_wrap"; 
                                }
                            },
                            "TotalUserCapacity": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.Free_Luns_TotalUserCapacity%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "SRM_NoWrap"; }
                            }
                        },
                        searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                        searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                        onLoad: function(rows, columnsInfo) {
                            var params = {
                                resourceID: <%=this.Resource.ID%>,
                                netObjectFieldName:"_NetObjectID",
                                rows:  rows, 
                                columnsInfo: columnsInfo,
                                classesName: {
                                    associatedPools:"LunListAssociatedPools",
                                    associatedVolumes:"LunListAssociatedVolumes"
                                },
                            };
                            SW.SRM.Common.FillAdditionColumns(params);
                        }
                    });
                SW.Core.Resources.CustomQuery.refresh('<%=CustomTable.UniqueClientID %>');
            });
        </script>
    </Content>
</orion:resourceWrapper>
