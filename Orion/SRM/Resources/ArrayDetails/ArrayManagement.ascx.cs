﻿using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Web.Plugins;
using SolarWinds.SRM.Web.UI;
using System;
using System.Collections.Generic;
using System.Linq;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_SRM_Resources_ArrayDetails_ArrayManagement : ArrayBaseResourceControl
{
    public bool Unmanaged
    {
        get
        {
            return Array.Array.Unmanaged; 
        }
    }

    protected override string DefaultTitle
    {
        get
        {
            return IsCluster
                ? SrmWebContent.ClusterManagement_DefaultPageTitle
                : SrmWebContent.ArrayManagement_DefaultPageTitle;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceArrayManagement";
        }
    }

    public string LinkToPerformanceStack
    {
        get
        {
            return PerformanceStackHelper.LinkToStorageArray(this.Array.GetIdentifier());
        }
    }
}