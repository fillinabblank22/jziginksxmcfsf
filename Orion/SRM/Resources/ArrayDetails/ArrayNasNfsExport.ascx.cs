﻿using System;
using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Web.UI.Models.Volumes;
using SolarWinds.SRM.Web.UI.Models;
using SolarWinds.SRM.Web.UI.Models.FileShares;

namespace Orion.SRM.Resources.ArrayDetails
{
    [ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
    public partial class Orion_SRM_Resources_ArrayDetails_ArrayNasNfsExport : ArrayBaseResourceControl
    {
        protected override string DefaultTitle
        {
            get { return SrmWebContent.ArrayNasNfsExport_Resource_Title; }
        }

        public override string HelpLinkFragment
        {
            get
            {
                return "SRMPHResourceExports";
            }
        }

        public override string EditControlLocation
        {
            get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            FileSharesList.ParentResourceId = ScriptFriendlyResourceId;
            var pageModel = this.Page as IModelProvider;
            if (pageModel != null)
            {
                IFileSharesModel model = (pageModel).PageModelProvider.FileShareModel;
                var query = model.GetFilterQuery((int) FileSharesType.NFS);
                FileSharesList.Where = query;
                ResourceSearchControl searchControl =
                    (ResourceSearchControl) LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
                Wrapper.HeaderButtons.Controls.Add(searchControl);
                FileSharesList.SearchControl = searchControl;
                FileSharesList.ResourceID = this.Resource.ID;
            }
            else
                FileSharesList.Visible = false;
        }
    }
}
