﻿using System;
using Resources;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.Resources;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Web.UI.Models;
using SolarWinds.SRM.Web.UI.Models.NasVolumesByPerformance;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using System.Collections.Generic;

public partial class Orion_SRM_Resources_NAS_NasVolumesByPerformance : ArrayBaseResourceControl
{
    #region Properties

    /// <summary>
    /// Override default title property.
    /// </summary>
    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.Nas_Volumes_Performance_Title;
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new Type[] { typeof(IStorageArrayProvider), typeof(IVServerProvider) };
        }
    }

    /// <summary>
    /// Override help link fragment property.
    /// </summary>
    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceVolumesPerformanceSummary";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
    }

    /// <summary>
    /// Gets or sets Search Control property.
    /// </summary>
    public ResourceSearchControl SearchControl
    {
        get;
        set;
    }

    #endregion

    /// <summary>
    /// OnLoad override method.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        ResourceWrapper.HeaderButtons.Controls.Add(SearchControl);

        var pageModel = this.Page as IModelProvider;
        if (pageModel != null)
        {
            INasVolumesByPerformanceModel model =
                (pageModel).PageModelProvider.NasVolumesByPerformanceModel;
            CustomTable.UniqueClientID = ScriptFriendlyResourceId;
            CustomTable.SWQL = model.GetQuery();
            CustomTable.SearchSWQL = model.GetSearchQuery();
        }
        else
            CustomTable.Visible = false;
    }

    protected override bool IsResourceVisible()
    {
        if (Array != null)
        {
            return ServiceLocator.GetInstance<IResourceVisibilityService>()
                .IsResourceVisibleForArray(Array, this.AppRelativeVirtualPath);
        }

        IVServerProvider vserverProvider = GetInterfaceInstance<IVServerProvider>();
        if (vserverProvider != null)
        {
            return ServiceLocator.GetInstance<IResourceVisibilityService>()
                .IsResourceVisibleForArray(vserverProvider.VServer.StorageArray, this.AppRelativeVirtualPath);
        }

        return true;

    }
}
