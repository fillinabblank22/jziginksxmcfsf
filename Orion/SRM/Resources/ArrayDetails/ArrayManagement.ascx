﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ArrayManagement.ascx.cs" Inherits="Orion_SRM_Resources_ArrayDetails_ArrayManagement" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Plugins" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="PollRediscoverDialog" Src="~/Orion/Controls/PollRediscoverDialog.ascx" %>
<%@ Register TagPrefix="orion" TagName="UnmanageDialog" Src="~/Orion/SRM/Controls/UnmanageDialog.ascx" %>
<%@ Register TagPrefix="orion" TagName="PollNowDialog" Src="~/Orion/SRM/Controls/PollNowDialog.ascx" %>

<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.css" />
<orion:Include ID="Include1" runat="server" Module="SRM" File="ArrayManagement.css" />

<orion:PollRediscoverDialog runat="server" />
<orion:UnmanageDialog runat="server" />
<orion:PollNowDialog runat="server" />

<orion:ResourceWrapper ID="Wrapper" runat="server">
    <Content>
        <div class="srm-array-management-wrapper">
            <%if (Profile.AllowNodeManagement || Profile.AllowUnmanage) {%>
                <div class="srm-array-management-header srm-uppercase">
                    <% if (!Array.Array.IsCluster)
                       { %>
                    <%= SrmWebContent.ArrayManagement_Array %>
                    <% }
                       else
                       { %>
                    <%= SrmWebContent.ArrayManagement_Cluster %>
                    <% } %>
                </div>
            <%}%>
            <%if (Profile.AllowNodeManagement) {%>
                <a href="#" onclick="return redirectToEditPage(['<%= Array.NetObjectID %>']);" class="srm-array-management-button-edit-icon">
                    <% if (!Array.Array.IsCluster)
                       { %>
                    <%= SrmWebContent.ArrayManagement_Edit_Array %>
                    <% }
                       else
                       { %>
                    <%= SrmWebContent.ArrayManagement_Edit_Cluster %>
                    <% } %>
                </a>
            <% } %>
            
            <%if (Profile.AllowUnmanage) {%>
                <% if (!Unmanaged) { %>
                <a href="#" onclick="return showUnmanageDialog(['<%= Array.Array.StorageArrayId.GetValueOrDefault(0) %>'], '<%= HttpUtility.HtmlEncode(Array.Name) %>');"  class="srm-array-management-button-unmanage-icon">
                    <%= SrmWebContent.ArrayManagement_Unmanage %>
                </a>
                <% } else { %>
                <a href="#" onclick="return remanageStorageArrays(['<%= Array.Array.StorageArrayId.GetValueOrDefault(0) %>']);"  class="srm-array-management-button-remanage-icon">
                    <%= SrmWebContent.ArrayManagement_Remanage %>
                </a>
                <% } %>
            <%}%>
            
            <%if (Profile.AllowNodeManagement) {%>
                <a href="#" onclick="return showDialogPollNow(['<%= Array.NetObjectID %>']);" class="srm-array-management-button-statisticsnow-icon">
                    <%= SrmWebContent.ArrayManagement_PollStatistics_Now %>
                </a>
                <a href="#" onclick="return showDialogPollForJobNow(['<%= Array.NetObjectID %>'], 'Topology');" class="srm-array-management-button-topologynow-icon">
                    <%= SrmWebContent.ArrayManagement_PollTopology_Now %>
                </a>
                <% if (Array.Array.ControllersPollingFeature == StorageControllerPollingFeature.Enabled) { %>
                    <a href="#" onclick="return showDialogPollForJobNow(['<%= Array.NetObjectID %>'], 'Controller');" class="srm-array-management-button-controllersnow-icon">
                        <%= SrmWebContent.ArrayManagement_PollController_Now %>
                    </a>
                <%}%>
                <a href="#" onclick="return showStorageArrayRediscoverNowDialog(['<%= Array.NetObjectID %>']);" class="srm-array-management-button-rediscover-icon">
                    <%= SrmWebContent.ArrayManagement_Rediscover %>
                </a>

                <a href="/Orion/SRM/Admin/ManualE2EMapping/" class="srm-array-management-button-edit-icon">
                    <%= SrmWebContent.ArrayManagement_Map_Server_Volumes %>
                </a>
            <%}%>
            
            <style type="text/css">
                .srm-array-management-button-performance-stack-icon {
                    background-image: url("<%= PerformanceStackHelper.PerformanceAnalyzerImage%>");
                    background-size: 16px 16px;
                }
            </style>

            <a href="<%= this.LinkToPerformanceStack %>" class="srm-array-management-button-performance-stack-icon">
                <%= SrmWebContent.PerformanceAnalizer_TextLink %>
            </a>

        </div>
       
        
        <asp:ScriptManagerProxy runat="server">
	    <Services>
		    <asp:ServiceReference path="~/Orion/SRM/Services/StorageManagementService.asmx" />
	    </Services>
        </asp:ScriptManagerProxy>

        <script type="text/javascript">
            function showStorageArrayRediscoverNowDialog(ids) {
                ProgressDialog({
                    title: "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_7) %>",
                    items: ids,
                    serverMethod: StorageManagementService.RediscoverNetObjNow,
                    getArg: function (id) { return id; },
                    sucessMessage: "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_8) %>",
                    failMessage: "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_9) %>",
                    afterSucceeded: function (id) { }
                });
                return false;
            }

            function redirectToEditPage(ids) {
                window.location = '/Orion/SRM/Admin/EditProperties.aspx?NetObject=' + ids + '&ReturnTo=' +  '<%= Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(Request.Url.AbsoluteUri)) %>';
            }

        </script>

    </Content>
</orion:ResourceWrapper>

