﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ServersOnThisArray.ascx.cs" Inherits="Orion_SRM_Resources_ArrayDetails_ServersOnThisArray" %>
<%@ Register src="../../Controls/AjaxTree.ascx" TagName="AjaxTree" TagPrefix="srm" %>

<srm:AjaxTree ID="AjaxTree" runat="server" ServicePath="/Orion/SRM/Services/ServersOnThisArrayTree.asmx" />
