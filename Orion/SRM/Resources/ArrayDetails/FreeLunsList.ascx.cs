﻿using System;
using System.Collections.Generic;

using Resources;

using SolarWinds.Orion.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Web.UI.Models.FreeLuns;
using SolarWinds.SRM.Web.UI.Models;

public partial class Orion_SRM_Resources_Summary_FreeLunsList : SrmBaseResourceControl, IResourceIsInternal
{
    #region Properties

    /// <summary>
    /// Override default title property.
    /// </summary>
    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.Free_Luns_Title;
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new Type[] { typeof(IStorageArrayProvider), typeof(IVServerProvider) };
        }
    }

    public bool IsInternal
    {
        get
        {
            return true;
        }
    }

    /// <summary>
    /// Override help link fragment property.
    /// </summary>
    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceFreeLUNs";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
    }

    /// <summary>
    /// Gets or sets Search Control property.
    /// </summary>
    public ResourceSearchControl SearchControl
    {
        get;
        set;
    }

    public string ParentColumnName
    {
        get
        {
            return Resource.Properties["ParentColumnName"] ?? "PoolName";
        }
    }

    #endregion

    /// <summary>
    /// OnLoad override method.
    /// </summary>
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        SearchControl = (ResourceSearchControl) LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        ResourceWrapper.HeaderButtons.Controls.Add(SearchControl);
        var pageModel = this.Page as IModelProvider;
        if (pageModel != null)
        {
            IFreeLunsModel model = (pageModel).PageModelProvider.FreeLunsModel;
            var query = model.GetQuery().Replace(ArrayFreeLunsModel.PoolVolumeColumnNamePlaceholder, ParentColumnName);

            CustomTable.UniqueClientID = ScriptFriendlyResourceId;
            CustomTable.SWQL = query;
            CustomTable.SearchSWQL = string.Concat(query, " AND l.[Caption] LIKE '%${SEARCH_STRING}%'");
        }
        else
            CustomTable.Visible = false;
    }
}