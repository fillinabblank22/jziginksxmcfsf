using System;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SRM_Resources_ArrayDetails_ArrayCustomProperties : SrmBaseResourceControl
{
    private const string SrmEntityName = SwisEntities.StorageArrays;
    private const string SrmEntityIDFieldName = "StorageArrayID";
    private NetObject Netobject { set; get; }
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.customPropertyList.Resource = this.Resource;

        var arrayProvider = GetInterfaceInstance<IStorageArrayProvider>();
        var array = arrayProvider.Array;

        if (array != null)
        {
            this.customPropertyList.NetObjectId = array.Array.StorageArrayId.Value;
            this.Netobject = array;
        }

        customPropertyList.CustomPropertyLoader = (netObjectId, displayProperties) =>
        {
            return CustomPropertyHelper.GetCustomPropertiesForObject(netObjectId, displayProperties, SrmEntityName, SrmEntityIDFieldName, Netobject);
        };

        customPropertyList.EditCustomPropertiesLinkGenerator = (netObjectId, redirectUrl) =>
        {
            if (Request.UrlReferrer != null)
            {
                redirectUrl = UrlHelper.ToSafeUrlParameter(Request.UrlReferrer.PathAndQuery);
            }
            return String.Format("/Orion/SRM/Admin/EditProperties.aspx?NetObject={0}&ReturnTo={1}", Netobject.NetObjectID, redirectUrl);
        };
    }

    protected override string DefaultTitle => Resources.SrmWebContent.CustomProperties_StorageArrays;

    public override string EditControlLocation => "/Orion/SRM/Controls/EditResourceControls/EditCustomPropertyList.ascx";

    public override string HelpLinkFragment => "SRMPHResourceCustomPropertiesStorageArrays";

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces => new [] { typeof(IStorageArrayProvider) };

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl;
}
