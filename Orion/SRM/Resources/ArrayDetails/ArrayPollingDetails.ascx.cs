﻿using Resources;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Common.BL;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Web.UI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.SRM.Common.Enums;

public partial class Orion_SRM_Resources_ArrayDetails_ArrayPollingDetails : ArrayBaseResourceControl
{
    private const string criticalCssClass = "SRM_CriticalText";

    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.ArrayPollingDetails_DefaultPageTitle;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceArrayPollingDetails";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.RenderControl;
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (!OrionConfiguration.IsDemoServer)
            Wrapper.HeaderButtons.Controls.Add(LoadControl("~/Orion/SRM/Controls/ManageNetObjectButton.ascx"));

        if (Array != null)
        {
            ArrayEntity array = Array.Array;

            capacityIntervalLbl.Text = string.Format(CultureInfo.CurrentCulture, Resources.CoreWebContent.WEBCODE_VB0_40, array.RediscoveryInterval.ToString(CultureInfo.CurrentCulture));
            performanceIntervalLbl.Text = string.Format(CultureInfo.CurrentCulture, Resources.CoreWebContent.WEBCODE_VB0_40, array.StatCollection.ToString(CultureInfo.CurrentCulture));
            topologyIntervalLbl.Text = string.Format(CultureInfo.CurrentCulture, Resources.CoreWebContent.WEBCODE_VB0_40, array.TopologyInterval.ToString(CultureInfo.CurrentCulture));
            controllerIntervalLbl.Text = string.Format(CultureInfo.CurrentCulture, Resources.CoreWebContent.WEBCODE_VB0_40, array.ControllerInterval.ToString(CultureInfo.CurrentCulture));

            SetLastPollTime(lastCapacityPollTimeLbl, array.LastCapacityPollTime, array.Created, array.RediscoveryInterval);
            SetLastPollTime(lastPerformancePollTimeLbl, array.LastPerformancePollTime, array.Created, array.StatCollection * 2);  // handle needs for two performance polls after restart and also be more forgiving for performance polls that take more than poll interval to finish
            SetLastPollTime(lastTopologyPollTimeLbl, array.LastTopologyPollTime, array.Created, array.TopologyInterval);
            SetLastPollTime(lastControllerPollTimeLbl, array.LastControllerPollTime, array.Created, array.ControllerInterval);

            using (var proxy = BusinessLayerFactory.Create())
            {
                bool isLicensed = false;
                List<string> licensedObjects = proxy.GetLicensedObjects();
                if (licensedObjects != null)
                    isLicensed = licensedObjects.Contains(array.ID);

                isArrayLicensedLbl.Text = isLicensed ? SrmWebContent.ArrayPollingDetails_IsLicensed_Yes : SrmWebContent.ArrayPollingDetails_IsLicensed_No;
                if (isLicensed == false)
                    AddCssClassToLabel(isArrayLicensedLbl, criticalCssClass);
            }

            bool isControllerPollingEnabled = array.ControllersPollingFeature == StorageControllerPollingFeature.Enabled;

            this.ControllerIntervalRow.Visible = isControllerPollingEnabled;
            this.LastControllerPollTimeRow.Visible = isControllerPollingEnabled;
            this.ControllerEmptyRow.Visible = isControllerPollingEnabled;
        }
    }

    private void SetLastPollTime(Label label, DateTime? lastPollTime, DateTime? created, int pollInterval)
    {
        if (lastPollTime.HasValue)
        {
            label.Text = Utils.FormatDateTimeForDisplayingOnWebsite(lastPollTime.Value.ToLocalTime(), true);
            SetStyleForLastPollTime(label, lastPollTime, pollInterval);
        }
        else
        {
            label.Text = SrmWebContent.ArrayPollingDetails_WaitForFirstPoll;
            SetStyleForLastPollTime(label, created, pollInterval);
        }
    }

    private void SetStyleForLastPollTime(Label label, DateTime? lastPollTime, int pollIterval)
    {
        if (lastPollTime.HasValue == false)
            return;

        var current = DateTime.UtcNow;
        var expiration = current - TimeSpan.FromMinutes(pollIterval);
        if (lastPollTime < expiration)
        {
            AddCssClassToLabel(label, criticalCssClass);
        }
    }

    private void AddCssClassToLabel(Label label, string className)
    {
        label.CssClass += string.IsNullOrEmpty(label.CssClass) ? className : "".PadLeft(1) + className;
    }
}