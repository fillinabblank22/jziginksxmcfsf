﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StoragePoolsList.ascx.cs" Inherits="Orion.SRM.Resources.ArrayDetails.Orion_SRM_Resources_ArrayDetails_StoragePoolsList" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Resources" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include ID="Include3" runat="server" Module="SRM" File="SRM.css" />
<orion:Include ID="Include4" runat="server" Module="SRM" File="CapacityRisk.css" />

<orion:ResourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div class="srm-storagepools">
            <orion:CustomQueryTable runat="server" ID="CustomTable" />
        </div>
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: '<%= CustomTable.UniqueClientID %>',
                        initialPage: 0,
                        rowsPerPage: <%= this.GetPageSize() %>,
                        allowSort: true,
                        columnSettings: {
                            "Name": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.ArrayDetails_StoragePools_Name %>'),
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_Icon_Cell srm_word_wrap";
                                }
                            },
                            "Type": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.ArrayDetails_StoragePools_Type %>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatPoolType(value);
                                }
                            },
                            "CapacityUserTotal": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.ArrayDetails_StoragePools_TotalSize %>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "SRM_NoWrap"; }
                            },
                            "CapacityAllocated": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.ArrayDetails_StoragePools_ProvisionedCapacity %>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_NoWrap " + SW.SRM.Formatters.FormatCellStyle(cellValue / rowArray[4] * 100,
                                        <%=ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.PoolProvisionedPercent)%>,
                                        <%=ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.PoolProvisionedPercent)%>);
                                }
                            },
                            "SubscribedCapacity": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.ArrayDetails_StoragePools_SubscribedCapacity %>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "SRM_NoWrap"; }
                            },
                            "CapacityOverSubscribed": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.ArrayDetails_StoragePools_OverSubscribedCapacity %>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "SRM_NoWrap"; }
                            },
                            "ProjectedRunOut": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.ArrayDetails_StoragePools_ProjectedRunOut %>'),
                                isHtml: true,
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatDaysDiff(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return SW.SRM.Formatters.FormatCapacityRunoutCellStyle(cellValue, <%= WarningThresholdDays %>, <%= CriticalThresholdDays %>);
                                }
                            }
                        },
                        searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                        searchButtonId: '<%= SearchControl.SearchButtonClientID %>'
                    });
                SW.Core.Resources.CustomQuery.refresh("<%=CustomTable.UniqueClientID %>");
            });
            
        </script>
    </Content>
</orion:ResourceWrapper>
