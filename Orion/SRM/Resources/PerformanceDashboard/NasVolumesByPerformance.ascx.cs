﻿using System;
using Resources;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.Resources;
using SolarWinds.SRM.Web.UI;

public partial class Orion_SRM_Resources_PerformanceDashboard_NasVolumesByPerformance : SrmBaseResourceControl
{
    #region Fields

    private string query;

    private const string queryTemplate = @"SELECT Caption AS Name, 
                                                 Concat('%{0}:', VolumeID,'%') AS [PoolName],
                                                 '/Orion/StatusIcon.ashx?entity={1}&status=' + ToString(ISNULL(Status, 0)) +'&size=small' AS [_IconFor_Name],    
                                                 DetailsUrl AS [_LinkFor_Name],
                                                 IOPSTotal,
                                                 IOLatencyTotal,
                                                 BytesPSTotal,
                                                 Concat('{0}:', VolumeID) AS [_NetObjectID],
                                                CASE WHEN _IOPSTotalWariningFlag = 1 THEN True ELSE False END AS[_IOPSTotalWariningFlag],
                                                CASE WHEN _IOPSTotalCriticalFlag = 1 THEN True ELSE False END AS[_IOPSTotalCriticalFlag],
                                                CASE WHEN _LatencyTotalWariningFlag = 1 THEN True ELSE False END AS[_LatencyTotalWariningFlag],
                                                CASE WHEN _LatencyTotalCriticalFlag = 1 THEN True ELSE False END AS[_LatencyTotalCriticalFlag],
                                                CASE WHEN _ThroughputTotalWariningFlag = 1 THEN True ELSE False END AS[_ThroughputTotalWariningFlag],
                                                CASE WHEN _ThroughputTotalCriticalFlag = 1 THEN True ELSE False END AS[_ThroughputTotalCriticalFlag]
        FROM(
            SELECT [VolumeID], [Caption], [DetailsUrl], [Status], [IOPSTotal], [IOLatencyTotal], [BytesPSTotal],
                     MAX([_IOPSTotalWariningFlag]) as [_IOPSTotalWariningFlag],
                     MAX([_IOPSTotalCriticalFlag]) as [_IOPSTotalCriticalFlag],
                     MAX([_LatencyTotalWariningFlag]) as [_LatencyTotalWariningFlag],
                     MAX([_LatencyTotalCriticalFlag]) as [_LatencyTotalCriticalFlag],
                     MAX([_ThroughputTotalWariningFlag]) as [_ThroughputTotalWariningFlag],
                     MAX([_ThroughputTotalCriticalFlag]) as [_ThroughputTotalCriticalFlag]
            FROM(
                    SELECT[VolumeID], [Caption], [DetailsUrl], [Status], [IOPSTotal], [IOLatencyTotal], [BytesPSTotal],
                             CASE WHEN v0.IOPSTotalThreshold.IsLevel1State = true THEN 1 ELSE 0 END AS [_IOPSTotalWariningFlag],
                             CASE WHEN v0.IOPSTotalThreshold.IsLevel2State = true THEN 1 ELSE 0 END AS [_IOPSTotalCriticalFlag],
	                             0 AS[_LatencyTotalWariningFlag],
	                             0 AS[_LatencyTotalCriticalFlag],
	                             0 AS[_ThroughputTotalWariningFlag],
	                             0 AS[_ThroughputTotalCriticalFlag]
                    FROM Orion.SRM.Volumes (nolock= true) v0
                     UNION ALL(
                         SELECT[VolumeID], [Caption], [DetailsUrl], [Status], [IOPSTotal], [IOLatencyTotal], [BytesPSTotal],
 			                     0 AS[_IOPSTotalWariningFlag],
 			                     0 AS[_IOPSTotalCriticalFlag],
                                  CASE WHEN v1.IOLatencyTotalThreshold.IsLevel1State = true THEN 1 ELSE 0 END AS [_LatencyTotalWariningFlag],
                                  CASE WHEN v1.IOLatencyTotalThreshold.IsLevel2State = true THEN 1 ELSE 0 END AS [_LatencyTotalCriticalFlag],
 			                     0 AS[_ThroughputTotalWariningFlag],
 			                     0 AS[_ThroughputTotalCriticalFlag]
                         FROM Orion.SRM.Volumes (nolock= true) v1
                    )
                    UNION ALL(
                        SELECT[VolumeID], [Caption], [DetailsUrl], [Status], [IOPSTotal], [IOLatencyTotal], [BytesPSTotal],
			                     0 AS[_IOPSTotalWariningFlag],
			                     0 AS[_IOPSTotalCriticalFlag],
			                     0 AS[_LatencyTotalWariningFlag],
			                     0 AS[_LatencyTotalCriticalFlag],
                                 CASE WHEN v2.BytesPSTotalThreshold.IsLevel1State = true THEN 1 ELSE 0 END AS [_ThroughputTotalWariningFlag],
                                 CASE WHEN v2.BytesPSTotalThreshold.IsLevel2State = true THEN 1 ELSE 0 END AS [_ThroughputTotalCriticalFlag]
                        FROM Orion.SRM.Volumes (nolock= true) v2
                    )
            ) GROUP BY[VolumeID], [Caption], [DetailsUrl], [Status], [IOPSTotal], [IOLatencyTotal], [BytesPSTotal]
        ) v
     ";

    #endregion

    #region Properties

    /// <summary>
    /// Override default title property.
    /// </summary>
    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.PerformanceDashboard_Nas_Volumes_Performance_Title;
        }
    }

    /// <summary>
    /// Override help link fragment property.
    /// </summary>
    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceVolumesPerformanceSummary";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
    }

    /// <summary>
    /// Gets or sets Search Control property.
    /// </summary>
    public ResourceSearchControl SearchControl
    {
        get;
        set;
    }

    /// <summary>
    /// Gets additional query for search control.
    /// </summary>
    public string SearchSwql
    {
        get
        {
            return string.Concat(query, " WHERE v.Caption LIKE '%${SEARCH_STRING}%'");
        }
    }

    #endregion

    /// <summary>
    /// OnLoad override method.
    /// </summary>
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        ResourceWrapper.HeaderButtons.Controls.Add(SearchControl);

        query = InvariantString.Format(queryTemplate, Volume.NetObjectPrefix, SwisEntities.Volume);

        base.OnLoad(e);
        CustomTable.UniqueClientID = ScriptFriendlyResourceId;
        CustomTable.SWQL = query;
        
        CustomTable.SearchSWQL = SearchSwql;
    }
}