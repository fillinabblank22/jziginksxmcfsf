﻿using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.UI;
using System;
using System.Collections.Generic;
using System.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
public partial class Orion_SRM_Resources_AllSrmAlerts : SrmBaseResourceControl
{
    private bool _showAcknowledgedAlerts;

    protected void Page_Load(object sender, EventArgs e)
    {
        string showAck = Resource.Properties["ShowAcknowledgedAlerts"];
        _showAcknowledgedAlerts = !string.IsNullOrEmpty(showAck) && showAck.Equals(bool.TrueString, StringComparison.OrdinalIgnoreCase);

        AlertsTable.ShowAcknowledgedAlerts = _showAcknowledgedAlerts;
        AlertsTable.ResourceID = Resource.ID;

        int rowsPerPage;
        if (!int.TryParse(Resource.Properties["RowsPerPage"], out rowsPerPage))
            rowsPerPage = 5;

        AlertsTable.InitialRowsPerPage = rowsPerPage;

        AddShowAllCurrentlyTriggeredAlertsControl(wrapper);
        PopulateSrmAlerts();
    }

    private void PopulateSrmAlerts()
    {
       var alertsDAL = ServiceLocator.GetInstance<IAlertsDAL>();
       AlertsTable.TriggeringObjectEntityNames = alertsDAL?.GetAlertSourceEntityTypes() ?? new List<string>();
    }

    protected override string DefaultTitle
    {
        get { return (Resource != null) ? Resource.Title : String.Empty; }
    }

    public override string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Resource.SubTitle.Trim()))
                return Resource.SubTitle;

            // if subtitle is not specified
            if (_showAcknowledgedAlerts)
                return Resources.CoreWebContent.WEBCODE_IB0_5;
            else
                return Resources.CoreWebContent.WEBCODE_IB0_6;
        }
    }

    private void AddShowAllCurrentlyTriggeredAlertsControl(Control resourceWrapperContents)
    {
        if (resourceWrapperContents.TemplateControl is IResourceWrapper)
        {
            IResourceWrapper wrapper = resourceWrapperContents.TemplateControl as IResourceWrapper;
            wrapper.ManageButtonText = Resources.CoreWebContent.WEBDATA_BV0_0043;
            wrapper.ManageButtonTarget = "/Orion/NetPerfMon/Alerts.aspx";
            wrapper.ShowManageButton = !OrionConfiguration.IsDemoServer;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "SRMREactiveAlertsOnThisStorageObject"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditAllAlerts.ascx"; }
    }
}