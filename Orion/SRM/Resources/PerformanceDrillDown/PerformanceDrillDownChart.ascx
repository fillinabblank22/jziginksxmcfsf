﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PerformanceDrillDownChart.ascx.cs" Inherits="Orion_SRM_Resources_PerformanceDrillDown_PerformanceDrillDownLineChart" %>

<orion:Include ID="Include1" runat="server" File="OrionCore.js" />
<orion:Include ID="Include2" runat="server" Module="SRM" File="Charts/Charts.SRM.PerformanceDrillDown.js" />
<orion:Include ID="Include3" runat="server" Module="SRM" File="Performance.css" />
<orion:Include ID="Include4" runat="server" Module="SRM" File="SRM.css" />
<orion:Include ID="Include5" runat="server" Module="SRM" File="SRM.Formatters.js" />

<orion:resourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <div id="srm-performance-chart-<%=Resource.ID%>"></div>
        <div id="srm-performance-legend-<%=Resource.ID%>" class="srm-legend"></div>
        <script type="text/javascript">
            $(function () {
                var performanceChart = new SW.SRM.Charts.PerformanceDrillDown();
                performanceChart.init(<%=Data%>);
                
                var islineChart = '<%= IsLineChart %>';
                if(islineChart.toLowerCase() === 'false') {
                    $('#srm-performance-legend-<%=Resource.ID%>').hide();
                }
            });

        </script>
    </Content>
</orion:resourceWrapper>
