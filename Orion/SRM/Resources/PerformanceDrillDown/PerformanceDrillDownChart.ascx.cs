﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Net.Http.Headers;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;

public partial class Orion_SRM_Resources_PerformanceDrillDown_PerformanceDrillDownLineChart : PerformanceChartBase, IResourceIsInternal
{
    #region Properties

    private readonly Dictionary<string, string> additionalParams = new Dictionary<string, string>();
    /// <summary>
    /// Determinate if resource is internal.
    /// </summary>
    public bool IsInternal
    {
        get
        {
            return true;
        }
    }

    /// <summary>
    /// Determine is chart is line type.
    /// </summary>
    public bool IsLineChart
    {
        get
        {
            return GetChartType == SrmWebContent.PerformanceDrillDown_LineChartType.ToLower();
        }
    }

    /// <summary>
    /// Gets chart type.
    /// </summary>
    public string GetChartType
    {
        get
        {
            return string.IsNullOrEmpty(Resource.Properties["ChartType"])
                       ? SrmWebContent.PerformanceDrillDown_LineChartType.ToLower()
                       : Resource.Properties["ChartType"].ToLower();
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            switch (Resource.Properties["RequestParameter"])
            {
                case "IOPS":
                    return "SRMPHResourceIopsPerformance";
                case "Throughput":
                    return "SRMPHResourceThroughputPerformance";
                case "Latency":
                    return "SRMPHResourceLatencyPerformance";
                case "IOSize":
                    return "SRMPHResourceIOSizePerformance";
                case "CacheHitRatio":
                    return "SRMPHResourceIORatioPerformance";
                case "IOPSRatio":
                    return "SRMPHResourceIORatioPerformance";
                default:
                    return "SRMPHResourcePerformanceSummary";
            }
        }
    }

    private void GetAdditionalParams()
    {
        additionalParams.Add("ChartTitle", Resource.Properties["ChartTitle"]);
        additionalParams.Add("YAxisTitle", Resource.Properties["YAxisTitle"]);
        additionalParams.Add("ResourceTitle", Resource.Properties["ResourceTitle"]);

        string reqParams = Resource.Properties["RequestParameter"];
        string netObjectPrefix = Request["NetObject"].Split(':')[0];

        switch (netObjectPrefix)
        {
            case StorageArray.NetObjectPrefix:
                if (reqParams.Equals("IOPS"))
                {
                    additionalParams["ChartTitle"] = additionalParams["ChartTitle"] ?? SrmWebContent.PerformanceDrillDown_ArrayIOPSChartTitle;
                    additionalParams["ResourceTitle"] = additionalParams["ResourceTitle"] ?? SrmWebContent.PerformanceDrillDown_ArrayIOPSResourceTitle;
                    additionalParams["YAxisTitle"] = additionalParams["YAxisTitle"] ?? SrmWebContent.PerformanceDrillDown_IOPS_Unit;
                }
                if (reqParams.Equals("Throughput"))
                {
                    additionalParams["ChartTitle"] = additionalParams["ChartTitle"] ?? SrmWebContent.PerformanceDrillDown_ArrayThroughputChartTitle;
                    additionalParams["ResourceTitle"] = additionalParams["ResourceTitle"] ?? SrmWebContent.PerformanceDrillDown_ArrayThroughputResourceTitle;
                    additionalParams["YAxisTitle"] = additionalParams["YAxisTitle"] ?? SrmWebContent.PerformanceDrillDown_Throughput_Unit;
                }
                break;
            case Lun.NetObjectPrefix:
                if (reqParams.Equals("IOPS"))
                {
                    additionalParams["ChartTitle"] = additionalParams["ChartTitle"] ?? SrmWebContent.PerformanceDrillDown_LunIOPSChartTitle;
                    additionalParams["ResourceTitle"] = additionalParams["ResourceTitle"] ?? SrmWebContent.PerformanceDrillDown_LunIOPSResourceTitle;
                    additionalParams["YAxisTitle"] = additionalParams["YAxisTitle"] ?? SrmWebContent.PerformanceDrillDown_IOPS_Unit;
                }
                if (reqParams.Equals("Latency"))
                {
                    additionalParams["ChartTitle"] = additionalParams["ChartTitle"] ?? SrmWebContent.PerformanceDrillDown_LunLatencyChartTitle;
                    additionalParams["ResourceTitle"] = additionalParams["ResourceTitle"] ?? SrmWebContent.PerformanceDrillDown_LunLatencyResourceTitle;
                    additionalParams["YAxisTitle"] = additionalParams["YAxisTitle"] ?? SrmWebContent.PerformanceDrillDown_Latency_Unit;
                }
                if (reqParams.Equals("Throughput"))
                {
                    additionalParams["ChartTitle"] = additionalParams["ChartTitle"] ?? SrmWebContent.PerformanceDrillDown_LunThroughputChartTitle;
                    additionalParams["ResourceTitle"] = additionalParams["ResourceTitle"] ?? SrmWebContent.PerformanceDrillDown_LunThroughputResourceTitle;
                    additionalParams["YAxisTitle"] = additionalParams["YAxisTitle"] ?? SrmWebContent.PerformanceDrillDown_Throughput_Unit;
                }
                break;
            case Pool.NetObjectPrefix:
                if (reqParams.Equals("IOPS"))
                {
                    additionalParams["ChartTitle"] = additionalParams["ChartTitle"] ?? SrmWebContent.PerformanceDrillDown_PoolIOPSChartTitle;
                    additionalParams["ResourceTitle"] = additionalParams["ResourceTitle"] ?? SrmWebContent.PerformanceDrillDown_PoolIOPSResourceTitle;
                    additionalParams["YAxisTitle"] = additionalParams["YAxisTitle"] ?? SrmWebContent.PerformanceDrillDown_IOPS_Unit;
                }
                if (reqParams.Equals("Throughput"))
                {
                    additionalParams["ChartTitle"] = additionalParams["ChartTitle"] ?? SrmWebContent.PerformanceDrillDown_PoolThroughputChartTitle;
                    additionalParams["ResourceTitle"] = additionalParams["ResourceTitle"] ?? SrmWebContent.PerformanceDrillDown_PoolThroughputResourceTitle;
                    additionalParams["YAxisTitle"] = additionalParams["YAxisTitle"] ?? SrmWebContent.PerformanceDrillDown_Throughput_Unit;
                }
                break;
            case Volume.NetObjectPrefix:
                if (reqParams.Equals("IOPS"))
                {
                    additionalParams["ChartTitle"] = additionalParams["ChartTitle"] ?? SrmWebContent.PerformanceDrillDown_VolumeIOPSChartTitle;
                    additionalParams["ResourceTitle"] = additionalParams["ResourceTitle"] ?? SrmWebContent.PerformanceDrillDown_VolumeIOPSResourceTitle;
                    additionalParams["YAxisTitle"] = additionalParams["YAxisTitle"] ?? SrmWebContent.PerformanceDrillDown_IOPS_Unit;
                }
                if (reqParams.Equals("Latency"))
                {
                    additionalParams["ChartTitle"] = additionalParams["ChartTitle"] ?? SrmWebContent.PerformanceDrillDown_VolumeLatencyChartTitle;
                    additionalParams["ResourceTitle"] = additionalParams["ResourceTitle"] ?? SrmWebContent.PerformanceDrillDown_VolumeLatencyResourceTitle;
                    additionalParams["YAxisTitle"] = additionalParams["YAxisTitle"] ?? SrmWebContent.PerformanceDrillDown_Latency_Unit;
                }
                if (reqParams.Equals("Throughput"))
                {
                    additionalParams["ChartTitle"] = additionalParams["ChartTitle"] ?? SrmWebContent.PerformanceDrillDown_VolumeThroughputChartTitle;
                    additionalParams["ResourceTitle"] = additionalParams["ResourceTitle"] ?? SrmWebContent.PerformanceDrillDown_VolumeThroughputResourceTitle;
                    additionalParams["YAxisTitle"] = additionalParams["YAxisTitle"] ?? SrmWebContent.PerformanceDrillDown_Throughput_Unit;
                }
                break;
            case FileShares.NetObjectPrefix:
                if (reqParams.Equals("IOPS"))
                {
                    additionalParams["ChartTitle"] = additionalParams["ChartTitle"] ?? SrmWebContent.PerformanceDrillDown_VolumeIOPSChartTitle;
                    additionalParams["ResourceTitle"] = additionalParams["ResourceTitle"] ?? SrmWebContent.PerformanceDrillDown_VolumeIOPSResourceTitle;
                    additionalParams["YAxisTitle"] = additionalParams["YAxisTitle"] ?? SrmWebContent.PerformanceDrillDown_IOPS_Unit;
                }
                if (reqParams.Equals("Latency"))
                {
                    additionalParams["ChartTitle"] = additionalParams["ChartTitle"] ?? SrmWebContent.PerformanceDrillDown_VolumeLatencyChartTitle;
                    additionalParams["ResourceTitle"] = additionalParams["ResourceTitle"] ?? SrmWebContent.PerformanceDrillDown_VolumeLatencyResourceTitle;
                    additionalParams["YAxisTitle"] = additionalParams["YAxisTitle"] ?? SrmWebContent.PerformanceDrillDown_Latency_Unit;
                }
                if (reqParams.Equals("Throughput"))
                {
                    additionalParams["ChartTitle"] = additionalParams["ChartTitle"] ?? SrmWebContent.PerformanceDrillDown_VolumeThroughputChartTitle;
                    additionalParams["ResourceTitle"] = additionalParams["ResourceTitle"] ?? SrmWebContent.PerformanceDrillDown_VolumeThroughputResourceTitle;
                    additionalParams["YAxisTitle"] = additionalParams["YAxisTitle"] ?? SrmWebContent.PerformanceDrillDown_Throughput_Unit;
                }
                break;
            case VServer.NetObjectPrefix:
                if (reqParams.Equals("IOPS"))
                {
                    additionalParams["ChartTitle"] = additionalParams["ChartTitle"] ?? SrmWebContent.PerformanceDrillDown_VserverIOPSChartTitle;
                    additionalParams["ResourceTitle"] = additionalParams["ResourceTitle"] ?? SrmWebContent.PerformanceDrillDown_VserverIOPSResourceTitle;
                    additionalParams["YAxisTitle"] = additionalParams["YAxisTitle"] ?? SrmWebContent.PerformanceDrillDown_IOPS_Unit;
                }
                if (reqParams.Equals("Throughput"))
                {
                    additionalParams["ChartTitle"] = additionalParams["ChartTitle"] ?? SrmWebContent.PerformanceDrillDown_VserverThroughputChartTitle;
                    additionalParams["ResourceTitle"] = additionalParams["ResourceTitle"] ?? SrmWebContent.PerformanceDrillDown_VserverThroughputResourceTitle;
                    additionalParams["YAxisTitle"] = additionalParams["YAxisTitle"] ?? SrmWebContent.PerformanceDrillDown_Throughput_Unit;
                }
                break;
        }
    }

    protected string Data
    {
        get
        {
            return
                Serializer.Serialize(
                    new
                        {
                            ResourceId = Resource.ID,
                            NetObjectId = Request["NetObject"],
                            SampleSizeInMinutes = SampleSizeInMinutesProperty,
                            DaysOfDataToLoad = DaysOfDataToLoadProperty,
                            ChartTitle = additionalParams["ChartTitle"],
                            YAxisTitle = additionalParams["YAxisTitle"],
                            DataBindMethod = Resource.Properties["DataBindMethod"],
                            ChartWidth = Resource.Width - 20,
                            ChartType = GetChartType,
                            RequestParameter = Resource.Properties["RequestParameter"]
                        });
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        GetAdditionalParams();
        if (String.IsNullOrEmpty(Resource.Properties["ResourceTitle"]))
        {
            Resource.Title = additionalParams["ResourceTitle"];
            this.Page.Title = this.Page.Title + additionalParams["ResourceTitle"];
        }
    }

    #endregion
}