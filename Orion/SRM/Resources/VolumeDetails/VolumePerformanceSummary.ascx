﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VolumePerformanceSummary.ascx.cs" Inherits="Orion_SRM_Resources_VolumeDetails_VolumePerformanceSummary" %>
<%@ Register TagPrefix="srm" TagName="PerformanceSummary" Src="~/Orion/SRM/Controls/PerformanceSummary.ascx" %>

<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <srm:PerformanceSummary runat="server" id="VolumePerformanceSummary" DataBindMethod="GetNasVolumePerformanceSummaryData" />
    </Content>
</orion:ResourceWrapper>