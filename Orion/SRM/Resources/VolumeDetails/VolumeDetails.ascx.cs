﻿using System;
using System.Globalization;
using System.Web.UI.WebControls;
using Resources;

using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Web.Plugins;


[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SRM_Resources_DetailsView_VolumeDetails : VolumeBaseResourceControl
{

    /// <summary>
    /// Gets or sets warning threshold days property.
    /// </summary>
    protected int WarningThresholdDays { get; set; }

    /// <summary>
    /// Gets or sets critical threshold days property.
    /// </summary>
    protected int CriticalThresholdDays { get; set; }

    /// <summary>
    /// Gets or sets capacity projected runout property.
    /// </summary>
    public int CapacityRunout { get; set; }

    protected override string DefaultTitle
    {
        get { return SrmWebContent.VolumeDetails_DefaultPageTitle; }
    }

    public override string HelpLinkFragment
    {
        get { return "SRMPHResourceVolumeDetails"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public ArrayEntity Array { get; set; }
    protected VolumeStatus VolumeDetailsStatus { get; set; }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            Wrapper.HeaderButtons.Controls.Add(LoadControl("~/Orion/SRM/Controls/ManageNetObjectButton.ascx"));
        WarningThresholdDays = (int) SettingsDAL.GetSetting(SRMConstants.WarningThresholdDays).SettingValue;
        CriticalThresholdDays = (int) SettingsDAL.GetSetting(SRMConstants.CriticalThresholdDays).SettingValue;

        if (Volume != null)
        {
            VolumeEntity v = Volume.VolumeEntity;
            Array = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayDetails(v.StorageArrayID);

            VolumeDetailsStatus = v.Status;
            StatusLabel.Text = String.Format(CultureInfo.CurrentCulture, "{0} {1}", v.DisplayName,
                SrmStatusLabelHelper.GetStatusLabel(v.Status));
            NameLabel.Text = v.DisplayName;

            if (Array.IsCluster)
            {
                ArrayRow.Visible = false;
                // Assign data source to the repeater and rebind
                VServersList.DataSource = ServiceLocator.GetInstance<IVServerDAL>().GetVServersByVolumeId(v.VolumeID);
                VServersList.DataBind();

                WebHelper.SetupHyperLink(ClusterHyperLink, Array.DisplayName, StorageArray.NetObjectPrefix,
                    Array.StorageArrayId.Value);
            }
            else
            {
                // Regular array
                VServerRow.Visible = false;
                ClusterRow.Visible = false;

                WebHelper.SetupHyperLink(ArrayHyperlink, Array.DisplayName, StorageArray.NetObjectPrefix,
                    Array.StorageArrayId.Value);
            }

            StoragePoolList.DataSource = ServiceLocator.GetInstance<IPoolDAL>().GetPoolsDataByVolumeId(v.VolumeID);
            StoragePoolList.DataBind();

            var dal = ServiceLocator.GetInstance<IThresholdDAL>();
            WebHelper.SetValueOrHide(VendorStatusCodeLabel, VendorStatusCodeRow, v.VendorStatusDescription);
            TypeLabel.Text = v.Thin ? SrmWebContent.VolumeDetails_Type_Thin : SrmWebContent.VolumeDetails_Type_Thick;
            TotalUserCapacityLabel.Text = WebHelper.FormatCapacityTotal(v.CapacityTotal);
            ConsumedCapacityLabel.Text = WebHelper.FormatCapacityTotal(v.CapacityAllocated);
            RemainingCapacityLabel.Text = WebHelper.FormatCapacityTotal(v.CapacityFree);
            if (v.Thin)
                WebHelper.MarkThresholds(ConsumedCapacityLabel, (float)v.ProvisionedCapacityPercent, dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeProvisionedPercent, v.VolumeID));
        
            if (v.CapacityRunout.HasValue)
            {
                CapacityRunout = WebHelper.GetDayDifference(v.CapacityRunout.Value);
            }

            LastSync.Text = v.LastSync.HasValue ? Utils.FormatDateTimeForDisplayingOnWebsite(v.LastSync.Value.ToLocalTime(), true) : String.Empty;
            ProjectedRunOutRow.Visible = v.Thin;

            this.PerformanceAnalyzerImage.ImageUrl = PerformanceStackHelper.PerformanceAnalyzerImage;
            SetupPerformanceHyperLink(this.PerformanceHyperLink, v.VolumeID);
        }
        else
        {
            StatusLabel.Text = SrmWebContent.VolumeDetails_VolumeStatusFail;
        }
    }

    private void SetupPerformanceHyperLink(HyperLink link, int volumeID)
    {
        link.Text = SrmWebContent.PerformanceAnalizer_TextLink;
        link.NavigateUrl = PerformanceStackHelper.LinkToNASVolume(volumeID);
    }

}
