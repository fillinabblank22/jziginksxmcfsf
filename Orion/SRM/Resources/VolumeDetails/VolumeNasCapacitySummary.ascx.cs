﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.Resources;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Common.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_SRM_Resources_VolumeDetails_VolumeNasCapacitySummary : CapacitySummaryBase
{
    #region Properties

    /// <summary>
    /// Override resource property.
    /// </summary>
    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.VolumeNasCapacitySummary_Title;
        }
    }

    /// <summary>
    /// Override help link fragment property.
    /// </summary>
    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceLUNCapacitySummary";
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new[] {typeof (IVolumeProvider)};
        }
    }

    public override string EditControlLocation { get { return null; } }
    
    /// <summary>
    /// Gets data for current resource.
    /// </summary>
    protected string Data
    {
        get
        {
            return
                Serializer.Serialize(
                    new
                        {
                            ResourceId = Resource.ID,
                            NetObjectId = Request["NetObject"],
                            PageSize = PageSize,
                            Expanded = SrmSessionManager.GetBlockCapacitySummaryExpanderState(Resource.ID),
                            RenderTo = chart.ClientID,
                            ResourceWidth = Resource.Width,
                            DataSourceMethod = "GetVolumeNasCapacitySummaryData",
                            LegendMapping = new Dictionary<String, String>() 
                                { 
                                    { "columnDarkBlue", Resources.SrmWebContent.BlockCapacitySummary_Consumed },
                                    { "columnGrey", Resources.SrmWebContent.BlockCapacitySummary_Remaining }
                                }
                        });
        }
    }

    public Volume Volume
    {
        get;
        set;
    }

    #endregion


    #region Methods

    /// <summary>
    /// Page load event handler.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        var volumeProvider = GetInterfaceInstance<IVolumeProvider>();

        this.Volume = volumeProvider.Volume;

        if (this.Volume.VolumeEntity.CapacityRunout.HasValue)
        {
            CapacityProjectedRunout = WebHelper.GetDayDifference(this.Volume.VolumeEntity.CapacityRunout.Value);
        }

        if (!IsPostBack)
        {
            TotalCapacityLabel.Text = WebHelper.FormatCapacityTotal(this.Volume.VolumeEntity.CapacityTotal);

            var dal = ServiceLocator.GetInstance<IThresholdDAL>();

            ConsumedLabel.Text = WebHelper.FormatCapacityTotal(this.Volume.VolumeEntity.CapacityAllocated);
            if (Volume.VolumeEntity.Thin)
                WebHelper.MarkThresholds(ConsumedLabel, (float)Volume.VolumeEntity.ProvisionedCapacityPercent, dal.GetThresholdDynamicOrGlobal(ThresholdDataType.VolumeProvisionedPercent, Volume.VolumeEntity.VolumeID));
        }

        ProjectedRunOutRow.Visible = Volume.VolumeEntity.Thin;
    }

    #endregion
}