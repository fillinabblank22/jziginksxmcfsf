﻿using System;
using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.UI;


[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_SRM_Resources_VolumeDetails_VolumeNasNfsExport : VolumeBaseResourceControl
{
    protected override string DefaultTitle
    {
        get { return SrmWebContent.VolumeNasNfsExport_Resource_Title; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceExports";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        FileSharesList.ParentResourceId = ScriptFriendlyResourceId;
        FileSharesList.Where = InvariantString.Format(@"WHERE fs.Type = {0} AND fs.Volume.VolumeID={1}", (int)FileSharesType.NFS, Volume.VolumeEntity.VolumeID);

        ResourceSearchControl searchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        Wrapper.HeaderButtons.Controls.Add(searchControl);
        FileSharesList.SearchControl = searchControl;
        FileSharesList.ResourceID = this.Resource.ID;
    }
}