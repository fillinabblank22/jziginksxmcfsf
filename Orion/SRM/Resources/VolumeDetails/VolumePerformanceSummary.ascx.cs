﻿using System;
using Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.UI;
using SolarWinds.SRM.Common.ServiceProvider;


[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_SRM_Resources_VolumeDetails_VolumePerformanceSummary : VolumeBaseResourceControl
{
    #region Properties

    /// <summary>
    /// Override resource property.
    /// </summary>
    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.NasVolumePerformanceSummary_Title;
        }
    }

    /// <summary>
    /// Override help link fragment property.
    /// </summary>
    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourcePerformanceSummary";
        }
    }

    public override string EditURL
    {
        get
        {
            return ExtendCustomEditUrl("/Orion/SRM/EditTopXXChartResource.aspx?EditXX=False");
        }
    }

    #endregion


    #region Methods

    /// <summary>
    /// Page load event handler.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        VolumePerformanceSummary.ResourceID = Resource.ID;
        VolumePerformanceSummary.ResourceWidth = Resource.Width;

        VolumePerformanceSummary.IOPSEnable = true;
        VolumePerformanceSummary.LatencyEnable = true;
        VolumePerformanceSummary.ThroughputEnable = true;
        VolumePerformanceSummary.IOSizeEnable = true;
        VolumePerformanceSummary.QueueLengthEnable = true;
        VolumePerformanceSummary.IOPSRatioEnable = true;
        VolumePerformanceSummary.DiskBusyEnable = false;
        VolumePerformanceSummary.CacheHitRatioEnable = false;

        VolumePerformanceSummary.CustomIOPSResourceTitle = SrmWebContent.PerformanceDrillDown_VolumeIOPSResourceTitle;
        VolumePerformanceSummary.CustomIOPSChartTitle = SrmWebContent.PerformanceDrillDown_VolumeIOPSChartTitle;
        VolumePerformanceSummary.CustomLatencyResourceTitle = SrmWebContent.PerformanceDrillDown_VolumeLatencyResourceTitle;
        VolumePerformanceSummary.CustomLatencyChartTitle = SrmWebContent.PerformanceDrillDown_VolumeLatencyChartTitle;
        VolumePerformanceSummary.CustomThroughputResourceTitle = SrmWebContent.PerformanceDrillDown_VolumeThroughputResourceTitle;
        VolumePerformanceSummary.CustomThroughputChartTitle = SrmWebContent.PerformanceDrillDown_VolumeThroughputChartTitle;
        VolumePerformanceSummary.CustomIOSizeResourceTitle = SrmWebContent.PerformanceDrillDown_VolumeIOSizeResourceTitle;
        VolumePerformanceSummary.CustomIOSizeChartTitle = SrmWebContent.PerformanceDrillDown_VolumeIOSizeChartTitle;
        VolumePerformanceSummary.CustomQueueLengthResourceTitle = SrmWebContent.PerformanceDrillDown_VolumeQueueLengthResourceTitle;
        VolumePerformanceSummary.CustomQueueLengthChartTitle = SrmWebContent.PerformanceDrillDown_VolumeQueueLengthChartTitle;
        VolumePerformanceSummary.CustomIOPSRatioResourceTitle = SrmWebContent.PerformanceDrillDown_VolumeIOPSRatioResourceTitle;
        VolumePerformanceSummary.CustomIOPSRatioChartTitle = SrmWebContent.PerformanceDrillDown_VolumeIOPSRatioChartTitle;

        VolumePerformanceSummary.SampleSizeInMinutes = SampleSizeInMinutes;
        VolumePerformanceSummary.DaysOfDataToLoad = DaysOfDataToLoad;
        VolumePerformanceSummary.InitialZoom = InitialZoom;

        VolumePerformanceSummary.ChartTitleProperty = Resource.Properties["ChartTitle"];
        VolumePerformanceSummary.ChartSubTitleProperty = Resource.Properties["ChartSubTitle"];
    }

    protected override bool IsResourceVisible()
    {
        return ServiceLocator.GetInstance<IResourceVisibilityService>()
            .IsResourceVisibleForArray(Volume.StorageArray, this.AppRelativeVirtualPath);
    }

    #endregion
}