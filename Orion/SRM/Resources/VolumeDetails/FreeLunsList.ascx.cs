﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;

public partial class Orion_SRM_Resources_VolumeDetails_FreeLunsList : VolumeBaseResourceControl
{
    #region Fields

    private string query;

    #endregion

    #region Properties
    /// <summary>
    /// Override default title property.
    /// </summary>
    protected override string DefaultTitle
    {
        get { return SrmWebContent.Free_Luns_Title; }
    }

    /// <summary>
    /// Override help link fragment property.
    /// </summary>
    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceFreeLUNs";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
    }

    /// <summary>
    /// Gets or sets Search Control property.
    /// </summary>
    public ResourceSearchControl SearchControl
    {
        get;
        set;
    }

    /// <summary>
    /// Gets additional query for search control.
    /// </summary>
    public string SearchSwql
    {
        get
        {
            return string.Concat(query, " AND l.[Caption] LIKE '%${SEARCH_STRING}%'");
        }
    }
    
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        ResourceWrapper.HeaderButtons.Controls.Add(SearchControl);

        query = String.Format(CultureInfo.InvariantCulture,
                              @"SELECT l.[Caption] AS Name,
                                '/Orion/StatusIcon.ashx?entity={2}&status=' + ToString(ISNULL([Status], 0)) +'&size=small' AS [_IconFor_Name],  
                                '/Orion/View.aspx?NetObject={1}:' + ToString(l.[LUNID]) AS [_LinkFor_Name],
                                l.CapacityTotal AS [TotalUserCapacity]
                                FROM Orion.SRM.Luns (nolock=true) l
                                WHERE l.[VolumeID]={0} AND l.Free = 1",
                              Volume.VolumeEntity.VolumeID,
                              Lun.NetObjectPrefix,
                              SwisEntities.Lun);

        CustomTable.UniqueClientID = ScriptFriendlyResourceId;
        CustomTable.SWQL = query;
        CustomTable.SearchSWQL = SearchSwql;
    }

    
}