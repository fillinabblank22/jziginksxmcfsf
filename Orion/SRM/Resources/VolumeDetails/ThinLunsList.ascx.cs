﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.SRM.Web.UI;

public partial class Orion_SRM_Resources_VolumeDetails_ThinLunsList : VolumeBaseResourceControl
{
    #region Properties

    /// <summary>
    /// Override default title property.
    /// </summary>
    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.ArrayDetails_Thick_Luns_Title;
        }
    }

    /// <summary>
    /// Override help link fragment property.
    /// </summary>
    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceThinsLUNs";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
    }

    #endregion

    /// <summary>
    /// OnLoad override method.
    /// </summary>
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        ThinLunsList.ParentResourceId = ScriptFriendlyResourceId;
        if (Volume == null || Volume.VolumeEntity == null)
        {
            throw new NullReferenceException("Volume array can't be null");
        }

        ThinLunsList.VolumeEntity = Volume.VolumeEntity;
        var searchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        Wrapper.HeaderButtons.Controls.Add(searchControl);
        ThinLunsList.SearchControl = searchControl;
    }
}