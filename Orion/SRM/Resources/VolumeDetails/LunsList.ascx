﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LunsList.ascx.cs" Inherits="Orion_SRM_Resources_VolumeDetails_LunsList" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Resources" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<orion:Include ID="Include1" runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include ID="Include3" runat="server" Module="SRM" File="SRM.Common.js" />
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.css" />

<orion:CustomQueryTable runat="server" ID="CustomTable" />
<script type="text/javascript">
    $(function () {
        SW.Core.Resources.CustomQuery.initialize(
            {
                uniqueId: '<%= CustomTable.UniqueClientID %>',
                initialPage: 0,
                rowsPerPage: <%= this.GetPageSize() %>,
                allowSort: true,
                columnSettings: {
                    "Name": {
                        header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.ArrayDetails_Luns_LunName%>'),
                        cellCssClassProvider: function (value, row, cellInfo) { return "SRM_Icon_Cell srm_word_wrap"; }
                    },
                    "AssociatedEndpoint": {
                        header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.ArrayDetails_Luns_AssociatedEndpoint%>'),
                        cellCssClassProvider: function (value, row, cellInfo) {
                            return "LunListAssociatedEndpoint<%=this.ParentResourceId%> SRM_HiddenGridCell srm_word_wrap";
                                }
                    },
                    "TotalSize": {
                        header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.ArrayDetails_Luns_TotalSize%>'),
                        formatter: function (value) {
                            return SW.SRM.Formatters.FormatCapacity(value);
                        },
                        cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "SRM_NoWrap"; }
                    },
                    "CapacityAllocated": {
                        header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.ArrayDetails_Luns_ProvisionedCapacity%>'),
                        formatter: function (value) {
                            return SW.SRM.Formatters.FormatCapacity(value);
                        },
                        cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                            return "SRM_NoWrap " + 
                                    SW.SRM.Formatters.FormatCellStyle(rowArray[7],
                                <%=ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.LunProvisionedPercent)%>,
                                        <%=ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.LunProvisionedPercent)%>);
                        }
                    },
                    "CapacityAllocatedPercentage": {
                        header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.ArrayDetails_Luns_ProvisionedPercentage%>'),
                        formatter: function (value) {
                            return SW.SRM.Formatters.FormatPercentageColumnValue(value);
                        },
                        cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                            return "SRM_NoWrap " + 
                                    SW.SRM.Formatters.FormatCellStyle(cellValue,
                                <%=ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.LunProvisionedPercent)%>,
                                        <%=ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.LunProvisionedPercent)%>);
                        }
                    },
                    "FileSystemUsedCapacity": {
                        header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.ArrayDetails_Luns_FileSystemUsedCapacity%>'),
                        cellCssClassProvider: function (value, row, cellInfo) {
                            return "SRM_NoWrap LunListFileSystemUsedCapacity<%=this.ParentResourceId%> SRM_HiddenGridCell"; 
                        }
                    },
                    "FileSystemUsedPercentage": {
                        header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.ArrayDetails_Luns_FileSystemUsedPercentage%>'),
                        cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                            return "SRM_NoWrap LunListFileSystemUsedPercentage<%=this.ParentResourceId%> SRM_HiddenGridCell"; 
                        }
                    }
                },
                searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                onLoad: function(rows, columnsInfo) {
                    var params = {
                        resourceID: <%=this.ParentResourceId%>,
                        netObjectFieldName:"_NetObjectID",
                                rows:  rows, 
                                columnsInfo: columnsInfo,
                                volumeSpacePercentWarning:<%=ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.LunFileSystemUsedPercent)%>,
                                volumeSpacePercentCritical:<%=ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.LunFileSystemUsedPercent)%>,
                                classesName: {
                                    associatedEndpoint: "LunListAssociatedEndpoint",
                                    volumeSpaceUsed: "LunListFileSystemUsedCapacity",
                                    volumeSpacePercent: "LunListFileSystemUsedPercentage"
                                }
                            };
                            SW.SRM.Common.FillAdditionColumns(params);
                        }
            });
        SW.Core.Resources.CustomQuery.refresh("<%=CustomTable.UniqueClientID %>");
    });
</script>
