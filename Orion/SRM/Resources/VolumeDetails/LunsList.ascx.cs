﻿using System;
using System.Globalization;
using System.Web.UI;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Models;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;

public partial class Orion_SRM_Resources_VolumeDetails_LunsList : SrmBaseResourceControl, IResourceIsInternal
{
    #region Fields

    private string query;

    private const string provisionedQueryTemplate = @"l.CapacityAllocated AS [CapacityAllocated],
                                    CASE WHEN 
                                    l.CapacityTotal is Null 
                                    OR l.CapacityTotal = 0 
                                    OR l.CapacityAllocated is Null
                                    THEN NULL
                                    ELSE ((l.[CapacityAllocated] * 1.0 / l.[CapacityTotal]) * 100) END AS [CapacityAllocatedPercentage],";

    #endregion

    #region Properties
    
    public bool IsInternal
    {
        get
        {
            return true;
        }
    }
    
    public ResourceSearchControl SearchControl { get; set; }

    public bool IsThick { get; set; }

    public int ParentResourceId { get; set; }

    public VolumeEntity VolumeEntity { get; set; }

    public string SearchSWQL
    {
        get { return string.Concat(query, " AND l.[Caption] LIKE '%${SEARCH_STRING}%'"); }
    }

    protected override string DefaultTitle
    {
        get { return SrmWebContent.LunsListResourceTitle; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
    }

    #endregion

    protected override void OnLoad(EventArgs e)
    {
        if (VolumeEntity != null)
        {
            base.OnLoad(e);
            query = String.Format(
                CultureInfo.InvariantCulture, @"SELECT l.[Caption] AS Name, 
                                Concat('SML:', l.LunID) AS [_NetObjectID],
                                '/Orion/View.aspx?NetObject={2}:' + ToString(LunID) AS [_LinkFor_Name],
                                '/Orion/StatusIcon.ashx?entity={3}&status=' + ToString(ISNULL([Status], 0)) +'&size=small' AS [_IconFor_Name],
                                Concat('%{2}:', l.LunID,'%') AS [AssociatedEndpoint],
                                l.CapacityTotal AS [TotalSize],
                                {4}
                                Concat('%{2}:', l.LunID,'%') AS [FileSystemUsedCapacity],
                                Concat('%{2}:', l.LunID,'%') AS [FileSystemUsedPercentage]
                                FROM Orion.SRM.Luns (nolock=true) l
                                WHERE l.VolumeID={0} AND l.[Thin]={1}",
                VolumeEntity.VolumeID, IsThick ? 0 : 1, Lun.NetObjectPrefix, SwisEntities.Lun,
                !IsThick ? provisionedQueryTemplate : String.Empty);

            CustomTable.UniqueClientID = ParentResourceId;
            CustomTable.SWQL = query;
            CustomTable.SearchSWQL = SearchSWQL;
        }
    }

}