<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VolumeCustomProperties.ascx.cs" Inherits="Orion_SRM_Resources_VolumeDetails_VolumeCustomProperties" %>
<%@ Register TagPrefix="orion" TagName="CustomPropertyList" Src="~/Orion/SRM/Controls/CustomPropertyList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:CustomPropertyList runat="server" ID="customPropertyList" EntityName="Orion.SRM.VolumeCustomProperties"/>
    </Content>
</orion:resourceWrapper>