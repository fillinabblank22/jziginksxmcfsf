﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VolumeDetails.ascx.cs" Inherits="Orion_SRM_Resources_DetailsView_VolumeDetails" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web" %>
<%@ Import Namespace="SolarWinds.SRM.Web.NetObjects" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.css" />
<orion:Include ID="Include3" runat="server" Module="SRM" File="SRM.Formatters.js" />

<orion:ResourceWrapper ID="Wrapper" runat="server">
    <Content>
        <table cellspacing="0" class="srm_details_table">
            <tr>
                <td class="PropertyHeader">
                    <div class="SRM_FirstRowDetailsTable">
                        <%= Resources.SrmWebContent.LinkToPerfStack_Title %>:
                    </div>
                </td>
                <td class="Property SRM_StatusImage">
                    <asp:Image ID="PerformanceAnalyzerImage" CssClass="StatusIcon" runat="server" />
                </td>
                <td class="Property">
                    <asp:HyperLink runat="server" ID="PerformanceHyperLink" Text="Performance Analyzer"/>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <div title ="<%= SrmWebContent.VolumeDetails_VolumeStatus_Tooltip%>">
                        <%= SrmWebContent.VolumeDetails_VolumeStatus%>:
                    </div>
                </td>
                <td class="Property SRM_StatusImage">
                    <img class="StatusIcon" src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.Volume%>&status=<%=(int)VolumeDetailsStatus%>&size=large" />      
                </td>
                <td class="Property">
                <asp:Label runat="server" ID="StatusLabel" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader" title="<%= SrmWebContent.VolumeDetails_Name_Tooltip %>">
                    <%= SrmWebContent.VolumeDetails_Name%>:
                </td>
                <td></td>
                <td class="Property statusAndText">
                    <asp:Label runat="server" ID="NameLabel" /></td>
            </tr>
            <tr>
                <td class="PropertyHeader" title="<%= SrmWebContent.VolumeDetails_StoragePool_Tooltip %>">
                    <%= SrmWebContent.VolumeDetails_StoragePool%>:
                </td>
                <td></td>
                 <td>
                    <asp:Repeater id="StoragePoolList" runat="server">
                        <ItemTemplate>
                            <span class="SRM_Icon_Cell">
                                <img src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.Pools%>&status=<%# (int)Eval("Status") %>" />
                                <asp:HyperLink 
                                    Text='<%# Eval("Caption") %>' 
                                    NavigateUrl='<%# String.Format(CultureInfo.CurrentCulture, "/Orion/View.aspx?NetObject={0}:{1}", Pool.NetObjectPrefix, Eval("PoolId")) %>' runat="server" ID="PoolHyperLink" />
                                <br />
                            </span>
                        </ItemTemplate>
                    </asp:Repeater>                    
                </td>
            </tr>
            <tr runat="server" ID="ArrayRow">
                <td class="PropertyHeader">
                    <span  title="<%= SrmWebContent.VolumeDetails_Array_Tooltip %>"> <%= SrmWebContent.VolumeDetails_Array%>:</span>
                </td>
                <td></td>
                <td class="SRM_Icon_Cell">
                    <img src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.StorageArrays%>&status=<%=(int)Array.Status%>" />
                    <asp:HyperLink runat="server" ID="ArrayHyperlink" />
                </td>
            </tr>
            <tr runat="server" ID="ClusterRow">
                <td class="PropertyHeader">
                    <span  title="<%= SrmWebContent.VolumeDetails_Cluster_Tooltip %>"><%= SrmWebContent.VolumeDetails_Cluster %>:</span>
                </td>
                <td></td>
                <td class="SRM_Icon_Cell">
                    <img src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.Clusters%>&status=<%=(int)Array.Status%>" />
                    <asp:HyperLink runat="server" ID="ClusterHyperLink" />
                </td>
            </tr>
            <tr runat="server" ID="VServerRow">
                <td class="PropertyHeader">
                    <span  title="<%= SrmWebContent.VolumeDetails_Vserver_Tooltip %>"><%= SrmWebContent.VolumeDetails_VServer%>:</span>
                </td>
                <td></td>
                <td>
                    <asp:Repeater id="VServersList" runat="server">
                        <ItemTemplate>
                            <span class="SRM_Icon_Cell">
                                <img src="/Orion/StatusIcon.ashx?entity=<%=SwisEntities.VServers%>&status=<%# (int)Eval("Status") %>" />
                                <asp:HyperLink 
                                    Text='<%# Eval("DisplayName") %>' 
                                    NavigateUrl='<%# String.Format(CultureInfo.CurrentCulture, "/Orion/View.aspx?NetObject={0}:{1}", VServer.NetObjectPrefix, Eval("VServerId")) %>' runat="server" ID="VServerHyperLink" />
                                <br />
                            </span>
                        </ItemTemplate>
                    </asp:Repeater>                    
                </td>
            </tr>
            <tr runat="server" id="VendorStatusCodeRow">
                <td class="PropertyHeader">
                   <span  title="<%= SrmWebContent.VolumeDetails_VendorStatusCode_Tooltip %>"><%= SrmWebContent.VolumeDetails_VendorStatusCode%>:</span> 
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="VendorStatusCodeLabel" /></td>
            </tr>
            <tr>
                <td class="PropertyHeader" title="<%= SrmWebContent.VolumeDetails_Type_Tooltip %>">
                    <%= SrmWebContent.VolumeDetails_Type%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="TypeLabel" /></td>
            </tr>
            <tr>
                <td class="PropertyHeader" title="<%= SrmWebContent.VolumeDetails_TotalUsableCapacity_Tooltip %>">
                    <%= SrmWebContent.VolumeDetails_TotalUserCapacity%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="TotalUserCapacityLabel" /></td>
            </tr>
            <tr>
                <td class="PropertyHeader" title="<%= SrmWebContent.VolumeDetails_ConsumedCapacity_Tooltip %>">
                    <%= SrmWebContent.VolumeDetails_ConsumedCapacity%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="ConsumedCapacityLabel" /></td>
            </tr>
            <tr>
            <td class="PropertyHeader" title=" <%= SrmWebContent.VolumeDetails_RemainingCapacity_Tooltip %>">
                    <%= SrmWebContent.VolumeDetails_RemainingCapacity%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="RemainingCapacityLabel" /></td>
            </tr>
            <tr>
                <td class="PropertyHeader" title="<%= SrmWebContent.VolumeDetails_LastSync_Tooltip %>">
                    <%= SrmWebContent.VolumeDetails_LastSync%>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Literal runat="server" ID="LastSync" /></td>
            </tr>
            <tr runat="server" ID="ProjectedRunOutRow">
                <td class="PropertyHeader" title=" <%= SrmWebContent.VolumeDetails_Projected_RunOut_Tooltip %>">
                    <%= SrmWebContent.VolumeDetails_ProjectedRunOut%>:
                </td>
                <td></td>               
                 <td >
                     <span id="RunSpan_<%=Resource.ID%>" class="SRM_BoldText">
                        <script type="text/javascript">$('#RunSpan_<%=Resource.ID%>').html(SW.SRM.Formatters.FormatDaysDiff(<%= CapacityRunout %>, <%= WarningThresholdDays %>, <%= CriticalThresholdDays %>))</script>
                    </span>
                </td>
            </tr>
        </table>
        <script type="text/javascript">
            $('.srm_details_table tr:even').addClass('SRM_AltRow1').removeClass('SRM_AltRow2');
            $('.srm_details_table tr:odd').addClass('SRM_AltRow2').removeClass('SRM_AltRow1');
        </script>
    </Content>
</orion:ResourceWrapper>
