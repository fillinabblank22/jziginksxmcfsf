﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ThickLunsList.ascx.cs" Inherits="Orion_SRM_Resources_VolumeDetails_ThickLunsList" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="srm" TagName="LunsList" Src="~/Orion/SRM/Resources/VolumeDetails/LunsList.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <srm:LunsList runat="server" ID="ThickLunsList" IsThick="True" />
    </Content>
</orion:resourceWrapper>