﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ServerVolumeDetails.ascx.cs" Inherits="Orion_SRM_Resources_ServerVolumeDetails_ServerVolumeDetails" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Web" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.css" />
<orion:Include ID="Include3" runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
    <Content>
        <table cellspacing="0" id="details_table">
            <tr  runat="server" id="nfsExportRow">
                <td class="PropertyHeader">
                    <span><asp:Literal runat="server" ID="NfsTypeLabel" />: </span>
                </td>
                <td></td>
                 <td >
                     <asp:Label runat="server" ID="NfsExportRowLabel" />
                </td>
            </tr>
            <tr runat="server" id="entityRow">
                <td class="PropertyHeader">
                    <span><%= IsLunType ? SrmWebContent.ServerVolumeDetails_Lun_Label : 
                                IsStoragePoolType ? SrmWebContent.ServerVolumeDetails_Pool_Label : 
                                    IsVServerType ? SrmWebContent.ServerVolumeDetails_VServer_Label : 
                                        SrmWebContent.ServerVolumeDetails_Volume_Label %>: </span>
                </td>
                <td class="Property  SRM_TD_Icon_Cell">
                    <img class="StatusIcon" id="entityStatus" runat="server" />    
                </td>
                <td class="Property">
                    <asp:HyperLink runat="server" ID="EntityHyperLink" /> 
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                        <%=SrmWebContent.ServerVolumeDetails_Array_Label %>:
                </td>
                <td class="Property  SRM_TD_Icon_Cell">
                    <img class="StatusIcon" src="/Orion/StatusIcon.ashx?entity=<%=Array.IsCluster?SwisEntities.Clusters:SwisEntities.StorageArrays%>&status=<%=(int) Array.Status%>&size=small"/>      
                </td>
                <td class="Property">
                    <asp:HyperLink runat="server" ID="ArrayHyperLink" />
                </td>
            </tr>

            <tr>
                <td class="PropertyHeader">
                    <span><%= IsLunType ? SrmWebContent.ServerVolumeDetails_LunTotalSize_Label: SrmWebContent.ServerVolumeDetails_VolumeTotalSize_Label %>: </span>
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="volumeSize" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                    <%=SrmWebContent.ServerVolumeDetails_ConsumedCapacity_Label %>:
                </td>
                <td></td>
                <td class="Property">
                    <asp:Label runat="server" ID="volumeSpaceUsed" />
                </td>
            </tr>
            <tr  runat="server" id="projectedRunOutRow" >
                <td class="PropertyHeader">
                    <%= SrmWebContent.ServerVolumeDetails_ProjectedRunOut_Label %>:
                </td>
                <td></td>
                <td class="Property">
                    <span id="RunSpan_<%=Resource.ID%>" class="SRM_BoldText">
                        <script type="text/javascript">$('#RunSpan_<%=Resource.ID%>').html(SW.SRM.Formatters.FormatDaysDiff(<%= CapacityRunout %>, <%= WarningThresholdDays %>, <%= CriticalThresholdDays %>))</script>
                    </span>
                </td>
            </tr>
        </table>
        <script type="text/javascript">
            $('#details_table tr:even').addClass('SRM_AltRow1').removeClass('SRM_AltRow2');
            $('#details_table tr:odd').addClass('SRM_AltRow2').removeClass('SRM_AltRow1');
        </script>
    </Content>
</orion:ResourceWrapper>
