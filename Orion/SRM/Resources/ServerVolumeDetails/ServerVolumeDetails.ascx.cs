﻿using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.Enums;
using SolarWinds.SRM.Common.Models;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.Interfaces;
using SolarWinds.SRM.Web.NetObjects;
using IVolumeProvider = SolarWinds.Orion.NPM.Web.IVolumeProvider;
using SettingsDAL = SolarWinds.Orion.Web.DAL.SettingsDAL;
using Volume = SolarWinds.Orion.NPM.Web.Volume;


[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_SRM_Resources_ServerVolumeDetails_ServerVolumeDetails : BaseResourceControl
{
    private ArrayEntity array;
    private static SolarWinds.Logging.Log Log = new SolarWinds.Logging.Log();

    protected override string DefaultTitle
    {
        get { return SrmWebContent.ServerVolumeDetailsResource_Title; }
    }

    protected int WarningThresholdDays { get; set; }

    protected int CriticalThresholdDays { get; set; }

    public int CapacityRunout { get; set; }

    public override string HelpLinkFragment
    {
        get { return "HelpLinkPHFragment"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IVolumeProvider) }; }
    }

    private string netObject;
    protected string NetObject
    {
        get { return netObject; }
        set
        {
            netObject = value;
            OnNetObjectChanged();
        }
    }

    private void OnNetObjectChanged()
    {
        if (String.IsNullOrEmpty(NetObject))
            return;

        string[] objects = NetObject.Split(':');
        NetObjectPrefix = objects[0];

        NetObjectID = Int32.Parse(objects[1]);

        if (IsFileshareType)
        {
            SmsNetObjectID = NetObjectID;
            NetObjectID = ServiceLocator.GetInstance<IFileSharesDAL>().GetVolumeIDByFileSharesID(SmsNetObjectID);
        }
    }

    protected string NetObjectPrefix { get; private set; }
    protected int SmsNetObjectID { get; private set; }
    protected int NetObjectID { get; private set; }

    protected Volume Volume { get; set; }

    protected SrmNetObjectBase NetObjectBase
    {
        get
        {
            if (IsLunType)
            {
                return new Lun(NetObjectID);
            }
            else if (IsStoragePoolType)
            {
                return new Pool(NetObjectID);
            }
            else if (IsStorageArrayType)
            {
                return new StorageArray(NetObjectID);
            }
            else if (IsVServerType)
            {
                return new VServer(NetObjectID);
            }
            else
            {
                //Fileshare and NAS Volume
                return new SolarWinds.SRM.Web.NetObjects.Volume(NetObjectID);
            }
        }
    }

    protected ArrayEntity Array
    {
        get
        {
            if (array == null)
            {
                if (IsStorageArrayType)
                {
                    array = ((StorageArray)NetObjectBase).Array;
                }
                else
                {
                    int arrayID = IsLunType ? ((Lun)NetObjectBase).LUN.StorageArrayId :
                                  IsStoragePoolType ? ((Pool)NetObjectBase).PoolEntity.StorageArrayID : 
                                  IsVServerType ? ((VServer)NetObjectBase).VServerEntity.StorageArrayID :
                                  //Fileshare and NAS Volume
                                  ((SolarWinds.SRM.Web.NetObjects.Volume)NetObjectBase).VolumeEntity.StorageArrayID;
                    array = ServiceLocator.GetInstance<IStorageArrayDAL>().GetArrayDetails(arrayID);
                }
            }
            return array;
        }
    }

    protected bool IsLunType
    {
        get
        {
            return NetObjectPrefix.Equals(Lun.NetObjectPrefix);
        }
    }

    protected bool IsStoragePoolType
    {
        get
        {
            return NetObjectPrefix.Equals(Pool.NetObjectPrefix);
        }
    }

    protected bool IsStorageArrayType
    {
        get
        {
            return NetObjectPrefix.Equals(StorageArray.NetObjectPrefix);
        }
    }

    protected bool IsVServerType
    {
        get
        {
            return NetObjectPrefix.Equals(VServer.NetObjectPrefix);
        }
    }

    protected bool IsNasVolumeType
    {
        get
        {
            return NetObjectPrefix.Equals(SolarWinds.SRM.Web.NetObjects.Volume.NetObjectPrefix);
        }
    }

    protected bool IsFileshareType
    {
        get
        {
            return NetObjectPrefix.Equals(FileShares.NetObjectPrefix);
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ResourceWrapper.ShowEditButton = ResourceWrapper.ShowEditButton && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        Volume = GetInterfaceInstance<IVolumeProvider>().Volume;
        NetObject = ServiceLocator.GetInstance<ITopologyDAL>().GetSRMNetObjectByServerVolumeID(Volume.VolumeID);
        Visible = !String.IsNullOrEmpty(NetObject);

        try
        {
        if (Visible)
        {
            WarningThresholdDays = (int)SettingsDAL.GetSetting(SRMConstants.WarningThresholdDays).SettingValue;
            CriticalThresholdDays = (int)SettingsDAL.GetSetting(SRMConstants.CriticalThresholdDays).SettingValue;

            volumeSize.Text = (Volume.TotalSize < 0)
                ? String.Empty
                : new Bytes(Volume.TotalSize).ToShort1024String();
            volumeSpaceUsed.Text = (Volume.SpaceUsed < 0)
                ? String.Empty
                : new Bytes(Volume.SpaceUsed).ToShort1024String();

            String swisEntities;
            int status;
            string entityName;
            int entityId;

            if (IsLunType)
            {
                var entity = ((Lun)NetObjectBase).LUN;
                CapacityRunout = WebHelper.GetDayDifference(entity.CapacityRunout);
                entityName = entity.DisplayName;
                entityId = entity.LUNID;
                status = (int)entity.Status;
                swisEntities = SwisEntities.Lun;
            }
            else if (IsStoragePoolType)
            {
                var entity = ((Pool)NetObjectBase).PoolEntity;
                CapacityRunout = WebHelper.GetDayDifference(entity.CapacityRunOut);
                entityName = entity.DisplayName;
                entityId = entity.PoolID;
                status = (int)entity.Status;
                swisEntities = SwisEntities.Pools;
            }
            else if (IsStorageArrayType)
            {
                entityRow.Visible = false;
                var entity = ((StorageArray)NetObjectBase).Array;
                CapacityRunout = WebHelper.GetDayDifference(entity.CapacityRunout);
                entityName = entity.DisplayName;
                entityId = entity.StorageArrayId.Value;
                status = (int)entity.Status;
                swisEntities = SwisEntities.StorageArrays;
            }
            else if (IsVServerType)
            {
                var entity = ((VServer)NetObjectBase).VServerEntity;
                // hide CapacityRunout row for VServer
                projectedRunOutRow.Visible = false;
                entityName = entity.DisplayName;
                entityId = entity.VServerID;
                status = (int)entity.Status;
                swisEntities = SwisEntities.VServers;
            }
            else
            {
                //Fileshare and NAS Volume
                var entity = ((SolarWinds.SRM.Web.NetObjects.Volume)NetObjectBase).VolumeEntity;
                CapacityRunout = WebHelper.GetDayDifference(entity.CapacityRunout);
                entityName = entity.DisplayName;
                entityId = entity.VolumeID;
                status = (int)entity.Status;
                swisEntities = SwisEntities.Volume;
            }

            entityStatus.Src = InvariantString.Format("/Orion/StatusIcon.ashx?entity={0}&status={1}", swisEntities, status);
            WebHelper.SetupHyperLink(EntityHyperLink, entityName, NetObjectPrefix, entityId);
            WebHelper.SetupHyperLink(ArrayHyperLink, Array.DisplayName, StorageArray.NetObjectPrefix, Array.StorageArrayId.Value);
            nfsExportRow.Visible = false;
            if (IsFileshareType)
            {
                nfsExportRow.Visible = true;

                NfsExportRowLabel.Text = ServiceLocator.GetInstance<IFileSharesDAL>().GetNfsExportByFileSharesID(SmsNetObjectID);
                var fileShare = ServiceLocator.GetInstance<IFileSharesDAL>().GetFileSharesDetails(SmsNetObjectID);
                NfsTypeLabel.Text = fileShare.Type == FileSharesType.NFS
                    ? SrmWebContent.ServerVolumeDetails_NFSExport
                    : SrmWebContent.ServerVolumeDetails_CIFSShare;
            }
            }
        }
        catch (NullReferenceException exception)
        {
            Log.Error("ServerVolumeDetails has been hidden because of inaccesible NetObject", exception);
            Visible = false;
        }
    }
}