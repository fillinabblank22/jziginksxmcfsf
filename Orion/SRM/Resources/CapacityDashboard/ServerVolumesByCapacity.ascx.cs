﻿using System;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;

public partial class Orion_SRM_Resources_CapacityDashboard_ServerVolumesByCapacity : SrmBaseResourceControl, IResourceIsInternal
{
    #region Fields

    private string query;

    private const string queryTemplate = @"SELECT 
                                              a.[_VolumeId],
                                              CONCAT(a.[IPAddress], CASE WHEN a.[Name] IS NULL OR  a.[Name] =''  THEN '' ELSE CONCAT(':',a.[Name])END ) as [Path],
                                              a.[Size] as [TotalSize],
                                              a.[Used] as [FileSystemUsed],
                                              a.[UsedPercent] as [UsedPercent] ,
                                              a.[RunOut] as [RunOut],
                                              '/Orion/StatusIcon.ashx?entity={0}&status=' + ToString(ISNULL(a.Status, 0)) +'&size=small' AS [_IconFor_Path]                                              
                                            FROM
                                            (
                                            SELECT t.Volumes.VolumeId as [_VolumeId],
                                            	t.Volumes.Node.IPAddress as [IPAddress],
                                            	'' as [Name],
                                            	AVG(t.Volumes.VolumeSize) as [Size],
				        	                    AVG(t.Volumes.VolumeSpaceUsed) as [Used],
                                                CASE WHEN Min(t.Volumes.VolumeSize) = 0 OR ISNULL(Min(t.Volumes.VolumeSize),0) = 0 THEN NULL
                                            	    ELSE AVG((t.Volumes.VolumeSpaceUsed * 1.0 / t.Volumes.VolumeSize) * 100) END AS [UsedPercent],
						                        AVG(HourDiff(GetUtcDate(), l.CapacityRunout) / 24) as [RunOut],
						                        t.Volumes.Status as [Status]
                                            FROM Orion.SRM.Topology t INNER JOIN Orion.SRM.LUNs l ON t.NetObjectId = l.LUNID
                                            WHERE t.NetObjectType = '{1}'
                                            GROUP BY t.Volumes.VolumeId,t.Volumes.Node.IPAddress, t.Volumes.Status
                                            
                                            UNION ALL
                                            (
                                            SELECT t.Volumes.VolumeId as [_VolumeId],
                                            	t.Volumes.Node.IPAddress as [IPAddress],
                                            	t.Volumes.VolumeDescription as [Name],
                                            	AVG(t.Volumes.VolumeSize) as [Size],
                                            	AVG(t.Volumes.VolumeSpaceUsed) as [Used],
                                            	CASE WHEN Min(t.Volumes.VolumeSize) = 0 OR ISNULL(Min(t.Volumes.VolumeSize),0) = 0 THEN NULL
                                            	    ELSE AVG((t.Volumes.VolumeSpaceUsed * 1.0 / t.Volumes.VolumeSize) * 100) END AS [UsedPercent],
						                        AVG(HourDiff(GetUtcDate(), sh.Volume.CapacityRunout) / 24) as [RunOut],
						                        t.Volumes.Status as [Status]
                                            FROM Orion.SRM.Topology t INNER JOIN Orion.SRM.FileShares sh ON t.NetObjectId = sh.FileShareId
                                            WHERE t.NetObjectType = '{2}'
                                            GROUP BY t.Volumes.VolumeId, t.Volumes.Node.IPAddress, t.Volumes.VolumeDescription, t.Volumes.Status
                                            )
                                            ) a";

    #endregion


    #region Properties

    /// <summary>
    /// Override default title property.
    /// </summary>
    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.CapacityServerVolumesResourceTitle;
        }
    }

    /// <summary>
    /// Override help link fragment property.
    /// </summary>
    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceServerVolumesCapacitySummary";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
    }

    /// <summary>
    /// Determinate if resource is internal.
    /// </summary>
    public bool IsInternal
    {
        get
        {
            return true;
        }
    }

    /// <summary>
    /// Gets or sets Search Control property.
    /// </summary>
    public ResourceSearchControl SearchControl
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets warning threshold days for projected runout.
    /// </summary>
    public int WarningThresholdDays
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets critical threshold days for projected runout.
    /// </summary>
    public int CriticalThresholdDays
    {
        get;
        set;
    }

    /// <summary>
    /// Gets additional query for search control.
    /// </summary>
    public string SearchSwql
    {
        get
        {
            return string.Concat(query, @" WHERE Concat( a.[IPAddress],':',a.[Name]) LIKE '%${SEARCH_STRING}%'");
        }
    }

    #endregion


    #region Methods

    /// <summary>
    /// OnLoad override method.
    /// </summary>
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        Wrapper.HeaderButtons.Controls.Add(SearchControl);

        WarningThresholdDays = (int)SettingsDAL.GetSetting(SRMConstants.WarningThresholdDays).SettingValue;
        CriticalThresholdDays = (int)SettingsDAL.GetSetting(SRMConstants.CriticalThresholdDays).SettingValue;

        query = InvariantString.Format(queryTemplate, SwisEntities.Volume, Lun.NetObjectPrefix, "SMS");

        CustomTable.UniqueClientID = ScriptFriendlyResourceId;
        CustomTable.SWQL = query;
        CustomTable.SearchSWQL = SearchSwql;
    }

    #endregion

}