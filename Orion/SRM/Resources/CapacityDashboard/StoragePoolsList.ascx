﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StoragePoolsList.ascx.cs" Inherits="Orion_SRM_Resources_CapacityDashboard_StoragePoolsList" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Resources" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<orion:Include ID="Include1" runat="server" Module="SRM" File="SRM.Common.js" />
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include ID="Include3" runat="server" Module="SRM" File="SRM.css" />
<orion:Include ID="Include4" runat="server" Module="SRM" File="CapacityRisk.css" />

<orion:resourceWrapper runat="server" CssClass="SRM_HiddenField" ID="Wrapper">
    <Content>
        <div class="srm-storagepools">
            <orion:CustomQueryTable runat="server" ID="CustomTable" />
        </div>
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: '<%= CustomTable.UniqueClientID %>',
                        initialPage: 0,
                        rowsPerPage: <%= this.GetPageSize() %>,
                        allowSort: true,
                        columnSettings: {
                            "Name": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.CapacityDashboard_StoragePools_Name %>'),
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_Icon_Cell srm_word_wrap";
                                }
                            },
                            "Type": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.CapacityDashboard_StoragePools_Type %>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatPoolType(value);
                                }
                            },
                            "TotalSize": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.CapacityDashboard_StoragePools_TotalSize %>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "SRM_NoWrap"; }
                            },
                            "CapacityUserUsed": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.CapacityDashboard_StoragePools_ProvisionedCapacity %>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_NoWrap " + SW.SRM.Formatters.FormatCellStyle(rowArray[5],
                                        <%=ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.PoolProvisionedPercent)%>,
                                        <%=ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.PoolProvisionedPercent)%>);
                                }
                            },
                            "SubscribedCapacity": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.CapacityDashboard_StoragePools_SubscribedCapacity %>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "SRM_NoWrap"; }
                            },
                            "OverSubscribedCapacity": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.CapacityDashboard_StoragePools_OverSubscribedCapacity %>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "SRM_NoWrap"; }
                            },
                            "ProjectedRunOut": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.CapacityDashboard_StoragePools_ProjectedRunOut %>'),
                                isHtml: true,
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatDaysDiff(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return SW.SRM.Formatters.FormatCapacityRunoutCellStyle(cellValue, <%= WarningThresholdDays %>, <%= CriticalThresholdDays %>);
                                }
                            }
                        },
                        searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                        searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                        onLoad: function(rows, columnsInfo) {
                            var wrapper = $("div[resourceid='" + <%=this.Resource.ID%> + "']");
                            SW.SRM.Common.SetVisible(wrapper, rows.length != 0 || wrapper.hasClass('GridLoaded'));
                            wrapper.addClass('GridLoaded');
                        }
                    });
                $('#OrderBy-<%= CustomTable.UniqueClientID %>').val('[TotalSize] DESC');
                SW.Core.Resources.CustomQuery.refresh("<%=CustomTable.UniqueClientID %>");
            });

        </script>
    </Content>
</orion:resourceWrapper>