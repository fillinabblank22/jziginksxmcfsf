﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ServerVolumesByCapacity.ascx.cs" Inherits="Orion_SRM_Resources_CapacityDashboard_ServerVolumesByCapacity" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Resources" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include ID="Include3" runat="server" Module="SRM" File="SRM.css" />

<orion:ResourceWrapper runat="server" CssClass="SRM_HiddenField" ID="Wrapper">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: '<%= CustomTable.UniqueClientID %>',
                        initialPage: 0,
                        rowsPerPage: <%= this.GetPageSize() %>,
                        allowSort: true,
                        columnSettings: {
                            "Path": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.CapacityDashboard_ServerVolumes_VolumePath%>'),
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_Icon_Cell srm_word_wrap";
                                }
                            },
                            "TotalSize": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.CapacityDashboard_ServerVolumes_TotalSize%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "SRM_NoWrap"; }
                            },
                            "FileSystemUsed": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.CapacityDashboard_ServerVolumes_FileSystemUsedCapacity%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_NoWrap " +  SW.SRM.Formatters.FormatCellStyle(rowArray[4],
                                        <%=ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.VolumeFileSystemUsedPercent)%>,
                                        <%=ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.VolumeFileSystemUsedPercent)%>);
                                }
                            },
                            "UsedPercent": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.CapacityDashboard_ServerVolumes_FileSystemUsedPercentage%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatPercentageColumnValue(value, 0);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_NoWrap " + 
                                        SW.SRM.Formatters.FormatCellStyle(cellValue,
                                        <%=ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.VolumeFileSystemUsedPercent)%>,
                                        <%=ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.VolumeFileSystemUsedPercent)%>);
                                }
                            },
                            "RunOut": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.CapacityDashboard_ServerVolumes_Runout %>'),
                                 isHtml: true,
                                 formatter: function (value) {
                                     return SW.SRM.Formatters.FormatDaysDiff(value);
                                 },
                                 cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                     return SW.SRM.Formatters.FormatCapacityRunoutCellStyle(cellValue, <%= WarningThresholdDays %>, <%= CriticalThresholdDays %>);
                                }
                             }
                        },
                        searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                        searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                        onLoad: function(rows, columnsInfo) {
                            var wrapper = $("div[resourceid='" + <%=this.Resource.ID%> + "']");
                            SW.SRM.Common.SetVisible(wrapper, rows.length != 0 || wrapper.hasClass('GridLoaded'));
                            wrapper.addClass('GridLoaded');
                        }
                    });
                $('#OrderBy-<%= CustomTable.UniqueClientID %>').val('[TotalSize] DESC');
                SW.Core.Resources.CustomQuery.refresh('<%=CustomTable.UniqueClientID %>');
            });
        </script>
    </Content>
</orion:ResourceWrapper>
