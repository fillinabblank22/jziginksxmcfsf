﻿using System;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;

public partial class Orion_SRM_Resources_CapacityDashboard_NasVolumesList : SrmBaseResourceControl, IResourceIsInternal
{
    #region Fields

    private string query;

    private const string queryTemplateThin = @"SELECT v.Caption AS [Name],
                                                '/Orion/StatusIcon.ashx?entity={0}&status=' + ToString(ISNULL(v.Status, 0)) +'&size=small' AS [_IconFor_Name],
                                                '/Orion/View.aspx?NetObject={1}:' + ToString(v.VolumeID) AS [_LinkFor_Name],
                                                Concat('%{1}:', v.VolumeID,'%') As [PoolName],
                                                ISNULL(v.CapacityTotal, '') AS [TotalSize],
                                                ISNULL(v.CapacityAllocated, '') AS [CapacityAllocated],                                                
                                                CASE WHEN 
                                                        v.CapacityTotal is Null 
                                                     OR v.CapacityTotal = 0 
                                                     OR v.CapacityAllocated is Null
                                                     THEN NULL
                                                     ELSE ((v.CapacityAllocated * 1.0 / v.CapacityTotal) * 100) END AS [CapacityAllocatedPercent],
                                                v.CapacityFileSystem AS [FileSystemUsed],
                                                CASE WHEN 
                                                        v.CapacityTotal is Null 
                                                     OR v.CapacityTotal = 0 
                                                     OR v.CapacityFileSystem is Null
                                                     THEN NULL
                                                     ELSE ((v.CapacityFileSystem * 1.0 / v.CapacityTotal) * 100) END AS [FileSystemUsedPercent],
                                                DayDiff(getUtcDate(), v.CapacityRunout) AS [CapacityRunout],
                                                Concat('{1}:', v.VolumeID) AS [_NetObjectID]
                                                FROM Orion.SRM.Volumes (nolock=true) v
                                                WHERE v.Thin = 1";

    private const string queryTemplateThick = @"SELECT v.Caption AS [Name],
                                                '/Orion/StatusIcon.ashx?entity={0}&status=' + ToString(ISNULL(v.Status, 0)) +'&size=small' AS [_IconFor_Name],
                                                '/Orion/View.aspx?NetObject={1}:' + ToString(v.VolumeID) AS [_LinkFor_Name],
                                                Concat('%{1}:', v.VolumeID,'%') As [PoolName],
                                                ISNULL(v.CapacityTotal, '') AS [TotalSize],
                                                v.CapacityFileSystem AS [FileSystemUsed],
                                                CASE WHEN 
                                                        v.CapacityTotal is Null 
                                                     OR v.CapacityTotal = 0 
                                                     OR v.CapacityFileSystem is Null
                                                     THEN NULL
                                                     ELSE ((v.CapacityFileSystem * 1.0 / v.CapacityTotal) * 100) END AS [FileSystemUsedPercent],
                                                DayDiff(getUtcDate(), v.CapacityRunout) AS [CapacityRunout],
                                                Concat('{1}:', v.VolumeID) AS [_NetObjectID]
                                                FROM Orion.SRM.Volumes (nolock=true) v
                                                WHERE v.Thin = 0";

    #endregion


    #region Properties

    /// <summary>
    /// Gets or sets default title.
    /// </summary>
    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.CapacityDashboard_NASVolumesResourceTitle;
        }
    }

    /// <summary>
    /// Override help link fragment property.
    /// </summary>
    public override string HelpLinkFragment
    {
        get
        {
            return IsThick ? "SRMPHResourceThickVolumesCapacitySummary" : "SRMPHResourceThinVolumesCapacitySummary";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
    }

    /// <summary>
    /// Determinate if resource is internal.
    /// </summary>
    public bool IsInternal
    {
        get
        {
            return true;
        }
    }

    /// <summary>
    /// Gets or sets search control resource property.
    /// </summary>
    public ResourceSearchControl SearchControl
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets warning threshold days for projected runout.
    /// </summary>
    public int WarningThresholdDays
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets critical threshold days for projected runout.
    /// </summary>
    public int CriticalThresholdDays
    {
        get;
        set;
    }

    /// <summary>
    /// Additional query for swql query.
    /// </summary>
    public string SearchSWQL
    {
        get
        {
            return string.Concat(query, " AND v.Caption LIKE '%${SEARCH_STRING}%'");
        }
    }

    /// <summary>
    /// Determinate is pool is thick.
    /// </summary>
    public bool IsThick
    {
        get;
        set;
    }

    #endregion


    #region Methods

    /// <summary>
    /// OnLoad resource event handler.
    /// </summary>
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        SearchControl = (ResourceSearchControl) LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        Wrapper.HeaderButtons.Controls.Add(SearchControl);

        IsThick = Convert.ToBoolean(Resource.Properties["IsThick"]);

        query = InvariantString.Format(IsThick ? queryTemplateThick : queryTemplateThin, SwisEntities.Volume, Volume.NetObjectPrefix, Pool.NetObjectPrefix);

        WarningThresholdDays = (int) SettingsDAL.GetSetting(SRMConstants.WarningThresholdDays).SettingValue;
        CriticalThresholdDays = (int) SettingsDAL.GetSetting(SRMConstants.CriticalThresholdDays).SettingValue;

        CustomTable.UniqueClientID = ScriptFriendlyResourceId;
        CustomTable.SWQL = query;
        CustomTable.SearchSWQL = SearchSWQL;
    }

    #endregion
}