﻿using System;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Web;
using SolarWinds.SRM.Web.NetObjects;
using SolarWinds.SRM.Web.UI;

public partial class Orion_SRM_Resources_CapacityDashboard_StoragePoolsList : SrmBaseResourceControl, IResourceIsInternal
{
    #region Fields

    private string query;

    private const string queryTemplateThin = @"SELECT p.Caption as [Name],  
                                                '/Orion/StatusIcon.ashx?entity={0}&status=' +ToString(ISNULL(p.Status, 0)) +'&size=small' AS [_IconFor_Name],
                                                '/Orion/View.aspx?NetObject={1}:' + ToString(p.PoolID) AS [_LinkFor_Name],
                                                p.Type as [Type],
                                                ISNULL(p.CapacityUserTotal, '') AS [TotalSize],

                                                CASE WHEN 
                                                    p.CapacityUserTotal is Null 
                                                    OR p.CapacityUserTotal = 0 
                                                    OR p.CapacityUserUsed is Null
                                                 THEN NULL
                                                 ELSE ((p.CapacityUserUsed * 1.0 / p.CapacityUserTotal) * 100) END AS [_CapacityUserUsedPercentage],
                
                                                ISNULL(p.CapacityUserUsed, '') AS [CapacityUserUsed],
                                                ISNULL(p.CapacitySubscribed, '') AS [SubscribedCapacity],
                                                ISNULL(p.CapacityOversubscribed, '') AS [OverSubscribedCapacity],
                                                DayDiff(getUtcDate(), p.CapacityRunout) AS [ProjectedRunOut]
                                                FROM Orion.SRM.Pools (nolock=true) p
                                                WHERE p.Thin = 1";

    private const string queryTemplateThick = @"SELECT p.Caption as [Name],  
                                                '/Orion/StatusIcon.ashx?entity={0}&status=' +ToString(ISNULL(p.Status, 0)) +'&size=small' AS [_IconFor_Name],
                                                '/Orion/View.aspx?NetObject={1}:' + ToString(p.PoolID) AS [_LinkFor_Name],
                                                p.Type as [Type],
                                                ISNULL(p.CapacityUserTotal, '') AS [TotalSize],

                                                CASE WHEN 
                                                    p.CapacityUserTotal is Null 
                                                    OR p.CapacityUserTotal = 0 
                                                    OR p.CapacityUserUsed is Null
                                                 THEN NULL
                                                 ELSE ((p.CapacityUserUsed * 1.0 / p.CapacityUserTotal) * 100) END AS [_CapacityUserUsedPercentage],

                                                ISNULL(p.CapacityUserUsed, '') AS [CapacityUserUsed]
                                                FROM Orion.SRM.Pools (nolock=true) p
                                                WHERE p.Thin = 0";

    #endregion


    #region Properties

    /// <summary>
    /// Gets or sets default title.
    /// </summary>
    protected override string DefaultTitle
    {
        get
        {
            return SrmWebContent.CapacityDashboard_StoragePools_ResourceTitle;
        }
    }

    /// <summary>
    /// Override help link fragment property.
    /// </summary>
    public override string HelpLinkFragment
    {
        get
        {
            return IsThick ? "SRMPHResourcePoolsCapacitySummary" : "SRMPHResourceThinPools";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/SRM/Controls/EditResourceControls/EditGridResourceControl.ascx"; }
    }

    /// <summary>
    /// Determinate if resource is internal.
    /// </summary>
    public bool IsInternal
    {
        get
        {
            return true;
        }
    }

    /// <summary>
    /// Gets or sets search control resource property.
    /// </summary>
    public ResourceSearchControl SearchControl
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets warning threshold days for projected runout.
    /// </summary>
    public int WarningThresholdDays
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets critical threshold days for projected runout.
    /// </summary>
    public int CriticalThresholdDays
    {
        get;
        set;
    }

    /// <summary>
    /// Additional query for swql query.
    /// </summary>
    public string SearchSWQL
    {
        get
        {
            return string.Concat(query, " AND p.Caption LIKE '%${SEARCH_STRING}%'");
        }
    }

    /// <summary>
    /// Determinate is pool is thick.
    /// </summary>
    public bool IsThick
    {
        get
        {
            return Convert.ToBoolean(Resource.Properties["IsThick"]);
        }
    }

    #endregion


    #region Methods

    /// <summary>
    /// OnLoad resource event handler.
    /// </summary>
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        SearchControl = (ResourceSearchControl) LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        Wrapper.HeaderButtons.Controls.Add(SearchControl);

        query = InvariantString.Format(IsThick ? queryTemplateThick : queryTemplateThin, SwisEntities.Pools, Pool.NetObjectPrefix);

        WarningThresholdDays = (int) SettingsDAL.GetSetting(SRMConstants.WarningThresholdDays).SettingValue;
        CriticalThresholdDays = (int) SettingsDAL.GetSetting(SRMConstants.CriticalThresholdDays).SettingValue;

        CustomTable.UniqueClientID = ScriptFriendlyResourceId;
        CustomTable.SWQL = query;
        CustomTable.SearchSWQL = SearchSWQL;
    }

    #endregion
}