﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NasVolumesList.ascx.cs" Inherits="Orion_SRM_Resources_CapacityDashboard_NasVolumesList" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Resources" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<orion:Include ID="Include4" runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include ID="Include1" runat="server" Module="SRM" File="SRM.Common.js" />
<orion:Include ID="include2" runat="server" File="OrionMinReqs.js" />
<orion:Include ID="include3" runat="server" File="OrionCore.js" />
<orion:Include ID="Include5" runat="server" Module="SRM" File="SRM.css" />

<orion:ResourceWrapper runat="server" CssClass="SRM_HiddenField" ID="Wrapper">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: '<%= CustomTable.UniqueClientID %>',
                        initialPage: 0,
                        rowsPerPage: <%= this.GetPageSize() %>,
                        allowSort: true,
                        columnSettings: {
                            "Name": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.CapacityDashboard_NasVolumes_Name %>'),
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_Icon_Cell srm_word_wrap";
                                }
                            },
                            "PoolName": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.CapacityDashboard_NasVolumes_PoolName %>'),
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "VolumeListAssociatedPools<%=this.Resource.ID%> SRM_HiddenGridCell srm_word_wrap"; 
                                }
                            },
                            "TotalSize": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.CapacityDashboard_NasVolumes_TotalSize %>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "SRM_NoWrap"; }
                            },
                            "CapacityAllocated": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.CapacityDashboard_NasVolumes_ProvisionedCapacity %>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    var capacityAllocatedPercent = rowArray[rowArray.length - 5];
                                    return "SRM_NoWrap " + SW.SRM.Formatters.FormatCellStyle(capacityAllocatedPercent,
                                        <%=ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.VolumeProvisionedPercent)%>,
                                        <%=ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.VolumeProvisionedPercent)%>);
                                }
                            },
                            "CapacityAllocatedPercent": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.CapacityDashboard_NasVolumes_ProvisionedCapacityPercent %>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatPercentageColumnValue(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_NoWrap " + 
                                        SW.SRM.Formatters.FormatCellStyle(cellValue,
                                        <%=ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.VolumeProvisionedPercent)%>,
                                        <%=ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.VolumeProvisionedPercent)%>);
                                }
                            },
                            "FileSystemUsed": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.CapacityDashboard_NasVolumes_FileSystemUsed %>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    var fileSystemUsedPercent = rowArray[rowArray.length - 3];
                                    return "SRM_NoWrap " + SW.SRM.Formatters.FormatCellStyle(fileSystemUsedPercent,
                                        <%=ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.VolumeFileSystemUsedPercent)%>,
                                        <%=ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.VolumeFileSystemUsedPercent)%>);
                                }
                            },
                            "FileSystemUsedPercent": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.CapacityDashboard_NasVolumes_FileSystemUsedInPercent %>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatPercentageColumnValue(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_NoWrap " + 
                                        SW.SRM.Formatters.FormatCellStyle(cellValue,
                                        <%=ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.VolumeFileSystemUsedPercent)%>,
                                        <%=ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.VolumeFileSystemUsedPercent)%>);
                                }
                            },
                            "CapacityRunout": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%= SrmWebContent.CapacityDashboard_NasVolumes_Runout %>'),
                                isHtml: true,
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatDaysDiff(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return SW.SRM.Formatters.FormatCapacityRunoutCellStyle(cellValue, <%= WarningThresholdDays %>, <%= CriticalThresholdDays %>);
                                }
                            }
                        },
                        searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                        searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                        onLoad: function(rows, columnsInfo) {
                            var wrapper = $("div[resourceid='" + <%=this.Resource.ID%> + "']");
                            SW.SRM.Common.SetVisible(wrapper, rows.length != 0 || wrapper.hasClass('GridLoaded'));
                            wrapper.addClass('GridLoaded');
                            var params = {
                                resourceID: <%=this.Resource.ID%>,
                                netObjectFieldName:"_NetObjectID",
                                rows:  rows, 
                                columnsInfo: columnsInfo,
                                classesName: {
                                    associatedPools:"VolumeListAssociatedPools"
                                }
                            };
                            SW.SRM.Common.FillAdditionColumns(params);
                        }
                    });

                $('#OrderBy-<%= CustomTable.UniqueClientID %>').val('[TotalSize] DESC');
                SW.Core.Resources.CustomQuery.refresh("<%=CustomTable.UniqueClientID %>");
            });
            
        </script>
    </Content>
</orion:ResourceWrapper>
