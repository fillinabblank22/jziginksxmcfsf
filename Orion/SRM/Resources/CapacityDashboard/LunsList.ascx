﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LunsList.ascx.cs" Inherits="Orion_SRM_Resources_CapacityDashboard_LunsList" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.SRM.Common.Enums" %>
<%@ Import Namespace="SolarWinds.SRM.Web.Resources" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<orion:Include ID="Include2" runat="server" Module="SRM" File="SRM.Formatters.js" />
<orion:Include ID="Include1" runat="server" Module="SRM" File="SRM.Common.js" />
<orion:Include ID="include4" runat="server" File="OrionMinReqs.js" />
<orion:Include ID="include5" runat="server" File="OrionCore.js" />
<orion:Include ID="Include3" runat="server" Module="SRM" File="SRM.css" />

<orion:ResourceWrapper runat="server" CssClass="SRM_HiddenField" ID="Wrapper">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: '<%= CustomTable.UniqueClientID %>',
                        initialPage: 0,
                        rowsPerPage: <%= this.GetPageSize() %>,
                        allowSort: true,
                        columnSettings: {
                            "Name": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.CapacityDashboard_Luns_LunName%>'),
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_Icon_Cell srm_word_wrap";
                                }
                            },
                            "PoolName": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.CapacityDashboard_Luns_StoragePool%>'),
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "LunListAssociatedPools<%=this.Resource.ID%> SRM_HiddenGridCell srm_word_wrap"; 
                                }
                            },
                            "AssociatedEndpoint": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.CapacityDashboard_Luns_AssociatedEndpoint%>'),
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "LunListAssociatedEndpoint<%=this.Resource.ID%> SRM_HiddenGridCell srm_word_wrap"; 
                                }
                            },
                            "TotalSize": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.CapacityDashboard_Luns_TotalSize%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) { return "SRM_NoWrap"; }
                            },
                            "CapacityAllocated": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.CapacityDashboard_Luns_ProvisionedCapacity%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatCapacity(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_NoWrap " + SW.SRM.Formatters.FormatCellStyle(rowArray[7],
                                        <%=ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.LunProvisionedPercent)%>,
                                        <%=ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.LunProvisionedPercent)%>);
                                }
                            },
                            "CapacityAllocatedPercentage": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.CapacityDashboard_Luns_ProvisionedPercentage%>'),
                                formatter: function (value) {
                                    return SW.SRM.Formatters.FormatPercentageColumnValue(value);
                                },
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_NoWrap " + 
                                        SW.SRM.Formatters.FormatCellStyle(cellValue,
                                        <%=ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.LunProvisionedPercent)%>,
                                        <%=ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.LunProvisionedPercent)%>);
                                }
                            },
                            "FileSystemUsedCapacity": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.CapacityDashboard_Luns_FileSystemUsedCapacity%>'),
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_NoWrap LunListFileSystemUsedCapacity<%=this.Resource.ID%> SRM_HiddenGridCell"; 
                                }
                            },
                            "FileSystemUsedPercentage": {
                                header: SW.SRM.Formatters.FormatColumnTitle('<%=SrmWebContent.CapacityDashboard_Luns_FileSystemUsedPercentage%>'),
                                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                                    return "SRM_NoWrap LunListFileSystemUsedPercentage<%=this.Resource.ID%> SRM_HiddenGridCell"; 
                                }
                            }
                        },
                        searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                        searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                        onLoad: function(rows, columnsInfo) {
                            var wrapper = $("div[resourceid='" + <%=this.Resource.ID%> + "']");
                            SW.SRM.Common.SetVisible(wrapper, rows.length != 0 || wrapper.hasClass('GridLoaded'));
                            wrapper.addClass('GridLoaded');
                            var params = {
                                resourceID: <%=this.Resource.ID%>,
                                netObjectFieldName:"_NetObjectID",
                                rows:  rows, 
                                columnsInfo: columnsInfo,
                                volumeSpacePercentWarning:<%=ValuesThresholdDataType.GetThresholdWarningValue(ThresholdDataType.LunFileSystemUsedPercent)%>,
                                volumeSpacePercentCritical:<%=ValuesThresholdDataType.GetThresholdCriticalValue(ThresholdDataType.LunFileSystemUsedPercent)%>,
                                classesName: {
                                    associatedPools:"LunListAssociatedPools",
                                    associatedEndpoint: "LunListAssociatedEndpoint",
                                    volumeSpaceUsed: "LunListFileSystemUsedCapacity",
                                    volumeSpacePercent: "LunListFileSystemUsedPercentage"
                                }
                            };
                            SW.SRM.Common.FillAdditionColumns(params);
                        }
                    });
                $('#OrderBy-<%= CustomTable.UniqueClientID %>').val('[TotalSize] DESC');
                SW.Core.Resources.CustomQuery.refresh('<%=CustomTable.UniqueClientID %>');
            });
        </script>
    </Content>
</orion:ResourceWrapper>
