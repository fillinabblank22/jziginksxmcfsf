﻿using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.UI;
using SolarWinds.SRM.Common;
using SolarWinds.SRM.Common.ServiceProvider;
using SolarWinds.SRM.Web.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Web.UI;
using SolarWinds.SRM.Web.UI;

public partial class Orion_SRM_Resources_ActiveAlerts : AlertsAndEventsBaseResourceControl
{
    private IInformationServiceProxy proxyService;

    protected void Page_Load(object sender, EventArgs e)
    {
        string showAck = Resource.Properties["ShowAcknowledgedAlerts"];
        AlertsTable.ShowAcknowledgedAlerts = !string.IsNullOrEmpty(showAck) && showAck.Equals(bool.TrueString, StringComparison.OrdinalIgnoreCase);
        AlertsTable.ResourceID = Resource.ID;

        int rowsPerPage;
        if (!int.TryParse(Resource.Properties["RowsPerPage"], out rowsPerPage))
            rowsPerPage = 5;

        AlertsTable.InitialRowsPerPage = rowsPerPage;

        AddShowAllCurrentlyTriggeredAlertsControl(wrapper);

        if (!AppropriateNetObjectType())
        {
            Visible = false;
            return;
        }

        PopulateAlerts();
    }

    private void PopulateAlerts()
    {
        proxyService = (IInformationServiceProxy)ServiceLocator.GetInstance<IInformationServiceProxyFactory>().Create();

        var triggeringObjectEntityUris = new List<String>();

        var entityName = InvariantString.Format("Orion.SRM.{0}", NetObject.Table);
        var keyPropertyName = GetKeyPropertyName(entityName);

        if (!NetObject.ObjectProperties.ContainsKey(keyPropertyName))
        {
            NetObject.ObjectProperties.Add(keyPropertyName, NetObject.GetIdentifier());
        }

        AddNetObjectUri(triggeringObjectEntityUris, entityName, keyPropertyName, NetObject.ObjectProperties[keyPropertyName]);

        AlertsTable.TriggeringObjectEntityNames = new List<string>(1) { string.Empty };
        AlertsTable.TriggeringObjectEntityUris = triggeringObjectEntityUris;
    }

    private String GetKeyPropertyName(String entityName)
    {
        return proxyService.Query(
           InvariantString.Format("SELECT Name FROM Metadata.Property WHERE EntityName = '{0}' AND IsKey = true",
               entityName)).Rows[0][0].ToString();
    }

    private void AddNetObjectUri(List<String> triggeringObjectEntityUris, String entityName, string keyPropertyName, object keyValue)
    {
        if (keyValue != null && keyValue != DBNull.Value)
        {
            var swisQuery = InvariantString.Format("SELECT Uri FROM {0} WHERE {1} = @entityID", entityName, keyPropertyName);
            triggeringObjectEntityUris.Add(
                proxyService.Query(swisQuery, new Dictionary<string, object>() { { "entityID", keyValue } }).Rows[0][0].ToString()
            );
        }
    }

    protected override string DefaultTitle
    {
        get { return (Resource != null) ? Resource.Title : String.Empty; }
    }

    private void AddShowAllCurrentlyTriggeredAlertsControl(Control resourceWrapperContents)
    {
        if (resourceWrapperContents.TemplateControl is IResourceWrapper)
        {
            IResourceWrapper wrapper = resourceWrapperContents.TemplateControl as IResourceWrapper;
            wrapper.ManageButtonText = Resources.CoreWebContent.WEBDATA_BV0_0043;
            wrapper.ManageButtonTarget = "/Orion/NetPerfMon/Alerts.aspx";
            wrapper.ShowManageButton = !OrionConfiguration.IsDemoServer;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "SRMREactiveAlertsOnThisStorageObject";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditAllAlerts.ascx"; }
    }
}