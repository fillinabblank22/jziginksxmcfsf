using System;
using System.Text;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using System.Collections.Specialized;
using SolarWinds.Orion.Web.DAL;
using System.Data;
using System.Web;
using System.Threading;
using SolarWinds.Orion.Web.InformationService;

public partial class View : System.Web.UI.Page
{
    private string NetObjectID
    {
        get { return Request.QueryString["NetObject"]; }
    }

    private string ViewType
    {
        get { return Request.QueryString["View"]; }
    }

    private string ViewName
    {
        get { return Request.QueryString["ViewName"]; }
    }

    private string ViewKey
    {
        get { return Request.QueryString["ViewKey"]; }
    }


    private int ViewID
    {
        get
        {
            try
            {
                return Convert.ToInt32(Request.QueryString["ViewID"]);
            }
            catch
            {
                return 0;
            }
        }
    }

    public void Page_Load()
    {
        try
        {
            if ((Request.UrlReferrer != null) && Request.UrlReferrer.LocalPath.Equals("/Orion/Login.aspx", StringComparison.InvariantCultureIgnoreCase)) {
                CheckRedirectToDiscovery();
            }

            CheckRedirectToDashboard();

            Response.Redirect(this.BuildPagePath());
        }
        catch (ThreadAbortException) { 
            //ignore
        }
        
    }

    private string GetBasePath()
    {
        string defaultPath = "/Orion/SummaryView.aspx";

        if (!string.IsNullOrEmpty(this.ViewType))
        {
            string tmpPath = ViewManager.GetPagePathByViewType(this.ViewType);
            if (!string.IsNullOrEmpty(tmpPath))
                return tmpPath;
        }

        if (!string.IsNullOrEmpty(this.ViewName))
        {
            foreach (ViewInfo info in ViewManager.GetAllViews())
            {
                if (info.ViewTitle.Equals(this.ViewName, StringComparison.InvariantCultureIgnoreCase))
                {
                    return info.PagePath;
                }
            }
        }

        if (!string.IsNullOrEmpty(this.ViewKey))
        {
            foreach (ViewInfo info in ViewManager.GetAllViews())
            {
                if (info.ViewKey.Equals(this.ViewKey, StringComparison.OrdinalIgnoreCase))
                {
                    return info.PagePath;
                }
            }
        }

        if (this.ViewID > 0)
        {
            ViewInfo info = ViewManager.GetViewById(this.ViewID);
            if (null != info)
                return info.PagePath;
        }

        return defaultPath;
    }

    private string BuildPagePath()
    {
        StringBuilder outStr = new StringBuilder();

        // StringDictionary converts all keys to lowercase
        StringDictionary queryParameters = new StringDictionary();

        if (string.IsNullOrEmpty(ViewType) && string.IsNullOrEmpty(ViewName) && ViewID <= 0 && string.IsNullOrEmpty(NetObjectID) && string.IsNullOrEmpty(ViewKey))
        {
            // Nothing specified. Use the user's home page
            int viewId = Profile.HomePageViewID;
            ViewInfo info = ViewManager.GetViewById(viewId);
            if (null != info)
            {
                outStr.Append(info.PagePath);
                queryParameters["ViewID"] = viewId.ToString();
                if (!String.IsNullOrWhiteSpace(Profile.DefaultNetObjectID))
                    queryParameters["NetObject"] = Profile.DefaultNetObjectID;
            }
        }

        if (outStr.Length == 0 && !string.IsNullOrEmpty(this.NetObjectID))
        {
            string url = ViewProxyDictionary.Instance.GetUrlFromNetObjectId(this.NetObjectID);
            if (!string.IsNullOrEmpty(url))
                return url;
        }

        if (outStr.Length == 0)
        {
            outStr.Append(this.GetBasePath());

            if (!string.IsNullOrEmpty(this.ViewName))
                queryParameters["ViewName"] = this.ViewName;

            if (this.ViewID > 0)
                queryParameters["ViewID"] = this.ViewID.ToString();

            if (!string.IsNullOrEmpty(this.NetObjectID))
                queryParameters["NetObject"] = this.NetObjectID;

            if (!string.IsNullOrEmpty(this.ViewKey))
                queryParameters["ViewKey"] = this.ViewKey;
        }

        foreach (string queryParameter in Request.QueryString.Keys)
        {
            if (!queryParameters.ContainsKey(queryParameter))
                queryParameters[queryParameter] = Request.QueryString[queryParameter];
        }

        bool first = true;
        foreach (string param in queryParameters.Keys)
        {
            outStr.AppendFormat("{0}{1}={2}", first ? "?" : "&", string.Compare(param, "viewid", true) != 0 ? HttpUtility.UrlEncode(param) : "ViewID", HttpUtility.UrlEncode(queryParameters[param]));
            first = false;
        }

        return outStr.ToString();
    }

    private static bool IsAdminUser()
    {
        return (bool)HttpContext.Current.Profile.GetPropertyValue("AllowAdmin");
    }

    private static bool HasNodesInLicense()
    {
        return new FeatureManager().GetMaxElementCount(WellKnownElementTypes.Nodes) > 0;
    }

    private static bool HasZeroNodes()
    {
        var noAddedNodes = false;

        try
        {
            DataTable dt;

            using (var swis = InformationServiceProxy.CreateV3())
            {
                dt = swis.Query("SELECT COUNT(NodeID) as NodeCount FROM Orion.Nodes");
            }
           
                if ((dt.Rows.Count < 1) || (int.Parse((dt.Rows[0]["NodeCount"]).ToString()) <= 1))
                {
                    noAddedNodes = true;
                }
        }
        catch
        {
            // if swis fails then all bets are off (don't redirect)
        }

        return noAddedNodes;
    }

    private static bool DoNotRedirectUser()
    {
        var settingValue = WebUserSettingsDAL.Get("DoNotRedirectToDiscoveryWizard");

        if (string.IsNullOrEmpty(settingValue))
        {
            // user override is off by default
            WebUserSettingsDAL.Set("DoNotRedirectToDiscoveryWizard", "False");
            return false;
        }
        else
        {
            return (!settingValue.Equals("False"));
        }
    }

    private void CheckRedirectToDiscovery()
    {
        // redirection only applies to admin users, without added nodes but with nodes in license and who didn't opt out
        if (!IsAdminUser() || !HasZeroNodes() || !HasNodesInLicense() || DoNotRedirectUser()) return;
        try
        {
            Response.Redirect("/Orion/Admin/DiscoveryCentral.aspx");
        }
        catch (ThreadAbortException) { 
            //ignore
        }
        
    }

    private void CheckRedirectToDashboard()
    {
        if (string.IsNullOrEmpty(ViewType) && string.IsNullOrEmpty(ViewName) && ViewID <= 0 && string.IsNullOrEmpty(NetObjectID)
            && string.IsNullOrEmpty(ViewKey) && Profile.HomePageDashboardID > 0 && Profile.HomePageViewID < 0)
        {
            // redirect to dashboard instead of view
            Response.Redirect($"/apps/platform/dashboard/{Profile.HomePageDashboardID}");
        }
    }
}
