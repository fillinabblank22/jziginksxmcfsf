﻿
(function (LastCloudEventsEdit) {
    LastCloudEventsEdit.selectAll = function (invoker) {
        var tbody = $("#" + invoker.id).parents("tbody").first();
        var inputElements = $(tbody).find('input');
       
        for (var i = 0; i < inputElements.length; i++) {
            var myElement = inputElements[i];

            // Filter through the input types looking for checkboxes
            if (myElement.type === "checkbox") {

                // Use the invoker (our calling element) as the reference 
                //  for our checkbox status
                myElement.checked = invoker.checked;
            }
        }
    }

    LastCloudEventsEdit.setUpRootCheckbox = function (container) {
        var selectedAll = true;
        container.find('[id*=eventTypeElementCheckbox]').each(function () {
            selectedAll &= this.checked;
        });

        container.find('[id*=selectAllCheckbox]').each(function () {
            this.checked = selectedAll;
        });
    }
}(SW.Core.namespace("CLM.LastCloudEvents.Edit")));
