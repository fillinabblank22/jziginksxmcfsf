﻿SW = SW || {};
SW.CloudMonitoring = SW.CloudMonitoring || {};

SW.CloudMonitoring.PermissionsRequiredDialog = function() {

    var element = $("<div />");
    var message = "@{R=CloudMonitoring.Strings;K=Web_GettingStartedResource_AdminPermissionsRequiredMessage;E=js}";

    element.html(message);

    element.dialog({
        title: "@{R=CloudMonitoring.Strings;K=Web_GettingStartedResource_AdminPermissionsRequired;E=js}",
        modal: true,
        position: ["middle", "middle"],
        width: 600,
        heigh: "auto",
        minWidth: 500,
        buttons: [
            {
                text: "@{R=CloudMonitoring.Strings;K=Web_Close_Button;E=js}",
                click: function() {
                    $(this).dialog("close");
                }
            }
        ]
    });
};