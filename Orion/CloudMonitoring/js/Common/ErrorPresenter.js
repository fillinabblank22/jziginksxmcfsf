﻿var cloudCommon = SW.Core.namespace("SW.CloudMonitoring.Common");

(function (cloudCommon) {

    cloudCommon.ErrorPresenter = function (htmlPlaceholderElementId) {
        this.show = function (message) {
            $(htmlPlaceholderElementId).html(message);
            $(htmlPlaceholderElementId).show();
        };
        this.hide = function () {
            $(htmlPlaceholderElementId).hide();
            $(htmlPlaceholderElementId).html("");
        };
    };
}(cloudCommon))