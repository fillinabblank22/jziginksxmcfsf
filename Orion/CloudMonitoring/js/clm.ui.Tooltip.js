﻿(function ($) {
    var isHintShown;

    $.fn.setHint = function (cluetipOptions) {
        isHintShown = false;

        var options = {
            positionBy: 'fixed',
            local: true,
            cluetipClass: "cloudMonitoring",
            dropShadow: false,
            arrows: true,
            mouseOutClose: true,
            sticky: true,
            topOffset: -30,
            hoverIntent: 0
        };

        if (cluetipOptions != null)
            for (var attribute in cluetipOptions)
                options[attribute] = cluetipOptions[attribute];

        this.cluetip(options);
		
		var $clueTip = $("#cluetip");

        this.mouseout(function () {
			$clueTip.data("currentHoverElement", this);
			
            setTimeout(function () {
                if (isHintShown === false) {
                    $("#cluetip-close").trigger('click');
                    isHintShown = false;
                }
            }, 125);
        });

        $clueTip.mouseover(function () {
            isHintShown = true;
        });
        $clueTip.mouseout(function () {
            isHintShown = false;
        });
    }
})(jQuery);