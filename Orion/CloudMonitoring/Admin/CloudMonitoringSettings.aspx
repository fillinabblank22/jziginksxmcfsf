﻿<%@ Page Title="<%$ Resources: CloudMonitoringWebContent, CloudMonitoringSummary_CloudMonitoringSettings %>" MasterPageFile="~/Orion/CloudMonitoring/Admin/CloudMonitoringAdmin.master" Language="C#"CodeFile="CloudMonitoringSettings.aspx.cs" Inherits="Orion_CloudMonitoring_Admin_CloudMonitoringSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include runat="server" File="Admin.css" />
    <style type="text/css">
        #container{background-color: #dfe0e1 !important;}  
        #adminContent{background-color: #dfe0e1;}  
        #adminContent p { width: auto; padding: 0; }
       
        #adminContent * .sw-hdr-title{font-weight: 700;font-size: 18px;text-transform: capitalize;line-height: 15px;padding: 15px;margin-right: 15px;background: #ffffff;box-shadow: 0 1px 5px 0 #b9b9b9;}
        .sw-is-locale-de .sw-hdr-title {text-transform: none!important;}
        #adminContent * h2.sw-hdr-title{border-bottom: white 1px solid;margin-top: 0;}
        #adminContent * .column1of2 { padding-left:0px; padding-right:0px; vertical-align:top; text-align: left;}
        #adminContent * .column2of2 {padding: 0; vertical-align:top; text-align: left; width: 500px; background-color: #ffffff;box-shadow: 0 1px 5px 0 #b9b9b9;}
        #adminContent * .column1of2 * img{ width: 95px;height: 75px;} 
    
    
        #adminContent * .blockHeader{ font-weight: 700;font-size: 16px;margin-top: 0;}
        #adminContent * td.column2of2 * .iconContainer{ padding-right: 10px;vertical-align: top;}
        #adminContent * .sw-settings-rightbox-content * .paragraphHeader{font-weight: 700;margin-bottom: 5px;}
        #adminContent * .sw-settings-rightbox-content p {margin-top: 0;margin-bottom: 0;}
        #adminContent * .sw-settings-rightbox-content .subParagraph{ margin-top: 20px;}
        #adminContent * .sw-settings-rightbox-content .collapseParagraph { margin-bottom: 5px;}
        #adminContent * .sw-settings-rightbox-content .collapseParagraph > span, #adminContent * .sw-settings-rightbox-content .collapseParagraph > img { cursor: pointer;}
        #adminContent * .sw-settings-rightbox-content .subParagraph > a{ cursor: pointer;display: block;margin-bottom: 5px;}
        #adminContent * .sw-settings-delimiter { border-top: 1px solid #fff;}
    
        #helpFromHumans * .collapseParagraph > div {margin: 10px 0px 15px 18px;display: none;}
        #helpFromHumans * .collapseParagraph > div > p > span{color:#888;}
        #helpFromHumans * .collapseParagraph > img {vertical-align: top;}
    
        #productEducation * .NewContentBucket { box-shadow: none;padding: 0;margin: 0;}
        #productEducation * .NewBucketLinkContainer { margin: 0;padding: 0;}
        #productEducation * .LinkColumn { padding-top: 0;}
        </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="AdminPageContentPlaceHolder" runat="Server">
    <div id="adminContent">

        <table class="settingsTable" style="width: 100%;">
            <tr>
                <td class="column1of2">
                    <h1 class="sw-hdr-title"><%= Page.Title %></h1>
                    <!-- Begin Getting started Bucket -->
                    <div class="NewIconContentBucket">
                        <img class="NewBucketIcon" src="/Orion/images/SettingPageIcons/product-specific.svg" />
                        <div class="HeaderBox">
                            <div class="NewBucketHeader">
                                <%= Resources.CloudMonitoringWebContent.CloudMonitoringSummary_CloudMonitoringSettings%>
                            </div>
                            <p><%=Resources.CloudMonitoringWebContent.CloudSettingsPage_Description %></p>
                        </div>
                        <div class="NewIconBucketLinkContainer">
                            <asp:DataList ID="LinksDataList" runat="server" CellSpacing="-1" EnableViewState="false"
                                RepeatColumns="3" RepeatDirection="Horizontal" CssClass="NewBucketLinkContainer"
                                ItemStyle-CssClass="LinkColumn" ItemStyle-Width="33%">
                                <ItemTemplate>
                                    <p><span class="LinkArrow">&#0187;</span> <a
                                            href='<%# Eval("Url") %>'><%# Server.HtmlEncode(Convert.ToString(Eval("LinkText"))) %></a>
                                    </p>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </div>
                    <!-- End Getting started Bucket -->
                </td>
                <td class="column2of2" style="visibility: hidden;"></td>
            </tr>
        </table>
    </div>
    <br />
</asp:Content>