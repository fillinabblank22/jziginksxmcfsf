<%@ Control Language="C#" CodeFile="EditCloudMonitoringSummaryView.ascx.cs" Inherits="Orion_CloudMonitoring_Admin_EditViews_EditCloudMonitoringSummaryView"%>
<%@ Register Src="~/Orion/Controls/SelectViewForViewType.ascx" TagPrefix="orion" TagName="SelectView" %>

<orion:SelectView runat="server" ID="ViewSelector" AllowViewsByDeviceType="false" ViewType="CloudMonitoring Summary" />
