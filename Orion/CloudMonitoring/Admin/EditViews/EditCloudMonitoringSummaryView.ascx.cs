﻿using System;
using SolarWinds.Orion.Web.UI;

public partial class Orion_CloudMonitoring_Admin_EditViews_EditCloudMonitoringSummaryView : ProfilePropEditUserControl
{
    public override string PropertyValue
    {
        get { return ViewSelector.PropertyValue; }
        set { ViewSelector.PropertyValue = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}