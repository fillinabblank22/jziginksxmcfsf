﻿using System;
using System.Web.UI;
using Resources;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_CloudMonitoring_Admin_CloudMonitoringAdmin : MasterPage
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (!Profile.AllowAdmin && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            var error = new LocalizedExceptionBase(() => CoreWebContent.WEBCODE_VB0_323);
            var errorDetails = new ErrorInspectorDetails {Error = error, Title = CoreWebContent.WEBDATA_VB0_567};
            OrionErrorPageBase.TransferToErrorPage(Context, errorDetails);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}