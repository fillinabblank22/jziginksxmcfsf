﻿using System;
using System.Collections.Generic;
using SolarWinds.CloudMonitoring.Web.CloudSettings;
using SolarWinds.CloudMonitoring.Contract.CloudSettings;

public partial class Orion_CloudMonitoring_Admin_CloudMonitoringSettings : System.Web.UI.Page
{
    private ICloudMonitoringSettingsLinkRetriever cloudMonitoringSettingsLinkRetriever;
    protected ICloudMonitoringSettingsLinkRetriever CloudMonitoringSettingsLinkRetriever
    {
        get
        {
            if (cloudMonitoringSettingsLinkRetriever == null)
            {
                cloudMonitoringSettingsLinkRetriever = new CloudMonitoringSettingsLinkRetriever();
            }
            return cloudMonitoringSettingsLinkRetriever;
        }
        set
        {
            cloudMonitoringSettingsLinkRetriever = value;
        }
    }
     
    protected override void OnInit(EventArgs e)
    {
        var links = new List<CloudMonitoringSettingsLink>();
        if (Profile.AllowAdmin || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            links.Add(new CloudMonitoringSettingsLink("/ui/clm/addAccount", Resources.CloudMonitoringWebContent.CloudSettingsPage_AddCloudAccount));
            links.Add(new CloudMonitoringSettingsLink("/ui/clm/accounts", Resources.CloudMonitoringWebContent.CloudSettingsPage_ManageCloudAccounts));
        }

        // Add links from other plugins
        links.AddRange(CloudMonitoringSettingsLinkRetriever.Retrieve());

        LinksDataList.DataSource = links;
        LinksDataList.DataBind();

        base.OnInit(e);
    }
}