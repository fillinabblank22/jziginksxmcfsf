﻿<%@ Control Language="C#" CodeFile="LastCloudEvents.ascx.cs" AutoEventWireup="true" Inherits="Orion_CloudMonitoring_Resources_Summary_LastCloudEvents" %>
<%@ Import Namespace="Resources" %>
<%@ Register Src="~/Orion/Controls/EventsReportControl.ascx" TagName="EventsList" TagPrefix="orion" %>
<orion:resourceWrapper runat="server" ID="ResourceWrapper">
    <Content>
        <orion:EventsList runat="server" ID="EventsListControl"/>
         <% if (NoEventsFound){%>
            <h6><%=CloudMonitoringWebContent.LastCloudEventsResource_NoEventsFoundMsg%></h6>
         <%}%>
    </Content>
</orion:resourceWrapper>