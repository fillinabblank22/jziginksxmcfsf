﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GatewayTrafficByRegion.ascx.cs" Inherits="Orion_NetManCloudMonitoring_Resources_Summary_GatewayTrafficByRegion" EnableViewState="false" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>

<orion:Include runat="server" Module="VIMCloudMonitoring" File="CloudMonitoringIcons.css" />

<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable"/>
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= CustomTable.UniqueClientID %>,
                        initialPage: 0,
                        columnSettings: {
                            "Region": {     
                                header: '<%= NetManCloudMonitoringWebContent.TableColumnTitle_GatewayTrafficByRegion_Region %>',
                                formatter: function (value, row, cellInfo) {
								    const linkUrl = String.format('<%= VirtualNetworkGatewaysViewLinkFormart %>', value);
                                    return String.format('<a href="{0}"><span class="clm-icon clm-icon-azure-region" style="vertical-align: middle;">&nbsp;</span>{1}</a>', linkUrl, value);
                                },
                                isHtml: true
                            },
                            "In": {
                                header: '<%= NetManCloudMonitoringWebContent.TableColumnTitle_GatewayTrafficByRegion_In %>',
                                formatter: formatTraffic,
                                isHtml: true
                            },
                            "Out": {
                                header: '<%= NetManCloudMonitoringWebContent.TableColumnTitle_GatewayTrafficByRegion_Out %>',
                                formatter: formatTraffic,
                                isHtml: true
                            },
                            "Total": {
                                header: '<%= NetManCloudMonitoringWebContent.TableColumnTitle_GatewayTrafficByRegion_Total %>',
                                formatter: formatTraffic,
                                isHtml: true
                            }
                        }
                    });

				function formatTraffic(bytes) {
					if (!bytes && bytes !== 0) {
					    return '<i><%= NetManCloudMonitoringWebContent.TableCellValue_GatewayTrafficByRegion_NotAvailable %></i>';
					}
					
					const sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
					
					var index = 0;
					if (bytes !== 0) {
                        index = Math.floor(Math.log(bytes) / Math.log(1024));
						bytes = parseFloat((bytes / Math.pow(1024, index)).toFixed(2));
					}

                    return String.format("<span>{0}&nbsp;{1}</span>", bytes.toLocaleString(), sizes[index]);
                };

                var refresh = function () { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });
        </script>
    </Content>
</orion:ResourceWrapper>