﻿using System;
using System.Web;
using System.Web.UI.HtmlControls;
using Resources;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Core.Common;

public partial class Orion_NetManCloudMonitoring_Resources_Summary_GatewayTrafficByRegion : BaseResourceControl
{
    protected override string DefaultTitle
    {
        get { return NetManCloudMonitoringWebContent.WidgetTitle_GatewayTrafficByRegion; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    public string SWQL
    {
        get
        {
            return @"
SELECT 
    Location AS [Region],
    SUM(TunnelTrafficBytesIn) AS [In],
    SUM(TunnelTrafficBytesOut) AS [Out],
    SUM(TunnelTrafficBytesIn) + SUM(TunnelTrafficBytesOut) AS [Total]
FROM Cortex.Orion.NetMan.CloudMonitoring.VirtualNetworkGateway
GROUP BY Location";
        }
    }
	
	public string OrderBy
    {
        get
        {
			return @"[Total] DESC";
		}
	}

    public string VirtualNetworkGatewaysViewLinkFormart 
	{
		get
		{
			return $@"/apps/netman-cloudmonitoring/virtual-network-gateways?ViewID={GetVirtualNetworkGatewaysViewId()}&location={{0}}";
		}	
	}

    protected void Page_Load(object sender, EventArgs e)
    {
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        CustomTable.SWQL = SWQL;
		CustomTable.OrderBy = OrderBy;
    }
	
	private int? GetVirtualNetworkGatewaysViewId()
    {
        int? viewId = null;
		
        ViewsDAL.GetRecordByViewKey("Virtual Network Gateways Subview", reader =>
        {
            viewId = Convert.ToInt32(reader["ViewID"]);
        });

        return viewId;
    }
}