﻿using System;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;
using System.Linq;
using Castle.Components.DictionaryAdapter;
using Resources;
using SolarWinds.CloudMonitoring.Swis.Model.Events;
using SolarWinds.CloudMonitoring.Web.CloudAccounts;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.CloudMonitoring.Swis.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Events)]
public partial class Orion_CloudMonitoring_Resources_Summary_LastCloudEvents : BaseResourceControl
{
    private const string Resources_PropertyName_MaxEvents = "MaxEvents";
    private const string Resources_PropertyName_Period = "Period";
    private const string Resources_PropertyName_SelectedTypes = "SelectedTypes";

    private const int DefaultNumberOfEvents = 25;

    // flag used to display message about no events matching specified critera found
    public bool NoEventsFound { get; set; }

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl; 

    protected override string DefaultTitle => CloudMonitoringWebContent.LastCloudEventsResource_DefaultTitle;

    // overriden main title of resources
    public override string DisplayTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Resource.Title.Trim()))
                return Resource.Title;

            return DefaultTitle;
        }
    }

    // overriden subtitle of resource
    public override string SubTitle
    {
        get
        {
            if (!string.IsNullOrWhiteSpace(Resource.SubTitle))
                return Resource.SubTitle;

            // if subtitle is not specified subtitle is a period
            string period = Resource.Properties[Resources_PropertyName_Period];

            if (string.IsNullOrEmpty(period))
                return string.Empty;

            return Periods.GetLocalizedPeriod(period);
        }
    }

   public override string HelpLinkFragment => "OrionCoreCloud_Last25CloudEvents";

    public override string EditControlLocation => "/Orion/CloudMonitoring/Controls/EditResourceControls/LastCloudEventsEdit.ascx";    

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            if (string.IsNullOrEmpty(Resource.Properties[Resources_PropertyName_Period]))
                Resource.Properties[Resources_PropertyName_Period] = "Last 12 Months";
            if (string.IsNullOrEmpty(Resource.Properties[Resources_PropertyName_MaxEvents]))
                Resource.Properties[Resources_PropertyName_MaxEvents] = "25";
            if (string.IsNullOrEmpty(Resource.Properties[Resources_PropertyName_SelectedTypes]))
            {
                Resource.Properties[Resources_PropertyName_SelectedTypes] = GetDefaultSelectedTypesPropertyValue();
            }
        }

        LoadEvents();
       
    }

    private string GetDefaultSelectedTypesPropertyValue()
    {
        var supportedEventTypeIds = NetObjectInfoRetrieverFactory.Create()
            .RetrievePrefixes()
            .SelectMany(p => p.SupportedEventTypeIds)
            .ToList()
            .Distinct();

        return string.Join(",", supportedEventTypeIds);

    }

    protected void LoadEvents()
    {

        var queryParams = BuildLastEventsQueryParams();
        
        var loadedEvents = EventsHelper.GetLastEvents(queryParams
            , EventsHelper.FormatEventTime
            , dataRow =>
            {
                var orionDetailViewUrl = "/Server/{2}/Orion/View.aspx?NetObject={0}:{1}";
                EventsHelper.FormatNetObjectUrl(dataRow, orionDetailViewUrl);
            }
            , EventsHelper.FormatBackColor
            );


        if (loadedEvents.Rows.Count > 0)
        {
            EventsListControl.LoadData(loadedEvents);
        }
        else
        {
            NoEventsFound = true;
        }

    }

    private GetLastEventsQueryParams BuildLastEventsQueryParams()
    {
        var selectedCloudEventTypeIds = GetSelectedCloudEventTypeIds();

        var netObjectPrefixes = NetObjectInfoRetrieverFactory.Create()
            .RetrievePrefixes()
            .ToList()
            .Select(nop => new EventQueryParams()
            {
                NetObjectType =  nop.Text,
                EventTypeIds =  nop.SupportedEventTypeIds.Intersect(selectedCloudEventTypeIds).ToList()
            })
            ;

        int maxEventCount = GetIntProperty(Resources_PropertyName_MaxEvents, DefaultNumberOfEvents);

        string periodName = GetStringValue(Resources_PropertyName_Period, "Last 12 Months");
        
        var periodBegin = new DateTime();
        var periodEnd = new DateTime();

        // parse period to begin and end
        Periods.Parse(ref periodName, ref periodBegin, ref periodEnd);

        return GetLastEventsQueryParams.Configure(maxEventCount, netObjectPrefixes, periodBegin, periodEnd);
    }

    private IEnumerable<int> GetSelectedCloudEventTypeIds()
    {
        if (Resource.Properties["SelectedTypes"] == null)
        {
            return new List<int>();
        }

        var selectedTypes = Resource.Properties["SelectedTypes"].Split(',').ToList();

        return selectedTypes.Select(int.Parse).ToList();
    }
}