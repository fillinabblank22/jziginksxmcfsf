﻿using System;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.CloudMonitoring.Web.CloudAccounts;
using SolarWinds.CloudMonitoring.Shared.Licensing;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_CloudMonitoring_Resources_Summary_GettingStarted : BaseResourceControl
{
    protected override string DefaultTitle => CloudMonitoringWebContent.GettingStarted_Title;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        var cloudAcccountPresenceChecker = new CloudAccountPresenceChecker();
        var cloudMonitoringLicenseManager = new LicenseManager();

        if (cloudAcccountPresenceChecker.AnyCloudAccountExists() || !cloudMonitoringLicenseManager.IsCloudMonitoringAllowed)
        {
            Wrapper.Visible = false;
            return;
        }

        RemoveResourceLink.HRef = Page.ClientScript.GetPostBackClientHyperlink(RemoveResourceButton, String.Empty);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Profile.AllowAdmin)
        {
            monitorMyCloudInstancesButton.Attributes["onclick"] = "SW.CloudMonitoring.PermissionsRequiredDialog(); return false;";
        }
    }

    protected void OnRemoveResourceButtonClicked(object sender, EventArgs e)
    {
        ResourceManager.DeleteById(Resource.ID);
        Response.Redirect(Request.Url.ToString());
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Static; }
    }
}
