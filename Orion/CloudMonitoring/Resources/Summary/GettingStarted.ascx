﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GettingStarted.ascx.cs" Inherits="Orion_CloudMonitoring_Resources_Summary_GettingStarted" %>
<%@ Import Namespace="Resources" %>

<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="core" TagName="HelpLink" %>

<orion:Include runat="server" Module="CloudMonitoring" File="icons.css"/>
<orion:Include runat="server" Module="VIMCloudMonitoring" File="CloudMonitoringIcons.css"/>
<orion:Include runat="server" Module="CloudMonitoring" File="Resources.css"/>
<orion:Include runat="server" Module="CloudMonitoring" File="cloud-shared.css"/>
<orion:Include runat="server" Module="CloudMonitoring" File="clm.PermissionsRequiredDialog.js" Section="Bottom"/>

<orion:resourceWrapper runat="server" ID="Wrapper" ShowHeaderBar="false" CssClass="CloudMonitoring">
    <Content>
        <div class="GettingStarted">
            <div>
                <div>
                    <div class="Left LeftIcon clm-icon clm-icon-orion-getting-started"></div>
                    <h3>
                        <%= CloudMonitoringWebContent.GettingStarted_Title_Part1 %>
                        <span><%= CloudMonitoringWebContent.GettingStarted_Title_Part2 %></span>
                    </h3>
                </div>
                <div class="Content">
                    <div class="Right RightIcon clm-icon clm-icon-getting-started"></div>
                    <h5><%= CloudMonitoringWebContent.GettingStarted_SubTitle %></h5>
                    <ul class="ContentList">
                        <li><%= CloudMonitoringWebContent.GettingStarted_Content_List_Item1 %></li>
                        <li><%= CloudMonitoringWebContent.GettingStarted_Content_List_Item2 %></li>
                        <li><%= CloudMonitoringWebContent.GettingStarted_Content_List_Item3 %></li>
                        <li><%= CloudMonitoringWebContent.GettingStarted_Content_List_Item4 %></li>
                        <li><%= CloudMonitoringWebContent.GettingStarted_Content_List_Item5 %></li>
                        <li><%= CloudMonitoringWebContent.GettingStarted_Content_List_Item6 %></li>
                    </ul>
                    <p><%= CloudMonitoringWebContent.GettingStarted_Description %></p>
                    <div>
                        <core:HelpLink ID="cloudMonitoringAgentHelpLink" runat="server" HelpUrlFragment="OrionCloudMonitorCloudLearnMore" LocalizedText="CustomText" HelpDescription="<%$ Resources: CloudMonitoringWebContent, GettingStarted_LearnMoreLink %>"/>
                    </div>
                </div>
                <div class="Footer">
                    <div class="Right">
                        <orion:LocalizableButtonLink runat="server" ID="monitorMyCloudInstancesButton" NavigateUrl="/ui/clm/addAccount" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources: CloudMonitoringWebContent, GettingStarted_MonitorInstancesButton_Label %>"/>
                    </div>
                    <div class="LeftLink">
                        <a ID="RemoveResourceLink" class="ToLeft" runat="server"><%= CloudMonitoringWebContent.GettingStarted_RemoveResourceButton %></a>
                    </div>
                    <orion:LocalizableButton runat="server" ID="RemoveResourceButton" CssClass="Hidden" OnClick="OnRemoveResourceButtonClicked" DisplayType="Secondary"/>
                </div>
            </div>
        </div>
    </Content>
</orion:resourceWrapper>