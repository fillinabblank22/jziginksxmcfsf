<%@ Page Title="<%$ Resources: CloudMonitoringWebContent, CloudAccounts_CloudSummary %>" MasterPageFile="~/Orion/CloudMonitoring/CloudMonitoringView.master" Language="C#" CodeFile="Summary.aspx.cs" Inherits="Orion_CloudMonitoring_Summary" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContentPlaceHolder">
    <div class="titleTable">
        <h1 class="sw-hdr-title"><%=HttpUtility.HtmlEncode(Page.Title) %></h1>
    </div>
	<orion:ResourceHostControl ID="ResourceHostControl1" runat="server">
		<orion:ResourceContainer runat="server" ID="resContainer" />
	</orion:ResourceHostControl>
</asp:Content>
