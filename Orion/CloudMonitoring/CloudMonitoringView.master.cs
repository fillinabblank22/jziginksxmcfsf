﻿using System;
using System.Web.UI;
using SolarWinds.CloudMonitoring.Web.Views;

public partial class Orion_CloudMonitoring_CloudMonitoringView : MasterPage
{
    private string HelpFragment
    {
        get
        {
            if (!(Page is CloudMonitoringViewPage))
            {
                return String.Empty;
            }

            return ((CloudMonitoringViewPage) Page).HelpFragment;
        }
    }

    protected bool AllowViewTimestamp { get; private set; }
    protected bool AllowCustomize { get; private set; }
    protected bool AllowCloudMonitoringSettings { get; private set; }
    // ReSharper disable once UnusedMember.Global
    // Used in Summary.aspx
    public string CustomizeViewHref
    {
        get
        {
            if (!(Page is CloudMonitoringViewPage))
            {
                return string.Empty;
            }

            return ((CloudMonitoringViewPage) Page).CustomizeViewHref;
        }
    }

    protected Orion_CloudMonitoring_CloudMonitoringView()
    {
        AllowViewTimestamp = true;
        AllowCustomize = Profile.AllowCustomize;
        AllowCloudMonitoringSettings = Profile.AllowAdmin || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(HelpFragment))
        {
            return;
        }
        btnHelp.HelpUrlFragment = HelpFragment;
        btnHelp.Visible = true;
    }
}
