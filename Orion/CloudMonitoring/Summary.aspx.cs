using System;
using SolarWinds.CloudMonitoring.Web.Views;

public partial class Orion_CloudMonitoring_Summary : CloudMonitoringViewPage
{
    public override string ViewType
    {
        get { return "CloudMonitoring Summary"; }
    }

    public override string HelpFragment
    {
        get { return "OrionCloudMonitoringSummary"; }
    }

    protected override void OnInit(EventArgs e)
    {
        Title = ViewInfo.ViewTitle;
        resContainer.DataSource = ViewInfo;
        resContainer.DataBind();

        base.OnInit(e);
    }
}