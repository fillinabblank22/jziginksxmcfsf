﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Orion.NPM.Web.UI;
using SolarWinds.Orion.Web;

public partial class Orion_CloudMonitoring_Controls_Breadcrumbs : UserControl
{
    protected void OnSiteMapPath_OnInit(Object sender, EventArgs e)
    {
        ISiteMapRenderer renderer = CreateAdminSiteMapRenderer();
        breadcrumbs.SetUpRenderer(renderer);
    }

    private ISiteMapRenderer CreateAdminSiteMapRenderer()
    {
        ISiteMapRenderer renderer = new AdminSiteMapRenderer();

        var viewId = Request.QueryString["ViewID"];
        var returnTo = Request.QueryString["ReturnTo"];
        var accountId = Request.QueryString["AccountID"];

        // Shouldn't IsNullOrWhiteSpace condition be inverted?
        renderer.SetUpData(
            new KeyValuePair<string, string>("ViewID", string.IsNullOrWhiteSpace(viewId) ? viewId : null),
            new KeyValuePair<string, string>("ReturnTo", string.IsNullOrWhiteSpace(returnTo) ? returnTo : null),
            new KeyValuePair<string, string>("AccountID", string.IsNullOrWhiteSpace(accountId) ? accountId : null));
        return renderer;
    }
}