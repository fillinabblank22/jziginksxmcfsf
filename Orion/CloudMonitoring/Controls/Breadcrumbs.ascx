﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Breadcrumbs.ascx.cs" Inherits="Orion_CloudMonitoring_Controls_Breadcrumbs" %>

	<orion:FileBasedDropDownMapPath ID="breadcrumbs" Provider="AdminSitemapProvider" OnInit="OnSiteMapPath_OnInit" runat="server">
		<RootNodeTemplate>
			<a href="<%# Eval("url") %>"><u> <%# Eval("title") %></u> </a>
		</RootNodeTemplate>
		<CurrentNodeTemplate>
			<a href="<%# Eval("url") %>"> <%# Eval("title") %> </a>
		</CurrentNodeTemplate>
	</orion:FileBasedDropDownMapPath>