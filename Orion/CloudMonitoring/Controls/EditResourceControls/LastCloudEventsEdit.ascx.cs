﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.CloudMonitoring.Swis.Helpers;
using SolarWinds.CloudMonitoring.Swis.Model.Events;
using SolarWinds.CloudMonitoring.Web.CloudAccounts;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.UI;

public partial class Orion_CloudMonitoring_Controls_EditResourceControls_LastCloudEventsEdit : BaseResourceEditControl
{
    private const string Resources_PropertyName_MaxEvents = "MaxEvents";
    private const string Resources_PropertyName_Period = "Period";
    private const string Resources_PropertyName_SelectedTypes = "SelectedTypes";

    private const int DefaultNumberOfEvents = 25;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        // fill in period list
        foreach (var period in Periods.GenerateSelectionList())
            Period.Items.Add(new ListItem(Periods.GetLocalizedPeriod(period), period));

        MaxEventsText.Text = string.IsNullOrEmpty(Resource.Properties[Resources_PropertyName_MaxEvents]) ? DefaultNumberOfEvents.ToString() : Resource.Properties[Resources_PropertyName_MaxEvents];
        Period.Text = string.IsNullOrEmpty(Resource.Properties[Resources_PropertyName_Period]) ? "Last 12 Months" : Resource.Properties[Resources_PropertyName_Period];

        if (!this.IsPostBack)
        {
            SelectedTypes = GetSelectedTypesOnInit();
            this.EventTypesDataSource = GetEventTypesDataSource();
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            var properties = new Dictionary<string, object>();
            try
            {
                Int32.Parse(MaxEventsText.Text);
                properties.Add(Resources_PropertyName_MaxEvents, MaxEventsText.Text);
            }
            catch
            {
                properties.Add(Resources_PropertyName_MaxEvents, DefaultNumberOfEvents.ToString());
            }

            properties.Add(Resources_PropertyName_Period, Period.Text);

            var selected = GetSelectedTypes();
            if (selected != null && selected.Count > 0)
                properties.Add(Resources_PropertyName_SelectedTypes, string.Join(",", selected.ToArray()));

            return properties;
        }
    }

    public override string DefaultResourceTitle
    {
        get
        {
            string titleFormat = Resources.CloudMonitoringWebContent.Web_LastCloudEvents_Title;

            return String.Format(titleFormat,
                String.IsNullOrEmpty(MaxEventsText.Text) ? DefaultNumberOfEvents.ToString() : MaxEventsText.Text);
        }
    }

    public List<string> SelectedTypes
    {
        get;
        set;
    }

    private List<string> GetSelectedTypesOnInit()
    {
        var selectedTypes = this.Resource.Properties[Resources_PropertyName_SelectedTypes];
        return selectedTypes.Split(',').ToList();
    }

    public List<string> GetSelectedTypes()
    {
        var selectedTypes = new List<string>();

        foreach (RepeaterItem rpItem in eventTypes.Items)
        {
            var eventTypeElementCheckbox = rpItem.FindControl("eventTypeElementCheckbox") as CheckBox;
            if (eventTypeElementCheckbox != null && eventTypeElementCheckbox.Checked)
            {
                selectedTypes.Add(eventTypeElementCheckbox.Attributes["EventId"]);
            }
        }

        return selectedTypes;
    }

    public DataTable EventTypesDataSource
    {
        set
        {
            eventTypes.DataSource = value;
            eventTypes.DataBind();
        }
    }

    private DataTable GetEventTypesDataSource()
    {
        var source = GetSupportedEventIdsAndNames();

        var dataTable = new DataTable();
        dataTable.Clear();
        dataTable.Columns.Add("EventId");
        dataTable.Columns.Add("EventDisplayName");

        foreach (var eventType in source)
        {
            var row = dataTable.NewRow();
            row["EventId"] = eventType.Id;
            row["EventDisplayName"] = eventType.Name;
            dataTable.Rows.Add(row);
        }

        return dataTable;
    }

    private IEnumerable<EventTypeInfo> GetSupportedEventIdsAndNames()
    {
        var supportedEventTypeIds = NetObjectInfoRetrieverFactory.Create()
            .RetrievePrefixes()
            .SelectMany(p => p.SupportedEventTypeIds)
            .ToList()
            .Distinct();

        return EventsHelper.GetEventTypesInfo(supportedEventTypeIds);
    }

    protected void EventTypes_ItemDataBound(Object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var cb = (CheckBox)e.Item.FindControl("eventTypeElementCheckbox");
            var eventType = ((DataRowView)e.Item.DataItem)["EventId"].ToString();
            cb.Attributes.Add("EventId", eventType);
            cb.Text = ((DataRowView)e.Item.DataItem)["EventDisplayName"].ToString();

            if (SelectedTypes != null && SelectedTypes.Count > 0)
                cb.Checked = SelectedTypes.Contains(eventType);
        }
    }

    protected void ColumnsValidation(object source, ServerValidateEventArgs args)
    {
        foreach (RepeaterItem rpItem in eventTypes.Items)
        {
            var eventTypeElementCheckbox = rpItem.FindControl("eventTypeElementCheckbox") as CheckBox;
            if (eventTypeElementCheckbox != null && eventTypeElementCheckbox.Checked)
            {
                args.IsValid = true;
                return;
            }
        }

        args.IsValid = false;
    }
}