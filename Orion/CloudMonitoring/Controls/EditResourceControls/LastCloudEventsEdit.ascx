﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LastCloudEventsEdit.ascx.cs" Inherits="Orion_CloudMonitoring_Controls_EditResourceControls_LastCloudEventsEdit" %>
<%@ Register TagPrefix="orion" TagName="ServerFilter" Src="~/Orion/Controls/OrionServerFilterControl.ascx" %>

<orion:Include runat="server" File="CloudMonitoring/js/clm.LastCloudEvents.Edit.js" />
<orion:Include runat="server" File="CloudMonitoring/styles/LastCloudEventsEdit.css" />

<script type="text/javascript">
    $(function () {
        CLM.LastCloudEvents.Edit.setUpRootCheckbox($("#<%=EventTypeContainer.ClientID%>"));
    });
</script>

<table border="0">
    <tr>
		<td>
			<orion:ServerFilter ID="ServerFilter" runat="server" />
		</td>
	</tr>
    <tr>
        <td>
            <asp:Label runat="server" ID="label" Text="<%$ Resources: CoreWebContent,WEBDATA_AK0_281 %>" class="font-bold" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox runat="server" ID="MaxEventsText" Width="50" />
        </td>
    </tr>
    <tr>
        <td class="spaced-top">
            <asp:Label runat="server" ID="label1" Text="<%$ Resources: CoreWebContent,WEBDATA_AK0_263 %>" class="font-bold" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:DropDownList runat="server" ID="Period" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RangeValidator ID="EventsRangeValidator" runat="server" ErrorMessage="<%$ Resources: CoreWebContent,WEBDATA_AK0_319 %>"
                Display="Dynamic" ControlToValidate="MaxEventsText" MinimumValue="1" MaximumValue="5000"
                Type="Integer" />
        </td>
    </tr>
    <tr>
        <td class="vertical-space">
            <div runat="server" id="EventTypeContainer">
                <asp:Repeater ID="eventTypes" runat="server" OnItemDataBound="EventTypes_ItemDataBound">
                    <HeaderTemplate>
                        <table class="full-size-width">
                            <tr>
                                <td>
                                    <table id="adminContentTable">
                                        <tr class="alternateRow">
                                            <td class="font-bold">
                                                <asp:CheckBox runat="server" Checked="true" class="spaced-checkbox" ID="selectAllCheckbox"
                                                    OnClick="javascript:CLM.LastCloudEvents.Edit.selectAll(this);" Text="<%$ Resources: CloudMonitoringWebContent,Web_LastCloudEventsEdit_SelectAll %>" />
                                            </td>
                                        </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="text-left">
                                <asp:CheckBox ID="eventTypeElementCheckbox" runat="server" Checked="true" class="spaced-checkbox"
                                    OnClick="javascript:CLM.LastCloudEvents.Edit.setUpRootCheckbox($(this).parent().parent().parent().parent());" />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table></td>
                        <td>
                            <asp:CustomValidator ID="RequiredColumn" runat="server" OnServerValidate="ColumnsValidation"
                                ErrorMessage="<%$ Resources: CoreWebContent,WEBDATA_SO0_49 %>" EnableClientScript="false"
                                Display="Dynamic">
                            </asp:CustomValidator>
                        </td>
                        </tr></table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </td>
    </tr>
</table>