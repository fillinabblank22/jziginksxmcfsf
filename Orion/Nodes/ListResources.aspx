<%@ Page Language="C#" MasterPageFile="~/Orion/Nodes/NodeManagementPage.master" AutoEventWireup="true"
    CodeFile="ListResources.aspx.cs" Inherits="Orion_Nodes_ListResources" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_116 %>" %>

<%@ Register Src="~/Orion/Discovery/Controls/ResourceTree.ascx" TagPrefix="orion" TagName="ResourceTree" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Nodes/Controls/IconQuickLinks.ascx" TagPrefix="orion" TagName="IconQuickLinks" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>
<%@ Register Src="~/Orion/Controls/InstallAgentProgress.ascx" TagPrefix="orion" TagName="InstallAgentProgress" %>
<%@ Register Src="~/Orion/Controls/MessageBox.ascx" TagPrefix="orion" TagName="MessageBox" %>
<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
<orion:Include ID="Include6" runat="server" File="/Nodes/styles/ToolTip.css" />
<orion:Include ID="Include7" runat="server" File="/Nodes/js/ToolTip.js" />    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconQuickLinks ID="IconQuickLinks1" runat="server" Caption="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PR_02 %>" HelpUrlFragment="#" ImageName="/Orion/images/nodemgmt_art/icons/icon_add.gif" />    
	<orion:IconLinkExportToPDF runat="server" />
    <orion:IconHelpButton runat="server" HelpUrlFragment="OrionPHNodeManagementListResources" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <style type="text/css">
        .fillwidth {
            width: 99%;
        }

        .infobox {
            display:inline-block; 
            width:100%; 
            margin-bottom:15px;
        }
    </style>

    <style type="text/css">
            #errorDialogContent .dialogItem { margin-bottom: 10px; }
            #errorDialogContent { margin-top: 20px; padding-left:50px; background: url('/Orion/images/stop_32x32.gif') top left no-repeat}
    </style>

   <h1 class="sw-hdr-title"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %> - <%= DefaultSanitizer.SanitizeHtml(this.NodeCaption) %></h1>

    <div id="TopInfoBox" runat="server" class="sw-pg-hint-yellow infobox">
        <div class="sw-pg-hint-body-warning">
            <span id="TopInfoBoxText" runat="server"></span>
        </div>
    </div>

   <div class="sw-hdr-subtitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_36) %></div>

   <div class="TooltipLinkPlace" id="pollerTooltip" runat="server">
        <a href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("Orion", "OrionCorePHDeviceStudioCreatingPollers")) %>" class="TooltipLink" target="_blank" rel="noopener noreferrer"><span id="toolTip"></span></a>
        <script type="text/javascript">
            $().ready(function () {
                $().ready(function () {
                    $('#addnodecontent').addClass('fillwidth');
                    $('#toolTip').html(GetParametrizedTooltipHtml('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PR_03) %>',
                    '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PR_04) %>',
                     '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PR_03) %>',
                    '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PR_05) %>',
                     '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PR_06) %>'));                     
                });
            });
        </script>
    </div>    

    <div id="DiscoveryJobWaitingImage" runat="server" visible="false">
        <img src="/Orion/images/AJAX-Loader.gif" />
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_11) %>
    </div>

    <orion:ResourceTree runat="server" ID="ResourceTree" />
    <br />
    <div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" automation="Submit" DisplayType="Primary" 
            OnClientClick="UpdateManagedState(); return false;" />
        <orion:LocalizableButton runat="server" LocalizedText="Cancel" automation="Cancel" DisplayType="Secondary" ID="imbtnCancel" OnClick="imbtnCancel_Click" />
    </div>

    <div id="AgentErrorDialog" runat="server" style="display: none;" class="disposable, common-dialog">
        <div id="errorDialogContent" class="content">
            <div id="errorDialogText" class="dialogItem"><%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.ListResources_AgentError_Text, NodeCaption)) %></div>
            <div class="dialogItem"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ListResources_AgentError_AMSHint) %></div>
            <div class="dialogItem">
                <span class="LinkArrow">&#0187;</span>
                <orion:HelpLink ID="HelpLink2" runat="server" HelpUrlFragment="OrionAgent-TroubleshootingAgentConnections"
                    HelpDescription="<%$ HtmlEncodedResources: CoreWebContent, ListResources_AgentError_HelpLinkText %>" CssClass="sw-link" />
            </div>
        </div>
        <div class="bottom">
            <div class="sw-btn-bar-wizard">
		        <orion:LocalizableButton runat="server" ID="GoToAMSButton" LocalizedText="CustomText" automation="GoToAMS" Text="<%$ HtmlEncodedResources: CoreWebContent, ListResources_AgentError_GoToAMS %>" DisplayType="Primary" OnClientClick="window.location.href='/Orion/AgentManagement/Admin/ManageAgents.aspx'; return false;" />
		        <orion:LocalizableButton runat="server" ID="CloseButton" LocalizedText="CustomText" automation="Close" Text="<%$ HtmlEncodedResources: CoreWebContent, WEB_JS_CODE_VB0_3 %>" DisplayType="Secondary" OnClientClick="window.history.back(); return false;" />
            </div>
	    </div>
    </div>

    <orion:InstallAgentProgress ID="InstallingAgent" runat="server" />

    <script type="text/javascript">
        UpdateManagedState = function () {
            SW.Core.ResourceTree.UpdateSelectionStateInSession(function () {
                ORION.callWebService("/Orion/Services/ResourceTree.asmx", "UpdateResourcesManagedState",
                                 {
                                     infoIdString: '<%= this.ResourceTree.InfoId %>',
                                     nodeId: <%= this.NodeId %>
                                 },
                                 function (result) {
                                     if (result != null){
                                        alert(result);
                                     }

                                     window.location = '<%= this.ReturnURL %>';
                                 });
            }); 
        }

        DisableSubmitButton = function()
        {
            var btnSubmit = $("#<%= btnSubmit.ClientID %>");
            btnSubmit[0].onClick = "";
            btnSubmit.addClass("sw-btn-disabled");
        }

        EnableSubmitButton = function() {
            var btnSubmit = $("#<%= btnSubmit.ClientID %>");
            btnSubmit[0].onClick = "UpdateManagedState(); return false;";
            btnSubmit.removeClass("sw-btn-disabled");
        }

        ShowAgentErrorDialog = function (nodeCaption) {
            dialog = $("#<%= AgentErrorDialog.ClientID%>").dialog({
                width: 450,
                modal: true,
                close: function () {
                    window.history.back();
                },
                overlay: { "background-color": "black", opacity: 0.4 },
                title: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ListResources_AgentError_Title) %>',
                resizable: false
            });
            dialog.show();
        }

        CloseInstallAgentErrorDialog = function () {
            $("#<%= AgentErrorDialog.ClientID%>").dialog('close');
        }

        ShowAgentProgressDialog = function(agentId, nodeCaption) {
            ShowInstallAgentProgress("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AddNodeWizard_DeployAgent_Progress_Title) %>", agentId, nodeCaption, DeployingAgentFinished, function(){ window.history.back(); });
        }

        DeployMissingPlugins = function(agentId, nodeCaption) {
            $.ajax({
                type: 'POST',
                url: '/Orion/Services/AgentManagerService.asmx/StartDeployingPlugins',
                data: JSON.stringify({
                    agentId: agentId
                }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {
                    ShowAgentProgressDialog(agentId, nodeCaption);
                },
                error: function (ex) {
                    ShowAgentErrorDialog(nodeCaption);
                }
            });
        }

        function DeployingAgentFinished(agentId, nodeCaption, status) {
            if (status.Status == 1) {
                __doPostBack('', 'AGENT_DEPLOYED');
            } else if (status.Status == 2) {
                ShowAgentErrorDialog(nodeCaption);
            } else if (status.Status == 3) {
                //RebootMachine(agentId, nodeCaption);
            }
        }

        function CreateDiscoveryJob(nodeId, engineId, ipAddress, forceRefresh) {
            $.ajax({
                type: 'POST',
                url: 'ListResources.aspx/CreateDiscoveryJob',
                data: JSON.stringify({
                    nodeId: nodeId,
                    engineId: engineId,
                    ipAddress: ipAddress,
                    forceRefresh: forceRefresh
                }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {
                    jobId = result.d;
                    if ((jobId === null) || jobId === ''){
                        //job not created, wait and try it again
                        setTimeout(function () { CreateDiscoveryJob(nodeId, engineId, ipAddress, forceRefresh) }, 5000);
                    } else {
                        __doPostBack('', 'JOB_ID=' + jobId);
                    }
                },
                error: function (ex) {
                }
            });
        }
    </script>
    
</asp:Content>
