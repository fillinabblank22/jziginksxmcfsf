using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.EntityManager;
using SolarWinds.Orion.Core.Common.ModuleManager;
using SolarWinds.Orion.Core.Common.PackageManager;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.InformationService;
using WebSettingsDAL = SolarWinds.Orion.Core.Common.WebSettingsDAL;
using WebUserSettingsDAL = SolarWinds.Orion.Web.DAL.WebUserSettingsDAL;

public partial class Orion_Nodes_Default : System.Web.UI.Page
{
	protected bool _allowInterfaces;
	protected bool _allowTopology;
    protected bool _isNPMInstalled;
    protected bool _isEnergyWise;
	protected bool _multipleEngines;
    protected bool _isNewPageMsgDismissed;
    protected bool _isNewPageUsed;

    protected string _objectType;
	protected string _groupByProperty;

    protected InheritedProperty[] _inheritedProperties;

    [DataContract]
    protected class InheritedProperty
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string NavigationName { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
        [DataMember]
        public string PropertyType { get; set; }
    }

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
        _allowInterfaces = PackageManager.InstanceWithCache.IsPackageInstalled("Orion.Interfaces");
        _allowTopology = PackageManager.InstanceWithCache.IsPackageInstalled("Orion.Topology");
        _isNPMInstalled = ModuleManager.InstanceWithCache.IsThereModule("NPM");
        _isEnergyWise = EntityManager.InstanceWithCache.IsThereEntity("Orion.NPM.EW.Entity");
		_multipleEngines = EnginesDAL.GetEnginesCount() > 1;
        _isNewPageUsed = WebUserSettingsDAL.Get("WebNodeManagement_UseLegacyPage") == "False";
        _isNewPageMsgDismissed = WebUserSettingsDAL.Get("WebNodeManagement_NewPageNotificationDismissed") == "True";

        if (_isNewPageUsed)
            Response.Redirect("/ui/manage/nodes");

        _inheritedProperties = GetInheritedProperties("Orion.Nodes");

		if (string.IsNullOrEmpty(Request["CustomPropertyId"])) return;
		
		var parts = Request["CustomPropertyId"].Split(':');
		// curently we support selecting default grouping only for node custom properties
        if (parts.Length == 2 && (parts[0].Equals("Orion.Nodes", StringComparison.OrdinalIgnoreCase) || parts[0].Equals("Orion.NPM.Interfaces", StringComparison.OrdinalIgnoreCase)))
		{
			_objectType = parts[0];
			_groupByProperty = string.Format("CustomProperties.[{0}]", parts[1]);
		}
	}

	protected override void OnLoad(EventArgs e)
    {
        // enable AngularJS for page content so that toggle bar for new node management page works
        AngularJsHelper.EnableAngularJsForPageContent();

		bool gotNcm55 = OrionModuleManager.IsModuleVersionAtLeast("NCM", "5.5");
        this.energyWiseListItem.Visible = gotNcm55;

        foreach (string pluginJSFiles in this.GetAllPluginJavascripts())
            OrionInclude.CoreFile(pluginJSFiles);

		if (DoNodeIconsBlink())
			ClientScript.RegisterStartupScript(GetType(), "MNG.BlinkingNodeIcons", "<script type='text/javascript'>if(!window.MNG)window.MNG={};window.MNG.BlinkingNodeIcons=1;</script>");
    }

	protected bool DoNodeIconsBlink()
	{
		try
		{
			return "Blinking".Equals( WebSettingsDAL.Get("NodeChildStatusDisplayMode"), StringComparison.OrdinalIgnoreCase );
		}
		catch (KeyNotFoundException)
		{
		}

		return false;
	}

	protected string[] GetCustomProperties(string table)
	{
        return new List<string>(CustomPropertyMgr.GetPropNamesForTable(table, false)).ToArray();
	}

    protected InheritedProperty[] GetInheritedProperties(string sourceEntity)
    {
        using (var swis = InformationServiceProxy.CreateV3())
        {
            return GetInheritedPropertiesRecursively(swis, sourceEntity,String.Empty)
                .OrderBy(prop => prop.DisplayName)
                .ThenBy(prop=> prop.Name)
                .ToArray();
        }
    }

    private string[] GetAllPluginJavascripts()
    {
        List<string> allJS = new List<string>();
        foreach (var plugin in NodeManagementPluginManager.NodeManagementPlugins.Values)
            allJS.Add(plugin.JSFilePath);

        return allJS.ToArray();
    }

    protected List<NodeManagementPluginManager.ActionListItem> GetActionListItems()
    {
        List<NodeManagementPluginManager.ActionListItem> list = new List<NodeManagementPluginManager.ActionListItem>();
        
        foreach (var plugin in NodeManagementPluginManager.NodeManagementPlugins.Values)
            list.AddRange(plugin.ActionListItems);

        return list;
    }

    protected List<InheritedProperty> GetInheritedPropertiesRecursively(InformationServiceProxy swis, string sourceEntity, string sourcePropertyNameFromRoot)
    {
        List<InheritedProperty> listOfProperties = new List<InheritedProperty>();

        string findEntity = @"SELECT T.TargetType, T.SourcePropertyName, ISNULL(E.DisplayName,E.Name) AS EntityDisplayName
                              FROM Metadata.Relationship T INNER JOIN Metadata.Entity E ON (T.TargetType = E.Type)
                              WHERE T.SourceType ISA @sourceEntity AND T.TargetCardinalityMax IN ('0','1') AND T.SourcePropertyName <> 'CustomProperties' AND T.IsInjected=0";

        string findEntityProperties = @"SELECT Name, DisplayName, Type
                                        FROM Metadata.Property 
                                        WHERE EntityName = @inspectedEntity AND isInjected = 0 AND GroupBy = 1";

        DataTable result = swis.Query(findEntity, new Dictionary<string, object>()
        {
            {"sourceEntity", sourceEntity}
        });

        if (result != null && result.Rows.Count > 0)
        {
            foreach (DataRow row in result.Rows)
            {
                string targetType = row["TargetType"] != DBNull.Value ? row["TargetType"].ToString() : null;
                string entityDisplayName = row["EntityDisplayName"] != DBNull.Value ? row["EntityDisplayName"].ToString() : null;
                string sourcePropertyName = row["SourcePropertyName"] != DBNull.Value ? row["SourcePropertyName"].ToString() : null;               

                if (String.IsNullOrEmpty(targetType) || String.IsNullOrEmpty(sourcePropertyName) || String.IsNullOrEmpty(entityDisplayName))
                    continue;

                string _sourcePropertyRoot = !String.IsNullOrEmpty(sourcePropertyNameFromRoot) ? sourcePropertyNameFromRoot + "." + sourcePropertyName : sourcePropertyName;

                DataTable propertyNames = swis.Query(findEntityProperties, new Dictionary<string, object>()
                {
                    {"inspectedEntity", targetType}
                });

                if (propertyNames != null && propertyNames.Rows.Count > 0)
                {
                    foreach (DataRow pRow in propertyNames.Rows)
                    {
                        InheritedProperty prop = new InheritedProperty()
                        {
                            DisplayName = entityDisplayName + " - " + (pRow["DisplayName"] != DBNull.Value ? pRow["DisplayName"].ToString() : pRow["Name"].ToString()),
                            Name = pRow["Name"].ToString(),
                            NavigationName = _sourcePropertyRoot + "." + pRow["Name"],
                            PropertyType = pRow["Type"].ToString()
                        };

                        listOfProperties.Add(prop);
                    }
                }

                listOfProperties.AddRange(GetInheritedPropertiesRecursively(swis, targetType, _sourcePropertyRoot));
            }   
        }

        return listOfProperties;
    }

    protected string ReturnUrl
    {
        get { return ReferrerRedirectorBase.GetReturnUrl(); }
    }

    protected bool AllowMaintenanceMode
    {
        get { return MaintenanceModeDAL.IsMaintenanceModeEnabled; }
    }
}
