﻿function GetParametrizedTooltipHtml(link, title, subtitle, tooltip, footer) {
    var value = '<span'
    + ' onmousemove="Tooltip_Show(event, $(this).next(\'div\'));"'
    + ' onmouseout="Tooltip_Hide($(this).next(\'div\'));">'
    + '<img src="/orion/images/ToolTip/Ninja_icon_16.png" />';    
    
    value += link;
    value += '</span>';
    
    value += '<div class="Tooltip" style="display: none;">';
    value += '  <div class="TooltipBody sw-suggestion">';
    value += '    <div class="TooltipIcon sw-suggestion"></div>';
    value += '    <div class="TooltipTitle">'+title+'</div>';
    value += '    <div class="TooltipSubTitle">'+subtitle+'</div>';
    value += '    <div class="TooltipText">'+tooltip+'</div>';
    value += '    <div class="TooltipFooter">'+footer+'</div>';
    value += '  </div>';
    value += '  <div class="TooltipArrow"></div>';
    value += '</div>';

    return value;
}


function Tooltip_Show(e, tooltipDiv) {
    if (!e)
        e = window.event;

    var tooltipHeight = $(tooltipDiv).height();
    var tooltipWidth = $(tooltipDiv).width();
    var linkPosition = $(tooltipDiv).parents('a').offset();
    var linkWidth = $(tooltipDiv).parents('a').width();

    $(tooltipDiv).css('top', linkPosition.top - tooltipHeight + 62);
    $(tooltipDiv).css('left', linkPosition.left - tooltipWidth);
    $(tooltipDiv).children('.TooltipArrow').css('top', tooltipHeight - 80);
    $(tooltipDiv).children('.TooltipArrow').css('left', tooltipWidth - 16);

    $(tooltipDiv).fadeIn("fast");
}
function Tooltip_Hide(tooltipDiv) {
    $(tooltipDiv).fadeOut("fast");
}
