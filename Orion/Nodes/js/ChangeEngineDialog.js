﻿Ext.namespace('SW');
Ext.namespace('SW.Orion');

Ext.QuickTips.init();

/* 
String format like in C#
*/
String.prototype.format = function (params) {
    var pattern = /{([^}]+)}/g;
    return this.replace(pattern, function ($0, $1) { return params[$1]; });
};

SW.Orion.EngineType = {
    Primary: 0,
    Additional: 1,
    RemoteCollector: 2
};

SW.Orion.ChangeEngineDialog = function () {
    var horizon = 2; //2 minutes

    function GetStatusImage(dbTimeDiff) {

        if ((null != dbTimeDiff) && (undefined !== dbTimeDiff)) {
            if (dbTimeDiff < horizon) {
                return "Small-Up.gif";
            }
            else if (dbTimeDiff > horizon * 1.5) {
                return "Small-Down.gif";
            }
            else {
                return "Small-Warning.gif";
            }
        }
        return "Small-Unknown.gif";
    }

    function renderStatus(keepAlive, meta, record) {

        if ((null != record.data.DatabaseTimeDiff) && (undefined != record.data.DatabaseTimeDiff)) {
            if (record.data.DatabaseTimeDiff < horizon) {
                return String.format('<span style="vertical-align: top;">{0}</span>', '@{R=Core.Strings;K=StatusDesc_Active;E=js}');
            }
            else if (record.data.DatabaseTimeDiff > horizon * 1.5) {
                return String.format('<span style="vertical-align: top;">{0}</span>', '@{R=Core.Strings;K=StatusDesc_Down;E=js}');
            }
            else {
                return String.format('<span style="vertical-align: top;">{0}</span>', '@{R=Core.Strings;K=StatusDesc_Warning;E=js}');
            }
        }
        return "";
    }

    function renderEngineLink(record) {
        return String.format('<a href="/Orion/Admin/Details/Engines.aspx"><img src="/NetPerfMon/images/{0}"/>&nbsp;<span style="vertical-align: top;">{1} ({2})</span></a>',
            GetStatusImage(record.data.DatabaseTimeDiff), record.data.ServerName, getServerType(record.data.ServerType));
    }

    function renderText(value, meta, record) {
        return String.format('<span style="vertical-align: top;">{0}</span>', Ext.util.Format.htmlEncode(value));
    }

    function renderName(value, meta, record) {
        return String.format('<img src="/NetPerfMon/images/{0}"/>&nbsp;<span style="vertical-align: top;">{1}</span>',
            GetStatusImage(record.data.DatabaseTimeDiff), Ext.util.Format.htmlEncode(value));
    }

    function renderServerType(value, meta, record) {
        return String.format('<span style="vertical-align: top;">{0}</span>', Ext.util.Format.htmlEncode(getServerType(value)));
    }

    function getServerType(serverType) {
        switch (serverType) {
            case "Primary": return "@{R=Core.Strings;K=ServerType_primary;E=js}";
            case "Additional": return "@{R=Core.Strings;K=ServerType_additional;E=js}";
            default: return serverType;
        }
    }
    
    function ajaxErrorHandler(error) {
        if (error.status == 500) {
            // Internal Server Error
            alert("@{R=Core.Strings;K=WEBJS_YK0_1;E=js}");
        } else {
            alert(error);
        }
        win.hide();
    }

    //Ext grid
    ORION.prefix = "Orion_ChangeEngineDialog_";
    var selectorModel;
    var dataStore;
    var grid;
    var supportedEngineTypes = [
        SW.Orion.EngineType.Primary, SW.Orion.EngineType.Additional, SW.Orion.EngineType.RemoteCollector
    ];

    return {
        init: function () {
            selectorModel = new Ext.sw.grid.RadioSelectionModel();
            dataStore = new ORION.WebServiceStore(
                "/Orion/Services/NodeManagement.asmx/GetEnginesTableForSupportedPollingEngineTypes",
                [
                    { name: 'EngineID', mapping: 0 },
                    { name: 'ServerName', mapping: 1 },
                    { name: 'ServerType', mapping: 2 },
                    { name: 'KeepAlive', mapping: 3 },
                    { name: 'Elements', mapping: 4 },
                    { name: 'DatabaseTimeDiff', mapping: 5 }
                ],
                "ServerName");

            var pagingToolbar = new Ext.PagingToolbar(
                { store: dataStore, pageSize: 15, displayInfo: true, split: true, displayMsg: '@{R=Core.Strings;K=WEBJS_AK0_4;E=js}', emptyMsg: "@{R=Core.Strings;K=WEBJS_AK0_3;E=js}" }
            );

            grid = new Ext.grid.GridPanel({
                region: 'center',
                store: dataStore,
                columns: [selectorModel,
                    { header: '@{R=Core.Strings;K=WEBJS_AK0_5;E=js}', width: 10, hidden: true, hideable: false, sortable: false, dataIndex: 'EngineID' },
                    { header: '@{R=Core.Strings;K=WEBJS_AK0_9;E=js}', width: 200, sortable: true, dataIndex: 'ServerName', renderer: renderName },
                    { header: '@{R=Core.Strings;K=WEBJS_AK0_6;E=js}', width: 90, sortable: true, dataIndex: 'KeepAlive', renderer: renderStatus },
                    { header: '@{R=Core.Strings;K=WEBJS_AK0_7;E=js}', width: 120, sortable: true, dataIndex: 'ServerType', renderer: renderServerType },
                    { header: '@{R=Core.Strings;K=WEBJS_AK0_8;E=js}', width: 150, sortable: true, dataIndex: 'Elements', renderer: renderText }
                ],
                sm: selectorModel, layout: 'fit', autoScroll: 'true', loadMask: true, width: 560, height: 400, stripeRows: true,
                bbar: pagingToolbar
            });


            var refreshObjects = function (callback) {
                grid.store.removeAll();
                grid.store.proxy.conn.jsonData = { supportedPollingEngineTypes: supportedEngineTypes };
                currentSearchTerm = "";
                grid.store.load({ callback: callback });
            };

            var orcInfoBox = new Ext.Panel({
                contentEl: 'AdditionalPollingOptionsOrcInfo',
                width: 560
            });

            var win = new Ext.Window({
                title: '@{R=Core.Strings;K=WEBJS_AK0_10;E=js}', resizable: false, closable: true, closeAction: 'hide', width: 580, plain: true, modal: true, items: [grid, orcInfoBox],
                buttons: [{
                    cls: "change-poll-engine-btn",
                    text: '@{R=Core.Strings;K=WEBJS_AK0_11;E=js}',
                    handler: function () {
                        if ($("#isDemoMode").length != 0) {
                            demoAction("Core_NodeManagement_ChangePollingEngine", this);
                            return;
                        } else {
                            if (grid.getSelectionModel().getSelections().length === 0)
                                return;
                            var selEngineId = grid.getSelectionModel().getSelections()[0].data.EngineID; //getting sel engine
                            var checkIsMainToOrcAssignment = function (callback, nodeIds) {
                                ORION.callWebService("/Orion/Services/NodeManagement.asmx",
                                    "GetNodesToEngineAssignmentInfo", {
                                        targetEngineId: selEngineId,
                                        nodesToReassignIds: nodeIds
                                    }, function (result) {
                                        if (result.isPollerToRemoteCollectorAssignment === false) {
                                            callback(nodeIds);
                                            return;
                                        }
                                        if (result.unsupportedAgentNodeSelected === true) {
                                            if (result.filteredNodesToReassignIds.length === 0) {
                                                Ext.Msg.show({
                                                    title: '@{R=Core.Strings;K=WEBJS_ML0_1;E=js}',
                                                    msg: '@{R=Core.Strings;K=WEBJS_ML0_2;E=js} <a href="' +
                                                        result.warningHelpLink +
                                                        '" target="_blank" rel="noopener noreferrer">@{R=Core.Strings;K=WEBDATA_AK0_404;E=js}&gt;&gt;&gt;</a>',
                                                    minWidth: 500,
                                                    buttons: Ext.Msg.OK,
                                                    icon: Ext.MessageBox.ERROR
                                                });
                                            } else {
                                                Ext.Msg.show({
                                                    title: '@{R=Core.Strings;K=WEBJS_ML0_1;E=js}',
                                                    msg: '@{R=Core.Strings;K=WEBJS_ML0_3;E=js}'
                                                        + '<br/><br/>@{R=Core.Strings;K=WEBJS_FL0_1;E=js} <a href="' + result.warningHelpLink + '" target="_blank" rel="noopener noreferrer">@{R=Core.Strings;K=WEBDATA_AK0_404;E=js}&gt;&gt;&gt;</a>'
                                                        + '<br/><br/>@{R=Core.Strings;K=WEBJS_ML0_4;E=js}',
                                                    minWidth: 500,
                                                    buttons: Ext.Msg.YESNO,
                                                    icon: Ext.MessageBox.WARNING,
                                                    fn: function (button) {
                                                        if (button === "yes") {
                                                            callback(result.filteredNodesToReassignIds);
                                                        }
                                                    }
                                                });
                                            }
                                            return;
                                        }
                                        Ext.Msg.show({
                                            title: '@{R=Core.Strings;K=WEBJS_AK0_10;E=js}',
                                            msg: '@{R=Core.Strings;K=WEBJS_FL0_1;E=js} <a href="' + result.warningHelpLink +'" target="_blank" rel="noopener noreferrer">@{R=Core.Strings;K=WEBDATA_AK0_404;E=js}&gt;&gt;&gt;</a>',
                                            minWidth: 500,
                                            buttons: Ext.Msg.OKCANCEL,
                                            icon: Ext.MessageBox.WARNING,
                                            fn: function (button) {
                                                if (button === "ok") {
                                                    callback(nodeIds);
                                                }
                                            }
                                        });
                                    }, ajaxErrorHandler);
                            };
                            
                            // check if we are on Manage Nodes page
                            if (window.MNG != undefined) {
                                //declaring update function
                                var updateNodesPollingEngine = function(nodeIds) {
                                    Ext.each(grid.getSelectionModel().getSelections(),
                                        function(item) {
                                            ORION.callWebService("/Orion/Services/NodeManagement.asmx",
                                                "UpdateNodesPollingEngine",
                                                {
                                                    engineId: item.data.EngineID,
                                                    nodeIds: nodeIds.join(",")
                                                }, function() {
                                                    win.close();
                                                    window.location.reload();
                                                }, ajaxErrorHandler
                                            );
                                        });
                                }
                                //updating engine for all nodes or just all selected
                                if (MNG.allSelected) {
                                    MNG.Nodes.WithAllIDsWithoutPrefix(function (nodeIds) {
                                        checkIsMainToOrcAssignment(updateNodesPollingEngine, nodeIds);
                                    });
                                } else {
                                    checkIsMainToOrcAssignment(updateNodesPollingEngine, MNG.GetSelectedObjectIDs().N);
                                }
                            } else {
                                var updatePagePollingEngines = function () {
                                    var items  = grid.getSelectionModel().getSelections();
                                    Ext.each(items, function (item) {
                                        $("#" + $("#selPollingEngineId")[0].value)[0].value = selEngineId;
                                        $("span[id*='pollEngineDescr']")[0].innerHTML = renderEngineLink(item);
                                    });
                                    __doPostBack($("#selPollingEngineId")[0].value, "OnValueChanged");
                                    win.close();
                                };
                                checkIsMainToOrcAssignment(updatePagePollingEngines, window.objectIds);
                            }
                        }
                    }
                },
                {
                    cls: "change-poll-engine-btn", text: '@{R=Core.Strings;K=CommonButtonType_Cancel;E=js}', handler: function () { win.hide(); }
                }]
            });

            $("a[id*='changePollEngineLink']").click(function () {
                if (!$(this).parent().hasClass("Disabled")) {
                    var selectedNodeIds = window.objectIds;
                    if (window.MNG != undefined) {
                        selectedNodeIds = MNG.GetSelectedObjectIDs().N;
                    }

                    orcInfoBox.hide();

                    ORION.callWebService("/Orion/Services/NodeManagement.asmx",
                        "GetSupportedEngineTypesAccordingSelectedNodes",
                        {
                            nodeIds: selectedNodeIds
                        },
                        function(result) {
                            supportedEngineTypes = result;

                            ORION.callWebService("/Orion/Services/NodeManagement.asmx",
                                "DoesAnyRemoteCollectorExist", {},
                                function (anyRemoteCollectorExists) {
                                    if (anyRemoteCollectorExists && !supportedEngineTypes.includes(SW.Orion.EngineType.RemoteCollector)) {
                                        orcInfoBox.show();
                                    }
                                    refreshObjects(function () {
                                        win.show();
                                    });
                                }, ajaxErrorHandler
                            );
                        }, ajaxErrorHandler
                    );
                }

                return false;
            });
        }
    };
}();

Ext.onReady(SW.Orion.ChangeEngineDialog.init, SW.Orion.ChangeEngineDialog);
