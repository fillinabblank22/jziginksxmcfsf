﻿printObject = function(obj)
{
    var output = "";

    $.each(obj, function(i, val)
    {
        output = output + '\n' + i + " : " + val;
    });

    alert(output);
};

/* 
    String format like in C#
*/
String.prototype.format = function(params)
{
    var pattern = /{([^}]+)}/g;
    return this.replace(pattern, function($0, $1) { return params[$1]; });
};

/*
    Returns array with unique values   
*/
Array.prototype.unique = function()
{
    var a = [], i, l = this.length;
    for (i = 0; i < l; i++)
    {
        if ($.inArray(this[i], a) < 0) { a.push(this[i]); }
    }
    return a;
};

/*
    Returns true if array contains specific element
*/
Array.prototype.contains = function(element)
{
    for (var i = 0; i < this.length; i++)
        if (this[i] == element) return true;
    return false;
};


var View =
{
    CancelLoading: function() { /* do nothing */ },

    expandImg: '/Orion/images/Button.Expand.gif',
    collapseImg: '/Orion/images/Button.Collapse.gif',
    pageSize: 250,

    Grid: '#MainGrid',
    GridPager: '#MainPager',

    FirstLoad: true,

    Searching: false,
    SearchMask: "",

    CustomProperties: null,
    
    APs:
    {
        Name: "@{R=Core.Strings;K=WEBJS_AK0_16;E=js}",
        GridObject: null,

        GroupBy: "NoGrouping",
        SelectedItem: "xxx",
        RowsNumber: 20,

        GroupByBox: [{ value: "NoGrouping", show: "@{R=Core.Strings;K=WEBJS_AK0_17;E=js}" },
                     { value: "Controllers", show: "@{R=Core.Strings;K=WEBJS_AK0_18;E=js}" },
                     { value: "WirelessType", show: "@{R=Core.Strings;K=WEBJS_AK0_19;E=js}" },
                     { value: "Vendor", show: "@{R=Core.Strings;K=WEBJS_AK0_20;E=js}" },
                     { value: "MachineType", show: "@{R=Core.Strings;K=WEBJS_AK0_21;E=js}" },
                     { value: "SNMPVersion", show: "@{R=Core.Strings;K=WEBJS_AK0_22;E=js}" },
                     { value: "Status", show: "@{R=Core.Strings;K=WEBJS_AK0_6;E=js}" },
                     { value: "Location", show: "@{R=Core.Strings;K=WEBJS_AK0_23;E=js}" },
                     { value: "Contact", show: "@{R=Core.Strings;K=WEBJS_AK0_24;E=js}"}],



        LoadValue: function(tagName, defaultValue)
        {
            return $("#APs_" + tagName).val() || defaultValue;
        },

        SaveValue: function(tagName, value)
        {
            $("#APs_" + tagName).val(value);
            WirelessSummary.SaveUserSetting("WirelessSummary_APs_" + tagName, value, function() { }, function(error) { alert(error.get_message()); })
        }
    },

    Clients:
    {
        Name: "@{R=Core.Strings;K=WEBJS_AK0_25;E=js}",
        GridObject: null,

        GroupBy: "ActiveClients",
        SelectedItem: "xxx",
        RowsNumber: 20,

        GroupByBox: [{ value: "ActiveClients", show: "@{R=Core.Strings;K=WEBJS_AK0_29;E=js}"}],
        
        
        LoadValue: function(tagName, defaultValue)
        {
            return $("#Clients_" + tagName).val() || defaultValue;
        },

        SaveValue: function(tagName, value)
        {
            $("#Clients_" + tagName).val(value);
            WirelessSummary.SaveUserSetting("WirelessSummary_Clients_" + tagName, value, function() { }, function(error) { alert(error.get_message()); })
        }
    },

    Objects: null,

    LoadValue: function(tagName, defaultValue)
    {
        return $("#" + tagName).val() || defaultValue;
    },

    SaveValue: function(tagName, value)
    {
        $("#" + tagName).val(value);
        WirelessSummary.SaveUserSetting("WirelessSummary_" + tagName, value, function() { }, function(error) { alert(error.get_message()); })
    }
};


View.LoadEmpty = function(pdata)
{
};


View.APs.Complete = function()
{
    var Rows = $(View.Grid).getGridParam('rowNum');
    View.APs.GridObject.rowNum = Rows;
    View.APs.SaveValue('RowsNumber', Rows);

    var rows = $(View.Grid).getDataIDs();

    if (rows)
    {
        $(rows).each(function()
        {
            var row = $(View.Grid).getRowData(this);
            var val = row['InfClientsCount'];

            if (val == undefined || val == 0) // when clients = 0 do not show toggle button for subgrid
                $("#MainGrid tbody tr[id=" + this + "] td:first").unbind();
            else  // when clients > 0 show toggle button for subgrid (hidden after load)
                $("#MainGrid tbody tr[id=" + this + "] td:first img").css('visibility', 'visible');
        });
    }
};


View.Clients.Complete = function()
{
    var Rows = $(View.Grid).getGridParam('rowNum');
    View.Clients.GridObject.rowNum = Rows;
    View.Clients.SaveValue('RowsNumber', Rows);
};


View.APs.LoadData = function(pdata)
{
    if (View.FirstLoad)
        return;

    if (View.Searching == true)
    {
        WirelessSummary.GetAPsSearch(View.SearchMask, pdata.page, pdata.rows, pdata.sidx, pdata.sord, function(xml)
        {
            var mygrid = $(View.Grid)[0];
            mygrid.addXmlData(xml);
        },
        PrintError);
        return;
    }

    switch (View.APs.GroupBy)
    {
        case "NoGrouping":
            {
                WirelessSummary.GetAPs(0, pdata.page, pdata.rows, pdata.sidx, pdata.sord, function(xml)
                {
                    var mygrid = $(View.Grid)[0];
                    mygrid.addXmlData(xml);
                },
                PrintError);
            }
            break;

        case "Controllers":
            {
                if (View.APs.SelectedItem != undefined)
                {
                    WirelessSummary.GetAPs(View.APs.SelectedItem, pdata.page, pdata.rows, pdata.sidx, pdata.sord, function(xml)
                    {
                        var mygrid = $(View.Grid)[0];
                        mygrid.addXmlData(xml);
                    },
                PrintError);
                }
            }
            break;


        case "WirelessType":
        case "Vendor":
        case "MachineType":
        case "SNMPVersion":
        case "Status":
        case "Location":
        case "Contact":
            {
                if (View.APs.SelectedItem != undefined)
                {
                    WirelessSummary.GetAPsWhere('Inf' + View.APs.GroupBy, View.APs.SelectedItem, pdata.page, pdata.rows, pdata.sidx, pdata.sord, function(xml)
                    {
                        var mygrid = $(View.Grid)[0];
                        mygrid.addXmlData(xml);
                    },
                    PrintError);
                }
            }
            break;

        // custom properties
        default:    
            {
                if (View.APs.SelectedItem != undefined)
                {
                    WirelessSummary.GetAPsWhereCustom(View.APs.GroupBy, View.APs.SelectedItem, pdata.page, pdata.rows, pdata.sidx, pdata.sord, function(xml)
                    {
                        var mygrid = $(View.Grid)[0];
                        mygrid.addXmlData(xml);
                    },
                    PrintError);
                }
            }
            break;
    }
};


View.APs.LoadSubgridData = function(pdata, grid_id, id)
{
    WirelessSummary.GetClients(id, pdata.page, pdata.rows, pdata.sidx, pdata.sord, function(xml)
    {
        var mygrid = $('#' + grid_id)[0];
        mygrid.addXmlData(xml);
    },
    PrintError);

};


View.Clients.LoadData = function(pdata)
{
    if (View.FirstLoad)
        return;
    //printObject(pdata);

    if (View.Searching == true)
    {
        WirelessSummary.GetClientsSearch(View.SearchMask, pdata.page, pdata.rows, pdata.sidx, pdata.sord, function(xml)
        {
            var mygrid = $(View.Grid)[0];
            mygrid.addXmlData(xml);
        },
        PrintError);
        return;
    }

    switch (View.Clients.GroupBy)
    {
        case "ActiveClients":
            {
                WirelessSummary.GetClients(0, pdata.page, pdata.rows, pdata.sidx, pdata.sord, function(xml)
                {
                    var mygrid = $(View.Grid)[0];
                    mygrid.addXmlData(xml);
                },
                PrintError);
            }
            break;

        default:
            break;
    }

};


InitGridObjects = function()
{
    var Pager = $('#MainPager')[0];
    var ImgFiles = "/Orion/js/jqGrid/themes/basic/images";
    var Height = '500px';
    //var Height = '100%';

    View.APs.GridObject =
    {
        // events
        gridComplete: View.APs.Complete,
        loadui: 'block',
        datatype: View.APs.LoadData,
        pager: $(View.GridPager),
        imgpath: ImgFiles,
        forceFit: true,
        colNames: ['@{R=Core.Strings;K=WEBJS_AK0_16;E=js}', '@{R=Core.Strings;K=WEBJS_AK0_30;E=js}', '@{R=Core.Strings;K=WEBJS_AK0_7;E=js}', '@{R=Core.Strings;K=WEBJS_AK0_33;E=js}', '@{R=Core.Strings;K=WEBJS_AK0_32;E=js}', '@{R=Core.Strings;K=WEBJS_AK0_31;E=js}', ''],
        colModel:
                [
                    { name: 'InfName', width: @{R=Core.Strings;K=UIDATA_TM0_9;E=js} },
                    { name: 'InfIPAddress', align: 'center', width: @{R=Core.Strings;K=UIDATA_TM0_10;E=js} },
                    { name: 'InfWirelessType', align: 'center', width: @{R=Core.Strings;K=UIDATA_TM0_11;E=js} },
                    { name: 'SSIDs', align: 'left', sortable: false, width: @{R=Core.Strings;K=UIDATA_TM0_12;E=js} },
                    { name: 'Channels', align: 'left', sortable: false, width: @{R=Core.Strings;K=UIDATA_TM0_13;E=js} },
    	            { name: 'InfClientsCount', align: 'center', width: @{R=Core.Strings;K=UIDATA_TM0_14;E=js} },
    	            { name: 'Notes', sortable: false, resizable: false, width: @{R=Core.Strings;K=UIDATA_TM0_15;E=js} }
                ],
        rowNum: 20,
        rowList: [10, 20, 30, 50, 100],
        sortname: 'InfName',
        sortorder: 'asc',
        viewrecords: true,
        height: Height,
        subGrid: true,
        subGridRowExpanded: function(subgrid_id, row_id)
        {
            var subgrid_table_id, subgrid_pager_id;
            subgrid_pager_id = subgrid_id + "_p";
            subgrid_table_id = subgrid_id + "_t";
            jQuery("#" + subgrid_id).html("<table id='" + subgrid_table_id + "' class='scroll'></table><div id='" + subgrid_pager_id + "' class='scroll' style='text-align:center;'></div>");
            jQuery("#" + subgrid_table_id).jqGrid(
            {
                //url: "subgrid.php?q=2&id=" + row_id,
                datatype: function(pdata)
                {
                    View.APs.LoadSubgridData(pdata, subgrid_table_id, row_id);
                },

                colNames: ['', '@{R=Core.Strings;K=WEBJS_AK0_34;E=js}', '@{R=Core.Strings;K=WEBJS_AK0_41;E=js}', '@{R=Core.Strings;K=WEBJS_AK0_30;E=js}', '@{R=Core.Strings;K=WEBJS_AK0_40;E=js}', '@{R=Core.Strings;K=WEBJS_AK0_39;E=js}', '@{R=Core.Strings;K=WEBJS_AK0_38;E=js}', '@{R=Core.Strings;K=WEBJS_AK0_37;E=js}', '@{R=Core.Strings;K=WEBJS_AK0_36;E=js}', '@{R=Core.Strings;K=WEBJS_AK0_35;E=js}'],
                colModel: [
                { name: "InfClient_Icon", sortable: false, resizable: false, align: "center", width: 20 },
                { name: "InfClient_Name", width: @{R=Core.Strings;K=UIDATA_TM0_16;E=js} },
                { name: "InfClient_SSID", width: @{R=Core.Strings;K=UIDATA_TM0_17;E=js} },
                { name: "InfClient_IPAddress", align: 'center', width: @{R=Core.Strings;K=UIDATA_TM0_18;E=js} },
                { name: "InfClient_MAC", align: 'center', width: @{R=Core.Strings;K=UIDATA_TM0_19;E=js} },
                { name: "InfClient_SignalStrength", sortable: false, align: "center", width: @{R=Core.Strings;K=UIDATA_TM0_20;E=js} },
                { name: "InfFirstUpdate", width: @{R=Core.Strings;K=UIDATA_TM0_21;E=js} },
                { name: "InfClient_TxRate", width: @{R=Core.Strings;K=UIDATA_TM0_22;E=js}, align: "center" },
                { name: "InfClient_TotalBytesRx", width: @{R=Core.Strings;K=UIDATA_TM0_23;E=js}, align: "right" },
                { name: "InfClient_TotalBytesTx", width: @{R=Core.Strings;K=UIDATA_TM0_24;E=js}, align: "right" }
                ],

                loadui: 'block',
                forceFit: true,
                viewrecords: true,
                height: '100%',
                pager: subgrid_pager_id,
                rowNum: 10,
                rowList: [5, 10, 20, 50, 75, 100],
                imgpath: ImgFiles,
                sortname: 'InfClient_SSID',
                sortorder: "asc"
            });
        }
    };

    View.Clients.GridObject =
    {
        gridComplete: View.Clients.Complete,
        datatype: View.Clients.LoadData,
        pager: $(View.GridPager),
        loadui: 'block',
        imgpath: ImgFiles,
        forceFit: true,
        colNames: ['', '@{R=Core.Strings;K=WEBJS_AK0_34;E=js}', '@{R=Core.Strings;K=WEBJS_AK0_41;E=js}', '@{R=Core.Strings;K=WEBJS_AK0_30;E=js}', '@{R=Core.Strings;K=WEBJS_AK0_40;E=js}', '@{R=Core.Strings;K=WEBJS_AK0_39;E=js}', '@{R=Core.Strings;K=WEBJS_AK0_38;E=js}', '@{R=Core.Strings;K=WEBJS_AK0_37;E=js}', '@{R=Core.Strings;K=WEBJS_AK0_36;E=js}', '@{R=Core.Strings;K=WEBJS_AK0_35;E=js}',''],
        colModel: [
                { name: "InfClient_Icon", sortable: false, resizable: false, align: "center", width: 20 },
                { name: "InfClient_Name", width: @{R=Core.Strings;K=UIDATA_TM0_25;E=js} },
                { name: "InfClient_SSID", width: @{R=Core.Strings;K=UIDATA_TM0_26;E=js} },
                { name: "InfClient_IPAddress", align: 'center', width: @{R=Core.Strings;K=UIDATA_TM0_27;E=js} },
                { name: "InfClient_MAC", align: 'center', width: @{R=Core.Strings;K=UIDATA_TM0_28;E=js} },
                { name: "InfClient_SignalStrength", sortable: false, align: "center",width: @{R=Core.Strings;K=UIDATA_TM0_29;E=js} },
                { name: "InfFirstUpdate", width: @{R=Core.Strings;K=UIDATA_TM0_30;E=js} },
                { name: "InfClient_TxRate", width: @{R=Core.Strings;K=UIDATA_TM0_31;E=js}, align: "center" },
                { name: "InfClient_TotalBytesRx", width: @{R=Core.Strings;K=UIDATA_TM0_32;E=js}, align: "right" },
                { name: "InfClient_TotalBytesTx", width: @{R=Core.Strings;K=UIDATA_TM0_33;E=js}, resizable: false, align: "right" },
                { name: "Scroll", sortable: false, resizable: false, width: 15 }
               ],
        rowNum: 20,
        rowList: [10, 20, 30, 50, 100],
        sortname: 'InfClient_SSID',
        sortorder: 'asc',
        viewrecords: true,
        height: Height,
        subGrid: false
    };
};


/* 
    Prints error message if web method returns error
*/
PrintError = function(msg)
{
    if (msg.get_message() == "@{R=Core.Strings;K=WEBJS_VB0_67;E=js}")
    {
        // login cookie expired. reload the page so we get bounced to the login page.
        alert('@{R=Core.Strings;K=WEBJS_AK0_26;E=js}');
        window.location.reload();
    }
    else
    {
        //alert('web method error:' + msg.get_message());
        $("#test").text(msg.get_message());
        $("#stackTrace").text(msg.get_stackTrace());
    }
};


/*
    Clears errors on web page
*/
View.ClearError = function()
{
    $("#originalQuery,#test,#stackTrace").text('');
};


/* 
    Fills group by list 
*/
View.Clients.LoadGroupByValues = function()
{
    var prop = $("#groupByProperty").val();
    
    View.Clients.SaveValue('GroupBy', prop);
    
    if (prop)
    {
        View.FirstLoad = false;
        View.Searching = false;
        $("#search").val('');
        View.ClearError(); 
        $(".NodeGroupItems").empty();

        switch (prop)
        {
            case "ActiveClients":
                View.Clients.GroupBy = "ActiveClients";
                $(View.Grid).trigger("reloadGrid");
                break;

            default:
                break;
        }
    }
};


SelectLink = function()
{
    var tag = $(".NodeGroupItem a[value='" + View.Objects.SelectedItem + "']");

    if (tag.length == 1)
    {
        tag.click();
    }
    else // select first item in list if exist
    {
        tag = $(".NodeGroupItem a:first");
        if (tag.length == 1)
        {
            tag.click();
        }
    }
};


View.Search = function()
{
    View.SearchMask = $("#search").val();

    if ($.trim(View.SearchMask))
    {
        View.Searching = true;

        // clear selected item
        $(".NodeGroupItems").children().removeClass("SelectedNodeGroupItem");
        // reload grid - search data
        $(View.Grid).trigger("reloadGrid");
    }
    else
    {
        // reload, redraw
        $("#groupByProperty").change();
    }
};

ParseStatus = function(statusValue)
{
    return SW.Core.Status.Get(statusValue).ShortDescription;
};

ParseBool = function(boolValue)
{
    switch (boolValue)
    {
        case "True": return "@{R=Core.Strings;K=WEBJS_AK0_28;E=js}";
        case "False": return "@{R=Core.Strings;K=WEBJS_AK0_27;E=js}";
        default: return "@{R=Core.Strings;K=WEBJS_AK0_27;E=js}";
    }
};

View.APs.LoadGroupByValues = function()
{
    var prop = $("#groupByProperty").val();

    View.APs.SaveValue('GroupBy', prop);

    if (prop)
    {
        View.FirstLoad = false;
        View.Searching = false;
        $("#search").val('');
        View.ClearError();
        $(".NodeGroupItems").empty();

        switch (prop)
        {
            case "NoGrouping":
                View.APs.GroupBy = "NoGrouping";
                $(View.Grid).trigger("reloadGrid");
                break;

            case "Controllers":
                {
                    View.APs.GroupBy = prop;

                    WirelessSummary.GroupByControllers(function(list)
                    {
                        if (list != undefined)
                        {
                            $(list).each(function()
                            {
                                /* add lines to list */
                                $("<a href='#'></a>").attr('value', this.ID).text(this.Name + ' (' + this.Count + ')')
                                .click(View.APs.GroupByList).appendTo(".NodeGroupItems").wrap("<li class='NodeGroupItem'/>");

                                // add icons    
                                $(this.Icon).prependTo(".NodeGroupItems a[value='" + this.ID + "']").after(" ");
                            });

                            SelectLink(); //select first or by value
                        }
                    },
                    PrintError);
                }
                break;

            case "WirelessType":
            case "Vendor":
            case "MachineType":
            case "SNMPVersion":
            case "Status":
            case "Location":
            case "Contact":
                {
                    View.APs.GroupBy = prop;

                    var preffix = 'N_';
                    if (prop == "WirelessType") preffix = '';

                    WirelessSummary.GroupByList(preffix + prop, function(list)
                    {
                        if (list != undefined)
                        {
                            $(list).each(function()
                            {
                                /* add lines to list */
                                var Title = this.Name;

                                if (prop == "Status")
                                    Title = ParseStatus(this.Value);

                                // names longer than 20 chars are cutted    
                                Title = TextCut(Title, 20);

                                $("<a href='#'></a>").attr('value', this.Value).html(Title + ' (' + this.Count + ')')
                                .click(View.APs.GroupByList).appendTo(".NodeGroupItems").wrap("<li class='NodeGroupItem'/>");

                                // add vendor icons    
                                if (prop == "Vendor")
                                {
                                    $('<img src="/NetPerfMon/images/Vendors/' + this.Custom + '" />').error(function()
                                    {
                                        this.src = "/NetPerfMon/images/Vendors/Unknown.gif";
                                        return true;
                                    }).prependTo(".NodeGroupItems a[value='" + this.Value + "']").after(" ");
                                }
                            });

                            SelectLink(); //select first or by value
                        }
                    },
                    PrintError);
                }
                break;

            // custom properties              
            default:
                {
                    View.APs.GroupBy = prop;

                    WirelessSummary.CustomPropertyList(prop, function(list)
                    {
                        if (list != undefined)
                        {
                            $(list).each(function()
                            {
                                var Title = this.Name;

                                if (View.CustomProperties[prop] == 'System.Boolean')
                                    Title = ParseBool(this.Value);

                                // names longer than 20 chars are cutted    
                                Title = TextCut(Title, 20);

                                /* add lines to list */
                                $("<a href='#'></a>").attr('value', this.Value).html(Title + ' (' + this.Count + ')')
                                .click(View.APs.GroupByList).appendTo(".NodeGroupItems").wrap("<li class='NodeGroupItem'/>");
                            });

                            SelectLink(); //select first or by value
                        }
                    },
                    PrintError);
                }
                break;
        }
    }
};


View.APs.GroupByList = function()
{
    $(this).parent().addClass("SelectedNodeGroupItem").siblings().removeClass("SelectedNodeGroupItem");
    View.Searching = false;
    $("#search").val('');
    View.ClearError();
    View.APs.SelectedItem = $(this).attr('value');
    View.APs.SaveValue('SelectedItem', View.APs.SelectedItem);
    $(View.Grid).trigger("reloadGrid");
    return false;
}

/*
    Switch between APs/Clients view (shows specific grids)
*/
View.ChangeObjectsType = function(objType)
{
    if (objType == View.Objects) return;

    View.Objects = objType;

    $("#groupByTag").empty();
    $(".NodeGroupItems").empty();

    var options = $.map(View.Objects.GroupByBox, function(c)
    {
        if (c.value == View.Objects.GroupBy)
            return "<option selected='selected' value='" + c.value + "'>" + c.show + "</option>"
        return "<option value='" + c.value + "'>" + c.show + "</option>"
    }).join("");

    $("#groupByTag").append($("<select>").attr("id", "groupByProperty").css("width", "100%").change(View.Objects.LoadGroupByValues).append(options));

    // unload and load grid
    $(View.Grid).GridUnload();
    View.FirstLoad = true; // do not show data
    $(View.Grid).jqGrid(View.Objects.GridObject);
    /*
    $(View.Grid).navGrid(View.GridPager, {
    refresh: true, add: false, edit: false, find: false, del: false, search: false
    });
    */

    // because of IE6, wait for items and than select one
    setTimeout("FillGroupByBox()", 25);

    //GroupBox.val(View.Objects.GroupBy);
    //GroupBox.change();
};

FillGroupByBox = function()
{
    //$('#groupByProperty').val(View.Objects.GroupBy);
    View.Objects.LoadGroupByValues();
    //$('#groupByProperty').change();
};

TextCut = function(text, length)
{
    /*
    if (text.length > length)
        text = text.substring(0, length) + '...';
    */
    return text;
};

/*
    Main function
*/
jQuery(document).ready(function()
{
    // load custom properties column names
    var CustomProperties = View.LoadValue('NodeCustomProperties', '').split(',');
    var CustomPropertiesTypes = View.LoadValue('NodeCustomPropertiesTypes', '').split(',');

    // add custom properties to the GroupByBox
    View.CustomProperties = new Object;
    if (CustomProperties != '' && CustomPropertiesTypes != '')
    {
        for (var i = 0; i < CustomProperties.length; i++)
        {
            // properties longer than 20 chars are cutted
            View.APs.GroupByBox.push({ value: CustomProperties[i], show: TextCut(CustomProperties[i],20) });
            View.CustomProperties[CustomProperties[i]] = CustomPropertiesTypes[i];
        }
    }

    $("form").submit(function() { return false; });

    InitGridObjects();
    
    // load saved properties
    View.APs.GroupBy = View.APs.LoadValue('GroupBy', View.APs.GroupBy);
    View.APs.SelectedItem = View.APs.LoadValue('SelectedItem', View.APs.SelectedItem);
    View.APs.GridObject.rowNum = View.APs.LoadValue('RowsNumber', View.APs.RowsNumber);
    View.Clients.GroupBy = View.Clients.LoadValue('GroupBy', View.Clients.GroupBy);
    View.Clients.SelectedItem = View.Clients.LoadValue('SelectedItem', View.Clients.SelectedItem);
    View.Clients.GridObject.rowNum = View.Clients.LoadValue('RowsNumber', View.Clients.RowsNumber);

    $("#showObjectType").val(View.LoadValue('ObjectType', 'View.APs'));
    $("#showObjectType").change(function()
    {
        var value = $("#showObjectType").val();

        // save object type
        View.SaveValue('ObjectType', value);

        if (value == "View.APs")
            View.ChangeObjectsType(View.APs);
        else
            View.ChangeObjectsType(View.Clients);
    });

    $("#showObjectType").change();



    /*
    $("#search").keypress(function(e)
    {
    if (e.which == 13)
    View.Search();
    });
    */

    $(".wirelessSearchClick").click(View.Search);
});
