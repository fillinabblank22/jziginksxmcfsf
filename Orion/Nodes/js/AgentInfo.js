﻿/// <reference path="../../typescripts/typings/jqueryui.d.ts" />
/// <reference path="../../js/asyncfileupload.ts" />
var SW;
(function (SW) {
    (function (Core) {
        (function (AddNode) {
            var AgentInfo = (function () {
                function AgentInfo() {
                }
                AgentInfo.refreshVisibility = function (privateKeyFileElementId, privateKeyValueElementId, privateKeyFileNameElementId) {
                    var fileName = $('#' + privateKeyFileNameElementId).val();
                    var certValue = $('#' + privateKeyValueElementId).val();

                    if (fileName && certValue) {
                        $('#AgentCertificateViaFileBlock').hide();
                        $('#AgentCertificateResetBlock').show();
                        $('#AgentCertificateFileName').text(fileName);
                    } else if (!fileName && certValue) {
                        $('#AgentCertificateViaFileBlock').hide();
                        $('#AgentCertificateManualBlock').show();
                        $('#AgentCertificateResetBlock').hide();
                    }
                };
                AgentInfo.uploadCertificateFile = function (privateKeyFileElementId, privateKeyValueElementId, privateKeyFileNameElementId) {
                    var _this = this;
                    if (!$('#' + privateKeyFileElementId).val())
                        return;

                    var processContent = function (fileContent) {
                        $('#' + privateKeyValueElementId).val(fileContent);
                        $('#AgentCertificateViaFileBlock').hide();
                        $('#AgentCertificateResetBlock').show();

                        var fileName = _this.getSelectedFileName(privateKeyFileElementId);
                        $('#AgentCertificateFileName').text(fileName);
                        $('#' + privateKeyFileNameElementId).val(fileName);
                    };

                    var upload = function () {
                        var uploader = new SW.Core.AsyncFileUpload();
                        uploader.uploadFile({
                            url: "/api/AgentManagementDeployment/GetFileContentAsText",
                            fileElementId: privateKeyFileElementId
                        }, function (fileContent) {
                            return processContent(fileContent);
                        });
                    };

                    try  {
                        var file = document.getElementById(privateKeyFileElementId).files[0];
                        if (file) {
                            var reader = new FileReader();
                            reader.readAsText(file, "UTF-8");
                            reader.onload = function (evt) {
                                return processContent(evt.target.result.toString());
                            };
                            reader.onerror = function () {
                                return upload();
                            };
                        }
                    } catch (e) {
                        console.log(e);
                        upload();
                    }
                };

                AgentInfo.pasteCertificateManually = function (privateKeyValueElementId, privateKeyFileNameElementId) {
                    $('#' + privateKeyFileNameElementId).val('');
                    $('#certificatePasteTextarea').val('');
                    $('#certificatePasteDialog').dialog({
                        modal: true,
                        position: ['middle', 100],
                        height: 'auto',
                        width: 'auto',
                        title: '@{R=Core.Strings;K=AgentDeployment_PasteKeyDialogLabel;E=js}',
                        buttons: {
                            Ok: function () {
                                $('#' + privateKeyValueElementId).val($('#certificatePasteTextarea').val());
                                $('#certificatePasteDialog').dialog("close");
                                $('#AgentCertificateViaFileBlock').hide();
                                $('#AgentCertificateManualBlock').show();
                                $('#AgentCertificateResetBlock').hide();
                            },
                            Cancel: function () {
                                $('#certificatePasteDialog').dialog("close");
                            }
                        }
                    });
                };

                AgentInfo.resetCertificate = function (privateKeyFileElementId, privateKeyValueElementId, privateKeyFileNameElementId) {
                    $('#' + privateKeyFileElementId).replaceWith($("#" + privateKeyFileElementId).val('').clone(true));
                    $('#' + privateKeyValueElementId).val('');
                    $('#' + privateKeyFileNameElementId).val('');
                    $('#AgentCertificateFileName').val('');
                    $('#AgentCertificateViaFileBlock').show();
                    $('#AgentCertificateManualBlock').hide();
                    $('#AgentCertificateResetBlock').hide();
                };

                AgentInfo.viewPrivateKey = function (privateKeyValueElementId) {
                    $('#certificateViewElement').text($('#' + privateKeyValueElementId).val());
                    $('#certificateViewDialog').dialog({
                        modal: true,
                        position: ['middle', 100],
                        height: 'auto',
                        width: 500,
                        title: '@{R=Core.Strings;K=AgentDeployment_ViewKeyDialogLabel;E=js}',
                        buttons: {
                            Close: function () {
                                setTimeout(function () {
                                    $('#certificateViewDialog').dialog("close");
                                }, 200);
                            }
                        }
                    });
                };

                AgentInfo.getSelectedFileName = function (privateKeyFileElementId) {
                    var path = $('#' + privateKeyFileElementId).val();
                    var index1 = path.lastIndexOf('/');
                    var index2 = path.lastIndexOf('\\');
                    return path.substring(Math.max(index1, index2) + 1);
                };
                return AgentInfo;
            })();
            AddNode.AgentInfo = AgentInfo;
        })(Core.AddNode || (Core.AddNode = {}));
        var AddNode = Core.AddNode;
    })(SW.Core || (SW.Core = {}));
    var Core = SW.Core;
})(SW || (SW = {}));
