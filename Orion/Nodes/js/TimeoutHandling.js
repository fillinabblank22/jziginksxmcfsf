﻿$(function () {
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequest);
});



function EndRequest(sender, args) {


    // Check to see if there's an error on this request.
    if (args.get_error() != undefined && args.get_error().name == 'Sys.WebForms.PageRequestManagerTimeoutException') {
        // If there is, show the custom error.
        alert('@{R=Core.Strings;K=WEBJS_AK0_13;E=js}');
        args.set_errorHandled(true);
    }
    else if (args.get_error() != undefined && args.get_error().name == 'Sys.WebForms.PageRequestManagerParserErrorException') {
        //show message
        var r = confirm('@{R=Core.Strings;K=WEBJS_AK0_14;E=js}');
        if (r) {
            window.location.reload();
        }
    }
    else if (args.get_error() != undefined) {
        alert('@{R=Core.Strings;K=WEBJS_AK0_15;E=js}' + args.get_error().name + '\n' + args.get_error().message + '\n'
           + args.get_error().description + '\n' + args.get_error().httpStatusCode);
    }


}