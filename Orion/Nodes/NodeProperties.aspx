<%@ Page Title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_396 %>" Language="C#" MasterPageFile="~/Orion/Nodes/NodeManagementPage.master"
    AutoEventWireup="true" CodeFile="NodeProperties.aspx.cs" Inherits="Orion_Nodes_NodeProperties" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Orion/Nodes/Controls/NodeNameIP.ascx" TagPrefix="orion" TagName="NodeNameIp" %>
<%@ Register Src="~/Orion/Nodes/Controls/DynamicIPVersion.ascx" TagPrefix="orion" TagName="DynamicIPVersion" %>
<%@ Register Src="~/Orion/Nodes/Controls/SnmpInfo.ascx" TagPrefix="orion" TagName="NodeSNMP" %>
<%@ Register Src="~/Orion/Nodes/Controls/WmiInfo.ascx" TagPrefix="orion" TagName="WMIInfo" %>
<%@ Register Src="~/Orion/Nodes/Controls/AgentInfo.ascx"  TagPrefix="orion" TagName="AgentInfo" %>
<%@ Register Src="~/Orion/Nodes/Controls/NodeCategory.ascx"  TagPrefix="orion" TagName="NodeCategory" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Nodes/Controls/PollingSettings.ascx" TagPrefix="orion" TagName="NodePolling" %>
<%@ Register Src="~/Orion/Nodes/Controls/DependenciesControl.ascx" TagPrefix="orion" TagName="Dependencies" %>
<%@ Register Src="~/Orion/Nodes/Controls/ListOfNodesValidation.ascx" TagPrefix="orion" TagName="ListOfNodesValidation" %>
<%@ Register Src="~/Orion/Nodes/Controls/ICMPSettings.ascx" TagPrefix="orion" TagName="ICMPSettings" %>
<%@ Register Src="~/Orion/Nodes/Controls/NodePropertyPluginSelector.ascx"  TagPrefix="orion" TagName="PluginSelector" %>
<%@ Register Src="~/Orion/Nodes/Controls/PollingMethodSelector.ascx"  TagPrefix="orion" TagName="PollingMethodSelector" %>
<%@ Register Src="~/Orion/Nodes/Controls/ViewSelector.ascx"  TagPrefix="orion" TagName="ViewSelector" %>
<%@ Register Src="~/Orion/Nodes/Controls/StatusRollupSelector.ascx"  TagPrefix="orion" TagName="StatusRollupSelector" %>
   
<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <style>
        #ctl00_ctl00_ContentPlaceHolder1_adminContentPlaceholder_UpdateProgress1
        {
            display: inline;
        }
        
        .sw-pg-selected-items ul { list-style-type: disc; margin: 20px; }
        .sw-pg-selected-items li { margin-left: 20px;}

        #changeMethodAgentUninstall { margin-top: 20px; }
        #changeMethodAgentInstall { margin-top: 20px; }
        #changeMethodAgentInstallTitle { width: 100%; border-bottom: 1px solid #D7D7D7; padding-bottom: 2px; margin-bottom: 2px; }
        #uninstallAgentText { margin-left: 5px; }
    </style>
    <orion:Include runat="server" File="js/jquery/jquery.autocomplete.css" />
    <orion:Include runat="server" File="jquery/jquery.autocomplete.js" />
    <orion:Include runat="server" File="Nodes/js/TimeoutHandling.js" />
    <orion:Include runat="server" File="RenderControl.js" />
</asp:Content>

<asp:Content ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton runat="server" HelpUrlFragment="OrionPHNodeManagementEditProperties" />
    <asp:HiddenField ID="HiddenFieldGuid" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">

   <h1 class="sw-hdr-title"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
   <div class="sw-hdr-subtitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_384) %></div>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" OnLoad="PanelOnLoad">
        <ContentTemplate>
            <div class="sw-pg-selected-items">
                <asp:BulletedList runat="server" ID="blNodes">
                </asp:BulletedList>
            </div>

            <div class="GroupBox">
                <div class="contentBlock">
                    <orion:NodeNameIp runat="server" ID="NodeNameIP" />
                    <table>
                        <orion:PluginSelector runat = "server" ID="PluginSelector"  visible="False"/>
                    </table>
                </div>

                <div class="contentBlock">
                    <orion:DynamicIpVersion runat="server" ID="DynamicIPVersion" />
                    <table>
                        <orion:PluginSelector runat = "server" ID="DynamicIPVersionSelector"  visible="False"/>
                    </table>
                </div>
                
                <orion:ViewSelector runat="server" ID="ViewSelector" />

                <orion:StatusRollupSelector runat="server" ID="StatusRollupSelector" />

                <table class="PollingMethodSelectorTable" width="100%">
                  <orion:PollingMethodSelector runat="server" ID="PollingMethodSelector" OnPollingMethodChanged="HostNameIP_SNMPModeChanged" TrCssClass="blueBox" Mode="EditProperies"/>                                                          
                    <%if (!PollingMethodSelector.Visible)
                      {%>
                    <tr>
                        <td>
                            <asp:Literal ID="OuterTitleDisabled" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_1 %>" />
                            <br />
                            <div class="sw-suggestion"><span class="sw-suggestion-icon"></span>
                                <%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_VS0_1, 
                                @"<a class='coloredLink' id='rediscoverLink' href='/Orion/Nodes/Default.aspx#' target='_blank'>", @"</a>",
                                                                                                string.Format(@"<a class='coloredLink' href='{0}' target='_blank'>", SolarWinds.Orion.Web.Helpers.KnowledgebaseHelper.GetKBUrl(3289)), @"</a>")) %>  </div> </td>
                    </tr>
                    <%
                        }
                    %>
                </table>
                                
                <div class="contentBlock">
                    <orion:NodePolling runat="server" ID="NodePolling" ShowPollingEngineDetails="true" OnEngineChanged="NodePolling_OnEngineChanged" />
                </div>
                
                <div id="dependencyObjectIds" style="display: none" runat="server"></div>
                <div id="dependenciesAsyncResource"><div class="leftSectionTitle" style="margin-top: 10px"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_385) %></div><br/><img src="/Orion/images/loading_gen_small.gif"/></div>
                
                <br />
                <div class="contentBlock">                    
                    <orion:NodeCategory runat="server" ID="NodeCategory" />                    
                </div>

                <div id="pluginDiv" runat="server">
                </div>
                <br />

                <table>
                    <tr>
                        <td class="leftLabelColumn"></td>
                        <td>
                            <asp:UpdateProgress DynamicLayout="false" DisplayAfter="50" ID="UpdateProgress1" runat="server">
                                <ProgressTemplate>
                                    <img alt="Loading" src="../images/AJAX-Loader.gif" />
                                    <span style="font-weight:bold;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_114) %></span>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                    </tr>
                    <tr>
                        <td class="leftLabelColumn">
                        </td>
                        <td>
                            <div class="sw-btn-bar">
                                <orion:LocalizableButton id="imbtnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" automation="Submit" OnClick="imbtnSubmit_Click" />
                                <orion:LocalizableButton id="imbtnCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" automation="Cancel" OnClick="imbtnCancel_Click" CausesValidation="false"/></div>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="display: none; visibility: hidden;">
                <asp:Button ID="FakeTarget" runat="server" />
            </div>
            <cc1:ModalPopupExtender ID="ListOfNodesValidationModalPopupExtender" PopupControlID="ListDiv"
                TargetControlID="FakeTarget" runat="server">
            </cc1:ModalPopupExtender>
            <div runat="server" id="ListDiv" style="background-color: #FFF; border: solid 1px #333;
                z-index: 110000;">
                <orion:ListOfNodesValidation runat="Server" ID="ListOfNodesValidation" Visible="false" />
            </div>

            <div style="display: none; visibility: hidden">
                <div id="ChangeMethod" style="background-color: #FFF; z-index: 110000;" title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_ET0_02) %>">

                    <table><tr><td style="width:40px;" valign="top">
                                <img src="/Orion/Images/stop_32x32.gif" alt="error" />
                    </td><td>
                                <div id="warningMessage">
                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_ET0_03) %>
                            <br /><br /> 
                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_ET0_04) %>
                            <br /><span style='font-weight:normal'>&#0187;&nbsp;<a class="sw-link" target="_blank" rel="noopener noreferrer" href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("OrionCoreAG-ChoosingPollingMethods")) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_ET0_06) %></a></span>
                            <br /><br />
                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_ET0_05) %>
                            <br /><span style='font-weight:normal'>&#0187;&nbsp;<a class="sw-link" target="_blank" rel="noopener noreferrer" href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.KnowledgebaseHelper.GetKBUrl(3621)) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_ET0_07) %></a></span>
                                    <br />
                                </div>
                                <div id="changeMethodAgentUninstall">
                                    <input id="uninstallAgent" type="checkbox" /><span id="uninstallAgentText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ChangePollingMethod_UninstallAgent) %></span>
                                </div>
                                <div id="changeMethodAgentInstall">
                                    <div id="changeMethodAgentInstallTitle" class="changeMethodSubtitle"><h2><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ChangePollingMethod_InstallAgent_Title) %></h2></div>
                                    <div id="changeMethodAgentInstallText" class="changeMethodSection"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ChangePollingMethod_InstallAgent_Text) %></div>
                                </div>
                    </td></tr></table>
                    <div class="bottom">
                        <div class="sw-btn-bar-wizard">
                            <orion:LocalizableButton runat="server" ID="btnWarnConfirm" LocalizedText="CustomText" DisplayType="Secondary"  Text="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_ET0_01 %>" OnClientClick="ChangeMethodClose();"  OnClick="imbtnSubmit_Click"/>
		                    <orion:LocalizableButton runat="server" ID="btnWarnCancel" LocalizedText="Cancel" DisplayType="Primary" OnClientClick="ChangeMethodClose(); return false;" />
                        </div>
                    </div>
                </div>
            </div>
            <asp:HiddenField runat="server" ID="hUninstallAgent" Value="0" />
            <asp:HiddenField runat="server" ID="hAgentInstalled" Value="0" />

            <div style="display: none; visibility: hidden">
                <div id="ConfirmValidation" style="background-color: #FFF; z-index: 110000;" title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_JP1_1) %>">

                    <table><tr><td style="width:40px;" valign="top">
                                <img src="/Orion/Images/info_32x32.gif" />
                    </td><td>
                                <div>
                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_JP1_2) %>
                            <br /><br /> 
                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_JP1_3) %>
                                </div>
                    </td></tr></table>
                    <div class="bottom">
                        <div class="sw-btn-bar-wizard">
                            <orion:LocalizableButton runat="server" ID="btnDoValidate" LocalizedText="CustomText" DisplayType="Primary"  Text="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_JP1_4 %>" OnClientClick="SetValidateAndClose(true);" OnClick="imbtnSubmit_Click"/>
		                    <orion:LocalizableButton runat="server" ID="btnDontValidate" LocalizedText="CustomText" DisplayType="Secondary" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_JP1_5 %>" OnClientClick="SetValidateAndClose(false);" OnClick="imbtnSubmit_Click"/>
                        </div>
                    </div>
                </div>
            </div>
            <asp:HiddenField runat="server" ID="hSkipValidate" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:LinkButton runat="server" Text="" ID="btnInvoker" OnClick="btnInvoker_Click" />

    <script type="text/javascript">
    var previousValue;
    var originalMessage;
    var originalMessageShort = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_Confirm_NodeSubtypeChange) %>";
    var btnTextConfirm = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_ET0_01) %>";
    var btnTextConfirmShort = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_Btn_Change_NodeSubtype) %>";
    var objectIds;

    $(document).ready(function () {
        previousValue = $(".PollingMethodSelectorTable input[type='radio']:checked").val();
        originalMessage = $("#warningMessage").html();
        objectIds = $.parseJSON($("div#<%= dependencyObjectIds.ClientID %>").text());
    });

    function ChangeMethodClose() {
        if($('#uninstallAgent').is(':checked')){
            $('#<%= hUninstallAgent.ClientID %>').val(1);
        } else {
            $('#<%= hUninstallAgent.ClientID %>').val(0);
        }
        $('#ChangeMethod').dialog('close');
    }
  
    function SetValidateAndClose(validate) {
        if(!validate) {
            $('#<%= hSkipValidate.ClientID %>').val(1);
        }
        $('#ConfirmValidation').dialog('close');
    }
  
    function pageLoad(sender, args){
        $("#<%=imbtnSubmit.ClientID%>").click(function () {
            var currentValue = $(".PollingMethodSelectorTable input[type='radio']:checked").val();
            var pollingMethodEnabled = !$(".PollingMethodSelectorTable input[type='radio']:checked").prop('disabled');
            var nodesCount = <%= NodesCount %>;

            var useShortMessage =
                    // nothing to loose from these types
                    (previousValue == 'rbICMP') || (previousValue == 'rbExternal')
                    // WMI compatible, volumes and pollers will be kept
                    || (previousValue == 'rbWMI' && currentValue == 'rbAgent')
                    || (previousValue == 'rbAgent' && currentValue == 'rbWMI');

            if (previousValue != currentValue) {
                var previousStr = previousValue.substring(2);
                var currentStr = currentValue.substring(2);

                var msgTemplate = useShortMessage ? originalMessageShort : originalMessage;

                $("#warningMessage").replaceWith(String.format(msgTemplate, previousStr, currentStr));
                if(previousValue == 'rbAgent'){
                    $('#changeMethodAgentUninstall').css({ 'display': 'block' });
                }else{
                    $('#changeMethodAgentUninstall').css({ 'display': 'none' });
                }
                $('#uninstallAgent').prop('checked', false);
                if(currentValue == 'rbAgent' && $('#<%= hAgentInstalled.ClientID %>').val() == 0){
                    $('#changeMethodAgentInstall').css({ 'display': 'block' });
                }else{
                    $('#changeMethodAgentInstall').css({ 'display': 'none' });
                }

                if (useShortMessage) {
                    $("#<%=btnWarnConfirm.ClientID%>").find(".sw-btn-t").text(btnTextConfirmShort);
                }else{
                    $("#<%=btnWarnConfirm.ClientID%>").find(".sw-btn-t").text(btnTextConfirm);
                }

                $("#ChangeMethod").dialog({
                    width: 600, modal: true,
                    open: function () { $("#<%=btnWarnCancel.ClientID%>").focus(); }
                });
                return false;
            }
            if(nodesCount > 5 && pollingMethodEnabled && (currentValue == 'rbSNMP' || currentValue == 'rbWMI')) { 
                $("#ConfirmValidation").dialog({
                        width: 400, modal: true,
                        open: function () { $("#<%=btnDoValidate.ClientID%>").focus(); }
                });
                return false;
            } 
            return true;
        });

        var objectIds = $("div#<%= dependencyObjectIds.ClientID %>").text();

        SW.Core.Loader.Control('#dependenciesAsyncResource',
               { Control: '/Orion/Nodes/Controls/DependenciesControl.ascx' },
               { 'config': { 'ObjectPrefix': 'N', 'ObjectIdsInJson' : objectIds } },
               'replace',
               function() { $.error('Error while loading DependenciesControl resource.'); },
               null
               );

        $("#<%= FakeTarget.ClientID %>").click(function() {
            $find("<%= ListOfNodesValidationModalPopupExtender.ClientID %>").hide();
        });
    }

    </script>

</asp:Content>
