using SolarWinds.InformationService.Contract2;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Agent;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common.i18n;
using SolarWinds.Orion.Core.Common.Indications;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Events;
using SolarWinds.Orion.Web.NpmAdapters;
using SolarWinds.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Extensions;
using SolarWinds.Orion.Web.UpdateNode;
using Node = SolarWinds.Orion.Core.Common.Models.Node;
using NodeSubType = SolarWinds.Orion.Core.Common.Models.NodeSubType;
using SNMPVersion = SolarWinds.Orion.Core.Common.Models.SNMPVersion;
using WebAgentManager = SolarWinds.Orion.Web.Agent.AgentManager;

public partial class Orion_Nodes_NodeProperties : System.Web.UI.Page
{
    private static readonly Log log = new Log();
    private static string defaultNodeManagementPageUrl = "~/Orion/Nodes/Default.aspx";
    private const int MaxParallelNodeUpdatesFallback = 5;
    private const string MaxParallelNodesUpdatesSettingName = "Web-MaxParallelNodesUpdates";
    private string pageGuid = string.Empty;
    private static readonly String ViewNodeSettingValue = "ViewId";
    UpdateNodePluginProvider updateNodePluginProvider = new UpdateNodePluginProvider();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    public bool SNMPNodes
    {
        get { return (bool)this.ViewState["SNMPNodes"]; }
        set { this.ViewState["SNMPNodes"] = value; }
    }

    public bool ICMPNodes
    {
        get { return (bool)this.ViewState["ICMPNodes"]; }
        set { this.ViewState["ICMPNodes"] = value; }
    }

    public bool WMINodes
    {
        get { return (bool)this.ViewState["WMINodes"]; }
        set { this.ViewState["WMINodes"] = value; }
    }

    public bool AgentNodes
    {
        get { return (bool)this.ViewState["AgentNodes"]; }
        set { this.ViewState["AgentNodes"] = value; }
    }

    public string ActivePageKey
    {
        get
        {
            if (Request["GuidID"] != null)
            {
                return Request["GuidID"];
            }

            if (!string.IsNullOrEmpty(pageGuid))
            {
                return pageGuid;
            }

            return IsPostBack ? Request.Form[HiddenFieldGuid.UniqueID] : null;
        }
    }

    protected int NodesCount
    {
        get;
        private set;
    }

    protected bool AgentInstalled
    {
        get
        {
            var nodes = GetSelectedNodes(NodeWorkflowHelper.GetMultiSelectedNodeIds(pageGuid).Count == 0);
            if (nodes.Count != 1)
                return false;//we are not going to check all selected nodes, let's assume agent is not installed on all of the nodes

            return (PollingMethodSelector.Agent &&
                    PollingMethodSelector.AgentInfo.DeploymentInfo != null &&
                    PollingMethodSelector.AgentInfo.DeploymentInfo.StatusInfo.Status == AgentDeploymentStatus.Finished);
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
        if (!IsPostBack)
        {
            pageGuid = Request["GuidID"];
            HiddenFieldGuid.Value = pageGuid;
        }
        else
        {
            pageGuid = HiddenFieldGuid.Value;
        }
        base.OnLoad(e);
        NodesCount = NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(pageGuid).Count;
    }

    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    private IList<Node> GetNodesForPropertyPlugins(string guid)
    {
        var nodesForPropertyPlugins = new List<Node>();

        NodeWorkflowHelper.FillNetObjectIds("Nodes", guid);

        if (NodeWorkflowHelper.GetMultiSelectedNodeIds(guid).Count > 0
            || NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(guid).Count > 0)
        {
            var nodes = GetSelectedNodes(NodeWorkflowHelper.GetMultiSelectedNodeIds(guid).Count == 0);
            foreach (var node in nodes)
            {
                nodesForPropertyPlugins.Add(node);
            }
        }
        return nodesForPropertyPlugins;
    }

    #region private void FillControls()

	private IList<Node> FillControls(string guid)
	{
		NodeWorkflowHelper.FillNetObjectIds("Nodes", guid);
		var multiSelection = false;
		var intNodeIds = new List<int>();

		if (NodeWorkflowHelper.GetMultiSelectedNodeIds(guid).Count == 0
			&& NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(guid).Count == 0)
		{
			ReferrerRedirectorBase.Return(defaultNodeManagementPageUrl);
		}
		else if (NodeWorkflowHelper.GetMultiSelectedNodeIds(guid).Count == 0)
		{
			foreach (var nodeId in NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(guid))
			{
				int id;
				if (int.TryParse(NetObjectHelper.GetObjectID(nodeId.Substring(0, nodeId.LastIndexOf(":"))), out id))
					intNodeIds.Add(id);
			}
		}
		else
		{
			intNodeIds = NodeWorkflowHelper.GetMultiSelectedNodeIds(guid);
		}


		if (intNodeIds.Count > 1)
			multiSelection = true;

		dependencyObjectIds.InnerText = new JavaScriptSerializer().Serialize(intNodeIds);

		NodeNameIP.MultipleSelection = multiSelection;
		NodePolling.MultipleSelection = multiSelection;
		DynamicIPVersion.MultipleSelection = multiSelection;
		PollingMethodSelector.MultipleSelection = multiSelection;
        NodeCategory.MultipleSelection = multiSelection;
        StatusRollupSelector.MultipleSelection = multiSelection;

        var nodes = GetSelectedNodes(NodeWorkflowHelper.GetMultiSelectedNodeIds(guid).Count == 0);
		var nodesForPropertyPlugins = new List<Node>();

		this.SNMPNodes = false;
		this.ICMPNodes = false;
		this.WMINodes = false;
        this.AgentNodes = false;

		bool external = false;

		HashSet<int> pluginPollingMethodIds = new HashSet<int>();

		foreach (var node in nodes)
		{
			nodesForPropertyPlugins.Add(node);

			if (node.UnPluggable)
			{
				external = true;
			}
			else
			{
			    switch (node.NodeSubType)
			    {
			        case NodeSubType.ICMP:
					    int? pluginMethodId = GetNodePluginPollingMethodId(node);
						if (pluginMethodId.HasValue)
						{
							pluginPollingMethodIds.Add(pluginMethodId.Value);
						}
					    else
					    {
						    this.ICMPNodes = true;
					    }
                        break;
                    case NodeSubType.SNMP:
                        this.SNMPNodes = true;
                        break;
                    case NodeSubType.WMI:
                        this.WMINodes = true;
                        break;
                    case NodeSubType.Agent:
                        this.AgentNodes = true;
                        PollingMethodSelector.AgentInfo.IsLinux = !("Windows".Equals(node.Vendor, StringComparison.OrdinalIgnoreCase));
                        break;
                }
			}

			blNodes.Items.Add(new System.Web.UI.WebControls.ListItem { Text = node.Caption });
		}

        List<bool> nodeTypes = new List<bool>() { external, ICMPNodes, SNMPNodes, WMINodes, AgentNodes };
		nodeTypes.AddRange(pluginPollingMethodIds.Select(id => true));
		PollingMethodSelector.Visible = nodeTypes.Where(p => p).Count() == 1;
		PollingMethodSelector.Nodes = nodesForPropertyPlugins;

		var defaultResolution = SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("SWNetPerfMon-Settings-Default Preferred AddressFamily DHCP");

		if (!multiSelection)
		{
			SingleSelectionFillControls(
				nodes.FindByID(intNodeIds[0]),
				defaultResolution);
		}
		else
		{
			MultiSelectionFillControls(nodes, intNodeIds.ToArray(), defaultResolution);
		}
        foreach (var node in nodes)
        {
            ViewInfo vi = GetViewFromNodeSettings(node.ID);
            if (vi != null)
                ViewSelector.SelectedViewID = vi.ViewID;
            else
                ViewSelector.SelectedViewID = 0;
            break;
        }

		return nodesForPropertyPlugins;
	}

    private Nodes GetSelectedNodes(bool loadFromPostData)
    {
        var nodes = new Nodes();
        if (loadFromPostData)
        {
            foreach (var engineNodes in GetGroupedNodeIds())
            {
                Nodes nodesPack;
                using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, engineNodes.Key, true))
                {
                    nodesPack = proxy.GetNodesByIds(engineNodes.Value.ToArray());
                }

                foreach (var node in nodesPack)
                {
                    nodes.Add(node.ID, node);
                }
            }
        }
        else
        {
            using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
            {
                for (var i = 0; i < NodeWorkflowHelper.GetMultiSelectedNodeIds(pageGuid).Count; i++)
                {
                    var engineId = GetEngineIdForNodeId(proxy, NodeWorkflowHelper.GetMultiSelectedNodeIds(pageGuid)[i]);
                    var node = GetNodeById(NodeWorkflowHelper.GetMultiSelectedNodeIds(pageGuid)[i], engineId);

                    if (node != null)
                        nodes.Add(node.ID, node);
                }
            }
        }
        return nodes;
    }

    private void SingleSelectionFillControls(Node node, OrionSetting defaultResolution)
    {
        DynamicIPVersion.IPVersionSelected = node.IpAddressResolution;
        if (!node.NodeSettings.ContainsKey(Node.IPResolutionSettingName) && defaultResolution != null)
        {
            DynamicIPVersion.IPVersionSelected =
                (defaultResolution.SettingValue == 4) ?
                System.Net.Sockets.AddressFamily.InterNetwork : System.Net.Sockets.AddressFamily.InterNetworkV6;
        }

        DynamicIPVersion.Visible = node.DynamicIP;
        DynamicIPVersion.DNS = node.DNS;

        NodeNameIP.Name = node.Name;
        NodeNameIP.IP = node.IpAddress;
        NodeNameIP.DynamicIP = node.DynamicIP;
        NodeNameIP.NodeID = node.ID;

        PollingMethodSelector.SetEngineId(node.EngineID);
        PollingMethodSelector.SnmpInfo.SNMPPort = node.SNMPPort;
        PollingMethodSelector.SnmpInfo.Allow64bitCounters = node.Allow64BitCounters;
        PollingMethodSelector.SnmpInfo.SNMPVersion = node.SNMPVersion;

        PollingMethodSelector.AgentInfo.SnmpInfo.SNMPPort = node.SNMPPort;
        PollingMethodSelector.AgentInfo.SnmpInfo.Allow64bitCounters = false;
        PollingMethodSelector.AgentInfo.SnmpInfo.SNMPVersion = node.SNMPVersion;
        PollingMethodSelector.AgentInfo.UseSnmp = (node.SNMPVersion != SNMPVersion.None);

        PollingMethodSelector.SnmpInfo.CommunityString = node.ReadOnlyCredentials.CommunityString;
        PollingMethodSelector.SnmpInfo.RWCommunityString = node.ReadWriteCredentials.CommunityString;

        PollingMethodSelector.AgentInfo.SnmpInfo.CommunityString = node.ReadOnlyCredentials.CommunityString;
        PollingMethodSelector.AgentInfo.SnmpInfo.RWCommunityString = node.ReadWriteCredentials.CommunityString;

        if (node.SNMPVersion == SNMPVersion.SNMP3)
        {
            PollingMethodSelector.SnmpInfo.SetControlFromSNMP3Node(node);
            PollingMethodSelector.AgentInfo.SnmpInfo.SetControlFromSNMP3Node(node);
        }

        PollingMethodSelector.LoadStateForPluginMethods(node);

        NodePolling.NodeStatusPolling = Convert.ToInt32(node.PollInterval.TotalSeconds);

        NodePolling.CollectStatisticsEvery = Convert.ToInt32(node.StatCollection.TotalMinutes);
        NodeNameIP.MultipleSelection_Selected = true;
        NodePolling.MultipleSelection_Selected = true;
        NodePolling.EngineID = node.EngineID;
        NodePolling.ObjectEngineIDs = new[] { node.EngineID };

        NodeCategory.Category = node.Category;
        NodeCategory.CustomCategory = node.CustomCategory;

        StatusRollupSelector.RollupMode = GetNodeStatusRollupMode(node);
        StatusRollupSelector.MultipleSelectionSelected = true;
    }

    private void MultiSelectionFillControls(Nodes nodes, int[] nodeIds, OrionSetting defaultResolution)
    {
        NodeNameIP.MultipleSelection = false;
        NodeNameIP.MultipleSelection_Selected = false;
        NodeNameIP.SetOnlyUnPluggableState();

        bool mSNMPVersion = true;
        bool mCommunityString = true;
        bool mRWCommunityString = true;
        bool mSNMPPort = true;

        bool mSNMPv3AuthType = true;
        bool mSNMPv3PrivacyType = true;
        bool mSnmpV3Context = true;
        bool mSNMPv3UserName = true;
        bool mSNMPv3AuthPassword = true;
        bool mSNMPv3PrivacyPassword = true;

        bool mRWSNMPv3AuthType = true;
        bool mRWSNMPv3PrivacyType = true;
        bool mRWSnmpV3Context = true;
        bool mRWSNMPv3UserName = true;
        bool mRWSNMPv3AuthPassword = true;
        bool mRWSNMPv3PrivacyPassword = true;

        bool mNodeStatusPolling = true;
        bool mCollectStatEvery = true;
        bool mEngineIDEvery = true;

        bool mNodeAllow64BitCounters = true;

        bool mSNMPnode = true;

        bool variousCategory = false;
        bool variousCustomCategory = false;

        var node = nodes.FindByID(nodeIds[0]);

        DynamicIPVersion.Visible = false;
        foreach (var n in nodes)
        {
            //if ((n.NodeSubType != NodeSubType.ICMP) || (n.UnPluggable != NodeNameIP.UnPluggable))
            //{
            //    NodeNameIP.EnableUnPluggable = false;
            //    NodeNameIP.UnPluggable = false;
            //}

            // detect setting differences
            variousCategory |= n.Category != node.Category;
            variousCustomCategory |= n.CustomCategory != node.CustomCategory;

            if (n.DynamicIP)
            {
                DynamicIPVersion.Visible = true;
                DynamicIPVersion.IPVersionSelected = System.Net.Sockets.AddressFamily.InterNetwork;
            }
        }

        if (DynamicIPVersion.Visible && defaultResolution != null)
            DynamicIPVersion.IPVersionSelected =
                (defaultResolution.SettingValue == 4) ? System.Net.Sockets.AddressFamily.InterNetwork : System.Net.Sockets.AddressFamily.InterNetworkV6;

        if (node.NodeSubType != NodeSubType.SNMP)
            mSNMPnode = false;

        for (int i = 1; i < nodeIds.Length; i++)
        {
            #region Update selection flags by node.

            if (mSNMPnode && nodes.FindByID(nodeIds[i]).NodeSubType != NodeSubType.SNMP)
            {
                mSNMPnode = false;
            }

            if (node.SNMPVersion != nodes.FindByID(nodeIds[i]).SNMPVersion)
            {
                mSNMPVersion = false;
            }

            if (node.ReadOnlyCredentials.CommunityString != nodes.FindByID(nodeIds[i]).ReadOnlyCredentials.CommunityString)
            {
                mCommunityString = false;
            }

            if (node.ReadWriteCredentials.CommunityString != nodes.FindByID(nodeIds[i]).ReadWriteCredentials.CommunityString)
            {
                mRWCommunityString = false;
            }

            if (node.ReadOnlyCredentials.SNMPv3AuthType != nodes.FindByID(nodeIds[i]).ReadOnlyCredentials.SNMPv3AuthType)
            {
                mSNMPv3AuthType = false;
            }

            if (node.ReadOnlyCredentials.SNMPv3PrivacyType != nodes.FindByID(nodeIds[i]).ReadOnlyCredentials.SNMPv3PrivacyType)
            {
                mSNMPv3PrivacyType = false;
            }

            if (node.ReadOnlyCredentials.SnmpV3Context != nodes.FindByID(nodeIds[i]).ReadOnlyCredentials.SnmpV3Context)
            {
                mSnmpV3Context = false;
            }

            if (node.ReadOnlyCredentials.SNMPv3UserName != nodes.FindByID(nodeIds[i]).ReadOnlyCredentials.SNMPv3UserName)
            {
                mSNMPv3UserName = false;
            }

            if (node.ReadOnlyCredentials.SNMPv3AuthPassword != nodes.FindByID(nodeIds[i]).ReadOnlyCredentials.SNMPv3AuthPassword)
            {
                mSNMPv3AuthPassword = false;
            }

            if (node.ReadOnlyCredentials.SNMPv3PrivacyPassword != nodes.FindByID(nodeIds[i]).ReadOnlyCredentials.SNMPv3PrivacyPassword)
            {
                mSNMPv3PrivacyPassword = false;
            }

            if (node.ReadWriteCredentials.SNMPv3AuthType != nodes.FindByID(nodeIds[i]).ReadWriteCredentials.SNMPv3AuthType)
            {
                mRWSNMPv3AuthType = false;
            }

            if (node.ReadWriteCredentials.SNMPv3PrivacyType != nodes.FindByID(nodeIds[i]).ReadWriteCredentials.SNMPv3PrivacyType)
            {
                mRWSNMPv3PrivacyType = false;
            }

            if (node.ReadWriteCredentials.SnmpV3Context != nodes.FindByID(nodeIds[i]).ReadWriteCredentials.SnmpV3Context)
            {
                mRWSnmpV3Context = false;
            }

            if (node.ReadWriteCredentials.SNMPv3UserName != nodes.FindByID(nodeIds[i]).ReadWriteCredentials.SNMPv3UserName)
            {
                mRWSNMPv3UserName = false;
            }

            if (node.ReadWriteCredentials.SNMPv3AuthPassword != nodes.FindByID(nodeIds[i]).ReadWriteCredentials.SNMPv3AuthPassword)
            {
                mRWSNMPv3AuthPassword = false;
            }

            if (node.ReadWriteCredentials.SNMPv3PrivacyPassword != nodes.FindByID(nodeIds[i]).ReadWriteCredentials.SNMPv3PrivacyPassword)
            {
                mRWSNMPv3PrivacyPassword = false;
            }

            if (node.SNMPPort != nodes.FindByID(nodeIds[i]).SNMPPort)
            {
                mSNMPPort = false;
            }

            if (node.PollInterval != nodes.FindByID(nodeIds[i]).PollInterval)
            {
                mNodeStatusPolling = false;
            }

            if (node.StatCollection != nodes.FindByID(nodeIds[i]).StatCollection)
            {
                mCollectStatEvery = false;
            }

            if (node.EngineID != nodes.FindByID(nodeIds[i]).EngineID)
            {
                mEngineIDEvery = false;
            }

            if (node.Allow64BitCounters != nodes.FindByID(nodeIds[i]).Allow64BitCounters)
            {
                mNodeAllow64BitCounters = false;
            }

            #endregion
        }

        #region Set up SNMP control with gained flags.

        if (mSNMPPort)
        {
            PollingMethodSelector.SnmpInfo.SNMPPort = node.SNMPPort;
            PollingMethodSelector.AgentInfo.SnmpInfo.SNMPPort = node.SNMPPort;
        }

        if (mCommunityString)
        {
            PollingMethodSelector.SnmpInfo.CommunityString = node.ReadOnlyCredentials.CommunityString;
            PollingMethodSelector.AgentInfo.SnmpInfo.CommunityString = node.ReadOnlyCredentials.CommunityString;
        }

        if (mRWCommunityString)
        {
            PollingMethodSelector.SnmpInfo.RWCommunityString = node.ReadWriteCredentials.CommunityString;
            PollingMethodSelector.AgentInfo.SnmpInfo.RWCommunityString = node.ReadWriteCredentials.CommunityString;
        }

        if (mSNMPVersion)
        {
            PollingMethodSelector.SnmpInfo.SNMPVersion = node.SNMPVersion;
            PollingMethodSelector.AgentInfo.SnmpInfo.SNMPVersion = node.SNMPVersion;
            PollingMethodSelector.AgentInfo.UseSnmp = (node.SNMPVersion != SNMPVersion.None);
        }

        if (mSNMPv3AuthType)
        {
            PollingMethodSelector.SnmpInfo.SNMPv3AuthType = node.ReadOnlyCredentials.SNMPv3AuthType;
            PollingMethodSelector.AgentInfo.SnmpInfo.SNMPv3AuthType = node.ReadOnlyCredentials.SNMPv3AuthType;
        }

        if (mSNMPv3PrivacyType)
        {
            PollingMethodSelector.SnmpInfo.SNMPv3PrivacyType = node.ReadOnlyCredentials.SNMPv3PrivacyType;
            PollingMethodSelector.AgentInfo.SnmpInfo.SNMPv3PrivacyType = node.ReadOnlyCredentials.SNMPv3PrivacyType;
        }

        if (mSNMPv3UserName)
        {
            PollingMethodSelector.SnmpInfo.SNMPv3Username = node.ReadOnlyCredentials.SNMPv3UserName;
            PollingMethodSelector.AgentInfo.SnmpInfo.SNMPv3Username = node.ReadOnlyCredentials.SNMPv3UserName;
        }

        if (mSnmpV3Context)
        {
            PollingMethodSelector.SnmpInfo.SNMPv3Context = node.ReadOnlyCredentials.SnmpV3Context;
            PollingMethodSelector.AgentInfo.SnmpInfo.SNMPv3Context = node.ReadOnlyCredentials.SnmpV3Context;
        }

        if (mSNMPv3AuthPassword)
        {
            PollingMethodSelector.SnmpInfo.AuthPasswordKey = node.ReadOnlyCredentials.SNMPv3AuthPassword;
            PollingMethodSelector.AgentInfo.SnmpInfo.AuthPasswordKey = node.ReadOnlyCredentials.SNMPv3AuthPassword;
        }

        if (mSNMPv3PrivacyPassword)
        {
            PollingMethodSelector.SnmpInfo.PrivacyPasswordKey = node.ReadOnlyCredentials.SNMPv3PrivacyPassword;
            PollingMethodSelector.AgentInfo.SnmpInfo.PrivacyPasswordKey = node.ReadOnlyCredentials.SNMPv3PrivacyPassword;
        }

        if (mRWSNMPv3AuthType)
        {
            PollingMethodSelector.SnmpInfo.RWSNMPv3AuthType = node.ReadWriteCredentials.SNMPv3AuthType;
            PollingMethodSelector.AgentInfo.SnmpInfo.RWSNMPv3AuthType = node.ReadWriteCredentials.SNMPv3AuthType;
        }

        if (mRWSNMPv3PrivacyType)
        {
            PollingMethodSelector.SnmpInfo.RWSNMPv3PrivacyType = node.ReadWriteCredentials.SNMPv3PrivacyType;
            PollingMethodSelector.AgentInfo.SnmpInfo.RWSNMPv3PrivacyType = node.ReadWriteCredentials.SNMPv3PrivacyType;
        }

        if (mRWSNMPv3UserName)
        {
            PollingMethodSelector.SnmpInfo.RWSNMPv3Username = node.ReadWriteCredentials.SNMPv3UserName;
            PollingMethodSelector.AgentInfo.SnmpInfo.RWSNMPv3Username = node.ReadWriteCredentials.SNMPv3UserName;
        }

        if (mRWSnmpV3Context)
        {
            PollingMethodSelector.SnmpInfo.RWSNMPv3Context = node.ReadWriteCredentials.SnmpV3Context;
            PollingMethodSelector.AgentInfo.SnmpInfo.RWSNMPv3Context = node.ReadWriteCredentials.SnmpV3Context;
        }

        if (mRWSNMPv3AuthPassword)
        {
            PollingMethodSelector.SnmpInfo.RWAuthPasswordKey = node.ReadWriteCredentials.SNMPv3AuthPassword;
            PollingMethodSelector.AgentInfo.SnmpInfo.RWAuthPasswordKey = node.ReadWriteCredentials.SNMPv3AuthPassword;
        }

        if (mRWSNMPv3PrivacyPassword)
        {
            PollingMethodSelector.SnmpInfo.RWPrivacyPasswordKey = node.ReadWriteCredentials.SNMPv3PrivacyPassword;
            PollingMethodSelector.AgentInfo.SnmpInfo.RWPrivacyPasswordKey = node.ReadWriteCredentials.SNMPv3PrivacyPassword;
        }

        if (mNodeAllow64BitCounters)
        {
            PollingMethodSelector.SnmpInfo.Allow64bitCounters = node.Allow64BitCounters;
            PollingMethodSelector.AgentInfo.SnmpInfo.Allow64bitCounters = node.Allow64BitCounters;
        }
        else
        {
            PollingMethodSelector.SnmpInfo.Allow64bitCounters = false;
            PollingMethodSelector.AgentInfo.SnmpInfo.Allow64bitCounters = false;
        }

        #endregion

        PollingMethodSelector.SnmpInfo.MultipleSelection = true;
        PollingMethodSelector.AgentInfo.SnmpInfo.MultipleSelection = true;
        PollingMethodSelector.WmiInfo.MultipleSelection = true;

        if (mNodeStatusPolling)
        {
            NodePolling.NodeStatusPolling = Convert.ToInt32(node.PollInterval.TotalSeconds);
        }

        if (mCollectStatEvery)
        {
            NodePolling.CollectStatisticsEvery = Convert.ToInt32(node.StatCollection.TotalMinutes);
        }

        if (mEngineIDEvery)
        {
            NodePolling.EngineID = node.EngineID;
        }

        NodePolling.ObjectEngineIDs = nodes.Select(x => x.EngineID).Distinct().ToArray();

        NodePolling.MultipleSelection_Selected = false;
        DynamicIPVersion.MultipleSelectionSelected = false;


        NodeCategory.Various = variousCategory || variousCustomCategory;

        NodeCategory.Category = variousCategory ? null : node.Category;
        NodeCategory.CustomCategory = variousCustomCategory ? null : node.CustomCategory;

        var rollupModes = nodes.Select(GetNodeStatusRollupMode).Distinct().ToArray();
        StatusRollupSelector.RollupMode = (rollupModes.Length == 1) ? rollupModes.First() : EvaluationMethod.Mixed;
        StatusRollupSelector.MultipleSelectionSelected = false;
    }

    #endregion

    private void DynamicIPCheckedChanged(bool val)
    {
        DynamicIPVersion.Visible = val;

        UpdatePageLayoutForPluginPollingMethod();
    }

    private void IPAddressChanged(object sender, EventArgs e)
    {
        //Is allowed only when single node selected
        var nodes = GetNodesForPropertyPlugins(pageGuid);

        if (nodes.Count == 1)
        {
            NodePropertyPluginManager.InitPlugins(nodes, NodeNameIP.IP);
        }
    }

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        // Node property plugin management
        if (!IsPostBack)
        {
            NodePropertyPluginManager.SetActivePage(ActivePageKey);
            NodePropertyPluginManager.Init(NodePropertyPluginExecutionMode.EditProperies);
            NodeWorkflowHelper.AgentCredential = null;
        }      
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        NodePropertyPluginManager.SetActivePage(ActivePageKey);

        // loads all plugin controls visble or not
        pluginDiv.Controls.Clear();
        var controls = NodePropertyPluginManager.LoadControls(this.LoadControl);
        foreach (var control in controls)
        {
            pluginDiv.Controls.Add(control);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
        NodeNameIP.CheckedChanged += new Orion_Nodes_Controls_NodeNameIP.CheckedChangedHandler(DynamicIPCheckedChanged);
        NodeNameIP.IPAddressChanged += new EventHandler(IPAddressChanged);

        PollingMethodSelector.SnmpInfo.Ip = NodeNameIP.IP;
        PollingMethodSelector.AgentInfo.SnmpInfo.Ip = NodeNameIP.IP;
        PollingMethodSelector.WmiInfo.Ip = NodeNameIP.IP;
        if (!IsPostBack)
        {
            IList<Node> nodes = FillControls(pageGuid);
            NodePropertyPluginManager.InitPlugins(nodes, NodeNameIP.IP);

            this.ClientScript.RegisterStartupScript(typeof(Orion_Nodes_NodeProperties), "cs_autocomplete",
                                                    @"  <script type=""text/javascript"">
    //<![CDATA[
		Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function() {
			$('.CommunityStringTextBox').unautocomplete();
            $('.CommunityStringTextBox').autocomplete([" +
                                                    this.GetReadOnlyCommunityStrings() +
                                                    @"], {max : 1000, minChars : 0});
            if(typeof(CommunityStringTextBoxChangedEvent) != 'undefined') {
                $('.CommunityStringTextBox').result(CommunityStringTextBoxChangedEvent);
            }
        });
    //]]>
    </script>
");
        }
        else
        {
            var nodes = GetSelectedNodes(NodeWorkflowHelper.GetMultiSelectedNodeIds(pageGuid).Count == 0);
            PollingMethodSelector.Nodes = new List<Node>(nodes);
            
            NodePropertyPluginManager.InitPlugins(null, NodeNameIP.IP);
        }
    }

    protected override bool OnBubbleEvent(object source, EventArgs args)
    {
        CommandEventArgs command = args as CommandEventArgs;

        if (command != null)
        {
            switch (command.CommandName)
            {
                case SNMPDetailsChangeEventArgs.CommandName :
                    if (IsPostBack)
                    {
                        IList<Node> nodes = GetNodesForPropertyPlugins(pageGuid);
                        if (nodes.Any())
                        {
                            SNMPDetailsChangeEventArgs snmpCommand = (SNMPDetailsChangeEventArgs)command;

                            //Distribute changes into all nodes
                            foreach (Node node in nodes)
                            {
                                snmpCommand.SetupNodeSNMP(node);
                            }

                            NodePropertyPluginManager.InitPlugins(nodes, NodeNameIP.IP);
                        }
                    }
                    return true;
            }
        }

        return base.OnBubbleEvent(source, args);
    }

    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);

        // adjust visibility of plugin controls
        NodePropertyPluginManager.AdjustPluginVisibility();

        //indicate whether or not an Agent is installed
        hAgentInstalled.Value = AgentInstalled ? "1" : "0";

        LoadPluginPollingMethodState();
    }

    protected void imbtnCancel_Click(object sender, EventArgs e)
    {
        NodeWorkflowHelper.GetMultiSelectedNodeIds(pageGuid).Clear();
        NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(pageGuid).Clear();
        NodePropertyPluginManager.Reset();
        ReferrerRedirectorBase.Return(defaultNodeManagementPageUrl);
    }

    protected void imbtnSubmit_Click(object sender, EventArgs e)
    {
        Page.Validate("EditCpValue");

        if (!Page.IsValid || !ValidateNode())
        {
            return;
        }

        if (PollingMethodSelector.NodeSubType != NodeSubType.Agent)
        {
            PollingMethodSelector.SnmpInfo.SaveSNMPv3CredentialsToSession();
        }
        else
        {
            PollingMethodSelector.AgentInfo.SnmpInfo.SaveSNMPv3CredentialsToSession();
        }

        if (NodeWorkflowHelper.GetMultiSelectedNodeIds(pageGuid).Count == 1 ||
            NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(pageGuid).Count == 1)
        {
            int engineId;
            Node node;
            if (NodeWorkflowHelper.GetMultiSelectedNodeIds(pageGuid).Count == 1)
            {
                using (ICoreBusinessLayer proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
                {
                    engineId = GetEngineIdForNodeId(proxy, NodeWorkflowHelper.GetMultiSelectedNodeIds(pageGuid)[0]);
                    node = GetNodeById(NodeWorkflowHelper.GetMultiSelectedNodeIds(pageGuid)[0], engineId);
                }
            }
            else
            {
                var id = GetGroupedNodeIds().First();
                engineId = id.Key;
                node = GetNodeById(id.Value.First(), engineId);
            }

            //FB 17210
            //create a proxy for the correct polling engine to test snmp connection via that polling engine
            using (ICoreBusinessLayer businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, node.EngineID, true))
            {
                if (PollingMethodSelector.SNMP)
                {
                    PerformSNMPValidation(businessProxy);
                }
                else if (PollingMethodSelector.Windows)
                {
                    PerformWMIValidation(businessProxy);
                }
                else if (PollingMethodSelector.Agent)
                {
                    if (PollingMethodSelector.AgentInfo.DeploymentInfo == null)//agent not deployed, validate settings
                    {
                        PerformAgentValidation();
                    }
                    else if(PollingMethodSelector.AgentInfo.DeploymentInfo.StatusInfo.Status == AgentDeploymentStatus.Finished ||
                            PollingMethodSelector.AgentInfo.DeploymentInfo.StatusInfo.Status == AgentDeploymentStatus.PluginsRequired ||
                            PollingMethodSelector.AgentInfo.DeploymentInfo.StatusInfo.Status == AgentDeploymentStatus.PendingPluginInstallation ||
                            PollingMethodSelector.AgentInfo.DeploymentInfo.StatusInfo.Status == AgentDeploymentStatus.PendingReboot)
                    {
                        Submit(true);
                    }
                    else
                    {
                        //error or deployment in progress. Do nothing, warning should be already displayed
                    }
                }
                else if (PollingMethodSelector.SelectedPluginMethod != null)
                {
                    if (ValidatePluginPollingMethod(node))
                    {
                        Submit(true);
                    }
                }
                else
                {
                    Submit(true);
                }
            }
        }
        else
        {
            Submit(true);
        }
    }

    private void OnPollingModeChanged()
    {
        var nodes = GetNodesForPropertyPlugins(pageGuid);       

        foreach (var node in nodes)
        {
            var originalSubType = node.NodeSubType;

            NodeSubType nodeType = NodeSubType.None;

            // update data if needed
            if (!this.PollingMethodSelector.UnPluggable && PollingMethodSelector.Visible)
            {
                if (!PollingMethodSelector.MultipleSelection || PollingMethodSelector.MultipleSelectionSelected)
                {
                    if (PollingMethodSelector.ICMP)
                        nodeType = NodeSubType.ICMP;
                    else if (PollingMethodSelector.SNMP)
                        nodeType = NodeSubType.SNMP;
                    else if (PollingMethodSelector.Windows)
                        nodeType = NodeSubType.WMI;
                    else if (PollingMethodSelector.Agent)
                        nodeType = NodeSubType.Agent;

                    switch (nodeType)
                    {
                        case NodeSubType.SNMP:
                            node.NodeSubType = NodeSubType.SNMP;
                            node.SNMPVersion = this.PollingMethodSelector.SnmpInfo.SNMPVersion;
                            node.ObjectSubType = Enum.GetName(typeof(NodeSubType), nodeType);
                            break;
                        case NodeSubType.WMI:
                            node.NodeSubType = NodeSubType.WMI;
                            node.SNMPVersion = SNMPVersion.None;
                            node.ObjectSubType = Enum.GetName(typeof(NodeSubType), nodeType);
                            break;
                        case NodeSubType.Agent:
                            node.NodeSubType = NodeSubType.Agent;
                            node.SNMPVersion = (PollingMethodSelector.AgentInfo.UseSnmp ? PollingMethodSelector.AgentInfo.SnmpInfo.SNMPVersion : SNMPVersion.None);
                            node.ObjectSubType = Enum.GetName(typeof(NodeSubType), nodeType);
                            break;
                        default:
                            node.NodeSubType = NodeSubType.ICMP;
                            node.SNMPVersion = SNMPVersion.None;
                            // reset node properties only in case real change occurrs.                            
                            node.ObjectSubType = Enum.GetName(typeof(NodeSubType), nodeType);
                            break;
                    }
                }
            }
        }

        NodePropertyPluginManager.InitPlugins(nodes);

        UpdatePageLayoutForPluginPollingMethod();
    }

    private void UpdatePageLayoutForPluginPollingMethod()
    {
        var selectedPluginMethod = PollingMethodSelector.SelectedPluginMethod;
        if (selectedPluginMethod != null)
        {
            var pageLayout = selectedPluginMethod.GetPageLayout();

            if (pageLayout != null)
            {
                NodeNameIP.SetEnabledState(pageLayout.HostnameEnabled);
                DynamicIPVersion.SetDNSEnabledState(pageLayout.HostnameEnabled);
            }
        }
    }

    /// <summary>
    /// Performs validation of plugin polling method (if selected) for given node
    /// </summary>
    /// <returns>true if plugin method not selected or selected and valid. false if plugin method selected and doesn't pass validation</returns>
    private bool ValidatePluginPollingMethod(Node node)
    {
        if (IsPluginPollingMethodEnabled(node))
        {
            var result = PollingMethodSelector.SelectedPluginMethod.Validate(node, PollingMethodSelector.SelectedPluginSettingsControl);

            if (!result.IsValid)
            {
                string validationMessages = (result.ErrorMessages == null || !result.ErrorMessages.Any() || result.ErrorMessages.All(message => string.IsNullOrWhiteSpace(message))) ? null : string.Join("\\n", result.ErrorMessages);

                if (validationMessages != null)
                {
                    ScriptManager.RegisterStartupScript(this.Page, GetType(), "PluginValidation", ControlHelper.JsFormat("alert('{0}');", validationMessages), true);
                }

                return false;
            }
        }

        return true;
    }

	private bool ValidateMultiEditPluginPollingMethod(Node node)
	{
		if (IsPluginPollingMethodEnabled(node))
		{
			var result = PollingMethodSelector.SelectedPluginMethod.Validate(node, PollingMethodSelector.SelectedPluginSettingsControl);

			if (!result.IsValid)
			{
				if (result.ErrorMessages != null)
				{
					foreach (var errorMessage in result.ErrorMessages)
					{
						ListOfNodesValidation.AddListItem(node.Caption, errorMessage, false);
					}
				}

				return false;
			}
		}

		return true;
	}

    /// <summary>
    /// Loads state of plugin polling method if selected.
    /// </summary>
    private void LoadPluginPollingMethodState()
    {
        if (!IsPostBack && PollingMethodSelector.Visible && (PollingMethodSelector.SelectedPluginMethod != null) && (PollingMethodSelector.Nodes != null) && PollingMethodSelector.Nodes.Any())
        {
			// in multi-edit mode we need to pass null instead of node, in single-edit mode we'll use actual node
			var node = PollingMethodSelector.MultipleSelection ? null : PollingMethodSelector.Nodes[0];

			PollingMethodSelector.SelectedPluginMethod.LoadState(node, PollingMethodSelector.SelectedPluginSettingsControl, null);
        }
    }

	/// <summary>
	/// Function returns true if there's polling selector visible with selected plugin method and
	/// it's either in single-edit mode or in multi-edit mode with polling method checked.
	/// </summary>
	/// <param name="node"></param>
	/// <returns></returns>
	private bool IsPluginPollingMethodEnabled(Node node)
	{
		return
			(node.NodeSubType == NodeSubType.ICMP) &&
			PollingMethodSelector.Visible &&
			(PollingMethodSelector.SelectedPluginMethod != null) &&
			((PollingMethodSelector.MultipleSelection && PollingMethodSelector.MultipleSelectionSelected) || !PollingMethodSelector.MultipleSelection);
	}

    /// <summary>
    /// Saves node if it uses plugin polling method
    /// </summary>
    /// <param name="node"></param>
    private void StorePluginPollingMethod(Node node)
    {
	    if (IsPluginPollingMethodEnabled(node))
	    {
			PollingMethodSelector.SelectedPluginMethod.Commit(node, PollingMethodSelector.SelectedPluginSettingsControl, null);
		}
    }

	/// <summary>
	/// This method checks whether given node is using plugin polling method and if so, returns its Id.
	/// </summary>
	/// <param name="node">Plugin ID or null if node uses hardcoded method</param>
	/// <returns></returns>
	private int? GetNodePluginPollingMethodId(Node node)
	{
		foreach (var addNodePollingMethodPluginConfig in OrionModuleManager.GetAddNodeMethodPlugins())
		{
			if (addNodePollingMethodPluginConfig.Value.Plugin.IsUsedByNode(node))
			{
				return addNodePollingMethodPluginConfig.Key;
			}
		}

		return null;
	}

    private void Submit(bool redirectAfterSave)
    {
        var nodes = GetSelectedNodes(NodeWorkflowHelper.GetMultiSelectedNodeIds(pageGuid).Count == 0);

        bool singleNodeSelected = nodes.Count == 1;

        // Run node update procedure in parallel to prevent UI timeouts
        // There is no CPU intensive work during node submission
        // IO operations for credential validation and BL/SWIS calls to persist update can slow down node changes submission
        int maxParallelNodeUpdates = GetMaxParallelNodeUpdates(singleNodeSelected);

        HttpContext currentHttpContext = HttpContext.Current;
        bool thereAreValidationFails = nodes.AsParallel()
            .WithDegreeOfParallelism(maxParallelNodeUpdates)
            .Select(n =>
            {
                // code deep inside TrySubmitSingleNode depends on HttpContext.Current, which is null if new parallel threads are created, let's pass correct HttpContext to them
                using (currentHttpContext.CaptureContext())
                {
                    return TrySubmitSingleNode(n, singleNodeSelected);
                }
            })
            .Aggregate(false, (anyFailed, nodeFailed) => anyFailed || nodeFailed);

        bool pluginValidationFailures = NodePropertyPluginManager.Update() == false;

        if (!thereAreValidationFails)
        {
            if (redirectAfterSave)
            {
                imbtnCancel_Click(null, null);
            }
            
            NodeWorkflowHelper.GetMultiSelectedNodeIds(pageGuid).Clear();
            NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(pageGuid).Clear();
            NodePropertyPluginManager.Reset(); // we've done with manager, so clear state 
        }
        else
        {
            ListOfNodesValidation.Visible = true;
            ListOfNodesValidationModalPopupExtender.Show();
        }
    }

    private void UpdateNodeByPluginMethod(Node node)
    {
        foreach (var plugin in updateNodePluginProvider.Plugins)
        {
            plugin.OnUpdate(node);
        }
    }

    private bool TrySubmitSingleNode(Node node, bool singleNodeSelected)
    {
            bool multipleNodesSelected = !singleNodeSelected;
            bool thereAreValidationFails = false;

            // handle updates of node category
            if (singleNodeSelected) // single edit
            {
                node.CustomCategory = NodeCategory.ResultCategory;
            }
            else // multiedit
            {
                if (NodeCategory.MultipleSelection_Checked) 
                {
                    node.CustomCategory = NodeCategory.ResultCategory;
                }
            }

            var originalSubType = node.NodeSubType;

            // Only set node details when setting one node
            if (singleNodeSelected)
            {
                UpdateNodeCommonData(node);
            }

            // update data if needed
            if (!this.PollingMethodSelector.UnPluggable && PollingMethodSelector.Visible)
            {
                NodeSubType nodeType = NodeSubType.None;

                if (!PollingMethodSelector.MultipleSelection || PollingMethodSelector.MultipleSelectionSelected)
                {
                    if (PollingMethodSelector.ICMP || (PollingMethodSelector.SelectedPluginMethod != null))     // plugin polling methods are fixed to ICMP subtype
                        nodeType = NodeSubType.ICMP;
                    else if (PollingMethodSelector.SNMP)
                        nodeType = NodeSubType.SNMP;
                    else if (PollingMethodSelector.Windows)
                        nodeType = NodeSubType.WMI;
                    else if (PollingMethodSelector.Agent)
                        nodeType = NodeSubType.Agent;

                    bool nodeSubTypeChange = node.NodeSubType != nodeType;

                    switch (nodeType)
                    {
                        case NodeSubType.SNMP:
                            node.NodeSubType = NodeSubType.SNMP;
                            UpdateNodeSNMPData(node, PollingMethodSelector.SnmpInfo, false);   //Sets node.SNMPVersion
                            node.ObjectSubType = Enum.GetName(typeof(NodeSubType), nodeType);
                            break;
                        case NodeSubType.WMI:
                            node.NodeSubType = NodeSubType.WMI;
                            node.SNMPVersion = SNMPVersion.None;
                            PollingMethodSelector.WmiInfo.Update(node);
                            node.ObjectSubType = Enum.GetName(typeof(NodeSubType), nodeType);
                            break;
                        case NodeSubType.Agent:
                            node.NodeSubType = NodeSubType.Agent;
                            PollingMethodSelector.AgentInfo.Update(node);
                            UpdateNodeSNMPData(node, PollingMethodSelector.AgentInfo.SnmpInfo, !this.PollingMethodSelector.AgentInfo.UseSnmp);
                            node.ObjectSubType = Enum.GetName(typeof(NodeSubType), nodeType);
                            break;
                        default:
                            node.NodeSubType = NodeSubType.ICMP;
                            node.SNMPVersion = SNMPVersion.None;
                            // reset node properties only in case real change occurrs.
                            if (nodeSubTypeChange)
                            {
                                UpdateNodeICMPData(node);
                            }
                            node.ObjectSubType = Enum.GetName(typeof(NodeSubType), nodeType);
                            break;
                    }
                }
            }

			if (PollingMethodSelector.Visible && 
				(!PollingMethodSelector.MultipleSelection || 
				PollingMethodSelector.MultipleSelectionSelected))
                node.UnPluggable = PollingMethodSelector.UnPluggable;

            bool pollingEngineChanged = false;
            if (NodePolling.MultipleSelection_Selected)
            {
                pollingEngineChanged = NodePolling.EngineID > 0 && node.EngineID != NodePolling.EngineID;
                UpdateNodePollingData(node);
            }

            if (node.DynamicIP && (singleNodeSelected || DynamicIPVersion.MultipleSelectionSelected))
                node.IpAddressResolution = DynamicIPVersion.IPVersionSelected;

            if (StatusRollupSelector.VisibleBySetting && (singleNodeSelected || StatusRollupSelector.MultipleSelectionSelected))
                UpdateNodeStatusRollupMode(node, StatusRollupSelector.RollupMode);

            using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
            {
                var nodeValidationFail = false;
                if (multipleNodesSelected && this.PollingMethodSelector.MultipleSelectionSelected)
                {
                    if (PollingMethodSelector.Visible)
                        node.UnPluggable = PollingMethodSelector.UnPluggable;

                    if (node.SNMPVersion != SNMPVersion.None)
                    {
                        var result = SkipValidation || NodeWorkflowHelper.ValidateSNMPNode(proxy, node).ValidationSucceeded;
                        if (result)
                        {
                            proxy.UpdateNode(node);
                            UpdateNodeByPluginMethod(node);

                        if (node.SNMPVersion == SNMPVersion.SNMP3)
                            {
                                PollingMethodSelector.UpdateSnmpV3Credentials(node);
                            }

                            ListOfNodesValidation.AddListItem(node.Caption, Resources.CoreWebContent.WEBCODE_AK0_129, true);
                        }
                        else
                        {
                            nodeValidationFail = true;
                            thereAreValidationFails = true;
                            ListOfNodesValidation.AddListItem(node.Caption,
                                                                Resources.CoreWebContent.WEBCODE_AK0_130,
                                                                false);
                        }
                    }
                    else if (node.NodeSubType == NodeSubType.WMI)
                    {
                        var result = SkipValidation || NodeWorkflowHelper.ValidateWMINode(proxy, node.IpAddress,
                                                                        PollingMethodSelector.WmiInfo.Login,
                                                                        PollingMethodSelector.WmiInfo.
                                                                            PlainPassword);
                        if (result)
                        {
                            PollingMethodSelector.WmiInfo.Update(node);
                            proxy.UpdateNode(node);
                            UpdateNodeByPluginMethod(node);
                        }
                        else
                        {
                            nodeValidationFail = true;
                            thereAreValidationFails = true;
                            ListOfNodesValidation.AddListItem(node.Caption,
                                                                Resources.CoreWebContent.WEBCODE_TM0_101,
                                                                false);
                        }
                    }
                    else if (node.NodeSubType == NodeSubType.Agent)
                    {
                        PollingMethodSelector.AgentInfo.DetectAgent(node);
                        //TODO RH do we need any validation?
                        //var result = SkipValidation || NodeWorkflowHelper.ValidateWMINode(proxy, node.IpAddress,
                        //                                                PollingMethodSelector.WmiInfo.Login,
                        //                                                PollingMethodSelector.WmiInfo.
                        //                                                    PlainPassword);
                        var result = true;//TODO RH temporary solution
                        if (result)
                        {
                            PollingMethodSelector.AgentInfo.Update(node);
                            proxy.UpdateNode(node);
                            UpdateNodeByPluginMethod(node);

                        new WebAgentManager().SyncPollingEngine(node.Id, node.EngineID);
                        }
                        else
                        {
                            nodeValidationFail = true;
                            thereAreValidationFails = true;
                            ListOfNodesValidation.AddListItem(node.Caption,
                                                                Resources.CoreWebContent.WEBCODE_TM0_101,
                                                                false);
                        }
                    }
                    else if (IsPluginPollingMethodEnabled(node))
	                {
		                if (ValidateMultiEditPluginPollingMethod(node))
		                {
			                StorePluginPollingMethod(node);
			                proxy.UpdateNode(node);
                            UpdateNodeByPluginMethod(node);
                        }
		                else
		                {
			                nodeValidationFail = true;
			                thereAreValidationFails = true;
		                }
	                }
	                else
	                {
						proxy.UpdateNode(node);
                        UpdateNodeByPluginMethod(node);
                    }
                }
                else
                {
                    StorePluginPollingMethod(node);
                    proxy.UpdateNode(node);
                    UpdateNodeByPluginMethod(node);

                if (node.SNMPVersion == SNMPVersion.SNMP3 && (!PollingMethodSelector.MultipleSelection || PollingMethodSelector.MultipleSelectionSelected) && PollingMethodSelector.Visible)
                    {
                        PollingMethodSelector.UpdateSnmpV3Credentials(node);
                        SnmpCredentialChangeNotification(node);
                    }
					    
                    if (node.NodeSubType == NodeSubType.Agent)
                    {
                        new WebAgentManager().SyncPollingEngine(node.Id, node.EngineID);
                    }
                }

                if (node.NodeSubType == NodeSubType.ICMP && node.UnPluggable)
                {
                    // Removes all pollers for ICMP external node
                    proxy.RemoveBasicPollersForNode(node.Id, NodeSubType.ICMP);
                }
                else if (node.NodeSubType != originalSubType)
                {
                    proxy.AddBasicPollersForNode(node.Id, node.NodeSubType);
                }

                if (!nodeValidationFail && originalSubType == NodeSubType.Agent && node.NodeSubType != originalSubType)
                {
                    if (hUninstallAgent.Value == "1")
                        new WebAgentManager().UninstallAgentByNodeId(node.Id);
                    else
                        proxy.ResetAgentNodeId(node.Id);
                }
                //Deploy agent to new Agent nodes
                if (!nodeValidationFail && node.NodeSubType == NodeSubType.Agent && originalSubType != NodeSubType.Agent)
                {
                    if (PollingMethodSelector.AgentInfo.DeploymentInfo == null)//agent not deployed, start deploying
                    {
                        var deploymentSettings = new AgentDeploymentSettings()
                        {
                            EngineId = node.EngineID,
                            IpAddress = node.IpAddress,
                            NodeId = node.Id,
                            Hostname = node.DNS,
                            Credentials = PollingMethodSelector.AgentInfo.GetEnteredCredentials(),
                            RequiredPlugins = new List<string>(proxy.GetRequiredAgentDiscoveryPlugins()),
                            InstallPackageId = PollingMethodSelector.AgentInfo.InstallPackageId
                        };
                        try
                        {
                            proxy.DeployAgent(deploymentSettings);
                        }
                        catch (Exception ex)
                        {
                            //no need for logging here, it's already logged by BusinessLayerExceptionHandler
                        }
                    }
                    else
                    {
                        switch (PollingMethodSelector.AgentInfo.DeploymentInfo.StatusInfo.Status)
                        {
                            case AgentDeploymentStatus.PluginsRequired:
                            case AgentDeploymentStatus.PendingPluginInstallation: //deploy missing plugins
                                proxy.DeployAgentDiscoveryPlugins(PollingMethodSelector.AgentInfo.DeploymentInfo.Agent.AgentId);
                                break;
                            default: // do nothing in all other situations. Agent is either deployed or deployment is in progres or agent is in error state, which is indicated in UI
                                break;
                        }
                        proxy.UpdateAgentNodeId(PollingMethodSelector.AgentInfo.DeploymentInfo.Agent.AgentId, node.Id);
                    }
                }

                UpdateViewForNode(node);
            }

            return thereAreValidationFails;
    }

    private int GetMaxParallelNodeUpdates(bool singleNodeSelected)
    {
        if (singleNodeSelected)
        {
            return 1;
        }
        var settingValue = (int)(SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting(MaxParallelNodesUpdatesSettingName)?.SettingValue ?? MaxParallelNodeUpdatesFallback);
        return Math.Max(settingValue, 1);
    }

    private bool ShouldApplySNMPSettings()
    {
        // apply SNMP when SNMP radio is checked and we edit one node or multiedit is checked for polling method selector
        return (!PollingMethodSelector.MultipleSelection || PollingMethodSelector.MultipleSelectionSelected) && PollingMethodSelector.SNMP;
    }

    private bool ShouldApplyICMPSettings()
    {
        // apply ICMP when ICMP radio is checked and we edit one node or multiedit is checked for polling method selector
        return (!PollingMethodSelector.MultipleSelection || PollingMethodSelector.MultipleSelectionSelected) &&
               PollingMethodSelector.ICMP;
    }

    private bool ShouldApplyWMISettings()
    {
        // apply WMI when WMI radio is checked and we edit one node or multiedit is checked for polling method selector
        return (!PollingMethodSelector.MultipleSelection || PollingMethodSelector.MultipleSelectionSelected) &&
               PollingMethodSelector.Windows;
    }

    private void UpdateNodeICMPData(Node node)
    {
        using (LocaleThreadState.EnsurePrimaryLocale())
        {
            node.MachineType = Resources.CoreWebContent.WEBCODE_UnknownMachineType;
            node.Vendor = Resources.CoreWebContent.WEBCODE_UnknownVendor;
        }
        node.ReadOnlyCredentials.CommunityString = string.Empty;
        node.ReadWriteCredentials.CommunityString = string.Empty;

        node.SysName = string.Empty;        
        node.VendorIcon = "Unknown.gif";
        node.IOSImage = string.Empty;
        node.IOSVersion = string.Empty;

        node.LastBoot = DateTime.MinValue;
        node.SystemUpTime = 0;        
        node.SysObjectID = string.Empty;

        node.Description = string.Empty;
        node.Location = string.Empty;
        node.Contact = string.Empty;

        node.SNMPV2Only = false;
        node.SNMPVersion = SNMPVersion.None;

        node.ReadOnlyCredentials.SNMPv3AuthType = SNMPv3AuthType.None;
        node.ReadOnlyCredentials.SNMPv3PrivacyType = SNMPv3PrivacyType.None;
        node.ReadOnlyCredentials.SnmpV3Context = String.Empty;
        node.ReadOnlyCredentials.SNMPv3UserName = String.Empty;
        node.ReadOnlyCredentials.SNMPv3AuthPassword = String.Empty;
        node.ReadOnlyCredentials.SNMPv3PrivacyPassword = String.Empty;
        node.ReadWriteCredentials.SNMPv3AuthType = SNMPv3AuthType.None;
        node.ReadWriteCredentials.SNMPv3PrivacyType = SNMPv3PrivacyType.None;
        node.ReadWriteCredentials.SnmpV3Context = String.Empty;
        node.ReadWriteCredentials.SNMPv3UserName = String.Empty;
        node.ReadWriteCredentials.SNMPv3AuthPassword = String.Empty;
        node.ReadWriteCredentials.SNMPv3PrivacyPassword = String.Empty;

        node.ReadOnlyCredentials.SNMPV3PrivKeyIsPwd = true;
        node.ReadOnlyCredentials.SNMPV3AuthKeyIsPwd = true;
        node.ReadWriteCredentials.SNMPV3PrivKeyIsPwd = true;
        node.ReadWriteCredentials.SNMPV3AuthKeyIsPwd = true;

        node.CPULoad = -2;
        node.TotalMemory = 0;
        node.MemoryUsed = -2;
        node.PercentMemoryUsed = -2;

        node.BufferNoMemThisHour =-2;               
        node.BufferNoMemToday =-2;                  
        node.BufferSmMissThisHour =-2;              
        node.BufferSmMissToday =-2;                 
        node.BufferMdMissThisHour =-2;              
        node.BufferMdMissToday =-2;                 
        node.BufferBgMissThisHour =-2;              
        node.BufferBgMissToday =-2;                 
        node.BufferLgMissThisHour =-2;               
        node.BufferLgMissToday =-2;                 
        node.BufferHgMissThisHour =-2;              
        node.BufferHgMissToday  =-2;

        // remove WMI credentials assignment
        node.NodeSettings.Remove(SolarWinds.Orion.Core.Common.DALs.NodeSettingsNames.WmiCredentialSettingName);
    }

    private void UpdateNodeSNMPData(Node node, Orion_Nodes_Controls_NodeSNMP snmpInfo, bool removeSnmpData)
    {

        node.SNMPPort = snmpInfo.SNMPPort;
        node.SNMPVersion = (removeSnmpData ? SNMPVersion.None : snmpInfo.SNMPVersion);
        node.Allow64BitCounters = snmpInfo.Allow64bitCounters;

        if (node.SNMPVersion != SNMPVersion.SNMP3)
        {
            node.ReadOnlyCredentials.CommunityString = snmpInfo.CommunityString;
            node.ReadWriteCredentials.CommunityString = snmpInfo.RWCommunityString;

            node.ReadOnlyCredentials.SNMPv3AuthType = SNMPv3AuthType.None;
            node.ReadOnlyCredentials.SNMPv3PrivacyType = SNMPv3PrivacyType.None;
            node.ReadOnlyCredentials.SnmpV3Context = String.Empty;
            node.ReadOnlyCredentials.SNMPv3UserName = String.Empty;
            node.ReadOnlyCredentials.SNMPv3AuthPassword = String.Empty;
            node.ReadOnlyCredentials.SNMPv3PrivacyPassword = String.Empty;
            node.ReadWriteCredentials.SNMPv3AuthType = SNMPv3AuthType.None;
            node.ReadWriteCredentials.SNMPv3PrivacyType = SNMPv3PrivacyType.None;
            node.ReadWriteCredentials.SnmpV3Context = String.Empty;
            node.ReadWriteCredentials.SNMPv3UserName = String.Empty;
            node.ReadWriteCredentials.SNMPv3AuthPassword = String.Empty;
            node.ReadWriteCredentials.SNMPv3PrivacyPassword = String.Empty;

            node.NodeSettings.Remove(SolarWinds.Orion.Core.Common.DALs.NodeSettingsNames.RoSnmpCredentialSettingName);
            node.NodeSettings.Remove(SolarWinds.Orion.Core.Common.DALs.NodeSettingsNames.RwSnmpCredentialSettingName);
        }
        else
        {
            node.ReadOnlyCredentials.CommunityString = String.Empty;
            node.ReadWriteCredentials.CommunityString = String.Empty;

            node.ReadOnlyCredentials.SNMPv3AuthType = snmpInfo.SNMPv3AuthType;
            node.ReadOnlyCredentials.SNMPv3PrivacyType = snmpInfo.SNMPv3PrivacyType;
            node.ReadOnlyCredentials.SnmpV3Context = snmpInfo.SNMPv3Context;
            node.ReadOnlyCredentials.SNMPv3UserName = snmpInfo.SNMPv3Username;
            node.ReadOnlyCredentials.SNMPv3AuthPassword = snmpInfo.AuthPasswordKey;
            node.ReadOnlyCredentials.SNMPv3PrivacyPassword = snmpInfo.PrivacyPasswordKey;
            node.ReadWriteCredentials.SNMPv3AuthType = snmpInfo.RWSNMPv3AuthType;
            node.ReadWriteCredentials.SNMPv3PrivacyType = snmpInfo.RWSNMPv3PrivacyType;
            node.ReadWriteCredentials.SnmpV3Context = snmpInfo.RWSNMPv3Context;
            node.ReadWriteCredentials.SNMPv3UserName = snmpInfo.RWSNMPv3Username;
            node.ReadWriteCredentials.SNMPv3AuthPassword = snmpInfo.RWAuthPasswordKey;
            node.ReadWriteCredentials.SNMPv3PrivacyPassword = snmpInfo.RWPrivacyPasswordKey;

            node.ReadOnlyCredentials.SNMPV3PrivKeyIsPwd = snmpInfo.PrivKeyIsPwd;
            node.ReadOnlyCredentials.SNMPV3AuthKeyIsPwd = snmpInfo.AuthKeyIsPwd;
            node.ReadWriteCredentials.SNMPV3PrivKeyIsPwd = snmpInfo.RWPrivKeyIsPwd;
            node.ReadWriteCredentials.SNMPV3AuthKeyIsPwd = snmpInfo.RWAuthKeyIsPwd;
        }

        // remove WMI credentials assignment
        node.NodeSettings.Remove(SolarWinds.Orion.Core.Common.DALs.NodeSettingsNames.WmiCredentialSettingName);
        node.WmiCredential = new WmiCredentials();
    }

    private void UpdateNodePollingData(Node node)
    {
        node.PollInterval = TimeSpan.FromSeconds((double)NodePolling.NodeStatusPolling);
        node.StatCollection = TimeSpan.FromMinutes((double)NodePolling.CollectStatisticsEvery);
        //FB83263: we have to avoid settinh -1 to EngineID 
        if(NodePolling.EngineID>0)
            node.EngineID = NodePolling.EngineID;
    }

    private void UpdateNodeCommonData(Node node)
    {
        node.Name = NodeNameIP.Name;
        node.Caption = node.Name;
        node.IpAddress = NodeNameIP.IP;
        node.DynamicIP = NodeNameIP.DynamicIP;
        if (node.DynamicIP)
        {
            node.DNS = this.DynamicIPVersion.DNS;
        }

        if (PollingMethodSelector.Visible)
            node.UnPluggable = PollingMethodSelector.UnPluggable;
    }

    private bool ValidSnmpValues
    {
        get
        {
            //Check SNMP Node type control, if visible.
            if (PollingMethodSelector.NodeSubType == NodeSubType.SNMP && 
                PollingMethodSelector.SnmpInfo.Visible)
            {
                if (PollingMethodSelector.SnmpInfo.SNMPVersion != SNMPVersion.SNMP3)
                {
                    //Validate that the community strings aren't missing for SNMPv1/2c.
                    if (string.IsNullOrEmpty(PollingMethodSelector.SnmpInfo.CommunityString.Trim()) &&
                        (string.IsNullOrEmpty(PollingMethodSelector.SnmpInfo.RWCommunityString.Trim())))
                    {
                        return false;
                    }
                }
            }

            //Check Agent Node type of control, if visible.
            if ((PollingMethodSelector.NodeSubType == NodeSubType.Agent) &&
                PollingMethodSelector.AgentInfo.SnmpInfo.Visible)
            {
                if (PollingMethodSelector.AgentInfo.SnmpInfo.SNMPVersion != SNMPVersion.SNMP3)
                {
                    //Validate that the community strings aren't missing for SNMPv1/2c.
                    if (string.IsNullOrEmpty(PollingMethodSelector.AgentInfo.SnmpInfo.CommunityString.Trim()) &&
                        string.IsNullOrEmpty(PollingMethodSelector.AgentInfo.SnmpInfo.RWCommunityString.Trim()))
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }

    private bool ValidateNode()
    {
        if (!ValidSnmpValues)
        {
            string script = string.Format("alert('{0}');", ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBCODE_AK0_128));
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "AlertSNMP", script, true);

            return false;
        }

        string msg = NodePropertyPluginManager.Validate();
        if (msg != null)
        {
            ClientScriptManager manager = Page.ClientScript;
            string postBackReference = manager.GetPostBackEventReference(btnInvoker, "");

            string script =
                string.Format(
                    @"if (confirm('{0}')) 
							{{ 
								{1}; 
							}}",
                    string.Format(ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBCODE_AK0_127), msg), postBackReference);

            // the same sript key "ConfirmSNMP" prevents registering and displaying sript twice
            if (!manager.IsStartupScriptRegistered("ConfirmSNMP"))
                System.Web.UI.ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ConfirmSNMP", script, true);

            return false;
        }

        return true;
    }

    private void UpdateViewForNode(Node node)
    {
        int selectedViewID = ViewSelector.SelectedViewID;
        if (selectedViewID <= 0)
        {
            NodeSettingsDAL.DeleteSpecificSettingForNode(node.Id, ViewNodeSettingValue);
            return;
        }
        var defaultViewId = ViewsByDeviceTypeDAL.GetViewIDForNodeType(node.MachineType);
        if (defaultViewId >= 0)
        {
            if (defaultViewId != selectedViewID)
                NodeSettingsDAL.SafeInsertNodeSetting(node.Id, ViewNodeSettingValue, selectedViewID);
            else
                NodeSettingsDAL.DeleteSpecificSettingForNode(node.Id, ViewNodeSettingValue);
        }
    }

    private ViewInfo GetViewFromNodeSettings(int nodeID)
    {
        IDictionary<String, String> nodeSettings = NodeSettingsDAL.GetNodeSettings(nodeID);
        String ViewId = String.Empty;
        if (nodeSettings.TryGetValue(ViewNodeSettingValue, out ViewId))
        {
            return ViewManager.GetViewById(Convert.ToInt32(ViewId));
        }
        return null;
    }

    private bool PerformSNMPValidation(ICoreBusinessLayer proxy)
    {
        string localizedErrorMessage;
        var result = NodeWorkflowHelper.ValidateSNMPNodeWithErrorMessage(
                    proxy,
                    PollingMethodSelector.SnmpInfo.SNMPVersion,
                    PollingMethodSelector.SnmpInfo.Ip,
                    PollingMethodSelector.SnmpInfo.SNMPPort,
                    PollingMethodSelector.SnmpInfo.CommunityString,
                    PollingMethodSelector.SnmpInfo.RWCommunityString,
                    PollingMethodSelector.SnmpInfo.AuthPasswordKey,
                    PollingMethodSelector.SnmpInfo.RWAuthPasswordKey,
                    PollingMethodSelector.SnmpInfo.SNMPv3AuthType,
                    PollingMethodSelector.SnmpInfo.RWSNMPv3AuthType,
                    PollingMethodSelector.SnmpInfo.SNMPv3PrivacyType,
                    PollingMethodSelector.SnmpInfo.RWSNMPv3PrivacyType,
                    PollingMethodSelector.SnmpInfo.PrivacyPasswordKey,
                    PollingMethodSelector.SnmpInfo.RWPrivacyPasswordKey,
                    PollingMethodSelector.SnmpInfo.SNMPv3Context,
                    PollingMethodSelector.SnmpInfo.RWSNMPv3Context,
                    PollingMethodSelector.SnmpInfo.SNMPv3Username,
                    PollingMethodSelector.SnmpInfo.RWSNMPv3Username,
                    PollingMethodSelector.SnmpInfo.AuthKeyIsPwd,
                    PollingMethodSelector.SnmpInfo.RWAuthKeyIsPwd,
                    PollingMethodSelector.SnmpInfo.PrivKeyIsPwd,
                    PollingMethodSelector.SnmpInfo.RWAuthKeyIsPwd,
                    out localizedErrorMessage);

        if (!result.ValidationSucceeded)
        {
            ClientScriptManager manager = Page.ClientScript;
            string postBackReference = manager.GetPostBackEventReference(btnInvoker, "");
            string message = ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBCODE_AK0_126);
            if (!string.IsNullOrEmpty(localizedErrorMessage))
            {
                message = ControlHelper.EncodeJsString(string.Format(Resources.CoreWebContent.WEBCODE_PS0_30, localizedErrorMessage));
            }

            string script = string.Format(@"if (confirm('{0}')) 
							{{ 
								{1}; 
							}}", message, postBackReference);

            if (!manager.IsStartupScriptRegistered("ConfirmSNMP"))
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "ConfirmSNMP", script, true);

            return false;
        }
        else
        {
            Submit(true);
            return true;
        }

    }

    private bool PerformWMIValidation(ICoreBusinessLayer proxy)
    {
        var result = NodeWorkflowHelper.ValidateWMINode(proxy, PollingMethodSelector.WmiInfo.Ip,
                                                        PollingMethodSelector.WmiInfo.Login,
                                                        PollingMethodSelector.WmiInfo.PlainPassword);

        if (!result)
        {
            ClientScriptManager manager = Page.ClientScript;
            string postBackReference = manager.GetPostBackEventReference(btnInvoker, "");

            string message =
                Resources.CoreWebContent.WEBCODE_TM0_100;

            string script = string.Format(@"if (confirm('{0}')) 
							{{ 
								{1}; 
							}}", message, postBackReference);

            if (!manager.IsStartupScriptRegistered("ConfirmWMI"))
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "ConfirmWMI", script, true);

            return false;
        }
        else
        {
            Submit(true);
            return true;
        }
    }

    private bool PerformAgentValidation()
    {
        var settings = PollingMethodSelector.AgentInfo.GetDeploymentSettings();
        if (settings == null)
            return false;

        var result = new WebAgentManager().ValidateDeploymentSettings(settings);
        if (!result.Success)
        {
            ClientScriptManager manager = Page.ClientScript;
            string postBackReference = manager.GetPostBackEventReference(btnInvoker, "");

            string message =
                Resources.CoreWebContent.EditNodeProperties_UnableValidateAgent;

            string script = string.Format(@"if (confirm('{0}')) 
							{{ 
								{1}; 
							}}", message, postBackReference);

            if (!manager.IsStartupScriptRegistered("ConfirmWMI"))
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "ConfirmWMI", script, true);

            return false;
        }
        else
        {
            Submit(true);
            return true;
        }
    }

    private Node GetNodeById(int nodeId, int engineId)
    {
        using (ICoreBusinessLayer coreProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, engineId, true))
        {
            return coreProxy.GetNodeWithOptions(nodeId, false, false);
        }
    }

    private Nodes GetNodesByIds(int[] ids, int engineId)
    {
        var filter = new Dictionary<string, object>() { { "EngineID", engineId } };
        using (ICoreBusinessLayer coreProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, engineId, false))
        {
            return coreProxy.GetNodesFiltered(filter, false, false);
        }
    }

    private int GetEngineIdForNodeId(ICoreBusinessLayer proxy, int nodeId)
    {
        int engineId = proxy.GetEngineIdForNetObject(string.Format("N:{0}", nodeId));
        return engineId;
    }

    protected void btnInvoker_Click(object sender, EventArgs e)
    {
        Submit(true);
    }

    protected string GetReadOnlyCommunityStrings()
    {
        var sb = new StringBuilder();
        var table = SqlDAL.GetReadOnlyCommunityStrings();
        foreach (DataRow row in table.Rows)
        {
            sb.AppendFormat("'{0}',", CommonHelper.FixReadOnlyCommunityString(row[0].ToString()));
        }
        if (sb.Length > 0)
        {
            sb.Length--;
        }
        return sb.ToString();
    }

    protected void HostNameIP_SNMPModeChanged(object sender, EventArgs e)
    {
        OnPollingModeChanged();


    }

    private Dictionary<int, List<int>> GetGroupedNodeIds()
    {
        var nodeIdsCollection = new Dictionary<int, List<int>>();
        var engines = new List<int>();

        for (var i = 0; i < NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(pageGuid).Count; i++)
        {
            int id, engineId;
            var parts = NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(pageGuid)[i].Split(':');
            if (parts.Length < 3 || !int.TryParse(parts[1], out id) || !int.TryParse(parts[2], out engineId))
            {
                throw new ArgumentException(string.Format("'{0}' is not a valid net object id", NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(pageGuid)[i]));
            }

            if (!engines.Contains(engineId))
            {
                engines.Add(engineId);
            }

            if (!nodeIdsCollection.ContainsKey(engineId))
            {
                nodeIdsCollection[engineId] = new List<int>();
            }

            nodeIdsCollection[engineId].Add(id);
        }
        return nodeIdsCollection;
    }

    private bool SkipValidation
    {
        get { return hSkipValidate.Value == "1"; }
    }

    private void SnmpCredentialChangeNotification(Node node)
    {
        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, node.EngineID, true))
        {
            var currentNode = proxy.GetNode(node.ID);

            var originProperty = new PropertyBag()
            {
                {"NodeId", node.ID},
                {"IP_Address", node.IpAddress},
                {"Caption", node.Caption},
                {"Status", node.Status},
            };

            var currentProperty = new PropertyBag()
            { 
                {"NodeId", currentNode.ID},
                {"IP_Address", currentNode.IpAddress},
                {"Caption", currentNode.Caption},
                {"Status", currentNode.Status},
            };
            
            var roSetting = SolarWinds.Orion.Core.Common.DALs.NodeSettingsNames.RoSnmpCredentialSettingName;
            var rwSetting = SolarWinds.Orion.Core.Common.DALs.NodeSettingsNames.RwSnmpCredentialSettingName;

            if (node.NodeSettings.ContainsKey(roSetting))
                originProperty.Add(roSetting, node.NodeSettings[roSetting]);
            if (node.NodeSettings.ContainsKey(rwSetting))
                originProperty.Add(rwSetting, node.NodeSettings[rwSetting]);

            if (currentNode.NodeSettings.ContainsKey(roSetting))
                currentProperty.Add(roSetting, currentNode.NodeSettings[roSetting]);
            if (currentNode.NodeSettings.ContainsKey(rwSetting))
                currentProperty.Add(rwSetting, currentNode.NodeSettings[rwSetting]);

            var indication = new NodeIndication (IndicationType.System_InstanceModified, currentProperty, originProperty);

            var publisher = IndicationPublisher.CreateV3();
            if (publisher != null)
            {
                publisher.ReportIndication(indication);
            }
        }
    }

    protected void PanelOnLoad(object sender, EventArgs e)
    {
    }

    protected void NodePolling_OnEngineChanged(object sender, EventArgs e)
    {
        PollingMethodSelector.SetEngineId(NodePolling.EngineID);
    }

    private EvaluationMethod GetNodeStatusRollupMode(Node node)
        => node.NodeSettings.ContainsKey(Node.StatusRollupModeSettingName) 
        ? (EvaluationMethod)Convert.ToInt32(node.NodeSettings[Node.StatusRollupModeSettingName]) 
        : EvaluationMethod.Mixed;

    private void UpdateNodeStatusRollupMode(Node node, EvaluationMethod rollupMode)
    {
        if (node.NodeSettings.ContainsKey(Node.StatusRollupModeSettingName))
            node.NodeSettings.Remove(Node.StatusRollupModeSettingName);

        if (rollupMode != EvaluationMethod.Mixed)
            node.NodeSettings[Node.StatusRollupModeSettingName] = Convert.ToInt32(rollupMode).ToString();
    }
}
