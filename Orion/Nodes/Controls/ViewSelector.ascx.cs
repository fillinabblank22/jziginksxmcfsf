﻿using System;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.Web;

public partial class Orion_Nodes_Controls_ViewSelector : System.Web.UI.UserControl
{
    private int selectedViewId = 0;
    public int SelectedViewID
    {
        get
        {
            return Convert.ToInt32(lbxViews.SelectedValue);
        }
        set
        {
            if (value < 0)
                selectedViewId = 0;
            else
            selectedViewId = value;
        }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lbxViews.Items.Add(new ListItem(CoreWebContent.WEBDATA_TM0_124, "-1"));
            foreach (ViewInfo vInfo in ViewManager.GetViewsByType("NodeDetails"))
                lbxViews.Items.Add(new ListItem(vInfo.ViewTitle, vInfo.ViewID.ToString()));
            if (lbxViews.Items.Count > 0)
                lbxViews.SelectedValue = Convert.ToString(selectedViewId);
        }
    }
    
}