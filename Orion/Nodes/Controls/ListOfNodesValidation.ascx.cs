using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web;

public partial class Orion_Nodes_Controls_ListOfNodesValidation : System.Web.UI.UserControl
{
	protected void Page_Load(object sender, EventArgs e)
	{

	}

	public void AddListItem(string nodeCaption, string nodeValidation, bool validated)
	{
		litList.Text += string.Format("<div {0}> {1} - {2} </div>", "style=\" background-color:" + (validated ? "#D6F6C6;\"" : "#FEEE90; color: Red;\""), nodeCaption, nodeValidation);
	}

	protected void imbtnOk_Click(object sender, EventArgs e)
	{
		NodeWorkflowHelper.MultiSelectedNodeIds.Clear();
		Response.Redirect("Default.aspx");
	}
}
