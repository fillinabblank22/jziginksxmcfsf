<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DependenciesControl.ascx.cs" Inherits="Orion_Nodes_Controls_DependenciesControl" %>

<div runat="server" id="DependenciesBlock" class="contentBlock">
                      <table width="100%">
                        <tr>
                            <td class="contentBlockHeader">
                                <asp:Literal ID="DepBlockTitle" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_385 %>" />
                            </td>
                            <td style="text-align: right">
                                <%if ((this.Profile.AllowAdmin) || (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer))
                      {%>
                                <span>
                                    <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="" />
                                    <a href="/Orion/Admin/DependenciesView.aspx" id="manage" target="_blank" class="RenewalsLink">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.ResourcesAll_ManageDependencies) %> </a> 
                                        </span>
                                         <%} %>
                            </td>
                        </tr>
                    </table>

<table style="background-color: #ecedee;" width="100%">
    <tr>
        <td class="leftLabelColumn" style="vertical-align:top;">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_391) %>
        </td>
        <td class="rightInputColumn">
            <table style="margin-top:-2px;">
                <asp:Repeater runat="server" ID="repParentDep">
                <ItemTemplate>
                    <tr><td><%# DefaultSanitizer.SanitizeHtml(((System.Data.DataRow)Container.DataItem)["Name"]) %></td></tr>
                </ItemTemplate>
                </asp:Repeater>
            </table>
            
        </td>
        <td >
        </td>
    </tr>
    <tr>
        <td class="leftLabelColumn" style="vertical-align:top;">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_393) %>
        </td>
        <td class="rightInputColumn">
            <table id="Table1" style="margin-top:-2px;">
                <asp:Repeater runat="server" ID="repChildDep">
                <ItemTemplate>
                    <tr><td><%# DefaultSanitizer.SanitizeHtml(((System.Data.DataRow)Container.DataItem)["Name"]) %></td></tr>
                </ItemTemplate>
                </asp:Repeater>
            </table>
        </td>
    </tr>
</table>

</div>