﻿using System.Collections.Generic;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Controllers.Nodes;

public partial class Orion_Nodes_Controls_NodeNote : System.Web.UI.UserControl, INodePropertyPlugin
{
    private IList<Node> nodes;

    public bool Validate()
    {
        return true;
    }

    public void Initialize(IList<Node> nodes, NodePropertyPluginExecutionMode mode,
        Dictionary<string, object> pluginState)
    {
        this.nodes = nodes;
    }

    public bool Update()
    {
        if (string.IsNullOrEmpty(NodeNoteContent.Text))
            return true;

        NodeNotesController controller = new NodeNotesController();

        foreach (var node in this.nodes)
        {
            var nodeTmp = node;

            if (nodeTmp.Id == 0)
            {
                nodeTmp = NodeWorkflowHelper.Node;
            }

            if (nodeTmp.Id > 0)
            {
                controller.AddNodeNote(nodeTmp.Id, NodeNoteContent.Text);
            }
        }
        return true;
    }
}