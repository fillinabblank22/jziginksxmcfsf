<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HostNameIP.ascx.cs" Inherits="Orion_Nodes_Controls_HostNameIP" %>
   
    <tr>
        <td class="leftLabelColumn">
            <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_30 %>" />
        </td>
        <td class="rightInputColumn"><asp:TextBox runat="server" ID="txtHostNameIP" Width="250"></asp:TextBox>
            <span class="helpfulText" style="display:inline;">
                <asp:Literal ID="Literal1" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_YK0_24 %>" />
            </span>
        </td>        
    </tr>
    <tr id="DynamicIpAddressTR" runat="server">
        <td class="leftLabelColumn">
        </td>
        <td class="rightInputColumn">
        <asp:CheckBox ID="DynamicIpAddress" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_58 %>" />
        <br />
            <span class="helpfulText">
                <asp:Literal runat="server" ID="DynamicIpAddressDescription" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_31 %>" />
            </span>
        </td>        
    </tr>   
  

