<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ICMPSettings.ascx.cs" Inherits="Orion_Nodes_Controls_ICMPSettings" %>

<div class="contentBlockHeader">
    <asp:CheckBox ID="cbMultipleSelection" runat="server" AutoPostBack="True" OnCheckedChanged="cbMultipleSelection_CheckedChanged" /> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_395) %>
</div>
<table class="blueBox">
<tr>
	<td class="leftLabelColumn"><asp:Label runat="server" ID="lblStatusPolling">&nbsp;</asp:Label></td>
	<td class="rightInputColumn" nowrap="nowrap">
	    <asp:CheckBox runat="server" ID="cbICMPOnly" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_IB0_59 %>" Checked="true" Visible="true" AutoPostBack="true" OnCheckedChanged="cbICMPOnly_OnCheckedChanged"/>
	</td>
	<td>&nbsp;</td>
</tr>
</table>