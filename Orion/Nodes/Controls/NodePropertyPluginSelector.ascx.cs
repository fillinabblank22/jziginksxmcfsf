using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Models;
using SolarWinds.Orion.Web;

public partial class Orion_Nodes_Controls_NodePropertyPluginSelector : System.Web.UI.UserControl
{
    public HashSet<EngineServerType> SelectedEngineTypes
    {
        get;
        set;
    }

    public Orion_Nodes_Controls_NodePropertyPluginSelector()
    {
        SelectedEngineTypes = new HashSet<EngineServerType> { EngineServerType.Primary};
    }

    protected override void OnInit(EventArgs e)
    {
        if (IsPostBack)
        {
            var hf = (ControlHelper.FindControlRecursive(Page, "HiddenFieldGuid") as HiddenField);
            if (hf != null)
            {
                NodePropertyPluginManager.SetActivePage(Request.Form[hf.UniqueID]);
            }

            // On post back NodePropertyPluginManager is guaranteed to be initialized and placing data binding here guaranties that checkbox event is wired up
            // repNodePropertyPluginSelector.DataSource = NodePropertyPluginManager.Plugins;
            repNodePropertyPluginSelector.DataSource = NodePropertyPluginManager.GetPluginsForCurrentMode();
            repNodePropertyPluginSelector.DataBind();
        }

        base.OnInit(e);
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (NodePropertyPluginManager.GetPluginsForCurrentMode().Count < 1)
        {
            repNodePropertyPluginSelector.Visible = false;
        } 

        if (!IsPostBack)
        { // On first page load data binding should be here after Init() executed on host page and NodePropertyPluginManager intialized
            //repNodePropertyPluginSelector.DataSource = NodePropertyPluginManager.Plugins;
            repNodePropertyPluginSelector.DataSource = NodePropertyPluginManager.GetPluginsForCurrentMode();
            repNodePropertyPluginSelector.DataBind();
        }

        SolveVisibilityOfPluginsAccordingSelectedEngineTypes();
    }

    public void SolveVisibilityOfPluginsAccordingSelectedEngineTypes()
    {
        int visiblePluginCount = 0;
        if (repNodePropertyPluginSelector.Items == null)
        {
            return;
        }
        
        var plugins = (List<NodePropertyPluginWrapper>) repNodePropertyPluginSelector.DataSource;
        foreach (RepeaterItem repeaterItem in repNodePropertyPluginSelector.Items)
        {
            var checkbox = (CheckBox) repeaterItem.FindControl("chbxPluginVisible");
            var plugin = plugins.FirstOrDefault(item =>
                item.Title.Equals(checkbox?.Text, StringComparison.OrdinalIgnoreCase));

            if (SelectedEngineTypes != null && plugin != null)
            {
                bool support =
                    SelectedEngineTypes.All(item => plugin.SupportedPollingEngineTypes.Any(se => se == item));
                repeaterItem.Visible = support;
                visiblePluginCount += support ? 1 : 0;
                if (!support)
                {
                    checkbox.Checked = false;
                    AdjustPluginVisibility(checkbox);
                }
            }
            else
            {
                repeaterItem.Visible = true;
                visiblePluginCount += 1;
            }
        }

        repNodePropertyPluginSelector.Visible = visiblePluginCount > 0;
        orcInfoDiv.Visible = SelectedEngineTypes != null
                             && SelectedEngineTypes.Contains(EngineServerType.RemoteCollector)
                             && visiblePluginCount < repNodePropertyPluginSelector.Items.Count;
    }

    /// <summary>
    /// Checks if Plugin div on Add node / Edit node page should be shown.
    /// Plugin div is a section which contains for example Palo Alto Polling Settings or Cisco ACI Controller Polling Settings.
    /// </summary>
    /// <returns>True if Plugin div on Add node / Edit node page should be shown</returns>
    public bool ShouldShowPluginDiv()
    {
        if (repNodePropertyPluginSelector.Items == null)
        {
            return false;
        }

        var plugins = (List<NodePropertyPluginWrapper>)repNodePropertyPluginSelector.DataSource;
        foreach (RepeaterItem repeaterItem in repNodePropertyPluginSelector.Items)
        {
            var checkbox = (CheckBox)repeaterItem.FindControl("chbxPluginVisible");
            var plugin = plugins.FirstOrDefault(item =>
                item.Title.Equals(checkbox?.Text, StringComparison.OrdinalIgnoreCase));

            if (SelectedEngineTypes == null || plugin == null ||
                SelectedEngineTypes.All(item => plugin.SupportedPollingEngineTypes.Any(se => se == item)))
            {
                return true;
            }
        }

        return false;
    }

    protected void repNodePropertyPluginSelector_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var checkBox = (CheckBox) e.Item.FindControl("chbxPluginVisible");
            var hint = (Literal) e.Item.FindControl("pluginHint");
            var plugin = (NodePropertyPluginWrapper) e.Item.DataItem;

            checkBox.Text = plugin.Title;
            checkBox.Checked = plugin.Visible;
            hint.Text = plugin.Hint;
        }
    }

    protected void chbxPluginVisible_CheckedChanged(object sender, EventArgs e)
    {
        var checkBox = (CheckBox) sender;

        AdjustPluginVisibility(checkBox);
    }

    /// <summary>
    /// Finds all plugins corresponding to checkbox and makes them visible
    /// if and only if the checkbox is checked.
    /// </summary>
    private void AdjustPluginVisibility(CheckBox checkBox)
    {
        // select plugin corresponding to this checkbox
        var plugins = NodePropertyPluginManager.Plugins;
        var selected =
            from p in plugins
            where p.Title.Equals(checkBox.Text, StringComparison.InvariantCultureIgnoreCase)
            select p;

        // adjust visible
        foreach (var plugin in selected)
        {
            plugin.Visible = checkBox.Checked;
        }
    }
}