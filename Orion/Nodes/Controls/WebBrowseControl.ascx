<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WebBrowseControl.ascx.cs" Inherits="Orion_Nodes_Controls_WebBrowseControl" %>
<div class="contentBlock"> 
 <div class="contentBlockHeader" runat="server" id="WebBrowseHeader">
     <table>
         <tr>
             <td class="contentBlockModuleHeader">
                 <asp:Literal ID="webBrowseDescription" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_YK0_85 %>"/>
             </td>
             <td class="rightInputColumn">
                 <asp:TextBox ng-non-bindable ID="txtWebBrowse" runat="server" Width="200" /><br/>
                 <span class="helpfulText" style="padding: 0px;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO_207) %></span>
             </td>
         </tr>
     </table>
    </div>
</div>