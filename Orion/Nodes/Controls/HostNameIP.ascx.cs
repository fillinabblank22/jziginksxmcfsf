using System;
using System.ComponentModel;
using SolarWinds.Common.Net;

public delegate void SnmpModeChangeEventHandler(object sender, SNMPModeEventArgs e);

public partial class Orion_Nodes_Controls_HostNameIP : System.Web.UI.UserControl
{
    private bool _dynamicIP = false;

    #region public properties
    [Browsable(true)]
    [Category("Data")]
    public bool isDynamicIP
    {
        get { return _dynamicIP; }
        set { _dynamicIP = value; }
    }

    [Browsable(true)]
    [Category("Data")]
    public bool DynamicIpAddressValue
    {
        get
        {
            if (isDynamicIP)
                return DynamicIpAddress.Checked;
            return false;
        }
    }

    [Browsable(true)]
    [Category("Data")]
    public string HostNameIP
    {
        get
        {
            return txtHostNameIP.Text;
        }
        set
        {
            txtHostNameIP.Text = value;
        }
    }

    [Browsable(true)]
    [Category("Data")]
    public bool Enabled
    {
        get
        {
            return txtHostNameIP.Enabled;
        }
        set
        {
            txtHostNameIP.Enabled = value;
        }
    }

    public event EventHandler HostNameIPChanged;
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        DynamicIpAddressTR.Visible = isDynamicIP;
        if (isDynamicIP)
        {
            txtHostNameIP.TextChanged += new EventHandler(txtHostNameIP_TextChanged);
            txtHostNameIP.AutoPostBack = true;
        }
        if (!IsPostBack)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["IpAddress"]))
            {
                txtHostNameIP.Text = Request.QueryString["IpAddress"].Trim();
            }

            txtHostNameIP.Focus();
        }

    }

    void txtHostNameIP_TextChanged(object sender, EventArgs e)
    {
        if (HostHelper.IsIpAddress(txtHostNameIP.Text))
        {
            DynamicIpAddress.Enabled = false;
            DynamicIpAddress.Checked = false;

        }
        else
        {
            DynamicIpAddress.Enabled = true;
        }

        if (HostNameIPChanged != null)
            HostNameIPChanged(this, null);
    }

    /// <summary>
    /// Setups the text and editability of Hostname textbox.
    /// </summary>
    /// <param name="isEnabled">When set to <code>true</code> the textbox is editable;otherwise textbox is set to read-only mode.</param>
    /// <param name="hostname">Default value for hostname.</param>
    public void SetupControls(bool isEnabled, string hostname = null)
    {
        if (hostname != null)
        {
            txtHostNameIP.Text = hostname;
            DynamicIpAddress.Checked = !HostHelper.IsIpAddress(hostname);
        }

        txtHostNameIP.Enabled = isEnabled;
        txtHostNameIP.ReadOnly = !isEnabled;
        DynamicIpAddress.Enabled = isEnabled;
        

    }
}
