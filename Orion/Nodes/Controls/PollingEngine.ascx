<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PollingEngine.ascx.cs"
    Inherits="Orion_Nodes_Controls_PollingEngine" %>
<table>
    <tr>
        <td class="leftLabelColumn">
            <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_26 %>" />
        </td>
        <td class="rightInputColumn">
            <asp:DropDownList runat="server" ID="ddlPollingEngine" AutoPostBack="true" 
				onselectedindexchanged="ddlPollingEngine_SelectedIndexChanged">
            </asp:DropDownList>
        </td>
        <td>
        &nbsp;
        </td>
    </tr>
</table>
