using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SNMPv3AuthType = SolarWinds.Orion.Core.Common.Models.SNMPv3AuthType;
using SNMPv3PrivacyType = SolarWinds.Orion.Core.Common.Models.SNMPv3PrivacyType;
using SnmpCredentials = SolarWinds.Orion.Core.Common.Models.SnmpCredentials;
using SolarWinds.Orion.Core.SharedCredentials;
using CRED = SolarWinds.Orion.Core.Models.Credentials;

public partial class Orion_Nodes_Controls_NodeSNMPv3Credentials : System.Web.UI.UserControl
{
	private static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    private bool fipsEnabledOnSelectedEngine;

    private IDictionary<int, string> snmpV3Credentials;

	public string Title
	{
		get { return (string)ViewState[this.ID + "_Title"]; }
		set { ViewState[this.ID + "_Title"] = value; }
	}

	public bool Enabled
	{
		get {
			if (ViewState[this.ID + "_Enabled"] == null)
				ViewState[this.ID + "_Enabled"] = true;
			return (bool)ViewState[this.ID + "_Enabled"];
		}
		set {		
			ViewState[this.ID + "_Enabled"] = value;
			if (!value)
			{
				tbPasswordKey.IsEnabledPasswordTextBox = value;
				tbSecurityPasswordKey.IsEnabledPasswordTextBox = value;
				ddlSecurityMethod.Enabled = value;
                tbPasswordKey.IsEnabledPasswordTextBox = false;
			}

			tbSNMPv3Context.Enabled = value;
			tbSNMPv3Username.Enabled = value;

			tbSNMPv3Context.CssClass = tbSNMPv3Context.Enabled ? string.Empty : "disabled";
			tbSNMPv3Username.CssClass = tbSNMPv3Username.Enabled ? string.Empty : "disabled";

			//Credentials set
			txtCredentialsName.Enabled = value;
			ddlCredentialsSet.Enabled = value;
			btnDelete.Enabled = value;
			btnSave.Enabled = value;

			ddlAuthMethod.Enabled = value;
			if (value)
			{
				ddlAuthMethod_SelectedIndexChanged(null, null);
				ddlSecurityMethod_SelectedIndexChanged(null, null);
			}
		}
	}

	public SNMPv3AuthType SNMPv3AuthType
	{
		get
		{
			switch (ddlAuthMethod.SelectedValue)
			{
				case "None":
					return SNMPv3AuthType.None;
				case "MD5":
					return SNMPv3AuthType.MD5;
				case "SHA1":
					return SNMPv3AuthType.SHA1;
				default:
					return SNMPv3AuthType.None;
			}
		}
		set 	{
			switch (value)
			{
				case SNMPv3AuthType.MD5:
                    if (ddlAuthMethod.Items.Cast<ListItem>().Any(item => string.Equals(item.Value, SNMPv3AuthType.MD5.ToString(), StringComparison.OrdinalIgnoreCase)))
                    {
                        ddlAuthMethod.SelectedValue = value.ToString();
                    }
                    else
                    {
                        ddlAuthMethod.SelectedIndex = 0;
                    }
                    break;
				case SNMPv3AuthType.SHA1:
					ddlAuthMethod.SelectedValue = value.ToString();
					break;
				default:
					ddlAuthMethod.SelectedIndex = 0;
					break;
			}
			ddlAuthMethod_SelectedIndexChanged(null, null);
		}
	}

	public SNMPv3PrivacyType SNMPv3PrivacyType
	{
		get
		{
			switch (ddlSecurityMethod.SelectedValue)
			{
				case "None":
					return SNMPv3PrivacyType.None;
				case "DES56":
					return SNMPv3PrivacyType.DES56;
				case "AES128":
					return SNMPv3PrivacyType.AES128;
                case "AES192":
                    return SNMPv3PrivacyType.AES192;
                case "AES256":
                    return SNMPv3PrivacyType.AES256;
                default:
					return SNMPv3PrivacyType.None;
			}
		}
		set
		{
			switch (value)
			{
				case SNMPv3PrivacyType.DES56:
                    if (ddlSecurityMethod.Items.Cast<ListItem>().Any(item => string.Equals(item.Value, SNMPv3PrivacyType.DES56.ToString(), StringComparison.OrdinalIgnoreCase)))
                    {
                        ddlSecurityMethod.SelectedValue = value.ToString(); 
                    }
                    else
                    {
                        ddlSecurityMethod.SelectedIndex = 0;
                    }
                    break;
                case SNMPv3PrivacyType.AES128:
                case SNMPv3PrivacyType.AES192:
                case SNMPv3PrivacyType.AES256:
			        ddlSecurityMethod.SelectedValue = value.ToString(); break;
                default:
					ddlSecurityMethod.SelectedIndex = 0; break;
			}
			ddlSecurityMethod_SelectedIndexChanged(null, null);
		}
	}

	public string AuthPasswordKey
	{
		get { return tbPasswordKey.Text; }
		set { tbPasswordKey.Text = value; }
	}

	public string PrivacyPasswordKey
	{
		get { return tbSecurityPasswordKey.Text; }
		set { tbSecurityPasswordKey.Text = value; }
	}

	public string Username
	{
		get { return tbSNMPv3Username.Text; }
		set { tbSNMPv3Username.Text = value; }
	}

	public string SNMPv3Context
	{
		get { return tbSNMPv3Context.Text; }
		set { tbSNMPv3Context.Text = value; }
	}


	public bool AuthKeyIsPwd
	{
		get { return !cbIsPasswordKey.Checked; }
		set { cbIsPasswordKey.Checked = !value; }
	}

	public bool PrivKeyIsPwd
	{
		get { return !cbIsSecurityPasswordKey.Checked; }
		set { cbIsSecurityPasswordKey.Checked = !value; }
	}

    public void HandleEngineChange(int engineID)
    {
        fipsEnabledOnSelectedEngine = EnginesDAL.IsFIPSModeEnabled(engineID);
        
        if (fipsEnabledOnSelectedEngine)
        {
            ListItem authMethod = ddlAuthMethod.Items.FindByValue("MD5");
            authMethod.Enabled = false;
            // unselect authorization method
            if (authMethod.Selected)
            {
                ddlAuthMethod.SelectedIndex = -1;
                ddlAuthMethod_SelectedIndexChanged(null, null);
            }

            ListItem securityMethod = ddlSecurityMethod.Items.FindByValue("DES56");
            securityMethod.Enabled = false;
            // unselect authorization method
            if (securityMethod.Selected)
            {
                ddlSecurityMethod.SelectedIndex = -1;
                ddlSecurityMethod_SelectedIndexChanged(null, null);
            }
        }
        else
        {
            ddlAuthMethod.Items.FindByValue("MD5").Enabled = true;
            ddlSecurityMethod.Items.FindByValue("DES56").Enabled = true;
        }
        FilterCredentialsSet();
    }

	protected void Page_Load(object sender, EventArgs e)
	{
        AuthPasswordKey = AuthPasswordKey;
        PrivacyPasswordKey = PrivacyPasswordKey;

		if (!IsPostBack)
		{
            //Disabled due to CORE-3688
		    btnDelete.Enabled = false;
            btnDelete.Style.Add("display", "none");

			UpdateDeleteConfirmationMessage();

			LoadCredentialsSet();
		}
	}

	protected void ddlAuthMethod_SelectedIndexChanged(object sender, EventArgs e)
	{
        tbPasswordKey.IsEnabledPasswordTextBox = ddlAuthMethod.SelectedValue != "None";
        cbIsPasswordKey.Enabled = tbPasswordKey.IsEnabledPasswordTextBox;
		ddlSecurityMethod.Enabled = ddlAuthMethod.SelectedValue != "None";
		ddlSecurityMethod_SelectedIndexChanged(sender, e);

        NotifyCredentialsChanged(); //Propagate change up in a hierarchy
	}

	protected void ddlSecurityMethod_SelectedIndexChanged(object sender, EventArgs e)
	{
		tbSecurityPasswordKey.IsEnabledPasswordTextBox = ddlSecurityMethod.SelectedValue != "None";
		cbIsSecurityPasswordKey.Enabled = tbSecurityPasswordKey.IsEnabledPasswordTextBox;

        NotifyCredentialsChanged(); //Propagate change up in a hierarchy
	}

    protected void OnCredentialDataChanged(object sender, EventArgs e)
    {
        NotifyCredentialsChanged();
    }

    private void NotifyCredentialsChanged()
    {
        RaiseBubbleEvent(this, new CommandEventArgs(CommonConstants.SNMPV3CredentialsChangedCommandName, null));
    }

    private CredentialManager cMan = new CredentialManager();

    private void LoadCredentialsSet()
    {
        var credentials = cMan.GetCredentials<CRED.SnmpCredentialsV3>();
        IDictionary<int, string> dict = new Dictionary<int, string>();
        foreach (var cred in credentials)
        {
            if (!cred.IsBroken)
                dict[cred.ID.Value] = cred.Name;
        }


        ddlCredentialsSet.DataSource = dict;
        ddlCredentialsSet.DataBind();
        ddlCredentialsSet.Items.Insert(0, new ListItem(string.Empty, "-1"));

        FilterCredentialsSet();
    }

    private void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}

	protected void btnSave_Click(object sender, EventArgs e)
	{
		SaveCredentials(false);
	}
	protected void btnDelete_Click(object sender, EventArgs e)
	{
        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
		{
			proxy.DeleteSnmpV3CredentialsByID(Int32.Parse(ddlCredentialsSet.SelectedValue));

			LoadCredentialsSet();
		}
	}

	protected void ddlCredentialsSet_SelectedIndexChanged(object sender, EventArgs e)
	{
		//UpdateDeleteConfirmationMessage();

	    int selCredID = Int32.Parse(ddlCredentialsSet.SelectedValue);

	    if (selCredID > 0)
	    {
	        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
	        {
	            var credentials = proxy.GetSnmpV3CredentialsByID(selCredID);

	            if (credentials != null)
	            {
	                tbSNMPv3Username.Text = credentials.UserName;
	                tbSNMPv3Context.Text = credentials.Context;

	                ddlAuthMethod.SelectedValue = credentials.AuthenticationType.ToString();
	                ddlAuthMethod_SelectedIndexChanged(ddlAuthMethod, new EventArgs());

	                this.AuthPasswordKey = credentials.AuthenticationPassword;

	                ddlSecurityMethod.SelectedValue = credentials.PrivacyType.ToString();
	                ddlSecurityMethod_SelectedIndexChanged(ddlSecurityMethod, new EventArgs());

	                tbSecurityPasswordKey.Text = credentials.PrivacyPassword;

	                if (!ddlCredentialsSet.SelectedValue.Equals("-1", StringComparison.OrdinalIgnoreCase))
	                {
	                    AuthKeyIsPwd = credentials.AuthenticationKeyIsPassword;
	                    PrivKeyIsPwd = credentials.PrivacyKeyIsPassword;
	                }
	                else
	                {
	                    AuthKeyIsPwd = true;
	                    PrivKeyIsPwd = true;
	                }
	                this.PrivacyPasswordKey = credentials.PrivacyPassword;
	            }
	        }
	    }

	    NotifyCredentialsChanged(); //Propagate change up in a hierarchy
	}

	protected void Invoke(object sender, EventArgs e)
	{
		SaveCredentials(true);
	}

	#region private methods
	private void UpdateDeleteConfirmationMessage()
	{
		if (ddlCredentialsSet.SelectedIndex == -1 || ddlCredentialsSet.SelectedIndex == 0)
		{
			btnDelete.Attributes["onclick"] = ControlHelper.JsFormat("window.alert('{0}'); return false;", Resources.CoreWebContent.WEBCODE_SO0_14);
		}
		else
		{
			btnDelete.Attributes["onclick"] = ControlHelper.JsFormat("return confirm('{0}')", string.Format(Resources.CoreWebContent.WEBCODE_SO0_15, ddlCredentialsSet.SelectedItem.Text));
		}
	}

    private void SaveCredentials(bool forceSave)
    {
        if (string.IsNullOrEmpty(txtCredentialsName.Text))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "emptyCredentials",
                ControlHelper.JsFormat("alert('{0}');", Resources.CoreWebContent.WEBCODE_SO0_16), true);
            return;
        }

        if (CredentialsExist(txtCredentialsName.Text) && !forceSave)
        {
            ClientScriptManager manager = this.Page.ClientScript;

            string script = ControlHelper.JsFormat("alert('{0}')", string.Format(Resources.CoreWebContent.WEBCODE_AY0_105, txtCredentialsName.Text));

            if (!manager.IsStartupScriptRegistered("SaveConfirmation"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SaveConfirmation", script, true);

            return;
        }

        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            var credentials = new CRED.SnmpCredentialsV3
            {
                UserName = this.tbSNMPv3Username.Text,
                AuthenticationKeyIsPassword = AuthKeyIsPwd,
                AuthenticationPassword = this.tbPasswordKey.Text,
                PrivacyKeyIsPassword = PrivKeyIsPwd,
                PrivacyPassword = this.tbSecurityPasswordKey.Text,
                Context = this.tbSNMPv3Context.Text,
                Name = txtCredentialsName.Text,
                Description = string.Empty,
                PrivacyType =
                    (CRED.SNMPv3PrivacyType)
                        Enum.Parse(typeof (CRED.SNMPv3PrivacyType), this.ddlSecurityMethod.SelectedValue),
                AuthenticationType =
                    (CRED.SNMPv3AuthType) Enum.Parse(typeof (CRED.SNMPv3AuthType), this.ddlAuthMethod.SelectedValue)
            };

            if (CredentialsExist(txtCredentialsName.Text))
            {
                if (snmpV3Credentials == null)
                    snmpV3Credentials = proxy.GetSnmpV3CredentialsSet();
                int? credId = (from p in snmpV3Credentials
                    where p.Value.Equals(credentials.Name)
                    select p.Key)
                    .FirstOrDefault();
                credentials.ID = credId;
                proxy.UpdateSnmpV3Credentials(credentials);
            }
            else
                proxy.InsertSnmpV3Credentials(credentials);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "credentialsSaved",
                ControlHelper.JsFormat("window.alert('{0}');",
                    string.Format(Resources.CoreWebContent.WEBCODE_SO0_18, txtCredentialsName.Text)), true);
            LoadCredentialsSet();
        }
    }

    private bool CredentialsExist(string textToSearch)
	{
		foreach (ListItem item in ddlCredentialsSet.Items)
		{
			if (item.Text.Equals(textToSearch, StringComparison.InvariantCultureIgnoreCase))
				return true;
		}
		return false;
	}

    private void FilterCredentialsSet()
    {
        if (fipsEnabledOnSelectedEngine)
        {
            // check and remove non FIPS complaint credentials
            using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
            {
                bool credentialSetRemoved = false;
                foreach (var credentialsItem in ddlCredentialsSet.Items.Cast<ListItem>())
                {
                    CRED.SnmpCredentialsV3 credentials = proxy.GetSnmpV3Credentials(credentialsItem.Value);
                    if (credentials != null && !CredentialHelper.IsFIPSCompatible(credentials))
                    {
                        credentialsItem.Enabled = false;
                        if (credentialsItem.Selected)
                        {
                            credentialSetRemoved = true;
                        }
                    }
                }

                if (credentialSetRemoved)
                {
                    ddlCredentialsSet.SelectedIndex = -1;
                    ddlCredentialsSet_SelectedIndexChanged(null, null);
                }
            }
        }
        else
        {
            foreach (var credentialsItem in ddlCredentialsSet.Items.Cast<ListItem>())
            {
                credentialsItem.Enabled = true;
            }
        }
    }
	#endregion
}
