using System;
using System.Net;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Net.Sockets;

using SolarWinds.Logging;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common.i18n;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Events;
using SolarWinds.Orion.Web.InformationService;
using Node = SolarWinds.Orion.Core.Common.Models.Node;
using NodeSubType = SolarWinds.Orion.Core.Common.Models.NodeSubType;
using SNMPv3AuthType = SolarWinds.Orion.Core.Common.Models.SNMPv3AuthType;
using SNMPv3PrivacyType = SolarWinds.Orion.Core.Common.Models.SNMPv3PrivacyType;
using SNMPVersion = SolarWinds.Orion.Core.Common.Models.SNMPVersion;
using System.Web.UI;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using CRED = SolarWinds.Orion.Core.Models.Credentials;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Nodes_Controls_NodeSNMP : System.Web.UI.UserControl
{
	private static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    private bool disableEventBubbling = false;
    private bool hideValidateSnmpButton = false;

    public int EngineID
    {
        get { return (int?)ViewState["EngineID"] ?? 0; }
        set
        {
            ViewState["EngineID"] = (int?)value;
            SNMPv3Cred.HandleEngineChange(value);
            RWSNMPv3Cred.HandleEngineChange(value);

            log.DebugFormat("SNMPInfo.EngineID set to {0}", value);
        }
    }

	protected static void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}


    public bool MultipleSelection
    {
        get { return (bool) ViewState["MultipleSelection"]; }
        set
        {
            ViewState["MultipleSelection"] = value;
            imbtnValidateSNMP.Visible = IsValidateSnmpButtonVisible;
        }
    }   

    public bool HideValidateSnmpButton
    {
        get { return hideValidateSnmpButton; }
        set
        {
            hideValidateSnmpButton = value;
            imbtnValidateSNMP.Visible = IsValidateSnmpButtonVisible;
        }
    }

    private bool IsValidateSnmpButtonVisible
    {
        get { return !hideValidateSnmpButton && !this.MultipleSelection; }
    }

	public string SNMPVersionDescription
	{
		get { return ltrlSNMPVersionDescr.Text; }
		set { ltrlSNMPVersionDescr.Text = value; }
	}

	public string Ip
	{
		get { return (string)ViewState[this.ID + "_ip"]; }
		set { ViewState[this.ID + "_ip"] = value; }
	}

    public AddressFamily AddressResolution
    {
        get 
        { 
            object value = ViewState[this.ID + "_addressResolution"];
            return value != null ? (AddressFamily)value : AddressFamily.Unspecified;
        }
        set { ViewState[this.ID + "_addressResolution"] = value; }
    }

	public DropDownList SnmpVersionControl
	{
		get { return ddlSNMPVersion; }
	}

	public SNMPVersion SNMPVersion
	{
		get
		{
			switch (ddlSNMPVersion.SelectedValue)
			{
				case "SNMPv1":
					return SNMPVersion.SNMP1;
				case "SNMPv2c":
					return SNMPVersion.SNMP2c;
				case "SNMPv3":
					return SNMPVersion.SNMP3;
				default:
					return SNMPVersion.None;
			}
		}
		set
		{
			switch (value)
			{
				case SNMPVersion.SNMP1:
					ddlSNMPVersion.SelectedIndex = 0; break;
				case SNMPVersion.SNMP2c:
					ddlSNMPVersion.SelectedIndex = 1; break;
				case SNMPVersion.SNMP3:
					ddlSNMPVersion.SelectedIndex = 2; break;
				//default: ddlSNMPVersion.SelectedIndex = 0; break;
			}
		}
	}

	public string CommunityString
	{
		get { return tbCommunityString.Text; }
		set { tbCommunityString.Text = value; }
	}

	public string RWCommunityString
	{
		get { return tbRWCommunityString.Text; }
		set { tbRWCommunityString.Text = value; }
	}

    /// <summary>
    /// Hides all fields related to RW community strings (includes all SNMPv3 fields as well)
    /// </summary>
    public bool HideRWCommunityString
    {
        get { return !trRWCommunityString.Visible; }
        set 
        {
            trRWCommunityString.Visible = !value;
            RWSNMPv3Cred.Visible = !value; 
        }
    }


	public uint SNMPPort
	{
		get
		{
			uint port = 0;
			uint.TryParse(tbSNMPPort.Text, out port);
			return port;
		}
		set { tbSNMPPort.Text = value.ToString(); }
	}

	public bool Allow64bitCounters
	{
		get { return cbAllow64bitCounters.Checked; }
		set { cbAllow64bitCounters.Checked = value; }
	}

    public bool HideAllow64bitCountersCheckbox
    {
        get { return !cbAllow64bitCounters.Visible; }
        set { cbAllow64bitCounters.Visible = !value; }
    }

    public string SNMPv3Username
    {
        get { return SNMPv3Cred.Username; }
        set { SNMPv3Cred.Username = value; }
    }

    public string SNMPv3Context
    {
        get { return SNMPv3Cred.SNMPv3Context; }
        set { SNMPv3Cred.SNMPv3Context = value; }
    }

    public SNMPv3AuthType SNMPv3AuthType
    {
        get
        {
            return SNMPv3Cred.SNMPv3AuthType;
        }
        set
        {
            SNMPv3Cred.SNMPv3AuthType = value;
        }
    }

	public SNMPv3PrivacyType SNMPv3PrivacyType
	{
		get
		{
            return SNMPv3Cred.SNMPv3PrivacyType;
		}
		set
		{
			SNMPv3Cred.SNMPv3PrivacyType = value;
		}
	}

	public string AuthPasswordKey
	{
        get { return SNMPv3Cred.AuthPasswordKey; }
		set { SNMPv3Cred.AuthPasswordKey = value; }
	}

	public string PrivacyPasswordKey
	{
        get { return SNMPv3Cred.PrivacyPasswordKey; }
		set { SNMPv3Cred.PrivacyPasswordKey = value; }
	}

	public bool AuthKeyIsPwd
	{
        get { return SNMPv3Cred.AuthKeyIsPwd; }
		set { SNMPv3Cred.AuthKeyIsPwd = value; }
	}

	public bool PrivKeyIsPwd
	{
		get { return SNMPv3Cred.PrivKeyIsPwd; }
		set { SNMPv3Cred.PrivKeyIsPwd = value; }
	}
	
	public string RWSNMPv3Username
	{
		get { return RWSNMPv3Cred.Username; }
		set { RWSNMPv3Cred.Username = value; }
	}

	public string RWSNMPv3Context
	{
		get { return RWSNMPv3Cred.SNMPv3Context; }
		set { RWSNMPv3Cred.SNMPv3Context = value; }
	}

	public SNMPv3AuthType RWSNMPv3AuthType
	{
		get
		{
			return RWSNMPv3Cred.SNMPv3AuthType;
		}
		set
		{
			RWSNMPv3Cred.SNMPv3AuthType = value;
		}
	}

	public SNMPv3PrivacyType RWSNMPv3PrivacyType
	{
		get
		{
			return RWSNMPv3Cred.SNMPv3PrivacyType;
		}
		set
		{
			RWSNMPv3Cred.SNMPv3PrivacyType = value;
		}
	}

	public string RWAuthPasswordKey
	{
		get { return RWSNMPv3Cred.AuthPasswordKey; }
		set { RWSNMPv3Cred.AuthPasswordKey = value; }
	}

	public string RWPrivacyPasswordKey
	{
		get { return RWSNMPv3Cred.PrivacyPasswordKey; }
		set { RWSNMPv3Cred.PrivacyPasswordKey = value; }
	}

	public bool RWAuthKeyIsPwd
	{
		get { return RWSNMPv3Cred.AuthKeyIsPwd; }
		set { RWSNMPv3Cred.AuthKeyIsPwd = value; }
	}

	public bool RWPrivKeyIsPwd
	{
		get { return RWSNMPv3Cred.PrivKeyIsPwd; }
		set { RWSNMPv3Cred.PrivKeyIsPwd = value; }
	}

	public void cbMultipleSelection_CheckedChanged(bool isChecked)
	{
		tbCommunityString.Enabled = isChecked;
        tbRWCommunityString.Enabled = isChecked;
        tbSNMPPort.Enabled = isChecked;
        ddlSNMPVersion.Enabled = isChecked;
        cbAllow64bitCounters.Enabled = isChecked;
        imbtnValidateSNMP.Enabled = isChecked;
        SNMPv3Cred.Enabled = isChecked;
        RWSNMPv3Cred.Enabled = isChecked;

		tbCommunityString.CssClass = tbCommunityString.Enabled ? "CommunityStringTextBox" : "disabled";
		tbRWCommunityString.CssClass = tbRWCommunityString.Enabled ? string.Empty : "disabled";
		tbSNMPPort.CssClass = tbSNMPPort.Enabled ? string.Empty : "disabled";
		tbSNMPPort.CssClass = tbSNMPPort.Enabled ? string.Empty : "disabled";
	}

	protected override void OnInit(EventArgs e)
	{
	    this.disableEventBubbling = true;

		base.OnInit(e);
		SetupSNMPv3CredentialsFromSession();

	    this.disableEventBubbling = false;
	}
    
	protected void Page_Load(object sender, EventArgs e)
	{
	    PageHelper.SetTestCredentialTimeoutForUpdatePanel(this.Page);

        if (SNMPVersion == SNMPVersion.SNMP3)
		{ 
			pnlSNMPV1_2.Visible = false;
			pnlSNMPV3.Visible = true;
		}
		else
		{
			pnlSNMPV1_2.Visible = true;
			pnlSNMPV3.Visible = false;
		}

		tbRWCommunityString.Attributes.Add("value", (this.RWCommunityString == " ") ? string.Empty : this.RWCommunityString);
        imbtnValidateSNMP.Visible = IsValidateSnmpButtonVisible;

		switch (SNMPVersion)
		{
			case SNMPVersion.SNMP3:
				ltrlSNMPVersionDescr.Text = Resources.CoreWebContent.WEBCODE_SO0_1;
				break;
			case SNMPVersion.SNMP2c:
				ltrlSNMPVersionDescr.Text = Resources.CoreWebContent.WEBCODE_SO0_2;
				break;
			case SNMPVersion.SNMP1:
				ltrlSNMPVersionDescr.Text = Resources.CoreWebContent.WEBCODE_SO0_3;
				break;
		}

	    var script = string.Format(@"
    <script type='text/javascript'>
    //<![CDATA[
        var CommunityStringTextBoxChangedEvent = function() {{ setTimeout(function(){{ {0} }},0);}};
    //]]>
    </script>", this.Page.ClientScript.GetPostBackEventReference(this.tbCommunityString, "") );
        //By registering the script in this manner we ensure it runs when it is loaded via update panel.  Standard scripts are not run.
        ScriptManager.RegisterStartupScript(this, this.GetType(), "CommunityStringTextBoxChangedEvent", script, false);
	}

    protected override bool OnBubbleEvent(object source, EventArgs args)
    {
        CommandEventArgs command = args as CommandEventArgs;

        if (command != null)
        {
            switch (command.CommandName)
            {
                case CommonConstants.SNMPV3CredentialsChangedCommandName :
                    if (!this.disableEventBubbling) //FB192775
                    {
                        SNMPSettingsChanged(source, args); //Build up propagation command up in a hierarchy    
                    }

                    return true; //Stop propagation from inner controls
            }
        }

        return base.OnBubbleEvent(source, args);
    }

    protected void SNMPSettingsChanged(object sender, EventArgs e)
    {
        Node master = new Node();
        FillNodeWithValues(master); //Collect current SNMP settings ans set it to "container" node isntance

        RaiseBubbleEvent(this, new SNMPDetailsChangeEventArgs(master));    
    }

    protected void imbtnValidateSNMP_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(this.Ip))
        {
            SetSNMPValidationBar(false, string.Empty);
            return;
        }

		if (!SolarWinds.Common.Net.HostHelper.IsIpAddress(this.Ip))
		{
			try
			{
                AddressFamily addressResolution = this.AddressResolution;
                //if address resolution is not specified take the default settings
                if ((addressResolution != AddressFamily.InterNetwork) && (addressResolution != AddressFamily.InterNetworkV6))
                {
                    var defaultResolution = SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("SWNetPerfMon-Settings-Default Preferred AddressFamily DHCP");
                    addressResolution = (defaultResolution != null) && (defaultResolution.SettingValue == 6) ? AddressFamily.InterNetworkV6 : AddressFamily.InterNetwork;
                }

                try
                {
                    using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, EngineID, false))
                    {
                        IPAddress ip = proxy.GetHostAddress(this.Ip.Trim(), addressResolution);
                        this.Ip = ip.ToString();
                    }
                }
                catch (System.ServiceModel.FaultException ex) 
                {
                    var i18nException = new CoreValidationException(CoreValidationException.Reason.MissingIpAddress, ex, this.Ip);
                    log.Error(i18nException);
                    throw i18nException;
                }

			}
			catch
			{
				SetSNMPValidationBar(false, string.Empty);
				return;
			}
		}
        bool validationSucceeded;
        string msg = string.Empty;

        try
        {
            using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, EngineID, false))
            {
                validationSucceeded = NodeWorkflowHelper.ValidateSNMPNodeWithErrorMessage(proxy, this.SNMPVersion, this.Ip, this.SNMPPort, this.CommunityString, this.RWCommunityString,
                    this.AuthPasswordKey, this.RWAuthPasswordKey, this.SNMPv3AuthType, this.RWSNMPv3AuthType, this.SNMPv3PrivacyType, this.RWSNMPv3PrivacyType,
                    this.PrivacyPasswordKey, this.RWPrivacyPasswordKey, this.SNMPv3Context, this.RWSNMPv3Context, this.SNMPv3Username, this.RWSNMPv3Username,
                    this.AuthKeyIsPwd, this.RWAuthKeyIsPwd, this.PrivKeyIsPwd, this.RWAuthKeyIsPwd, out msg).ValidationSucceeded;
            }
        }
        catch (Exception ex)
        {
            validationSucceeded = false;
            msg = ex is i18nReadyException ? (ex as i18nReadyException).UserMessage : ex.Message;
        }
        SetSNMPValidationBar(validationSucceeded, msg);

    }

    private void SetSNMPValidationBar(bool valid, string msg)
    {
        SNMPValidationFalse.Visible = !valid;
        SNMPValidationTrue.Visible = valid;
        SNMPValidationFailedMessage.Text = msg;
    }

    public Node FillNodeWithValues(Node node)
	{
		node.SNMPPort = this.SNMPPort;
		node.SNMPVersion = this.SNMPVersion;
		node.Allow64BitCounters = this.Allow64bitCounters;
		node.NodeSubType = (node.NodeSubType == NodeSubType.Agent ? NodeSubType.Agent : (this.SNMPVersion == SNMPVersion.None ? NodeSubType.ICMP : NodeSubType.SNMP));

		node.ObjectSubType = node.NodeSubType.ToString();

        if (node.SNMPVersion == SNMPVersion.SNMP1 || node.SNMPVersion == SNMPVersion.SNMP2c)
        {
            node.ReadOnlyCredentials.CommunityString = this.CommunityString;
            node.ReadWriteCredentials.CommunityString = this.RWCommunityString;
        }
        else
        {
            node.ReadOnlyCredentials.CommunityString = String.Empty;
            node.ReadWriteCredentials.CommunityString = String.Empty;
        }

		if (node.SNMPVersion == SNMPVersion.SNMP3)
		{
			SetSNMP3NodeFromControl(node);
		}

	    ClearWmiNodeSettings(node);
        
        return node;
	}

    private void ClearWmiNodeSettings(Node node)
    {
        node.WmiCredential = new WmiCredentials();
    }

    private void SetSNMP3NodeFromControl(Node node)
    {
        if (node.SNMPVersion == SNMPVersion.SNMP3)
        {
            node.ReadOnlyCredentials.SNMPv3AuthType = this.SNMPv3AuthType;
            node.ReadOnlyCredentials.SNMPv3PrivacyType = this.SNMPv3PrivacyType;
            node.ReadOnlyCredentials.SnmpV3Context = this.SNMPv3Context;
            node.ReadOnlyCredentials.SNMPv3UserName = this.SNMPv3Username;
            node.ReadOnlyCredentials.SNMPv3AuthPassword = this.AuthPasswordKey;
            node.ReadOnlyCredentials.SNMPv3PrivacyPassword = this.PrivacyPasswordKey;
            node.ReadOnlyCredentials.SNMPV3AuthKeyIsPwd = this.AuthKeyIsPwd;
            node.ReadOnlyCredentials.SNMPV3PrivKeyIsPwd = this.PrivKeyIsPwd;

            node.ReadWriteCredentials.SNMPv3AuthType = this.RWSNMPv3AuthType;
            node.ReadWriteCredentials.SNMPv3PrivacyType = this.RWSNMPv3PrivacyType;
            node.ReadWriteCredentials.SnmpV3Context = this.RWSNMPv3Context;
            node.ReadWriteCredentials.SNMPv3UserName = this.RWSNMPv3Username;
            node.ReadWriteCredentials.SNMPv3AuthPassword = this.RWAuthPasswordKey;
            node.ReadWriteCredentials.SNMPv3PrivacyPassword = this.RWPrivacyPasswordKey;
            node.ReadWriteCredentials.SNMPV3AuthKeyIsPwd = this.RWAuthKeyIsPwd;
            node.ReadWriteCredentials.SNMPV3PrivKeyIsPwd = this.RWPrivKeyIsPwd;
        }
        else
        {
            // If node is not SNMP v3 then credentials should be empty. This allows user to erase old credentials in DB 
            // when SNMP version is changed.
            node.ReadOnlyCredentials.SNMPv3AuthType = SNMPv3AuthType.None;
            node.ReadOnlyCredentials.SNMPv3PrivacyType = SNMPv3PrivacyType.None;
            node.ReadOnlyCredentials.SnmpV3Context = String.Empty;
            node.ReadOnlyCredentials.SNMPv3UserName = String.Empty;
            node.ReadOnlyCredentials.SNMPv3AuthPassword = String.Empty;
            node.ReadOnlyCredentials.SNMPv3PrivacyPassword = String.Empty;
            node.ReadWriteCredentials.SNMPv3AuthType = SNMPv3AuthType.None;
            node.ReadWriteCredentials.SNMPv3PrivacyType = SNMPv3PrivacyType.None;
            node.ReadWriteCredentials.SnmpV3Context = String.Empty;
            node.ReadWriteCredentials.SNMPv3UserName = String.Empty;
            node.ReadWriteCredentials.SNMPv3AuthPassword = String.Empty;
            node.ReadWriteCredentials.SNMPv3PrivacyPassword = String.Empty;

            node.ReadOnlyCredentials.SNMPV3PrivKeyIsPwd = true;
            node.ReadOnlyCredentials.SNMPV3AuthKeyIsPwd = true;
            node.ReadWriteCredentials.SNMPV3PrivKeyIsPwd = true;
            node.ReadWriteCredentials.SNMPV3AuthKeyIsPwd = true;
        }
    }
    
    public void UpdateSnmpV3Credentials(Node node)
    {
        var credentials = GetRoSnmpV3Credentials(node);
        var uri = string.Format("swis://./Orion/Orion.Nodes/NodeID={0}/SNMPv3Credentials", node.ID);

        using (var context = SwisContextFactory.CreateSystemContext())
        {
            context.Proxy.Update(uri, GetCredentialsParameters(credentials));
            var rwCredentials = GetRwSnmpV3Credentials(node);
            context.Proxy.Update(uri, GetCredentialsParameters(rwCredentials, true));
        }
    }

    private IDictionary<string, object> GetCredentialsParameters(CRED.SnmpCredentialsV3 credentials, bool isRw = false)
    {
        var prefix = isRw ? "RW" : string.Empty;
        return new Dictionary<string, object>()
        {
            {string.Format("{0}Username", prefix), credentials.UserName},
            {string.Format("{0}Context", prefix), credentials.Context},
            {string.Format("{0}PrivacyType", prefix), credentials.PrivacyType.ToString()},
            {string.Format("{0}PrivacyPassword", prefix), credentials.PrivacyPassword},
            {string.Format("{0}PrivacyKeyIsPassword", prefix), credentials.PrivacyKeyIsPassword},
            {string.Format("{0}AuthenticationType", prefix), credentials.AuthenticationType.ToString()},
            {string.Format("{0}AuthenticationPassword", prefix), credentials.AuthenticationPassword},
            {string.Format("{0}AuthenticationKeyIsPassword", prefix), credentials.AuthenticationKeyIsPassword}
        };
    }

    public CRED.SnmpCredentialsV3 GetRoSnmpV3Credentials(Node node)
    {
        var cred = 
         new CRED.SnmpCredentialsV3
        {
            UserName = this.SNMPv3Username,
            AuthenticationKeyIsPassword = AuthKeyIsPwd,
            AuthenticationPassword = this.AuthPasswordKey,
            PrivacyKeyIsPassword = PrivKeyIsPwd,
            PrivacyPassword = this.PrivacyPasswordKey,
            Context = this.SNMPv3Context,
            Name = string.Format("{0} ({1})", this.SNMPv3Username, node.Caption),
            PrivacyType =
                (CRED.SNMPv3PrivacyType)Enum.Parse(typeof(CRED.SNMPv3PrivacyType), this.SNMPv3PrivacyType.ToString()),
            AuthenticationType =
                (CRED.SNMPv3AuthType)Enum.Parse(typeof(CRED.SNMPv3AuthType), this.SNMPv3AuthType.ToString()),
            Description = string.Empty
        };

        if (cred.Name.Length > maxAllowed)
            cred.Name = cred.Name.Substring(0, maxAllowed);

        return cred;
    }

    const int maxAllowed = 255;
    public CRED.SnmpCredentialsV3 GetRwSnmpV3Credentials(Node node)
    {
        var cred = 
         new CRED.SnmpCredentialsV3
        {
            UserName = this.RWSNMPv3Username,
            AuthenticationKeyIsPassword = RWAuthKeyIsPwd,
            AuthenticationPassword = this.RWAuthPasswordKey,
            PrivacyKeyIsPassword = RWPrivKeyIsPwd,
            PrivacyPassword = this.RWPrivacyPasswordKey,
            Context = this.RWSNMPv3Context,
            Name = string.Format("{0} ({1})",this.RWSNMPv3Username, node.Caption),
            PrivacyType =
                (CRED.SNMPv3PrivacyType)
                    Enum.Parse(typeof(CRED.SNMPv3PrivacyType), this.RWSNMPv3PrivacyType.ToString()),
            AuthenticationType =
                (CRED.SNMPv3AuthType)Enum.Parse(typeof(CRED.SNMPv3AuthType), this.RWSNMPv3AuthType.ToString()),
            Description = string.Empty
        };

        if (cred.Name.Length > maxAllowed)
            cred.Name = cred.Name.Substring(0, maxAllowed);
        return cred;
    }

    public void LoadSNMPPropertiesFromSession()
    {
        LoadSNMPPropertiesFromSession(true);
    }

    public void LoadSNMPPropertiesFromSession(bool withoutEventBubling)
	{
        bool disableEventBubblingInitialValue = this.disableEventBubbling;
        if (withoutEventBubling) this.disableEventBubbling = true;

		Node node = NodeWorkflowHelper.Node;

		if (node == null || node.SNMPPort == 0)
			return;

		this.Ip = node.IpAddress;       

		this.SNMPPort = node.SNMPPort;
		this.Allow64bitCounters = node.Allow64BitCounters;
		this.SNMPVersion = node.SNMPVersion;

		this.CommunityString = node.ReadOnlyCredentials.CommunityString;
		this.RWCommunityString = node.ReadWriteCredentials.CommunityString;



		if (node.SNMPVersion == SNMPVersion.SNMP3)
		{
			this.SNMPv3AuthType = node.ReadOnlyCredentials.SNMPv3AuthType;
			this.SNMPv3PrivacyType = node.ReadOnlyCredentials.SNMPv3PrivacyType;
			this.SNMPv3Context = node.ReadOnlyCredentials.SnmpV3Context;
			this.SNMPv3Username = node.ReadOnlyCredentials.SNMPv3UserName;
			this.AuthPasswordKey = node.ReadOnlyCredentials.SNMPv3AuthPassword;
			this.AuthKeyIsPwd = (!string.IsNullOrEmpty(node.ReadOnlyCredentials.SNMPv3AuthPassword)) ?
				node.ReadOnlyCredentials.SNMPV3AuthKeyIsPwd : true;
			this.PrivacyPasswordKey = node.ReadOnlyCredentials.SNMPv3PrivacyPassword;
			this.PrivKeyIsPwd = (!string.IsNullOrEmpty(node.ReadOnlyCredentials.SNMPv3PrivacyPassword)) ?
				node.ReadOnlyCredentials.SNMPV3PrivKeyIsPwd : true;

			this.RWSNMPv3AuthType = node.ReadWriteCredentials.SNMPv3AuthType;
			this.RWSNMPv3PrivacyType = node.ReadWriteCredentials.SNMPv3PrivacyType;
			this.RWSNMPv3Context = node.ReadWriteCredentials.SnmpV3Context;
			this.RWSNMPv3Username = node.ReadWriteCredentials.SNMPv3UserName;
			this.RWAuthPasswordKey = node.ReadWriteCredentials.SNMPv3AuthPassword;
			this.RWAuthKeyIsPwd = (!string.IsNullOrEmpty(node.ReadWriteCredentials.SNMPv3AuthPassword)) ?
				node.ReadWriteCredentials.SNMPV3AuthKeyIsPwd : true;
			this.RWPrivacyPasswordKey = node.ReadWriteCredentials.SNMPv3PrivacyPassword;
			this.RWPrivKeyIsPwd = (!string.IsNullOrEmpty(node.ReadWriteCredentials.SNMPv3PrivacyPassword)) ?
				node.ReadWriteCredentials.SNMPV3PrivKeyIsPwd : true;
		}

        if (withoutEventBubling) this.disableEventBubbling = disableEventBubblingInitialValue;
	}

	public void SetupSNMPv3CredentialsFromSession()
	{
		if (Session["SNMPv3SNMPv3CredAvailable"] != null)
		{
			SNMPv3Cred.Username = Session["SNMPv3Username"].ToString();
			SNMPv3Cred.SNMPv3Context = Session["SNMPv3Context"].ToString();
			SNMPv3Cred.SNMPv3AuthType = (Session["SNMPv3AuthType"] != null) ?
				(SNMPv3AuthType)Session["SNMPv3AuthType"] : SNMPv3AuthType.None;
			SNMPv3Cred.AuthPasswordKey = Session["AuthPasswordKey"].ToString();
			SNMPv3Cred.SNMPv3PrivacyType = (Session["SNMPv3PrivacyType"] != null) ?
				(SNMPv3PrivacyType)Session["SNMPv3PrivacyType"] : SNMPv3PrivacyType.None;
			SNMPv3Cred.PrivacyPasswordKey = Session["PrivacyPasswordKey"].ToString();
			SNMPv3Cred.AuthKeyIsPwd = (Session["AuthKeyIsPwd"] != null) ? (bool)Session["AuthKeyIsPwd"] : true;
			SNMPv3Cred.PrivKeyIsPwd = (Session["PrivKeyIsPwd"] != null) ? (bool)Session["PrivKeyIsPwd"] : true;

			RWSNMPv3Cred.Username = Session["RWSNMPv3Username"].ToString();
			RWSNMPv3Cred.SNMPv3Context = Session["RWSNMPv3Context"].ToString();
			RWSNMPv3Cred.SNMPv3AuthType = (Session["RWSNMPv3AuthType"] != null) ?
				(SNMPv3AuthType)Session["RWSNMPv3AuthType"] : SNMPv3AuthType.None;
			RWSNMPv3Cred.AuthPasswordKey = Session["RWAuthPasswordKey"].ToString();
			RWSNMPv3Cred.SNMPv3PrivacyType = (Session["RWSNMPv3PrivacyType"] != null) ?
				(SNMPv3PrivacyType)Session["RWSNMPv3PrivacyType"] : SNMPv3PrivacyType.None;
			RWSNMPv3Cred.PrivacyPasswordKey = Session["RWPrivacyPasswordKey"].ToString();
			RWSNMPv3Cred.AuthKeyIsPwd = (Session["RWAuthKeyIsPwd"] != null) ? (bool)Session["RWAuthKeyIsPwd"] : true;
			RWSNMPv3Cred.PrivKeyIsPwd = (Session["RWPrivKeyIsPwd"] != null) ? (bool)Session["RWPrivKeyIsPwd"] : true;
		}
	}

	public void SaveSNMPv3CredentialsToSession()
	{
		Session.Add("SNMPv3SNMPv3CredAvailable", true);
		Session.Add("SNMPv3Username", SNMPv3Cred.Username);
		Session.Add("SNMPv3Context", SNMPv3Cred.SNMPv3Context);
		Session.Add("SNMPv3AuthType", SNMPv3Cred.SNMPv3AuthType);
		Session.Add("AuthPasswordKey", SNMPv3Cred.AuthPasswordKey);
		Session.Add("AuthKeyIsPwd", SNMPv3Cred.AuthKeyIsPwd);
		Session.Add("SNMPv3PrivacyType", SNMPv3Cred.SNMPv3PrivacyType);
		Session.Add("PrivacyPasswordKey", SNMPv3Cred.PrivacyPasswordKey);
		Session.Add("PrivKeyIsPwd", SNMPv3Cred.PrivKeyIsPwd);

		Session.Add("RWSNMPv3Username", RWSNMPv3Cred.Username);
		Session.Add("RWSNMPv3Context", RWSNMPv3Cred.SNMPv3Context);
		Session.Add("RWSNMPv3AuthType", RWSNMPv3Cred.SNMPv3AuthType);
		Session.Add("RWAuthPasswordKey", RWSNMPv3Cred.AuthPasswordKey);
		Session.Add("RWAuthKeyIsPwd", RWSNMPv3Cred.AuthKeyIsPwd);
		Session.Add("RWSNMPv3PrivacyType", RWSNMPv3Cred.SNMPv3PrivacyType);
		Session.Add("RWPrivacyPasswordKey", RWSNMPv3Cred.PrivacyPasswordKey);
		Session.Add("RWPrivKeyIsPwd", RWSNMPv3Cred.PrivKeyIsPwd);

	}

    public void SetControlFromSNMP3Node(Node node)
    {
        this.SNMPv3AuthType = node.ReadOnlyCredentials.SNMPv3AuthType;
        this.SNMPv3PrivacyType = node.ReadOnlyCredentials.SNMPv3PrivacyType;
        this.SNMPv3Context = node.ReadOnlyCredentials.SnmpV3Context;
        this.SNMPv3Username = node.ReadOnlyCredentials.SNMPv3UserName;
        this.AuthPasswordKey = node.ReadOnlyCredentials.SNMPv3AuthPassword;
		this.AuthKeyIsPwd = (!string.IsNullOrEmpty(node.ReadOnlyCredentials.SNMPv3AuthPassword)) ? 
			node.ReadOnlyCredentials.SNMPV3AuthKeyIsPwd : true;
        this.PrivacyPasswordKey = node.ReadOnlyCredentials.SNMPv3PrivacyPassword;
		this.PrivKeyIsPwd = (!string.IsNullOrEmpty(node.ReadOnlyCredentials.SNMPv3PrivacyPassword)) ? 
			node.ReadOnlyCredentials.SNMPV3PrivKeyIsPwd : true;

        this.RWSNMPv3AuthType = node.ReadWriteCredentials.SNMPv3AuthType;
        this.RWSNMPv3PrivacyType = node.ReadWriteCredentials.SNMPv3PrivacyType;
        this.RWSNMPv3Context = node.ReadWriteCredentials.SnmpV3Context;
        this.RWSNMPv3Username = node.ReadWriteCredentials.SNMPv3UserName;
        this.RWAuthPasswordKey = node.ReadWriteCredentials.SNMPv3AuthPassword;
		this.RWAuthKeyIsPwd = (!string.IsNullOrEmpty(node.ReadWriteCredentials.SNMPv3AuthPassword)) ?
			node.ReadWriteCredentials.SNMPV3AuthKeyIsPwd : true;
        this.RWPrivacyPasswordKey = node.ReadWriteCredentials.SNMPv3PrivacyPassword;
		this.RWPrivKeyIsPwd = (!string.IsNullOrEmpty(node.ReadWriteCredentials.SNMPv3PrivacyPassword)) ?
			node.ReadWriteCredentials.SNMPV3PrivKeyIsPwd : true;
    }
}
