﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common.Indications;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Nodes_Controls_WebBrowseControl : System.Web.UI.UserControl, INodePropertyPlugin
{
    private static readonly Log log = new Log();
    private IList<Node> nodes;
    private const string WebBrowseSettingName = "Core.WebBrowseTemplate";
    public void Initialize(IList<Node> nodes, NodePropertyPluginExecutionMode mode, Dictionary<string, object> pluginState)
    {
        this.nodes = nodes;

        if (!IsPostBack)
        {
            var webBrowse = this.nodes.First().WebBrowseTemplate;
            txtWebBrowse.Text = string.IsNullOrEmpty(webBrowse) ? CoreConstants.WebBrowseTemplate : HttpUtility.HtmlDecode(webBrowse);
        }
    }

    public bool Validate()
    {
        return true;
    }

    public bool Update()
    {
        var webBrowse = string.IsNullOrEmpty(txtWebBrowse.Text) ? CoreConstants.WebBrowseTemplate : HttpUtility.HtmlEncode(txtWebBrowse.Text);
        foreach (var node in this.nodes)
        {
            var nodeTmp = node;

            if (nodeTmp.Id == 0)
            {
                nodeTmp = NodeWorkflowHelper.Node;
            }

            if (nodeTmp.Id > 0)
            {
                try
                {
                    ReportIndicationLocalData(node);
                }
                catch (Exception ex)
                {
                    log.Error("Error publish report indication", ex);
                }
                nodeTmp.WebBrowseTemplate = webBrowse;
                NodeSettingsDAL.DeleteSpecificSettingForNode(nodeTmp.ID, WebBrowseSettingName);
                NodeSettingsDAL.InsertNodeSetting(nodeTmp.ID, WebBrowseSettingName, webBrowse);
            }
        }
        return true;
    }

    private void ReportIndicationLocalData(Node node)
    {
        var webBrowse = string.IsNullOrEmpty(txtWebBrowse.Text) ? CoreConstants.WebBrowseTemplate : HttpUtility.HtmlEncode(txtWebBrowse.Text);

        var originNodeProperties = new PropertyBag
        {
            {"NodeId", node.ID},
            {"IP_Address", node.IpAddress},
            {"Caption", node.Caption},
            {"Status", node.Status},
            {"WebBrowseTemplate", node.WebBrowseTemplate}
        };

        var currentNodeProperties = new PropertyBag
        {
            {"NodeId", node.ID},
            {"IP_Address", node.IpAddress},
            {"Caption", node.Caption},
            {"Status", node.Status},
            {"WebBrowseTemplate", webBrowse}
        };

        var indication = new NodeIndication(IndicationType.System_InstanceModified,
            currentNodeProperties, originNodeProperties);

        var publisher = IndicationPublisher.CreateV3();
        if (publisher != null)
        {
            publisher.ReportIndication(indication);
        }
    }
}