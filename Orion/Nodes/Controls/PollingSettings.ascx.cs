using System;
using System.Data;
using System.Web.UI;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_Nodes_Controls_NodePolling : System.Web.UI.UserControl
{
	protected bool _showPollingEngineDetails;

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		changePollEngineLink.Visible = EnginesDAL.GetEnginesCount() > 1;
		Page.ClientScript.RegisterStartupScript(this.GetType(), "initChangeEngineDialog", "Sys.WebForms.PageRequestManager.getInstance().add_endRequest(SW.Orion.ChangeEngineDialog.init); ", true);
	}

    public event EventHandler EngineChanged;

	protected override void OnLoad(EventArgs e)
	{
		base.OnLoad(e);
		if (EngineID > 0)
		{
            DataTable data = EnginesDAL.GetEngineById(EngineID);
			if (data != null && data.Rows.Count > 0)
			{
			    object keepAlive = data.Rows[0]["KeepAlive"];
                
                pollEngineDescr.Controls.Clear();
				pollEngineDescr.Controls.Add(new LiteralControl(
					String.Format("<a href=\"/Orion/Admin/Details/Engines.aspx\"><img src=\"/NetPerfMon/images/{0}\"/>&nbsp;<span style=\"vertical-align: top;\">{1} ({2})</span></a>",
						CommonWebHelper.GetEngineStatusImage(Convert.ToDateTime(keepAlive is DBNull ? null : keepAlive)), data.Rows[0]["ServerName"], ResourceLookup("ServerType", data.Rows[0]["ServerType"].ToString()))));
			}
		}
		changePollEngineLink.Visible = !HideChangePollingEngineButton;
	}

	private string ResourceLookup(string prefix, string value)
	{
		const string managerid = "Core.Strings";
		var manager = ResourceManagerRegistrar.Instance;
		var key = manager.CleanResxKey(prefix, value.Trim());
		var result = manager.SearchAll(key, new[] { managerid });
		return String.IsNullOrEmpty(result) ? value : result;
	}

	[PersistenceMode(PersistenceMode.Attribute)]
	public bool ShowPollingEngineDetails
	{
		get { return _showPollingEngineDetails; }
		set { _showPollingEngineDetails = value; }
	}

	[PersistenceMode(PersistenceMode.Attribute)]
	public bool HideChangePollingEngineButton { get; set; }

	public bool MultipleSelection
	{
		get { return cbMultipleSelection.Visible; }
		set { cbMultipleSelection.Visible = value; }
	}

	public bool MultipleSelection_Selected
	{
		get { return cbMultipleSelection.Checked; }
		set
		{
			cbMultipleSelection.Checked = value;
			cbMultipleSelection_CheckedChanged(null, null);
		}
	}

	public string StatusPollingText
	{
		set { lblStatusPolling.Text = value; }
	}

	public string CollectStatEveryText
	{
		set { lblCollectStatEvery.Text = value; }
	}

	public int NodeStatusPolling
	{
		get { return int.Parse(tbNodeStatusPolling.Text); }
		set { tbNodeStatusPolling.Text = value.ToString(); }
	}

	public int EngineID
	{
        get {
            int ret;
            if (int.TryParse(hfPollingEngineId.Value, out ret))
                return ret;
            return -1;
        }
		set { hfPollingEngineId.Value = value.ToString(); }
	}

    public int[] ObjectEngineIDs {
        get
        {
            if (ViewState["ObjectEngineIDs" + ClientID] == null)
            {
                ViewState["ObjectEngineIDs" + ClientID] = new int[0];
            }
            return (int[])ViewState["ObjectEngineIDs" + ClientID];
        }
        set { ViewState["ObjectEngineIDs" + ClientID] = value; }
    }

	public int CollectStatisticsEvery
	{
		get { return int.Parse(tbCollectSatisticsEvery.Text); }
		set { tbCollectSatisticsEvery.Text = value.ToString(); }
	}
	
	[Obsolete("Topology componetization cleanup, moved to Topology package", false)]
	[PersistenceMode(PersistenceMode.Attribute)]
	public bool HideTopologySection
	{
		get;set;
	}

	protected void cbMultipleSelection_CheckedChanged(object sender, EventArgs e)
	{
		tbNodeStatusPolling.Enabled = cbMultipleSelection.Checked;
		tbCollectSatisticsEvery.Enabled = cbMultipleSelection.Checked;

		if (!tbNodeStatusPolling.Enabled)
			tbNodeStatusPolling.CssClass = "disabled";
		else
			tbNodeStatusPolling.CssClass = string.Empty;

		if (!tbCollectSatisticsEvery.Enabled)
			tbCollectSatisticsEvery.CssClass = "disabled";
		else
			tbCollectSatisticsEvery.CssClass = string.Empty;

		
		if (!cbMultipleSelection.Checked)
			ChangePollingEngineBlock.Attributes.Add("class", "rightInputColumn Disabled");
		else
			ChangePollingEngineBlock.Attributes.Add("class", "rightInputColumn");
	}

    protected void hfPollingEngineId_OnValueChanged(object sender, EventArgs e)
    {
        EngineChanged?.Invoke(sender, e);
    }
}
