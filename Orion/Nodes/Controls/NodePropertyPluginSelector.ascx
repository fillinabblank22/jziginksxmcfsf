<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodePropertyPluginSelector.ascx.cs" Inherits="Orion_Nodes_Controls_NodePropertyPluginSelector" %>
<%@ Register Src="~/Orion/Nodes/Controls/AdditionalPollingOptionsOrcInfo.ascx" TagPrefix="orion" TagName="AdditionalPollingOptionsOrcInfo" %>

    <asp:Repeater runat = "server" ID="repNodePropertyPluginSelector" OnItemDataBound="repNodePropertyPluginSelector_ItemDataBound">
        <HeaderTemplate>
            <tr>
                <td class="leftLabelColumn" style="vertical-align:top;padding-top: 7px">
                    <asp:Literal ID="Literal1" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_YK0_1 %>" />
                 </td>
                 <td>
                <table>
        </HeaderTemplate>
        <ItemTemplate >
            <tr>
                <td class="rightInputColumn">
                    <asp:CheckBox runat="server" ID="chbxPluginVisible" AutoPostBack="true" OnCheckedChanged="chbxPluginVisible_CheckedChanged"
                        Text=""></asp:CheckBox>
                    <br />
                    <span class="helpfulText">
                        <asp:Literal runat="server" ID="pluginHint" Text =""></asp:Literal>
                    </span>
                </td>
                <%--<td class="helpfulText">
                    <asp:Literal runat="server" ID="pluginHint" Text =""></asp:Literal>
                </td>--%>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
            </td>
            </tr>
        </FooterTemplate>
    </asp:Repeater>

    <div runat="server" ID="orcInfoDiv" Visible="False">
        <table>
            <tr>
                <td class="leftLabelColumn" style="vertical-align:top;padding-top: 7px">
                </td>
                <td>
                    <table>
                        <tr>
                            <td class="rightInputColumn">
                                <div style="width:650px; margin-bottom:15px;">
                                    <orion:AdditionalPollingOptionsOrcInfo runat="server"/>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>