<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SnmpInfo.ascx.cs" Inherits="Orion_Nodes_Controls_NodeSNMP" %>
<%@ Register Src="~/Orion/Nodes/Controls/NodeSNMPv3Credentials.ascx" TagPrefix="orion"
    TagName="SNMPv3Cred" %>
<style type="text/css">
        .checkbox label{
            padding-left:5px;
        }
    </style>

   
         
<table class="blueBox">
    <tr>
        <td class="leftLabelColumn">
            <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_50 %>" /></td>
        <td class="rightInputColumn" colspan="2">
			<asp:DropDownList ID="ddlSNMPVersion" runat="server" AutoPostBack="True" OnSelectedIndexChanged="SNMPSettingsChanged">
                <asp:ListItem Value="SNMPv1" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_72 %>"></asp:ListItem>
                <asp:ListItem Selected="True" Value="SNMPv2c" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_73 %>"></asp:ListItem>
                <asp:ListItem Value="SNMPv3" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_74 %>"></asp:ListItem>
            </asp:DropDownList>
            &nbsp;
            <%if (((this.Profile.AllowAdmin) || (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)) && this.ddlSNMPVersion.SelectedValue.Equals("SNMPv3"))
                      {%>
                                <span>
                                    <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="" />
                                    <a href="/Orion/Admin/Credentials/SNMPCredentialManager.aspx" id="manage" target="_blank" class="RenewalsLink">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_SO0_53) %> </a> 
                                        </span>
                                         <%} %>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td colspan="2"><span class="helpfulText"><asp:Literal ID="ltrlSNMPVersionDescr" runat="server"></asp:Literal></span></td>
    </tr>
    <tr>
        <td class="leftLabelColumn">
            <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_51 %>" /></td>
        <td class="rightInputColumn">
            <asp:TextBox OnTextChanged="SNMPSettingsChanged" ID="tbSNMPPort" runat="server" Text="161" AutoPostBack="True"></asp:TextBox></td>
			<td>&nbsp;</td>
    </tr>
    <tr>
        <td class="leftLabelColumn">
            &nbsp;</td>
        <td class="rightInputColumn" style="width: 170px;">
            <asp:CheckBox OnTextChanged="SNMPSettingsChanged" ID="cbAllow64bitCounters" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_61 %>" Checked="true" CssClass="checkbox" AutoPostBack="True" /></td>
		<td>&nbsp;</td>
    </tr>
</table>
<div runat="server" id="pnlSNMPV1_2">
    <table class="blueBox">
        <tr>
            <td class="leftLabelColumn">
                <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_52 %>" /></td>
            <td class="rightInputColumn" style="width:100px;">
                <asp:TextBox OnTextChanged="SNMPSettingsChanged" ID="tbCommunityString" runat="server" Text="public" CssClass="CommunityStringTextBox disposable" AutoPostBack="True"></asp:TextBox>
            </td>
			<td class="helpfulText"><asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_53 %>" /></td>
        </tr>
        <tr runat="server" id="trRWCommunityString">
            <td class="leftLabelColumn">
                <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_54 %>" /></td>
            <td class="rightInputColumn">
                <asp:TextBox OnTextChanged="SNMPSettingsChanged" ID="tbRWCommunityString" runat="server" TextMode="Password" AutoPostBack="True" autocomplete="off"></asp:TextBox></td>
			<td>&nbsp;</td>
        </tr>
    </table>
</div>
<div runat="server" id="pnlSNMPV3" visible="false">
    <orion:SNMPv3Cred runat="server" ID="SNMPv3Cred" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_62 %>" />
    <br />
    <orion:SNMPv3Cred runat="server" ID="RWSNMPv3Cred" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_63 %>" />
</div>
<asp:UpdatePanel runat="server" ID="snmpUpdatePanel" UpdateMode="Conditional">
    <ContentTemplate>
        <div runat="server" id="SNMPValidationTrue" style="padding-left: 118px; background-color: #D6F6C6;
            text-align: center" visible="false">
            <img runat="server" id="SNMPValidationTrueIMG" src="~/Orion/images/nodemgmt_art/icons/icon_OK.gif" />
            <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_55 %>" />
        </div>
        <div runat="server" id="SNMPValidationFalse" style="padding-left: 118px; background-color: #FEEE90;
            text-align: center; color: Red" visible="false">
            <img runat="server" id="SNMPValidationFalseIMG" src="~/Orion/images/nodemgmt_art/icons/icon_warning.gif" />
            <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_56 %>" /> <asp:Label runat="server" ID="SNMPValidationFailedMessage" />
        </div>
        <table>
            <tr>
                <td class="leftLabelColumn">
                    &nbsp;</td>
                <td style="white-space:nowrap;">
                    <orion:LocalizableButton ID="imbtnValidateSNMP" runat="server" LocalizedText="Test" OnClick="imbtnValidateSNMP_Click" DisplayType="Small" />
                </td>
                <td>
                    <asp:UpdateProgress runat="server" ID="snmpTestUpdateProgress" DynamicLayout="true" DisplayAfter="0" AssociatedUpdatePanelID="snmpUpdatePanel" style="margin-left: 10px">
                        <ProgressTemplate>
                                &nbsp;<img src="/Orion/images/animated_loading_sm3_whbg.gif" />
                                <span style="font-weight:bold;"><asp:Literal ID="Literal1" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_8 %>" /></span>                  
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>