﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_Nodes_Controls_DependecyRepeater : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void BindData(DataTable sourceData)
    {        
        DependencyTable.DataSource = sourceData;
        DependencyTable.DataBind();
    }
}