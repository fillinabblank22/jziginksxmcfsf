﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Enums;

public partial class Orion_Nodes_Controls_NodeCategory : System.Web.UI.UserControl
{
    private static ListItem autoListItem;
    private static ListItem variousListItem;    

    public bool MultipleSelection
    {
        get
        {
            return cbMultipleSelection.Visible;
        }
        set
        {
            cbMultipleSelection.Visible = value;
        }        
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool Various
    {
        get
        {
            bool value;
            if (Boolean.TryParse(hfVarious.Value, out value))
                return value;

            return false;
        }
        set
        {
            hfVarious.Value = value.ToString();
        }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public NodeCategory? CustomCategory
    {        
        get
        {
            NodeCategory result;
            if (Enum.TryParse(hfCustomCategory.Value, out result))
                return result;

            return null;
        }
        set
        {
            if (value.HasValue)
                hfCustomCategory.Value = Enum.GetName(typeof(NodeCategory), value);
            else
                hfCustomCategory.Value = String.Empty;
        }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public NodeCategory? Category 
    {
        get 
        { 
            NodeCategory result;
            if (Enum.TryParse(hfCategory.Value, out result))
                return result;

            return null;
        }
        set
        {
            if (value.HasValue)
                hfCategory.Value = Enum.GetName(typeof(NodeCategory), value);
            else
                hfCategory.Value = String.Empty;
        }
    }

    public NodeCategory? ResultCategory
    {
        get 
        {
            if (dlNodeCategory.SelectedValue == autoListItem.Value) return null;

            NodeCategory result;
            if (Enum.TryParse(dlNodeCategory.SelectedValue, out result))
                return result;

            return null;
        }
    }

    public bool MultipleSelection_Checked 
    {
        get 
        {
            return cbMultipleSelection.Checked;
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!IsPostBack && dlNodeCategory.Items.Count == 0)
        {
            autoListItem = new ListItem(Resources.CoreWebContent.WEBDATA_OF0_04, "auto");
            variousListItem = new ListItem(Resources.CoreWebContent.WEBDATA_OF0_03, "various");
            
            // Add standart enum values
            foreach (NodeCategory category in Enum.GetValues(typeof(NodeCategory)))
            {
                var item = new ListItem(NodeCategoryHelper.GetLocalizedName(category), Enum.GetName(typeof(NodeCategory), category));
                dlNodeCategory.Items.Add(item);
            }

            if (Category.HasValue)
            {
                // Add aditional info about detected value
                autoListItem.Text += String.Format(" ({0})", NodeCategoryHelper.GetLocalizedName(Category.Value));
            }

            // Add autodetect option
            dlNodeCategory.Items.Add(autoListItem);
            dlNodeCategory.SelectedValue = CustomCategory.HasValue ? Enum.GetName(typeof(NodeCategory), CustomCategory) : autoListItem.Value;

            if (MultipleSelection)
            {
                // Disable dropdown initialy in multi-edit
                dlNodeCategory.Enabled = MultipleSelection_Checked;

                if (Various)
                {
                    // Add various item if values varies
                    dlNodeCategory.Items.Add(variousListItem);
                    dlNodeCategory.SelectedValue = variousListItem.Value;
                }
            }
        }
    }
}