<%@ Control Language="C#" ClassName="NodesHelpButton" %>
<%@ Register Src="~/Orion/Controls/HelpButton.ascx" TagPrefix="orion" TagName="HelpButton" %>

<script runat="server">
	public string HelpUrlFragment
	{
		get { return OrionHelpButton.HelpUrlFragment; }
		set { OrionHelpButton.HelpUrlFragment = value; }
	}
</script>

<span style="float:right;">
	<orion:HelpButton runat="server" ID="OrionHelpButton" />
</span>
