<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeNote.ascx.cs" Inherits="Orion_Nodes_Controls_NodeNote" %>
    <div class="contentBlock">
    <div runat="server" id="CustomPropertiesHeader">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td class="contentBlockHeader">
                    <asp:Literal ID="Literal1" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, Resource_ActiveAlerts_Notes%>" />
                </td>
            </tr>
        </table>
    </div>
    <table class="blueBox">
        <tr>
	    <td>
	    </td>
        <td class="rightInputColumn" style="border: none; padding: 5px;">
                 <asp:TextBox ID="NodeNoteContent" ClientIDMode="Static" runat="server" TextMode="MultiLine" ReadOnly="false" Rows="5"  style="resize: none; width: 500px;"></asp:TextBox>
            </td>
        </tr>
    </table>
</div>
