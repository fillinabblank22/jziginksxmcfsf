<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WmiInfo.ascx.cs" Inherits="Orion_Nodes_Controls_WmiInfo" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<div class="contentBlock">  
    <div class="blueBox" id="credentialsTable" runat="server">
        <table>
            <tr>
	            <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="lblTemplateLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_234) %>&nbsp;</asp:Label>
                </td>
	            <td class="rightInputColumn" colspan="2">
                    <asp:DropDownList ID="DdlWmiCredentials" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DdlWmiCredentials_IndexChanged" >
                    </asp:DropDownList>
                    &nbsp;
                    <%if ((this.Profile.AllowAdmin) || (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer))
                      {%>
                                <span>
                                    <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="" />
                                    <a href="/Orion/Admin/Credentials/CredentialManager.aspx" id="manage" target="_blank" class="RenewalsLink">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_312) %> </a> 
                                        </span>
                                         <%} %>
	            </td>
                </tr>
            <tr>
	            <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label1"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_235) %>&nbsp;</asp:Label>
                </td>
	            <td class="rightInputColumn">
                    <asp:TextBox ID="tbCaption" runat="server" MaxLength=50 ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredCredentialNameValidator" runat="server" Display="Dynamic"  ErrorMessage="RequiredFieldValidator" ControlToValidate="tbCaption" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_237 %>"></asp:RequiredFieldValidator>
	            </td>
	            <td>&nbsp;</td>
            </tr>
            <tr>
	            <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label3"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AK0_165) %>&nbsp;</asp:Label>
                </td>
	            <td class="modifiedLeftLabelColumn pollingSelector">
                    <asp:TextBox ID="tbLogin" runat="server" MaxLength=50></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredUserNameValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" ControlToValidate="tbLogin" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_238 %>" ></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td></td>
                <td class="modifiedLeftLabelColumn pollingSelector">
                    <div class="smallText">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_522) %></div>
                    <div id="EditCredentialHelp">
                        <span class="LinkArrow">&#0187;</span>
                        <orion:HelpLink ID="IPSLADiscoveryHelpLink" runat="server" HelpUrlFragment="OrionPHWhatPrivilegesNeed"
                            HelpDescription="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_516 %>" CssClass="helpLink" />
                    </div>
	            </td>	            
            </tr>
            <tr>
	            <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label5"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_40) %>&nbsp;</asp:Label>
                </td>
	            <td class="rightInputColumn">
                    <asp:TextBox ID="tbPassword" runat="server" TextMode="Password" MaxLength="127" autocomplete="off"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredPasswordValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" ControlToValidate="tbPassword" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_239 %>"></asp:RequiredFieldValidator>
	            </td>
	            <td>&nbsp;</td>
            </tr>
            <tr>
	            <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label7"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_236) %>&nbsp;</asp:Label>
                </td>
	            <td class="rightInputColumn">
                    <asp:TextBox ID="tbPasswordConfirmation" runat="server" TextMode="Password" MaxLength="127" autocomplete="off"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredConfirmPasswordValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" ControlToValidate="tbPasswordConfirmation" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_239 %>"></asp:RequiredFieldValidator>
                    <asp:CompareValidator
                        ID="CompareConfirmPasswordValidator" runat="server" ErrorMessage="CompareConfirmPasswordValidator" ControlToCompare="tbPassword" Display="Dynamic" ControlToValidate="tbPasswordConfirmation" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_240 %>"></asp:CompareValidator>
	            </td>
	            <td>&nbsp;</td>
            </tr>
        </table>
        <div runat="server" id="VMwareValidationTrue" style="padding-left: 118px; background-color: #D6F6C6;
            text-align: center" visible="false">
            <img runat="server" id="VMwareValidationTrueIMG" src="~/Orion/images/nodemgmt_art/icons/icon_OK.gif" />
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_55) %>
        </div>
        <div runat="server" id="VMwareValidationFalse" style="padding-left: 118px; background-color: #FEEE90;
            text-align: center; color: Red" visible="false">
            <img runat="server" id="VMwareValidationFalseIMG" src="~/Orion/images/nodemgmt_art/icons/icon_warning.gif" />
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_56) %> <asp:Label runat="server" ID="VMwareValidationFailedMessage" />
        </div>
        <asp:UpdatePanel runat="server" ID="wmiUpdatePanel" UpdateMode="Conditional">
            <ContentTemplate>
                <table>
                    <tr>
                        <td class="leftLabelColumn">&nbsp;</td>
                        <td style="white-space:nowrap;">
                            <orion:LocalizableButton ID="imbtnValidateWmi" runat="server" LocalizedText="Test"
                                OnClick="imbtnValidateWmi_Click" DisplayType="Small" />
                        </td>
                        <td>
                            <asp:UpdateProgress runat="server" ID="wmiTestUpdateProgress" DynamicLayout="true" DisplayAfter="0" AssociatedUpdatePanelID="wmiUpdatePanel" style="margin-left: 10px">
                                <ProgressTemplate>
                                    &nbsp;<img src="/Orion/images/animated_loading_sm3_whbg.gif" />
                                    <span style="font-weight: bold;">
                                        <asp:Literal ID="Literal1" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_8 %>" /></span>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="blueBox" id="vmwareNodeTable" runat="server" visible="false">
        <div style="padding-left: 5px; padding-right: 5px;"><asp:Literal ID="litVMwareNodeTableText" runat="server" /></div>
    </div>
</div>
