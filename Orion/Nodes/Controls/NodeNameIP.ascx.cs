using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.Common.Net;


public partial class Orion_Nodes_Controls_NodeNameIP : System.Web.UI.UserControl
{

	public delegate void CheckedChangedHandler(bool val);

	public event CheckedChangedHandler CheckedChanged;

    public event EventHandler IPAddressChanged;

	public void SetOnlyUnPluggableState()
	{
		foreach (HtmlTableRow r in MainTable.Rows)
			if (r.ID != "UnPluggableSection") r.Visible = false;
	}

	public bool MultipleSelection
	{
		get { return cbMultipleSelection.Visible; }
		set
		{ cbMultipleSelection.Visible = value; }
	}

	public bool MultipleSelection_Selected
	{
		get { return cbMultipleSelection.Checked; }
		set
		{
			cbMultipleSelection.Checked = value;
			cbMultipleSelection_CheckedChanged(null, null);
		}
	}

	public bool DynamicIP
	{
		get { return cbDynamicIP.Checked; }
		set
		{
			cbDynamicIP.Checked = value;
			tbIP.Enabled = !value;
		}
	}

	public string Name
	{
		get { return tbName.Text; }
		set { tbName.Text = value; }
	}

	public string IP
	{
		get { return tbIP.Text; }
		set { tbIP.Text = value; }
	}

    public int NodeID { get; set; }

    protected string CustomSWISQuery
    {
        // String needs to be on one line because of JS
        get { return String.Format(@"SELECT 0 AS _RadioButtonFor_IP_Address2, SUB.[IPAddressType], SUB.[IP_Address2] FROM (SELECT IP_Address AS [IP_Address2], IPAddressType, 1 AS IPOrder FROM ORION.Nodes WHERE NodeID = {0} UNION                             (SELECT ni.IPAddress AS [IP_Address2], ni.IPAddressType, 2 AS IPOrder FROM ORION.NodeIPAddresses ni JOIN ORION.Nodes n ON ni.NodeID = n.NodeID AND n.IP_Address != ni.IPAddress WHERE ni.NodeID = {0})) SUB ORDER BY SUB.[IPOrder]", 
            NodeID);
        }
    }


	protected void cbMultipleSelection_CheckedChanged(object sender, EventArgs e)
	{
		tbName.Enabled = cbMultipleSelection.Checked;
		tbIP.Enabled = (cbMultipleSelection.Checked && !DynamicIP);
		cbDynamicIP.Enabled = cbMultipleSelection.Checked;
	}

	protected void DynamicIP_CheckedChanged(object sender, EventArgs e)
	{
		DynamicIP = DynamicIP;
		if (CheckedChanged != null)
			CheckedChanged(DynamicIP);
	}

    protected void IPAddress_TextChanged(object sender, EventArgs e)
    {
        if (this.IPAddressChanged != null)
        {
            this.IPAddressChanged(sender, e);
        }
    }

    protected void VMware_CheckedChanged(object sender, EventArgs e)
	{
		DynamicIP = DynamicIP;
	}

	protected void IPAddress_Validate(object source, ServerValidateEventArgs args)
	{
		args.IsValid = false;
		if (HostHelper.IsIpAddress(args.Value))
		{
			if (IPAddress.Parse(args.Value).AddressFamily == AddressFamily.InterNetwork)
				args.IsValid = args.Value.Split('.').Length == 4;
			else
				args.IsValid = true;
		}
	}

    /// <summary>
    /// Setups the text and editability of Hostname textbox and Dynamic IP checkbox.
    /// </summary>
    /// <param name="isEnabled">When set to <code>true</code> the textbox is editable;otherwise textbox is set to read-only mode.</param>
    public void SetEnabledState(bool isEnabled)
    {
        tbIP.Enabled = isEnabled;
        tbIP.ReadOnly = !isEnabled;
        changePollingIPBTN.Visible = isEnabled;

        cbDynamicIP.Enabled = isEnabled;
    }
}
