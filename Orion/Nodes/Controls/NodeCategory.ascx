<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeCategory.ascx.cs" Inherits="Orion_Nodes_Controls_NodeCategory" %>
<table>
    <tr>
        <td class="leftSectionTitle">
            <asp:CheckBox ID="cbMultipleSelection" runat="server"/>
            <asp:Literal ID="Literal1" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_OF0_02 %>" />
        </td>
    </tr>
</table>
<table class="blueBox">    
    <tr>
        <td class="leftLabelColumn">            
            <asp:Literal ID="liOutterTitle" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VT0_10 %>" />
        </td>
        <td class="rightInputColumn">
            <asp:DropDownList ID="dlNodeCategory" runat="server" Width="200px"/>
        </td>
        <td>
        &nbsp;
        </td>
    </tr>    
</table>
<asp:HiddenField ID="hfCategory" runat="server" />
<asp:HiddenField ID="hfCustomCategory" runat="server" />
<asp:HiddenField ID="hfVarious" runat="server" />

<script type="text/javascript">
    var multiEditCheck = document.getElementById('<%= this.cbMultipleSelection.ClientID%>');    
    var categoryList = document.getElementById('<%= this.dlNodeCategory.ClientID%>');

    if(multiEditCheck != null){
        multiEditCheck.onchange = function () {
            // remove various from select options
            for (i = 0; i < categoryList.length; i++) {
                if (categoryList.options[i].value == 'various')
                    categoryList.remove(i);
            }
            categoryList.disabled = !this.checked;
        }
    }
</script>

