using System.Web.UI;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Nodes_Controls_AdditionalPollingOptionsOrcInfo : UserControl
{
    public string OrcHelpLink => HelpHelper.GetHelpUrl("Orion", "orioncoreorcsupport");
}