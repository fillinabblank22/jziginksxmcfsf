﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.ModuleManager;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Models.Credentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;


public partial class Orion_Nodes_Controls_WmiInfo : System.Web.UI.UserControl
{
    [Serializable]
    class UiWmiNode
    {
        internal int NodeID;
        internal string IPAddress;
        internal int EngineID;
    }

    private static readonly SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
    private const string WmiInfoNodesKey = "WmiInfoNodes";
    private const string WmiInfoExecutionModeKey = "WmiInfoExecutionMode";
    private const string asteriskPass = "**********";

    private const int NonExistingWmiCredentialID = 0;    

    private List<UsernamePasswordCredential> wmiCredentialses;

    private IList<UiWmiNode> nodes
    {
        get { return ViewState[WmiInfoNodesKey] as IList<UiWmiNode>; }
        set { ViewState[WmiInfoNodesKey] = value; }
    }

    private NodePropertyPluginExecutionMode mode
    {
        get
        {
            return (NodePropertyPluginExecutionMode)ViewState[WmiInfoExecutionModeKey];
        }
        set { ViewState[WmiInfoExecutionModeKey] = value; }
    }
    private Dictionary<string, object> propertyBag;    

    #region Form values   

    private int CredentialsId
    {
        get
        {
            int credentialId;
            if (!int.TryParse(DdlWmiCredentials.SelectedValue, out credentialId))
                credentialId = NonExistingWmiCredentialID;

            return credentialId;
        }        
    }    

    public string Login
    {
        get { return tbLogin.Text; }
        set { tbLogin.Text = value; }
    }

    public string Password
    {
        get
        {
            return tbPassword.Text==asteriskPass ? PlainPassword : tbPassword.Text;
        }
        set
        {
            EncodePassword(value);
        }
    }

    public string Ip
    {
        get {
			var ip = (string)ViewState[this.ID + "_ip"];
			if (!string.IsNullOrEmpty(ip) && SolarWinds.Common.Net.HostHelper.IsIpAddress(ip)) return ip;
			
			var node = nodes.FirstOrDefault();
            return node != null ? node.IPAddress : String.Empty;
        }
		set { ViewState[this.ID + "_ip"] = value; }
    }  

    public string CredentialName
    {
        get
        {
            return tbCaption.Text;
        }
        set
        {
            tbCaption.Text = value;
        }
    }

    public string PlainPassword
    {
        get
        {
            return NodeWorkflowHelper.GetPageSessionValue("WMI.NodeManagement.PlainPassword")?.ToString();
        }
        set
        {
            NodeWorkflowHelper.SetPageSessionValue("WMI.NodeManagement.PlainPassword", value);
        }
    }

    public bool ChangeForMultipleNodes
    {
        get
        {
            //return cbMultipleSelection.Checked;
            return true;
        }
        //set
        //{
        //    cbMultipleSelection.Checked = value;
        //}
    }    
    

    public bool EditControlsVisible
    {
        get
        {
            return credentialsTable.Visible;
        }
        set
        {
            credentialsTable.Visible = value;
        }
    }

    public bool MultipleSelection
    {
        get { return (bool)ViewState["MultipleSelection"]; }
        set
        {
            ViewState["MultipleSelection"] = value;
            imbtnValidateWmi.Visible = !value;
            imbtnValidateWmi.Enabled = !value;

            if (CredentialsId != NonExistingWmiCredentialID)
            {
                WmiCredentialControlsSetEnabledState(!value);
            }
        }
    }   
    #endregion

    #region Controls events handlers
    protected void DdlWmiCredentials_IndexChanged(object sender, EventArgs e)
    {
        SetCredentialsFromDropDown();
    }

    protected void imbtnValidateWmi_Click(object sender,EventArgs e)
    {
        Validate(false);
    }        
    #endregion

    private IEnumerable<BaseValidator> GetAllValidators()
    {
        return new BaseValidator[]
        {
            this.RequiredCredentialNameValidator,
            this.RequiredUserNameValidator,
            this.RequiredPasswordValidator,
            this.RequiredConfirmPasswordValidator,
            this.CompareConfirmPasswordValidator
        };
    }

    private bool CheckAllValidators()
    {
        return GetAllValidators().All(v => v.IsValid);
    }

    private void ChangeAllValidatorsState(bool enabled)
    {
        foreach (var validator in GetAllValidators())
        {
            validator.Enabled = enabled;
        }
    }

    private bool IsMultipleNodes
    {
        get
        {
            return (nodes.Count > 1);
        }
    }

    /// <summary>
    /// The bool value if we are adding existing VMware host or virtual machine.
    /// </summary>
    private bool ExistingVMwareNode
    {
        get
        {
            if (propertyBag.ContainsKey("VIMExistingVMwareNode"))
                return Convert.ToBoolean(propertyBag["VIMExistingVMwareNode"]);
            else
                return false;
        }
        set
        {
            propertyBag["VIMExistingVMwareNode"] = value;
        }
    }    

    /// <summary>
    /// Set password fields values to asterisks and store plain password to PlainPassword property
    /// </summary>
    private void EncodePassword(string value)
    {        
        string passBlock = GetPassBlock(value);
        this.PlainPassword = value;


        tbPassword.Text =
            tbPasswordConfirmation.Text =
            this.tbPassword.Attributes["value"] =
            this.tbPasswordConfirmation.Attributes["value"] = passBlock;        
    }

    private string GetPassBlock(string value)
    {
        return (!string.IsNullOrEmpty(value)) ? asteriskPass : "";
    }

    /// <summary>
    /// Shows valiadtion result message
    /// </summary>
    private void SetUIValidationState(bool validationSucceeded, string text)
    {
        VMwareValidationTrue.Visible = validationSucceeded;
        VMwareValidationFalse.Visible = !validationSucceeded;
        VMwareValidationFailedMessage.Text = text;
    }

    /// <summary>
    /// Creates new credentails and returns credential object
    /// </summary>
    /// <returns>New credential object</returns>
    private UsernamePasswordCredential CreateNewCredentials(string name, string username, string password)
    {
        UsernamePasswordCredential credential = new UsernamePasswordCredential()
        {
            Name = name,
            Username = username,
            Password = password
        };

        int credID;
        using (var proxy = _blProxyCreator.Create(ex => _log.Error(ex)))
        {
            credID = proxy.InsertWmiCredential(credential,CoreConstants.CoreCredentialOwner).Value;            
        }

        credential.ID = credID;
        return credential;
    }

    /// <summary>
    /// Validate new credentials. 
    /// If credentials are valid, true is returned and existingCredentialId is set to credential ID.
    /// It can be NonExistingWmiCredentialID if credentials are new or it can be an existing credential ID if entered credentials
    /// are the same as existing credentials.
    /// 
    /// If credentails are not valid, false is returned and errorMessage is set.
    /// </summary>
    private bool ValidateNewCredentials(string name, string username, string password, out string errorMessage, out int existingCredentialId)
    {
        errorMessage = string.Empty;
        existingCredentialId = NonExistingWmiCredentialID;

       UsernamePasswordCredential existingCredential = wmiCredentialses.FirstOrDefault(c => c.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase));
        if (existingCredential != null)
        {
            if ((existingCredential.Username == username) && (existingCredential.Password == password))
            {
                existingCredentialId = existingCredential.ID.Value;
                return true;
            }
            else
            {
                errorMessage = Resources.CoreWebContent.WEBCODE_TM0_90;
                return false;
            }
        }
        return true;
    }      

    private void LoadData()
    {
        using (var businessProxy = _blProxyCreator.Create(ex => { _log.Error(ex); }))
        {
            wmiCredentialses = businessProxy.GetSharedWmiCredentials(CoreConstants.CoreCredentialOwner);
            wmiCredentialses.Insert(0, new UsernamePasswordCredential() { Name = Resources.CoreWebContent.WEBCODE_TM0_93, ID = (int)NonExistingWmiCredentialID });

            wmiCredentialses.ForEach(c => DdlWmiCredentials.Items.Add(new ListItem(c.Name,c.ID.Value.ToString())));            
        }                
    }    

    private void SetCredentialsFromDropDown()
    {
        long credentialId = CredentialsId;
        if (credentialId == NonExistingWmiCredentialID)
        {
            this.CredentialName = string.Empty;
            this.Login = string.Empty;
            this.Password = string.Empty;
           
            WmiCredentialControlsSetEnabledState(true);

            ChangeAllValidatorsState(true);

            return;
        }

        UsernamePasswordCredential selected = wmiCredentialses.First(c => c.ID == credentialId);
        this.CredentialName = selected.Name;
        this.Login = selected.Username;
        this.Password = selected.Password;
              
        WmiCredentialControlsSetEnabledState(false);

        ChangeAllValidatorsState(false);

        // If any error occured during initialization (e.g. the IP address cannot be resolved) 
        // then 'propertyBag' property isn't set.
        if (propertyBag != null)
        {
            propertyBag["credentialsId"] = credentialId;
        }
    }
    
    /// <summary>
    /// Method takes care about proper setting wmi credentials controls Enable property state.
    /// </summary>
    /// <param name="state">if set to <c>true</c> [set wmi credential controls enabled to true] otherwise set wmi credential controls enabled to false.</param>
    private void WmiCredentialControlsSetEnabledState(bool state)
    {
        tbCaption.Enabled = state;
        tbLogin.Enabled = state;
        tbPassword.Enabled = state;
        tbPasswordConfirmation.Enabled = state;

        if (state)
        {
            tbCaption.CssClass = String.Empty;
            tbLogin.CssClass = String.Empty;
            tbPassword.CssClass = String.Empty;
            tbPasswordConfirmation.CssClass = String.Empty;
        }
        else
        {
            tbCaption.CssClass = "disabled";
            tbLogin.CssClass = "disabled";
            tbPassword.CssClass = "disabled";
            tbPasswordConfirmation.CssClass = "disabled";
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        LoadData();
        
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (IsPostBack)
        {
            if (CredentialsId == NonExistingWmiCredentialID)
            {
                EncodePassword(Password);                                
            }
        }           
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        PageHelper.SetTestCredentialTimeoutForUpdatePanel(this.Page);
    }

    #region INodePropertyPlugin Members

        public void Initialize(IList<Node> nodes, NodePropertyPluginExecutionMode mode, Dictionary<string, object> pluginState)
    {
        if (nodes != null)
        {
            var uiNodes = nodes.Select(n => new UiWmiNode()
                                                {
                                                    NodeID = n.ID,
                                                    EngineID = n.EngineID,
                                                    IPAddress = n.IpAddress
                                                });
            this.nodes = uiNodes.ToList();
        }

        this.propertyBag = pluginState;
        this.mode = mode;
        
        switch (mode)
        {
            case NodePropertyPluginExecutionMode.WizDefineNode:
                if (!this.IsPostBack)
                {
                    int credentialsId;
                    if (nodes != null && nodes.FirstOrDefault() != null && nodes.FirstOrDefault().NodeSettings.ContainsKey(NodeSettingsNames.WmiCredentialSettingName))
                        if (int.TryParse(nodes.FirstOrDefault().NodeSettings[NodeSettingsNames.WmiCredentialSettingName], out credentialsId) && credentialsId != NonExistingWmiCredentialID)
                        {
                            DdlWmiCredentials.SelectedValue = credentialsId.ToString();
                            SetCredentialsFromDropDown();
                        }                 
                }                                
                break;

            case NodePropertyPluginExecutionMode.WizChangeProperties:
                if (!this.IsPostBack)
                {                    
                    int credentialsId;
                    if (nodes != null && nodes.FirstOrDefault() != null)
                    {
                        var nodeSettings = nodes.FirstOrDefault().NodeSettings;
                        if (nodeSettings.ContainsKey(NodeSettingsNames.WmiCredentialSettingName) && int.TryParse(nodeSettings[NodeSettingsNames.WmiCredentialSettingName],
                            out credentialsId) && credentialsId != NonExistingWmiCredentialID)
                        {
                            DdlWmiCredentials.SelectedValue = credentialsId.ToString();
                            SetCredentialsFromDropDown();
                        }
                    }                   
                }
                break;

            case NodePropertyPluginExecutionMode.EditProperies:                
                if (!this.IsPostBack)
                {                    
                    // even if there are multiple nodes, set values from first node only
                    Node node = nodes.First();

                    //do not initialize for non WMI nodes
                    if (node.NodeSubType != NodeSubType.WMI)
                        break;

                    using (var proxy = _blProxyCreator.Create(ex => _log.Error(ex)))
                    {
                        int credentialId;
                        if(node.NodeSettings[NodeSettingsNames.WmiCredentialSettingName]!=null && Int32.TryParse(node.NodeSettings[NodeSettingsNames.WmiCredentialSettingName],out credentialId))
                        {
                            UsernamePasswordCredential credential =
                                proxy.GetWmiCredential(credentialId);
                            if (credential == null)
                                throw new ArgumentNullException(String.Format(Resources.CoreWebContent.WEBCODE_TM0_91,credentialId));                                                        

                            if (credential.ID.Value > 0)
                            {
                                DdlWmiCredentials.SelectedValue = credential.ID.Value.ToString();
                                SetCredentialsFromDropDown();
                            }
                        }
                    }
                    
                }
                
                break;
        }
    }

    public bool Update()
    {
        var credentialsId = CredentialsId;
        switch (mode)
        {
            case NodePropertyPluginExecutionMode.WizDefineNode:
            case NodePropertyPluginExecutionMode.WizChangeProperties:
                if (credentialsId == NonExistingWmiCredentialID)
                {
                    var credential = CreateNewCredentials(CredentialName, Login, PlainPassword);
                    NodeWorkflowHelper.Node.NodeSettings[NodeSettingsNames.WmiCredentialSettingName]=
                                                             credential.ID.HasValue
                                                                 ? credential.ID.Value.ToString()
                                                                 : NonExistingWmiCredentialID.ToString();
                    NodeWorkflowHelper.WmiCredential = credential;
                }
                else
                {
                    NodeWorkflowHelper.Node.NodeSettings[NodeSettingsNames.WmiCredentialSettingName] = CredentialsId.ToString();
                    NodeWorkflowHelper.WmiCredential = wmiCredentialses.Where(c => c.ID.Value == CredentialsId).FirstOrDefault();
                }
                break;           
            case NodePropertyPluginExecutionMode.EditProperies:
               
                break;
        }
        
        return true;
    }

    private void SetNodeWindowsCredentials(Node node, string username, string password)
    {
        node.WmiCredential.UserName = username;
        node.WmiCredential.Password = password;
    }

    public bool Update(Node node)
    {
        if(CredentialsId == NonExistingWmiCredentialID)
        {
            var credential = CreateNewCredentials(CredentialName, Login, PlainPassword);            
            node.NodeSettings[NodeSettingsNames.WmiCredentialSettingName] =
                                                     credential.ID.HasValue
                                                         ? credential.ID.Value.ToString()
                                                         : NonExistingWmiCredentialID.ToString();            
            DdlWmiCredentials.Items.Add(new ListItem(credential.Name,credential.ID.Value.ToString()));
            DdlWmiCredentials.SelectedValue = credential.ID.Value.ToString();
            SetNodeWindowsCredentials(node, Login, PlainPassword);
        }
        else
        {
            node.NodeSettings[NodeSettingsNames.WmiCredentialSettingName] = CredentialsId.ToString();            
            SetNodeWindowsCredentials(node, Login, PlainPassword);
        }

        //
        // reset SNMP credentials
        node.SNMPV2Only = false;
        node.SNMPVersion= SolarWinds.Orion.Core.Common.Models.SNMPVersion.None;

        node.ReadOnlyCredentials.SNMPv3AuthType = SolarWinds.Orion.Core.Common.Models.SNMPv3AuthType.None;
        node.ReadOnlyCredentials.SNMPv3PrivacyType = SolarWinds.Orion.Core.Common.Models.SNMPv3PrivacyType.None;
        node.ReadOnlyCredentials.SnmpV3Context = String.Empty;
        node.ReadOnlyCredentials.SNMPv3UserName = String.Empty;
        node.ReadOnlyCredentials.SNMPv3AuthPassword = String.Empty;
        node.ReadOnlyCredentials.SNMPv3PrivacyPassword = String.Empty;
        node.ReadWriteCredentials.SNMPv3AuthType = SolarWinds.Orion.Core.Common.Models.SNMPv3AuthType.None;
        node.ReadWriteCredentials.SNMPv3PrivacyType = SolarWinds.Orion.Core.Common.Models.SNMPv3PrivacyType.None;
        node.ReadWriteCredentials.SnmpV3Context = String.Empty;
        node.ReadWriteCredentials.SNMPv3UserName = String.Empty;
        node.ReadWriteCredentials.SNMPv3AuthPassword = String.Empty;
        node.ReadWriteCredentials.SNMPv3PrivacyPassword = String.Empty;

        node.ReadOnlyCredentials.SNMPV3PrivKeyIsPwd = true;
        node.ReadOnlyCredentials.SNMPV3AuthKeyIsPwd = true;
        node.ReadWriteCredentials.SNMPV3PrivKeyIsPwd = true;
        node.ReadWriteCredentials.SNMPV3AuthKeyIsPwd = true;

        node.NodeSettings.Remove(SolarWinds.Orion.Core.Common.DALs.NodeSettingsNames.RoSnmpCredentialSettingName);
        node.NodeSettings.Remove(SolarWinds.Orion.Core.Common.DALs.NodeSettingsNames.RwSnmpCredentialSettingName);

        return true;
    }

    public bool Validate()
    {    
        return Validate(true);
    }


    public bool Validate(bool throwExceptions)
    {
        if (!this.CheckAllValidators())
        {
            if (throwExceptions)
            {
                throw new ArgumentException(Resources.CoreWebContent.WEBCODE_TM0_92);
            }
            return false;
        }       

        string validationText = string.Empty;
        bool valid = false;
        bool credentialsValid = true;
       
        if (CredentialsId == NonExistingWmiCredentialID)
        {
            int tmpId;
            credentialsValid = ValidateNewCredentials(CredentialName, Login, Password, out validationText,
                                                      out tmpId);            

            if ((credentialsValid) && (tmpId != CredentialsId))
                DdlWmiCredentials.SelectedValue = tmpId.ToString();
        }

        if (credentialsValid)
        {
            int engineID = nodes.Select(n => n.EngineID).First();

            try
            {
                using (var businessProxy = _blProxyCreator.Create(ex => _log.Error(ex), engineID, false))
                {
                    switch (mode)
                    {
                        case NodePropertyPluginExecutionMode.WizDefineNode:
                        case NodePropertyPluginExecutionMode.WizChangeProperties:

                            // in this step there is always only one node so we can get it's engineID directly 
                            //    CredentialsId, Login, PlainPassword);
                            valid = NodeWorkflowHelper.ValidateWMINodeWithErrorMessage(businessProxy,
                                                                       Ip,
                                                                       Login, PlainPassword, out validationText);
                            break;
                        case NodePropertyPluginExecutionMode.EditProperies:
                            if (!IsMultipleNodes)
                            {
                                valid = NodeWorkflowHelper.ValidateWMINodeWithErrorMessage(businessProxy,
                                                                       Ip,
                                                                       Login, PlainPassword, out validationText);
                            }
                            break;

                    }
                }
            }
            catch
            {
                valid = false;
            }
        }
       
        SetUIValidationState(valid, validationText);

        if ((!valid) && (throwExceptions))
            throw new ArgumentException((mode == NodePropertyPluginExecutionMode.EditProperies)
                                            ? validationText
                                            : string.Empty);

        return valid;
    }

    public void MultipleSelection_CheckedChanged(bool isChecked)
    {
        DdlWmiCredentials.Enabled = isChecked;

        bool enabled = true;
        if (CredentialsId != NonExistingWmiCredentialID)
        {
            enabled = !MultipleSelection; // FB85798
        }
                
        WmiCredentialControlsSetEnabledState(isChecked && enabled);
    }

    #endregion
}
