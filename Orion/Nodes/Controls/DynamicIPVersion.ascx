<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DynamicIPVersion.ascx.cs"
    Inherits="Orion_Nodes_Controls_DynamicIPVersion" %>
<div class="contentBlockHeader">
    <asp:CheckBox ID="cbMultipleSelection" runat="server" AutoPostBack="True" OnCheckedChanged="MultipleSelectionCheckedChanged" />
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_386) %> 
</div>
<table id="MainTable" runat="server" class="blueBox">
    <tr>
		<td class="leftLabelColumn">
		<asp:Literal ID="Literal2" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_523%>" />
		</td>
		<td class="rightInputColumn"><asp:TextBox runat="server" ID="tbDNS" Width="250"></asp:TextBox>
        <asp:CustomValidator ID="CustomValidator2" runat="server" ControlToValidate="tbDNS"
				ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_YK0_29 %>" SetFocusOnError="True" ValidateEmptyText="true" OnServerValidate="DNS_Validate" Display="Dynamic"/></td>
		<td>&nbsp;</td>
	</tr>
    <tr>
        <td class="leftLabelColumn">&nbsp;</td>
        <td class="helpfulText"><asp:Literal ID="Literal3" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_524%>" /></td>
    </tr>
    <tr>
        <td class="leftLabelColumn">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_387) %>
        </td>
        <td class="rightInputColumn">
            <asp:RadioButtonList runat="server" ID="SWNetPerfMon_Settings_Default_IP_Address_Resolution"
                class="CounterRollover">
                <asp:ListItem Value="2" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_389 %>"/>
                <asp:ListItem Value="23" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_390 %>"/>
            </asp:RadioButtonList>
        </td>        
    </tr>
    <tr>
        <td class="leftLabelColumn">&nbsp;</td>
        <td class="helpfulText" ><asp:Literal ID="Literal1" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_388%>" /></td>
    </tr>
</table>
