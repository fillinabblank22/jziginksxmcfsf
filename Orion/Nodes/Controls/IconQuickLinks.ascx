<%@ Control Language="C#" ClassName="IconQuickLinks" %>

<script runat="server">
	public string NavigateToUrl { get; set; }
    
    private string _imageName;

	public string ImageName
	{
		get { return _imageName; }
		set { _imageName = value; }
	}


    public string Caption { get; set; }
    
    public bool Show
    {
        set
        {
            if (value == false)
                Caption = "";
        }
    }
</script>

<span class="IconHelpButton">
<a class="HelpText" href="<%= DefaultSanitizer.SanitizeHtml(this.NavigateToUrl) %>" style="background: transparent url(<%= DefaultSanitizer.SanitizeHtml(this.ImageName) %>) scroll no-repeat left center; padding: 2px 0px 2px 20px;"><%= DefaultSanitizer.SanitizeHtml(this.Caption) %></a>

</span>