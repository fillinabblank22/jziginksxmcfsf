using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.Orion.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.Plugins.AddNode;

public delegate void PollingMethodModeChangeEventHandler(object sender, SNMPModeEventArgs e);

public delegate void SelectedPollingEnginesChangedEventHandler(object sender, SelectedPollingEnginesEventArgs e);

public partial class Orion_Nodes_Controls_PollingMethodSelector : System.Web.UI.UserControl
{
    private const string WmiInfoExecutionModeKey = "WmiInfoExecutionMode";

    public event SelectedPollingEnginesChangedEventHandler SelectedPollingEnginesChanged;

    private static readonly Log Log = new Log();

    public Orion_Nodes_Controls_NodeSNMP SnmpInfo
    {
        get { return this.SNMPInfo1; }
    }

    public Orion_Nodes_Controls_WmiInfo WmiInfo
    {
        get { return this.WMIInfo; }
    }

    public Orion_Nodes_Controls_AgentInfo AgentInfo
    {
        get { return this.AgentInfo1; }
    }

    public string TrCssClass { get; set; }

    /// <summary>
    /// Caches last used value for <see cref="EnablePollingMethodControls"/> method. Default state is enabled.
    /// </summary>
    private bool ArePollingMethodControlsEnabled { get; set; } = true;

    /// <summary>
    /// Holds value if Agent polling method is enabled. Default is true.
    /// </summary>
    private bool IsAgentPollingMethodAllowed
    {
        get { return ViewState[nameof(IsAgentPollingMethodAllowed)] as bool? != false; }
        set { ViewState[nameof(IsAgentPollingMethodAllowed)] = value; }
    }

    [Browsable(true)]
    [Category("Data")]
    public bool UnPluggable
    {
        get { return rbExternal.Checked; }
    }

    [Browsable(true)]
    [Category("Appearance")]
    public bool ICMP
    {
        get { return rbICMP.Checked || rbExternal.Checked; }

    }

    [Browsable(true)]
    [Category("Appearance")]
    public bool Agent
    {
        get { return rbAgent.Checked; }
    }

    [Browsable(true)]
    [Category("Appearance")]
    public bool Windows
    {
        get { return rbWMI.Checked; }
    }

    [Browsable(true)]
    [Category("Appearance")]
    public bool SNMP
    {
        get { return rbSNMP.Checked; }

    }

    private Tuple<IAddNodePollingMethodPlugin, Control> selectedPlugin = null;

    /// <summary>
    /// Finds whether radio button for plugin polling method is selected and its control. Cached the result.
    /// </summary>
    /// <returns></returns>
    private Tuple<IAddNodePollingMethodPlugin, Control> GetSelectedPlugin()
    {
        if (selectedPlugin == null)
        {
            // create empty tuple so that even in case no plugin is selected we don't iterate again
            selectedPlugin = new Tuple<IAddNodePollingMethodPlugin, Control>(null, null);

            foreach (var addNodeMethodPlugin in OrionModuleManager.GetAddNodeMethodPlugins())
            {
                var pluginRadioButton = GetPluginPollingMethodRadioButton(addNodeMethodPlugin.Key);

                if (pluginRadioButton?.Checked == true)
                {
                    Control control = GetPluginPollingMethodSettingsControl(addNodeMethodPlugin.Key);

                    selectedPlugin = new Tuple<IAddNodePollingMethodPlugin, Control>(addNodeMethodPlugin.Value.Plugin, control);
                    break;
                }
            }
        }

        return selectedPlugin;
    }

    /// <summary>
    /// Gets selected polling method plugin or null if hardcoded method selected.
    /// </summary>
    public IAddNodePollingMethodPlugin SelectedPluginMethod
    {
        get
        {
            var plugin = GetSelectedPlugin();
            return plugin?.Item1;
        }
    }

    /// <summary>
    /// Gets settings control for selected plugin polling method. Returns null if either hardcoded method selected or plugin doesn't provide settings control.
    /// </summary>
    public Control SelectedPluginSettingsControl
    {
        get
        {
            var plugin = GetSelectedPlugin();
            return plugin?.Item2;

        }
    }

    public List<Node> Nodes { get; set; }

    private NodeSubType nodeSubType;

    public NodeSubType NodeSubType
    {
        get
        {
            if (Nodes != null)
                nodeSubType = Nodes.First().NodeSubType;
            else
                nodeSubType = NodeSubType.None;
            return nodeSubType;

        }
    }

    public bool IsExternal
    {
        get
        {
            if (Nodes != null && Nodes.Count > 0)
                return Nodes.First().UnPluggable;
            return false;
        }
    }

    private Node FirstNode
    {
        get { return Nodes?.FirstOrDefault(); }
    }

    public bool MultipleSelection
    {
        get { return cbMultipleSelection.Visible; }
        set { cbMultipleSelection.Visible = value; }
    }

    public bool MultipleSelectionSelected
    {
        get { return cbMultipleSelection.Checked; }
        set
        {
            cbMultipleSelection.Checked = value;
            cbMultipleSelection_CheckedChanged(null, null);
        }
    }

    public NodePropertyPluginExecutionMode Mode
    {
        get { return (NodePropertyPluginExecutionMode) (ViewState[WmiInfoExecutionModeKey] ?? NodePropertyPluginExecutionMode.Undefined); }
        set { ViewState[WmiInfoExecutionModeKey] = value; }
    }

    public event PollingMethodModeChangeEventHandler PollingMethodChanged;

    public void UpdateSnmpV3Credentials(Node node)
    {
        if (node.NodeSubType == NodeSubType.SNMP)
        {
            SnmpInfo.UpdateSnmpV3Credentials(node);
        }
        else if (node.NodeSubType == NodeSubType.Agent)
        {
            AgentInfo.SnmpInfo.UpdateSnmpV3Credentials(node);
        }
    }

    private void SetMode(NodePropertyPluginExecutionMode mode)
    {
        SelectPollingMethodByNodeSubType(false);
        switch (mode)
        {
            case NodePropertyPluginExecutionMode.WizDefineNode:
                OuterTitle.Visible = false;
                InnerTitle.Visible = true;
                cbMultipleSelection.Visible = false;
                break;
            case NodePropertyPluginExecutionMode.EditProperies:
                OuterTitle.Visible = true;
                InnerTitle.Visible = false;
                SetAllowedOperations(NodeSubType);
                cbMultipleSelection_CheckedChanged(null, null);
                break;
            case NodePropertyPluginExecutionMode.WizChangeProperties:
                OuterTitle.Visible = true;
                InnerTitle.Visible = false;
                EnablePollingMethodControls(false);
                cbMultipleSelection.Visible = false;
                break;
        }
    }

    private void SelectPollingMethodByNodeSubType(bool ignorePollingMethodPlugins)
    {
        switch (NodeSubType)
        {
            case NodeSubType.ICMP:
                var pluginPollingMethodId = (FirstNode != null) ? GetPluginPollingMethodId(FirstNode) : null;
                if (pluginPollingMethodId.HasValue && !ignorePollingMethodPlugins)
                {
                    var radioButton = GetPluginPollingMethodRadioButton(pluginPollingMethodId.Value);

                    if (radioButton != null)
                    {
                        radioButton.Checked = true;
                        OnPluginCheckedChanged(radioButton, null);
                    }
                }
                else if (IsExternal)
                {
                    rbExternal.Checked = true;
                    OnExternalNodeChecked(null, null);
                }
                else
                {
                    rbICMP.Checked = true;
                    OnICMPChecked(null, null);
                }

                break;
            case NodeSubType.SNMP:
                rbSNMP.Checked = true;
                OnSNMPChecked(null, null);
                break;
            case NodeSubType.WMI:
                rbWMI.Checked = true;
                OnWindowsServersChecked(null, null);
                break;
            case NodeSubType.Agent:
                rbAgent.Checked = true;
                OnAgentChecked(null, null);
                break;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        GeneratePluginPollingMethods();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState["NodeSubType"] = NodeSubType;
            SetMode(Mode);
        }
        WMIInfo.Initialize(Nodes, Mode, new Dictionary<string, object>());
        AgentInfo.InitializeAgent(Nodes, Mode, new Dictionary<string, object>());
        // Configure polling method state
        if (SnmpInfo.EngineID > 0)
        {
            // Engine ID has already been set from single node or by user
            SetStateForEngineIds(false, SnmpInfo.EngineID);
        }
        else if (Nodes != null)
        {
            // Editing multiple nodes and user has not changed them to single polling engine yet
            SetStateForEngineIds(false, Nodes.Select(x => x.EngineID).ToArray());
        }
        SetEditControlsVisibility();
    }

    protected void OnPollingMethodChanged(object sender, SNMPModeEventArgs e)
    {
        if (PollingMethodChanged != null) PollingMethodChanged(sender, e);
    }

    protected void OnExternalNodeChecked(object sender, EventArgs e)
    {
        HidePluginPollingMethodControls();

        SNMPModeEventArgs arg = new SNMPModeEventArgs();
        arg.ICMP = true;
        OnPollingMethodChanged(this, arg);
    }

    protected void OnICMPChecked(object sender, EventArgs e)
    {
        HidePluginPollingMethodControls();

        SNMPModeEventArgs arg = new SNMPModeEventArgs();
        arg.ICMP = true;
        OnPollingMethodChanged(this, arg);
    }

    protected void OnWindowsServersChecked(object sender, EventArgs e)
    {
        HidePluginPollingMethodControls();

        SNMPModeEventArgs arg = new SNMPModeEventArgs();
        arg.ICMP = false;
        arg.Windows = true;
        OnPollingMethodChanged(this, arg);
    }

    protected void OnSNMPChecked(object sender, EventArgs e)
    {
        HidePluginPollingMethodControls();

        SNMPModeEventArgs arg = new SNMPModeEventArgs();
        arg.ICMP = false;
        OnPollingMethodChanged(this, arg);
    }

    protected void OnAgentChecked(object sender, EventArgs e)
    {
        HidePluginPollingMethodControls();

        SNMPModeEventArgs arg = new SNMPModeEventArgs() {Agent = true};
        OnPollingMethodChanged(this, arg);
        //AgentInfo1.SetControls();
    }

    private void SetEditControlsVisibility()
    {
        if (rbSNMP.Checked)
            SNMPInfo.Visible = true;
        else
            SNMPInfo.Visible = false;

        if (rbWMI.Checked)
            WMIInfoPanel.Visible = true;
        else
            WMIInfoPanel.Visible = false;

        AgentInfoPanel.Visible = rbAgent.Checked;
    }

    private void SetAllowedOperations(NodeSubType nodeSubType)
    {
        switch (nodeSubType)
        {
            case NodeSubType.SNMP:
                rbExternal.Enabled = false;
                rbICMP.Enabled = true;
                rbWMI.Enabled = true;
                rbAgent.Enabled = true;
                break;
            case NodeSubType.WMI:
                rbExternal.Enabled = false;
                rbICMP.Enabled = true;
                rbSNMP.Enabled = true;
                rbAgent.Enabled = true;
                break;
            case NodeSubType.Agent:
                rbExternal.Enabled = false;
                rbICMP.Enabled = true;
                rbSNMP.Enabled = true;
                rbWMI.Enabled = true;
                break;
        }

        DisablePluginPollingMethodsForEdit(nodeSubType);
    }

    /// <summary>
    /// Sets all polling methods (plugins included) to given state. Additional logic for Agent polling method.
    /// </summary>
    /// <param name="enabled"></param>
    private void EnablePollingMethodControls(bool enabled)
    {
        ArePollingMethodControlsEnabled = enabled;

        rbExternal.Enabled = enabled;
        rbICMP.Enabled = enabled;
        rbSNMP.Enabled = enabled;
        rbWMI.Enabled = enabled;
        rbAgent.Enabled = enabled;

        SetPluginRadionButtonsEnabledState(enabled);
    }

    /// <summary>
    /// Changes Agent polling method selection to SNMP when conditions for this enforced transition are met.
    /// Used when polling engine is changed to ORC where Agent polling method is disabled.
    /// </summary>
    private void TryUpdateAgentPollingMethodSelection()
    {
        if (!Visible || !rbAgent.Checked || agentPollingMethodBlock.Visible)
        {
            return;
        }

        // Move polling method selection to SNMP
        rbAgent.Checked = false;
        rbSNMP.Checked = true;
        OnSNMPChecked(this, EventArgs.Empty);
        SetEditControlsVisibility();
    }

    /// <summary>
    /// Sets selected engine ID and updates polling method controls.
    /// </summary>
    /// <param name="engineId">Selected engine ID</param>
    public void SetEngineId(int engineId)
    {
        if (SnmpInfo.EngineID != engineId)
        {
            SnmpInfo.EngineID = engineId;
            SetStateForEngineIds(true, engineId);
        }
    }

    /// <summary>
    /// Configures polling method control states based on selected engine IDs.
    /// Currently toggles only Agent polling method in case any engine is of type Remote Collector.
    /// </summary>
    /// <param name="engineIds">Engine IDs to check.</param>
    private void SetStateForEngineIds(bool selectedEngineChanged,params int[] engineIds)
    {
        // Control is not visible when editing multiple nodes with different polling methods
        if (!Visible || (MultipleSelection && !MultipleSelectionSelected))
        {
            return;
        }

        HashSet<EngineServerType> engineServerTypes = GetEngineServerTypes(engineIds);
        if (Mode == NodePropertyPluginExecutionMode.WizDefineNode)
        {
            HideOrShowPluginPollingMethodWhichDoesntSupportSelectedPollingEngineType(engineServerTypes, selectedEngineChanged);
        }

        // Just toggle entire agent polling method table row visibility and update polling method selection if Agent is now selected
        bool anyRemoteCollector = engineServerTypes.Any(item => item == EngineServerType.RemoteCollector);
        agentPollingMethodBlock.Visible = !anyRemoteCollector;
        TryUpdateAgentPollingMethodSelection();
        SelectedPollingEnginesChanged?.Invoke(this, new SelectedPollingEnginesEventArgs(engineIds, engineServerTypes));
    }

    private static HashSet<EngineServerType> GetEngineServerTypes(int[] engineIds)
    {
        HashSet<EngineServerType> engineServerTypes = new HashSet<EngineServerType>();

        foreach (var engineId in engineIds)
        {
            try
            {
                var dataTable = EnginesDAL.GetEngineById(engineId);
                if (dataTable.Rows.Count > 0)
                {
                    string strServerType = Convert.ToString(dataTable.Rows[0]["ServerType"]);
                    EngineServerType engineServerType;
                    if (Enum.TryParse(strServerType, true, out engineServerType))
                    {
                        engineServerTypes.Add(engineServerType);
                    }
                }
                else
                {
                    Log.WarnFormat("No engine with ID {0} found", engineId);
                }
            }
            catch (Exception ex)
            {
                Log.Error($"Unable to get engine information for engine {engineId}", ex);
            }
        }

        return engineServerTypes;
    }
    
    /// <summary>
	/// This method is intended for node edit. If the node uses plugin polling method any method change is disabled. If it uses hardcoded polling method
	/// only plugin polling methods are disabled. The result is that changing of polling method to/from plugin method is disabled.
	/// </summary>
	/// <param name="nodeSubType"></param>
	private void DisablePluginPollingMethodsForEdit(NodeSubType nodeSubType)
	{
		if (Mode == NodePropertyPluginExecutionMode.EditProperies)
		{
			if (nodeSubType == NodeSubType.ICMP)
			{
				if (SelectedPluginMethod != null)
				{
					EnablePollingMethodControls(false);
				}
				else
				{
					SetPluginRadionButtonsEnabledState(false);
				}
			}
			else
			{
				SetPluginRadionButtonsEnabledState(false);
			}
		}
	}

    public void cbMultipleSelection_CheckedChanged(object sender, EventArgs e)
    {
        bool pollingMethodSelectorChecked = this.cbMultipleSelection.Checked || !this.cbMultipleSelection.Visible;

        NodeSubType nodeSubType = SolarWinds.Orion.Core.Common.Models.NodeSubType.None;
        if (ViewState["NodeSubType"] != null)
        {
            try
            {
                nodeSubType = (NodeSubType) ViewState["NodeSubType"];
            }
            catch (Exception)
            {
            }
        }

        if (MultipleSelection)
        {
            EnablePollingMethodControls(pollingMethodSelectorChecked);
        }

        if (nodeSubType == SolarWinds.Orion.Core.Common.Models.NodeSubType.SNMP)
        {
            SNMPInfo1.cbMultipleSelection_CheckedChanged(pollingMethodSelectorChecked);
            SetAllowedOperations(NodeSubType.SNMP);
        }

        if (nodeSubType == SolarWinds.Orion.Core.Common.Models.NodeSubType.WMI)
        {
            WMIInfo.MultipleSelection_CheckedChanged(pollingMethodSelectorChecked);
            SetAllowedOperations(NodeSubType.WMI);
        }

        if (nodeSubType == SolarWinds.Orion.Core.Common.Models.NodeSubType.Agent)
        {
            AgentInfo1.MultipleSelection_CheckedChanged(pollingMethodSelectorChecked);
            SetAllowedOperations(NodeSubType.Agent);
        }

	    if (nodeSubType == NodeSubType.ICMP)
	    {
		    if ((SelectedPluginMethod != null) && (SelectedPluginSettingsControl != null))
		    {
			    SelectedPluginMethod.SetControlEnabledState(SelectedPluginSettingsControl, pollingMethodSelectorChecked);
		    }
			SetAllowedOperations(NodeSubType.ICMP);
	    }

        // when polling method selctor isn't checked, let's disable again all radiobuttons that might have been enabled by SetAllowedOperations() 
        if (MultipleSelection && !pollingMethodSelectorChecked)
        {
            EnablePollingMethodControls(false);
        }
    }

    /// <summary>
    /// This method adds polling methods from plugins into the selector
    /// </summary>
    private void GeneratePluginPollingMethods()
    {
        foreach (var addNodePollingMethodPluginConfig in OrionModuleManager.GetAddNodeMethodPlugins())
        {
            pluginMethodsPlaceholder.Controls.Add(GeneratePluginPollingMethodSection(addNodePollingMethodPluginConfig));
        }
    }

    /// <summary>
    /// This method hides settings controls for all plugins
    /// </summary>
    private void HidePluginPollingMethodControls()
    {
        foreach (var addNodeMethodPlugin in OrionModuleManager.GetAddNodeMethodPlugins())
        {
            var pluginPanel = FindControl(pluginSettingsPanelIdPrefix + addNodeMethodPlugin.Key);

            if (pluginPanel != null)
            {
                pluginPanel.Visible = false;
            }
        }
    }

    private void HideOrShowPluginPollingMethodWhichDoesntSupportSelectedPollingEngineType(HashSet<EngineServerType> selectedPollingEngineTypes, bool selectedEngineChanged)
    {
        foreach (var addNodeMethodPlugin in OrionModuleManager.GetAddNodeMethodPlugins())
        {
            bool show = selectedPollingEngineTypes.All(item =>
                addNodeMethodPlugin.Value.SupportedPollingEngineTypes.Contains(item));
           
            var pluginPollingSelectorTr = FindControl(pluginPollingSelectorPrefix + addNodeMethodPlugin.Key);
            if (pluginPollingSelectorTr != null)
            {
                pluginPollingSelectorTr.Visible = show;
            }

            var pluginRadioButton = (RadioButton)FindControl(pluginRadioButtonIdPrefix + addNodeMethodPlugin.Key);
            if (pluginRadioButton != null && pluginRadioButton.Checked)
            {
                if (!show)
                {
                    if (selectedEngineChanged)
                    {
                        pluginRadioButton.Checked = false;
                        SelectPollingMethodByNodeSubType(true);
                    }  
                }
                else if (show)
                {
                    var pluginPanel = FindControl(pluginSettingsPanelIdPrefix + addNodeMethodPlugin.Key);

                    if (pluginPanel != null)
                    {
                        pluginPanel.Visible = true;
                    }
                }
            }
        }
    }

    private const string pluginRadioButtonIdPrefix = "pluginRadioButton";
    private const string pluginSettingsPanelIdPrefix = "pluginSettingsPanel";
    private const string pluginPollingSelectorPrefix = "pluginPollingSelector";

    /// <summary>
    /// Generates section for one polling method from plugin. This mimics polling method section for hard coded methods.
    /// </summary>
    /// <param name="pluginItem"></param>
    /// <returns></returns>
    private Control GeneratePluginPollingMethodSection(KeyValuePair<int, AddNodePollingMethodPluginConfig> pluginItem)
    {
        var tr = new HtmlGenericControl("tr");
        tr.Attributes["class"] = TrCssClass;
        tr.ID = pluginPollingSelectorPrefix + pluginItem.Key;

        var td = new HtmlGenericControl("td");
        td.Attributes["class"] = "leftLabelColumn pollingSelector";

        tr.Controls.Add(td);

        td = new HtmlGenericControl("td");
        td.Attributes["class"] = "rightInputColumn pollingSelector";
        tr.Controls.Add(td);

        var label = new HtmlGenericControl("label");
        td.Controls.Add(label);

        var radioButton = new RadioButton();
        radioButton.ID = pluginRadioButtonIdPrefix + pluginItem.Key;
        radioButton.AutoPostBack = true;
        radioButton.GroupName = "PollingMethod";
        radioButton.CheckedChanged += OnPluginCheckedChanged;
        label.Controls.Add(radioButton);

        var span = new HtmlGenericControl("span");
        var bold = new HtmlGenericControl("b");
        bold.InnerHtml = "&nbsp;" + pluginItem.Value.Plugin.TitleHeading + ": ";

        span.Controls.Add(bold);
        span.Controls.Add(new LiteralControl(pluginItem.Value.Plugin.Title));
        label.Controls.Add(span);

        td.Controls.Add(new LiteralControl("<br/>"));

        span = new HtmlGenericControl("span");
        span.Attributes["class"] = "helpfulText";
        span.InnerHtml = pluginItem.Value.Plugin.Description;
        td.Controls.Add(span);

        var panel = new Panel();
        panel.CssClass = "snmpInfo";
        panel.ID = pluginSettingsPanelIdPrefix + pluginItem.Key;
        td.Controls.Add(panel);

        if (!string.IsNullOrEmpty(pluginItem.Value.UserControlPath))
        {
            panel.Controls.Add(LoadControl(pluginItem.Value.UserControlPath));
            panel.Visible = false;
        }

        return tr;
    }

    protected void OnPluginCheckedChanged(object sender, EventArgs e)
    {
        HidePluginPollingMethodControls();

        string radioButtonIdWithoutPrefix = (sender as RadioButton)?.ID?.Replace(pluginRadioButtonIdPrefix, string.Empty);
        int pluginMethodId;

        if (int.TryParse(radioButtonIdWithoutPrefix, out pluginMethodId))
        {
            var pluginSettingsPanel = FindControl(pluginSettingsPanelIdPrefix + pluginMethodId);
            if (pluginSettingsPanel != null)
            {
                pluginSettingsPanel.Visible = true;
            }

            SNMPModeEventArgs arg = new SNMPModeEventArgs();
            arg.ICMP = true;
            OnPollingMethodChanged(this, arg);
        }
    }

    /// <summary>
    /// This value has three 'states'. Null means we didn't check yet, -1 means node doesn't use plugin method and values greated than -1 are actual plugin IDs.
    /// </summary>
    private int? nodePluginPollingMethodIndex = null;

    /// <summary>
    /// Gets polling method plugin ID if node uses plugin method. Null otherwise.
    /// </summary>
    /// <param name="node"></param>
    /// <returns></returns>
    private int? GetPluginPollingMethodId(Node node)
    {
        if (!nodePluginPollingMethodIndex.HasValue)
        {
            nodePluginPollingMethodIndex = -1;

            foreach (var addNodePollingMethodPluginConfig in OrionModuleManager.GetAddNodeMethodPlugins())
            {
                if (addNodePollingMethodPluginConfig.Value.Plugin.IsUsedByNode(node))
                {
                    nodePluginPollingMethodIndex = addNodePollingMethodPluginConfig.Key;
                    break;
                }
            }
        }

        return (nodePluginPollingMethodIndex.Value == -1) ? (int?) null : nodePluginPollingMethodIndex.Value;
    }

    private RadioButton GetPluginPollingMethodRadioButton(int pluginMethodId)
    {
        return FindControl(pluginRadioButtonIdPrefix + pluginMethodId) as RadioButton;
    }

    private Control GetPluginPollingMethodSettingsControl(int pluginMethodId)
    {
        var pluginPanel = FindControl(pluginSettingsPanelIdPrefix + pluginMethodId) as Panel;

        if (pluginPanel?.Controls.Count > 0)
        {
            return pluginPanel.Controls[0];
        }

        return null;
    }

    /// <summary>
    /// This methods calls LoadState method on all plugin polling methods with empty state. Used to init controls on edit page.
    /// </summary>
    /// <param name="node"></param>
    public void LoadStateForPluginMethods(Node node)
    {
        foreach (var addNodePollingMethod in OrionModuleManager.GetAddNodeMethodPlugins())
        {
            var settingsControl = GetPluginPollingMethodSettingsControl(addNodePollingMethod.Key);

            addNodePollingMethod.Value.Plugin.LoadState(node, settingsControl, null);
        }
    }

    private void SetPluginRadionButtonsEnabledState(bool state)
    {
        foreach (int pollingMethodId in OrionModuleManager.GetAddNodeMethodPlugins().Keys)
        {
            RadioButton radioButton = GetPluginPollingMethodRadioButton(pollingMethodId);

            if (radioButton != null)
            {
                radioButton.Enabled = state;
            }
        }
    }
}
