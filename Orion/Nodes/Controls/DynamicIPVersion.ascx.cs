using System;
using System.Net.Sockets;
using System.Web.UI.WebControls;

public partial class Orion_Nodes_Controls_DynamicIPVersion : System.Web.UI.UserControl
{

	public bool MultipleSelection
	{
		get { return cbMultipleSelection.Visible; }
		set { cbMultipleSelection.Visible = value; }
	}

	public bool MultipleSelectionSelected
	{
		get { return cbMultipleSelection.Checked; }
		set
		{
			cbMultipleSelection.Checked = value;
			MultipleSelectionCheckedChanged(null, null);
		}
	}

    public string DNS
    {
        get { return tbDNS.Text; }
        set { tbDNS.Text = value; }
    }

    public AddressFamily IPVersionSelected
    {
        get
        {
            return SWNetPerfMon_Settings_Default_IP_Address_Resolution.SelectedIndex == 0
                       ? AddressFamily.InterNetwork
                       : AddressFamily.InterNetworkV6;
        }
        set
        {
            
            SWNetPerfMon_Settings_Default_IP_Address_Resolution.SelectedIndex = value == AddressFamily.InterNetwork
                                                                                    ? 0
                                                                                    : 1;
        }   
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (MultipleSelection)
        {
            MainTable.Rows.RemoveAt(0); // dns edit
            MainTable.Rows.RemoveAt(0); // label
            SWNetPerfMon_Settings_Default_IP_Address_Resolution.Enabled = cbMultipleSelection.Checked;
        }
    }

    protected void DNS_Validate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = !string.IsNullOrEmpty(DNS) && Uri.CheckHostName(DNS) > 0;
    }

	protected void MultipleSelectionCheckedChanged(object sender, EventArgs e)
	{
		SWNetPerfMon_Settings_Default_IP_Address_Resolution.Enabled = cbMultipleSelection.Checked;
	}

    /// <summary>
    /// Sets the Enabled state of DNS textbox based on argument.
    /// </summary>
    /// <param name="isEnabled">Desired Enabled state of DNS textbox.</param>
    public void SetDNSEnabledState(bool isEnabled)
    {
        tbDNS.Enabled = isEnabled;
        tbDNS.ReadOnly = !isEnabled;
    }
}
