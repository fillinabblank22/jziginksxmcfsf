<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StatusRollupSelector.ascx.cs" Inherits="Orion_Nodes_Controls_RollupStatusModeSelector" %>
<div class="contentBlock" runat="server" id="RollupModeWrapper" automation="RollupModeWrapper">
    <div runat="server" id="CustomPropertiesHeader">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td class="contentBlockHeader">
                    <asp:CheckBox ID="cbMultipleSelection" runat="server" AutoPostBack="True" OnCheckedChanged="cbMultipleSelection_CheckedChanged" />
                    <asp:Literal ID="Literal1" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_213 %>" />
                </td>
            </tr>
        </table>
    </div>
    <table class="blueBox">
        <tr>
            <td class="leftLabelColumn" style="vertical-align: text-top;">
                <asp:Literal ID="Literal2" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_213 %>" />
            </td>
            <td class="rightInputColumn">
                <asp:DropDownList runat="server" ID="RollupModeDropDown" SelectionMode="single" automation="RollupModeDropDown" />
                <a href="#" id="RollupHelpLink" runat="server" class="formHelpfulText" target="_blank" rel="noopener noreferrer"><u><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_214) %></u></a>
            </td>
        </tr>
    </table>
</div>
