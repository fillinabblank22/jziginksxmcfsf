<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomProperties.ascx.cs"
    Inherits="Orion_Nodes_Controls_CustomProperties" %>
<%@ Register Src="~/Orion/Controls/EditCpValueControl.ascx" TagPrefix="orion" TagName="EditCpValueControl" %>
<orion:Include ID="Include1" runat="server" File="CustomPropertyEditor.css" />
<style type="text/css">
    .linktoCPE {text-align: right !important;}
</style>
<div class="contentBlock">
    <div runat="server" id="CustomPropertiesHeader">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td class="contentBlockHeader">
                        <asp:Label runat="server"><%= DefaultSanitizer.SanitizeHtml(this.Caption) %></asp:Label>
                    </td>
				<% if (UseDefaultManageCustomPropertiesLink) { %>
                    <td class="linktoCPE">
                        <span>
                            <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="" />
                            <a href="/Orion/Admin/CPE/Default.aspx" class="RenewalsLink" target="_blank">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_4) %></a> </span>
                    </td>
				<% } %>
            </tr>
             <% if (! string.IsNullOrEmpty(this.HelpFullHintText))
           { %>
            <tr>
                <td colspan="2" style="vertical-align: top !important; padding: 0">
                    <span id="cpHint" class="helpfulText" style="padding: 3px; vertical-align: text-top;"><%= DefaultSanitizer.SanitizeHtml(this.HelpFullHintText) %>
					<% if (!UseDefaultManageCustomPropertiesLink) { %>
						<a href="/Orion/Admin/CPE/Default.aspx" class="sw-link" target="_blank" style="vertical-align: top;">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_4) %></a>
						<%--&raquo;--%>
					<% } %>
					</span>
                </td>
            </tr>
            <% }
           %>
        </table>
    </div>
    <table class="blueBox cpRepeaterTable">
        <asp:Repeater runat="server" ID="repCustomProperties" OnItemDataBound="repCustomProperties_ItemDataBound">
            <ItemTemplate>
                <tr>
                    <td class="leftLabelColumn" style="vertical-align: text-top;white-space: nowrap">
                        <asp:CheckBox runat="server" ID="cbSelectedProperty" AutoPostBack="true" OnCheckedChanged="cbSelectedProperty_CheckedChanged" />&nbsp;
                        <asp:Literal runat="server" ID="litPropertyName"></asp:Literal><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Punctuation_mark_colon) %>
                    </td>
                    <td class="rightInputColumn" style="vertical-align: middle;">
                        <orion:EditCpValueControl ID="PropertyValue" runat="server" />
                        <span class="helpfulText" style="padding: 0px;">
                            <asp:Literal ID="descriptionText" runat="server" Text="" />
                        </span>
                        <asp:PlaceHolder runat="server" ID="placeHolder"></asp:PlaceHolder>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
</div>
