<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViewSelector.ascx.cs" Inherits="Orion_Nodes_Controls_ViewSelector" %>
    <div class="contentBlock">
    <div runat="server" id="CustomPropertiesHeader">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td class="contentBlockHeader">
                    <asp:Literal ID="Literal1" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PR_07 %>" />
                </td>
            </tr>
        </table>
    </div>
    <table class="blueBox">
        <tr>
            <td class="leftLabelColumn" style="vertical-align: text-top;">
                <asp:Literal ID="Literal2" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PR_08 %>" />
            </td>
            <td class="rightInputColumn">
                <asp:DropDownList runat="server" ID="lbxViews" SelectionMode="single" />
            </td>
        </tr>
    </table>
</div>
