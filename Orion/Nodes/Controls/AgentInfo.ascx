<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AgentInfo.ascx.cs" Inherits="Orion_Nodes_Controls_AgentInfo" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>
<%@ Register Src="~/Orion/Nodes/Controls/SnmpInfo.ascx" TagPrefix="orion" TagName="SNMPInfo" %>

<orion:Include runat="server" File="AsyncFileUpload.js" />
<orion:Include runat="server" File="Nodes/js/AgentInfo.js" />

<div class="contentBlock">
    <div style="max-width: 350px">
        <div id="agentInstalledInfo" runat="server">
            <div class="sw-pg-hint-green">
                <div class="sw-pg-hint-body-confirm">
                    <span class="sw-pg-hint-text"><%= DefaultSanitizer.SanitizeHtml(String.Format(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_AgentDetected, 
                    !string.IsNullOrEmpty(NodeInfo.Caption) ? HttpUtility.HtmlEncode(NodeInfo.Caption) : NodeInfo.IpAddress)) %></span>
                </div>
            </div>
        </div>
        <div id="agentProgressInfo" runat="server">
            <div class="sw-pg-hint-blue">
                <div class="sw-pg-hint-body-info">
                    <span runat="server" id="agentProgressInfoText" class="sw-pg-hint-text"></span>
                </div>
            </div>
        </div>
        <div id="agentWarning" runat="server">
            <div class="sw-pg-hint-yellow">
                <div class="sw-pg-hint-body-warning">
                    <span runat="server" id="agentWarningText" class="sw-pg-hint-text"></span>
                </div>
            </div>
        </div>
        <div id="agentError" runat="server">
            <div class="sw-pg-hint-red">
                <div class="sw-pg-hint-body-error">
                    <div runat="server" id="agentErrorText" class="sw-pg-hint-text"></div>
                    <br />
                    <div class="sw-pg-hint-text">
                        <span class="LinkArrow">&#0187;</span>
                        <orion:HelpLink ID="HelpLink2" runat="server" HelpUrlFragment="OrionAgent-TroubleshootingAgentInstalls" HelpDescription="<%$ HtmlEncodedResources: CoreWebContent, AddNodeWizard_AgentError_LinkText %>" CssClass="sw-link" />
                    </div>
                </div>
            </div>
        </div>
    </div>
<asp:Panel runat="server" ID="deployAgentDiv">
    <div class="blueBox sw-agent-info-credentials-table" id="credentialsTable" runat="server">
        <table>
            <tr runat="server" ID="agentSettingsTitleRow">
                <td colspan="3">
                    <div class="helpfulText">
                        <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_Title) %></span>
                        <span>
                            <span class="LinkArrow">&#0187;</span>
                            <orion:HelpLink ID="HelpLink1" runat="server" HelpUrlFragment="OrionAgent-AgentPollingMethod"
                                HelpDescription="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_404 %>" CssClass="sw-link" />
                        </span>
                    </div>
	            </td>	            
            </tr>
            <% if (IsLinuxAgentEnabled)
               { %>
            <tr class="sw-agent-info-credentials-tabs-row" runat="server" ID="AgentTargetOsRow">
                <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label13"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_TargetOS) %>:&nbsp;</asp:Label>
                </td>
                 <td class="rightInputColumn" colspan="2">
                    <asp:RadioButton ID="OSTypeWindows" runat="server" AutoPostBack="True" OnCheckedChanged="OsTypeChanged" 
                    GroupName="osType" Checked="True" CssClass="active" />
                    <asp:RadioButton ID="OSTypeLinux" runat="server" AutoPostBack="True" OnCheckedChanged="OsTypeChanged" 
                    GroupName="osType" />
	            </td>
            </tr>
            <tr class="sw-agent-info-credentials-spacer-row" runat="server" ID="AgentSpacerRow">
                <td colspan="3"></td>
            </tr>
            <tr runat="server" ID="credentialTypeRow">
                <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label2"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_CredentialType) %>:&nbsp;</asp:Label>
                </td>
                 <td class="rightInputColumn" colspan="2">
                    <asp:RadioButton ID="UsernamePasswordCredentialTypeRadio" runat="server" AutoPostBack="True" OnCheckedChanged="CredentialTypeChanged" 
                    GroupName="credentialType" Checked="True" />
                    <asp:RadioButton ID="PrivateKeyTypeRadio" runat="server" AutoPostBack="True" OnCheckedChanged="CredentialTypeChanged" 
                    GroupName="credentialType" />
	            </td>
            </tr>
            <% } %>
            <tr runat="server" ID="ExistingCredentialRow">
	            <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="lblTemplateLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_234) %>&nbsp;</asp:Label>
                </td>
	            <td class="rightInputColumn">
                    <asp:DropDownList ID="DdlAgentCredentials" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DdlAgentCredentials_IndexChanged" >
                    </asp:DropDownList>
	            </td>
	            <td rowspan="2" class="helpColumn" style="vertical-align: top">
                    <div class="sw-pg-hint-blue">
                        <div class="sw-pg-hint-body-info">
                            <span class="sw-pg-hint-text"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_CredentialsNeeded) %></span>
                        </div>
                    </div>
	            </td>
            </tr>
            <tr>
                <td colspan="3"></td>
            </tr>
            <tr runat="server" ID="CredentialNameRow">
	            <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label1"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_235) %>&nbsp;</asp:Label>
                </td>
	            <td class="rightInputColumn">
                    <asp:TextBox ID="tbCaption" runat="server" MaxLength=50 ></asp:TextBox>
	            </td>
	            <td class="helpColumn">
                    <asp:RequiredFieldValidator ID="RequiredCredentialNameValidator" runat="server" Display="Dynamic"  ErrorMessage="RequiredFieldValidator" ControlToValidate="tbCaption" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_237 %>"></asp:RequiredFieldValidator>
	            </td>
            </tr>
            <tr runat="server" ID="UserNameRow">
	            <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label3"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AK0_165) %>&nbsp;</asp:Label>
                </td>
	            <td class="rightInputColumn">
                    <asp:TextBox ID="tbLogin" runat="server" MaxLength=50></asp:TextBox>
                </td>
                <td class="helpColumn">
                    <asp:RequiredFieldValidator ID="RequiredUserNameValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" ControlToValidate="tbLogin" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_238 %>" ></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr runat="server" id="windowsUsernameHelpRow">
                <td class="leftLabelColumn">
                    &nbsp;
                </td>
                <td colspan="2" class="rightInputColumn">
                    <div class="smallText">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_UsernameHelpText) %>
                    </div>
                    <div id="EditCredentialHelp">
                        <span class="LinkArrow">&#0187;</span>
                        <orion:HelpLink ID="IPSLADiscoveryHelpLink" runat="server" HelpUrlFragment="OrionPHWhatPrivilegesNeed"
                            HelpDescription="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_516 %>" CssClass="helpLink" />
                    </div>
	            </td>	            
            </tr>
            <tr runat="server" ID="PasswordRow">
	            <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label5"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_40) %>&nbsp;</asp:Label>
                </td>
	            <td class="rightInputColumn">
                    <asp:TextBox ID="tbPassword" runat="server" TextMode="Password" MaxLength="127" autocomplete="off" ></asp:TextBox>
	            </td>
	            <td class="helpColumn">
                    <asp:RequiredFieldValidator ID="RequiredPasswordValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" ControlToValidate="tbPassword" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_239 %>"></asp:RequiredFieldValidator>
	            </td>
            </tr>
            <tr runat="server" ID="ConfirmPasswordRow">
	            <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label7"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_236) %>&nbsp;</asp:Label>
                </td>
	            <td class="rightInputColumn">
                    <asp:TextBox ID="tbPasswordConfirmation" runat="server" TextMode="Password" MaxLength="127" autocomplete="off" ></asp:TextBox>
	            </td>
	            <td class="helpColumn">
                    <asp:RequiredFieldValidator ID="RequiredConfirmPasswordValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" ControlToValidate="tbPasswordConfirmation" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_239 %>"></asp:RequiredFieldValidator>
                    <asp:CompareValidator
                        ID="CompareConfirmPasswordValidator" runat="server" ErrorMessage="CompareConfirmPasswordValidator" ControlToCompare="tbPassword" Display="Dynamic" ControlToValidate="tbPasswordConfirmation" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_240 %>"></asp:CompareValidator>
	            </td>
            </tr>
            
            <tr runat="server" ID="PrivateKeyRow">
	            <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label4"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_PrivateKeyLabel) %>:&nbsp;</asp:Label>
                </td>
	            <td class="rightInputColumn">
                    <div id="AgentCertificateViaFileBlock">
                        <input type="file" id="PrivateKeyFile" name="file"
                            onchange="SW.Core.AddNode.AgentInfo.uploadCertificateFile('PrivateKeyFile', '<%= PrivateKeyValue.ClientID %>', '<%= PrivateKeyFileName.ClientID %>');" />
	                    <br/>
                        <a href="javascript:void(0);" onclick="SW.Core.AddNode.AgentInfo.pasteCertificateManually('<%= PrivateKeyValue.ClientID %>', '<%= PrivateKeyFileName.ClientID %>');">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_PastePrivateKeyManually) %>
                        </a>
                    </div>
                    <div id="AgentCertificateManualBlock" class="hidden">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_PrivateKeyPastedManually) %>
                        <br/>
                        <a href="javascript:void(0);" onclick="SW.Core.AddNode.AgentInfo.resetCertificate('PrivateKeyFile', '<%= PrivateKeyValue.ClientID %>', '<%= PrivateKeyFileName.ClientID %>');">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_UsePrivateKeyFromFile) %>
                        </a>
                    </div>
                    <div id="AgentCertificateResetBlock" class="hidden">
                        <span id="AgentCertificateFileName"></span>
                        <a href="javascript:void(0);" onclick="SW.Core.AddNode.AgentInfo.resetCertificate('PrivateKeyFile', '<%= PrivateKeyValue.ClientID %>', '<%= PrivateKeyFileName.ClientID %>');">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_ResetCertificate) %>
                        </a>
                    </div>
                    <asp:HiddenField runat="server" ID="PrivateKeyValue"/>
                    <asp:HiddenField runat="server" ID="PrivateKeyFileName"/>
                    <div id="certificatePasteDialog" class="hidden sw-agent-info-certificate-dialog">
                        <label for="certificatePasteTextarea"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_PastePrivateKeyTextboxLabel) %>:</label>
                        <br />
                        <textarea id="certificatePasteTextarea"></textarea>
                    </div>
                    <div id="certificateViewDialog" class="hidden sw-agent-info-certificate-dialog">
                        <pre id="certificateViewElement"></pre>
                    </div>
	            </td>
	            <td class="sw-agent-info-align-baseline helpColumn">
                    <a href="javascript:void(0);" onclick="SW.Core.AddNode.AgentInfo.viewPrivateKey('<%= PrivateKeyValue.ClientID %>');">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_ViewPrivateKeyLabel) %>
                    </a>
	            </td>
            </tr>

            <tr runat="server" ID="PrivateKeyPasswordRow">
	            <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label6"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_PrivateKeyPassword) %>:&nbsp;</asp:Label>
                </td>
	            <td class="rightInputColumn">
                    <asp:TextBox ID="tbPrivateKeyPassword" runat="server" TextMode="Password" MaxLength="127" autocomplete="off" ></asp:TextBox>
                    <br />
                    <span class="smallText">(<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_Optional) %>)</span>
	            </td>
	            <td class="helpColumn">
	            </td>
            </tr>
            
            <tr runat="server" ID="PrivateKeyForSavedCredentialsRow" Visible="False">
                <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label15"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_PrivateKeyLabel) %>:&nbsp;</asp:Label>
                </td>
	            <td class="rightInputColumn">
	                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_PrivateKeyStoredInCredentials) %>
                </td>
            </tr>

            <tr runat="server" id="useSudoCredentialsRow" class="sw-agent-info-use-sudo-row">
                <td class="sw-agent-info-use-sudo-cell" colspan="2">
                    <label><asp:CheckBox runat="server" ID="useSudoCheckbox" OnCheckedChanged="useSudoCheckbox_CheckedChanged" AutoPostBack="true" /><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_UseSudoCredentials) %></label>
                </td>
                <td rowspan="2" class="helpColumn" style="vertical-align: top; padding-left: 5px;">
                    <div class="sw-pg-hint-blue">
                        <div class="sw-pg-hint-body-info">
                            <span class="sw-pg-hint-text">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_DoINeedSudoCredentials) %>
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_SudoHelpText) %>
                                <a href="<%= DefaultSanitizer.SanitizeHtml(SudoCredentialsHelpUrl) %>" target="_blank" rel="noopener noreferrer"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_WhatCredentialsDoINeedForLinux) %></a>
                            </span>
                        </div>
                    </div>
	            </td>
            </tr>
            <tr>
                <td colspan="3"></td>
            </tr>
            <tr runat="server" id="sudoCredentialsTitleRow" Visible="false">
	            <td class="leftLabelColumn">
                    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_SudoCredentials) %></b>
                </td>
	        </tr>
            <tr runat="server" id="sudoCredentialsDropDownRow" Visible="false">
	            <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label8"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_234) %>&nbsp;</asp:Label>
                </td>
	            <td class="rightInputColumn">
                    <asp:DropDownList ID="DdlSudoCredentials" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DdlSudoCredentials_IndexChanged" >
                    </asp:DropDownList>
	            </td>
	        </tr>
            <tr runat="server" id="sudoCredentialsNameRow" Visible="false">
	            <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label9"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_235) %>&nbsp;</asp:Label>
                </td>
	            <td class="rightInputColumn">
                    <asp:TextBox ID="tbSudoCredentialName" runat="server" MaxLength=50 ></asp:TextBox>
	            </td>
	            <td>
                    <asp:RequiredFieldValidator ID="RequiredSudoCredentialNameValidator" runat="server" Display="Dynamic"  ErrorMessage="RequiredFieldValidator" ControlToValidate="tbSudoCredentialName" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_237 %>"></asp:RequiredFieldValidator>
	            </td>
            </tr>
            <tr runat="server" id="sudoCredentialsLoginRow" Visible="false">
	            <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label10"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AK0_165) %>&nbsp;</asp:Label>
                </td>
	            <td class="rightInputColumn">
                    <asp:TextBox ID="tbSudoLogin" runat="server" MaxLength=50></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredSudoUsernameValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" ControlToValidate="tbSudoLogin" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_238 %>" ></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr runat="server" id="sudoCredentialsPasswordRow" Visible="false">
	            <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label11"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_40) %>&nbsp;</asp:Label>
                </td>
	            <td class="rightInputColumn">
                    <asp:TextBox ID="tbSudoPassword" runat="server" TextMode="Password" MaxLength="127" autocomplete="off" ></asp:TextBox>
	            </td>
	            <td>
                    <asp:RequiredFieldValidator ID="RequiredSudoPasswordValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" ControlToValidate="tbSudoPassword" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_239 %>"></asp:RequiredFieldValidator>
	            </td>
            </tr>
            <tr runat="server" id="sudoCredentialsConfirmPasswordRow" Visible="false">
	            <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label12"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_236) %>&nbsp;</asp:Label>
                </td>
	            <td class="rightInputColumn">
                    <asp:TextBox ID="tbSudoPasswordConfirmation" runat="server" TextMode="Password" MaxLength="127" autocomplete="off" ></asp:TextBox>
	            </td>
	            <td>
                    <asp:RequiredFieldValidator ID="RequiredSudoPasswordConfirmationValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" ControlToValidate="tbSudoPasswordConfirmation" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_239 %>"></asp:RequiredFieldValidator>
                    <asp:CompareValidator
                        ID="CompareSudoPasswordValidator" runat="server" ErrorMessage="CompareConfirmPasswordValidator" ControlToCompare="tbSudoPassword" Display="Dynamic" ControlToValidate="tbSudoPasswordConfirmation" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_240 %>"></asp:CompareValidator>
	            </td>
            </tr>

            <tr runat="server" id="InstallPackageSelectionRow" Visible="false">
                <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label14"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_InstallPackageLabel) %>&nbsp;</asp:Label>
                </td>
	            <td class="rightInputColumn">
                    <asp:DropDownList ID="LinuxInstallPackagesDropdown" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DdlLinuxInstallPackages_IndexChanged" >
                    </asp:DropDownList>
	            </td>
            </tr> 
            <tr>
                <td colspan="2" class="leftLabelColumn"></td>
                <td rowspan="2" class="helpColumn" style="vertical-align: top; padding-left: 5px;">
                    <div class="sw-pg-hint-blue">
                        <div class="sw-pg-hint-body-info">
                            <span class="sw-pg-hint-text">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_MoreDeployMethods) %>
                                <a href="../../AgentManagement/Admin/DownloadAgent.aspx" target="_blank"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AppStack_LearnMore) %></a>                             
                            </span>
                        </div>
                    </div>
                </td>
            </tr> 
            <tr>
                <td colspan="3"></td>
            </tr>
            <tr runat="server" id="testButtonRow" >
                <td class="leftLabelColumn">
                    &nbsp;
                </td>
                <td class="rightInputColumn">
                    <orion:LocalizableButton ID="imbtnValidateAgent" runat="server" LocalizedText="Test"
                        OnClick="imbtnValidateAgent_Click" OnClientClick="ShowValidationProgress(); return true;" DisplayType="Small" />
                </td>
	            <td class="sw-agent-info-credential-test">
                    <div runat="server" id="ValidationSuccess" visible="false">
                        <div class="sw-pg-hint-green">
                            <div class="sw-pg-hint-body-confirm">
                                <span class="sw-pg-hint-text"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_CredentialsOK) %></span>
                            </div>
                        </div>
                    </div>
                    <div runat="server" id="ValidationError" visible="false">
                        <div class="sw-pg-hint-yellow">
                            <div class="sw-pg-hint-body-warning">
                                <span runat="server" id="ValidationErrorMessage" class="sw-pg-hint-text"></span>
                            </div>
                        </div>
                    </div>
                    <div runat="server" id="ValidationProgress" style="display:none;">
                        <div class="sw-agent-info-validation-progress">
                            <span class="sw-pg-hint-text"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_TestingCredentials) %></span>
                        </div>
                    </div>
	            </td>
            </tr>

            <tr runat="server" id="useSnmpCredentialsRow" class="sw-agent-info-use-snmp-row">
                <td class="sw-agent-info-use-snmp-cell" colspan="2">
                    <label><asp:CheckBox runat="server" ID="useSnmpCheckbox" OnCheckedChanged="useSnmpCheckbox_CheckedChanged" AutoPostBack="true" /><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_UseSnmpCredentials) %></label>
                </td>
                <td rowspan="2" class="helpColumn" style="vertical-align: top; padding-left: 5px;">
                    <div class="sw-pg-hint-blue">
                        <div class="sw-pg-hint-body-info">
                            <span class="sw-pg-hint-text">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_DoINeedSnmpCredentials) %>
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_SnmpCredentialsHelpText) %>
                                <a href="<%= DefaultSanitizer.SanitizeHtml(SnmpCredentialsHelpUrl) %>" target="_blank" rel="noopener noreferrer"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_SnmpCredentialsHelpLinkText) %></a>
                            </span>
                        </div>
                    </div>
	            </td>
            </tr>

            <tr runat="server" id="snmpCredentialsTitleRow" Visible="false">
	            <td class="leftLabelColumn">
                    <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_SnmpCredentials) %></b>
                </td>
	            <td>
	            </td>
	        </tr>

            <tr runat="server" id="snmpCredentialsSnmpInfoRow" Visible="false">
                <td colspan="3" style="padding-left: 10px">
                <orion:SNMPInfo runat="server" ID="SNMPInfo1" MultipleSelection="false" SNMPTitle=""
                    SNMPVersionDescription="SNM" HideValidateSnmpButton="true" HideAllow64bitCountersCheckbox="true" 
                    HideRWCommunityString="true" />                
                </td>
	        </tr>

            <tr>
                <td colspan="3">
                    <asp:UpdatePanel runat="server" ID="agentSnmpUpdatePanel" UpdateMode="Conditional"  Visible="false">
                        <ContentTemplate>
                            <div runat="server" id="agentSnmpValidationTrue" style="padding-left: 118px; background-color: #D6F6C6;
                                text-align: center" visible="false">
                                <img runat="server" id="agentSnmpValidationTrueIMG" src="~/Orion/images/nodemgmt_art/icons/icon_OK.gif" />
                                <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_55 %>" />
                            </div>
                            <div runat="server" id="agentSnmpValidationFalse" style="padding-left: 118px; background-color: #FEEE90;
                                text-align: center; color: Red" visible="false">
                                <img runat="server" id="agentSnmpValidationFalseIMG" src="~/Orion/images/nodemgmt_art/icons/icon_warning.gif" />
                                <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_56 %>" /> <asp:Label runat="server" ID="SNMPValidationFailedMessage" />
                            </div>
                            <table>
                                <tr>
                                    <td class="leftLabelColumn">
                                        &nbsp;</td>
                                    <td style="width: 50px; white-space:nowrap;">
                                        <orion:LocalizableButton ID="imbtnValidateAgentSNMP" runat="server" LocalizedText="Test" OnClick="imbtnValidateAgentSNMP_Click" DisplayType="Small" />
                                    </td>
                                    <td>
                                        <asp:UpdateProgress runat="server" ID="agentSnmpTestUpdateProgress" DynamicLayout="true" DisplayAfter="0" AssociatedUpdatePanelID="agentSnmpUpdatePanel" style="margin-left: 10px">
                                            <ProgressTemplate>
                                                    &nbsp;<img src="/Orion/images/animated_loading_sm3_whbg.gif" />
                                                    <span style="font-weight:bold;"><asp:Literal ID="Literal1" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_8 %>" /></span>                  
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

            <%-- 
            <tr>
	            <td class="leftLabelColumn">
                    <asp:Label runat="server" ID="Label2"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_Port) %>&nbsp;</asp:Label>
                </td>
	            <td class="rightInputColumn">
                    <asp:TextBox ID="tbPort" runat="server"></asp:TextBox>
	            </td>
	            <td>&nbsp;</td>
            </tr>
            --%>

        </table>

    </div>
</asp:Panel>
<asp:Panel runat="server" ID="deployAgentError">
    <span class="sw-suggestion-fail"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Settings_FipsEnabledError) %></span>
</asp:Panel>
</div>
