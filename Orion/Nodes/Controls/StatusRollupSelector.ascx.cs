﻿using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Shared;
using System;
using System.Web.UI.WebControls;

public partial class Orion_Nodes_Controls_RollupStatusModeSelector : System.Web.UI.UserControl
{
    private EvaluationMethod rollupMode = EvaluationMethod.Mixed;

    public EvaluationMethod RollupMode
    {
        get
        {
            return (EvaluationMethod)Enum.Parse(typeof(EvaluationMethod), RollupModeDropDown.SelectedValue);
        }
        set
        {
            rollupMode = value;

            if (RollupModeDropDown != null && RollupModeDropDown.Items.Count > 0)
                RollupModeDropDown.SelectedValue = rollupMode.ToString();
        }
    }

    public bool VisibleBySetting
    {
        get { return RollupModeWrapper.Visible; }
        private set { RollupModeWrapper.Visible = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            VisibleBySetting = (SettingsDAL.GetSetting("EnhancedNodeStatusCalculation")?.SettingValue ?? 0) == 1;

            RollupModeDropDown.Items.Add(
                    new ListItem(
                        Resources.CoreWebContent.StatusRollup_Best,
                        EvaluationMethod.Best.ToString()));

            RollupModeDropDown.Items.Add(
                new ListItem(
                    Resources.CoreWebContent.StatusRollup_Mixed,
                    EvaluationMethod.Mixed.ToString()));

            RollupModeDropDown.Items.Add(
                new ListItem(
                    Resources.CoreWebContent.StatusRollup_Worst,
                    EvaluationMethod.Worst.ToString()));

            foreach (ListItem item in RollupModeDropDown.Items)
                item.Attributes.Add("automation", item.Value);

            RollupModeDropDown.SelectedValue = rollupMode.ToString();
            RollupHelpLink.HRef = HelpHelper.GetHelpUrl("OrionCoreAGManagingDisplayNodeStatus");
        }
    }

    public bool MultipleSelection
    {
        get { return cbMultipleSelection.Visible; }
        set { cbMultipleSelection.Visible = value; }
    }

    public bool MultipleSelectionSelected
    {
        get { return cbMultipleSelection.Checked; }
        set
        {
            cbMultipleSelection.Checked = value;
            cbMultipleSelection_CheckedChanged(null, null);
        }
    }

    protected void cbMultipleSelection_CheckedChanged(object sender, EventArgs e)
    {
        RollupModeDropDown.Enabled = cbMultipleSelection.Checked;
        RollupModeDropDown.CssClass = RollupModeDropDown.Enabled ? string.Empty : "disabled";
    }
}