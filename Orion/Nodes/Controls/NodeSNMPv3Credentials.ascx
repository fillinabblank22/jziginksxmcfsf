<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeSNMPv3Credentials.ascx.cs"
    Inherits="Orion_Nodes_Controls_NodeSNMPv3Credentials" %>
<%@ Register Src="~/Orion/Controls/PasswordTextBox.ascx"  TagPrefix="orion" TagName="PasswordTextBox" %>
   
<table class="blueBox">
    <tr>
        <td class="contentBlockHeader" style="padding: 6px 0 0 10px;" colspan="2">
            <%= DefaultSanitizer.SanitizeHtml(this.Title) %>        
        </td>
    </tr>
    <tr>
        <td class="leftLabelColumn">
            <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_35 %>" />
        </td>
        <td>
            <asp:TextBox ID="tbSNMPv3Username" runat="server" AutoPostBack="True" OnTextChanged="OnCredentialDataChanged"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="leftLabelColumn">
            <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_36 %>" /></td>
        <td>
            <asp:TextBox ID="tbSNMPv3Context" runat="server" AutoPostBack="True" OnTextChanged="OnCredentialDataChanged"></asp:TextBox>
        </td>
    </tr>
      <tr>
                   <td style="padding-top: 0px;"></td>
                   <td style="padding-top: 0px;"><div class="helpfulText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_234) %></div></td>
                   </tr>
    <tr>
        <td style="padding: 10px 0 0 10px;" colspan="2">
            <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_37 %>" />
        </td>
    <tr>
        <td class="leftLabelColumn">
            <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_38 %>" />
        </td>
        <td>
            <asp:DropDownList ID="ddlAuthMethod" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAuthMethod_SelectedIndexChanged">
                <asp:ListItem Value="None" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_39 %>"></asp:ListItem>
                <asp:ListItem Value="MD5" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_75 %>"></asp:ListItem>
                <asp:ListItem Value="SHA1" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_76 %>"></asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="leftLabelColumn">
            <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_40 %>" />
        </td>
        <td>
            <orion:PasswordTextBox ID="tbPasswordKey" runat="server" IsEnabledPasswordTextBox="False" AutoPostBack="True" OnTextChanged="OnCredentialDataChanged"/>
            <br/>
        </td>
    </tr>
    <tr>
        <td></td>
        <td><asp:CheckBox ID="cbIsPasswordKey" runat="server" Enabled="false" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_57 %>" AutoPostBack="True" OnCheckedChanged="OnCredentialDataChanged"/></td>
    </tr>
    <tr>
                    <td style="padding-top: 0px;"></td>
                   <td style="padding-left: 5px; padding-top: 0px;"><div class="helpfulText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_235) %></div></td>
                   </tr>
    <tr>
        <td style="padding: 10px 0 0 10px;" colspan="2">
            <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_41 %>" />
        </td>
    </tr>
    <tr>
        <td class="leftLabelColumn">
            <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_38 %>" />
        </td>
        <td>
            <asp:DropDownList ID="ddlSecurityMethod" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSecurityMethod_SelectedIndexChanged" Enabled="False" >
                <asp:ListItem Value="None" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_39 %>"></asp:ListItem>
                <asp:ListItem Value="DES56" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_77 %>"></asp:ListItem>
                <asp:ListItem Value="AES128" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_78 %>"></asp:ListItem>
                <asp:ListItem Value="AES192" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_85 %>"></asp:ListItem>
                <asp:ListItem Value="AES256" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_86 %>"></asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="leftLabelColumn">
            <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_40 %>" />
        </td>
        <td>
             <orion:PasswordTextBox ID="tbSecurityPasswordKey" runat="server" IsEnabledPasswordTextBox="False" AutoPostBack="True" OnTextChanged="OnCredentialDataChanged"/>
        </td>
    </tr>
    <tr>
        <td></td>
        <td><asp:CheckBox ID="cbIsSecurityPasswordKey" runat="server" Enabled="false" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_57 %>"  AutoPostBack="True" OnCheckedChanged="OnCredentialDataChanged"/></td>
    </tr>
    <tr>
                   <td style="padding-top: 0px;"></td>
                   <td style="padding-left: 5px; padding-top: 0px;"><div class="helpfulText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_236) %></div></td>
                   </tr>
</table>
<table class="blueBox">
    <tr>
        <td class="contentBlockHeader" style="padding-left: 10px;" colspan="2">
            <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_42 %>" />
        </td>
    </tr>
    <tr>
        <td class="leftLabelColumn">
            <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_14 %>" />
        </td>
        <td>
            <asp:TextBox ID="txtCredentialsName" runat="server" Width="150px" 
                ValidationGroup="credentials" AutoPostBack="True" OnTextChanged="OnCredentialDataChanged"></asp:TextBox>            
            <orion:LocalizableButton ID="btnSave" runat="server" LocalizedText="Save" DisplayType="Small" onclick="btnSave_Click" />
        </td>
    </tr>
    <tr>
        <td class="leftLabelColumn">
            <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_43 %>" />
        </td>
        <td>
            <asp:DropDownList ID="ddlCredentialsSet" runat="server" Width="156px" AutoPostBack="True" DataTextField="Value" DataValueField="Key"
                onselectedindexchanged="ddlCredentialsSet_SelectedIndexChanged">
            </asp:DropDownList>
            <orion:LocalizableButton ID="btnDelete" runat="server" LocalizedText="Delete" DisplayType="Small" onclick="btnDelete_Click" />
        </td>
    </tr>
</table>
<asp:LinkButton runat="server" ID="lbtnInvoker" Text="" OnClick="Invoke"></asp:LinkButton>
