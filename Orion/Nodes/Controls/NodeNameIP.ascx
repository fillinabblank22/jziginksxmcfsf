<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeNameIP.ascx.cs" Inherits="Orion_Nodes_Controls_NodeNameIP" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>

<orion:Include ID="Include2" runat="server" File="NetPerfMon/resources/NodeDetails/NodeIPAddressesQuery.js" />
<orion:Include ID="Include3" runat="server" Framework="Ext" FrameworkVersion="4.0" />

<style type="text/css">
    .checkbox label
    {
        padding-left: 5px;
    }
</style>
 
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" >
    <Services>
        <asp:ServiceReference path="/Orion/Services/Information.asmx" />
    </Services>
</asp:ScriptManagerProxy>

<table id="MainTable" runat="server">
	<tr>
		<td class="leftLabelColumn">
		<asp:CheckBox ID="cbMultipleSelection" runat="server" AutoPostBack="True" OnCheckedChanged="cbMultipleSelection_CheckedChanged" />&nbsp;
		</td>
		<td class="rightInputColumn">&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="leftLabelColumn">
		<asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_14 %>" />
		</td>
		<td class="rightInputColumn">
		<asp:TextBox runat="server" ID="tbName" Width="250"></asp:TextBox>
		</td>
		<td class="helpfulText">&nbsp;</td>
	</tr>
	<tr>
		<td class="leftLabelColumn">
		<asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_34 %>" />
		</td>
		<td class="rightInputColumn"><asp:TextBox runat="server" ID="tbIP" Width="250" selectIP="tbIPID" AutoPostBack="True" OnTextChanged="IPAddress_TextChanged"></asp:TextBox>
        <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="tbIP"
				ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_64 %>" SetFocusOnError="True" ValidateEmptyText="true" OnServerValidate="IPAddress_Validate" Display="Dynamic"/></td>
		<td>
            <orion:LocalizableButton runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TP0_4 %>" ID="changePollingIPBTN" OnClientClick="OpenIPAddressDialog(); return false;"/>
        </td>
	</tr>
    <tr>
        <td class="leftLabelColumn">&nbsp;</td>
        <td class="helpfulText" colspan="2"><asp:Literal ID="Literal1" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_YK0_24%>" /></td>
    </tr>
    <tr>
        <td class="leftLabelColumn" colspan="3">&nbsp;</td>
    </tr>
	<tr>
		<td class="leftLabelColumn">&nbsp;</td>
		<td class="rightInputColumn" nowrap="nowrap" colspan="2"><asp:CheckBox runat="server" ID="cbDynamicIP" OnCheckedChanged="DynamicIP_CheckedChanged" AutoPostBack="true" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_65 %>" CssClass="checkbox"/></td>
	</tr>	
</table>

    <script type="text/javascript">
        var _dialog;
        var defaultValue = '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TP0_3) %>';
        var customSWISQuery = '<%= CustomSWISQuery %>';

        function OpenIPAddressDialog() {

            if (!_dialog) {
                _dialog = Ext4.create("Ext.Window", {
                    title: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TP0_4) %>',
                    width: 600,
                    height: 120,
                    id: 'window-dialog-change-ip',
                    layout: 'fit',
                    closable: true,
                    items: [
		                    Ext4.create("Ext.form.FormPanel", {
		                        autoHeight: true,
		                        items: [
                                    { html: '<br /><table><tr><td style="width:584px;padding:2px" valign="top"><div id="Search" style="float:right;"><input type="text" id="SearchInputID" style="font-style:italic;color:Gray;width:130px;" value="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TP0_3) %>" ></input></div><div style="clear:both"></div><div id="ErrorMsg-0"></div><table id="Grid-0" cellpadding="2" cellspacing="0" width="100%"><tr class="HeaderRow"></tr></table><div id="Pager-0" class="ReportHeader hidden" style="line-height: 15px; text-align: center; float:left"></div></td></tr></table>' }
			                    ]
		                    })
                            ],
                    closeAction: 'hide',
                    modal: true,
                    buttons: [{
                        text: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TP0_4) %>',
                        handler: function () { SelectIPAddress(); }
                    }, {
                        text: '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PCC_28) %>',
                        handler: function () { CloseIPAddressDialog(); }
                    }]
                });

                _dialog.show();
                $('#window-dialog-change-ip-body').css({ 'background': 'White' });

                var searchInput = $('#SearchInputID');
                var searchValue = searchInput.val();

                searchInput.focus(function () {
                    var value = searchInput.val();
                    if (defaultValue == value) {
                        searchInput.val('');
                        searchInput.css({ 'font-style': 'normal',
                            'color': 'Black'
                        });
                    }
                });
                searchInput.blur(function () {
                    var value = searchInput.val();
                    if (!value) {
                        searchInput.val(defaultValue);
                        searchInput.css({ 'font-style': 'italic',
                            'color': 'Gray'
                        });
                    }
                });

                searchInput.keyup(function checkSearchChanged() {
                    var currentValue = searchInput.val();
                    if ((currentValue) && currentValue != searchValue && currentValue != '' && currentValue != defaultValue) {
                        searchValue = searchInput.val();
                        SW.Core.Resources.NodeIPAddressesQuery.FilterInResult(0, 2, '<%= IP %>', searchValue, customSWISQuery);
                    } else
                        if (currentValue === '') {
                            SW.Core.Resources.NodeIPAddressesQuery.createTableFromQuery(0, 0, 5, '<%= IP %>', undefined, 2,
                        undefined, customSWISQuery);
                        }
                    });

                    _dialog.SetSearchValue = function (value) {
                        searchValue = value;
                    }

            } else {
                _dialog.show();
            }

            SW.Core.Resources.NodeIPAddressesQuery.createTableFromQuery(0, 0, 5, '<%= IP %>', undefined, 2,
                undefined, customSWISQuery, function () {
                    var height = $('#Grid-0').height() + 122;
                    _dialog.setHeight(height);
                });
            
        }

        function CloseIPAddressDialog() {
            if (_dialog == undefined)
                return;

            _dialog.hide();
            $('#SearchInputID').val(defaultValue).css(
                { 'font-style': 'italic',
                    'color': 'Gray'
                });

            _dialog.SetSearchValue(defaultValue);

        }

        function SelectIPAddress() {

            var selectedIP = $('input[name=rbGroup-0]:checked', '#Grid-0').val();
            var inputIP = $("input[selectIP='tbIPID']");
            if ((selectedIP != undefined) && (inputIP != undefined)) {
                inputIP.val(selectedIP.split(' ')[0]);
            }            

            CloseIPAddressDialog();
        }
           
           
    </script>