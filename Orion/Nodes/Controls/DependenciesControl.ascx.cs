using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Script.Serialization;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Dependencies;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Nodes_Controls_DependenciesControl : System.Web.UI.UserControl
{
    private string _objectPrefix;
    private List<int> _objectIds;

    public string ObjectPrefix
    {
        set
        {
            _objectPrefix = value;
        }
        get
        {
            return _objectPrefix;
        }
    }

    public string ObjectIdsInJson
    {
        set { _objectIds = new JavaScriptSerializer().Deserialize<List<int>>(value); }
        get { return new JavaScriptSerializer().Serialize(_objectIds); }
    }

    public List<int> ObjectIds
    {
        set
        {
            _objectIds = value;
        }
        get
        {
            return _objectIds;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (_objectIds.Count > 1 || _objectIds.Count == 0)
            {
                return;
            }
            var depData = LoadDependencies();
            if (depData != null)
            {
                repParentDep.DataSource = depData.Select("Type='Parent'");
                repParentDep.DataBind();

                repChildDep.DataSource = depData.Select("Type='Child'");
                repChildDep.DataBind();
            }
        }
    }

    private DataTable LoadDependencies()
    {
        DataTable res = null;
        DataTable groups;
        int objId = _objectIds[0];

        var netObject = string.Format("{0}:{1}", _objectPrefix, objId);

        using (var dal = new DependenciesDAL())
        {
            res = dal.GetDependenciesData(objId, NetObjectHelper.GetNetObjectEntity(netObject));
            groups = dal.GetGroupDependencies();
        }

        foreach (DataRow row in groups.Rows)
        {
            var depType = row["Type"].ToString();
            var id = UriHelper.GetId(((depType.Equals("Parent")) ? row["ParentUri"] : row["ChildUri"]).ToString());
            var depId = int.Parse(row["DependencyId"].ToString());
            var cacheId = string.Format("{0}_{1}", depType.Equals("Parent") ? "P" : "C", depId);

            using (var dal = new ContainersDAL())
            {
                bool addGroup = false;
                if (!DependencyCache.Store.ContainsKey(cacheId))
                {
                    DependencyCache.Store[cacheId] = new Dictionary<string, DependencyCacheItem>();
                }

                if (DependencyCache.Store[cacheId].ContainsKey(netObject))
                {
                    var updateCache = DependencyCache.Store[cacheId][netObject].ShouldUpdate;
                    if (updateCache)
                    {
                        DependencyCache.Store[cacheId][netObject] = new DependencyCacheItem(depId, row["Name"].ToString(), row["ParentUri"].ToString(),
                            row["ChildUri"].ToString(), int.Parse(row["Status"].ToString()), depType, netObject, DateTime.UtcNow, (bool)row["AutoManaged"]);
                        addGroup = DependencyCache.UpdateCache(cacheId, dal, netObject, id, depId, row["Name"].ToString(), row["ParentUri"].ToString(), 
                            row["ChildUri"].ToString(), int.Parse(row["Status"].ToString()), depType, (bool)row["AutoManaged"]);
                    }
                    else
                    {
                        addGroup = DependencyCache.Store[cacheId][netObject].GroupsIds.Contains(id);
                    }
                }
                else
                {
                    addGroup = DependencyCache.UpdateCache(cacheId, dal, netObject, id, depId, row["Name"].ToString(), row["ParentUri"].ToString(),
                        row["ChildUri"].ToString(), int.Parse(row["Status"].ToString()), depType, (bool)row["AutoManaged"]);
                }

                if (addGroup)
                {
                    DataRow newRow = res.NewRow();
                    newRow["DependencyId"] = row["DependencyId"];
                    newRow["Name"] = row["Name"];
                    newRow["ParentUri"] = row["ParentUri"];
                    newRow["ChildUri"] = row["ChildUri"];
                    newRow["Type"] = row["Type"];
                    newRow["AutoManaged"] = row["AutoManaged"];

                    res.Rows.Add(newRow);
                }
            }
        }

        res.DefaultView.Sort = "Type Desc, Name Asc";
        res = res.DefaultView.ToTable();

        return res;
    }
}
