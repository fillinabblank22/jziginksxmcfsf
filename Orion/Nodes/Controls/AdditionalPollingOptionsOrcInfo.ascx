<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdditionalPollingOptionsOrcInfo.ascx.cs" Inherits="Orion_Nodes_Controls_AdditionalPollingOptionsOrcInfo" %>

<div class="contentBlock">
    <div class="sw-suggestion sw-suggestion-info" style="display:inline-block;">
        <span class="sw-suggestion-icon"></span>
        <%= Resources.CoreWebContent.WEBDATA_AdditionalPollingOptionsOrcInfo_Content %>
        <a href="<%= OrcHelpLink %>" target="_blank" rel="noopener noreferrer">
            <%= Resources.CoreWebContent.WEBDATA_AdditionalPollingOptionsOrcInfo_Link %>
        </a>
    </div>
</div>
