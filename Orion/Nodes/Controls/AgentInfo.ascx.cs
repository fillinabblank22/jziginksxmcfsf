using SolarWinds.AgentManagement.Contract;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Agent;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Discovery.Contract.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Agent;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_Nodes_Controls_AgentInfo : System.Web.UI.UserControl
{
    [Serializable]
    protected class AgentNodeInfo
    {
        public int NodeID;
        public string IpAddress;
        public string Dns;
        public string Caption;
        public int EngineID;
        public int AgentId;
    }

    protected class ValidationResult
    {
        public bool Valid;
        public string ValidationText;
    }

    public enum ControlViewMode
    {
        Windows,
        Linux
    }

    private enum CredentialType
    {
        UsernamePassword,
        PrivateKey
    }

    private static readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    private const string NodeInfoKey = "AgentNodeInfo";
    private const string ModeKey = "AgentInfoExecutionMode";
    private const int NewCredentialId = 0;
    private const string asteriskPass = "**********";

    private NodePropertyPluginExecutionMode mode
    {
        get { return (NodePropertyPluginExecutionMode)ViewState[ModeKey]; }
        set { ViewState[ModeKey] = value; }
    }

    private List<UsernamePasswordCredential> credentialsList = new List<UsernamePasswordCredential>();
    private List<SshPrivateKeyCredential> sshCredentialsList = new List<SshPrivateKeyCredential>();
    private string privateKeyFromPostback = null;

    protected AgentNodeInfo NodeInfo
    {
        get { return ViewState[NodeInfoKey] as AgentNodeInfo; }
        set { ViewState[NodeInfoKey] = value; }
    }
    public bool MultipleSelection
    {
        get { return (bool)ViewState["MultipleSelection"]; }
        set
        {
            ViewState["MultipleSelection"] = value;
            imbtnValidateAgent.Visible = !value;
            imbtnValidateAgent.Enabled = !value;
        }
    }

    private void UpdateView()
    {
        PrivateKeyTypeRadio.Visible = (!AgentInstallAttempted && IsLinux);
        useSudoCredentialsRow.Visible = (!AgentInstallAttempted && IsLinux);

        useSnmpCredentialsRow.Visible = !HideSnmpCredentialsEntry && IsLinux;

        windowsUsernameHelpRow.Visible = (!AgentInstallAttempted && !IsLinux);
        credentialTypeRow.Visible = (!AgentInstallAttempted && IsLinux);
        UsernamePasswordCredentialTypeRadio.Text = !IsLinux
            ? Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_UsernameAndPasswordCredential
            : Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_SSHCredential;
    }

    public Orion_Nodes_Controls_NodeSNMP SnmpInfo
    {
        get { return this.SNMPInfo1; }
    }

    private AgentDeploymentInfo _deploymentInfo;
    public AgentDeploymentInfo DeploymentInfo 
    {
        get { return _deploymentInfo; }
        private set
        {
            _deploymentInfo = value;
            if (_deploymentInfo != null && _deploymentInfo.Agent != null)
            {
                IsLinux = (_deploymentInfo.Agent.OsType != SolarWinds.AgentManagement.Contract.Models.OsType.Windows) &&
                    (_deploymentInfo.Agent.OsType != SolarWinds.AgentManagement.Contract.Models.OsType.Unknown);
            }

        }
    }

    public AgentDetectionInfo DetectionInfo { get; private set; }

    public bool IsLinuxAgentEnabled { get; private set; }

    public string AgentPollingMethodLabel { get; set; }

    public string AgentPollingMethodDescription { get; set; }

    public string AgentPollingMethodHelpLink { get; set; }

    public bool HideSnmpCredentialsEntry { get; private set; }

    public bool HideSnmpTestButton { get; set; }

    protected void Page_Init(object sender, EventArgs e)
    {
        var manager = new CredentialManager();
        credentialsList = manager.GetCredentials<UsernamePasswordCredential>(CoreConstants.CoreCredentialOwner).ToList();
        credentialsList.Insert(0, new UsernamePasswordCredential()
        {
            Name = Resources.CoreWebContent.WEBCODE_TM0_93,
            ID = (int)NewCredentialId
        });
        sshCredentialsList = manager.GetCredentials<SshPrivateKeyCredential>(CoreConstants.CoreCredentialOwner).ToList();
        sshCredentialsList.Insert(0, new SshPrivateKeyCredential()
        {
            Name = Resources.CoreWebContent.WEBCODE_TM0_93,
            ID = (int)NewCredentialId
        });

        UpdateUsernameAndPrivateKeyCredentialControls(true);
        ChangeSudoCredentialsValidatorsState(false);

        HideOrShowLinuxAgentControls();

        HideSnmpCredentialsEntry = new AgentManager().HideSnmpCredentialsEntry(SwisConnectionProxyPool.GetSystemCreator());
    }

    private void HideOrShowLinuxAgentControls()
    {
        IsLinuxAgentEnabled = new AgentManager().IsLinuxAgentEnabled(SwisConnectionProxyPool.GetSystemCreator());
        if (IsLinuxAgentEnabled)
        {
            AgentPollingMethodLabel = Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Name;
            AgentPollingMethodDescription = Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_Hint;
        }
        else
        {
            AgentPollingMethodLabel = Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_WindowsOnly_Name;
            AgentPollingMethodDescription = Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_WindowsOnly_Hint;
        }
        AgentPollingMethodHelpLink = HelpHelper.GetHelpUrl("Orion","OrionAgent-AgentPollingMethod");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        UsernamePasswordCredentialTypeRadio.Text = !IsLinux
            ? Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_UsernameAndPasswordCredential
            : Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_SSHCredential;
        PrivateKeyTypeRadio.Text = Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_CertificateCredential;
        OSTypeWindows.Text = Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_OSTypeWindows;
        OSTypeLinux.Text = Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_OSTypeLinux;

        if (IsPostBack)
        {
            this.InstallPackageId = LinuxInstallPackagesDropdown.SelectedValue;

            if (CredentialsId == NewCredentialId)
            {
                StorePassword();
            }
            if (SudoCredentialsId == NewCredentialId)
            {
                StoreSudoPassword();
            }
        }
        else
        {
            LoadLinuxInstallPackages();
        }
    }

    private void LoadLinuxInstallPackages()
    {
        LinuxInstallPackagesDropdown.Items.Clear();

        var packages = new AgentManager().GetLinuxInstallPackages(SwisConnectionProxyPool.GetSystemCreator());

        LinuxInstallPackagesDropdown.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_InstallPackageChoosePrompt, string.Empty));

        foreach (AgentInstallPackage package in packages)
        {
            LinuxInstallPackagesDropdown.Items.Add(new ListItem(package.Name, package.PackageId));
        }
    }

    private void StorePassword()
    {
        if (IsPrivateKey)
        {
            PlainPrivateKeyPassword = PrivateKeyPassword;
            EncodePassword(tbPrivateKeyPassword, PlainPrivateKeyPassword);
        }
        else
        {
            PlainPassword = Password;
            EncodePassword(tbPassword, PlainPassword);
            EncodePassword(tbPasswordConfirmation, PlainPassword);
        }
    }

    private void StoreSudoPassword()
    {
        PlainSudoPassword = SudoPassword;
        EncodePassword(tbSudoPassword, PlainSudoPassword);
        EncodePassword(tbSudoPasswordConfirmation, PlainSudoPassword);
    }

    /// <summary>
    /// Set password fields values to asterisks and store plain password to PlainPassword property
    /// </summary>
    private void EncodePassword(TextBox field, string value)
    {
        string passBlock = !string.IsNullOrEmpty(value) ? asteriskPass : string.Empty;
            field.Text =
            field.Attributes["value"] = passBlock;
    }

    protected void DdlAgentCredentials_IndexChanged(object sender, EventArgs e)
    {
        SetCredentialsFromDropDown();
    }

    protected void DdlLinuxInstallPackages_IndexChanged(object sender, EventArgs e)
    {
        this.InstallPackageId = LinuxInstallPackagesDropdown.SelectedValue;

        ValidationResult result = new ValidationResult();
        if (string.IsNullOrEmpty(InstallPackageId))
        {
            result.Valid = false;
            result.ValidationText = Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_CredentialsFailed;
        }
        else
        {
            result.Valid = true;
            result.ValidationText = Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_CredentialsOK;
        }
        UpdateValidationResult(result);
    }

    protected void DdlSudoCredentials_IndexChanged(object sender, EventArgs e)
    {
        SetSudoCredentialsFromDropDown();
    }

    protected void imbtnValidateAgent_Click(object sender, EventArgs e)
    {
        Validate();
    }

    protected void imbtnValidateAgentSNMP_Click(object sender, EventArgs e)
    {
        if (NodeInfo == null)
        {
            SetSNMPValidationBar(false, Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_ValidateAgentNullNodeInfo);
            return;
        }

        bool validationSucceeded = false;
        string msg = string.Empty;

        try
        {
            using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
            {
                //Create and populate the credentials instance.
                var credentials = new SnmpCredentialsV2 { CommunityString = SNMPInfo1.CommunityString };

                if (SNMPInfo1.SNMPVersion == SNMPVersion.SNMP3)
                {
                    credentials.SNMPv3UserName = SNMPInfo1.SNMPv3Username;
                    credentials.SnmpV3Context = SNMPInfo1.SNMPv3Context;
                    credentials.SNMPv3AuthType = SNMPInfo1.SNMPv3AuthType;
                    credentials.SNMPV3AuthKeyIsPwd = SNMPInfo1.AuthKeyIsPwd;
                    credentials.SNMPv3AuthPassword = SNMPInfo1.AuthPasswordKey;
                    credentials.SNMPv3PrivacyType = SNMPInfo1.SNMPv3PrivacyType;
                    credentials.SNMPV3PrivKeyIsPwd = SNMPInfo1.PrivKeyIsPwd;
                    credentials.SNMPv3PrivacyPassword = SNMPInfo1.PrivacyPasswordKey;

                }

                var testResult = proxy.TestSnmpCredentialsOnAgent(NodeInfo.NodeID, SNMPInfo1.SNMPPort, credentials);

                validationSucceeded = testResult.Success;
                msg = testResult.Message;
            }
        }
        catch (Exception ex)
        {
            validationSucceeded = false;
            msg = ex is i18nReadyException ? (ex as i18nReadyException).UserMessage : ex.Message;
        }

        SetSNMPValidationBar(validationSucceeded, msg);
    }

    protected void CredentialTypeChanged(object sender, EventArgs e)
    {
        SetControls(true);
        UpdateUsernameAndPrivateKeyCredentialControls(true);
    }

    private void UpdateOsTypeTabs()
    {
        if (IsLinux)
        {
            OSTypeWindows.CssClass = "";
            OSTypeLinux.CssClass = "active";
        }
        else
        {
            OSTypeWindows.CssClass = "active";
            OSTypeLinux.CssClass = "";
        }
    }

    protected void OsTypeChanged(object sender, EventArgs e)
    {
        UpdateView();
        UpdateOsTypeTabs();
        UpdateUsernameAndPrivateKeyCredentialControls(true);

        SetControls(true);
        RefreshSudoSection();
        RefreshSnmpSection();
    }

    private void ClearCredentialFields()
    {
        CredentialName = string.Empty;
        Login = string.Empty;
        tbPassword.Text = string.Empty;
        tbPasswordConfirmation.Text = string.Empty;
        tbPrivateKeyPassword.Text = string.Empty;
        tbPassword.Attributes["value"] = string.Empty;
        tbPasswordConfirmation.Attributes["value"] = string.Empty;
        privateKeyFromPostback = string.Empty;
        PrivateKeyValue.Value = string.Empty;
        PrivateKeyFileName.Value = string.Empty;
    }

    private void ClearSudoCredentialFields()
    {
        SudoCredentialName = string.Empty;
        SudoLogin = string.Empty;
        tbSudoPassword.Text = string.Empty;
        tbSudoPasswordConfirmation.Text = string.Empty;
        tbSudoPassword.Attributes["value"] = string.Empty;
        tbSudoPasswordConfirmation.Attributes["value"] = string.Empty;
    }

    public void UpdateUsernameAndPrivateKeyCredentialControls(bool loadCredentials)
    {
        if (loadCredentials)
        {
            if (IsPrivateKey)
                LoadSshPrivateKeyCredentials();
            else
                LoadUsernamePasswordCredentials();

            //Since we just reset the contents of these dropdown controls, let's clear the credential fields, so everything remains consistent.
            SetCredentialsFromDropDown();
        }

        bool isNewCredentials = CredentialsId == NewCredentialId;

        PasswordRow.Visible = (IsPrivateKey ? false : !AgentInstallAttempted);
        ConfirmPasswordRow.Visible = (IsPrivateKey ? false : !AgentInstallAttempted);
        PrivateKeyRow.Visible = isNewCredentials && (IsPrivateKey ? !AgentInstallAttempted : false);
        PrivateKeyPasswordRow.Visible = isNewCredentials && (IsPrivateKey ? !AgentInstallAttempted : false);
        PrivateKeyForSavedCredentialsRow.Visible = !isNewCredentials && (IsPrivateKey ? !AgentInstallAttempted : false);
    }


    private CredentialType SelectedCredentialType
    {
        get
        {
            return PrivateKeyTypeRadio.Checked && IsLinux ? CredentialType.PrivateKey : CredentialType.UsernamePassword;
        }
    }

    private bool IsPrivateKey { get { return SelectedCredentialType == CredentialType.PrivateKey; } }

    public bool IsLinux
    {
        get { return OSTypeLinux.Checked; }
        set 
        { 
            OSTypeLinux.Checked = value;
            OSTypeWindows.Checked = !value;
            UpdateOsTypeTabs();
            UpdateView();
        }
    }


    private int CredentialsId
    {
        get
        {
            int credentialId;
            if (!int.TryParse(DdlAgentCredentials.SelectedValue, out credentialId))
                credentialId = NewCredentialId;

            return credentialId;
        }
    }

    public string CredentialName
    {
        get
        {
            return tbCaption.Text;
        }
        set
        {
            tbCaption.Text = value;
        }
    }

    public string Login
    {
        get { return tbLogin.Text; }
        set { tbLogin.Text = value; }
    }

    public string Password
    {
        get
        {
            return tbPassword.Text == asteriskPass ? PlainPassword : tbPassword.Text;
        }
        set
        {
            PlainPassword = value;
            EncodePassword(tbPassword, value);
            EncodePassword(tbPasswordConfirmation, value);
        }
    }

    public string PlainPassword
    {
        get
        {
            return NodeWorkflowHelper.GetPageSessionValue("Agent.NodeManagement.PlainPassword")?.ToString();
        }
        set
        {
            NodeWorkflowHelper.SetPageSessionValue("Agent.NodeManagement.PlainPassword", value);
        }
    }

    public string PrivateKey
    {
        get
        {
            return PrivateKeyValue.Value;
        }
    }

    public string PrivateKeyPassword
    {
        get
        {
            return tbPrivateKeyPassword.Text == asteriskPass ? PlainPrivateKeyPassword : tbPrivateKeyPassword.Text;
        }
        set
        {
            PlainPrivateKeyPassword = value;
            EncodePassword(tbPrivateKeyPassword, value);
        }
    }
    
    public string PlainPrivateKeyPassword
    {
        get
        {
            return (string)Session["Agent.NodeManagement.PlainPrivateKeyPassword"];
        }
        set
        {
            Session["Agent.NodeManagement.PlainPrivateKeyPassword"] = value;
        }
    }

    private bool UseSudo
    {
        get { return useSudoCheckbox.Checked && IsLinux; }
    }

    private int SudoCredentialsId
    {
        get
        {
            int credentialId;
            if (!int.TryParse(DdlSudoCredentials.SelectedValue, out credentialId))
                credentialId = NewCredentialId;

            return credentialId;
        }
    }

    public string SudoCredentialName
    {
        get
        {
            return tbSudoCredentialName.Text;
        }
        set
        {
            tbSudoCredentialName.Text = value;
        }
    }

    public string SudoLogin
    {
        get { return tbSudoLogin.Text; }
        set { tbSudoLogin.Text = value; }
    }

    public string SudoPassword
    {
        get
        {
            return tbSudoPassword.Text == asteriskPass ? PlainSudoPassword : tbSudoPassword.Text;
        }
        set
        {
            PlainSudoPassword = value;
            EncodePassword(tbSudoPassword, value);
            EncodePassword(tbSudoPasswordConfirmation, value);
        }
    }

    public string PlainSudoPassword
    {
        get
        {
            return (string)Session["Agent.NodeManagement.PlainSudoPassword"];
        }
        set
        {
            Session["Agent.NodeManagement.PlainSudoPassword"] = value;
        }
    }

    public string InstallPackageId { get; set; }

    public AgentDeploymentCredentials GetEnteredCredentials()
    {
        AgentDeploymentCredentials credentials = new AgentDeploymentCredentials();

        if (IsPrivateKey)
        {
            LoadPrivateKeyCredential(credentials);
        }
        else
        {
            LoadUsernamePasswordCredential(credentials);
        }

        if (UseSudo)
        {
            LoadSudoCredential(credentials);
        }

        return credentials;
    }

    private void LoadSudoCredential(AgentDeploymentCredentials credentials)
    {
        if (SudoCredentialsId != NewCredentialId)
        {
            var existingCredentials = credentialsList.First(x => x.ID == SudoCredentialsId);
            credentials.AdditionalUsername = existingCredentials.Username;
            credentials.AdditionalPassword = existingCredentials.Password;
        }
        else
        {
            credentials.AdditionalUsername = SudoLogin;
            credentials.AdditionalPassword = SudoPassword;
        }
    }

    private void LoadUsernamePasswordCredential(AgentDeploymentCredentials credentials)
    {
        if (CredentialsId != NewCredentialId)
        {
            var existingCredentials = credentialsList.First(x => x.ID == CredentialsId);
            credentials.Username = existingCredentials.Username;
            credentials.Password = existingCredentials.Password;
        }
        else
        {
            credentials.Username = Login;
            credentials.Password = Password;
        }
    }

    private void LoadPrivateKeyCredential(AgentDeploymentCredentials credentials)
    {
        credentials.PasswordIsPrivateKey = true;

        if (CredentialsId != NewCredentialId)
        {
            var existingCredentials = sshCredentialsList.First(x => x.ID == CredentialsId);
            credentials.Username = existingCredentials.Username;
            credentials.Password = existingCredentials.PrivateKey;
            credentials.PrivateKeyPassword = existingCredentials.PrivateKeyPassword;
        }
        else
        {
            credentials.Username = Login;
            credentials.Password = PrivateKey;
            credentials.PrivateKeyPassword = PrivateKeyPassword;
        }
    }

    public bool UseSnmp
    {
        get { return useSnmpCheckbox.Checked && IsLinux; }
        set { useSnmpCheckbox.Checked = value; }
    }

    public bool HideCredentialsTable { get; set; }

    private bool AgentInstallAttempted
    { 
        get 
        {
            return DeploymentInfo != null;
        } 
    }

    private bool AgentInstalledSuccessfully 
    { 
        get 
        {
            return DeploymentInfo?.StatusInfo.Status == AgentDeploymentStatus.Finished; 
        } 
    }

    private bool DisplaySnmpCredentialsOnly
    {
        get
        {
            return IsLinux && AgentInstallAttempted;
        }
    }

    //The parameter 'agentId' is 0 when caller indicates that the agent isn't installed.  A -1 value implies that the argument was defaulted.
    public void DetectAgent(Node node, int agentId = -1)
    {
        if (node == null)
        {
            NodeInfo = null;
            NodeWorkflowHelper.AgentCredential = null;
            DeploymentInfo = null;
            return;
        }

        NodeWorkflowHelper.AgentCredential = (agentId == -1 ? null : new SolarWinds.Orion.Discovery.Contract.Models.AgentManagementCredential()
                                                                        {
                                                                            AgentId = agentId
                                                                        });

        NodeInfo = new AgentNodeInfo()
        {
            NodeID = node.ID,
            EngineID = node.EngineID,
            IpAddress = node.IpAddress,
            Dns = node.DNS,
            Caption = node.Caption
        };

        DeploymentInfo = DetectAgent();
    }

    private AgentDeploymentInfo DetectAgent()
    {
        var node = NodeInfo;
        var agentCredential = NodeWorkflowHelper.AgentCredential;

        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            AgentInfo agent = null;

            //first check if AgentId is set. It means Node is created for existing agent (e.g. 'Manage as Node' functionality in Agent Management)
            if (agentCredential != null && agentCredential.AgentId > 0)
            {
                agent = proxy.GetAgentInfo(agentCredential.AgentId);
            }

            if (agent == null && node != null)
            {
                //if NodeID is set, try to find Agent by NodeId
                if (node.NodeID > 0)
                {
                    agent = proxy.GetAgentInfoByNodeId(node.NodeID);
                }

                if (agent == null && !(string.IsNullOrEmpty(node.IpAddress) && string.IsNullOrEmpty(node.Dns)))
                {
                    //agent still not found, try to find it by IP Address  or hostname
                    agent = proxy.DetectAgent(node.IpAddress, node.Dns);
                    //TODO RH consider scenario when more agents can match the criteria
                }
            }

            AgentDeploymentInfo result = agent != null ? proxy.GetAgentDeploymentInfo(agent.AgentId) : null;
            if (result != null && (agentCredential == null || agentCredential.AgentGuid == Guid.Empty))
            {
                // NodeWorkflowHelper.AgentCredential structure is incomplete - let's fix it to allow validation tests on plugins
                agentCredential = new AgentManagementCredential
                    {
                        AgentGuid = result.Agent.AgentGuid,
                        AgentId = result.Agent.AgentId,
                        Plugins = result.Agent.Plugins.Select(p => p.PluginId).ToArray()
                    };
                NodeWorkflowHelper.AgentCredential = agentCredential;
            }
            return result;
        }
    }

    private UsernamePasswordCredential CreateNewUsernamePasswordCredentials(string name, string username, string password)
    {
        UsernamePasswordCredential credential = new UsernamePasswordCredential()
        {
            Name = name,
            Username = username,
            Password = password
        };

        CredentialManager credMan = new CredentialManager();
        credMan.AddCredential<UsernamePasswordCredential>(CoreConstants.CoreCredentialOwner, credential);
        return credential;
    }

    private SshPrivateKeyCredential CreateNewSshPrivateKeyCredentials(string name, string username, string privateKey, string privateKeyPassword)
    {
        SshPrivateKeyCredential credential = new SshPrivateKeyCredential()
        {
            Name = name,
            Username = username,
            PrivateKey = privateKey,
            PrivateKeyPassword = privateKeyPassword
        };

        CredentialManager credMan = new CredentialManager();
        credMan.AddCredential<SshPrivateKeyCredential>(CoreConstants.CoreCredentialOwner, credential);
        return credential;
    }

    private void LoadUsernamePasswordCredentials()
    {
        DdlAgentCredentials.Items.Clear();
        DdlSudoCredentials.Items.Clear();
        
        credentialsList.ForEach(c =>
        {
            DdlAgentCredentials.Items.Add(new ListItem(c.Name, c.ID.Value.ToString()));
            DdlSudoCredentials.Items.Add(new ListItem(c.Name, c.ID.Value.ToString()));
        });
    }

    private void LoadSshPrivateKeyCredentials()
    {
        DdlAgentCredentials.Items.Clear();
        sshCredentialsList.ForEach(c => DdlAgentCredentials.Items.Add(new ListItem(c.Name, c.ID.Value.ToString())));
    }

    private void SetCredentialsFromDropDown()
    {
        long credentialId = CredentialsId;
        if (credentialId == NewCredentialId)
        {
            ClearCredentialFields();

            SetControls(true);
            ChangePrimaryCredentialsValidatorsState(true);
        }
        else
        {
            if (IsPrivateKey)
            {
                SshPrivateKeyCredential selected = sshCredentialsList.First(c => c.ID == credentialId);
                this.CredentialName = selected.Name;
                this.Login = selected.Username;
            }
            else
            {
                UsernamePasswordCredential selected = credentialsList.First(c => c.ID == credentialId);
                this.CredentialName = selected.Name;
                this.Login = selected.Username;
                this.tbPassword.Text = asteriskPass;
                this.tbPasswordConfirmation.Text = asteriskPass;
                this.tbPassword.Attributes["value"] = asteriskPass;
                this.tbPasswordConfirmation.Attributes["value"] = asteriskPass;
            }

            SetControls(false);
            ChangePrimaryCredentialsValidatorsState(false);
        }
    }

    private void SetSudoCredentialsFromDropDown()
    {
        long credentialId = SudoCredentialsId;
        if (credentialId == NewCredentialId)
        {
            ClearSudoCredentialFields();

            SetSudoControls(true);
            ChangeSudoCredentialsValidatorsState(true);
        }
        else
        {
            UsernamePasswordCredential selected = credentialsList.First(c => c.ID == credentialId);
            this.SudoCredentialName = selected.Name;
            this.SudoLogin = selected.Username;
            this.tbSudoPassword.Text = asteriskPass;
            this.tbSudoPasswordConfirmation.Text = asteriskPass;
            this.tbSudoPassword.Attributes["value"] = asteriskPass;
            this.tbSudoPasswordConfirmation.Attributes["value"] = asteriskPass;

            SetSudoControls(false);
            ChangeSudoCredentialsValidatorsState(false);
        }
    }

    public void SetAgentInfo()
    {
        //set all to false by default
        agentInstalledInfo.Visible = false;
        agentProgressInfo.Visible = false;
        agentWarning.Visible = false;
        agentError.Visible = false;

        //check if remote deployment is possible
        if (EnginesDAL.IsFIPSModeEnabledOnPrimaryEngine())
        {
            deployAgentDiv.Visible = false;
            deployAgentError.Visible = true;
        } 
        else
        {
            deployAgentDiv.Visible = true;
            deployAgentError.Visible = false;

            //Set the visiblity of all sub-elements of the credentials table
            agentSettingsTitleRow.Visible = !AgentInstallAttempted;
            AgentTargetOsRow.Visible = !AgentInstallAttempted;
            AgentSpacerRow.Visible = !AgentInstallAttempted;
            ExistingCredentialRow.Visible = !AgentInstallAttempted;
            CredentialNameRow.Visible = !AgentInstallAttempted;
            UserNameRow.Visible = !AgentInstallAttempted;
            UpdateUsernameAndPrivateKeyCredentialControls(false);
            testButtonRow.Visible = !AgentInstallAttempted;
            credentialsTable.Visible = !HideCredentialsTable && (!AgentInstallAttempted || IsLinux);
            UpdateView();
            RefreshSnmpSection();
        }

        if (DeploymentInfo != null)
        {
            deployAgentError.Visible = false;
            switch (DeploymentInfo.StatusInfo.Status)
            {
                case AgentDeploymentStatus.Finished:
                    agentInstalledInfo.Visible = true;
                    break;
                case AgentDeploymentStatus.InProgress:
                case AgentDeploymentStatus.PendingPluginInstallation:
                    credentialsTable.Visible = false;
                    agentProgressInfo.Visible = true;
                    agentProgressInfoText.InnerText = Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_InstallInProgress;
                    break;
                case AgentDeploymentStatus.Failed:
                    credentialsTable.Visible = false;
                    if (DeploymentInfo.Agent.ConnectionStatus == (int)ConnectionStatus.DeploymentFailed)
                    {
                        //agent deployment failed
                        agentErrorText.InnerHtml = Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_AgentDeploymentFailed;
                    }
                    else if (DeploymentInfo.Agent.ConnectionStatus == (int)ConnectionStatus.Ok && 
                             DeploymentInfo.Agent.AgentStatus == (int)AgentStatus.Ok &&
                             DeploymentInfo.Agent.Plugins.Any(p => p.Status == (int)PluginStatus.InstallationFailed))
                    {
                        //agent is OK, plugins deployment failed
                        agentErrorText.InnerHtml = Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_PluginsDeploymentFailed;
                    }
                    else
                    {
                        //another error
                        agentErrorText.InnerHtml = string.Format(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_AgentFailed, DeploymentInfo.StatusInfo.StatusMessage);
                    }
                    agentError.Visible = true;
                    break;
                case AgentDeploymentStatus.PendingReboot:
                    agentWarning.Visible = true;
                    agentWarningText.InnerText = string.Format(CultureInfo.CurrentUICulture, Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_RebootRequired, NodeInfo != null ? NodeInfo.Caption : string.Empty);
                    break;
                case AgentDeploymentStatus.PluginsRequired:
                    agentProgressInfo.Visible = true;
                    agentProgressInfoText.InnerText = Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_PluginsRequired;
                    break;
            }
        }
        else if (mode == NodePropertyPluginExecutionMode.WizChangeProperties)
        {
             agentInstalledInfo.Visible = true;
             ChangePrimaryCredentialsValidatorsState(false);
        }
    }

    private IEnumerable<BaseValidator> GetPrimaryCredentialValidators()
    {
        return new BaseValidator[]
        {
            this.RequiredCredentialNameValidator,
            this.RequiredUserNameValidator,
            this.RequiredPasswordValidator,
            this.RequiredConfirmPasswordValidator,
            this.CompareConfirmPasswordValidator
        };
    }

    private IEnumerable<BaseValidator> GetSudoCredentialValidators()
    {
        return new BaseValidator[]
        {
            this.RequiredSudoCredentialNameValidator,
            this.RequiredSudoUsernameValidator,
            this.RequiredSudoPasswordValidator,
            this.RequiredSudoPasswordConfirmationValidator,
            this.CompareSudoPasswordValidator
        };
    }

    private bool CheckAllValidators()
    {
        return GetPrimaryCredentialValidators().Concat(GetSudoCredentialValidators()).All(v => v.IsValid);
    }

    private void ChangePrimaryCredentialsValidatorsState(bool enabled)
    {
        foreach (var validator in GetPrimaryCredentialValidators())
        {
            validator.Enabled = enabled;
        }
    }

    private void ChangeSudoCredentialsValidatorsState(bool enabled)
    {
        foreach (var validator in GetSudoCredentialValidators())
        {
            validator.Enabled = enabled;
        }
    }

    private void SetControls(bool state)
    {
        tbCaption.Enabled = state;
        tbLogin.Enabled = state;
        tbPassword.Enabled = state;
        tbPasswordConfirmation.Enabled = state;

        if (state)
        {
            tbCaption.CssClass = String.Empty;
            tbLogin.CssClass = String.Empty;
            tbPassword.CssClass = String.Empty;
            tbPasswordConfirmation.CssClass = String.Empty;
            tbPrivateKeyPassword.CssClass = String.Empty;
        }
        else
        {
            tbCaption.CssClass = "disabled";
            tbLogin.CssClass = "disabled";
            tbPassword.CssClass = "disabled";
            tbPasswordConfirmation.CssClass = "disabled";
            tbPrivateKeyPassword.CssClass = "disabled";
        }
    }

    private void SetSudoControls(bool state)
    {
        tbSudoCredentialName.Enabled = state;
        tbSudoLogin.Enabled = state;
        tbSudoPassword.Enabled = state;
        tbSudoPasswordConfirmation.Enabled = state;

        if (state)
        {
            tbSudoCredentialName.CssClass = String.Empty;
            tbSudoLogin.CssClass = String.Empty;
            tbSudoPassword.CssClass = String.Empty;
            tbSudoPasswordConfirmation.CssClass = String.Empty;
        }
        else
        {
            tbSudoCredentialName.CssClass = "disabled";
            tbSudoLogin.CssClass = "disabled";
            tbSudoPassword.CssClass = "disabled";
            tbSudoPasswordConfirmation.CssClass = "disabled";
        }
    }

    public AgentDeploymentSettings GetDeploymentSettings()
    {
        var node = NodeInfo;
        if (node != null)
        {
            using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
            {
                return new AgentDeploymentSettings()
                {
                    IpAddress = node.IpAddress,
                    Hostname = node.Dns,
                    EngineId = node.EngineID,
                    Credentials = GetEnteredCredentials(),
                    RequiredPlugins = new List<string>(proxy.GetRequiredAgentDiscoveryPlugins()),
                    InstallPackageId = this.InstallPackageId
                };
            }
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Validate new credentials. 
    /// If credentials are valid, true is returned and existingCredentialId is set to credential ID.
    /// It can be NonExistingWmiCredentialID if credentials are new or it can be an existing credential ID if entered credentials
    /// are the same as existing credentials.
    /// 
    /// If credentails are not valid, false is returned and errorMessage is set.
    /// </summary>
    private ValidationResult ValidateNewUsernamePasswordCredentials(string name, string username, string password, out int existingCredentialId)
    {
        existingCredentialId = NewCredentialId;

        UsernamePasswordCredential existingCredential = credentialsList.FirstOrDefault(c => c.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase));
        if (existingCredential != null)
        {
            if ((existingCredential.Username == username) && (existingCredential.Password == password))
            {
                existingCredentialId = existingCredential.ID.Value;
                return new ValidationResult() { Valid = true };
            }
            else
            {
                return new ValidationResult() 
                { 
                    Valid = false, 
                    ValidationText = Resources.CoreWebContent.WEBCODE_TM0_90 
                };
            }
        }
        return new ValidationResult() { Valid = true };
    }

    private ValidationResult ValidateNewSshPrivateKeyCredentials(string name, string username, string privateKey, out int existingCredentialId)
    {
        existingCredentialId = NewCredentialId;

        SshPrivateKeyCredential existingCredential = sshCredentialsList.FirstOrDefault(c => c.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase));
        if (existingCredential != null)
        {
            if ((existingCredential.Username == username) && (existingCredential.PrivateKey == privateKey))
            {
                existingCredentialId = existingCredential.ID.Value;
                return new ValidationResult() { Valid = true };
            }
            else
            {
                return new ValidationResult()
                {
                    Valid = false,
                    ValidationText = Resources.CoreWebContent.WEBCODE_TM0_90
                };
            }
        }
        return new ValidationResult() { Valid = true };
    }

    //Validate the credentials provided to connect to the agent.
    public bool Validate()
    {
        if (!CheckAllValidators())
            return false;

        ValidationResult result = new ValidationResult() { Valid = true };

        string validationText = string.Empty;
        bool credentialsValid = true;
        DetectionInfo = AgentDetectionInfo.DoesNotExist;
        
        if (CredentialsId == NewCredentialId)
        {
            int existingCredentialsId;
            result = IsPrivateKey
                ? ValidateNewSshPrivateKeyCredentials(CredentialName, Login, Password, out existingCredentialsId)
                : ValidateNewUsernamePasswordCredentials(CredentialName, Login, Password, out existingCredentialsId);

            if ((credentialsValid) && (existingCredentialsId != CredentialsId))
            {
                DdlAgentCredentials.SelectedValue = existingCredentialsId.ToString();
                SetControls(false);
            }
        }

        if (result.Valid && UseSudo && SudoCredentialsId == NewCredentialId)
        {
            int existingCredentialsId;
            result = ValidateNewUsernamePasswordCredentials(SudoCredentialName, SudoLogin, SudoPassword, out existingCredentialsId);

            if ((credentialsValid) && (existingCredentialsId != CredentialsId))
            {
                DdlSudoCredentials.SelectedValue = existingCredentialsId.ToString();
                SetControls(false);
            }
        }

        UpdateUsernameAndPrivateKeyCredentialControls(false);

        if (result.Valid)
        {
            var settings = GetDeploymentSettings();
            if ((settings == null) || (string.IsNullOrWhiteSpace(settings.IpAddress) && string.IsNullOrWhiteSpace(settings.Hostname)))
            {
                result = new ValidationResult()
                {
                    Valid = false,
                    ValidationText = Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_CredentialsFailed
                };
            }
            else
            {
                var validationResult = new AgentManager().ValidateDeploymentSettings(settings);
                result = new ValidationResult() { Valid = validationResult.Success };
                result.ValidationText = validationResult.Success ? string.Empty : validationResult.Message;
                DetectionInfo = validationResult.DetectionInfo;
                if (validationResult.IsUnsupportedLinuxOs)
                {
                    InstallPackageSelectionRow.Visible = true;

                    // override failed test if user selected custom install package
                    if (!string.IsNullOrEmpty(InstallPackageId))
                    {
                        result.Valid = true;
                        result.ValidationText = Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_CredentialsOK;
                    }
                }
            }

        }

        UpdateValidationResult(result);

        return result.Valid;
    }

    private void UpdateValidationResult(ValidationResult result)
    {
        ValidationSuccess.Visible = result.Valid;
        ValidationError.Visible = !result.Valid;
        if (!result.Valid)
            ValidationErrorMessage.InnerText = result.ValidationText;
    }

    public void InitializeAgent(IList<Node> nodes, NodePropertyPluginExecutionMode mode, Dictionary<string, object> pluginState)
    {
        this.mode = mode;

        if (nodes != null && nodes.Count == 1)
        {
            // If there are already agent credentials it means we are on the last step in Add Node wizard and we should use their AgentId.
            // Otherwise they get replaced by "null" and node may not be properly paired with the agent.
            DetectAgent(nodes.First(), NodeWorkflowHelper.AgentCredential != null ? NodeWorkflowHelper.AgentCredential.AgentId : -1);
            MultipleSelection = false;
        }
        else
        {
            MultipleSelection = true;
        }

        switch (mode)
        {
            case NodePropertyPluginExecutionMode.WizDefineNode:
            case NodePropertyPluginExecutionMode.WizChangeProperties:
                HideSnmpTestButton = true;

                if (!this.IsPostBack)
                {
                    var credentials = NodeWorkflowHelper.WmiCredential;
                    if (credentials != null && credentials.ID != NewCredentialId)
                    {
                        DdlAgentCredentials.SelectedValue = credentials.ID.ToString();
                        SetCredentialsFromDropDown();
                    }
                }
                break;
            case NodePropertyPluginExecutionMode.EditProperies:
                if (!this.IsPostBack)
                {
                }

                break;
        }
        SetAgentInfo();
        RegisterValidationProgress();
    }
    
    private void RegisterValidationProgress()
    {
        string script = string.Format(@"function ShowValidationProgress () {{
    $('#{0}').hide(); 
    $('#{1}').hide();
    var pass = $('#{3}').val();
    var confirmPass = $('#{4}').val();
    if (pass.length == 0 || pass != confirmPass)
        $('#{2}').hide(); 
	else
		$('#{2}').show(); 
}}", ValidationSuccess.ClientID, ValidationError.ClientID, ValidationProgress.ClientID, tbPassword.ClientID, tbPasswordConfirmation.ClientID);

        ScriptManager.RegisterStartupScript(this.Page, GetType(), "ShowValidationProgress", script, true);
        ScriptManager.RegisterStartupScript(this.Page, GetType(), "RefreshCertificateElementsVisibility",
            string.Format("SW.Core.AddNode.AgentInfo.refreshVisibility('PrivateKeyFile', '{0}', '{1}')",
                PrivateKeyValue.ClientID, PrivateKeyFileName.ClientID), true);

    }

    public bool Update()
    {
        switch (mode)
        {
            case NodePropertyPluginExecutionMode.WizDefineNode:
            case NodePropertyPluginExecutionMode.WizChangeProperties:
                if (CredentialsId == NewCredentialId)
                {
                    Credential credential;
                    if (IsPrivateKey)
                    {
                        credential = CreateNewSshPrivateKeyCredentials(CredentialName, Login, PrivateKey, PrivateKeyPassword);
                        sshCredentialsList.Add((SshPrivateKeyCredential)credential);
                    }
                    else
                    {
                        credential = CreateNewUsernamePasswordCredentials(CredentialName, Login, Password);
                        credentialsList.Add((UsernamePasswordCredential)credential);

                        NodeWorkflowHelper.Node.NodeSettings[NodeSettingsNames.WmiCredentialSettingName] = credential.ID.HasValue ? credential.ID.Value.ToString() : NewCredentialId.ToString();
                        NodeWorkflowHelper.WmiCredential = credential;
                    }

                    DdlAgentCredentials.Items.Add(new ListItem(credential.Name, credential.ID.Value.ToString()));
                    DdlAgentCredentials.SelectedValue = credential.ID.Value.ToString();

                    SetCredentialsFromDropDown();
                }
                else if (!IsPrivateKey)
                {
                    NodeWorkflowHelper.Node.NodeSettings[NodeSettingsNames.WmiCredentialSettingName] = CredentialsId.ToString();
                    NodeWorkflowHelper.WmiCredential = credentialsList.Where(c => c.ID.Value == CredentialsId).FirstOrDefault();
                }

                if (SudoCredentialsId == NewCredentialId && !string.IsNullOrEmpty(SudoCredentialName) && !string.IsNullOrEmpty(SudoLogin) && !string.IsNullOrEmpty(SudoPassword))
                {
                    var credential = CreateNewUsernamePasswordCredentials(SudoCredentialName, SudoLogin, SudoPassword);
                    credentialsList.Add(credential);

                    DdlSudoCredentials.Items.Add(new ListItem(credential.Name, credential.ID.Value.ToString()));
                    DdlSudoCredentials.SelectedValue = credential.ID.Value.ToString();

                    SetSudoCredentialsFromDropDown();
                }
                break;
            case NodePropertyPluginExecutionMode.EditProperies:

                break;
        }

        return true;
    }

    public bool Update(Node node)
    {
        if (DeploymentInfo == null)
        {
            if (CredentialsId == NewCredentialId)
            {
                Credential credential = null;
                if (IsPrivateKey)
                {
                    credential = CreateNewSshPrivateKeyCredentials(CredentialName, Login, PrivateKey, PrivateKeyPassword);
                    sshCredentialsList.Add((SshPrivateKeyCredential)credential);
                }
                else
                {
                    credential = CreateNewUsernamePasswordCredentials(CredentialName, Login, Password);
                    credentialsList.Add((UsernamePasswordCredential)credential);
                }

                DdlAgentCredentials.Items.Add(new ListItem(credential.Name, credential.ID.Value.ToString()));
                DdlAgentCredentials.SelectedValue = credential.ID.Value.ToString();
            }

            if (SudoCredentialsId == NewCredentialId && !string.IsNullOrEmpty(SudoCredentialName) && !string.IsNullOrEmpty(SudoLogin))
            {
                var credential = CreateNewUsernamePasswordCredentials(SudoCredentialName, SudoLogin, SudoPassword);
                credentialsList.Add(credential);

                DdlSudoCredentials.Items.Add(new ListItem(credential.Name, credential.ID.Value.ToString()));
                DdlSudoCredentials.SelectedValue = credential.ID.Value.ToString();

                SetSudoCredentialsFromDropDown();
            }
        }

        return true;
    }

    public Guid SaveSettings()
    {
        var settings = GetDeploymentSettings();
        if (settings != null)
        {
            var settingsId = Guid.NewGuid();
            AgentsDeployment.Instance.AddOrUpdateDeploymentSettings(settingsId, settings);
            return settingsId;
        }
        else
        {
            return Guid.Empty;
        }
    }

    public void MultipleSelection_CheckedChanged(bool isChecked)
    {
        DdlAgentCredentials.Enabled = isChecked;
        
        bool enabled = isChecked && CredentialsId == NewCredentialId;
        bool sudoEnabled = isChecked && SudoCredentialsId == NewCredentialId;

        SetControls(enabled);
        UpdateUsernameAndPrivateKeyCredentialControls(true);
        ChangePrimaryCredentialsValidatorsState(enabled);
        SetSudoControls(sudoEnabled);
        ChangeSudoCredentialsValidatorsState(sudoEnabled);
    }

    protected void useSudoCheckbox_CheckedChanged(object sender, EventArgs e)
    {
        RefreshSudoSection();
    }

    private void RefreshSudoSection()
    {
        var displaySudoCredentialsRow = UseSudo && !AgentInstallAttempted;
        sudoCredentialsTitleRow.Visible = displaySudoCredentialsRow;
        sudoCredentialsDropDownRow.Visible = displaySudoCredentialsRow;
        sudoCredentialsNameRow.Visible = displaySudoCredentialsRow;
        sudoCredentialsLoginRow.Visible = displaySudoCredentialsRow;
        sudoCredentialsPasswordRow.Visible = displaySudoCredentialsRow;
        sudoCredentialsConfirmPasswordRow.Visible = displaySudoCredentialsRow;
        ChangeSudoCredentialsValidatorsState(displaySudoCredentialsRow);
    }

    protected void useSnmpCheckbox_CheckedChanged(object sender, EventArgs e)
    {
        RefreshSnmpSection();
    }

    private void RefreshSnmpSection()
    {
        snmpCredentialsTitleRow.Visible = UseSnmp;
        snmpCredentialsSnmpInfoRow.Visible = UseSnmp;
        agentSnmpUpdatePanel.Visible = !HideSnmpTestButton && UseSnmp && AgentInstalledSuccessfully;
    }

    private void SetSNMPValidationBar(bool valid, string msg)
    {
        agentSnmpValidationFalse.Visible = !valid;
        agentSnmpValidationTrue.Visible = valid;
        SNMPValidationFailedMessage.Text = msg;
    }


    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    protected string SudoCredentialsHelpUrl
    {
        get { return HelpHelper.GetHelpUrl("OrionAgent_LinuxCredentials"); }
    }

    protected string SnmpCredentialsHelpUrl
    {
        get { return HelpHelper.GetHelpUrl("OrionAgent_SNMPCredentials"); }
    }
}