<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PollingSettings.ascx.cs" Inherits="Orion_Nodes_Controls_NodePolling" %>
<%@ Register TagPrefix="orion" TagName="AdditionalPollingOptionsOrcInfo" Src="~/Orion/Nodes/Controls/AdditionalPollingOptionsOrcInfo.ascx" %>

<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="Nodes/js/ChangeEngineDialog.js" />
<orion:Include runat="server" File="extjs/Ext.sw.grid.RadioSelectionModel.js" />
<table>
    <tr>
        <td class="leftSectionTitle pollingSelector">
            <asp:CheckBox ID="cbMultipleSelection" runat="server" AutoPostBack="True" OnCheckedChanged="cbMultipleSelection_CheckedChanged" />
            <asp:Literal ID="OuterTitle" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_44 %>" />
        </td>
        <td class="rightInputColumn pollingSelector">
        </td>
    </tr>
</table>
<table class="blueBox">
<tr>
	<td class="leftLabelColumn"><asp:Label runat="server" ID="lblStatusPolling" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_45 %>"/></td>
	<td class="rightInputColumn" nowrap="nowrap"><asp:TextBox runat="server" ID="tbNodeStatusPolling">120</asp:TextBox><asp:RegularExpressionValidator
			ID="regexvalidatorNodeStatusPolling" runat="server" ControlToValidate="tbNodeStatusPolling"
			ErrorMessage="*" ValidationExpression="^\d+$" SetFocusOnError="True"></asp:RegularExpressionValidator><asp:RangeValidator
				ID="RangeValidator1" runat="server" ControlToValidate="tbNodeStatusPolling" ErrorMessage="(10-32767)"
				MaximumValue="32767" MinimumValue="10" Display="Dynamic" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
		<asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_46 %>" />
	</td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td class="leftLabelColumn"><asp:Label runat="server" ID="lblCollectStatEvery" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_47 %>" /></td>
	<td class="rightInputColumn" nowrap="nowrap"><asp:TextBox runat="server" ID="tbCollectSatisticsEvery">10</asp:TextBox><asp:RegularExpressionValidator
        ID="regexvalidatorCollectStatEvery" runat="server" ControlToValidate="tbCollectSatisticsEvery"
        ErrorMessage="*" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="tbCollectSatisticsEvery"
        Display="Dynamic" ErrorMessage="(1-32767)" MaximumValue="32767" MinimumValue="1"
        SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
    <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_48 %>" /></td>
	<td>&nbsp;</td>
</tr>
<% if (ShowPollingEngineDetails)
   {%>
<tr>
	<td class="leftLabelColumn"><asp:Label runat="server" ID="Label1" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_26 %>" /></td>
	<td class="rightInputColumn" nowrap="nowrap" runat="server" id="ChangePollingEngineBlock">
        <span id="pollEngineDescr" runat="server"><asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_49 %>" />&nbsp;</span>
        <orion:LocalizableButton runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_70 %>" ID="changePollEngineLink" OnClientClick="return false;" />
        <asp:HiddenField runat="server" ID="hfPollingEngineId" OnValueChanged="hfPollingEngineId_OnValueChanged" />
        <input type="hidden" id="selPollingEngineId" value='<%= DefaultSanitizer.SanitizeHtml(hfPollingEngineId.ClientID) %>' />
        <input type="hidden" id="objectEngineIDs" value='<%= DefaultSanitizer.SanitizeHtml(string.Join(",", ObjectEngineIDs.Select(x=>x.ToString()).ToArray())) %>' />
    </td>
	<td>&nbsp;</td>
</tr>
<%} %>
</table>
<%--used in Change Polling Engine dialog--%>
<div id="AdditionalPollingOptionsOrcInfo" class="x-hide-display">
    <orion:AdditionalPollingOptionsOrcInfo runat="server"/>
</div>