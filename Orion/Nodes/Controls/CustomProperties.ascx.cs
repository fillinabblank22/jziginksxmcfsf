using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.CPE;
using Node = SolarWinds.Orion.Core.Common.Models.Node;
using SolarWinds.Orion.Core.Common.Indications;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

public partial class Orion_Nodes_Controls_CustomProperties : System.Web.UI.UserControl, INodePropertyPlugin
{
	private IList<Node> _nodes;
	private string pageGuid;
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

	protected void Page_Load(object sender, EventArgs e)
	{
		HiddenField hf = (ControlHelper.FindControlRecursive(Page, "HiddenFieldGuid") as HiddenField);
		if (hf != null)
		{
			pageGuid = hf.Value;
		}
		if (!IsPostBack)
		{
			LoadCustomProperties();
		}
	}

	#region public properties
	public string CustomPropertiesTable
	{
		get
		{
			if (ViewState["CustomPropertiesTable"] == null)
			{
				ViewState["CustomPropertiesTable"] = "NodesCustomProperties";
			}
			return (string)ViewState["CustomPropertiesTable"];
		}
		set
		{
			ViewState["CustomPropertiesTable"] = value;
		}

	}

	public string Caption
	{
		get
		{
			if (ViewState["Caption"] == null)
			{
				ViewState["Caption"] = Resources.CoreWebContent.WEBDATA_IB0_25;
			}
			return (string)ViewState["Caption"];
		}
		set
		{
			ViewState["Caption"] = value;
		}
	}

	public bool UseDefaultManageCustomPropertiesLink
	{
		get
		{
			return (ViewState["UseDefaultManageCustomPropertiesLink"] == null ? true : (bool)ViewState["UseDefaultManageCustomPropertiesLink"]);
		}
		set
		{
			ViewState["UseDefaultManageCustomPropertiesLink"] = value;
		}
	}

	public string HelpFullHintText
	{
		get
		{
			return (ViewState["HelpFullHintText"] == null ? string.Empty : ViewState["HelpFullHintText"].ToString());
		}
		set
		{
			ViewState["HelpFullHintText"] = value;
		}
	}

	public bool IsMultiselected
	{
		get
		{
			IList<int> ids = null;

			switch (this.CustomPropertiesTable)
			{
                case "NodesCustomProperties":
					ids = NodeWorkflowHelper.GetMultiSelectedNodeIds(pageGuid);
					if (ids.Count == 0 && NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(pageGuid) != null)
						return NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(pageGuid).Count > 1;
					break;
				case "Interfaces":
					ids = NodeWorkflowHelper.GetMultiSelectedInterfaceIds(pageGuid);
					if (ids.Count == 0 && NodeWorkflowHelper.GetMultiSelectedInterfaceIdsWithEngineIds(pageGuid) != null)
						return NodeWorkflowHelper.GetMultiSelectedInterfaceIdsWithEngineIds(pageGuid).Count > 1;
					break;
				case "Volumes":
					ids = NodeWorkflowHelper.GetMultiSelectedVolumeIds(pageGuid);
					if (ids.Count == 0 && NodeWorkflowHelper.GetMultiSelectedVolumeIdsWithEngineIds(pageGuid) != null)
						return NodeWorkflowHelper.GetMultiSelectedVolumeIdsWithEngineIds(pageGuid).Count > 1;
					break;
			}

			if (ids == null)
			{
				return false;
			}

			return (ids.Count > 1);
		}
	}

    public IList<int> EntityIds { set; get; }

	public Dictionary<string, object> CustomProperties
	{
		get
		{
			Dictionary<string, object> result = new Dictionary<string, object>();
			foreach (RepeaterItem item in repCustomProperties.Items)
			{
				CheckBox cbSelectedProperty = (CheckBox)item.FindControl("cbSelectedProperty");
				if (IsMultiselected && !cbSelectedProperty.Checked)
					continue;

				Literal litPropertyName = (Literal)item.FindControl("litPropertyName");
				var txtCustomPropertyValue = (Orion_Controls_EditCpValueControl)item.FindControl("PropertyValue");

				result.Add(litPropertyName.Text, TryGetCustomPropertyValueWithType(litPropertyName.Text, txtCustomPropertyValue.Text));
			}
			return result;
		}
        set
        {
            if (value==null) return;
            foreach (RepeaterItem item in repCustomProperties.Items)
            {
                Literal litPropertyName = (Literal)item.FindControl("litPropertyName");
                if (!value.ContainsKey(litPropertyName.Text)) continue;

                var txtCustomPropertyValue = (Orion_Controls_EditCpValueControl)item.FindControl("PropertyValue");
                if (txtCustomPropertyValue != null)
                    txtCustomPropertyValue.Text = value[litPropertyName.Text].ToString();
            }
        }
	}
	#endregion

	#region private methods
	private object TryGetCustomPropertyValueWithType(string propertyName, string valueAsString)
	{
		Type cpType = CustomPropertyMgr.GetTypeForProp(NetObjectType, propertyName);

		try
		{
			// Default bool value is False
			if ((cpType.Equals(typeof(Boolean))) && (string.IsNullOrEmpty(valueAsString)))
			{
				return false;
			}
			// float values are already converted to invariant string
			if (cpType.Equals(typeof(float)) || cpType.Equals(typeof(double)))
			{
				return Convert.ChangeType(valueAsString, cpType, CultureInfo.InvariantCulture);
			}
			
			return Convert.ChangeType(valueAsString, cpType);
			
		}
		catch
		{
			return valueAsString;
		}
	}

	private void LoadCustomProperties()
	{
		CustomPropertiesHeader.Visible = CustomPropertyMgr.GetPropNamesForTable(CustomPropertiesTable, true).Count > 0;

		IList<string> customProperties = CustomPropertyMgr.GetPropNamesForTableClean(NetObjectType);
		repCustomProperties.DataSource = customProperties;
		repCustomProperties.DataBind();
	}
	#endregion

	protected void repCustomProperties_ItemDataBound(object sender, RepeaterItemEventArgs e)
	{
		if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
		{
			CheckBox cbSelectedProperty = (CheckBox)e.Item.FindControl("cbSelectedProperty");
			Literal litCustomPropertyName = (Literal)e.Item.FindControl("litPropertyName");
			var customPropertyValue = (Orion_Controls_EditCpValueControl)e.Item.FindControl("PropertyValue");
			Literal txtDescr = (Literal)e.Item.FindControl("descriptionText");

			litCustomPropertyName.Text = e.Item.DataItem.ToString();

			CustomProperty customProperty = CustomPropertyMgr.GetCustomProperty(NetObjectType, e.Item.DataItem.ToString());
            customPropertyValue.Configure(true, customProperty);

            IList<int> ids = EntityIds ?? new List<int>();
			var oldPropertyValue = "";
            txtDescr.Text = DefaultSanitizer.SanitizeHtml(customProperty.Description)?.ToString();

			switch (this.CustomPropertiesTable)
			{
                case "NodesCustomProperties":
					if (NodeWorkflowHelper.GetMultiSelectedNodeIds(pageGuid).Count > 0)
					{
						ids = NodeWorkflowHelper.GetMultiSelectedNodeIds(pageGuid);
					}
					else if (NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(pageGuid).Count > 0)
					{
						ids = new List<int>();
						foreach (var id in NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(pageGuid))
							ids.Add(NodeWorkflowHelper.GetObjectId(id));
					}
					break;
				case "Interfaces":
					if (NodeWorkflowHelper.GetMultiSelectedInterfaceIds(pageGuid).Count > 0)
						ids = NodeWorkflowHelper.GetMultiSelectedInterfaceIds(pageGuid);
					else if (NodeWorkflowHelper.GetMultiSelectedInterfaceIdsWithEngineIds(pageGuid).Count > 0)
					{
						ids = new List<int>();
						foreach (var id in NodeWorkflowHelper.GetMultiSelectedInterfaceIdsWithEngineIds(pageGuid))
							ids.Add(NodeWorkflowHelper.GetObjectId(id));
					}
					break;
				case "Volumes":
					if (NodeWorkflowHelper.GetMultiSelectedVolumeIds(pageGuid).Count > 0)
					{
						ids = NodeWorkflowHelper.GetMultiSelectedVolumeIds(pageGuid);
					}
					else if (NodeWorkflowHelper.GetMultiSelectedVolumeIdsWithEngineIds(pageGuid).Count > 0)
					{
						ids = new List<int>();
						foreach (var id in NodeWorkflowHelper.GetMultiSelectedVolumeIdsWithEngineIds(pageGuid))
							ids.Add(NodeWorkflowHelper.GetObjectId(id));
					}
					break;
			}

            if ((ids == null || ids.Count == 0) && customProperty.Mandatory )
                //if new and mandatory - initiate population 
                    customPropertyValue.Text = string.Empty;

			if (ids == null)
			{
				return;
			}

			if (ids.Count > 0)
			{
				oldPropertyValue = CustomPropertyMgr.GetCustomProp(NetObjectType, e.Item.DataItem.ToString(), ids[0]);
				customPropertyValue.Text = oldPropertyValue;
			}

			for (int i = 1; i < ids.Count; i++)
			{
				var goPropertyValue = CustomPropertyMgr.GetCustomProp(NetObjectType, e.Item.DataItem.ToString(), ids[i]);
				if (!oldPropertyValue.Equals(goPropertyValue, StringComparison.InvariantCultureIgnoreCase))
				{
					customPropertyValue.Text = string.Empty;
					cbSelectedProperty.Checked = false;

					break;
				}
				oldPropertyValue = goPropertyValue;
			}

			if (!IsMultiselected)
			{
				cbSelectedProperty.Visible = false;
			}
			else
			{
				cbSelectedProperty_CheckedChanged(cbSelectedProperty, EventArgs.Empty);
			}
		}
	}

	protected void cbSelectedProperty_CheckedChanged(object sender, EventArgs e)
	{
		CheckBox cbSelected = (CheckBox)sender;

		var txtPropertyValue = (Orion_Controls_EditCpValueControl)cbSelected.Parent.FindControl("PropertyValue");
		txtPropertyValue.Enabled = cbSelected.Checked;
		//txtPropertyValue.CssClass = cbSelected.Checked ? string.Empty : "disabled";

		//AssignValidator(((Literal)cbSelected.Parent.FindControl("litPropertyName")).Text, txtPropertyValue);
	}

	public string NetObjectType
	{
		get
		{
			object o = ViewState["NetObjectType"];

			return (o == null ? "NodesCustomProperties" : o.ToString());
		}
		set
		{
			ViewState["NetObjectType"] = value;
		}
	}

	public bool ValidateDateTimePropertiesAtServer()
	{
		return true;
	}

	public void UpdateCustomPropertiesRestrictions()
	{
		foreach (var property in CustomProperties)
		{
		    var customProperty = CustomPropertyMgr.GetCustomProperty(CustomPropertiesTable, property.Key);

		    CustomPropertyHelper.UpdateRestrictedValuesIfNeeded(customProperty,
                    new[] { CustomPropertyHelper.GetInvariantCultureString(property.Value.ToString(), customProperty.PropertyType) });
		}
	}

    public void AssignCustomPropertiesValues(string entity, string uri)
    {
        foreach (var property in CustomProperties.Where(property => property.Value != null))
        {
            var propertyType = property.Value.GetType();
            var invariantStringValue = (propertyType == typeof (DateTime))
                                           ? ((DateTime) property.Value).ToString("s")
                                           : CustomPropertyHelper.GetInvariantCultureString(property.Value.ToString(), propertyType);

            var parameters = new Dictionary<string, List<string>>();
            parameters[invariantStringValue] = new List<string> { uri };
            CustomPropertyHelper.AssignCpValuesForEntity(property.Key, parameters, entity);
        }
    }

    #region == INodePropertyPlugin Members ==

	public void Initialize(IList<SolarWinds.Orion.Core.Common.Models.Node> nodes, NodePropertyPluginExecutionMode mode, Dictionary<string, object> pluginState)
	{
		//_propertyBag = pluginState;
		_nodes = nodes;
		//_mode = mode;
	}

	public bool Validate()
	{
		return true;
	}

	protected static void BusinessLayerExceptionHandler(Exception ex)
	{
		throw new Exception("BusinessLayerException:CustomProperties");
	}

	private int GetEngineIdForNodeId(ICoreBusinessLayer proxy, int nodeId)
	{
		int engineId = proxy.GetEngineIdForNetObject(string.Format("N:{0}", nodeId));
		return engineId;
	}

	private Node GetNodeById(int nodeId, int engineId)
	{
        using (var coreProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, engineId, true))
		{
			return coreProxy.GetNodeWithOptions(nodeId, false, false);
		}
	}

	private List<Node> GetNodes(string guid)
	{
		var nodes = new List<Node>();
        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
		{
			switch (NetObjectType)
			{
				case "NodesCustomProperties":
					if (NodeWorkflowHelper.GetMultiSelectedNodeIds(guid).Count > 0)
					{
						foreach (int nodeID in NodeWorkflowHelper.GetMultiSelectedNodeIds(guid))
						{
							int engineID = GetEngineIdForNodeId(proxy, nodeID);
							var n = GetNodeById(nodeID, engineID);
							nodes.Add(n);
						}
					}
					else if (NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(guid).Count > 0)
					{
						foreach (string nodeStr in NodeWorkflowHelper.GetMultiSelectedNodeIdsWithEngineIds(guid))
						{
							int nodeID = Int32.Parse(nodeStr.Split(':')[1]);
							int engineID = Int32.Parse(nodeStr.Split(':')[2]);
							var n = GetNodeById(nodeID, engineID);
							nodes.Add(n);
						}
					}
					break;
				case "Volumes":
					if (NodeWorkflowHelper.GetMultiSelectedVolumeIds(guid).Count > 0)
					{
						foreach (int nodeID in NodeWorkflowHelper.GetMultiSelectedVolumeIds(guid))
						{
							int engineID = GetEngineIdForNodeId(proxy, nodeID);
							var n = GetNodeById(nodeID, engineID);
							nodes.Add(n);
						}
					}
					else if (NodeWorkflowHelper.GetMultiSelectedVolumeIdsWithEngineIds(guid).Count > 0)
					{
						foreach (string nodeStr in NodeWorkflowHelper.GetMultiSelectedVolumeIdsWithEngineIds(guid))
						{
							int nodeID = Int32.Parse(nodeStr.Split(':')[1]);
							int engineID = Int32.Parse(nodeStr.Split(':')[2]);
							var n = GetNodeById(nodeID, engineID);
							nodes.Add(n);
						}
					}
					break;
			}
		}

		return nodes;
	}

	public bool Update()
	{
		if (_nodes == null) return true;

		//TODO: Temporary approach
		//will use NodePropertyPluginManager.Instance to get nodes
		if (pageGuid != null)
		{
			//get nodes using guid
			_nodes = GetNodes(pageGuid);
		}

	    //using (var proxy = _blProxyCreator.Create(ex => log.Error(ex)))
		//{
		foreach (var node in _nodes)
		{
			Node node1 = node;
			var isNew = false;
			if (node1.ID <= 0) // in case of new node being added cached node is not up to date and we have to update it
			{
				isNew = true;
				node1 = NodeWorkflowHelper.Node;
			}

			//as Custom properties are no longer saved in NodeBL we need to take care of PubSub indication
			PropertyBag changedProperties = GetChangedProperties(node1.CustomProperties, CustomProperties, isNew);

			node1.CustomProperties.Clear();
			foreach (KeyValuePair<string, object> prop in CustomProperties)
				node1.CustomProperties.Add(prop);

			if (node1.ID > 0)
			{
				//proxy.UpdateCustomProperties(node1); // does not exists meantime and thus:
				UpdateCustomProperties(node1);

				if (changedProperties.Count > 0)
				{
					//add mandatory fields
					changedProperties.Add("InstanceType", "Orion.NodesCustomProperties");
					changedProperties.Add("NodeId", node1.ID);
					changedProperties.Add("ObjectName", node1.Caption);
					changedProperties.Add("ObjectStatus", node1.Status);

					IndicationPublisher.CreateV3().ReportIndication(
						IndicationHelper.GetIndicationType(IndicationType.System_InstanceModified),
						IndicationHelper.GetIndicationProperties(),
						changedProperties);
				}
			}
		}
		//}

		UpdateCustomPropertiesRestrictions();

		return true;
	}

	private object GetCPValue(object value)
	{
		if ((value == null) || (value == DBNull.Value))
			return string.Empty;
		else
			return value;
	}

	private PropertyBag GetChangedProperties(IDictionary<string, object> oldProperties, Dictionary<string, object> newProperties, bool isNew)
	{
		PropertyBag changedProperties = new PropertyBag();

		foreach (var property in newProperties)
		{
			object newValue = GetCPValue(property.Value);
			if (oldProperties.ContainsKey(property.Key))
			{
				object oldValue = GetCPValue(oldProperties[property.Key]);
				if (!newValue.Equals(oldValue))
					changedProperties.Add(property.Key, newValue);
			}
			// in case we're creating new node then put into changedProperties only properties with non default values 
			else if (!isNew || !string.IsNullOrEmpty(GetCPValue(property.Value).ToString()))
			{
				changedProperties.Add(property.Key, newValue);
			}
		}

		return changedProperties;
	}

	#region == UpdateCustomProperties() ==
	/// <summary>
	/// This function is a copy of NodeBLDAL.UpdateCustomProperties() which is private.
	/// BL proxy does not provide meantime a way to update custom properties separetely and 
	/// this is the reason we temporary copied it here to address FB24316, 24296 and alike.
	/// </summary>
	private static void UpdateCustomProperties(Node node)
	{
		IDictionary<string, object> customProperties = node.CustomProperties;

		if (customProperties.Count == 0)
			return;

		List<string> clauses = new List<string>(customProperties.Count);
		List<SqlParameter> parameters = new List<SqlParameter>(customProperties.Count);
        int paramCounter = 0;

		foreach (string propertyName in customProperties.Keys)
		{
            string paramName = string.Format("p{0}", paramCounter);
            paramCounter++;

			clauses.Add(string.Format("[{0}]=@{1}", propertyName, paramName));
			if (customProperties[propertyName] == null || customProperties[propertyName] == DBNull.Value || string.IsNullOrEmpty(customProperties[propertyName].ToString()))
                parameters.Add(new SqlParameter(paramName, DBNull.Value));
			else
                parameters.Add(new SqlParameter(paramName, customProperties[propertyName]));
		}

		string sql = "UPDATE [NodesCustomProperties] SET {0} WHERE NodeID=@node";
		using (SqlCommand cmd = SolarWinds.Orion.Common.SqlHelper.GetTextCommand(string.Format(sql, string.Join(", ", clauses.ToArray()))))
		{
			foreach (SqlParameter parameter in parameters)
				cmd.Parameters.Add(parameter);

			cmd.Parameters.AddWithValue("node", node.ID);

			SolarWinds.Orion.Common.SqlHelper.ExecuteNonQuery(cmd);
		}
	}
	#endregion

	#endregion
}
