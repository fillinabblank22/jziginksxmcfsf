﻿using System;
using System.Web.UI;

public partial class Orion_Nodes_Controls_ICMPSettings : UserControl
{
    #region Delegates

    public delegate void ChangeEventHandler(object sender, EventArgs args);

    #endregion

    public bool MultipleSelection
    {
        get { return cbMultipleSelection.Visible; }
        set { cbMultipleSelection.Visible = value; }
    }

    public bool MultipleSelection_Selected
    {
        get { return cbMultipleSelection.Checked; }
        set
        {
            cbMultipleSelection.Checked = value;
            cbMultipleSelection_CheckedChanged(null, null);
        }
    }

    public bool ICMPOnly
    {
        get { return cbICMPOnly.Checked; }
        set { cbICMPOnly.Checked = value; }
    }

    public bool Enabled
    {
        get { return cbICMPOnly.Enabled; }
        set { cbICMPOnly.Enabled = cbMultipleSelection.Enabled = value; }
    }

    public event ChangeEventHandler Changed;

    protected void cbMultipleSelection_CheckedChanged(object sender, EventArgs e)
    {
        cbICMPOnly.Enabled = cbMultipleSelection.Checked;
        cbICMPOnly.CssClass = cbICMPOnly.Enabled ? string.Empty : "disabled";

        if (Changed != null)
            Changed(this, e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void cbICMPOnly_OnCheckedChanged(object sender, EventArgs e)
    {
        if (Changed != null)
            Changed(this, e);
    }
}