using System;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

public partial class Orion_Nodes_Controls_PollingEngine : System.Web.UI.UserControl
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    public event EventHandler EngineChanged;

    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    public int EngineID
    {
        get
        {
            if (ddlPollingEngine.Items.Count == 0)
                LoadDropDown();
            return Convert.ToInt32(ddlPollingEngine.SelectedValue);
        }
        set
        {
            if (!IsPostBack && ddlPollingEngine.Items.Count == 0)
                LoadDropDown();
            if (ddlPollingEngine.Items.FindByValue(value.ToString()) != null)
                ddlPollingEngine.SelectedValue = value.ToString();
        }
    }

    public bool Enabled
    {
        get
        {
            return ddlPollingEngine.Enabled;
        }
        set
        {
            ddlPollingEngine.Enabled = value;
        }
    }

    private void LoadDropDown()
    {
        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            ddlPollingEngine.DataSource = proxy.GetEngines()
                // GetEngines return Dictionary<string, Dictionary<int, string>>
                // that represents engine information <[ServerType], <[EngineID], [HostName]>>
                .SelectMany(x => x.Value.Select(e => new { Id = e.Key, HostName = $"{e.Value.ToUpperInvariant()} ({x.Key})" }))
                .OrderBy(x => x.Id);
            ddlPollingEngine.DataTextField = "HostName";
            ddlPollingEngine.DataValueField = "Id";
            ddlPollingEngine.DataBind();
            this.Visible = (ddlPollingEngine.Items.Count > 1); // hide the whole control if there's only one polling engine
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (ddlPollingEngine.Items.Count == 0)
            {
                LoadDropDown();
            }

            if (!String.IsNullOrEmpty(Request.QueryString["EngineID"]))
            {
                int index = 0;
                bool found = false;

                foreach (ListItem item in ddlPollingEngine.Items)
                {
                    if (item.Value == Request.QueryString["EngineID"])
                    {
                        found = true;
                        break;
                    }
                    index++;
                }

                if (found)
                {
                    ddlPollingEngine.SelectedIndex = index;
                }
            }
        }
    }

    protected void ddlPollingEngine_SelectedIndexChanged(object sender, EventArgs e)
    {
        log.DebugFormat("Polling engine selected index changed to {0}", EngineID);
        if (EngineChanged != null)
            EngineChanged(this, EventArgs.Empty);
    }
}
