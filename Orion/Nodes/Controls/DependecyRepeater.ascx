<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DependecyRepeater.ascx.cs" Inherits="Orion_Nodes_Controls_DependecyRepeater" %>

<asp:Repeater ID="DependencyTable" runat="server">
    <HeaderTemplate>
        <table width="100%" cellspacing="0" cellpadding="2">
            <tr class="ReportHeader">
                <td class="ReportHeader" colspan="2">
                    &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_142) %>
                </td>
                <td class="ReportHeader" colspan="3">
                    &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_143) %>
                </td>
                <td class="ReportHeader" colspan="2">
                    &nbsp;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_144) %>
                </td>
            </tr>
    </HeaderTemplate>
    <ItemTemplate>
        <tr>
            <td class="Property" width="10px" style="padding: 0px 5px 0px 5px;">
                <img src="/Orion/images/dependency_16x16.gif" alt="" />
            </td>
            <td class="Property" width="35%">
                <%# DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(DataBinder.Eval(Container.DataItem,"Name").ToString())) %>
            </td>
            <td class="Property" style="padding: 0px 5px 0px 5px;"
                width="10px">
                <img src="<%# DefaultSanitizer.SanitizeHtml(CommonWebHelper.GetStatusIconPath(DataBinder.Eval(Container.DataItem,"ParentUri"), DataBinder.Eval(Container.DataItem,"ParentStatus"))) %>"
                    alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_14) %>" />
            </td>
            <td class="Property" width="25%">
                <%# DefaultSanitizer.SanitizeHtml(CommonWebHelper.RenderObjectName(DataBinder.Eval(Container.DataItem,"ParentUri"), DataBinder.Eval(Container.DataItem,"ParentDisplayName"))) %>
            </td>
            <td class="Property" style="padding: 0px 5px 0px 5px;"
                width="16px">
                <img src="/Orion/images/arrow_forward.gif" alt="" />
            </td>
            <td class="Property" style="padding: 0px 5px 0px 5px;"
                width="10px">
                <img src="<%# DefaultSanitizer.SanitizeHtml(CommonWebHelper.GetStatusIconPath(DataBinder.Eval(Container.DataItem,"ChildUri"), DataBinder.Eval(Container.DataItem,"ChildStatus"))) %>"
                    alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_14) %>" />
            </td>
            <td class="Property" width="25%">
                <%# DefaultSanitizer.SanitizeHtml(CommonWebHelper.RenderObjectName(DataBinder.Eval(Container.DataItem,"ChildUri"), DataBinder.Eval(Container.DataItem,"ChildDisplayName"))) %>
            </td>
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr>
            <td class="Property" style="background-color: #f9f9f9; padding: 0px 5px 0px 5px;"
                width="10px">
                <img src="/Orion/images/dependency_16x16.gif" alt="" />
            </td>
            <td class="Property" style="background-color: #f9f9f9;" width="35%">
                <%# DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(DataBinder.Eval(Container.DataItem,"Name").ToString())) %>
            </td>
            <td class="Property" style="background-color: #f9f9f9; padding: 0px 5px 0px 5px;"
                width="10px">
                <img src="<%# DefaultSanitizer.SanitizeHtml(CommonWebHelper.GetStatusIconPath(DataBinder.Eval(Container.DataItem,"ParentUri"), DataBinder.Eval(Container.DataItem,"ParentStatus"))) %>"
                    alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_14) %>" />
            </td>
            <td class="Property" style="background-color: #f9f9f9;" width="25%">
                <%# DefaultSanitizer.SanitizeHtml(CommonWebHelper.RenderObjectName(DataBinder.Eval(Container.DataItem,"ParentUri"), DataBinder.Eval(Container.DataItem,"ParentDisplayName"))) %>
            </td>
            <td class="Property" style="background-color: #f9f9f9; padding: 0px 5px 0px 5px;"
                width="16px">
                <img src="/Orion/images/arrow_forward.gif" alt="" />
            </td>
            <td class="Property" style="background-color: #f9f9f9; padding: 0px 5px 0px 5px;"
                width="10px">
                <img src="<%# DefaultSanitizer.SanitizeHtml(CommonWebHelper.GetStatusIconPath(DataBinder.Eval(Container.DataItem,"ChildUri"), DataBinder.Eval(Container.DataItem,"ChildStatus"))) %>"
                    alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_14) %>" />
            </td>
            <td class="Property" style="background-color: #f9f9f9;" width="25%">
                <%# DefaultSanitizer.SanitizeHtml(CommonWebHelper.RenderObjectName(DataBinder.Eval(Container.DataItem,"ChildUri"), DataBinder.Eval(Container.DataItem,"ChildDisplayName"))) %>
            </td>
        </tr>
    </AlternatingItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>
