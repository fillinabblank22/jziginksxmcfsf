<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListOfNodesValidation.ascx.cs"
    Inherits="Orion_Nodes_Controls_ListOfNodesValidation" %>
<center><b style="text-align:center"> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_394) %></b></center>
<div style="padding: 15px;">
     <div style="overflow:auto; height:250px;">
    <asp:Literal ID="litList" runat="server"></asp:Literal>
    </div>
    <br />
    <div style="text-align: right;">
        <orion:LocalizableButton runat="server" DisplayType="Primary" LocalizedText="Ok" ID="imbtnOk" OnClick="imbtnOk_Click" />
    </div>
</div>
