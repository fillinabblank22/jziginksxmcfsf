<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PollingMethodSelector.ascx.cs" Inherits="Orion_Nodes_Controls_PollingMethodSelector" %>
<%@ Register Src="~/Orion/Nodes/Controls/SnmpInfo.ascx" TagPrefix="orion" TagName="SNMPInfo" %>
<%@ Register Src="~/Orion/Nodes/Controls/WmiInfo.ascx"  TagPrefix="orion" TagName="WMIInfo" %>
<%@ Register Src="~/Orion/Nodes/Controls/AgentInfo.ascx"  TagPrefix="orion" TagName="AgentInfo" %>

<tr>
    <td class="leftSectionTitle pollingSelector">
        <asp:CheckBox ID="cbMultipleSelection" runat="server" AutoPostBack="True" OnCheckedChanged="cbMultipleSelection_CheckedChanged"/>
        <asp:Literal ID="OuterTitle" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_1 %>" />
    </td>
    <td class="rightInputColumn pollingSelector">
    </td>
</tr>
<tr>
    <td class="leftLabelColumn pollingSelector">
        <asp:Literal ID="InnerTitle" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_SO0_1 %>" />
    </td>
    <td class="rightInputColumn pollingSelector">
        <div class="sw-pg-hint-body-info">
            <a href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("OrionCoreAG-ChoosingPollingMethods")) %>" class="sw-link" target="_blank" rel="noopener noreferrer">
                <span class="sw-pg-hint-text"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.PollingMethod_Hint) %></span>
            </a>
        </div>
    </td>
</tr>
 <tr class="<%= DefaultSanitizer.SanitizeHtml(TrCssClass) %>">
        <td class="leftLabelColumn pollingSelector" >
        </td>
         <td class="rightInputColumn pollingSelector">
            <label>
                <asp:RadioButton ID="rbExternal" runat="server" AutoPostBack="true" GroupName="PollingMethod" OnCheckedChanged="OnExternalNodeChecked" />
                <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_228) %></span>
            </label>
            <br />
            <span class="helpfulText">
            <asp:Literal ID="Literal6" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_33 %>" />
            </span>            
        </td>        
 </tr>
 <tr class="<%= DefaultSanitizer.SanitizeHtml(TrCssClass) %>">
        <td class="leftLabelColumn pollingSelector">            
        </td>
        <td class="rightInputColumn pollingSelector">
            <label>
                <asp:RadioButton ID="rbICMP" runat="server" AutoPostBack="true" GroupName="PollingMethod" OnCheckedChanged="OnICMPChecked"/>
                <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_229) %></span>
            </label>
            <br />
            <span class="helpfulText">
                <asp:Literal ID="Literal3" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_32 %>" />
            </span>            
        </td>        
 </tr>
 <tr class="<%= DefaultSanitizer.SanitizeHtml(TrCssClass) %>">
        <td class="leftLabelColumn pollingSelector">            
        </td>
        <td class="rightInputColumn pollingSelector">
            <label>
                <asp:RadioButton ID="rbSNMP" runat="server" AutoPostBack="true" GroupName="PollingMethod" OnCheckedChanged="OnSNMPChecked"/>
                <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_231) %></span>
            </label>
            <br />
            <span class="helpfulText">
                <asp:Literal ID="Literal2" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_233 %>" />
            </span>
            <asp:Panel runat="server" ID="SNMPInfo" CssClass="snmpInfo">
                <orion:SNMPInfo runat="server" ID="SNMPInfo1" MultipleSelection="false" SNMPTitle=""
                    SNMPVersionDescription="SNM" />                
            </asp:Panel>
        </td>        
 </tr>
 <tr class="<%= DefaultSanitizer.SanitizeHtml(TrCssClass) %>">
        <td class="leftLabelColumn pollingSelector">            
        </td>
        <td class="rightInputColumn pollingSelector">
            <label>
                <asp:RadioButton ID="rbWMI" runat="server" AutoPostBack="true" GroupName="PollingMethod" OnCheckedChanged="OnWindowsServersChecked"/>
                <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_230) %></span>
            </label>
            <br />
            <span class="helpfulText">
                <asp:Literal ID="Literal5" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_232 %>" />
            </span>
            <asp:Panel runat="server" ID="WMIInfoPanel" CssClass="snmpInfo">
                <orion:WMIInfo runat="server" ID="WMIInfo" MultipleSelectionVisible="false"   />
            </asp:Panel>
        </td>        
 </tr>
<asp:Panel runat="server" ID="agentPollingMethodBlock">
 <tr class="<%= DefaultSanitizer.SanitizeHtml(TrCssClass) %>">
        <td class="leftLabelColumn pollingSelector">            
        </td>
        <td class="rightInputColumn pollingSelector">
            <label>
                <asp:RadioButton ID="rbAgent" runat="server" AutoPostBack="true" GroupName="PollingMethod" OnCheckedChanged="OnAgentChecked"/>
                <span><%= DefaultSanitizer.SanitizeHtml(AgentInfo.AgentPollingMethodLabel) %></span>  
            </label>
            <br />
            <span class="helpfulText">
                <%= DefaultSanitizer.SanitizeHtml(AgentInfo.AgentPollingMethodDescription) %>
                <a href="<%= DefaultSanitizer.SanitizeHtml(AgentInfo.AgentPollingMethodHelpLink) %>" class="sw-link" target="_blank" rel="noopener noreferrer">
                    &#0187;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_HintLinkText) %>
                </a>
            </span>
            <asp:Panel runat="server" ID="AgentInfoPanel" CssClass="snmpInfo">
                <orion:AgentInfo runat="server" ID="AgentInfo1" MultipleSelectionVisible="false" ViewMode="Windows" />
            </asp:Panel>
        </td>
 </tr>
</asp:Panel>

<asp:PlaceHolder runat="server" ID="pluginMethodsPlaceholder" />