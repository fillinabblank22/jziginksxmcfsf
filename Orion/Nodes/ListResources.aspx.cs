using System;
using System.Collections.Generic;
using System.Web.UI;

using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.ModuleManager;
using SolarWinds.Orion.Web;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.NpmAdapters;
using System.Net;
using SolarWinds.Orion.Web.Discovery;
using SolarWinds.Orion.Web.DAL;
using System.Linq;
using SolarWinds.Orion.Core.Models.Credentials;
using SolarWinds.Orion.Core.Common.Agent;
using System.Data;
using System.Web.Services;
using System.Globalization;
using SolarWinds.Orion.Core.Common.Enums;
using System.Web;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

public partial class Orion_Nodes_ListResources : System.Web.UI.Page
{
    private static readonly Log log = new Log();
    private const string defaultNodeManagementPageUrl = "/Orion/Nodes/Default.aspx";

    private static readonly ICoreBusinessLayerProxyCreator blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    [Serializable]
    protected class NodeInfo
    {
        public string Caption;
        public int EngineId;
        public IPAddress IpAddress;
        public NodeSubType NodeSubType;

        public static NodeInfo FromDataRow(DataRow row)
        {
            NodeInfo nodeInfo = new NodeInfo();

            nodeInfo.Caption = row["Caption"].ToString();
            string ip = row["IPAddress"].ToString();
            if (!IPAddress.TryParse(ip, out nodeInfo.IpAddress))
            {
                log.ErrorFormat("Unable to get IP Address - {0}", ip);
                nodeInfo.IpAddress = IPAddress.None;
            }
            nodeInfo.EngineId = (int)row["EngineID"];

            nodeInfo.NodeSubType = NodeSubType.None;
            Enum.TryParse<NodeSubType>(row["ObjectSubType"].ToString(), out nodeInfo.NodeSubType);
            
            return nodeInfo;
        }
    }

    public string ReturnURL
    {
        get
        {
            return ReferrerRedirectorBase.GetDecodedReferrerUrlOrDefault(defaultNodeManagementPageUrl);
        }
    }

    protected NodeInfo MyNodeInfo
    {
        get { return (NodeInfo)this.ViewState["nodeInfo"]; }
        set { this.ViewState["nodeInfo"] = value; }
    }

    public string NodeCaption 
    {
        get { return MyNodeInfo != null ? HttpUtility.HtmlEncode(MyNodeInfo.Caption) : string.Empty; }
    }

    public int NodeId 
    {
        get { return (int)this.ViewState["nodeId"]; }
        set { this.ViewState["nodeId"] = value; }
    }

    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    private void LogAndDisplayError(string message)
    {
        log.ErrorFormat(message);
    }

    private bool ForceRefresh
    {
        get
        {
            return !string.IsNullOrEmpty(this.Request.QueryString["ForceRefresh"]) &&
                   string.Equals(this.Request.QueryString["ForceRefresh"], "true", StringComparison.OrdinalIgnoreCase);
        }
    }

    private bool GetNodeId(out int nodeId)
    {
        string nodeIdString = this.Request.QueryString["Nodes"];

        if (String.IsNullOrEmpty(nodeIdString) || !Int32.TryParse(nodeIdString, out nodeId))
        {
            LogAndDisplayError(String.Format("Node ID [{0}] is not a valid number", nodeIdString));
            nodeId = 0;
            return false;
        }

        return true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        int nodeId;

        if (!GetNodeId(out nodeId))
            return;
        this.NodeId = nodeId;

        IconQuickLinks1.NavigateToUrl = String.Format("/Orion/DeviceStudio/Admin/CreatePollerWizard/default.aspx?NodeId={0}", NodeId);

        if (!this.IsPostBack)
        {
            
            var nodeInfoRow = new WebDAL().GetNodeInfo(nodeId);
            if (nodeInfoRow == null)
            {
                LogAndDisplayError(String.Format("Unable to get node for node ID [{0}]", nodeId));
                return;
            }

            var nodeInfo = NodeInfo.FromDataRow(nodeInfoRow);
            MyNodeInfo = nodeInfo;

            this.TopInfoBox.Visible = false;
            this.ResourceTree.Visible = false;
            bool nodeReady = nodeInfo.NodeSubType == NodeSubType.Agent ? CheckAgentStatus(NodeId) : true;
            if (nodeReady)
            {
                StartCreatingJob(NodeId, nodeInfo.EngineId, nodeInfo.IpAddress, ForceRefresh);
            }

            
            if (ShowPollerLinks())
            {
                pollerTooltip.Visible = true;
                IconQuickLinks1.Visible = true;
            }
            else
            {
                pollerTooltip.Visible = false;
                IconQuickLinks1.Visible = false;
            }
        }
        else
        {
            string eventArgument = Request["__EVENTARGUMENT"];
            if (eventArgument.Equals("AGENT_DEPLOYED", StringComparison.OrdinalIgnoreCase))
            {
                var nodeInfo = MyNodeInfo;
                StartCreatingJob(NodeId, nodeInfo.EngineId, nodeInfo.IpAddress, ForceRefresh);
            }
            else if (eventArgument.StartsWith("JOB_ID", StringComparison.OrdinalIgnoreCase))
            {
                var parts = eventArgument.Split('=');
                if (parts.Length > 1)
                {
                    Guid jobId;
                    if (Guid.TryParse(parts[1], out jobId))
                    {
                        InitResourceTree(jobId, MyNodeInfo);
                    }
                }
            }
        }
    }

    private void StartCreatingJob(int nodeId, int engineId, IPAddress ip, bool forceRefresh)
    {
        ScriptManager.RegisterStartupScript(this.Page, GetType(), "CreateDiscoveryJob", 
            ControlHelper.JsFormat("$(function() {{ CreateDiscoveryJob('{0}', '{1}', '{2}', {3}); }});", 
            nodeId.ToString(), engineId.ToString(), ip.ToString(),
            forceRefresh ? "true" : "false"
            ), true);
        DiscoveryJobWaitingImage.Visible = true;
    }

    [WebMethod]
    public static string CreateDiscoveryJob(int nodeId, int engineId, string ipAddress, bool forceRefresh)
    {
        IPAddress ip = IPAddress.None;
        if (!IPAddress.TryParse(ipAddress, out ip))
        {
            log.ErrorFormat("Error parsing IP address - {0}", ipAddress);
            return null;
        }

        using (var proxy = blProxyCreator.Create(BusinessLayerExceptionHandler, engineId))
        {
            try
            {
                return proxy.CreateOneTimeDiscoveryJobWithCache(nodeId, null, null, ip, engineId, null,
                    forceRefresh ? DiscoveryCacheConfiguration.ForceInvalidation : DiscoveryCacheConfiguration.EnableCaching
                    ).ToString();
            }
            catch (Exception ex)
            {
                log.Warn(ex);
                return null;
            }
        }
    }

    private void InitResourceTree(Guid jobId, NodeInfo nodeInfo)
    {
        DiscoveryJobWaitingImage.Visible = false;
        this.ResourceTree.Visible = true;

        // set resource tree info into session
        ResourceTreeHelper.AddOrReplaceResourceTreeInfo(jobId, nodeInfo.IpAddress, nodeInfo.EngineId);

        // set info ID on control - will be carried by viewstate like a permanent link
        this.ResourceTree.InfoId = jobId;
        this.ResourceTree.NodeName = this.NodeCaption;
    }

    private bool CheckAgentStatus(int nodeId)
    {
        try
        {
            using (var proxy = blProxyCreator.Create(BusinessLayerExceptionHandler))
            {
                var agentInfo = proxy.GetAgentInfoByNodeId(nodeId);
                if (agentInfo == null)
                {
                    log.ErrorFormat("Agent not found for NodeID={0}", nodeId);
                    DisplayAgentError();
                    return false;
                }

                var deploymentInfo = proxy.GetAgentDeploymentInfo(agentInfo.AgentId);
                switch (deploymentInfo.StatusInfo.Status)
                {
                    case AgentDeploymentStatus.Finished:
                        return true;
                    case AgentDeploymentStatus.PendingReboot:
                        TopInfoBox.Visible = true;
                        TopInfoBoxText.InnerText = string.Format(CultureInfo.CurrentUICulture, Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_RebootRequired, NodeCaption);
                        return true;
                    case AgentDeploymentStatus.Failed:
                        DisplayAgentError();
                        return false;
                    case AgentDeploymentStatus.PluginsRequired:
                        ScriptManager.RegisterStartupScript(this.Page, GetType(), "DeployMissingPlugins", ControlHelper.JsFormat("$(function() {{ DeployMissingPlugins('{0}', '{1}'); }});", agentInfo.AgentId.ToString(), NodeCaption), true);
                        return false;
                    case AgentDeploymentStatus.InProgress:
                    case AgentDeploymentStatus.PendingPluginInstallation:
                        ScriptManager.RegisterStartupScript(this.Page, GetType(), "DisplayAgentProgress", ControlHelper.JsFormat("$(function() {{ ShowAgentProgressDialog('{0}', '{1}'); }});", agentInfo.AgentId.ToString(), NodeCaption), true);
                        return false;
                    default:
                        return false;
                }
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
            DisplayAgentError();
            return false;
        }
    }

    private void DisplayAgentError()
    {
        ScriptManager.RegisterStartupScript(this.Page, GetType(), "DisplayAgentError", ControlHelper.JsFormat("$(function() {{ ShowAgentErrorDialog('{0}'); }});", NodeCaption), true);
    }

    protected bool ShowPollerLinks()
    {
        string nodeSubType = this.Request.QueryString["SubType"];
        return (ModuleManager.InstanceWithCache.IsThereModule("NPM") && !string.IsNullOrEmpty(nodeSubType) &&
                string.Compare(nodeSubType, "WMI", StringComparison.OrdinalIgnoreCase) != 0);
    }
    protected void imbtnCancel_Click(object sender, EventArgs e)
    {
        ReferrerRedirectorBase.Return(defaultNodeManagementPageUrl);
    }
 }
