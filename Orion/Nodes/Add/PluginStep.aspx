<%@ Page Language="C#" MasterPageFile="~/Orion/Nodes/NodeManagementPage.master" AutoEventWireup="true"
    CodeFile="PluginStep.aspx.cs" Inherits="Orion_Nodes_Add_PluginStep" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_10 %>" %>
    <%@ Register Src="~/Orion/Nodes/Add/ProgressIndicator.ascx" TagName="Progress" TagPrefix="orion" %>
    <%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
</asp:Content>

<asp:Content ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton runat="server" HelpUrlFragment="OrionPHNodeManagementAddNodeAddPollers" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    
    <h1 class="sw-hdr-title"><asp:Literal ID="Literal2" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_10 %>" /></h1>

    <orion:Progress ID="ProgressIndicator1" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress runat="server" ID="UpdateProgress1" DynamicLayout="false">
                <ProgressTemplate>
				   <div style="margin:5px 0 5px 0">
                    &nbsp;<img src="../../images/AJAX-Loader.gif" />
                    <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_11 %>" />
				   </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:PlaceHolder ID="phPluginStepContainer" runat="server"></asp:PlaceHolder>
            <div class="sw-btn-bar-wizard">
                <orion:LocalizableButton runat="server" DisplayType="Secondary" LocalizedText="Back" automation="Back" ID="imgbBack" OnClick="imgbBack_Click" />
                <orion:LocalizableButton runat="server" DisplayType="Primary" LocalizedText="Next" automation="Next" ID="imgbNext" OnClick="imgbNext_Click" />
                <orion:LocalizableButton runat="server" DisplayType="Secondary" LocalizedText="Cancel" automation="Cancel" ID="imgbCancel" OnClick="imgbCancel_Click" CausesValidation="false" />
            </td>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
