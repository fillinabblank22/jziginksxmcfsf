<%@ Page Language="C#" MasterPageFile="~/Orion/Nodes/NodeManagementPage.master" AutoEventWireup="true"
    CodeFile="Properties.aspx.cs" Inherits="Orion_Nodes_Add_Properties" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_10 %>" EnableEventValidation="false" %>
<%@ Register Src="~/Orion/Nodes/Add/ProgressIndicator.ascx" TagName="Progress" TagPrefix="orion" %>
<%@ Register Src="~/Orion/Nodes/Controls/HostNameIP.ascx" TagPrefix="orion" TagName="HostNameIP" %>
<%@ Register Src="~/Orion/Nodes/Controls/SnmpInfo.ascx" TagPrefix="orion" TagName="SNMPInfo" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Nodes/Controls/PollingSettings.ascx" TagPrefix="orion" TagName="PollingSettings" %>
<%@ Register Src="~/Orion/Nodes/Controls/CustomProperties.ascx" TagPrefix="orion" TagName="CustomProperties" %>
<%@ Register Src="~/Orion/Nodes/Controls/PollingMethodSelector.ascx"  TagPrefix="orion" TagName="PollingMethodSelector" %>
<%@ Register Src="~/Orion/Nodes/Controls/WmiInfo.ascx" TagPrefix="orion" TagName="WmiInfo" %>
<%@ Register Src="~/Orion/Nodes/Controls/NodeCategory.ascx"  TagPrefix="orion" TagName="NodeCategory" %>
<%@ Register Src="~/Orion/Nodes/Controls/AgentInfo.ascx"  TagPrefix="orion" TagName="AgentInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <orion:Include runat="server" File="js/jquery/jquery.autocomplete.css" />
    <orion:Include runat="server" File="jquery/jquery.autocomplete.js" />
</asp:Content>

<asp:Content ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton ID="NodesHelpButton1" runat="server" HelpUrlFragment="OrionPHNodeManagementAddNodeChangeProperties" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">

<script type="text/javascript">function ExecutePostBackEventReference(){<%=this.Page.ClientScript.GetPostBackEventReference(imgbOk, "") %>};</script>
<orion:Include runat="server" File="WebForm_FireDefaultButton.js" />

    <h1 class="sw-hdr-title"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>

<asp:LinkButton ID="RedirectLbt" runat="server" OnClick="DoRedirectLbt" />
    <orion:Progress ID="ProgressIndicator1" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="GroupBox">
                <span class="ActionName"><asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_12 %>" /></span>
                <br />
                <span><asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_19 %>" /></span>
                <table width="100%" class="PollingMethodSelectorTable">
                    <tr>
                        <td colspan="2">
							<table>
								<tr>
									<td class="leftLabelColumn">
										<asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_14 %>" />
									</td>
									<td class="rightInputColumn"><asp:TextBox runat="server" Width="250px" ID="txtCaption"></asp:TextBox>            
									</td>
									<td>
									</td>
								</tr>
                                <tr>
									<td class="leftLabelColumn">
										<asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_34 %>" />
									</td>
									<td class="rightInputColumn">
                                        <asp:Label ID="pollingIP" runat="server" />           
									</td>
									<td>
									</td>
								</tr>								
							</table>
                        </td>
                    </tr>                    
                    <orion:PollingMethodSelector runat="server" ID="PollingMethodSelector" OnPollingMethodChanged="HostNameIP_SNMPModeChanged" TrCssClass="blueBox" Mode="WizChangeProperties" />
                    <tr>
                        <td colspan="2">
                            <orion:PollingSettings runat="server" ID="NodePolling" MultipleSelection="false" ShowPollingEngineDetails="true" HideTopologySection="true" HideChangePollingEngineButton="True" />
                        </td>
                    </tr>
                </table>

                <div class="contentBlock">                    
                    <orion:NodeCategory runat="server" ID="NodeCategory"  MultipleSelection="false" />                    
                </div>

                <div id="pluginDiv" runat="server">
                </div>
                
                <div><table border=0 width="100%"><tr><td style="padding: 0;">
				<asp:UpdateProgress runat="server" ID="UpdateProgress1" DynamicLayout="false">
					<ProgressTemplate>
					    &nbsp;<img src="../../images/AJAX-Loader.gif" />
					    <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_11 %>" />
					</ProgressTemplate>
				</asp:UpdateProgress>				        
                </td><td style="padding: 0;">
                    <div class="sw-btn-bar-wizard">
                        <orion:LocalizableButton runat="server" DisplayType="Secondary" LocalizedText="Back" automation="Back" ID="imgbBack" OnClick="imgbBack_Click" />
                        <orion:LocalizableButton runat="server" DisplayType="Primary" LocalizedText="CustomText" automation="AddNode" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_69 %>" ID="imgbOk" OnClick="imgbOk_Click"/>
                        <orion:LocalizableButton runat="server" DisplayType="Secondary" LocalizedText="Cancel" automation="Cancel" ID="imbtnCancel" OnClick="imgbCancel_Click" CausesValidation="false" />
                    </div
                </td></tr></table></div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
