using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI.Localizer;

public enum AddNodeMode
{
	Standard,
	ICMPOnly
} ;


[Obsolete]
public enum AddNodeStep
{
	Default,
	Resources,
	CustomPollers,
	Properties,
	PluginStep,
};

public partial class Orion_Nodes_Controls_ProgressIndicator : System.Web.UI.UserControl
{
	private readonly string indicatorImageFolderPath = "~/Orion/images/nodemgmt_art/progress_indicator/";

	protected void Page_Init(object sender, EventArgs e)
	{
		
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
			Reload(); 
	}

	protected override void OnInit(EventArgs e) 
	{
		base.OnInit(e);
        OrionInclude.CoreFile("ProgressIndicator.css");
    }

	#region public properties
	[Browsable(true)]
	[Category("Appearence")]
	public AddNodeMode Mode
	{
		get
		{
			object o = ViewState["AddNodeMode"];
			return (o == null ? AddNodeMode.Standard : (AddNodeMode)ViewState["AddNodeMode"]);
		}
		set
		{
			ViewState["AddNodeMode"] = value;
		}
	}

    [Obsolete]
	public AddNodeStep Step
	{
		get
		{
			return (AddNodeStep)ViewState["AddNodeStep"];
		}
		set
		{
			ViewState["AddNodeStep"] = value;
		}
	}
	#endregion

	#region public methods
	public void Reload()
	{
		//LoadFromUrl();
		LoadFromCurrentStep();
		RenderProgressImages();
	}
	#endregion

	#region private methods
	private void RenderProgressImages()
	{
		phPluginImages.Controls.Clear();

		List<SolarWinds.Orion.Web.WizardStep> steps = NodeWorkflowHelper.Steps;
		int count = steps.Count;		

		for (int i=0; i<count; i++)
		{
			bool nextActive;
			bool isCurrentStep = false;
			
			// Could be removed due to it doesn't use anymore as well as from Discovery wizard
			Image imgStep = new Image();
			imgStep.ID = string.Format("imgStep{0}",i+1);

			Image imgSeparator = new Image();
			imgSeparator.ID = string.Format("imgSep{0}", i+1);

			if (NodeWorkflowHelper.GetCurrentWizardStep().IsPluginStep && steps[i].IsPluginStep && NodeWorkflowHelper.CurrentPluginIndex < NodeWorkflowHelper.PluginSteps.Count)
			{
				if (NodeWorkflowHelper.PluginSteps[NodeWorkflowHelper.CurrentPluginIndex].UserControlPath.Equals(steps[i].UserControlPath))
				{
					isCurrentStep = true;
					imgStep.ImageUrl = steps[i].IndicatorActiveImage;
					nextActive = ((i < count - 1) && NodeWorkflowHelper.PluginSteps[NodeWorkflowHelper.CurrentPluginIndex].UserControlPath.Equals(steps[i + 1].UserControlPath) ? true : false);
					imgSeparator.ImageUrl = nextActive ? string.Format(string.Format("{0}pi_sep_{1}_{2}.gif", indicatorImageFolderPath, "on", "on")) : 
						string.Format(string.Format("{0}pi_sep_{1}_{2}.gif", indicatorImageFolderPath, "on", "off"));
				}
				else
				{
					imgStep.ImageUrl = steps[i].IndicatorImage;
					nextActive = ((i < count - 1) && NodeWorkflowHelper.PluginSteps[NodeWorkflowHelper.CurrentPluginIndex].UserControlPath.Equals(steps[i + 1].UserControlPath) ? true : false);
					imgSeparator.ImageUrl = nextActive ? string.Format(string.Format("{0}pi_sep_{1}_{2}.gif", indicatorImageFolderPath, "off", "on")) : 
						string.Format(string.Format("{0}pi_sep_{1}_{2}.gif", indicatorImageFolderPath, "off", "off"));
				}
			}
			else
				if (NodeWorkflowHelper.GetCurrentWizardStep().Equals(steps[i]))
				{
					isCurrentStep = true;
					if (!steps[i].IsPluginStep)
					{
						imgStep.ImageUrl = string.Format("{0}PI_{1}_on.gif", indicatorImageFolderPath, steps[i].Name);
					}
					else
					{
						imgStep.ImageUrl = steps[i].IndicatorActiveImage;
					}
					nextActive = ((i < count - 1) && NodeWorkflowHelper.GetCurrentWizardStep().Equals(steps[i + 1]) ? true : false);

					if (nextActive)
						imgSeparator.ImageUrl = string.Format(string.Format("{0}pi_sep_{1}_{2}.gif", indicatorImageFolderPath, "on", "on"));
					else imgSeparator.ImageUrl = string.Format(string.Format("{0}pi_sep_{1}_{2}.gif", indicatorImageFolderPath, "on", "off"));
				}
				else
				{
					if (!steps[i].IsPluginStep)
					{
						imgStep.ImageUrl = string.Format("{0}PI_{1}_off.gif", indicatorImageFolderPath, steps[i].Name);
					}
					else
					{
						imgStep.ImageUrl = steps[i].IndicatorImage;
					}

					nextActive = ((i < count - 1) && NodeWorkflowHelper.GetCurrentWizardStep().Equals(steps[i + 1]) ? true : false);
					if (NodeWorkflowHelper.GetCurrentWizardStep().IsPluginStep && NodeWorkflowHelper.CurrentPluginIndex > 0)
					{
						nextActive = false;
					}

					if (nextActive)
						imgSeparator.ImageUrl = string.Format(string.Format("{0}pi_sep_{1}_{2}.gif", indicatorImageFolderPath, "off", "on"));
					else 
						imgSeparator.ImageUrl = string.Format(string.Format("{0}pi_sep_{1}_{2}.gif", indicatorImageFolderPath, "off", "off"));
				}


			Label tb = new Label();
			tb.Text = GetPluginStepDescription(steps[i]);
			tb.CssClass = string.Format("PI_{0}", isCurrentStep ? "on" : "off");
			// Text of first step must be move to the right a little 
			if (i == 0)
			{
				tb.Style.Add("padding-left", "10px;");
			}

			phPluginImages.Controls.Add(tb);
			
			//phPluginImages.Controls.Add(imgStep);
			phPluginImages.Controls.Add(imgSeparator);			
		}		
	}

	private string GetPluginStepDescription(SolarWinds.Orion.Web.WizardStep step)
	{
		if (!string.IsNullOrEmpty(step.Title))
			return step.Title;

		if (!string.IsNullOrEmpty(step.IndicatorImage))
		{
			var parts = step.IndicatorImage.Split('/');
			switch (parts[parts.Length - 1].ToLowerInvariant())
			{
				case "button.addapplicationmonitors.gif":
					return SolarWinds.Orion.Core.Common.i18n.TokenSubstitution.Parse("@{R=Core.Strings;K=NodeAddWizard_APM_AddApplicationMonitors;E=xml}");
				case "pi_custompollers_off.gif":
					return SolarWinds.Orion.Core.Common.i18n.TokenSubstitution.Parse("@{R=Core.Strings;K=NodeAddWizard_NPM_AddPollers;E=xml}");
				case "pi_pollforucs_off.gif":
					return SolarWinds.Orion.Core.Common.i18n.TokenSubstitution.Parse("@{R=Core.Strings;K=NodeAddWizard_NPM_PollForUCS;E=xml}");
			}
		}

		return step.Name;
 	}

	private void LoadFromCurrentStep()
	{		
	}

	[Obsolete("The invocation of method is deprecated.")]
	private void LoadFromUrl()
	{
		//if (SolarWinds.Orion.Web.NodeWorkflowHelper.Node != null)
		//{
		//    if (SolarWinds.Orion.Web.NodeWorkflowHelper.Node.SNMPVersion == SolarWinds.NPM.Common.Models.SNMPVersion.None)
		//    {
		//        Mode = AddNodeMode.ICMPOnly;
		//    }
		//    else
		//        Mode = AddNodeMode.Standard;
		//}

		//string currentPath = Request.Path;
		//if (Mode == AddNodeMode.ICMPOnly)
		//{

		//    if (currentPath.Contains("Default.aspx"))
		//        Step = AddNodeStep.DefineNode;
		//    if (currentPath.Contains("Properties.aspx"))
		//        Step = AddNodeStep.ChangeProperties;
		//}
		//else if (Mode == AddNodeMode.Standard)
		//{
		//    if (currentPath.Contains("Default.aspx"))
		//        Step = AddNodeStep.DefineNode;
		//    if (currentPath.Contains("Resources.aspx"))
		//        Step = AddNodeStep.ChooseResource;
		//    if (currentPath.Contains("CustomPollers.aspx"))
		//        Step = AddNodeStep.AssignCustomPollers;
		//    if (currentPath.Contains("Properties.aspx"))
		//        Step = AddNodeStep.ChangeProperties;
		//}
	}
	#endregion
}
