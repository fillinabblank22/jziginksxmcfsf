<%@ Page Language="C#" MasterPageFile="~/Orion/Nodes/NodeManagementPage.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="Orion_Nodes_Add_Default" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_10 %>"  %>

<%@ Register Src="~/Orion/Nodes/Add/ProgressIndicator.ascx" TagName="Progress" TagPrefix="orion" %>
<%@ Register Src="~/Orion/Nodes/Controls/HostNameIP.ascx" TagPrefix="orion" TagName="HostNameIP" %>
<%@ Register Src="~/Orion/Nodes/Controls/SnmpInfo.ascx" TagPrefix="orion" TagName="SNMPInfo" %>
<%@ Register Src="~/Orion/Nodes/Controls/PollingEngine.ascx" TagPrefix="orion" TagName="PollingEngine" %>
<%@ Register Src="~/Orion/Nodes/Controls/NodePropertyPluginSelector.ascx"  TagPrefix="orion" TagName="PluginSelector" %>
<%@ Register Src="~/Orion/Nodes/Controls/PollingMethodSelector.ascx"  TagPrefix="orion" TagName="PollingMethodSelector" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Nodes/Controls/WmiInfo.ascx" TagPrefix="orion" TagName="WmiInfo" %>
<%@ Register Src="~/Orion/Nodes/Controls/AgentInfo.ascx"  TagPrefix="orion" TagName="AgentInfo" %>
<%@ Register Src="~/Orion/Controls/InstallAgentDialog.ascx" TagPrefix="orion" TagName="InstallAgentDialog" %>
<%@ Register Src="~/Orion/Controls/InstallAgentProgress.ascx" TagPrefix="orion" TagName="InstallAgentProgress" %>
<%@ Register Src="~/Orion/Controls/InstallAgentError.ascx" TagPrefix="orion" TagName="InstallAgentError" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include runat="server" File="NodeMNG.css"  />
    <orion:Include runat="server" File="js/jquery/jquery.autocomplete.css" />
    <orion:Include runat="server" File="jquery/jquery.autocomplete.js" />
    <orion:Include runat="server" File="Nodes/js/TimeoutHandling.js" />
    <style type="text/css">        
        .rightInputColumn .helpfulText
        {
            color: #5F5F5F;
            font-size: 10px;
            padding-left: 25px;
            text-align: left;
            display: block;
        }
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton runat="server" HelpUrlFragment="OrionPHNodeManagementAddNodeDefineNode" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
<script type="text/javascript">function ExecutePostBackEventReference(){<%=this.Page.ClientScript.GetPostBackEventReference(imgbNext, "") %>};</script>
    <orion:Include runat="server" File="WebForm_FireDefaultButton.js" />

	<h1 class="sw-hdr-title"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>


    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>

            <div id="TopInfoBox" runat="server">
                <div class="sw-pg-hint-yellow" style="display:inline-block; width:100%; margin-bottom:15px;">
                    <div class="sw-pg-hint-body-warning">
                        <span id="TopInfoBoxText" runat="server"></span>
                    </div>
                </div>
            </div>

            <orion:Progress ID="ProgressIndicator1" runat="server" />
			<div class="GroupBox" id="NormalText">
                <span class="ActionName"><asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_5 %>" />
                    <asp:Literal runat="server" ID="litNodeIP"></asp:Literal></span>
				
                <br />
                <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_9 %>" />
				<span class="calloutHint">
				<asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_6 %>" />
				<a target="_blank" href="../../Discovery/Default.aspx?origUrl=Admin"><asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_7 %>" /></a>.</span>
				<div class="contentBlock">
                      <table>
    			        <orion:HostNameIP isDynamicIP="true" runat="server" ID="HostNameIP1" OnHostNameIPChanged="HostNameIP_OnHostNameIPChanged" />                        
                        <orion:PollingMethodSelector runat="server" ID="PollingMethodSelector" OnPollingMethodChanged="PollingMethodSelector_OnPollingMethodChanged" OnSelectedPollingEnginesChanged="PollingMethodSelector_OnSelectedPollingEnginesChanged" Mode="WizDefineNode" />
                    </table>
				</div>
				<div class="contentBlock">
					<orion:PollingEngine runat="server" ID="PollingEngine" OnEngineChanged="PollingEngine_EngineChanged" />
                    <table>
                    <orion:PluginSelector runat = "server" ID="pluginSelector"  />
                    </table>
				</div>
				<div class="contentBlock">                
				</div>
                <div id="pluginDiv" runat="server">
                </div>
                <div>
                    <table border="0" width="100%">
                        <tr>
                            <td style="padding: 0;">
                                <div id="validationProgress" style="display: none">
                        &nbsp;<img src="../../images/AJAX-Loader.gif" />
                                    <span style="font-weight: bold;">
                                        <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_8 %>" /></span>
                                </div>
                            </td>
                            <td style="padding: 0;">
                    <div class="sw-btn-bar-wizard">
                        <orion:LocalizableButton runat="server" DisplayType="Primary" LocalizedText="Next" automation="Next" ID="imgbNext" OnClick="imgbNext_Click" />
                        <orion:LocalizableButton runat="server" DisplayType="Secondary" LocalizedText="Cancel" ID="imgbCancel" automation="Cancel" OnClick="imgbCancel_Click" CausesValidation="false" />
                    </div>
                            </td>
                        </tr>
                    </table>
            </div>
            </div>
       </ContentTemplate>
    </asp:UpdatePanel>
    <asp:LinkButton runat="server" Text="" ID="btnInvoker" OnClick="btnInvoker_Click" />
    
    <%-- Duplicated node dialog definition --%>
    <div style="display:none; visibility:hidden">
        <div id="DuplicatedNodeDetectedDialog" style="background-color: #FFF;z-index: 110000;" title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_PF0_1) %>">

            <table><tr><td style="width:40px;" valign="top">
                <img src="/Orion/Images/stop_32x32.gif" alt="error" />
            </td><td>
                <div>
                    <div id="DuplicatedNodeDetectedDialogPart1" style="font-weight: bold"></div><br />
                    <div id="DuplicatedNodeDetectedDialogPart2"></div>
                    <% if (PollingEngine.Visible) { %>
                    <br />
                    <div id="DuplicatedNodeDetectedDialogPart3"></div>
                    <% } %>
                    <br />
                </div>  
            </td></tr></table>
            <div class="bottom">
                <div class="sw-btn-bar-wizard">
                    <orion:LocalizableButton runat="server" ID="btnContinue" LocalizedText="Next" automation="Next" OnClick="imgbNext_Click" />
                    <orion:LocalizableButton runat="server" ID="btnReAdd" LocalizedText="CustomText" automation="ReAdd" Text="<%$ HtmlEncodedResources:CoreWebContent, WEBCODE_PF0_4 %>" DisplayType="Secondary" OnClick="imgbNext_Click" />
		            <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" automation="Cancel" DisplayType="Primary" OnClientClick="CloseDuplicatedNodeDetectedDialog(); return false;" />
                </div>
	        </div>
        </div>
    </div>

    <script type="text/javascript">
        var _dialog;

        function OpenDuplicatedNodeDetectedDialog(part1, part2, part3, btnReAddVisible, btnContinueVisible) {
            _dialog = $('#DuplicatedNodeDetectedDialog').dialog({
                width: 600,
                modal: true,
                autoOpen: false,
                open: function () {
                    $('#DuplicatedNodeDetectedDialogPart1').html(part1);
                    $('#DuplicatedNodeDetectedDialogPart2').html(part2);
                    $('#DuplicatedNodeDetectedDialogPart3').html(part3);
                    $('#<%=btnCancel.ClientID%>').focus();
                    if (btnReAddVisible) {
                        $('#<%=btnReAdd.ClientID%>').show();
                    } else {
                        $('#<%=btnReAdd.ClientID%>').hide();
                    }
                    if (btnContinueVisible) {
                        $('#<%=btnContinue.ClientID%>').show();                        
                    } else {
                        $('#<%=btnContinue.ClientID%>').hide();
                    }
                }
            });
            $(_dialog).dialog('open');
        }

        function CloseDuplicatedNodeDetectedDialog() {
            if (_dialog == undefined)
                return;

            $(_dialog).dialog('close');
        }

        Sys.Application.add_init(appl_init);

        function appl_init() {
            var pageRequestManager = Sys.WebForms.PageRequestManager.getInstance();
            pageRequestManager.add_initializeRequest(initializeRequest);
            pageRequestManager.add_endRequest(EndRequest);
        }
        var postBackElement;

        function initializeRequest(sender, args) {
            postBackElement = args.get_postBackElement();
            if (postBackElement.id == '<%= imgbNext.ClientID %>') {
                document.getElementById('validationProgress').style.display = 'block';
            } else {
                document.getElementById('validationProgress').style.display = 'none';
            }
        }
        
        function EndRequest(sender, args) {
            if (postBackElement.id == '<%= imgbNext.ClientID %>') {
                document.getElementById('validationProgress').style.display = 'none';
            }
        }
    </script>
    <%-- End of duplicated node dialog definition --%>

    <%-- Agent deployment stuff --%>
    <orion:InstallAgentDialog ID="InstallAgent" runat="server" />
    <orion:InstallAgentProgress ID="InstallingAgent" runat="server" />
    <orion:InstallAgentError ID="InstallAgentFail" runat="server" />

    <script type="text/javascript">
        function DeployAgent(settingsId, nodeCaption, detectionInfo) {
            ShowInstallAgentDialog("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AddNodeWizard_DeployAgent_Title) %>",
                "/Orion/images/icon.question_32x32.png",
                String.format("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AddNodeWizard_DeployAgent_Text) %>", nodeCaption),
                detectionInfo,
                "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AddNodeWizard_DeployAgent_Start) %>", "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AddNodeWizard_DeployAgent_Cancel) %>", function (closeDialogCallback) {
                    params = JSON.stringify({
                        deploymentSettingsId: settingsId
                    });
                    $.ajax({
                        type: 'POST',
                        url: '/Orion/Services/AgentManagerService.asmx/StartDeployingAgent',
                        data: params,
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        success: function (result) {
                            var agentId = parseInt(result.d);
                            closeDialogCallback();
                            ShowInstallAgentProgress("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AddNodeWizard_DeployAgent_Progress_Title) %>", agentId, nodeCaption, DeployingAgentFinished);
                        },
                        error: function (ex) {
                            closeDialogCallback();
                            r = jQuery.parseJSON(ex.responseText);
                            ShowInstallAgentErrorDialog(nodeCaption, r.Message);
                        }
                    });
            });
        }

        function DeployAgentPlugins(agentId, nodeCaption) {
            params = JSON.stringify({
                agentId: agentId
            });
            $.ajax({
                type: 'POST',
                url: '/Orion/Services/AgentManagerService.asmx/StartDeployingPlugins',
                data: params,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {
                    ShowInstallAgentProgress("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AddNodeWizard_DeployAgent_Progress_Title) %>", agentId, nodeCaption, DeployingAgentFinished);
                },
                error: function (ex) {
                    r = jQuery.parseJSON(ex.responseText);
                    ShowInstallAgentErrorDialog(nodeCaption, r.Message);
                }
            });
        }

        function ReDeployFailedAgentPlugins(agentId, nodeCaption) {
            params = JSON.stringify({
                agentId: agentId
            });
            $.ajax({
                type: 'POST',
                url: '/Orion/Services/AgentManagerService.asmx/StartDeployingPlugins',
                data: params,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (result) {
                    ShowInstallAgentProgress("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AddNodeWizard_DeployAgent_Progress_Title) %>", agentId, nodeCaption, DeployingAgentFinished);
                },
                error: function (ex) {
                    r = jQuery.parseJSON(ex.responseText);
                    ShowInstallAgentErrorDialog(nodeCaption, r.Message);
                }
            });
        }

        function RebootMachine(agentId, nodeCaption) {
            ShowInstallAgentDialog(
                "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AddNodeWizard_DeployAgent_Reboot_Title) %>",
                "/Orion/images/NotificationImages/notification_warning.gif",
                String.format("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AddNodeWizard_DeployAgent_Reboot_Text) %>", nodeCaption),
                0,
                "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AddNodeWizard_DeployAgent_Reboot_Yes) %>",
                "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AddNodeWizard_DeployAgent_Reboot_No) %>",
                function (closeDialogCallback) {
                    params = JSON.stringify({
                        agentId: agentId
                    });
                    $.ajax({
                        type: 'POST',
                        url: '/Orion/Services/AgentManagerService.asmx/ApproveReboot',
                        data: params,
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        success: function (result) {
                            closeDialogCallback();
                            setTimeout(function () { ShowInstallAgentProgress("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AddNodeWizard_DeployAgent_Progress_Title) %>", agentId, nodeCaption, DeployingAgentFinished) }, 500);
                        },
                        error: function (ex) {
                            closeDialogCallback();
                            CloseInstallAgentProgress();
                            r = jQuery.parseJSON(ex.responseText);
                            ShowInstallAgentErrorDialog(nodeCaption, r.Message);
                        }
                    });
                },
                function () {
                    __doPostBack('<%= imgbNext.UniqueID %>', 'AGENT_DEPLOYED:' + agentId);
                });
        }

        function ShowDeploymentProgress(agentId, nodeCaption) {
            ShowInstallAgentProgress("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AddNodeWizard_DeployAgent_Progress_Title) %>", agentId, nodeCaption, DeployingAgentFinished);
        }

        function DeployingAgentFinished(agentId, nodeCaption, status) {
            if (status.Status == 1) {
                __doPostBack('<%= imgbNext.UniqueID %>', 'AGENT_DEPLOYED:'+agentId);
            } else if (status.Status == 2) {
                ShowInstallAgentErrorDialog(nodeCaption, status.StatusMessage);
            } else if (status.Status == 3) {
                RebootMachine(agentId, nodeCaption);
            }
        }

    </script>
    <%-- End of agent deployment stuff --%>

</asp:Content>
