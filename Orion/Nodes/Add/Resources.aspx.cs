using System;
using System.Collections.Generic;
using System.Net;
using System.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.ModuleManager;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Discovery;
using System.Web.Services;
using SolarWinds.Orion.Core.Common.Enums;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

public partial class Orion_Nodes_Add_Resources : System.Web.UI.Page, IStep
{
	private static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

	protected static void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}

	protected void Page_Init(object sender, EventArgs e)
	{
		Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
        if (!this.IsPostBack)
        {
            ResourceTree.Visible = false;

            if (IsDiscoveryNeeded())
            {
                StartDiscovery();
            }
            else
            {
                this.ResourceTree.Visible = true;
            }


            // set info ID on control - will be carried by viewstate like a permanent link
            this.ResourceTree.InfoId = NodeWorkflowHelper.ResourceTreeInfoId;
        }
        else
        {
            ProgressIndicator1.Reload();
        }
	}

    private bool IsDiscoveryNeeded()
    {
        if (NodeWorkflowHelper.ResourceTreeInfoId == Guid.Empty)
        {
            //First load requires discovery
            return true;
        }

        var info = ResourceTreeHelper.GetResourceTreeInfo(NodeWorkflowHelper.ResourceTreeInfoId);
        var node = NodeWorkflowHelper.Node;

        // IP, Engine or ObjectSubType change requires rediscovery
        return info != null && (info.IP.ToString() != node.IpAddress ||
                                info.ObjectSubType != node.ObjectSubType ||
                                info.EngineId != node.EngineID);
    }

    private void StartDiscovery()
	{
        ScriptManager.RegisterStartupScript(this.Page, GetType(), "CreateDiscoveryJob", "$(function() { CreateDiscoveryJob(); });", true);
        this.DiscoveryJobWaitingImage.Visible = true;
    }

    [WebMethod(EnableSession = true)]
    public static string CreateDiscoveryJob()
    {
        var node = NodeWorkflowHelper.Node;
        ICoreBusinessLayerProxyCreator blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

        IPAddress ip = IPAddress.Parse(node.IpAddress);

        List<Credential> credentials = new List<Credential>();

        if (node.NodeSubType == NodeSubType.Agent)
        {
            if (NodeWorkflowHelper.AgentCredential != null)
                credentials.Add(NodeWorkflowHelper.AgentCredential);
        }
        else
        {
            var snmpCredential = CredentialHelper.ParseCredentialsFromNode(node);
            if (snmpCredential != null)
                credentials.Add(snmpCredential);
            if (NodeWorkflowHelper.WmiCredential != null)
                credentials.Add(NodeWorkflowHelper.WmiCredential);
        }

        using (var proxy = blProxyCreator.Create(BusinessLayerExceptionHandler, node.EngineID))
        {
            try
            {
                return proxy.CreateOneTimeDiscoveryJobWithCache(null, node.SNMPPort, (SolarWinds.Orion.Core.Models.Credentials.SNMPVersion)((int)node.SNMPVersion), ip, node.EngineID, credentials,
                    // we're getting listresources for new node, so do not check the cache table
                    DiscoveryCacheConfiguration.ForceInvalidation 
                    ).ToString();
            }
            catch (Exception ex)
            {
                log.Warn(ex);
                return null;
            }
        }
    }

    private void InitResourceTree(Guid jobId)
    {
        //set visibility of controls
        this.DiscoveryJobWaitingImage.Visible = false;
        this.ResourceTree.Visible = true;

        var node = NodeWorkflowHelper.Node;
        IPAddress ip = IPAddress.Parse(node.IpAddress);

        // set resource tree info into session
        ResourceTreeHelper.AddOrReplaceResourceTreeInfo(jobId, ip, node.ObjectSubType, node.EngineID);

        // set info ID on control - will be carried by viewstate like a permanent link
        this.ResourceTree.InfoId = jobId;
        this.ResourceTree.NodeName = node.Caption;

        NodeWorkflowHelper.ResourceTreeInfoId = jobId;
    }

	protected void Page_Load(object sender, EventArgs e)
	{
		this.Page.Form.DefaultButton = imgbNext.UniqueID;
		imgbNext.Focus();
        if (!IsPostBack)
        {
            ValidatePage();
            //TODO: The text of message I assume can be retrieved from some resource file. Replace the message when needed.
            imgbCancel.Attributes["OnClick"] = ControlHelper.JsFormat("return confirm('{0}');", Resources.CoreWebContent.WEBCODE_YK0_14);
        }
        else
        {
            string eventArgument = Request["__EVENTARGUMENT"];
            if (eventArgument.StartsWith("JOB_ID", StringComparison.OrdinalIgnoreCase))
            {
                var parts = eventArgument.Split('=');
                if (parts.Length > 1)
                {
                    Guid jobId;
                    if (Guid.TryParse(parts[1], out jobId))
                    {
                        InitResourceTree(jobId);
                    }
                }
            }
        }

        if (CanShowHelpTips())
	    {
	        pollerTooltip.Visible = true;
	    }
	    else
	    {
	        pollerTooltip.Visible = false;
	    }
	}

	protected void btnBack_Click(object sender, EventArgs e)
	{
		Response.Redirect(NodeWorkflowHelper.GetPreviousStepUrl(), true);
	}
	protected void btnNext_Click(object sender, EventArgs e)
	{
		Response.Redirect(NodeWorkflowHelper.GetNextStepUrl(), true);
	}

	protected void imgbCancel_Click(object sender, EventArgs e)
	{
		SolarWinds.Orion.Web.NodeWorkflowHelper.Reset();
		Response.Redirect("../Default.aspx", true);
	}

	#region private methods
	private void ValidatePage()
	{
		if (SolarWinds.Orion.Web.NodeWorkflowHelper.Node == null)
			Response.Redirect("Default.aspx");
	}
	#endregion

    protected bool CanShowHelpTips()
    {
        var uimModule = ModuleManager.InstanceWithCache.IsThereModule("UIM");
        var npmModule = ModuleManager.InstanceWithCache.IsThereModule("NPM");
        return ((uimModule || npmModule) && NodeWorkflowHelper.Node.NodeSubType != NodeSubType.WMI);
    }
	#region IStep Members

	public string Step
	{
		get { return "Resources"; }
	}
	#endregion
}
