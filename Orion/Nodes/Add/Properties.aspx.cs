using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Json;
using System.ServiceModel;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web.NpmAdapters;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using System.Data;
using Node = SolarWinds.Orion.Core.Common.Models.Node;
using NodeSubType = SolarWinds.Orion.Core.Common.Models.NodeSubType;
using SNMPVersion = SolarWinds.Orion.Core.Common.Models.SNMPVersion;
using Interface = SolarWinds.Orion.Core.Common.Models.Interface;
using Interfaces = SolarWinds.Orion.Core.Common.Models.Interfaces;
using Volume = SolarWinds.Orion.Core.Common.Models.Volume;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Discovery;
using SolarWinds.Orion.Core.Models.Discovery;
using System.Linq;
using SolarWinds.Orion.Core.Models.DiscoveredObjects;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Core.Common.i18n;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web.Events;
using SolarWinds.Orion.Web.Agent;
using SolarWinds.Orion.Core.Common.Enums;
using SolarWinds.Orion.Core.Common.ModuleManager;
using SolarWinds.Orion.Core.Common.Indications;
using SolarWinds.Orion.Web.AddNode;
using SolarWinds.Orion.Web.UpdateNode;

public partial class Orion_Nodes_Add_Properties : System.Web.UI.Page, IStep
{
    private const string addNodePluginStateName = "AddNodeState";

    private static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
    private readonly AddNodePluginProvider addNodePluginProvider = new AddNodePluginProvider();
    private readonly UpdateNodePluginProvider updateNodePluginProvider = new UpdateNodePluginProvider();

    protected static void BusinessLayerExceptionHandler(Exception ex)
	{
		log.Error(ex);
	}

	protected override void OnPreInit(EventArgs e)
	{
		base.OnPreInit(e);

        // Reset active page key to avoid collision with edit node tabs
        NodePropertyPluginManager.ResetActivePage();

        Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
		if (!IsPostBack)
			NodePropertyPluginManager.Init(NodePropertyPluginExecutionMode.WizChangeProperties);
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		// loads all plugin controls visble or not
		pluginDiv.Controls.Clear();
		var controls = NodePropertyPluginManager.LoadControls(this.LoadControl);
		foreach (var control in controls)
		{
			pluginDiv.Controls.Add(control);
		}
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		this.Page.Form.DefaultButton = imgbOk.UniqueID;
		
        if (!IsPostBack)
		{
            imbtnCancel.Attributes["OnClick"] = ControlHelper.JsFormat("return confirm('{0}');", Resources.CoreWebContent.WEBCODE_YK0_14);

            if (NodeWorkflowHelper.Node == null)
				return;

			PollingMethodSelector.SetEngineId(NodeWorkflowHelper.Node.EngineID);
			ValidatePage();
			LoadProperties();

			NodePropertyPluginManager.InitPlugins(new List<Node>() { NodeWorkflowHelper.Node });

			this.ClientScript.RegisterStartupScript(typeof(Orion_Nodes_Add_Properties), "cs_autocomplete",
@"  <script type=""text/javascript"">
    //<![CDATA[
		Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function() {
			$('.CommunityStringTextBox').unautocomplete();
            $('.CommunityStringTextBox').autocomplete([" + this.GetReadOnlyCommunityStrings() + @"], {max : 1000, minChars : 0});
            if(typeof(CommunityStringTextBoxChangedEvent) != 'undefined') {
                $('.CommunityStringTextBox').result(CommunityStringTextBoxChangedEvent);
            }
        });
    //]]>
    </script>
");
		}
		else
		{
		    PollingMethodSelector.SetEngineId(NodePolling.EngineID);
			NodePropertyPluginManager.InitPlugins(null);

            PollingMethodSelector.AgentInfo.SnmpInfo.LoadSNMPPropertiesFromSession();
		}

		if (NodeWorkflowHelper.Node != null)
			PollingMethodSelector.Nodes = new List<Node>() { NodeWorkflowHelper.Node };
	}
	
	protected override bool OnBubbleEvent(object source, EventArgs args)
    {
        CommandEventArgs command = args as CommandEventArgs;
		
		if (command != null)
        {
            switch (command.CommandName)
            {
                case SNMPDetailsChangeEventArgs.CommandName :
                    
                    if (NodeWorkflowHelper.Node != null)
                    {
                        ((SNMPDetailsChangeEventArgs)command).SetupNodeSNMP(NodeWorkflowHelper.Node); //Apply snmp settings changes
  
                        NodePropertyPluginManager.InitPlugins(null); //Re-initialize Property plugins with new info
                    }
					
                    return true; // Stop bubbling the event
            }
        }

        return base.OnBubbleEvent(source, args);
	}

	protected override void OnLoadComplete(EventArgs e)
	{
		base.OnLoadComplete(e);

        LoadPluginProperties();

		// adjust visibility of plugin controls
		NodePropertyPluginManager.AdjustPluginVisibility();
	}

	protected void imgbBack_Click(object sender, EventArgs e)
	{
		Response.Redirect(NodeWorkflowHelper.GetPreviousStepUrl());
	}

	protected void imgbCancel_Click(object sender, EventArgs e)
	{
		NodeWorkflowHelper.Reset();
		Response.Redirect("../Default.aspx", true);
	}

	protected void DoRedirectLbt(object sender, EventArgs e)
	{
		string groupBy = WebUserSettingsDAL.Get("Orion_Nodes_Controls_NodeGrouping_GroupBy");

		string redirectUrl = string.Format("~/Orion/Nodes/Default.aspx{0}", string.IsNullOrEmpty(groupBy) || groupBy.Equals("none", StringComparison.InvariantCultureIgnoreCase) ? string.Empty : "?groupBy=" + groupBy);

		SolarWinds.Orion.Web.Helpers.AddNodeReturnPageHelper.Return(null, true);
		SolarWinds.Orion.Web.Helpers.DiscoveryCentralHelper.Return(redirectUrl, true);
		//Response.Redirect(redirectUrl, true);
	}
	
	private bool IsAssignedToParentFreePollingEngine(SolarWinds.Orion.Core.Common.Models.Engine engine)
    {
        if (engine.MasterEngineID == null)
            return false;

        var parentEngine = new EngineDAL().GetEngine(engine.MasterEngineID.Value);
        return parentEngine.IsFree;
    }

    protected void imgbOk_Click(object sender, EventArgs e)
	{
	    Page.Validate("EditCpValue");

        if (!Page.IsValid) return;

        Dictionary<string, int> licenseExceedement = new Dictionary<string, int>();
		string errorMessage = string.Empty;
		bool success = false;

		errorMessage = NodePropertyPluginManager.Validate();
		if (errorMessage != null)
		{
			if (errorMessage != string.Empty) ScriptManager.RegisterStartupScript(this.Page, GetType(), "PluginValidateFailed", ControlHelper.JsFormat("alert('{0}');", errorMessage), true);
			return;
		}

        // perform plugin validation if selected
        if (PollingMethodSelector.SelectedPluginMethod != null)
        {
            var result = PollingMethodSelector.SelectedPluginMethod.Validate(NodeWorkflowHelper.Node, PollingMethodSelector.SelectedPluginSettingsControl);

            if (!result.IsValid)
            {
                string validationMessages = (result.ErrorMessages == null || !result.ErrorMessages.Any() || result.ErrorMessages.All(message => string.IsNullOrWhiteSpace(message))) ? null : string.Join("\\n", result.ErrorMessages);

                if (validationMessages != null)
                {
                    ScriptManager.RegisterStartupScript(this.Page, GetType(), "PluginValidation", ControlHelper.JsFormat("alert('{0}');", validationMessages), true);
                }
                return;
            }
        }

        Node node = NodeWorkflowHelper.Node;
        node.Caption = txtCaption.Text;
		node.UnPluggable = PollingMethodSelector.UnPluggable;

        ICoreBusinessLayer coreProxy = null;
		node.EngineID = NodePolling.EngineID;

		try
		{
			// get resources for actual node
			ResourceTreeInfo info = ResourceTreeHelper.GetResourceTreeInfo(NodeWorkflowHelper.ResourceTreeInfoId);

			DiscoveredNode discoveredNode = null;

			if (info != null && info.resultTree != null)
			{
				discoveredNode = info.resultTree.GetAllTreeObjectsOfType<DiscoveredNode>().FirstOrDefault();
			}

			try
			{
			    coreProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, node.EngineID, false);
            }
			catch (Exception ex)
			{
				ScriptManager.RegisterStartupScript(this, this.GetType(), "BLContactFail", ControlHelper.JsFormat("alert('{0}');", string.Format(Resources.CoreWebContent.WEBCODE_YK0_13, ex.Message)), true);
				return;
			}

			if (node.SNMPVersion == SNMPVersion.None)
			{
				node.ReadOnlyCredentials.CommunityString = string.Empty;
			}

			if (!ValidateNode(coreProxy, node))
				return;

            if (node.NodeSubType != NodeSubType.Agent)
            {
                PollingMethodSelector.SnmpInfo.SaveSNMPv3CredentialsToSession();
            }
            else
            {
                PollingMethodSelector.AgentInfo.SnmpInfo.SaveSNMPv3CredentialsToSession();
            }

			Intervals intervals = coreProxy.GetSettingsPollingIntervals();

			node.PollInterval = TimeSpan.FromSeconds((double)NodePolling.NodeStatusPolling);
			node.StatCollection = TimeSpan.FromMinutes((double)NodePolling.CollectStatisticsEvery);

			node.RediscoveryInterval = intervals.RediscoveryInterval;

			if (node.UnPluggable)
			{
				node.Status = "11"; // external
				node.StatusDescription = Resources.CoreWebContent.WEBCODE_YK0_23;
				node.GroupStatus = "External.gif";
				node.StatusLED = "External.gif";
                using (LocaleThreadState.EnsurePrimaryLocale())
                {
                    node.MachineType = Resources.CoreWebContent.WEBCODE_UnknownMachineType;
                }
			}
			else
			{
				node.Status = "1"; // up
			}

            if (node.SNMPVersion != SNMPVersion.None)
            {
                if (node.NodeSubType != NodeSubType.Agent)
                {
                    PollingMethodSelector.SnmpInfo.FillNodeWithValues(node);
                }
                else
                {
                    if (PollingMethodSelector.AgentInfo.UseSnmp)
                    {
                        PollingMethodSelector.AgentInfo.SnmpInfo.FillNodeWithValues(node);
                    }
                    else
                    {
                        node.SNMPVersion = SNMPVersion.None;
                    }
                }
            }

			if (node.NodeSubType == NodeSubType.WMI)
			{
				PollingMethodSelector.WmiInfo.Update();
			}

			DiscoveredObjectTree tree = ResolveResourceTreeInfo(node);

            try
            {
                // fill knowledge about node from discovery result into node model
                if (discoveredNode != null && node.NodeSubType != NodeSubType.None && node.NodeSubType != NodeSubType.ICMP)
                {
                    node.MachineType = discoveredNode.MachineType;
                    node.VendorIcon = discoveredNode.VendorIcon;
                    node.Vendor = discoveredNode.Vendor;
                    node.Location = discoveredNode.Location;
                    node.Description = discoveredNode.SysDescription;
                    node.Contact = discoveredNode.Contact;

                    // set node category to value detected from OID if available                
                    node.Category = String.IsNullOrEmpty(discoveredNode.SysObjectId) ? null : (NodeCategory?)NodeDetailHelper.GetNodeCategory(discoveredNode.SysObjectId, discoveredNode.MachineType, discoveredNode.SysDescription);

                    if (node.SNMPVersion != SNMPVersion.None)
                    {
                        node.SysObjectID = discoveredNode.SysObjectId;
                    }
                }                
                
                node.CustomCategory = NodeCategory.ResultCategory;
                bool newNode = node.ID <= 0;

                if (newNode)
                {
                    node.CPULoad = -2;
                    node.MemoryUsed = -2;

                    if (node.UnPluggable)
                    {
                        node.AvgResponseTime = -2;
                        node.MinResponseTime = -2;
                        node.MaxResponseTime = -2;
                        node.ResponseTime = -2;
                        node.PercentLoss = -2;
                    }

                    if(PollingMethodSelector.SelectedPluginMethod != null)
                    {
                        PollingMethodSelector.SelectedPluginMethod.Commit(node, PollingMethodSelector.SelectedPluginSettingsControl, Session[addNodePluginStateName]);
                    }

                    // insert the node
                    var pollingPlugin = PollingMethodSelector.SelectedPluginMethod;
                    bool allowNodeDuplication = (pollingPlugin?.AllowNodeDuplication == true) || node.NodeSubType == NodeSubType.Agent;
                    node = coreProxy.InsertNodeWithFaultContract(node, allowNodeDuplication, false);

                    if (node.SNMPVersion == SNMPVersion.SNMP3)
                    {
                        PollingMethodSelector.UpdateSnmpV3Credentials(node);

                        //Sync with Discovery process where RO creds included into indication
                        var roSetting = NodeSettingsDAL.GetLastNodeSettings(node.ID, NodeSettingsNames.RoSnmpCredentialSettingName);
                        node.NodeSettings[NodeSettingsNames.RoSnmpCredentialSettingName] = roSetting;
                    }

                    foreach (var plugin in addNodePluginProvider.Plugins)
                    {
                        plugin.OnCreate(node);
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(node.Contact))
                        node.Contact = string.Empty;
                    if (string.IsNullOrEmpty(node.VendorIcon))
                        node.VendorIcon = string.Empty;
                    if (string.IsNullOrEmpty(node.StatusDescription))
                        node.StatusDescription = string.Empty;

                    coreProxy.UpdateNode(node);

                    foreach(var plugin in updateNodePluginProvider.Plugins)
                    {
                        plugin.OnUpdate(node);
                    }
					
                    if (node.SNMPVersion == SNMPVersion.SNMP3)
                    {
                        PollingMethodSelector.UpdateSnmpV3Credentials(node);
                    }

                    node = coreProxy.GetNodeWithOptions(node.ID, true, true);
                }

                if (node.NodeSubType == NodeSubType.Agent && NodeWorkflowHelper.AgentCredential != null && NodeWorkflowHelper.AgentCredential.AgentId > 0)
                {
                    var agentManager = new AgentManager();
                    agentManager.UpdateAgentNodeId(NodeWorkflowHelper.AgentCredential.AgentId, node.Id);
                    agentManager.SyncPollingEngine(node.Id, node.EngineID);
                }

                if (tree != null && node.NodeSubType != NodeSubType.None && node.NodeSubType != NodeSubType.ICMP)
                {
                    // import node resources
                    foreach (var kvp in coreProxy.ImportOneTimeJobResult(new DiscoveredObjectTreeWcfWrapper(tree), node.Id))
                    {
                        licenseExceedement.Add(kvp.Key, kvp.Value);
                    }
                }
                else if (node.NodeSubType == NodeSubType.ICMP && !node.UnPluggable)
                {
                    coreProxy.AddBasicPollersForNode(node.ID, NodeSubType.ICMP);
                }

                if (newNode) //need to move it here to handle Cortex nodes
                    IndicationPublisher.CreateV3().ReportIndication(new NodeIndication(IndicationType.System_InstanceCreated, node));

                if (PollingMethodSelector.SelectedPluginMethod != null)
                {
                    bool leaveDefaultPollers = true;
                    string[] newPollers;

                    PollingMethodSelector.SelectedPluginMethod.UpdatePollers(out leaveDefaultPollers, out newPollers);

                    if (!leaveDefaultPollers)
                        coreProxy.RemoveBasicPollersForNode(node.ID, NodeSubType.ICMP);

                    if ((newPollers != null) && (newPollers.Length > 0))
                        coreProxy.AddPollersForNode(node.ID, newPollers);
                }
            }
            catch (FaultException<CoreFaultContract> faultException)
            {
                CoreFaultContract coreFaultContract = faultException.Detail;

                if (coreFaultContract != null)
                {
                    errorMessage = coreFaultContract.LocalizedMessage ?? coreFaultContract.Message;
                }
                else
                {
                    errorMessage = faultException.Message;
                }

                log.Error(string.Format("Error adding or updating node. Message: {0}", errorMessage), faultException);
              
                errorMessage = errorMessage.Replace("\r", "\\r").Replace("\n", "\\n");
            }
            catch (Exception ex)
            {
                if ((ex is SWException) && !string.IsNullOrEmpty((ex as SWException).LocalizedMessage))
                {
                    errorMessage = (ex as SWException).LocalizedMessage; // we prefer display to user localized shape of message
                }
                else
                {
                    errorMessage = ex.Message.Replace("\r", "\\r").Replace("\n", "\\n");
                }
                
                log.Error(string.Format("Error adding or updating node. Message: {0}", errorMessage), ex);
            }

			success = true;
		}
		finally
		{
            if (coreProxy != null)
				coreProxy.Dispose();
		}

        // compose license exceedment message if needed
        if (licenseExceedement.Count > 0)
        {
            errorMessage = LicensingHelper.ComposeExceedementMessage(licenseExceedement,
                 Resources.CoreWebContent.LIBCODE_TP0_1); // Node was imported successfully. Additional objects may not be managed due to license limit: {0}
        }

		foreach (IAddNodePluginWizard plugin in NodeWorkflowHelper.PluginStepControls)
		{
			try
			{
				if (plugin is IAddCoreNodePlugin)
				{
					(plugin as IAddCoreNodePlugin).Save(node);
				}
				else
				{
					CallSaveForObsoleteInterface(plugin, node, "SolarWinds.Orion.Web.IAddNodePlugin");
				}
			}
			catch (Exception ex)
			{
				log.Warn("Unable to save plugin step", ex);
				success = false;
				break;
			}
		}

        // Call property plugins Update(), but first update helper class with reference on the node. (cached version plugin holds may be not up to date and plugin may take latest from NodeWorkflowHelper.Node)
        NodeWorkflowHelper.Node = node;
		if (!NodePropertyPluginManager.Update())
			errorMessage = Resources.CoreWebContent.WEBCODE_YK0_7;

		SolarWinds.Orion.Web.Helpers.AddNodeReturnPageHelper.NetObjectId = null;
        if (success)
		{
			NodeWorkflowHelper.Reset();
			if (string.IsNullOrEmpty(errorMessage))
			{
				SolarWinds.Orion.Web.Helpers.AddNodeReturnPageHelper.NetObjectId = node.ID.ToString();
				ScriptManager.RegisterStartupScript(this, this.GetType(), "NodeAdded", string.Format("{0} {1};",
					ControlHelper.JsFormat("alert('{0}');", Resources.CoreWebContent.WEBCODE_YK0_12),
					this.Page.ClientScript.GetPostBackEventReference(RedirectLbt, new Guid().ToString())),
					true);
			}
			else
			{
				ScriptManager.RegisterStartupScript(this, this.GetType(), "", string.Format("{0} {1};",
					ControlHelper.JsFormat("alert('{0}');", errorMessage),
					this.Page.ClientScript.GetPostBackEventReference(RedirectLbt, new Guid().ToString())),
					true);
			}
		}
		else
		{
			if (!string.IsNullOrEmpty(errorMessage))
			{
				NodeWorkflowHelper.Reset();
				ScriptManager.RegisterStartupScript(this, this.GetType(), "", string.Format("{0} {1};",
					ControlHelper.JsFormat("alert('{0}');", errorMessage),
					this.Page.ClientScript.GetPostBackEventReference(RedirectLbt, new Guid().ToString())),
					true);
			}
		}
	}

    /// <summary>
    /// Tries to resolve <see cref="DiscoveredObjectTree"/> for given <see cref="Node"/> if applicable.
    /// </summary>
    /// <param name="node"><see cref="Node"/> to be <see cref="DiscoveredObjectTree"/> resolved for.</param>
    /// <returns><see cref="DiscoveredObjectTree"/> if found and applicable, otherwise null.</returns>
    private static DiscoveredObjectTree ResolveResourceTreeInfo(Node node)
    {
        DiscoveredObjectTree tree = null;

        if ((node.NodeSubType == NodeSubType.None) || (node.NodeSubType == NodeSubType.ICMP))
        {
            //For external and ICMP node type is no discovery scheduled.
            return null;
        }

        if (NodeWorkflowHelper.ResourceTreeInfoId == Guid.Empty)
        {
            log.Error("Unable to find resource tree info ID");
        }
        else
        {
            var treeInfo = ResourceTreeHelper.GetResourceTreeInfo(NodeWorkflowHelper.ResourceTreeInfoId);

            if (treeInfo == null)
            {
                log.Error("Unable to find resource tree info");
            }
            else
            {
                if (treeInfo.resultTree == null)
                {
                    log.Error("Unable to find resource tree");
                }
                else
                {
                    tree = treeInfo.resultTree;
                }
            }
        }

        return tree;
    }

    protected void HostNameIP_SNMPModeChanged(object sender, EventArgs e)
	{

	}

	private static void CallSaveForObsoleteInterface(IAddNodePluginWizard plugin, Node node, string interfaceName)
	{
		Type interfaceType = plugin.GetType().GetInterface(interfaceName);
		if (interfaceType == null)
			return;

		MethodInfo info = interfaceType.GetMethod("Save");
		if (info == null)
			return;

		ParameterInfo firstParamInfo = info.GetParameters()[0];
		object firstParam = Activator.CreateInstance(firstParamInfo.ParameterType, node);

		info.Invoke(plugin, new[] { firstParam });
	}

	protected string GetReadOnlyCommunityStrings()
	{
		var sb = new StringBuilder();
		var table = SqlDAL.GetReadOnlyCommunityStrings();
		foreach (DataRow row in table.Rows)
		{
            sb.AppendFormat("'{0}',", CommonHelper.FixReadOnlyCommunityString(row[0].ToString()));
		}
		if (sb.Length > 0)
		{
			sb.Length--;
		}
		return sb.ToString();
	}

	#region private methods
	private bool ValidateNode(ICoreBusinessLayer proxy, Node node)
	{
		if (node.NodeSubType != NodeSubType.Agent && node.SNMPVersion != SNMPVersion.None)
		{
			var result = NodeWorkflowHelper.ValidateSNMPNode(proxy,
				node.SNMPVersion,
				node.IpAddress,
				node.SNMPPort,
				node.ReadOnlyCredentials.CommunityString,
				node.ReadWriteCredentials.CommunityString,
				node.ReadOnlyCredentials.SNMPv3AuthPassword,
				node.ReadWriteCredentials.SNMPv3AuthPassword,
				node.ReadOnlyCredentials.SNMPv3AuthType,
				node.ReadWriteCredentials.SNMPv3AuthType,
				node.ReadOnlyCredentials.SNMPv3PrivacyType,
				node.ReadWriteCredentials.SNMPv3PrivacyType,
				node.ReadOnlyCredentials.SNMPv3PrivacyPassword,
				node.ReadWriteCredentials.SNMPv3PrivacyPassword,
				node.ReadOnlyCredentials.SnmpV3Context,
				node.ReadWriteCredentials.SnmpV3Context,
				node.ReadOnlyCredentials.SNMPv3UserName,
				node.ReadWriteCredentials.SNMPv3UserName,
				node.ReadOnlyCredentials.SNMPV3AuthKeyIsPwd,
				node.ReadWriteCredentials.SNMPV3AuthKeyIsPwd,
				node.ReadOnlyCredentials.SNMPV3PrivKeyIsPwd,
				node.ReadWriteCredentials.SNMPV3PrivKeyIsPwd);

			if (!result.ValidationSucceeded)
			{
				string msg;
				if ((node.SNMPVersion == SNMPVersion.SNMP1) || (node.SNMPVersion == SNMPVersion.SNMP2c))
				{
					if (result.ValidationWriteFailed)
						msg = ControlHelper.JsFormat("alert('{0}');", string.Format(Resources.CoreWebContent.WEBCODE_YK0_11, node.Name));
					else
						msg = ControlHelper.JsFormat("alert('{0}');", string.Format(Resources.CoreWebContent.WEBCODE_YK0_10, node.Name, node.ReadOnlyCredentials.CommunityString));
				}
				else
				{
					if (result.ValidationWriteFailed)
						msg = ControlHelper.JsFormat("alert('{0}');", string.Format(Resources.CoreWebContent.WEBCODE_YK0_9, node.Name));
					else
						msg = ControlHelper.JsFormat("alert('{0}');", string.Format(Resources.CoreWebContent.WEBCODE_YK0_8, node.Name));
				}

				System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "NodeNotResponding", msg, true);
				return false;
			}
		}
		else if (node.NodeSubType == NodeSubType.WMI)
		{
			var result = PollingMethodSelector.WmiInfo.Validate(false);
			return result;
		}

		return true;
	}

	private void ValidatePage()
	{
		if (NodeWorkflowHelper.Node == null)
			Response.Redirect("Default.aspx");
	}

	private void LoadProperties()
	{
		Node node = NodeWorkflowHelper.Node;

        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, node.EngineID))
		{
			Intervals intervals = proxy.GetSettingsPollingIntervals();
			NodePolling.NodeStatusPolling = intervals.NodePollInterval;
			NodePolling.CollectStatisticsEvery = intervals.NodeStatPollInterval;
		}
		NodePolling.EngineID = NodeWorkflowHelper.Node.EngineID;
	    NodePolling.ObjectEngineIDs = new[] {NodeWorkflowHelper.Node.EngineID};

		if (node.SNMPVersion == SNMPVersion.None)
		{
			PollingMethodSelector.SnmpInfo.Visible = false;
		}

		txtCaption.Text = node.Caption;

        PollingMethodSelector.AgentInfo.HideCredentialsTable = true;
        PollingMethodSelector.SnmpInfo.LoadSNMPPropertiesFromSession();
        PollingMethodSelector.AgentInfo.SnmpInfo.LoadSNMPPropertiesFromSession();
        PollingMethodSelector.AgentInfo.UseSnmp = (node.SNMPVersion != SNMPVersion.None);

        pollingIP.Text = node.IpAddress;

        // get resources for actual node
        ResourceTreeInfo info = ResourceTreeHelper.GetResourceTreeInfo(NodeWorkflowHelper.ResourceTreeInfoId);
        DiscoveredNode discoveredNode = null;

        if (info != null && info.resultTree != null)
        {
            discoveredNode = info.resultTree.GetAllTreeObjectsOfType<DiscoveredNode>().FirstOrDefault();

            if (!String.IsNullOrEmpty(discoveredNode.SysObjectId)) // if there is something to detect from lets propose it.
                NodeCategory.Category = (NodeCategory?)NodeDetailHelper.GetNodeCategory(discoveredNode.SysObjectId, discoveredNode.MachineType, discoveredNode.SysDescription);
        }

    }

    private void LoadPluginProperties()
    {
        if ((!IsPostBack) && (PollingMethodSelector.SelectedPluginMethod != null))
            PollingMethodSelector.SelectedPluginMethod.LoadState(NodeWorkflowHelper.Node, PollingMethodSelector.SelectedPluginSettingsControl, Session[addNodePluginStateName]);
    }

    #endregion

    static string ToJSON(object value, params Type[] types)
	{
		DataContractJsonSerializer serializer = new DataContractJsonSerializer(value.GetType(), types);
		using (MemoryStream ms = new MemoryStream())
		{
			serializer.WriteObject(ms, value);
			return Encoding.UTF8.GetString(ms.ToArray());
		}
	}

	#region IStep Members

	public string Step
	{
		get { return "Properties"; }
	}

	#endregion
}
