using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.UI;
using System.Data;
using System.Linq;
using System.Net.Sockets;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Events;
using SolarWinds.Orion.Web.Helpers;
using Node = SolarWinds.Orion.Core.Common.Models.Node;
using NodeSubType = SolarWinds.Orion.Core.Common.Models.NodeSubType;
using SNMPVersion = SolarWinds.Orion.Core.Common.Models.SNMPVersion;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Web.Agent;
using SolarWinds.Orion.Core.Common.Agent;
using System.Globalization;
using SolarWinds.AgentManagement.Contract;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;


public partial class Orion_Nodes_Add_Default : System.Web.UI.Page, IStep
{
    private static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
    bool listResourceSuceeded = false;
    bool nodeAlreadyExists = false;
    string currentIp;

    private const string addNodePluginStateName = "AddNodeState";

    /// <summary>
    /// Indicates whether duplicate node is allowed.
    /// for non-agent nodes it allows only duplicate node on different polling engine,  Agent node with agent already detected can be duplicated on the same engine
    /// </summary>
    private bool AllowDuplicateNode
    {
        get { return ViewState["AllowDuplicateNode"] as bool? ?? false; }
        set { ViewState["AllowDuplicateNode"] = value; }
    }

    /// <summary>
    /// Indicates that Node can be upgraded. (User clicked 'UPGRADE NODE' in duplicate node dialog)
    /// </summary>
    private bool AllowNodeUpgrade
    {
        get { return ViewState["AllowNodeUpgrade"] as bool? ?? false; }
        set { ViewState["AllowNodeUpgrade"] = value; }
    }

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        // Reset active page key to avoid collision with edit node tabs
        NodePropertyPluginManager.ResetActivePage();
        ResetWizardOnRestart();

        if (!IsPostBack)
            NodePropertyPluginManager.Init(NodePropertyPluginExecutionMode.WizDefineNode);
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);

        if (!IsPostBack)
        {
            NodeWorkflowHelper.LoadWizardSteps();
        }

        if (IsPostBack)
            ProgressIndicator1.Reload();

        // Plugins handling (FB 22290)   !!! Must be inside Init() event to wire up plugin control events
        pluginDiv.Controls.Clear();
        // loads all plugin controls visble or not
        var controls = NodePropertyPluginManager.LoadControls(this.LoadControl);
        foreach (var control in controls)
        {
            pluginDiv.Controls.Add(control);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Session["HostName"] != null)
        {
            HostNameIP1.HostNameIP = (string)this.Session["HostName"];
            this.Session["HostName"] = null;
        }

        PageHelper.SetTestCredentialTimeoutForUpdatePanel(this.Page);

        this.Page.Form.DefaultButton = imgbNext.UniqueID;

        if (!IsPostBack)
        {
            bool targetDisabled = "true".Equals(Request.QueryString["targetDisabled"] ?? string.Empty, StringComparison.OrdinalIgnoreCase);
            HostNameIP1.Enabled = !targetDisabled;

            //TODO: The text of message I assume can be retrieved from some resource file. Replace the message when needed.
            imgbCancel.Attributes["OnClick"] = ControlHelper.JsFormat("return confirm('{0}');", Resources.CoreWebContent.WEBCODE_YK0_14);
            LoadProperties();
            NodePropertyPluginManager.InitPlugins(new List<Node>() { NodeWorkflowHelper.Node });

            this.ClientScript.RegisterStartupScript(typeof(Orion_Nodes_Add_Default), "cs_autocomplete",
@"  <script type=""text/javascript"">
    //<![CDATA[
		Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function() {
			$('.CommunityStringTextBox').unautocomplete();
            $('.CommunityStringTextBox').autocomplete([" + this.GetReadOnlyCommunityStrings() + @"], {max : 1000, minChars : 0});
            if(typeof(CommunityStringTextBoxChangedEvent) != 'undefined') {
                $('.CommunityStringTextBox').result(CommunityStringTextBoxChangedEvent);
            }
        });
    //]]>
    </script>
");
        }
        else
        {
            //OA-1298: Add Node Page doesn't reset when clearing hostname.
            if (string.IsNullOrEmpty(HostNameIP1.HostNameIP))
            {
                //The hostname has been cleared in the textbox, need to make sure that dependent node type controls are cleared too.
                NodeWorkflowHelper.Node = NodeWorkflowHelper.CreateNode();
            }


            #region == Check Ip/Host name validity, if required, and pass Node to plugins ==
            var el = ScriptManager.GetCurrent(this).AsyncPostBackSourceElementID;
            // do check only if plugin checkbox has been changed or Next hit
            //    !!! it is OK now, when default state of plugins is invisible, but may become a problem if plugin is visible.
            //       in that case NodePropertyPluginManager.InitPlugins() is called only once on first page load
            bool skip = false;
            if (CheckIpOrHostName(NodeWorkflowHelper.Node))
            {
                if (el.Contains(pluginSelector.UniqueID) ||
                    el == imgbNext.UniqueID)
                {
                    if (currentIp != NodeWorkflowHelper.Node.IpAddress)
                    {
                    	currentIp = NodeWorkflowHelper.Node.IpAddress;
                        NodePropertyPluginManager.InitPlugins(new List<Node>() { NodeWorkflowHelper.Node });
                        skip = true;
                    }
                }
            }
            if (!skip)
                NodePropertyPluginManager.InitPlugins(null);
            #endregion
        }

        PollingMethodSelector.Nodes = new List<Node>() { NodeWorkflowHelper.Node };
        PollingMethodSelector.SnmpInfo.Ip = HostNameIP1.HostNameIP;
        PollingMethodSelector.AgentInfo.SnmpInfo.Ip = HostNameIP1.HostNameIP;
        PollingEngine_EngineChanged(this, EventArgs.Empty);
    }

    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);

        LoadPluginProperties();

        // adjust visibility of plugin controls
        NodePropertyPluginManager.AdjustPluginVisibility();

        if (PollingMethodSelector.Agent && PollingMethodSelector.AgentInfo.DeploymentInfo != null && PollingMethodSelector.AgentInfo.DeploymentInfo.StatusInfo.Status == AgentDeploymentStatus.PendingReboot)
        {
            TopInfoBox.Visible = true;
            TopInfoBoxText.InnerText = string.Format(CultureInfo.CurrentUICulture, Resources.CoreWebContent.WEBDATA_PollingMethod_Agent_RebootRequired, NodeWorkflowHelper.Node.Caption);
        }
        else
        {
            TopInfoBox.Visible = false;
        }

        PollingEngine.Enabled = !(PollingMethodSelector.Agent && PollingMethodSelector.AgentInfo.DeploymentInfo != null);
    }

    protected override bool OnBubbleEvent(object source, EventArgs args)
    {
        CommandEventArgs command = args as CommandEventArgs;

        if (command != null)
        {
            switch (command.CommandName)
            {
                case SNMPDetailsChangeEventArgs.CommandName:

                    if (NodeWorkflowHelper.Node != null) //FB171319 - There can be orphaned SNMP details in session which broke up the functionality
                    {
                        ((SNMPDetailsChangeEventArgs)command).SetupNodeSNMP(NodeWorkflowHelper.Node); //Apply snmp settings changes

                        NodePropertyPluginManager.InitPlugins(null); //Re-initialize Property plugins with new info
                    }
                    return true; // Stop bubbling the event
            }
        }

        return base.OnBubbleEvent(source, args);
    }

    private SNMPVersion GetSnmpVersion(SNMPModeEventArgs e)
    {
        if (e.ICMP || e.Windows)
            return SNMPVersion.None;

        if (e.Agent)
        {
            return (PollingMethodSelector.AgentInfo.UseSnmp ? PollingMethodSelector.AgentInfo.SnmpInfo.SNMPVersion : SNMPVersion.None);
        }

        return PollingMethodSelector.SnmpInfo.SNMPVersion;
    }

    protected void PollingMethodSelector_OnPollingMethodChanged(object sender, SNMPModeEventArgs e)
    {
        var plugin = PollingMethodSelector.SelectedPluginMethod;
        if (plugin != null)
        {
            var layout = plugin.GetPageLayout();

            if (layout != null)
            {
                string hostname = string.IsNullOrWhiteSpace(layout.Hostname) ? null : layout.Hostname;
                HostNameIP1.SetupControls(layout.HostnameEnabled, hostname);
            }
        }
        else
        {
            // by default is hostname enabled and editable
            HostNameIP1.SetupControls(true);
        }


        ProgressIndicator1.Mode = (e.ICMP ? ProgressIndicator1.Mode = AddNodeMode.ICMPOnly : AddNodeMode.Standard);
        NodeWorkflowHelper.Node.SNMPVersion = GetSnmpVersion(e);

        if (e.ICMP)
        {
            NodeWorkflowHelper.LoadWizardSteps("ICMP");
        }
        else
        {
            if (e.Windows)
            {
                NodeWorkflowHelper.LoadWizardSteps("WMI");
            }
            else if (e.Agent)
            {
                NodeWorkflowHelper.LoadWizardSteps("WMI");//TODO RH do we need special steps for agent

                if (PollingMethodSelector.AgentInfo.DeploymentInfo != null)//agent already exists, sync polling engine with the agent
                {
                    var engineId = PollingMethodSelector.AgentInfo.DeploymentInfo.Agent.PollingEngineId;
                    NodeWorkflowHelper.Node.EngineID = engineId;
                    PollingEngine.EngineID = engineId;
                }
            }
            else
                NodeWorkflowHelper.LoadWizardSteps();
        }

        ProgressIndicator1.Mode = (e.ICMP ? ProgressIndicator1.Mode = AddNodeMode.ICMPOnly : AddNodeMode.Standard);
        NodeWorkflowHelper.Node.SNMPVersion = GetSnmpVersion(e);

        if (e.ICMP)
            NodeWorkflowHelper.Node.ObjectSubType = "ICMP";
        else
            if (e.Windows)
            NodeWorkflowHelper.Node.ObjectSubType = "WMI";
        else if (e.Agent)
            NodeWorkflowHelper.Node.ObjectSubType = "Agent";
        else
            NodeWorkflowHelper.Node.ObjectSubType = "SNMP";


        NodeWorkflowHelper.Node.NodeSubType = (NodeSubType)Enum.Parse(typeof(NodeSubType), NodeWorkflowHelper.Node.ObjectSubType);

        ProgressIndicator1.Reload();
    }

    protected void PollingMethodSelector_OnSelectedPollingEnginesChanged(object sender, SelectedPollingEnginesEventArgs e)
    {
        this.pluginSelector.SelectedEngineTypes = e.SelectedServerTypes;
        this.pluginSelector.SolveVisibilityOfPluginsAccordingSelectedEngineTypes();
        pluginDiv.Visible = pluginSelector.ShouldShowPluginDiv();
    }

    protected void imgbNext_Click(object sender, EventArgs e)
    {
        if ((NodeWorkflowHelper.Node.NodeSubType == NodeSubType.Agent) && Request["__EVENTARGUMENT"].StartsWith("AGENT_DEPLOYED", StringComparison.OrdinalIgnoreCase))
        {
            var eventArgument = Request["__EVENTARGUMENT"];
            var parts = eventArgument.Split(':');
            int agentId = 0;
            if (parts.Length > 1 && int.TryParse(parts[1], out agentId))
            {
                PollingMethodSelector.AgentInfo.DetectAgent(NodeWorkflowHelper.Node, agentId);
                var plugins = (from p in PollingMethodSelector.AgentInfo.DeploymentInfo.Agent.Plugins select p.PluginId).ToArray();
                NodeWorkflowHelper.AgentCredential = new SolarWinds.Orion.Discovery.Contract.Models.AgentManagementCredential()
                {
                    AgentId = PollingMethodSelector.AgentInfo.DeploymentInfo.Agent.AgentId,
                    AgentGuid = PollingMethodSelector.AgentInfo.DeploymentInfo.Agent.AgentGuid,
                    Plugins = plugins
                };
            }
            else
            {
                log.ErrorFormat("Bad value in __EVENTARGUMENT:{0}", eventArgument);
                return;
            }

            //repeat validation of plugins so plugins (e.g. VIM) have chance to validate credentials via deployed agent
            var msg = NodePropertyPluginManager.Validate();
            if (msg != null)
            {
                if (msg != string.Empty) ScriptManager.RegisterStartupScript(this.Page, GetType(), "PluginValidateFailed", ControlHelper.JsFormat("alert('{0}');", msg), true);
                return;
            }

            var node = NodeWorkflowHelper.Node;
            if (string.IsNullOrEmpty(node.Caption))
            {
                node.Caption = !string.IsNullOrEmpty(node.DNS) ? node.DNS : node.IpAddress;
            }

            NodePropertyPluginManager.Update();
            Response.Redirect(NodeWorkflowHelper.GetNextStepUrl(), true);
        }
        else
        {
            if (sender == btnContinue)
            {
                AllowDuplicateNode = true;
            }
            else if (sender == btnReAdd)
            {
                AllowNodeUpgrade = true;
            }

            Next();

            if (!listResourceSuceeded)
                return;
            if (nodeAlreadyExists)
                return;

            NodePropertyPluginManager.Update();

            Response.Redirect(NodeWorkflowHelper.GetNextStepUrl(), true);
        }
    }

    protected void imgbCancel_Click(object sender, EventArgs e)
    {
        NodeWorkflowHelper.Reset();
        Response.Redirect("../Default.aspx");
    }

    private void ResetWizardOnRestart()
    {
        bool Restart = "true".Equals(Request.QueryString["restart"] ?? "true", StringComparison.OrdinalIgnoreCase);

        if (!this.IsAsync && !this.IsPostBack && Restart)
        {
            var node = NodeWorkflowHelper.Node;
            NodeWorkflowHelper.Reset();
            // FB30317
            // If we are getting data from a module to make a new node, then set up the node again
            // We want to reset everything else so that previous operations like list resources do not
            // affect the add node wizard.
            if (node != null && node.Id == 0)
            {
                NodeWorkflowHelper.Node = node;
            }
            var col = Request.QueryString;
            var qs = string.Join("&", col.AllKeys
                .Where(x => x.Equals("restart", StringComparison.OrdinalIgnoreCase) == false)
                .Select(y => System.Web.HttpUtility.UrlEncode(y) + "=" + System.Web.HttpUtility.UrlEncode(col[y]))
                .ToArray()
                );

            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.Redirect(Request.Path + "?" + qs + "&restart=false", true);
        }
    }
    
    private void LoadProperties()
    {
        if (NodeWorkflowHelper.Node != null)
        {
            HostNameIP1.HostNameIP = !string.IsNullOrEmpty(NodeWorkflowHelper.Node.Name) ? NodeWorkflowHelper.Node.Name : NodeWorkflowHelper.Node.IpAddress;

            PollingEngine.EngineID = NodeWorkflowHelper.Node.EngineID;

            PollingMethodSelector.SnmpInfo.LoadSNMPPropertiesFromSession();
            PollingMethodSelector.AgentInfo.SnmpInfo.LoadSNMPPropertiesFromSession();

            NodeWorkflowHelper.Node.Name = string.Empty;
        }
        else
        {
            NodeWorkflowHelper.Node = NodeWorkflowHelper.CreateNode();
        }
    }

    private void LoadPluginProperties()
    {
        if ((!IsPostBack) && (PollingMethodSelector.SelectedPluginMethod != null))
            PollingMethodSelector.SelectedPluginMethod.LoadState(NodeWorkflowHelper.Node, PollingMethodSelector.SelectedPluginSettingsControl, Session[addNodePluginStateName]);
    }

	private void ClearAllPluginStates(Node node)
	{
		foreach (var addNodePollingMethodPlugin in OrionModuleManager.GetAddNodeMethodPlugins().Values)
		{
			addNodePollingMethodPlugin.Plugin.ClearState(node);
		}

		Session[addNodePluginStateName] = null;
	}

	private void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    private bool ValidateUserInput()
    {
        if (string.IsNullOrEmpty(HostNameIP1.HostNameIP))
        {
            ScriptManager.RegisterStartupScript(this.Page, GetType(), "EmptyIPHostName", ControlHelper.JsFormat("alert('{0}');", Resources.CoreWebContent.WEBCODE_YK0_17), true);
            return false;
        }

        if (PollingMethodSelector.SNMP && string.IsNullOrEmpty(PollingMethodSelector.SnmpInfo.CommunityString) && PollingMethodSelector.SnmpInfo.SNMPVersion != SNMPVersion.SNMP3)
        {
            ScriptManager.RegisterStartupScript(this.Page, GetType(), "EmptySNMPCommunityString", ControlHelper.JsFormat("alert('{0}');", Resources.CoreWebContent.WEBCODE_YK0_16), true);
            return false;
        }

        if (PollingMethodSelector.SNMP && PollingMethodSelector.SnmpInfo.SNMPPort == 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, GetType(), "EmptySNMPPort", ControlHelper.JsFormat("alert('{0}');", Resources.CoreWebContent.WEBCODE_YK0_15), true);
            return false;
        }

        if (PollingMethodSelector.Agent && string.IsNullOrEmpty(PollingMethodSelector.AgentInfo.SnmpInfo.CommunityString) && PollingMethodSelector.AgentInfo.SnmpInfo.SNMPVersion != SNMPVersion.SNMP3)
        {
            ScriptManager.RegisterStartupScript(this.Page, GetType(), "EmptySNMPCommunityString", ControlHelper.JsFormat("alert('{0}');", Resources.CoreWebContent.WEBCODE_YK0_16), true);
            return false;
        }

        if (PollingMethodSelector.Agent && PollingMethodSelector.AgentInfo.SnmpInfo.SNMPPort == 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, GetType(), "EmptySNMPPort", ControlHelper.JsFormat("alert('{0}');", Resources.CoreWebContent.WEBCODE_YK0_15), true);
            return false;
        }

        if (PollingMethodSelector.SelectedPluginMethod != null)
        {
            var result = PollingMethodSelector.SelectedPluginMethod.Validate(NodeWorkflowHelper.Node, PollingMethodSelector.SelectedPluginSettingsControl);

            if (!result.IsValid)
            {
                string validationMessages = (result.ErrorMessages == null || !result.ErrorMessages.Any() || result.ErrorMessages.All(message => string.IsNullOrWhiteSpace(message))) ? null : string.Join("\\n", result.ErrorMessages);

                if (validationMessages != null)
                {
                    ScriptManager.RegisterStartupScript(this.Page, GetType(), "PluginValidation", ControlHelper.JsFormat("alert('{0}');", validationMessages), true);
                }
                return false;
            }
        }

        return true;
    }

    private void CheckListResousces(Node node, string newIP, bool setCaption)
    {
        if (newIP != node.IpAddress)
        {
            node.IpAddress = newIP;
            NodeWorkflowHelper.NewNodeInfo.Remove("ListResources");
            if (setCaption)
                node.Caption = newIP;
        }
    }

    private void Next()
    {
        if (!ValidateUserInput())
            return;

		Node node = NodeWorkflowHelper.Node;

		ClearAllPluginStates(node);

		if (!CheckIpOrHostName(node))
            return;

        if (PollingMethodSelector.ICMP || string.IsNullOrEmpty(node.Name))
            node.Caption = node.IpAddress;

        if (PollingMethodSelector.ICMP && !string.IsNullOrEmpty(node.Name))
            node.Caption = node.Name;


        if (!(PollingMethodSelector.ICMP || PollingMethodSelector.Windows || PollingMethodSelector.Agent || PollingMethodSelector.SelectedPluginMethod != null))
        {
            node = PollingMethodSelector.SnmpInfo.FillNodeWithValues(node);
        }

        if (PollingMethodSelector.Agent)
        {
            node.NodeSubType = NodeSubType.Agent;

            if (PollingMethodSelector.AgentInfo.UseSnmp)
                node = PollingMethodSelector.AgentInfo.SnmpInfo.FillNodeWithValues(node);
            else
                node.SNMPVersion = SNMPVersion.None;
        }

        // call to plugin manager mover here from ValidateUserInput() so that NodeWorkflowHelper.Node has IP set
        string msg = NodePropertyPluginManager.Validate();
        if (msg != null)
        {
            if (msg != string.Empty) ScriptManager.RegisterStartupScript(this.Page, GetType(), "PluginValidateFailed", ControlHelper.JsFormat("alert('{0}');", msg), true);
            return;
        }

        var defaultResolution = SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("SWNetPerfMon-Settings-Default Preferred AddressFamily DHCP");
        if (node.DynamicIP && defaultResolution != null)
        {
            node.IpAddressResolution = (defaultResolution.SettingValue == 4) ? AddressFamily.InterNetwork : AddressFamily.InterNetworkV6;
        }

        ICoreBusinessLayer coreProxy = null;

        try
        {
            try
            {
                coreProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, node.EngineID);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "BLContactFail", ControlHelper.JsFormat("alert('{0}');", string.Format(Resources.CoreWebContent.WEBCODE_YK0_13, ex.Message)), true);
                return;
            }

            // clear node ID session
            NodeWorkflowHelper.MultiSelectedNodeIds.Clear();

            string existingNodePollingEngineName = string.Empty;
            
            bool allowNodeDuplication = PollingMethodSelector.SelectedPluginMethod?.AllowNodeDuplication == true;
            // when plugin specifies that node duplication is allowed, don't search for existing nodes at all
            Node existingNode = allowNodeDuplication ? null : GetExistingNode(node, coreProxy, AllowDuplicateNode);
            nodeAlreadyExists = existingNode != null;
            bool canUpgrade = NodeSubTypeHelper.CanUpgrade(existingNode != null ? existingNode.NodeSubType : NodeSubType.None, node.NodeSubType);
            bool canAddDuplicateNode = allowNodeDuplication || (PollingMethodSelector.Agent && PollingMethodSelector.AgentInfo.DeploymentInfo != null) ||
                                            (existingNode != null && existingNode.NodeSubType == NodeSubType.Agent);

            // existing node found
            if (existingNode != null)
            {
                JobEngineInfo engineInfo = coreProxy.GetEngine(existingNode.EngineID);
                existingNodePollingEngineName = engineInfo != null ? engineInfo.HostName : string.Empty;

                if (AllowNodeUpgrade) // node can be upgraded (updated) and user clicked that option - we keep some existing settings 
                {
                    node.Id = existingNode.ID;

                    if (node.SNMPVersion == SNMPVersion.SNMP3)
                    {
                        node.ReadOnlyCredentials.CommunityString = existingNode.ReadOnlyCredentials.CommunityString;
                        node.ReadWriteCredentials.CommunityString = existingNode.ReadWriteCredentials.CommunityString;
                    }

                    NodeWorkflowHelper.MultiSelectedNodeIds.Add(node.ID);
                }
                else if (AllowDuplicateNode) // user clicked 'Next'
                {
                    node.Id = 0;// just in case when user choose Upgrade first, then went back to this page and selected 'Next'

                    //for agent nodes connected to existing agent do nothing here, let's simply add duplicate node

                    //othherwise...
                    if (!canAddDuplicateNode)
                    {
                        //dialog has been already displayed and user clicked 'Next', but still duplicate node was detected
                        RegisterNodeIsBeingMonitored(canUpgrade && node.EngineID == existingNode.EngineID, node.EngineID != existingNode.EngineID || canAddDuplicateNode, existingNodePollingEngineName, existingNode.ID);
                        return;
                    }
                }    // Node cannot be upgraded -> then we finish
                else //the dialog hasn't been display yet or user clicked 'Cancel'
                {
                    //RegisterNodeIsBeingMonitored(false, false, existingNodePollingEngineName, existingNode.ID);
                    RegisterNodeIsBeingMonitored(canUpgrade && node.EngineID == existingNode.EngineID, node.EngineID != existingNode.EngineID || canAddDuplicateNode, existingNodePollingEngineName, existingNode.ID);
                    return;
                }
            }

            if (!(PollingMethodSelector.ICMP || PollingMethodSelector.SelectedPluginMethod != null))
            {
                if (PollingMethodSelector.Agent) // Agent polling method was selected
                {
                    PollingMethodSelector.AgentInfo.SnmpInfo.SaveSNMPv3CredentialsToSession();

                    var deploymentInfo = PollingMethodSelector.AgentInfo.DeploymentInfo;
                    if (deploymentInfo == null)
                    {
                        //agent not detected, let's deploy it

                        if (!PollingMethodSelector.AgentInfo.Validate())
                            return;

                        PollingMethodSelector.AgentInfo.Update();
                        var settingsId = PollingMethodSelector.AgentInfo.SaveSettings();
                        ScriptManager.RegisterStartupScript(this.Page, GetType(), "DeployAgent", ControlHelper.JsFormat("$(function(){{DeployAgent('{0}', '{1}', '{2}');}});", settingsId.ToString(), node.Caption, ((int)PollingMethodSelector.AgentInfo.DetectionInfo).ToString()), true);
                        return;
                    }

                    var plugins = (from p in deploymentInfo.Agent.Plugins select p.PluginId).ToArray();
                    NodeWorkflowHelper.AgentCredential = new SolarWinds.Orion.Discovery.Contract.Models.AgentManagementCredential()
                    {
                        AgentId = deploymentInfo.Agent.AgentId,
                        AgentGuid = deploymentInfo.Agent.AgentGuid,
                        Plugins = plugins

                    };
                    switch (deploymentInfo.StatusInfo.Status)
                    {
                        case AgentDeploymentStatus.PluginsRequired:
                            //required plugins are not installed. Let's install them
                            ScriptManager.RegisterStartupScript(this.Page, GetType(), "DeployAgentPlugins", ControlHelper.JsFormat("$(function(){{DeployAgentPlugins('{0}', '{1}');}});", deploymentInfo.Agent.AgentId.ToString(), node.Caption), true);
                            return;
                        case AgentDeploymentStatus.InProgress:
                        case AgentDeploymentStatus.PendingPluginInstallation:
                            //deployment still in progress and running, display progress dialog
                            ScriptManager.RegisterStartupScript(this.Page, GetType(), "ShowDeploymentProgress", ControlHelper.JsFormat("$(function(){{ShowDeploymentProgress('{0}', '{1}');}});", deploymentInfo.Agent.AgentId.ToString(), node.Caption), true);
                            return;
                        case AgentDeploymentStatus.Finished:
                            //deployment finished, go to next step
                            NodePropertyPluginManager.Update();
                            Response.Redirect(NodeWorkflowHelper.GetNextStepUrl());
                            break;
                        case AgentDeploymentStatus.Failed:
                            //previous deployment failed. Error message should be already displayed, let's re-deploy agent or plugins
                            if (deploymentInfo.Agent.ConnectionStatus == (int)ConnectionStatus.Ok && deploymentInfo.Agent.AgentStatus == (int)AgentStatus.Ok)
                            {
                                //agent is OK, re-deploy plugins
                                ScriptManager.RegisterStartupScript(this.Page, GetType(), "ReDeployFailedAgentPlugins", ControlHelper.JsFormat("$(function(){{ReDeployFailedAgentPlugins('{0}', '{1}');}});", deploymentInfo.Agent.AgentId.ToString(), node.Caption), true);
                            }
                            else
                            {
                                //re-deploy the whole agent
                                //TODO RH  re-deploying is not ready yet
                                return;
                            }
                            return;
                        case AgentDeploymentStatus.PendingReboot:
                            //reboot is required
                            ScriptManager.RegisterStartupScript(this.Page, GetType(), "RebootMachine", ControlHelper.JsFormat("$(function(){{RebootMachine('{0}', '{1}');}});", deploymentInfo.Agent.AgentId.ToString(), node.Caption), true);
                            return;
                    }
                }
                else if (PollingMethodSelector.Windows) // WMI & RPC polling method was selected
                {
                    if (!PollingMethodSelector.WmiInfo.Validate(false))
                        return;

                    PollingMethodSelector.WmiInfo.Update();
                    NodePropertyPluginManager.Update();

                    UsernamePasswordCredential wmiCredential = NodeWorkflowHelper.WmiCredential as UsernamePasswordCredential;

                    if (wmiCredential != null)
                    {
                        NodeWorkflowHelper.Node.SysName =
                            coreProxy.GetWmiSysName(NodeWorkflowHelper.Node.IpAddress, wmiCredential.Username, wmiCredential.Password);

                        if (!String.IsNullOrEmpty(NodeWorkflowHelper.Node.SysName))
                            NodeWorkflowHelper.Node.Caption = NodeWorkflowHelper.Node.SysName;
                    }

                    Response.Redirect(NodeWorkflowHelper.GetNextStepUrl());
                }
                else
                {
                    //suggested fix for FB9814, which duplicates the behaviour already coded for when it is ICMP (see further down). 
                    //The decision was made not to implement this but I'll leave the code in place and commented out in case we want
                    //to put it in at a later date.
                    //if (existingNode)
                    //{
                    //    ScriptManager.RegisterStartupScript(this.Page, GetType(), "isbeingmonitored", string.Format("alert('{0} is already being monitored.');", HostNameIP1.HostNameIP), true);
                    //    NodeWorkflowHelper.Node = NodeWorkflowHelper.CreateNode();
                    //    return;
                    //}

                    node.ObjectSubType = "SNMP";

                    // Validate SNMP, version specified by user:
                    string snmpNodeValidateErrorMessage;
                    var result = ValidateSNMPNode(coreProxy, node.IpAddress, out snmpNodeValidateErrorMessage);
                    if (result.ValidationSucceeded)
                    {
                        if (nodeAlreadyExists)
                        {
                            NodeWorkflowHelper.AllNodePollers = coreProxy.GetAllPollersForNode(node.Id);
                        }

                        PollingMethodSelector.SnmpInfo.SaveSNMPv3CredentialsToSession();

                        NodePropertyPluginManager.Update();

                        NodeWorkflowHelper.Node.SysName = GetSysName(NodeWorkflowHelper.Node, coreProxy);

                        // SysName is prefered caption among hostname and IP
                        if (!String.IsNullOrEmpty(NodeWorkflowHelper.Node.SysName))
                            NodeWorkflowHelper.Node.Caption = NodeWorkflowHelper.Node.SysName;

                        Response.Redirect(NodeWorkflowHelper.GetNextStepUrl());
                    }
                    // If SNMPv2c validation failed, try to validate using SNMPv1:
                    else if (string.IsNullOrEmpty(snmpNodeValidateErrorMessage) && node.SNMPVersion == SNMPVersion.SNMP2c)
                    {
                        PollingMethodSelector.SnmpInfo.SNMPVersion = SNMPVersion.SNMP1;

                        result = ValidateSNMPNode(coreProxy, node.IpAddress, out snmpNodeValidateErrorMessage);
                        if (result.ValidationSucceeded)
                        {
                            if (nodeAlreadyExists)
                            {
                                NodeWorkflowHelper.AllNodePollers = coreProxy.GetAllPollersForNode(node.Id);
                            }
                            ClientScriptManager manager = Page.ClientScript;
                            string postBackReference = manager.GetPostBackEventReference(btnInvoker, "");

                            //If the Node can only support SNMP1 run the javascript below to pop a dialog and if they confirm  
                            //the button click is called again setting it to SNMP1. 
                            string script =
                                ControlHelper.JsFormat(@"if (confirm('{0}'))
							{{
								{1};
							}}",
                                                       Resources.CoreWebContent.WEBCODE_YK0_18, postBackReference);

                            if (!manager.IsStartupScriptRegistered("SNMP1"))
                                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "SNMP1", script,
                                                                                  true);
                        }
                        else
                        {
                            node.SNMPVersion = SNMPVersion.SNMP2c;
                            PollingMethodSelector.SnmpInfo.SnmpVersionControl.SelectedValue = "SNMPv2c";

                            RegisterNodeNotRespondingScript(result, node, snmpNodeValidateErrorMessage);
                            NodeWorkflowHelper.Node = NodeWorkflowHelper.CreateNode();
                            return;
                        }
                    }
                    else
                    {
                        RegisterNodeNotRespondingScript(result, node, snmpNodeValidateErrorMessage);
                        NodeWorkflowHelper.Node = NodeWorkflowHelper.CreateNode();
                        return;
                    }
                }
            }
            else
            // ICMP
            {
				var selectedPluginMethod = PollingMethodSelector.SelectedPluginMethod;

				if (selectedPluginMethod != null)
                {
                    Session[addNodePluginStateName] = selectedPluginMethod.SaveState(node, PollingMethodSelector.SelectedPluginSettingsControl);
                }

                using (SolarWinds.Orion.Core.Common.i18n.LocaleThreadState.EnsurePrimaryLocale())
                {

                    node.ReadOnlyCredentials.CommunityString = string.Empty;
                    node.SysName = string.Empty;
                    node.Vendor = Resources.CoreWebContent.WEBCODE_UnknownVendor;
                    node.VendorIcon = "Unknown.gif";
                    node.SysObjectID = string.Empty;
                    node.Description = string.Empty;
                    node.Location = string.Empty;
                    node.UnPluggable = PollingMethodSelector.UnPluggable;
                    node.Id = 0;
                    node.MachineType = Resources.CoreWebContent.WEBCODE_UnknownMachineType;

                    try
                    {
                        var pageLayout = PollingMethodSelector.SelectedPluginMethod?.GetPageLayout();

                        // do reverse DNS only if not specified already (e.g. by IAddNodePollingMethodPlugin)
                        // and when polling plugin did not specified hostname or is standard polling method selected (ie. plugin is null)
                        if (string.IsNullOrEmpty(node.Caption) && string.IsNullOrEmpty(pageLayout?.Hostname))
                        {
                            var rdns = coreProxy.ResolveNodeNameByIP(node.IpAddress);
                            if (!string.IsNullOrEmpty(rdns))
                            {
                                node.Caption = coreProxy.ResolveNodeNameByIP(node.IpAddress);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        log.ErrorFormat("failed reverse dns for ip {0}", node.IpAddress);
                    }
                }

                listResourceSuceeded = true;

                if (nodeAlreadyExists && canAddDuplicateNode)
                {
                    NodeWorkflowHelper.Node = node;
                    NodePropertyPluginManager.Update();
                    Response.Redirect(NodeWorkflowHelper.GetNextStepUrl(), true);
                }
            }
        }
        finally
        {
            if (coreProxy != null)
                coreProxy.Dispose();
        }

        NodeWorkflowHelper.Node = node;
    }

    private string GetSysName(Node node, ICoreBusinessLayer blProxy)
    {
        var response = new Dictionary<string, string>();

        if (blProxy.NodeSNMPQuery(node, OID.sysName, "GET", out response))
        {
            if (response.Count > 0)
            {
                var setting = SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("SWNetPerfMon-Settings-VulnerabilityCheckDisabled");

                if (setting != null && setting.SettingValue == 1)
                    return response.Values.First();

                return VulnerabilityHelper.GetSafeText(response.Values.First());
            }

            return String.Empty;
        }
        else
        {
            return String.Empty;
        }
    }

    private bool CheckIpOrHostName(Node node)
    {
        string userInput = IPAddressHelper.ToStringIp(HostNameIP1.HostNameIP.Trim());

        bool skipDNS = false;
        // if input is the same as before, return
        if ((node.IpAddress != string.Empty) &&
            (userInput != string.Empty) &&
            (node.IpAddress == userInput || node.DNS == userInput) &&
            (node.EngineID == PollingEngine.EngineID))
            skipDNS = true;

        //Only bail early if the user hasn't changed the host/IP textbox and the agent is already install. Ref: OA-1288
        if (!skipDNS && IsAgentAndHasAgent(node))
            return true; // we have agent, we don't need anything else


        node.EngineID = PollingEngine.EngineID;
        node.DynamicIP = HostNameIP1.DynamicIpAddressValue;

        //if the user entered an ip address
        if (SolarWinds.Common.Net.HostHelper.IsIpAddress(userInput))
        {
            IPAddress ip = IPAddress.Parse(userInput);

            //FB 17675 - It seems that we're assuming that '.IsIpAddress' above is only going to return true if we have entered a 
            //valid ip address but this does not appear to be the case, we can for example just enter 50 and it returns true
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                string[] strBytes = userInput.Split('.');

                //FB 17675 - check we've got a 4 part ip address so we don't get an out of bounds error and pop a warning
                // and bail out if we have
                if (strBytes.Count() != 4)
                {
                    ScriptManager.RegisterStartupScript(this.Page, GetType(), "NotValidIP", ControlHelper.JsFormat("alert('{0}');", string.Format(Resources.CoreWebContent.WEBCODE_YK0_5, userInput)), true);
                    node.Name = string.Empty;
                    node.DNS = string.Empty;
                    return false;
                }

                int b0 = int.Parse(strBytes[0]);
                int b1 = int.Parse(strBytes[1]);
                int b2 = int.Parse(strBytes[2]);
                int b3 = int.Parse(strBytes[3]);

                string newIP = string.Format("{0}.{1}.{2}.{3}", b0, b1, b2, b3);
                CheckListResousces(node, newIP, true);

            }
            else
            {
                CheckListResousces(node, userInput, true);
            }

            string hostname = LookupName(ip, false);
            node.Caption = node.Name = node.DNS = !String.IsNullOrEmpty(hostname) ? hostname : string.Empty;
        }
        else
        //if the user entered a hostname
        {
            if (!SolarWinds.Common.Net.HostHelper.IsIpAddress(userInput) && !userInput.Trim().Equals(node.Name))
            {
                try
                {
                    bool userInputHasChanged = node.Name != userInput;
                    node.Name = userInput;
                    node.DNS = node.Name;
                    node.Caption = node.Name;
                    node.UnPluggable = PollingMethodSelector.UnPluggable;

                    /*try
                    {*/
                    if (!skipDNS)
                    {
                        var defaultResolution = SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting("SWNetPerfMon-Settings-Default Preferred AddressFamily DHCP");
                        AddressFamily addressResolution = (defaultResolution != null) && (defaultResolution.SettingValue == 6) ? AddressFamily.InterNetworkV6 : AddressFamily.InterNetwork;

                        try
                        {
                            IPAddress ip;
                            //for agent polling method,  if agent already exists then try to get IP address from the agent rather than resolve it
                            if (node.NodeSubType == NodeSubType.Agent && NodeWorkflowHelper.AgentCredential != null)
                            {
                                if (userInputHasChanged && !SolarWinds.Common.Net.HostHelper.IsIpAddress(userInput))
                                {
                                    using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, node.EngineID))
                                    {
                                        ip = proxy.GetHostAddress(userInput, addressResolution); 
                                    }
                                }
                                else
                                {
                                    //ip address should already predefined in this case, if not something went wrong
                                    ip = IPAddress.Parse(node.IpAddress);
                                }
                            }
                            else
                            {
                                using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, node.EngineID))
                                {
                                    ip = proxy.GetHostAddress(userInput, addressResolution); 
                                }
                            }
                            CheckListResousces(node, IPAddressHelper.ToStringIp(ip), false);
                        }
                        catch (System.ServiceModel.FaultException)
                        {
                            string errorGetHostAddress = String.Format("IP address for host {0} is missing", userInput);
                            log.Error(errorGetHostAddress);
                            throw;
                        }
                    }
                }
                catch (Exception)
                {
                    ScriptManager.RegisterStartupScript(this.Page, GetType(), "CannotResolveIP", ControlHelper.JsFormat("alert('{0}');", string.Format(Resources.CoreWebContent.WEBCODE_YK0_3, userInput)), true);
                    node.Name = string.Empty;
                    node.DNS = string.Empty;
                    return false;
                }
            }
        }
        return true;
    }

    private static bool IsAgentAndHasAgent(Node node)
    {
        return node.ObjectSubType == NodeSubTypeString.Agent && NodeWorkflowHelper.AgentCredential != null && NodeWorkflowHelper.AgentCredential.AgentId > 0;
    }

    private string ComposeSNMPValidationErrorMsg(NodeWorkflowHelper.SNMPValidationResult result, SNMPVersion snmpVersion, string nodeName, string readOnlyCommunity)
    {
        if (snmpVersion == SNMPVersion.SNMP3)
        {
            if (result.ValidationBothFailed)
                return string.Format(Resources.CoreWebContent.WEBCODE_YK0_22, nodeName);
            else if (result.ValidationWriteFailed)
                return string.Format(Resources.CoreWebContent.WEBCODE_YK0_21, nodeName);
            else
                return string.Format(Resources.CoreWebContent.WEBCODE_YK0_20, nodeName);
        }

        // SNMPv1 or SNMPv2:
        if (result.ValidationBothFailed)
            return string.Format(Resources.CoreWebContent.WEBCODE_YK0_19, nodeName);
        else if (result.ValidationWriteFailed)
            return string.Format(Resources.CoreWebContent.WEBCODE_YK0_11, nodeName);
        else
            return string.Format(Resources.CoreWebContent.WEBCODE_YK0_10, nodeName, readOnlyCommunity);
    }

    protected void btnInvoker_Click(object sender, EventArgs e)
    {
        PollingMethodSelector.SnmpInfo.SnmpVersionControl.SelectedValue = "SNMPv1";
        Next();
        Response.Redirect(NodeWorkflowHelper.GetNextStepUrl());
    }

    protected void PollingEngine_EngineChanged(object sender, EventArgs e)
    {
        var engineId = PollingEngine.EngineID;
        NodeWorkflowHelper.Node.EngineID = engineId;
        PollingMethodSelector.SetEngineId(engineId);
    }

    static string ToJSON(object value, params Type[] types)
    {
        DataContractJsonSerializer serializer = new DataContractJsonSerializer(value.GetType(), types);
        using (MemoryStream ms = new MemoryStream())
        {
            serializer.WriteObject(ms, value);
            return Encoding.UTF8.GetString(ms.ToArray());
        }
    }

    protected string GetReadOnlyCommunityStrings()
    {
        var sb = new StringBuilder();
        var table = SqlDAL.GetReadOnlyCommunityStrings();
        foreach (DataRow row in table.Rows)
        {
            sb.AppendFormat("'{0}',", CommonHelper.FixReadOnlyCommunityString(row[0].ToString()));
        }
        if (sb.Length > 0)
        {
            sb.Length--;
        }
        return sb.ToString();
    }

    public static string LookupName(IPAddress ipAddress, bool useNetBios)
    {
        string hostName;

        if (ipAddress.AddressFamily == AddressFamily.InterNetworkV6)
        {
            try
            {
                hostName = DnsHelper.ReverseLookup(ipAddress);
            }
            catch (Exception exc)
            {
                log.Debug(String.Format("Reverse DNS lookup for IP {0} failed", ipAddress), exc);
                return String.Empty;
            }
        }
        else
        {
            try
            {
                var hostEntry = DnsHelper.GetIPv4HostEntry(ipAddress, useNetBios);
                hostName = hostEntry.HostName;
            }
            catch (Exception exc)
            {
                log.Debug(String.Format("Reverse DNS lookup for IP {0} failed", ipAddress), exc);
                return String.Empty;
            }
        }

        if (log.IsDebugEnabled)
        {
            log.DebugFormat("Reverse DNS lookup result for {0} is {1}", ipAddress, hostName);
        }

        return hostName;
    }

    protected void HostNameIP_OnHostNameIPChanged(object sender, EventArgs e)
    {
        if (PollingMethodSelector.Agent && PollingMethodSelector.AgentInfo.DeploymentInfo != null)
        {
            var engineId = PollingMethodSelector.AgentInfo.DeploymentInfo.Agent.PollingEngineId;
            NodeWorkflowHelper.Node.EngineID = engineId;
            PollingEngine.EngineID = engineId;
        }

        CheckIpOrHostName(NodeWorkflowHelper.Node);
        AllowDuplicateNode = false;
        NodeWorkflowHelper.AgentCredential = null;
    }

    private NodeWorkflowHelper.SNMPValidationResult ValidateSNMPNode(ICoreBusinessLayer coreProxy, string ipAddress, out string errorMessage)
    {
        return NodeWorkflowHelper.ValidateSNMPNodeWithErrorMessage(
            coreProxy,
            PollingMethodSelector.SnmpInfo.SNMPVersion,
            ipAddress,
            PollingMethodSelector.SnmpInfo.SNMPPort,
            PollingMethodSelector.SnmpInfo.CommunityString,
            PollingMethodSelector.SnmpInfo.RWCommunityString,
            PollingMethodSelector.SnmpInfo.AuthPasswordKey,
            PollingMethodSelector.SnmpInfo.RWAuthPasswordKey,
            PollingMethodSelector.SnmpInfo.SNMPv3AuthType,
            PollingMethodSelector.SnmpInfo.RWSNMPv3AuthType,
            PollingMethodSelector.SnmpInfo.SNMPv3PrivacyType,
            PollingMethodSelector.SnmpInfo.RWSNMPv3PrivacyType,
            PollingMethodSelector.SnmpInfo.PrivacyPasswordKey,
            PollingMethodSelector.SnmpInfo.RWPrivacyPasswordKey,
            PollingMethodSelector.SnmpInfo.SNMPv3Context,
            PollingMethodSelector.SnmpInfo.RWSNMPv3Context,
            PollingMethodSelector.SnmpInfo.SNMPv3Username,
            PollingMethodSelector.SnmpInfo.RWSNMPv3Username,
            PollingMethodSelector.SnmpInfo.AuthKeyIsPwd,
            PollingMethodSelector.SnmpInfo.RWAuthKeyIsPwd,
            PollingMethodSelector.SnmpInfo.PrivKeyIsPwd,
            PollingMethodSelector.SnmpInfo.RWPrivKeyIsPwd,
            out errorMessage);
    }

    private void RegisterNodeIsBeingMonitored(bool canReAdd, bool showContinue, string existingNodePollingEngineName, int existingNodeId)
    {
        // open dialog and pass actual IP address and engine name into it 
        ScriptManager.RegisterStartupScript(this.Page, GetType(), "isbeingmonitored",
            string.Format("OpenDuplicatedNodeDetectedDialog(\"{0}\", \"{1}\", \"{2}\", {3}, {4});",
                string.Format(Resources.CoreWebContent.WEBCODE_YK0_6, NodeWorkflowHelper.Node.IpAddress, existingNodePollingEngineName),
                string.Format(Resources.CoreWebContent.WEBCODE_PF0_2,
                    string.Format("<a href='/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}' class='sw-link' style='text-decoration: underline!important;' target='_blank'>",
                    existingNodeId), "</a>"),
                string.Format(Resources.CoreWebContent.WEBCODE_PF0_3, NodeWorkflowHelper.Node.IpAddress),
                canReAdd.ToString().ToLowerInvariant(),
                showContinue.ToString().ToLowerInvariant()),
            true);
    }

    private void RegisterNodeNotRespondingScript(NodeWorkflowHelper.SNMPValidationResult result, Node node, string customErrrorMessage = "")
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "NodeNotResponding",
                                            ControlHelper.JsFormat("alert('{0}');",
                                                                    string.IsNullOrEmpty(customErrrorMessage) ?
                                                                   ComposeSNMPValidationErrorMsg(
                                                                       result, node.SNMPVersion,
                                                                       node.Name,
                                                                       node.ReadOnlyCredentials.
                                                                           CommunityString) : customErrrorMessage), true);
    }

    private static Node GetExistingNode(Node node, ICoreBusinessLayer coreProxy, bool searchOnlySelectedEngine)
    {
        Dictionary<string, object> filters = new Dictionary<string, object>();
        Nodes nodes;
        Node existingNode = null;

        // Look-up node by DNS name
        if (!string.IsNullOrEmpty(node.DNS) && !string.IsNullOrEmpty(node.DNS.Trim()))
        {
            filters.Add("DNS", node.DNS);
            if (searchOnlySelectedEngine)
            {
                filters.Add("EngineId", node.EngineID);
            }

            nodes = coreProxy.GetNodesFiltered(filters, false, false);
            if (nodes.Count > 0)
            {
                var temp = nodes.FirstOrDefault();

                // if one node is IPv4 and the other IPv6 i.e. has different IP, then node is
                // claimed not as axisting because we want user to be able to add both
                if (temp != null && temp.IpAddress == node.IpAddress)
                    existingNode = temp;
            }
        }

        // Look-up node by IP address
        if (existingNode == null)
        {
            if (!((node.UnPluggable) && (String.IsNullOrEmpty(node.IpAddress)))) //Do not check unique of IP addresses on external device (device is without IP address)
            {
                filters.Clear();
                filters.Add("IP_Address", node.IpAddress);
                if (searchOnlySelectedEngine)
                {
                    filters.Add("EngineId", node.EngineID);
                }

                var candidates = coreProxy.GetNodesFiltered(filters, false, false);

                // prioritize non agent nodes
                existingNode = candidates.FirstOrDefault(n => n.NodeSubType != NodeSubType.Agent);

                // if there is only agent node return it so user is informed about conflict but have chance to add it
                if (existingNode == null && candidates.Any())
                {
                    existingNode = candidates.FirstOrDefault();
                }
            }
        }

        return existingNode;
    }

    #region IStep Members
    public string Step
    {
        get
        {
            return "Default";
        }
    }
    #endregion
}
