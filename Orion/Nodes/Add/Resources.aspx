<%@ Page Language="C#" MasterPageFile="~/Orion/Nodes/NodeManagementPage.master" AutoEventWireup="true"
    CodeFile="Resources.aspx.cs" Inherits="Orion_Nodes_Add_Resources" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_10 %>" %>

<%@ Register Src="~/Orion/Discovery/Controls/ResourceTree.ascx" TagPrefix="orion" TagName="ResourceTree" %>
<%@ Register Src="~/Orion/Nodes/Add/ProgressIndicator.ascx" TagName="Progress" TagPrefix="orion" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
<orion:Include ID="Include6" runat="server" File="/Nodes/styles/ToolTip.css" />
<orion:Include ID="Include7" runat="server" File="/Nodes/js/ToolTip.js" />    
</asp:Content>

<asp:Content ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton ID="NodesHelpButton1" runat="server" HelpUrlFragment="OrionPHNodeManagementAddNodeChooseResources" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <style type="text/css">
        .fillwidth {
            width: 99%;
        }
    </style>
    <script type="text/javascript">function ExecutePostBackEventReference(){
        if  (document.getElementById('<%= imgbNext.ClientID %>').disabled)  {return false;}
        <%=this.Page.ClientScript.GetPostBackEventReference(imgbNext, "") %>};
    </script>
    
    <orion:Include runat="server" File="WebForm_FireDefaultButton.js" />
    
    <h1 class="sw-hdr-title">
        <%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    <div class="TooltipLinkPlace" id="pollerTooltip" runat="server">
        <a href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("Orion", "OrionCorePHDeviceStudioCreatingPollers")) %>" class="TooltipLink" target="_blank" rel="noopener noreferrer"><span id="toolTip"></span></a>
        <script type="text/javascript">
            $().ready(function () {
                $().ready(function () {
                    $('#addnodecontent').addClass('fillwidth');
                    $('#toolTip').html(GetParametrizedTooltipHtml('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PR_03) %>',
                    '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PR_04) %>',
                     '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PR_03) %>',
                    '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PR_05) %>',
                     '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PR_06) %>'));
                });
            });
        </script>
    </div>    
    <orion:Progress ID="ProgressIndicator1" runat="server" />
    
    <div class="GroupBox">
        <span class="ActionName">
            <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_15 %>" />
            <%= DefaultSanitizer.SanitizeHtml(NodeWorkflowHelper.Node.Caption) %>
        </span>
        
        <br />
        
        <span style="padding-bottom: 10px; margin-bottom: 10px;">
            <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_18 %>" />
        </span>

        <br />
        
        <script type="text/javascript">
            var imgNextClientId = '#<%= imgbNext.ClientID %>';
            var imgNextHref = 'javascript: ExecutePostBackEventReference()';

            function BeforeResultReceived() {
                $(function () {
                    var $imgNextClientId = $(imgNextClientId);
                    $imgNextClientId.removeAttr("href");
                    $imgNextClientId.addClass('aspNetDisabled');
                    $imgNextClientId.addClass('sw-btn-disabled');
                });
            }

            function AfterResultReceived() {
                $(function () {
                    var $imgNextClientId = $(imgNextClientId);
                    $imgNextClientId.removeClass('aspNetDisabled');
                    $imgNextClientId.removeClass('sw-btn-disabled');
                });
            }

            function CreateDiscoveryJob() {
                $.ajax({
                    type: 'POST',
                    url: 'Resources.aspx/CreateDiscoveryJob',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (result) {
                        jobId = result.d;
                        if ((jobId === null) || jobId === '') {
                            //job not created, wait and try it again
                            setTimeout(function () { CreateDiscoveryJob() }, 5000);
                        } else {
                            __doPostBack('', 'JOB_ID=' + jobId);
                        }
                    },
                    error: function (ex) {
                    }
                });
            }

        </script>

        <div id="DiscoveryJobWaitingImage" runat="server" visible="false">
            <img src="/Orion/images/AJAX-Loader.gif" />
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_11) %>
        </div>

        <orion:ResourceTree runat="server" ID="ResourceTree" OnClientBeforeResultReceived="BeforeResultReceived" OnClientAfterResultReceived="AfterResultReceived" />
        
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" DisplayType="Secondary" LocalizedText="Back" automation="Back" ID="imgbBack" OnClick="btnBack_Click" 
                OnClientClick="SW.Core.ResourceTree.UpdateSelectionStateInSession();" />
            <orion:LocalizableButton runat="server" DisplayType="Primary" LocalizedText="Next" automation="Next" ID="imgbNext" OnClick="btnNext_Click" 
                OnClientClick="SW.Core.ResourceTree.UpdateSelectionStateInSession(ExecutePostBackEventReference); return false;" />
            <orion:LocalizableButton runat="server" DisplayType="Secondary" LocalizedText="Cancel" automation="Cancel" ID="imgbCancel" OnClick="imgbCancel_Click" />
        </div>
    </div>
</asp:Content>
