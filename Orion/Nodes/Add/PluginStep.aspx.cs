using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Orion.Web;

public partial class Orion_Nodes_Add_PluginStep : System.Web.UI.Page, IStep
{
	protected IAddNodePluginWizard PluginControl
	{
		get 
		{
			Control ctrl = ControlHelper.FindControlRecursive(this, "phPluginStepContainer");
			if (ctrl != null)
			{
                if (NodeWorkflowHelper.PluginSteps.Count == 0)
                {
                    //Plugins haven't been loaded, but PluginStep.aspx has been called somehow (possibly ReturnUrl after session timeout)
                    //Redirect to first node workflow step
                    List<WizardStep> totalSteps = AddNodeStepsManager.GetStepsForAddNodeWorkflow(String.Empty);
                    Response.Redirect(totalSteps[0].Url);
                }
                
                return (IAddNodePluginWizard)ctrl.Controls[0];
			}
				
			throw new Exception("Plugin control was not specified");
		}
	}

	[Obsolete("Remove when Core no longer supports APM 4.02")]
	protected void ApplyAPM402Patch()
	{
		if (!OrionModuleManager.IsInstalled("APM") || OrionModuleManager.IsModuleVersionAtLeast("APM", "4.2"))
			return;

        // APM 4.02 is hard wired to hook the next button via a jquery selector which includes an 'input' tag rule.
        // this is a fragile method and has since broken. This code will patch APM's javascript to look in the right location.

		Page.Form.Controls.AddAt(0, new System.Web.UI.WebControls.Literal{
			Text = @"<script type=""text/javascript"">Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function(){
	if( !SW || !SW.APM || !SW.APM.AssignApplicationsAddNodeWizard || !SW.APM.AssignApplicationsAddNodeWizard.initValidator ) return;
	try { eval('SW.APM.AssignApplicationsAddNodeWizard.initValidator=' + SW.APM.AssignApplicationsAddNodeWizard.initValidator.toString().replace(/\$\(""input\[id\$='_(TestButton|imgbNext)/g,'$(""[id$=\'_$1') ); } catch(ex) {}
});</script>"
		});
	}
	
	protected void Page_Init(object sender, EventArgs e)
	{
		ApplyAPM402Patch();
		LoadPluginStep();
	}
	

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
			LoadControlsValues();
	}


	protected void imgbNext_Click(object sender, EventArgs e)
	{
	    var addNodePluginValidator = PluginControl as IAddNodePluginValidator;
        if(addNodePluginValidator!=null && !addNodePluginValidator.Validate())
        {
            return;
        }

		PluginControl.Next();
		NodeWorkflowHelper.PluginStepControls.Add(PluginControl);		

		CurrentPluginIndex += 1;

		if (CurrentPluginIndex >= NodeWorkflowHelper.PluginSteps.Count)
		{
			CurrentPluginIndex -= 1; //in case we will be back to plugin page
			Response.Redirect(NodeWorkflowHelper.GetNextStepUrl(true)); //leave plugin page
		}
		else
		{
			Response.Redirect(Request.Url.AbsoluteUri); // stay on plugin page
		}
	}

	protected void imgbBack_Click(object sender, EventArgs e)
	{
		PluginControl.Previous();
		CurrentPluginIndex -= 1;

		if (CurrentPluginIndex < 0)
		{
			CurrentPluginIndex = 0; //in case we will be back to plugin page
			Response.Redirect(NodeWorkflowHelper.GetPreviousStepUrl()); //leave plugin page
		}
		else
		{
			Response.Redirect(Request.Url.AbsoluteUri); // stay on plugin page
		}
	}

	protected void imgbCancel_Click(object sender, EventArgs e)
	{
		PluginControl.Cancel();
	}

	private void LoadControlsValues()
	{
		//litTitle.Text = PluginControl.Title;
		this.Title = PluginControl.Title;
	}

	private void LoadPluginStep()
	{
		if (CurrentPluginIndex >= NodeWorkflowHelper.PluginSteps.Count)
		{
			return;
		}

		Control c = LoadControl(NodeWorkflowHelper.PluginSteps[CurrentPluginIndex].UserControlPath);
		c.ID = "PluginStepControl";

		//ensure there is only one user control in the placeholders control
		phPluginStepContainer.Controls.Clear();
		phPluginStepContainer.Controls.Add(c);
	}	

	private int CurrentPluginIndex
	{
		get
		{
			//object o = ViewState["CurrentPluginIndex"];
			//return (o == null ? 0 : Convert.ToInt32(ViewState["CurrentPluginIndex"]));
			return NodeWorkflowHelper.CurrentPluginIndex;
		}
		set
		{
			//ViewState["CurrentPluginIndex"] = value;
			NodeWorkflowHelper.CurrentPluginIndex = value;
		}
	}

	#region IStep Members

	public string Step
	{
		get { return "PluginStep"; }
	}

	#endregion
}
