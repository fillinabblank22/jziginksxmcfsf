using System;
using System.Collections.Generic;
using System.Web;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web;


public partial class Orion_Nodes_VolumeProperties : System.Web.UI.Page
{
    private static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
    private string pageGuid;

    public string ReturnTo
    {
        get { return ViewState["ReturnTo"]?.ToString() ?? string.Empty; }
        set { ViewState["ReturnTo"] = value; }
    }

    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }
    
    private void FillControls(string guid)
    {
        NodeWorkflowHelper.FillNetObjectIds("Volumes", guid);
        CustomProperties.CustomPropertiesTable = "Volumes";

        var multiselection = false;
        var intVolumeIds = new List<int>();
        if (NodeWorkflowHelper.GetMultiSelectedVolumeIds(guid).Count == 0
            && NodeWorkflowHelper.GetMultiSelectedVolumeIdsWithEngineIds(guid).Count == 0)
        {
            ReferrerRedirectorBase.Return("Default.aspx");
        }
        else if (NodeWorkflowHelper.GetMultiSelectedVolumeIds(guid).Count == 0)
        {
            foreach (var volumeId in NodeWorkflowHelper.GetMultiSelectedVolumeIdsWithEngineIds(guid))
            {
                int id;
                if (int.TryParse(NetObjectHelper.GetObjectID(volumeId.Substring(0, volumeId.LastIndexOf(":"))), out id))
                    intVolumeIds.Add(id);
            }
        }
        else
        {
            intVolumeIds = NodeWorkflowHelper.GetMultiSelectedVolumeIds(guid);
        }

        if (intVolumeIds.Count > 1)
            multiselection = true;

        PollingSettings.MultipleSelection = multiselection;


        var volumes = GetSelectedVolumes(NodeWorkflowHelper.GetMultiSelectedVolumeIds(guid).Count == 0);
        foreach (var volume in volumes)
            blVolumes.Items.Add(volume.FullName);

        if (!multiselection)
            SingleSelectionFillControls(volumes.FindByID(intVolumeIds[0]));
        else
            MultiSelectionFillControls(volumes, intVolumeIds.ToArray());
    }

    private void SingleSelectionFillControls(Volume volume)
    {
        PollingSettings.MultipleSelection_Selected = true;

        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            Node node = proxy.GetNodeWithOptions(volume.NodeID, false, false);
            lblNodeName.Text = HttpUtility.HtmlEncode(node.Caption);
            lblNodeName.NavigateUrl = Page.ResolveUrl(string.Format("../NetPerfMon/NodeDetails.aspx?NetObject=N:{0}", node.ID));
        }
        tbVolumeCaption.Text = volume.Caption;
        PollingSettings.NodeStatusPolling = volume.PollInterval;
        PollingSettings.CollectStatisticsEvery = volume.StatCollection;
    }

    private void MultiSelectionFillControls(Volumes volumes, int[] volumeIds)
    {
        lblNodeName.Visible = false;

        tbVolumeCaption.Visible = false;
        lblVolumeCaption.Visible = false;

        bool pollInterval = true;
        bool statCollEvery = true;

        var volume = volumes.FindByID(volumeIds[0]);

        for (int i = 1; i < volumes.Count; i++)
        {
            if (volume.PollInterval != volumes.FindByID(volumeIds[i]).PollInterval)
                pollInterval = false;

            if (volume.StatCollection != volumes.FindByID(volumeIds[i]).StatCollection)
                statCollEvery = false;
        }

        if (pollInterval)
            PollingSettings.NodeStatusPolling = volume.PollInterval;

        if (statCollEvery)
            PollingSettings.CollectStatisticsEvery = volume.StatCollection;

        PollingSettings.MultipleSelection_Selected = false;
    }

    private Volumes GetSelectedVolumes(bool loadFromPostData)
    {
        var volumes = new Volumes();
        if (loadFromPostData)
        {
            foreach (var engineVolumes in GetGroupedVolumeIds())
            {
                Volumes volumesPack;
                using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, engineVolumes.Key))
                {
                    volumesPack = proxy.GetVolumesByIds(engineVolumes.Value.ToArray());
                }

                foreach (var volume in volumesPack)
                {
                    volumes.Add(volume.ID, volume);
                }
            }
        }
        else
        {
            using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
            {
                for (var i = 0; i < NodeWorkflowHelper.GetMultiSelectedVolumeIds(pageGuid).Count; i++)
                {
                    var engineId = proxy.GetEngineIdForNetObject("V:" + NodeWorkflowHelper.GetMultiSelectedVolumeIds(pageGuid)[i]);
                    using (var proxy2 = _blProxyCreator.Create(BusinessLayerExceptionHandler, engineId))
                    {
                        var volume = proxy2.GetVolume(NodeWorkflowHelper.GetMultiSelectedVolumeIds(pageGuid)[i]);
                        volumes.Add(volume.ID, volume);
                    }
                }
            }
        }
        return volumes;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            pageGuid = Request["GuidID"];
            if (Request.UrlReferrer != null)
                ReturnTo = Request.UrlReferrer.ToString();
            HiddenFieldGuid.Value = pageGuid;
            FillControls(pageGuid);
            PollingSettings.StatusPollingText = Resources.CoreWebContent.WEBCODE_TM0_52;
        }
        else
        {
            pageGuid = HiddenFieldGuid.Value;
        }
        var volumes = GetSelectedVolumes(NodeWorkflowHelper.GetMultiSelectedVolumeIds(pageGuid).Count == 0);
        VolumesThresholds.Initialize(volumes);
    }

    protected void imbtnSubmit_Click(object sender, EventArgs e)
    {
		if (!Page.IsValid) return;

        var volumes = GetSelectedVolumes(NodeWorkflowHelper.GetMultiSelectedVolumeIds(pageGuid).Count == 0);

        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            foreach (var volume in volumes)
            {
                var engineId = proxy.GetEngineIdForNetObject("V:" + volume.ID);
                using (var proxy2 = _blProxyCreator.Create(BusinessLayerExceptionHandler, engineId))
                {
                    if (PollingSettings.MultipleSelection_Selected)
                    {
                        volume.PollInterval = PollingSettings.NodeStatusPolling;
                        volume.StatCollection = (short)PollingSettings.CollectStatisticsEvery;
                    }

                    if (tbVolumeCaption.Visible)
                    {
                        if (!string.IsNullOrEmpty(tbVolumeCaption.Text))
                            volume.Caption = tbVolumeCaption.Text;
                    }

                    volume.CustomProperties.Clear();
                    foreach (KeyValuePair<string, object> prop in CustomProperties.CustomProperties)
                        volume.CustomProperties.Add(prop);

                    proxy2.UpdateVolume(volume);
                }

				CustomProperties.UpdateCustomPropertiesRestrictions();
            }
        }

        VolumesThresholds.Update();

        imbtnCancel_Click(sender, e);
    }

    protected void imbtnCancel_Click(object sender, EventArgs e)
    {
        NodeWorkflowHelper.GetMultiSelectedVolumeIds(pageGuid).Clear();
        NodeWorkflowHelper.GetMultiSelectedVolumeIdsWithEngineIds(pageGuid).Clear();
        ReferrerRedirectorBase.Return(!string.IsNullOrEmpty(ReturnTo) ? ReturnTo : "Default.aspx");
    }

    private Dictionary<int, List<int>> GetGroupedVolumeIds()
    {
        var volumeIdsCollection = new Dictionary<int, List<int>>();
        var engines = new List<int>();

        for (var i = 0; i < NodeWorkflowHelper.GetMultiSelectedVolumeIdsWithEngineIds(pageGuid).Count; i++)
        {
            int id, engineId;
            var parts = NodeWorkflowHelper.GetMultiSelectedVolumeIdsWithEngineIds(pageGuid)[i].Split(':');
            if (parts.Length < 3 || !int.TryParse(parts[1], out id) || !int.TryParse(parts[2], out engineId))
            {
                throw new ArgumentException(string.Format("'{0}' is not a valid net object id", NodeWorkflowHelper.GetMultiSelectedVolumeIdsWithEngineIds(pageGuid)[i]));
            }

            if (!engines.Contains(engineId))
            {
                engines.Add(engineId);
            }

            if (!volumeIdsCollection.ContainsKey(engineId))
            {
                volumeIdsCollection[engineId] = new List<int>();
            }

            volumeIdsCollection[engineId].Add(id);
        }
        return volumeIdsCollection;
    }
}

