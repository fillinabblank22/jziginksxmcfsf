<%@ Page Language="C#" MasterPageFile="~/Orion/Nodes/NodeManagementPage.master" AutoEventWireup="true"
    CodeFile="VolumeProperties.aspx.cs" Inherits="Orion_Nodes_VolumeProperties"
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_110 %>" %>

<%@ Register Src="~/Orion/Nodes/Controls/PollingSettings.ascx" TagPrefix="orion" TagName="PollingSettings" %>
<%@ Register Src="~/Orion/Nodes/Controls/CustomProperties.ascx" TagPrefix="orion" TagName="CustomProperties" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Controls/VolumesThresholdControl.ascx" TagPrefix="orion" TagName="VolumesThresholdControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
<style type="text/css">
    .sw-pg-selected-items ul { margin: 0px 20px 20px 20px; } /* progress indicator is blowing this margin out */
    table#breadcrumb { margin-bottom: 0px; }
    .sw-hdr-title { margin-top: 15px; } /* old breadcrumb + new title styling */
    .sw-pg-selected-items li, ul {list-style: disc outside none; margin-left: 20px;}
</style>
    <orion:Include runat="server" File="styles/Forecasting.css" />

</asp:Content>

<asp:Content ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionPHNodeManagementEditVolume" />
     <asp:HiddenField ID="HiddenFieldGuid" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <table id="breadcrumb">
		<tr>
			<td><a href="../Admin"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_66) %></a> &gt; <a href="../Nodes"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_115) %></a> &gt; <asp:HyperLink runat="server" ID="lblNodeName"></asp:HyperLink>
			</td>
		</tr>
	</table>
   
   <h1 class="sw-hdr-title"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
   <div class="sw-hdr-subtitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_111) %></div>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress DynamicLayout="false" ID="UpdateProgress1" runat="server">
                <ProgressTemplate>
                    <img alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_113) %>" src="../images/AJAX-Loader.gif" /><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_114) %>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="sw-pg-selected-items">
                <asp:BulletedList runat="server" ID="blVolumes">
                </asp:BulletedList>
            </div>
            <div class="node-groupbox">
                <table width="100%">
                    <tr>
                        <td colspan="2">
                            <asp:Label runat="server" ID="lblVolumeCaption"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_112) %> </asp:Label>
                            <asp:TextBox Width="65%" ID="tbVolumeCaption" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <orion:PollingSettings runat="server" ID="PollingSettings" HideTopologySection="true" />
                        </td>
                    </tr>
                </table>
                <orion:CustomProperties runat="server" ID="CustomProperties" NetObjectType="Volumes" />
                <orion:VolumesThresholdControl runat="server" ID="VolumesThresholds" />
            </div>
            <br />
            <div class="sw-btn-bar">
                <orion:LocalizableButton runat="server" ID="imbtnSubmit" OnClick="imbtnSubmit_Click" LocalizedText="Submit" DisplayType="Primary" />
                <orion:LocalizableButton runat="server" ID="imbtnCancel" OnClick="imbtnCancel_Click" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="False" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
