<%@ Page Language="C#" MasterPageFile="~/Orion/Nodes/NodeManagementPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_Nodes_Default" Title="<%$ HtmlEncodedResources: CoreWebContent, ResourcesAll_ManageNodes %>"  %>
<%@ Import Namespace="SolarWinds.Orion.Core.Common"%>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="orion" TagName="UnmanageDialog" Src="~/Orion/Controls/UnmanageDialog.ascx" %>
<%@ Register TagPrefix="orion" TagName="OverridePowerLevel" Src="~/Orion/Controls/OverridePowerLevel.ascx" %>
<%@ Register TagPrefix="orion" TagName="IncludeExtJs" Src="~/Orion/Controls/IncludeExtJs.ascx" %>
<%@ Register TagPrefix="orion" TagName="StartmaintenanceModeDialog" Src="~/Orion/Controls/StartmaintenanceModeDialog.ascx" %>
<%@ Register tagPrefix="orion" TagName="MaintenanceSchedulerDialog" Src="~/Orion/Controls/MaintenanceSchedulerDialog.ascx"%>
<%@ Register TagPrefix="orion" TagName="ProgressDialog" Src="~/Orion/Controls/ProgressDialog.ascx" %>
<%@ Register TagPrefix="orion" TagName="AdditionalPollingOptionsOrcInfo" Src="~/Orion/Nodes/Controls/AdditionalPollingOptionsOrcInfo.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include runat="server" File="js/jquery/timePicker.css" />
<orion:Include runat="server" File="NodeMNGMenu.css" />
<orion:Include runat="server" File="jquery/jquery.timePicker.js" />
<orion:Include runat="server" File="Nodes/js/NodeManagementPaging.js" />
<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="extjs/Ext.sw.grid.RadioSelectionModel.js" />
<orion:Include runat="server" File="Nodes/js/ChangeEngineDialog.js" />
<orion:Include runat="server" File="jquery/jquery.bgiframe.min.js" Section="Bottom" />
<orion:Include runat="server" File="js/StringUtils.js" />

<%--used for the delete nodes confirmation dialog--%>
<style type="text/css">
        #delDialog .dialogBody { padding-left:60px; margin-top:10px; background: url('/Orion/images/NotificationImages/notification_warning.gif') top left no-repeat }
        #delDialog .dialogItem { margin-bottom: 20px; }
        #delDialogAgentText { margin-left:5px; }
        .dialogBox { display: none; }

        #addDialog td.x-btn-center, #updateDialog td.x-btn-center { text-align: center !important; }
        #addDialog td, #updateDialog td { padding-right: 0; padding-bottom: 0; }
        #adminContent .dialogBody ul { margin-left: 20px; list-style-type: square; }    
        .StatusDialog div button { width: auto; }
</style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <span class="xui" ng-init="messageDissmessed = <%= _isNewPageMsgDismissed.ToString().ToLower() %>" ng-show="messageDissmessed">
        <sw-entity-management-new-page-switcher link-icon="star-full"></sw-entity-management-new-page-switcher>
    </span>
    <a href="/Orion/Admin/Pollers/ManagePollers.aspx"><img src="images/icons/icon_assignpoller.gif" alt="" /><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TD0_11) %></a> 
    <orion:IconLinkExportToPDF runat="server" />
    <a href="/Orion/Discovery/Default.aspx?origUrl=Nodes" class="sw-hdr-links-discover"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_7) %></a>
    <orion:IconHelpButton ID="IconHelpButton2" runat="server" HelpUrlFragment="OrionPHNodeManagementHome" />
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
	<asp:ScriptManagerProxy ID="ScriptManager1" runat="server" >
		<Services>
			<asp:ServiceReference path="/Orion/Services/Information.asmx" />
			<asp:ServiceReference path="../Services/NodeManagement.asmx" />
		</Services>
	</asp:ScriptManagerProxy>

<%foreach (var setting in new [] { "NodeSortProp", "NodeSortPropType", "NodeSortAsc",
		"InterfaceSortProp", "InterfaceSortPropType", "InterfaceSortAsc", "NodeColumns", "InterfaceColumns", "PageSize", "ActualPage","TotalCount"
			}) { %>
	<input type="hidden" name="WebNodeManagement_<%= DefaultSanitizer.SanitizeHtml(setting) %>" id="WebNodeManagement_<%= DefaultSanitizer.SanitizeHtml(setting) %>" value='<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("WebNodeManagement_" + setting)) %>' />
<%} if (!string.IsNullOrEmpty(_objectType) && !string.IsNullOrEmpty(_groupByProperty)) {%>
    <input type="hidden" name="WebNodeManagement_GroupBy" id="WebNodeManagement_GroupBy" value='<%= DefaultSanitizer.SanitizeHtml(_groupByProperty) %>' />
    <input type="hidden" name="WebNodeManagement_GroupByValue" id="WebNodeManagement_GroupByValue" value='' />
    <input type="hidden" name="WebNodeManagement_ObjectType" id="WebNodeManagement_ObjectType" value='<%= DefaultSanitizer.SanitizeHtml(_objectType) %>' />
<%} else { foreach (var setting in new [] { "GroupBy", "GroupByValue", "ObjectType" }) {%>
    <input type="hidden" name="WebNodeManagement_<%= DefaultSanitizer.SanitizeHtml(setting) %>" id="WebNodeManagement_<%= DefaultSanitizer.SanitizeHtml(setting) %>" value='<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("WebNodeManagement_" + setting)) %>' />
<%}} %>
	<input type="hidden" id="AllowInterfaces" value='<%= _allowInterfaces.ToString() %>'/>
	<input type="hidden" id="IsNPMInstalled" value='<%= _isNPMInstalled.ToString() %>'/>
	<input type="hidden" id="IsEnergyWise" value='<%= _isEnergyWise.ToString() %>'/>
	<input type="hidden" id="InterfaceCustomProperties" value='<%= DefaultSanitizer.SanitizeHtml(string.Join(",", GetCustomProperties("Interfaces"))) %>' />
		
    <input type="hidden" id="NodeCustomProperties" value='<%= DefaultSanitizer.SanitizeHtml(string.Join(",", GetCustomProperties("NodesCustomProperties"))) %>' />
        
    <input type="hidden" id="NodeInheritedProperties" value='<%= DefaultSanitizer.SanitizeHtml(OrionSerializationHelper.ToJSON(_inheritedProperties)) %>'/>
	<input type="hidden" id="ReturnToUrl" value="<%= DefaultSanitizer.SanitizeHtml(ReturnUrl) %>" />
    <input type="hidden" id="AllowMaintenanceMode" value="<%= AllowMaintenanceMode %>" />
	
    <table width="100%" style="border: 0; border-collapse: collapse;">
		<tr>
			<td width=250 style="padding: 0; margin:0;"><h1 class="sw-hdr-title"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1></td>

			<td valign="middle" style="padding-top: 2px;">
			<%if (_allowInterfaces)
				{%>
				<label for="showObjectType"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_252) %></label>
				<select id="showObjectType">
					<option value="Orion.Nodes" selected="selected"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_162) %></option>
					<option value="Orion.NPM.Interfaces"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_36) %></option>
				</select>
				<%}
			    else
			    { %>
                <select id="showObjectType" style="display: none">
					<option value="Orion.Nodes" selected="selected"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_162) %></option>
				</select>			      
			    <%} %>
				<input id="search" style="width: 250px" type="text" />
                <orion:LocalizableButtonLink runat="server" CssClass="searchButton" LocalizedText="CustomText" 
                                                Text="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_VB0_131 %>" 
                                                DisplayType="Small" style="vertical-align: baseline;"/>
			</td>
		</tr>
	</table>
    
    <div class="xui" ng-hide="messageDissmessed">
        <sw-entity-management-new-page-message on-dismiss="messageDissmessed = true"></sw-entity-management-new-page-message>
    </div>

	<table class="NodeManagement" <%= DefaultSanitizer.SanitizeHtml(AngularJsHelper.RenderNgNonBindable()) %> >
		<tr valign="top" align="left">
			<td style="border-right: #e1e1e0 1px solid; border-left: 0; border-top: 0; border-bottom: 0; background-color: white;">
				<div class="NodeGrouping">
					<div class="GroupSection">
						<div style="width: 180px"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_5) %></div>
						<select id="groupByProperty" style="width:100%">
							<option value=""><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AK0_70) %></option>
							<% if (_isEnergyWise)
							{%>
							    <option value="EnergyWise"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_338) %></option>
							<%} %>
							<option value="Vendor"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_124) %></option>
							<option value="MachineType"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_16) %></option>
                            <option value="Category" propertyType="System.Int32"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VS0_01) %></option>
							<option value="EngineID"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_134) %></option>
                            <option value="ObjectSubType"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_BV0_0047) %></option>
							<option value="SNMPVersion"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_340) %></option>
							<option value="Status"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_14) %></option>
							<option value="Location"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_128) %></option>
							<option value="Contact"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_129) %></option>
							<option value="Community"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_190) %></option>
							<option value="RWCommunity"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_339) %></option>
                            <option value="IsServer"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO_206) %></option>
                            <option value="IsOrionServer"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PC0_43) %></option>
                            <option value="IPAddressType"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_352) %></option>
                            <option value="Outage"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VS1_3) %></option>
                            <% if (_isNPMInstalled)
                                { %>
                            <option value="VlanId" propertyType="System.Int32"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS1_1) %></option>
                            <% } %>
                            <%foreach (InheritedProperty iprop in _inheritedProperties) { %>
                                <option value="<%= DefaultSanitizer.SanitizeHtml(iprop.NavigationName) %>" propertyType="<%= DefaultSanitizer.SanitizeHtml(iprop.PropertyType) %>"><%= DefaultSanitizer.SanitizeHtml(iprop.DisplayName) %></option>
                            <%} %>
                            <optgroup label="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_198) %>">
                                <% foreach (string prop in CustomPropertyMgr.GetGroupingPropNamesForTable("NodesCustomProperties", false))
                                    { %>
								<option value="CustomProperties.[<%= DefaultSanitizer.SanitizeHtml(prop) %>]" propertyType="<%= DefaultSanitizer.SanitizeHtml(CustomPropertyMgr.GetTypeForProp("NodesCustomProperties", prop)) %>"><%= DefaultSanitizer.SanitizeHtml(prop) %></option>
							    <%}%>
                            </optgroup>
						</select>
					</div>
					<ul class="NodeGroupItems"></ul>
				</div>
			</td>
			<td>
				<div class="NodeManagementMenuWrapper ui-helper-clearfix">
					<ul class="NodeManagementMenu ui-helper-clearfix">
						<li><a id="addLink" href="/Orion/Nodes/Add/Default.aspx"><img src="images/icons/icon_add.gif" alt="" /><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_10) %></a></li>
                        <li><a id="cpInlineEditorLink" href="#"><img src="/Orion/images/CPE/custom_properties_inline_editor.png" alt="" /><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_YK0_19) %></a></li>
						<li><a id="editLink" href="#"><img src="images/icons/icon_edit.gif" alt="" /><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_396) %></a></li>
						<li class="nodeCommand"><a id="listResourcesLink" href="#"><img src="images/icons/icon_list.gif" alt="" /><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_253) %></a></li>
                        <li <%= DefaultSanitizer.SanitizeHtml((!_isNPMInstalled) ? "style='display:none;'" : "") %>><a id="pollersLink" href="#"><img src="images/icons/icon_assignpoller.gif" alt="" /> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_254) %></a></li>
        
                        <% if (Profile.AllowUnmanage) { %>
                        <li class="submenu"><img src='/Orion/Nodes/images/icons/maintenance_mode_icon16.png' alt='' /><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_MH0_01) %></span>
							<ul>
								<li><a id="suppressAlertsNowLink" href="#" title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.HoverTooltipMutedAlerts) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LH0_16) %></a></li>
								<li><a id="resumeAlertsLink" href="#"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LH0_15) %></a></li>
                                <li class="groupSeparator"><a id="unmanageNowLink" href="#"><%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Core.Common.i18n.TokenSubstitution.Parse("@{R=Core.Strings.2;K=MaintenanceMode_UnmanageNow;E=xml}")) %></a></li>
                                <li><a id="manageAgainLink" href="#"><%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Core.Common.i18n.TokenSubstitution.Parse("@{R=Core.Strings.2;K=MaintenanceMode_ManageAgain;E=xml}")) %></a></li>
		    					<li class="groupSeparator"><a id="scheduleMaintenanceLink" href="#"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_LH0_17) %></a></li>
                            </ul>
                        </li>
                        <% } %>

        	            <li class="submenu"><span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_255) %></span>
							<ul>
								<li><a id="pollNowLink" href="#"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_117) %></a></li>
								<li><a id="rediscoverLink" href="#"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_118) %></a></li>
                                <li><a id="usePolledStatusLink" href="#"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AV0_O1) %></a></li>
								<li style='display: <%= _allowInterfaces? "block": "none" %>'><a id="shutdownLink" href="#"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_256) %></a></li>
								<li style='display: <%= _allowInterfaces? "block": "none" %>'><a id="enableLink" href="#"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_257) %></a></li>
								<li style='display: <%= _allowInterfaces? "block": "none" %>'><a id="powerLevelLink" href="#"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_258) %></a></li>
								<li id="energyWiseListItem" runat="server"><a id="energyWiseLink" href="#"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_259) %></a></li>
								<li style='display: <%= _multipleEngines? "block": "none" %>'><a id="changePollEngineLink" href="#"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_70) %></a></li>
                                <li <%= !_allowTopology ? "style='display:none;'" : String.Empty %>><a id="topologyLink" href="#"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_260) %></a></li>
                                <li><a id="importCPLink" href="#"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_23) %></a></li>
                                <li><a id="exportCPLink" href="#"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_24) %></a></li>
                                <li><a id="cancelOutagesLink" href="#"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBJS_VS1_1) %></a></li>
                                <%
                                    foreach (NodeManagementPluginManager.ActionListItem item in this.GetActionListItems())
                                    {
                                        %>
                                            <li><a id='<%= DefaultSanitizer.SanitizeHtml(item.ID) %>' data-enabled='<%= DefaultSanitizer.SanitizeHtml(item.JSEnabled) %>' data-callback='<%= DefaultSanitizer.SanitizeHtml(item.JSCallback) %>' class='pluginActionListItem<%= DefaultSanitizer.SanitizeHtml(item.EnabledOnlyIfSelected?String.Empty:" alwaysEnabled") %>' href="#"><%= DefaultSanitizer.SanitizeHtml(item.DisplayName) %></a></li>
                                        <%
                                    }
                                %>
							</ul>
						</li>
						<li><a id="deleteLink" href="#"><img src="images/icons/icon_delete.gif" alt="" /> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_31) %></a></li>
					</ul>
				</div>
				
                <div id="selectAllPan"></div>	
                    
                <table id="NodeTree2" cellpadding="0" cellspacing="0">
					<thead>
						<tr>
						</tr>
					</thead>
					<tbody></tbody>					
				</table>				
							    
                <%-- bottom pager based on extJs v 2.2 --%>
                <div class="x-toolbar x-small-editor" id="extPager" style="height: 24px;">
                    <table cellspacing="0">
                        <tbody>
                            <tr>
                                <td id="Td1">
                                    <table cellspacing="0" cellpadding="0" border="0" class="x-btn-wrap x-btn x-btn-icon x-item-disabled"
                                        id="firstPageTable" style="width: auto;">
                                        <tbody>
                                            <tr>
                                                <td class="x-btn-left">
                                                    <i>&nbsp;</i>
                                                </td>
                                                <td class="x-btn-center">
                                                    <em unselectable="on">
                                                        <button type="button" class="x-btn-text x-tbar-page-first" id="firstPage">
                                                            &nbsp;</button></em>
                                                </td>
                                                <td class="x-btn-right">
                                                    <i>&nbsp;</i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td id="Td2">
                                    <table cellspacing="0" cellpadding="0" border="0" class="x-btn-wrap x-btn x-btn-icon x-item-disabled"
                                        id="previousPageTable" style="width: auto;">
                                        <tbody>
                                            <tr>
                                                <td class="x-btn-left">
                                                    <i>&nbsp;</i>
                                                </td>
                                                <td class="x-btn-center">
                                                    <em unselectable="on">
                                                        <button type="button" class="x-btn-text x-tbar-page-prev" id="previousPage">
                                                            &nbsp;</button></em>
                                                </td>
                                                <td class="x-btn-right">
                                                    <i>&nbsp;</i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <span class="ytb-sep" id="Span1"></span>
                                </td>
                                <td style="width: 30px;">
                                    <span class="ytb-text" id="Span2"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_9) %></span>
                                </td>
                                <td style="width: 30px;">
                                    <input type="text" class="sw-tbar-page-number x-tbar-page-number" value="1" size="3"
                                        id="pageNumber" />
                                </td>
                                <td>
                                    <span class="ytb-text" id="totalPageCount"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_264) %> </span>
                                </td>
                                <td>
                                    <span class="ytb-sep" id="Span3"></span>
                                </td>
                                <td id="Td3">
                                    <table cellspacing="0" cellpadding="0" border="0" class="x-btn-wrap x-btn x-btn-icon "
                                        id="nextPageTable" style="width: auto;">
                                        <tbody>
                                            <tr>
                                                <td class="x-btn-left">
                                                    <i>&nbsp;</i>
                                                </td>
                                                <td class="x-btn-center">
                                                    <em unselectable="on">
                                                        <button type="button" class="x-btn-text x-tbar-page-next" id="nextPage">
                                                            &nbsp;</button></em>
                                                </td>
                                                <td class="x-btn-right">
                                                    <i>&nbsp;</i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td id="Td4">
                                    <table cellspacing="0" cellpadding="0" border="0" class="x-btn-wrap x-btn x-btn-icon "
                                        id="lastPageTable" style="width: auto;">
                                        <tbody>
                                            <tr>
                                                <td class="x-btn-left">
                                                    <i>&nbsp;</i>
                                                </td>
                                                <td class="x-btn-center">
                                                    <em unselectable="on">
                                                        <button type="button" class="x-btn-text x-tbar-page-last" id="lastPage">
                                                            &nbsp;</button></em>
                                                </td>
                                                <td class="x-btn-right">
                                                    <i>&nbsp;</i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <span class="ytb-sep" id="Span4"></span>
                                </td>
                                <td style="width: 55px;">
                                    <span class="ytb-text" id="paegSizeTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_10) %></span>
                                </td>
                                <td>
                                    <input type="text" class="x-tbar-page-number sw-tbar-page-number" value="1" size="3"
                                        id="pageSize" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="x-paging-info" id="displayInfo" style="padding: 2px 2px 0px 2px; color: Black;">
                    </div>
                </div>
                <div id="testoutput">
                </div>
            </td>
        </tr>
    </table>
    <div id="lastErrorMessage"></div>	

	<orion:UnmanageDialog ID="UnmanageDialog1" runat="server" />
    <orion:OverridePowerLevel ID="OverridePowerLevel1" runat="server" />
    <orion:StartmaintenanceModeDialog ID="StartMaintenanceModeDialog" runat="server" />
    <orion:MaintenanceSchedulerDialog ID="MaintenanceSchedulerDialog" runat="server" />
    <orion:ProgressDialog ID="ProgressDialog" runat="server" />

	<div id='columnChooserDialog' style="display:none;" title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_263) %>">
		<div id='columnChooserDialogContents'>
			<ul id='availableColumnList'></ul>
		</div>
	</div>

<%--used for the poll net objects info box--%>
<div id="pollDialog" class="x-hidden">
	    <div class="x-window-header" id="pollDialogHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_261) %></div>
	    <div id="pollDialogBody" class="x-panel-body dialogBody">
	        <table class="pollDialogBodyTable">
	            <tr>
	                <td><div id="pollDialogDescription"></div></td>
	            </tr>
	            <tr>
	                <td><ul id="Ul2"></ul></td>
	            </tr>
            </table>
	    </div>
	</div>
    
<%--used for the delete nodes confirmation dialog--%>
    <div id="delDialog" class="dialogBox common-dialog">
	    <div id="delDialogBody" class="dialogBody">
	        <div id="delDialogDescription" class="dialogItem"></div>
            <div id="delDialogAgent" class="dialogItem">
                <input id="delDialogUninstallAgent" type="checkbox" /><span id="delDialogAgentText"></span>
            </div>
	    </div>
	    <div id="delDialogButtons" class="dialogButtons"></div>
	</div>
    
    <%--is this used anywhere ??? --%>
    <div id="Div1" class="x-hidden">
	    <div class="x-window-header"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_399) %></div>
	    <div id="Div2" class="x-panel-body dialogBody">
	    <table>
	        <tr>
	            <td><div id="Div3"></div></td>
	        </tr>
	        <tr>
	            <td><ul id="Ul1"></ul></td>
	        </tr>
        </table>
	    </div>
	</div>       
    
    <%--used in Change Polling Engine dialog--%>
    <div id="AdditionalPollingOptionsOrcInfo" class="x-hide-display">
        <orion:AdditionalPollingOptionsOrcInfo runat="server"/>
    </div>

	<%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer || !Profile.AllowNodeManagement) {%>
		<div id="isDemoMode" style="display:none;"></div>
	<%}%>

	<div id="originalQuery"></div>	
	<div id="test"></div>
	<pre id="stackTrace"></pre>

</asp:Content>
