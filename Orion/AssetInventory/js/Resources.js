﻿/*jslint browser: true*/
/*global SW: false*/
(function () {

    var Resources = SW.Core.namespace('SW.AssetInventory.Resources');

    function bytesToSize(bytes) {
        var sizes = ['B', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return '0 B';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        if (i == 0) return bytes + ' ' + sizes[i];
        return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
    }

    function hideRow(value, explicit) {
		// extra empty row in the modified table has to be added to respect zebra stripes.
        return (value === null || value === undefined || value === '' || value === explicit)
            ? ' class="aiHideData"></tr><tr class="aiHideData"'
            : '';
    }

    function hideResourceIfNoRows(tableSelector, resourceSelector) {
        var $tab = $(tableSelector);
        if ($tab.find('tr:not(.aiHideData)').length <= 1) {
            $(resourceSelector).hide();
        }
    }

    function prepareResultObject(properties) {
        var resultObject = {};
        for (var i = 0; i < properties.length; i++) {
            resultObject[properties[i]] = '';
        }
        return resultObject;
    }

    function displayTemplate(templateId, targetId, resultObject) {
        var source = $('#' + templateId).html();
        var newHtml = _.template(source, resultObject);
        $('#' + targetId).empty();
        $('#' + targetId).append(newHtml);
    }

    function displayTemplateFromList(templateId, targetId, resultObject) {
        var source = $('#' + templateId).html();
        var newHtml = _.template(source, { objects: resultObject });
        $('#' + targetId).empty();
        $('#' + targetId).append(newHtml);
    }

    function copyRowColumns(result, resultObject, row) {
        if (!result.Rows.length) {
            return;
        }
        row = row || result.Rows[0];
        result.Columns.forEach(function (column, i) {
            resultObject[column] = row[i];
        });
    }

    function displayTemplateFromSingleResultSWQL(templateId, targetId, swql) {
        Information.Query(swql, function (result) {
            var resultObject = prepareResultObject(result.Columns);
            copyRowColumns(result, resultObject);
            displayTemplate(templateId, targetId, resultObject);
        });
    }

    function getArguments(args, start, extra) {
        return Array.prototype.slice.call(args, start).concat(extra);
    }

    function displayTemplateFromWebServiceCall(templateId, targetId, methodName) {
        function resultHandler(result) {
            var resultObject = prepareResultObject(result.Columns);
            copyRowColumns(result, resultObject);
            displayTemplate(templateId, targetId, resultObject);
        }

        var args = getArguments(arguments, 3, [resultHandler]);
        AssetInventoryResources[methodName].apply(this, args);
    }

    function displayTemplateFromWebServiceCallByObject(templateId, targetId, methodName) {
        function resultHandler(result) {
            displayTemplate(templateId, targetId, result);
        }

        var args = getArguments(arguments, 3, [resultHandler]);
        AssetInventoryResources[methodName].apply(this, args);
    }

    function displayTemplateFromWebServiceCallByObjectList(templateId, targetId, methodName) {
        function resultHandler (result) {
            displayTemplateFromList(templateId, targetId, result);
        }

        var args = getArguments(arguments, 3, [resultHandler]);
        AssetInventoryResources[methodName].apply(this, args);
    }

    function displayTemplateFromWebServiceCallByObjectCallback(templateId, targetId, methodName, callback) {
        function resultHandler(result) {
            displayTemplate(templateId, targetId, result);
            callback();
        }

        var args = getArguments(arguments, 4, [resultHandler]);
        AssetInventoryResources[methodName].apply(this, args);
    }

    function displayTemplateFromWebServiceCallByObjectListCallback(templateId, targetId, methodName, callback) {
        function resultHandler(result) {
            displayTemplateFromList(templateId, targetId, result);
            callback();
        }

        var args = getArguments(arguments, 4, [resultHandler]);
        AssetInventoryResources[methodName].apply(this, args);
    }

    function displayTemplateFromWebServiceCallByMultRowsDataTable(templateId, targetId, methodName) {
        function resultHandler(result) {
            var results = [];
            result.Rows.forEach(function (row) {
                var resultObject = prepareResultObject(result.Columns);
                copyRowColumns(result, resultObject, row);
                results.push(resultObject);
            });
            displayTemplateFromList(templateId, targetId, results);
        }

        var args = getArguments(arguments, 3, [resultHandler]);
        AssetInventoryResources[methodName].apply(this, args);
    }

    function formatShortDate(value) {
        return (Date.isInstanceOfType(value))
            ? value.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern)
            : '';
    }

    function parseCustomQueryDate(value) {
        return (typeof value === 'string')
            ? Date.parseLocale(value, Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern + ' ' + Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern)
            : new Date(NaN);
    }

    function dateOnlyFormatter(value) {
        if (Date.isInstanceOfType(value)) {
            return formatShortDate(value);
        }
        if (typeof value === 'string' && value.length) {
            return formatShortDate(parseCustomQueryDate(value));
        }
        return '';
    }

    // publish methods

    Resources.displayTemplateFromSingleResultSWQL = displayTemplateFromSingleResultSWQL;
    Resources.displayTemplateFromWebServiceCall = displayTemplateFromWebServiceCall;
    Resources.displayTemplateFromWebServiceCallByObject = displayTemplateFromWebServiceCallByObject;
    Resources.displayTemplateFromWebServiceCallByObjectList = displayTemplateFromWebServiceCallByObjectList;
    Resources.displayTemplateFromWebServiceCallByObjectCallback = displayTemplateFromWebServiceCallByObjectCallback;
    Resources.displayTemplateFromWebServiceCallByObjectListCallback = displayTemplateFromWebServiceCallByObjectListCallback;
    Resources.displayTemplateFromWebServiceCallByMultRowsDataTable = displayTemplateFromWebServiceCallByMultRowsDataTable;

    Resources.dateOnlyFormatter = dateOnlyFormatter;

    Resources.hideRow = hideRow;
    Resources.hideResourceIfNoRows = hideResourceIfNoRows;

}());
