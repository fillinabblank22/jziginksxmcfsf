﻿using System;
using SolarWinds.AssetInventory.Web;
using SolarWinds.AssetInventory.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_AssetInventory_Resources_NodeDetails_OSUpdates : AssetInventoryNodeResourceBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // SNMP does not provide patches information
        if (WebHelper.IsNodeSnmp(Node.NodeID) || 
            !WebHelper.IsAnyRecordsAvalible(Node.NodeID, "Orion.AssetInventory.OSUpdates"))
        {
            Wrapper.Visible = false;
            return;
        }

        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        Wrapper.HeaderButtons.Controls.Add(SearchControl);

        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        CustomTable.SWQL = SWQL;
        CustomTable.SearchSWQL = SearchSWQL;
    }

    protected ResourceSearchControl SearchControl { get; private set; }

    public string SWQL
    {
        get { return string.Format(@"
SELECT Name AS [{1}], Url AS [_LinkFor_{1}], Type AS [{2}], InstallDate AS [{3}], InstalledBy AS [{4}]
FROM Orion.AssetInventory.OSUpdates
WHERE NodeID = {0}", Node.NodeID,
        Resources.AssetInventoryWebContent.AIWEBCODE_JT0_1, Resources.AssetInventoryWebContent.AIWEBCODE_JT0_2,
        Resources.AssetInventoryWebContent.AIWEBCODE_JT0_3, Resources.AssetInventoryWebContent.AIWEBCODE_JT0_4);
        }
    }

    public string SearchSWQL
    {
        get
        {
            return string.Format(@"
SELECT Name AS [{1}], Url AS [_LinkFor_{1}], Type AS [{2}], InstallDate AS [{3}], InstalledBy AS [{4}]
FROM Orion.AssetInventory.OSUpdates
WHERE NodeID = {0} AND ([{1}] LIKE '%${{SEARCH_STRING}}%' OR [{2}] LIKE '%${{SEARCH_STRING}}%' OR [{4}] LIKE '%${{SEARCH_STRING}}%')", 
        Node.NodeID,
        Resources.AssetInventoryWebContent.AIWEBCODE_JT0_1, Resources.AssetInventoryWebContent.AIWEBCODE_JT0_2,
        Resources.AssetInventoryWebContent.AIWEBCODE_JT0_3, Resources.AssetInventoryWebContent.AIWEBCODE_JT0_4);
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSAMAGAIOSUpdates"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.AssetInventoryWebContent.WEBCODE_OSUpdatesResourceTitle; }
    }

    public override string EditControlLocation
    {
        get
        {
            return @"/Orion/AssetInventory/Controls/EditPagableResourceControl.ascx";
        }
    }

    public override string IconPath
    {
        get { return "/Orion/AssetInventory/images/Resources/OS_patches_applied_icon16x16.png"; }
    }
}