﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OSUpdates.ascx.cs" Inherits="Orion_AssetInventory_Resources_NodeDetails_OSUpdates" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="AssetInventoryResource">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />

        <script type="text/javascript">
            $(function () {
                var resourceId = Number('<%= ScriptFriendlyResourceID %>');

                function refresh() {
                    SW.Core.Resources.CustomQuery.refresh(resourceId);
                }

                SW.Core.Resources.CustomQuery.initialize({
                    uniqueId: resourceId,
                    initialPage: 0,
                    rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,
                    searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                    searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                    allowSort: true,
                    customFormatters: {
                        '<%= Resources.AssetInventoryWebContent.AIWEBCODE_JT0_3 %>': SW.AssetInventory.Resources.dateOnlyFormatter
                    }
                });

                $('#<%= SearchControl.SearchBoxClientID %>').attr('placeholder', '<%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_5 %>');
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();

                // We need links from table to go to new window but we can't touch then directly because it's Core control.
                // This sets target to blank window as user clicks on the link.
                $(document).on('click', 'div[ResourceID=<%= ScriptFriendlyResourceID %>] .column0 a', function () {
                    $(this).attr('target', '_blank');
                });
            });
        </script>
    </Content>
</orion:resourceWrapper>
