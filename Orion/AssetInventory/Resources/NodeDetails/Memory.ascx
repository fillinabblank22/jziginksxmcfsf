﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Memory.ascx.cs" Inherits="Orion_AssetInventory_Resources_NodeDetails_Memory" %>

<asp:ScriptManagerProxy runat="server" >
    <Services>
        <asp:ServiceReference path="/Orion/Services/Information.asmx" />
        <asp:ServiceReference path="/Orion/AssetInventory/Services/AssetInventoryResources.asmx" />
    </Services>
</asp:ScriptManagerProxy>

<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="AssetInventoryResource">
    <Content>
        <script id="ResourceTemplate<%= ScriptFriendlyResourceID %>" type="text/x-template">
        {#
            function IsEmpty(totalMemoryMB, freeMemoryMB, virtualMemoryMB, freeVirtualMemoryMB) {
                return (totalMemoryMB <= 0) && (freeMemoryMB <= 0) && (virtualMemoryMB <= 0) && (freeVirtualMemoryMB <= 0);
            }
        #}
        {# if (!IsEmpty(TotalMemoryMB,FreeMemoryMB,VirtualMemoryMB,FreeVirtualMemoryMB)) { #}
            <div id="MemoryDetails">
                <span class="AssetInventoryResourceTableHeading"><%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_1 %></span><br />
                <table class="AssetInventoryResourceTable NeedsZebraStripes">
                    <tr class="HeaderRow">
                        <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_3 %></td>
                        <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_4 %></td>
                        <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_7 %></td>
                        <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_8 %></td>
                    </tr>
                    <tr>
                        <td>{{TotalMemoryMB == -1 ? "<%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_25 %>" : TotalMemoryMB.toString() + " MB"}}</td>
                        <td>{{FreeMemoryMB == -1 ? "<%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_25 %>" : FreeMemoryMB.toString() + " MB"}}</td>
                        <td>{{VirtualMemoryMB == -1 ? "<%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_25 %>" : VirtualMemoryMB.toString() + " MB"}}</td>
                        <td>{{FreeVirtualMemoryMB == -1 ? "<%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_25 %>" : FreeVirtualMemoryMB.toString() + " MB"}}</td>
                    </tr>
                </table>
            </div>
        {# } #}
        {# if (DisplayModules && Modules) { #}
            <div id="MemoryModules">
                <br />
                <span class="AssetInventoryResourceTableHeading"><%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_2 %></span><br />
                <table class="AssetInventoryResourceTable NeedsZebraStripes">
                    <tr class="HeaderRow">
                        <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBCODE_MD0_5 %></td>
                        <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBCODE_MD0_6 %></td>
                        <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBCODE_MD0_12 %></td>
                        <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBCODE_MD0_13 %></td>
                    </tr>
                {# _.each(Modules, function(current, index) { #}
                    <tr>
                        <td>{{current.Slot}}</td>
                        <td>{{current.CapacityMB == 0 ? "<%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_25 %>" : current.CapacityMB.toString() + " MB"}}</td>
                        <td>{{current.Model}}</td>
                        <td>{{current.Speed == 0 || current.Speed == 1 ? "<%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_25 %>" : current.Speed.toString() + " MHz"}}</td>
                    </tr>
                {# }); #}
                </table>
            </div>
        {# } #}
        {# if (UsedSlotsCount != null) { #}
            <br>
            <span class="AssetInventoryResourceTableHeading"><%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_18 %> {{UsedSlotsCount}}</span><br />
        {# } #}
        </script>
        <div id="ResourceContent<%= ScriptFriendlyResourceID %>"></div>
        <script type="text/javascript">
            $(function () {
                function refresh() {
                    SW.AssetInventory.Resources.displayTemplateFromWebServiceCallByObjectCallback(
                        'ResourceTemplate<%= ScriptFriendlyResourceID %>',
                        'ResourceContent<%= ScriptFriendlyResourceID %>',
                        'GetMemorySummary',
                        function () {
                            if (!$('#MemoryDetails,#MemoryModules').length) {
                                $('#[ResourceID=<%= ScriptFriendlyResourceID %>]').hide();
                            }
                        },
                        <%= Node.NodeID %>
                    );
                }

                SW.Core.View.AddOnRefresh(refresh, 'ResourceContent<%= ScriptFriendlyResourceID %>');
                refresh();
            });
        </script>
    </Content>
</orion:resourceWrapper>
