﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Ports.ascx.cs" Inherits="Orion_AssetInventory_Resources_NodeDetails_Ports" %>

<asp:ScriptManagerProxy runat="server">
    <services>
        <asp:ServiceReference path="/Orion/Services/Information.asmx" />
        <asp:ServiceReference path="/Orion/AssetInventory/Services/AssetInventoryResources.asmx" />
    </services>
</asp:ScriptManagerProxy>

<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="AssetInventoryResource">
    <Content>
        <script id="ResourceTemplate<%= ScriptFriendlyResourceID %>" type="text/x-template">
            {# if (SerialPortsCount != null || ParallelPortsCount != null) { #}
            <div id="Ports">
                <table class="AssetInventoryResourceTable NeedsZebraStripes">
                    <tr class="HeaderRow">
                        <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_19 %></td>
                        <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_20 %></td>
                    </tr>
                    <tr>
                        <td><img src="/Orion/AssetInventory/images/Resources/Serial_icon16x16.png"/>
                            <%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_22 %></td>
                        <td id="SerialPortsCount">{{SerialPortsCount != null ? SerialPortsCount : "<%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_25 %>" }}</td>
                    </tr>
                    <tr>
                        <td><img src="/Orion/AssetInventory/images/Resources/Parallel_icon16x16.png"/>
                            <%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_23 %></td>
                        <td id="ParallelPortsCount">{{ParallelPortsCount != null ? ParallelPortsCount : "<%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_25 %>" }}</td>
                    </tr>
                </table>
                <br/>
            </div>
            {# } #}
            {# if (USBs != null) { #}
            <div id="USBControllers">
                <table class="sw-custom-query-table, AssetInventoryResourceTable NeedsZebraStripes">
                    <tr class="HeaderRow">
                        <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_21 %></td>
                    </tr>
                {# _.each(USBs, function (current) { #}
                    <tr>
                        <td><img src="/Orion/AssetInventory/images/Resources/USB_icon16x16.png"/>{{current}}</td>
                    </tr>
                {# }); #}
                </table>
                <br/>
            </div>
            {# } #}
        </script>
        <div id="ResourceContent<%= ScriptFriendlyResourceID %>"></div>
        <script type="text/javascript">
            $(function () {
                function refresh() {
                    SW.AssetInventory.Resources.displayTemplateFromWebServiceCallByObjectCallback(
                        'ResourceTemplate<%= ScriptFriendlyResourceID %>',
                        'ResourceContent<%= ScriptFriendlyResourceID %>',
                        'GetPortsInfo',
                        function () {
                            if (!$('#Ports,#USBControllers').length) {
                                $('[ResourceID=<%= ScriptFriendlyResourceID %>]').hide();
                            }
                        },
                        <%= Node.NodeID %>
                    );
                }

                SW.Core.View.AddOnRefresh(refresh, 'ResourceContent<%= ScriptFriendlyResourceID %>');
                refresh();
            });
        </script>
    </Content>
</orion:resourceWrapper>
