﻿using SolarWinds.AssetInventory.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_AssetInventory_Resources_NodeDetails_StorageControllers : AssetInventoryNodeResourceBase
{
    /// <summary>
    /// Gets Storage Controllers title
    /// </summary>
    protected override string DefaultTitle
    {
        get { return Resources.AssetInventoryWebContent.WEBCODE_StorageControllersTitle; }
    }

    /// <summary>
    /// Gets Storage Controllers icon path
    /// </summary>
    public override string IconPath
    {
        get { return "/Orion/AssetInventory/images/Resources/Array_Controllers_icon16x16.png"; }
    }

    /// <summary>
    /// Gets Storage Controllers help link
    /// </summary>
    public override string HelpLinkFragment
    {
        get { return "OrionSAMAGAIStorageControllers"; }
    }
}
