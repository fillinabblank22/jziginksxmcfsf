﻿using System;
using SolarWinds.AssetInventory.Web;
using SolarWinds.AssetInventory.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_AssetInventory_Resources_NodeDetails_InstalledSoftware : AssetInventoryNodeResourceBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!WebHelper.IsAnyRecordsAvalible(Node.NodeID, "Orion.AssetInventory.Software"))
        {
            Wrapper.Visible = false;
            return;
        }

        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        Wrapper.HeaderButtons.Controls.Add(SearchControl);

        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        //CustomTable.UniqueClientID = Resource.ID;
        CustomTable.SWQL = SWQL;
        CustomTable.SearchSWQL = SearchSWQL;
    }

    protected ResourceSearchControl SearchControl { get; private set; }

    public string SWQL
    {
        get
        {
            return string.Format(@"
SELECT Name as [{1}], Publisher as [{3}], Version as [{2}], InstallDate as [{4}]  
FROM Orion.AssetInventory.Software WHERE NodeId = {0} ORDER BY Name", Node.NodeID,
                Resources.AssetInventoryWebContent.AIWEBCODE_MD0_1, Resources.AssetInventoryWebContent.AIWEBCODE_MD0_2,
                Resources.AssetInventoryWebContent.AIWEBCODE_MD0_3, Resources.AssetInventoryWebContent.AIWEBCODE_MD0_4);
        }
    }

    public string SearchSWQL
    {
        get
        {
            return string.Format(@"
SELECT Name AS [{1}], Publisher AS [{3}], Version AS [{2}], InstallDate AS [{4}]
FROM Orion.AssetInventory.Software
WHERE NodeID = {0} AND ([{1}] LIKE '%${{SEARCH_STRING}}%' OR [{3}] LIKE '%${{SEARCH_STRING}}%')
ORDER BY CASE WHEN Name IS NULL OR Name ='' THEN 1 ELSE 0 END, Name ASC", Node.NodeID,
        Resources.AssetInventoryWebContent.AIWEBCODE_MD0_1, Resources.AssetInventoryWebContent.AIWEBCODE_MD0_2,
        Resources.AssetInventoryWebContent.AIWEBCODE_MD0_3, Resources.AssetInventoryWebContent.AIWEBCODE_MD0_4);
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSAMAGAISoftwareInventory"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.AssetInventoryWebContent.WEBCODE_InstalledSoftwareResourceTitle; }
    }

    public override string EditControlLocation
    {
        get
        {
            return @"/Orion/AssetInventory/Controls/EditPagableResourceControl.ascx";
        }
    }

    public override string IconPath
    {
        get { return "/Orion/AssetInventory/images/Resources/Software_Inventory_icon16x16.png"; }
    }
}
