﻿using System;
using SolarWinds.AssetInventory.Web;
using SolarWinds.AssetInventory.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_AssetInventory_Resources_NodeDetails_WindowsUpdate : AssetInventoryNodeResourceBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Wrapper.Visible = WebHelper.IsNodeWMI(Node.NodeID) && 
                          WebHelper.IsAnyRecordsAvalible(Node.NodeID, "Orion.AssetInventory.ServerInformation");
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSAMAGAIWindowsUpdateInfo"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.AssetInventoryWebContent.WEBCODE_WindowsUpdateResourceTitle; }
    }

    public override string IconPath
    {
        get { return "/Orion/AssetInventory/images/Resources/Windows_Update_icon16x16.png"; }
    }
}