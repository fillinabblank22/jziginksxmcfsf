﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GraphicsAndAudio.ascx.cs" Inherits="Orion_AssetInventory_Resources_NodeDetails_GraphicsAndAudio" %>

<asp:scriptmanagerproxy runat="server">
    <Services>
        <asp:ServiceReference path="/Orion/Services/Information.asmx" />
        <asp:ServiceReference path="/Orion/AssetInventory/Services/AssetInventoryResources.asmx" />
    </Services>
</asp:scriptmanagerproxy>

<orion:resourcewrapper runat="server" id="Wrapper" CssClass="AssetInventoryResource">
    <Content>
        <!-- This is Underscore Javascript Template. See https://cp.solarwinds.com/display/OrionPlatformRD/Underscore+JavaScript+Templates -->
        <script id="ResourceTemplate<%= ScriptFriendlyResourceID %>" type="text/x-template">
            {# var hideRow = SW.AssetInventory.Resources.hideRow; #}
            <table class="AssetInventoryResourceTable NeedsZebraStripes">
                <tr class="HeaderRow">
                    <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBCODE_MD0_1 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_45 %></td>
                </tr>
                <tr{{hideRow(MonitorManufacturer)}}>
                    <td><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_46 %></td>
                    <td>{{MonitorManufacturer}}</td>
                </tr>
                <tr{{hideRow(MonitorModelNumber)}}>
                    <td><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_47 %></td>
                    <td>{{MonitorModelNumber}}</td>
                </tr>
                <tr{{hideRow(MonitorSerialNumber)}}>
                    <td><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_48 %></td>
                    <td>{{MonitorSerialNumber}}</td>
                </tr>
                <tr{{hideRow(MonitorResolution)}}>
                    <td><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_49 %></td>
                    <td>{{MonitorResolution}}</td>
                </tr>
                <tr{{hideRow(VideoCard)}}>
                    <td><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_50 %></td>
                    <td>{{VideoCard}}</td>
                </tr>
                <tr{{hideRow(VideoChipset)}}>
                    <td><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_51 %></td>
                    <td>{{VideoChipset}}</td>
                </tr>
                <tr{{hideRow(VideoMemoryMB, 0)}}>
                    <td><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_52 %></td>
                    <td><%= String.Format("{0} MB", "{{VideoMemoryMB}}") %></td>
                </tr>
                <tr{{hideRow(SoundCards)}}>
                    <td><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_53 %></td>
                    <td>{{SoundCards}}</td>
                </tr>
            </table>
        </script>
        <div id="ResourceContent<%= ScriptFriendlyResourceID %>">
        </div>
        <script type="text/javascript">
            $(function () {
                function refresh() {
                    SW.AssetInventory.Resources.displayTemplateFromWebServiceCallByObjectCallback(
                        'ResourceTemplate<%= ScriptFriendlyResourceID %>',
                        'ResourceContent<%= ScriptFriendlyResourceID %>',
                        'GetMultimedia',
                        function () {
                            SW.AssetInventory.Resources.hideResourceIfNoRows('#ResourceContent<%= ScriptFriendlyResourceID %>', '[ResourceID=<%= ScriptFriendlyResourceID %>]');
                        },
                        <%= Node.NodeID %>
                    );
                }
                SW.Core.View.AddOnRefresh(refresh, 'ResourceContent<%= ScriptFriendlyResourceID %>');
                refresh();
            });
        </script>
    </Content>
</orion:resourcewrapper>
