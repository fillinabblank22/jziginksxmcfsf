﻿using System;
using SolarWinds.AssetInventory.Web;
using SolarWinds.AssetInventory.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_AssetInventory_Resources_NodeDetails_NetworkInterfaces : AssetInventoryNodeResourceBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        Wrapper.HeaderButtons.Controls.Add(SearchControl);
        this.Wrapper.Visible = WebHelper.IsAnyRecordsAvalible(Node.NodeID, "Orion.AssetInventory.NetworkInterface");
    }

    protected ResourceSearchControl SearchControl { get; private set; }

    public override string IconPath
    {
        get { return "/Orion/AssetInventory/images/Resources/Network_Interfaces_icon16x16v2.png"; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSAMAGAINetworkInterfaces"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.AssetInventoryWebContent.WEBCODE_NetworkInterfacesResourceTitle; }
    }
}