﻿using SolarWinds.AssetInventory.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_AssetInventory_Resources_NodeDetails_GraphicsAndAudio : AssetInventoryNodeResourceBase
{
    public override string IconPath
    {
        get { return "/Orion/AssetInventory/images/Resources/Graphics_and_Audio_icon16x16.png"; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSAMAGAIGraphicsandAudio"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.AssetInventoryWebContent.WEBCODE_GraphicsAndAudio; }
    }
}