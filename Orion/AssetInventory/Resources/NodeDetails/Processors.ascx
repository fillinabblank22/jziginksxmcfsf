﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Processors.ascx.cs" Inherits="Orion_AssetInventory_Resources_NodeDetails_Processors" %>

<asp:scriptmanagerproxy runat="server">
    <Services>
        <asp:ServiceReference path="/Orion/Services/Information.asmx" />
        <asp:ServiceReference path="/Orion/AssetInventory/Services/AssetInventoryResources.asmx" />
    </Services>
</asp:scriptmanagerproxy>

<orion:resourcewrapper runat="server" id="Wrapper" CssClass="AssetInventoryResource">
    <Content>
        <!-- This is Underscore Javascript Template. See https://cp.solarwinds.com/display/OrionPlatformRD/Underscore+JavaScript+Templates -->
        <script id="ResourceTemplate<%= ScriptFriendlyResourceID %>" type="text/x-template">
        {#
            function ValidateNumber(number) {
                return (Number(number) === 0)
                    ? '<%= Resources.AssetInventoryWebContent.AIWEBDATA_JT0_4 %>'
                    : number;
            }
        #}
            <table class="AssetInventoryResourceTable NeedsZebraStripes">
                <tr class="HeaderRow">
                    <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_32 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_33 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_35 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_36 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_63 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_37 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_38 %></td>
                </tr>
                {# _.each(objects, function (current) { #}
                <tr>
                    <td>{{current.ProcessorName}}</td>
                    <td>{{current.Manufacturer}}</td>
                    <td><%= String.Format("{0} GHz", "{{current.SpeedGHzString}}")%></td>
                    <td>{{ValidateNumber(current.CoreNumber)}}</td>
                    <td>{{ValidateNumber(current.ThreadsNumber)}}</td>
                    <td>{{current.Model}}</td>
                    <td>{{current.Stepping}}</td>
                </tr>
                {# }); #}
            </table>
            <span class="aiCountLabel"><%= String.Format(Resources.AssetInventoryWebContent.AIWEBDATA_VB1_39, "{{objects.length}}") %></span>
        </script>
        <div id="ResourceContent<%= ScriptFriendlyResourceID %>"></div>
        <script type="text/javascript">
            $(function () {
                function refresh() {
                    SW.AssetInventory.Resources.displayTemplateFromWebServiceCallByObjectListCallback(
                        'ResourceTemplate<%= ScriptFriendlyResourceID %>',
                        'ResourceContent<%= ScriptFriendlyResourceID %>',
                        'GetProcessors',
                        function () {
                            SW.AssetInventory.Resources.hideResourceIfNoRows('#ResourceContent<%= ScriptFriendlyResourceID %>', '[ResourceID=<%= ScriptFriendlyResourceID %>]');
                        },
                        <%= Node.NodeID %>
                    );
                }

                SW.Core.View.AddOnRefresh(refresh, 'ResourceContent<%= ScriptFriendlyResourceID %>');
                refresh();
            });
        </script>
    </Content>
</orion:resourcewrapper>
