﻿using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.AssetInventory.Web;
using SolarWinds.AssetInventory.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_AssetInventory_Resources_NodeDetails_Drivers : AssetInventoryNodeResourceBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!WebHelper.IsAnyRecordsAvalible(Node.NodeID, "Orion.AssetInventory.Driver"))
        {
            Wrapper.Visible = false;
            return;
        }

        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        Wrapper.HeaderButtons.Controls.Add(SearchControl);

        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        CustomTable.SWQL = SWQL;
        CustomTable.SearchSWQL = SearchSWQL;
    }

    protected ResourceSearchControl SearchControl { get; private set; }

    public string SWQL
    {
        get
        {
            return string.Format(@"
SELECT Device AS [{2}], Name AS [{1}], Publisher AS [{4}], Version AS [{3}], DriverDate AS [{5}]
FROM Orion.AssetInventory.Driver
WHERE NOT (Name = '' AND Device = '' AND Publisher = '' AND Version = '') AND
	    NOT (Name = '' AND Device = '' AND Publisher = '' AND Version <> '') AND 
        NodeID = {0}", Node.NodeID,
                Resources.AssetInventoryWebContent.AIWEBCODE_MD0_7, Resources.AssetInventoryWebContent.AIWEBCODE_MD0_8,
                Resources.AssetInventoryWebContent.AIWEBCODE_MD0_10, Resources.AssetInventoryWebContent.AIWEBCODE_MD0_9,
                Resources.AssetInventoryWebContent.AIWEBCODE_MD0_11);
        }
    }

    public string SearchSWQL
    {
        get
        {
            return string.Format(@"
SELECT Device AS [{2}], Name AS [{1}], Publisher AS [{4}], Version AS [{3}], DriverDate AS [{5}]
FROM Orion.AssetInventory.Driver
WHERE  NOT (Name = '' AND Device = '' AND Publisher = '' AND Version = '') AND
	    NOT (Name = '' AND Device = '' AND Publisher = '' AND Version <> '') AND 
        NodeID = {0} AND ([{2}] LIKE '%${{SEARCH_STRING}}%' OR 
[{1}] LIKE '%${{SEARCH_STRING}}%' OR [{4}] LIKE '%${{SEARCH_STRING}}%')", Node.NodeID,
        Resources.AssetInventoryWebContent.AIWEBCODE_MD0_7, Resources.AssetInventoryWebContent.AIWEBCODE_MD0_8,
        Resources.AssetInventoryWebContent.AIWEBCODE_MD0_10, Resources.AssetInventoryWebContent.AIWEBCODE_MD0_9,
        Resources.AssetInventoryWebContent.AIWEBCODE_MD0_11);
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSAMAGAIDrivers"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.AssetInventoryWebContent.WEBCODE_DriversResourceTitle; }
    }

    public override string EditControlLocation
    {
        get
        {
            return @"/Orion/AssetInventory/Controls/EditPagableResourceControl.ascx";
        }
    }

    public override string IconPath
    {
        get { return "/Orion/AssetInventory/images/Resources/Drivers_icon16x16.png"; }
    }
}