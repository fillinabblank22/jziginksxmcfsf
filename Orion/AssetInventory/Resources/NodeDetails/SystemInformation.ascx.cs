﻿using System;
using SolarWinds.AssetInventory.Common;
using SolarWinds.AssetInventory.Web;
using SolarWinds.AssetInventory.Web.DAL;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.Pendo;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_AssetInventory_Resources_NodeDetails_SystemInformation : AssetInventoryNodeResourceBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var isNodeCompatibleWithScm =
            Node.IsServer
            && (Node.MachineType.StrMatch("linux") || Node.Vendor.StrMatch("linux") || Node.Vendor.StrMatch("windows"))
            && Node.MachineType.ToLowerInvariant() != "raspbian" && Node.MachineType.ToLowerInvariant() != "ibm powerpc";

        if (!isNodeCompatibleWithScm)
        {
            return;
        }

        var lastCollectionTime = new SystemInformationDAL().GetSystemInformation(Node.NodeID).LastCollectionTime;
        if (lastCollectionTime > DateTime.MinValue)
        {            
			this.DecorateWrapper(PendoTriggerConstants.AssetInventoryServer, this.Wrapper); 
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSAMAGAISysInfo"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.AssetInventoryWebContent.WEBCODE_SystemInformationResourceTitle; }
    }

    public override string IconPath
    {
        get { return "/Orion/AssetInventory/images/Resources/system_info_icon16x16.png"; }
    }

    protected int WarrantyExpirationWarningThreshold
    {
        get
        {
            return Math.Max((int)(SettingsDAL.GetSetting(AssetInventoryConstants.WarningThresholdDays).SettingValue),
                            (int)(SettingsDAL.GetSetting(AssetInventoryConstants.CriticalThresholdDays).SettingValue));
        }
    }
}