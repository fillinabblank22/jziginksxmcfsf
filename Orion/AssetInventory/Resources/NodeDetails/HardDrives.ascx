﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HardDrives.ascx.cs" Inherits="Orion_AssetInventory_Resources_NodeDetails_HardDrivers" %>

<asp:ScriptManagerProxy runat="server">
    <Services>
        <asp:ServiceReference Path="/Orion/Services/Information.asmx" />
        <asp:ServiceReference Path="/Orion/AssetInventory/Services/AssetInventoryResources.asmx" />
    </Services>
</asp:ScriptManagerProxy>

<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="AssetInventoryResource">
    <Content>
        <!-- This is Underscore Javascript Template. See https://cp.solarwinds.com/display/OrionPlatformRD/Underscore+JavaScript+Templates -->
        <script id="ResourceTemplate<%= ScriptFriendlyResourceID %>" type="text/x-template">
            <table class="AssetInventoryResourceTable NeedsZebraStripes" id="HardDrives<%= ScriptFriendlyResourceID %>">
                <tr class="HeaderRow">
                    <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_MS0_1 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_MS0_2 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_MS0_3 %></td>
                </tr>
            {# _.each(objects, function (current) { #}
                <tr>
                    <td>{{current.Model}}</td>
                    <td class="BreakableText">{{current.SerialNumber}}</td>
                    <td>{{current.SizeDisplayString}}</td>
                </tr>
            {# }); #}
            </table>
        </script>
        <div id="ResourceContent<%= ScriptFriendlyResourceID %>">
        </div>

        <script type="text/javascript">
            $(function () {
                function refresh() {
                    SW.AssetInventory.Resources.displayTemplateFromWebServiceCallByObjectListCallback(
                        'ResourceTemplate<%= ScriptFriendlyResourceID %>',
                        'ResourceContent<%= ScriptFriendlyResourceID %>',
                        'GetHardDrives',
                        function () {
                            SW.AssetInventory.Resources.hideResourceIfNoRows('#HardDrives<%= ScriptFriendlyResourceID %>', '[ResourceID=<%= ScriptFriendlyResourceID %>]');
                        },
                        <%= Node.NodeID %>
                    );
                }

                SW.Core.View.AddOnRefresh(refresh, 'ResourceContent<%= ScriptFriendlyResourceID %>');
                refresh();
            });
        </script>
    </Content>
</orion:resourceWrapper>
