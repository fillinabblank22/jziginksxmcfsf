﻿using System;
using SolarWinds.AssetInventory.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_AssetInventory_Resources_NodeDetails_Ports : AssetInventoryNodeResourceBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSAMAGAIPortsUSB"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.AssetInventoryWebContent.WEBCODE_PortsResourceTitle; }
    }

    public override string IconPath
    {
        get
        {
            return @"/Orion/AssetInventory/images/Resources/Ports_icon16x16v2.png";
        }
    }
}