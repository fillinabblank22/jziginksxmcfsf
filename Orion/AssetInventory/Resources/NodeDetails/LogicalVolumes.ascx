﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LogicalVolumes.ascx.cs" Inherits="Orion_AssetInventory_Resources_NodeDetails_LogicalVolumes" %>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>

<asp:ScriptManagerProxy runat="server">
    <services>
        <asp:ServiceReference path="/Orion/Services/Information.asmx" />
        <asp:ServiceReference path="/Orion/AssetInventory/Services/AssetInventoryResources.asmx" />
    </services>
</asp:ScriptManagerProxy>

<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="AssetInventoryResource">
    <Content>
        <script id="ResourceTemplate<%= ScriptFriendlyResourceID %>" type="text/x-template">
            {#
            var diskSpaceError = <%= 100 - Thresholds.DiskSpaceError.SettingValue %>;
            var diskSpaceWarning = <%= 100 - Thresholds.DiskSpaceWarning.SettingValue %>;

            function getVolumeProgressClass(volumeId, availableSpace) {
                if (volumeId == 0) {
                    return 'LogicalVolumePercentValueDefault';
                }
                if (availableSpace <= diskSpaceError) {
                     return 'LogicalVolumePercentValueCritical';
                }
                if (availableSpace <= diskSpaceWarning) {
                    return 'LogicalVolumePercentValueWarning';
                }
                return 'LogicalVolumePercentValueNonCritical';
            }

            function getVolumeProgressFontClass(volumeId, availableSpace) {
                if (volumeId == 0) {
                    return 'aiLogicalVolumesNormalFont';
                }
                if (availableSpace <= diskSpaceError) {
                     return 'aiLogicalVolumesCriticalFont';
                }
                if (availableSpace <= diskSpaceWarning) {
                    return 'aiLogicalVolumesWarningFont';
                }
                return 'aiLogicalVolumesNormalFont';
             }

            function getVolumeName(current) {
                var name = current.DeviceID;
                if ((current.SerialNumber != '') && (current.VolumeName != '')) {
                    name += ' <%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_24 %>:';
                }
                if ((current.VolumeName != '')) {
                    name += ' ' + current.VolumeName;
                }
                if ((current.SerialNumber != '') && (current.SerialNumber != 0) && (current.SerialNumber != null)) {
                    name += ' ' + current.SerialNumber;
                }
                return name;
            }
            #}
            <table class="AssetInventoryResourceTable NeedsZebraStripes">
                <tr class="HeaderRow">
                    <td class="ReportHeader" colspan="2" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_9 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_10 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_11 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_12 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_13 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_14 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_16 %></td>
                    <td class="ReportHeader" colspan="2" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_17 %></td>
                </tr>
                 {# _.each(objects, function(current, index) { #}
                <tr>
                    <td><img src="/Orion/AssetInventory/images/LogicalVolumes/{{current.TypeName}}.gif"/></td>
                    <td class="BreakableText">
                        {# if (current.VolumeID != 0) { #}
                             <a href="/Orion/NetPerfMon/VolumeDetails.aspx?NetObject=V:{{current.VolumeID}}">
                                 {{getVolumeName(current)}}</a>
                        {# } else { #}
                             {{getVolumeName(current)}}
                        {# } #}
                    </td>
                    <td>{{current.Type}}</td>
                    <td>{{current.FileSystem}}</td>
                    <td>{{current.SerialNumber || ''}}</td>
                    <td>{{current.SizeString}}</td>
                    <td>{{current.SpaceUsedString}}</td>
                    <td>{{current.AvailableSpaceString}}</td>
                    <td><span class="{{getVolumeProgressFontClass(current.VolumeID, current.AvailableSpacePercent)}}"> <%= String.Format(Resources.AssetInventoryWebContent.AIWEBDATA_YP0_1, "{{current.AvailableSpacePercentString}}") %></td>
                    <td width="50px">
                        <div class="LogicalVolumePercentBackground">
                            <div style="width: {{current.AvailableSpacePercent}}%" class="{{getVolumeProgressClass(current.VolumeID, current.AvailableSpacePercent)}}"/>
                        </div>
                    </td>
                </tr>
                 {# }); #}
            </table>
        </script>
        <div id="ResourceContent<%= ScriptFriendlyResourceID %>"></div>
        <script type="text/javascript">
            $(function () {
                function refresh() {
                    SW.AssetInventory.Resources.displayTemplateFromWebServiceCallByObjectListCallback(
                        'ResourceTemplate<%= ScriptFriendlyResourceID %>',
                        'ResourceContent<%= ScriptFriendlyResourceID %>',
                        'GetLogicalDrives',
                        function () {
                            if ($('#ResourceContent<%= ScriptFriendlyResourceID %>').find('tr').length === 1) {
                                $('[ResourceID=<%= ScriptFriendlyResourceID %>]').hide();
                            }
                        },
                        <%= Node.NodeID %>
                    );
                }

                SW.Core.View.AddOnRefresh(refresh, 'ResourceContent<%= ScriptFriendlyResourceID %>');
                refresh();
            });
        </script>
    </Content>
</orion:resourceWrapper>
