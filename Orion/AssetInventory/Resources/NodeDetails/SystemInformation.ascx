﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SystemInformation.ascx.cs" Inherits="Orion_AssetInventory_Resources_NodeDetails_SystemInformation" %>
<%@ Register TagPrefix="orion" TagName="ManagementTasks" Src="~/Orion/NetPerfMon/Resources/NodeDetails/ManagementTasks.ascx" %>
<%@ Register TagPrefix="orion" TagName="OrionManagementPlugin" Src="~/Orion/Controls/OrionManagementPlugin.ascx" %>

<orion:OrionManagementPlugin ID="OrionManagementPlugin1" runat="server" />

<asp:ScriptManagerProxy runat="server">
    <services>
        <asp:ServiceReference path="/Orion/Services/Information.asmx" />
        <asp:ServiceReference path="/Orion/AssetInventory/Services/AssetInventoryResources.asmx" />
    </services>
</asp:ScriptManagerProxy>

<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="AssetInventoryResource"> 
    <Content>
        <!-- This is Underscore Javascript Template. See https://cp.solarwinds.com/display/OrionPlatformRD/Underscore+JavaScript+Templates -->
        <script id="ResourceTemplate<%= ScriptFriendlyResourceID %>" type="text/x-template">
            {# var hideRow = SW.AssetInventory.Resources.hideRow; #}
            <table class="AssetInventoryDataGrid NeedsZebraStripes">
                <tbody>
                    <tr>
                        <td class="ReportHeader" colspan="2"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_1 %></td>
                    </tr>
                    <tr>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_2 %></td>
                        <td>{{LastCollectionTimeString}}
                            <% if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer && Profile.AllowNodeManagement) { %>
                            <a id="pollNowLink<%= ScriptFriendlyResourceID %>" href="javascript:void(0);">
                                <span class="NodeManagementIcons">
                                    <img src="/Orion/images/pollnow_16x16.gif" style="padding-right: 5px;" />
                                    <%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_3 %>
                                </span>
                            </a>
                            <% } %>
                        </td>
                    </tr>
                    <tr{{hideRow(SystemName)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_4 %></td>
                        <td><a href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{{NodeID}}">
                            <img alt="" src="/Orion/images/StatusIcons/Small-{{StatusImage === null ? 'EmptyIcon.gif' : StatusImage}}" />&nbsp;{{SystemName}}</a></td>
                    </tr>
                    <tr{{hideRow(HostName)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_5 %></td>
                        <td>{{HostName}}</td>
                    </tr>
                    <tr{{hideRow(Domain)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_6 %></td>
                        <td>{{Domain}}</td>
                    </tr>
                    <tr{{hideRow(DNSName)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_7 %></td>
                        <td>{{DNSName}}</td>
                    </tr>
                    <tr{{hideRow(DomainRole)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_8 %></td>
                        <td>{{DomainRole}}</td>
                    </tr>
                    <tr{{hideRow(IPAddress)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_9 %></td>
                        <td>{{IPAddress}}</td>
                    </tr>
                    <tr{{hideRow(DynamicIP)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_10 %></td>
                        <td>{{DynamicIP}}</td>
                    </tr>
                </tbody>
                <tbody>
                    <tr>
                        <td class="ReportHeader" colspan="2"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_11 %></td>
                    </tr>
                    <tr{{hideRow(AssetType)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_12 %></td>
                        <td><img src="/Orion/AssetInventory/images/device_16x16.gif"/>&nbsp;{{AssetType}}</td>
                    </tr>
                    <tr{{hideRow(Hardware)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_13 %></td>
                        <td>{{Hardware}}</td>
                    </tr>
                    <tr{{hideRow(Manufacturer)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_14 %></td>
                        <td>{{Manufacturer}}</td>
                    </tr>
                    <tr{{hideRow(Model)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_15 %></td>
                        <td>{{Model}}</td>
                    </tr>
                    <tr{{hideRow(HardwareSerialNumber)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_16 %></td>
                        <td>{{HardwareSerialNumber}}</td>
                    </tr>
                    <tr>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_17 %></td>
                        <td>
                            {# if (WarrantyDateString == '') { #}
                            <%= Resources.AssetInventoryWebContent.AIWEBDATA_JT0_4 %>
                            {# } else { #}
                                {# if (WarrantyExpirationInDays > <%= WarrantyExpirationWarningThreshold %>) { #}
                                <img class="AssetInventoryWarrantyIcon" src="/Orion/AssetInventory/images/WarrantyStatus/warranty_ok.gif" />
                                <%= Resources.AssetInventoryWebContent.AIWEBDATA_JT0_1 %>
                                {# } else if (WarrantyExpirationInDays >= 0) { #}
                                <img class="AssetInventoryWarrantyIcon" src="/Orion/AssetInventory/images/WarrantyStatus/warranty_warning.gif" />
                                <%= Resources.AssetInventoryWebContent.AIWEBDATA_JT0_1 %>
                                {# } else { #}
                                <img class="AssetInventoryWarrantyIcon" src="/Orion/AssetInventory/images/WarrantyStatus/warranty_expired.gif" />
                                <%= Resources.AssetInventoryWebContent.AIWEBDATA_JT0_2 %>
                                {# } #}
                                {{WarrantyDateString}}
                                <% if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                                   { %>
                                {# if (WarrantyDetailsUrl != null && WarrantyDetailsUrl != '') { #}
                                <a href="{{WarrantyDetailsUrl}}" class="AssetInventoryWarrantyLink" target="_blank">&raquo; <%= Resources.AssetInventoryWebContent.AIWEBDATA_JT0_3 %></a>
                                {# } #}
                                <% } %>
                            {# } #}
                        </td>
                    </tr>
                    <tr{{hideRow(Location)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_18 %></td>
                        <td>{{Location}}</td>
                    </tr>
                    <tr{{hideRow(DeviceTimeZone)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_19 %></td>
                        <td>{{DeviceTimeZone}}</td>
                    </tr>
                    <tr{{hideRow(Contact)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_20 %></td>
                        <td>{{Contact}}</td>
                    </tr>
                </tbody>
                <tbody>
                    <tr>
                        <td class="ReportHeader" colspan="2"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_21 %></td>
                    </tr>
                    <tr{{hideRow(OperatingSystem)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_21 %></td>
                        <td>
                            <img src="{{OSImage === null ? '//:0' : OSImage}}" style="padding-right: 5px;" />{{OperatingSystem}}</td>
                    </tr>
                    <tr{{hideRow(OSVersion)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_22 %></td>
                        <td>{{OSVersion}}</td>
                    </tr>
                    <tr{{hideRow(OSArchitecture)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_23 %></td>
                        <td>{{OSArchitecture}}</td>
                    </tr>
                    <tr{{hideRow(OSLanguage)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_24 %></td>
                        <td>{{OSLanguage}}</td>
                    </tr>
                    <tr{{hideRow((ServicePack == null || ServicePack == '0') ? null : ServicePack)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_25 %></td>
                        <td>{{ServicePack != '0' ? ServicePack : '<%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_27 %>'}}</td>
                    </tr>
                    <tr{{hideRow(LastBoot)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_26 %></td>
                        <td>{{LastBootString}}</td>
                    </tr>
                    <tr{{hideRow(LastLoggedInUser)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_27 %></td>
                        <td>{{LastLoggedInUser}}</td>
                    </tr>
                    <tr{{hideRow(AntivirusStatus)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_28 %></td>
                        <td><img src="{{AntivirusStatusImage === null ? '//:0' : AntivirusStatusImage}}" />&nbsp;&nbsp;{{AntivirusStatus}}</td>
                    </tr>
                    <tr{{hideRow(FirewallStatus)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_29 %></td>
                        <td>
                            <img src="{{FirewallStatusImage === null ? '//:0' : FirewallStatusImage}}" />&nbsp;&nbsp;{{FirewallStatus}}</td>
                    </tr>
                    <tr{{hideRow(Hypervisor)}}> 
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_30 %></td>
                        <td>
                            <img src="{{HypervisorImage === null ? '//:0' : HypervisorImage}}" style="padding-right: 5px;" />{{Hypervisor}}
                        </td>
                    </tr>
                    <tr{{hideRow(HardwareAgent)}}>
                        <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_31 %></td>
                        <td>
                        {# if (HardwareAgentURL === null || HardwareAgentURL === '') { #}
                            {{HardwareAgent}}
                        {# } else { #}
                            <a href="{{String.format(HardwareAgentURL, IPAddress)}}" class="AssetInventoryWarrantyLink"
                            <% if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                               { %>
                             onclick="demoAction('AI_SystemInformation_HardwareAgent', this); return false;"
                            <% }%>
                            target="_blank">{{HardwareAgent}}</a>
                        {# } #}
                        </td>
                    </tr>
                </tbody>
            </table>
        </script>
        <div id="ResourceContent<%= ScriptFriendlyResourceID %>"></div>
        <script type="text/javascript">
            $(function () {
                function refresh() {
                    SW.AssetInventory.Resources.displayTemplateFromWebServiceCallByObject(
                        'ResourceTemplate<%= ScriptFriendlyResourceID %>',
                        'ResourceContent<%= ScriptFriendlyResourceID %>',
                        'GetServerInformation',
                        <%= Node.NodeID %>
                    );
                }

                SW.Core.View.AddOnRefresh(refresh, 'ResourceContent<%= ScriptFriendlyResourceID %>');
                refresh();

                // JS events in underscore templates do not work when Active Scripting is disabled in Internet zone. This solves the issue. See FB230000 for reference.
                $(document).on('click', '#pollNowLink<%= ScriptFriendlyResourceID %>', function () {
                    return showPollNowDialog(['N:<%= Node.NodeID %>']);
                });
            });
        </script>
    </Content>
</orion:resourceWrapper>
