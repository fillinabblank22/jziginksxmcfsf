﻿using System;
using SolarWinds.AssetInventory.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_AssetInventory_Resources_NodeDetails_Processors : AssetInventoryNodeResourceBase
{
    public override string IconPath
    {
        get { return "/Orion/AssetInventory/images/Resources/Processors_icon16x16.png"; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSAMAGAIProcessors"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.AssetInventoryWebContent.WEBCODE_ProcessorsTitle; }
    }
}