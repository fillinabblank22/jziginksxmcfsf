﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HostedVMs.ascx.cs" Inherits="Orion_AssetInventory_Resources_NodeDetails_HostedVMs" %>

<asp:scriptmanagerproxy runat="server">
    <Services>
        <asp:ServiceReference path="/Orion/Services/Information.asmx" />
        <asp:ServiceReference path="/Orion/AssetInventory/Services/AssetInventoryResources.asmx" />
    </Services>
</asp:scriptmanagerproxy>

<orion:resourcewrapper runat="server" id="Wrapper" cssclass="AssetInventoryResource">
    <Content>
        <asp:PlaceHolder ID="PlaceholderManageNodeDialog1" runat="server" ></asp:PlaceHolder>
        <!-- This is Underscore Javascript Template. See https://cp.solarwinds.com/display/OrionPlatformRD/Underscore+JavaScript+Templates -->
        <script id="ResourceTemplate<%= ScriptFriendlyResourceID %>" type="text/x-template">
            <table class="AssetInventoryResourceTable NeedsZebraStripes">
                <tr class="HeaderRow">
                    <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_55 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_21 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;" colspan="2"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_56 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_57 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_58 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_59 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_9 %></td>
                </tr>
                {# _.each(objects, function(current, index)  { #}
                <tr>
                    <td>
                    {# if(current.VMNodeID != 0) { #}
                        <img style="vertical-align: bottom;" src="/Orion/images/StatusIcons/Small-{{current.VMNodeStateImage}}"/>
                        <a href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{{current.VMNodeID}}"> {{current.Name}}</a>
                    {# } else { #}
                        <img src="/Orion/images/StatusIcons/Small-{{current.VMNodeStateImage}}" style="vertical-align: bottom;"/>
                            <% if (Profile.AllowNodeManagement) { %>
                                <a href="javascript:void(0)"
                                    class="aiManageVMLink<%= ScriptFriendlyResourceID %>"
                                    aiEngineId="{{current.NodeEngineID}}"
                                    aiIP="{{current.IPAddress || current.Name}}"
                                    aiNodeID="{{current.VMNodeID}}"
                                    aiHostPlatformID="{{current.HostPlatformID}}">
                                {{current.Name}}</a>
                            <% } else { %>
                                {{current.Name}}
                            <% } %>
                    {# } #}
                    </td>
                    <td>{{current.OSName || ''}}</td>
                    <td><img src="{{current.StateImage}}"/></td>
                    <td>{{current.State}}</td>
                    <td>{{current.MemorySizeString}}</td>
                    <td>{{current.CPUCount || '---'}}</td>
                    <td><%= String.Format(Resources.AssetInventoryWebContent.AIWEBDATA_VB1_60, "{{current.CPUSharesString}}") %></td>
                    <td>{{current.IPAddress || ''}}</td>
                </tr>
                {# }); #}
            </table>
        </script>
        <div id="ResourceContent<%= ScriptFriendlyResourceID %>"></div>
        <script type="text/javascript">
            $(function () {
                function refresh() {
                    SW.AssetInventory.Resources.displayTemplateFromWebServiceCallByObjectListCallback(
                        'ResourceTemplate<%= ScriptFriendlyResourceID %>',
                        'ResourceContent<%= ScriptFriendlyResourceID %>',
                        'GetHostedVMs',
                        function () {
                            SW.AssetInventory.Resources.hideResourceIfNoRows('#ResourceContent<%= ScriptFriendlyResourceID %>', '[ResourceID=<%= ScriptFriendlyResourceID %>]');
                        },
                        <%= Node.NodeID %>
                    );
                }

                SW.Core.View.AddOnRefresh(refresh, 'ResourceContent<%= ScriptFriendlyResourceID %>');
                refresh();

                // JS events in underscore templates do not work when Active Scripting is disabled in Internet zone. This solves the issue. See FB230000 for reference.
                $(document).on('click', '.aiManageVMLink<%= ScriptFriendlyResourceID %>', function () {
                    var $el = $(this);
                    ManageNodeDialog(
                        $el.attr('aiEngineId'),
                        $el.attr('aiIP'),
                        'G' + $el.attr('aiNodeID'),
                        false,
                        $el.attr('aiHostPlatformID')
                    );
                    return false;
                });
            });
        </script>
    </Content>
</orion:resourcewrapper>
