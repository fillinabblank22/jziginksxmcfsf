﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RemovableMedia.ascx.cs" Inherits="Orion_AssetInventory_Resources_NodeDetails_RemovableMedia" %>

<asp:ScriptManagerProxy runat="server" >
    <Services>
        <asp:ServiceReference path="/Orion/Services/Information.asmx" />
        <asp:ServiceReference path="/Orion/AssetInventory/Services/AssetInventoryResources.asmx" />
    </Services>
</asp:ScriptManagerProxy>

<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="AssetInventoryResource">
    <Content>
         <script id="ResourceTemplate<%= ScriptFriendlyResourceID %>" type="text/x-template">

            <table class="AssetInventoryResourceTable NeedsZebraStripes">
                <tr class="HeaderRow">
                    <td class="ReportHeader" style="text-transform: uppercase;" colspan="2"><%=Resources.AssetInventoryWebContent.AIWEBCODE_MD0_15 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBCODE_MD0_16 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBCODE_MD0_17 %></td>
                </tr>
                {# _.each(objects, function(current, index) { #}
                <tr>
                    <td width="1%"><img src="/Orion/AssetInventory/images/LogicalVolumes/{{current.MediaType}}.gif"/></td>
                    <td>
                        {# if(current.VolumeID != 0){ #}
                             <a href="/Orion/NetPerfMon/VolumeDetails.aspx?NetObject=V:{{current.VolumeID}}">{{current.Name}}</a>
                        {# }else{ #}
                             {{current.Name}}
                        {# } #}
                    </td>
                    <td>{{current.Type}}</td>
                    <td>{{current.Manufacturer}}</td>
                </tr>
                {# }); #}
            </table>
        </script>
        <div id="ResourceContent<%= ScriptFriendlyResourceID %>"></div>
        <script type="text/javascript">
            $(function () {
                function refresh() {
                    SW.AssetInventory.Resources.displayTemplateFromWebServiceCallByObjectListCallback(
                        'ResourceTemplate<%= ScriptFriendlyResourceID %>',
                        'ResourceContent<%= ScriptFriendlyResourceID %>',
                        'GetRemovableMedia',
                        function () {
                            SW.AssetInventory.Resources.hideResourceIfNoRows('#ResourceContent<%= ScriptFriendlyResourceID %>', '[ResourceID=<%= ScriptFriendlyResourceID %>]');
                        },
                        <%= Node.NodeID %>
                    );
                }

                SW.Core.View.AddOnRefresh(refresh, 'ResourceContent<%= ScriptFriendlyResourceID %>');
                refresh();
            });
        </script>
    </Content>
</orion:resourceWrapper>
