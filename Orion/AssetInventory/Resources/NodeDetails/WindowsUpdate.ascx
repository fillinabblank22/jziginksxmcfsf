﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WindowsUpdate.ascx.cs" Inherits="Orion_AssetInventory_Resources_NodeDetails_WindowsUpdate" %>
<%@ Register TagPrefix="orion" TagName="ManagementTasks" Src="~/Orion/NetPerfMon/Resources/NodeDetails/ManagementTasks.ascx" %>
<%@ Register TagPrefix="orion" TagName="OrionManagementPlugin" Src="~/Orion/Controls/OrionManagementPlugin.ascx" %>

<asp:ScriptManagerProxy runat="server">
    <services>
        <asp:ServiceReference path="/Orion/Services/Information.asmx" />
        <asp:ServiceReference path="/Orion/AssetInventory/Services/AssetInventoryResources.asmx" />
    </services>
</asp:ScriptManagerProxy>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <!-- This is Underscore Javascript Template. See https://cp.solarwinds.com/display/OrionPlatformRD/Underscore+JavaScript+Templates -->
        <script id="ResourceTemplate<%= ScriptFriendlyResourceID %>" type="text/x-template">
            {# var hideRow = SW.AssetInventory.Resources.hideRow; #}
            <table class="AssetInventoryDataGrid NeedsZebraStripes">
                <tr{{hideRow(Status)}}>
                    <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_28 %></td>
                    <td>
            {# if (InstallMethod > 1) { #}
                <img src="/Orion/images/ok_16x16.gif"/> <%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_33 %>
            {# } else { #}
                <img src="/Orion/images/failed_16x16.gif"/> <%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_34 %>
            {# } #}
                    </td>
                </tr>
                <tr{{hideRow(AvailableUpdatesString)}}>
                    <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_29 %></td>
                    <td>{{AvailableUpdatesString}}</td>
                </tr>
                <tr{{hideRow(LastCheckString)}}>
                    <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_30 %></td>
                    <td>{{LastCheckString}}</td>
                </tr>
                <tr{{hideRow(LastInstalledString)}}>
                    <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_31 %></td>
                    <td>{{LastInstalledString}}</td>
                </tr>
                <tr{{hideRow(InstallMethodDescription)}}>
                    <td class="aiBold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_32 %></td>
                    <td>
                        {{InstallMethodDescription}}
                    </td>
                </tr>
            </table>
        </script>
        <div id="ResourceContent<%= ScriptFriendlyResourceID %>"></div>
        <script type="text/javascript">
            $(function () {
                function refresh() {
                    SW.AssetInventory.Resources.displayTemplateFromWebServiceCallByObject(
                        'ResourceTemplate<%= ScriptFriendlyResourceID %>',
                        'ResourceContent<%= ScriptFriendlyResourceID %>',
                        'GetWindowsUpdate',
                        <%= Node.NodeID %>
                    );
                }

                SW.Core.View.AddOnRefresh(refresh, 'ResourceContent<%= ScriptFriendlyResourceID %>');
                refresh();
            });
        </script>
    </Content>
</orion:resourceWrapper>
