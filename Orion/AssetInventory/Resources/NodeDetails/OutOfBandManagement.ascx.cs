﻿using System;
using SolarWinds.AssetInventory.Web;
using SolarWinds.AssetInventory.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_AssetInventory_Resources_NodeDetails_OutOfBandManagement : AssetInventoryNodeResourceBase
{
    /// <summary>
    /// Gets Out of Band Management title
    /// </summary>
    protected override string DefaultTitle
    {
        get { return Resources.AssetInventoryWebContent.WEBCODE_OutOfBandManagementTitle; }
    }

    /// <summary>
    /// Gets Out of Band Management icon path
    /// </summary>
    public override string IconPath
    {
        get { return "/Orion/AssetInventory/images/Resources/Out_of_Band_Management_icon16x16.png"; }
    }

    /// <summary>
    /// Gets Out of Band help link
    /// </summary>
    public override string HelpLinkFragment
    {
        get { return "OrionSAMAGAIOutofBand"; }
    }
}

