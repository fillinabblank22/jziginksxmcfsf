﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InstalledSoftware.ascx.cs" Inherits="Orion_AssetInventory_Resources_NodeDetails_InstalledSoftware" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="AssetInventoryResource">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable" />

        <script type="text/javascript">
            $(function () {
                var resourceId = Number('<%= ScriptFriendlyResourceID %>');

                SW.Core.Resources.CustomQuery.initialize({
                    uniqueId: resourceId,
                    initialPage: 0,
                    rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,
                    searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                    searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                    allowSort: true,
                    customFormatters: {
                        '<%= Resources.AssetInventoryWebContent.AIWEBCODE_MD0_4 %>': SW.AssetInventory.Resources.dateOnlyFormatter,
                        '<%= Resources.AssetInventoryWebContent.AIWEBCODE_MD0_2 %>': function (value) {
                            return (String(value) !== '0') ? value : ' ';
                        },
                    }
                });

                $('#<%= SearchControl.SearchBoxClientID %>').attr('placeholder', '<%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_5 %>');

                function refresh() {
                    SW.Core.Resources.CustomQuery.refresh(resourceId);
                }

                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });
        </script>
    </Content>
</orion:resourceWrapper>
