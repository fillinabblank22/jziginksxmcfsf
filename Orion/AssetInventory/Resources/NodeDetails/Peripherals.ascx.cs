﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.AssetInventory.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_AssetInventory_Resources_NodeDetails_Peripherals : AssetInventoryNodeResourceBase
{
    public override string IconPath
    {
        get { return "/Orion/AssetInventory/images/Resources/peripherals_icon16x16.png"; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSAMAGAIPeripherals"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.AssetInventoryWebContent.WEBCODE_PeripheralsTitle; }
    }
}