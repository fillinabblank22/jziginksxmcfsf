﻿using System;
using SolarWinds.AssetInventory.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_AssetInventory_Resources_NodeDetails_Memory : AssetInventoryNodeResourceBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSAMAGAIMemory"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.AssetInventoryWebContent.WEBCODE_MemoryResourceTitle; }
    }

    public override string IconPath
    {
        get
        {
            return @"/Orion/AssetInventory/images/Resources/Memory_Modules_icon16x16.png";
        }
    }
}