﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetworkInterfaces.ascx.cs" Inherits="Orion_AssetInventory_Resources_NodeDetails_NetworkInterfaces" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>

<asp:scriptmanagerproxy runat="server">
    <Services>
        <asp:ServiceReference path="/Orion/Services/Information.asmx" />
        <asp:ServiceReference path="/Orion/AssetInventory/Services/AssetInventoryResources.asmx" />
    </Services>
</asp:scriptmanagerproxy>

<orion:resourcewrapper runat="server" id="Wrapper" CssClass="AssetInventoryResource">
    <Content>
        <!-- This is Underscore Javascript Template. See https://cp.solarwinds.com/display/OrionPlatformRD/Underscore+JavaScript+Templates -->

        <script id="ResourceTemplate<%= ScriptFriendlyResourceID %>" type="text/x-template">
            <table class="AssetInventoryResourceTable NeedsZebraStripes">
                <tr class="HeaderRow">
                    <td class="ReportHeader" style="text-transform: uppercase;" colspan="3"><%= Resources.AssetInventoryWebContent.AIWEBCODE_MD0_7 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_14 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_40 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_9 %></td>
                </tr>
                {# _.each(objects, function (current, index) { #}
                <tr>
                    <td class="aiCollapse<%= ScriptFriendlyResourceID %>" aiIndex="{{index}}" style="cursor:pointer;visibility:{{current.HasAdditionalInfo === true ? 'visible' : 'hidden'}}"><img id="image<%= ScriptFriendlyResourceID %>_{{index}}" src="/NetPerfMon/images/Icon_Plus.gif"/></td>
                    <td><img id="interfaceImg<%= ScriptFriendlyResourceID %>_{{index}}" src="{{current.InterfaceTypeImage}}"/></td>
                    <td>
                    {# if (current.InterfaceID != 0) { #}
                         <a href="/Orion/Interfaces/InterfaceDetails.aspx?NetObject=I:{{current.InterfaceID}}">{{current.Name}}</a>
                    {# } else { #}
                         {{current.Name}}
                    {# } #}
                    </td>
                    <td>{{current.Manufacturer || ''}}</td>
                    <td>{{current.MacAddress || ''}}</td>
                    <td>{{current.IpAddress || ''}}</td>
                </tr>
                <tr></tr>
                <tr>
                    <td colspan="6" style="border-bottom: 0px; padding: 0px">
                        <table style="display: none" cellspacing="0" id="collapsableTable<%= ScriptFriendlyResourceID %>_{{index}}" width="100%">
                            <tr>
                                <td class="aiCollapsableTableHeader"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_41 %></td>
                                <td>{{current.Netmask || ''}}</td>
                            </tr>
                            <tr>
                                <td class="aiCollapsableTableHeader"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_42 %></td>
                                <td>{{current.Gateway || ''}}</td>
                            </tr>
                            <tr>
                                <td class="aiCollapsableTableHeader"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_43 %></td>
                                <td>{{current.DhcpServer || ''}}</td>
                            </tr>
                            {# _.each(current.DnsServers, function (dnsServer, dnsServerIndex) { #}
                            <tr>
                                <td class="aiCollapsableTableHeader"><%= String.Format(Resources.AssetInventoryWebContent.AIWEBDATA_VB1_44, "{{dnsServerIndex + 1}}") %></td>
                                <td>{{dnsServer}}</td>
                            </tr>
                            {# }); #}
                        </table>
                    </td>
                </tr>
                {# }); #}
            </table>
        </script>
        <div id="ResourceContent<%= ScriptFriendlyResourceID %>"></div>
        <script type="text/javascript">
            (function () {
                var $searchButton = $('#<%= SearchControl.SearchButtonClientID %>');
                var $searchTextBox = $('#<%= SearchControl.SearchBoxClientID %>');
                var searchText = $searchTextBox.val() || '';
                var isSearchEnabled = false;
                var paths = {
                    false: '/NetPerfMon/images/Icon_Minus.gif',
                    true: '/NetPerfMon/images/Icon_Plus.gif'
                }

                function showHidePanel(controlId, imgId) {
                    var $ctl = $('#' + controlId);
                    var $img = $('#' + imgId);
                    var path = paths[$ctl.is(':visible')];
                    $ctl.toggle();
                    $img.attr('src', path);
                }

                function searchInterfaces() {
                    SW.AssetInventory.Resources.displayTemplateFromWebServiceCallByObjectList(
                        'ResourceTemplate<%= ScriptFriendlyResourceID %>',
                        'ResourceContent<%= ScriptFriendlyResourceID %>',
                        'GetNetworkInterfaces',
                        <%= Node.NodeID %>,
                        searchText
                    );
                }

                function triggerSearchFunction() {
                    if (isSearchEnabled) {
                        $searchButton.attr('src', '/Orion/images/Button.SearchIcon.gif');
                        $searchTextBox.val('');
                        isSearchEnabled = false;
                    }
                    searchText = $searchTextBox.val();
                    if (searchText && searchText.length) {
                        // cancel search on click
                        $searchButton.attr('src', '/Orion/images/Button.SearchCancel.gif');
                        isSearchEnabled = true;
                    }
                    searchInterfaces();
                }

                $searchTextBox.keyup(function (e) {
                    if (e.keyCode === 13) {
                        triggerSearchFunction();
                        return false;
                    } else if (e.keyCode === 27) {
                        $searchTextBox.val('');
                        triggerSearchFunction();
                        return false;
                    }
                    return true;
                });

                $searchButton.click(function () {
                    triggerSearchFunction();
                    return false;
                });

                $(function () {
                    $searchTextBox.attr('placeholder', '<%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_54 %>');
                    SW.Core.View.AddOnRefresh(searchInterfaces, 'ResourceContent<%= ScriptFriendlyResourceID %>');
                    searchInterfaces();

                    // JS events in underscore templates do not work when Active Scripting is disabled in Internet zone. This solves the issue. See FB230000 for reference.
                    $(document).on('click', '.aiCollapse<%= ScriptFriendlyResourceID %>', function () {
                        var index = $(this).attr('aiIndex');
                        showHidePanel('collapsableTable<%= ScriptFriendlyResourceID %>_' + index, 'image<%= ScriptFriendlyResourceID %>_' + index);
                        return false;
                    });
                });
            }());
        </script>
    </Content>
</orion:resourcewrapper>
