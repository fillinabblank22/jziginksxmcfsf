﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StorageControllers.ascx.cs" Inherits="Orion_AssetInventory_Resources_NodeDetails_StorageControllers" %>

<asp:scriptmanagerproxy runat="server">
    <Services>
        <asp:ServiceReference path="/Orion/Services/Information.asmx" />
        <asp:ServiceReference path="/Orion/AssetInventory/Services/AssetInventoryResources.asmx" />
    </Services>
</asp:scriptmanagerproxy>

<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="AssetInventoryResource">
    <Content>
        <!-- This is Underscore Javascript Template. See https://cp.solarwinds.com/display/OrionPlatformRD/Underscore+JavaScript+Templates -->
        <script id="ResourceTemplate<%= ScriptFriendlyResourceID %>" type="text/x-template">
            <table class="AssetInventoryResourceTable NeedsZebraStripes" id="StorageController<%= ScriptFriendlyResourceID %>">
            <tr class="HeaderRow">
                <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_MS0_4 %></td>
                <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_MS0_5 %></td>
                <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_MS0_6 %></td>
                <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_MS0_7 %></td>
                <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_MS0_8 %></td>
            </tr>
             {# _.each(objects, function(current, index)  { #}
                <tr>
                    <td>{{current.Name == "" ? "<%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_25 %>" : current.Name }}</td>
                    <td>{{current.ModelNumber == "" ? "<%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_25 %>" : current.ModelNumber }}</td>
                    <td>{{current.SerialNumber == "" ? "<%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_25 %>" : current.SerialNumber }}</td>
                    <td>{{current.FirmwareVersion == "" ? "<%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_25 %>" : current.FirmwareVersion }}</td>
                    <td>{{current.DriverVersion == "" ? "<%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_25 %>" : current.DriverVersion }}</td>
                </tr>
                {# }); #}
            </table>
        </script>
        <div id="ResourceContent<%= ScriptFriendlyResourceID %>">
        </div>
        <script type="text/javascript">
            $(function () {
                function refresh() {
                    SW.AssetInventory.Resources.displayTemplateFromWebServiceCallByObjectListCallback(
                        'ResourceTemplate<%= ScriptFriendlyResourceID %>',
                        'ResourceContent<%= ScriptFriendlyResourceID %>',
                        'GetStorageControllers',
                        function () {
                            SW.AssetInventory.Resources.hideResourceIfNoRows('#StorageController<%= ScriptFriendlyResourceID %>', '[ResourceID=<%= ScriptFriendlyResourceID %>]');
                        },
                        <%= Node.NodeID %>
                    );
                }

                SW.Core.View.AddOnRefresh(refresh, 'ResourceContent<%= ScriptFriendlyResourceID %>');
                refresh();
            });
        </script>
    </Content>
</orion:resourceWrapper>
