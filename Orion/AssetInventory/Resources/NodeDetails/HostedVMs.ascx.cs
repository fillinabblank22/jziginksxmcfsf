﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.AssetInventory.Web;
using SolarWinds.AssetInventory.Web.DAL;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_AssetInventory_Resources_NodeDetails_HostedVMs : AssetInventoryNodeResourceBase
{
    protected override void OnLoad(EventArgs e)
    {
        Wrapper.Visible = new HostedVMDAL().IsNodeEsxHost(Node.NodeID);
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.CreateVimManageNodeDialog();
    }

    private void CreateVimManageNodeDialog()
    {
        if (!new HostedVMDAL().IsVimInstalled())
            return;

        var manageNodeDialog = LoadControl("~/Orion/VIM/Controls/ManageNodeDialog.ascx");
        PlaceholderManageNodeDialog1.Controls.Clear();
        PlaceholderManageNodeDialog1.Controls.Add(manageNodeDialog);
    }

    public override string IconPath
    {
        get { return "/Orion/AssetInventory/images/Resources/VM_icon16x16.png"; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSAMAGAIHostedVMs"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.AssetInventoryWebContent.WEBCODE_HostedVirtuakMachineResourceTitle; }
    }
}