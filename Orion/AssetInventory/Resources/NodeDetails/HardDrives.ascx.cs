﻿using SolarWinds.AssetInventory.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_AssetInventory_Resources_NodeDetails_HardDrivers : AssetInventoryNodeResourceBase
{
    /// <summary>
    /// Gets HardDrivers title
    /// </summary>
    protected override string DefaultTitle
    {
        get { return Resources.AssetInventoryWebContent.WEBCODE_HardDriversResourceTitle; }
    }

    /// <summary>
    /// Gets HardDrivers icon path
    /// </summary>
    public override string IconPath
    {
        get { return "/Orion/AssetInventory/images/Resources/Hard_Drives_icon16x16.png"; }
    }

    /// <summary>
    /// Gets HardDrivers help link
    /// </summary>
    public override string HelpLinkFragment
    {
        get { return "OrionSAMAGAIHardDrives"; }
    }
}