﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.AssetInventory.Web;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_AssetInventory_Resources_NodeDetails_CustomAssetInformation : AssetInventoryNodeResourceBase
{
    public override string IconPath
    {
        get { return "/Orion/AssetInventory/images/Resources/Custom_Asset_info_icon16x16.png"; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionSAMAGAICustomAssetInfo"; }
    }

    public override string EditControlLocation
    {
        get
        {
            return @"/Orion/AssetInventory/Controls/EditCustomAssetInformation.ascx";
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.AssetInventoryWebContent.WEBCODE_CustomAssetInformationResourceTitle; }
    }

    public bool ShowPopulatedData
    {
        get
        {
            return !this.Resource.Properties.ContainsKey("ShowPopulated") || Boolean.Parse(this.Resource.Properties["ShowPopulated"]);
        }
    }
}
