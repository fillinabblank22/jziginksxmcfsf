﻿using System;
using SolarWinds.AssetInventory.Web;
using SolarWinds.AssetInventory.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_AssetInventory_Resources_NodeDetails_Firmware : AssetInventoryNodeResourceBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!WebHelper.IsAnyRecordsAvalible(Node.NodeID, "Orion.AssetInventory.Firmware"))
        {
            Wrapper.Visible = false;
            return;
        }

        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        Wrapper.HeaderButtons.Controls.Add(SearchControl);

        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        CustomTable.SWQL = SWQL;
        CustomTable.SearchSWQL = SearchSWQL;
    }

    protected ResourceSearchControl SearchControl { get; private set; }

    /// <summary>
    /// Gets Firmware data
    /// </summary>
    public string SWQL
    {
        get
        {
            return string.Format(@"
SELECT Name AS [{1}], Publisher AS [{2}], Version AS [{3}], ReleaseDate AS [{4}]
FROM Orion.AssetInventory.Firmware WHERE NodeID = {0}", Node.NodeID,
                Resources.AssetInventoryWebContent.AIWEBCODE_MD0_18, Resources.AssetInventoryWebContent.AIWEBCODE_MD0_19,
                Resources.AssetInventoryWebContent.AIWEBCODE_MD0_20, Resources.AssetInventoryWebContent.AIWEBCODE_MD0_21);
        }
    }

    /// <summary>
    /// Gets Firmware data
    /// </summary>
    public string SearchSWQL
    {
        get
        {
            return string.Format(@"
SELECT Name AS [{1}], Publisher AS [{2}], Version AS [{3}], ReleaseDate AS [{4}]
FROM Orion.AssetInventory.Firmware 
WHERE NodeID = {0} AND ([{1}] LIKE '%${{SEARCH_STRING}}%' OR [{3}] LIKE '%${{SEARCH_STRING}}%')", Node.NodeID,
                Resources.AssetInventoryWebContent.AIWEBCODE_MD0_18, Resources.AssetInventoryWebContent.AIWEBCODE_MD0_19,
                Resources.AssetInventoryWebContent.AIWEBCODE_MD0_20, Resources.AssetInventoryWebContent.AIWEBCODE_MD0_21);
        }
    }

    /// <summary>
    /// Gets Firmware title
    /// </summary>
    protected override string DefaultTitle
    {
        get { return Resources.AssetInventoryWebContent.WEBCODE_FirmwareTitle; }
    }

    /// <summary>
    /// Gets Firmware help link
    /// </summary>
    public override string HelpLinkFragment
    {
        get { return "OrionSAMAGAIFirmware"; }
    }

    public override string EditControlLocation
    {
        get
        {
            return @"/Orion/AssetInventory/Controls/EditPagableResourceControl.ascx";
        }
    }

    public override string IconPath
    {
        get { return "/Orion/AssetInventory/images/Resources/Firmware_icon16x16.png"; }
    }
}