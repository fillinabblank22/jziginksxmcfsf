﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomAssetInformation.ascx.cs" Inherits="Orion_AssetInventory_Resources_NodeDetails_CustomAssetInformation" %>
<asp:ScriptManagerProxy runat="server" >
    <Services>
        <asp:ServiceReference path="/Orion/Services/Information.asmx" />
        <asp:ServiceReference path="/Orion/AssetInventory/Services/AssetInventoryResources.asmx" />
    </Services>
</asp:ScriptManagerProxy>

<orion:resourceWrapper runat="server" ID="Wrapper" CssClass="AssetInventoryResource">
    <Content>
        <!-- This is Underscore Javascript Template. See https://cp.solarwinds.com/display/OrionPlatformRD/Underscore+JavaScript+Templates -->
        <script id="ResourceTemplate<%= ScriptFriendlyResourceID %>" type="text/x-template">
            {# var hideRow = SW.AssetInventory.Resources.hideRow; #}
            <table class="AssetInventoryResourceTable NeedsZebraStripes">
                {# _.each(objects, function (current) { #}
                <tr{{hideRow(current.Value || '<%= this.ShowPopulatedData ? "" : " " %>')}}>
                    <td class="aiBold">{{current.Name}}</td>
                    <td>{{current.Value}}</td>
                </tr>
                {# }); #}
            </table>
        </script>
        <div id="ResourceContent<%= ScriptFriendlyResourceID %>"></div>
        <script type="text/javascript">
            $(function () {
                function refresh() {
                    SW.AssetInventory.Resources.displayTemplateFromWebServiceCallByObjectList(
                        'ResourceTemplate<%= ScriptFriendlyResourceID %>',
                        'ResourceContent<%= ScriptFriendlyResourceID %>',
                        'GetCustomProperties',
                        <%= Node.NodeID %>
                    );
                }
                SW.Core.View.AddOnRefresh(refresh, 'ResourceContent<%= ScriptFriendlyResourceID %>');
                refresh();
            });
        </script>
    </Content>
</orion:resourceWrapper>
