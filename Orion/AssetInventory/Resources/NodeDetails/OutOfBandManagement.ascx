﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OutOfBandManagement.ascx.cs" Inherits="Orion_AssetInventory_Resources_NodeDetails_OutOfBandManagement" %>

<asp:scriptmanagerproxy runat="server">
    <Services>
        <asp:ServiceReference path="/Orion/Services/Information.asmx" />
        <asp:ServiceReference path="/Orion/AssetInventory/Services/AssetInventoryResources.asmx" />
    </Services>
</asp:scriptmanagerproxy>

<orion:resourcewrapper runat="server" id="Resourcewrapper1" CssClass="AssetInventoryResource">
    <Content>
        <!-- This is Underscore Javascript Template. See https://cp.solarwinds.com/display/OrionPlatformRD/Underscore+JavaScript+Templates -->
        <script id="ResourceTemplate<%= ScriptFriendlyResourceID %>" type="text/x-template">
            <table class="AssetInventoryResourceTable NeedsZebraStripes" >
            <tr class="HeaderRow">
                <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_MS0_10 %></td>
                <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_MS0_11 %></td>
                <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_MS0_12 %></td>
                <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_9 %></td>
                <td class="ReportHeader" style="text-transform: uppercase;"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_40 %></td>
            </tr>
             {# _.each(objects, function(current, index)  { #}
                <tr>
                    <td>{{(current.Card === null || current.Card == '')
                        ? '<%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_25 %>'
                        : current.Card }}</td>
                    <td>{{current.Type}}</td>
                    <td>{{(current.FirmwareVersion === null || current.FirmwareVersion == '')
                        ? '<%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_25 %>'
                        : current.FirmwareVersion }}</td>
                    <td>{# if (current.IPAddress === null || current.IPAddress == '') { #}
                        <%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_25 %>
                    {# } else { #}
                         <a class="AssetInventoryWarrantyLink" href="http://{{current.IPAddress}}" target="_blank">{{current.IPAddress}}</a>
                    {# } #}</td>
                    <td>{{(current.MACAddress === null || current.MACAddress == '')
                        ? '<%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_25 %>'
                        : current.MACAddress }}</td>
                </tr>
                {# }); #}
            </table>
        </script>
        <div id="ResourceContent<%= ScriptFriendlyResourceID %>"></div>
        <script type="text/javascript">
            $(function () {
                function refresh() {
                    SW.AssetInventory.Resources.displayTemplateFromWebServiceCallByObjectListCallback(
                        'ResourceTemplate<%= ScriptFriendlyResourceID %>',
                        'ResourceContent<%= ScriptFriendlyResourceID %>',
                        'GetOutOfBandManagement',
                        function () {
                            SW.AssetInventory.Resources.hideResourceIfNoRows('#ResourceContent<%= ScriptFriendlyResourceID %>', '[ResourceID=<%= ScriptFriendlyResourceID %>]');
                        },
                        <%= Node.NodeID %>
                    );
                }

                SW.Core.View.AddOnRefresh(refresh, 'ResourceContent<%= ScriptFriendlyResourceID %>');
                refresh();
            });
        </script>
    </Content>
</orion:resourcewrapper>
