﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WarrantyAssetInformation.ascx.cs" Inherits="Orion_AssetInventory_Resources_NodeDetails_WarrantyAssetInformation" %>
<link id="AssetInventory" rel="stylesheet" runat="server" href="/Orion/AssetInventory/Styles/AssetInventory.css" />
<script type="text/javascript" src="/Orion/AssetInventory/js/Resources.js"></script>

<asp:scriptmanagerproxy runat="server">
    <Services>
        <asp:ServiceReference Path="/Orion/Services/Information.asmx" />
        <asp:ServiceReference Path="/Orion/AssetInventory/Services/AssetInventoryResources.asmx" />
    </Services>
</asp:scriptmanagerproxy>

<orion:resourcewrapper runat="server" id="Wrapper" cssclass="AssetInventoryResource">
    <Content>
    <% if (IsShowExpiredWarranties) { %>
        <script id="ResourceTemplate1_<%= ScriptFriendlyResourceID %>" type="text/x-template">
            <table class="AssetInventoryResourceTable NeedsZebraStripes" id="ExpiredWTable<%= ScriptFriendlyResourceID %>">
                <tr class="HeaderRow">
                    <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_63 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_84 %></td>
                </tr>
                {# _.each(obj, function (current) { #}
                <tr>
                    <td><a href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{{current.NodeId}}">
                        <img alt="" src="/Orion/images/StatusIcons/Small-{{current.StatusIcon}}" />&nbsp;{{current.HostName}}</a>
                    </td>
                    <td>{{current.WarrantyDateString}}</td>
                </tr>
                {# }); #}
            </table>
        </script>
        <orion:CollapsePanel runat="server" Collapsed="false">
            <TitleTemplate><b><%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_62 %></b>&nbsp;<label id="Expired<%= ScriptFriendlyResourceID %>"> </label></TitleTemplate>
            <BlockTemplate>
                <div id="ResourceContent1_<%= ScriptFriendlyResourceID %>"></div>
            </BlockTemplate>
        </orion:CollapsePanel>
    <% } %>
        <br />
        <script id="ResourceTemplate2_<%= ScriptFriendlyResourceID %>" type="text/x-template">
            {#
                function getProgressBarClass(value, critical, warning) {
                    if (value <= critical) {
                        return 'aiWarrantyCritical';
                    }
                    if (value <= warning) {
                        return 'aiWarrantyWarning';
                    }
                    return 'aiWarrantyNormal';
                }

                function getFontClass(value, critical, warning) {
                    if (value <= critical) {
                        return 'aiWarrantyCriticalFont';
                    }
                    if (value <= warning) {
                        return 'aiWarrantyWarningFont';
                    }
                    return 'aiWarrantyNormalFont';
                }
            #}
            <table class="AssetInventoryResourceTable" id="DueToExpireTable<%= ScriptFriendlyResourceID %>" >
                <tr class="HeaderRow">
                    <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_63 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_84 %></td>
                    <td colspan="2" class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_85 %></td>
                </tr>
                {# _.each(obj, function (current) { #}
                <tr>
                    <td><a href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{{current.NodeId}}">
                        <img alt="" src="/Orion/images/StatusIcons/Small-{{current.StatusIcon}}" />&nbsp;{{current.HostName}}</a>
                    </td>
                    <td>{{current.WarrantyDateString}}</td>
                    <td><span class="{{getFontClass(current.ExpiringDays, <%= CriticalDays %>, <%= WarningDays %>)}}">{{current.ExpiringDays}}</span></td>
                    <td width="60px">
                        <div class="aiWarrantyPercentBackground">
                            <div style="width: {{100 * current.ExpiringDays / <%= ExpiringWithin %>}}%" class="{{getProgressBarClass(current.ExpiringDays, <%= CriticalDays %>, <%= WarningDays %>)}}"/>
                        </div>
                    </td>
                </tr>
                {# }); #}
            </table>
        </script>

        <orion:CollapsePanel ID="CollapsePanel2" runat="server" Collapsed="false">
            <TitleTemplate>
                &nbsp;<b><%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_80 %></b>&nbsp;<label id="DueToExpire<%= ScriptFriendlyResourceID %>"> </label>
            </TitleTemplate>
            <BlockTemplate>
              <span style="text-transform:uppercase"><%=WithinDays %></span>
                <div id="ResourceContent2_<%= ScriptFriendlyResourceID %>"></div>
            </BlockTemplate>
        </orion:CollapsePanel>
        <br />
        <% if (IsShowTopXXApproaching) { %>
        <script id="ResourceTemplate3_<%= ScriptFriendlyResourceID %>" type="text/x-template">
            <table class="AssetInventoryResourceTable" id="TopXXApproachingTable<%= ScriptFriendlyResourceID %>" >
                <tr class="HeaderRow">
                    <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_63 %></td>
                    <td class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_84 %></td>
                    <td colspan="2" class="ReportHeader" style="text-transform: uppercase;"><%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_85 %></td>
                </tr>
                {# _.each(obj, function (current) { #}
                <tr>
                    <td>
                        <a href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{{current.NodeId}}"><img alt="" src="/Orion/images/StatusIcons/Small-{{current.StatusIcon}}" />&nbsp;{{current.HostName}}</a>
                    </td>
                    <td>{{current.WarrantyDateString}}</td>
                    <td align="center">{{current.ExpiringDays}}</td>
                </tr>
                {# }); #}
            </table>
        </script>
        <orion:CollapsePanel ID="CollapsePanel1" runat="server" Collapsed="false">
            <TitleTemplate><b><%= string.Format(Resources.AssetInventoryWebContent.AIWEBDATA_MD0_81, TopApproachingCount) %></b></TitleTemplate>
            <BlockTemplate>
                <div id="ResourceContent3_<%= ScriptFriendlyResourceID %>"></div>
            </BlockTemplate>
        </orion:CollapsePanel>
        <% } %>

        <script type="text/javascript">
            $(function () {
                function refresh1() {
                    SW.AssetInventory.Resources.displayTemplateFromWebServiceCallByObjectCallback(
                        'ResourceTemplate1_<%= ScriptFriendlyResourceID %>',
                        'ResourceContent1_<%= ScriptFriendlyResourceID %>',
                        'GetExpiredWarranties',
                        function () {
                            var count = $('#ExpiredWTable<%= ScriptFriendlyResourceID %>').find('tr').length - 1;
                            var str = (count === 1)
                                ? '<%=Resources.AssetInventoryWebContent.AIWEBDATA_YP0_2 %>'
                                : '<%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_86 %>';
                            $('#Expired<%= ScriptFriendlyResourceID %>').text(String.format(str, count));
                        },
                        '<%= SwqlString %>',
                        <%= (HttpContext.Current.Items[typeof(ViewInfo).Name] as ViewInfo).LimitationID %>
                    );
                }

                function refresh2() {
                    SW.AssetInventory.Resources.displayTemplateFromWebServiceCallByObjectCallback(
                        'ResourceTemplate2_<%= ScriptFriendlyResourceID %>',
                        'ResourceContent2_<%= ScriptFriendlyResourceID %>',
                        'GetDueToExpireWarranties',
                        function () {
                            var count = $('#DueToExpireTable<%= ScriptFriendlyResourceID %>').find('tr').length - 1;
                            var str = (count === 1)
                                ? '<%=Resources.AssetInventoryWebContent.AIWEBDATA_YP0_2 %>'
                                : '<%=Resources.AssetInventoryWebContent.AIWEBDATA_MD0_86 %>';
                            $('#DueToExpire<%= ScriptFriendlyResourceID %>').text(String.format(str, count));
                        },
                        '<%= SwqlString %>',
                        <%= ExpiringWithin %>,
                        <%= (HttpContext.Current.Items[typeof(ViewInfo).Name] as ViewInfo).LimitationID %>
                    );
                }

                function refresh3() {
                    SW.AssetInventory.Resources.displayTemplateFromWebServiceCallByObject(
                        'ResourceTemplate3_<%= ScriptFriendlyResourceID %>',
                        'ResourceContent3_<%= ScriptFriendlyResourceID %>',
                        'GetTopXXApproachingWarranties',
                        '<%= SwqlString %>',
                        <%= ExpiringWithin %>,
                        <%= TopApproachingCount %>,
                        <%= (HttpContext.Current.Items[typeof(ViewInfo).Name] as ViewInfo).LimitationID %>
                    );
                }

                if ('<%= IsShowExpiredWarranties %>' === 'True') {
                    SW.Core.View.AddOnRefresh(refresh1, 'ResourceContent1_<%= ScriptFriendlyResourceID %>');
                    refresh1();
                }

                if ('<%= IsShowTopXXApproaching %>' === 'True') {
                    SW.Core.View.AddOnRefresh(refresh3, 'ResourceContent3_<%= ScriptFriendlyResourceID %>');
                    refresh3();
                }

                SW.Core.View.AddOnRefresh(refresh2, 'ResourceContent2_<%= ScriptFriendlyResourceID %>');
                refresh2();
            });
        </script>
    </Content>
</orion:resourcewrapper>
