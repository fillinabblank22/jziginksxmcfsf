﻿using System;
using SolarWinds.AssetInventory.Common;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_AssetInventory_Resources_NodeDetails_WarrantyAssetInformation : BaseResourceControl
{
    public const int DefaultWarrantiesExpiringWithin = 2;
    public const bool DefaultShowExpiredWarranties = true;
    public const bool DefaultShowTopXXApproaching = true;
    public const int DefaultTopApproachingCount = 5;

    private static int[] DayPeriods = new int[] { 30, 60, 90, 180 };

    public override string HelpLinkFragment
    {
        get { return "OrionSAMAGServerWarrantySummary"; }
    }

    public override string EditControlLocation
    {
        get
        {
            return @"/Orion/AssetInventory/Controls/EditWarrantyAssetInformation.ascx";
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.AssetInventoryWebContent.WEBCODE_ServerWarrantySummary; }
    }

    public bool ShowPopulatedData
    {
        get
        {
            return this.Resource.Properties.ContainsKey("ShowPopulated") && Boolean.Parse(this.Resource.Properties["ShowPopulated"]);
        }
    }

    public bool IsShowExpiredWarranties
    {
        get
        {
            if (Resource != null)
            {
                if (!Resource.Properties.ContainsKey(ResourceProperties.ShowExpiredWarranties))
                {
                    return DefaultShowExpiredWarranties;
                }
                return bool.Parse(Resource.Properties[ResourceProperties.ShowExpiredWarranties]);
            }


            return false;
        }
    }

    public bool IsShowTopXXApproaching
    {
        get
        {
            if (Resource != null)
            {
                if (!Resource.Properties.ContainsKey(ResourceProperties.ShowTopXXApproaching))
                {
                    return DefaultShowTopXXApproaching;
                }
                return bool.Parse(Resource.Properties[ResourceProperties.ShowTopXXApproaching]);
            }
            return false;
        }
    }

    public int TopApproachingCount
    {
        get
        {
            if (Resource != null)
            {
                if (!Resource.Properties.ContainsKey(ResourceProperties.TopXXApproachingWarrantiesCount))
                {
                    return DefaultTopApproachingCount;
                }
                int value;
                if (int.TryParse(Resource.Properties[ResourceProperties.TopXXApproachingWarrantiesCount], out value))
                {
                    return value;
                }
                return DefaultTopApproachingCount;
            }
            return DefaultTopApproachingCount;
        }
    }

    public string WithinDays
    {
        get
        {
            if (Resource != null)
            {
                if (!Resource.Properties.ContainsKey(ResourceProperties.ShowWarrantiesExpiringWithin))
                {
                    return string.Format(Resources.AssetInventoryWebContent.AIWEBDATA_MD0_82, DayPeriods[DefaultWarrantiesExpiringWithin]);
                }
                if (!string.IsNullOrEmpty(Resource.Properties[ResourceProperties.ShowWarrantiesExpiringWithin]))
                {
                    return string.Format(Resources.AssetInventoryWebContent.AIWEBDATA_MD0_82, DayPeriods[int.Parse(Resource.Properties[ResourceProperties.ShowWarrantiesExpiringWithin])]); 
                }
                return string.Format(Resources.AssetInventoryWebContent.AIWEBDATA_MD0_82, DayPeriods[DefaultWarrantiesExpiringWithin]);
            }
            return string.Empty;
        }
    }

    public string SwqlString
    {
        get
        {
            if (Resource != null)
            {
                if (!Resource.Properties.ContainsKey(ResourceProperties.SwqlQuery))
                {
                    return string.Empty;
                }
                return Resource.Properties[ResourceProperties.SwqlQuery].Replace("'", "&#39;");
            }
            return string.Empty;
        }
    }

    public int WarningDays
    {
        get
        {           
            return (int)(SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting(AssetInventoryConstants.WarningThresholdDays).SettingValue);            
        }
    }

    public int CriticalDays
    {
        get
        {
            return (int)(SolarWinds.Orion.Web.DAL.SettingsDAL.GetSetting(AssetInventoryConstants.CriticalThresholdDays).SettingValue); 
        }
    }

    public int ExpiringWithin
    {
        get
        {
            if (Resource != null)
            {
                if (!Resource.Properties.ContainsKey(ResourceProperties.ShowWarrantiesExpiringWithin))
                {
                    return DayPeriods[DefaultWarrantiesExpiringWithin];
                }
                return DayPeriods[int.Parse(Resource.Properties[ResourceProperties.ShowWarrantiesExpiringWithin])];
            }
            return DayPeriods[DefaultWarrantiesExpiringWithin];
        }
    }

    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }
}
