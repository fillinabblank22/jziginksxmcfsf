<%@ WebService Language="C#" Class="AssetInventoryResources" %>
using System.Collections.Generic;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.AssetInventory.Web.DAL;
using SolarWinds.AssetInventory.Web.Model;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class AssetInventoryResources : WebService
{
    [WebMethod]
    public object GetWindowsUpdate(int nodeID)
    {
        return new WindowsUpdateDAL().GetWindowsUpdateSummary(nodeID);
    }

    [WebMethod]
    public object GetExpiredWarranties(string swqlFilter, int limitationId)
    {
        return new WarrantyDAL().GetExpiredWarranties(swqlFilter, limitationId);
    }

    [WebMethod]
    public object GetDueToExpireWarranties(string swqlFilter, int days, int limitationId)
    {
        return new WarrantyDAL().GetDueToExpireWarranties(swqlFilter, days, limitationId);
    }

    [WebMethod]
    public object GetTopXXApproachingWarranties(string swqlFilter, int days, int approachingCount, int limitationId)
    {
        return new WarrantyDAL().GetTopXXApproachingWarranties(swqlFilter, days, approachingCount, limitationId);
    }

    [WebMethod]
    public List<OutOfBandManagement> GetOutOfBandManagement(int nodeID)
    {
        return new OutOfBandManagementDAL().GetOutOfBandManagement(nodeID);
    }

    [WebMethod]
    public List<StorageController> GetStorageControllers(int nodeID)
    {
        return new StorageControllersDAL().GetStorageControllers(nodeID);
    }

    [WebMethod]
    public object GetServerInformation(int nodeID)
    {
        return new SystemInformationDAL().GetSystemInformation(nodeID);
    }

    [WebMethod]
    public MemorySummary GetMemorySummary(int nodeID)
    {
        return new MemoryDAL().GetMemoryInfo(nodeID);
    }

    [WebMethod]
    public List<RemovableMedia> GetRemovableMedia(int nodeID)
    {
        return new RemovableMediaDAL().GetRemovableMedia(nodeID);
    }

    [WebMethod]
    public object GetLogicalDrives(int nodeID)
    {
        return new LogicalVolumesDAL().GetLogicalVolumes(nodeID);
    }

    [WebMethod]
    public List<Processor> GetProcessors(int nodeID)
    {
        return new ProcessorDAL().GetProcessorInformation(nodeID);
    }

    [WebMethod]
    public List<HardDrive> GetHardDrives(int nodeID)
    {
        return new HardDriveDAL().GetHardDrives(nodeID);
    }

    [WebMethod]
    public PortsAndUsbControllers GetPortsInfo(int nodeID)
    {
        return new PortAndUsbControllerDAL().GetPortsInfo(nodeID);
    }

    [WebMethod]
    public List<NetworkInterface> GetNetworkInterfaces(int nodeID, string searchText)
    {
        return new NetworkInterfaceDAL().GetNetworkIterfaces(nodeID, searchText);
    }

    [WebMethod]
    public List<PeripheralDevice> GetPeripherals(int nodeID)
    {
        return new PeripheralDeviceDAL().GetPeripheralDevices(nodeID);
    }

    [WebMethod]
    public Multimedia GetMultimedia(int nodeID)
    {
        return new MultimediaDAL().GetMultimedia(nodeID);
    }

    [WebMethod]
    public List<HostedVM> GetHostedVMs(int nodeID)
    {
        return new HostedVMDAL().GetHostedVMs(nodeID);
    }

    [WebMethod]
    public List<AssetInventoryCustomProperty> GetCustomProperties(int nodeID)
    {
        return new CustomPropertyDAL().GetCustomProperties(nodeID);
    }
}
