﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;

public partial class Orion_AssetInventory_Resources_NodeDetails_EditCustomAssetInformation : BaseResourceEditControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.cbShowPopulated.Checked = String.IsNullOrEmpty(this.Resource.Properties["ShowPopulated"]) || Boolean.Parse(this.Resource.Properties["ShowPopulated"]);
        }

        if (SolarWinds.AssetInventory.Web.Helpers.WebHelper.IsCustomObjectResource())
        {
            cbShowPopulated.Attributes.Add("onchange", String.Format("SaveData('ShowPopulated', document.getElementById('{0}').checked);", cbShowPopulated.ClientID));
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            properties["ShowPopulated"] = this.cbShowPopulated.Checked;
            return properties;
        }
    }
}