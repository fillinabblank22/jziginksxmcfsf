﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditCustomAssetInformation.ascx.cs" Inherits="Orion_AssetInventory_Resources_NodeDetails_EditCustomAssetInformation" %>
<link id="AssetInventoryCSS" rel="stylesheet" runat="server" media="screen" href="/Orion/AssetInventory/Styles/AssetInventory.css" />

<div>
    <asp:checkbox runat="server" id="cbShowPopulated" text="<%$ Resources : AssetInventoryWebContent, AIWEBDATA_VB1_61 %>" />
</div>

<div id="tip" style="position: absolute; top: 250px; left: 400px">
    <span class="crn crn-tl"></span>
    <span class="crn crn-tr"></span>
    <span class="crn crn-bl"></span>
    <span class="crn crn-br"></span>
    <div class="sw-ce-hint-nib" style="display: none;"></div>
    <div class="sw-ce-hint-inner">
        <div class="sw-ce-hint-body">
            <%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_93 %><br />
            <br />
            >> <a href="/Orion/Admin/CPE/Default.aspx"><span style="text-decoration: underline; color: #6E8CB2;">
                <%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_94 %></span></a>
        </div>
    </div>
</div>
