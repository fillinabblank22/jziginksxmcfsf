﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.AssetInventory.Common;

public partial class Orion_AssetInventory_Resources_NodeDetails_EditWarrantyAssetInformation : BaseResourceEditControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            //Show Warranties Expiring in: 
            if (!String.IsNullOrEmpty(this.Resource.Properties[ResourceProperties.ShowWarrantiesExpiringWithin]))
                cboShowWarrantiesExpiring.SelectedIndex = int.Parse(this.Resource.Properties[ResourceProperties.ShowWarrantiesExpiringWithin]);
            else
                cboShowWarrantiesExpiring.SelectedIndex = 2;
            //ShowExpiredWarranties
            if (!String.IsNullOrEmpty(this.Resource.Properties[ResourceProperties.ShowExpiredWarranties]))
            {
                cbShowExpiredWarranties.Checked = Boolean.Parse(this.Resource.Properties[ResourceProperties.ShowExpiredWarranties]);
            }
            else
            {
                cbShowExpiredWarranties.Checked = true;
            }
            //ShowExpiredWarranties
            if (!String.IsNullOrEmpty(this.Resource.Properties[ResourceProperties.ShowTopXXApproaching]))
            {
                cbShowTopXXApproaching.Checked = Boolean.Parse(this.Resource.Properties[ResourceProperties.ShowTopXXApproaching]);
            }
            else
            {
                cbShowTopXXApproaching.Checked = true;
            }
            pnlShowTopXXApproaching.Visible = cbShowTopXXApproaching.Checked;
            if (!String.IsNullOrEmpty(this.Resource.Properties[ResourceProperties.TopXXApproachingWarrantiesCount]))
            {
                txtTopXXApproaching.Text = this.Resource.Properties[ResourceProperties.TopXXApproachingWarrantiesCount];
            }
            else
            {
                txtTopXXApproaching.Text = @"5";
            }

            txtWarningDays.Text = ((int)(SettingsDAL.GetSetting(AssetInventoryConstants.WarningThresholdDays).SettingValue)).ToString();
            txtCriticalDays.Text = ((int)(SettingsDAL.GetSetting(AssetInventoryConstants.CriticalThresholdDays).SettingValue)).ToString();
            if (!String.IsNullOrEmpty(this.Resource.Properties[ResourceProperties.SwqlQuery]))
                txtSwqlQuery.Text = this.Resource.Properties[ResourceProperties.SwqlQuery];
        }

        if (SolarWinds.AssetInventory.Web.Helpers.WebHelper.IsCustomObjectResource())
        {
            this.txtWarningDays.Attributes.Add("onchange", String.Format("SaveData('{1}', document.getElementById('{0}').value);",
                this.txtWarningDays.ClientID, AssetInventoryConstants.WarningThresholdDays));
            this.txtCriticalDays.Attributes.Add("onchange", String.Format("SaveData('{1}', document.getElementById('{0}').value);",
                this.txtCriticalDays.ClientID, AssetInventoryConstants.CriticalThresholdDays));
            this.txtSwqlQuery.Attributes.Add("onchange", String.Format("SaveData('{1}', document.getElementById('{0}').value);",
                this.txtSwqlQuery.ClientID, ResourceProperties.SwqlQuery));
            this.txtTopXXApproaching.Attributes.Add("onchange", String.Format("SaveData('{1}', document.getElementById('{0}').value);",
                this.txtSwqlQuery.ClientID, ResourceProperties.TopXXApproachingWarrantiesCount));
            cbShowTopXXApproaching.Attributes.Add("onchange", String.Format("SaveData('{1}', document.getElementById('{0}').checked);",
                cbShowTopXXApproaching.ClientID, ResourceProperties.ShowTopXXApproaching));
            cbShowExpiredWarranties.Attributes.Add("onchange", String.Format("SaveData('{1}', document.getElementById('{0}').checked);",
                cbShowExpiredWarranties.ClientID, ResourceProperties.ShowExpiredWarranties));
            cboShowWarrantiesExpiring.Attributes.Add("onchange", String.Format("SaveData('{1}', document.getElementById('{0}').checked);",
                cboShowWarrantiesExpiring.ClientID, ResourceProperties.ShowWarrantiesExpiringWithin));
        }
    }

    protected override void OnUnload(EventArgs e)
    {
        int value;
        if (int.TryParse(txtWarningDays.Text, out value))
            SettingsDAL.SetValue(AssetInventoryConstants.WarningThresholdDays, value);
        if (int.TryParse(txtCriticalDays.Text, out value))
            SettingsDAL.SetValue(AssetInventoryConstants.CriticalThresholdDays, value);
        base.OnUnload(e);
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            properties[ResourceProperties.ShowWarrantiesExpiringWithin] = this.cboShowWarrantiesExpiring.SelectedIndex;
            properties[ResourceProperties.ShowExpiredWarranties] = this.cbShowExpiredWarranties.Checked;
            properties[ResourceProperties.ShowTopXXApproaching] = this.cbShowTopXXApproaching.Checked;
            properties[ResourceProperties.SwqlQuery] = txtSwqlQuery.Text;
            properties[ResourceProperties.TopXXApproachingWarrantiesCount] = txtTopXXApproaching.Text;
            return properties;
        }
    }
    protected void cbShowTopXXApproaching_CheckedChanged(object sender, EventArgs e)
    {
        pnlShowTopXXApproaching.Visible = cbShowTopXXApproaching.Checked;
    }
}