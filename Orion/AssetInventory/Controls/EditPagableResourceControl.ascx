﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditPagableResourceControl.ascx.cs" Inherits="Orion_AssetInventory_Controls_EditPagableResourceControl" %>
<div>
    <span style="padding-right: 20px; font-weight:bold"><%= Resources.AssetInventoryWebContent.AIWEBDATA_VB1_62 %> </span><asp:TextBox runat="server" ID="txtRowsPerPage" Width="50px"/>
	<asp:RangeValidator id="RowsPerPageRangeValidator" runat="server" controltovalidate="txtRowsPerPage" errormessage="<%$ Resources: AssetInventoryWebContent, AIWEBDATA_MD0_92 %>" MinimumValue="1" MaximumValue="100000" Type="Integer" />
</div>
