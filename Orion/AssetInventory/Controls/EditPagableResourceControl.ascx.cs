﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;

public partial class Orion_AssetInventory_Controls_EditPagableResourceControl : BaseResourceEditControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (!String.IsNullOrEmpty(this.Resource.Properties["RowsPerPage"]))
                this.txtRowsPerPage.Text = this.Resource.Properties["RowsPerPage"];
            else
            {
                this.txtRowsPerPage.Text = @"5";
            }
        }

        if (SolarWinds.AssetInventory.Web.Helpers.WebHelper.IsCustomObjectResource())
        {
            this.txtRowsPerPage.Attributes.Add("onchange", String.Format("SaveData('RowsPerPage', document.getElementById('{0}').value);", this.txtRowsPerPage.ClientID));
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            int rowsPerPage;
            properties["RowsPerPage"] = int.TryParse(this.txtRowsPerPage.Text, out rowsPerPage) ? rowsPerPage : 5;
            return properties;
        }
    }
}