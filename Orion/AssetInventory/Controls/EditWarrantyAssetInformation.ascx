﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditWarrantyAssetInformation.ascx.cs" Inherits="Orion_AssetInventory_Resources_NodeDetails_EditWarrantyAssetInformation" %>
<link id="AssetInventoryCSS" rel="stylesheet" runat="server" media="screen" href="~/css/styles.css" />
<div style="line-height: 30px;">
    <b>
        <asp:label runat="server" text="<%$ Resources : AssetInventoryWebContent, AIWEBDATA_MD0_48 %>" />
    </b>
    <br />

    <asp:dropdownlist runat="server" id="cboShowWarrantiesExpiring">
        <asp:ListItem Text="<%$ Resources: AssetInventoryWebContent, AIWEBDATA_MD0_49 %>" />
        <asp:ListItem Text="<%$ Resources: AssetInventoryWebContent, AIWEBDATA_MD0_50 %>" />
        <asp:ListItem Text="<%$ Resources: AssetInventoryWebContent, AIWEBDATA_MD0_51 %>" />
        <asp:ListItem Text="<%$ Resources: AssetInventoryWebContent, AIWEBDATA_MD0_52 %>" />
    </asp:dropdownlist>
    <br />
    <asp:checkbox runat="server" id="cbShowExpiredWarranties" text="<%$ Resources : AssetInventoryWebContent, AIWEBDATA_MD0_53 %>" />
    <br />

    <asp:checkbox runat="server" id="cbShowTopXXApproaching" text="<%$ Resources : AssetInventoryWebContent, AIWEBDATA_MD0_54 %>"
         OnCheckedChanged="cbShowTopXXApproaching_CheckedChanged" AutoPostBack="true"/>  <br />
      <span style="text-indent: 25px">
    <asp:panel runat="server" id="pnlShowTopXXApproaching" >
      
        <asp:label runat="server" text="<%$ Resources : AssetInventoryWebContent, AIWEBDATA_MD0_91 %>" />
        <asp:textbox runat="server" id="txtTopXXApproaching" width="50px" />
        <asp:RangeValidator id="RegularExpressionValidator3" runat="server"
            controltovalidate="txtTopXXApproaching" enableclientscript="false"
            errormessage="<%$ Resources: AssetInventoryWebContent, AIWEBDATA_MD0_92 %>"
            MinimumValue="1" MaximumValue="100000" Type="Integer" />
    </asp:panel></span>
    <b>
        <asp:label runat="server" id="lbExpiringWarrantyThresholds" text="<%$ Resources : AssetInventoryWebContent, AIWEBDATA_MD0_55 %>" />
    </b>
    <br />

    <table>
        <tr>
            <td style="width: 80px;">
                <img src="/Orion/AssetInventory/Images/WarrantyStatus/Warning_icon16x16.png" />
                <asp:label runat="server" text="<%$ Resources : AssetInventoryWebContent, AIWEBDATA_MD0_58 %>" />
                :
            </td>
            <td style="width: 120px;">
                <asp:label runat="server" text="<%$ Resources : AssetInventoryWebContent, AIWEBDATA_MD0_59 %>" />
            </td>
            <td style="width: 62px;">
                <asp:textbox runat="server" id="txtWarningDays" width="50px" />
            </td>
            <td>
                <asp:label runat="server" text="<%$ Resources : AssetInventoryWebContent, AIWEBDATA_MD0_60 %>" />
            </td>
            <td>
                <asp:RangeValidator id="RegularExpressionValidator1" runat="server"
                    controltovalidate="txtWarningDays" enableclientscript="false"
                    errormessage="<%$ Resources: AssetInventoryWebContent, AIWEBDATA_MD0_88 %>"
                    MinimumValue="1" MaximumValue="100000" Type="Integer" />
            </td>
        </tr>
        <tr>
            <td>
                <img src="/Orion/AssetInventory/Images/WarrantyStatus/Critical_icon16x16.png" />
                <asp:label runat="server" id="Label4" text="<%$ Resources : AssetInventoryWebContent, AIWEBDATA_MD0_61 %>" />
                :
            </td>
            <td>
                <asp:label runat="server" text="<%$ Resources : AssetInventoryWebContent, AIWEBDATA_MD0_59 %>" />
            </td>
            <td>
                <asp:textbox runat="server" id="txtCriticalDays" width="50px" />
            </td>
            <td>
                <asp:label runat="server" text="<%$ Resources : AssetInventoryWebContent, AIWEBDATA_MD0_60 %>" />
            </td>
            <td>
                <asp:RangeValidator id="RegularExpressionValidator2" runat="server"
                    controltovalidate="txtCriticalDays" enableclientscript="false"
                    errormessage="<%$ Resources: AssetInventoryWebContent, AIWEBDATA_MD0_88 %>"
                    MinimumValue="1" MaximumValue="100000" Type="Integer" />
            </td>

        </tr>
    </table>
    <b>
        <asp:label id="Label1" runat="server" text="<%$ Resources : AssetInventoryWebContent, AIWEBDATA_MD0_67 %>" />
    </b>
    <br />
    <asp:textbox id="txtSwqlQuery" runat="server" width="300px" />
    <br />
    <asp:label id="Label2" runat="server" text="<%$ Resources : AssetInventoryWebContent, AIWEBDATA_MD0_68 %>" />
    <br />

    <orion:collapsepanel id="CollapsePanel1" runat="server" collapsed="true">
        <TitleTemplate><b><%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_77 %></b></TitleTemplate>
        <BlockTemplate>
            <table cellspacing="0" cellpadding="1px" style="line-height: 20px;">
                <tr style="background-color: #E4E4E4">
                    <td style="width: 30%"><%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_69 %></td>
                    <td><%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_70 %></td>
                </tr>
                <tr>
                    <td><%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_71 %></td>
                    <td><%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_72 %></td>
                </tr>
                <tr style="background-color: #F6F6F6">
                    <td><%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_73 %></td>
                    <td><%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_74 %></td>
                </tr>
                <tr>
                    <td><%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_75 %></td>
                    <td><%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_76 %></td>
                </tr>
                <tr>
                    <td><%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_87 %></td>
                    <td><%= Resources.AssetInventoryWebContent.AIWEBDATA_MD0_89 %></td>
                </tr>
            </table>
        </BlockTemplate>
    </orion:collapsepanel>     
</div>
