﻿using System;
using System.Web;
using System.Web.UI;
using SolarWinds.Orion.Web.UI;

public partial class SCM_View : MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(HelpFragment))
        {
            btnHelp.HelpUrlFragment = HelpFragment;
            btnHelp.Visible = true;
        }
    }

    public string GetRequestString()
    {
        return
            string.Format("/Orion/Admin/CustomizeView.aspx?ViewID={0}{1}&ReturnTo={2}", ((OrionView)Page).ViewInfo.ViewID, (!string.IsNullOrEmpty(Request.QueryString["NetObject"]) ? string.Format("&NetObject={0}", HttpUtility.UrlEncode(Request.QueryString["NetObject"])) : ""), ((OrionView)Page).ReturnUrl);
    }

    public string HelpFragment { get; set; }
}