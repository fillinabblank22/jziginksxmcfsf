﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditAllowSetBaseline.ascx.cs" Inherits="Orion_SCM_Admin_EditAllowSetBaseline_EditAllowSetBaseline" %>
<asp:ListBox ID="rolesListBox" SelectionMode="Single" Rows="1" runat="server">
    <asp:ListItem Text="<%$ Resources: ScmWebContent, Web_AllowSetBaseline_Enabled %>" Value="1"/>
    <asp:ListItem Text="<%$ Resources: ScmWebContent, Web_AllowSetBaseline_Disabled %>" Value="0"/>
</asp:ListBox>