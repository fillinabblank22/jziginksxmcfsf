(function (SCM, $, undefined) {

	var scmCompatibleNodes = [];
	var scmNodesWithProfiles = [];

	SCM.assignProfilesEnabled = function (grid) {
		var gridNodeIds = grid.GetSelectedNodeIDs();
		if (gridNodeIds.length === 0 || checkAuthorization() !== true) {
			return false;
		}

		result = loadNodeIdsFromServer(gridNodeIds, '/api2/scm/nodes/orionIds', scmCompatibleNodes);
		scmCompatibleNodes = result.newItems;
		return result.found;
	};

	SCM.unAssignProfilesEnabled = function (grid) {
		var gridNodeIds = grid.GetSelectedNodeIDs();
		if (gridNodeIds.length === 0 || checkAuthorization() !== true) {
			return false;
		}

		result = loadNodeIdsFromServer(gridNodeIds, '/api2/scm/nodes/getscmnodeids', scmNodesWithProfiles);
		scmNodesWithProfiles = result.newItems;
		return result.found;
	};

	var loadNodeIdsFromServer = function (gridNodeIds, url, currentItems) {
		var result = {
			found: false,
			newItems: currentItems
		};

		callWebMethod(url, function (loaded) {
			result.newItems = []
			gridNodeIds.forEach(function (id) {
				idNumber = parseInt(id, 10);
				if (loaded.includes(idNumber)) {
					result.found = true;
					result.newItems.push(id);
				}
			});
		});

		return result;
	};

	var checkAuthorization = function () {
		var result = false;
		callWebMethod('/api2/scm/authorization/scmadminordemo', function (allowed) {
			result = allowed;
		});
		return result;
	};

	SCM.assignProfile = function (grid) {
		openPageWithSelectedNodes('/ui/scm/monitorConfigurationChanges?', scmCompatibleNodes);
	};

	SCM.unAssignProfile = function (grid) {
		openPageWithSelectedNodes('/ui/scm/unAssignProfileDialogView?', scmNodesWithProfiles);
	};

	var openPageWithSelectedNodes = function (urlTemplate, selectedNodes) {
		if ($("#isDemoMode").length !== 0) {
			demoAction('SCM_addNodesCallback');
		}
		else {
			var idStrings = buildIdArguments(selectedNodes);
			var url = urlTemplate + idStrings;
			window.open(url, '_self');
		}
	};

	var buildIdArguments = function (allowed) {
		var idStrings = '';
		var i = 1;
		allowed.forEach(function (id) {
			var lastChar = '&'
			if (allowed.length === i) {
				lastChar = ''
			}
			i++;

			idStrings = idStrings + 'id=' + id + lastChar;
		});

		return idStrings;
	};

	var callWebMethod = function (url, success) {
		$.ajax({
			url: url,
			contentType: 'application/json; charset=utf-8',
			dataType: "json",
			async: false,
			success: function (result) {
				if (success)
					success(result);
			}
		});
	};

}(window.SCM = window.SCM || {}, jQuery));

// node management can't handle functions nested in objects therefore assigning to window object.
var SCM_AssignProfilesEnabled = SCM.assignProfilesEnabled;
var SCM_AssignProfile = SCM.assignProfile;
var SCM_UnAssignProfilesEnabled = SCM.unAssignProfilesEnabled;
var SCM_UnAssignProfile = SCM.unAssignProfile;