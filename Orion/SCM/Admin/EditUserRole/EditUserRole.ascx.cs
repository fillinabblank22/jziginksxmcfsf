﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_SCM_Admin_EditUserRole_EditUserRole : SolarWinds.Orion.Web.UI.ProfilePropEditUserControl
{
    public override String PropertyValue
    {
        get
        {
            return rolesListBox.SelectedValue;
        }

        set
        {
            rolesListBox.SelectedValue = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}