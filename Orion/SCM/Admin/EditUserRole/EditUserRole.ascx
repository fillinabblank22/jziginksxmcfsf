﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditUserRole.ascx.cs" Inherits="Orion_SCM_Admin_EditUserRole_EditUserRole" %>
<asp:ListBox ID="rolesListBox" SelectionMode="Single" Rows="1" runat="server">
    <asp:ListItem Text="<%$ Resources: ScmWebContent, Web_SCMUserRole_User %>" Value="0"/>
    <asp:ListItem Text="<%$ Resources: ScmWebContent, Web_SCMUserRole_Admin %>" Value="1"/>
</asp:ListBox>