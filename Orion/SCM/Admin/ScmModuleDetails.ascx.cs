﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.SCM.Common.Licensing;

public partial class Orion_SCM_Admin_ScmModuleDetails : System.Web.UI.UserControl
{
    private static Log _log;
    private static Log Logger { get { return _log ?? (_log = new Log()); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                GetModuleAndLicenseInfo();
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error while displaying details for Orion Core module. Details: {0}", ex.ToString());
            }
        }
    }

    private void GetModuleAndLicenseInfo()
    {
        var module = ModuleDetailsHelper.LoadModuleInfoForEngines("Primary", false)
            .FirstOrDefault(mi => String.Equals(mi.ProductShortName, LicenseInfo.ApplicationName, StringComparison.OrdinalIgnoreCase));
        if (module == null)
        {
            return;
        }

        var values = new Dictionary<string, string>
        {
            {Resources.CoreWebContent.WEBCODE_AK0_131, module.ProductName},
            {Resources.CoreWebContent.WEBDATA_AK0_84, module.Version},
            {Resources.CoreWebContent.WEBDATA_VB0_176, String.IsNullOrEmpty(module.HotfixVersion) ? Resources.CoreWebContent.WEBCODE_SO0_9 : module.HotfixVersion}
        };
        if (!String.IsNullOrEmpty(module.LicenseInfo))
        {
            values.Add(Resources.CoreWebContent.WEBCODE_AK0_132, module.LicenseInfo);
        }

        AddScmInfo(values);

        ScmDetails.Name = module.ProductDisplayName;
        ScmDetails.DataSource = values;
    }

    private void AddScmInfo(Dictionary<string, string> values)
    {
        int currentCount;
        using (var proxy = new SwisConnectionProxyFactory().Create())
        {
            var dataTable = proxy.Query(@"SELECT COUNT(*) AS SCMNodesCount FROM Orion.SCM.ServerConfiguration WHERE Enabled = 1");
            currentCount = (int)dataTable.Rows[0][0];
        }

        var maxCount = LicenseInfo.GetMaxElementCount();

        values.Add(Resources.ScmWebContent.LicenseDetails_Limit, maxCount == int.MaxValue ? Resources.CoreWebContent.WEBCODE_AK0_133 : maxCount.ToString());
        values.Add(Resources.ScmWebContent.LicenseDetails_Current, currentCount.ToString());
    }
}