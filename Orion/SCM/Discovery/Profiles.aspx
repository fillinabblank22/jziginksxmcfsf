﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Discovery/Results/ResultsWizardPage.master" AutoEventWireup="true" CodeFile="Profiles.aspx.cs" Inherits="Orion_SCM_Discovery_Profiles" Title="<%$ Resources: CoreWebContent, WEBDATA_VB0_23 %>" %>

<%@ Register Src="~/Orion/SCM/Discovery/Controls/ProfileList.ascx" TagPrefix="scm" TagName="ProfileList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
   
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <span class="ActionName"><%= Resources.ScmWebContent.Discovery_Step_Name %></span>
    <br />
    <%= Resources.ScmWebContent.Discovery_Step_Text %>
		
    <div class="contentBlock">
        <scm:ProfileList ID="ProfileList" runat="server" />
    </div>
	
    <table class="NavigationButtons">
        <tr>
            <td>
                <div class="sw-btn-bar-wizard">
                    <orion:LocalizableButton LocalizedText="Back" DisplayType="Secondary" runat="server" ID="imgBack" OnClick="imgbBack_Click" />
                    <orion:LocalizableButton LocalizedText="Next" DisplayType="Primary"  runat="server" ID="imgbNext" OnClick="imgbNext_Click" />
                    <orion:LocalizableButton LocalizedText="Cancel" DisplayType="Secondary" runat="server" ID="imgbCancel" OnClick="imgbCancel_Click" CausesValidation="false" />
                </div>
            </td>
        </tr>
    </table>    
</asp:Content>
