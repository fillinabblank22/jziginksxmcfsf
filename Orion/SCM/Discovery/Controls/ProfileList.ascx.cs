﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Orion.SCM.Models.Discovery;
using SolarWinds.Orion.SCM.Web.Common.Discovery;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_SCM_Discovery_Controls_ProfileList : System.Web.UI.UserControl
{
    private static class SessionKeys
    {
        public const string CheckAllProfilesIsSelected = "ScmResultsDiscoveryCheckAllProfilesIsSelected";
        public const string DiscoveryProfileTypeList = "ScmResultsDiscoveryProfileTypeListWorkflow";
        public const string LastImportId = "ScmResultsDiscoveryLastImportId";
    }

    private static readonly Log log = new Log();

    protected void CheckAllSelector_OnCheckedChanged(object sender, EventArgs e)
    {
        CheckAllProfilesIsSelected = ((CheckBox)this.ProfileTableGrid.HeaderRow.FindControl("CheckAllSelector")).Checked;
    }

    protected void CheckAllSelector_OnPreRender(object sender, EventArgs e)
    {
        ((CheckBox)this.ProfileTableGrid.HeaderRow.FindControl("CheckAllSelector")).Checked = CheckAllProfilesIsSelected;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.ProfileTableGrid.OnGetDataSource += ProfileTableGrid_GetDataSource;
    }

    private List<ProfileTypeGroup> ProfileTableGrid_GetDataSource()
    {
        return DiscoveryProfileTypeList;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (LastImportId == Guid.Empty || ResultsWorkflowHelper.ImportID != LastImportId)
        {
            DiscoveryProfileTypeList = null;
            LastImportId = ResultsWorkflowHelper.ImportID;
        }

        if (!IsPostBack)
        {
            // clear list in Session, because I can enter this page after action, which changed DiscoveredNodes collection
            if (ResultsWorkflowHelper.DiscoveryOnlyNotImportedResults)
            {
                DiscoveryProfileTypeList = null;
            }

            CreateProfileTypeList();
			if (DiscoveryProfileTypeList.Count > 0)
			{
                this.ProfileTableGrid.DataSource = DiscoveryProfileTypeList;
                this.ProfileTableGrid.DataBind();
			}
			else
			{
				this.ProfileTableGrid.Visible = false;
				this.DivNoResult.Visible = true;
			}
        }

        StoreSelectedProfileTypes();
    }

    private List<DiscoveredScmPoller> GetDiscoveredProfiles(DiscoveryResultBase discoveryResult)
    {
        var scmResult = discoveryResult.GetPluginResultOfType<ScmDiscoveryPluginResult>();
        return scmResult.DiscoveredPollers;
    }

    private HashSet<int> GetSelectedNodeIds(DiscoveryResultBase discoveryResult)
    {
        var coreResult = discoveryResult.GetPluginResultOfType<CoreDiscoveryPluginResult>();
        if (coreResult == null)
        {
            log.Error("Unable to get Core discovery plugin result for profile");
            return new HashSet<int>();
        }

        return new HashSet<int>(coreResult.DiscoveredNodes.Where(n => n.IsSelected).Select(n => n.NodeID));
    }

    private void CreateProfileTypeList()
    {
        // Return if there is no result at all
        if (ResultsWorkflowHelper.DiscoveryResults == null || ResultsWorkflowHelper.DiscoveryResults.Count == 0)
        {
            log.Error("DiscoveryResult is null");
            return;
        }

        // if we have some previous result, store info about selected items
        var oldGroups = new Dictionary<int, bool>();
        if (DiscoveryProfileTypeList != null)
        {
            oldGroups = DiscoveryProfileTypeList.ToDictionary<ProfileTypeGroup, int, bool>(p => p.ProfileId, p => p.IsSelected);
            DiscoveryProfileTypeList = null;
        }

        //take all discovered profiles for selected nodes
        var allProfiles = new List<DiscoveredScmPoller>();
        foreach (var discoveryResult in ResultsWorkflowHelper.DiscoveryResults)
        {
            var selectedNodeIDs = GetSelectedNodeIds(discoveryResult);
            var profiles = GetDiscoveredProfiles(discoveryResult).Where(a => selectedNodeIDs.Contains(a.NetObjectID));
            allProfiles.AddRange(profiles);
        }

        var profileGroups = allProfiles
            .GroupBy(p => p, new SolarWinds.Orion.SCM.Web.Common.Discovery.ProfilePollerGroupingComparer())
            .Select(g => new ProfileTypeGroup
            {
                ProfileId = g.Key.ScmProfileId,
                ProfileName = g.Key.ScmProfileName,
                IsSelected = oldGroups.ContainsKey(g.Key.ScmProfileId) ? oldGroups[g.Key.ScmProfileId] : g.All(p => p.IsSelected),
                Count = g.Count()
            }).ToList();

        DiscoveryProfileTypeList = profileGroups;
    }

    private void StoreSelectedProfileTypes()
    {
        for (var i = 0; i < ProfileTableGrid.Rows.Count; i++)
        {
            var item = ProfileTableGrid.Rows[i];
            var checkBox = (CheckBox) item.FindControl("Selector");
            DiscoveryProfileTypeList[i].IsSelected = checkBox.Checked;
        }

        foreach (var discoveryResult in ResultsWorkflowHelper.DiscoveryResults)
        {
            var selectedNodeIDs = GetSelectedNodeIds(discoveryResult);
            var groups = DiscoveryProfileTypeList.ToDictionary(p => p.ProfileId, p => p.IsSelected);

            GetDiscoveredProfiles(discoveryResult)
                .ForEach(profile =>
                    {
                        profile.IsSelected = selectedNodeIDs.Contains(profile.NetObjectID) &&
                            (!groups.ContainsKey(profile.ScmProfileId) || groups[profile.ScmProfileId]);
                    });
        }
    }

    private static bool CheckAllProfilesIsSelected
    {
        get { return SessionHelper.GetSessionValue<bool>(SessionKeys.CheckAllProfilesIsSelected); }
        set { SessionHelper.SetSession(SessionKeys.CheckAllProfilesIsSelected, value); }
    }

    private static Guid LastImportId
    {
        get { return SessionHelper.GetSessionValue<Guid>(SessionKeys.LastImportId); }
        set { SessionHelper.SetSession(SessionKeys.LastImportId, value); }
    }

    public static List<ProfileTypeGroup> DiscoveryProfileTypeList
    {
        get { return SessionHelper.GetSession<List<ProfileTypeGroup>>(SessionKeys.DiscoveryProfileTypeList); }
        set { SessionHelper.SetSession(SessionKeys.DiscoveryProfileTypeList, value); }
    }
}
