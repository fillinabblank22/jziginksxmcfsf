<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ScmDiscoveryCentralPlugin.ascx.cs" Inherits="SCM_Discovery_Controls_ScmDiscoveryCentralPlugin" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>
<style type="text/css">
    .coreDiscoveryIcon { float:left; padding-left: 20px; }
    .coreDiscoveryPluginBody { padding:0px 75px 5px 75px; }
    .coreDiscoveryPluginBody a.helpLink { color: #336699; font-size:smaller; }
    .coreDiscoveryPluginBody a.helpLink:hover { color:Orange; }
    .coreDiscoveryPluginBody img.stateImage { margin-top:-5px; }
</style>
<div class="coreDiscoveryIcon">
    <asp:Image ID="ScmDiscoveryCentralIcon" runat="server" ImageUrl="~/Orion/SCM/images/Icon.SCM.svg"/>
</div>
<div class="coreDiscoveryPluginBody">
    <h2><%= Resources.ScmWebContent.DiscoveryCentral_Title %></h2>
    <div><%= Resources.ScmWebContent.DiscoveryCentral_Text %></div>
    <span class="LinkArrow">&#0187;</span>
    <orion:HelpLink ID="ApplicationDiscoveryHelpLink" runat="server" HelpUrlFragment="OrionSCMDiscovery"
        HelpDescription="<%$ Resources: ScmWebContent, DiscoveryCentral_HelpDescription %>" CssClass="helpLink" />
    <br />
    <div class="managedObjInfoWrapper">   
        <span id="monitoredObjectsInfo" runat="server">
            <asp:Image ID="okImage" runat="server" ImageUrl="~/Orion/images/ok_16x16.gif" CssClass="stateImage"></asp:Image>
            <asp:Image ID="bulbImage" runat="server" ImageUrl="~/Orion/images/lightbulb_tip_16x16.gif" Visible="false" CssClass="stateImage"></asp:Image>
            <span style='font-weight:bold'><%= ScmNodesCount %></span> <%= Resources.ScmWebContent.DiscoveryCentral_NodesCountText %>
        </span>
    </div>
    <orion:LocalizableButton ID="DiscoverNetwork" runat="server" LocalizedText="CustomText"
        Text="<%$ Resources: ScmWebContent, DiscoveryCentral_DiscoverNetworkButton_Text%>" DisplayType="Secondary"
        OnClick="DiscoverNetwork_Click" />
    <orion:LocalizableButton runat="server" ID="ManuallyAssignProfiles" LocalizedText="CustomText"
        Text="<%$ Resources: ScmWebContent, DiscoveryCentral_ManuallyAssignProfilesButton_Text %>" DisplayType="Secondary"
        OnClick="ManuallyAssignProfiles_Click" />
</div>