using SolarWinds.Common.Snmp;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.Discovery;
using SolarWinds.Orion.SCM.Models;
using SolarWinds.Orion.SCM.Models.Discovery;
using SolarWinds.Orion.SCM.Data.ResourcesDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.InformationService;
using System.Data;

public partial class Orion_SCM_Discovery_Controls_AutoImportScmProfiles : AutoImportWizardStep
{
    private static Log log = new Log();

	private const string ProfileIconFormat = "<img src=\"/Orion/SCM/images/StatusIcons/SCMProfileDiscovery.png\" />";
	
	List<BuiltInProfileInfo> ScmBuiltInProfilesCollection = new List<BuiltInProfileInfo>();
	
    protected ScmDiscoveryPluginConfiguration Config
    {
        get 
		{ 
			var config = ConfigWorkflowHelper.DiscoveryConfigurationInfo.GetDiscoveryPluginConfiguration<ScmDiscoveryPluginConfiguration>();
            if (config == null)
            {
                config = new ScmDiscoveryPluginConfiguration();
                config.IsAutoImport = true;
                config.BuiltInProfiles = ScmBuiltInProfilesCollection.Select(profile => profile.ProfileId).ToList();
                ConfigWorkflowHelper.DiscoveryConfigurationInfo.AddDiscoveryPluginConfiguration(config);
            }
            return config;
		}
	}
	
	protected string LearnMoreUrl
    {
        get { return HelpHelper.GetHelpUrl("OrionSCMConfigurationProfiles"); }
    }
	
    public override string Name
    {
        get
        {
            return Resources.ScmWebContent.AutoImportWizardProfilesStepName;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        EnsureDataBound();
    }

	private void EnsureDataBound()
	{
		ScmBuiltInProfilesCollection = new ProfilesDal().GetBuiltInProfiles();

        if (ProfilesCheckboxes.Items.Count == 0)
        {
            ProfilesCheckboxes.DataTextField = "DisplayName";
            ProfilesCheckboxes.DataValueField = "Value";
            ProfilesCheckboxes.DataSource = ScmBuiltInProfilesCollection.Select(profile => new { 
                DisplayName = ProfileIconFormat + profile.ProfileName,
                Value = profile.ProfileId
            });
            ProfilesCheckboxes.DataBind();
        }
	}
	
    public override void LoadData()
    {
        EnsureDataBound();

        foreach (ListItem item in ProfilesCheckboxes.Items)
		{
            item.Selected = Config.BuiltInProfiles.Contains(AsProfileId(item.Value));
		}
    }

    public override void SaveData()
    {
		EnsureDataBound();
		
		Config.BuiltInProfiles = ProfilesCheckboxes.Items
            .OfType<ListItem>()
            .Where(item => item.Selected)
            .Select(item => AsProfileId(item.Value)).ToList();
	}
	
	private int AsProfileId(string value)
	{
		return int.Parse(value);
	}
 }
