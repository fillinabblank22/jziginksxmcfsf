﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProfileList.ascx.cs" Inherits="Orion_SCM_Discovery_Controls_ProfileList" %>
<%@ Register TagPrefix="scm" Namespace="SolarWinds.Orion.SCM.Web.Common.Discovery" Assembly="SolarWinds.Orion.SCM.Web.Common" %>

<script type="text/javascript">
    //<![CDATA[
    $(document).ready(function () {
        $(".CheckAllSelector input[type=checkbox]").click(function () {
            var checked_status = this.checked;
            $(".Selector input[type=checkbox]").each(function () {
                this.checked = checked_status;
            });
        });
    });
    //]]>
</script>


<div>
    <scm:ProfileGridView ID="ProfileTableGrid" runat="server" AutoGenerateColumns="False" GridLines="None" CssClass="NetObjectTable" AlternatingRowStyle-CssClass="NetObjectAlternatingRow"  AllowSorting="true" >
        <Columns>
            <asp:TemplateField ItemStyle-Width="20px">
                <HeaderTemplate>
                    <asp:CheckBox ID="CheckAllSelector" runat="server" class="CheckAllSelector" OnCheckedChanged="CheckAllSelector_OnCheckedChanged" OnPreRender="CheckAllSelector_OnPreRender" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="Selector" runat="server" Checked='<%# Eval("IsSelected") %>' CssClass="Selector" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Count" HeaderText="<%$ Resources: CoreWebContent, WEBDATA_VB0_39 %>" ItemStyle-Width="50px" SortExpression="Count" />
            <asp:ImageField DataImageUrlField="Icon" ItemStyle-Width="16px"></asp:ImageField>
            <asp:BoundField DataField="ProfileName" HeaderText="<%$ Resources: ScmWebContent, Discovery_Step_ColumnProfile %>" SortExpression="ProfileName" />
        </Columns>
    </scm:ProfileGridView>
	<div id="DivNoResult" runat="server" Visible="False"><%= Resources.ScmWebContent.Discovery_Step_NoResult_Text %></div>
</div>




