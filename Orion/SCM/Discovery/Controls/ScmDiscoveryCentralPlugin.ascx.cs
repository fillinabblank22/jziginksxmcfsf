using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.InformationService.Linq;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Swis;
using SolarWinds.Orion.SCM.Data.ResourcesDAL.FilteredNodeLists;
  
public partial class SCM_Discovery_Controls_ScmDiscoveryCentralPlugin : System.Web.UI.UserControl
{
    private Log _log = new Log();
    private int? _scmNodesCount;

	protected int ScmNodesCount
    {
        get
        {
            if (_scmNodesCount == null)
            {
                var swisProxy = GetSwisProxy();
                var dal = new FilteredScmNodeListDal(swisProxy, new SolarWinds.Orion.SCM.Data.ResourcesDAL.Swis.SwisMetadataDal(GetSwisContext(), swisProxy));
                try
                {
                    _scmNodesCount = dal.GetFilteredNodeListItems(new SolarWinds.Orion.SCM.WebApi.Models.FilteredList.NodeListDataSourceParameters()).TotalCount;
                }
                catch (Exception ex)
                {
                    _log.Error("Error on GetFilteredNodeListItems", ex);
                }
            }

            return _scmNodesCount ?? 0;
        }
    }
	
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ScmNodesCount == 0)
        {
            monitoredObjectsInfo.Attributes["class"] = "noObjectsStyle";
            bulbImage.Visible = true;
            okImage.Visible = false;
        }
    }
	
    protected void DiscoverNetwork_Click(object sender, EventArgs e)
    {
        DiscoveryCentralHelper.Leave();
        Response.Redirect("~/Orion/Discovery/Default.aspx");
    }
	
    protected void ManuallyAssignProfiles_Click(object sender, EventArgs e)
    {
        DiscoveryCentralHelper.Leave();
        Response.Redirect("~/ui/scm/settings/manageProfiles");
    }
	
	private ISwisContext GetSwisContext()
	{
        var userName = HttpContext.Current.User.Identity.Name;
        return SwisContextFactory.CreateContextForUser(userName);
	}
	
	private IInformationServiceProxy2 GetSwisProxy()
	{
        var userName = HttpContext.Current.User.Identity.Name;
        return SwisConnectionProxyPool.GetCreatorForUser(userName).Create();
	}
}
