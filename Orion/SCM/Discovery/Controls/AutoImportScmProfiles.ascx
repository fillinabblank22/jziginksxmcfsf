<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AutoImportScmProfiles.ascx.cs" Inherits="Orion_SCM_Discovery_Controls_AutoImportScmProfiles" %>
<orion:Include runat="server" module="SCM" File="SCM.css"/>

<div class="profile-templates-title"><%= Resources.ScmWebContent.AutoImportWizardProfilesTitle %></div>

<asp:CheckBoxList runat="server" ID="ProfilesCheckboxes" RepeatLayout="UnorderedList" CssClass="profiles-checkboxes"/>

<div class="profile-templates-suggestion" style="padding-top: 5px;">
    <span class="sw-suggestion">
        <span class="sw-suggestion-icon"></span>
        <%= Resources.ScmWebContent.AutoImportWizardProfilesHintText %>
        <a href="<%= LearnMoreUrl %>" target="_blank" rel="noopener noreferrer" class="sw-link">&raquo; <%= Resources.ScmWebContent.AutoImportWizardProfilesHintLink %></a>
    </span>
</div>