﻿<%@ Page Language="C#" MasterPageFile="~/Orion/SCM/SCMView.master" AutoEventWireup="true" CodeFile="Summary.aspx.cs" Inherits="SCM_Summary" Title="Untitled Page" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="orion" TagName="Refresher" Src="~/Orion/Controls/Refresher.ascx" %>

<asp:Content runat="server" ContentPlaceHolderID="ViewPageTitle">
	<h1>
	    <%= ViewInfo.ViewGroupTitle%>
        <%= ViewInfo.ViewHtmlTitle %> 
	</h1>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="MainContentPlaceHolder">
	<orion:ResourceHostControl runat="server">
		<orion:ResourceContainer runat="server" ID="resContainer" />
	</orion:ResourceHostControl>
</asp:Content>
