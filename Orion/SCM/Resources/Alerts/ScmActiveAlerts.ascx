<%@ Control Language="C#" CodeFile="ScmActiveAlerts.ascx.cs" AutoEventWireup="true" Inherits="Orion_Scm_Resources_Summary_ActiveAlerts" %>
<%@ Import Namespace="Resources" %>
<%@ Register Src="~/Orion/NetPerfMon/Controls/AlertsOnThisEntity.ascx" TagName="AlertsTable" TagPrefix="orion" %>
<orion:resourceWrapper runat="server" ID="wrapper">
    <Content>
        <orion:AlertsTable runat="server" ID="AlertsTable"/>
    </Content>
</orion:resourceWrapper>