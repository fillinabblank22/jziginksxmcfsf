using System;
using System.Collections.Generic;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.SCM.Data.ResourcesDAL;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Core.Common.Swis;
using System.Web.UI;
using Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
public partial class Orion_Scm_Resources_Summary_ActiveAlerts : BaseResourceControl
{	
    private const string Resources_PropertyName_Period = "Period";
	
    protected override string DefaultTitle => ScmWebContent.Resource_ScmActiveAlerts_DefaultTitle;
	

    // overriden main title of resources
    public override string DisplayTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Resource.Title.Trim()))
                return Resource.Title;

            return DefaultTitle;
        }
    }

    // overriden subtitle of resource
    public override string SubTitle
    {
        get
        {
            if (!string.IsNullOrWhiteSpace(Resource.SubTitle))
                return Resource.SubTitle;

             if (this.showAcknowledgedAlerts)
                return ScmWebContent.Resource_ScmActiveAlerts_SubtitleAllTrigerredAlerts;
            else
                return ScmWebContent.Resource_ScmActiveAlerts_SubtitleAllUnacknowledgedAlerts;
        }
    }

    public override string HelpLinkFragment => "OrionPHResourceAllTriggeredAlerts";

    public override string EditControlLocation => "/Orion/NetPerfMon/Controls/EditResourceControls/EditAllAlerts.ascx";

	private bool showAcknowledgedAlerts;
	
	protected void Page_Load(object sender, EventArgs e)
	{
		string showAck = Resource.Properties["ShowAcknowledgedAlerts"];
		AlertsTable.ShowAcknowledgedAlerts = showAcknowledgedAlerts = !String.IsNullOrEmpty(showAck) && showAck.Equals("true", StringComparison.OrdinalIgnoreCase);
		AlertsTable.ResourceID = Resource.ID;

		int rowsPerPage;
		if (!int.TryParse(Resource.Properties["RowsPerPage"], out rowsPerPage))
			rowsPerPage = 5;

		AlertsTable.InitialRowsPerPage = rowsPerPage;

		AddShowAllCurrentlyTriggeredAlertsControl(this.wrapper);
				
		this.LoadScmActiveAlerts();
	}
	
	protected void LoadScmActiveAlerts()
	{
		var nodeId = GetCurrentNetObjectID();
		
		if(nodeId.HasValue) {
			var dal = new ServerConfigurationDal(SwisContextFactory.CreateContextForUser(Profile.UserName));
			var triggeringObjectEntityUri = dal.GetUri(nodeId.Value);
			
			if(triggeringObjectEntityUri == null) {
				return;
			}
				
			AlertsTable.TriggeringObjectEntityUris.Add(triggeringObjectEntityUri);
			
		} else {
			
			AlertsTable.TriggeringObjectEntityNames = new List<string>(1) { "Orion.SCM.ServerConfiguration" };
			
		}
	}
	
	protected int? GetCurrentNetObjectID()
    {
		// if is netObject a Node set filter for a node
		INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
		if (nodeProvider != null)
		{
			return nodeProvider.Node.NodeID;
		}

        return null;
    }  
	
	private void AddShowAllCurrentlyTriggeredAlertsControl(Control resourceWrapperContents)
	{
		if (resourceWrapperContents.TemplateControl is IResourceWrapper)
		{
			IResourceWrapper wrapper = resourceWrapperContents.TemplateControl as IResourceWrapper;
			wrapper.ManageButtonText = Resources.CoreWebContent.WEBDATA_BV0_0043;
			wrapper.ManageButtonTarget = "/Orion/NetPerfMon/Alerts.aspx";
			wrapper.ShowManageButton = true;
		}
	}
}