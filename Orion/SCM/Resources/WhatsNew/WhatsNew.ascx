﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WhatsNew.ascx.cs" Inherits="Orion_SCM_Resources_WhatsNew_WhatsNew" %>

<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
	<Content>
		<orion:Include ID="Include1" runat="server" File="SCM/styles/Resources.css" />
		<table class="whatsNew">
			<tr>
				<td class="whatsNew_img">
					<img src="\Orion\SCM\images\pic_newfeature.gif" alt="new feature" />
				</td>
				<td>
					<div>
						<span class="title">
							<a href="../apps/scm/dashboard/policy" runat="server" target="_blank" rel="noopener noreferrer" class="link">
								<%= Resources.ScmWebContent.Resource_WhatsNew_PolicyCompliance_Title %>
							</a>
						</span>
						<span>
							<%= Resources.ScmWebContent.Resource_WhatsNew_PolicyCompliance_Text %>
						</span>
					</div>
				</td>
			</tr>
			<tr>
				<td/>
				<td>
					<div>
						<span class="title">
							<%= Resources.ScmWebContent.Resource_WhatsNew_OutOfTheBoxPolicies_Title %>
						</span>
						<span>
							<a href="../ui/scm/settings/policies" runat="server" target="_blank" rel="noopener noreferrer" class="link" Visible="<%# ShouldConfigurePolicyBeLink %>">
								<%= Resources.ScmWebContent.Resource_WhatsNew_OutOfTheBoxPolicies_LinkText %>
							</a>
							<ul>
								<li>
									<%= Resources.ScmWebContent.Resource_WhatsNew_OutOfTheBoxPolicies_WindowsServer2016_Text %>
								</li>
								<li>
									<%= Resources.ScmWebContent.Resource_WhatsNew_OutOfTheBoxPolicies_SQLServer2016_Text %>
								</li>
								<li>
									<%= Resources.ScmWebContent.Resource_WhatsNew_OutOfTheBoxPolicies_IISServer85_Text %>
								</li>
							</ul>
						</span>
					</div>
				</td>
			</tr>
			<tr>
				<td/>
				<td>
					<div>
						<span class="title">
							<%= Resources.ScmWebContent.Resource_WhatsNew_ComplianceReports_Title %>
						</span>
						<span>
							<%= Resources.ScmWebContent.Resource_WhatsNew_ComplianceReports_Text %>
						</span>
					</div>
				</td>
			</tr>
			<tr>
				<td/>
				<td>
					<div>
						<span class="title">
							<%= Resources.ScmWebContent.Resource_WhatsNew_ComplianceAlerts_Title %>
						</span>
						<span>
							<%= Resources.ScmWebContent.Resource_WhatsNew_ComplianceAlerts_Text %>
						</span>
					</div>
				</td>
			</tr>
			<tr>
				<td/>
				<td>
					<div>
						<span class="title">
							<%= Resources.ScmWebContent.Resource_WhatsNew_ImportAndExportPolicies_Title %>
						</span>
						<span>
							<%= Resources.ScmWebContent.Resource_WhatsNew_ImportAndExportPolicies_Text %>
							<a href="https://thwack.solarwinds.com/t5/SCM-Documents/tkb-p/scm-documents" runat="server" target="_blank" rel="noopener noreferrer" class="link">
								<%= Resources.ScmWebContent.Resource_WhatsNew_ImportAndExportPolicies_LinkText %>
							</a>
						</span>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding-right: 0; padding-top: 10px; text-align: right; padding-bottom: 0;">
					<orion:LocalizableButtonLink runat="server" ID="imgNewFeature" Target="_blank" Text="<%$ Resources:ScmWebContent, WhatsNew_LearnMoreButtonText %>" DisplayType="Primary" NavigateUrl="<%# HelpUrl %>">
					</orion:LocalizableButtonLink>&nbsp;<orion:LocalizableButton runat="server" ID="btnRemoveResource" Text="<%$ Resources:ScmWebContent, WhatsNew_RemoveResourceButtonText %>" DisplayType="Secondary" OnClick="OnRemoveResourceButtonClicked"/>
				</td>
			</tr>
		</table>
	</Content>
</orion:ResourceWrapper>
