﻿using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_SCM_Resources_WhatsNew_WhatsNew : BaseResourceControl
{
    protected override string DefaultTitle => Resources.ScmWebContent.Resource_WhatsNew_Title;

    public override sealed string DisplayTitle
    {
        get
        {
            var version = typeof(SolarWinds.Orion.SCM.Web.Common.ScmCentralizedSettingsAccessor).Assembly.GetName().Version;
            return this.DefaultTitle + " " + (version.Build == 0 ? version.ToString(2) : version.ToString(3));
        }
    }

    public override string HelpLinkFragment => "OrionSCMWhatsNew";
    public const string HelpLinkFimFragment = "OrionSCMEnableRealTimeFileMonitoring";
    public const string HelpLinkPowerShellFragment = "OrionSCMPowerShellScript";
    public const string HelpLinkAzureFragment = "OrionSCMAzureSQLDBSupport";

    protected string HelpUrl
    {
        get { return HelpHelper.GetHelpUrl("scm", this.HelpLinkFragment); }
    }

    protected string HelpUrlFim
    {
        get { return HelpHelper.GetHelpUrl("scm", Orion_SCM_Resources_WhatsNew_WhatsNew.HelpLinkFimFragment); }
    }

    protected string HelpUrlPowerShell
    {
        get { return HelpHelper.GetHelpUrl("scm", Orion_SCM_Resources_WhatsNew_WhatsNew.HelpLinkPowerShellFragment); }
    }

    protected string HelpUrlAzure
    {
        get { return HelpHelper.GetHelpUrl("scm", Orion_SCM_Resources_WhatsNew_WhatsNew.HelpLinkAzureFragment); }
    }

    protected bool ShouldEnableRealTimePollingBeLink
    {
        get { return (Profile.AllowNodeManagement && SolarWinds.Orion.SCM.Web.Common.ScmRoleAccessor.IsUserScmAdmin && !SolarWinds.Orion.SCM.Web.Common.ScmCentralizedSettingsAccessor.IsFimDriverWatchingEnabled) || (SolarWinds.Orion.SCM.Web.Common.ScmRoleAccessor.IsUserScmDemoUser); }
    }

    protected bool ShouldConfigurePolicyBeLink
    {
        get { return Profile.AllowAdmin; }
    }

    protected void OnRemoveResourceButtonClicked(object sender, EventArgs e)
    {
        ResourceManager.DeleteById(this.Resource.ID);
        this.Response.Redirect(this.Request.Url.ToString());
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            Visible = false;
        }
    }
}