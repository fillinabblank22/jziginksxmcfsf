using System;
using System.Collections.Generic;
using System.Linq;
using Resources;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.SCM.Data.ResourcesDAL;
using SolarWinds.Orion.SCM.Data.ResourcesDAL.Contract;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Core.Common.Swis;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Events)]
public partial class Orion_Scm_Resources_Summary_LastSCMEvents : BaseResourceControl
{
    private const string Resources_PropertyName_MaxEvents = "MaxEvents";
    private const string Resources_PropertyName_Period = "Period";
    private const string Resources_PropertyName_SelectedTypes = "SelectedTypes";
    private const string DefaultScmEventTypeIds = "7500, 7501, 7502, 7503, 7504, 7505, 7506, 7507";

    private const string EventTimeFormater = "{0:g}";

    private const int DefaultNumberOfEvents = 25;

    // flag used to display message about no events matching specified critera found
    public bool NoEventsFound { get; set; }

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.RenderControl;

    protected override string DefaultTitle => ScmWebContent.Resource_LastScmEvents_DefaultTitle;

    // overriden main title of resources
    public override string DisplayTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Resource.Title.Trim()))
                return Resource.Title;

            return DefaultTitle;
        }
    }

    // overriden subtitle of resource
    public override string SubTitle
    {
        get
        {
            if (!string.IsNullOrWhiteSpace(Resource.SubTitle))
                return Resource.SubTitle;

            // if subtitle is not specified subtitle is a period
            string period = Resource.Properties[Resources_PropertyName_Period];

            if (string.IsNullOrEmpty(period))
                return string.Empty;

            return Periods.GetLocalizedPeriod(period);
        }
    }

    public override string HelpLinkFragment => "OrionPHResourceLastXXEvents";

    public override string EditControlLocation => "/Orion/NetPerfMon/Controls/EditResourceControls/EditLastXXEvents.ascx";

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (string.IsNullOrEmpty(Resource.Properties[Resources_PropertyName_Period]))
                Resource.Properties[Resources_PropertyName_Period] = "Last 12 Months";
            if (string.IsNullOrEmpty(Resource.Properties[Resources_PropertyName_MaxEvents]))
                Resource.Properties[Resources_PropertyName_MaxEvents] = "25";
            if (string.IsNullOrEmpty(Resource.Properties[Resources_PropertyName_SelectedTypes]))
            {
                Resource.Properties[Resources_PropertyName_SelectedTypes] = DefaultScmEventTypeIds;
            }
        }

        EventsListControl.EventTimeFormater = EventTimeFormater;

        LoadEvents();

    }

    protected void LoadEvents()
    {
        var queryParams = BuildLastEventsQueryParams();

        var dal = new ScmEventsDal(SwisContextFactory.CreateContextForUser(Profile.UserName));
        var loadedEvents = dal.GetLastScmEvents(queryParams);

        if (loadedEvents.Rows.Count > 0)
        {
            EventsListControl.LoadData(loadedEvents);
        }
        else
        {
            NoEventsFound = true;
        }

    }

    private LastEventsSettings BuildLastEventsQueryParams()
    {
        string periodName = GetStringValue(Resources_PropertyName_Period, "Last 12 Months");

        var periodBegin = new DateTime();
        var periodEnd = new DateTime();

        // parse period to begin and end
        Periods.Parse(ref periodName, ref periodBegin, ref periodEnd);

        var settings = new LastEventsSettings
        {
            EventTimeFrom = periodBegin,
            EventTimeTo = periodEnd,
            MaxEventCount = GetIntProperty(Resources_PropertyName_MaxEvents, DefaultNumberOfEvents),
            NetObjectId = GetCurrentNetObjectID()
        };

        return settings;
    }

	protected int? GetCurrentNetObjectID()
    {
		// if is netObject a Node set filter for a node
		INodeProvider nodeProvider = GetInterfaceInstance<INodeProvider>();
		if (nodeProvider != null)
		{
			return nodeProvider.Node.NodeID;
		}

        return null;
    }
}
