<%@ Control Language="C#" CodeFile="LastScmEvents.ascx.cs" AutoEventWireup="true" Inherits="Orion_Scm_Resources_Summary_LastSCMEvents" %>
<%@ Import Namespace="Resources" %>
<%@ Register Src="~/Orion/Controls/EventsReportControl.ascx" TagName="EventsList" TagPrefix="orion" %>
<orion:resourceWrapper runat="server" ID="ResourceWrapper">
    <Content>
        <orion:EventsList runat="server" ID="EventsListControl"/>
         <% if (NoEventsFound){%>
            <h6><%=ScmWebContent.LastScmEventsResource_NoEventsFoundMsg%></h6>
         <%}%>
    </Content>
</orion:resourceWrapper>