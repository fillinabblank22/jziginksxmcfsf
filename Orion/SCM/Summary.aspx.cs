﻿using System;
using SolarWinds.Orion.Web.UI;

public partial class SCM_Summary : OrionView
{
    protected override void OnInit(EventArgs e)
    {
        this.Title = ViewInfo.ViewTitle;
        this.resContainer.DataSource = ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    public override string ViewType
    {
        get { return "SCM Summary"; }
    }
}