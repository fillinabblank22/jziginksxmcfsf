<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true"
    CodeFile="RemoteDesktop.aspx.cs" Inherits="Orion_RemoteDesktop" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadPlaceHolder" runat="Server">
    <meta content="vbscript" name="vs_defaultClientScript" />

    <script language="vbscript" type="text/vbscript">
    <!--
    const L_FullScreenWarn1_Text = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_1) %>"
    const L_FullScreenWarn2_Text = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_2) %>"
    const L_FullScreenTitle_Text = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_3) %>"
    const L_ErrMsg_Text         = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_4) %>"
    const L_PlatformCheck_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_5) %>"

    ' error messages
    const L_DisconnectedCaption_ErrorMessage =  "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_6) %>"
    const L_ErrConnectCallFailed_ErrorMessage =  "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_7) %>"
    const L_DisconnectRemoteByServer_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_8) %>"
    const L_LowMemory_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_9) %>"
    const L_SecurityErr_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_10) %>"
    const L_BadServerName_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_11) %>"
    const L_ConnectFailedProtocol_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_12) %>"
    const L_CannotLoopBackConnect_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_13) %>"
    const L_NetworkErr_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_14) %>"
    const L_InternalErr_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_15) %>"
    const L_NotResponding_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_16) %>"
    const L_VersionMismatch_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_17) %>"
    const L_EncryptionError_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_18) %>"
    const L_ProtocolErr_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_19) %>"
    const L_IllegalServerName_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_20) %>"
    const L_ConnectionTimeout_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_21) %>"
    const L_DisconnectIdleTimeout_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_22) %>"
    const L_DisconnectLogonTimeout_ErrorMessage ="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_23) %>"
    const L_ProtocolErrWITHCODE_ErrorMessage  = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_24) %>"
    const L_LicensingTimeout_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_25) %>"
    const L_LicensingNegotFailed_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_26) %>"
    const L_DisconnectRemoteByServerTool_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_27) %>"
    const L_LogoffRemoteByServer_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_28) %>"
    const L_DisconnectByOtherConnection_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_29) %>"
    const L_ConnectionBroken_ErrorMessage  = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_30) %>"
    const L_ServerOutOfMemory_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_31) %>"
    const L_LicenseInternal_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_32) %>"
    const L_NoLicenseServer_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_33) %>"
    const L_NoLicense_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_34) %>"
    const L_LicenseBadClientMsg_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_35) %>"
    const L_LicenseHwidDoesntMatch_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_36) %>"
    const L_BadClientLicense_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_37) %>"
    const L_LicenseCantFinishProtocol_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_38) %>"
    const L_LicenseClientEndedProtocol_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_39) %>"
    const L_LicenseBadClientEncryption_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_40) %>"
    const L_CantUpgradeLicense_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_41) %>"
    const L_LicenseNoRemoteConnections_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_42) %>"
    const L_DecompressionFailed_ErrorMessage = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_43) %>"
    const L_ServerDeniedConnection_ErrorMessage ="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_44) %>"

    sub window_onload()
       if not LCase(Navigator.CpuClass) = "x86" then
          msgbox L_PlatformCheck_ErrorMessage
       end if
       if not autoConnect() then
           Document.all.editServer.Focus
       end if
    end sub

    function autoConnect()
	    Dim sServer
	    Dim iFS, iAutoConnect


	    sServer = getQS("Server")
        if (sServer <> "") then
            Document.all.Server.value = sServer
        end if
	    iAutoConnect = getQS("AutoConnect")
	    iFS = getQS("FS")

	    if NOT IsNumeric ( iFS ) then
		    iFS = 0
	    else
		    iFS = CInt ( iFS )
	    end if

	    if iAutoConnect <> 1 then
		    autoConnect = false
		    exit function
	    else
		    if iFS <= 0 or iFS >= Document.all.comboResolution.options.length then
			    iFS = 2
		    end if

		    if IsNull ( sServer ) or sServer = "" then
			    sServer = window.location.hostname
		    end if

		    Document.all.comboResolution.selectedIndex	= iFS
		    Document.all.Server.value = sServer

		    btnConnect ()

		    autoConnect = true
	    end if

    end function

    function getQS ( sKey )
	    Dim iKeyPos, iDelimPos, iEndPos
	    Dim sURL, sRetVal
	    iKeyPos = iDelimPos = iEndPos = 0
	    sURL = window.location.href

	    if sKey = "" Or Len(sKey) < 1 then
		    getQS = ""
		    exit function
	    end if

	    iKeyPos = InStr ( 1, sURL, sKey )

	    if iKeyPos = 0 then
		    sRetVal = ""
		    exit function
	    end if

	    iDelimPos = InStr ( iKeyPos, sURL, "=" )
	    iEndPos = InStr ( iDelimPos, sURL, "&" )

	    if iEndPos = 0 then
		    sRetVal = Mid ( sURL, iDelimPos + 1 )
	    else
		    sRetVal = Mid ( sURL, iDelimPos + 1, iEndPos - iDelimPos - 1 )
	    end if

        sRetVal = Replace(Replace (sRetVal, "]", ""), "[", "")

	    getQS = sRetVal
    end function

    sub checkClick
          if Document.all.Check1.Checked then
             Document.all.tableLogonInfo.style.display = ""
             Document.all.editUserName.Disabled = false
             Document.all.editDomain.Disabled = false
          else
             Document.all.tableLogonInfo.style.display = "none"
             Document.all.editUserName.Disabled = true
             Document.all.editDomain.Disabled = true
          end if
    end sub

    sub OnControlLoad
       set Control = Document.getElementById("MsRdpClient")
       if Not Control is Nothing then
          if Control.readyState = 4 then
             Document.all.<%= connectbutton.ClientID%>.disabled = FALSE
          end if
       end if
    end sub


    sub BtnConnect

       Dim serverName
       if not Document.all.Server.value = "" then
          serverName = Document.all.Server.value
       else
          serverName = Document.location.hostname
       end if
       
       serverName = trim(serverName)
       
       Document.MsRdpClient.server = serverName
       
       Dim strFormat
       strFormat = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEB_JS_CODE_VB1_45) %>"
       strFormat = Replace(strFormat, "{0}", serverName)
       strFormat = Replace(strFormat, "{1}", "<br><font size=1>")
       strFormat = Replace(strFormat, "{2}", "<b>")
       strFormat = Replace(strFormat, "{3}", "</b>")
       strFormat = Replace(strFormat, "{4}", "</font>")
       Document.all.srvNameField.innerHtml = strFormat
       
       'Resolution
       Document.MsRdpClient.FullScreen = FALSE
       select case document.all.comboResolution.value
       case "1"
          Document.MsRdpClient.FullScreen = TRUE
          resWidth  = screen.width
          resHeight = screen.height
       case "2"
          resWidth  = "640"
          resHeight = "480"
       case "3"
          resWidth  = "800"
          resHeight = "600"
       case "4"
          resWidth  = "1024"
          resHeight = "768"
       case "5"
          resWidth  = "1280"
          resHeight = "1024"
       case "6"
          resWidth  = "1600"
          resHeight = "1200"
       end select
       Document.MsRdpClient.DesktopWidth = resWidth
       Document.MsRdpClient.DesktopHeight = resHeight
       
       
       Document.MsRdpClient.Width = resWidth
       Document.MsRdpClient.Height = resHeight
       
       'Device redirection options
       Document.MsRdpClient.AdvancedSettings2.RedirectDrives     = FALSE
       Document.MsRdpClient.AdvancedSettings2.RedirectPrinters   = TRUE
       Document.MsRdpClient.AdvancedSettings2.RedirectPorts      = FALSE
       Document.MsRdpClient.AdvancedSettings2.RedirectSmartCards = FALSE
       
       'FullScreen title
       Document.MsRdpClient.FullScreenTitle = L_FullScreenTitle_Text & "(" & serverName & ")"
       
       'Display connect region
       Document.all.loginArea.style.display = "none"
       Document.all.connectArea.style.display = "block"
       
       'Connect
       Document.MsRdpClient.Connect
    end sub

    sub ReturnToConnectPage()
       Document.all.loginArea.style.display = "block"
       
       ' If not commented, makes IE crashing when returning from RDP
       'Document.all.connectArea.style.display = "none"
       Document.all.srvNameField.innerHtml = ""
    end sub

    sub MsRdpClient_OnDisconnected(disconnectCode)
       extendedDiscReason = Document.MsRdpClient.ExtendedDisconnectReason
       majorDiscReason = disconnectCode And &hFF

       if (disconnectCode = &hB08 or majorDiscReason = 2 or majorDiscReason = 1) and not (extendedDiscReason = 5) then
          'Switch back to login area
          ReturnToConnectPage
          exit sub
       end if
       
       errMsgText = L_DisconnectRemoteByServer_ErrorMessage
       if not extendedDiscReason = 0 then
          'Use the extended disconnect code
          select case extendedDiscReason
          case 0   errMsgText  = ""
          case 1   errMsgText  = L_DisconnectRemoteByServerTool_ErrorMessage
          case 2   errMsgText  = L_LogoffRemoteByServer_ErrorMessage
          case 3   errMsgText  = L_DisconnectIdleTimeout_ErrorMessage
          case 4   errMsgText  = L_DisconnectLogonTimeout_ErrorMessage
          case 5   errMsgText  = L_DisconnectByOtherConnection_ErrorMessage
          case 6   errMsgText  = L_ServerOutOfMemory_ErrorMessage
          case 7   errMsgText  = L_ServerDeniedConnection_ErrorMessage
          case 256 errMsgText  = L_LicenseInternal_ErrorMessage
          case 257 errMsgText  = L_NoLicenseServer_ErrorMessage
          case 258 errMsgText  = L_NoLicense_ErrorMessage
          case 259 errMsgText  = L_LicenseBadClientMsg_ErrorMessage
          case 260 errMsgText  = L_LicenseHwidDoesntMatch_ErrorMessage
          case 261 errMsgText  = L_BadClientLicense_ErrorMessage
          case 262 errMsgText  = L_LicenseCantFinishProtocol_ErrorMessage
          case 263 errMsgText  = L_LicenseClientEndedProtocol_ErrorMessage
          case 264 errMsgText  = L_LicenseBadClientEncryption_ErrorMessage
          case 265 errMsgText  = L_CantUpgradeLicense_ErrorMessage
          case 266 errMsgText  = L_LicenseNoRemoteConnections_ErrorMessage
          case else errMsgText = L_ErrMsg_Text
          end select
          if extendedDiscReason > 4096 then
             errMsgText = L_ProtocolErrWITHCODE_ErrorMessage  & errMsgText
          end if
       else
          ' no extended error information, use the disconnect code
          select case disconnectCode
          case 0   errMsgText  = L_ErrMsg_Text
          case 1   errMsgText  = L_ErrMsg_Text
          case 2   errMsgText  = L_ErrMsg_Text
          case 260 errMsgText  = L_BadServerName_ErrorMessage
          case 262 errMsgText  = L_LowMemory_ErrorMessage
          case 264 errMsgText  = L_ConnectionTimeout_ErrorMessage
          case 516 errMsgText  = L_NotResponding_ErrorMessage
          case 518 errMsgText  = L_LowMemory_ErrorMessage
          case 520 errMsgText  = L_BadServerName_ErrorMessage
          case 772 errMsgText  = L_NetworkErr_ErrorMessage
          case 774 errMsgText  = L_LowMemory_ErrorMessage
          case 776 errMsgText  = L_BadServerName_ErrorMessage
          case 1028 errMsgText = L_NetworkErr_ErrorMessage
          case 1030 errMsgText = L_SecurityErr_ErrorMessage
          case 1032 errMsgText = L_IllegalServerName_ErrorMessage
          case 1286 errMsgText = L_EncryptionError_ErrorMessage
          case 1288 errMsgText = L_BadServerName_ErrorMessage
          case 1540 errMsgText = L_BadServerName_ErrorMessage
          case 1542 errMsgText = L_SecurityErr_ErrorMessage
          case 1544 errMsgText = L_LowMemory_ErrorMessage
          case 1796 errMsgText = L_NotResponding_ErrorMessage
          case 1798 errMsgText = L_SecurityErr_ErrorMessage
          case 1800 errMsgText = L_CannotLoopBackConnect_ErrorMessage
          case 2052 errMsgText = L_BadServerName_ErrorMessage
          case 2056 errMsgText = L_LicensingNegotFailed_ErrorMessage
          case 2310 errMsgText = L_SecurityErr_ErrorMessage
          case 2566 errMsgText = L_SecurityErr_ErrorMessage
          case 2822 errMsgText = L_EncryptionError_ErrorMessage
          case 3078 errMsgText = L_EncryptionError_ErrorMessage
          case 3080 errMsgText = L_DecompressionFailed_ErrorMessage
          case 3334 errMsgText = L_ProtocolErr_ErrorMessage
          case 10500 errMsgText = L_ProtocolErr_ErrorMessage
          case else errMsgText = L_InternalErr_ErrorMessage
          end select
       end if
       
       msgbox errMsgText,0,L_DisconnectedCaption_ErrorMessage
       ReturnToConnectPage
       
    end sub
    -->
    </script>

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_9) %></h1>
    <div style="margin-left: 5px; margin-right: 5px;">
        <span id="srvNameField"></span>
        <div id="loginArea">
            <table style="width: 100%;">
                <tr>
                    <td>
                        <asp:PlaceHolder runat="server" ID="details">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 170px; font-size: 7pt;">
                                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_10) %>
                                    </td>
                                    <td class="formTable">
                                        <table class="formTable">
                                        <asp:PlaceHolder ID="WarningPlaceHolder" runat="server" Visible="false">
                                        <tr>
                                        <td class="Warning"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_18) %></td>
                                        </tr>
                                        </asp:PlaceHolder>
                                        
                                            <tr>
                                                <td>
                                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AK0_140) %>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="text" name="Server" size="41" id="editServer" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="formTable">
                                            <tr>
                                                <td>
                                                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_11) %>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select size="1" name="comboResolution" id="comboRes">
                                                        <option value="2">
                                                            <id id="option2"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_12) %></id>
                                                        </option>
                                                        <option value="3">
                                                            <id id="option3"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_13) %></id>
                                                        </option>
                                                        <option selected value="4">
                                                            <id id="option4"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_14) %></id>
                                                        </option>
                                                        <option value="5">
                                                            <id id="option5"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_15) %></id>
                                                        </option>
                                                        <option value="6">
                                                            <id id="option6"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_16) %></id>
                                                        </option>
                                                        <option value="1">
                                                            <id id="option1"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB1_17) %></id>
                                                        </option>
                                                    </select>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <orion:LocalizableButton ID="connectbutton" runat="server"  DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VB1_19 %>" OnClientClick="BtnConnect()" Enabled="false"/>
                                    </td>
                                </tr>
                            </table>
                        </asp:PlaceHolder>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="connectArea" style="display: none">
        <center>
            <object language="vbscript" id="MsRdpClient" onreadystatechange="OnControlLoad" classid="CLSID:9059f30f-4eb1-4bd2-9fdc-36f43a218f4a"
                codebase="/msrdp.cab#version=5,1,2600,2180" width="<%= DefaultSanitizer.SanitizeHtml(RdpWidth) %>" height="<%= DefaultSanitizer.SanitizeHtml(RdpHeight) %>">
            </object>
        </center>
    </div>
</asp:Content>
